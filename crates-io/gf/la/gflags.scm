(define-module (crates-io gf la gflags) #:use-module (crates-io))

(define-public crate-gflags-0.0.0 (c (n "gflags") (v "0.0.0") (d (list (d (n "inventory") (r "^0.1") (d #t) (k 0)))) (h "0dvd35pbgb4h50711r88islpf5sivfmjkamf71shrn7l4rlijqfy") (y #t)))

(define-public crate-gflags-0.1.0 (c (n "gflags") (v "0.1.0") (d (list (d (n "gflags-impl") (r "^0.1") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "ref-cast") (r "^0.2") (d #t) (k 0)))) (h "05d445vp5200slj2x8bicmgiwyibbhvm49qia42h4aimg7mx9gwh")))

(define-public crate-gflags-0.1.1 (c (n "gflags") (v "0.1.1") (d (list (d (n "gflags-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "inventory") (r "^0.1.1") (d #t) (k 0)) (d (n "ref-cast") (r "^0.2") (d #t) (k 0)))) (h "1z8j5vfw3sbjsnz5as7rmn50vnjk7lkz6a8b7qwfgkqgix6wjqzh")))

(define-public crate-gflags-0.2.0 (c (n "gflags") (v "0.2.0") (d (list (d (n "argv") (r "^0.1") (d #t) (k 0)) (d (n "gflags-impl") (r "^0.2") (d #t) (k 0)) (d (n "inventory") (r "^0.1.1") (d #t) (k 0)) (d (n "ref-cast") (r "^0.2") (d #t) (k 0)))) (h "0hbc3p50w2r8x8dycdfaslhg6qylhym0ynxsa043amjp454s6l15")))

(define-public crate-gflags-0.3.0 (c (n "gflags") (v "0.3.0") (d (list (d (n "argv") (r "^0.1") (d #t) (k 0)) (d (n "gflags-impl") (r "^0.3") (d #t) (k 0)) (d (n "inventory") (r "^0.1.1") (d #t) (k 0)) (d (n "ref-cast") (r "^0.2") (d #t) (k 0)))) (h "12z8nvdh52j0418znq1x8n1y0c64q01nijr5snmldbgr6ddad5r0")))

(define-public crate-gflags-0.3.1 (c (n "gflags") (v "0.3.1") (d (list (d (n "argv") (r "^0.1") (d #t) (k 0)) (d (n "gflags-impl") (r "^0.3") (d #t) (k 0)) (d (n "inventory") (r "^0.1.1") (d #t) (k 0)) (d (n "ref-cast") (r "^0.2") (d #t) (k 0)))) (h "0fi5cn8mjlmmqigxj4h9ymnsd0qxqcg3yvc3d7dy2ldnv581fpfd")))

(define-public crate-gflags-0.3.2 (c (n "gflags") (v "0.3.2") (d (list (d (n "argv") (r "^0.1") (d #t) (k 0)) (d (n "gflags-impl") (r "= 0.3.2") (d #t) (k 0)) (d (n "inventory") (r "^0.1.1") (d #t) (k 0)) (d (n "ref-cast") (r "^0.2") (d #t) (k 0)))) (h "08qa385gw2jmzj25zxh0p2bxa4i345ffjvgkp8ia4hc79fpk2wdj")))

(define-public crate-gflags-0.3.3 (c (n "gflags") (v "0.3.3") (d (list (d (n "argv") (r "^0.1.1") (d #t) (k 0)) (d (n "gflags-impl") (r "= 0.3.3") (d #t) (k 0)) (d (n "inventory") (r "^0.1.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "05yixzni4r620fq9am6gfdaypbf2zyavq9rk67mmydq33280z89q")))

(define-public crate-gflags-0.3.5 (c (n "gflags") (v "0.3.5") (d (list (d (n "argv") (r "^0.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "gflags-impl") (r "=0.3.5") (d #t) (k 0)) (d (n "inventory") (r "^0.1.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "087kmb94qkfv9spsj4p5zkwwg81pdnmwjk0lxay9y71307r4lpmq")))

(define-public crate-gflags-0.3.6 (c (n "gflags") (v "0.3.6") (d (list (d (n "argv") (r "^0.1.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "gflags-impl") (r "=0.3.6") (d #t) (k 0)) (d (n "inventory") (r "^0.1.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0jlv87jpk1w3fg5ahy24ws8gqha5lp6y7k574776cjfha96ai7l1")))

(define-public crate-gflags-0.3.7 (c (n "gflags") (v "0.3.7") (d (list (d (n "argv") (r "^0.1.3") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "gflags-impl") (r "=0.3.7") (d #t) (k 0)) (d (n "inventory") (r "^0.1.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0547ifr7lfcmgl896hw2an6hn0qbqmvh3hy0rjwlg9ypdyzzzgjr")))

(define-public crate-gflags-0.3.8 (c (n "gflags") (v "0.3.8") (d (list (d (n "argv") (r "^0.1.3") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "gflags-impl") (r "=0.3.8") (d #t) (k 0)) (d (n "inventory") (r "^0.1.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "081m5vjnqlpgj806rmhjc19qy0a4az9spdv8nyvs24f7xpkbphyr")))

(define-public crate-gflags-0.3.9 (c (n "gflags") (v "0.3.9") (d (list (d (n "argv") (r "^0.1.3") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "gflags-impl") (r "=0.3.9") (d #t) (k 0)) (d (n "inventory") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^2.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "076kf9cr1aalca504g51kcdckq3slhrhjplirqz1jjj1xy8jz51m") (r "1.37")))

(define-public crate-gflags-0.3.10 (c (n "gflags") (v "0.3.10") (d (list (d (n "argv") (r "^0.1.3") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "gflags-impl") (r "=0.3.10") (d #t) (k 0)) (d (n "inventory") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^2.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0affcqgd3rs2wy6j1ndr394ah2zjxvnia5nrwhrzqv14paqznakc") (r "1.37")))

(define-public crate-gflags-0.3.11 (c (n "gflags") (v "0.3.11") (d (list (d (n "argv") (r "^0.1.3") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "gflags-impl") (r "=0.3.11") (d #t) (k 0)) (d (n "inventory") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^2.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "18giimh4ba7fx12xfizsl2xs8v01y7sp5vgr66xisyg35y75r0wy") (r "1.37")))

(define-public crate-gflags-0.3.12 (c (n "gflags") (v "0.3.12") (d (list (d (n "argv") (r "^0.1.3") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "gflags-impl") (r "=0.3.12") (d #t) (k 0)) (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "predicates") (r "^2.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "06k2ip1fdrnj93x4l14kgfmknfy3w3pamcq079skxck9bmnx2cc3") (r "1.37")))

