(define-module (crates-io gf la gflags-impl) #:use-module (crates-io))

(define-public crate-gflags-impl-0.1.0 (c (n "gflags-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1kir28qajk39dbcn1psq2gjrss22i6qdn2pg4sih040kg7k5m5n9")))

(define-public crate-gflags-impl-0.1.1 (c (n "gflags-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1lg2ciyxg0hjnavlfcpn4cicfiym807yc8w3vv4v7p3vqkvhshxg")))

(define-public crate-gflags-impl-0.2.0 (c (n "gflags-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0xi61v79knqd8y2lqyjk53bg5q5n9j9ipi939jhzzyi9rk6ny9cq")))

(define-public crate-gflags-impl-0.3.0 (c (n "gflags-impl") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1chf146kra65kx4hv2p0kd773zmhcgvapdpk58p9hwbi6dmvykj9")))

(define-public crate-gflags-impl-0.3.1 (c (n "gflags-impl") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0jr5yqqbzps509ivmph21rz86cv755czjl6jd6ds4qf3gl98w4hp")))

(define-public crate-gflags-impl-0.3.2 (c (n "gflags-impl") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vlykky1ij2rw8mdiqqn8niqsw9rlbfx4dgpzw8avabk47hq2cf2")))

(define-public crate-gflags-impl-0.3.3 (c (n "gflags-impl") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jxxda959idalgfka93ib455na6pdzanrbkn3pp72ac5a3jd2arq")))

(define-public crate-gflags-impl-0.3.4 (c (n "gflags-impl") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yzajnjwna5jwsnxp5g37aab7shnqpiybykcjbb4582krqiwhvpp")))

(define-public crate-gflags-impl-0.3.5 (c (n "gflags-impl") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wsa827zhik4a5ga801mi45w320icb1aj6cdi2hvyjqg562km699")))

(define-public crate-gflags-impl-0.3.6 (c (n "gflags-impl") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cr351qzv40q0nasgrh75rlrpd42rfmkq2nsv5rf6nwjnib3ywy4")))

(define-public crate-gflags-impl-0.3.7 (c (n "gflags-impl") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dylpzjsfb9h2lmfcz44x7a0iqcjwlfdhvpfk1y6fn4achrivaq7")))

(define-public crate-gflags-impl-0.3.8 (c (n "gflags-impl") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "137haz5r8yyw4n7dpks14krcpdwkp4pqw2m1jjlw8fnbbkkac4zc")))

(define-public crate-gflags-impl-0.3.9 (c (n "gflags-impl") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r96v6vmwhj1bddq8kmqsah7d08vnq6s76fma3ngwbcrvzbyn15s") (r "1.37")))

(define-public crate-gflags-impl-0.3.10 (c (n "gflags-impl") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18znzqh52xmhii36cyj228pprhhs140xbw6n5hkiys8s6chisdbf") (r "1.37")))

(define-public crate-gflags-impl-0.3.11 (c (n "gflags-impl") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z1g9m3hmij72cm4ldwff3irwz608qxqw62m6czh4xwavfi4lhr7") (r "1.37")))

(define-public crate-gflags-impl-0.3.12 (c (n "gflags-impl") (v "0.3.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qc58js3gys0npcmpvmf6i67y0klgzm2hsdgkx0paa6kfv2nsx0w") (r "1.37")))

