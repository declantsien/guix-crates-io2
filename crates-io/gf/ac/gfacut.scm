(define-module (crates-io gf ac gfacut) #:use-module (crates-io))

(define-public crate-gfacut-0.1.0 (c (n "gfacut") (v "0.1.0") (d (list (d (n "gfaR") (r "^0.1.2") (d #t) (k 0)))) (h "0rrmfiivkgw498s9hyz3b0dwwahjgqkihjw8nb9y1k3qnzn845c8")))

(define-public crate-gfacut-0.1.1 (c (n "gfacut") (v "0.1.1") (d (list (d (n "gfaR") (r "^0.1.2") (d #t) (k 0)))) (h "0x3i56d7bydj4lcsd370793xswg0lhqd2g3hmzqjx6kqg8nqdxgp")))

(define-public crate-gfacut-0.1.3 (c (n "gfacut") (v "0.1.3") (d (list (d (n "gfaR") (r "^0.1.2") (d #t) (k 0)))) (h "0kqp9jwaxhipvdh2y3ds1qyih006skx7vbhcz6nsrkyrrdayb804")))

(define-public crate-gfacut-0.1.4 (c (n "gfacut") (v "0.1.4") (d (list (d (n "gfaR") (r "^0.1.2") (d #t) (k 0)))) (h "07jih540s38m2gajnsw4qbr20rww3vhzf2yid8x6fpr81dhyxq54")))

