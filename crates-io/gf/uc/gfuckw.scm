(define-module (crates-io gf uc gfuckw) #:use-module (crates-io))

(define-public crate-gfuckw-0.0.1 (c (n "gfuckw") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "07rz6h73h8lz12vp0p5wagn4a0p1xmph77xwz8qc3583z00ql7hp") (y #t)))

(define-public crate-gfuckw-0.2.0 (c (n "gfuckw") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "smol-timeout") (r "^0.6.0") (d #t) (k 0)))) (h "0isqqb7283jw376w5kidsgl82g9hgas4mci5mj97lyqp6im3a0zi") (y #t)))

(define-public crate-gfuckw-0.2.1 (c (n "gfuckw") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "smol-timeout") (r "^0.6.0") (d #t) (k 0)))) (h "04zr1q6g62dvmwlfqvjbp7z921a8k2ngpqrcbchfg7jxgh0m217n") (y #t)))

(define-public crate-gfuckw-0.2.2 (c (n "gfuckw") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "smol-timeout") (r "^0.6.0") (d #t) (k 0)))) (h "0fmvx4fkmli5fkii4mq4wszm8z4kzdg7i5mk0pv45qigj9c15yr6") (y #t)))

(define-public crate-gfuckw-0.2.3 (c (n "gfuckw") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "smol-timeout") (r "^0.6.0") (d #t) (k 0)))) (h "0gp18h5h6v6ryvknwaw4dl0rqlwlzlycmb1m7byxi40050p2j27a")))

(define-public crate-gfuckw-0.2.4-alpha0 (c (n "gfuckw") (v "0.2.4-alpha0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "smol-timeout") (r "^0.6.0") (d #t) (k 0)))) (h "1dip9w4fmvsqig8x4fa9s57hninapykh6ldkfmmd7gcnl9w6ain6")))

(define-public crate-gfuckw-0.2.5 (c (n "gfuckw") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "smol-timeout") (r "^0.6.0") (d #t) (k 0)))) (h "1b4k2naxgkkjzpfc4l5zk5nw2ayb7dxfx0vjibckk3zdr1hfidvk")))

(define-public crate-gfuckw-0.2.6 (c (n "gfuckw") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "smol-timeout") (r "^0.6.0") (d #t) (k 0)))) (h "1wfskf5w4db405q156hs316p7spqmg4k93ghajb5s2f2km35navw")))

(define-public crate-gfuckw-0.2.7 (c (n "gfuckw") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "smol-timeout") (r "^0.6.0") (d #t) (k 0)))) (h "0vfndjkbflr72xxnjhnknalimds1d6qdwj59kd0l45pzl9hfm29l")))

