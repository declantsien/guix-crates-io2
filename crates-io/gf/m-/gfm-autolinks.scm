(define-module (crates-io gf m- gfm-autolinks) #:use-module (crates-io))

(define-public crate-gfm-autolinks-0.1.0 (c (n "gfm-autolinks") (v "0.1.0") (d (list (d (n "once_cell") (r ">=1.0.1, <2") (d #t) (k 0)) (d (n "rstest") (r "^0.17") (d #t) (k 2)) (d (n "unicode_categories") (r "^0.1") (d #t) (k 0)))) (h "00v0nlils304a2n1yawg3kmc2x76gs1p38css06m6g7jdcdpzgh6")))

(define-public crate-gfm-autolinks-0.2.0 (c (n "gfm-autolinks") (v "0.2.0") (d (list (d (n "once_cell") (r ">=1.0.1, <2") (d #t) (k 0)) (d (n "rstest") (r "^0.17") (d #t) (k 2)) (d (n "unicode_categories") (r "^0.1") (d #t) (k 0)))) (h "0maxadskmzc75m4gbhl5i4r0nwx516ry3sac98rg4a6yxmy40ng7")))

