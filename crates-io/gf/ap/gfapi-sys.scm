(define-module (crates-io gf ap gfapi-sys) #:use-module (crates-io))

(define-public crate-gfapi-sys-0.1.0 (c (n "gfapi-sys") (v "0.1.0") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0i63imgvq60klv8d3wmb8ilnzvghm3f9hdg4m5lv05368smv13xh")))

(define-public crate-gfapi-sys-0.2.1 (c (n "gfapi-sys") (v "0.2.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ya7him9wn1zqa9g998a2c8q22q5gr9axfkhh0vdb78k0fvw43nz")))

(define-public crate-gfapi-sys-0.2.2 (c (n "gfapi-sys") (v "0.2.2") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n4njhpkayd22rnmrc0vwpm09r6s8hzpy1gh1ydn7m1cixsakf15")))

(define-public crate-gfapi-sys-0.2.3 (c (n "gfapi-sys") (v "0.2.3") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nrs3ymg2fazw0ip3rlzj7z6gcbplfjdgaqx9w8c3sb8fmfghv33")))

(define-public crate-gfapi-sys-0.2.4 (c (n "gfapi-sys") (v "0.2.4") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "087p27rbawgc4crssxhc22m2kfsmzfr0qxy18n4mx4693c314mjx")))

(define-public crate-gfapi-sys-0.2.5 (c (n "gfapi-sys") (v "0.2.5") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.4") (d #t) (k 0)))) (h "1wjhajfaw5zm688vg4sz53kj0kmsj39wh8sk5276vg6r55gvnkh1")))

(define-public crate-gfapi-sys-0.2.6 (c (n "gfapi-sys") (v "0.2.6") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.4") (d #t) (k 0)))) (h "0c6y03iazmkgq2ybc7jwc24d8fgkzb2spnr5wdzy3v78ibhwajrk")))

(define-public crate-gfapi-sys-0.2.7 (c (n "gfapi-sys") (v "0.2.7") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.4") (d #t) (k 0)))) (h "0l6brr22ay6hfy9zigqql5fz0yq6w5nz9rxw5c9p5z3jhpizyx6n")))

(define-public crate-gfapi-sys-0.2.8 (c (n "gfapi-sys") (v "0.2.8") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.4") (d #t) (k 0)))) (h "0nwrh7gz6xswxxhabf3scd9m87bc9lx1xarzc1ccicw76gz4lhbd")))

(define-public crate-gfapi-sys-0.2.9 (c (n "gfapi-sys") (v "0.2.9") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.4") (d #t) (k 0)))) (h "0xjfa9l3wvx8r9zhixi642cc2znlap938z555hydpx9pn2dg8gsb")))

(define-public crate-gfapi-sys-0.2.10 (c (n "gfapi-sys") (v "0.2.10") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi") (r "^0.4") (d #t) (k 0)))) (h "1vqp9a511lp3xpyrbbll0ag7n6ndgk82y1jl64ajyh211q3ysf0p")))

(define-public crate-gfapi-sys-0.3.0 (c (n "gfapi-sys") (v "0.3.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bbkvpv9pj4ly6qr5lxzvxbmksxlnhx726b34i3km1ah3l814y6n")))

(define-public crate-gfapi-sys-0.3.1 (c (n "gfapi-sys") (v "0.3.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "049szw93f2hmbi1hggfia71s8pxkjzr0n0w08bmgrc8iki5wmm1k")))

(define-public crate-gfapi-sys-0.3.2 (c (n "gfapi-sys") (v "0.3.2") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "1zsv000d2sdfdjb8mgdbx0xqz0wqpwlwlb2w500yc7jgpd0095yc")))

(define-public crate-gfapi-sys-0.3.3 (c (n "gfapi-sys") (v "0.3.3") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "1bs7x5hgvrahlh8w25b6qngx20kshddmlca1wrznqf8mfiaq97p6")))

(define-public crate-gfapi-sys-0.3.4 (c (n "gfapi-sys") (v "0.3.4") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "0f4y5kzzqdvwkpvf51fdgkj7v10jxg39yy386lsl1i8pmdq098wk")))

(define-public crate-gfapi-sys-0.3.5 (c (n "gfapi-sys") (v "0.3.5") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "0wl4b39lq2x5whs9yp3gfqc2m9fxhpl01p5xgc6mrxc4r76i4s3k")))

(define-public crate-gfapi-sys-0.3.6 (c (n "gfapi-sys") (v "0.3.6") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "uuid") (r "~0.4") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0di4yjb5f6gvf7hwzl2rn486kpy89ym5il2wbk9wilah7f4ip8fw")))

(define-public crate-gfapi-sys-1.0.0 (c (n "gfapi-sys") (v "1.0.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "uuid") (r "~0.4") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0n4pawc8jrls78l9k6viam2vpx0llfnbax6xrqvmflv3l4426vny")))

(define-public crate-gfapi-sys-1.0.1 (c (n "gfapi-sys") (v "1.0.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "uuid") (r "~0.4") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0yk40qqv4yb9k5yzmkcx4skfi9yjgapily46d5hs6xn0gg60wa2m")))

(define-public crate-gfapi-sys-1.0.2 (c (n "gfapi-sys") (v "1.0.2") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)))) (h "1ykwrmpbmdw0c9jj4rkpdsb8mbagvvahb4fwbwx90binaz1b1hpx")))

(define-public crate-gfapi-sys-1.0.3 (c (n "gfapi-sys") (v "1.0.3") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)))) (h "1ch6jx9nr18mp7zmaqcxv4m2lpsjg8p04sp6zw4drhrhpg9cyaxq")))

(define-public crate-gfapi-sys-2.0.0 (c (n "gfapi-sys") (v "2.0.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)))) (h "0jkw808gv341xa3zxsnjcgda9ry4s8blrimsvyfc7ar2vmf4a9sh")))

(define-public crate-gfapi-sys-2.0.1 (c (n "gfapi-sys") (v "2.0.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("std"))) (d #t) (k 0)))) (h "1h4dy3c0yh5f28r48n87miw7vc9bxm47ak7v5g0agq7r8g3xgq8y")))

(define-public crate-gfapi-sys-3.0.0 (c (n "gfapi-sys") (v "3.0.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("std"))) (d #t) (k 0)))) (h "1dx2sxxjv7cgqfsdnihmd97hwn9gdpi27yw7xs06j2ksbzyv9s27")))

(define-public crate-gfapi-sys-3.0.1 (c (n "gfapi-sys") (v "3.0.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("std"))) (d #t) (k 0)))) (h "191jssgi0xfxzkx2wkk0vwny1858x6v7cc98q8zzk79nl0c68ann")))

(define-public crate-gfapi-sys-4.0.0 (c (n "gfapi-sys") (v "4.0.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("std"))) (d #t) (k 0)))) (h "0lkn5d5186q4ac7q2ix31fsvx5pn4navd141zgi0900g81wys0kb")))

(define-public crate-gfapi-sys-4.0.1 (c (n "gfapi-sys") (v "4.0.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("std"))) (d #t) (k 0)))) (h "03nmwggxn20llhsh04659m36bnyamnq1n0agm0rr0cj1bvh1njh7")))

