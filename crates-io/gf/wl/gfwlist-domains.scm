(define-module (crates-io gf wl gfwlist-domains) #:use-module (crates-io))

(define-public crate-gfwlist-domains-0.1.0 (c (n "gfwlist-domains") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "publicsuffix") (r "^1.5.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)))) (h "02ml707pn0cwv4az0sb76rslgi666yr23wwaz1fn9rvgw5y0v2lh")))

(define-public crate-gfwlist-domains-0.1.1 (c (n "gfwlist-domains") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "publicsuffix") (r "^1.5.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)))) (h "0xr8bzmm386bhyzbnm1mh8irra7izxyxajrjc2ym6lphvdji3q70")))

(define-public crate-gfwlist-domains-0.1.2 (c (n "gfwlist-domains") (v "0.1.2") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "publicsuffix") (r "^1.5.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)))) (h "0bjdl6x6wb449rwhfgsc0wpwmikyi8harqlbmwwqcxmbs7nrkg1y")))

