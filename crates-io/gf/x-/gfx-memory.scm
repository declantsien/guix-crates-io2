(define-module (crates-io gf x- gfx-memory) #:use-module (crates-io))

(define-public crate-gfx-memory-0.1.0 (c (n "gfx-memory") (v "0.1.0") (d (list (d (n "colorful") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hal") (r "^0.5") (d #t) (k 0) (p "gfx-hal")) (d (n "hibitset") (r "^0.6") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0npwcg6q9qbpwhfqwidfkc62azddg4dcrff8pihrb10wb9dd1q0j")))

(define-public crate-gfx-memory-0.1.1 (c (n "gfx-memory") (v "0.1.1") (d (list (d (n "colorful") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hal") (r "^0.5") (d #t) (k 0) (p "gfx-hal")) (d (n "hibitset") (r "^0.6") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0afd5mn3h3mz273qkva36gaxxalps99xxbsbz944mgagvn8zbyih")))

(define-public crate-gfx-memory-0.1.2 (c (n "gfx-memory") (v "0.1.2") (d (list (d (n "colorful") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hal") (r "^0.5") (d #t) (k 0) (p "gfx-hal")) (d (n "hibitset") (r "^0.6") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1052frn5wzicz0xqh0pdjdjs72zblpyjx37wri0jq0v56cwgxglx")))

(define-public crate-gfx-memory-0.1.3 (c (n "gfx-memory") (v "0.1.3") (d (list (d (n "colorful") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hal") (r "^0.5") (d #t) (k 0) (p "gfx-hal")) (d (n "hibitset") (r "^0.6") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0dv18nmszsc7z426j8rxacj22aa4spdh5492j96wvnbllv6xdvn2")))

(define-public crate-gfx-memory-0.2.0 (c (n "gfx-memory") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hal") (r "^0.6") (d #t) (k 0) (p "gfx-hal")) (d (n "hibitset") (r "^0.6") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "17vaay4hnici023rpdd39rj1v0jvdlsr0pknibmkix07vxaqi3gy")))

(define-public crate-gfx-memory-0.2.1 (c (n "gfx-memory") (v "0.2.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hal") (r "^0.6") (d #t) (k 0) (p "gfx-hal")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0f0hqk0s45dv57if57v5prblcmwmnmwknqlyw00vvv767kh4rglh")))

(define-public crate-gfx-memory-0.2.2 (c (n "gfx-memory") (v "0.2.2") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hal") (r "^0.6") (d #t) (k 0) (p "gfx-hal")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "01cgv274qxa2afc5wvv0n233ly42b85wf5fbl962yh9r5dfxmkfw")))

