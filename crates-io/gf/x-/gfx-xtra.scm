(define-module (crates-io gf x- gfx-xtra) #:use-module (crates-io))

(define-public crate-gfx-xtra-0.1.0 (c (n "gfx-xtra") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "14rwg066nrh5qd2c6yzjlk82xif92fjkc6ywg5q5dl5bm5zcpl15") (r "1.65")))

(define-public crate-gfx-xtra-0.1.1 (c (n "gfx-xtra") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0mm4s01gj38ldxgqc94l6bqrv90dlbx250fqg2vhsw0yapr551db") (r "1.65")))

(define-public crate-gfx-xtra-0.2.0 (c (n "gfx-xtra") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1q4y4dabn7y5n43mmnhndnardl2ya9lq22qq1sdvj5a9flm3c9f2") (r "1.65")))

