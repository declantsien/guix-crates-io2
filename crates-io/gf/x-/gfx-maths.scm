(define-module (crates-io gf x- gfx-maths) #:use-module (crates-io))

(define-public crate-gfx-maths-0.1.0 (c (n "gfx-maths") (v "0.1.0") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)))) (h "10i5m14h30v6f9fy7xck8126qzpbhwzxczi6q603n0gz9j7ynzx8") (f (quote (("mat-vulkan") ("mat-row-major") ("default" "mat-vulkan"))))))

(define-public crate-gfx-maths-0.2.0 (c (n "gfx-maths") (v "0.2.0") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)))) (h "18h5a5kbw675h2rijps5927pyqzqiygzkmbd9jdlsyrqhhz4vzp8") (f (quote (("mat-row-major") ("default"))))))

(define-public crate-gfx-maths-0.2.1 (c (n "gfx-maths") (v "0.2.1") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)))) (h "0g0lv6smc1d31glsk4k9ajqr8nh4kgcxfwan804v3sdx5qjpsj26") (f (quote (("mat-row-major") ("default"))))))

(define-public crate-gfx-maths-0.2.2 (c (n "gfx-maths") (v "0.2.2") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jprr87arbqld0vkybr12kkccaz5v8bl1blsfhfn4wl3v3j28aw0") (f (quote (("mat-row-major") ("default"))))))

(define-public crate-gfx-maths-0.2.3 (c (n "gfx-maths") (v "0.2.3") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wfa5vj34bj6zq0pvr3x45m3i8f909hsy8wqjx218lx4nhd0wi4s") (f (quote (("mat-row-major") ("default"))))))

(define-public crate-gfx-maths-0.2.4 (c (n "gfx-maths") (v "0.2.4") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zmvmi8pzn886xblmqp2dpm0r48v8f72ssfxz3prbif1s6ms41r6") (f (quote (("mat-row-major") ("default"))))))

(define-public crate-gfx-maths-0.2.5 (c (n "gfx-maths") (v "0.2.5") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gvxcr1gpjmx300h1z57v6l5a968pnxxp3m43vd60xx9iiajvq7i") (f (quote (("mat-row-major") ("default"))))))

(define-public crate-gfx-maths-0.2.6 (c (n "gfx-maths") (v "0.2.6") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "158rqgymv25w9y22yv7wfir5jq09lbpbba5jmbzb0mazg23fkg5c") (f (quote (("mat-row-major") ("default"))))))

(define-public crate-gfx-maths-0.2.7 (c (n "gfx-maths") (v "0.2.7") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gkn1swmc4qa8dm0m0lzanhp214hcqsac57bam7py5ilyc8scx4n") (f (quote (("swizzle" "paste") ("mat-row-major") ("default")))) (y #t)))

(define-public crate-gfx-maths-0.2.8 (c (n "gfx-maths") (v "0.2.8") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kqwg60dqz408cgvg565dpl9b2p3qh5rsdgin2zfqjd0mmirilhl") (f (quote (("swizzle" "paste") ("mat-row-major") ("default"))))))

(define-public crate-gfx-maths-0.2.9 (c (n "gfx-maths") (v "0.2.9") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "170qa3w3lh5y5rqmdgpiqdd70gm7a05832rzw9h0xna1gd8vlyvm") (f (quote (("swizzle" "paste") ("mat-row-major") ("default"))))))

