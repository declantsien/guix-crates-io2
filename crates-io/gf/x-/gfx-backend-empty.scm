(define-module (crates-io gf x- gfx-backend-empty) #:use-module (crates-io))

(define-public crate-gfx-backend-empty-0.1.0 (c (n "gfx-backend-empty") (v "0.1.0") (d (list (d (n "gfx-hal") (r "^0.1") (d #t) (k 0)))) (h "0z4ikdg4231hjb4m5wgrql45vvf2hs4p60damcaa15gdg8lnic3b")))

(define-public crate-gfx-backend-empty-0.1.1 (c (n "gfx-backend-empty") (v "0.1.1") (d (list (d (n "gfx-hal") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0k7b3g6g1j9rancywjkvjj57a6jjgar55nj8lx78xf48kwv1a32r")))

(define-public crate-gfx-backend-empty-0.2.0 (c (n "gfx-backend-empty") (v "0.2.0") (d (list (d (n "gfx-hal") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.19") (o #t) (d #t) (k 0)))) (h "1dbz0kgi9q9pysngnhs6bing83jfcspyk02kj3ja0pf762vp1na8")))

(define-public crate-gfx-backend-empty-0.2.1 (c (n "gfx-backend-empty") (v "0.2.1") (d (list (d (n "gfx-hal") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.19") (o #t) (d #t) (k 0)))) (h "1d0426xz9bdkh8svy0cv2f5cm0wddm51whk3c2qg8p6silslqcma")))

(define-public crate-gfx-backend-empty-0.3.0 (c (n "gfx-backend-empty") (v "0.3.0") (d (list (d (n "gfx-hal") (r "^0.3") (d #t) (k 0)) (d (n "winit") (r "^0.19") (o #t) (d #t) (k 0)))) (h "16rhm9r1wr163yrdi1wapnsska3bcwv72klijl4npfd5mb06kvhr")))

(define-public crate-gfx-backend-empty-0.3.1 (c (n "gfx-backend-empty") (v "0.3.1") (d (list (d (n "gfx-hal") (r "^0.3.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r "^0.19") (o #t) (d #t) (k 0)))) (h "02kgssqqchabc1rqnc948sp1fgdibgxxjbdkbmazi4jgs0f0vx6d")))

(define-public crate-gfx-backend-empty-0.4.0 (c (n "gfx-backend-empty") (v "0.4.0") (d (list (d (n "gfx-hal") (r "^0.4") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)))) (h "1iqc8wjyyl0wrn84cnbdiy7jkv0yzlwh38lqs8vwnrw8qimkwf1x")))

(define-public crate-gfx-backend-empty-0.5.0 (c (n "gfx-backend-empty") (v "0.5.0") (d (list (d (n "gfx-hal") (r "^0.5") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)))) (h "11fv3khz9qx6nvr0jd1c6y9c4a8cn6imzg6svdyjaaq2pkbx4yxn")))

(define-public crate-gfx-backend-empty-0.5.1 (c (n "gfx-backend-empty") (v "0.5.1") (d (list (d (n "gfx-hal") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)))) (h "01zriqnjn52nrbk0q9qrsc19clyhd4mgl9xrki5v6f3sdm6a1m41")))

(define-public crate-gfx-backend-empty-0.5.2 (c (n "gfx-backend-empty") (v "0.5.2") (d (list (d (n "gfx-hal") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)))) (h "01szjhiml4qhdngz5kf14ni4c77m2f1db8dphcaq05ir4qmr43rf")))

(define-public crate-gfx-backend-empty-0.6.0 (c (n "gfx-backend-empty") (v "0.6.0") (d (list (d (n "gfx-hal") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)))) (h "0z7p331csqz2zm26iy7awfdnmd6ys3r2kj00m5bnd3xp29y25190")))

(define-public crate-gfx-backend-empty-0.7.0 (c (n "gfx-backend-empty") (v "0.7.0") (d (list (d (n "gfx-hal") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)))) (h "14bsr9ycimhpv011p9fn1cxvpl80a547yn2cggfwym2rlqkfy1wz")))

(define-public crate-gfx-backend-empty-0.8.0 (c (n "gfx-backend-empty") (v "0.8.0") (d (list (d (n "gfx-hal") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)))) (h "0phs0jvzskkdngn6cbsff9gxmmrn9lrfl7j2ndwm9lzw9gd5mi9a")))

(define-public crate-gfx-backend-empty-0.9.0 (c (n "gfx-backend-empty") (v "0.9.0") (d (list (d (n "gfx-hal") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)))) (h "1x1092a4za4frgk6kn55qal3zlk1z7fwkj8dl25934bpqh9zij19")))

