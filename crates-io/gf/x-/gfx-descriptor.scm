(define-module (crates-io gf x- gfx-descriptor) #:use-module (crates-io))

(define-public crate-gfx-descriptor-0.1.0 (c (n "gfx-descriptor") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hal") (r "^0.5") (d #t) (k 0) (p "gfx-hal")) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nn5yafnag6j151vhm6r5fcjags586255mv87vk5dg6icrfmzwqv")))

(define-public crate-gfx-descriptor-0.2.0 (c (n "gfx-descriptor") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hal") (r "^0.6") (d #t) (k 0) (p "gfx-hal")) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0sqcws7r7x5bg4i39fbrf99kf4762xqy545487apkwh0s3y7m36d")))

