(define-module (crates-io gf x- gfx-auxil) #:use-module (crates-io))

(define-public crate-gfx-auxil-0.1.0 (c (n "gfx-auxil") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hal") (r "^0.4") (d #t) (k 0) (p "gfx-hal")) (d (n "spirv_cross") (r "^0.16") (d #t) (k 0)))) (h "068r6970mn53dzzpfg5ci650b13p4xfxjkryzsfcj8ws5aaywbjp")))

(define-public crate-gfx-auxil-0.3.0 (c (n "gfx-auxil") (v "0.3.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hal") (r "^0.5") (d #t) (k 0) (p "gfx-hal")) (d (n "spirv_cross") (r "^0.18") (o #t) (d #t) (k 0)))) (h "1j45wwv1qkyb02apma8hcyriradavkr20y6i12za0c0k0gqfciiv")))

(define-public crate-gfx-auxil-0.4.0 (c (n "gfx-auxil") (v "0.4.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hal") (r "^0.5") (d #t) (k 0) (p "gfx-hal")) (d (n "spirv_cross") (r "^0.20") (o #t) (d #t) (k 0)))) (h "13ks2wf8yb8j6rmk0q9pg899bb5kianpjwqdb87cg0v8in7bzgb7")))

(define-public crate-gfx-auxil-0.5.0 (c (n "gfx-auxil") (v "0.5.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hal") (r "^0.6") (d #t) (k 0) (p "gfx-hal")) (d (n "spirv_cross") (r "^0.20") (o #t) (d #t) (k 0)))) (h "1c9fn68m69xxfakgzw0ck4vyr278rg93qlf4jjpcid53qwyfw4b3")))

(define-public crate-gfx-auxil-0.6.0 (c (n "gfx-auxil") (v "0.6.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hal") (r "^0.6") (d #t) (k 0) (p "gfx-hal")) (d (n "spirv_cross") (r "^0.21") (o #t) (d #t) (k 0)))) (h "03qv9kdh2qi6apmh5kdinfykz2nw4idb325nh28l7iin42ii5h3y")))

(define-public crate-gfx-auxil-0.7.0 (c (n "gfx-auxil") (v "0.7.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hal") (r "^0.6") (d #t) (k 0) (p "gfx-hal")) (d (n "spirv_cross") (r "^0.22") (o #t) (d #t) (k 0)))) (h "05hj3bn7yrmq4zd3jvrvhi8mmf8fb23mncmr8n2z0w19b5mrbk87")))

(define-public crate-gfx-auxil-0.8.0 (c (n "gfx-auxil") (v "0.8.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hal") (r "^0.7") (d #t) (k 0) (p "gfx-hal")) (d (n "spirv_cross") (r "^0.23") (o #t) (d #t) (k 0)))) (h "0v8qh2sppqix5ym4qcgczz8yn8zjwprb1jciimk1f8bz0v7kxcz7")))

(define-public crate-gfx-auxil-0.9.0 (c (n "gfx-auxil") (v "0.9.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hal") (r "^0.8") (d #t) (k 0) (p "gfx-hal")) (d (n "spirv_cross") (r "^0.23") (o #t) (d #t) (k 0)))) (h "1krriplv42hjxgj2qhschxrf2qhlmvivwrkl6csglkcrr48qgkww")))

(define-public crate-gfx-auxil-0.10.0 (c (n "gfx-auxil") (v "0.10.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hal") (r "^0.9") (d #t) (k 0) (p "gfx-hal")) (d (n "spirv_cross") (r "^0.23") (o #t) (d #t) (k 0)))) (h "075bn42rys05nxh7dyjyh1mmaxdxqb3panh7h876hhnn24drk50n")))

