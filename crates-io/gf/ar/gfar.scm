(define-module (crates-io gf ar gfar) #:use-module (crates-io))

(define-public crate-gfaR-0.1.0 (c (n "gfaR") (v "0.1.0") (h "0g7rrxjgww2pyjfi8impxb619jxzcnxqhmbrqiwbw5vg3y6aq3ii") (y #t)))

(define-public crate-gfaR-0.1.1 (c (n "gfaR") (v "0.1.1") (h "1bzw2ikqss24n8lrhwh216r0w8wjmf8z9aslvmbbqx5rp8yj919h")))

(define-public crate-gfaR-0.1.2 (c (n "gfaR") (v "0.1.2") (h "0g80b11yqlic8smsgl0gji64ad6pyz6zcb0zzxvgaj1bxpi2hzid")))

(define-public crate-gfaR-0.1.3 (c (n "gfaR") (v "0.1.3") (h "1y5pyicrywl6g1c9kwib0h4z1ci45s0pm6cb32ic79b5j2g7r5vc")))

