(define-module (crates-io gf fk gffkit) #:use-module (crates-io))

(define-public crate-gffkit-0.1.1 (c (n "gffkit") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "noodles-gff") (r "^0.18.0") (d #t) (k 0)))) (h "0s0dr1wpv3vknfh9bz2hs6nlr397vsgb7f85md246y0zbijpqb33") (r "1.71.0")))

(define-public crate-gffkit-0.1.2 (c (n "gffkit") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "bio") (r "^1.6.0") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "human-sort") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "noodles") (r "^0.74.0") (f (quote ("gff" "gtf" "fasta" "core"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (d #t) (k 0)))) (h "0ifxlx3ckmzgv8297fsp0fr7wl0mav396kcsir1skqjds6n4lc2p") (r "1.71.0")))

