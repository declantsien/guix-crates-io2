(define-module (crates-io gf aa gfaas-cli) #:use-module (crates-io))

(define-public crate-gfaas-cli-0.1.0 (c (n "gfaas-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "13k5g5fhrxdvq9bgpk8cpbhpp4gwa3v5p4800mv72spi0isd0gqv")))

(define-public crate-gfaas-cli-0.1.1 (c (n "gfaas-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05iqq17v9lwkj0abmvdl129pa5y84f9x41y7f31bbrkly6lz609c")))

(define-public crate-gfaas-cli-0.1.2 (c (n "gfaas-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0k807ib9lliqynx657ill9r3jjywzd2wi5c0ski78kcx907cj368")))

(define-public crate-gfaas-cli-0.1.3 (c (n "gfaas-cli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02vnxryq1cflm0i3a1g6kcf2g0jbd4ca6q8cm72bnbva9mzbfv4i")))

(define-public crate-gfaas-cli-0.2.0 (c (n "gfaas-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1g52b9yy27fix58pfj6wd05gsf2a78wjxmgsig60l4zmima9lxjh")))

