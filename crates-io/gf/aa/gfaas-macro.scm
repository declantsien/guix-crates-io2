(define-module (crates-io gf aa gfaas-macro) #:use-module (crates-io))

(define-public crate-gfaas-macro-0.1.0 (c (n "gfaas-macro") (v "0.1.0") (d (list (d (n "appdirs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kh3q8zx9bh3xnqxk2qf3mrl4h4fv1sgf62rhhlr3jl0df6jaqqa")))

(define-public crate-gfaas-macro-0.1.1 (c (n "gfaas-macro") (v "0.1.1") (d (list (d (n "appdirs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1izvglnq89lf4nimzdwbxlbav0m0c1nkch7nr7nkscq60wxcxb3v")))

(define-public crate-gfaas-macro-0.2.0 (c (n "gfaas-macro") (v "0.2.0") (d (list (d (n "appdirs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xlaxrs3grv68i4nh24isbdmjy9l7ss71qyjk1qs31qq20knrwd0")))

(define-public crate-gfaas-macro-0.3.0 (c (n "gfaas-macro") (v "0.3.0") (d (list (d (n "appdirs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cwic49qhg0039xmgfbqd451mq645n288bbjvpmc83923xg0l43v")))

