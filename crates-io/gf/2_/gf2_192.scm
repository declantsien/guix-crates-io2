(define-module (crates-io gf #{2_}# gf2_192) #:use-module (crates-io))

(define-public crate-gf2_192-0.15.0 (c (n "gf2_192") (v "0.15.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kbhxq2hgabrf5382jnl0r9msj0s2n61wcwnrz9hw4n3bv3cwn6a")))

(define-public crate-gf2_192-0.16.0 (c (n "gf2_192") (v "0.16.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0yc3ravzysa9c8c0xzc3hmgwpz2ph0f0vwnrnz4kplx10g0b9r5l")))

(define-public crate-gf2_192-0.16.1 (c (n "gf2_192") (v "0.16.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0s6nyqvdb2cg31g126mg23794qci9kz78njh13cn3z9fa8azv44g")))

(define-public crate-gf2_192-0.17.0 (c (n "gf2_192") (v "0.17.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0c2ikyz37zqjsd81xw0vjabb6x0w0q60mzm4ncgmc713sfjwrld6") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.18.0 (c (n "gf2_192") (v "0.18.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y68g2qy85jz101xy9kh4l5y5chnmx2gh0xsj8g952c32ymzivwy") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.19.0 (c (n "gf2_192") (v "0.19.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zqhsclg642cdldv1s6k9sqi6nlac1s13bcri6vmwg9zbvg9cvwq") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.20.0 (c (n "gf2_192") (v "0.20.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xgswkbbfxlf7cwpy1l4kcba5g0z26zz10hdp85jlr974w30s4dr") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.21.0 (c (n "gf2_192") (v "0.21.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0520gg3pmn28qps5nzyw3grzzq3r09py2yyci4j52cylwlyjiswi") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.21.1 (c (n "gf2_192") (v "0.21.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1nmj2zr4nf64mzjnrvhdy0sl6xqia2f51pn1ym4wbcarj5f8y495") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.22.0 (c (n "gf2_192") (v "0.22.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0a0x4zji45irqhrpsmara9g5yi0vvpin6r3ac2v94nn0g3jjhaby") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.23.0 (c (n "gf2_192") (v "0.23.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cyz95y1yvynms737xbx2bbg5a2xgcj7m0dr1p0ryc7hkv3q34f7") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.24.0 (c (n "gf2_192") (v "0.24.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0d3lyssmdchdiz6qrq1j00qzfmg1mn8ybglmfv7pgzjpnf9pb7l4") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.24.1 (c (n "gf2_192") (v "0.24.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xif4saxvw71xwh0m4j4svbm5ff55mmimbvhbvfq7qbmj9gp2f2y") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.25.0 (c (n "gf2_192") (v "0.25.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g1mgmy4y7qdylr4bjfsinzkv4zp0bvjrr7gpzmm89zg34ih8dfy") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.26.0 (c (n "gf2_192") (v "0.26.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14az447gqnkg1zwg8y4ykl2pd5x3qxpkr5xcihqqwn1lwvkvlmpl") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.27.0 (c (n "gf2_192") (v "0.27.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1s2mlms4fsn08a6nbpdmnrsi6fnf6ns5s8hzrfhl7md7qswc5pwc") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-gf2_192-0.27.1 (c (n "gf2_192") (v "0.27.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proptest") (r "=1.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0an5qa0zrmhz8cqb3jwk757jwvpwbsrsdij84hiyg406sh774rdn") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

