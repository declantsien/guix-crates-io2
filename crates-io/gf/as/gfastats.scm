(define-module (crates-io gf as gfastats) #:use-module (crates-io))

(define-public crate-gfastats-0.1.0 (c (n "gfastats") (v "0.1.0") (d (list (d (n "gfaR") (r "^0.1.1") (d #t) (k 0)))) (h "01mnkgj9xmrv431wdi78ky12xsrazwx29nnrkrdfm8n1g27z0fkb")))

(define-public crate-gfastats-0.1.1 (c (n "gfastats") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "gfaR") (r "^0.1.2") (d #t) (k 0)))) (h "0g5jhip28fyfvsj56jpkg23679jihj8vyb3a54ysw7p1ghm7808n")))

(define-public crate-gfastats-0.1.2 (c (n "gfastats") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "gfaR") (r "^0.1.2") (d #t) (k 0)))) (h "0fn09170q3n404kakx5lhncy050fwd8x2jxh2hpafbykd5fcknp1")))

(define-public crate-gfastats-0.1.3 (c (n "gfastats") (v "0.1.3") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "gfaR") (r "^0.1.2") (d #t) (k 0)))) (h "14zc9ndybwdv58pnlw1iwy9jvsr28ysrm97w3i4psxz5p6k4k505")))

