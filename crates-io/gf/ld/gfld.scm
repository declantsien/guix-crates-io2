(define-module (crates-io gf ld gfld) #:use-module (crates-io))

(define-public crate-gfld-1.0.0 (c (n "gfld") (v "1.0.0") (d (list (d (n "git2") (r "^0.13") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "walkdir") (r "^2") (k 0)))) (h "1970dvdn6pmblfgvpfnmsd3s9rnbb0k3sj7l35p4blj5zp8655hj") (y #t)))

(define-public crate-gfld-1.0.1 (c (n "gfld") (v "1.0.1") (d (list (d (n "git2") (r "^0.13") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "walkdir") (r "^2") (k 0)))) (h "00lcv5zgrxs6pj1hck3rcq8b221y1gpqbc51bpd4cy6hps5abhbm") (y #t)))

(define-public crate-gfld-1.0.2 (c (n "gfld") (v "1.0.2") (d (list (d (n "git2") (r "^0.13") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "walkdir") (r "^2") (k 0)))) (h "0dif6v475gk4wisxci7cjvdhs4i9jvhxqmwrbl86h8jlv5l8khhq") (y #t)))

(define-public crate-gfld-1.0.3 (c (n "gfld") (v "1.0.3") (d (list (d (n "git2") (r "^0") (k 0)) (d (n "log") (r "^0") (k 0)) (d (n "walkdir") (r "^2") (k 0)))) (h "1hgvc00654hx5p4k75gkh7km5cs60vvwarxgiw4f2kdwg8sbk4d4") (y #t)))

(define-public crate-gfld-1.0.4 (c (n "gfld") (v "1.0.4") (d (list (d (n "git2") (r "^0") (k 0)) (d (n "log") (r "^0") (k 0)) (d (n "walkdir") (r "^2") (k 0)))) (h "12mndwvr9cyhw4bxa03n613ffky95z462pn7afhzhpmg03k0f8dq") (y #t)))

