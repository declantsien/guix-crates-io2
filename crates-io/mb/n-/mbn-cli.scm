(define-module (crates-io mb n- mbn-cli) #:use-module (crates-io))

(define-public crate-mbn-cli-0.1.0 (c (n "mbn-cli") (v "0.1.0") (d (list (d (n "asn1-rs") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mbn") (r "^0.1.0") (d #t) (k 0)) (d (n "x509-parser") (r "^0.16") (d #t) (k 0)))) (h "0yxvbfwgnvwrl774068pql5v6pj8yh9g84b2knggys0h7x3kqf37") (y #t)))

(define-public crate-mbn-cli-0.2.0 (c (n "mbn-cli") (v "0.2.0") (d (list (d (n "asn1-rs") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mbn") (r "^0.2.0") (d #t) (k 0)) (d (n "x509-parser") (r "^0.16") (d #t) (k 0)))) (h "07dnnp4g6d3vp989qlhgpfbqa76frkji0pv5cz18sxqi7p6ygfz3") (y #t)))

(define-public crate-mbn-cli-0.3.0 (c (n "mbn-cli") (v "0.3.0") (d (list (d (n "asn1-rs") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mbn") (r "^0.3.0") (d #t) (k 0)) (d (n "x509-parser") (r "^0.16") (d #t) (k 0)))) (h "0zycrgq0s7bpb9qck5hfigc2bs300b2ajwxakqqccrw18hjkp3h8") (y #t)))

(define-public crate-mbn-cli-0.3.1 (c (n "mbn-cli") (v "0.3.1") (d (list (d (n "asn1-rs") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mbn") (r "^0.3.1") (d #t) (k 0)) (d (n "x509-parser") (r "^0.16") (d #t) (k 0)))) (h "0088fhlcv4cnrihzvlp5kz0rh9zckw54n6mnxd2jhfi7id9j6biz") (y #t)))

(define-public crate-mbn-cli-0.3.2 (c (n "mbn-cli") (v "0.3.2") (d (list (d (n "asn1-rs") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mbn") (r "^0.3.2") (d #t) (k 0)) (d (n "x509-parser") (r "^0.16") (d #t) (k 0)))) (h "1rfmqzi14n175lqawa48s42smz65zx7iagmrwhwvjyra4s3g2chp") (y #t)))

(define-public crate-mbn-cli-0.3.3 (c (n "mbn-cli") (v "0.3.3") (d (list (d (n "asn1-rs") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mbn") (r "^0.3.2") (d #t) (k 0)) (d (n "x509-parser") (r "^0.16") (d #t) (k 0)))) (h "1gwwqa99fanbjzx61akx7bdlwl9202x0dxwm6cpirfzdb6767gdn") (y #t)))

(define-public crate-mbn-cli-0.3.4 (c (n "mbn-cli") (v "0.3.4") (d (list (d (n "asn1-rs") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mbn") (r "^0.3.2") (d #t) (k 0)) (d (n "x509-parser") (r "^0.16") (d #t) (k 0)))) (h "0s0vvgr1la42fcc1fy8crajxy7gchfqj50b69m14zq1zi5pvrk93")))

(define-public crate-mbn-cli-1.0.0 (c (n "mbn-cli") (v "1.0.0") (d (list (d (n "asn1-rs") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mbn") (r "^1.0.0") (d #t) (k 0)) (d (n "x509-parser") (r "^0.16") (d #t) (k 0)))) (h "1z9hanyn7q6bhvl14gqzdgjvd76l2bys7f3s44s4byvymakhwz76")))

