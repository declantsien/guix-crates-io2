(define-module (crates-io mb rs mbrs) #:use-module (crates-io))

(define-public crate-mbrs-0.1.0 (c (n "mbrs") (v "0.1.0") (d (list (d (n "arbitrary-int") (r "^1.2.6") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1ch73kjcxxi2b8yf286cyn4mw53a4q0xcy1rj9f0rf3s1jmn4l58") (f (quote (("std") ("default" "std"))))))

(define-public crate-mbrs-0.1.1 (c (n "mbrs") (v "0.1.1") (d (list (d (n "arbitrary-int") (r "^1.2.6") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1w2z2nmwc598rybhnxwkdb9fzma9hw0dj1gm8l3l6k9a8z8886gj") (f (quote (("std") ("default" "std"))))))

(define-public crate-mbrs-0.2.0 (c (n "mbrs") (v "0.2.0") (d (list (d (n "arbitrary-int") (r "^1.2.6") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1vqabg950k5fbg89x4qqcbd8i4jc54knhlxs524vmbyczx9s54mq") (f (quote (("std") ("default" "std"))))))

(define-public crate-mbrs-0.2.1 (c (n "mbrs") (v "0.2.1") (d (list (d (n "arbitrary-int") (r "^1.2.6") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1cxlbc62s2pf3zpi8xa99a8rby99l21cx72wc97q6yaw53gn1dgs") (f (quote (("std") ("default" "std"))))))

(define-public crate-mbrs-0.2.2 (c (n "mbrs") (v "0.2.2") (d (list (d (n "arbitrary-int") (r "^1.2.6") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "14xkl2v2ni1m4c0acc9hs7mdpp4bg8vmw8xp95jwfbnvp23mhywl") (f (quote (("std") ("default" "std"))))))

(define-public crate-mbrs-0.2.3 (c (n "mbrs") (v "0.2.3") (d (list (d (n "arbitrary-int") (r "^1.2.6") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1l7qxnrpycjfr4p1hs4ba3fg1b2r0yps06mry09d89wdmis7sjs7") (f (quote (("std") ("default" "std"))))))

(define-public crate-mbrs-0.2.4 (c (n "mbrs") (v "0.2.4") (d (list (d (n "arbitrary-int") (r "^1.2.6") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0ivpm4cbfdydqs02b0wxwlbqbd11fz5lynhwps4d788isq25x7vl") (f (quote (("std") ("default" "std"))))))

(define-public crate-mbrs-0.3.0 (c (n "mbrs") (v "0.3.0") (d (list (d (n "arbitrary-int") (r "^1.2.6") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0iyrsn0bm7nfwvvzh5ynp69ahgl71jngjhv5lamzk0z1rhhwzq80") (f (quote (("std") ("default" "std"))))))

(define-public crate-mbrs-0.3.1 (c (n "mbrs") (v "0.3.1") (d (list (d (n "arbitrary-int") (r "^1.2.6") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (o #t) (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (o #t) (d #t) (k 0)))) (h "0vii7g504kmyas0pnc3qhnzl3gprb547hiwk0gh3115m1dr0l22x") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:thiserror") ("no-std" "dep:thiserror-no-std"))))))

