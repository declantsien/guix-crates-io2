(define-module (crates-io mb ed mbedtls-selftest) #:use-module (crates-io))

(define-public crate-mbedtls-selftest-0.1.0 (c (n "mbedtls-selftest") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "mbedtls-sys-auto") (r "^2.25.0") (f (quote ("custom_printf"))) (k 0)))) (h "0ww614vi0qwdfxblgp3g9nfjhihrbnbzwvbn408wg8avlvd50n9b") (f (quote (("std" "mbedtls-sys-auto/std")))) (y #t) (l "mbedtls-selftest-support")))

