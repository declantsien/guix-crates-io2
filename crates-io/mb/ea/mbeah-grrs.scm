(define-module (crates-io mb ea mbeah-grrs) #:use-module (crates-io))

(define-public crate-mbeah-grrs-0.1.0 (c (n "mbeah-grrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 0)) (d (n "assert_fs") (r "^1.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 0)))) (h "1zy477z81rr2as7igxmigv48gfxnxb57bqfxdl8b3imk6cx3rkgp")))

