(define-module (crates-io mb f_ mbf_gtf) #:use-module (crates-io))

(define-public crate-mbf_gtf-0.1.3 (c (n "mbf_gtf") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.6.0-alpha.4") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1fgdzi0kzhf3llw88zzikbglf0n3xq5vpvs2za5bppaslqbcak84")))

(define-public crate-mbf_gtf-0.1.4 (c (n "mbf_gtf") (v "0.1.4") (d (list (d (n "flate2") (r "^1.0.7") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.6.0-alpha.4") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "12if2bygbwfgwrvhr5qr1v3frgaw1rmmvy83bdfxbysh1g6bnrxd")))

(define-public crate-mbf_gtf-0.2.0 (c (n "mbf_gtf") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0.7") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.6.0-alpha.4") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1k2mcbidnb0cyb9alhab3x3h2zmvpd11cx1bv3lbbdwvbf56nb28")))

