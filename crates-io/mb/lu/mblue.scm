(define-module (crates-io mb lu mblue) #:use-module (crates-io))

(define-public crate-mblue-0.1.0 (c (n "mblue") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-multipart") (r "^0.6.0") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "actix-web-lab") (r "^0.19.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (f (quote ("std"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0y7kvzcqp1v216qwivrkm1cnjwl9hdwki63h9b7z8nimj2sph3ic")))

