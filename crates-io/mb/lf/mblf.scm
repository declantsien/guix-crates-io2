(define-module (crates-io mb lf mblf) #:use-module (crates-io))

(define-public crate-mblf-0.1.0 (c (n "mblf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "08mcg5xxa6ddbzwqqbxqqisavy59sf995mb9nfl3g0pk5nkf07j8")))

