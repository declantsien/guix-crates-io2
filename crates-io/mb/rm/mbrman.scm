(define-module (crates-io mb rm mbrman) #:use-module (crates-io))

(define-public crate-mbrman-0.1.0 (c (n "mbrman") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.15") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.83") (d #t) (k 0)))) (h "09jyz9h7hfcpm0d8dil3wsd616skvfdza5df07y662jbci4vi344")))

(define-public crate-mbrman-0.2.0 (c (n "mbrman") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.16") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.83") (d #t) (k 0)))) (h "0rbmw5dc91qkrcblbmdqk1bmlwmlvxmzf2ldxd1y7hyzmy1q3far")))

(define-public crate-mbrman-0.3.0 (c (n "mbrman") (v "0.3.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.16") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.83") (d #t) (k 0)))) (h "1mnlrkp9v40ayx7mlzsjkg52lbhdkindkblvi467b4lhvl6pf3a5")))

(define-public crate-mbrman-0.3.1 (c (n "mbrman") (v "0.3.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.16") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.83") (d #t) (k 0)))) (h "1jywgwnjk1lsfnsnp8z5gp3fj8asw1xlb8rnnm5karymkcd1na4w")))

(define-public crate-mbrman-0.3.2 (c (n "mbrman") (v "0.3.2") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r4pdsllbmznb4qxab25fjqzipdckcrz2cxhx4v3yhz8q2fc51a4")))

(define-public crate-mbrman-0.3.3 (c (n "mbrman") (v "0.3.3") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "074iy67m0052azcmsgh9d5fs7snzh3dr1bc5fwcdapy5bryir4ql") (y #t)))

(define-public crate-mbrman-0.3.4 (c (n "mbrman") (v "0.3.4") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qn1bfyigb6qs6h5gha5vws1zsc7aq0749q4x597ms90n1z78rwz")))

(define-public crate-mbrman-0.4.0 (c (n "mbrman") (v "0.4.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0dxa32qj8xqq81c6lp76sjcpm6an3q7zw3pshaq0jygvmrpl9rb4")))

(define-public crate-mbrman-0.4.1 (c (n "mbrman") (v "0.4.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0kkywnw12ahp5nlvbq0fdhw52iwm6aygppy6jmsl6rajri15y5gi")))

(define-public crate-mbrman-0.4.2 (c (n "mbrman") (v "0.4.2") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1583w14wxar5xg8b5kpbwfjqm7s1v5icdhrsvh9nxifgp3h6svzg")))

(define-public crate-mbrman-0.5.0 (c (n "mbrman") (v "0.5.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "06cqc601lk7jnrxs0zjclgsnqfsmwn8vbdhwmvlbmc7bysm9rvnd")))

(define-public crate-mbrman-0.5.1 (c (n "mbrman") (v "0.5.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1aclzhcc4mhdqk5wcbrmh1snr9kpmzg5vmf179p4c02xfps3kcm4")))

(define-public crate-mbrman-0.5.2 (c (n "mbrman") (v "0.5.2") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "17jypzkgpy95gb1j6js74jrv4awkx0dxsdq2c628bqrsc8j70j4w")))

