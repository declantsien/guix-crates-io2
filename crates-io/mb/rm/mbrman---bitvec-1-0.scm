(define-module (crates-io mb rm mbrman---bitvec-1-0) #:use-module (crates-io))

(define-public crate-mbrman---bitvec-1-0-0.0.1 (c (n "mbrman---bitvec-1-0") (v "0.0.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1hprjkmwsw680x7ag2p9m5qzg54x1c0b5vgf2rgsc2i2dl3r7mws")))

