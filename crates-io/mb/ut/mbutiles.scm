(define-module (crates-io mb ut mbutiles) #:use-module (crates-io))

(define-public crate-mbutiles-0.1.0 (c (n "mbutiles") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "jlens") (r "^0.0.1") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "stdio_logger") (r "^0.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0f9fb1aa8z4jj9cdi04rdc9rm3yk6drvnmqkjgb1hzad07k26axf")))

(define-public crate-mbutiles-0.1.1 (c (n "mbutiles") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "jlens") (r "^0.0.1") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "stdio_logger") (r "^0.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1pv7j298yd4w3mxb6xdf43fn8wfi7n6w5zykjwh6wjx7y74d9k54")))

