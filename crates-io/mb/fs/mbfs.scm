(define-module (crates-io mb fs mbfs) #:use-module (crates-io))

(define-public crate-mbfs-0.1.0 (c (n "mbfs") (v "0.1.0") (h "07b4ihy4ac0wbl7ya6l90nh7axc7wyivy9bbayxczaxsnmic4dsm")))

(define-public crate-mbfs-0.1.1 (c (n "mbfs") (v "0.1.1") (h "00bfrvjww7r8hm4inn8pi1riqyj40c445w6nql5z80z9x4hpy5b7")))

(define-public crate-mbfs-0.1.2 (c (n "mbfs") (v "0.1.2") (h "07q6m2axyd4iaabwfll9d6yaqf737h5wv7nirffz7ggvfaaxn6a0")))

(define-public crate-mbfs-0.1.3 (c (n "mbfs") (v "0.1.3") (h "0la9ssl0svxfhvvcvimdzwsncn6ymdlf4n9c8nmjbn01bay9zgxs")))

(define-public crate-mbfs-0.1.4 (c (n "mbfs") (v "0.1.4") (h "04bm0wiy92agzprzgpf45nprirjm98s314s9342k4dn9mlksjap5")))

(define-public crate-mbfs-0.1.5 (c (n "mbfs") (v "0.1.5") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)))) (h "1znr9xqfci7bh757680wilchnakwx254rm92mpw3izfvkg7qvpkj")))

(define-public crate-mbfs-0.1.6 (c (n "mbfs") (v "0.1.6") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)))) (h "12psscfnr2s2935p13jnwjhrdy1qy679pwbyk6c39g5gyrpavg9x")))

(define-public crate-mbfs-0.2.0 (c (n "mbfs") (v "0.2.0") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)))) (h "0q4xrlykc62k8rp7rkp4wwrs5l64mdbsvbhgsr304fdj0qjl2van")))

