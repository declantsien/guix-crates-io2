(define-module (crates-io mb ot mbot_proc_macro_helpers) #:use-module (crates-io))

(define-public crate-mbot_proc_macro_helpers-0.0.1 (c (n "mbot_proc_macro_helpers") (v "0.0.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "derive-syn-parse") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (d #t) (k 0)))) (h "14v6prl4vmqjk56kapx5rb4d0zy0sipvx7b6ay2y8bjigg3l9nx5")))

(define-public crate-mbot_proc_macro_helpers-0.0.2 (c (n "mbot_proc_macro_helpers") (v "0.0.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "derive-syn-parse") (r "^0.1.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "1lswxg4rxws31d43sm3ml8fkilr6dwz8x8bv9k87l29cxhpwd88w")))

