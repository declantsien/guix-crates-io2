(define-module (crates-io mb ar mbar-rs) #:use-module (crates-io))

(define-public crate-mbar-rs-0.1.0 (c (n "mbar-rs") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "numpy") (r "^0.12.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.12.3") (d #t) (k 0)))) (h "0fszvfh4shqg20wg2iy30zhwc8xmklqzj5nfrby50j7xcmppipz9")))

(define-public crate-mbar-rs-0.1.1 (c (n "mbar-rs") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "numpy") (r "^0.12.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.12.3") (d #t) (k 0)))) (h "00ilz4wfgn66cyb3nb3fc02862yz11caamzgvahrifaf9m3rl77v")))

(define-public crate-mbar-rs-0.2.0 (c (n "mbar-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "numpy") (r "^0.12.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.12.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0v6iiiwkk854dc1cs0fk1rri3s6h63j6h1nd9b47qif130qbd9la")))

(define-public crate-mbar-rs-0.3.0 (c (n "mbar-rs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "numpy") (r "^0.12.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.12.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0yn35dqfvb4x8lyiix5kb266z1ks7pqy0anz88bxyj9kahx7qvrb")))

