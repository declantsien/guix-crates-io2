(define-module (crates-io mb -d mb-dev) #:use-module (crates-io))

(define-public crate-mb-dev-0.1.0 (c (n "mb-dev") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.6") (d #t) (k 0)))) (h "08g0ldppii2cisvjvm7ccy57a6gzgxwbxkcgbh83n2gb2k2hnphf")))

(define-public crate-mb-dev-0.1.1 (c (n "mb-dev") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.6") (d #t) (k 0)))) (h "1qzbczqyjfc51wyzndhm7crff5ghwmyr225qm9g1igk6zarrjdgr")))

(define-public crate-mb-dev-0.1.2 (c (n "mb-dev") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.6") (d #t) (k 0)))) (h "0xb7jwi0iwi5v612dfzsjmp6lfz0nf36a6yyphwdkpipvva5j6ya")))

(define-public crate-mb-dev-0.1.3 (c (n "mb-dev") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "167207hz0s84p10m0gjwlx660bxx08bynyfl44w5bj6pnxnnvvhm")))

(define-public crate-mb-dev-0.1.4 (c (n "mb-dev") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "11w0fclysikak0i2shqpxpx2dlkvl31hq37d1554sc0v2ijfj8z6")))

(define-public crate-mb-dev-0.1.5 (c (n "mb-dev") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "1lv94x2vyashwq0imk5jhvada5viyq6vzivpc8v0klir0nd4bnbi")))

(define-public crate-mb-dev-0.1.6 (c (n "mb-dev") (v "0.1.6") (d (list (d (n "clap") (r "^3.0.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "05qzqzd0f0ya3sfnr2drd3b4vckclgc445j1s2sqbqrq6jyv7lh6")))

(define-public crate-mb-dev-0.1.7 (c (n "mb-dev") (v "0.1.7") (d (list (d (n "clap") (r "^3.0.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "07mvhdh03aj3l7q5kld2lfav17iga6jn8pdwd4s0h10n0zj6fqcy")))

(define-public crate-mb-dev-0.1.8 (c (n "mb-dev") (v "0.1.8") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "0kcf408m0rir084s8w594b3r6ca1zvy9s21zag5vvg26yp57i1gw")))

(define-public crate-mb-dev-0.1.9 (c (n "mb-dev") (v "0.1.9") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "0954s3211psnrbvi34z0s5rzxf927dzjmndmk4g3ncqdag1hr8ar")))

(define-public crate-mb-dev-0.1.10 (c (n "mb-dev") (v "0.1.10") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "1g5krxzsjl8aih9mf0qlgdy80s523i7bxn02d36241ymh3xk22g6")))

(define-public crate-mb-dev-0.1.11 (c (n "mb-dev") (v "0.1.11") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)))) (h "070rqnqk9hwdvvm2apks3gna5j841y96b41q3jr5daizs2vnibi1")))

(define-public crate-mb-dev-0.1.12 (c (n "mb-dev") (v "0.1.12") (d (list (d (n "clap") (r "^3.2.14") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ql8x05fmx70vk7z1j39amar2s8jwrgcwjyjzji6d3fmsskw901h")))

