(define-module (crates-io mb ry mbryant-aoc2021) #:use-module (crates-io))

(define-public crate-mbryant-aoc2021-0.25.0 (c (n "mbryant-aoc2021") (v "0.25.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7") (f (quote ("std"))) (d #t) (k 0)) (d (n "bit") (r "^0.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1fdgl3i9j3392gr17hvbqari4gibb1syqb9l31zycsbbl4ax2fp5")))

(define-public crate-mbryant-aoc2021-0.25.1 (c (n "mbryant-aoc2021") (v "0.25.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7") (f (quote ("std"))) (d #t) (k 0)) (d (n "bit") (r "^0.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "07f79af9c61s7slb51jb7jnvc7mbh6q2ygw444gjpqip8m9bizbf")))

