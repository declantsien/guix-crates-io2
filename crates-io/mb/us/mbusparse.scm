(define-module (crates-io mb us mbusparse) #:use-module (crates-io))

(define-public crate-mbusparse-0.1.0 (c (n "mbusparse") (v "0.1.0") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)))) (h "12g4wcn4y05f1yny0lvakmq0k84zag86y25v3c3p5xzfq3j314am") (f (quote (("std") ("default" "std"))))))

(define-public crate-mbusparse-0.1.1 (c (n "mbusparse") (v "0.1.1") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)))) (h "1asmrwdg8q1bf4254x0rzpsvc541n17k6bcdr6hamkfp6b0rn1lp") (f (quote (("std") ("default" "std"))))))

(define-public crate-mbusparse-0.1.2 (c (n "mbusparse") (v "0.1.2") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "11anagwpgfs6ixcdva53ghnpj0xngzr5637w28jnlniz882z45f8") (f (quote (("std") ("default" "std"))))))

(define-public crate-mbusparse-0.1.3 (c (n "mbusparse") (v "0.1.3") (d (list (d (n "nom") (r "^7.1") (k 0)))) (h "1s9ryywi37sh5wl0kxqilc7myxd8a440vs3dbh2l41ri2h6y2pa3") (f (quote (("std" "nom/std") ("default" "std"))))))

