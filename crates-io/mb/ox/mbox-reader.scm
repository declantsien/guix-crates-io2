(define-module (crates-io mb ox mbox-reader) #:use-module (crates-io))

(define-public crate-mbox-reader-0.1.0 (c (n "mbox-reader") (v "0.1.0") (d (list (d (n "memmap") (r "^0.5") (d #t) (k 0)))) (h "0sj4hfx3770k0mv94pmw9yllm503rhx523jhbwwlspwn2ygawl6y")))

(define-public crate-mbox-reader-0.2.0 (c (n "mbox-reader") (v "0.2.0") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1az2x4i61cwqna4mjdihw8zsfaq1amakbizsf7nwxjm8q1ryjcb2")))

