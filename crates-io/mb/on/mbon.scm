(define-module (crates-io mb on mbon) #:use-module (crates-io))

(define-public crate-mbon-0.1.0 (c (n "mbon") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0pkwv9a8nmv04477ypv02v5p0ifphlr2kc0xqjy93075wih64naf")))

(define-public crate-mbon-0.1.1 (c (n "mbon") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1knnq9j8v10nl5r62zl7w8i6msrim52a6hzgc3qy9p9dl0xlmqg3")))

(define-public crate-mbon-0.2.0 (c (n "mbon") (v "0.2.0") (d (list (d (n "async-recursion") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "132q6x1yxwkgg4vvcbsi036vcgmvh40ss7zwxasvzl2yzjg3dmrx") (s 2) (e (quote (("async" "dep:futures" "dep:async-recursion"))))))

