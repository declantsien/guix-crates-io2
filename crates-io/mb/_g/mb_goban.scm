(define-module (crates-io mb _g mb_goban) #:use-module (crates-io))

(define-public crate-mb_goban-0.0.0 (c (n "mb_goban") (v "0.0.0") (h "1pqq70zvcn7b91lqifsil69ydr8k83hr3sk7453hfmg1ds32dcp7")))

(define-public crate-mb_goban-0.1.0 (c (n "mb_goban") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "0f2yl7s0wn6nf40p3s6p4741jm4n28b2wq6h7ggcsmb7p13lv37f")))

(define-public crate-mb_goban-0.2.0 (c (n "mb_goban") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "1maxbvv68sah0pv7n3yv63cvhi5xb8k19c16iza13vq9hxyd5fcp")))

