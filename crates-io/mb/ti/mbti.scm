(define-module (crates-io mb ti mbti) #:use-module (crates-io))

(define-public crate-mbti-0.1.0 (c (n "mbti") (v "0.1.0") (d (list (d (n "maplit") (r ">= 1.0.1") (d #t) (k 0)) (d (n "strum") (r ">= 0.15.0") (d #t) (k 0)) (d (n "strum_macros") (r ">= 0.15.0") (d #t) (k 0)) (d (n "tuple") (r ">= 0.4.2") (d #t) (k 0)))) (h "1ww06b875mkvi9z427cs206l2jbp4wfsxw018nm9kmahg04fq45r")))

(define-public crate-mbti-0.1.1 (c (n "mbti") (v "0.1.1") (d (list (d (n "maplit") (r ">= 1.0.1") (d #t) (k 0)) (d (n "strum") (r ">= 0.15.0") (d #t) (k 0)) (d (n "strum_macros") (r ">= 0.15.0") (d #t) (k 0)))) (h "0jp2amvshv1ssqm3fnf6dssxlq75l85phqswmnxkp9w9a7x41d7s")))

(define-public crate-mbti-0.1.2 (c (n "mbti") (v "0.1.2") (d (list (d (n "maplit") (r ">= 1.0.1") (d #t) (k 0)) (d (n "strum") (r ">= 0.15.0") (d #t) (k 0)) (d (n "strum_macros") (r ">= 0.15.0") (d #t) (k 0)))) (h "0nr6m1w06iii08s179vkkc8mbmb2vr8l71bpp5gp3q2ww7nsg7ii")))

(define-public crate-mbti-0.1.3 (c (n "mbti") (v "0.1.3") (d (list (d (n "maplit") (r ">= 1.0.1") (d #t) (k 0)) (d (n "strum") (r ">= 0.15.0") (d #t) (k 0)) (d (n "strum_macros") (r ">= 0.15.0") (d #t) (k 0)))) (h "06lxm3dnwdrxqkp7vjblp41sv319r30pyhz3728yyvfyc7kbm8di")))

