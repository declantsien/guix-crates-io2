(define-module (crates-io oj cm ojcmp) #:use-module (crates-io))

(define-public crate-ojcmp-0.2.0 (c (n "ojcmp") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3.46") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "13lvhr8qxqpwr4qi07j86996z7kj5kycnl3096wj3h8k1bqlwhfk")))

(define-public crate-ojcmp-0.2.1 (c (n "ojcmp") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.3.46") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "lexical-core") (r "^0.7.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1crqzmbfjj0cp4p8fd01pf8djr9hw7vmcay331cay4v9pl6mdl24") (y #t)))

(define-public crate-ojcmp-0.2.2 (c (n "ojcmp") (v "0.2.2") (d (list (d (n "backtrace") (r "^0.3.46") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "lexical-core") (r "^0.7.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)))) (h "127x9vil6xp4q74vfmrzw3qi1a13y3blr0a72ddgiqbn7wfd52kh")))

(define-public crate-ojcmp-0.3.0 (c (n "ojcmp") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lexical-core") (r "^0.7.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1abnqkhc0r0j2llfdlwh6j5jwq9d1bk5xla9lx112krphqgqbizm")))

(define-public crate-ojcmp-0.3.1 (c (n "ojcmp") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lexical-core") (r "^0.7.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0pijlvhagl8j1z8i3l3csc02qyady00dra6mijip1hga9k7s360n")))

(define-public crate-ojcmp-0.3.2 (c (n "ojcmp") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "lexical-core") (r "^0.7.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1hpcw7y818kb3xr5z46ligjy31rygx4gbnfzfqwx6sa6yscg0hj0")))

(define-public crate-ojcmp-0.4.0 (c (n "ojcmp") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "lexical-core") (r "^0.7.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "10zz4fgv7wk9hrmz075r556bvdfc2i25kf5ai83h4qwd92k456kq")))

