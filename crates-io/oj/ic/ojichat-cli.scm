(define-module (crates-io oj ic ojichat-cli) #:use-module (crates-io))

(define-public crate-ojichat-cli-0.1.0 (c (n "ojichat-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ojichat") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "02xmz0x9q4700dbhb4mk0jzq5g2hshjpi3kb929z3shyh2ci4ah6")))

(define-public crate-ojichat-cli-0.1.1 (c (n "ojichat-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ojichat") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0dp1fakyi88np3z8c4mm8mf2zy6dfdzyiq04lprbhc93b62wshjr")))

(define-public crate-ojichat-cli-0.1.2 (c (n "ojichat-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ojichat") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "14dbxsfbak2m3a96zfs4w49vldnxl1dpqc4ajcg9rfnhjk63893f")))

(define-public crate-ojichat-cli-0.1.3 (c (n "ojichat-cli") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ojichat") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "0zc0imxsgb5nxx7ji4awjiasgx76i3s95pisiq2rp1jzh0rwi226")))

(define-public crate-ojichat-cli-0.1.4 (c (n "ojichat-cli") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.3") (k 0)) (d (n "ojichat") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "07xgyy6lkz7m0j1h93d1p7l608xxqpy3395d1sxkjndc4yf9jdzk")))

