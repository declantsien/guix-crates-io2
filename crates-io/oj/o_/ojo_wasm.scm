(define-module (crates-io oj o_ ojo_wasm) #:use-module (crates-io))

(define-public crate-ojo_wasm-0.1.0 (c (n "ojo_wasm") (v "0.1.0") (d (list (d (n "console_log") (r "^0.1") (d #t) (k 0)) (d (n "libojo") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ojo_graph") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "01k0wsiabckm3i1gkqndgzrdm5v0mnwhimkqc7mgk1dwckz0mw63")))

