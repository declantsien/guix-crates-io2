(define-module (crates-io oj o_ ojo_partition) #:use-module (crates-io))

(define-public crate-ojo_partition-0.1.0 (c (n "ojo_partition") (v "0.1.0") (d (list (d (n "ojo_multimap") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0yc2y8h9mj6dzy8k6wi1x5dwl8698d7ca21a4n0wq89yfvw3ykx5")))

