(define-module (crates-io vg md vgmdb-rust) #:use-module (crates-io))

(define-public crate-vgmdb-rust-0.2.0 (c (n "vgmdb-rust") (v "0.2.0") (d (list (d (n "argparse") (r "*") (d #t) (k 0)) (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "id3") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "03hxbq9nzhw2sj1acgvp7vq3nccc7azzz842xg5krf8sz69wdw9n") (y #t)))

(define-public crate-vgmdb-rust-0.2.1 (c (n "vgmdb-rust") (v "0.2.1") (d (list (d (n "argparse") (r "*") (d #t) (k 0)) (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "id3") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0y4g40674vn9mlik0ry78n2sqwz3drzb1c9104sdixl5slp8861a") (y #t)))

