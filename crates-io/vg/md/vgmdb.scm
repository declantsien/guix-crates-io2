(define-module (crates-io vg md vgmdb) #:use-module (crates-io))

(define-public crate-vgmdb-0.2.2 (c (n "vgmdb") (v "0.2.2") (d (list (d (n "argparse") (r "*") (d #t) (k 0)) (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "id3") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1bnxcpwdvy3m1pqk2kda5dk85j5wy4r73bzbzxybnkckn90ngas1")))

(define-public crate-vgmdb-0.3.0 (c (n "vgmdb") (v "0.3.0") (d (list (d (n "argparse") (r "*") (d #t) (k 0)) (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "id3") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "035z9y8p6n7n5zh94mm8rz4pq2bpbxpjxy37mkbwbz78nk3mqfwc")))

(define-public crate-vgmdb-0.3.1 (c (n "vgmdb") (v "0.3.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "id3") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "simple_parallel") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "1lnb56pczmmir251yc8jihpnvx1ad2vm3g2m1a5vns7crwbr66rj")))

(define-public crate-vgmdb-0.4.0 (c (n "vgmdb") (v "0.4.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1wf47b5c6ch0v54dlndmdzmsbxkkrw5yw3zyhfrk5nbyzqqdgjfn")))

(define-public crate-vgmdb-0.4.2 (c (n "vgmdb") (v "0.4.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "047vfqpvj943i7q9q71g35iwly81xw8mlaw8fd3qp5m77vk24vcl")))

(define-public crate-vgmdb-0.5.0 (c (n "vgmdb") (v "0.5.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1jrgxj53jnc461h4qbsfbm13846kjy0hffmxpyqdcn3hnxf8slwq")))

