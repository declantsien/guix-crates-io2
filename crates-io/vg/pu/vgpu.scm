(define-module (crates-io vg pu vgpu) #:use-module (crates-io))

(define-public crate-vgpu-0.1.0 (c (n "vgpu") (v "0.1.0") (h "10n59l2grr26ldj015r1iahd8rj3k60dlavqvdvwn3sjj6vf0ag7") (y #t)))

(define-public crate-vgpu-0.1.1 (c (n "vgpu") (v "0.1.1") (h "14nzylywsjq23y7crhz72f18aq84jzi05yb7pyk6xyv6m2v76s7r") (y #t)))

