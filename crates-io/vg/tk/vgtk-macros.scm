(define-module (crates-io vg tk vgtk-macros) #:use-module (crates-io))

(define-public crate-vgtk-macros-0.1.0 (c (n "vgtk-macros") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.17.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.5") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)))) (h "1n6c2l2gbzsg6n08158vmngd0bxkbm7ipn46x28g3ipjanjfpi80")))

(define-public crate-vgtk-macros-0.2.0 (c (n "vgtk-macros") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.17.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.5") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)))) (h "05ydgbdm00ii930nqhgz39dsq7kyx55k2jgqi8kcy9iw5lymfi8i")))

(define-public crate-vgtk-macros-0.3.0 (c (n "vgtk-macros") (v "0.3.0") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.5") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)))) (h "03hzcpw4m48v7clvc1nxl0sxx9xxk9i79kk67vn10c98haji3z4s")))

