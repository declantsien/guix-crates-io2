(define-module (crates-io vg ai vgainfo-rs) #:use-module (crates-io))

(define-public crate-vgainfo-rs-0.1.0 (c (n "vgainfo-rs") (v "0.1.0") (h "044ds0zdhfv8nbr3dc2zdsvhh4fvrlhfxykac8ggqma837ivgffd") (y #t)))

(define-public crate-vgainfo-rs-0.1.1 (c (n "vgainfo-rs") (v "0.1.1") (h "1gankadbr57xy46jgdjyj7r9068zd0vv5j0qsi1b6sndi5i9i007")))

