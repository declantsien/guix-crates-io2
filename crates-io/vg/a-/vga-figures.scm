(define-module (crates-io vg a- vga-figures) #:use-module (crates-io))

(define-public crate-vga-figures-0.1.0 (c (n "vga-figures") (v "0.1.0") (d (list (d (n "vga") (r "^0.2.4") (d #t) (k 0)))) (h "1r489crisa88irxab44q21qvkzflknr18i6dsbzq27qnmcqld3nm")))

(define-public crate-vga-figures-0.2.0 (c (n "vga-figures") (v "0.2.0") (d (list (d (n "vga") (r "^0.2.4") (d #t) (k 0)))) (h "0j2bi2rz28yff3i0hvdqyirbwz8279a28sd9d39lyf96hw2rx99k")))

(define-public crate-vga-figures-0.2.1 (c (n "vga-figures") (v "0.2.1") (d (list (d (n "vga") (r "^0.2.9") (d #t) (k 0)))) (h "1nw6900j7mssskkamb9gwwblvnnq5xnyfknpnd3f7v41dhl62flp")))

