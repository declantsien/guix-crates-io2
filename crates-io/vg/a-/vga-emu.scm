(define-module (crates-io vg a- vga-emu) #:use-module (crates-io))

(define-public crate-vga-emu-0.1.0 (c (n "vga-emu") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.34.5") (f (quote ("ttf"))) (k 0)))) (h "07c1f1jc9sg1m22zy8fis3v83ibflpzjvsd6pblgs4q19l5qa0ib")))

(define-public crate-vga-emu-0.2.0 (c (n "vga-emu") (v "0.2.0") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (k 0)))) (h "01iqz11m8bb847fn8g8dfs1sv5iv8za5r7pzw0rjlq716yrdxib0")))

(define-public crate-vga-emu-0.2.1 (c (n "vga-emu") (v "0.2.1") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (k 0)))) (h "104cb2ypgv6n6vf1a01z8bvl25mcckf7hzhpsbf14n84iixmmw28")))

(define-public crate-vga-emu-0.3.0 (c (n "vga-emu") (v "0.3.0") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (k 0)))) (h "0h3yxcm08fkiaf918qnj0wxgpvxfjcn07rxgjxmydsmswmdzb1mk")))

(define-public crate-vga-emu-0.4.0 (c (n "vga-emu") (v "0.4.0") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (k 0)))) (h "1x4fl4d212k0x7ppp7pyy5nfvc7wyxgflhpiq7dqd3j7906g17qm") (f (quote (("web") ("sdl"))))))

(define-public crate-vga-emu-0.4.1 (c (n "vga-emu") (v "0.4.1") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (o #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "0h617rhl2c0jzhppdwspdzkijsdmc0dz9gclgxgw4j6alzzzldmf") (s 2) (e (quote (("web" "dep:wasm-bindgen") ("sdl" "dep:sdl2"))))))

(define-public crate-vga-emu-0.5.0 (c (n "vga-emu") (v "0.5.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.65") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (o #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.88") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.38") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.65") (f (quote ("Document" "Window" "CanvasRenderingContext2d" "HtmlCanvasElement" "ImageData" "KeyboardEvent" "console"))) (o #t) (d #t) (k 0)))) (h "05g8mmg2xrjpll2353aap6hzb7x4n3r7d7yw139z1g9g16nwbvnb") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:wasm-bindgen-futures" "dep:web-sys" "dep:js-sys") ("sdl" "dep:sdl2"))))))

(define-public crate-vga-emu-0.5.1 (c (n "vga-emu") (v "0.5.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.65") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("ttf"))) (o #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.88") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.38") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.65") (f (quote ("Document" "Window" "CanvasRenderingContext2d" "HtmlCanvasElement" "ImageData" "KeyboardEvent" "console"))) (o #t) (d #t) (k 0)))) (h "0vbc5ak6qv8jphh1mrhi8wyarxwyh8ikvrg8rzxzv6vvk7iqbbpm") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:wasm-bindgen-futures" "dep:web-sys" "dep:js-sys") ("sdl" "dep:sdl2"))))))

(define-public crate-vga-emu-0.5.3 (c (n "vga-emu") (v "0.5.3") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.67") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.36.0") (f (quote ("ttf"))) (o #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.40") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Document" "Window" "CanvasRenderingContext2d" "HtmlCanvasElement" "ImageData" "KeyboardEvent" "console"))) (o #t) (d #t) (k 0)))) (h "00sw7mcyjn4rs5n0a21lh09c8z39h90prmwh8302n4zhhis3wdnr") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:wasm-bindgen-futures" "dep:web-sys" "dep:js-sys") ("sdl" "dep:sdl2"))))))

(define-public crate-vga-emu-0.6.0 (c (n "vga-emu") (v "0.6.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.69") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.36.0") (f (quote ("ttf"))) (o #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.42") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.69") (f (quote ("Document" "Window" "CanvasRenderingContext2d" "HtmlCanvasElement" "ImageData" "KeyboardEvent" "console"))) (o #t) (d #t) (k 0)))) (h "0551xdnhgs4gj2m3r201nymcz8rwa6yjnxhr5xazlbfc4x5qsqbr") (s 2) (e (quote (("web" "dep:wasm-bindgen" "dep:wasm-bindgen-futures" "dep:web-sys" "dep:js-sys") ("sdl" "dep:sdl2"))))))

