(define-module (crates-io vg a- vga-framebuffer) #:use-module (crates-io))

(define-public crate-vga-framebuffer-0.2.0 (c (n "vga-framebuffer") (v "0.2.0") (d (list (d (n "console-traits") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "00azxqzyy1lbgppjr33wxpx3b5vahrfdva6l8l4kmpnylm37qwv1")))

(define-public crate-vga-framebuffer-0.2.1 (c (n "vga-framebuffer") (v "0.2.1") (d (list (d (n "console-traits") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "0d5jqlar9frmpzwf8g3lcf0i55p6dld4k2a8qn6i3dmw6xf7wvqs")))

(define-public crate-vga-framebuffer-0.3.0 (c (n "vga-framebuffer") (v "0.3.0") (d (list (d (n "console-traits") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "108xs91adz7r1695gal20kab2mjkzfrk40dyw76af7ay8qzi5zmh")))

(define-public crate-vga-framebuffer-0.4.0 (c (n "vga-framebuffer") (v "0.4.0") (d (list (d (n "console-traits") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "1vkn5yrp8yrqpah6ffjr1mhqj4rvb386003762cbzilq1y5kbsrl")))

(define-public crate-vga-framebuffer-0.5.0 (c (n "vga-framebuffer") (v "0.5.0") (d (list (d (n "console-traits") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "0wkfzfflrsbfwvb30g8y9n68m3kqbdln175fpnxp99cpx8h5kr7w")))

(define-public crate-vga-framebuffer-0.6.0 (c (n "vga-framebuffer") (v "0.6.0") (d (list (d (n "console-traits") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "1b4wnvqmdkbfmn3h8xvbpw9pm7b426fa6qia9wb2w3y9vwczp5hz")))

(define-public crate-vga-framebuffer-0.7.0 (c (n "vga-framebuffer") (v "0.7.0") (d (list (d (n "console-traits") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "1sahla7bybgrpi35s540yjj61v0z6h3qcj3vg7b9ii4iazdgs99r")))

(define-public crate-vga-framebuffer-0.7.2 (c (n "vga-framebuffer") (v "0.7.2") (d (list (d (n "console-traits") (r "^0.3") (d #t) (k 0)) (d (n "const-ft") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "0mycijkv423y0n4c19s6iwjc7nmbg8dmg3qzg0b3r56gvp4sl3g1") (f (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.7.3 (c (n "vga-framebuffer") (v "0.7.3") (d (list (d (n "console-traits") (r "^0.3") (d #t) (k 0)) (d (n "const-ft") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "06ha5n5yl21n5wimh7n94bjh26achd4273r84pk1a5pfzpx42zag") (f (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.7.4 (c (n "vga-framebuffer") (v "0.7.4") (d (list (d (n "console-traits") (r "^0.3") (d #t) (k 0)) (d (n "const-ft") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "1mnp3gxlg4j889vzijjkhfgpyg9qdsw0csyhl84z8w3vam7zv1pk") (f (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.7.5 (c (n "vga-framebuffer") (v "0.7.5") (d (list (d (n "console-traits") (r "^0.3") (d #t) (k 0)) (d (n "const-ft") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "07xabm42fd07zg8h2lc2ryq80yrrvwj35r2r9qbsv98p098xzivw") (f (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.8.0 (c (n "vga-framebuffer") (v "0.8.0") (d (list (d (n "console-traits") (r "^0.3") (d #t) (k 0)) (d (n "const-ft") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "1si00y0pfj0l59irn1isqwl3bcmr6c8lqbhakqp011r4hhs62j1m") (f (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.8.1 (c (n "vga-framebuffer") (v "0.8.1") (d (list (d (n "console-traits") (r "^0.3") (d #t) (k 0)) (d (n "const-ft") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "199z7gngvmvl83qlvdn8imwdjw036xn6lndw8qmiw24a03b515fx") (f (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.9.0 (c (n "vga-framebuffer") (v "0.9.0") (d (list (d (n "console-traits") (r "^0.3") (d #t) (k 0)) (d (n "const-ft") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "1hdy5lrydparlsdp7szlgbarxm24xkyspvxwgfiivlj96c7604rf") (f (quote (("const_fn" "const-ft/const_fn"))))))

