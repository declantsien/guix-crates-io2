(define-module (crates-io vg _e vg_errortools) #:use-module (crates-io))

(define-public crate-vg_errortools-0.1.0 (c (n "vg_errortools") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("fs"))) (o #t) (d #t) (k 0)))) (h "19m484c9naf79bs0nig7ibk6yl5n41484pnnqymgi4w4d0r7j3cg") (s 2) (e (quote (("tokio" "dep:tokio"))))))

