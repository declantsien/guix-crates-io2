(define-module (crates-io vg td vgtd) #:use-module (crates-io))

(define-public crate-vgtd-1.0.0 (c (n "vgtd") (v "1.0.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1fa7q509af4xr3dqxpj4qdhamsc36p2pyiia0v6sr8vn0qw59nzp")))

(define-public crate-vgtd-2.0.0 (c (n "vgtd") (v "2.0.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0fr21wywxmwbimx32cv0mn1kvnhd5nbda1mj56579ilrcd6lqf86")))

(define-public crate-vgtd-2.0.1 (c (n "vgtd") (v "2.0.1") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0khzqsawf9j441995w4g5c0rxb4s9jakqx0f1jfcdd46d3ma1rc9")))

