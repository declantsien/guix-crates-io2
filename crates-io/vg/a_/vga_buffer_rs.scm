(define-module (crates-io vg a_ vga_buffer_rs) #:use-module (crates-io))

(define-public crate-vga_buffer_rs-0.1.0 (c (n "vga_buffer_rs") (v "0.1.0") (d (list (d (n "volatile") (r "^0.2") (d #t) (k 0)))) (h "1hhc4s4sahirh2bd4cdvf9da0yccpp8jjnrwhscf0nkzkjbnjgrk")))

(define-public crate-vga_buffer_rs-0.1.1 (c (n "vga_buffer_rs") (v "0.1.1") (d (list (d (n "volatile") (r "^0.2") (d #t) (k 0)))) (h "19jdjxz0lnaabjdfhnl72w91n91l6plqf1bcgg3bdvfc2m8hlb5q")))

(define-public crate-vga_buffer_rs-0.1.2 (c (n "vga_buffer_rs") (v "0.1.2") (d (list (d (n "volatile") (r "^0.2") (d #t) (k 0)))) (h "11h4gnykqzmqxxndn0il7s03cvd06aq363myvl4nw83gzjhmdjpj")))

