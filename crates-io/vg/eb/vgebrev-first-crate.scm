(define-module (crates-io vg eb vgebrev-first-crate) #:use-module (crates-io))

(define-public crate-vgebrev-first-crate-0.1.0 (c (n "vgebrev-first-crate") (v "0.1.0") (h "0lhl5qbhmqrg6gfpv0yfk9x7dy78yba31wy8v5af432nq3xj49zg") (y #t)))

(define-public crate-vgebrev-first-crate-0.2.0 (c (n "vgebrev-first-crate") (v "0.2.0") (h "048w6snz478mkjqd96571dil6jw74qha0lyk483djm0fgcs50xps")))

