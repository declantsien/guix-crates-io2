(define-module (crates-io #{2}# ko) #:use-module (crates-io))

(define-public crate-ko-1.0.1 (c (n "ko") (v "1.0.1") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "1nvwfyx2f889jf27p3gjanyg1s0q4s6f2c4zai59qz3k4p7mwjnn")))

(define-public crate-ko-1.0.2 (c (n "ko") (v "1.0.2") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "0gq5207qb6n0g2nv80vbiwgcmxpmgdj4jv0krylg5zx2zaii6jnb")))

(define-public crate-ko-1.0.3 (c (n "ko") (v "1.0.3") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "19ji9hn6x2ckqba78zwhz0n4k9c5q285f0zmg6si49kf9mj5mqj5")))

