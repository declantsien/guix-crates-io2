(define-module (crates-io #{2}# zb) #:use-module (crates-io))

(define-public crate-zb-0.1.0 (c (n "zb") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.10") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "orgize") (r "^0.9") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate" "zstd"))) (k 0)))) (h "0924sz371jwvvq6awmg81dy4c67hjxd0zdg5ib0rqxgm16i3bgfq")))

