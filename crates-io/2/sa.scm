(define-module (crates-io #{2}# sa) #:use-module (crates-io))

(define-public crate-sa-1.0.0 (c (n "sa") (v "1.0.0") (h "1gki8xwyh9w8lpin6nv5xa5v1pvkdzi9n38w9vk880k6rvs4fjdx")))

(define-public crate-sa-1.0.1 (c (n "sa") (v "1.0.1") (h "0zp8gysslynag8vkj3mlnqxdz7l3fyr1nncf24skkc1pw9wi6l5z")))

(define-public crate-sa-2.0.0 (c (n "sa") (v "2.0.0") (h "0abrxk06vlyiwc3vn97cckdhz7wb33l4p4pkwlwjwfs2p44vqvi3")))

