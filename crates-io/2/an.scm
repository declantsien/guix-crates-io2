(define-module (crates-io #{2}# an) #:use-module (crates-io))

(define-public crate-an-0.0.1 (c (n "an") (v "0.0.1") (h "073bzda4n647y18hg2s246frpqq6zcl3vc5ph5mjhcnmk5wy1jcp")))

(define-public crate-an-0.0.2 (c (n "an") (v "0.0.2") (h "0m46sfvb53sxbvlv31i8y24pm6s7j23g79lbs2aabvnrzj6c34m4")))

