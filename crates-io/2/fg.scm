(define-module (crates-io #{2}# fg) #:use-module (crates-io))

(define-public crate-fg-0.0.0 (c (n "fg") (v "0.0.0") (h "1v4bqvsc896g0pfcr1gly57c6ga2m5bd7cj53mygb851z72813x8")))

(define-public crate-fg-0.1.3 (c (n "fg") (v "0.1.3") (h "0n28khrczckr4wabr6dyn2i4rys8lb7ma16bkwgiyp7qsnzfvj8g")))

(define-public crate-fg-0.1.4 (c (n "fg") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q93x1hmzbi3jdnhm0i8nwvhgbzkn1lc1qrlkx8vsnj0kqqns7y6")))

