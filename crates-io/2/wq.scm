(define-module (crates-io #{2}# wq) #:use-module (crates-io))

(define-public crate-wq-0.0.0 (c (n "wq") (v "0.0.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "watson") (r "^0.3.2") (d #t) (k 0)))) (h "0p8xz4lx8fdi4w79d0simq70bsrfs69j9pj0hkjb3x9yqnpjpgc1")))

(define-public crate-wq-0.0.1 (c (n "wq") (v "0.0.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "watson") (r "^0.3.2") (d #t) (k 0)))) (h "0diqslb3bnm2scbdqf28wi2948y8pr2zh8x5mnp2491q8jnwhlm5")))

(define-public crate-wq-0.0.2 (c (n "wq") (v "0.0.2") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "watson") (r "^0.3.3") (d #t) (k 0)))) (h "0pm2dv99d7ilhyhxc0zqxii0nli6sq7l3gpxiq5sk23vf3bi9pkd")))

(define-public crate-wq-0.0.3 (c (n "wq") (v "0.0.3") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "watson") (r "^0.4.0") (d #t) (k 0)))) (h "0i1dywwik62x0lafw8vfrmcwyqn4yapgwqrqa794803j6rdrmzm2")))

(define-public crate-wq-0.0.4 (c (n "wq") (v "0.0.4") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (f (quote ("alloc"))) (k 0)) (d (n "watson") (r "^0.9.0") (d #t) (k 0)))) (h "0my2y1vm18zfi2vx4kafclzqgdgfp0asg6rbp1fwils9x14hbvks")))

