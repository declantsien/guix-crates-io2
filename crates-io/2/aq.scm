(define-module (crates-io #{2}# aq) #:use-module (crates-io))

(define-public crate-aq-0.1.0 (c (n "aq") (v "0.1.0") (d (list (d (n "accessibility") (r "^0.1.2") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)))) (h "103k7f5xcd5vpsdp11p2059aa63axiykirng3c9ww84jd62cvks6")))

(define-public crate-aq-0.1.1 (c (n "aq") (v "0.1.1") (d (list (d (n "accessibility") (r "^0.1.3") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)))) (h "0z3bwzqxab3l7alkiwd719k4nwyk1bhnd63f571yarchkld004pz")))

(define-public crate-aq-0.1.2 (c (n "aq") (v "0.1.2") (d (list (d (n "accessibility") (r "^0.1.4") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)))) (h "0wrsl06ch2d2psyr7g1p1kcnxn23d3pnbxrgk5vbqvsah3qfwppj")))

(define-public crate-aq-0.1.3 (c (n "aq") (v "0.1.3") (d (list (d (n "accessibility") (r "^0.1.5") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "18fjmwpq8p1da4dzjr8s99ij10n817ffapd1hcd1miclll6hk6xs")))

(define-public crate-aq-0.1.4 (c (n "aq") (v "0.1.4") (d (list (d (n "accessibility") (r "^0.1.6") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "11i0kh6diywdkg2qh4lac1nffdci28mw88rm6fd2z96j3mlipq0k")))

