(define-module (crates-io #{2}# eg) #:use-module (crates-io))

(define-public crate-eg-0.1.0 (c (n "eg") (v "0.1.0") (d (list (d (n "eg_derive") (r "=0.1.0") (d #t) (k 0)))) (h "1j8y871agfd1dny5ym8pfm74wl214z1fm9yp47jlz42sdckksizk")))

(define-public crate-eg-0.1.1 (c (n "eg") (v "0.1.1") (d (list (d (n "eg_derive") (r "=0.1.0") (d #t) (k 0)))) (h "0da4pnbn5w61vm4wi6myc3cyd55mkh31l56px3r5nyvg76jf4xk7")))

(define-public crate-eg-0.1.2 (c (n "eg") (v "0.1.2") (d (list (d (n "eg_derive") (r "=0.1.0") (d #t) (k 0)))) (h "0jz3lz69fywzy86s4q2dq069shdsrx6l3byk6pj6iykciwmvckqg")))

