(define-module (crates-io #{2}# vi) #:use-module (crates-io))

(define-public crate-vi-0.1.0 (c (n "vi") (v "0.1.0") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0874s7hv2vqbrmgdz7f8kjvig4mfcwypy5y4lqsklrg8zf83wn03")))

(define-public crate-vi-0.1.1 (c (n "vi") (v "0.1.1") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0j0lz1psrhqr0nybdp4bvgkzk7sqbhriiqh28q5bapm0535abdjg")))

(define-public crate-vi-0.2.0 (c (n "vi") (v "0.2.0") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "18p4q22530p80dczwjyzvn1ksr4fhgsyay6vbs9zad3wz8mlm3n7")))

(define-public crate-vi-0.2.1 (c (n "vi") (v "0.2.1") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "07ha54hppz0djz6z4b9q391v01cvvhkbdz40lq8c14xakk7bshda")))

(define-public crate-vi-0.3.0 (c (n "vi") (v "0.3.0") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "11ipl5748f2nb7ia2623nz94ddciybaq38n3a3cw6jp5pkln1w3s")))

(define-public crate-vi-0.3.1 (c (n "vi") (v "0.3.1") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "1320662dyayz2icbacmh3bwq78ifqlagnd921saqpamfbywj323l")))

(define-public crate-vi-0.3.2 (c (n "vi") (v "0.3.2") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "145zbxjw7sqn9k4rjmfdsvwnh9lzys77w7lhkqc6qhhnsclb0rvb")))

(define-public crate-vi-0.3.3 (c (n "vi") (v "0.3.3") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "0lawhdi58pd09wl1kffpq0qimbcprnczr0b76xa55phkzcpn47r1")))

(define-public crate-vi-0.3.4 (c (n "vi") (v "0.3.4") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "1ar6ki2gs2vs2ngml8icd3xrkmakfa66w96nsg0yalj5n7h9yf0k")))

(define-public crate-vi-0.3.5 (c (n "vi") (v "0.3.5") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "1ls68gflsbsnx405ggjzgc8ds41dc7lrw9jll88wcffl2pdzfrin")))

(define-public crate-vi-0.3.6 (c (n "vi") (v "0.3.6") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "1fzls1byv44q352a8f3v7ifdca6wdjnmf0nmp05qd1b22gws3x54")))

(define-public crate-vi-0.3.7 (c (n "vi") (v "0.3.7") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "0k01fgmakwll5v6fm6fb9caz6lw62ab444y727zidx6ilqk1q3l1")))

(define-public crate-vi-0.3.8 (c (n "vi") (v "0.3.8") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "0r6nqln98znhhni1ssx0n32llkqqz0z91rgsbca7j37v1xx54ib0")))

(define-public crate-vi-0.4.0 (c (n "vi") (v "0.4.0") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "1marb8gq5iyg83d30v9kaxk8pdd18ji880rcpdawb00h0dfq4cdl")))

(define-public crate-vi-0.5.0 (c (n "vi") (v "0.5.0") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "0h080nayg3i6fli0z1abmya3hkzhf88ymss19sll0rzlaq9v9kcm")))

(define-public crate-vi-0.5.1 (c (n "vi") (v "0.5.1") (d (list (d (n "cargo-insta") (r "^1.26.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "1gnl48rf81f8jykvxid5f2fvwz2f8zs4lrnbg55z4gz98vn2yf76")))

(define-public crate-vi-0.6.0 (c (n "vi") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^1.26") (f (quote ("serde"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "06i9pxpl68mcw0cysjl00lmdqnjscgac3mzpdx2qd6x2rk80fzx4")))

