(define-module (crates-io #{2}# nc) #:use-module (crates-io))

(define-public crate-nc-0.1.0 (c (n "nc") (v "0.1.0") (h "0fw2sw6v5qs39j0da317iy72cki71wd5v94a61hhljhnzwcbfa6i")))

(define-public crate-nc-0.1.1 (c (n "nc") (v "0.1.1") (h "1z9m4kg7xw70mirrd12m11r3p3y8vybjkdxcm24ipqsajhdaxfr1")))

(define-public crate-nc-0.2.1 (c (n "nc") (v "0.2.1") (h "1sz23kd77jslpmyppgdfsv1gn7jbajbllm1zmhn2bzj6xkk2zi5j")))

(define-public crate-nc-0.2.2 (c (n "nc") (v "0.2.2") (h "1c84yzkxrdygw6glzvsnwjkb0l6gsli80bmp64zcy41a9yvgavvx")))

(define-public crate-nc-0.3.1 (c (n "nc") (v "0.3.1") (h "1sl026bzqaqjh1q0dwl4qhlf79y5ypfjmmdgm8bkz6kl6c1mklfb")))

(define-public crate-nc-0.3.2 (c (n "nc") (v "0.3.2") (h "1gk3mdjzj391xykfzsklrk067jf2rk5pp3vj905vslbmrzqxdg90")))

(define-public crate-nc-0.3.3 (c (n "nc") (v "0.3.3") (h "0j799a0mpf4avlrdy4wvmd7wgsppjzzwx1a8m783iy6dnvj5yjx6")))

(define-public crate-nc-0.3.4 (c (n "nc") (v "0.3.4") (h "0838839j8y0mjglnrj8vkyrly0j1xalw4d2yv0z9yf46l6x85k46")))

(define-public crate-nc-0.3.5 (c (n "nc") (v "0.3.5") (h "1n1qjdc3291a4k3sj1iky7h9djbcv4wpn5i616cnll3mdccvz6il")))

(define-public crate-nc-0.3.6 (c (n "nc") (v "0.3.6") (h "1220x4b9ykzp5w7rih6nb03ix3q7lg25sydbgsphl05wfm2kzqw0")))

(define-public crate-nc-0.3.7 (c (n "nc") (v "0.3.7") (h "0dqgmvlivp47rpy6mfyjmvjmzca4s4fmkrvgqqpvbgjfx9p5fv7s")))

(define-public crate-nc-0.4.1 (c (n "nc") (v "0.4.1") (h "0qq5azjb8hmqd9cqf5xrzcg8cijj5h8s07ndbfzy0s3fdzkb0mjc")))

(define-public crate-nc-0.4.2 (c (n "nc") (v "0.4.2") (h "0rpmcxjyx5fa2l715f3k3gd6233kn9gd91akk14ld2bs74qki9v3")))

(define-public crate-nc-0.4.3 (c (n "nc") (v "0.4.3") (h "0d8x9nmvh8ipf3hx9r96nfvdf0zj1vpdmb755qm45h5ans2sawnb")))

(define-public crate-nc-0.4.4 (c (n "nc") (v "0.4.4") (h "0wsllfj2qna7v8js0qdsnc4j7wl9k4f1h1cyk419k0c8wyzfy2p9")))

(define-public crate-nc-0.4.5 (c (n "nc") (v "0.4.5") (h "1c7lbpvzc4zqdj7sn0b25ibw160bqx3dj401b08nvdvgbay0gnw9")))

(define-public crate-nc-0.4.6 (c (n "nc") (v "0.4.6") (h "1k0y006mw055xilkky225hpc1czjs665nmk8ji9srzmwb4wlz1jd")))

(define-public crate-nc-0.4.7 (c (n "nc") (v "0.4.7") (h "0v251xp43fkf5jn5gwnr948vc61r8wwsc0ci3npn37j4p694lgbn")))

(define-public crate-nc-0.4.8 (c (n "nc") (v "0.4.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ma0qzapmy99flgv1i91w5zhmm86szaq10zgmmijj12km8larxs0")))

(define-public crate-nc-0.4.9 (c (n "nc") (v "0.4.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15c8091xzfh1v9sgk344izw8k79zrkj64k0yadaiimpdxcf8a8p3")))

(define-public crate-nc-0.4.10 (c (n "nc") (v "0.4.10") (h "1yspbgkvvqhvx43r16n4jqh4k67k8xs3n31c0mm50d6qwcy5f8by")))

(define-public crate-nc-0.4.11 (c (n "nc") (v "0.4.11") (h "1q0kxl41cnqva2x7nr2wkcdbkc2qa7m3qpwi6cg8vlq87amynmy3")))

(define-public crate-nc-0.4.12 (c (n "nc") (v "0.4.12") (h "1lpdmhx0wrr544f4gpxjx3gmnri3z1qabr6ahbhnn1x73q3jkab1")))

(define-public crate-nc-0.4.13 (c (n "nc") (v "0.4.13") (h "11zmbyqc952n9s0sbz0mi2fphq2i7adgpryjyv8rf393lkj3k0dy")))

(define-public crate-nc-0.4.14 (c (n "nc") (v "0.4.14") (d (list (d (n "cc") (r "^1.0.60") (d #t) (k 1)))) (h "0m8d102z0ln2nx17iapf2kfa14x6k29hm7vy7f3nig9niq4538hb")))

(define-public crate-nc-0.5.1 (c (n "nc") (v "0.5.1") (d (list (d (n "cc") (r "^1.0.60") (d #t) (k 1)))) (h "11sj7x9p46wjfkjgvp7inl8jqb9vzh5ar4820g831pf7dgpkbkk1")))

(define-public crate-nc-0.5.2 (c (n "nc") (v "0.5.2") (d (list (d (n "cc") (r "^1.0.62") (d #t) (k 1)))) (h "0hr623407sw9imsj04jkpbnywnwpgdwafpkr5b3qinwbzql3rrsx")))

(define-public crate-nc-0.6.1 (c (n "nc") (v "0.6.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "1dvimq9rlplzlpi2mhi2q9lsl23z7790i4rc15zhr8qbk42dqjgi")))

(define-public crate-nc-0.6.2 (c (n "nc") (v "0.6.2") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "1w4960bz9x33prd75vw40pl3xsdv33vm72q07gdqd7s6jldalfji")))

(define-public crate-nc-0.7.1 (c (n "nc") (v "0.7.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "0jd76ypah0q35rikx9nbdb2aykp6q0blc2ps1fv8vzqhcyl70db5")))

(define-public crate-nc-0.7.2 (c (n "nc") (v "0.7.2") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "00i4jaqqj2c2wkmas405126wsl70yjdpx0lsn7xi2a22jxaypvza")))

(define-public crate-nc-0.7.3 (c (n "nc") (v "0.7.3") (d (list (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "10kbcbgmj0bbxd8p99hsdxqcqh9sdhg3prsbpq5q62z9wl7cn8p7")))

(define-public crate-nc-0.7.4 (c (n "nc") (v "0.7.4") (d (list (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "1bwm81nf5bv3bczvf3rxv2g06ngjwr1c8zyhix1bd251kyhfz766")))

(define-public crate-nc-0.7.5 (c (n "nc") (v "0.7.5") (d (list (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "0nvl3s9kl4jgxc8i5wx5vazlsz5alndarrajsms6wfwc034qgl67")))

(define-public crate-nc-0.7.6 (c (n "nc") (v "0.7.6") (d (list (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "13svrhf0317iv23k8mmmpwmz50kh7vwc380fj3dbicsfcn7hc181")))

(define-public crate-nc-0.7.7 (c (n "nc") (v "0.7.7") (d (list (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "0paia2wmmn8pvr7q9hihrqlf6h6l3k7v8jq7h15padv0rp1yqsv9")))

(define-public crate-nc-0.7.8 (c (n "nc") (v "0.7.8") (d (list (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "0a09q3jgg09bb1053j5hrpipmiv7liqi58ibcyxds8km0zcbfj3v") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.7.9 (c (n "nc") (v "0.7.9") (d (list (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "1qflhxv65rj00ap5h04pa5dwi9pyrvfr11wfw0d8labysvh606c0") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.7.10 (c (n "nc") (v "0.7.10") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 1)))) (h "0njjs2crhqlpq0mcbgwc2kpi1yiibzmibhwhp0l5w2xii35yap4r") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.7.11 (c (n "nc") (v "0.7.11") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 1)))) (h "0m2hlgm8dxdb3qjkx4nll413cxjksf4js61366yii597p3nb9lkk") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.7.12 (c (n "nc") (v "0.7.12") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)))) (h "1b5r0c8rn6ng9wlkz6nw32prx3isrqq6pd1ndw6yjj6lssvix2cg") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.7.13 (c (n "nc") (v "0.7.13") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "088fyjvlrakm2azgaivcwhh0wbh0fhclm9y6l3ydkg8f1dx1vz2s") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.7.14 (c (n "nc") (v "0.7.14") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "1fjj62gskkixakx6a0gm4nklfcnnq2n76mhz42jmgqk362byh8an") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.7.15 (c (n "nc") (v "0.7.15") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "1clcfrrip1v6rfq4i5qw6bwzndwsl13856z86ry7f1pg1659p00v") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.7.16 (c (n "nc") (v "0.7.16") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1nq9mw2ryx3nbclvni1czyzrslrs395hbd9q4vjiznpp6sa6r1ih") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.7.17 (c (n "nc") (v "0.7.17") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0h3xnfqggkhim5jdj111a18fwwj67wwh2k0yh8g0avk429fmjdy8") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.1 (c (n "nc") (v "0.8.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "05xhd9w3rg1f33i3gyacz0w8yzs2y5nliy4skvm690bppqfbqsvf") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.2 (c (n "nc") (v "0.8.2") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1i1l0sgcwnl13y443xfmvpyw235rbzm17a5vjwf3wn95zwqj20b8") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.3 (c (n "nc") (v "0.8.3") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0p4862c2wvhqnd5klypqba309y9bdr00s1d6mzc0g37y4gbk2cl0") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.4 (c (n "nc") (v "0.8.4") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0a1jkf9mkkymn7jdlm11dam8hhlqm8sdqjgyv7906srm0z4918an") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.5 (c (n "nc") (v "0.8.5") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0yclxvf8wj409ci6a2i1r0zgmrcbz28gakzilhqh6818fhxxbsl6") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.6 (c (n "nc") (v "0.8.6") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "00s142mjzpgis6bxdjwws5hcl3iy75i9bsj2jh95zg9769azcc25") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.7 (c (n "nc") (v "0.8.7") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1qkiasn95cgvswf6x5q90qpwjwvz5g0rflsm2w31l6d8xqa7s9jw") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.8 (c (n "nc") (v "0.8.8") (d (list (d (n "cc") (r "^1.0.74") (d #t) (k 1)))) (h "0fz1yz6rf1nv0697dfdqvnshm77f3d0fngkpahbiyfv67cl2kjwx") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.9 (c (n "nc") (v "0.8.9") (d (list (d (n "cc") (r "^1.0.74") (d #t) (k 1)))) (h "132nn6xh8980cfdai0qsmi0a9iz0qsr2bjrsg5b78ccqkdjsfjgi") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.10 (c (n "nc") (v "0.8.10") (d (list (d (n "cc") (r "^1.0.76") (d #t) (k 1)))) (h "14hfc350v3d1f8rx7b9y98zi7aj0x3gri5zhwpbzxar4z6kmwix0") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.11 (c (n "nc") (v "0.8.11") (d (list (d (n "cc") (r "^1.0.76") (d #t) (k 1)))) (h "10y69g05c1qhjc5k6s1vngm57v6j3cib523xqqr6abxvsvrin2fw") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.12 (c (n "nc") (v "0.8.12") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)))) (h "11ic260y70127xjb1fqfd64v814w3b96y2168j50cqzb3kk92q92") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.13 (c (n "nc") (v "0.8.13") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)))) (h "154gg1p938d5ya9m340bp3aadf3vl7pkk7xpp7vpzmxws1igsbf5") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.14 (c (n "nc") (v "0.8.14") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0pv699vqqbc48217fy2xhdgkfllyp90svy796k2gavkk5zz9k5km") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.15 (c (n "nc") (v "0.8.15") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1dgmzn9xvar7yrmizmc87jkx5iw9md3ghrvcw9n40gscxm2rfp0g") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.16 (c (n "nc") (v "0.8.16") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1v37bg59ifhgfqxsjs7cpwyr5yka6hcbxs5q87cjvcqsb6x2hfgd") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.17 (c (n "nc") (v "0.8.17") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "083hkg93ivr6hndcr4m3lpchfihlpkky55x6n5p9zyisqmgv5nvd") (f (quote (("std") ("default" "std"))))))

(define-public crate-nc-0.8.18 (c (n "nc") (v "0.8.18") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "18bj7kf7rl27vhb787zpks7j943jwsnra7cjfxhsialq6ji8rj43") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

(define-public crate-nc-0.8.19 (c (n "nc") (v "0.8.19") (d (list (d (n "cc") (r "^1.0.83") (d #t) (t "cfg(not(any(target_arch = \"x86\", target_arch = \"x86_64\", target_arch = \"arm\", target_arch = \"aarch64\")))") (k 1)))) (h "0bw4nf74kcskm4zzvn131gjh3f9jv8s3f8f0iv7nqs98c3za5hlq") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

(define-public crate-nc-0.8.20 (c (n "nc") (v "0.8.20") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)))) (h "1vjdlz2mjn25aackzwj66wfpraz6w0bp8lyfx9m8dik05v25q2cr") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

