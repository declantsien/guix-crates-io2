(define-module (crates-io #{2}# fs) #:use-module (crates-io))

(define-public crate-fs-0.0.5 (c (n "fs") (v "0.0.5") (d (list (d (n "bytes") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)))) (h "0m1i4vph7f5d339mdma05xzrmhq8z7l6ygvzchwfc514r2sfyjz9")))

