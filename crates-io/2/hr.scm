(define-module (crates-io #{2}# hr) #:use-module (crates-io))

(define-public crate-hr-0.1.0 (c (n "hr") (v "0.1.0") (d (list (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "04qbgrp7xcgka4k2l9p15ry5cy7nqwj8ax57najiprk870ckl2ch")))

(define-public crate-hr-0.1.1 (c (n "hr") (v "0.1.1") (d (list (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "14lvgrlhh7jl8pd0k1zq963bvwvffl9pfvaqn8fxmd5lp5pmb78w")))

