(define-module (crates-io #{2}# oz) #:use-module (crates-io))

(define-public crate-oz-0.0.0 (c (n "oz") (v "0.0.0") (h "0msh0cdb5ihni9dz1n1a05faywdbybkmp3f2km5ks83gxw7i5dsx")))

(define-public crate-oz-0.0.3 (c (n "oz") (v "0.0.3") (h "01627mkjinspawfnmpfk5wh6pzq7wvgzamg2y3hb8hq2lvg7z4gp")))

