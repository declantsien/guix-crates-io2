(define-module (crates-io #{2}# jt) #:use-module (crates-io))

(define-public crate-jt-0.1.0 (c (n "jt") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "12054c6d6mw81wp2wrsizyizq6k1dk17k973f37xq53gzdfypblj")))

(define-public crate-jt-0.1.1 (c (n "jt") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "18d2d3x21swk1r20mxffqzjv1czpa1c7is5nwq01mjzf16alpbp0")))

(define-public crate-jt-0.1.2 (c (n "jt") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "dunce") (r "^1.0.3") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1wwfrn2cy8833cbjk765d9ybvg8fgl4xrywk65jl31wzwi21amiy")))

