(define-module (crates-io #{2}# sh) #:use-module (crates-io))

(define-public crate-sh-0.2.0 (c (n "sh") (v "0.2.0") (d (list (d (n "sh-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f6smm1fqchyvr8vjk092vbirryzalx9jcw52shi78g4ppys6dp0")))

(define-public crate-sh-0.2.1 (c (n "sh") (v "0.2.1") (d (list (d (n "sh-macro") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18mmks7yk9zsv8kwb2dvjh26dp3skd3lyaqn1ppkdy50kci8nx20")))

