(define-module (crates-io #{2}# jh) #:use-module (crates-io))

(define-public crate-jh-0.0.0 (c (n "jh") (v "0.0.0") (h "0w4zg2r95z13syw6nzdaycbgifc0zqc2npm8mbxsd5l91lkgahf6")))

(define-public crate-jh-0.1.0 (c (n "jh") (v "0.1.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 0)) (d (n "simd") (r "^0.2.6") (d #t) (k 0) (p "ppv-lite86")))) (h "199savnck5kcw9fzd4605s1pw7gpmr957q6j2ws21p1swzwkamzn") (r "1.57")))

