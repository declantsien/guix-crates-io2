(define-module (crates-io #{2}# qn) #:use-module (crates-io))

(define-public crate-qn-0.1.0 (c (n "qn") (v "0.1.0") (h "0dil3y6d27h5p85ngqgzldsbald2shs3k55sz43mjz70frfcwi0v")))

(define-public crate-qn-0.1.1 (c (n "qn") (v "0.1.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1ghvb5aglmlcrbfm11x9fvplp5sckn8vz099yvvdh6iw6pjb0kmp")))

(define-public crate-qn-0.1.2 (c (n "qn") (v "0.1.2") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1rdn2ncxwi26df5f12n1dydiv87swphj3a9s6py5sy62rhi0g9v9")))

(define-public crate-qn-0.2.0 (c (n "qn") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0b5l82q20v481qnlpppdpilxbrpi8bi46wpzsjn9cd8p5dqjwd78")))

