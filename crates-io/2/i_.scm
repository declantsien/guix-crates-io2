(define-module (crates-io #{2}# i_) #:use-module (crates-io))

(define-public crate-i_-0.1.1 (c (n "i_") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0byps7a78lw9a30f269447l8y2s816v1kaj9vb949gf36ki46mfh")))

