(define-module (crates-io #{2}# g2) #:use-module (crates-io))

(define-public crate-g2-0.1.0 (c (n "g2") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1kqkdh3d5f3hsbwfkidk6v220k91zdsj1r154nsv40qsjfhh7zf1")))

(define-public crate-g2-0.2.0 (c (n "g2") (v "0.2.0") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0wcsf75zpah72xyi1rynrwlv9fcp3f4h3h3dzk3zz0dcnv34nx0n")))

(define-public crate-g2-0.3.0 (c (n "g2") (v "0.3.0") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "16mqg2f2wn9ry30fxydm5pf0zm91bgidy4c4324282bzjaxhvzx7")))

(define-public crate-g2-0.4.0 (c (n "g2") (v "0.4.0") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0gsa3l34jwf5bh2vbf2a26kbpa38vqkprvngbsmlkn3fbprbk63c")))

