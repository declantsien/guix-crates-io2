(define-module (crates-io #{2}# eb) #:use-module (crates-io))

(define-public crate-eb-0.1.0 (c (n "eb") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "13dx25r94ga2b1scprlx3qczz8jbc833aw05q0l4nr8blahw3hwx")))

(define-public crate-eb-0.2.0 (c (n "eb") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "004adm8i5jmz8n6p0jpi64hb9kisqfli6fr2a5d3kbckkj7d7dyw")))

(define-public crate-eb-0.2.1 (c (n "eb") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1qaaksf0pwn9chyjf73sv3yswqva35mcw84bwqrm7g19gnk9jcir")))

(define-public crate-eb-0.3.0 (c (n "eb") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color"))) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "1pnh6z07zbpj2nk88f0243hih1sm0a8998b934v4avk5qk3pigk2") (f (quote (("default"))))))

(define-public crate-eb-0.3.1 (c (n "eb") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color"))) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "0j7aq1h16q8zrx29wvx8kz3svn1qplcb49jc35n0mbd95sdscy3r") (f (quote (("default"))))))

(define-public crate-eb-0.3.2 (c (n "eb") (v "0.3.2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color"))) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "1bakizgbhw1k0mj4lj9v48qj0d5vlrz967qix24caknwhb0w3fhr") (f (quote (("default"))))))

(define-public crate-eb-0.3.3 (c (n "eb") (v "0.3.3") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color"))) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "12d8p4cshzi1x88pr9rhl061gk79lfl5rb0dykqayf4wf9hhyjnz") (f (quote (("default"))))))

(define-public crate-eb-0.3.5 (c (n "eb") (v "0.3.5") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color"))) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "1yklmwc77jr13r70kwz4w1kr1862kacwi1q2lf4nqzksvsrb0c32") (f (quote (("default"))))))

(define-public crate-eb-0.4.0 (c (n "eb") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.2") (f (quote ("suggestions" "color"))) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.12.1") (o #t) (d #t) (k 0)))) (h "1809s25dp99p2a40vjb00kfljgapc7wr718v28a5p0g3gvc0kyx7") (f (quote (("default"))))))

(define-public crate-eb-0.5.0 (c (n "eb") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color"))) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bjpv7s3ssdiajdxfla3k949mhx51hwg1rg0d75w7r77x7i4d32k") (f (quote (("default"))))))

