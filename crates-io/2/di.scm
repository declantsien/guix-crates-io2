(define-module (crates-io #{2}# di) #:use-module (crates-io))

(define-public crate-di-0.0.1 (c (n "di") (v "0.0.1") (d (list (d (n "metafactory") (r "*") (d #t) (k 0)))) (h "1sv69456g0c18g6dmls4w9vaz3kjg7hqni7bscwlpg9kybibbqb4")))

(define-public crate-di-0.1.0 (c (n "di") (v "0.1.0") (d (list (d (n "metafactory") (r "*") (d #t) (k 0)))) (h "0xa5fcp1ywdghvwrd6jyvjdq3ccp9lgz4gdwc575259cjsncm84z")))

(define-public crate-di-0.1.1 (c (n "di") (v "0.1.1") (d (list (d (n "metafactory") (r "*") (d #t) (k 0)))) (h "1jxfx9jy919ffamhc1y8697rbsjmblqc9c0j27cc7vhpv3xlyf0h")))

(define-public crate-di-0.1.2 (c (n "di") (v "0.1.2") (d (list (d (n "metafactory") (r "*") (d #t) (k 0)))) (h "0x5k22ngf1i73kc2858ygkqpizbc9hivwa7jcaz9bh2kyg6a4q26")))

