(define-module (crates-io #{2}# vb) #:use-module (crates-io))

(define-public crate-vb-0.0.5 (c (n "vb") (v "0.0.5") (d (list (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "vbyte") (r "^0.1.0") (d #t) (k 0)))) (h "1pbyiqil4qrv2dpracm6fwjha6jrs55fyhramvv8710yfih5sbq3")))

(define-public crate-vb-0.0.6 (c (n "vb") (v "0.0.6") (d (list (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "vbyte") (r "^0.1.0") (d #t) (k 0)))) (h "1rxkv6zzl3by6wakmailvp5513ksi3hw6n7mzgrkyxi43c3w8kcn")))

(define-public crate-vb-0.0.7 (c (n "vb") (v "0.0.7") (d (list (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "vbyte") (r "^0.1.0") (d #t) (k 0)))) (h "0lqgikza9w2kibidsynghvdzvwlmiii12slhga9qsn51381nyswg")))

(define-public crate-vb-0.1.1 (c (n "vb") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "vbyte") (r "^0.1.0") (d #t) (k 0)))) (h "10i87rb04p46czj4cl804bhssa8wi748iyiazsf5rrkdimh7p8d1")))

(define-public crate-vb-0.1.3 (c (n "vb") (v "0.1.3") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "vbyte") (r "^0.1.0") (d #t) (k 0)))) (h "14sq6lwqakjmbabd806b0kfad1nrwjg3k1fgj281xhmwcm80vzsw")))

