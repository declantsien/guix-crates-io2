(define-module (crates-io #{2}# pp) #:use-module (crates-io))

(define-public crate-pp-1.0.0 (c (n "pp") (v "1.0.0") (d (list (d (n "clap") (r "~2.31") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06cmflxhy4jjldbc24wg4j5032yxfwdhpnq9406v9b8ivggijwlb") (y #t)))

(define-public crate-pp-1.1.0 (c (n "pp") (v "1.1.0") (d (list (d (n "clap") (r "~2.31") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14i8mf4a54j15i674mq77cg74y8wal6x7x1h7h847jpgv3rnsnd7") (y #t)))

