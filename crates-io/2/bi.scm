(define-module (crates-io #{2}# bi) #:use-module (crates-io))

(define-public crate-bi-0.0.1 (c (n "bi") (v "0.0.1") (h "1n8wsqvhd51j5scnqs7za7k0kpxdzwkjl9hq7ry7kdpjly8snhy1")))

(define-public crate-bi-0.0.2 (c (n "bi") (v "0.0.2") (h "1fkx2vcjfddbmxx7v96c3hspp9wrh64nx0wn76rr8jzibfnq4fnh")))

(define-public crate-bi-0.1.0 (c (n "bi") (v "0.1.0") (h "02n8cxgj4iqmgc7p71a59aac5sv0hi0wibfi8jiilg63hwq97b3m")))

(define-public crate-bi-0.2.0 (c (n "bi") (v "0.2.0") (h "1h8d0q8ridsmazjrx10ld93g5vk8yyhdaxqpvg7qvs39irgwxf4v")))

(define-public crate-bi-0.3.0 (c (n "bi") (v "0.3.0") (h "0g7a55k7kdkl65bjr8nl2dn1fbj25v65chj4bx9iy73f9fpxr3dv")))

(define-public crate-bi-0.4.0 (c (n "bi") (v "0.4.0") (h "04ya7xvhmkms7qxwdbc3plpj6ij0bjzjcjd6qx25gp7m36cg84hy")))

(define-public crate-bi-0.5.0 (c (n "bi") (v "0.5.0") (d (list (d (n "zeros") (r ">=10.0.2, <11") (d #t) (k 2)))) (h "1939p4p8q7kjk1n1arbqyq3a880i9np52pgvpjnshpxjgnr9l4z3")))

(define-public crate-bi-0.6.0 (c (n "bi") (v "0.6.0") (d (list (d (n "zeros") (r ">=11, <12") (d #t) (k 2)))) (h "0kay08yqgwvhgp3ibz1cc4dv7zx1pafav82cd6sk92dpjrkywlxk")))

(define-public crate-bi-0.7.0 (c (n "bi") (v "0.7.0") (d (list (d (n "zeros") (r ">=11.2, <12") (d #t) (k 2)))) (h "0bw29b3pw7aiwzi1l3yq1rvk569ikvxb9qwndsjz0yl4p819nw7l")))

(define-public crate-bi-0.7.1 (c (n "bi") (v "0.7.1") (d (list (d (n "zeros") (r ">=11.2, <12") (d #t) (k 2)))) (h "0cn33b09k7bqkiiy5kp6nwfswnxl5s3dcxc86rzisd5bsq6xl5vz")))

(define-public crate-bi-0.7.2 (c (n "bi") (v "0.7.2") (d (list (d (n "zeros") (r ">=11.2, <12") (d #t) (k 2)))) (h "0lpig2lk1940hqic5mninsjwyiw9sdi2spfdxbrvgaxz0m30v50q")))

(define-public crate-bi-0.8.0 (c (n "bi") (v "0.8.0") (d (list (d (n "zeros") (r ">=16, <17") (d #t) (k 2)))) (h "1473a058cv7b88br3z8hk989w5cayajsi0lnbfknpgxwnicfyjrv")))

(define-public crate-bi-0.9.0 (c (n "bi") (v "0.9.0") (d (list (d (n "zeros") (r ">=16, <17") (d #t) (k 2)))) (h "0p61mviz261283gzih9a3p3gh0vzj82q43r7ijn0r3w9yxxsnkyw")))

(define-public crate-bi-0.9.1 (c (n "bi") (v "0.9.1") (d (list (d (n "zeros") (r ">=16, <17") (d #t) (k 2)))) (h "0c7wbihwvi2bfy9c18v2h3f2rcn8kwfcvnhbny72k0jmzf0nv7pj")))

(define-public crate-bi-0.9.2 (c (n "bi") (v "0.9.2") (d (list (d (n "zeros") (r ">=16, <17") (d #t) (k 2)))) (h "062zb2dwf24qf4fwjarhlmglwii3mcqif3prsamdns9n1yismxm6")))

(define-public crate-bi-0.9.3 (c (n "bi") (v "0.9.3") (d (list (d (n "zeros") (r ">=16, <17") (d #t) (k 2)))) (h "036h3g6ph7p983y0456rmhr20jlnjqq9rw67ryccwf4rd5qjqbba")))

(define-public crate-bi-0.10.0 (c (n "bi") (v "0.10.0") (d (list (d (n "async-std") (r ">=1, <2") (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=16, <17") (d #t) (k 2)))) (h "1s3fdjiwc6bsgwcdqyii8zgrz914b88asp5r8gx66idsb1cd7g4w") (f (quote (("async_std" "async-std"))))))

(define-public crate-bi-0.11.0 (c (n "bi") (v "0.11.0") (d (list (d (n "zeros") (r ">=16, <17") (d #t) (k 2)))) (h "1lpjz74asvi87g6afgwbfzjyw3dm2z7886acyj8iqky1cca92mc8")))

