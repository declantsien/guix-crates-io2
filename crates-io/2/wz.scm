(define-module (crates-io #{2}# wz) #:use-module (crates-io))

(define-public crate-wz-0.1.0 (c (n "wz") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "wz-conf") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-utf8") (r "^0.1.0") (f (quote ("runtime-dispatch-simd"))) (d #t) (k 0)))) (h "10risnlahaqmwlfykpbmghic03lc82bpzyx7q9yici55m71gjjlf")))

(define-public crate-wz-0.1.1 (c (n "wz") (v "0.1.1") (d (list (d (n "bumpalo") (r "^3.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "wz-conf") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-utf8") (r "^0.1.0") (f (quote ("runtime-dispatch-simd"))) (d #t) (k 0)))) (h "0ami91122i9scmnvh1qlv72d4c5vdvg6cjhj4qw98m6iqlsi07ry")))

(define-public crate-wz-0.1.3 (c (n "wz") (v "0.1.3") (d (list (d (n "bumpalo") (r "^3.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "wz-conf") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-utf8") (r "^0.1.0") (f (quote ("runtime-dispatch-simd"))) (d #t) (k 0)))) (h "10dd0mn8zv48m3p9vp5zm98h3ywxxqa8l9idilkfh4yqk5p79kbr")))

(define-public crate-wz-0.1.4 (c (n "wz") (v "0.1.4") (d (list (d (n "bumpalo") (r "^3.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "wz-conf") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-utf8") (r "^0.1.0") (f (quote ("runtime-dispatch-simd"))) (d #t) (k 0)))) (h "1qicc6056ln7lfywbkkfr89r0laj8kaxd03f3gq9ha3s748r2p6f")))

(define-public crate-wz-0.1.5 (c (n "wz") (v "0.1.5") (d (list (d (n "bumpalo") (r "^3.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "wz-conf") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "wz-utf8") (r "^0.1.0") (f (quote ("runtime-dispatch-simd"))) (d #t) (k 0)))) (h "1gxrws9aqnipvwzi5a7i5n9kva440xqbh105qsyn13hpvspn5fvg")))

(define-public crate-wz-1.0.0 (c (n "wz") (v "1.0.0") (d (list (d (n "bumpalo") (r "^3.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "wz-conf") (r "^1.0.0") (d #t) (k 0)) (d (n "wz-core") (r "^1.0.0") (d #t) (k 0)) (d (n "wz-fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "wz-utf8") (r "^1.0.0") (f (quote ("runtime-dispatch-simd"))) (d #t) (k 0)))) (h "0b697pnzfhyzn18akh9ai69209azhyzya6dvhx4lvhp0fbqnvv8i")))

(define-public crate-wz-1.0.2 (c (n "wz") (v "1.0.2") (d (list (d (n "bumpalo") (r "^3.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "wz-conf") (r "^1.0.0") (d #t) (k 0)) (d (n "wz-core") (r "^1.0.0") (d #t) (k 0)) (d (n "wz-fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "wz-utf8") (r "^1.0.0") (f (quote ("runtime-dispatch-simd"))) (d #t) (k 0)))) (h "0d2qbb1ff7yrjmb8kk3zdl80vish4jshh39n7g2bzilq9l5bd7rp")))

(define-public crate-wz-1.0.3 (c (n "wz") (v "1.0.3") (d (list (d (n "bumpalo") (r "^3.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "wz-conf") (r "^1.0.0") (d #t) (k 0)) (d (n "wz-core") (r "^1.0.0") (d #t) (k 0)) (d (n "wz-fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "wz-utf8") (r "^1.0.0") (f (quote ("runtime-dispatch-simd"))) (d #t) (k 0)))) (h "0l5smw17ra6my21jx28gxlqs2zlndwfxdw4i4327mhgclim8n434")))

