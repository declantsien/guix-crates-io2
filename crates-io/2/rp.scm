(define-module (crates-io #{2}# rp) #:use-module (crates-io))

(define-public crate-rp-0.1.0 (c (n "rp") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0q3rnipla8a8iha78h23p4x4qqsshf20hx83kkm64l346wgg05x5")))

(define-public crate-rp-0.1.1 (c (n "rp") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0q41s7axq86f5hq7nl4m82l2r5wg78j0z0anb32jik6bq2ggnlzq")))

