(define-module (crates-io #{2}# xz) #:use-module (crates-io))

(define-public crate-xz-0.0.1 (c (n "xz") (v "0.0.1") (h "0m6a1vra5fkpq5b9ba12pnbqk742h8rwjlnm78d18i66x13ahg32")))

(define-public crate-xz-0.1.0 (c (n "xz") (v "0.1.0") (d (list (d (n "xz2") (r "^0.1") (d #t) (k 0)))) (h "0d6sq57g1969hjl5k7gzzdbyr60za9hk8qs9iqz26biazy87d21w") (f (quote (("tokio" "xz2/tokio"))))))

