(define-module (crates-io #{2}# pb) #:use-module (crates-io))

(define-public crate-pb-0.0.1 (c (n "pb") (v "0.0.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "14z2hivz9xfz7xdajjlhzm033mhl8waq7b51c7wwkkbzwdxryb94")))

(define-public crate-pb-0.0.2 (c (n "pb") (v "0.0.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0wrlapn0b0lrvmjzfx5ig618bafw71dww4qjjiaz48raqw3lgkpb")))

(define-public crate-pb-0.0.3 (c (n "pb") (v "0.0.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1p2rychy8k0b6k2n7p8vxc9xi98ms8cjg1pnb5p1dnv3ljqdb9v1")))

(define-public crate-pb-0.0.4 (c (n "pb") (v "0.0.4") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1x6ikhb2a0k5rwvlsjlwgz74iryfcm3nlmsr9md16j07mydfvhdv")))

(define-public crate-pb-0.0.5 (c (n "pb") (v "0.0.5") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "004wxz896kyg12ghjrn4hn2vmik1ax4yccb4zfiwhcp0jikn195l")))

(define-public crate-pb-0.0.6 (c (n "pb") (v "0.0.6") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0s0y6cgvn22j74gxnd54lkm5aknf3s07ax1x5papjdr39msxispy")))

(define-public crate-pb-0.0.8 (c (n "pb") (v "0.0.8") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1fc9bclnry0ql8wvvc1z7n3nrbk294jdqn3ab5ibj5kq2ikrbjbq")))

(define-public crate-pb-0.1.0 (c (n "pb") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "06hhm1i1lwga0xg78xsvxmxm6f73xn90fywiwg8k0kc4qi3bivg5")))

(define-public crate-pb-0.1.1 (c (n "pb") (v "0.1.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0s3sh06874yx9j3lpgyk6m2flhhbvvlb044dbs35xyvvp1ccl05j")))

(define-public crate-pb-0.2.0 (c (n "pb") (v "0.2.0") (d (list (d (n "hyper") (r "^0.8.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "url") (r "^0.5.9") (d #t) (k 0)))) (h "0xma2vzgndad3kqv4jby9bingv98wjmslf88a1fh27m14qyp1pli")))

