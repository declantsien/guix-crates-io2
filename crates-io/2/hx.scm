(define-module (crates-io #{2}# hx) #:use-module (crates-io))

(define-public crate-hx-0.2.0 (c (n "hx") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)))) (h "1w1iz7nywrk780cs2wa7g6cqsqfy6szk3hfhfdmj503348k3g9h7")))

(define-public crate-hx-0.2.1 (c (n "hx") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)))) (h "0brgrcjximwz205jgszvpav878ikavxc02i4apmwhfkyqahinvf1")))

(define-public crate-hx-0.3.0 (c (n "hx") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)))) (h "1j75biz6hjn2fccwk1b1lbicwzaiqjz2v44fxmkllh99g3vrwipj")))

(define-public crate-hx-0.3.1 (c (n "hx") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)))) (h "1pb0vyprljl2308rnyxpbflcr8x96lr6fchz7qlpgrlf4lnlzl47")))

(define-public crate-hx-0.3.2 (c (n "hx") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)))) (h "02a8rcmdj294l9v7h6s1qj3vsvsrlc0vqssjhmxgr54j64095nlb")))

(define-public crate-hx-0.4.0 (c (n "hx") (v "0.4.0") (d (list (d (n "ansi_term") (r ">=0.12.0, <0.13.0") (d #t) (k 0)) (d (n "assert_cmd") (r ">=1.0.1, <2.0.0") (d #t) (k 2)) (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "clap") (r ">=2.33.0, <3.0.0") (d #t) (k 0)) (d (n "no_color") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1cjc8vwdd6ih98c04k4qvkjr4vpx2mcbhrzl4xqbc4svyv75kchj")))

(define-public crate-hx-0.4.1 (c (n "hx") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "no_color") (r "^0.1") (d #t) (k 0)))) (h "172b9lm6k50rycdkn4ik11lqr36pa3y7i55kv7cnph3ijxc415pm")))

(define-public crate-hx-0.4.2 (c (n "hx") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "no_color") (r "^0.1") (d #t) (k 0)))) (h "184kjygx0g0761c2hk814j2v4f6m43mhz56zbdpb3mw631nbyvbw")))

(define-public crate-hx-0.5.0 (c (n "hx") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "no_color") (r "^0.1") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "1pw9p3bzms6bxrakhflw1xciz36iwbvzvzs8wg83pmnnia9d7h9r")))

(define-public crate-hx-0.6.0 (c (n "hx") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.4") (d #t) (k 0)) (d (n "no_color") (r "^0.1") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "1wxdjx94xzz1a1bxjp9w8idgp8jjxgb4k7gcfwrh8gwrjy62qb81")))

