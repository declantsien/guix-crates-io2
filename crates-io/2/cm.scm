(define-module (crates-io #{2}# cm) #:use-module (crates-io))

(define-public crate-cm-0.0.0 (c (n "cm") (v "0.0.0") (h "0nin491hgfpg01ky8g8bg9ygymix13bimdyw8bizz38xfnbkk1rr") (y #t)))

(define-public crate-cm-0.0.1 (c (n "cm") (v "0.0.1") (h "00p64axp22m76bym8fk8jdp0fvx87zjxyqcwk9qpjpfiwhnbsssd") (y #t)))

(define-public crate-cm-0.0.2 (c (n "cm") (v "0.0.2") (h "08nv5c3pjhwbkadywqfqifnn304l8cm53dq7k71g6ysq0ik1i09i") (y #t)))

