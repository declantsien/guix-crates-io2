(define-module (crates-io #{2}# rc) #:use-module (crates-io))

(define-public crate-rc-0.1.0 (c (n "rc") (v "0.1.0") (h "1kbx4mahl5b1hysddjfxnjlmr6swkhc7yfm591n69z7yigc2xak1")))

(define-public crate-rc-0.1.1 (c (n "rc") (v "0.1.1") (h "1fwkls7wbzzq0rvm9pwr4pfpq2vqz48qa8n0ggrn7srrp4y5fypa") (f (quote (("unstable"))))))

