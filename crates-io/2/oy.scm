(define-module (crates-io #{2}# oy) #:use-module (crates-io))

(define-public crate-oy-0.1.0 (c (n "oy") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 2)) (d (n "core-error") (r "^0.0.1-rc4") (d #t) (k 0)) (d (n "inventory") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "oy-derive") (r "^0.1") (k 0)) (d (n "rustyline") (r "^6") (d #t) (k 2)) (d (n "rustyline-derive") (r "^0.3") (d #t) (k 2)))) (h "17fbqq3gwk2l8syx710k7sk70bb2b6kpn1xwn33vggbq7rsa8dgp") (f (quote (("std" "core-error/std" "oy-derive/std") ("default" "std" "inventory" "lazy_static"))))))

