(define-module (crates-io #{2}# ay) #:use-module (crates-io))

(define-public crate-ay-0.1.0 (c (n "ay") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08bzwz4h8j1h23vmhz34r2m0ydpfgr8y1mvy1sryi8nwb5hb3p5m")))

