(define-module (crates-io #{2}# bv) #:use-module (crates-io))

(define-public crate-bv-0.1.0 (c (n "bv") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0nn6yjpfw76qk95j986hin386flr1dwp35mhsdlg7j5x06sadpsj")))

(define-public crate-bv-0.1.1 (c (n "bv") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0xl41r9rhka9056c0cqx2nh0xd4ks7qldic1gh396qwhjzbg425q")))

(define-public crate-bv-0.1.2 (c (n "bv") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "1sqlp48541qjhvhvg3ghyh3nxrv2sxg8nigr71q8g5qvw9q6far6")))

(define-public crate-bv-0.2.0 (c (n "bv") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0g32wyk40si27nys1q4ndinwgl1bv4wvnbmpwbx7i0bfb3g1z47c")))

(define-public crate-bv-0.3.0 (c (n "bv") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0mlrghh4a5kiy9plgq71xj9xd1nr9lsynkjh5rl8jgzwqidan9dz")))

(define-public crate-bv-0.3.1 (c (n "bv") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "1gkkxn41ys6ksqasdb64xb0j4n9bgdww9yhlr478i0vn63asaq87")))

(define-public crate-bv-0.4.0 (c (n "bv") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "1bcg53ypqnvb6ddm62qfs81xl63gqw1p5hbh429fqcd8lmwl10sq")))

(define-public crate-bv-0.4.1 (c (n "bv") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "114r2jbfapb5vdak9qkpchljm7kadbsqxv5ddd1z3ph3w7flcfnp")))

(define-public crate-bv-0.4.2 (c (n "bv") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0bg0my7dypilfayiyykcvpc6ri5gvgnq44f9bdkmnyfyqh0gvbki")))

(define-public crate-bv-0.4.3 (c (n "bv") (v "0.4.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "101jxs8x3rpxc10dfbbfjp3fvibyqn7lbsxs8zx0rja6q45dpf7z")))

(define-public crate-bv-0.4.4 (c (n "bv") (v "0.4.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0fzrq2pcgw1fph77gpggazvfsgq4ic5g0l7nwhvnflp74ylmbil6")))

(define-public crate-bv-0.5.0 (c (n "bv") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1cz3hx7ih331ych49wv7xm93i5vwl9wjfclchgf86f45xhvnrvrn") (f (quote (("u128" "num-traits/i128"))))))

(define-public crate-bv-0.6.0 (c (n "bv") (v "0.6.0") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0cf5q0kl09kawicy6h8mmm6c06hp6bv3m1yvhjcp56zcgxvvjf38") (f (quote (("u128"))))))

(define-public crate-bv-0.6.1 (c (n "bv") (v "0.6.1") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1qfsja67clax8026i5nh6ri343gfdlfdp6xn2rxfy8zr0jwlp58z") (f (quote (("u128"))))))

(define-public crate-bv-0.6.2 (c (n "bv") (v "0.6.2") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "057sgxj7s4f1n4ap5fprb2ddqkyamc131nhif95h796k8bnlsglm") (f (quote (("u128"))))))

(define-public crate-bv-0.6.3 (c (n "bv") (v "0.6.3") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1isnq4b1yrhq6r6z300vylqy5hn9vrclr7s2xwih4yq7qmv5q3vi") (f (quote (("u128"))))))

(define-public crate-bv-0.6.4 (c (n "bv") (v "0.6.4") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jii13ydmw2lviyxwq3c1r9j7rlhs3yfakrm2j603c18r9qfx4n0")))

(define-public crate-bv-0.6.5 (c (n "bv") (v "0.6.5") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d5rgpbdl3ksqgqnrpdg1gap8kjry2x19mfnhjp8d4d6d6n28f9c")))

(define-public crate-bv-0.6.6 (c (n "bv") (v "0.6.6") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vbqa9lgp12020gjrans35qziy3zi0l4jhf4phj6qzbq19h3pifs")))

(define-public crate-bv-0.7.0 (c (n "bv") (v "0.7.0") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rxf1zw6vxr255bdxksyk937pqvlkyp14mnrvzmqiwwa6rd9skrm")))

(define-public crate-bv-0.7.1 (c (n "bv") (v "0.7.1") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mxhj0zr1iiav21jg5v36qzzk3q2q7ir6kn5gayxqj2wwa5206xs")))

(define-public crate-bv-0.7.2 (c (n "bv") (v "0.7.2") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0918i1gfz1dnf4g9wrf8i4zfi7mx0ql04y6mhdjdxx04aivwr9xs")))

(define-public crate-bv-0.7.3 (c (n "bv") (v "0.7.3") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10c3dd08l9n9pi6pnbs4m2fzhq859cvdsskk7ipf2nakdl7jiq7h")))

(define-public crate-bv-0.7.4 (c (n "bv") (v "0.7.4") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h7ihjs55h6gx1grvp7iphl13qijwxhikfq4ri17fpj6h6k9c76k")))

(define-public crate-bv-0.8.0 (c (n "bv") (v "0.8.0") (d (list (d (n "feature-probe") (r "^0.1.0") (d #t) (k 1)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0a6mcc5cd5b05ka1ni1qp29i7s29ymk5jkvvqc4rcs3xylavz9w8")))

(define-public crate-bv-0.8.1 (c (n "bv") (v "0.8.1") (d (list (d (n "feature-probe") (r "^0.1.0") (d #t) (k 1)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04q110d84ap3yk9s8zbl1amzj2bski9a9xc550amgjizr0vrrjma")))

(define-public crate-bv-0.8.2 (c (n "bv") (v "0.8.2") (d (list (d (n "feature-probe") (r "^0.1.0") (d #t) (k 1)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0az0wzpv1v0ay42x1m7casx79mlc9j92kjjgb0ss31ykyvhrc6fm")))

(define-public crate-bv-0.9.0 (c (n "bv") (v "0.9.0") (d (list (d (n "feature-probe") (r "^0.1.0") (d #t) (k 1)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d0f585272l6fa0mrv2nd8xr19k42a81zppmpgivfv2pg5fjqwiw")))

(define-public crate-bv-0.10.0 (c (n "bv") (v "0.10.0") (d (list (d (n "feature-probe") (r "^0.1.0") (d #t) (k 1)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1r0vigkffy014l5nc0dngpljjs8kl8552x5c351x6d9xb17zavhd")))

(define-public crate-bv-0.11.0 (c (n "bv") (v "0.11.0") (d (list (d (n "feature-probe") (r "^0.1.0") (d #t) (k 1)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09s7l60d3y4yghpsqr1n72vgpk9shqhymc0lrmb3fy2yb2gaxm3c")))

(define-public crate-bv-0.11.1 (c (n "bv") (v "0.11.1") (d (list (d (n "feature-probe") (r "^0.1.0") (d #t) (k 1)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h5kbl54fsccznfixw83xndbripw39y2qkqjwf709p75iqfvnd48")))

