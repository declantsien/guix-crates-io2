(define-module (crates-io #{2}# cx) #:use-module (crates-io))

(define-public crate-cx-0.1.0 (c (n "cx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.37") (d #t) (k 0)) (d (n "derive-getters") (r "^0.1.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros" "time" "process" "io-util" "io-std"))) (d #t) (k 0)))) (h "1l0dbf06qz1v6l7m4h1fd1d8i922xm2hs81j1142kwirvm7hg17p")))

