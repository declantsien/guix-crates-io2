(define-module (crates-io #{2}# zz) #:use-module (crates-io))

(define-public crate-zz-0.1.0 (c (n "zz") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "01s1g4s18qq2qkixrv880ikb2vhscm7czmw5mm0zxrnzi0x38bf9") (y #t)))

