(define-module (crates-io #{2}# p1) #:use-module (crates-io))

(define-public crate-p1-0.1.0 (c (n "p1") (v "0.1.0") (h "10par1qdlsajqjkraafripacw043hg3z8m2zm5m7sngjnkzlyxg6")))

(define-public crate-p1-0.1.1 (c (n "p1") (v "0.1.1") (h "1y8qz3v82czmzfrzv8s8j2nh95hsr1a9jah3nlan23nm40k47shc")))

