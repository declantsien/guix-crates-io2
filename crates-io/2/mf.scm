(define-module (crates-io #{2}# mf) #:use-module (crates-io))

(define-public crate-mf-0.1.0 (c (n "mf") (v "0.1.0") (h "01r4ih6gr8imvgvfihw9mvmgd9qpakk2yl0wibrqwdhdaasbarw7")))

(define-public crate-mf-0.1.1 (c (n "mf") (v "0.1.1") (h "1s085hsaprd0whzxi15isrjw7f4gsfsqay0pxv89bfgjmcq4w48d")))

(define-public crate-mf-0.1.2 (c (n "mf") (v "0.1.2") (h "1psn22p8pqqsk945ag9px3vpdb39krwdasvb7by0l7sxg4avv084")))

(define-public crate-mf-2.0.0 (c (n "mf") (v "2.0.0") (h "0yn0i3iil47lb2k8izs7dbgzrdrf7hws2h2qmykz66wx9xf7n7vl")))

(define-public crate-mf-2.0.1 (c (n "mf") (v "2.0.1") (h "1m1fa6h7yksy4vkxkv0alc6k37vwdkib2zw222mxd1yd5yqjcxwz")))

(define-public crate-mf-2.0.2 (c (n "mf") (v "2.0.2") (h "1c5w7i470s5yvn3k03jqr6vwgrvmf7d57ivk7ybrv2sp8fsyg0ml")))

(define-public crate-mf-2.1.0 (c (n "mf") (v "2.1.0") (h "1r0jq90faanmgn6q0j1ymw6gr3g3xr3gznz4z6qbk8dmjz6jvv94")))

(define-public crate-mf-2.1.1 (c (n "mf") (v "2.1.1") (h "0iv61471qyibrw5cgbzqi6irliqys9hsbw3sgjfzkkha67b4m01y")))

