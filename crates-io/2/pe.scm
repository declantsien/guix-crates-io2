(define-module (crates-io #{2}# pe) #:use-module (crates-io))

(define-public crate-pe-0.1.0 (c (n "pe") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)))) (h "1f24ymgmfh8kmpjlsj490kn3a9kpwzzmkwccc2n0nq1gwxakzn8d")))

(define-public crate-pe-0.1.1 (c (n "pe") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)))) (h "0j4hgd86xr5a6nvf44qywp3rlsnzshv6bjgcbpkgr5wzxr9yjcfy")))

