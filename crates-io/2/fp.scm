(define-module (crates-io #{2}# fp) #:use-module (crates-io))

(define-public crate-fp-0.1.0 (c (n "fp") (v "0.1.0") (h "0vxrk354qz2vslcwlqfx36kkg1qkxciwd5mmxmwlrnnkxg6pl2g0") (y #t)))

(define-public crate-fp-0.2.0 (c (n "fp") (v "0.2.0") (h "11bvk1lmq9wq7hc1lnfksw7c7nbanmdp1y6b5ywhrjgj5ma1311w")))

(define-public crate-fp-0.3.0 (c (n "fp") (v "0.3.0") (h "1j0f1fz18lsyq3h494n6zx2d9xxzj87d44rn9535xh6cgbxw45xw")))

