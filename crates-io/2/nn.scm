(define-module (crates-io #{2}# nn) #:use-module (crates-io))

(define-public crate-nn-0.0.1 (c (n "nn") (v "0.0.1") (d (list (d (n "rand") (r "^0.2.0") (d #t) (k 0)))) (h "1xjnif9cjacrnihik32y8x2zdsqj72cmwc7byc0jg2kjav8zxk77")))

(define-public crate-nn-0.1.0 (c (n "nn") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.7") (d #t) (k 0)) (d (n "threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.1.21") (d #t) (k 0)))) (h "1yhj2a4af8klr9dc6mnff1x9kcgqkp49bi72y5zlj21z7bc9n6jn")))

(define-public crate-nn-0.1.1 (c (n "nn") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.7") (d #t) (k 0)) (d (n "threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.1.21") (d #t) (k 0)))) (h "08qz89bwzvzmz29x19gm1jm5wbdz0pfl9ag34j8av5164ww3c3ys")))

(define-public crate-nn-0.1.2 (c (n "nn") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.10") (d #t) (k 0)) (d (n "threadpool") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (d #t) (k 0)))) (h "0532kabzasrvshdn1w7wdgb8diwblrankc8nb1hk9bv5vh48ifzi")))

(define-public crate-nn-0.1.3 (c (n "nn") (v "0.1.3") (d (list (d (n "rand") (r "^0.3.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.10") (d #t) (k 0)) (d (n "threadpool") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (d #t) (k 0)))) (h "1dxpc8wxdm4fp2w5q3paxhid3gn69skxdpaz9x4axdbsnhf35ra6")))

(define-public crate-nn-0.1.4 (c (n "nn") (v "0.1.4") (d (list (d (n "rand") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.10") (d #t) (k 0)) (d (n "threadpool") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1.23") (d #t) (k 0)))) (h "06bxxq0w058sabiwlmbsvr9kgx4a1500fmf834il79rnwazfahkq")))

(define-public crate-nn-0.1.5 (c (n "nn") (v "0.1.5") (d (list (d (n "rand") (r "^0.3.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "threadpool") (r "^0.1.3") (d #t) (k 0)) (d (n "time") (r "^0.1.24") (d #t) (k 0)))) (h "1g2alqlyv7zsixicsdb11mz3cp7pmam986jp8mxadgiw6vbmzadc")))

(define-public crate-nn-0.1.6 (c (n "nn") (v "0.1.6") (d (list (d (n "rand") (r "^0.3.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "time") (r "^0.1.24") (d #t) (k 0)))) (h "1k04campxby6ssvs6jdqaf917l1syni4g82k93rxl6pw89ls1nrp")))

