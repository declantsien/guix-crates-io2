(define-module (crates-io #{2}# tr) #:use-module (crates-io))

(define-public crate-tr-0.0.0 (c (n "tr") (v "0.0.0") (h "1br0kg3mmkjwnram9ma6rmbmb4jf9s8chj3fwkk7mdsnq9pyv26h")))

(define-public crate-tr-0.1.0 (c (n "tr") (v "0.1.0") (d (list (d (n "gettext-rs") (r "^0.4.1") (d #t) (k 0)))) (h "1xm5gr7krshw9x2ry1n74yx87m7ykly8mivwxfv3pwwwfbi59cs6")))

(define-public crate-tr-0.1.1 (c (n "tr") (v "0.1.1") (d (list (d (n "gettext-rs") (r "^0.4.1") (d #t) (k 0)))) (h "1d4lfhf9yf890gyzf4b9fjqys50li7xmr2vb903l23yhz8an1ja5")))

(define-public crate-tr-0.1.2 (c (n "tr") (v "0.1.2") (d (list (d (n "gettext") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "gettext-rs") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)))) (h "01sxjaba7xknjkdy8wmr80ks6c0jzv5mp24p3znjh5frvmry0rn7") (f (quote (("default" "gettext-rs"))))))

(define-public crate-tr-0.1.3 (c (n "tr") (v "0.1.3") (d (list (d (n "gettext") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "gettext-rs") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)))) (h "1kq0hgb79z729ng26knai5886rjsfnd5nhj075f0hig605z4wdvx") (f (quote (("default" "gettext-rs"))))))

(define-public crate-tr-0.1.6 (c (n "tr") (v "0.1.6") (d (list (d (n "gettext") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "gettext-rs") (r "^0.5") (f (quote ("gettext-system"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)))) (h "144v71bc4krd972ldflpm2jcbfvqhzlwhqcldlcp05cf7xvsiqz9") (f (quote (("default" "gettext-rs"))))))

(define-public crate-tr-0.1.7 (c (n "tr") (v "0.1.7") (d (list (d (n "gettext") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "gettext-rs") (r "^0.7") (f (quote ("gettext-system"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)))) (h "0xksi7qq5h8i779ik51i81147858d5nxr3ng39pm47l9asg1s491") (f (quote (("default" "gettext-rs"))))))

