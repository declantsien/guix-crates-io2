(define-module (crates-io #{2}# xo) #:use-module (crates-io))

(define-public crate-xo-0.1.0 (c (n "xo") (v "0.1.0") (h "1sx93q3vibv4wbn51i27ys771dapr9c7ldiyl27qp1x8kzpxywqm")))

(define-public crate-xo-0.1.1 (c (n "xo") (v "0.1.1") (h "15gkh8n1fipsq96yl37cssapxs6s1spzq2jb95mr0ynqjfwfvsdc")))

