(define-module (crates-io #{2}# dd) #:use-module (crates-io))

(define-public crate-dd-0.1.0 (c (n "dd") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "encoding8") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1ybr6pp1qq8ikvwm18s5yqvw5qjmcy1vlqbjax7vxq5lyqa4fqbq")))

(define-public crate-dd-0.1.1 (c (n "dd") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "encoding8") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1sgachwimx4ykx48jh43hr94qymy2hwk9xrg3m768cpk3p83qxda")))

(define-public crate-dd-0.2.0 (c (n "dd") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "encoding8") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1jkmkbcng7n4r987cia28f5pp4afd91bspc10c7c0ba47kxir814")))

(define-public crate-dd-0.3.0 (c (n "dd") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "encoding8") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1xm453bk2igcxp7y75bzdcxj2bcmhimxbxawwlpjy0hs9lfvvkis")))

(define-public crate-dd-0.3.1 (c (n "dd") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "encoding8") (r "^0.3.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0324ay0cq18f1grj5x2xb3jhi7frxf43fhs2j6gzkmkr2kb5pczr")))

(define-public crate-dd-0.4.0 (c (n "dd") (v "0.4.0") (d (list (d (n "dd-lib") (r "^0.2") (d #t) (k 0)))) (h "12313i159dzx7zrrwgnxj7m24r6dv2amg8q113bpv58yfz4jpxs2")))

