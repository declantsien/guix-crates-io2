(define-module (crates-io #{2}# r0) #:use-module (crates-io))

(define-public crate-r0-0.1.0 (c (n "r0") (v "0.1.0") (h "0bqmra8lmpavnj8gibpja70lb7f5v1fgii16c70z8nvga02xf9qn")))

(define-public crate-r0-0.2.0 (c (n "r0") (v "0.2.0") (h "0a2nwwx31hsa3phwpqnpy7gc0y0ylcxiigavraml2cknv18zznv2")))

(define-public crate-r0-0.2.1 (c (n "r0") (v "0.2.1") (h "13jirrhg1iacdpjyxvjwq6lbf8j8jrfb7ssr6yzmn90arpcbwyvf")))

(define-public crate-r0-0.2.2 (c (n "r0") (v "0.2.2") (h "07wl91ljvr8ca0d08j5d33fbv5p3v129f62lhrz5r3awn7sqv8z2")))

(define-public crate-r0-1.0.0 (c (n "r0") (v "1.0.0") (h "04gjrcvl56x45jiz5awggxkz8wlfj1hp3bcjbp4wn7ars7p32ymx")))

