(define-module (crates-io #{2}# cy) #:use-module (crates-io))

(define-public crate-cy-0.1.0 (c (n "cy") (v "0.1.0") (d (list (d (n "color-core") (r "^0.1.0") (d #t) (k 0)) (d (n "color-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1nxwf4hv3qqsilrnjxkb2j6b9cl78xmwdkmd4zp3fik2wqi8pzhn") (f (quote (("strict" "color-core/strict" "color-macro/strict") ("default"))))))

