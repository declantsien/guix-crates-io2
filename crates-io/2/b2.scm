(define-module (crates-io #{2}# b2) #:use-module (crates-io))

(define-public crate-b2-0.1.0 (c (n "b2") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "19iw1rj5wmn6r2ybx9qaqmsrn6aigg0cdpy0fmm83pap8i619dkl")))

