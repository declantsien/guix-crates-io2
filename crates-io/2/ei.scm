(define-module (crates-io #{2}# ei) #:use-module (crates-io))

(define-public crate-ei-0.1.0 (c (n "ei") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "08w6gichp09asng56l0s1fwd1k5rj6k7nnim307jy6h9a7ijiqgr")))

(define-public crate-ei-0.1.1 (c (n "ei") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0sjilpk6m5vimilfa1p2pr14ly51vjsashlng5i200rldcs1xdpa")))

(define-public crate-ei-0.1.2 (c (n "ei") (v "0.1.2") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1d307n4hrgcnhfwhfgxi9806k771dhjmb9iidavmf79i9wp9iwbz")))

(define-public crate-ei-0.2.0 (c (n "ei") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v9na7f4rfa6wsx25yviwflsbzhfrssqhj99lz8a3xrdsbmv13w1")))

