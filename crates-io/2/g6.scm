(define-module (crates-io #{2}# g6) #:use-module (crates-io))

(define-public crate-g6-0.0.0 (c (n "g6") (v "0.0.0") (h "0jln83bx38wcd843cchkmxfswy912m5bnhcvg7jyfyqj1yn61g3r") (f (quote (("default"))))))

(define-public crate-g6-0.0.1 (c (n "g6") (v "0.0.1") (d (list (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)))) (h "1pd01nzk1m6rr85w07sb10yyggw7c189bafsg823mn16gqf2msj6") (f (quote (("default"))))))

(define-public crate-g6-0.0.2 (c (n "g6") (v "0.0.2") (d (list (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.6") (o #t) (d #t) (k 0)))) (h "1fqnm8xd2j1d0alwq8y0xa8rggk24wk68qn7pviix29bc6bv7hxd") (f (quote (("default"))))))

(define-public crate-g6-0.1.0 (c (n "g6") (v "0.1.0") (d (list (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.6") (o #t) (d #t) (k 0)))) (h "1i04vj3mrfmqqic9qac82pxg27488vk6jzdqhlk093gngcz9niyb") (f (quote (("default"))))))

(define-public crate-g6-0.1.1 (c (n "g6") (v "0.1.1") (d (list (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)))) (h "0mvw8hz9cw4fmwyz53lcjgw0n6cxvgf1f20hf8l0xf8va7bglvx8") (f (quote (("default"))))))

