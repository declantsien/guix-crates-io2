(define-module (crates-io #{2}# og) #:use-module (crates-io))

(define-public crate-og-0.0.0 (c (n "og") (v "0.0.0") (h "0jmxmlwzpymxmrjqqxlzqyhdi5gip8ax8lf5s42qmj09m30d9awq")))

(define-public crate-og-0.0.1 (c (n "og") (v "0.0.1") (h "1ab594q7l3sn7182bw463d3nkkfvhnmlsf0yby5m84kdyxzplhqc")))

