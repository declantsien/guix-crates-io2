(define-module (crates-io #{2}# ld) #:use-module (crates-io))

(define-public crate-ld-0.0.0 (c (n "ld") (v "0.0.0") (h "1djnj89yirhmfjs1ycsax4zm6wb96r7pvmsmjnwrd4aprwcbmbr6")))

(define-public crate-ld-0.0.1 (c (n "ld") (v "0.0.1") (h "1p3kx50w04k3iwq2infi8wpab351a2ffwarb2lwh1bbp4vp2fxgm")))

