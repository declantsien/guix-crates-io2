(define-module (crates-io #{2}# au) #:use-module (crates-io))

(define-public crate-au-0.10.0 (c (n "au") (v "0.10.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)))) (h "1213r9zg70fhrky4hcjkv30fi7wi4dz22n6vh6ga90hz5vzgc31i")))

