(define-module (crates-io #{2}# xr) #:use-module (crates-io))

(define-public crate-xr-0.0.0 (c (n "xr") (v "0.0.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)))) (h "0i171cjmqxbaf6wgim2rx9bdsxb7w3rcdmwk30qfchgwa9jpizbr") (f (quote (("default"))))))

