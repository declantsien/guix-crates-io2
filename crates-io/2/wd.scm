(define-module (crates-io #{2}# wd) #:use-module (crates-io))

(define-public crate-wd-0.1.0 (c (n "wd") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0h5n8ri41x4madi3sjsd63q2pb205xa89swf472s5kav5agk7gl8")))

(define-public crate-wd-0.1.1 (c (n "wd") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1givh6mqjis33v41fdm4rmg92drmfqkw4brr7z6ar050mdbmn9yz")))

(define-public crate-wd-0.1.2 (c (n "wd") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0gs8zjxvn3839f4ngm1fbzrr7zgapxlpxm0fs6qy03xyl4bzfdcj")))

(define-public crate-wd-0.1.3 (c (n "wd") (v "0.1.3") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0a1l3rkbja2jznrz601r1742w3nhw12px07370rywgk66qnjgjr0")))

