(define-module (crates-io #{2}# qp) #:use-module (crates-io))

(define-public crate-qp-0.1.0 (c (n "qp") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0h92yfla6mq3pjmnjhyxk21ipq0rv6l7dzbn85ldklbdziysbs9l") (r "1.56")))

(define-public crate-qp-0.1.1 (c (n "qp") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0sjg82ccvgicsxipaar0lzyyrcr59b7fiz81kwzb5fcw8d65w8g9") (r "1.56")))

(define-public crate-qp-0.1.2 (c (n "qp") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1w89r6pqgmsh20v6pnm0l00lpb45b42ql1mvwx4i6mm5vj651d11") (r "1.56")))

(define-public crate-qp-0.1.3 (c (n "qp") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "04d50vfhjj4ng9d7ganvxnalxvi24z01y5fhf7sff79n3dzaxc2c") (f (quote (("tokio-semaphore" "tokio")))) (r "1.56")))

(define-public crate-qp-0.2.0 (c (n "qp") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0v35xpyd8fa7ml0mliyy53ya8r8pqb4p3p687xzi3ry93vc1ch49") (r "1.56")))

(define-public crate-qp-0.2.1 (c (n "qp") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.8") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1s2jspwnk9b2lfv34wmslla6kv8c2zdjgzi1nizm887lf6zkfv04") (r "1.56")))

