(define-module (crates-io #{2}# vr) #:use-module (crates-io))

(define-public crate-vr-0.0.0 (c (n "vr") (v "0.0.0") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "imagequant") (r "^4.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1ly4xrvb2jf02pxlzlgdwy3jric2is8190hjl92y0mqqm0plbbd6") (f (quote (("default"))))))

