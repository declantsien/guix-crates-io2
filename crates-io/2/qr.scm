(define-module (crates-io #{2}# qr) #:use-module (crates-io))

(define-public crate-qr-0.1.0 (c (n "qr") (v "0.1.0") (d (list (d (n "qrcode") (r "^0.11.2") (k 0)))) (h "1vmzyvjqg2ygjgnhzig07ss43pqh800fpafbw93m8wd74jahkqhd")))

(define-public crate-qr-0.1.1 (c (n "qr") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "qrcode") (r "^0.11.2") (k 0)))) (h "11fsdnzz2msdj7rf3z1yhn6pl1c3jhh7hc7c6zl8x2056ms2pv20")))

(define-public crate-qr-0.1.2 (c (n "qr") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "qrcode") (r "^0.11.2") (k 0)))) (h "17r57avpx3ry07jimjbm1k9mml4pabj7i3vv3k699jh6w4hsnx39")))

