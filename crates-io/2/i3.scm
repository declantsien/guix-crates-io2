(define-module (crates-io #{2}# i3) #:use-module (crates-io))

(define-public crate-i3-0.0.1 (c (n "i3") (v "0.0.1") (h "0winkjpjyg60a3l3xxa58rblsqkjhdid58ba26ddf7bmjvxjxc7b")))

(define-public crate-i3-0.0.2 (c (n "i3") (v "0.0.2") (h "1d46cyn8rabri14ar1nda5svkzxnbnhc3w5zb9ll4ycw4zhbnd4m")))

(define-public crate-i3-0.0.3 (c (n "i3") (v "0.0.3") (h "1jf21awgq01dp21ff1q26knnsv18zv5nv69l609vqvbf9vpmi7vq")))

(define-public crate-i3-0.0.4 (c (n "i3") (v "0.0.4") (h "1f60cmfgdn57xb5kzjx1zlnnh0ybjyz6bhs4xnyfp1rb399czx10")))

(define-public crate-i3-0.0.5 (c (n "i3") (v "0.0.5") (h "0s8xc5lkpczd98j3p7am5zfwhn6aylg765y5pz2jcxxqscxah7b1")))

(define-public crate-i3-0.0.6 (c (n "i3") (v "0.0.6") (h "1h23s3shcar66iwr044qpdn6f2qqfsccrk43zql773iqmkxq0z6k")))

(define-public crate-i3-0.0.8 (c (n "i3") (v "0.0.8") (h "1375r995v6fml0ccnqbcr8wrsqbwk39j42v4pw3p4847gg836smc")))

(define-public crate-i3-0.0.9 (c (n "i3") (v "0.0.9") (h "149bgxy720k7bgfz2y5qr8l0jj3i2xmj3zrqa7rz6kr12049g4w9")))

(define-public crate-i3-0.0.10 (c (n "i3") (v "0.0.10") (h "0db896j1frw1gciq7ldv9qsb7ksvwfj9iz1l1b869xx4xcv3wj9j")))

(define-public crate-i3-0.0.11 (c (n "i3") (v "0.0.11") (h "134m2rapsjrfkxf7plwk5lb588q849vijrl6703sw0hx12mg4f1a")))

(define-public crate-i3-0.0.12 (c (n "i3") (v "0.0.12") (h "0ydrd01jhbp9vpcm50il9dqjpn26cq84dfmh24dqqx8ngfl271ip")))

(define-public crate-i3-0.0.13 (c (n "i3") (v "0.0.13") (d (list (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "1v1bbhhxa3zxqf1nd6irvh9vw79zrzmw39a3rcpnb93cmmj8xvzs")))

(define-public crate-i3-0.0.14 (c (n "i3") (v "0.0.14") (d (list (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "1i1xhx7g54vf0kwj7hyv4i18rdymd6g5wyfi7x9smzn6r9q3kdr3")))

(define-public crate-i3-0.0.15 (c (n "i3") (v "0.0.15") (d (list (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "1yx1qxykfy9f1v01z5kvzb5rap8y9rws3fzmnpvkihm7bfjxxpvp")))

(define-public crate-i3-0.0.16 (c (n "i3") (v "0.0.16") (d (list (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "0nljnljalsazwralqzyvswrrgfrpnsgdals2pzmn2wp57xa7g0qb")))

(define-public crate-i3-0.0.17 (c (n "i3") (v "0.0.17") (d (list (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "1a42lb23fscqj16lx49wnri14vp0rkn5wgf9n6p53iiyk9r2w3br")))

