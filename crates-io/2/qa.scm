(define-module (crates-io #{2}# qa) #:use-module (crates-io))

(define-public crate-qa-0.0.1 (c (n "qa") (v "0.0.1") (h "1ck7ld71yixs9lwy7x68awfxxvbgndgx2fb3iz9dn87n3xjx0sds")))

(define-public crate-qa-0.1.0 (c (n "qa") (v "0.1.0") (h "075j3xg2z5yhn9vy2x5xb58rm56rv96c2gwi3lryxhigfhi87s4r")))

