(define-module (crates-io #{2}# yj) #:use-module (crates-io))

(define-public crate-yj-0.6.0 (c (n "yj") (v "0.6.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "1xc9bxi96v4gwka1dsvvn7jj63z618r1lys1zbbgd5jm9ikb77vi")))

(define-public crate-yj-0.7.4 (c (n "yj") (v "0.7.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "1gxmmhpr0n6sn5b3sl3vr7h437l0d765ks4m0x2z6dn38586gmhb")))

(define-public crate-yj-0.7.7 (c (n "yj") (v "0.7.7") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "04slm7g5g0hnbvbj0idwlb0lir9srcgwcinm8y3975pk9c12daa8")))

(define-public crate-yj-0.7.8 (c (n "yj") (v "0.7.8") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "163c8k4ka3bpy5y30jhi4262psczljiazg2rxnsgli0dpqm7v17r")))

(define-public crate-yj-0.7.9 (c (n "yj") (v "0.7.9") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "145ja4ag5a5wjhh7rdlip730nkzf1w43w53i3nngfmd70qjkpkg7")))

(define-public crate-yj-0.7.10 (c (n "yj") (v "0.7.10") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "1xmyp7wx4p34n7szpry6ha6233cnhf5x6vhgrrnpql3pyy8dgg52")))

(define-public crate-yj-0.7.11 (c (n "yj") (v "0.7.11") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "08isji0yc9pnpbj2y11yqv1ir3mn1rw3sdnma7669d277zkpa5jp")))

(define-public crate-yj-0.7.15 (c (n "yj") (v "0.7.15") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "0plc0pxzpla39xx40fp8icfywqn1r75nj1z63nwqk3rycn3zk8b7")))

(define-public crate-yj-0.7.16 (c (n "yj") (v "0.7.16") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "14hwhl4bymlg51z4nbvmi82hj61gq6lxbpn9zm61dch7qldb77yk")))

(define-public crate-yj-0.7.17 (c (n "yj") (v "0.7.17") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "1v66gz8946b8yip2vv8b40wsdz5r1fmqrsd9rwk0bkvxvgrjwvxw")))

(define-public crate-yj-1.0.0 (c (n "yj") (v "1.0.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "0mawkj44l83rk0xs9k0bsngg0z287wnxf9s6n7yi3cdafac1mdkk")))

(define-public crate-yj-1.0.1 (c (n "yj") (v "1.0.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "02xwqnh9g09gz2khyla6aapfzq6vycy4gz00ficfx92dp52swbwx")))

(define-public crate-yj-1.1.0 (c (n "yj") (v "1.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "1wlip21lg80wsffd254q758g25xqwd5xpz2a1ihvh04jsq63fcw7")))

(define-public crate-yj-1.1.25 (c (n "yj") (v "1.1.25") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "snafu") (r "^0.6.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "1jfcvgvrv1klf5w6r8m9qws8snc5nn0imm8ldyg3gdp4fnykfm08")))

(define-public crate-yj-1.1.33 (c (n "yj") (v "1.1.33") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)))) (h "1llznmb12ag3al9shvgynxhq5kckhyf9gnybj8as68j5fzrxi7gn")))

(define-public crate-yj-1.1.34 (c (n "yj") (v "1.1.34") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)))) (h "00dnsagv73n0jr79n3nlwvj0fp0wmv8zzrh0wyah8zx2bkrgbjaa")))

(define-public crate-yj-1.1.35 (c (n "yj") (v "1.1.35") (d (list (d (n "clap") (r "^3.0.0-rc.0") (f (quote ("derive" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)))) (h "142rw1vc91id287f6mmlchadlkijzrl78z307spi194b9jlnbn6y")))

(define-public crate-yj-1.2.0 (c (n "yj") (v "1.2.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (f (quote ("arbitrary_precision" "preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)))) (h "1yh84shjdzn4kp50yql3wcxy7pq84wbc35ba09zcdypjpyz1z9wx")))

(define-public crate-yj-1.2.2 (c (n "yj") (v "1.2.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (f (quote ("arbitrary_precision" "preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (d #t) (k 0)))) (h "1v7gjh1k0i99ri1lyly6y9j2mins9pdn17zfi6wszfr0w2bxxx2x")))

(define-public crate-yj-1.2.3 (c (n "yj") (v "1.2.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("cargo" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("arbitrary_precision" "preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.16") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "0lb9mhxffm4fb3sn7gqkwny6b8ims29f4narq2m1ibg9mbbghx9g")))

