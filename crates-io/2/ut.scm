(define-module (crates-io #{2}# ut) #:use-module (crates-io))

(define-public crate-ut-0.3.0 (c (n "ut") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1cc80sqxflvmy4vrx0n034pivhqialzpn5c16sj2z0vgzqsg1nr0") (f (quote (("sync") ("default" "sync"))))))

