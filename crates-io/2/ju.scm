(define-module (crates-io #{2}# ju) #:use-module (crates-io))

(define-public crate-ju-0.1.0 (c (n "ju") (v "0.1.0") (h "1fb55ij1i3rc9nhaij5j46v7yhsxrckfgvqc1f8g7q3fyp925gqv")))

(define-public crate-ju-0.1.1 (c (n "ju") (v "0.1.1") (d (list (d (n "inquire") (r "^0.5.3") (d #t) (k 0)))) (h "0rkbid7nvvf1l380r8yixw8wlxb4rwnrfs07mpn9a99shldlvgxy")))

(define-public crate-ju-0.1.2 (c (n "ju") (v "0.1.2") (d (list (d (n "inquire") (r "^0.5.3") (d #t) (k 0)))) (h "040vjf721rfx42zmqch341j4q4yl467rp3c5chj2yib3crj9h71l")))

(define-public crate-ju-0.2.0 (c (n "ju") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "0kcqvr3qbi3m4ska0f5i5w5s8gjs8iw1gay2k2bljgl3byzrjqn7")))

(define-public crate-ju-0.2.2 (c (n "ju") (v "0.2.2") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "1w29vnalkw2p7d25vscnabagqq8hs8j1gsg0rawxv1a2m46knagd")))

(define-public crate-ju-0.2.3 (c (n "ju") (v "0.2.3") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "1qbjrzsqlywz9lcb4ypyzf19xfp6ksp49gj729gidbgr08688lml")))

