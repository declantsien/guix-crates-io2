(define-module (crates-io #{2}# po) #:use-module (crates-io))

(define-public crate-po-0.0.1 (c (n "po") (v "0.0.1") (d (list (d (n "curl") (r "^0.2.1") (d #t) (k 0)) (d (n "docopt") (r "^0.6.45") (d #t) (k 0)) (d (n "regex") (r "^0.1.18") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.1") (d #t) (k 0)) (d (n "url") (r "^0.2.23") (d #t) (k 0)))) (h "0x0bhnjza9gvw8zr5mz7k08hfw72z42ggm4jz5m19ridlszn5lsp")))

(define-public crate-po-0.0.2 (c (n "po") (v "0.0.2") (d (list (d (n "curl") (r "^0.2.2") (d #t) (k 0)) (d (n "docopt") (r "^0.6.50") (d #t) (k 0)) (d (n "regex") (r "^0.1.19") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.3") (d #t) (k 0)) (d (n "url") (r "^0.2.23") (d #t) (k 0)))) (h "0pg4yph8ckvyanxlpag5vsrzsppdrh6va83a6dkkvw0sbazwmp61")))

(define-public crate-po-0.1.0 (c (n "po") (v "0.1.0") (d (list (d (n "curl") (r "^0.2.2") (d #t) (k 0)) (d (n "docopt") (r "^0.6.50") (d #t) (k 0)) (d (n "regex") (r "^0.1.19") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.3") (d #t) (k 0)) (d (n "url") (r "^0.2.23") (d #t) (k 0)))) (h "1vffh5v66aavwn59ynpp36alx3ma3r8mjj6r4lbzwqrdq1cxv105")))

(define-public crate-po-0.1.1 (c (n "po") (v "0.1.1") (d (list (d (n "curl") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1gjyk145g15sxg7jgjwa3cxf2b9cadma0ss8fi6pq1bgimf9w4xj")))

(define-public crate-po-0.1.2 (c (n "po") (v "0.1.2") (d (list (d (n "curl") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0gxcmbswqgjvqrlwicyh2fkl2dpvmxwj34lkrghnn6kjnmmsjxmd")))

(define-public crate-po-0.1.3 (c (n "po") (v "0.1.3") (d (list (d (n "curl") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "03wp0m3pdlp4vw38f5hyx7lxxz9g0wcrzqrch0m0g586isirrr3k")))

(define-public crate-po-0.1.4 (c (n "po") (v "0.1.4") (d (list (d (n "curl") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1zfn6314qz9vcbk0qqmzph319hix6c475pmvwbw2x4l7qmjnxvb3")))

