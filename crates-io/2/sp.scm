(define-module (crates-io #{2}# sp) #:use-module (crates-io))

(define-public crate-sp-0.1.0 (c (n "sp") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "clipboard-ext") (r "^0.2.0") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "03c0nyrybnrj447iyhqzrxm23wqnlb5fqaja4mxl6kfw7yfilhq8")))

(define-public crate-sp-0.1.1 (c (n "sp") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "clipboard-ext") (r "^0.2.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0j1d9xxbakwzclcfjagm4wcxlhrxcapmzvgxzpdwy050b8cb3fm1")))

(define-public crate-sp-0.1.2 (c (n "sp") (v "0.1.2") (d (list (d (n "clipboard-ext") (r "^0.2.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)) (d (n "os_info") (r "^2.0.6") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0fi3ywalcxa1a6khn5wykm4x3684yrqjyz4bn1v0vf0nw99lc3l4")))

(define-public crate-sp-0.1.3 (c (n "sp") (v "0.1.3") (d (list (d (n "copypasta-ext") (r "^0.3.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)) (d (n "os_info") (r "^3.0.0") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "181hiwyr2ra682pnab1r1n4gp23hfzhcq3ldvihz4cv7mfffc0hq")))

