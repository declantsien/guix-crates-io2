(define-module (crates-io #{2}# sf) #:use-module (crates-io))

(define-public crate-sf-0.0.0 (c (n "sf") (v "0.0.0") (h "1bvxvjdgqc1wp3ijc5vjly2j82wys8d2avcmvdnc28a6sv55wqzw")))

(define-public crate-sf-0.0.1 (c (n "sf") (v "0.0.1") (h "1i6w9i4fdx5swdr2hmwsi3fas6lmvczkq3pyq1kwazgdjxl2fydn")))

(define-public crate-sf-0.1.0 (c (n "sf") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (o #t) (k 0)) (d (n "diesel") (r "^1.4.1") (o #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "19m8ndijdxsphrpk0vbz8yz4dlapblvjwbfzix0mx1jb8rllydlh") (f (quote (("readable" "base64"))))))

(define-public crate-sf-0.1.1 (c (n "sf") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.1") (o #t) (k 0)) (d (n "diesel") (r "^1.4.1") (o #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "0qpp2ylrfkxxmvq2v1cssig4p184f537pv72jcg1bfif5jyq4kqx") (f (quote (("serializable" "readable" "serde") ("readable" "base64") ("extras" "readable" "diesel" "serde"))))))

