(define-module (crates-io #{2}# ry) #:use-module (crates-io))

(define-public crate-ry-0.1.0 (c (n "ry") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1l49mpnr90jh1b5xqnqmfrrycblxvlkamrsj1wnnmjj7j4vs6drg")))

(define-public crate-ry-0.1.1 (c (n "ry") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "06svm0pqdzsbzjq6jashc990k308c1dbypwgw9g4hv2ix6ds5j9r")))

