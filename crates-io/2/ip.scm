(define-module (crates-io #{2}# ip) #:use-module (crates-io))

(define-public crate-ip-1.0.0 (c (n "ip") (v "1.0.0") (h "113fg4zwzd1nd9j5y3z8y18yb0dxwf7cpkjd6sdins5yqq001drs")))

(define-public crate-ip-1.1.0 (c (n "ip") (v "1.1.0") (h "1kk6s34lvsdwhviviv14ly4wwx9cyb12hw03jyfzmdl64n3h64kp")))

(define-public crate-ip-1.1.1 (c (n "ip") (v "1.1.1") (h "1vx0p1cadzml7zrywgrpjpi2pv75pgqid2pav1sv9zqhk7qbljbk")))

