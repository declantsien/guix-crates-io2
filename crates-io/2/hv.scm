(define-module (crates-io #{2}# hv) #:use-module (crates-io))

(define-public crate-hv-0.1.0 (c (n "hv") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1sf9x7dihs15qmkd2ni4aymwmy5n6yxxycv93hg28ipy69h0kcw5") (f (quote (("hv_10_15") ("default" "hv_10_15"))))))

(define-public crate-hv-0.1.1 (c (n "hv") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hv-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1in2q4wy6dw55x7bp5y7mpdpaw9fg4njh4w989x9qq2p0kbvhffy") (f (quote (("hv_10_15") ("default" "hv_10_15"))))))

(define-public crate-hv-0.1.2 (c (n "hv") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hv-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1gcb8sb2sibbac5ggw4xzrdpicxa4zji0lw77gm3xbcrag6qky0v") (f (quote (("hv_10_15") ("default" "hv_10_15"))))))

