(define-module (crates-io #{2}# az) #:use-module (crates-io))

(define-public crate-az-0.0.0 (c (n "az") (v "0.0.0") (h "1vfrn7gcsj32wj6d30bd4jrrcg18d8qzzw7c5s7a1lfq2af9rbqc") (f (quote (("fail-on-warnings"))))))

(define-public crate-az-0.1.0 (c (n "az") (v "0.1.0") (h "0j6rlqbrvc1rj8s1gydq07nwmwak40qd8saf8bw4c6czz70a0pwg") (f (quote (("fail-on-warnings"))))))

(define-public crate-az-0.2.0 (c (n "az") (v "0.2.0") (h "039y0sirx200vygbknf1ch92qd3hbfkadd13fb1j3qkkypbnhj2d") (f (quote (("fail-on-warnings"))))))

(define-public crate-az-0.3.0 (c (n "az") (v "0.3.0") (h "014spcvfp3via0jmx6yz9xnr7wh66d9zlbdm3plq30907f25zish") (f (quote (("fail-on-warnings"))))))

(define-public crate-az-0.3.1 (c (n "az") (v "0.3.1") (h "1pkai3hc0yg6dndb5x3ldsxxmi8pmf88rjhq12q0jgm3i61bg9j1") (f (quote (("fail-on-warnings"))))))

(define-public crate-az-1.0.0 (c (n "az") (v "1.0.0") (h "0sb51w9pjcqb315dg6zv9wwqj1q2fldcc3xmfv0bhkmajiyx9g79") (f (quote (("fail-on-warnings"))))))

(define-public crate-az-1.1.0 (c (n "az") (v "1.1.0") (h "12pc997lxgs65bzm0ligiwyff7hlcs3fz5fvmnk9amzwgf81sknq") (f (quote (("fail-on-warnings"))))))

(define-public crate-az-1.1.1 (c (n "az") (v "1.1.1") (h "1jk4pl8i0klhxlkabb1hp3mbjyawdwqhs7vb1w2hl9n0w1ipsbc2") (f (quote (("fail-on-warnings"))))))

(define-public crate-az-1.1.2 (c (n "az") (v "1.1.2") (h "09f310qixwjp560isyb55212w6b4f2hzfyrpmxq4vdcj315gyvcx") (f (quote (("fail-on-warnings"))))))

(define-public crate-az-1.2.0 (c (n "az") (v "1.2.0") (h "1i2s9bqar8hfxjmfr2cbhi4s26s2sd1kc3x3g517ygshyp8sawgp") (f (quote (("fail-on-warnings"))))))

(define-public crate-az-1.2.1 (c (n "az") (v "1.2.1") (h "0ww9k1w3al7x5qmb7f13v3s9c2pg1pdxbs8xshqy6zyrchj4qzkv") (f (quote (("fail-on-warnings"))))))

