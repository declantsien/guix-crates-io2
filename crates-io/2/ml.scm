(define-module (crates-io #{2}# ml) #:use-module (crates-io))

(define-public crate-ml-0.0.1 (c (n "ml") (v "0.0.1") (h "0smlwxqh5zqsb11q7ybbqx23j1wnxkn9q4fghlqz2c5h8qs1v28j")))

(define-public crate-ml-0.0.2 (c (n "ml") (v "0.0.2") (h "0xqskwbcaxfbv29mciw8yrirl6zmrn3n766c19sf9jhn95i724w7")))

