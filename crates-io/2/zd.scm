(define-module (crates-io #{2}# zd) #:use-module (crates-io))

(define-public crate-zd-0.0.0 (c (n "zd") (v "0.0.0") (h "1aixrf0f938davnv571awl5vpl7xx5ja5dl5zpnf4yrrgascxzvi") (y #t)))

(define-public crate-zd-0.0.1 (c (n "zd") (v "0.0.1") (h "13b0sxklbmradmjn64277yjnb8liy7v0nzngxiib6acsmlzba4wr") (y #t)))

