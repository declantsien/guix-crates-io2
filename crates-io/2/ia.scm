(define-module (crates-io #{2}# ia) #:use-module (crates-io))

(define-public crate-ia-0.1.0 (c (n "ia") (v "0.1.0") (d (list (d (n "futures") (r "^0.2") (d #t) (k 0)))) (h "042kv9gdyb77m83wjpg2k7dj1918lz4d92n1h4lpjyxx2p17nkis")))

(define-public crate-ia-0.1.1 (c (n "ia") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0rzkxv9d196b6q07hqkp50bb5va27dbq4svbs4j18lvk1yq8dqqa")))

(define-public crate-ia-0.1.2 (c (n "ia") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1qz04alp3417dnkkyplk6m5dqdzs9mfxmrsxyhldj8psj1yj1mq2")))

