(define-module (crates-io #{2}# ks) #:use-module (crates-io))

(define-public crate-ks-0.1.0 (c (n "ks") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "05i312hd7zcbw4gw4lq8di7zkjy4aa7n856hnhs8nl7k0bv5f20q")))

(define-public crate-ks-0.2.0 (c (n "ks") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0y934lvbhr1nbsdjfdc8cpqv2gx8lprmcrj4a8g6v4xzv7hnq6x1")))

