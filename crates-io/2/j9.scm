(define-module (crates-io #{2}# j9) #:use-module (crates-io))

(define-public crate-j9-0.1.0 (c (n "j9") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "j9-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "03mrdcw22mia68c6kcblydsk9r499s4gafbdykl4v1ilapcc9qcn")))

(define-public crate-j9-0.1.1 (c (n "j9") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "j9-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "18w4gnafplrrdb1kahr42rccm4x1xr7yxq7l3n99yb2bps5d5wx2")))

(define-public crate-j9-0.1.2 (c (n "j9") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "j9-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "06psa880b6rlmlxdz20r4hlkc2l4bsjn825jxxcap2fpkfj8hbq7")))

(define-public crate-j9-0.1.3 (c (n "j9") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "j9-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "16dnlzyarbl4jas8iqmfxgvvjqx4fr65qbg8dlkqivrnspv7ca3c")))

