(define-module (crates-io #{2}# pd) #:use-module (crates-io))

(define-public crate-pd-0.0.1 (c (n "pd") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.1") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "heim") (r "^0.1.0-beta.3") (f (quote ("process"))) (d #t) (k 0)))) (h "0r44g6j3lxshpl6yf39w5fs248pgyv6rvy63x5rm14prrjax1dh6") (y #t)))

(define-public crate-pd-0.0.2 (c (n "pd") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.1") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "heim") (r "^0.1.0-beta.3") (f (quote ("process"))) (d #t) (k 0)) (d (n "time") (r "^0.2.22") (d #t) (k 0)))) (h "0vd34nr2frpwlq0c6dx808f3qakhiyd4j8m09acm8d01jd0bqx2s") (y #t)))

(define-public crate-pd-0.1.0 (c (n "pd") (v "0.1.0") (h "1b6cl6kg3pxhj7bblqbxwq92j9b8i4i8yh7rw775nd7qsa3cw349")))

