(define-module (crates-io #{2}# es) #:use-module (crates-io))

(define-public crate-es-0.1.0 (c (n "es") (v "0.1.0") (h "0fps15fjbi0bz2d4pnihsaivv9w2j8cjkimk1ravncff2zlv1z0x")))

(define-public crate-es-0.1.1 (c (n "es") (v "0.1.1") (h "0v28x6v155ryzmxl6bx4g2kx5nnrjishx9ifkkl44r7pv9qmppf9")))

