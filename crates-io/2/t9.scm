(define-module (crates-io #{2}# t9) #:use-module (crates-io))

(define-public crate-t9-0.1.0 (c (n "t9") (v "0.1.0") (h "1l1avpjad1q9jvg9b8cif3bk6fnqdpikyalgygf1hxhjx1mvkx7z")))

(define-public crate-t9-0.1.1 (c (n "t9") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0pfxlpn189ws3pymma1bdsk997sa49afb7hkhwc35a6gi5sk278a")))

(define-public crate-t9-0.2.0 (c (n "t9") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "130rhms3i87ifrjcyyv3rwqvklg0av2dgfw3s1zwk86bv522lhzi")))

(define-public crate-t9-0.2.1 (c (n "t9") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0yr3p275jwkzihhbk5f9hr03j3rb63yb1hz5cs9vga2gq5if1snk")))

(define-public crate-t9-0.2.2 (c (n "t9") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "08p0xqjv8q6z2lmzgfws7pdb1ahkgyq78dg6gqcxbyxhgcvz7l2f")))

