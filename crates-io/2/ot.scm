(define-module (crates-io #{2}# ot) #:use-module (crates-io))

(define-public crate-ot-0.1.0 (c (n "ot") (v "0.1.0") (h "0mdrd3hi5mivn6bcs8bhl1azddd7phq1nlx514crpjyabd5lanrn")))

(define-public crate-ot-0.2.0 (c (n "ot") (v "0.2.0") (d (list (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.21.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "03jzq5d9nh9bgy8c239qdrxfndx1l91h0a7gmnbmz3h65mdvfrw2")))

