(define-module (crates-io #{2}# rd) #:use-module (crates-io))

(define-public crate-rd-0.1.0 (c (n "rd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1i1x2p1jcbwr45wpsc4r6kgnv3ghc39ib3ysvy4aw21dn441nrhf")))

(define-public crate-rd-0.1.1 (c (n "rd") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0gixdrw638dpvj40z7sw8xyf5ha2hh338a40bilv41bbxdiydngh")))

(define-public crate-rd-0.1.2 (c (n "rd") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0z97c2z004p8l7n3x7sbszpd81zafng03pp2f5xa5c68ml99zjw7")))

(define-public crate-rd-0.2.0 (c (n "rd") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "simple_logger") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0i0qp88pyzs131vhsyq6az1dfz5d02lz4xk9xvr5k8nqxivsjzgh")))

