(define-module (crates-io #{2}# bg) #:use-module (crates-io))

(define-public crate-bg-0.1.0 (c (n "bg") (v "0.1.0") (h "1d7qk19y382d403flj4hg5fkjpymdccl6a9dz252znhv0z414pvd")))

(define-public crate-bg-0.1.1 (c (n "bg") (v "0.1.1") (h "0vcsacnymikfpp7ps6vrjsrhlqh1r9yn4fn1hx9wpmnljzi7bzjc")))

(define-public crate-bg-0.2.0 (c (n "bg") (v "0.2.0") (d (list (d (n "ghost-lite") (r "^0.1.2") (d #t) (k 0)))) (h "10k9k38jqz0mr2s8j6l8635hvk8v4sn8vfia3zas4y5xb6cc3a1p")))

