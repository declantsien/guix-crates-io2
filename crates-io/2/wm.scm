(define-module (crates-io #{2}# wm) #:use-module (crates-io))

(define-public crate-wm-0.1.0 (c (n "wm") (v "0.1.0") (d (list (d (n "async-task") (r "^4.4.0") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.5.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "higher-kinded-types") (r "^0.1.1") (d #t) (k 0)) (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "pin-list") (r "^0.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "scoped-tls-hkt") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "unique") (r "^0.9.1") (d #t) (k 0)) (d (n "winit") (r "^0.28.6") (d #t) (k 0)))) (h "1l37nvgvgdyw52xgsdpwpw1d7iv965dczmn8hqa71r5gqqf496sy") (y #t)))

