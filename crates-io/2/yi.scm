(define-module (crates-io #{2}# yi) #:use-module (crates-io))

(define-public crate-yi-0.1.1 (c (n "yi") (v "0.1.1") (d (list (d (n "anchor-lang") (r ">=0.17, <0.21") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17, <0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "vipers") (r "^1.5.9") (d #t) (k 0)))) (h "00s1lhrga9ar3ly28xa8waa76v1675wvx52msnzcj3q78l3v3j2s") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-yi-0.1.3 (c (n "yi") (v "0.1.3") (d (list (d (n "anchor-lang") (r ">=0.21") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "vipers") (r "^1.5.9") (d #t) (k 0)))) (h "153k3gqdl9sr2qv6cjs33a0jjm7jx2z0s0jgd2934k8n8lns7k2m") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-yi-0.2.0 (c (n "yi") (v "0.2.0") (d (list (d (n "anchor-lang") (r ">=0.21") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "vipers") (r "^1.6") (d #t) (k 0)))) (h "1msrnrbvdvp5z0ay70hphs44j40gmcavqbp91pnvyx6ikbh94b0l") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-yi-0.3.0 (c (n "yi") (v "0.3.0") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "0mznyc17nypm70bamy55bqaw25ifgl0x4lj4y3jkvjp094drpmcx") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-yi-0.4.0 (c (n "yi") (v "0.4.0") (d (list (d (n "anchor-lang") (r "^0.24") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1nkn6z7ik8v4p4jp2nh9l6vm9nzfs53rkkrlm0d6skkmyn65yli4") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

