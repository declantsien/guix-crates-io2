(define-module (crates-io #{2}# lg) #:use-module (crates-io))

(define-public crate-lg-0.1.0 (c (n "lg") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "15d9ly3yz6098m0jb0j9zvgz5r069pdm3nmwlplndb9m8p2dg6w1")))

(define-public crate-lg-0.1.1 (c (n "lg") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fliaybkllq6wq76m9zbz87rc4lpfpq9v6h7jxi1xnz24zghnmf6")))

(define-public crate-lg-0.1.2 (c (n "lg") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fyd7y2sy5czb6xmz28yarzqk5m4krvz3pjbkl183z2b8vzv4kdp")))

