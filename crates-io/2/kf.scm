(define-module (crates-io #{2}# kf) #:use-module (crates-io))

(define-public crate-kf-0.1.0 (c (n "kf") (v "0.1.0") (h "0v059xk3q9z14rk3rvyrw81qjak4wqbqq09mz3i248p2lc5msrg9")))

(define-public crate-kf-0.1.1 (c (n "kf") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.21.0") (d #t) (k 0)))) (h "0jp5bjaxmll4vhvpgnkfpyanb4zh6wwdgmw5d0nwbl7jvhdvcsr5")))

(define-public crate-kf-0.1.2 (c (n "kf") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.21.0") (d #t) (k 0)))) (h "0lx8lj13s5xawl10vc3jlfxw9cpw5iwcwkqpnradhdcv7kcflcb5")))

(define-public crate-kf-0.1.3 (c (n "kf") (v "0.1.3") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1ykgrbkqhbffyksc45w01ryfya73grp5nvx6h20aaml9pl0sil0z")))

(define-public crate-kf-0.1.4 (c (n "kf") (v "0.1.4") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0z0lf4b2i3xgc22zvvxy0d52zm9j5qkxpmba006mm2f06jrl8dg0")))

