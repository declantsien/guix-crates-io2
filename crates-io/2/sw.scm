(define-module (crates-io #{2}# sw) #:use-module (crates-io))

(define-public crate-sw-0.1.0 (c (n "sw") (v "0.1.0") (d (list (d (n "appdirs") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0pxnkqdhwrsxxh1hljx0paq90mn2n8ff8yapjbmpkrbkbs5y5prx")))

