(define-module (crates-io #{2}# yq) #:use-module (crates-io))

(define-public crate-yq-0.1.0 (c (n "yq") (v "0.1.0") (h "1fd8h803b4pvd4slbdlcq93h73hlr8fl2y0p8a5rznvmzxbynqf9")))

(define-public crate-yq-0.2.0 (c (n "yq") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "redis") (r "^0.22") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0714ci8idb3ivl7khd8b2scnckc7xywc3ir0q3sn0yr9y5m2qw36")))

(define-public crate-yq-0.3.0 (c (n "yq") (v "0.3.0") (d (list (d (n "redis") (r "^0.22") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0xc4qx97xzgympijbqz7d6zcc20g91qca1bs0i9r2v94fz74b6n1")))

(define-public crate-yq-0.3.1 (c (n "yq") (v "0.3.1") (d (list (d (n "redis") (r "^0.22") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1fsh8il2h949lqyh5hpwsihz8j9i3pdridsxb6vlz2ff3l5kcldn")))

(define-public crate-yq-0.4.0 (c (n "yq") (v "0.4.0") (d (list (d (n "redis") (r "^0.22") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "16kj1f6z22mpj06yagm3cb6qk22s9l6i5wi9npqszk6sn655yal2")))

(define-public crate-yq-0.4.1 (c (n "yq") (v "0.4.1") (d (list (d (n "redis") (r "^0.23") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0sxk50mzl1w2988yi0szhm0j1zmxvap952mzg4mbh7wm8qp9kgx6")))

