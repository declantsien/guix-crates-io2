(define-module (crates-io #{2}# ap) #:use-module (crates-io))

(define-public crate-ap-0.1.0 (c (n "ap") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gh2x3fapaxl0bpn0x5k2vigyb5qjdvi2zf0788w6rb9xzdzmbf9")))

(define-public crate-ap-0.1.1 (c (n "ap") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a57apd4gin33jmxjzdb10v9p8s5j00qv4flxxki93r30yzcnf32")))

(define-public crate-ap-0.1.2 (c (n "ap") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ixw7bnl6sqflila1sxsybyhyz23l5j4kvlh8j4d2gbr4phx6ncz")))

(define-public crate-ap-0.1.3 (c (n "ap") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01wf7nlk5s8s02jzd4saygw4vspq8mrnyrha8ml3maz517an9h98")))

(define-public crate-ap-0.1.4 (c (n "ap") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1svwhbrn6gqkxcjw4xzf4yr095a2fayvn4c6hkhcwqgf1pxblcas")))

(define-public crate-ap-0.1.5 (c (n "ap") (v "0.1.5") (d (list (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g9w8x3zfv17bmkwkrwj42ri678yfm60aiqjp3v9d6ic3xdrr37m")))

