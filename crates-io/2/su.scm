(define-module (crates-io #{2}# su) #:use-module (crates-io))

(define-public crate-su-0.0.0 (c (n "su") (v "0.0.0") (h "0dlpf0ykg2bxr37m00n4j60psq3sj9253i5cpq1hsqxy5i6w33pi")))

(define-public crate-su-0.1.3 (c (n "su") (v "0.1.3") (h "1bab2jm5gcc8ssywpx6mzp86rrv8lwiw7r5yzwcidpa7nj5rny93")))

