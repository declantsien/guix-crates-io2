(define-module (crates-io #{2}# zr) #:use-module (crates-io))

(define-public crate-zr-0.6.3 (c (n "zr") (v "0.6.3") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "185z5ls5ymiydypxjbfk3wjwa6wjn4fk1h4442j5cc8vcappr2hl") (f (quote (("default"))))))

(define-public crate-zr-0.6.4 (c (n "zr") (v "0.6.4") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zksdjx6sggdpzgsykisgv1598yjbjrdamrfgps0851h0v7wms2n") (f (quote (("default"))))))

(define-public crate-zr-0.7.1 (c (n "zr") (v "0.7.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1gx1wf5wwdpir3j0bmys6yfid52dpdjxc9qki1k3sl6l3939xr6r")))

(define-public crate-zr-0.7.2 (c (n "zr") (v "0.7.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1f9vdvl90c4n7421g2c2ay46asyzihfxxbvl7zvahf6mwpjlfi90")))

(define-public crate-zr-1.0.0 (c (n "zr") (v "1.0.0") (d (list (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.6") (d #t) (k 0)) (d (n "git2_credentials") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1snymdjn53sx405xjsy0ga37yh6wiiml5xbkwcf14w5mirl91gh5")))

(define-public crate-zr-1.1.0 (c (n "zr") (v "1.1.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "git2_credentials") (r "^0.10.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1xn4svzmd4w9r21wqrs6bwp7xs9n6iv5c9ji25v3ya3dznp69gwz")))

