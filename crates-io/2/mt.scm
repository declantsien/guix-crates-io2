(define-module (crates-io #{2}# mt) #:use-module (crates-io))

(define-public crate-mt-0.1.0 (c (n "mt") (v "0.1.0") (h "0i9hh99j7l5yg4ry7zn45089fjwq4h6p2b45p5v5wl4g8w51r9p3")))

(define-public crate-mt-0.2.0 (c (n "mt") (v "0.2.0") (h "18qhrjnsk7i1xrv2v3z7p8hx4p4gs5265k4zb26sp7lf7gkm31fa")))

