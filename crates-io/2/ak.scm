(define-module (crates-io #{2}# ak) #:use-module (crates-io))

(define-public crate-ak-0.1.0 (c (n "ak") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1sajb6lab1pfcpik009k21wjfykf1spj9x16zdybi41w1q4xm998")))

(define-public crate-ak-0.1.1 (c (n "ak") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.13") (d #t) (k 0)) (d (n "codegen") (r "^0.1.0") (d #t) (k 0) (p "ak-codegen")) (d (n "futures") (r "^0.3.0-alpha.19") (f (quote ("async-await"))) (d #t) (k 0) (p "futures-preview")) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "rt") (r "^0.1.0") (d #t) (k 0) (p "ak-rt")) (d (n "tokio") (r "^0.2.0-alpha.6") (k 0)))) (h "1gnplgg5xl2q1hpl2g6a74p4vjlqfw5f4h5dqkp93xsn3wf3dx05")))

