(define-module (crates-io #{2}# q3) #:use-module (crates-io))

(define-public crate-Q3-0.1.0 (c (n "Q3") (v "0.1.0") (h "0awc27xfdbf3ydfi6jz3m6jk211v8ifcb9wfrdl3jfryr009qmws")))

(define-public crate-Q3-0.1.1 (c (n "Q3") (v "0.1.1") (h "1c628f5f2psbx7if6hba45r5nfibghzm4v6043zvv7hkipspfsc1")))

