(define-module (crates-io #{2}# ye) #:use-module (crates-io))

(define-public crate-ye-0.0.8 (c (n "ye") (v "0.0.8") (d (list (d (n "async-trait") (r "^0.1.31") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "06si3k3dcxir0w19xp2p87056hz3xrrpdhw7dvg1nac84x26hvhs")))

