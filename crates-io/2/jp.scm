(define-module (crates-io #{2}# jp) #:use-module (crates-io))

(define-public crate-jp-0.1.0 (c (n "jp") (v "0.1.0") (d (list (d (n "piston") (r "^0.35.0") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.23.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.49.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.42.0") (d #t) (k 0)))) (h "10d17bqyhbb3n7zr96cv5gaqy11xv3dbcwyy4wm02719rzdknwjb")))

(define-public crate-jp-0.2.0 (c (n "jp") (v "0.2.0") (d (list (d (n "piston") (r "^0.35.0") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.23.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.49.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.42.0") (d #t) (k 0)))) (h "1f9m1d3g46r7dwhn3wzh060dwjbs7rab3wbgf8cvldp344alv6nv")))

(define-public crate-jp-0.3.0 (c (n "jp") (v "0.3.0") (d (list (d (n "piston") (r "^0.35.0") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.23.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.49.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.42.0") (d #t) (k 0)))) (h "0gsq1br5lzygbbknbdfdszjigr4cy9p3l28hq4lqbhf46si31i61")))

(define-public crate-jp-0.4.0 (c (n "jp") (v "0.4.0") (d (list (d (n "piston") (r "^0.35.0") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.23.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.49.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.42.0") (d #t) (k 0)))) (h "1bwfdg2qxxzxa8rj39zd087482h6s9md2703ajw5khllalf8qifc")))

