(define-module (crates-io #{2}# wu) #:use-module (crates-io))

(define-public crate-wu-0.1.0 (c (n "wu") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1fg2xlgj73gqihymi7x9h1x4b964gnblbx49ny2jc8n12514m9ly")))

