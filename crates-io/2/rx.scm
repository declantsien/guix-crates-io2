(define-module (crates-io #{2}# rx) #:use-module (crates-io))

(define-public crate-rx-0.0.0 (c (n "rx") (v "0.0.0") (h "1id9ndnlr5qxg5n6sk69r62yv7kri8wxb7mr7ji25hqnkg9q7rx0")))

(define-public crate-rx-0.0.1 (c (n "rx") (v "0.0.1") (h "10pqc2b9wd3sc44bgishwjs798giw10zgrldlf7kc4fdsr8s14lx")))

