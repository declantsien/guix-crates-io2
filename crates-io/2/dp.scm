(define-module (crates-io #{2}# dp) #:use-module (crates-io))

(define-public crate-dp-0.0.1 (c (n "dp") (v "0.0.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "02j3sp43185q60r1wi6pz4lly4pyglzpsndi4rp51nryfx5pbjcs")))

(define-public crate-dp-0.0.1-1 (c (n "dp") (v "0.0.1-1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1in2xhbzg3ldg8zvq3wiq2l6zdf3gmdmvxx87gvfs4bkc10lyrzz")))

