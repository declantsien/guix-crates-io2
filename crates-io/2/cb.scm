(define-module (crates-io #{2}# cb) #:use-module (crates-io))

(define-public crate-cb-0.1.0 (c (n "cb") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "1z6lasmwz3gn9hxd8h3j8flbc93yxia34xz1x5l5p6zh8592002c")))

(define-public crate-cb-0.1.1 (c (n "cb") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "0hlm2300pdkv7iwrlfwhqsdb5ly3f5p6srv86y7xlgdfag1v1452")))

(define-public crate-cb-0.1.2 (c (n "cb") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "0rbvqf53pgzp8nvy0f2055qvx1p5289mpmw8g24bimcg8v1z8wpv")))

