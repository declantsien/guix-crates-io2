(define-module (crates-io #{2}# pv) #:use-module (crates-io))

(define-public crate-pv-0.1.0 (c (n "pv") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.12") (d #t) (k 0)))) (h "0m4myjzdqrgx8r0cqf2qrg699anz5y3dk7jm3ssqslgl99fvnhci")))

(define-public crate-pv-0.2.0 (c (n "pv") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.12") (d #t) (k 0)))) (h "15ds010hfab2ambhcz7l54qjdqsjag8sihnd70x0xvbj7py2c3bf")))

(define-public crate-pv-0.2.1 (c (n "pv") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.12") (d #t) (k 0)))) (h "0zbhwpb9ksmrwcjshvfcb7jlzhay2m0skq48mw9cj0l5xj1qyq9c")))

(define-public crate-pv-0.3.0 (c (n "pv") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)))) (h "05g4xrv1v2q5hbmriswz9jqd62chdfdp3ii19665xn0gnbz67bfi")))

