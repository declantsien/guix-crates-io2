(define-module (crates-io #{2}# s_) #:use-module (crates-io))

(define-public crate-s_-0.1.1 (c (n "s_") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1xdbsg0gk5fa1vd9xxmxbpa049cvckhn7hz52j3v9pig5a60n1kv")))

