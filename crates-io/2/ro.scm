(define-module (crates-io #{2}# ro) #:use-module (crates-io))

(define-public crate-ro-0.1.0 (c (n "ro") (v "0.1.0") (h "1csvxqb7qd00pg8vc0psxy4niar5lwfzj0kdiam1pvyr11w58bqf")))

(define-public crate-ro-0.1.1 (c (n "ro") (v "0.1.1") (h "1mhiq2jqi1c1p2jakxyagkgvwgq27jg0r1dwrb1mvyiy5lgfhck0")))

(define-public crate-ro-0.2.0 (c (n "ro") (v "0.2.0") (h "0a6h0ippialhcvyizi22vni2aihngw83k0nc6ij6wdln5nr2wsaj")))

(define-public crate-ro-0.3.0 (c (n "ro") (v "0.3.0") (h "1l9q91bgfq8j9xahclxyb4k2v6qhys9bxbx93w1s5gbq3p7gq949")))

(define-public crate-ro-0.4.0 (c (n "ro") (v "0.4.0") (h "01rnp72ixj17x8ky7887k1yl0x0cm7yw5xm9xgrws05c2m6xxsby")))

(define-public crate-ro-0.5.0 (c (n "ro") (v "0.5.0") (h "0lxq48knkrrgl2abprnripflbwjciaqhn9nnma6rrb5iimi6dgjj")))

(define-public crate-ro-0.6.0 (c (n "ro") (v "0.6.0") (h "0j8jw883krmp2cpi290d8q2nwqj9c3vd341r7lfz2z11ph5s55mn")))

(define-public crate-ro-1.0.0 (c (n "ro") (v "1.0.0") (h "0wp4wa3wc4qmdk3m75nk74h543avmpssbhhd3cirp15l2b9aa9w2")))

