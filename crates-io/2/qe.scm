(define-module (crates-io #{2}# qe) #:use-module (crates-io))

(define-public crate-qe-0.4.0 (c (n "qe") (v "0.4.0") (h "0crdijp1ksbg4c7916hzr5vd7sqpkk462zn45zyz10pffngsyybp")))

(define-public crate-qe-0.4.1 (c (n "qe") (v "0.4.1") (h "1gcsfdirzy7njhp68dbxmm2n2xa7k5y77s0064rbsmpi4v2cn8gp")))

(define-public crate-qe-0.4.2 (c (n "qe") (v "0.4.2") (h "0igy8l3bd4hynfvq3ihmfr8dm89sdyjyspl8v471yva986v88c1s")))

