(define-module (crates-io #{2}# ga) #:use-module (crates-io))

(define-public crate-ga-0.1.1 (c (n "ga") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "0ka43lrkg6r4qmwllfqxc1795v0w0rdj25bgb20aavy3djfqlgxw")))

(define-public crate-ga-0.1.2 (c (n "ga") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.10.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "05i3y8ig56084rm6p6cvc23wi430q40wzhv9vc39hyalvfkql67h")))

(define-public crate-ga-0.1.3 (c (n "ga") (v "0.1.3") (d (list (d (n "ndarray") (r "^0.10.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "1ky343g3j7n9rh3ncccdskxbpydc3s0hpzryya462kdink05wri3")))

(define-public crate-ga-0.1.4 (c (n "ga") (v "0.1.4") (d (list (d (n "ndarray") (r "^0.10.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "038lbwybl54rcyrnnamxp1hrnbr8yyv1c8pgvkpmkrm8m0mf0rxf")))

