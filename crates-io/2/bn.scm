(define-module (crates-io #{2}# bn) #:use-module (crates-io))

(define-public crate-bn-0.1.0 (c (n "bn") (v "0.1.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1xqn4wzp2ibzbg1y8v5cqg5kb7njzkcp5f861inav558ab950850")))

(define-public crate-bn-0.2.0 (c (n "bn") (v "0.2.0") (d (list (d (n "bincode") (r "~0.6.0") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "~0.5.3") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)))) (h "1q215kw3a1a4pf75pixz2lll9y38x8z5p6c6axvml58lfwgz6zlp")))

(define-public crate-bn-0.2.1 (c (n "bn") (v "0.2.1") (d (list (d (n "bincode") (r "~0.6.0") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "~0.5.3") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)))) (h "193107c04512h53dqqd5nc566vjxa0x49lp3kmljvd0kcai2qy72")))

(define-public crate-bn-0.2.2 (c (n "bn") (v "0.2.2") (d (list (d (n "bincode") (r "~0.6.0") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "~0.5.3") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)))) (h "10g30r7a2djcivsy9hm3bq3vmbi3gs8kfpcfv0wz0v07akj1r3xr")))

(define-public crate-bn-0.2.3 (c (n "bn") (v "0.2.3") (d (list (d (n "bincode") (r "~0.6.0") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "~0.5.3") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)))) (h "07lki1vfrjqk4fy4vy5l8jldfwn0js469rwsdlmlpk1j3h00mqp0")))

(define-public crate-bn-0.3.0 (c (n "bn") (v "0.3.0") (d (list (d (n "bincode") (r "~0.6.0") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "~0.5.3") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)))) (h "1q0grmn0fh5adasss85vlm5d5fyxyg9j46iwy1869i7xq822088n")))

(define-public crate-bn-0.4.0 (c (n "bn") (v "0.4.0") (d (list (d (n "bincode") (r "~0.6.0") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "~0.5.3") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)))) (h "0c6hbi783hwrqpcy0ibyxj46ydpn42p4n001lngzzdjfv0klcq1x")))

(define-public crate-bn-0.4.1 (c (n "bn") (v "0.4.1") (d (list (d (n "bincode") (r "~0.6.0") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "~0.5.3") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)))) (h "0bzvb8qr613lz9v2zygmcv3nwckasx26fpv4pmr5aq2g50b5d3cz")))

(define-public crate-bn-0.4.2 (c (n "bn") (v "0.4.2") (d (list (d (n "bincode") (r "~0.6.0") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "~0.5.3") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)))) (h "1ssyg37i2b6m505m0ccnqgxc362jg2dhflsf4l64b1l8b4hzbmnz")))

(define-public crate-bn-0.4.3 (c (n "bn") (v "0.4.3") (d (list (d (n "bincode") (r "~0.6.0") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "~0.5.3") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)))) (h "01jrjxcd3mlys13hpp472ari058qqkaqa5w4ankiy5jygydjf3qi")))

