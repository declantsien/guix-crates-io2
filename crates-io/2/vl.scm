(define-module (crates-io #{2}# vl) #:use-module (crates-io))

(define-public crate-vl-0.0.1 (c (n "vl") (v "0.0.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 1)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1sxzjfjmvfc1jw8mhbdrvci9y37ig5ds570fh698i8w288g4rrvi")))

(define-public crate-vl-0.0.2 (c (n "vl") (v "0.0.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 1)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1wyhwfv06k9qwz8j6lazf7nwj9x3pwjid3xvl472ixim290nny7l")))

(define-public crate-vl-0.0.3 (c (n "vl") (v "0.0.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 1)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mlzi77r1391x4ny2kqvrl9i0gwf5lnv0x4dkhrjs98ymrs4d2v2")))

