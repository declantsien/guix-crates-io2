(define-module (crates-io #{2}# px) #:use-module (crates-io))

(define-public crate-px-0.0.0-alpha (c (n "px") (v "0.0.0-alpha") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "0378wpkvi8vpnzkabacw77pdvd6lc2ji0khnmk405bm9l2n19ynj") (y #t)))

(define-public crate-px-0.0.1-alpha (c (n "px") (v "0.0.1-alpha") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "16z3y1cl90lyh5bysnk09ifj83bq2cifnmgh2fjhrbapacmq1i9d") (y #t)))

(define-public crate-px-0.0.2 (c (n "px") (v "0.0.2") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "1b7kp13nadrfq3dgjqx2ws1hz2i34555a2kbd7bs05p8zcr0cy5h") (y #t)))

