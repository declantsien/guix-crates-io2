(define-module (crates-io #{2}# aa) #:use-module (crates-io))

(define-public crate-aa-0.1.0 (c (n "aa") (v "0.1.0") (h "0mm4wn4k3lq3yg33l4sl3pqa2gnasir6yg162fpqp3ryqqsv518f")))

(define-public crate-aa-0.1.1 (c (n "aa") (v "0.1.1") (h "05h0wznq9sbqjh151zp3kfkmgzggzb0cgb6p4hih5akkdyhv4jjh")))

