(define-module (crates-io #{2}# nz) #:use-module (crates-io))

(define-public crate-nz-0.1.0 (c (n "nz") (v "0.1.0") (h "19lrzd5gjwahzaj353ccq65qli6as50vjybvfw58vajzgm57h8fx") (r "1.47.0")))

(define-public crate-nz-0.1.1 (c (n "nz") (v "0.1.1") (h "1zbawrlqwkn81syisrnik1bkb7rbzac8j5rgif16q1ydz9245q0h") (r "1.47.0")))

(define-public crate-nz-0.1.2 (c (n "nz") (v "0.1.2") (h "1cf79ng0khsq93pm2lm16qfck6gkqgl3h9w1yypsmvpyrh1ha1cr") (r "1.47.0")))

(define-public crate-nz-0.1.3 (c (n "nz") (v "0.1.3") (h "13s2lprg70q0p84bxjj8i9jfpg8h118fkqa4g99m1gm70hvvbv8l") (r "1.47.0")))

(define-public crate-nz-0.1.4 (c (n "nz") (v "0.1.4") (h "15p418l8zlic4z9jn76r5312dq9m9f2298y5y90mfmjg5y6k5hnn") (r "1.47.0")))

(define-public crate-nz-0.2.0 (c (n "nz") (v "0.2.0") (h "0frscql0msrvbdwwbgwd0lkck6c8r3n47hxqc6q72nfzxqg6xinw") (r "1.56.0")))

(define-public crate-nz-0.2.1 (c (n "nz") (v "0.2.1") (h "1krma3zik29ycj391681yv6fv9b00cmsiwq5dkmshiq6963qsxlj") (r "1.56.0")))

(define-public crate-nz-0.2.2 (c (n "nz") (v "0.2.2") (h "0h0r7ifq9gp7xp3q61y2f5zm8bniwjhh2kyx1097sp83jdfnbzx7") (r "1.56.0")))

(define-public crate-nz-0.3.0 (c (n "nz") (v "0.3.0") (h "1kcxm2n1h616yd5wh9bdmx4yrapkba05n3ag3lp7xws3ddn4rbdl") (r "1.56.0")))

(define-public crate-nz-0.3.1 (c (n "nz") (v "0.3.1") (h "0fjrcfqx1kd1inc6s1asr4jbh4vj1pm80rjiqifx9wppj9iyvicb") (r "1.56.0")))

(define-public crate-nz-0.3.2 (c (n "nz") (v "0.3.2") (h "0ldxpqbd09s11bwpyvsiy2mp7v8p2y59cf5z9g77wzasfamrrfi8") (r "1.56.0")))

(define-public crate-nz-0.3.3 (c (n "nz") (v "0.3.3") (h "1c1myv1pir8wqp9c9lran3r41k181b2gxb2gvmlz3qiv5gn9wx6b") (r "1.56.0")))

(define-public crate-nz-0.3.4 (c (n "nz") (v "0.3.4") (h "12bxivys2q3mf1jan0q1qiplk3hxyp10jd73zwx87rf7zahy97j1") (r "1.56.0")))

(define-public crate-nz-0.4.0-beta.0 (c (n "nz") (v "0.4.0-beta.0") (h "0l61vz510xmsrfxkbb7hwp22x8jiqx1rlv24lmj4951dqz8jqqhz")))

(define-public crate-nz-0.3.5 (c (n "nz") (v "0.3.5") (h "1ac68dxpha9lc23d4xs1lrgjqml7sw05mci64y0j64zmydfnbsfi") (r "1.56.0")))

(define-public crate-nz-0.4.0-beta.1 (c (n "nz") (v "0.4.0-beta.1") (h "1mbcq1g95b69jjn6gwysj3rwdsy2szid44hwg69c7gdb17aj05l4")))

(define-public crate-nz-0.4.0-beta.2 (c (n "nz") (v "0.4.0-beta.2") (h "17wk189ly865hsrm4myn07981dqbpc136vzdfzb6dvyy2ng7y2la")))

