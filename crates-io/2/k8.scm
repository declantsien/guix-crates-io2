(define-module (crates-io #{2}# k8) #:use-module (crates-io))

(define-public crate-k8-0.1.0 (c (n "k8") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.18.0") (f (quote ("v1_26"))) (d #t) (k 0)) (d (n "kube") (r "^0.83.0") (f (quote ("runtime" "derive"))) (d #t) (k 0)))) (h "10n1xs47m550fka0r1i9ygdyzvd0yv0jvsv2ks4l90z8zafqyfbb")))

