(define-module (crates-io #{2}# lx) #:use-module (crates-io))

(define-public crate-lx-0.1.0 (c (n "lx") (v "0.1.0") (h "1pb7qjjfd9y09zwdrmh3kdlspwa8mdwhnlglmzn0gymqb0ncn226")))

(define-public crate-lx-0.2.0 (c (n "lx") (v "0.2.0") (h "15jdd45qx556cmmlk6apf28j0ssc68r0xszs8a1zwpf844d97s5j")))

(define-public crate-lx-0.3.0 (c (n "lx") (v "0.3.0") (h "1xhg680wk4851jw6v8q8i334kh9a7w46dldv4347f31d8l6xchj4")))

(define-public crate-lx-0.3.1 (c (n "lx") (v "0.3.1") (h "10z7s4dfqfx2pyan7f4khz8zyg25586ypqrlgwxgq9g6rg3nm94i")))

(define-public crate-lx-0.3.2 (c (n "lx") (v "0.3.2") (h "13qi58zbv6p2xd6abfpajcmmiv5bi2nb27djk8a7l6519a7cqdyg") (y #t)))

(define-public crate-lx-0.3.3 (c (n "lx") (v "0.3.3") (h "1q1b7mvxr2mr46vx22a5hzzafb7x0bmfk7f4gww5hpqyz8zydwpv")))

(define-public crate-lx-0.3.4 (c (n "lx") (v "0.3.4") (h "0q3xyxlm650b9by9b1mgxw28y3mr365nd4kfrphv7qpw1izfn7mz")))

(define-public crate-lx-0.2.1 (c (n "lx") (v "0.2.1") (h "1cwd3c0z5vfnyvfv85wn9r477vx7mvnvdvf8i1yj048qn661lc7h")))

(define-public crate-lx-0.4.0 (c (n "lx") (v "0.4.0") (h "04dgjk9zbcdlkqsamnf063agsnvyrf9md7f6l0872srfg27aq94z")))

