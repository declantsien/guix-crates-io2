(define-module (crates-io #{2}# ge) #:use-module (crates-io))

(define-public crate-ge-0.1.0 (c (n "ge") (v "0.1.0") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "1g6piskqnrhs1hrng5g0cw9yfjbw18a7qc5bvfdcc49zzgdm4ijk")))

(define-public crate-ge-0.2.0 (c (n "ge") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "1a1bqi7llgbca69v1q6vdha2ppqahymj8yhnny2f4jhzw4rmz4xh")))

