(define-module (crates-io #{2}# tu) #:use-module (crates-io))

(define-public crate-tu-0.1.0 (c (n "tu") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.7") (d #t) (k 0)))) (h "0772vhj49n0izg68s7swg9ismyzljkjnzxaqldnhl0v055hg15gz")))

(define-public crate-tu-0.2.0 (c (n "tu") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "scanlex") (r "^0.1.2") (d #t) (k 0)))) (h "0v9jl7is0dd35f2lx7wpfb8rnc873ikvj3rjm6rbhlf0z815hqdm")))

