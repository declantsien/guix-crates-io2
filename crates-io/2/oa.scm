(define-module (crates-io #{2}# oa) #:use-module (crates-io))

(define-public crate-oa-0.1.0 (c (n "oa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "08cisghas2r0pfn078scncm4lssa2yy3lhjh7mgjdmzqvnvfzd3h")))

