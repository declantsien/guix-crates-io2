(define-module (crates-io #{2}# uq) #:use-module (crates-io))

(define-public crate-uq-0.1.0 (c (n "uq") (v "0.1.0") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)))) (h "0ggvl62iprr574kgx4q03angffkswqyr4bd6mzmd8znqrrj11kiv")))

(define-public crate-uq-0.1.1 (c (n "uq") (v "0.1.1") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)))) (h "0x3906ry62635ywwl7rqrkq40hk7b8lq8xa1vbijq9kyr7dbkhxm")))

(define-public crate-uq-0.1.2 (c (n "uq") (v "0.1.2") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)))) (h "0iy1a8qhjnm2q5mhzbc8apjqwjar3f63h7hkzw2k0h721979h7hy")))

