(define-module (crates-io #{2}# ux) #:use-module (crates-io))

(define-public crate-ux-0.0.1 (c (n "ux") (v "0.0.1") (h "0pnmck2d2v335699i4i3cfhrv5jn9sg206wphd5kflhz4d4lb0mk") (f (quote (("std") ("default" "std"))))))

(define-public crate-ux-0.1.0 (c (n "ux") (v "0.1.0") (h "0dajxnhx7crwkbzz3wfga6xk31958gi7klivzsh5zk72xm5pcql7") (f (quote (("std") ("default" "std"))))))

(define-public crate-ux-0.1.1 (c (n "ux") (v "0.1.1") (h "1lazisx37z1183i5cq5ij27s5s2biqk12kykgxxyjwm7920mgdch") (f (quote (("std") ("default" "std"))))))

(define-public crate-ux-0.1.2 (c (n "ux") (v "0.1.2") (h "0hdb3wx1ds2ryiwh9drxxbdgm0a4v63iiph2sb6fszyhv1fxzn2k") (f (quote (("std") ("default" "std"))))))

(define-public crate-ux-0.1.3 (c (n "ux") (v "0.1.3") (h "0bq1xn45xqmkqiijmnkir7q251k77s79zzdn1h665kk13dqyppw8") (f (quote (("std") ("default" "std"))))))

(define-public crate-ux-0.1.4 (c (n "ux") (v "0.1.4") (h "1n53jgf0s9ai7vjc0rll8j434l0dq7mnrrinjfy83frkbf4czzay") (f (quote (("std") ("default" "std"))))))

(define-public crate-ux-0.1.5 (c (n "ux") (v "0.1.5") (h "0zi05iclcf75fai8mpiia6n7ivrjzycf7hbjflkac1v9wd3zzcrc") (f (quote (("std") ("default" "std"))))))

(define-public crate-ux-0.1.6 (c (n "ux") (v "0.1.6") (h "15mhn6anqj2f5i1pfmgx4j33apc2jq0xksxv4qrfadp02xagqn9v") (f (quote (("std") ("default"))))))

