(define-module (crates-io #{2}# la) #:use-module (crates-io))

(define-public crate-la-0.1.0 (c (n "la") (v "0.1.0") (d (list (d (n "num") (r "^0.1.25") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "01n09cicxrkjwya5qpz9fjs54dmhm5bkwa5vd2wzzjrs9fc5f4c1")))

(define-public crate-la-0.2.0 (c (n "la") (v "0.2.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0msrwqkkgdnrf9bg05lv8xvzsh70xaz61lajv5rvfjvspfahwl60")))

