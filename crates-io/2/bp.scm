(define-module (crates-io #{2}# bp) #:use-module (crates-io))

(define-public crate-bp-0.0.0 (c (n "bp") (v "0.0.0") (h "0q8wxsw4qb2v342wl2a0c8d8lk1533fvavn83vjd3pzg9xpyx7rx")))

(define-public crate-bp-0.0.1 (c (n "bp") (v "0.0.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)))) (h "0975k1xhfk9y56ydyzqw4vp1p9s4rlwn8ksq6i4rnhld0mjimg97")))

(define-public crate-bp-0.1.0 (c (n "bp") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)))) (h "0azdbl5px17zf08nsflwp43yvwv30v0djmq2zzk9q2wjhnwh1ys6")))

(define-public crate-bp-0.1.1 (c (n "bp") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)))) (h "10dwrcn1dxahci0n9dyxfkppd40bw6wm89yri7z11q6mpx9vdckx")))

(define-public crate-bp-0.1.2 (c (n "bp") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)))) (h "09a6d3gc4qzwr56fzd7by3hhrzz2fin691wd52kl6dz9n9n9psv6")))

(define-public crate-bp-0.1.3 (c (n "bp") (v "0.1.3") (d (list (d (n "copypasta") (r "^0.10.0") (d #t) (k 0)))) (h "07653247fi1mbndj8ifjnm6wprm274z141xd97ijbv921wc04xn9")))

(define-public crate-bp-0.1.4 (c (n "bp") (v "0.1.4") (d (list (d (n "copypasta") (r "^0.10.0") (d #t) (k 0)))) (h "14ny3q2rzm71r9cg35jijv1wvbi9d5xmq1gh5jm1p3b03kp0qzdq")))

