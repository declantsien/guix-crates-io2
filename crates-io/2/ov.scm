(define-module (crates-io #{2}# ov) #:use-module (crates-io))

(define-public crate-ov-0.0.1 (c (n "ov") (v "0.0.1") (h "1k0d4s7h7xxs0swslh5dhbx61hq8wlmdci5rpfgfbl0s1f0w3f5n")))

(define-public crate-ov-0.1.0 (c (n "ov") (v "0.1.0") (h "0gnqqiryrnri9iwypvzhv20c47isysajpffnpglbiz2cxak7mamj")))

