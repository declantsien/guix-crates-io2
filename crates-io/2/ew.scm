(define-module (crates-io #{2}# ew) #:use-module (crates-io))

(define-public crate-ew-0.4.0 (c (n "ew") (v "0.4.0") (d (list (d (n "ew-testfunc") (r "^0.1.0") (d #t) (k 2)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "02a29k2dihic59n0s01pka38wmpcv2wyxndl0pyfk3cr0lvsnb0y")))

