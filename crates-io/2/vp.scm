(define-module (crates-io #{2}# vp) #:use-module (crates-io))

(define-public crate-vp-0.1.0 (c (n "vp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1kzvbgxk4fmcjzcdkx34vckckhjs5sd8jzknpwmn4jklfn2f5p5d") (y #t)))

(define-public crate-vp-0.1.1 (c (n "vp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0h7lmladc7d5xjw0yyd9in9awn0l1myh9b20acgadnna2qjk86jf")))

(define-public crate-vp-0.1.2 (c (n "vp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1rbmwc3fm7a0mayiv2y9l0cyc881bvj6sgf1j076i65qxnza1fyj")))

