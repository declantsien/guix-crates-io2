(define-module (crates-io #{2}# gd) #:use-module (crates-io))

(define-public crate-gd-0.0.0 (c (n "gd") (v "0.0.0") (h "0y1s1xjq5q6jj6m3n8lkdxf5r7w90h0j15zqgfza18wa4n7mrqi5")))

(define-public crate-gd-0.0.1 (c (n "gd") (v "0.0.1") (h "0zw06w8hxz88x944zdqj7052w2g2hs07xhd68h37x52lnm7f5qjx")))

