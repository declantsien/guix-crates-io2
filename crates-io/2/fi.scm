(define-module (crates-io #{2}# fi) #:use-module (crates-io))

(define-public crate-fi-0.2.0 (c (n "fi") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0imygyfhq153s6g1zhkpx3svv2kp12c32n9gh2k549cwpirqpkr2") (f (quote (("schedule") ("default"))))))

