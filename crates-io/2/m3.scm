(define-module (crates-io #{2}# m3) #:use-module (crates-io))

(define-public crate-m3-1.1.0 (c (n "m3") (v "1.1.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vd1x9bpqxvb802ci9jy1jhrf41587c9485py7ik360b9syxnsys")))

