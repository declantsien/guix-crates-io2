(define-module (crates-io #{2}# zp) #:use-module (crates-io))

(define-public crate-zp-0.1.0 (c (n "zp") (v "0.1.0") (d (list (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)))) (h "0ip15rfvy0r4s4lvdndykmsn86fnnpqvf3vs0aq9h57nldd6n6yv") (y #t)))

(define-public crate-zp-0.1.1 (c (n "zp") (v "0.1.1") (d (list (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)))) (h "0m6y4qsfzx97jm03qhcr14sfvwhxlvkf3775fdk9mca4qlad74fd")))

(define-public crate-zp-0.2.0 (c (n "zp") (v "0.2.0") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bhvj1afxjdh61qs9vwvwlwrcbrzdj3sxpmnxwml9ypn32rlnbnv")))

(define-public crate-zp-1.0.0 (c (n "zp") (v "1.0.0") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ksj2jpwv8jvqndhq0p9yj6gxrxcbih1zfiw1jq513bzsjvb3cbg")))

