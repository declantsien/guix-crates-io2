(define-module (crates-io #{2}# wn) #:use-module (crates-io))

(define-public crate-wn-0.1.0 (c (n "wn") (v "0.1.0") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cf5rmk3ybvbzxka7napqzws1mbc9p6adrvk8dkq11y5lbqnvhi1")))

(define-public crate-wn-0.2.0 (c (n "wn") (v "0.2.0") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1n0h41xli3jqpzph58yc7i7b2h1vml4x12c29489fbiwyla2n8li")))

(define-public crate-wn-0.3.0 (c (n "wn") (v "0.3.0") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gzmbh4y4pixn3rskqgwfh0l8p2xf5bhaw1mfjgyhiryssf11jr2")))

(define-public crate-wn-0.4.0 (c (n "wn") (v "0.4.0") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0b93f3pfz567h31zvskfzzsrvz3hxz3ld7b7nv1pg9bnz5mq11q7")))

