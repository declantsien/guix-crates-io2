(define-module (crates-io #{2}# ou) #:use-module (crates-io))

(define-public crate-ou-0.1.0 (c (n "ou") (v "0.1.0") (d (list (d (n "axum") (r "^0.5.13") (d #t) (k 0)) (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.3.0") (f (quote ("fs"))) (d #t) (k 0)))) (h "1n4m1na0mdyc3i5qix7mbk105gwq3b0xsx7zw6xwrm8r15brzrsk") (r "1.62.1")))

(define-public crate-ou-0.1.1 (c (n "ou") (v "0.1.1") (d (list (d (n "axum") (r "^0.5.15") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.3.4") (f (quote ("fs"))) (d #t) (k 0)))) (h "1sdi4kmsi2n3fpmk2fjrf20bx4rc5by4rbx66abd7caaigkbqsp5") (r "1.62.1")))

(define-public crate-ou-0.1.2 (c (n "ou") (v "0.1.2") (d (list (d (n "axum") (r "^0.5.15") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.3.4") (f (quote ("fs"))) (d #t) (k 0)))) (h "1hdm6b31gmgs1h5cm8avgzbna1xlmvb42fg0xkizcrli76vbxzq3") (r "1.62.1")))

(define-public crate-ou-0.1.3 (c (n "ou") (v "0.1.3") (d (list (d (n "axum") (r "^0.5.15") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.3.4") (f (quote ("fs"))) (d #t) (k 0)))) (h "01mk4iliwm61xpg9zadzf7xwfnhlb9y0ij8xk7mn4zfmrz7jvamc") (r "1.62.1")))

(define-public crate-ou-0.1.4 (c (n "ou") (v "0.1.4") (d (list (d (n "axum") (r "^0.5.15") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.3.4") (f (quote ("fs"))) (d #t) (k 0)))) (h "0v8wmiiz5a3a2p1ixnql6vbzxalxmdwd6bp1layl3p8q2mdvihfs") (r "1.62.1")))

(define-public crate-ou-0.1.5 (c (n "ou") (v "0.1.5") (d (list (d (n "axum") (r "^0.5.15") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.3.4") (f (quote ("fs" "cors"))) (d #t) (k 0)))) (h "06sjb8ijq25p73935vznflm1cg52059awpkyd5k16fqgmjvbgvw3") (r "1.62.1")))

(define-public crate-ou-0.1.6 (c (n "ou") (v "0.1.6") (d (list (d (n "axum") (r "^0.5.15") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "open") (r "^3.0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.3.4") (f (quote ("fs" "cors"))) (d #t) (k 0)))) (h "06c363lljjbxam64zga5i87nihjsja7hbn0q8fig5snijwcnlhva") (r "1.62.1")))

