(define-module (crates-io #{2}# wa) #:use-module (crates-io))

(define-public crate-wa-0.1.0 (c (n "wa") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0672nw2bpkz76hsd9acrswaxk7gvdzknrsqfg8mrwnvv41qmd4v0") (f (quote (("default" "console_error_panic_hook")))) (y #t)))

(define-public crate-wa-0.1.1 (c (n "wa") (v "0.1.1") (h "142rj131qbhjlynicif43qaflracqaarxr2z5bzdk54ih305vqcc") (f (quote (("vector") ("string") ("array")))) (y #t) (r "1.64.0")))

(define-public crate-wa-0.1.2 (c (n "wa") (v "0.1.2") (h "1y06vwv538mgifrw1qdbbclcv0waykikzshmb8pl5hmcainllbcr") (f (quote (("vector") ("string") ("array")))) (y #t) (r "1.64.0")))

(define-public crate-wa-0.1.3 (c (n "wa") (v "0.1.3") (h "16n0lf287izgwd99vbwwbzasyqk7xwdlazask7h2wj2hxk07q6ks") (f (quote (("vector") ("string") ("array")))) (y #t) (r "1.64.0")))

(define-public crate-wa-0.1.4 (c (n "wa") (v "0.1.4") (h "1i8bvm4nc8j7yk2bv8x6x5ygcb5fzp01yc2x8yi15g19sska9jzg") (f (quote (("vector") ("string") ("array")))) (y #t) (r "1.64.0")))

(define-public crate-wa-0.1.5 (c (n "wa") (v "0.1.5") (h "0mvgj7zr6vzrvq4cdyn6vkxqm1m4lm3vmm64531ydp3bshbiwhip") (f (quote (("vector") ("string") ("array")))) (y #t) (r "1.64.0")))

(define-public crate-wa-0.1.6 (c (n "wa") (v "0.1.6") (h "0hsjkbqwiq83biihichy9fvgw93lickcw4xbvxa383s6mck9kgas") (f (quote (("vector") ("string") ("default" "string" "array" "vector") ("array")))) (r "1.64.0")))

