(define-module (crates-io #{2}# iv) #:use-module (crates-io))

(define-public crate-iv-0.1.0 (c (n "iv") (v "0.1.0") (d (list (d (n "html2md") (r "^0.2.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "09lzxi8s7wj7axgxyvhwp1mbsy417biw1igvdj2clix7rlnhczf7")))

(define-public crate-iv-0.1.1 (c (n "iv") (v "0.1.1") (d (list (d (n "html2md") (r "^0.2.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "04qp4dq0x4r45mr3id9rs4g9w6afjw563f4k0n5315rhgfljw292")))

