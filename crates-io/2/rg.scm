(define-module (crates-io #{2}# rg) #:use-module (crates-io))

(define-public crate-rg-0.1.0 (c (n "rg") (v "0.1.0") (h "0j0jbp27944qyx4w6w5gqqwzrrkd6h5c8mxrasmb5mrrf65gmnyz")))

(define-public crate-rg-0.2.0 (c (n "rg") (v "0.2.0") (h "101k3m6r6nh2vvjyv6zm80aysc53184k5sg3ak1yf1mj7pjz2ib1")))

(define-public crate-rg-0.2.1 (c (n "rg") (v "0.2.1") (h "0krlpslg71ma472q8ks4fa96kph2z0n9j1hq9s9dczpb49c1wp87")))

