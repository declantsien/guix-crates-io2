(define-module (crates-io #{2}# oi) #:use-module (crates-io))

(define-public crate-oi-0.0.0 (c (n "oi") (v "0.0.0") (h "07sd1p60hsdnj2rsksx1a2hlymgy87vd8h9rslxci3yhmndk70p1")))

(define-public crate-oi-0.0.1 (c (n "oi") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0fw2776d3bi800n54iqfwri8gxw7mf4ab1g2ck79b61924vsvjs3")))

(define-public crate-oi-0.0.2 (c (n "oi") (v "0.0.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0h991sraqv3k8qsml2bj3gn8qm3vkyq7rjy5fqnyprgvgsza0x3a")))

