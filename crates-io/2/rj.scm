(define-module (crates-io #{2}# rj) #:use-module (crates-io))

(define-public crate-rj-0.0.1 (c (n "rj") (v "0.0.1") (d (list (d (n "fns") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "weak-table") (r "^0.3") (d #t) (k 0)))) (h "0jg69mn2ndhsli8vk3zrkg3v15f8ckw83bhsig1gv4x1xais5kb4")))

(define-public crate-rj-0.1.0 (c (n "rj") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04g84nl7yp84bsss5f0gm7h0hsgirv4080w2l6wm0pc2hc88njgy")))

(define-public crate-rj-0.1.1 (c (n "rj") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1gqnnl0wn5dk315sibgb2hjii108gli8k8ycwc5x093fq5kng1yc")))

