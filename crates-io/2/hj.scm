(define-module (crates-io #{2}# hj) #:use-module (crates-io))

(define-public crate-hj-0.1.0 (c (n "hj") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "0qv0fmhf1zfrgwl418lrr66l18rp2qj1akdf4kssh82486bimfb5")))

