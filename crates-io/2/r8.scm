(define-module (crates-io #{2}# r8) #:use-module (crates-io))

(define-public crate-r8-0.1.0 (c (n "r8") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpal") (r "^0.14.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pixels") (r "^0.11.0") (d #t) (k 0)) (d (n "r8lib") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.13.0") (d #t) (k 0)))) (h "1xcb2k6maqj4i1lc3f07ml7m7jgjjy9jg6vfs744kar87m9zqq0j")))

