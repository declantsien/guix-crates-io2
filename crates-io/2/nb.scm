(define-module (crates-io #{2}# nb) #:use-module (crates-io))

(define-public crate-nb-0.1.0 (c (n "nb") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)))) (h "0vs0lxgsz98vip4g72mvqv1i676y00vcc8yzwzn2wmspymz8bm0n") (f (quote (("unstable")))) (y #t)))

(define-public crate-nb-0.1.1 (c (n "nb") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)))) (h "1p7cjs2bj4q8vamc7jxjxlxldh1c7sicv6bag86qrawzzssq1wv9") (f (quote (("unstable"))))))

(define-public crate-nb-0.1.2 (c (n "nb") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)))) (h "1p6v8ngaabssq8sgzkf2ks5x4ivb4pxa140azgnivhdkpr8iahdi") (f (quote (("unstable"))))))

(define-public crate-nb-1.0.0 (c (n "nb") (v "1.0.0") (h "1blc9143cqh3cn2imr050qczbnfrfdl10xxnfdggamlybnn3fv2l")))

(define-public crate-nb-0.1.3 (c (n "nb") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "0vyh31pbwrg21f8hz1ipb9i20qwnfwx47gz92i9frdhk0pd327c0") (f (quote (("unstable"))))))

(define-public crate-nb-1.1.0 (c (n "nb") (v "1.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "179kbn9l6vhshncycagis7f8mfjppz4fhvgnmcikqz30mp23jm4d") (s 2) (e (quote (("defmt-0-3" "dep:defmt")))) (r "1.60")))

