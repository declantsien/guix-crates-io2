(define-module (crates-io #{2}# oe) #:use-module (crates-io))

(define-public crate-oe-0.0.2 (c (n "oe") (v "0.0.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (d #t) (k 0)))) (h "11qks4r4a913nk24nfqh330315qyxan8gzykrbbm48ydhblgkams")))

