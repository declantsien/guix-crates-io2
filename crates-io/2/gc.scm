(define-module (crates-io #{2}# gc) #:use-module (crates-io))

(define-public crate-gc-0.0.1 (c (n "gc") (v "0.0.1") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)) (d (n "gc_plugin") (r "^0.0.1") (d #t) (k 0)))) (h "171b7qiirb3agdishfdby4qgnkrgbcxh5ys6246m3czlz3ph1c31")))

(define-public crate-gc-0.0.2 (c (n "gc") (v "0.0.2") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)) (d (n "gc_plugin") (r "^0.0.1") (d #t) (k 0)))) (h "1caqvvg2z1s16w3wk03wqcm32dj8ljgv6gfba8rch1sg83cb5kj9")))

(define-public crate-gc-0.0.3 (c (n "gc") (v "0.0.3") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)) (d (n "gc_plugin") (r "^0.0.3") (d #t) (k 0)))) (h "0vr6xbdll8wzi887akkzfdbcjkq6gipf21hvbhlssk1vz6s2s21a")))

(define-public crate-gc-0.0.4 (c (n "gc") (v "0.0.4") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)) (d (n "gc_plugin") (r "^0.0.4") (d #t) (k 0)))) (h "1n3x48qhfhlxzhqgrds1gjhg3203v8rk6rbl185p8z6d4lk5a2mw")))

(define-public crate-gc-0.0.5 (c (n "gc") (v "0.0.5") (d (list (d (n "gc_plugin") (r "^0.0.5") (d #t) (k 0)))) (h "0s1iqndpa5dx9m9ihzavmcr0a3g8grl9vjv14rs2fbilrbkdidfs")))

(define-public crate-gc-0.0.6 (c (n "gc") (v "0.0.6") (d (list (d (n "gc_plugin") (r "^0.0.6") (d #t) (k 2)))) (h "0nylnv30x57jc9y3h56ddvg4qdsvrl50an36fg77wqrx88pv3zmb")))

(define-public crate-gc-0.0.7 (c (n "gc") (v "0.0.7") (d (list (d (n "gc_plugin") (r "^0.0.6") (d #t) (k 2)))) (h "02h8i0jf3yxainpsqbg0wvzayxln60i7q840piilra6hbsawgx39")))

(define-public crate-gc-0.0.8 (c (n "gc") (v "0.0.8") (d (list (d (n "gc_plugin") (r "^0.0.8") (d #t) (k 2)))) (h "0dji4fhx8w7cqfdmxil59chy6iphbhs4lnmcv2zj23g22583x7q3")))

(define-public crate-gc-0.1.0 (c (n "gc") (v "0.1.0") (d (list (d (n "gc_plugin") (r "^0.1.0") (d #t) (k 2)))) (h "112v1zxjfqsq08c40sk8z8ns6hmcpfwi0fflrs5fz16smphjyps5")))

(define-public crate-gc-0.0.9 (c (n "gc") (v "0.0.9") (d (list (d (n "gc_plugin") (r "^0.0.9") (d #t) (k 2)))) (h "1l9cfybqrr6l8br2w5fvycx796phvip9bikrr7344y12qdnd8mnz")))

(define-public crate-gc-0.1.1 (c (n "gc") (v "0.1.1") (d (list (d (n "gc_plugin") (r "^0.1.1") (d #t) (k 2)))) (h "1zjlckbsra8zk96aazxgivqhhjz587ag9bqgikqcf10ib0p14j66")))

(define-public crate-gc-0.2.0 (c (n "gc") (v "0.2.0") (d (list (d (n "gc_derive") (r "^0.2.0") (d #t) (k 2)))) (h "04c94j0azh4scchd1g8h47dk5v7cnw57fvfnqyfgm8ah2rsn0hk4")))

(define-public crate-gc-0.2.1 (c (n "gc") (v "0.2.1") (d (list (d (n "gc_derive") (r "^0.2.1") (d #t) (k 2)))) (h "1jsz23vgxz2i31nhjpmygf5ldwl5aairxy6i5ad9jnynchzs3kxh")))

(define-public crate-gc-0.3.0 (c (n "gc") (v "0.3.0") (d (list (d (n "gc_derive") (r "^0.3.0") (d #t) (k 2)))) (h "1ckw75yi61cfi4fg9q18gh7s16lj7kay3hg1wrab3hswvf59vla3") (f (quote (("nightly"))))))

(define-public crate-gc-0.3.1 (c (n "gc") (v "0.3.1") (d (list (d (n "gc_derive") (r "^0.3") (d #t) (k 2)))) (h "1sfb198vnd28rqjqzgwfivhvfql7sm0rcdnczgihddfal12myzbi") (f (quote (("nightly"))))))

(define-public crate-gc-0.3.2 (c (n "gc") (v "0.3.2") (d (list (d (n "gc_derive") (r "^0.3") (d #t) (k 2)))) (h "16pa6448nmcm7zn9kgfpbqm68l5rp3msjkchvs4zqw12zkx1a2v3") (f (quote (("nightly"))))))

(define-public crate-gc-0.3.3 (c (n "gc") (v "0.3.3") (d (list (d (n "gc_derive") (r "^0.3") (d #t) (k 2)))) (h "04mmxgmhjkqcns71z6z5k800hwvylr1f91dn6c63v2r4xh06hrbm") (f (quote (("nightly"))))))

(define-public crate-gc-0.3.4 (c (n "gc") (v "0.3.4") (d (list (d (n "gc_derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "gc_derive") (r "^0.3") (d #t) (k 2)))) (h "1la1fvqfa1i61aqa576njyq531cwcqyfqvhkz6x92w1r6dr7p4gl") (f (quote (("nightly") ("derive" "gc_derive"))))))

(define-public crate-gc-0.3.5 (c (n "gc") (v "0.3.5") (d (list (d (n "gc_derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "gc_derive") (r "^0.3") (d #t) (k 2)))) (h "0rypncjkgcshmgcfdhigmphgf4xa0c2mx91b8g9h7gp3qfdx8mhs") (f (quote (("nightly") ("derive" "gc_derive"))))))

(define-public crate-gc-0.3.6 (c (n "gc") (v "0.3.6") (d (list (d (n "gc_derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "gc_derive") (r "^0.3") (d #t) (k 2)))) (h "0mk7xzlpd7j4j66af245gn51zvd98fr3sj71k514fcczk138sjy9") (f (quote (("nightly") ("derive" "gc_derive"))))))

(define-public crate-gc-0.4.0 (c (n "gc") (v "0.4.0") (d (list (d (n "gc_derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "gc_derive") (r "^0.4") (d #t) (k 2)))) (h "1ykyy51dg0b53xlczsfnc1j7s1sa3sy2vs9b0ii4wr509fq8jx53") (f (quote (("nightly") ("derive" "gc_derive"))))))

(define-public crate-gc-0.4.1 (c (n "gc") (v "0.4.1") (d (list (d (n "gc_derive") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "gc_derive") (r "^0.4.1") (d #t) (k 2)))) (h "0lipbfgmgmk2vkhhplj58q602j12jdybf32jk6y2w81jb07srniy") (f (quote (("nightly") ("derive" "gc_derive"))))))

(define-public crate-gc-0.5.0 (c (n "gc") (v "0.5.0") (d (list (d (n "gc_derive") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "1hy8gdyi3pfj0vg83vxvi1q76gd6s4i9bcglcpkyvjjqgmwh6fpp") (f (quote (("unstable-stats") ("unstable-config") ("nightly") ("derive" "gc_derive"))))))

