(define-module (crates-io #{2}# sc) #:use-module (crates-io))

(define-public crate-sc-0.1.0 (c (n "sc") (v "0.1.0") (h "02lw4j38yqrcvsvzciph1g40irxl8mz92n3wwmwff29jafijkhyr")))

(define-public crate-sc-0.1.1 (c (n "sc") (v "0.1.1") (h "1c23mp5grzgi6zc8p1xz7kxfzyjahhnzqxpnql37ixybz8yrn56y")))

(define-public crate-sc-0.1.2 (c (n "sc") (v "0.1.2") (h "1skxxvyj2534m5bdbj4f93zl0wb3p8nbhy61hknjlm0f08hkj6ya")))

(define-public crate-sc-0.1.3 (c (n "sc") (v "0.1.3") (h "13j9nzjxs4r9181di6gg8xbzwr1d4b9ijdkv5smxdrzkk4lg34m4")))

(define-public crate-sc-0.1.4 (c (n "sc") (v "0.1.4") (h "1jddvgb3rrhmnh9jd6dm46bfcmxp0sgcjr01xn53zyw4dma8ixm3")))

(define-public crate-sc-0.1.5 (c (n "sc") (v "0.1.5") (h "1w1rbdhj2b8rkwkc8dc0qz3589qiw9w7z0mmgzkndnh3dmrzm9bm")))

(define-public crate-sc-0.2.0 (c (n "sc") (v "0.2.0") (h "195bad372np1cf7wvg38jylqrh6czzcqv0gi4dkxy86cq79kcncr")))

(define-public crate-sc-0.2.1 (c (n "sc") (v "0.2.1") (h "0c3zzfwac0a0c0yvsfd057iayx5wzv358w2pa665d2h8zjdlq361")))

(define-public crate-sc-0.2.2 (c (n "sc") (v "0.2.2") (h "0xpkj742jqp0axid1jkpmpvl32xdb7pvabgcr8jpqw2ap8kb1fsf")))

(define-public crate-sc-0.2.3 (c (n "sc") (v "0.2.3") (h "1csm99kninfp3r9az65lglnzxcx162bnsjvp8z8ilf1y4p46aqqp")))

(define-public crate-sc-0.2.4 (c (n "sc") (v "0.2.4") (h "0144klj1zxfjw4id2qvi287ll82m11hlmbg08hjw3n4wxjsn5hyn")))

(define-public crate-sc-0.2.5 (c (n "sc") (v "0.2.5") (h "15xm8bw2crr75pzdylhw59nybjn3i357ifaqbbi8fqf7js8ya75n")))

(define-public crate-sc-0.2.6 (c (n "sc") (v "0.2.6") (h "0sqgvkcr1fhrs3z7flxx9y9g58i6y0lrxlwsf2cwm2gdfwcq80km")))

(define-public crate-sc-0.2.7 (c (n "sc") (v "0.2.7") (h "12x3c3mn36am3jfamswqfsd0vpr0hz3kdck6wskla7gx7fyih3h1")))

