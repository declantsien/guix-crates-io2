(define-module (crates-io #{2}# fj) #:use-module (crates-io))

(define-public crate-fj-0.1.0 (c (n "fj") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "decorum") (r "^0.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.26.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.18") (d #t) (k 0)) (d (n "wgpu") (r "^0.8.1") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "1gn9ba0lm4bp24kj3kfjflz66mxbgvhaljsz7va7c1953hgml4ad")))

(define-public crate-fj-0.1.1 (c (n "fj") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "decorum") (r "^0.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.26.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.18") (d #t) (k 0)) (d (n "wgpu") (r "^0.8.1") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "01nhmygybdysk7yqg7sl7glz5aqngpfvq4r95nwz0sfm93jfsjf3")))

(define-public crate-fj-0.2.0 (c (n "fj") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytemuck") (r "^1.6.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "decorum") (r "^0.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.27.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.18") (d #t) (k 0)) (d (n "wgpu") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "0v5pinqrfzd6nk8bydyq6vsj5qns3sh6a6s0q0if7hrzil3vdayq")))

(define-public crate-fj-0.3.0 (c (n "fj") (v "0.3.0") (h "108j0m8gx2q2ax84n7s9zhl2cvdqhyj85sj7p7q0csyi1dij64sx")))

(define-public crate-fj-0.3.1 (c (n "fj") (v "0.3.1") (h "0vl9gqn7wvc57dz7w16arnpg3yzr3l78dhqxpxxkm1a3dz4frj5s")))

(define-public crate-fj-0.4.0 (c (n "fj") (v "0.4.0") (h "0mznnjhz5bxz9xz36l70rfq2y0a80fwz5xq4q148jl152rxxd33i")))

(define-public crate-fj-0.5.0 (c (n "fj") (v "0.5.0") (h "1wxbd5yfx1fzq12178n5bw37lqfd21gm3hig2bj20yngn69crr21")))

(define-public crate-fj-0.6.0 (c (n "fj") (v "0.6.0") (h "0sn6cr2ahdqan2dsgs9i04bs71fv4lyhjsm8ykrcxc993940bqib")))

(define-public crate-fj-0.7.0 (c (n "fj") (v "0.7.0") (d (list (d (n "fj-proc") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "0g5nd0s1x0ji1ww12dz2s1h6ysdyb21qq6yk1pgpwlqirkl6avck")))

(define-public crate-fj-0.8.0 (c (n "fj") (v "0.8.0") (d (list (d (n "fj-proc") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "0aly9z1k3vjqz8ag13y2bds2ggf990wv6v0kzyl1qn02a3mv62sc")))

(define-public crate-fj-0.9.0 (c (n "fj") (v "0.9.0") (d (list (d (n "fj-proc") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "1idgh3qlq2i5g8kimcxdwi3zp3lbi155himhf5r4jvcqmsw8ygpj")))

(define-public crate-fj-0.10.0 (c (n "fj") (v "0.10.0") (d (list (d (n "fj-proc") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "14xn5ia31ffc9xnmdq8kzi7f8y3csaf81gf3yjsd0b07gmn8k310")))

(define-public crate-fj-0.11.0 (c (n "fj") (v "0.11.0") (d (list (d (n "fj-proc") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "02z48y6s8swx13jglj2c4714yxml2cixgd57fgddjm2gp3ad9zir")))

(define-public crate-fj-0.12.0 (c (n "fj") (v "0.12.0") (d (list (d (n "fj-proc") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 2)))) (h "1a5dw0g3nmqchn4zxm068la12ccyg7j78807jhl3pi4z36dsp3d1")))

(define-public crate-fj-0.13.0 (c (n "fj") (v "0.13.0") (d (list (d (n "fj-proc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "038b2ncdnrn9jh82m4d3yfjyrihflxr08xy2f5gwzgj6bqgkzak3")))

(define-public crate-fj-0.14.0 (c (n "fj") (v "0.14.0") (d (list (d (n "fj-proc") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "08x3ijsnqsf34pgl8h0pf50pmj4cxhsyjhj5kbkybhp1i7byjjnf")))

(define-public crate-fj-0.15.0 (c (n "fj") (v "0.15.0") (d (list (d (n "fj-proc") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "1hdxfc15zjf7kl3394xjch2lg729xqcfmrb9bsfr03w0jmjsn5np")))

(define-public crate-fj-0.16.0 (c (n "fj") (v "0.16.0") (d (list (d (n "fj-proc") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "02l0cq98q0vcv485jaqyrxvygsmzxi20g1sdbpgli5lry4sp4p8m")))

(define-public crate-fj-0.17.0 (c (n "fj") (v "0.17.0") (d (list (d (n "fj-proc") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "0mjrgva9rnhanzmdk91f5jmqcy7y4hkg7qi30hbv5p2dhl2l7sa4")))

(define-public crate-fj-0.18.0 (c (n "fj") (v "0.18.0") (d (list (d (n "fj-proc") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "1kj55mjamwcxgym0chvwvrgzby8kzf91nhq47w2wkvczsky76xdr")))

(define-public crate-fj-0.19.0 (c (n "fj") (v "0.19.0") (d (list (d (n "fj-proc") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "1ys90zyh3c8smmid3ia8z87lz9sfpn1l14v5q0rj5na9ws0na0rn")))

(define-public crate-fj-0.20.0 (c (n "fj") (v "0.20.0") (d (list (d (n "fj-proc") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "179pf1dlgqq4xxg8gcnzpihss8n6px3ph4zjcr03gz2zpgnrg3s9")))

(define-public crate-fj-0.21.0 (c (n "fj") (v "0.21.0") (d (list (d (n "fj-proc") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "0kmkmrjpp53amimqkks1iwlqhm3qlvfx7cg1rzv4n95ixjcxd50r")))

(define-public crate-fj-0.22.0 (c (n "fj") (v "0.22.0") (d (list (d (n "fj-proc") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "01slgig7mbx112wq27qdil74vwfk9m728qd5chc1ncf0sdi7ly1y")))

(define-public crate-fj-0.23.0 (c (n "fj") (v "0.23.0") (d (list (d (n "fj-proc") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "1nmry70iiwfaw38vhd1qdicgpqkqhc3ziz6077g4dagvz5pwsqmg")))

(define-public crate-fj-0.24.0 (c (n "fj") (v "0.24.0") (d (list (d (n "fj-proc") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "05hw4l6zzjfgj87sy0hiylr130c0lr7mh5kysb75n13kvryiiqz5")))

(define-public crate-fj-0.25.0 (c (n "fj") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 1)) (d (n "fj-proc") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "0nimm3v89p8knlw2kl6wyjrqq1mnwway0lxxyqp895kc9rlyv86a")))

(define-public crate-fj-0.26.0 (c (n "fj") (v "0.26.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 1)) (d (n "fj-proc") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "14fxjhlzpvsimllch0nc5raf3p501rab4qz4lzr75jik3in5ryks")))

(define-public crate-fj-0.27.0 (c (n "fj") (v "0.27.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 1)) (d (n "fj-proc") (r "^0.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "0qydh6brfjz7g1n1w4f081xgzrchpypddy4a6j4qxq6ifz4j1b6k")))

(define-public crate-fj-0.28.0 (c (n "fj") (v "0.28.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 1)) (d (n "fj-proc") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "0v68ybd5a3fnywqg2fjvwaw5255q71v0x0nfvpc03l755s2zx05l")))

(define-public crate-fj-0.29.0 (c (n "fj") (v "0.29.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 1)) (d (n "fj-proc") (r "^0.29.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "1r9y34dngj5b87c4p31y5p9dhny0avsj81423mhr42v8sy0iqwmw")))

(define-public crate-fj-0.30.0 (c (n "fj") (v "0.30.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "fj-proc") (r "^0.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "0y5sxmmrw2ss8qpg8qi8nisbjd8l4fria30pa10nl8456d96dp3k")))

(define-public crate-fj-0.31.0 (c (n "fj") (v "0.31.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "fj-proc") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "1738331ivww862x9pdb5c7436qdq3qbnr7wls4pabw2yhyslwipm")))

(define-public crate-fj-0.32.0 (c (n "fj") (v "0.32.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "fj-proc") (r "^0.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "04yqjzgs46ixl00pc505al7pja1k7dhd45afvlmj0679dwlw4k7r")))

(define-public crate-fj-0.33.0 (c (n "fj") (v "0.33.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "fj-proc") (r "^0.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "16vk1ajvhrdfvyzj7zywj9qgnvdkmjbfya40ladlak1cc50mswr7")))

(define-public crate-fj-0.34.0 (c (n "fj") (v "0.34.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "fj-proc") (r "^0.34.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "08j82kqcb453b6gim1p2rysg2rbb0v37arcgbldbxymhimlj78rk")))

(define-public crate-fj-0.35.0 (c (n "fj") (v "0.35.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "fj-proc") (r "^0.35.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "1rsn2i9fv6g8cyy9vcsfzsrspmdjlyia2hb19scwpsjsmvzpv2w9")))

(define-public crate-fj-0.36.0 (c (n "fj") (v "0.36.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 1)) (d (n "fj-proc") (r "^0.36.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 2)))) (h "08rirvpndyc8dnahh3zxcgildfbpr6acny0inhcpzh34gy23s0z1")))

(define-public crate-fj-0.37.0 (c (n "fj") (v "0.37.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 1)) (d (n "fj-proc") (r "^0.37.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "1fwm3fbsbnlxzmwbn7vxm0ws9nhch412biqlwmgdgyjb1z754vk5")))

(define-public crate-fj-0.38.0 (c (n "fj") (v "0.38.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 1)) (d (n "fj-proc") (r "^0.38.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "07hyi0mbhrg7z8jm3ip9azkgr4dashsrvgp83b2r1vsqmd9zwd56")))

(define-public crate-fj-0.39.0 (c (n "fj") (v "0.39.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 1)) (d (n "fj-proc") (r "^0.39.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "1id0hbhx1j4w2fl2vcq67wn1mi5n90630alz26y32y8xq1qibbnw")))

(define-public crate-fj-0.40.0 (c (n "fj") (v "0.40.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 1)) (d (n "fj-proc") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "09nc0bk4v4q33dpivcybmijf3iz03ydrzszydp92qw4bfdd1cba0")))

(define-public crate-fj-0.41.0 (c (n "fj") (v "0.41.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 1)) (d (n "fj-proc") (r "^0.41.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "197f1snm10s40afw1knx0sa1cg29hl5shj29z82hi8izwwzyy19y")))

(define-public crate-fj-0.42.0 (c (n "fj") (v "0.42.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "fj-proc") (r "^0.42.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "1wn7basd7jhn6s667mhlln511ljcrilmscln5divm8ghvmcwzvx5")))

(define-public crate-fj-0.43.0 (c (n "fj") (v "0.43.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "fj-proc") (r "^0.43.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0z9vlhj55bax7jiihk1l55lw1f4crby7zaxhihrdf7m4hnmbbma2")))

(define-public crate-fj-0.44.0 (c (n "fj") (v "0.44.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "fj-proc") (r "^0.44.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "1rx7hmfb43bgx4f8wdshzabhll72nbilwmcramkiz6vnbbkcghzs")))

(define-public crate-fj-0.45.0 (c (n "fj") (v "0.45.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 1)) (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "fj-proc") (r "^0.45.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "01vmgldr8qw1wn8jmhvh53kdq1ahxlq94w6k2ckdx1vgrywgzg44")))

(define-public crate-fj-0.46.0 (c (n "fj") (v "0.46.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 1)) (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "fj-proc") (r "^0.46.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "1dbfx7b7gd4p9aiaqlj2x0ng3m3swiskp00jw3nj01fhlhy27p7l")))

(define-public crate-fj-0.46.1 (c (n "fj") (v "0.46.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 1)) (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "fj-proc") (r "^0.46.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0vg7x4s5k38cx84f4k4d6ripv64pdhp31g2vvfh73bqsrv9q326i")))

(define-public crate-fj-0.46.2 (c (n "fj") (v "0.46.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 1)) (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "fj-proc") (r "^0.46.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "19kh327gc1vx7r21y0cgq4cvcqnfz1vg468agkjb108y04ybgil0")))

(define-public crate-fj-0.47.0 (c (n "fj") (v "0.47.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fj-core") (r "^0.47.0") (d #t) (k 0)) (d (n "fj-export") (r "^0.47.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.47.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.47.0") (d #t) (k 0)) (d (n "fj-viewer") (r "^0.47.0") (d #t) (k 0)) (d (n "fj-window") (r "^0.47.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0cfxqdacpsf9w3c3xwf90fqwcq3r5y6spw5h2n0mdjm5b65n4jc4")))

(define-public crate-fj-0.48.0 (c (n "fj") (v "0.48.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fj-core") (r "^0.48.0") (d #t) (k 0)) (d (n "fj-export") (r "^0.48.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.48.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.48.0") (d #t) (k 0)) (d (n "fj-viewer") (r "^0.48.0") (d #t) (k 0)) (d (n "fj-window") (r "^0.48.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0zicsbr025zxhp7dmw4a6jk5xv845wcfrr0zyll7yhvmd092ba34")))

(define-public crate-fj-0.49.0 (c (n "fj") (v "0.49.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fj-core") (r "^0.49.0") (d #t) (k 0)) (d (n "fj-export") (r "^0.49.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.49.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.49.0") (d #t) (k 0)) (d (n "fj-viewer") (r "^0.49.0") (d #t) (k 0)) (d (n "fj-window") (r "^0.49.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0v8shfn6d4dqq0vrl6dnqzinmqg4xqy4dzkyill17pppxiydp4v7")))

