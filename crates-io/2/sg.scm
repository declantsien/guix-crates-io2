(define-module (crates-io #{2}# sg) #:use-module (crates-io))

(define-public crate-sg-0.1.0 (c (n "sg") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "vecio") (r "^0.1.0") (d #t) (k 0)))) (h "1s80damc4bz97cywj6fwc7728a2l80av9w5fkswim3i08c51j3ax")))

(define-public crate-sg-0.2.0 (c (n "sg") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "vecio") (r "^0.1.0") (d #t) (k 0)))) (h "0r5qcbl722k4chshsci0g68801pm0qali0jjknnlianr6lz8dy29")))

(define-public crate-sg-0.2.1 (c (n "sg") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "vecio") (r "^0.1.0") (d #t) (k 0)))) (h "0gcnb7fak6lanj628j6va3hxcmdyzy2dg5ffqwnv6z978wv9v39m")))

(define-public crate-sg-0.3.0 (c (n "sg") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.43.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "mio") (r "^0.6.16") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "0p2zl734vzb8dp0s16p6381j23z2bm57l73qgfgrxig7fhfkcq1y") (f (quote (("polling" "mio") ("default"))))))

