(define-module (crates-io #{2}# cc) #:use-module (crates-io))

(define-public crate-cc-0.0.1 (c (n "cc") (v "0.0.1") (h "038jjxncbqwq30cqrk2p6jcaslhygx3l2ph2vbs2bz4li7d8dx5d")))

(define-public crate-cc-1.0.0 (c (n "cc") (v "1.0.0") (d (list (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1cbn5aqp6kdd5lkfgm05hz1wqlbzbbb9rc31wyz0yzld413g3ckx") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.1 (c (n "cc") (v "1.0.1") (d (list (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1bgg57zbn49bili9x74kd6pchchwrdd07sl4a48d9nz3f044yrrc") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.2 (c (n "cc") (v "1.0.2") (d (list (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1x5hjcyjzi17knysdv0md6f74k8pgi82yb61avfis34wp6yijh7g") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.3 (c (n "cc") (v "1.0.3") (d (list (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "06cpxw95v9k80iblf1j7nlq7jqfa5hqdpswqcp6hxcynxxbkmcd9") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.4 (c (n "cc") (v "1.0.4") (d (list (d (n "rayon") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1q7y1y55mbicz4qkv4wwnb5vj1vj158fylac81dv4sr5av39xbyy") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.5 (c (n "cc") (v "1.0.5") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "13v139n3vlnqh5glni863pq29ln7y0qd2dlpn44m8ql8x4j6pqlv") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.6 (c (n "cc") (v "1.0.6") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0jbzg2ssf05lhdmip9kfdvnkffvp63wzhgpl9xf8d7mc35sngpzy") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.7 (c (n "cc") (v "1.0.7") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1xnw3amjpic07q7imdqr9n91s5h0di97wxc5m4cm6qdm5l98zww7") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.8 (c (n "cc") (v "1.0.8") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0qbdcl2z1wssd7m0md916rviwr2cpjd50vkfl745w4hrwwkl2cnr") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.9 (c (n "cc") (v "1.0.9") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1z0z6h0jinp3zag0g7jd9nrz28wfyd7qazhfd33h0hfbppj12j9b") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.10 (c (n "cc") (v "1.0.10") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1q3yp75cd7rhn4vdscydxx9p40ny7q1wkmn5dn3sacc6yw02k7cb") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.11 (c (n "cc") (v "1.0.11") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "139kg9bcadyv4z910x2n3jwsa0ayrqcc5wgs1whivvb3k0al5xc4") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.12 (c (n "cc") (v "1.0.12") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "086yi3n4lkn96126ff8hvcbagls8d43wj1wgnnn1jk6hl1c0dfd9") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.13 (c (n "cc") (v "1.0.13") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1546zgnrznhj06bh0dssnlayw9kcim0mnryg01knl8f9nbp4byrq") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.14 (c (n "cc") (v "1.0.14") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0604345fc6yz99a3bhgm1dj3q7w6ii40f1hq09l8cnv9ay0p36vf") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.15 (c (n "cc") (v "1.0.15") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1fj0mi8a1yjd9dwh0qzx438wq323nqzhwspn1in42lb1278qgfqf") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.16 (c (n "cc") (v "1.0.16") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "08m7a9s8s9ix55588sfji0ihc8slzmypsp3h5qbch3zj9bp62gdr") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.17 (c (n "cc") (v "1.0.17") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0zblx1k79vvnrf9n76dsp6j512dvppyw7sracavvbvv8awpi9v29") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.18 (c (n "cc") (v "1.0.18") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0xajy7m6d06n5xvrzq6g3ddxbci0jxq6gd6amv9qwaxxcx4fl691") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.19 (c (n "cc") (v "1.0.19") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0vcfzjl95idsjhghw62v2ckdmwd4n8njgw6dp0algpwscc65y3c5") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.20 (c (n "cc") (v "1.0.20") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "050vgwq66yzizg6xa3l8fljsh5b9xlcrc2clmxrmhqgakarkiirw") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.21 (c (n "cc") (v "1.0.21") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1hg6fkr5sxr4jnhslgw7klw0gn66iw2bkw23qhx8a5cb68s5wlma") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.22 (c (n "cc") (v "1.0.22") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "01q09asiybpzvp2f1ql6pzk8k3lxzrzv1557292qvlpx8v0hfq2a") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.23 (c (n "cc") (v "1.0.23") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0cg1lgribw74wd8prw2d879rgbz4vr2bdm02dzx03c5rlkx0wzy3") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.24 (c (n "cc") (v "1.0.24") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1l8lwn4x3znib7ylvwf789gsdh8zp0jyzn09qa8yxkk95s6aiwkh") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.25 (c (n "cc") (v "1.0.25") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "05lx7kdn4cjsjsb4sn6hqkdb01ilgbmh6xx0an0d1i336gadyngi") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.26 (c (n "cc") (v "1.0.26") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "00lfcrymf7g5769knjsjcan3agaawhnjn2fbzvkl5lkkd7ih761q") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.27 (c (n "cc") (v "1.0.27") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0vrxk685v4z5jx20393wz9wa1xn1s2wna0xkzqfjswmxyyax2phm") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.28 (c (n "cc") (v "1.0.28") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0jgpzik1wr9jgzgnadq3j9s2qi8x5x5wiiw7x8372ndlbiqqnjmv") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.29 (c (n "cc") (v "1.0.29") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0kpcp8r96lkay690xlzxfwvyaczlklvjh0f0s30ykg7nyjss7423") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.30 (c (n "cc") (v "1.0.30") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "14pb0lgz07xhnzw81y84bf0iq3y7hh7y75kiy0qz41zjiz86j76h") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.31 (c (n "cc") (v "1.0.31") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0gg5l8iy1wi7jmrz8mavwlpzw46rmspda2rzcdjzikxahyq8pkn9") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.32 (c (n "cc") (v "1.0.32") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1cmryzqlzvrbckv0jrcvd9pv0hsbfxw73vgpylw442zs0krsw3dd") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.33 (c (n "cc") (v "1.0.33") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "011wdjgb7jf3sd51qan6l4jggav92ml5cq5rmkdrz3qqcycsc855") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.34 (c (n "cc") (v "1.0.34") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1zc8hl8rjdjs58bl633larh18i46nz3d63qrm7nii2h48nzi7y1h") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.35 (c (n "cc") (v "1.0.35") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "10wcqmgfszsqfb3b7ygza0w97vmxhq9f9wc14x634q7bbvp3ypsy") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.36 (c (n "cc") (v "1.0.36") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "03f9n36ls3lkdm0s2whd56ig59484mxk65j5kkn0xf3v90b65id0") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.37 (c (n "cc") (v "1.0.37") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0zby3adq7zdxpn4dnx1q2mv9rzs9ss7z4s111mb5gbxvsx25bxrr") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.38 (c (n "cc") (v "1.0.38") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0imzcz53wg7m3gr6657yrikp7icyc2bpkvssnyd0xvj8imihqh6f") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.39 (c (n "cc") (v "1.0.39") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0lj8idylgyi01y3ka5q0v8sls6lgjn6hbgk2xp1dd0s0ac2frjhw") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.40 (c (n "cc") (v "1.0.40") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "19wx2frj1nvq654ra14gsyzkwsf7hgmcy8jfkn8rbjzwh7pa8j5m") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.41 (c (n "cc") (v "1.0.41") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1zxzd559dbbf1iwdzmkj7czapzccs17kqqmsj9ayijpdix5rrbld") (f (quote (("parallel" "rayon"))))))

(define-public crate-cc-1.0.42 (c (n "cc") (v "1.0.42") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "01mlsln1qi4a27v90mzrl5cjjv15fsymx4ybr1pawbydap77n756") (f (quote (("parallel" "num_cpus" "jobserver"))))))

(define-public crate-cc-1.0.43 (c (n "cc") (v "1.0.43") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "01vb844n8sj8w5dv5dbzg42niajyzx1y1ipwq2avni6h8z2qgwvw") (f (quote (("parallel" "num_cpus" "jobserver"))))))

(define-public crate-cc-1.0.44 (c (n "cc") (v "1.0.44") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "16pdih6vfwvzmdg1mvc4nj39bsiyc5ab869qx5ih0yjwkmyn41m3") (f (quote (("parallel" "num_cpus" "jobserver"))))))

(define-public crate-cc-1.0.45 (c (n "cc") (v "1.0.45") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1gm1llpdm3ch58myngjca6id47k7837scjy5bygfp4223xga7jag") (f (quote (("parallel" "num_cpus" "jobserver"))))))

(define-public crate-cc-1.0.46 (c (n "cc") (v "1.0.46") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "177rxsz16y5cf3k5xwhqbj13kmiww8xpq0qbqhc2rsn4sdbd64q2") (f (quote (("parallel" "num_cpus" "jobserver"))))))

(define-public crate-cc-1.0.47 (c (n "cc") (v "1.0.47") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1f08560cwbalni1fc2jcmh1dszl3rc31azvr45bgz8vhrs6hb1xa") (f (quote (("parallel" "num_cpus" "jobserver"))))))

(define-public crate-cc-1.0.48 (c (n "cc") (v "1.0.48") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0xlzv0k5a9xsgd967d7bwwpl289l710v425zxcwdi8vccrd4capm") (f (quote (("parallel" "num_cpus" "jobserver"))))))

(define-public crate-cc-1.0.49 (c (n "cc") (v "1.0.49") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "03n2lj4kyyw9hz7wzfkkzc0xgrickwpnjdv4gi72fvxajbdbhl74") (f (quote (("parallel" "num_cpus" "jobserver"))))))

(define-public crate-cc-1.0.50 (c (n "cc") (v "1.0.50") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1kdqm8ka7xg9h56b694pcz29ka33fsz27mzrphqc78gx96h8zqlm") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.51 (c (n "cc") (v "1.0.51") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1fx19piqfxdnxxjrrf7rxlyyg8s3crnrjp4sw53ymh4h9g5894ww") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.52 (c (n "cc") (v "1.0.52") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "07g3qpa0gab3b0niw5ljzak6lgq34zjsv98hylxd0b59sqippn63") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.53 (c (n "cc") (v "1.0.53") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "073mrlm2szqwz2dj9v4k8iwfx5j5l0v3nzmiadvmg22jyvj1yjs0") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.54 (c (n "cc") (v "1.0.54") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "04c31hi55lhv1kkssixa3wjjx6izlbxx01z36j86w9616vdp7fvv") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.55 (c (n "cc") (v "1.0.55") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0s33v6h00d6kqi7sg9sxw803z6wx0d8wbypmnppdqysdz44k9gmi") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.56 (c (n "cc") (v "1.0.56") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0imxsbn0nrkvxmfr9yi5wrwb214j86mn0i8z5czbghci03bg3hbp") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.57 (c (n "cc") (v "1.0.57") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1gnyxsrj4b6f24jndx48w7zy7vmx3x9kzimvd2b4ramzlb95bphg") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.58 (c (n "cc") (v "1.0.58") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "065m2fphrv5csy8hvv37msi3a739mfkgw7pcg71dfw9jwnr6z87r") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.59 (c (n "cc") (v "1.0.59") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "109k07z56xwj9hafgmhbwn6fq0fj3ck7rh4dfw2v0gvp2pshl4k6") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.60 (c (n "cc") (v "1.0.60") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0p42g8m4cb0jnv5zbi2jv09mf9w5044dszfp6n2z30zpiz31qqgg") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.61 (c (n "cc") (v "1.0.61") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0pcq3hidadsjl8hg66cyn3zyx0clfkj6nrf4bzkkhqim13gcnrzd") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.62 (c (n "cc") (v "1.0.62") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0h0vchyzij5aci7w65wjapsadv0lri65jwy4cy5ahdkk6znhqxzi") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.63 (c (n "cc") (v "1.0.63") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "11xc6jcmj6lb51v5ph458fs64lsy4hhiissnx90dpix2nm06375d") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.64 (c (n "cc") (v "1.0.64") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1rs9p4mrm3p8bk7iqy9190rn2ms8w7w0ml6bgji8sq5azr67pk4j") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.65 (c (n "cc") (v "1.0.65") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "05famj6kp81wvq3xcw7isf564idkjlkdi37lpaa26mgpr1c26xcm") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.66 (c (n "cc") (v "1.0.66") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0j7d7h4n81z5f22l3v8ggjvvw8m64636nlaqax4x1y44da1rc12c") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.67 (c (n "cc") (v "1.0.67") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1z9p27ys80shv09zhknmlal8jjra78agdwg97i6jjd6lg83rpip3") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.68 (c (n "cc") (v "1.0.68") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "11ypa8b7iwhjf5fg5j3hvbn2116h9g8v67vyd9s7ljgzq52c4wja") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.69 (c (n "cc") (v "1.0.69") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1cly36mskqby2yqiygls7myd0qjcfrvxcyw2cf4qdqbc5kvc4377") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.70 (c (n "cc") (v "1.0.70") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1h0dm9pr7l2g6228pzl5fawd43s3zjkfzxvhvczgm154nvj6qsnj") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.71 (c (n "cc") (v "1.0.71") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1pgflzb5dc9pli1lfwfv5jksmz57j15iqqxqpdbnjq4lclfnihkr") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.72 (c (n "cc") (v "1.0.72") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1vl50h2qh0nh0iddzj6gd1pnxnxpvwmbfxc30578c1pajmxi7a92") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.73 (c (n "cc") (v "1.0.73") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "04ccylrjq94jssh8f7d7hxv64gs9f1m1jrsxb7wqgfxk4xljmzrg") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.74 (c (n "cc") (v "1.0.74") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0x0m14cizayy1ydiyvw76gl0wij8120w8ppb7zm55b1sj2x5s7sq") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.75 (c (n "cc") (v "1.0.75") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0hj86zmcl2qfb268w87qp1b1hy0mc7rk46r3zdnfzflpgw839jj1") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.76 (c (n "cc") (v "1.0.76") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "13wjzcry62yqmli0w0v0zlni2qsk8c9igrak4cphkqkg5vd898kn") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.77 (c (n "cc") (v "1.0.77") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1r7bv6sxwmpw9xaibz4fdfs2w8xfdabki1yi35dr0zcg6c2kbxz9") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.78 (c (n "cc") (v "1.0.78") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0gcch8g41jsjs4zk8fy7k4jhc33sfqdab4nxsrcsds2w6gi080d2") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.79 (c (n "cc") (v "1.0.79") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "07x93b8zbf3xc2dggdd460xlk1wg8lxm6yflwddxj8b15030klsh") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.80 (c (n "cc") (v "1.0.80") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1fj1c5q6hg80m6v2c3r4x91j64svvm2i4gbm6ir5hmfsv5n25wai") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.81 (c (n "cc") (v "1.0.81") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1w1rf8i34p8mxsi74mzyrnla22mgkywjvw7p74s8rwlv25i2asvc") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.82 (c (n "cc") (v "1.0.82") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "00cgp2xjw0jbryp2xqajgb9rh9s23nk6nwmnm07jli61xm2ycprh") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.83 (c (n "cc") (v "1.0.83") (d (list (d (n "jobserver") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1l643zidlb5iy1dskc5ggqs4wqa29a02f44piczqc8zcnsq4y5zi") (f (quote (("parallel" "jobserver"))))))

(define-public crate-cc-1.0.84 (c (n "cc") (v "1.0.84") (d (list (d (n "libc") (r "^0.2.62") (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0mn8wp0zr6f6z6cf4ppvf13y76i0ds5x0lqnla2092ddmy87r3hg") (f (quote (("parallel")))) (y #t) (r "1.53")))

(define-public crate-cc-1.0.85 (c (n "cc") (v "1.0.85") (d (list (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1lzpnzfsdbal1201zw2jdz52s1qxqpqawlrpqa5v8qh9cxqqd4cv") (f (quote (("parallel" "libc")))) (y #t) (r "1.53")))

(define-public crate-cc-1.0.86 (c (n "cc") (v "1.0.86") (d (list (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0c076l2jf6alfxz8h8vpsba2xb3i3amgd3fld06vw9a3gs4s37vz") (f (quote (("parallel" "libc")))) (r "1.53")))

(define-public crate-cc-1.0.87 (c (n "cc") (v "1.0.87") (d (list (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "092f1zbssszz6zs6id3c0f3ijw8fjz2n2gs3mwaxvjzws12vi1ij") (f (quote (("parallel" "libc")))) (r "1.53")))

(define-public crate-cc-1.0.88 (c (n "cc") (v "1.0.88") (d (list (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1p0w5ni5zh9464ai48i6d2lfki5af5cwwc8nwjk5b4fijg043wq2") (f (quote (("parallel" "libc")))) (r "1.53")))

(define-public crate-cc-1.0.89 (c (n "cc") (v "1.0.89") (d (list (d (n "jobserver") (r "^0.1.20") (o #t) (k 0)) (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "08ypcdgh6253hgh3z1ir7jfd9k4ff3v64546nbak0bq1m9x8zfm0") (f (quote (("parallel" "libc" "jobserver")))) (r "1.53")))

(define-public crate-cc-1.0.90 (c (n "cc") (v "1.0.90") (d (list (d (n "jobserver") (r "^0.1.20") (o #t) (k 0)) (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1xg1bqnq50dpf6g1hl90caxgz4afnf74pxa426gh7wxch9561mlc") (f (quote (("parallel" "libc" "jobserver")))) (r "1.53")))

(define-public crate-cc-1.0.91 (c (n "cc") (v "1.0.91") (d (list (d (n "jobserver") (r "^0.1.20") (o #t) (k 0)) (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0lyi8ni7l88ml8xbf4xpimvb710h3illrz2sb8wr6r6cm20p7n8z") (f (quote (("parallel" "libc" "jobserver")))) (r "1.53")))

(define-public crate-cc-1.0.92 (c (n "cc") (v "1.0.92") (d (list (d (n "jobserver") (r "^0.1.20") (o #t) (k 0)) (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0hcbiasyps4m6sc2nncni7fb6qcfl83bbyd619dyjxcl8kiv4y16") (f (quote (("parallel" "libc" "jobserver")))) (r "1.53")))

(define-public crate-cc-1.0.93 (c (n "cc") (v "1.0.93") (d (list (d (n "jobserver") (r "^0.1.20") (o #t) (k 0)) (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "147wwmsrbfrg35vp66dnpjzqz0dz4q7i1qdjvfcvnj62y9b10zks") (f (quote (("parallel" "libc" "jobserver")))) (r "1.53")))

(define-public crate-cc-1.0.94 (c (n "cc") (v "1.0.94") (d (list (d (n "jobserver") (r "^0.1.20") (o #t) (k 0)) (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1rrm9yw419rwhnkdx9s31nlidqp2s5arf26ckwai3h4x48jf7xhp") (f (quote (("parallel" "libc" "jobserver")))) (r "1.53")))

(define-public crate-cc-1.0.95 (c (n "cc") (v "1.0.95") (d (list (d (n "jobserver") (r "^0.1.30") (o #t) (k 0)) (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.19") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0fy80zm1ay4qviv5y5v7925czf4gz2wkp1r9wv1rgbsrq5dp4ank") (f (quote (("parallel" "libc" "jobserver" "once_cell")))) (r "1.63")))

(define-public crate-cc-1.0.96 (c (n "cc") (v "1.0.96") (d (list (d (n "jobserver") (r "^0.1.30") (o #t) (k 0)) (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.19") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ggqp5lgmxr02kbgxl9wq6x6kymzlsf9yqkj14k59fjk3lk2jnh6") (f (quote (("parallel" "libc" "jobserver" "once_cell")))) (r "1.63")))

(define-public crate-cc-1.0.97 (c (n "cc") (v "1.0.97") (d (list (d (n "jobserver") (r "^0.1.30") (o #t) (k 0)) (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.19") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1d6rv3nk5q6lrr3mf7lifqpjr44slylsz3pw6pmn2k2cv1bm76h9") (f (quote (("parallel" "libc" "jobserver" "once_cell")))) (r "1.63")))

(define-public crate-cc-1.0.98 (c (n "cc") (v "1.0.98") (d (list (d (n "jobserver") (r "^0.1.30") (o #t) (k 0)) (d (n "libc") (r "^0.2.62") (o #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.19") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0gzhij74hblfkzwwyysdc8crfd6fr0m226vzmijmwwhdakkp1hj1") (f (quote (("parallel" "libc" "jobserver" "once_cell")))) (r "1.63")))

