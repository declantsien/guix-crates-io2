(define-module (crates-io #{2}# r4) #:use-module (crates-io))

(define-public crate-r4-1.0.0 (c (n "r4") (v "1.0.0") (h "14sl0ypnw53scmasd873fyfy5rjf5mr3a931lrlvgzi3fi6q3lb7")))

(define-public crate-r4-1.0.1 (c (n "r4") (v "1.0.1") (h "0wwq8la4602xqxslzvdvbp9zwd4a5whvx5w8qjxr02nmbviaxn2h")))

