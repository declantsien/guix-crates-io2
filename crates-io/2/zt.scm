(define-module (crates-io #{2}# zt) #:use-module (crates-io))

(define-public crate-zt-0.1.0 (c (n "zt") (v "0.1.0") (h "1s69bw43dcp4p2hp3xsigvdfr3s30sx74avbr37xg8fvbgs0i8kb") (y #t)))

(define-public crate-zt-0.1.1 (c (n "zt") (v "0.1.1") (h "04rhz3g100qqw1k50qpxz8shvlwygz2l77sxzqr5v1pip4gljd41")))

