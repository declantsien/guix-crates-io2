(define-module (crates-io #{2}# wy) #:use-module (crates-io))

(define-public crate-wy-1.0.0 (c (n "wy") (v "1.0.0") (h "0j09zxsdfqnjyd6802rk5w0qp3nbw79f3va7avwipmr1zl8y1mqs")))

(define-public crate-wy-1.1.0 (c (n "wy") (v "1.1.0") (h "0kpmx5zjcyshh0sd6r6wrsddbbw56srlcfh3f1qhky78wsxpyglj")))

(define-public crate-wy-1.1.1 (c (n "wy") (v "1.1.1") (h "17rqsfzf25avq8fchg1iqkx933zywh0yzr5p6bjh63dm89ck2rmp")))

(define-public crate-wy-1.1.2 (c (n "wy") (v "1.1.2") (h "0mvk5kaxpsnhw7l3bqywmyd1wzf9jay1433viqk0pzsxhindxppk")))

