(define-module (crates-io #{2}# pm) #:use-module (crates-io))

(define-public crate-pm-0.0.1 (c (n "pm") (v "0.0.1") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "ring") (r "^0.9") (d #t) (k 0)) (d (n "rpassword") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y4km36sxjzyyqrrirq8rg7by3ab57j6dmm6fymc09afgdappdc8") (y #t)))

