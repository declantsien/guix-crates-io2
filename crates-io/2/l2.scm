(define-module (crates-io #{2}# l2) #:use-module (crates-io))

(define-public crate-l2-1.0.0 (c (n "l2") (v "1.0.0") (d (list (d (n "blas") (r "^0.20.0") (d #t) (k 0)) (d (n "blas-src") (r "^0.6") (f (quote ("accelerate"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1s30i036gyw06gd7s43569qmb7ji51qwyklpx5w7yi75fy166xk0")))

(define-public crate-l2-1.0.1 (c (n "l2") (v "1.0.1") (d (list (d (n "blas") (r "^0.20.0") (d #t) (k 0)) (d (n "blas-src") (r "^0.6") (f (quote ("accelerate"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1cvnlqbj7slp7xcjf6wcsc303dgcdy5bbwmi1smy4yhfm405scab")))

(define-public crate-l2-1.0.2 (c (n "l2") (v "1.0.2") (d (list (d (n "blas") (r "^0.20.0") (d #t) (k 0)) (d (n "blas-src") (r "^0.6") (f (quote ("accelerate"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "0jl0nb5h1qcvdnr9bxr1w0hw38g89bynbhxxsslkzyhb6lzccslq")))

(define-public crate-l2-1.0.3 (c (n "l2") (v "1.0.3") (d (list (d (n "blas") (r "^0.20.0") (d #t) (k 0)) (d (n "blas-src") (r "^0.6") (f (quote ("accelerate"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1hyg71iwsby4cgyk5qyvrxdz8j1k0yildnqbms50ryh10z05fhv3")))

