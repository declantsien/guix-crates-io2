(define-module (crates-io #{2}# ao) #:use-module (crates-io))

(define-public crate-ao-0.2.1 (c (n "ao") (v "0.2.1") (h "0jysmqcv3m2crnzj3zr223fappc4y3nh43ysirv583iyfc3yw2rx")))

(define-public crate-ao-0.3.0 (c (n "ao") (v "0.3.0") (h "1x0v459c56fhnk8h6yzjg2ig078cydlaxj2jhpz39smsn3kx3i76")))

(define-public crate-ao-0.4.0 (c (n "ao") (v "0.4.0") (h "0vsdih0i0gv5gmffliyi78gcqra36rgx8z4cz6r90lb5v811al9j")))

(define-public crate-ao-0.4.1 (c (n "ao") (v "0.4.1") (h "0lyfd67lhkyhpqvc3zpycl9b5gdx0zygk1wdvif73746jrmafd8r")))

(define-public crate-ao-0.5.0 (c (n "ao") (v "0.5.0") (h "0gvsgfbx3yz6ibzrjz55nvzbxd3vb7bdignxpilzvwpnrynzvlkj")))

(define-public crate-ao-0.5.1 (c (n "ao") (v "0.5.1") (h "0qfc1251zizyjrg3v3mm1f3faxranqahnwvfavahw8hrzq9rhss6")))

(define-public crate-ao-0.6.0 (c (n "ao") (v "0.6.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0rl7k9p75w1582cps9iz95hy0p4mrxb13mv58bgwhsp0n4vv1lkf")))

(define-public crate-ao-0.6.1 (c (n "ao") (v "0.6.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1gqq5mp92xk5in7k1cgwqwx4b5f8mwadyma2ybcbfyzwc5gdwa0j")))

