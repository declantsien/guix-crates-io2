(define-module (crates-io #{2}# js) #:use-module (crates-io))

(define-public crate-js-0.0.2 (c (n "js") (v "0.0.2") (h "0vxg6yvhaayzddbmm2zmc6qffjk0i79hbxj1y97fibcchyl8svq2")))

(define-public crate-js-0.1.0 (c (n "js") (v "0.1.0") (h "0rnvvc17xwshp3sp7xbqc8yfps036kyngdqng8gxddcysj5rk3s3")))

(define-public crate-js-0.1.1 (c (n "js") (v "0.1.1") (h "1ksl8kl03am99p09dm6dgz8wfvsakvdws23k3mzhazhm79ma8dhs")))

(define-public crate-js-0.1.2 (c (n "js") (v "0.1.2") (h "08044nmz2f7vf4cp2xlzl902l1r9jsvlcgvpqqrph82d05p4ljgs")))

(define-public crate-js-0.2.0 (c (n "js") (v "0.2.0") (h "1ijjr6wngw0aksnyasid8b52ca3c5bzcqhbnzib687als51692s1")))

(define-public crate-js-0.2.1 (c (n "js") (v "0.2.1") (h "0lzmwjlg59iml42lqa3ym8mrqvhbd96ijiv0rg16ds2k7ccmfwl6")))

(define-public crate-js-0.2.2 (c (n "js") (v "0.2.2") (h "0f6b6bn35ns4rdkfh2b3k2ad0n9gs75a61a26qy5di45gm9vp2sq")))

(define-public crate-js-0.2.3 (c (n "js") (v "0.2.3") (h "13x62ypscry9m1crayv33hrsg59wbfzvx22fg7qpbfn7r7mk0gkx")))

(define-public crate-js-0.2.4 (c (n "js") (v "0.2.4") (h "0d4yl0af4fg9v3j7cyddis8ra57dd1h1rpy3w2m2sxiqgxpf8i5n")))

(define-public crate-js-0.2.5 (c (n "js") (v "0.2.5") (h "1n9z28zmvig3phaywmbhna393zf8dswnf8rlvxmkwnf0vfwaryrh")))

(define-public crate-js-0.2.6 (c (n "js") (v "0.2.6") (h "128ig7lfvk4nvsh8xanmm4wrzspxw0lyd29dg1qysyqx4hv5402z")))

(define-public crate-js-0.2.7 (c (n "js") (v "0.2.7") (h "0zr6aw88krkw1g0pjd8anlza4wk17mwiq7b1szndjh7kfy1j5l1p")))

(define-public crate-js-0.2.8 (c (n "js") (v "0.2.8") (d (list (d (n "cstring") (r ">=0.0.0, <0.1.0") (d #t) (k 0)))) (h "1238nw980k632crnwb3k06cy3515nfgsh5l8m0rcqzxijfhg2wcf")))

(define-public crate-js-0.2.9 (c (n "js") (v "0.2.9") (d (list (d (n "cstring") (r ">=0.0.0, <0.1.0") (d #t) (k 0)))) (h "12kd1wm72ql0k687p78kqy35ccrwl77kp9j50jgcjr85kxfjkrvx")))

(define-public crate-js-0.2.10 (c (n "js") (v "0.2.10") (d (list (d (n "cstring") (r ">=0.0.0, <0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (f (quote ("spin_no_std"))) (k 0)))) (h "02y39w3hykysd217678c3fsdbm9j2dsdp60090fh7qy0qjsy0xq8")))

(define-public crate-js-0.2.11 (c (n "js") (v "0.2.11") (d (list (d (n "callback") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "cstring") (r ">=0.0.0, <0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (f (quote ("spin_no_std"))) (k 0)))) (h "0l6zlncgfqcp1xwf888ym72hi2sq4qwdi3cfy4jqi94xmazrjiaf")))

(define-public crate-js-0.2.12 (c (n "js") (v "0.2.12") (d (list (d (n "callback") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "cstring") (r ">=0.0.0, <0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (f (quote ("spin_no_std"))) (k 0)))) (h "02pmvf6266rpdwz554zsbcfsgh1b8xjzg89frcsa5bj0sykdm6mk")))

(define-public crate-js-0.2.13 (c (n "js") (v "0.2.13") (d (list (d (n "callback") (r "^0.5") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)))) (h "1wzaibgjz1158vr393m8z8dlnp9agmxr46wx17dd9s8zvv2wvgkv")))

(define-public crate-js-0.3.0 (c (n "js") (v "0.3.0") (d (list (d (n "callback") (r "^0.5") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)))) (h "0105c1h95sx6nlkdxcl505lpwf2210kah2d9gbi1kgpbl1g5f737")))

(define-public crate-js-0.3.1 (c (n "js") (v "0.3.1") (d (list (d (n "callback") (r "^0.5") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)))) (h "0pffg4glfprqc97y0k7vc8x905dmazrxmm78s3q91j7rb3vb5007")))

(define-public crate-js-0.3.2 (c (n "js") (v "0.3.2") (d (list (d (n "callback") (r "^0.5") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)))) (h "01qaaj0cfdwkyrapnd7vl7prz0vig8wzpr5hsk8f6swb2qd2b4vh") (r "1.63")))

(define-public crate-js-0.4.0 (c (n "js") (v "0.4.0") (d (list (d (n "callback") (r "^0.5") (d #t) (k 0)))) (h "1zjw2f2sbydwfr2a34ag8jn1vbc42f7f20ixdnmwg8gj64zsbkaa") (r "1.63")))

(define-public crate-js-0.4.1 (c (n "js") (v "0.4.1") (d (list (d (n "callback") (r "^0.5") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "0ydgwia8hmbdchxcrpy0gj8jir0g1zr2pyz095idiawg8x0ics3f") (r "1.63")))

(define-public crate-js-0.4.2 (c (n "js") (v "0.4.2") (d (list (d (n "callback") (r "^0.5") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "0g18mi9485069xx0vq9fjd1zc5yd8a388pdghzra92adain9gaaa") (r "1.63")))

(define-public crate-js-0.4.3 (c (n "js") (v "0.4.3") (d (list (d (n "callback") (r "^0.5") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "1ya9sa5zp0fj064ib04flnik07qsf14w9l8dcwpp5615avkl4221") (r "1.63")))

(define-public crate-js-0.5.0 (c (n "js") (v "0.5.0") (d (list (d (n "externref_polyfill") (r "^0.1.0") (d #t) (k 0)) (d (n "raw-parts") (r "^1.1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "100wvxh7hcnzdnw8bkz3j4gyj8jrb11632m1ixqrpk5aqll13wfj") (r "1.63")))

(define-public crate-js-0.5.1 (c (n "js") (v "0.5.1") (d (list (d (n "externref_polyfill") (r "^0.1.0") (d #t) (k 0)) (d (n "raw-parts") (r "^1.1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "0bqs8hndszv758vx9pq2s3dx56jk01bcy2w8vrl1ykh87ryw93pj") (r "1.63")))

(define-public crate-js-0.5.2 (c (n "js") (v "0.5.2") (d (list (d (n "externref_polyfill") (r "^0.1.0") (d #t) (k 0)) (d (n "raw-parts") (r "^1.1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "1qrlzzykgfs0p2id1fmjmfigsrj1akhhdgy9xn1kqyhqk1fvmb1b") (r "1.63")))

(define-public crate-js-0.5.3 (c (n "js") (v "0.5.3") (d (list (d (n "externref_polyfill") (r "^0.1.0") (d #t) (k 0)) (d (n "raw-parts") (r "^1.1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "1li132mcvjqjbsp99ish5x30xl0imdn9q9f9n00g8jcv00s2zhw7") (r "1.63")))

(define-public crate-js-0.5.4 (c (n "js") (v "0.5.4") (d (list (d (n "externref_polyfill") (r "^0.1.0") (d #t) (k 0)) (d (n "raw-parts") (r "^1.1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "19lf9h1m1b6sh4vl9pa7gdrnm2j6i1dwbrac98zi7bb1ip2kgzpc") (r "1.63")))

(define-public crate-js-0.5.6 (c (n "js") (v "0.5.6") (d (list (d (n "externref_polyfill") (r "^0.1.0") (d #t) (k 0)) (d (n "raw-parts") (r "^1.1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "1dh3r3mqpfxqjz6c0zg5v8fgy5mbcpkqy5rn80vrw831qqcd16zs") (r "1.63")))

(define-public crate-js-0.5.7 (c (n "js") (v "0.5.7") (d (list (d (n "externref_polyfill") (r "^0.1.0") (d #t) (k 0)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "1a5ciw2b454y4rzah1gkbrgipy7vzc5ab494fcvvrj1flg4xhsvz") (f (quote (("std" "no-std-compat/std") ("default" "std")))) (r "1.63")))

