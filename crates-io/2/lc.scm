(define-module (crates-io #{2}# lc) #:use-module (crates-io))

(define-public crate-lc-0.1.0 (c (n "lc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0sfg94k27sdikhdd26qgvr25kqz4jk9dbz5hbqkij8l3hay5wj26")))

(define-public crate-lc-0.2.0 (c (n "lc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0giyrrzvg1rdbl6g14kz3ijix6kqxgkdzzf8wk7s8hg1k8av2a26")))

(define-public crate-lc-0.3.0 (c (n "lc") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.16") (f (quote ("full"))) (d #t) (k 0)))) (h "186dz5vwqn767vl4h2sdqbx60bxlyh3rqa10qlqmmp2wyb587hd4")))

