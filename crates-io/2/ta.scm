(define-module (crates-io #{2}# ta) #:use-module (crates-io))

(define-public crate-ta-0.0.1-alpha (c (n "ta") (v "0.0.1-alpha") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "1pz66i5vsrxjgwwv3nzkr72ag3lhiwglds37bxn6k74vna5h6his")))

(define-public crate-ta-0.0.1-beta (c (n "ta") (v "0.0.1-beta") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "csv") (r "^0.15.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "047f9n3k99xslygy920r4zlkal126ny4g8ribxw2vq5cvp9c4jmq")))

(define-public crate-ta-0.1.0 (c (n "ta") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "csv") (r "^0.15.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "04s7g6hzllzpybsq1dhais4k79izjwd3w5748cfi9b3hgdhcv5mr")))

(define-public crate-ta-0.1.1 (c (n "ta") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "csv") (r "^0.15.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "08dvqas761nbjrkz59k1nd4rbd74jg481jirdq0s4cb5mlvnzzl5")))

(define-public crate-ta-0.1.2 (c (n "ta") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "csv") (r "^0.15.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "0w9qvzqwcavqf3m4q3blnbr6a49mm8mz7ff4p6xgjq0magd43rpv")))

(define-public crate-ta-0.1.3 (c (n "ta") (v "0.1.3") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "csv") (r "^0.15.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "09qn58514j4m2xickgqji9rggsav1zk6w62c10a95rnbqxnmq178")))

(define-public crate-ta-0.1.4 (c (n "ta") (v "0.1.4") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "csv") (r "^0.15.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1k9y5fq9k0hpn3lllzsabxdq9ra0pk664afw9i8gphclhcf5z9xi")))

(define-public crate-ta-0.1.5 (c (n "ta") (v "0.1.5") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "csv") (r "^0.15.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1ynxyi3v8wxm7akkj1qick9nzdvrk0s4q84iql5d4ywkp9qlkhks")))

(define-public crate-ta-0.2.0 (c (n "ta") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "csv") (r "^0.15.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "038799wg5rlx57lkdfyzwqkql4bnjqziig9rrmqccr5rg92xwz61")))

(define-public crate-ta-0.3.0 (c (n "ta") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "csv") (r "^0.15.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14km05bfx32ha3v7fq09nxyyx94xr55s4ynvb09jsd181h37ni99")))

(define-public crate-ta-0.3.1 (c (n "ta") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "csv") (r "^1.1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1q4612ra9yylianqhxqacmb2f60siwdifv54a4i71kmivxz12yp4")))

(define-public crate-ta-0.4.0 (c (n "ta") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "csv") (r "^1.1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bds3kshihy2r1qwk5hjzj9hyv3jyb8vi54g628hdzlwqcjgs2l7")))

(define-public crate-ta-0.5.0 (c (n "ta") (v "0.5.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "csv") (r "^1.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09hjakf948g0s1bk0337rsfnnm7gpldqj6cyvpadi9x0fba0k530")))

