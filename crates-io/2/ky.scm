(define-module (crates-io #{2}# ky) #:use-module (crates-io))

(define-public crate-ky-0.0.1 (c (n "ky") (v "0.0.1") (d (list (d (n "rocksdb") (r "^0.16.0") (f (quote ("lz4"))) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1nd0rf0ny3a1yy5hh1jxl930gihbdcxfmgs0jagrn1f7vhcihplk")))

