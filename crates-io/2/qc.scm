(define-module (crates-io #{2}# qc) #:use-module (crates-io))

(define-public crate-qc-0.1.0 (c (n "qc") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.4") (d #t) (k 0)))) (h "12byqv5fral1cj0saxd4ppp2nqiih3cdlaknqs1q5f87dial47ia")))

(define-public crate-qc-1.0.0 (c (n "qc") (v "1.0.0") (d (list (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "1lmvwbla2i53fcacyj59d3hinzw7fyhgc5kd4iyz9lzggm1pyv18")))

