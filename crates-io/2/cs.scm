(define-module (crates-io #{2}# cs) #:use-module (crates-io))

(define-public crate-cs-0.0.1 (c (n "cs") (v "0.0.1") (d (list (d (n "suffix") (r "^1.2.0") (d #t) (k 0)))) (h "16s4hvvliq42b3ljvz1l5f1ixi6azflasykks73jhs5g0yjgbiyw")))

(define-public crate-cs-0.0.2 (c (n "cs") (v "0.0.2") (d (list (d (n "suffix") (r "^1.2.0") (d #t) (k 0)))) (h "0wb25fam3c0vs6s2z321wdbabjhcmbhj9k06hs16b02h85ax316a")))

(define-public crate-cs-0.0.3 (c (n "cs") (v "0.0.3") (d (list (d (n "suffix") (r "^1.2.0") (d #t) (k 0)))) (h "1zdyylbs489xwxp632z2alb4prhzxj3w4xh2jw80m3qxvwwdclwx")))

(define-public crate-cs-0.0.4 (c (n "cs") (v "0.0.4") (d (list (d (n "suffix") (r "^1.2.0") (d #t) (k 0)))) (h "0wmfwi7pb5aaw5bk399jhvva7870rdfc64j6vx2x2fpw2p6yy23a")))

