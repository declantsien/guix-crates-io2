(define-module (crates-io #{2}# tl) #:use-module (crates-io))

(define-public crate-tl-0.1.0 (c (n "tl") (v "0.1.0") (h "0vvylvbk147wp337n312ysiivxlifqmq6bkhdpimda75bppww25h")))

(define-public crate-tl-0.1.1 (c (n "tl") (v "0.1.1") (h "0ns47l7gzz108zkmzgqgfk548qg6a2nq0s3hbd4884lnfr7mwd5h")))

(define-public crate-tl-0.2.0 (c (n "tl") (v "0.2.0") (h "00szxj7r65qlkvdjrghagvdlsn9y47v58nqa3x8s5vrmjszny5fp")))

(define-public crate-tl-0.2.1 (c (n "tl") (v "0.2.1") (h "1w0zzfv8ls07q5xpzdxk174z9mhwvmjzs99ma4r8kl883239vrn2")))

(define-public crate-tl-0.3.0 (c (n "tl") (v "0.3.0") (h "0fld0nvdll271vr076lnvggckhvgk8l8j3x99jgdajza6aj5khba")))

(define-public crate-tl-0.4.0 (c (n "tl") (v "0.4.0") (h "1rlp2q6flnqyd3hka3yd9h9wpd5g9iycd18k7sg8vkq5pqihl0zp")))

(define-public crate-tl-0.4.1 (c (n "tl") (v "0.4.1") (h "0wdqy92v2qxlkjap8zniw9s2k68mwc0m13y8zvgmb96yqa3qxg9p")))

(define-public crate-tl-0.4.2 (c (n "tl") (v "0.4.2") (h "0acl2ywbr8imvf6fjx02afn0q2flf7z8hvh6l12vhr83p601wg7y")))

(define-public crate-tl-0.4.3 (c (n "tl") (v "0.4.3") (h "0l6c567kqc2w6r6ibbkw48ml4855rncskvkr4gbgkqf69vpf9f8m") (f (quote (("simd"))))))

(define-public crate-tl-0.4.4 (c (n "tl") (v "0.4.4") (h "1pkcsyhsyy53sq2yis8xf6srnmd2fhs6s031n5bwnvwj43cpz5ff") (f (quote (("simd"))))))

(define-public crate-tl-0.5.0 (c (n "tl") (v "0.5.0") (h "1yjzaphyb5kji0br4nyf0kl8yz77s79kw2b6gml04dp7jl0bsgjm") (f (quote (("simd"))))))

(define-public crate-tl-0.6.0 (c (n "tl") (v "0.6.0") (h "17rarqhiksp9bp6l5qh20pgavbsqniizlvjvp9rmjh7xfvqn5xvq") (f (quote (("simd"))))))

(define-public crate-tl-0.6.1 (c (n "tl") (v "0.6.1") (h "15m8n9i5ybky0zmf590ryb788y81rc0bnsjy069pdqw7hhr4m8cf") (f (quote (("simd"))))))

(define-public crate-tl-0.6.2 (c (n "tl") (v "0.6.2") (h "1wr2zzr3nw5kjx0cxas1wv5i3ifqvwg7qgbkal8hni9jhg21qzv5") (f (quote (("simd"))))))

(define-public crate-tl-0.6.3 (c (n "tl") (v "0.6.3") (h "1kni9mf9m02pm6csrm1sffgiby82c0qmisywcjw4f77rb6ki4h7l") (f (quote (("simd"))))))

(define-public crate-tl-0.7.0 (c (n "tl") (v "0.7.0") (h "143r1ddjrczv5fwdh50c4h1qbfs2n77lwmaiqnq8g5kyhl9jid0n") (f (quote (("simd"))))))

(define-public crate-tl-0.7.1 (c (n "tl") (v "0.7.1") (h "0pyd22g6702phm5g142jm45hwsy3zkj0qsq380hkmb0gcksz5147") (f (quote (("simd"))))))

(define-public crate-tl-0.7.2 (c (n "tl") (v "0.7.2") (h "0xf04lwipy945g44bydhhhvl9hq5xrs6qvyibi4v8sq6yvpmivsq") (f (quote (("simd"))))))

(define-public crate-tl-0.7.3 (c (n "tl") (v "0.7.3") (h "19smxxnbka119alr9n20lazj78qqw3kjk4zf12ws2jz4kmdq7h3a") (f (quote (("simd"))))))

(define-public crate-tl-0.7.4 (c (n "tl") (v "0.7.4") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "03hbdw0zj03c379ar5xhnq78c535nrjjwf77vcrsrh1c2ywgdrny") (f (quote (("simd") ("__INTERNALS_DO_NOT_USE"))))))

(define-public crate-tl-0.7.5 (c (n "tl") (v "0.7.5") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "107xs01mx1i2vz5mlfvldi28gdhj3wjnhzfdm029xi64a5vinc9v") (f (quote (("simd") ("__INTERNALS_DO_NOT_USE"))))))

(define-public crate-tl-0.7.6 (c (n "tl") (v "0.7.6") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0w3ay52xp9vkhp80x5vhvpqqwgp95n21b6nx4ml52zg2nrfhkfck") (f (quote (("simd") ("__INTERNALS_DO_NOT_USE"))))))

(define-public crate-tl-0.7.7 (c (n "tl") (v "0.7.7") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1czxchb354m0wzb9p46c79bv41zmazac9kh8lf8dyby3qyhr7sfm") (f (quote (("simd") ("__INTERNALS_DO_NOT_USE"))))))

(define-public crate-tl-0.7.8 (c (n "tl") (v "0.7.8") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1xz3ar8d2qf53zm2zk7n5gc2gfd7khiv85z28i5j4qy1b25bsc5i") (f (quote (("simd") ("__INTERNALS_DO_NOT_USE"))))))

