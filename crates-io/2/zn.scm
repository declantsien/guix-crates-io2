(define-module (crates-io #{2}# zn) #:use-module (crates-io))

(define-public crate-zn-0.0.1 (c (n "zn") (v "0.0.1") (h "0plc468j6vwxb2zykk1mhig6cpzk7m29y21zc559jm32cr958pvd")))

(define-public crate-zn-1.0.0 (c (n "zn") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "surf") (r "^2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1jbdj101b3kgq50mgdd4xffl650w1a0c3q0yn8vaa301v1nrzrrq")))

(define-public crate-zn-1.0.1 (c (n "zn") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "surf") (r "^2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0w1s1q5s2qvxa0xzpbc8mgbhm2qpfny4yzjzisba1xslm63di0g8")))

(define-public crate-zn-1.0.2 (c (n "zn") (v "1.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "surf") (r "^2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1w2wijrp8k2mdb6h4wmd66hmr7n06nvgr61ncrd94l0gb87hahfw")))

