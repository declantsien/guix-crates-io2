(define-module (crates-io #{2}# rb) #:use-module (crates-io))

(define-public crate-rb-0.1.0 (c (n "rb") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1pdm0fqb8wb23dw79ibf174qnh42jdmh0kzvsmkzgc5ag36varka")))

(define-public crate-rb-0.1.1 (c (n "rb") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "19n4p4afrffp9vh08x2jq8m7qn3y13wnamr7v6y15abh6prgahz3")))

(define-public crate-rb-0.1.2 (c (n "rb") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "0gci2g8q612wam1sjjhplldq5fa0fld2ibq7k448b84g6scidvfn")))

(define-public crate-rb-0.1.3 (c (n "rb") (v "0.1.3") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "0584jdyqpb71m28c9xf8qfd2gkqp1mvz3yg0jpa53ihjqzmxnzdb")))

(define-public crate-rb-0.2.0 (c (n "rb") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1049w821bq1sksvljwqvfggjpy7laki9rcbqpyn5jyanimk48hmz")))

(define-public crate-rb-0.3.0 (c (n "rb") (v "0.3.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "0kg6v32hy801g4h8q6k267c4sahmpi26hn2nfjrbwilavx3j74cv")))

(define-public crate-rb-0.3.1 (c (n "rb") (v "0.3.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "0p4rigy37cnr8ld5vv2rcmy2na05kzi8k38p070dz709xlyyr01h")))

(define-public crate-rb-0.3.2 (c (n "rb") (v "0.3.2") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "059gaddzlaqsb1759w7fi0iwcmanh1l833bm89cznaxxarblqzx2")))

(define-public crate-rb-0.4.0 (c (n "rb") (v "0.4.0") (d (list (d (n "rand_core") (r "^0") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0") (d #t) (k 2)))) (h "1rmdwz0qiwwc00z8vh72mlyg76g50qxp9c3rfdxknghhwqf87vbx")))

(define-public crate-rb-0.4.1 (c (n "rb") (v "0.4.1") (d (list (d (n "rand_core") (r "^0") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0") (d #t) (k 2)))) (h "1ajx6fx3pbf5db9j7fsgpyipqa4kw05iszq9lhcawq2va20q7ksn")))

