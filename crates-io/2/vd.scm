(define-module (crates-io #{2}# vd) #:use-module (crates-io))

(define-public crate-vd-0.1.0 (c (n "vd") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.5") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 1)))) (h "1al8bjb6b0hhjiyl5zs4dvk63mpaq6f1hqc8gxpymwrcck9j398h")))

