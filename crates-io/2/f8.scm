(define-module (crates-io #{2}# f8) #:use-module (crates-io))

(define-public crate-f8-0.1.0 (c (n "f8") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qj4sdyv720znkfilpwxd4d8x9vzfd5xhwbzk8kl5x3qmhmij7n9") (f (quote (("default" "serde"))))))

(define-public crate-f8-0.1.1 (c (n "f8") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0i2qwvs4zpj894wannd0cm5yw5r38n2yjyyv3mry7dqw9zfw1p6n") (f (quote (("default" "serde"))))))

(define-public crate-f8-0.1.2 (c (n "f8") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pzdi3g9dsyss466br7mcbav2vxq0pyzjgmrwdgkvxi1c032jkk8") (f (quote (("default" "serde"))))))

