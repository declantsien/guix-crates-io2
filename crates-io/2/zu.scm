(define-module (crates-io #{2}# zu) #:use-module (crates-io))

(define-public crate-zu-0.1.0 (c (n "zu") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (f (quote ("csr"))) (d #t) (k 0)))) (h "1w0j1cv54r14w61kj47923rd9laig1yvy4qd2qw20r86l4v60gis")))

(define-public crate-zu-0.2.1 (c (n "zu") (v "0.2.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rsass") (r "^0.27.0") (d #t) (k 1)) (d (n "stylist") (r "^0.12.0") (f (quote ("parser" "yew_integration"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)))) (h "06sqw5dqfawr9492hgvhqzlbzpj5fbf53y1srdx0yawij6mp2x9d")))

(define-public crate-zu-0.2.2 (c (n "zu") (v "0.2.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rsass") (r "^0.27.0") (d #t) (k 1)) (d (n "stylist") (r "^0.12.0") (f (quote ("parser" "yew_integration"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)))) (h "1fl3kmvfdk8avxmy07d01j43kq10ypdfrbfsb31bq0v28ca99hkq")))

(define-public crate-zu-0.2.3 (c (n "zu") (v "0.2.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rsass") (r "^0.27.0") (d #t) (k 1)) (d (n "stylist") (r "^0.12.0") (f (quote ("parser" "random" "yew"))) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)))) (h "0r6yb4sx0klhdbxjhbima2gv4kqacl2akpnz8xhyzm8gz5kj1fsz")))

(define-public crate-zu-0.2.4 (c (n "zu") (v "0.2.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rsass") (r "^0.27.0") (d #t) (k 1)) (d (n "stylist") (r "^0.12.0") (f (quote ("parser" "random" "yew"))) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)) (d (n "zu-util") (r "^0.2.1") (d #t) (k 0)))) (h "0djx5nbikwy3fdax67grd9xzbhwlh59mh20hq3flq5i64y80ljga")))

(define-public crate-zu-0.2.5 (c (n "zu") (v "0.2.5") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "rsass") (r "^0.27.0") (d #t) (k 1)) (d (n "stylist") (r "^0.12.0") (f (quote ("parser" "random" "yew"))) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)) (d (n "zu-util") (r "^0.3.1") (d #t) (k 0)) (d (n "zu-util") (r "^0.3.1") (f (quote ("icon"))) (d #t) (k 1)))) (h "1jhhiac1p3dilhhi8yyv7bsnb8vvwvpj4086xgj6sqz2k13iv97g")))

(define-public crate-zu-0.3.1 (c (n "zu") (v "0.3.1") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rsass") (r "^0.27.0") (d #t) (k 1)) (d (n "stylist") (r "^0.12.1") (f (quote ("parser" "random" "yew"))) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)) (d (n "zu-util") (r "^0.3.1") (d #t) (k 0)) (d (n "zu-util") (r "^0.3.1") (f (quote ("icon"))) (d #t) (k 1)))) (h "004nvvgbj652p0r1c98h9p2s5vfz9fjd3rdyyana2k2lc8v2cy1y")))

(define-public crate-zu-0.3.2 (c (n "zu") (v "0.3.2") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rsass") (r "^0.28.0") (d #t) (k 1)) (d (n "stylist") (r "^0.12.1") (f (quote ("parser" "random" "yew"))) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)) (d (n "yew-hooks") (r "^0.2.0") (d #t) (k 0)) (d (n "zu-util") (r "^0.3.2") (d #t) (k 0)) (d (n "zu-util") (r "^0.3.2") (f (quote ("icon"))) (d #t) (k 1)))) (h "0vjri5grlmlgkqw3v7vc0ddp0101kph0l3ch0kji6yqkf0j68qbh")))

