(define-module (crates-io #{2}# ca) #:use-module (crates-io))

(define-public crate-ca-0.1.0 (c (n "ca") (v "0.1.0") (d (list (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "1v8hawp0vnynwwq34w6kfgvw0qbxb8wlqky163hd4hh7szhbqasx")))

(define-public crate-ca-0.1.1 (c (n "ca") (v "0.1.1") (d (list (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0a30wbgjhqnmxc6041wv1x2f0nk90l6r0i3cpjr14pf8sard5206")))

