(define-module (crates-io #{2}# ve) #:use-module (crates-io))

(define-public crate-ve-0.0.1 (c (n "ve") (v "0.0.1") (h "14g6yvggwz24fnblmhbch6x464ig40pb0nd14adf0safrzqx601g")))

(define-public crate-ve-0.0.2 (c (n "ve") (v "0.0.2") (h "02m3z0asppl8dh0yw72bb4ahgqhpc0qcgzknnawgx8bf6y2y3pg6")))

(define-public crate-ve-0.0.3 (c (n "ve") (v "0.0.3") (h "13i8l8cifz78l4zwq31hw3azdd44x7xgxak3lsnpykcpv3dqfhx9")))

