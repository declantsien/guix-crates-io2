(define-module (crates-io #{2}# kt) #:use-module (crates-io))

(define-public crate-kt-0.0.1 (c (n "kt") (v "0.0.1") (d (list (d (n "ring") (r "^0.8") (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "00axknm23ma7lq52v9y46pbqd2s146i4snhdyx73i3gfb83qv9zb") (y #t)))

(define-public crate-kt-0.0.2 (c (n "kt") (v "0.0.2") (d (list (d (n "ring") (r "^0.8") (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "0hjn5vdv85rx2dvy2d6r71455vvk2ws8bpf5wac5lkw356v4r7ql")))

(define-public crate-kt-0.0.3 (c (n "kt") (v "0.0.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ring") (r "^0.9.1") (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "0wcry7c3280mjba4gnkqssw7fl8hbzqbw59c4113p8vbjpjfqg09")))

(define-public crate-kt-0.0.4 (c (n "kt") (v "0.0.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ring") (r "^0.9.4") (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "01c8nfdjf0gdnv6lv5zb9j17y8pwsdfi2wmml31ma57kj4qn6nyg")))

(define-public crate-kt-0.0.5 (c (n "kt") (v "0.0.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ring") (r "^0.9.6") (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "04lmxgcbncba9ilqkw7aglhmkxccz4f20b9mvnm36vsrawprx6c5")))

(define-public crate-kt-0.0.6 (c (n "kt") (v "0.0.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ring") (r "^0.12") (d #t) (k 0)) (d (n "untrusted") (r "^0.5.1") (d #t) (k 0)))) (h "183ayk2vyp8smbf5rf9327ih99k0w4zrs5d8grys9rs61fdm8qm0")))

(define-public crate-kt-0.0.7 (c (n "kt") (v "0.0.7") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ring") (r "^0.14.2") (d #t) (k 0)) (d (n "untrusted") (r "^0.6") (d #t) (k 0)))) (h "0zyima6lc58kxl9lg84dmd87glx3hllwmdl43mdgihmkvwhz0j6h")))

