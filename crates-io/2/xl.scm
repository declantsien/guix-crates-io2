(define-module (crates-io #{2}# xl) #:use-module (crates-io))

(define-public crate-xl-0.1.0 (c (n "xl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "136s7wv14614akrfw10a0mjlk9c9xg1gmssay51gqalkc77n9zp0")))

(define-public crate-xl-0.1.1 (c (n "xl") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "0yw042ga1sgyv205a8aw2ywpc1qy8ivii78q3iqrd82mkpsghq1x")))

(define-public crate-xl-0.1.2 (c (n "xl") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "01czy9r3hb6i1ghi6c9s8jigrgn4i0423915qz1z79b385dfvwqm")))

(define-public crate-xl-0.1.3 (c (n "xl") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "08xaciald5jxspqkklkw351nx0jd8dgnzxzmrqrclgm47qvj97lb")))

(define-public crate-xl-0.1.4 (c (n "xl") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "1hazvzjfxx7bdascsvhsn5hap07fjzpns4mvm218vl78qghhqkld")))

(define-public crate-xl-0.1.5 (c (n "xl") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "1nl0s76b3hfic0nfqb10id9kd8ixdvm6j2j3q5pirswm2m9dfqds")))

(define-public crate-xl-0.1.6 (c (n "xl") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "1ywgmv8bi92n7im0xid9zxp2lzwlxcx0wqf64amvl2hfvjq13mfc")))

(define-public crate-xl-0.1.7 (c (n "xl") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "0v52yhvm2f1ix0br0vkc0q2h8lmhd1p24x71f5p34jpzlwxp28rx")))

