(define-module (crates-io #{2}# bd) #:use-module (crates-io))

(define-public crate-bd-0.1.0 (c (n "bd") (v "0.1.0") (d (list (d (n "nom") (r "^4.0.0") (f (quote ("regexp" "regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1gr6ahcxzq7kddf1jq2awwm1wm4vdnjp5zzxl2dg03m2j4mml96c")))

(define-public crate-bd-0.1.1 (c (n "bd") (v "0.1.1") (d (list (d (n "nom") (r "^4.0.0") (f (quote ("regexp" "regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0h5lx6k04all9wkar03fiaz7qflxr9s2vmgk409049npcsbc6g2h")))

