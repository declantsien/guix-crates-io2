(define-module (crates-io #{2}# rm) #:use-module (crates-io))

(define-public crate-rm-0.1.0 (c (n "rm") (v "0.1.0") (h "0bl7w60xjdvadj3qfj38ydb18j1xha4wyrzmhfx8fnpafpg0x85q") (y #t)))

(define-public crate-rm-0.1.1 (c (n "rm") (v "0.1.1") (h "1qf157q9q1sx6v2isdivw82dw75a478sl23ga510y4ccvhmvxffm")))

(define-public crate-rm-0.1.2 (c (n "rm") (v "0.1.2") (h "0xsrcm13z0g0cdmx3azbya93b7avmrpngc4a2azfbwsb8l6yvb8s")))

(define-public crate-rm-0.2.0 (c (n "rm") (v "0.2.0") (h "1cv0rawvikj4yc777yvrk7sryj6iwlj6fm7fnal4q2vwvyd20s1y")))

(define-public crate-rm-0.3.0 (c (n "rm") (v "0.3.0") (h "123cc81ndq67ixf2hjbv2zb1h9m1f61vidzrz6b5y6f2dkpnfy2c")))

(define-public crate-rm-0.3.1 (c (n "rm") (v "0.3.1") (h "1m5x9b5s97a01ly5ah0faqi73lgbb5rfrp7b6j743b7r4lm6bs7x")))

(define-public crate-rm-0.3.2 (c (n "rm") (v "0.3.2") (h "1zfy9y3s04s0mdswflmfgiyvvcsd17bpidcz7z74iq5yqiaiqnqp")))

