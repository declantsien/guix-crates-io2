(define-module (crates-io #{2}# kx) #:use-module (crates-io))

(define-public crate-kx-0.0.0 (c (n "kx") (v "0.0.0") (h "0hnn6q500v9bn5pzklzvjd7v6r60sarnway0hccp5wc5kdm4hzv0") (y #t)))

(define-public crate-kx-1.0.0 (c (n "kx") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "skim") (r "^0.7.0") (d #t) (k 0)))) (h "14czl1lwy35i5sv0gzymwp09ph0l7rqybrw7bhp6jqjvhpldxb3b")))

(define-public crate-kx-1.1.0 (c (n "kx") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "skim") (r "^0.7.0") (d #t) (k 0)))) (h "17wvd2fl9aysdflhr3s1vmqj89i6krzk200y92bqr4vhs4wwisxj")))

(define-public crate-kx-1.1.1 (c (n "kx") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "skim") (r "^0.7.0") (d #t) (k 0)))) (h "1a61sb3n17w30j2a5bvran6gmcpf45s6xsibjysrmcdnnmvr7fxj")))

(define-public crate-kx-1.2.0 (c (n "kx") (v "1.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "skim") (r "^0.7.0") (d #t) (k 0)))) (h "0gr6r8i39nkz024fkni8isqyx1cbihi3j2khighjw00vx5254adp")))

(define-public crate-kx-1.3.0 (c (n "kx") (v "1.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "skim") (r "^0.10") (d #t) (k 0)))) (h "1dc63cplnj7w21wq7h47zyis2gi4q6hnk1imglqsi5gwjl8gb3in")))

