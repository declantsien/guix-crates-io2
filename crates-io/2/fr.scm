(define-module (crates-io #{2}# fr) #:use-module (crates-io))

(define-public crate-fr-0.1.0 (c (n "fr") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "comment") (r "^0.1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.17.2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0m7021g3rjfsxvy76qmfskay69sfnrk29abhnh6jkz2qhri2im9p")))

(define-public crate-fr-0.1.1 (c (n "fr") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "comment") (r "^0.1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.17.2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0n52wv23g9n9r7wfapn16hgvzxdbxh0i4lfbra9jlwbrk5dlfyp4")))

