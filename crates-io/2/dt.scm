(define-module (crates-io #{2}# dt) #:use-module (crates-io))

(define-public crate-dt-1.0.1 (c (n "dt") (v "1.0.1") (d (list (d (n "lodepng") (r "^2.6.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 2)))) (h "0jvl4jjmgw18qa24bjp6qywfc7ighbxm516bz5h838kl348kww91")))

(define-public crate-dt-1.0.2 (c (n "dt") (v "1.0.2") (d (list (d (n "lodepng") (r "^2.6.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 2)))) (h "0m77d5im2sc7wp3j6asjlpy82qcy9q690zya85pqpx7dzgyj8d37")))

(define-public crate-dt-1.0.3 (c (n "dt") (v "1.0.3") (d (list (d (n "lodepng") (r "^2.6.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 2)))) (h "0g44zgjz2mrs3v66fzxy6s9fxfa15yn2q1y15qp0mwvv8634r8ix")))

(define-public crate-dt-1.0.4 (c (n "dt") (v "1.0.4") (d (list (d (n "lodepng") (r "^2.6.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 2)))) (h "0sw5kxl6cvap7wk0sswjpa0rqky0h7ds67y7qg3i3k5yjl9c7a6p")))

(define-public crate-dt-1.0.5 (c (n "dt") (v "1.0.5") (d (list (d (n "lodepng") (r "^2.6.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 2)))) (h "0qip977mmy89ba3mhrr5j07hwc9sy4fkd81acpfkaj3vykjqkvds")))

(define-public crate-dt-1.0.6 (c (n "dt") (v "1.0.6") (d (list (d (n "lodepng") (r "^2.6.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 2)))) (h "14kifklilzakzncc6jaz0cscy92wspn4makyqdcj8mwz84ja77ya")))

