(define-module (crates-io #{2}# bc) #:use-module (crates-io))

(define-public crate-bc-0.1.0 (c (n "bc") (v "0.1.0") (d (list (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "06mwa750jkix0alnvb980m1hyfnjjnvlwwjjyca6f7if7p3wf0m3")))

(define-public crate-bc-0.1.1 (c (n "bc") (v "0.1.1") (d (list (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "15j4iglk9dbhh1gbwi7h38qzjr5hl8i8kf0xip7wm3w750nqgx0g")))

(define-public crate-bc-0.1.2 (c (n "bc") (v "0.1.2") (d (list (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "1xxw4gc7az97557j0v4z8ah2snrd8731avl6fp2sc3h01dh5d7a3")))

(define-public crate-bc-0.1.3 (c (n "bc") (v "0.1.3") (d (list (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "0m3l8x2s3wwg9h5ixjgw2pncizf0dp837f5132ncvbs9gw0l3g85")))

(define-public crate-bc-0.1.4 (c (n "bc") (v "0.1.4") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "1p6kz85y30c2bgjjxgyqimbkvscq55hgrj5r5hpnqsnma73zc6jp")))

(define-public crate-bc-0.1.5 (c (n "bc") (v "0.1.5") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "subprocess") (r "^0.2.4") (d #t) (k 0)))) (h "1qi466l2sshwiwcqb1xnfa0rasngs5mgdidaqli7dp02n5v94jrz")))

(define-public crate-bc-0.1.6 (c (n "bc") (v "0.1.6") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "subprocess") (r "^0.2.4") (d #t) (k 0)))) (h "1y6qxncd0l5vw1kpp9kan6l9vmbrxf8wdiqangg5xz75mklyamlv")))

(define-public crate-bc-0.1.7 (c (n "bc") (v "0.1.7") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "execute") (r "^0.2.2") (d #t) (k 0)))) (h "123l69w1qfycxmnimkxw8vgjgdrsmxigh7b1l8pddkw089y582hk")))

(define-public crate-bc-0.1.8 (c (n "bc") (v "0.1.8") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)))) (h "0jy7mzp43fsbl2386af5qghzz0q5waj06sgxdjdf70yzs8bviphy")))

(define-public crate-bc-0.1.9 (c (n "bc") (v "0.1.9") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)))) (h "14kg31fknz4yh79whx6bgkhfkx09226p08zhnnjk74iczg8j8gdh")))

(define-public crate-bc-0.1.10 (c (n "bc") (v "0.1.10") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)))) (h "1alv130prlj15jxr7cx9hcrd8543n3avbkkp1mlb4d9xhnr784bh")))

(define-public crate-bc-0.1.11 (c (n "bc") (v "0.1.11") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)))) (h "1mrzamqpgnrr3gz9x5pjig7b1i9zqvvb9kywzipdbva2gc9dmmc5")))

(define-public crate-bc-0.1.12 (c (n "bc") (v "0.1.12") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)))) (h "1c5dpx4aqgxnn5vrkdzsyq2x75kshz6pkx9mjsa8qb18bpb0kgvc")))

(define-public crate-bc-0.1.13 (c (n "bc") (v "0.1.13") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "execute") (r "^0.2.4") (d #t) (k 0)))) (h "0xm3nvqzwc9i6ijbaw6862ay5j55dk04ambvcbfr86kybqd6src2")))

(define-public crate-bc-0.1.15 (c (n "bc") (v "0.1.15") (d (list (d (n "assert-eq-float") (r "^0.1") (d #t) (k 2)) (d (n "execute") (r "^0.2") (d #t) (k 0)))) (h "14c4vhrqx7y8n4wdjrqx44wzwj6pzxdapjg9j8vmhdmaxr8q9wmz") (r "1.65")))

