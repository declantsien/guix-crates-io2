(define-module (crates-io #{2}# nd) #:use-module (crates-io))

(define-public crate-nd-0.1.0 (c (n "nd") (v "0.1.0") (h "0fznafxwivd5lk68gxspf1385b246j74hzk82w9zmkfsj2dqswiq")))

(define-public crate-nd-0.1.1 (c (n "nd") (v "0.1.1") (d (list (d (n "nd_lib") (r "^0.1.0") (d #t) (k 0)))) (h "006x3zcnllr846k8lza84bm50brs1h99n7hn93mkmycni6hcxbcp")))

