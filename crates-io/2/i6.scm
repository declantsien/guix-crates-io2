(define-module (crates-io #{2}# i6) #:use-module (crates-io))

(define-public crate-i6-0.1.0 (c (n "i6") (v "0.1.0") (h "18mgcpmg3naylwssva5wv9bmpvzk3wggglgdrqdkpawcsfs3d920")))

(define-public crate-i6-0.1.1 (c (n "i6") (v "0.1.1") (h "0nsljbb1sycd46fml2hfmxavxlmclmiwxdcsnnavfmmsbqwnzynd")))

(define-public crate-i6-0.1.2 (c (n "i6") (v "0.1.2") (h "036di092n0bdr6k2zx0l6bipng7kbzgjwmwc333iwc9dapykmzqd")))

(define-public crate-i6-0.1.3 (c (n "i6") (v "0.1.3") (h "14hhq6qfpf5iqc286yrny0bwscc4bbn4m74caz60czblrk1vhxia")))

(define-public crate-i6-0.1.4 (c (n "i6") (v "0.1.4") (h "0rvdc1j1qb7sq2095w4gz5rmwkvhg70lmf7qpijcrkgadjr7b15r")))

(define-public crate-i6-0.1.5 (c (n "i6") (v "0.1.5") (h "025wa4gzhxn0439nnqa0dzd2i0ks8ag9kxyljgycldqsrl5i14nb")))

(define-public crate-i6-0.1.6 (c (n "i6") (v "0.1.6") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rapiddb-web") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "warp") (r "^0.3") (f (quote ("default" "tls"))) (d #t) (k 0)))) (h "0m2d7zs6hs2893wjhvbiipjz8m0lg84h8sxkpcvs9plczpfkfz33")))

(define-public crate-i6-0.1.7 (c (n "i6") (v "0.1.7") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rapiddb-web") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "warp") (r "^0.3") (f (quote ("default" "tls"))) (d #t) (k 0)))) (h "1r7j2w1j6qkm0jsfnibyqlqyj70qfzfyyb7bylbc4raw581h73x6")))

