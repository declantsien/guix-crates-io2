(define-module (crates-io #{2}# kc) #:use-module (crates-io))

(define-public crate-kc-0.0.0 (c (n "kc") (v "0.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0kh5j3lw38qjvzw89m054sn89hl32yyrljav7mpxs3rw6zmi3bs2")))

(define-public crate-kc-0.1.0 (c (n "kc") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)))) (h "18c3xxcvavifa4kgq3xaa2679d6rbdacz68zcj47hyzn3m37xr13")))

(define-public crate-kc-0.2.0 (c (n "kc") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)))) (h "15xvl7w5fq0q46xcf0vv70lgz46319k6x30ykbv421nfsni59dv6")))

(define-public crate-kc-0.3.0 (c (n "kc") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)))) (h "16kd6s3f1bvywakq3gc3xwqj3pygz4fws15v71hh60s0axd73lb6")))

(define-public crate-kc-0.4.0 (c (n "kc") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)))) (h "1x8926pnmwh0xjwvfj6s3y3a5jx5mk4f0s3ys25qnhziwywbjf82")))

(define-public crate-kc-0.4.1 (c (n "kc") (v "0.4.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)))) (h "10b582fvv7vki43596p350ywa7xpmkjc4891356rvg2s5kxkndwh")))

(define-public crate-kc-0.4.2 (c (n "kc") (v "0.4.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)))) (h "120w4ssyal20gl5mfk7xa2l50h21s5wwfpvk91l5p9b7xil5488s")))

(define-public crate-kc-0.5.0 (c (n "kc") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.5") (d #t) (k 0)))) (h "0px64hj9yrxgivmpj6jjngcm4q7f39jk3dal449lpf3nidnwbxv9")))

(define-public crate-kc-0.6.1 (c (n "kc") (v "0.6.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.5") (d #t) (k 0)))) (h "0rgw6za07bjjqbpmj6590rxvkqsbcpm8gig1ibvryal9yih0smjy")))

(define-public crate-kc-0.6.2 (c (n "kc") (v "0.6.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0zp0qjj7iif5mwlpdqh9sjmfs6qjkji0qg1yhjmj1vdak1ffb189")))

(define-public crate-kc-0.7.0 (c (n "kc") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1ymiclss54wm3mlzcnw9ywfmyj1f89gm50hrlbfg1dvynbhirxrd")))

(define-public crate-kc-0.7.1 (c (n "kc") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0viprsrsqzscl0p3y619fxg9r6rnsw9j3l3krw8msamv8bjzbxlz")))

(define-public crate-kc-0.7.2 (c (n "kc") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0hl6h5jwdl1s3lxm391mrcfkkqbv299m3b197g6hvcds1grm2983")))

(define-public crate-kc-0.8.0 (c (n "kc") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1q1kw23570679xwca4192avfl3fq0275rdc9yscy653lpmllgs6l")))

(define-public crate-kc-0.8.1 (c (n "kc") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "14m17zgnpvjdpz5b1bm78hynfdv36a9pgqh1i3bkq74fjgccg7k5")))

(define-public crate-kc-0.9.0 (c (n "kc") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0dvxk713j3vy5l4axqsvrfy9gvjfp0n7xx2dg50lgfck4ansl1yz")))

(define-public crate-kc-0.10.0 (c (n "kc") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1ypljgmxhcnwm06z1n6sdqwz0wsdhkl8wyp000jd2cf33r801vy2")))

(define-public crate-kc-0.10.1 (c (n "kc") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1srncgk02gmh5kd6ykkw4vjyizya0x72i3pmqsbcq9aq2r3vizn1")))

