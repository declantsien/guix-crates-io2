(define-module (crates-io #{2}# uu) #:use-module (crates-io))

(define-public crate-uu-0.1.0 (c (n "uu") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.5.0") (f (quote ("xcb"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.7.0") (f (quote ("xcb"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pango") (r "^0.5.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.6.0") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (d #t) (k 0)))) (h "0bm1ipd1w5lp8529ij42jm7gmipclsicnfr0wvwihp3njgjggnq2")))

(define-public crate-uu-0.1.1 (c (n "uu") (v "0.1.1") (d (list (d (n "cairo-rs") (r "^0.5.0") (f (quote ("xcb"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.7.0") (f (quote ("xcb"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pango") (r "^0.5.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.6.0") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (d #t) (k 0)))) (h "1i2zylzzbfd0pmjsdsg12rim323kscs5d24c8djr0a1z4cgyvg5w")))

