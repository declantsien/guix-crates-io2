(define-module (crates-io #{2}# wh) #:use-module (crates-io))

(define-public crate-wh-0.1.0 (c (n "wh") (v "0.1.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)))) (h "1hjqryj2xj4jrjp34mmansw332pz7mzqw895q8lrpz2li3wjzrs3")))

(define-public crate-wh-0.2.0 (c (n "wh") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.4") (d #t) (k 0)))) (h "005hflwqrrqm03ki1x21nfl316q9n043wjh6iz0gv361x71brp4q")))

(define-public crate-wh-0.2.1 (c (n "wh") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.4") (d #t) (k 0)))) (h "0mfwassy51xcdwqdcslwzkpax2ismdya8w4p246ld6vfd06la4zw")))

(define-public crate-wh-0.3.0 (c (n "wh") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "10zh2gqpwqw9fgv9s4dhsnnxfd8vp3f6lrlbpj3i90dkjmz7zxyh")))

(define-public crate-wh-0.4.0 (c (n "wh") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "1xnbgxj8gjiw3gxywii9vgip11lwmyv9rjdvn0ly85b80hcj2j2n")))

(define-public crate-wh-0.5.1 (c (n "wh") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "04c87lvd63dpqq3sw9332dp284dxgvlz9bmhad8sw7vlgcp5zbw9")))

(define-public crate-wh-0.5.2 (c (n "wh") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0a2gvgxd9j66f91y3lr2jqqlq7ly82qr264anvzsjnzwc7axjnib")))

(define-public crate-wh-0.5.3 (c (n "wh") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1s59vni63anm1r90jg2v6q8xscxw5jnip71hbx5l87gai5fnqx84")))

(define-public crate-wh-0.5.4 (c (n "wh") (v "0.5.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1siwhas31y6zj0jc7d594g0bzzimfbg281vm1mwq29gh711sk1bq")))

(define-public crate-wh-0.6.0 (c (n "wh") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1a2b804kdd3ypp2gr5jq7x5l9fhj90s6ac8bmjankv9xrzx7pa6n")))

(define-public crate-wh-0.7.0 (c (n "wh") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0h13562sq8d9f2nk5ix05xv0ksggxn9yhy1g6pi3z698158sl8bb")))

(define-public crate-wh-0.7.1 (c (n "wh") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0rbs9w9vb062g1gn6bm7f0p2qjq2x6jy2c8f53j55m13c0rycbk8")))

(define-public crate-wh-0.8.0 (c (n "wh") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "057r6asw8mvf106mczf5wk2nlc7rc5xdpxaq6ckqn43q88j419py")))

(define-public crate-wh-0.8.1 (c (n "wh") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "10dpi8jd2w28fil7lmm5611kxf5fgq1ih6bjvy8nwvnqws9nqz77")))

(define-public crate-wh-0.9.0 (c (n "wh") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "00a1c3pav5vvb38drms8ifvwq42s5pql2i88gdvkdybgcg2fd78y")))

(define-public crate-wh-0.9.1 (c (n "wh") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1nv1plrjys7nkrrr5m4fr53y5qzjfbqs0idkp0c6xhscv8dgrgd1")))

(define-public crate-wh-0.9.2 (c (n "wh") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "08vqyf5mka7lj4wkl0ydkj451qhlnqsrl355lywy5my1a2r2p19k")))

