(define-module (crates-io #{2}# ex) #:use-module (crates-io))

(define-public crate-ex-0.1.0 (c (n "ex") (v "0.1.0") (d (list (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "060kxx62pm599xfvddkbrb13nahh51lj5xbf4vjvlvdb0ba7j6bb") (y #t)))

(define-public crate-ex-0.1.1 (c (n "ex") (v "0.1.1") (d (list (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0vl7fmapzd7vrd889qskkz5xbv91yx5qjhchb4r8wjfna81xvp2i") (y #t)))

(define-public crate-ex-0.1.2 (c (n "ex") (v "0.1.2") (d (list (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "1ncks3dr1yxvkfj40qj8diymwzmhi3wn9jlsrnm007c1zhj01zas") (y #t)))

(define-public crate-ex-0.1.3 (c (n "ex") (v "0.1.3") (d (list (d (n "quick-error") (r "^1") (d #t) (k 0)))) (h "1mbl6vpzln3gjwhlsdm8xkr2nm2mc7b5axccgx96p35ihwm8sqgv")))

