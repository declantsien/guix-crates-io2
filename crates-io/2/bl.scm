(define-module (crates-io #{2}# bl) #:use-module (crates-io))

(define-public crate-bl-0.0.0 (c (n "bl") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "12j62y95548ahzspp5vyxjqzhz65chrpfj7z9zq2ir2474afiwfg")))

