(define-module (crates-io #{2}# x3) #:use-module (crates-io))

(define-public crate-x3-0.1.0 (c (n "x3") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "12rq5yn7wxp57l0b4jcxs38dvl7mmg64cwcjrv2f9k4dpq3sn7fk")))

(define-public crate-x3-0.1.1 (c (n "x3") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "0880ahx3vsymfsqhbx1n5dwkhda1hv00jqkcv5dkxml96rgswj0s") (f (quote (("oceaninstruments") ("default" "classic") ("classic"))))))

(define-public crate-x3-0.1.2 (c (n "x3") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "1jxdr4r05fw9i8qbd91qb355wrklf0sgqhk63qz37sy06n02jr6r") (f (quote (("oceaninstruments") ("default" "classic") ("classic"))))))

(define-public crate-x3-0.1.3 (c (n "x3") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "1jsqaf6w7q9a57nzlqwd9845g6ga2j3b7ydxjhqaa3wrlb4g646n") (f (quote (("oceaninstruments") ("default" "classic") ("classic"))))))

(define-public crate-x3-0.1.4 (c (n "x3") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "0plikix56sjhafgjpk00jbmfygw6zypx4lmkwjv3kqprxv0qg8q2") (f (quote (("oceaninstruments") ("default" "classic") ("classic"))))))

(define-public crate-x3-0.1.5 (c (n "x3") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "18sas1xnk09crjfamxvh1wxwp74yri6ay544g0k7f1d45v2ag99l") (f (quote (("oceaninstruments") ("default" "classic") ("classic"))))))

(define-public crate-x3-0.1.6 (c (n "x3") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "13p2dx6w2p1fgbr0vh1dbpwc5rykhw08jd957jq4fg3i05vi48kz") (f (quote (("oceaninstruments") ("default" "classic") ("classic"))))))

(define-public crate-x3-0.1.7 (c (n "x3") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "1cxsfd0qsah1h9hz0gw8fp2kcnj4s37y2h02in0v3fv1zzczmzah") (f (quote (("oceaninstruments") ("default" "classic") ("classic"))))))

(define-public crate-x3-0.1.8 (c (n "x3") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "1qd92b8q7lh93ajjrd2ckwbl3y1qrd5i9p23pvf7240smn2a96i0") (f (quote (("oceaninstruments") ("default" "classic") ("classic"))))))

(define-public crate-x3-0.1.9 (c (n "x3") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "01m5qr953byxmaamxkj3ycx7dckvdmbli0w1n93akiywx85cqfjq") (f (quote (("oceaninstruments") ("default" "classic") ("classic"))))))

(define-public crate-x3-0.2.0 (c (n "x3") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "0zf0ljd16wf7dfdyz43c9p6m8kax74jzaa546nlgs9wf01qimj84") (f (quote (("oceaninstruments") ("default" "classic") ("classic"))))))

(define-public crate-x3-0.2.1 (c (n "x3") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.3") (d #t) (k 0)))) (h "08ic5vyxi6dh0rfrmx9wlbwyb1pmn2kbfsqp10rsl4qjzk0whhjp") (f (quote (("oceaninstruments") ("default" "classic") ("classic"))))))

(define-public crate-x3-0.2.2 (c (n "x3") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.18.1") (d #t) (k 0)))) (h "0y5wnr891wmrncr7w31c90yqmqjjjb73s8vcihgdrnscj6nlh7l8")))

(define-public crate-x3-0.3.0 (c (n "x3") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.18.1") (d #t) (k 0)))) (h "1qdc0pxdbrjg75h8969gd27hrj77vnxyvbpw3rhnj33il1sgfzaw")))

