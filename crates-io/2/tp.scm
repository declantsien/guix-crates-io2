(define-module (crates-io #{2}# tp) #:use-module (crates-io))

(define-public crate-tp-0.1.2 (c (n "tp") (v "0.1.2") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "01vvpdxz9v7fhgawhhyagywl0pf70k34g0d15lkxm4nh0c22yxqg")))

(define-public crate-tp-0.1.3 (c (n "tp") (v "0.1.3") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "0fbya1r451jrvv7i90b6xbbww45lzb20h7ciml1qvyx29fbdj4l9")))

(define-public crate-tp-0.1.4 (c (n "tp") (v "0.1.4") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "04sq1h3nlgszypizhapwvj61g0jr7kpi1q81h7m9ycm71kq208d4")))

(define-public crate-tp-0.1.5 (c (n "tp") (v "0.1.5") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "0afqgh01xg7pxa9pgbxk4pxj7isdc2mb7g457qhy4nmbp8fb3rh7")))

(define-public crate-tp-0.1.6 (c (n "tp") (v "0.1.6") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "0gz6p9yznycqqmm60kfvqgwxq69fgj7g9qm7a893s2mz5d9sbvf5")))

(define-public crate-tp-0.1.7 (c (n "tp") (v "0.1.7") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0s7wlindw2mf4kdgwpwnddvdm8c974zacawfm9hmdkhpprd5w87v")))

(define-public crate-tp-0.1.8 (c (n "tp") (v "0.1.8") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "14jnylmghf91yfmr95j70yimdks2agna0070gwp5ambhb1y50wnx")))

(define-public crate-tp-0.1.9 (c (n "tp") (v "0.1.9") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1pmn0m891nyaqkxmxb1hbmz87p2hpwgfxbgcg7b83l5z6nrnzxf1")))

(define-public crate-tp-0.1.10 (c (n "tp") (v "0.1.10") (d (list (d (n "daachorse") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (d #t) (k 0)))) (h "1xvplyf9zw8h0b47v5xf4sd2vqgigcr5h5aldzygj84fknv26n47") (f (quote (("default")))) (s 2) (e (quote (("mut" "dep:daachorse" "dep:static_init" "dep:thiserror"))))))

(define-public crate-tp-0.1.11 (c (n "tp") (v "0.1.11") (d (list (d (n "daachorse") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (d #t) (k 0)))) (h "13mwgygf29yga0ijndradsmyv8vp6l772mn425dqjqbbp8cjcwbc") (f (quote (("default")))) (s 2) (e (quote (("mut" "dep:daachorse" "dep:static_init" "dep:thiserror"))))))

(define-public crate-tp-0.1.12 (c (n "tp") (v "0.1.12") (d (list (d (n "daachorse") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (d #t) (k 0)))) (h "1r3644769g9y2fm78ddzdmz41iyxw4ndps354d8v4ryik3wbggpg") (f (quote (("extend") ("default")))) (s 2) (e (quote (("mut" "dep:daachorse" "dep:static_init" "dep:thiserror"))))))

(define-public crate-tp-0.1.13 (c (n "tp") (v "0.1.13") (d (list (d (n "daachorse") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (d #t) (k 0)) (d (n "xhash") (r "^0.1.16") (f (quote ("hash_li"))) (o #t) (d #t) (k 0)))) (h "079m33vabqz21h4hnprycq8rdc3pwbzr4is2rkanr2sg5ni9zp2m") (f (quote (("extend") ("default")))) (s 2) (e (quote (("mut" "dep:daachorse" "dep:static_init" "dep:thiserror") ("hash" "dep:xhash"))))))

(define-public crate-tp-0.1.14 (c (n "tp") (v "0.1.14") (d (list (d (n "daachorse") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (o #t) (d #t) (k 0)) (d (n "xhash") (r "^0.1.21") (f (quote ("hash_li"))) (o #t) (d #t) (k 0)))) (h "0fhnxhblxglhc6drnmvbwq2nb3bcz5g9nf5bif0vvy1z5zw1v2hb") (f (quote (("extend") ("default")))) (s 2) (e (quote (("mut" "dep:daachorse" "dep:static_init" "dep:thiserror") ("hash" "dep:xhash"))))))

