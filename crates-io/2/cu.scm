(define-module (crates-io #{2}# cu) #:use-module (crates-io))

(define-public crate-cu-0.1.0 (c (n "cu") (v "0.1.0") (d (list (d (n "cu-core") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0prgb0q2mn005nj8cll4dgf3i3qxyyx1y9nmylpmcmjvaqbdglf1")))

(define-public crate-cu-0.1.1 (c (n "cu") (v "0.1.1") (d (list (d (n "cu-core") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1l8khw950k0lb2wm91yzrh0zy3g96hn881apf6akrkz3lciw5szl")))

