(define-module (crates-io #{2}# pj) #:use-module (crates-io))

(define-public crate-pj-0.1.0 (c (n "pj") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0gvqzw7wmlqqpygyyys4q12nzhbgh1khznl9ycng7j3vs2iglkca")))

(define-public crate-pj-1.0.0 (c (n "pj") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.6") (d #t) (k 0)) (d (n "crossterm_input") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "^0.4.0") (f (quote ("crossterm"))) (k 0)))) (h "1wzyhinws9pp4zv2ycyvv7jhdh5lvcsd14k8zldhhnrza779y997")))

