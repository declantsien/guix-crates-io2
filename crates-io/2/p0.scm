(define-module (crates-io #{2}# p0) #:use-module (crates-io))

(define-public crate-p0-0.0.1 (c (n "p0") (v "0.0.1") (h "0iwn430h068svhminfva9nk47lwqnv2f4mlj05y8fgg26hc3lkjn") (y #t)))

(define-public crate-p0-0.0.2 (c (n "p0") (v "0.0.2") (h "0myibh2mhd933xssjsmrjkx531vi2w15x7dk6023n4170dnpjcgh") (y #t)))

(define-public crate-p0-0.0.3 (c (n "p0") (v "0.0.3") (h "1ilhb3c035fiq6cm9rzbqfpxawwvw2i7lwgsrz9bva3mk53zqbc4")))

