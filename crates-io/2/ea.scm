(define-module (crates-io #{2}# ea) #:use-module (crates-io))

(define-public crate-ea-0.1.2 (c (n "ea") (v "0.1.2") (h "0x28kfwr25hb1394fw5d82sy2p6s6ahnkcpxa8h2yv7gcix3wh71")))

(define-public crate-ea-0.1.0 (c (n "ea") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qjazcvj69spl8kz62z96v1kp7w5vyb646nwmjb0dvrxwfc08lld")))

