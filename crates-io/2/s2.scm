(define-module (crates-io #{2}# s2) #:use-module (crates-io))

(define-public crate-s2-0.0.1 (c (n "s2") (v "0.0.1") (d (list (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "19pkbx2nsj4waixb59x4qgr7fhfp5nasvh2wcpwxkll1xfy4hi9s")))

(define-public crate-s2-0.0.2 (c (n "s2") (v "0.0.2") (d (list (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1mr5m190igj859wpwbnlkkf7f5ycaficgg4d57lmzh2sgl535hmy")))

(define-public crate-s2-0.0.3 (c (n "s2") (v "0.0.3") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 0)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0mwmvs0fac4lici2y375v4jl1lgxiac7gyi58x293hvjlhn2lk03")))

(define-public crate-s2-0.0.4 (c (n "s2") (v "0.0.4") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 0)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "06gzfm7h7q4xcclfzz91sbmjvn93rjcr4c3g4qspihp1njqgv03b")))

(define-public crate-s2-0.0.5 (c (n "s2") (v "0.0.5") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 0)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1lyimaki8f5chybplllvp3m6wxw0i47y04h743ir027c8k6g1hqk")))

(define-public crate-s2-0.0.6 (c (n "s2") (v "0.0.6") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 0)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1c1ygzi7yx1s166b87b51bmbfvvi3n02rl4yl7by3p170n036bz9")))

(define-public crate-s2-0.0.7 (c (n "s2") (v "0.0.7") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0fbqwaajvmxcjlzz14ly482h31h5mp35vxs362xbj58gyl32r94y")))

(define-public crate-s2-0.0.8 (c (n "s2") (v "0.0.8") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1za6kv6k4faays8cmnsg6ylvf6sch77db002yms766y1zk5mpi2j")))

(define-public crate-s2-0.0.9 (c (n "s2") (v "0.0.9") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libm") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "18ad8ingbsbki6jnyxp94pd13l3c48131xjrdx49fv9yfz1wlvap")))

(define-public crate-s2-0.0.10 (c (n "s2") (v "0.0.10") (d (list (d (n "bigdecimal") (r "^0.1") (d #t) (k 0)) (d (n "bigdecimal") (r "^0.1") (f (quote ("serde"))) (d #t) (t "cfg(feature = \"serde\")") (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libm") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "143ynm2xwybifaaaqqghcmb7zi3w8d4j12qgp0i6bx5apiiv2wys")))

(define-public crate-s2-0.0.11 (c (n "s2") (v "0.0.11") (d (list (d (n "bigdecimal") (r "^0.3.0") (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "float_extras") (r "^0.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0nffb0dn1s8hyqibsxcdb0d4lwmxpcgrvgmcpf5vcv6n3a7vjb9z") (s 2) (e (quote (("serde" "dep:serde" "bigdecimal/serde"))))))

(define-public crate-s2-0.0.12 (c (n "s2") (v "0.0.12") (d (list (d (n "bigdecimal") (r "^0.3.0") (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "float_extras") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0p6di3am81kmfgrxj98cds8h2lrpc4ljvfy991ghpi2jpc2bqzyc") (f (quote (("default" "serde" "float_extras")))) (s 2) (e (quote (("serde" "dep:serde" "bigdecimal/serde"))))))

