(define-module (crates-io #{2}# ef) #:use-module (crates-io))

(define-public crate-ef-0.0.0 (c (n "ef") (v "0.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "comrak") (r "^0.9") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "html-minifier") (r "^3") (d #t) (k 0)) (d (n "lazy-static-include") (r "^3") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0890z9qsjfdi2ciw5f8767fcyvkywrc1x023cdm6w03y0863r4aj")))

