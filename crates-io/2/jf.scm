(define-module (crates-io #{2}# jf) #:use-module (crates-io))

(define-public crate-jf-0.1.0 (c (n "jf") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "002qmzfr49nyicyybld2f79z1673wf3i2jvs7risgkhh161x2dcc") (y #t)))

(define-public crate-jf-0.1.1 (c (n "jf") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1k963g32l4ns35hbxm3k4dr16ah6jmkkiw8s76gcalplfby34rgm")))

(define-public crate-jf-0.1.2 (c (n "jf") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "18klc8z04dabrxyfkanpfg74a1307jhv8mbcc5j0lmr4n0z8a59f")))

(define-public crate-jf-0.2.1 (c (n "jf") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1z62alsdbkk7ik0l221h83xgpvdbdxjr2pzmfv18r2rcfdwx1kgk")))

(define-public crate-jf-0.2.2 (c (n "jf") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "181bvf3vy742c8gr1a0aarjg7kn8ii6wvdnq6vfq8crw192wak9n")))

(define-public crate-jf-0.2.3 (c (n "jf") (v "0.2.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "03rb1rp2m0b22vns07497m5pkl8syp0zlwzp4vf4c2l0dwm9yfdd")))

(define-public crate-jf-0.2.4 (c (n "jf") (v "0.2.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1gdamady0z099lyz2wq1g7ljbqjm6v79x048xvms6fqq5di5nbds")))

(define-public crate-jf-0.2.5 (c (n "jf") (v "0.2.5") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0smdp3jbfk75pak8awrjknk2y0f8vdb2kp7r5jqdcnkvd0dn2z2k")))

(define-public crate-jf-0.2.6 (c (n "jf") (v "0.2.6") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "044vgjqk1wmfx9c7qcjpx72n23b0s00mppx9hk1x899gj5z0dw2f")))

(define-public crate-jf-0.2.7 (c (n "jf") (v "0.2.7") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0841myk935yppkzirshnh686abgam2jansx8pxp4j2mrhnpgwpng") (f (quote (("manpage") ("default" "manpage"))))))

(define-public crate-jf-0.3.0 (c (n "jf") (v "0.3.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1vc38w33fbqxz6ga43b0lj6z3giranf3m5vbncpv6vmhbhcmp1vf") (f (quote (("manpage") ("default" "manpage"))))))

(define-public crate-jf-0.3.1 (c (n "jf") (v "0.3.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "161778qch5v2rwc2dfy418xl6gpqrgn0hya6rgj34mbhd641z79p") (f (quote (("manpage") ("default" "manpage"))))))

(define-public crate-jf-0.3.2 (c (n "jf") (v "0.3.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "04j1485msb2a6b83j6sfzph7118xs9dhzisq0f1380l1hdk8kflk") (f (quote (("manpage") ("default" "manpage"))))))

(define-public crate-jf-0.3.3 (c (n "jf") (v "0.3.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0iihsllgg69q4x8vz3ikp8ncr5xkn970a87mhprhhn9aiqvflg6w") (f (quote (("manpage") ("default" "manpage"))))))

(define-public crate-jf-0.4.0 (c (n "jf") (v "0.4.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0r9r6mfg35a77lm484fs35iwr4y7ry2ipca7nq9s13lax8zbq7m4") (f (quote (("manpage") ("default" "manpage"))))))

(define-public crate-jf-0.4.1 (c (n "jf") (v "0.4.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "081x79rsybybjapyqylmhzb1igivl38y5v7wxbl1nsswby4js67y") (f (quote (("manpage") ("default" "manpage"))))))

(define-public crate-jf-0.4.2 (c (n "jf") (v "0.4.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1jm0nzxmbhi0g5acc5kd1yqw59jsa2wi2q99i0dwysvsd2l29r9c") (f (quote (("manpage") ("default" "manpage"))))))

(define-public crate-jf-0.5.0 (c (n "jf") (v "0.5.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0zb9xvw5jldblvz5nar8ig0d28zh4hzdnlmr6gk6855b6b40azk1") (f (quote (("manpage") ("default" "manpage"))))))

(define-public crate-jf-0.6.0 (c (n "jf") (v "0.6.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "124mz47h88hpdbh8skhbfhyf63s74g7rqq97z7z90xwwwbhc6h5j") (f (quote (("manpage") ("default" "manpage"))))))

(define-public crate-jf-0.6.1 (c (n "jf") (v "0.6.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0kiq266gnjzpw1mbl4755m4xl9686ddirr2pm0gaxjxi8ixfv0sp") (f (quote (("manpage") ("default" "manpage"))))))

(define-public crate-jf-0.6.2 (c (n "jf") (v "0.6.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0kv9lcb0ja0xsnf24ylxgnpyrplvkhx3d0gmci34kjhp16282qi1") (f (quote (("manpage") ("default" "manpage"))))))

