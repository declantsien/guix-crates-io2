(define-module (crates-io #{2}# is) #:use-module (crates-io))

(define-public crate-is-0.1.0 (c (n "is") (v "0.1.0") (h "0cxfqzp9i74kjinrp371l0hg5bq3w0xwn91gj98xd7spw6g84k7a") (y #t)))

(define-public crate-is-0.2.0 (c (n "is") (v "0.2.0") (h "0x3csw531ws2jdfqq9immywlcmz02zlclpcg23gfk74y58hbpjgj") (y #t)))

(define-public crate-is-0.2.1 (c (n "is") (v "0.2.1") (h "0d7045sqn7ikzyc5ldh2qyn086760z2932a72498qkydw8fvf3nk") (y #t)))

(define-public crate-is-0.3.0 (c (n "is") (v "0.3.0") (h "1a7922qp5gfh9z4agpz79wrhrn9lxm6yxbiiyz6zp3p4w24rv548") (y #t)))

(define-public crate-is-0.4.0 (c (n "is") (v "0.4.0") (h "03v5cshp96pa777a2w4j19k6g0g3riw8rl8plm591hp2sy81qagm") (y #t)))

(define-public crate-is-0.5.1 (c (n "is") (v "0.5.1") (h "0bjffycdq8n6b25bbghrdbir1m1zkah15frr0fbxw7bb5r4l1469") (y #t)))

(define-public crate-is-0.6.0 (c (n "is") (v "0.6.0") (h "1ya05dfsg1l7c0y2k8qqdxcm0qq77wphv0v4qy5wkd3rnvb03n1n")))

