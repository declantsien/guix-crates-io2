(define-module (crates-io #{2}# pz) #:use-module (crates-io))

(define-public crate-pz-0.0.0 (c (n "pz") (v "0.0.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)))) (h "0vn4x0il61ifp629ljqg6d7fnfbpidqbghy114bz9bk48s4pbkg7") (f (quote (("default"))))))

(define-public crate-pz-0.0.1 (c (n "pz") (v "0.0.1") (h "02z70b8775di1fvayw4gj83kh3895i9z38hg16df3p2rcwjqwsw7") (f (quote (("default"))))))

(define-public crate-pz-0.0.2 (c (n "pz") (v "0.0.2") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "imagequant") (r "^4.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "12frjfkqm3igf7jl4mkcd6w32rgzjxnavcg4vl7317yav2wr4l7j") (f (quote (("default"))))))

(define-public crate-pz-0.0.3 (c (n "pz") (v "0.0.3") (d (list (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "imagequant") (r "^4.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1mjq45yfxas9w1lnwr3fzgadawyki6a9gl906y5xr1k543ybf48m") (f (quote (("default"))))))

