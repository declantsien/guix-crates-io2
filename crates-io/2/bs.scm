(define-module (crates-io #{2}# bs) #:use-module (crates-io))

(define-public crate-bs-0.1.0 (c (n "bs") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "18g2jhjr2jmqmslwj3p27bapzkfhlwbqnvjd2dc2r82k9gv703xg")))

(define-public crate-bs-0.2.0 (c (n "bs") (v "0.2.0") (d (list (d (n "bit-set") (r "^0.5.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1h83mfqlavan5j5l688qjshhmfv9n9vi3dshb2k31p5sb51vyf3x")))

