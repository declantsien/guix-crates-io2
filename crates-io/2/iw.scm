(define-module (crates-io #{2}# iw) #:use-module (crates-io))

(define-public crate-iw-0.0.1 (c (n "iw") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1lxl01vmma3ylbfrva8rakm0lfg8r32hhl530hy3wplhchpbm4p1")))

