(define-module (crates-io #{2}# ar) #:use-module (crates-io))

(define-public crate-ar-0.1.0 (c (n "ar") (v "0.1.0") (h "1x2xab4ljjq5b88mafnf0nmdkv39vjlwhf479w9fb28nvmwkr77k")))

(define-public crate-ar-0.2.0 (c (n "ar") (v "0.2.0") (h "0wxsw5s07nq65nqzxdrh3yway1gp9610n50dm2si7x6n49qcqxg7")))

(define-public crate-ar-0.3.0 (c (n "ar") (v "0.3.0") (h "1j8pjxmmg7gxs0cxqg91d0x9v3fzkh1ycmxifp5f59rzivplwkmj")))

(define-public crate-ar-0.3.1 (c (n "ar") (v "0.3.1") (h "0cgcg5iynk5x7fvg3dv44h148vafnw416ziq75bhhkxnkikabirm")))

(define-public crate-ar-0.4.0 (c (n "ar") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0g7wgyrl5ynqa8fi432adcqk1ndxk9gyb8851zzk44ihfwap5rxx")))

(define-public crate-ar-0.5.0 (c (n "ar") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0ndhgazv4dn5ggrgjmj0p2347q4pvvw97glpbnvj4idg8pcymikh")))

(define-public crate-ar-0.6.0 (c (n "ar") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "118ari31rnxr9mb98zbz0rvdrdgy7jngk13h8b12mdlhh9h1am89")))

(define-public crate-ar-0.6.1 (c (n "ar") (v "0.6.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1cyj43qv9140bhb416fj4clfy9f2iz21mlv6d266mfz60rnvcrib")))

(define-public crate-ar-0.6.2 (c (n "ar") (v "0.6.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1i3vb75aarhipsazbnpdjvr17fgyz42rx5p6bimrs7ndzsrq35jp")))

(define-public crate-ar-0.7.0 (c (n "ar") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1mbvp597kwsrw55f1d49rfvhsb0dxibyf3pcxsl5y8vam5kgs6kc")))

(define-public crate-ar-0.8.0 (c (n "ar") (v "0.8.0") (h "1qajmxnl762bxnn0w7m8qabp6ya7ph64gzxvda0k5vkvizspa1a5")))

(define-public crate-ar-0.9.0 (c (n "ar") (v "0.9.0") (h "0sdvvrf4inrkmrh6lzwg4z8x38b3gncbb8yqrgayqcd9d1yzfynn")))

