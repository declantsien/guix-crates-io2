(define-module (crates-io #{2}# ok) #:use-module (crates-io))

(define-public crate-ok-0.1.0 (c (n "ok") (v "0.1.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "000cf1ll6ws4r6rc4izqkwppk69ypq301ic6a12lhsi7c7wk8ldq")))

