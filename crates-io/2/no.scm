(define-module (crates-io #{2}# no) #:use-module (crates-io))

(define-public crate-no-0.1.0 (c (n "no") (v "0.1.0") (h "14li8xgh35j5d6g560lgphs7wh08m0h2ziq6kw6yr2zmrd04w6az")))

(define-public crate-no-0.2.0 (c (n "no") (v "0.2.0") (d (list (d (n "clap") (r "~2.24.0") (d #t) (k 0)))) (h "1sbp3s0rpz751prx5nvjpw1agm0izznbqbmc95f7rs9dbb08jlh6")))

(define-public crate-no-0.3.0 (c (n "no") (v "0.3.0") (d (list (d (n "clap") (r "~2.24.0") (d #t) (k 0)))) (h "1fgvy5lisk3fjnn51bkk07vpy7yzs517p8dyl8abcm3jkcjw2lm7")))

(define-public crate-no-0.3.1 (c (n "no") (v "0.3.1") (d (list (d (n "clap") (r "~2.24.0") (d #t) (k 0)))) (h "1i6i2ffwimdxjh4ycyrzmkkzsaypgc2nq7hq93ycfa9is3xnd0x2")))

