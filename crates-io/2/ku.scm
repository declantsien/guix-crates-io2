(define-module (crates-io #{2}# ku) #:use-module (crates-io))

(define-public crate-ku-0.0.0 (c (n "ku") (v "0.0.0") (h "15rzdbr65w2qc4m8n28jig87i4xj7ykqrks709mgj5yxk1rvjy58")))

(define-public crate-ku-0.1.0 (c (n "ku") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "061f5bilhpahjsr3ljlmkfih7gzdqz8qwaszxckd57yxw3c49alm") (f (quote (("default" "2D") ("9D") ("8D") ("7D") ("6D") ("5D") ("4D") ("3D") ("2D") ("12D") ("11D") ("10D"))))))

