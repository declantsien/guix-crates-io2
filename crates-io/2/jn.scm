(define-module (crates-io #{2}# jn) #:use-module (crates-io))

(define-public crate-jn-0.1.0 (c (n "jn") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s445d4b41235vzrn19y2jjlwvdxp1n73qpw9z48i9kzijblp72d")))

