(define-module (crates-io #{2}# se) #:use-module (crates-io))

(define-public crate-se-0.1.0 (c (n "se") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "dialect") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "syntax-rust") (r "^0.1") (d #t) (k 0)))) (h "1dhayi81v970b7cnrbh3hz87lx080ibfkqr3ww7y3cxgjnqlz93r")))

