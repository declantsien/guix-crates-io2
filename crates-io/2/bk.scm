(define-module (crates-io #{2}# bk) #:use-module (crates-io))

(define-public crate-bk-0.1.0 (c (n "bk") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "roxmltree") (r "^0.11") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "0y2j1pr8zkrcc3qfhhkwhnj1xwcgpc2r0m9243ifhi9cm4jhj09x")))

(define-public crate-bk-0.1.1 (c (n "bk") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "roxmltree") (r "^0.11") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "0dl8lcvdwz20qyj5i3faws6n64qlby7401531im2w8a91j0dnf6h")))

(define-public crate-bk-0.2.0 (c (n "bk") (v "0.2.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "roxmltree") (r "^0.11") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "0nr2v32yia1bgj70dw8ac9dhpq128pydj6w7k48fc8mfz4mcm9zj")))

(define-public crate-bk-0.3.0 (c (n "bk") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "crossterm") (r "^0") (d #t) (k 0)) (d (n "ron") (r "^0") (d #t) (k 0)) (d (n "roxmltree") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0") (f (quote ("deflate"))) (k 0)))) (h "1yql1zvpy3qs8cc1m2kahdas1avia0jpq2fk4s584gd16ami7hl5")))

(define-public crate-bk-0.4.1 (c (n "bk") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "crossterm") (r "^0") (d #t) (k 0)) (d (n "ron") (r "^0") (d #t) (k 0)) (d (n "roxmltree") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0") (f (quote ("deflate"))) (k 0)))) (h "0656b5m8mf4ip2lkzrlmbkdvm3wznv45f9crfnr2irif3r5p4y05") (y #t)))

(define-public crate-bk-0.4.2 (c (n "bk") (v "0.4.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "crossterm") (r "^0") (d #t) (k 0)) (d (n "ron") (r "^0") (d #t) (k 0)) (d (n "roxmltree") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0") (f (quote ("deflate"))) (k 0)))) (h "0xwqs5j06zq4fd1mq4kv2nqnw27sj113gs40r37zkg00qmbcm09b")))

(define-public crate-bk-0.5.0 (c (n "bk") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "crossterm") (r "^0") (d #t) (k 0)) (d (n "ron") (r "^0") (d #t) (k 0)) (d (n "roxmltree") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0") (d #t) (k 0)) (d (n "zip") (r "^0") (f (quote ("deflate"))) (k 0)))) (h "1z62rzxgmm08lbr99ccdzapsh6vp3aysj9aswp1j4sjx44ky2wgd")))

(define-public crate-bk-0.5.1 (c (n "bk") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "crossterm") (r "^0") (d #t) (k 0)) (d (n "ron") (r "^0") (d #t) (k 0)) (d (n "roxmltree") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0") (d #t) (k 0)) (d (n "zip") (r "^0") (f (quote ("deflate"))) (k 0)))) (h "1y657vdxw79gr3d4jrgg5bzpr6qy6jj3jpl5vrvgsfqvi9h61xbf")))

(define-public crate-bk-0.5.2 (c (n "bk") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "12glgz8vjfzi0csgp7p842asaqdkvfr0wgb4li74qjlhry0achd4")))

(define-public crate-bk-0.5.3 (c (n "bk") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "17gjwy117iww4ash42942y11gcvd2df1gm4zpsqhf9n5khkb0rvy")))

(define-public crate-bk-0.6.0 (c (n "bk") (v "0.6.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "09m3prk4z149nilnjnbvqlrqh7v7gxjw1jx2hscafqqm3qxjj8qy")))

