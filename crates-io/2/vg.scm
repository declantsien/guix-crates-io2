(define-module (crates-io #{2}# vg) #:use-module (crates-io))

(define-public crate-vg-0.0.0-squat (c (n "vg") (v "0.0.0-squat") (h "1jnclzagbgw5my7k81zqkm4s5zjc3621dj5pn5kdp7h9i75l0yjm")))

(define-public crate-vg-0.0.0-obsolete (c (n "vg") (v "0.0.0-obsolete") (d (list (d (n "png") (r "^0.15") (d #t) (k 0)))) (h "083h2iir6ikyrqk1482msa2wqv7d0f577sga3ll21hy1q94ndnyk")))

