(define-module (crates-io #{2}# v9) #:use-module (crates-io))

(define-public crate-v9-0.1.0 (c (n "v9") (v "0.1.0") (d (list (d (n "mopa") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1jspjn0jyhdm8kpgdj5jb2jd745qalvy3vx1lalh2dw8riv2xxi2")))

(define-public crate-v9-0.1.1 (c (n "v9") (v "0.1.1") (d (list (d (n "mopa") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0wfvdy39sbx9ba2zjky8ycghvbjyvrl4avmpqs5jayl8k21mi5a0")))

(define-public crate-v9-0.1.2 (c (n "v9") (v "0.1.2") (d (list (d (n "mopa") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1nakcp2jir2x9d82i3v2gg94glj6mij309lrnkaf9fjl57p83kfc")))

(define-public crate-v9-0.1.37 (c (n "v9") (v "0.1.37") (d (list (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "v9-attr") (r "^0.1.0") (d #t) (k 0)))) (h "0d8xfkmicyfyhyhan4zv4pk1lsdsh0izmiw849xg9w6iyr7yx14m")))

(define-public crate-v9-0.1.38 (c (n "v9") (v "0.1.38") (d (list (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "v9-attr") (r "^0.1.0") (d #t) (k 0)))) (h "192ggz9blkjwmqp6z6jnz0qxpdv91w4rmfgc6h8n8i3l1gmhf43n")))

(define-public crate-v9-0.1.39 (c (n "v9") (v "0.1.39") (d (list (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "v9-attr") (r "^0.1.0") (d #t) (k 0)))) (h "1b6jckm54xj0pq9i15ahqsiz74rcqjxkfpr5zmx2vnl3ky8zypaj")))

(define-public crate-v9-0.1.40 (c (n "v9") (v "0.1.40") (d (list (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "v9-attr") (r "^0.1.0") (d #t) (k 0)))) (h "0v6mw70i0z19z3zm8ssykwzv4mh3kppg8l7p9insd23aiapqc18k")))

(define-public crate-v9-0.1.41 (c (n "v9") (v "0.1.41") (d (list (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "v9-attr") (r "^0.1.0") (d #t) (k 0)))) (h "010ws7mwpn7pmn9zqmbliaak70glax616rclh2v59xdm1w29alwv")))

(define-public crate-v9-0.1.42 (c (n "v9") (v "0.1.42") (d (list (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "v9-attr") (r "^0.1.0") (d #t) (k 0)))) (h "0p5wrgaxmpy1zm7shpqmbsbblf18bfdvwf1aqdk3qiv09dnbh0hx")))

(define-public crate-v9-0.1.43 (c (n "v9") (v "0.1.43") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "v9-attr") (r "^0.1.0") (d #t) (k 0)))) (h "18qb68aizkd8sfl1bim623m1hxzbc8yz1fwyp30yj0whyyw0a7s8")))

