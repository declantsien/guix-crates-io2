(define-module (crates-io #{2}# mo) #:use-module (crates-io))

(define-public crate-mo-0.1.0 (c (n "mo") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.1") (d #t) (k 0)) (d (n "pest_vm") (r "^2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "stacker") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0l0fdkjc6ily0h8ajb8msnji9c9z4z66m5bxqd1239ls26pnl4l2")))

