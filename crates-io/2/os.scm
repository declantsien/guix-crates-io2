(define-module (crates-io #{2}# os) #:use-module (crates-io))

(define-public crate-os-0.0.0 (c (n "os") (v "0.0.0") (h "12lsr8n92k961dgnfxyb2apr5w6iln837x90fpims60y9n2b4cd1")))

(define-public crate-os-0.1.0 (c (n "os") (v "0.1.0") (h "04d9iiicpg46xn5wnx1p9jvnzdqrz87jj1ahcwbjpa44p4msdzcz")))

