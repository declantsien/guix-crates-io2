(define-module (crates-io #{2}# dw) #:use-module (crates-io))

(define-public crate-dw-0.1.0 (c (n "dw") (v "0.1.0") (d (list (d (n "dw-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18r07wri4gh7k3wpbbqhwzyzvmsrl1vgv9yk4sp8m38rwmhk1blj")))

(define-public crate-dw-0.2.0 (c (n "dw") (v "0.2.0") (d (list (d (n "dw-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hw0rbrydijcf6y059vq80pj10ymbg4z4jwfnjgvfajwfqmxh3pg")))

