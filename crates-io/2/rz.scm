(define-module (crates-io #{2}# rz) #:use-module (crates-io))

(define-public crate-rz-0.1.0 (c (n "rz") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1j8b3a1ry0y7anl2q2a3661ig34by3q44z5d24h09r9brd36in2f")))

(define-public crate-rz-0.1.1 (c (n "rz") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1266l6wqvja41v3if43ggzjqr47krh6a9x948w123c07ajyrm5n2")))

(define-public crate-rz-0.1.2 (c (n "rz") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "12xha0yfddm5h94n516d6abw4abnzgps2x4pa8sgafv1xgn5w380")))

(define-public crate-rz-0.1.3 (c (n "rz") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1x42zkr7f4w1kgr6k9q6wlyykkfwlqwj59p55qk84qjyayfs21g3")))

(define-public crate-rz-0.1.4 (c (n "rz") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1h1xs8i71b4ys5nf2lpkx6hd3y92hwqkasv5snzdh38ha290kwkc")))

(define-public crate-rz-0.1.5 (c (n "rz") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "16hmwb6zpl109fmfagbjcssmbx9s33nmr573f0dm3z821bdp18kp")))

