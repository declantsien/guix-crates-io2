(define-module (crates-io #{2}# ul) #:use-module (crates-io))

(define-public crate-ul-0.1.0-rc2 (c (n "ul") (v "0.1.0-rc2") (h "1lwxd84msjj0b3xvk9ykajycwqwy7nbzhil3slyias7k4f579ngw")))

(define-public crate-ul-1.0.0-pre2 (c (n "ul") (v "1.0.0-pre2") (d (list (d (n "image") (r "^0.21.0") (d #t) (t "cfg(feature = \"image\")") (k 0)) (d (n "ul-sys") (r "^1.0.0") (d #t) (k 0)))) (h "1zyi5rj6i70vw4p81gcwkzgyc30valiziz8qh0sf4j34352ipg51")))

(define-public crate-ul-1.0.0-pre3 (c (n "ul") (v "1.0.0-pre3") (d (list (d (n "image") (r "^0.21.0") (d #t) (t "cfg(feature = \"image\")") (k 0)) (d (n "ul-sys") (r "^1.0.0") (d #t) (k 0)))) (h "1hr4fb92qw71zcqzanjvb5jqjhrfxxl83y2dvwafgy9hbng5227m")))

(define-public crate-ul-1.0.0 (c (n "ul") (v "1.0.0") (d (list (d (n "ul-sys") (r "^1.0.1") (d #t) (k 0)))) (h "1q5b1q5nzn5dp5vyf208xsy2vpbw4f6skn569bjc7w5rxnzk0hpx")))

(define-public crate-ul-1.0.1 (c (n "ul") (v "1.0.1") (d (list (d (n "ul-sys") (r "^1.0.1") (d #t) (k 0)))) (h "0sckwvrmgl16zd669miapcrjkwvmlvxczi0lai5dv5yfpy4pgr3s")))

(define-public crate-ul-1.1.1 (c (n "ul") (v "1.1.1") (d (list (d (n "ul-sys") (r "^1.1.1") (d #t) (k 0)))) (h "14sj8yygmv17qlzpzi44b7917dzd6djnhrp8jcyqai99yzz46nx2")))

(define-public crate-ul-1.1.2 (c (n "ul") (v "1.1.2") (d (list (d (n "ul-sys") (r "^1.1.1") (d #t) (k 0)))) (h "10k0aw03zs4k8aibds9mw7v5ns4q3ahl2hzfh7xigz7li2nsrhph")))

(define-public crate-ul-1.1.3 (c (n "ul") (v "1.1.3") (d (list (d (n "ul-sys") (r "^1.1.1") (d #t) (k 0)))) (h "04cqappzwp0kckmyn3f71j69dwrzhf9nkp241z7753sxr31pjksb")))

(define-public crate-ul-1.1.4 (c (n "ul") (v "1.1.4") (d (list (d (n "ul-sys") (r "^1.1.2") (d #t) (k 0)))) (h "0qbwabxqizpighl2w79yxqya4i3pk0h0gxn8k9839lvjsbgk1d0c")))

