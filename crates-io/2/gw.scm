(define-module (crates-io #{2}# gw) #:use-module (crates-io))

(define-public crate-gw-0.1.3 (c (n "gw") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)))) (h "19zdq30zhkkhsri0agjrvm5108pa5da7gzmpdijpvrs1s67gkwnh")))

(define-public crate-gw-0.2.0 (c (n "gw") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)))) (h "04mnsh1g0fl59clgjdxqx5s7jdnk1gzpxgbd75w886ny4zhdknfa")))

