(define-module (crates-io #{2}# bf) #:use-module (crates-io))

(define-public crate-bf-0.2.2 (c (n "bf") (v "0.2.2") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0s9disrbrigvgz6808gcgnwjgy6ym4qxmhjhi846v1havfljqrq5") (f (quote (("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.2.5 (c (n "bf") (v "0.2.5") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0q9hg0l95nqh2sz1rympjclhapwgi1cnxanbbhkimjlbz206znr3") (f (quote (("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.2.6 (c (n "bf") (v "0.2.6") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "02mx645dwi9q6fr4p933wq3da43277r1pj9vmv3vv97mbm61yl7s") (f (quote (("u32count") ("u16count") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.3.0 (c (n "bf") (v "0.3.0") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1x2hajhvz25vgfi7gagh73syni7y3gn7s263mvgwsi3i1kppq84c") (f (quote (("u32count") ("u16count") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.3.1 (c (n "bf") (v "0.3.1") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0afzqwwxrjij7n469ma0cpf7wddgjb083c5mp5j7kfcm6rhyql8f") (f (quote (("u32count") ("u16count") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.3.2 (c (n "bf") (v "0.3.2") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0qqja5sjcs37xd92x0s7n4bla7fg301gzgbq6fjj0s3q293iq70j") (f (quote (("u32count") ("u16count") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.3.3 (c (n "bf") (v "0.3.3") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0p3rwh9z5s93bxxzj4zrpf8y1i5r790d3qbqs02hpg9zdxmfwv51") (f (quote (("u32count") ("u16count") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.3.4 (c (n "bf") (v "0.3.4") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0qsp9qb841kg8dlzhb7cd2d9rygr5axa4vxmpsfzd3f936bwjkqg") (f (quote (("u32count") ("u16count") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.4.0 (c (n "bf") (v "0.4.0") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "002vhlk34h2nxzg042827sc5dmh59qrlqimrjj8w2x53n2ixddc8") (f (quote (("u32count") ("u16count") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.4.1 (c (n "bf") (v "0.4.1") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1x2d3sffgc65r2rfaw1pbd9czb61nkbb916mgqv7pq0s2cvfcvjx") (f (quote (("u32count") ("u16count") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.4.2 (c (n "bf") (v "0.4.2") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1rwgh8qnx5ii9h99jacvqmmislbsnkymvdpvd7y79ahblkxqx28v") (f (quote (("u32count") ("u16count") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.4.3 (c (n "bf") (v "0.4.3") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ad66c5y98x4crswyskizrga6ky4h2y30235nhb3bvvv4i0jr0df") (f (quote (("u32count") ("u16count") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.4.4 (c (n "bf") (v "0.4.4") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "llvm-sys") (r "^38") (o #t) (d #t) (k 0)))) (h "1y56h1zp96mrlw8y0if9bh104990zhnmlmnlks068g1sqawhwc2g") (f (quote (("u32count") ("u16count") ("llvm" "llvm-sys") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.4.5 (c (n "bf") (v "0.4.5") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "dynasm") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "llvm-sys") (r "^38") (o #t) (d #t) (k 0)))) (h "1dqv4ryrzc7i0pzpdd5735vzrqzfcvp3a449xpy025910sbkirhl") (f (quote (("u32count") ("u16count") ("llvm" "llvm-sys") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.4.6 (c (n "bf") (v "0.4.6") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "dynasm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "llvm-sys") (r "^38") (o #t) (d #t) (k 0)))) (h "1fmd68758p8iys179ncfhb0c7czjs2q194pmabj4wcv2a41giqq8") (f (quote (("u32count") ("u16count") ("llvm" "llvm-sys") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.4.7 (c (n "bf") (v "0.4.7") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "dynasm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "llvm-sys") (r "^38") (o #t) (d #t) (k 0)))) (h "19l9kagazwc9x8c6a7rvkvdanqk64idb3w3sr11kv48ivzisijkv") (f (quote (("u32count") ("u16count") ("llvm" "llvm-sys") ("jit" "dynasmrt" "dynasm"))))))

(define-public crate-bf-0.4.8 (c (n "bf") (v "0.4.8") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "dynasm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "llvm-sys") (r "^38") (o #t) (d #t) (k 0)))) (h "0rdiin4d6wl7w3kpv93vbz31s1lqqvhx6h450c3wjqqgnbj9pkca") (f (quote (("u32count") ("u16count") ("llvm" "llvm-sys") ("jit" "dynasmrt" "dynasm"))))))

