(define-module (crates-io #{2}# na) #:use-module (crates-io))

(define-public crate-na-0.1.0 (c (n "na") (v "0.1.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.11") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1.4") (d #t) (k 0)))) (h "1akpvr9x2q8q5hmqx5kzz5i335bqlnqwn7j7vzcgwlllwibi4sxd")))

(define-public crate-na-0.12.0 (c (n "na") (v "0.12.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1.4") (d #t) (k 0)))) (h "0yp6h7biyfs576fg1z96wb2khwpj9ivz80awja9wb3lblbkyjpqp")))

(define-public crate-na-0.13.0 (c (n "na") (v "0.13.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "01snms0x5005nm5j957fippn9rmj3zqwha8ck96jm31h9na1x5la") (f (quote (("unstable"))))))

(define-public crate-na-0.14.0 (c (n "na") (v "0.14.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1ym2w1avwmpjn63vn765k6finspps7s3g6x0ihfrsanbkmx2h56i") (f (quote (("unstable"))))))

(define-public crate-na-0.14.2 (c (n "na") (v "0.14.2") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0nm4dz6zppl3dwcm72zw04f6z4hfskw7qr0zkl917bahjjxhq264") (f (quote (("unstable"))))))

(define-public crate-na-0.14.3 (c (n "na") (v "0.14.3") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1h2kjfxd0hjg790a4nw80sirgyy36220j562p8jy8s2yjsc9f806") (f (quote (("unstable"))))))

(define-public crate-na-0.14.4 (c (n "na") (v "0.14.4") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "19szddwpjwpxipy2m7ra7m3y9cbd9fwcz11cdbrij5shmriafrnq") (f (quote (("unstable"))))))

(define-public crate-na-0.15.0 (c (n "na") (v "0.15.0") (d (list (d (n "alga") (r "^0.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1p8gl44b5rg8va0v3bhzmqwmsmxidrp95vx26rixb8shw4xilgsl") (f (quote (("unstable"))))))

(define-public crate-na-0.16.0 (c (n "na") (v "0.16.0") (d (list (d (n "alga") (r "^0.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1l6wmm2qfag5fj1d4yjx2pmc2avh160xvwwfp8wr5fwx48wbfbai") (f (quote (("unstable"))))))

(define-public crate-na-0.16.4 (c (n "na") (v "0.16.4") (d (list (d (n "alga") (r "^0.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1cyi5r3yb2xv26l72ywv70xczgz08v4dmraa4wrz3ljcjl9730hi") (f (quote (("unstable"))))))

(define-public crate-na-0.16.5 (c (n "na") (v "0.16.5") (d (list (d (n "alga") (r "^0.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "05wcny423xsak8gqg6888cvhhld4bvwxxznv6k1dl5wg844pa4ab") (f (quote (("unstable"))))))

(define-public crate-na-0.16.6 (c (n "na") (v "0.16.6") (d (list (d (n "alga") (r "^0.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1v1r4b2r4maqdna4g4z5nrpxdwh9sfz2ixlndqcbdyanm5z9lr8f") (f (quote (("unstable") ("serde-serialize" "nalgebra/serde-serialize"))))))

(define-public crate-na-0.17.0 (c (n "na") (v "0.17.0") (d (list (d (n "alga") (r "^0.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1mrqr2kvp86slmi0mymnq5ixirf48mcxibbq4zqfifyl0x56sg39") (f (quote (("unstable") ("serde-serialize" "nalgebra/serde-serialize"))))))

(define-public crate-na-0.18.0 (c (n "na") (v "0.18.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0j8gwcz9cyc2jf974482f7absrdfs3y078fqp6kjq1hg03wagxq8") (f (quote (("unstable") ("serde-serialize" "nalgebra/serde-serialize"))))))

(define-public crate-na-0.18.1 (c (n "na") (v "0.18.1") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "096i7y1ngh29rih9i7nn9hjyl41b9wnssvbilwf53b20irylhwbs") (f (quote (("unstable") ("serde-serialize" "nalgebra/serde-serialize"))))))

(define-public crate-na-0.18.2 (c (n "na") (v "0.18.2") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1rcjwisbxj105dm92zsy20x77kdm6x6lybpj4mkz4z0r7fjcg6pa") (f (quote (("unstable") ("serde-serialize" "nalgebra/serde-serialize"))))))

(define-public crate-na-0.18.3 (c (n "na") (v "0.18.3") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1srkp9h377yxn14h5rky46xkrm06nc7z7413jbbxsiaipm7hk21m") (f (quote (("unstable") ("serde-serialize" "nalgebra/serde-serialize"))))))

(define-public crate-na-0.19.0 (c (n "na") (v "0.19.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0adil3vavzxyflirl0n6wgldqkcinz9j726j7969q7q8ld0nlqm9") (f (quote (("unstable") ("serde-serialize" "nalgebra/serde-serialize"))))))

(define-public crate-na-0.20.0 (c (n "na") (v "0.20.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "nalgebra") (r "^0.20") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1gxrfcs4czx9qljlhg2xi1i9pbjwhnvzfyz4c4d36fvw2gq888ds") (f (quote (("unstable") ("serde-serialize" "nalgebra/serde-serialize"))))))

(define-public crate-na-0.20.1 (c (n "na") (v "0.20.1") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "nalgebra") (r "^0.20") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0fbv3pr3xm5jdzcg69z3gpdhr0r4hr89i3qrs89q5gwms1m2zria") (f (quote (("unstable") ("serde-serialize" "nalgebra/serde-serialize"))))))

(define-public crate-na-0.21.0 (c (n "na") (v "0.21.0") (d (list (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.1") (d #t) (k 0)))) (h "117019m5h8mz9g4zdgm4i262fv0zhrrlv574yd37jgkl7ns6vxzz") (f (quote (("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("alga" "nalgebra/alga"))))))

(define-public crate-na-0.21.1 (c (n "na") (v "0.21.1") (d (list (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.1") (d #t) (k 0)))) (h "0vl4bpspwc8jspnc88djipa5y70613csd04almplfhrvq2gxgl2q") (f (quote (("wide" "simba/wide") ("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("packed_simd" "simba/packed_simd") ("alga" "nalgebra/alga"))))))

(define-public crate-na-0.22.0 (c (n "na") (v "0.22.0") (d (list (d (n "nalgebra") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.2") (d #t) (k 0)))) (h "094r6xp6g7pnjbapqw8pb5qybsj1nnlxlh1d4srgfsgwpdc974gi") (f (quote (("wide" "simba/wide") ("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("packed_simd" "simba/packed_simd") ("alga" "nalgebra/alga"))))))

(define-public crate-na-0.23.0 (c (n "na") (v "0.23.0") (d (list (d (n "nalgebra") (r ">=0.23.0, <0.24.0") (d #t) (k 0)) (d (n "num-traits") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "simba") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "15mvs271sl8347all6pz1y4i30ln7ysppzq5xlaimij5lfdp9qv0") (f (quote (("wide" "simba/wide") ("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("packed_simd" "simba/packed_simd") ("alga" "nalgebra/alga"))))))

(define-public crate-na-0.24.0 (c (n "na") (v "0.24.0") (d (list (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.3") (d #t) (k 0)))) (h "0d14qjp8d77cg2r4cyigryir6mpgxhigfkr90ra2qv331dny4rjp") (f (quote (("wide" "simba/wide") ("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("packed_simd" "simba/packed_simd") ("alga" "nalgebra/alga"))))))

(define-public crate-na-0.30.0 (c (n "na") (v "0.30.0") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.7") (d #t) (k 0)))) (h "1iplhvqas507aj05ng6qkrgyw385lg2xg2pidc14wb7gdmdz6k75") (f (quote (("wide" "simba/wide") ("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("packed_simd" "simba/packed_simd") ("alga" "nalgebra/alga"))))))

(define-public crate-na-0.30.1 (c (n "na") (v "0.30.1") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.7") (d #t) (k 0)))) (h "103nva20zilbr6igbrndhmx27licbx9ard78zqxsgcwl8vam5cgy") (f (quote (("wide" "simba/wide") ("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("packed_simd" "simba/packed_simd") ("alga" "nalgebra/alga"))))))

(define-public crate-na-0.31.0 (c (n "na") (v "0.31.0") (d (list (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.7") (d #t) (k 0)))) (h "0s06m1f1kmbsha6zbs75pffg9gbq47f1cv1jjc2wlw0krqinyak3") (f (quote (("wide" "simba/wide") ("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("packed_simd" "simba/packed_simd") ("alga" "nalgebra/alga"))))))

(define-public crate-na-0.32.0 (c (n "na") (v "0.32.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.8") (d #t) (k 0)))) (h "03p2kv7dk9mbap7v0mlmgax2xz49850m2v4g76hzs1y83lq7m0df") (f (quote (("wide" "simba/wide") ("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("packed_simd" "simba/packed_simd") ("alga" "nalgebra/alga"))))))

(define-public crate-na-0.31.1 (c (n "na") (v "0.31.1") (d (list (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.7") (d #t) (k 0)))) (h "077bg7d6sf5yni948c7dsxbp9w5vkml462srvbxrb7yr438413ds") (f (quote (("wide" "simba/wide") ("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("packed_simd" "simba/packed_simd") ("alga" "nalgebra/alga"))))))

(define-public crate-na-0.32.1 (c (n "na") (v "0.32.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.8") (d #t) (k 0)))) (h "0jvbv4rgmfflv7002cbnc8g61p05i9d9idmc6m2b4d42jdpvalm8") (f (quote (("wide" "simba/wide") ("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("packed_simd" "simba/packed_simd") ("alga" "nalgebra/alga"))))))

(define-public crate-na-0.32.2 (c (n "na") (v "0.32.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.8") (d #t) (k 0)))) (h "17wc2k7pk9zm2jdns1shp1fxxd2li1fqk8r9avyfw9gwjwa9apnm") (f (quote (("wide" "simba/wide") ("unstable") ("serde-serialize" "nalgebra/serde-serialize") ("packed_simd" "simba/packed_simd") ("bytemuck" "nalgebra/bytemuck") ("alga" "nalgebra/alga"))))))

