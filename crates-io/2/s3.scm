(define-module (crates-io #{2}# s3) #:use-module (crates-io))

(define-public crate-s3-0.0.0 (c (n "s3") (v "0.0.0") (h "1qvfxhrivr7mbdimrc89gzv0ldp3nfm4ycyy8zbhcbfnmsbpdwvn")))

(define-public crate-s3-0.1.0 (c (n "s3") (v "0.1.0") (h "1gmhwwbj18cv9h6rd72fbg6ya6qz1842fgfxn4bjpk1ki3m5s12p")))

