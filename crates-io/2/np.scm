(define-module (crates-io #{2}# np) #:use-module (crates-io))

(define-public crate-np-0.1.0 (c (n "np") (v "0.1.0") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0kqwglcqqr9ygqwwz8zxabdrfldyxxn6n8gx0aiv36yklw6gkm11")))

(define-public crate-np-0.1.1 (c (n "np") (v "0.1.1") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0x00887i46sw9zm12dw4s3qjnb2zjsq9x04wm9xlm94v4dkspqn1")))

(define-public crate-np-0.1.2 (c (n "np") (v "0.1.2") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0k00kv8c9lkp2db4fl4bnnmmqxdhmggfslkr9j063mkn35902z6p")))

(define-public crate-np-2018.3.2 (c (n "np") (v "2018.3.2") (h "1519nhah650ani2a9a6yk8vhnpq3ja2ir2bf7ldgs78i1riaidc1") (y #t)))

(define-public crate-np-0.1.4 (c (n "np") (v "0.1.4") (h "1bb6h90h9zcn1vbqjh7bxqklsqi3wvvpdbs0d1p24x3n9pa2nkfx")))

(define-public crate-np-0.1.5 (c (n "np") (v "0.1.5") (h "0ixmamsqf0agpzffwhivs91jhgiq66x0m8jmmaf6zwi2vs1f6pai")))

(define-public crate-np-2019.3.1 (c (n "np") (v "2019.3.1") (h "1dlxjivljb27v0x1kd3fy1cfxwr6lxmmjmcv8wi0j4sxcicc0pkz")))

(define-public crate-np-2019.3.2 (c (n "np") (v "2019.3.2") (h "170xsms32r27ii1iswlfmph2x9zxs9n7b4xmdm5b8acsl31dpfa1")))

(define-public crate-np-2019.3.3 (c (n "np") (v "2019.3.3") (h "1y535r27kwb6g616icgna2h9k3z44x0qjls9n2vprlzf50ly306f")))

(define-public crate-np-2019.3.12 (c (n "np") (v "2019.3.12") (h "13wlxy9hzlimpafvsgy7lhf1v0c8gww7w91brr3xb1wzv3ad3b9w")))

(define-public crate-np-2019.3.13 (c (n "np") (v "2019.3.13") (h "0p9am2pbqjyvqzky8092adahz2pp3h9sx5aa3624qc8w4wd2wwbp")))

