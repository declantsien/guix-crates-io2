(define-module (crates-io #{2}# bw) #:use-module (crates-io))

(define-public crate-bw-0.0.1 (c (n "bw") (v "0.0.1") (h "0fc1l7bfh43xzc40whwyp941kd5yh5nxdc6bkb42pxxdkd2fzbxd")))

(define-public crate-bw-0.0.2 (c (n "bw") (v "0.0.2") (h "12yzf7yx8ixmiyd8xn42hi36aa5kxirrcnca2xdfrcpzd7f2jajh")))

