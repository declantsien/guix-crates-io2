(define-module (crates-io #{2}# re) #:use-module (crates-io))

(define-public crate-re-0.1.2 (c (n "re") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1ad46h0zbdh1g685wgk0i4m085nxwsf46w88s22pqw2rb40gs1ja")))

(define-public crate-re-0.1.3 (c (n "re") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1wcz412c11n86qndmy790v52h9lm2i1fd8vd0fhbk7ag2mpa8qq3")))

(define-public crate-re-0.1.4 (c (n "re") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1bplwnl9r3c4p2gf60b2w528zzz6fdhyk9v50j70x9nbh1vp90kw")))

(define-public crate-re-0.1.5 (c (n "re") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sonic-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "17j34d6fvswvm4py76wyhz9fcxmbnrk1zj26rwqy9377r6218vr0")))

