(define-module (crates-io #{2}# xf) #:use-module (crates-io))

(define-public crate-xf-0.1.0 (c (n "xf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "shell-words") (r "^1.1") (d #t) (k 0)))) (h "1phgx2kdkgg96r0gn3md1xm3yfz17syz0cgbc9j0rzdpb6c46dbs")))

(define-public crate-xf-0.2.0 (c (n "xf") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "shell-words") (r "^1.1") (d #t) (k 0)))) (h "03aix5cwr2njm249icndwgw0rv5ssgn82p6v1xbawzfh343vc243")))

(define-public crate-xf-0.6.0 (c (n "xf") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "00slpid9lmnjf94r5954cmd74ngdwkwfm2fc314w3gcx1pgn3r8k")))

(define-public crate-xf-0.7.0 (c (n "xf") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0x4k9af0jwjp08a71yvhf3kphysg5s1r7xzn6kcq186gs3pv711h")))

