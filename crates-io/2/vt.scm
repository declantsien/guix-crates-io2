(define-module (crates-io #{2}# vt) #:use-module (crates-io))

(define-public crate-vt-0.1.0 (c (n "vt") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "1j4s4p4g2ax0kx5mgldb768kdnzfspbxslmby8qdbvhyy6a5lgax")))

(define-public crate-vt-0.1.1 (c (n "vt") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "1jx4aiq9slq7fqjvpz8kl79pf6g4qgkajg2fyi69malri8nkr1aw")))

(define-public crate-vt-0.1.2 (c (n "vt") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "0hqgj7pq0c98r45zpg93jn0vzqlw6acj7f3hq7gadsiclvdacgvh")))

(define-public crate-vt-0.1.3 (c (n "vt") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "0b3qzbzwh3sgzncglrv0b1dgfd85rkbb71xl30xcicfx42msmf05")))

(define-public crate-vt-0.2.0 (c (n "vt") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "02ab18nz3718vj4d2sm5j4xs4vz8gf6p5z9lxjxdb90gpzwpkag4")))

(define-public crate-vt-0.2.1 (c (n "vt") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "1pf2sm52dp6vj620z1cnm61lx5b17a52kwvfhvg0qg8p5mik0pb8")))

