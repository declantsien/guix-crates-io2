(define-module (crates-io #{2}# sn) #:use-module (crates-io))

(define-public crate-sn-0.1.0 (c (n "sn") (v "0.1.0") (h "1rk1wjvch82fnqr9ymlpwhxkmbi0f1a8xlvd94vr53izcy7k0dx7") (y #t)))

(define-public crate-sn-0.1.1 (c (n "sn") (v "0.1.1") (h "0vvsdl1sj6jhvgryxq1sdv8llghcjqdsbmss32vv9sngnbdnjp1z")))

(define-public crate-sn-0.1.2 (c (n "sn") (v "0.1.2") (h "0yv4hhw88jnxb565v0ss8vc32177d0f3m75ihs3msl799vl6mavg")))

