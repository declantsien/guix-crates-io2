(define-module (crates-io #{2}# ox) #:use-module (crates-io))

(define-public crate-ox-0.0.1 (c (n "ox") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "docopt") (r "^0.6.82") (d #t) (k 0)) (d (n "docopt_macros") (r "^0.6.85") (d #t) (k 0)) (d (n "peg") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1h824lp917044iy5hk5xbi24v1ljnbf56ihdbihkpryka5sczjxf") (y #t)))

