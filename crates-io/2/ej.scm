(define-module (crates-io #{2}# ej) #:use-module (crates-io))

(define-public crate-ej-0.0.1 (c (n "ej") (v "0.0.1") (h "0rlb4jaypsnzm8j21h03aqv91c404hjyg9a30k9snz59xc38bmag")))

(define-public crate-ej-0.0.2 (c (n "ej") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "polars") (r "^0.39") (f (quote ("lazy" "parquet" "csv"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "09jkmsfgzby4h4cnqj9wc4nn2rsj6kwb7vxp5mgjv4x93yl15gc2")))

(define-public crate-ej-0.0.3 (c (n "ej") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "polars") (r "^0.39") (f (quote ("lazy" "parquet"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0x3z6ab8hm3ydc8hyrh0af0x1j1a6vpr264dnzbl8hizblm5v296")))

