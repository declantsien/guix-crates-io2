(define-module (crates-io #{2}# uy) #:use-module (crates-io))

(define-public crate-uy-0.1.0 (c (n "uy") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)))) (h "0mmn7x94awgqynmjqjsl4h1b8mc6bkjhs5aw974f87a910zjnxc8")))

(define-public crate-uy-0.1.1 (c (n "uy") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)))) (h "0565a28kv9w28abl8736hr5yapzkgmwm2jcvsfxjafc4z9bfh3m0")))

(define-public crate-uy-0.1.2 (c (n "uy") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)))) (h "10w2x4l2qir3bk7vy8ibfg429jw7yfw7d0bwjr979hmq326lrb7q")))

