(define-module (crates-io #{2}# a4) #:use-module (crates-io))

(define-public crate-a4-0.0.1 (c (n "a4") (v "0.0.1") (d (list (d (n "anachro-forth-core") (r "^0.0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "kolben") (r "^0.0.3") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "rzcobs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qr9igl3jvsfdc2kr7mjsk8nrwzh0516dd5dn7c9sb2kwp2y882l")))

(define-public crate-a4-0.0.2 (c (n "a4") (v "0.0.2") (d (list (d (n "a4-core") (r "^0.0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "kolben") (r "^0.0.3") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "rzcobs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1ica8fybs1pank66n9ap1jpxnwv8gcdgknx0hd6k7793pm3gp0p0")))

(define-public crate-a4-0.0.4 (c (n "a4") (v "0.0.4") (d (list (d (n "a4-core") (r "^0.0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "kolben") (r "^0.0.3") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "rzcobs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "018jpc3x8zwh6s7x1rbv040928sh0g9xdfygg9v62p17skssjv73")))

