(define-module (crates-io #{2}# cf) #:use-module (crates-io))

(define-public crate-cf-0.0.1 (c (n "cf") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0pz0l3sna6lvnpvhr22m3886vg3iwfza6cm8agbbiqxq26xdynnv")))

(define-public crate-cf-0.0.2 (c (n "cf") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0a1fly4nqr0p6bdy00l8jvpn0qpmiryp1rcfrl9zvvvbwys84fzi")))

(define-public crate-cf-0.0.3 (c (n "cf") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "172zbyhffvbf2dk2lwpvpfrci2sskkswifaiwcdal4g4cmg5pi18")))

(define-public crate-cf-0.0.4 (c (n "cf") (v "0.0.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "15hv7n9g93f9hyfgw7saqx25c36w2v36vrly5mlsdb8y3ik2a52q")))

(define-public crate-cf-0.0.5 (c (n "cf") (v "0.0.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1pn2k18nla8sj1in11hm8k79k3c42sx9qsac666ymwkps0ck5sbh")))

(define-public crate-cf-0.0.6 (c (n "cf") (v "0.0.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0xrwqg9qii6jqwfllnkysq5x9cc4r1jhqaba7d1p2pjsxwszz41y")))

(define-public crate-cf-0.0.7 (c (n "cf") (v "0.0.7") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1vd0xnqm6ap7hr5pzn1f7h09z5mwcky429rfkxpi4n99z7zacqny")))

(define-public crate-cf-0.0.9 (c (n "cf") (v "0.0.9") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "07v1yn47qyhwjfrdyv131s4k358w01vl59pn6cw4phdjcj3clas9")))

(define-public crate-cf-0.0.10 (c (n "cf") (v "0.0.10") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pq3gri1llrpscnbf8gm5mk3f7zfa75kiwqfiqb7sb3k7c3kwi72")))

