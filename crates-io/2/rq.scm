(define-module (crates-io #{2}# rq) #:use-module (crates-io))

(define-public crate-rq-0.1.0 (c (n "rq") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "1ll3qqvqdqwb7ylwp3ldriwqx2p6qbpz40hcq891q4jhr0vpnlzc")))

(define-public crate-rq-0.1.1 (c (n "rq") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.35") (d #t) (k 1)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "rq_derive") (r "^0.1.0") (d #t) (k 0)))) (h "049m94l1yimp4psjnw3f70kvn6h2myf1v3wqbvw2x9ls1dj45sb9")))

(define-public crate-rq-0.1.2 (c (n "rq") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.35") (d #t) (k 1)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "rq_derive") (r "^0.1.2") (d #t) (k 0)))) (h "1xhcdaz4fdqzicjlbiaxdbs8zr0bx9831znm176kpsgn13m6imgw")))

