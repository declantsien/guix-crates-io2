(define-module (crates-io #{2}# sm) #:use-module (crates-io))

(define-public crate-sm-0.1.0 (c (n "sm") (v "0.1.0") (h "165lb89qg1jrxwl86sq60rrmfma8667mafqpdkl5a1kwy7bna2d7")))

(define-public crate-sm-0.2.0 (c (n "sm") (v "0.2.0") (h "0bnq03vmy861xnx8gx3aw1s79wx5019dxydik8jw7fys99mawpd1")))

(define-public crate-sm-0.3.0 (c (n "sm") (v "0.3.0") (h "04qzsxyr9z49gckp7nbvd046r6ynp9xahqpd0j2rji1cpj5wn79h")))

(define-public crate-sm-0.4.0 (c (n "sm") (v "0.4.0") (d (list (d (n "cargo-readme") (r "^3.0") (d #t) (k 2)) (d (n "cargo-release") (r "^0.10") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1qymg4r5nhddblkqh1ga6rm2vw7jw0j8fi3lbljnjabbdw7k6zy7")))

(define-public crate-sm-0.5.0 (c (n "sm") (v "0.5.0") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (f (quote ("real_blackbox"))) (d #t) (k 2)))) (h "0w1nb0f8r5fvcdpr8v31n9c5kx61s5ggwwcjkz4aan4hzcfiq7fm")))

(define-public crate-sm-0.6.1 (c (n "sm") (v "0.6.1") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (f (quote ("real_blackbox"))) (d #t) (k 2)) (d (n "sm_macro") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0m1i9i6ai8bg936awm7jj5z0nlqb9cxzv5dy19bvb99cccra23mf") (f (quote (("macro" "sm_macro") ("default" "macro"))))))

(define-public crate-sm-0.7.0 (c (n "sm") (v "0.7.0") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (f (quote ("real_blackbox"))) (d #t) (k 2)) (d (n "sm_macro") (r "^0.7") (o #t) (d #t) (k 0)))) (h "00sj8z2jm2j1jgjcpa0r2xvva5bdyab3xa8k91d0qk759kqxc33a") (f (quote (("macro" "sm_macro") ("default" "macro"))))))

(define-public crate-sm-0.9.0 (c (n "sm") (v "0.9.0") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "sm_macro") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "sm_macro") (r "^0.9") (d #t) (k 2)))) (h "0sapiv8s338plja6n00cm4cikl8hrypp6irjli2i0h8scprs7r6q") (f (quote (("macro" "sm_macro") ("default" "macro"))))))

