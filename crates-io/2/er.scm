(define-module (crates-io #{2}# er) #:use-module (crates-io))

(define-public crate-er-0.1.0 (c (n "er") (v "0.1.0") (h "106l2bfizjyi3svx9nhw8463qgbrq5fn62p3a6kd2wyfw5m55s7k")))

(define-public crate-er-0.2.0 (c (n "er") (v "0.2.0") (h "00agdzs6nlib4w96sazy03yswa2vr6vnm3yswvdpfklcsmzhjiyf")))

(define-public crate-er-0.0.0 (c (n "er") (v "0.0.0") (h "1pgr4afbh3j5fqi0y3ayycq4lxlck0cvxj3rmnmb18bizb1znscw")))

(define-public crate-er-0.2.1 (c (n "er") (v "0.2.1") (h "0zklz2w3f0q7a1sqazk2rjcs46x9lzmj109asxxlr5xw40jpkws1")))

