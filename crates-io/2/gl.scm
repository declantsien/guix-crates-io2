(define-module (crates-io #{2}# gl) #:use-module (crates-io))

(define-public crate-gl-0.0.2 (c (n "gl") (v "0.0.2") (d (list (d (n "gl_generator") (r "^0.0.4") (d #t) (k 0)))) (h "1c23glrzwiy3sd20pk6asz0cmy6mr6b57dwzasyv44k8xj1cznim")))

(define-public crate-gl-0.0.3 (c (n "gl") (v "0.0.3") (d (list (d (n "gl_common") (r "^0.0.3") (d #t) (k 0)) (d (n "gl_generator") (r "^0.0.5") (d #t) (k 1)))) (h "1xv9bw2i3xmqcvy43cj21wpw5bh343qhsrj2y28z9n6csjk1cgsk")))

(define-public crate-gl-0.0.4 (c (n "gl") (v "0.0.4") (d (list (d (n "gl_common") (r "^0.0.3") (d #t) (k 0)) (d (n "gl_generator") (r "^0.0.7") (d #t) (k 1)))) (h "10q5hgg54iiycy17mfkg63zp4q8mxx9y4j51y13r0bvc20r6krb3")))

(define-public crate-gl-0.0.5 (c (n "gl") (v "0.0.5") (d (list (d (n "gl_common") (r "^0.0.3") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)))) (h "05djzajrzghp5c951vdwqj0b24r97kbpvrq9i7k5in9vq751m4am")))

(define-public crate-gl-0.0.6 (c (n "gl") (v "0.0.6") (d (list (d (n "gl_common") (r "^0.0.3") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.5") (d #t) (k 1)))) (h "1mix53rks9izrc9hs24m5d2iv62vs0nrsxi4jg8kwp06m5jz7k2d")))

(define-public crate-gl-0.0.7 (c (n "gl") (v "0.0.7") (d (list (d (n "gl_common") (r "^0.0.3") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.5") (d #t) (k 1)))) (h "1hxmndmy3lsm5gjd5jgalwr2wbqsxx3hidxa1lv806gcb730sxqy")))

(define-public crate-gl-0.0.8 (c (n "gl") (v "0.0.8") (d (list (d (n "gl_common") (r "^0.0.4") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.5") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ms9jck0l1ri4pg2h25flx0jigk91zsj3miqvjx1jwh8lxsgwbc1")))

(define-public crate-gl-0.0.9 (c (n "gl") (v "0.0.9") (d (list (d (n "gl_common") (r "^0.0.4") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.5") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0m0p42dnp3372aij5hj98lqrn6i72jbaav5sc3zxmnsyzfrhbpq3")))

(define-public crate-gl-0.0.10 (c (n "gl") (v "0.0.10") (d (list (d (n "gl_common") (r "^0.0.4") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.5") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0h5amb2mhwwwxd00cjjbiybyfjfkgd9pvr1v4aplmiixwd03z9zz")))

(define-public crate-gl-0.0.11 (c (n "gl") (v "0.0.11") (d (list (d (n "gl_common") (r "^0.0.4") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.5") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0l35xj21wddywxsfw2lcdixyr8m1ag9zp9r4hnj9pa0klhvby1yl")))

(define-public crate-gl-0.0.12 (c (n "gl") (v "0.0.12") (d (list (d (n "gl_common") (r "^0.0.4") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.5") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1m7dddcf02z5rsrbd3073y2qmn1cg8b7k3xh2havmq9ww8yi8j08")))

(define-public crate-gl-0.1.0 (c (n "gl") (v "0.1.0") (d (list (d (n "gl_common") (r "^0.1.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.1.0") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.8") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0l3bmb2wdp5ms10xvp35320j5741r07dxzwrifmjdwrlw7wx9d43")))

(define-public crate-gl-0.3.0 (c (n "gl") (v "0.3.0") (d (list (d (n "gl_generator") (r "^0.2.0") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.8") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "046mibg6qihk2q7hlaah1i2i3cmsj8qba3fq8ssbn1ymcwc0aidx")))

(define-public crate-gl-0.4.0 (c (n "gl") (v "0.4.0") (d (list (d (n "gl_generator") (r "^0.3.0") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.8") (d #t) (k 1)))) (h "14pad1s22kyy8j949g522jqbpcwhi0g3c73yr4fym8iyiirgi3wd")))

(define-public crate-gl-0.5.0 (c (n "gl") (v "0.5.0") (d (list (d (n "gl_generator") (r "^0.4.0") (d #t) (k 1)) (d (n "glutin") (r "^0.4") (d #t) (k 2)) (d (n "khronos_api") (r "^0.0.8") (d #t) (k 1)))) (h "0shj6z2vf2f0c8mdi8im1kkmb00wchsrb2dzhkqr3hhbbl7msqcl")))

(define-public crate-gl-0.5.1 (c (n "gl") (v "0.5.1") (d (list (d (n "gl_generator") (r "^0.4.0") (d #t) (k 1)) (d (n "glutin") (r "^0.4") (d #t) (k 2)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)))) (h "0cyk6xwcp1byhijanq76ak0mxi7mn6hi72zhim17bhjkk0z80nky")))

(define-public crate-gl-0.5.2 (c (n "gl") (v "0.5.2") (d (list (d (n "gl_generator") (r "^0.4.2") (d #t) (k 1)) (d (n "glutin") (r "^0.4") (d #t) (k 2)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)))) (h "1a1xjar29zpw0lksqcskcf1x04zzs230x5qwchiigkpfriwykss9")))

(define-public crate-gl-0.6.0 (c (n "gl") (v "0.6.0") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.4") (d #t) (k 2)))) (h "04pagdy46lz8qpw5vygq04zxy1qls9ny7vhpz5jw1rfc3rwkhn7x")))

(define-public crate-gl-0.6.1 (c (n "gl") (v "0.6.1") (d (list (d (n "gl_generator") (r "^0.5.2") (d #t) (k 1)) (d (n "glutin") (r "^0.4") (d #t) (k 2)))) (h "1w72nnxn7bpbh0rvgnamp6c18849ky0v1mplpgxcg9crv8sx00d2")))

(define-public crate-gl-0.6.2 (c (n "gl") (v "0.6.2") (d (list (d (n "gl_generator") (r "^0.5.3") (d #t) (k 1)) (d (n "glutin") (r "^0.7") (d #t) (k 2)))) (h "0gpb8ca3rmmdw9d0a253dvssy7ki81wv0z7fwswkh0w3fdrv48w9")))

(define-public crate-gl-0.6.3 (c (n "gl") (v "0.6.3") (d (list (d (n "gl_generator") (r "^0.5.4") (d #t) (k 1)) (d (n "glutin") (r "^0.7") (d #t) (k 2)))) (h "0ar0lij26h9n76byy2j6k92lya7q0vcm2rhmnzn7g4z3ngzkqm4p")))

(define-public crate-gl-0.6.4 (c (n "gl") (v "0.6.4") (d (list (d (n "gl_generator") (r "^0.6.0") (d #t) (k 1)) (d (n "glutin") (r "^0.10.0") (d #t) (k 2)))) (h "037bw9w9944vvn586ci2b5cxjc0lfqqd54lw27fj209wa82hg3cd") (y #t)))

(define-public crate-gl-0.6.5 (c (n "gl") (v "0.6.5") (d (list (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.10.0") (d #t) (k 2)))) (h "0hdl3p3lyr950wp37az3q0calpxs8p36pim66jdha0jz506bjwqw")))

(define-public crate-gl-0.7.0 (c (n "gl") (v "0.7.0") (d (list (d (n "gl_generator") (r "^0.7.0") (d #t) (k 1)) (d (n "glutin") (r "^0.10.0") (d #t) (k 2)))) (h "0d22riycbn0q42swr624ix2qwgy21p7aqzmp3amkps2gqs6awq06")))

(define-public crate-gl-0.9.0 (c (n "gl") (v "0.9.0") (d (list (d (n "gl_generator") (r "^0.8.0") (d #t) (k 1)) (d (n "glutin") (r "^0.10.0") (d #t) (k 2)))) (h "0dma2b6yn6fr63yi22b86prajdnsp8vik6pqid8cp51p7x584rw0")))

(define-public crate-gl-0.10.0 (c (n "gl") (v "0.10.0") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "glutin") (r "^0.10.0") (d #t) (k 2)))) (h "1jkslnd097dwmn3p0nxa0hc7c2l3a4f58j7b6nsxa2li0aw7nic1")))

(define-public crate-gl-0.11.0 (c (n "gl") (v "0.11.0") (d (list (d (n "gl_generator") (r "^0.10.0") (d #t) (k 1)) (d (n "glutin") (r "^0.18.0") (d #t) (k 2)))) (h "1wcqpyhck0xriffkmgmldy33lwk2044hb4l02d44vm4fbvicin6p")))

(define-public crate-gl-0.12.0 (c (n "gl") (v "0.12.0") (d (list (d (n "gl_generator") (r "^0.11.0") (d #t) (k 1)) (d (n "glutin") (r "^0.19.0") (d #t) (k 2)))) (h "135v8ld0545qwd6igi07600p6wra2nnjhqxwaj29f2sjjdsq27dx")))

(define-public crate-gl-0.13.0 (c (n "gl") (v "0.13.0") (d (list (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)) (d (n "glutin") (r "^0.19.0") (d #t) (k 2)))) (h "0jnqwy7zidlvbxiy2wq0y26b8nvbg071jb210qv9wngw1dz1qhab")))

(define-public crate-gl-0.14.0 (c (n "gl") (v "0.14.0") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "glutin") (r "^0.21.0") (d #t) (k 2)))) (h "015lgy3qpzdw7mnh59a4y4hdjq1fhv7nkqlmh1h6fzc212qxlkm9")))

