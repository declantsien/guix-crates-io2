(define-module (crates-io #{2}# cj) #:use-module (crates-io))

(define-public crate-cj-0.0.0 (c (n "cj") (v "0.0.0") (h "0q8js3pcfglnl8avqqn9vpfdgslhnifcyn9av3h22kf78mk4d0b0")))

(define-public crate-cj-0.0.1 (c (n "cj") (v "0.0.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "jsonpath-rust") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p3ylc8irx9rv3vmsswf6x08dr2r1lsxhxiq48a9hvii4ng6vb44") (y #t)))

(define-public crate-cj-0.0.2 (c (n "cj") (v "0.0.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "jsonpath-rust") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bas3yaqv3zkhj6wfwi1frybsg7bx8zmayxv8xfg1x8gsypa81qg")))

