(define-module (crates-io #{2}# yn) #:use-module (crates-io))

(define-public crate-yn-0.1.0 (c (n "yn") (v "0.1.0") (h "17krbdaw7s4ps264b2kn137pszx2vkmc8lh5v0ssjfzy181am5rn")))

(define-public crate-yn-0.1.1 (c (n "yn") (v "0.1.1") (h "1v1rhxg44d59bcl0kjs380bxllc1qihk1bg6wqj721naa15b52fp")))

