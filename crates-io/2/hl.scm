(define-module (crates-io #{2}# hl) #:use-module (crates-io))

(define-public crate-hl-0.1.0 (c (n "hl") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)))) (h "09rhxzi0lkk7ha54n159w70bnrslqjl566qa9mxbpjrq068yqxfb")))

(define-public crate-hl-0.2.0 (c (n "hl") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)))) (h "1756wgdqx9n2h27m6cyyiclc2gwrrk8l222d4znq18lxb1rjv7j1")))

(define-public crate-hl-0.3.0 (c (n "hl") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)))) (h "0kfdwimyvnm1nv6ibagzxvfxkypifj2h784i36xxi3pfij331vwf")))

(define-public crate-hl-0.4.0 (c (n "hl") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)))) (h "15il4x26mgr1h0pwwb4wdd8vcj3ybwrq2l4vq31b0q5v7vcwm4pf")))

