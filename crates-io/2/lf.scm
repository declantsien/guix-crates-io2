(define-module (crates-io #{2}# lf) #:use-module (crates-io))

(define-public crate-lf-0.1.0 (c (n "lf") (v "0.1.0") (h "1h7cb0swqg6xqv7qzyv216975d8cq870r7shqwp310w1ggcicfxl")))

(define-public crate-lf-0.2.0 (c (n "lf") (v "0.2.0") (h "042dvw2hqbm9f44vnkshfrqdz2ap59gpnjhjndbrr6cwzq8inv5m")))

