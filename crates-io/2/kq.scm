(define-module (crates-io #{2}# kq) #:use-module (crates-io))

(define-public crate-kq-0.1.0 (c (n "kq") (v "0.1.0") (h "0n5n9g457383md75pkjh22y3mg1m9wravqli4kyar42xwr7hwnss")))

(define-public crate-kq-1.0.0 (c (n "kq") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "kdl") (r "^3.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "predicates") (r "^2.0.3") (d #t) (k 2)))) (h "00547s14vplfwriih48h23aal1vbpxgf71jvl11blnhv1yb1xldi")))

(define-public crate-kq-1.0.2 (c (n "kq") (v "1.0.2") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "kdl") (r "^3.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "predicates") (r "^2.0.3") (d #t) (k 2)))) (h "0zk93fbx3cy98av7mgff6a2xfg08h824sdwvpgn851lj7z8zx2pv")))

(define-public crate-kq-1.0.4 (c (n "kq") (v "1.0.4") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "kdl") (r "^3.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "predicates") (r "^2.0.3") (d #t) (k 2)))) (h "0b7axyrq2avk3xvzpj22zw7p37xajyd23gc6hy6yx4gdp82d2pvw")))

(define-public crate-kq-1.0.6 (c (n "kq") (v "1.0.6") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "kdl") (r "^3.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "predicates") (r "^2.0.3") (d #t) (k 2)))) (h "0g8c7sf2vgak5zqsd4fhl63f5gffr2yvqj0s3nf4h45324icc5a3")))

