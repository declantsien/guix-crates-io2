(define-module (crates-io #{2}# pu) #:use-module (crates-io))

(define-public crate-pu-0.0.0 (c (n "pu") (v "0.0.0") (h "0lmdw2nza07gakmldw6fq650v4cxn57v1k1mwh4ndbmvqvddk1gw") (y #t)))

(define-public crate-pu-0.0.1 (c (n "pu") (v "0.0.1") (h "0hmbgj9mld0gbgjjbv745pyh8czdcs227zp0zxzx6jigjx60c8w6") (y #t)))

(define-public crate-pu-0.0.2 (c (n "pu") (v "0.0.2") (h "1p8d611wl79bjnd5apw6f3dqqpaprgknsmwnrcr9zc0p50qmdzyb") (y #t)))

