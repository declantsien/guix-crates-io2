(define-module (crates-io #{2}# mm) #:use-module (crates-io))

(define-public crate-mm-0.0.1 (c (n "mm") (v "0.0.1") (h "0bdknqi8qyh7d4zb0112hz1g83iv4ag388vjm5fq7hg1wrsdcjcl") (y #t)))

(define-public crate-mm-0.0.2 (c (n "mm") (v "0.0.2") (h "1z0mwldkcl7b9j0j3n64j9lm783a0p9cmaj5aby9l5ww47w2la9w") (y #t)))

