(define-module (crates-io #{2}# ed) #:use-module (crates-io))

(define-public crate-ed-0.1.0 (c (n "ed") (v "0.1.0") (d (list (d (n "ed-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "seq-macro") (r "^0.1.5") (d #t) (k 0)))) (h "1ba9hihqj2d7nrwjx9xpkdiid98f9hbrah40d1clwibnb9kgccz3")))

(define-public crate-ed-0.1.1 (c (n "ed") (v "0.1.1") (d (list (d (n "ed-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "seq-macro") (r "^0.1.5") (d #t) (k 0)))) (h "1hrwy4ys6s9rcwyqvilkrbdn44chfxzw09hi0flgqw0yxiii6c76")))

(define-public crate-ed-0.1.2 (c (n "ed") (v "0.1.2") (d (list (d (n "ed-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "seq-macro") (r "^0.1.5") (d #t) (k 0)))) (h "1x86zb01wjcn5lyq15agqq9wjacgqs9vr5q93j8qnigk0w45jrhz")))

(define-public crate-ed-0.1.3 (c (n "ed") (v "0.1.3") (d (list (d (n "ed-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "seq-macro") (r "^0.1.5") (d #t) (k 0)))) (h "08hz8nq6n93vmhrfh3800q88avmdwcadghfrwhajlbxq71dadm2h")))

(define-public crate-ed-0.1.4 (c (n "ed") (v "0.1.4") (d (list (d (n "ed-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "seq-macro") (r "^0.1.5") (d #t) (k 0)))) (h "1a2lpwxqny0g3czy59kh7ypkz1y9gim6niz6kqxwm6cjj67q9lmh")))

(define-public crate-ed-0.1.5 (c (n "ed") (v "0.1.5") (d (list (d (n "ed-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "seq-macro") (r "^0.1.5") (d #t) (k 0)))) (h "1qbp337zhh7s74lcmrywnkr8spwlsl9vnxizfidmi3v51lk471as")))

(define-public crate-ed-0.1.6 (c (n "ed") (v "0.1.6") (d (list (d (n "ed-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "seq-macro") (r "^0.1.5") (d #t) (k 0)))) (h "04c31kw532afhfxs4krxrzaxk8zj9x59l2a1fgibihaxm0a61h0s")))

(define-public crate-ed-0.2.0 (c (n "ed") (v "0.2.0") (d (list (d (n "ed-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18gndzj52hwg6wbcy9b89icg0mgk05yayaz2bcpf3bw32qv55dfa")))

(define-public crate-ed-0.2.1 (c (n "ed") (v "0.2.1") (d (list (d (n "ed-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g5gbsshaxk0aa9k6pd5lzaxb47b5wd4ivqk50ib9s4n0dhxbgcl")))

(define-public crate-ed-0.2.2 (c (n "ed") (v "0.2.2") (d (list (d (n "ed-derive") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nnx6vjpgav7y9v7xd0r6p41gmwnnq14izyiwf3wvbbgj7mddj7r")))

(define-public crate-ed-0.3.0 (c (n "ed") (v "0.3.0") (d (list (d (n "ed-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "178nw297n6pb91p4lqrjqy2dq60qfasjwljs3zagvs49xijmq80g")))

