(define-module (crates-io #{2}# ri) #:use-module (crates-io))

(define-public crate-ri-0.6.0 (c (n "ri") (v "0.6.0") (d (list (d (n "colored") (r "~1.9") (d #t) (k 0)) (d (n "json") (r "~0.12") (d #t) (k 0)) (d (n "libloading") (r "~0.6") (d #t) (k 0)) (d (n "nix") (r "~0.17") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("blocking" "json"))) (k 0)) (d (n "rouille") (r "~3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0agvsvgdk7k2psr9s1n4b5aj53x8668zbj6zf2i20g491p1cnh8f") (y #t)))

(define-public crate-ri-0.6.1 (c (n "ri") (v "0.6.1") (d (list (d (n "colored") (r "~1.9") (d #t) (k 0)) (d (n "libloading") (r "~0.6") (d #t) (k 0)) (d (n "nix") (r "~0.17") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("blocking" "json"))) (k 0)) (d (n "rouille") (r "~3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09pxzqfw6c1qmirmrdqgs0ayvmxqhd3lgihj4hy4l7y1r9agjymp") (y #t)))

