(define-module (crates-io #{2}# s7) #:use-module (crates-io))

(define-public crate-s7-0.1.0 (c (n "s7") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0kfk2k549vnnap46q6pngpww7ffwxqvncnnn3bxl7fys4y4nffgb")))

(define-public crate-s7-0.1.1 (c (n "s7") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1y2i9ha7j5flr9mpxbf5fn1kyyijnfd8njrvl4kfl0fhc34i6hhz")))

(define-public crate-s7-0.1.2 (c (n "s7") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "136qc93qf1lyav6lbimsb0srjg5wigwrg2y11lfql1n4af023p71")))

(define-public crate-s7-0.1.3 (c (n "s7") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "14skgr5hd08n792w6lvih34nh506myj2iviyrc2rmlsfkbag9dbq")))

(define-public crate-s7-0.1.4 (c (n "s7") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "0f3z4ij6dmr7iz3iz502ixc79lv3bgdy5sl92pacpg241lfi47gj")))

(define-public crate-s7-0.1.5 (c (n "s7") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1fhilgrdjjg21bjwfk0cakbf8v8r43f27mms97sygh42kpi5kqhf")))

(define-public crate-s7-0.1.6 (c (n "s7") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1dr709ssj7lwzg994qjla7cli1www7d0h86m76898yid3gshqmmq")))

(define-public crate-s7-0.1.7 (c (n "s7") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "16yhfq3pgm8pijhxjslqgx87pzywg4n5sxsia0pckrnmvfdi4qsd")))

(define-public crate-s7-0.1.8 (c (n "s7") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1y9ra3p3dsqs32xf34hsqj6lbl1758a77s70yz21wapdvl0n1dp7")))

(define-public crate-s7-0.1.9 (c (n "s7") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "07l8mhrhv1br6qwrwjijy2bgz4ijs97vq2rva0gx780ywafykxws")))

