(define-module (crates-io #{2}# c2) #:use-module (crates-io))

(define-public crate-c2-0.1.0 (c (n "c2") (v "0.1.0") (d (list (d (n "c2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0vjcwl896h3924p8x62009q5z1v1w2b3gwjwmfyhnkkax2hmrdaa")))

(define-public crate-c2-0.2.0 (c (n "c2") (v "0.2.0") (d (list (d (n "c2-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0cbqlh6s2sm02l9msaq1s2l8s19qx8lifm8h00rxqp3p4sndshps")))

