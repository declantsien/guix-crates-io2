(define-module (crates-io #{2}# nx) #:use-module (crates-io))

(define-public crate-nx-0.2.0 (c (n "nx") (v "0.2.0") (h "0ysdcrm85hziswn072gdfr1ga4078dzkfjf4l7gilhz91i4wl20p")))

(define-public crate-nx-0.2.1 (c (n "nx") (v "0.2.1") (h "19j7n3zzbgpdwdq78mvlvcjxyc0jcb1lnidc5hylashc8myn8yg0")))

(define-public crate-nx-0.2.2 (c (n "nx") (v "0.2.2") (h "1xnbqbl4qjdr4x17mfndv0jxnx64r1jbhy7sgyrg1i7iwf57mi72")))

(define-public crate-nx-0.2.3 (c (n "nx") (v "0.2.3") (h "0i3z2yd9yajw3vdkxn5q41k7pmybg25py3xj5lxsbcj4sq7gdzpy")))

(define-public crate-nx-0.2.4 (c (n "nx") (v "0.2.4") (d (list (d (n "memmap") (r "*") (d #t) (k 0)))) (h "1bylf9g11iyg4s60qcjjbzzv8jcawb20hvykkwv88l2jz4831ak1")))

(define-public crate-nx-0.3.0 (c (n "nx") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.18") (f (quote ("png_codec"))) (k 2)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "0j0z5ppgk994hzyym05cicli3ln6hzx5rnyqcfia40l7w30n2bc4")))

