(define-module (crates-io #{2}# rt) #:use-module (crates-io))

(define-public crate-rt-0.0.1 (c (n "rt") (v "0.0.1") (h "0ywxzq32jgsx4503jgia4kh4c2qsmz5d6804h6pyn23n0m84j5y0") (y #t)))

(define-public crate-rt-0.0.2 (c (n "rt") (v "0.0.2") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0vf5nsaakiicw9210pmcqbydhd5znn9wdybf92ix9nhwg7k44dnv") (y #t) (r "1.64.0")))

(define-public crate-rt-0.0.3 (c (n "rt") (v "0.0.3") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "02hkzlfrjp0rbg3s7lpsz6s2hhpwjgy5sck3xbxz9mwpqkfdz6mj") (y #t) (r "1.64.0")))

(define-public crate-rt-0.0.4 (c (n "rt") (v "0.0.4") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1rc9cfcqg0z8hmd3kasi2fmjm8j9mppxr9xp13nzycfx9gyl00jj") (y #t) (r "1.64.0")))

(define-public crate-rt-0.0.5 (c (n "rt") (v "0.0.5") (d (list (d (n "atomic_float") (r "^0.1.0") (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0di3v3mcgdwk3nv3qn7wypkwgy7j4y1b9gl0zncr4rgn9zvvc12y") (y #t) (r "1.64.0")))

(define-public crate-rt-0.0.6 (c (n "rt") (v "0.0.6") (d (list (d (n "atomic_float") (r "^0.1.0") (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1sn5x7pzyv3gl8c48mdwik1jr3sqhgk5vbayn49fv48y7f7gxcfl") (y #t) (r "1.64.0")))

(define-public crate-rt-0.0.7 (c (n "rt") (v "0.0.7") (d (list (d (n "atomic_float") (r "^0.1.0") (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0fd37nazv1rrj43x4v38s9f50xjxwmyxf7ilq361pakffa9r879i") (y #t) (r "1.75.0")))

(define-public crate-rt-0.1.0 (c (n "rt") (v "0.1.0") (d (list (d (n "atomic_float") (r "^0.1.0") (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "024ml78dn4zs6aqj6ciaqyav9q1r4qh85wrjh258frfs72y3s09x") (y #t) (r "1.76.0")))

(define-public crate-rt-0.1.1 (c (n "rt") (v "0.1.1") (d (list (d (n "atomic_float") (r "^0.1.0") (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1yc38znbzvaswna51ilmmg68zfga9pk38w4x2dls150ym0ywf6cv") (f (quote (("sitara-vim") ("sitara-intc") ("hercules")))) (y #t) (r "1.76.0")))

(define-public crate-rt-0.1.2 (c (n "rt") (v "0.1.2") (d (list (d (n "atomic_float") (r "^0.1.0") (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0142k780f1s5pmxwc464a7sbvidrb6w7kflcsrfgqx08k5idp2ml") (f (quote (("sitara-vim") ("sitara-intc") ("hercules")))) (r "1.76.0")))

(define-public crate-rt-0.2.0 (c (n "rt") (v "0.2.0") (d (list (d (n "atomic_float") (r "^0.1.0") (k 2)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0y44azannq76891h6fsf266j3mlrmhm9b8j8fwd6k5wjw89ln1bv") (f (quote (("sitara-vim") ("sitara-intc") ("hercules")))) (r "1.76.0")))

(define-public crate-rt-0.3.0 (c (n "rt") (v "0.3.0") (d (list (d (n "atomic_float") (r "^0.1.0") (k 2)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1n2rdwmjg0hib0iix1zxn2szbpwjd2la35jnhbpgkd6kclj6z74n") (f (quote (("sitara-vim") ("sitara-intc") ("hercules")))) (r "1.76.0")))

(define-public crate-rt-0.4.0 (c (n "rt") (v "0.4.0") (d (list (d (n "atomic_float") (r "^1.0") (k 2)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1rpd7b4r2im375d5vvd4h5zlvaw3gxh1y8k1cgdlibdx5vd8ksz0") (f (quote (("sitara-vim") ("sitara-intc") ("hercules")))) (r "1.76.0")))

(define-public crate-rt-0.4.1 (c (n "rt") (v "0.4.1") (d (list (d (n "atomic_float") (r "^1.0") (k 2)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "14abk5znwmbw7hq5399bdcs8cjzgzdajf9alx790d22v6ff1iizh") (f (quote (("sitara-vim") ("sitara-intc") ("hercules")))) (r "1.76.0")))

