(define-module (crates-io #{2}# w8) #:use-module (crates-io))

(define-public crate-w8-0.1.0 (c (n "w8") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0fr6pwl1bdyzbj9wk848iqmsi7djdf2pikvygdvl9cvhmh0qznd0")))

