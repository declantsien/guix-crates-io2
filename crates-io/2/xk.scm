(define-module (crates-io #{2}# xk) #:use-module (crates-io))

(define-public crate-xk-0.1.0 (c (n "xk") (v "0.1.0") (h "0p83y9x3dsyiqj8fswvvs4l6kgf73wblrrrqdn7vi2nzlx2vhppd")))

(define-public crate-xk-0.1.1 (c (n "xk") (v "0.1.1") (h "19pk2qdlcr2zyv4yd2f14f3fvsiqh5lznb4nkzwg9452p4pv33wy")))

(define-public crate-xk-0.1.3 (c (n "xk") (v "0.1.3") (h "0kghpkp43m3n13kc7fhm276p49mjs5g7f6d83vi5zqdr1kfg4i86")))

(define-public crate-xk-0.1.4 (c (n "xk") (v "0.1.4") (h "0nhxnbixnjfjpa26s2hfb08jngagi9may9zfv659ijgk3ayqm260")))

