(define-module (crates-io #{2}# nq) #:use-module (crates-io))

(define-public crate-nq-0.0.3 (c (n "nq") (v "0.0.3") (h "08a7vjz999p0z7gziymy46ylzkzdn015rpmvvkq7ljl6fwmkm6aj")))

(define-public crate-nq-0.0.4 (c (n "nq") (v "0.0.4") (h "1ynlwwv7vbzq3hcisd8x45bcax475lfl23zn83dwq4ipzkryg6pa")))

(define-public crate-nq-0.0.5 (c (n "nq") (v "0.0.5") (h "0b8gqqg0qi9cri2an1f25b0f4avkjyzqlwca8f72npz2w55iwwnm")))

(define-public crate-nq-0.0.6 (c (n "nq") (v "0.0.6") (h "1z0ix93zr4lgiwp7zsvf46yv432cng23smb7a5c0jw0rhfslfkba")))

