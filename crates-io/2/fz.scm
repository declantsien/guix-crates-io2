(define-module (crates-io #{2}# fz) #:use-module (crates-io))

(define-public crate-fz-0.1.0 (c (n "fz") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)))) (h "0j30w0lpi061wbiisvy6368pbpi1pp5jd4swpq12315qlcwq6r2r") (y #t)))

