(define-module (crates-io #{2}# fd) #:use-module (crates-io))

(define-public crate-fd-0.1.0 (c (n "fd") (v "0.1.0") (h "06cnyl14dylsalxqd3y8n9hws0cjxcq5k224625cw0wss1l0q9pk")))

(define-public crate-fd-0.1.1 (c (n "fd") (v "0.1.1") (h "1hfgi6ggh3s789j63p2d8w3yyy3sv2yjai29pl417v5pgpvvgf0i")))

(define-public crate-fd-0.2.0 (c (n "fd") (v "0.2.0") (h "1gxcsslcw6sm4g24gfshjp8bym8g0ndxwshp6mwk37rqlh0p74fx")))

(define-public crate-fd-0.2.1 (c (n "fd") (v "0.2.1") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1lf98l3x0wb000c4fijjn8wqz3q5mf8lapn4a0cy94dmakky60n7")))

(define-public crate-fd-0.2.2 (c (n "fd") (v "0.2.2") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1p1rkpjp16qbkdqwspcagiml8dnbrn15zla5zml0xay6is4qidlx")))

(define-public crate-fd-0.2.3 (c (n "fd") (v "0.2.3") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1l06s19k9rfhxcnnjbkaq6v41bgad56wv58f97nanxj0bj6b4ni7")))

