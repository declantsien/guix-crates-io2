(define-module (crates-io #{2}# zw) #:use-module (crates-io))

(define-public crate-zw-0.1.0 (c (n "zw") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "0q7lf4016wda1h7wnrw4kj92w3vhivbhi35k242h69d5zqx8dhw1")))

(define-public crate-zw-0.2.0 (c (n "zw") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1vhf4icwk7mkq5rxgipqcm893y36lb5l5i3ifrhhljzgw2ric36v")))

