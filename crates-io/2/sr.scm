(define-module (crates-io #{2}# sr) #:use-module (crates-io))

(define-public crate-sr-0.1.0 (c (n "sr") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1357xr27gvjhn968k93x5lyk1hpchcailq74a8bzkh9cp8dm3j4p")))

(define-public crate-sr-0.1.1 (c (n "sr") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "yansi") (r "^0.4") (d #t) (k 0)))) (h "1zhic6w7wqydhknfaqwi8pzh92ax5yk3269bp7f1crb72w79flw7")))

