(define-module (crates-io #{2}# gp) #:use-module (crates-io))

(define-public crate-gp-0.0.1 (c (n "gp") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1986nh4rg1zf2yw17z0i6wpi3p1a5g9i9rc9lp4bc1lhv29vgywf")))

