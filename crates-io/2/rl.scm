(define-module (crates-io #{2}# rl) #:use-module (crates-io))

(define-public crate-rl-0.1.0 (c (n "rl") (v "0.1.0") (h "010n5jwv0xmxm0dfddjj5xs5w31qqcsn3gmgfqj5nyfsgji0wf26")))

(define-public crate-rl-0.2.0 (c (n "rl") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1giqyfcqz2zwzv1rmqjzps1ynji97j6iaff2gnkk8ramkn7zgfng")))

