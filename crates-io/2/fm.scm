(define-module (crates-io #{2}# fm) #:use-module (crates-io))

(define-public crate-fm-0.1.0 (c (n "fm") (v "0.1.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "12bizz8s8ly5njqccrry134igpxzllqqzf2gpn1z27figr5pvrxc")))

(define-public crate-fm-0.1.1 (c (n "fm") (v "0.1.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "03qsq1h2s564yb081m8svg5yixgbgmn7f3mf45qdmaxrlmkmbqr5")))

(define-public crate-fm-0.1.2 (c (n "fm") (v "0.1.2") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0jci7fli4788w55zqmmxcsnf2hlhiakdfmp4pqzdjs932ikcm8cy")))

(define-public crate-fm-0.1.3 (c (n "fm") (v "0.1.3") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0kk6bmal947pnr1qlikk7l9dcws84py4vih5m3nb90km2nf0y94m")))

(define-public crate-fm-0.1.4 (c (n "fm") (v "0.1.4") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1jmchi4sb9mq0a010gw9igqw1lim9fd1g9fzwlclrs6cyb7s7zb8")))

(define-public crate-fm-0.2.0 (c (n "fm") (v "0.2.0") (d (list (d (n "regex") (r ">=1.3.0, <2.0.0") (d #t) (k 0)))) (h "00idhm5xfylvvim5g7yjvfriv2qisqvzymlcr6zf5cbdvmj5h5lm")))

(define-public crate-fm-0.2.1 (c (n "fm") (v "0.2.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0np3pfan7hgngy52vpwa1n5fcnyh9cxgzji5awpqfw8nkcrq24v7")))

(define-public crate-fm-0.2.2 (c (n "fm") (v "0.2.2") (d (list (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "1m6z2hca6bnn0wjkn84n6g2ai0xcsjsvqkyqx7vlr00acbdz9g11")))

(define-public crate-fm-0.3.0 (c (n "fm") (v "0.3.0") (d (list (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "0jn91j7ip4f62lrmv7n4m2kzx6jsfvgll7dhifbrzl4zdf8pyzhp")))

