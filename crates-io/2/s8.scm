(define-module (crates-io #{2}# s8) #:use-module (crates-io))

(define-public crate-s8-0.1.0 (c (n "s8") (v "0.1.0") (d (list (d (n "attohttpc") (r "^0.22.0") (f (quote ("tls"))) (k 0)) (d (n "rust_json") (r "^0.1.5") (d #t) (k 0)))) (h "1c1rxkk9d5l8jdvswvkq30awgz7maiv1c9k4kmx4fba2z1bqm0i5")))

(define-public crate-s8-0.1.1 (c (n "s8") (v "0.1.1") (d (list (d (n "attohttpc") (r "^0.22.0") (f (quote ("tls"))) (k 0)) (d (n "rust_json") (r "^0.1.5") (d #t) (k 0)))) (h "0lmxckfln07i5h1ri1m3q9mkw9ifz7px2gpaammr48wha70g7b56")))

(define-public crate-s8-0.1.2 (c (n "s8") (v "0.1.2") (d (list (d (n "attohttpc") (r "^0.22.0") (f (quote ("tls"))) (k 0)) (d (n "rust_json") (r "^0.1.5") (d #t) (k 0)))) (h "06dl77gkl5f0nc29vrrs1il673a95x57rcgq461q4kqzq21bqcfj")))

