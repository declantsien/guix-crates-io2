(define-module (crates-io #{2}# ev) #:use-module (crates-io))

(define-public crate-ev-0.0.1 (c (n "ev") (v "0.0.1") (h "1h1x550pwgvrifl3fzcnvfcgk88fp4yigaad07yyx8aar0hv46h9") (y #t)))

(define-public crate-ev-0.0.2 (c (n "ev") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "12691p6cbs0z0lb0mqn15x4dwky6s2kw0ibp3jcz0fyvjmcgnrjc") (y #t)))

(define-public crate-ev-0.1.0 (c (n "ev") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "0x6gqbxfjihligmkw5x18r094wkvnk6gq0w5r0gphwfjrn3ck1m5")))

