(define-module (crates-io #{2}# p9) #:use-module (crates-io))

(define-public crate-p9-0.2.0 (c (n "p9") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "p9_wire_format_derive") (r "^0.2.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0mqzh6v8vgrpm3a61z3f8gjc7czkdjd9r0fc3b0l5knkamhjwvm8") (f (quote (("trace")))) (y #t)))

(define-public crate-p9-0.2.1 (c (n "p9") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "p9_wire_format_derive") (r "^0.2.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0n8lh7fpj6lwnv8fbh1pmiwrwa35bqzax8ph8gnhi4hl9pad580g") (f (quote (("trace")))) (y #t)))

(define-public crate-p9-0.2.2 (c (n "p9") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "p9_wire_format_derive") (r "^0.2.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (t "cfg(unix)") (k 0)))) (h "06fyc7vsj7qbkd4rfd2dp1qwry95r4xp3600d06z7f1xflmk0ls2") (f (quote (("trace"))))))

(define-public crate-p9-0.2.3 (c (n "p9") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "p9_wire_format_derive") (r "^0.2.3") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (t "cfg(unix)") (k 0)))) (h "10clsaqjpfy4dqd9w0dhkyqbkrg3rgb7wd0q3w2svg6wkgca4f28") (f (quote (("trace"))))))

