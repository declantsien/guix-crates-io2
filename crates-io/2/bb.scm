(define-module (crates-io #{2}# bb) #:use-module (crates-io))

(define-public crate-bb-0.0.0 (c (n "bb") (v "0.0.0") (h "027lr5c27xzz2rkzcid0r3dbxl4mp6r801qqa9q0b8cfnps41xij")))

(define-public crate-bb-0.2.0 (c (n "bb") (v "0.2.0") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "1yfqh22skw9gqxssz3qnlapab1sxwi5pyn66r36viqib5fiv9p74")))

(define-public crate-bb-0.2.1 (c (n "bb") (v "0.2.1") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0w6g689w3mz9kmrivvxb67l8kcp82b7fhl8vznzhg12ci5j2wdvz")))

(define-public crate-bb-0.2.2 (c (n "bb") (v "0.2.2") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "1a5wx45wkfzd6xazf632hnxaq2s01n24cwx18kf2vn9c7hr5j1r5")))

(define-public crate-bb-0.3.1 (c (n "bb") (v "0.3.1") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0nkw6z7zj8j3d4xjsfr5mr4a759iggikw4l1d949d9186r0f6q7f")))

(define-public crate-bb-0.4.0 (c (n "bb") (v "0.4.0") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0im0skj35a0zy8p4lblh5kbkjrsnqfgbmk2nfhyy1axy43i3sk5n")))

(define-public crate-bb-0.4.1 (c (n "bb") (v "0.4.1") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "nix") (r "^0.16.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.12") (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "1iyjvnb1fcwaj7kljfcypzan22gdswxf02czvwrw16mvcnsfpfax")))

(define-public crate-bb-0.4.2 (c (n "bb") (v "0.4.2") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "nix") (r "^0.16.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.12") (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0m2jw42fpcfldsxrzbhkb52p2iq4gq4bqszv7wbpl8n9c5ncj8kl")))

(define-public crate-bb-0.4.3 (c (n "bb") (v "0.4.3") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "nix") (r "^0.16.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.12") (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "133adq1lsd39bki90nk7zwnwyn6hza6mnja7y2y7bw56a957ra25")))

