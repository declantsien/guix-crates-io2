(define-module (crates-io #{2}# pg) #:use-module (crates-io))

(define-public crate-pg-0.0.0 (c (n "pg") (v "0.0.0") (h "121bgjgx37d6cn2236mqvbgsjwx3mg2p538xcxvs17rmvvm39mpx")))

(define-public crate-pg-0.1.0 (c (n "pg") (v "0.1.0") (d (list (d (n "flags") (r "^0.1") (d #t) (k 0)) (d (n "real-memory") (r "^0.1.1") (d #t) (k 0)))) (h "0xn5ks4lf5dzblnvgd568hi4yp0mf7x430f4dfb9lr1rkrh8bypa")))

