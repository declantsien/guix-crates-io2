(define-module (crates-io #{2}# le) #:use-module (crates-io))

(define-public crate-le-0.0.0 (c (n "le") (v "0.0.0") (h "0rvrymanvmcd58ybsylvgsd5mmwcabziqh8gsniacyarp1qc0s2q") (y #t)))

(define-public crate-le-0.0.1 (c (n "le") (v "0.0.1") (h "0nb8w0mfjwcggfzz4l6cibp7c9za5zcnvf5h39saash8zh6i0745") (y #t)))

(define-public crate-le-0.0.2 (c (n "le") (v "0.0.2") (h "17s5jjj7ywpyas2x67g08bmxs431842bd2khdgn4wzrkyrkq4wl2") (y #t)))

