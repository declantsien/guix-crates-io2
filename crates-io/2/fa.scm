(define-module (crates-io #{2}# fa) #:use-module (crates-io))

(define-public crate-fa-0.1.0 (c (n "fa") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libfa-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1q7irsriy6r63gs66nbk9kfrlzhj4qmqqbfrq925yqqhs10n34w6")))

(define-public crate-fa-0.1.1 (c (n "fa") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libfa-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1517hspyhsd8yxh2da1rr7njyflnbcbwqb7yi6pdz6n5jxw35k3y")))

(define-public crate-fa-0.1.2 (c (n "fa") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfa-sys") (r "^0.1") (d #t) (k 0)))) (h "1ic8wykgjaa41mk9lvb8dmysb8aicyy9hcg1dgw0lz3rc8lkrdyv")))

(define-public crate-fa-0.1.3 (c (n "fa") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfa-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0jmq3qa0njvpv425xg6ak1bbrcrp5bfs3g44z1bmnvdd981v02km")))

(define-public crate-fa-0.1.4 (c (n "fa") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfa-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0j2yxwckppjakr7hc22vww92gdj049mpxk9rkv8f8d14g5cgz282")))

