(define-module (crates-io #{2}# gf) #:use-module (crates-io))

(define-public crate-gf-0.1.0 (c (n "gf") (v "0.1.0") (h "1zwhh5ajlnpr8gdqvarfkwy281i4dmiq141xlfzg5dfj9dcq096q") (y #t)))

(define-public crate-gf-0.1.1 (c (n "gf") (v "0.1.1") (h "1x89391ccrgsybbbzb2nzzh5ifhr2dqbdjz6gyylyr9w95rd2ywb") (y #t)))

(define-public crate-gf-0.1.2 (c (n "gf") (v "0.1.2") (h "1fx7fyzc5gdg8q3jr7lz7w9yfzr9ww2nlmy16lbld8xnx6sd66f3") (y #t)))

(define-public crate-gf-0.2.0 (c (n "gf") (v "0.2.0") (h "13q442lfvryq1cjs4l9iw6pv1c0v0vb0ywcnqziix15a3qdrylyk") (y #t)))

(define-public crate-gf-0.2.1 (c (n "gf") (v "0.2.1") (h "0shj8dq9manb5snsyqqd97yax8x5crwpnwqvix1znka3f79hj1mi") (y #t)))

(define-public crate-gf-0.2.2 (c (n "gf") (v "0.2.2") (h "1q3138583bxgcz4bnddvr2ay6305mn5pwis3hklcddr3nzs28bn1") (y #t)))

(define-public crate-gf-0.3.0 (c (n "gf") (v "0.3.0") (h "1p35vd0w977f35kdzs0gzfpbjm1q2fv1hviylpl4fvcpfniws855")))

(define-public crate-gf-0.3.1 (c (n "gf") (v "0.3.1") (h "10h327aybgbl7wchjqrlq53qisrba7jnp5p7nm4lw05j1xwm13yz")))

(define-public crate-gf-0.4.0 (c (n "gf") (v "0.4.0") (h "0l88psq03hfdnd0a2kmdky140qb7iyl0246rsc4qwn7p77vrcyhw")))

(define-public crate-gf-0.5.0 (c (n "gf") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.29.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1jggkfyxrbqczxllnyl7857wh9w1r0f16asvaxs0kmmq7v2bcryb")))

(define-public crate-gf-0.6.0 (c (n "gf") (v "0.6.0") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1fzg3mkaavdjw1c9n88i2qp48mmiw74savwf3rad1sip15n6cv4y") (f (quote (("unstable_simd"))))))

(define-public crate-gf-0.6.1 (c (n "gf") (v "0.6.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1b7q1g26kla1wppv445fhdly04m8agm03yghs619j4d5bscpncir") (f (quote (("unstable_simd"))))))

