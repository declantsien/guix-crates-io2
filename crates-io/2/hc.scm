(define-module (crates-io #{2}# hc) #:use-module (crates-io))

(define-public crate-hc-0.1.0 (c (n "hc") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.5.4") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "1wm6aq63d3mwhxa249nc55s71f72g0q12aysgjyfmffql59p9ym0")))

(define-public crate-hc-0.1.1 (c (n "hc") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.5.4") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "1v527i213dfqakffm8n3ljvqd0dwcvv3w2mzs9z5wdj8qjgyk935")))

(define-public crate-hc-0.1.2 (c (n "hc") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.5.4") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0chxbbhhaclkm544k3xp0yh13g8p2czil306nafbs2mxg599x8ms")))

