(define-module (crates-io #{2}# al) #:use-module (crates-io))

(define-public crate-al-0.1.0 (c (n "al") (v "0.1.0") (d (list (d (n "kerr") (r "^0.1") (d #t) (k 0)))) (h "14vznnxq9qhrhwb2f1pq29p4y7b7980g9kmfva5bf2c2sqlvwwjq")))

(define-public crate-al-0.1.3 (c (n "al") (v "0.1.3") (d (list (d (n "kerr") (r "^0.1") (d #t) (k 0)))) (h "1nml622pmgfmlxlv33664yly9w15d909kisrs41cdriay9ifjylw")))

(define-public crate-al-0.1.4 (c (n "al") (v "0.1.4") (d (list (d (n "kerr") (r "^0.1") (d #t) (k 0)))) (h "0hh43zxbhg3qacy8ay4kyfl4brzsaqlyyfj898iqvwxz95vzqdva") (f (quote (("unsafe-vars") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-al-0.1.5 (c (n "al") (v "0.1.5") (h "01kyf7v7zjxdpzagqkphlzbjl080qg6gwx6xpnpj21y94q73v5h4")))

