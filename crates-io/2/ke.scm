(define-module (crates-io #{2}# ke) #:use-module (crates-io))

(define-public crate-ke-0.1.0 (c (n "ke") (v "0.1.0") (h "1xf2ncyw4wyj9ylcn30a3gm692wzca1y3dn9cps1dd356my4jdbr")))

(define-public crate-ke-0.1.1 (c (n "ke") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "16qkr5lzqpr76638nskj300ppb4hqfpzcla0mpkg5lg1qgxzsys7")))

(define-public crate-ke-0.1.2 (c (n "ke") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1xxkv55wanf9lxc36k79ah6rlzl54s5ykg83swfwfl0xjws76nj8")))

(define-public crate-ke-0.1.3 (c (n "ke") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "10bpq5va88ps841rhd7dwg45pl8wv8zb75m0d7xjxy9slfmpz5sp")))

(define-public crate-ke-0.1.4 (c (n "ke") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "02w7z0wh1v2zxad09k3xp63f60hna1f9vgwlcrywsyaa240il2ks")))

