(define-module (crates-io #{2}# c3) #:use-module (crates-io))

(define-public crate-c3-0.4.0 (c (n "c3") (v "0.4.0") (d (list (d (n "c3_clang_extensions") (r "^0.3.1") (d #t) (k 0)) (d (n "clang-sys") (r "^0.18") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "10c50wzky5nvl8f1axasyc65c9wr0qmggl31ap8k32k3k3alx52l")))

(define-public crate-c3-0.5.0 (c (n "c3") (v "0.5.0") (d (list (d (n "c3_clang_extensions") (r "^0.3.2") (d #t) (k 0)) (d (n "clang-sys") (r "^0.18") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "0spj54vgpff1rjx8zrav0vcg9s4yk8yqjb3vm8g1azp84y8zbbim")))

(define-public crate-c3-0.6.0 (c (n "c3") (v "0.6.0") (d (list (d (n "c3_clang_extensions") (r "^0.3.3") (d #t) (k 0)) (d (n "clang-sys") (r "^0.19") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "15bv6yfggcbmjwhhcd5q00vwvwg1b4mpwzrbp5c36908svzsarqr")))

(define-public crate-c3-0.6.1 (c (n "c3") (v "0.6.1") (d (list (d (n "c3_clang_extensions") (r "^0.3.4") (d #t) (k 0)) (d (n "clang-sys") (r "^0.19") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "0jqi6zyz7hvmpw7nm5xkyh0749g74rwb5p45qcm8zw0psm2lhmaz")))

(define-public crate-c3-0.6.2 (c (n "c3") (v "0.6.2") (d (list (d (n "c3_clang_extensions") (r "^0.3.4") (d #t) (k 0)) (d (n "clang-sys") (r "^0.19") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "1z1qj0s28ahg9zwjlrni2bk2sznf859h4isy12kkshd61rhs5aaj")))

(define-public crate-c3-0.6.3 (c (n "c3") (v "0.6.3") (d (list (d (n "c3_clang_extensions") (r "^0.3.4") (d #t) (k 0)) (d (n "clang-sys") (r "^0.19") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "0mfrlvqig2j2p2zac6qfh9z1i07w5k9mbaw8c9j628ddjn0zhi44")))

(define-public crate-c3-0.6.4 (c (n "c3") (v "0.6.4") (d (list (d (n "c3_clang_extensions") (r "^0.3.4") (d #t) (k 0)) (d (n "clang-sys") (r "^0.19") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "0r3zhn92y8ydwcg84bx5zrjcs0w6hwdsngknvm3mfsdk6jk35hr3")))

(define-public crate-c3-0.7.0 (c (n "c3") (v "0.7.0") (d (list (d (n "c3_clang_extensions") (r "^0.3.4") (d #t) (k 0)) (d (n "clang-sys") (r "^0.19") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "1bxby446h7mvglhy27pzscr9vyq7nvn7ddi2sx8dsh8spxyyi23b")))

(define-public crate-c3-0.8.1 (c (n "c3") (v "0.8.1") (d (list (d (n "c3_clang_extensions") (r "^0.3.4") (d #t) (k 0)) (d (n "clang-sys") (r "^0.19") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "01c4rm7mn0kn5nz5r8mc8nw5srwa8sj30z3nb7dnsk3kgqrfgv3b")))

(define-public crate-c3-0.9.0 (c (n "c3") (v "0.9.0") (d (list (d (n "c3_clang_extensions") (r "^0.3.5") (d #t) (k 0)) (d (n "clang-sys") (r "^0.19") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "0f7nanfvak5baqvhnp1mbf7p6qzbf26kaf4vjr6xaafxbbklywa0")))

(define-public crate-c3-0.10.0 (c (n "c3") (v "0.10.0") (d (list (d (n "c3_clang_extensions") (r "^0.3.6") (d #t) (k 0)) (d (n "clang-sys") (r "^0.20") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "0s0z3xay8br5rl80fl4mqvsx66333czlgdv7h1hb42gxywj6bcb1")))

(define-public crate-c3-0.11.0 (c (n "c3") (v "0.11.0") (d (list (d (n "c3_clang_extensions") (r "^0.3.6") (d #t) (k 0)) (d (n "clang-sys") (r "^0.20") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "0zkbflbgnska1zsp2a6c4z2b0gga0ssifvkg6p1qx864gmz8g07q")))

(define-public crate-c3-0.11.2 (c (n "c3") (v "0.11.2") (d (list (d (n "c3_clang_extensions") (r "^0.3.8") (d #t) (k 0)) (d (n "clang-sys") (r "^0.29") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)))) (h "1zcf710izzih7igsb1jk5nbwcmj986hd0yv99aaam3bn594bk7mc")))

