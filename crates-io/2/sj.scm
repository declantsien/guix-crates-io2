(define-module (crates-io #{2}# sj) #:use-module (crates-io))

(define-public crate-sj-0.1.0 (c (n "sj") (v "0.1.0") (h "021krr8c8z5g5xjg3vj9lg8d8pdfjyk2b5a39rg9mxc0m308rr09") (f (quote (("std"))))))

(define-public crate-sj-0.1.1 (c (n "sj") (v "0.1.1") (h "030r51vbcxxx3xnbbai41lp4xn9ja1cgsj6zlvjplhq417pzil76") (f (quote (("std"))))))

(define-public crate-sj-0.2.0 (c (n "sj") (v "0.2.0") (h "1mbsghlb5b5f1lxd062h7wpy1yxjbqh7dq09lj0j0d3rvcbsvsnr") (f (quote (("std"))))))

(define-public crate-sj-0.3.0 (c (n "sj") (v "0.3.0") (h "14747cn1qrzczh0vq3247pjpp1hp9imsqpkppbh8ih1i37llmi36") (f (quote (("std"))))))

(define-public crate-sj-0.4.0 (c (n "sj") (v "0.4.0") (h "1nl1dzbrpwixzs1v07qjgc99cs148vh7116lm5g1jnddsgjwffp3") (f (quote (("std"))))))

(define-public crate-sj-0.5.0 (c (n "sj") (v "0.5.0") (h "0cnalgq39d3vrh4g8drpdj5ddd3nmjiqlssbv66nvy7p2lb9mf3x") (f (quote (("std"))))))

(define-public crate-sj-0.6.0 (c (n "sj") (v "0.6.0") (h "1lsgklpp52drs33jpm790fbzbfm0hcy6k73w646mngqkhzxdjfb9") (f (quote (("std"))))))

(define-public crate-sj-0.7.0 (c (n "sj") (v "0.7.0") (h "1grzdb3cmljr60add9z4p116pgqzll5ghc5gi0sndgq3lyhmw1zc") (f (quote (("std"))))))

(define-public crate-sj-0.8.0 (c (n "sj") (v "0.8.0") (h "14hy0nswhajjp1r6jki2h5vlkakibg3bwb6cywq13slnzmy2x1ak") (f (quote (("std"))))))

(define-public crate-sj-0.9.0 (c (n "sj") (v "0.9.0") (h "12g1r4g8vhr1j6wdzzcq9ny5qcjarrpawnqyya0rarakylr039rk") (f (quote (("std"))))))

(define-public crate-sj-0.10.0 (c (n "sj") (v "0.10.0") (h "18v9yrlgkqf1gj3a2a7cxp1liiksjh6px1b3i2br60s0n361vv1b") (f (quote (("std"))))))

(define-public crate-sj-0.11.0 (c (n "sj") (v "0.11.0") (h "11p89jvha9fjrj6ycy0506wajkjsyxnmdgd9ghkl0qg9y5iilbf7") (f (quote (("std"))))))

(define-public crate-sj-0.12.0 (c (n "sj") (v "0.12.0") (h "0lz5phq5g1vz8627kdd7j77npbax9gh703fw40gbbixy2i9hkj5s") (f (quote (("std"))))))

(define-public crate-sj-0.13.0 (c (n "sj") (v "0.13.0") (h "04mj2rr8qllgyna432h3v9fmphy6knyzdmd1wxjz761j8hvi5c2p") (f (quote (("std"))))))

(define-public crate-sj-0.13.1 (c (n "sj") (v "0.13.1") (h "1ll6r0hxr88whz30hn7mlzljf59wsbahf4vf8kdmb6vv978l10i9") (f (quote (("std"))))))

(define-public crate-sj-0.14.0 (c (n "sj") (v "0.14.0") (h "1l6rwps7ipmqla4sh8zhr8xj8qi4xv0mqd2ydab85al196fcdvx3") (f (quote (("std"))))))

(define-public crate-sj-0.15.0 (c (n "sj") (v "0.15.0") (h "0xwim303n961w1dcp5c6q4i35bgna3lh9rs9j29p6dazsak4m9nl") (f (quote (("std"))))))

(define-public crate-sj-0.16.0 (c (n "sj") (v "0.16.0") (h "128pw0ayciwrcx2m5pfin33llwilnz3fdnnhypqhn0vxy5wnsr0s") (f (quote (("std"))))))

(define-public crate-sj-0.16.1 (c (n "sj") (v "0.16.1") (h "07bmq74wwikrysnr13fgh5vh00sqw29ac11mm6f418ypafzn2x6m") (f (quote (("std"))))))

(define-public crate-sj-0.17.0 (c (n "sj") (v "0.17.0") (h "1ss5m8pjcxirmnpv55icj61ckwh04mphi77g6ikmqxchp53x79x3") (f (quote (("std"))))))

(define-public crate-sj-0.17.1 (c (n "sj") (v "0.17.1") (h "17k41saszj5mmhc3dlhixl2hr1kzqy645nfnxr49yv88h08ky9aa") (f (quote (("std"))))))

(define-public crate-sj-0.18.0 (c (n "sj") (v "0.18.0") (h "15a55anabnn7xnv3yadbqdsdnfi7pxz48zp7xpmlx4l0gd85jbv2") (f (quote (("std"))))))

(define-public crate-sj-0.18.1 (c (n "sj") (v "0.18.1") (h "13frmk0vdsfvbf86wpf40pxrsbigb56kg5vr39v9r9b7qflk2ak3") (f (quote (("std"))))))

(define-public crate-sj-0.18.2 (c (n "sj") (v "0.18.2") (h "0i8z0v8h2b2i21s8w8fzm3wkxglbws2irbn1jmpyaw0ccyy7xmlw") (f (quote (("std"))))))

(define-public crate-sj-0.18.3 (c (n "sj") (v "0.18.3") (h "11s7nsbrxldpik63lk8sk9ns99pbj766rhd7f3b49visd0z10srk") (f (quote (("std"))))))

(define-public crate-sj-0.18.4 (c (n "sj") (v "0.18.4") (h "1j5gxn4x1c2yccyvznm3cymx738jp3mjik979fy8vlfqk9hlgisb") (f (quote (("std"))))))

(define-public crate-sj-0.18.5 (c (n "sj") (v "0.18.5") (h "048mn1s2ljqj6526xk4bwm1aaj2pcl0fwgq1q23qzcgv1hjqk16l") (f (quote (("std"))))))

(define-public crate-sj-0.18.6 (c (n "sj") (v "0.18.6") (h "0zfqay9qfb0av72pzasnp5pr1gqiyx6zyg3dzbl2x5jymx7i66wl") (f (quote (("std"))))))

(define-public crate-sj-0.18.7 (c (n "sj") (v "0.18.7") (h "1hh442nivb55vbg72xm3askja1fs5gi3cbb0yrpyq8ba9iclyzvd") (f (quote (("std"))))))

(define-public crate-sj-0.18.8 (c (n "sj") (v "0.18.8") (h "05myg9fqqnmf8m41zvbvf5zwm82jvqgdkys7yi5wx5dwp4nqsz7y") (f (quote (("std"))))))

(define-public crate-sj-0.19.0 (c (n "sj") (v "0.19.0") (h "1cxv2szdr99z071pfghn57p2w314v9qqsdcihnsvanbw0yhqaz08") (f (quote (("std"))))))

(define-public crate-sj-0.20.0 (c (n "sj") (v "0.20.0") (h "0czkr2xbgjhibiy2mpbfq5scai6p5xglj9yglrf194rsg3w8r3br") (f (quote (("std"))))))

(define-public crate-sj-0.21.0 (c (n "sj") (v "0.21.0") (h "08lmyfhp6bv8jm4z1jqmx19gy2mw4j13smm3hc43msqrvqpzv8mr") (f (quote (("std") ("hash-map"))))))

(define-public crate-sj-0.21.1 (c (n "sj") (v "0.21.1") (h "1xj1mwh24lbjj6i3701k2svxzh8818mnisb0z4wvh2wnq7d6x998") (f (quote (("std") ("hash-map"))))))

(define-public crate-sj-0.22.0 (c (n "sj") (v "0.22.0") (h "0xgq61kn09d0hgwjli1nx1z4x2ha82jl6a8bn618fj9y7kvfkpfl") (f (quote (("x-unicode") ("std") ("hash-map"))))))

(define-public crate-sj-0.23.0 (c (n "sj") (v "0.23.0") (h "1pwi3l03ga6ibfgzlwmymnzzr434bs26y4qirdh1r1c4gdgy2hiw") (f (quote (("x-unicode") ("std") ("hash-map"))))))

(define-public crate-sj-0.23.1 (c (n "sj") (v "0.23.1") (h "0mkmzv1p3clwir50l3gc9p0xjaqxwkjz6h4qs3nyyznk0sm284qw") (f (quote (("x-unicode") ("std") ("hash-map"))))))

(define-public crate-sj-0.24.0 (c (n "sj") (v "0.24.0") (h "12db1dmvqq9b09y3f39zkvgi8nkib4gvjcn1465zpbr7s2f1iy12") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.25.0 (c (n "sj") (v "0.25.0") (h "18xckj439zj4azx5qhiv3193rbfcqs4n3plim2rkb481hj8ikdgy") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.25.1 (c (n "sj") (v "0.25.1") (h "0ivihbjg09hqzwljkgysj65vkgdvsnakcxjkjvf0s09g8zm52mqk") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.26.0 (c (n "sj") (v "0.26.0") (h "1rb6xj88knlj28dqlbbl6dl5d1hl2ph6prjqlvacihcrffkbfb4c") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.26.1 (c (n "sj") (v "0.26.1") (h "0w9skw842d48wpbv1skamydrdvxazq5rf39r923gqddyj8bbgsn5") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.27.0 (c (n "sj") (v "0.27.0") (h "0qg70ria8zqkmgw1cllh0xgchqyi4fmani49dimsha3mxl1v4cb7") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.27.1 (c (n "sj") (v "0.27.1") (h "1vhbxmjy8mvcxg8qbhcjqjwrysiar5m5gvfz43v9ngqaciicifrn") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.27.2 (c (n "sj") (v "0.27.2") (h "1yfahb8964gmv4nqj47446gczczxc98dzxlvc279h4avzgkjabmm") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.27.3 (c (n "sj") (v "0.27.3") (h "1lnwlkxyncccsqagj2ysinc95pimyybl15cwgn6r3vdbrjb8jhc3") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.27.4 (c (n "sj") (v "0.27.4") (h "00ypbiklp37m9hvzlnxamjjf098klqbfd3igpx7z9ya2qkf1hf1r") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.27.5 (c (n "sj") (v "0.27.5") (h "0h3wqdpvvp7672lc4ypkazmn1fnphk2yvfr2nmcqqhx4axgrwn3m") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.27.6 (c (n "sj") (v "0.27.6") (h "18ppmhg4irfbm1dsib65aavb8fiyjvdi95gf88llgjgwmpm9d3dw") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.27.7 (c (n "sj") (v "0.27.7") (h "1q55f2c7qb78ziiik9mgs7nn5b11njv83qnsyfrvhzy1pvfbhm6d") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.27.8 (c (n "sj") (v "0.27.8") (h "156yx696vdxw0vw7as26vbgpbazmqwflma3bii5kmr84zwkdmzfv") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.27.9 (c (n "sj") (v "0.27.9") (h "08bhaaim4qhkqhxjf88j9gsaxr5dh3qh311al3v5an2as37i6211") (f (quote (("x-unicode") ("std"))))))

(define-public crate-sj-0.27.10 (c (n "sj") (v "0.27.10") (h "1xsx1l3172716x435xn58z0p6b44vngzwdyyxq68ym85g4pknw29") (f (quote (("x-unicode") ("std"))))))

