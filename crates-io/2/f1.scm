(define-module (crates-io #{2}# f1) #:use-module (crates-io))

(define-public crate-f1-0.1.0 (c (n "f1") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14rs4hyq6dlyf5fvllxxa594b9hgcvi5jnldhc2hmafkaxw7cd7b")))

(define-public crate-f1-0.2.0 (c (n "f1") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tungstenite") (r "^0.16") (d #t) (k 0)))) (h "1az48l8fvzsk3dn2bj9i2xyzab5ddgddv7vycwpqsz3gpy3ax1cq")))

