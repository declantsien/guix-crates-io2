(define-module (crates-io #{2}# lb) #:use-module (crates-io))

(define-public crate-lb-0.1.0 (c (n "lb") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 2)) (d (n "termion") (r "^2.0.1") (d #t) (k 2)))) (h "1b2ivbpvikqlhxfba6kjsrknqs8vxr0jhw0jaazgg9vxmdv8y0mk")))

(define-public crate-lb-0.1.1 (c (n "lb") (v "0.1.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.139") (o #t) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 2)))) (h "111c32a2jcs4mffdf1y5gmy8k2pivy4f0ncbfkigw6j64x8kdvdg") (s 2) (e (quote (("term" "dep:libc"))))))

(define-public crate-lb-0.2.0 (c (n "lb") (v "0.2.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.139") (o #t) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 2)))) (h "0n35l3bz00401vkdpddsls6zss9kkcz8whl8xvdjlw7s19mi0m23") (s 2) (e (quote (("term" "dep:libc"))))))

(define-public crate-lb-0.3.0 (c (n "lb") (v "0.3.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 2)))) (h "1k31agyycwf842x9w18djkhig2jvgs9qfz6v9bq8l1pv29s8qyrm") (s 2) (e (quote (("term" "dep:libc"))))))

(define-public crate-lb-0.4.0 (c (n "lb") (v "0.4.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 2)))) (h "074iw1x5png87za5rhwzr5g1fz3hrxny87wsz6xb37b6rvhgyzyl") (s 2) (e (quote (("term" "dep:libc"))))))

(define-public crate-lb-0.4.1 (c (n "lb") (v "0.4.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 2)))) (h "1laf70h5496z8lm1gl3sh27330viscc2c97v4nslj41hpswdizyx") (s 2) (e (quote (("term" "dep:libc"))))))

(define-public crate-lb-0.5.0 (c (n "lb") (v "0.5.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 2)))) (h "0r4p56j22vvrw5cdwd130kk0m81v9kf7xn96w9mq52m0nc2wk1a5") (s 2) (e (quote (("term" "dep:libc"))))))

