(define-module (crates-io #{2}# jy) #:use-module (crates-io))

(define-public crate-jy-1.0.0 (c (n "jy") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0q6q2cgy8s4w839a5l1zc5xcy4kv33bqiwckxc216jd3x2d25wdq")))

(define-public crate-jy-1.0.1 (c (n "jy") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "08l02v57bdxbcvn325nf4ff3f3ph7c5gnjngjylccd44jbhhmjwj")))

