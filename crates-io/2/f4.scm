(define-module (crates-io #{2}# f4) #:use-module (crates-io))

(define-public crate-f4-0.2.0 (c (n "f4") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.8.2") (d #t) (k 0)) (d (n "cast") (r "^0.2.0") (k 0)) (d (n "stm32f429x") (r "^0.3.0") (d #t) (k 0)))) (h "0nmdx7w0i239x63qp5482b75p4wrd3pna11k27c2yvar1ag3bprc")))

