(define-module (crates-io #{2}# ff) #:use-module (crates-io))

(define-public crate-ff-0.1.0 (c (n "ff") (v "0.1.0") (d (list (d (n "ff_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "03nfmnx9vckg2k7q64z6053xd7qv0rwm6psan3h75x8jdm911gry")))

(define-public crate-ff-0.2.0 (c (n "ff") (v "0.2.0") (d (list (d (n "ff_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0v5i4snk2kw5gxynia410i3ikcicrn702z851bjxvhbjkck8qil0")))

(define-public crate-ff-0.3.0 (c (n "ff") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1s5r06qkl8a16slj8q5pwpg2jks6w4byz39x1xvmhx6a79f8vhzg") (f (quote (("u128-support") ("derive" "ff_derive") ("default" "derive"))))))

(define-public crate-ff-0.4.0 (c (n "ff") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1x05xnmd882v2q2rsir5vq9jjwz3f2mzpg2d4admi1ih88p1xj7f") (f (quote (("derive" "ff_derive") ("default"))))))

(define-public crate-ff-0.5.0 (c (n "ff") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "18rf4rsivpmh1ba03r68cd6jzdiag8nmvbw2p3mz2957v1xcgd24") (f (quote (("derive" "ff_derive") ("default"))))))

(define-public crate-ff-0.5.1 (c (n "ff") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0ljy55kxxw7k4rii5v4jzy1ckqyh28sn7586i9qi3dl9hh66v59f") (f (quote (("derive" "ff_derive") ("default"))))))

(define-public crate-ff-0.5.2 (c (n "ff") (v "0.5.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0gkicnzb1m8md8qvwgahkjyl5jxmcsm460afnkh0w53yjrbxlc25") (f (quote (("derive" "ff_derive") ("default"))))))

(define-public crate-ff-0.6.0 (c (n "ff") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "01l9sab8kv51mshng7g7igsihn2a81yjax2117q97sbaxsingff4") (f (quote (("derive" "ff_derive") ("default"))))))

(define-public crate-ff-0.7.0 (c (n "ff") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "ff_derive") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (k 0)) (d (n "subtle") (r "^2.2.1") (f (quote ("i128"))) (k 0)))) (h "0iv0d724ild1mx961y00ny4hynqa0645y3ssvryn9wi5q7yixl81") (f (quote (("std") ("derive" "ff_derive") ("default" "std"))))))

(define-public crate-ff-0.8.0 (c (n "ff") (v "0.8.0") (d (list (d (n "bitvec") (r "^0.18") (k 0)) (d (n "byteorder") (r "^1") (o #t) (k 0)) (d (n "ff_derive") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (k 0)) (d (n "subtle") (r "^2.2.1") (f (quote ("i128"))) (k 0)))) (h "1vsh371vqacpwylk296zfwd5d48yx81a1g7ifcmsigafgl3nwr01") (f (quote (("std") ("derive" "byteorder" "ff_derive") ("default" "std"))))))

(define-public crate-ff-0.9.0 (c (n "ff") (v "0.9.0") (d (list (d (n "bitvec") (r "^0.20.0") (k 0)) (d (n "byteorder") (r "^1") (o #t) (k 0)) (d (n "ff_derive") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.2.1") (f (quote ("i128"))) (k 0)))) (h "1zibhpdfrxjzmrvy2cfvv4rp39ilyv6lzm72499aghmplm0xk93j") (f (quote (("std") ("derive" "byteorder" "ff_derive") ("default" "std"))))))

(define-public crate-ff-0.10.0 (c (n "ff") (v "0.10.0") (d (list (d (n "bitvec") (r "^0.22") (o #t) (k 0)) (d (n "byteorder") (r "^1") (o #t) (k 0)) (d (n "ff_derive") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.2.1") (f (quote ("i128"))) (k 0)))) (h "1w0s794scg4qsr2yccwdq8i1ka4nwmr66vky1z7yx1z4c5nc1vk3") (f (quote (("std") ("derive" "byteorder" "ff_derive") ("default" "bits" "std") ("bits" "bitvec"))))))

(define-public crate-ff-0.10.1 (c (n "ff") (v "0.10.1") (d (list (d (n "bitvec") (r "^0.22") (o #t) (k 0)) (d (n "byteorder") (r "^1") (o #t) (k 0)) (d (n "ff_derive") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.2.1") (f (quote ("i128"))) (k 0)))) (h "0kraavkyrjxgkqpfxv520bi1v7cybyp5jrazg8hj5hwbrlnhpx6h") (f (quote (("std" "alloc") ("derive" "byteorder" "ff_derive") ("default" "bits" "std") ("bits" "bitvec") ("alloc"))))))

(define-public crate-ff-0.11.0 (c (n "ff") (v "0.11.0") (d (list (d (n "bitvec") (r "^0.22") (o #t) (k 0)) (d (n "byteorder") (r "^1") (o #t) (k 0)) (d (n "ff_derive") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.2.1") (f (quote ("i128"))) (k 0)))) (h "17jvl09izh92pbcaszl3mac600ix72grmszafpqjg7sb2828v5dj") (f (quote (("std" "alloc") ("derive" "byteorder" "ff_derive") ("default" "bits" "std") ("bits" "bitvec") ("alloc"))))))

(define-public crate-ff-0.11.1 (c (n "ff") (v "0.11.1") (d (list (d (n "bitvec") (r "^0.22") (o #t) (k 0)) (d (n "byteorder") (r "^1") (o #t) (k 0)) (d (n "ff_derive") (r "^0.11.1") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.2.1") (f (quote ("i128"))) (k 0)))) (h "0909134xgwhxc1432mi5viv6brxqj9szjkyp2w5751727d45a5hk") (f (quote (("std" "alloc") ("derive_bits" "bits" "ff_derive/bits") ("derive" "byteorder" "ff_derive") ("default" "bits" "std") ("bits" "bitvec") ("alloc"))))))

(define-public crate-ff-0.12.0 (c (n "ff") (v "0.12.0") (d (list (d (n "bitvec") (r "^1") (o #t) (k 0)) (d (n "byteorder") (r "^1") (o #t) (k 0)) (d (n "ff_derive") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.2.1") (f (quote ("i128"))) (k 0)))) (h "0ghc60a77gfqflsn37kpbqx0iz6vz22pc4l7zq6vkilmyc0r4s6z") (f (quote (("std" "alloc") ("derive_bits" "bits" "ff_derive/bits") ("derive" "byteorder" "ff_derive") ("default" "bits" "std") ("bits" "bitvec") ("alloc"))))))

(define-public crate-ff-0.12.1 (c (n "ff") (v "0.12.1") (d (list (d (n "bitvec") (r "^1") (o #t) (k 0)) (d (n "byteorder") (r "^1") (o #t) (k 0)) (d (n "ff_derive") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.2.1") (f (quote ("i128"))) (k 0)))) (h "0q3imz4m3dj2cy182i20wa8kbclgj13ddfngqb2miicc6cjzq4yh") (f (quote (("std" "alloc") ("derive_bits" "bits" "ff_derive/bits") ("derive" "byteorder" "ff_derive") ("default" "bits" "std") ("bits" "bitvec") ("alloc"))))))

(define-public crate-ff-0.13.0 (c (n "ff") (v "0.13.0") (d (list (d (n "bitvec") (r "^1") (o #t) (k 0)) (d (n "blake2b_simd") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (o #t) (k 0)) (d (n "ff_derive") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "subtle") (r "^2.2.1") (f (quote ("i128"))) (k 0)))) (h "0jcl8yhcs5kbfxfpnrhpkkvnk7s666vly6sgawg3nri9nx215m6y") (f (quote (("std" "alloc") ("derive_bits" "bits" "ff_derive/bits") ("derive" "byteorder" "ff_derive") ("default" "bits" "std") ("bits" "bitvec") ("alloc"))))))

