(define-module (crates-io #{2}# fe) #:use-module (crates-io))

(define-public crate-fe-1.0.0 (c (n "fe") (v "1.0.0") (d (list (d (n "clap") (r "~2.19.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term-painter") (r "^0.2.3") (d #t) (k 0)))) (h "02vwd510zakfny4yk7npm0hskzp96mkwm9igf4s9h6vxf45vcj4i")))

(define-public crate-fe-1.0.1 (c (n "fe") (v "1.0.1") (d (list (d (n "clap") (r "~2.19.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term-painter") (r "^0.2.3") (d #t) (k 0)))) (h "1sgwjn4jm408cs23f0sjjrs97dyqw71dm41qi2hxvj38dpnz2s9z")))

(define-public crate-fe-1.0.2 (c (n "fe") (v "1.0.2") (d (list (d (n "clap") (r "~2.19.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term-painter") (r "^0.2.3") (d #t) (k 0)))) (h "1kbmmgkys81w4mqwy1cqsb1qz41ki9b1yddfx4z1kdxizbpap032")))

(define-public crate-fe-1.0.3 (c (n "fe") (v "1.0.3") (d (list (d (n "clap") (r "~2.19.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term-painter") (r "^0.2.3") (d #t) (k 0)))) (h "0ja356fdwxn9sdl3b5irxzln6wg93akfkv32f898jw7cb7cp612q")))

