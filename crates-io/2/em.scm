(define-module (crates-io #{2}# em) #:use-module (crates-io))

(define-public crate-em-0.1.0 (c (n "em") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0rm404a46qj3f33mlw0ii4pvlbp2rbpmm4713pggsr1g59m7ja1k")))

(define-public crate-em-0.1.1 (c (n "em") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0kw6wn13x7vldnpknssgmw5j3zgs4iqxb90wqz1yb3dyzaiqdipr")))

(define-public crate-em-0.1.2 (c (n "em") (v "0.1.2") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0iwnr26hi2xgwx6ddwar0b55gwm1mq7bn0f8ll80bw7sss50r3p1")))

(define-public crate-em-0.1.3 (c (n "em") (v "0.1.3") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "16kpr13avdrb43slv35id6vh7sjbhs17gvkwdm6q1wf894pcxsd2")))

(define-public crate-em-0.2.0 (c (n "em") (v "0.2.0") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "08ghii98ljal17d08vnigbalmpz8zfax5zhdfr9caarzfxa1dzdd")))

(define-public crate-em-0.3.0 (c (n "em") (v "0.3.0") (d (list (d (n "emu_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)))) (h "0rzv37sf0mh5iw55yqbjyhg9xjch0y47s7an6a9fj0vb9yzvqmh7")))

