(define-module (crates-io #{2}# en) #:use-module (crates-io))

(define-public crate-en-0.1.0 (c (n "en") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "1017zlq0s2k08lsn13bspf1cx3lc3k2qw2i8r2kbavb8kfxmlmls")))

(define-public crate-en-0.1.1 (c (n "en") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0b6c74r862knj5sljp1fm3qijdg94xdp37rcrq7l1w6c1mb21ixs")))

(define-public crate-en-0.1.2 (c (n "en") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0bp0wd20qq4an1fp74m8a7z5xkb31mpgaa4p1pjxsg6n0hv6xhds")))

(define-public crate-en-0.1.3 (c (n "en") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "1aj9vahgpcmlxggxzk0964dhm3h1c50ahcjs16c3m3m5lw9b3a6b")))

(define-public crate-en-0.1.4 (c (n "en") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "03h7vq9k6q3b51qmagdh7jzjgiy79zmhaqzwlv3s9hm6r31x7dls")))

(define-public crate-en-0.1.5 (c (n "en") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "1f7v3jcs7iglxq7lzyadaq9n26486512lzqp5ijbj3idlail5j5v")))

