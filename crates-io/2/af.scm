(define-module (crates-io #{2}# af) #:use-module (crates-io))

(define-public crate-af-0.0.1 (c (n "af") (v "0.0.1") (h "0pw0qcgxah24nmqdf6inl3wnkaswl2jf04ric6jkfaknnlq05bmp")))

(define-public crate-af-0.0.2 (c (n "af") (v "0.0.2") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1d614kc1wgv8w7l1p09cd5idgymnbpbnfcw7j9734npvyi1zlp0z")))

(define-public crate-af-0.0.3 (c (n "af") (v "0.0.3") (d (list (d (n "http") (r "^0.2.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w83wij9z1645iqs20q1f5gg7b27qzbva4v0bql7sviknh6bh1lq")))

(define-public crate-af-0.0.4 (c (n "af") (v "0.0.4") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "http") (r "^0.2.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bm9v675x6qywrfx52bki6q7zw02h9kxqv6ilhsnpa3ncqj3rrla")))

(define-public crate-af-0.0.5 (c (n "af") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h1llbk9byrf74n7jd2wb3h0qar2asknbls5ih37h2j22krz2l30")))

(define-public crate-af-0.0.6 (c (n "af") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07nzp5fjpzyb94jby4a03czyy7yxbzr0szg2lbxpdrm2i0a1bqlg")))

(define-public crate-af-0.0.7 (c (n "af") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ql9qjd3qlwgw2lq59ihhr2k3xp3vzwbf27gpi4bn6dr21qqgam4")))

(define-public crate-af-0.0.8 (c (n "af") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0r2zb3cm4cq65pa1i7cw41qmd49pcirwmgva1djrns9nviqn6lk8")))

