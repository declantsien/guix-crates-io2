(define-module (crates-io ez ui ezui) #:use-module (crates-io))

(define-public crate-ezui-0.0.1 (c (n "ezui") (v "0.0.1") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "glium") (r "^0.16.0") (d #t) (k 0)) (d (n "glium_text_rusttype") (r "^0.0.4") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18.1") (d #t) (k 0)) (d (n "winit") (r "^0.5.11") (d #t) (k 0)))) (h "0lyilsan0pxcbcm7f9svnxvcyw0a6z3dlzb7f27l6ijqlbka1g49")))

(define-public crate-ezui-0.0.2 (c (n "ezui") (v "0.0.2") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "glium") (r "^0.16.0") (d #t) (k 0)) (d (n "glium_text_rusttype") (r "^0.0.4") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18.1") (d #t) (k 0)) (d (n "winit") (r "^0.5.11") (d #t) (k 0)))) (h "17359jf8jm3cgkf8kzb01s852akj6p7sng6dicm4g356yp3z648v")))

