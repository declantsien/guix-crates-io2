(define-module (crates-io ez _p ez_pqcrypto) #:use-module (crates-io))

(define-public crate-ez_pqcrypto-0.1.0 (c (n "ez_pqcrypto") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "pqcrypto") (r "^0.9.0") (d #t) (k 0)))) (h "0cyjfj5djbr3jsmgnn3fn017cq61607r74lcv4lbjlxsc2hyxbxd") (y #t)))

