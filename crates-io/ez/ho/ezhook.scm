(define-module (crates-io ez ho ezhook) #:use-module (crates-io))

(define-public crate-ezhook-0.1.0 (c (n "ezhook") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.71") (t "cfg(unix)") (k 2)) (d (n "winapi") (r "^0.3.8") (f (quote ("memoryapi"))) (d #t) (t "cfg(windows)") (k 2)))) (h "0x0jgxjb18n1znhqv00hj8ysnhcs4l7zbbi4gj9q475w745cb9v2")))

(define-public crate-ezhook-0.2.0 (c (n "ezhook") (v "0.2.0") (d (list (d (n "lde") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (t "cfg(unix)") (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi"))) (d #t) (t "cfg(windows)") (k 2)))) (h "0ixkbcb7ar4lh9rfgid71g6f9ww8ja16vcdiqp3njrxz3cjyzr8n") (f (quote (("trampoline" "lde"))))))

(define-public crate-ezhook-0.2.1 (c (n "ezhook") (v "0.2.1") (d (list (d (n "lde") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (t "cfg(unix)") (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi"))) (d #t) (t "cfg(windows)") (k 2)))) (h "1gz358d6wfd6k6kmp6x7x9b6kmyg4gkg143c5mxbxn1rxs4w112z") (f (quote (("trampoline" "lde"))))))

(define-public crate-ezhook-0.2.2 (c (n "ezhook") (v "0.2.2") (d (list (d (n "lde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("memoryapi"))) (d #t) (t "cfg(windows)") (k 2)))) (h "066ihmby2p2ni4wa77akyx6ssrwybm0rp0pv6mmha9k5mjykl5r3") (f (quote (("trampoline" "lde"))))))

