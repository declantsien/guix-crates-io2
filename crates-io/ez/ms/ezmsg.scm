(define-module (crates-io ez ms ezmsg) #:use-module (crates-io))

(define-public crate-ezmsg-0.6.0 (c (n "ezmsg") (v "0.6.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0wr20k1hq7z4m6crhmsykqk3di3qnh2ld4965r2dd7m1ci5i5nrh")))

(define-public crate-ezmsg-0.6.1 (c (n "ezmsg") (v "0.6.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "11a2ajcx7mvpcz8i4r8virgxhfaq7c44pcvm0j5vvcbwmpj2pwnn")))

(define-public crate-ezmsg-0.6.2 (c (n "ezmsg") (v "0.6.2") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1pmbdvy2axgcy6h80hfjxxbg8g2xh3s37hvh86vv2702zl5x5izn")))

