(define-module (crates-io ez em ezemoji) #:use-module (crates-io))

(define-public crate-ezemoji-0.0.1 (c (n "ezemoji") (v "0.0.1") (h "12ab0kv8ayx0jacsw7n2p6b6kypcfabwsqqivyf4qas1gga94n60")))

(define-public crate-ezemoji-0.0.2 (c (n "ezemoji") (v "0.0.2") (h "0g63idhqfrf5whml4bwwg7bappy451sqx4v8dhjmvkqv497rqwh0")))

(define-public crate-ezemoji-0.0.3 (c (n "ezemoji") (v "0.0.3") (h "1h96p02gy4nzjk8nxaxylj2bchqqrwav2xjga1rql2p7wa96z0f1")))

(define-public crate-ezemoji-0.1.0 (c (n "ezemoji") (v "0.1.0") (h "0g8vvw7il9n4qbdcbqs4yrp382m362qvs68605f867b4gqifnd2x")))

(define-public crate-ezemoji-0.2.0 (c (n "ezemoji") (v "0.2.0") (h "0474qx7fsn62akfk8h5pkva05ks2qp7p3jwb51b3by6097mnhhmh")))

(define-public crate-ezemoji-0.2.1 (c (n "ezemoji") (v "0.2.1") (h "16w186alry45d06lz96hbdsvfbwgwmymsz68r0q39xab57ndwl02")))

