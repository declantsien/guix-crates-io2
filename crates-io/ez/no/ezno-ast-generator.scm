(define-module (crates-io ez no ezno-ast-generator) #:use-module (crates-io))

(define-public crate-ezno-ast-generator-0.0.1 (c (n "ezno-ast-generator") (v "0.0.1") (d (list (d (n "parser") (r "^0.0.1") (f (quote ("self-rust-tokenize"))) (d #t) (k 0) (p "ezno-parser")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "1h276vqxbbmdgrlqllqdcqlpw3v748a3safnhszb6hqfqf33xknl")))

(define-public crate-ezno-ast-generator-0.0.2 (c (n "ezno-ast-generator") (v "0.0.2") (d (list (d (n "parser") (r "^0.0.2") (f (quote ("self-rust-tokenize"))) (d #t) (k 0) (p "ezno-parser")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "0l8aaw1njb03iyzrslzq203b4ihgnnq1w37b8nms9r89yf0gdlaj")))

(define-public crate-ezno-ast-generator-0.0.3 (c (n "ezno-ast-generator") (v "0.0.3") (d (list (d (n "parser") (r "^0.0.3") (f (quote ("self-rust-tokenize"))) (d #t) (k 0) (p "ezno-parser")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "19jr9knwk4290mj9l01vfalw2lz5nkv1k01whxrpzlcpd8w5hfhb")))

(define-public crate-ezno-ast-generator-0.0.4 (c (n "ezno-ast-generator") (v "0.0.4") (d (list (d (n "parser") (r "^0.0.4") (f (quote ("self-rust-tokenize"))) (d #t) (k 0) (p "ezno-parser")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "1c8irw37l0pqarilm2njlq1xp26jdch2wa1dy0xr14da39mfxahp")))

(define-public crate-ezno-ast-generator-0.0.5 (c (n "ezno-ast-generator") (v "0.0.5") (d (list (d (n "parser") (r "^0.0.5") (f (quote ("self-rust-tokenize"))) (d #t) (k 0) (p "ezno-parser")) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "0ys34vv1ga2bpfh4cqlrp6ri56c8371bi97izihmcbjmm0x0mhkf")))

(define-public crate-ezno-ast-generator-0.0.6 (c (n "ezno-ast-generator") (v "0.0.6") (d (list (d (n "parser") (r "^0.0.6") (f (quote ("self-rust-tokenize"))) (d #t) (k 0) (p "ezno-parser")) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "0032v2m21gisd8v7qalzl1ghf2zmy16nag0d8hgyhqf7s5rkd5w1")))

(define-public crate-ezno-ast-generator-0.0.7 (c (n "ezno-ast-generator") (v "0.0.7") (d (list (d (n "ezno-parser") (r "^0.0.7") (f (quote ("self-rust-tokenize"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "0ak2542linw28vmy4z1sizcx577c2b4cvf2gbn0ijmm7rvwrxraz")))

(define-public crate-ezno-ast-generator-0.0.8 (c (n "ezno-ast-generator") (v "0.0.8") (d (list (d (n "ezno-parser") (r "^0.0.8") (f (quote ("self-rust-tokenize"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "1k9qh5k4ixz5r4bza775yv7n2lpamvhcr6749rmdmj9hrpfrxpzn")))

(define-public crate-ezno-ast-generator-0.0.9 (c (n "ezno-ast-generator") (v "0.0.9") (d (list (d (n "ezno-parser") (r "^0.1.1") (f (quote ("self-rust-tokenize"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "0xngl2zvsps137n9ay8439qg5c9dhzscfbw1kc3ra5dz0a9d2fnp")))

(define-public crate-ezno-ast-generator-0.0.10 (c (n "ezno-ast-generator") (v "0.0.10") (d (list (d (n "ezno-parser") (r "^0.1.2") (f (quote ("self-rust-tokenize"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "09l8lr0nw8zi358hf43y1qm8yn7fjar3gfxgwd8g815ivxfy2w5l")))

(define-public crate-ezno-ast-generator-0.0.11 (c (n "ezno-ast-generator") (v "0.0.11") (d (list (d (n "ezno-parser") (r "^0.1.3") (f (quote ("self-rust-tokenize"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "055qw2848y0kchyxfyykr5i58w623cfd5f0lqbqn32ywjpxggw1n")))

(define-public crate-ezno-ast-generator-0.0.12 (c (n "ezno-ast-generator") (v "0.0.12") (d (list (d (n "ezno-parser") (r "^0.1.4") (f (quote ("self-rust-tokenize"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "^0.3.3") (d #t) (k 0)))) (h "1664cnaahmcblyby5gj3pq5h2q4m2s00vkgf38pschggmf7macqn")))

