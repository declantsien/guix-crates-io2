(define-module (crates-io ez no ezno-parser-visitable-derive) #:use-module (crates-io))

(define-public crate-ezno-parser-visitable-derive-0.0.1 (c (n "ezno-parser-visitable-derive") (v "0.0.1") (d (list (d (n "syn-helpers") (r "^0.4.2") (d #t) (k 0)))) (h "07ri3y34yag5cjv6yy7zzh8kw90z1127pg3nfh08chn1a0bc9w26")))

(define-public crate-ezno-parser-visitable-derive-0.0.2 (c (n "ezno-parser-visitable-derive") (v "0.0.2") (d (list (d (n "string-cases") (r "^0.2") (d #t) (k 0)) (d (n "syn-helpers") (r "^0.4.2") (d #t) (k 0)))) (h "1df1s69kknqb3z5zar8r88r3v59ix2wnrhnjbv7ffh944j5inqkg")))

(define-public crate-ezno-parser-visitable-derive-0.0.3 (c (n "ezno-parser-visitable-derive") (v "0.0.3") (d (list (d (n "string-cases") (r "^0.2") (d #t) (k 0)) (d (n "syn-helpers") (r "^0.4.2") (d #t) (k 0)))) (h "110sd5rnrgz3n1sa86mzbc4gi690m5g8wk4f8q5b0zx8zc1biy8g")))

(define-public crate-ezno-parser-visitable-derive-0.0.4 (c (n "ezno-parser-visitable-derive") (v "0.0.4") (d (list (d (n "string-cases") (r "^0.2") (d #t) (k 0)) (d (n "syn-helpers") (r "^0.4.2") (d #t) (k 0)))) (h "0zy91s5yjs3znc7qafg390xzjmnp0g4crjzwr8idjdmawnhx0bcb")))

(define-public crate-ezno-parser-visitable-derive-0.0.5 (c (n "ezno-parser-visitable-derive") (v "0.0.5") (d (list (d (n "string-cases") (r "^0.2") (d #t) (k 0)) (d (n "syn-helpers") (r "^0.4.2") (d #t) (k 0)))) (h "0afmn4fhpmsd7pz3drbycvrfbiv42amnsa7cxhfj7fvb7jsl8jrs")))

(define-public crate-ezno-parser-visitable-derive-0.0.6 (c (n "ezno-parser-visitable-derive") (v "0.0.6") (d (list (d (n "string-cases") (r "^0.2") (d #t) (k 0)) (d (n "syn-helpers") (r "^0.5") (d #t) (k 0)))) (h "0l69imr29ykicdwddlpqnmkq6469rzw9fhhzag541l258xq3ay0d")))

