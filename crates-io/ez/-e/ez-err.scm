(define-module (crates-io ez -e ez-err) #:use-module (crates-io))

(define-public crate-ez-err-0.1.0 (c (n "ez-err") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "0xzr3j7ywiw95rymb9vvbdjdhpk26j3hqdi9xbpqyl4knkpvm0d0") (f (quote (("no_stacktrace") ("default")))) (y #t) (s 2) (e (quote (("log" "dep:log")))) (r "1.31")))

(define-public crate-ez-err-0.1.1 (c (n "ez-err") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "0ap9q0x3xzfqpyv1jx4kmlwxwqld0bayjzb3vi20p10bilmzaf0h") (f (quote (("no_stacktrace") ("default")))) (y #t) (s 2) (e (quote (("log" "dep:log")))) (r "1.31")))

(define-public crate-ez-err-0.1.2 (c (n "ez-err") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "1mh9px2fz4dlmsmqaf7m0hpdcnka6jz4fkn9hgj5rayl5s47k9nh") (f (quote (("no_stacktrace") ("default")))) (y #t) (s 2) (e (quote (("log" "dep:log")))) (r "1.31")))

(define-public crate-ez-err-0.1.3 (c (n "ez-err") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "0ks5crl01qspmk7bjp01v49kzkbknsqf745dwpvz5cc29vzk9qxb") (f (quote (("no_stacktrace") ("default")))) (s 2) (e (quote (("log" "dep:log")))) (r "1.31")))

