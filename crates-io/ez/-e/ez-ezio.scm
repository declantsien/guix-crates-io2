(define-module (crates-io ez -e ez-ezio) #:use-module (crates-io))

(define-public crate-ez-ezio-0.0.5-dev (c (n "ez-ezio") (v "0.0.5-dev") (d (list (d (n "eyre") (r "^0.6.6") (d #t) (k 0)) (d (n "ez") (r "^0.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "082m16pxqf5sz6s56iisf9mpvjl7y1ryz424i1y2vhswa0xwa7wk") (y #t)))

(define-public crate-ez-ezio-0.0.5-dev.1 (c (n "ez-ezio") (v "0.0.5-dev.1") (d (list (d (n "eyre") (r "^0.6.6") (d #t) (k 0)) (d (n "ez") (r "^0.0.5-dev") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "001laprcrkl67cxz81fc3jhqljgs9b4v1yby4qdkc7zl8kk7ib30") (y #t)))

(define-public crate-ez-ezio-0.0.5-dev.2 (c (n "ez-ezio") (v "0.0.5-dev.2") (d (list (d (n "eyre") (r "^0.6.6") (d #t) (k 0)) (d (n "ez") (r "^0.0.5-dev") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1zka4bzgcaxbfhv54by6rwbgavbcsy1kxgrsnry8xr69w1v0f0za") (y #t)))

(define-public crate-ez-ezio-0.0.5 (c (n "ez-ezio") (v "0.0.5") (d (list (d (n "eyre") (r "^0.6.6") (d #t) (k 0)) (d (n "ez") (r "=0.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1dbas2wyf7l9p1cvy7jb2gjna2h3hkz79r1wpkvzjh11nr355jd3") (y #t)))

(define-public crate-ez-ezio-0.0.6 (c (n "ez-ezio") (v "0.0.6") (d (list (d (n "eyre") (r "^0.6.6") (d #t) (k 0)) (d (n "ez") (r "=0.0.5") (d #t) (k 0) (p "ez")) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1piqr5wv59cp428j77j8qgx1lpjaf8xq28262fv0sd6q0z4s525s") (y #t)))

(define-public crate-ez-ezio-0.0.0-- (c (n "ez-ezio") (v "0.0.0--") (h "0lzy27za32ji9k5yk0686xbl82aq7m2jggvpb2735ia5lnkwhg3j") (y #t)))

