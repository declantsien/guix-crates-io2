(define-module (crates-io ez co ezconf) #:use-module (crates-io))

(define-public crate-ezconf-0.1.0 (c (n "ezconf") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)) (d (n "toml-query") (r "^0.6.0") (d #t) (k 0)))) (h "0b8sqf1hy0d6b2bmj9ihca1khv47cky1y1w622sc94sbgrxws4s1")))

(define-public crate-ezconf-0.2.0 (c (n "ezconf") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)) (d (n "toml-query") (r "^0.6.0") (d #t) (k 0)))) (h "1kiajhv78rvpmp0jhsfl44dmamcb8qg4qryl6fv0hr1ssb28aq0c")))

(define-public crate-ezconf-0.3.0 (c (n "ezconf") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 2)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "once_cell") (r "^0.1.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)) (d (n "toml-query") (r "^0.6.0") (d #t) (k 0)))) (h "15x85awqx7k2sz9vd9av4js08qrvmmcblfv9smr1adk2xwwb84wa")))

