(define-module (crates-io ez _a ez_audio) #:use-module (crates-io))

(define-public crate-ez_audio-0.1.0 (c (n "ez_audio") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "18768saplwaq744hzn2yn982lzds7c6766d3b49q9h4f9i9kr6lq")))

(define-public crate-ez_audio-0.1.1 (c (n "ez_audio") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1vm6x1p7csxmw1904x6674hq06j7gwyi95jfysaz35w9nvh9rjbz")))

(define-public crate-ez_audio-0.1.2 (c (n "ez_audio") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1msrnknl6vzn3v1c14ji7hyay37rkjsrchakhfbpr1nvc0ai9jr4")))

(define-public crate-ez_audio-0.1.3 (c (n "ez_audio") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "152y8qrq2kg29c82kfpjsqcv91r2s6vhmfn5hanl4xv7q2rnh7al")))

(define-public crate-ez_audio-0.1.4 (c (n "ez_audio") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1d7ns8550w89zxz9s4jzfyhwkiy567chp9nsjhng1lcbdnc61mz5")))

(define-public crate-ez_audio-0.1.5 (c (n "ez_audio") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0jxzx7w7iv3gyfklfw8ykqxcrpbvdwhdvrxmjd504d4k49myar34")))

(define-public crate-ez_audio-0.1.6 (c (n "ez_audio") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0hyavbxqlqwg28q670ybhgirxlnvm9bglf8dyyhv9n4aiqmviz9b")))

