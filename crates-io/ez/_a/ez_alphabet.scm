(define-module (crates-io ez _a ez_alphabet) #:use-module (crates-io))

(define-public crate-ez_alphabet-0.1.0 (c (n "ez_alphabet") (v "0.1.0") (d (list (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0q9kdz6jd5z40f92rp3kiqvffb90ny6i9a6p2gh1mv76gf55vsfg") (f (quote (("default")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ez_alphabet-0.1.1 (c (n "ez_alphabet") (v "0.1.1") (d (list (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1y3nh9afjivldrdi1cpz1xwsjbd4s4chns5sgjm1jj19p9izqzwv") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

