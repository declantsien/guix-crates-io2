(define-module (crates-io ez _a ez_al) #:use-module (crates-io))

(define-public crate-ez_al-0.1.0 (c (n "ez_al") (v "0.1.0") (d (list (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "linear_model_allen") (r "^0.2.1") (d #t) (k 0)))) (h "13qnzybviyg0wz7x0vpylqsc9cijpgp8bqhx11mpk8ndwx0k6nkf")))

(define-public crate-ez_al-0.1.1 (c (n "ez_al") (v "0.1.1") (d (list (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "linear_model_allen") (r "^0.2.1") (d #t) (k 0)))) (h "15vq7ys59a9bgcxkas94p91mk60z6vdvby7mpi7qg0rp223chlhx")))

(define-public crate-ez_al-0.2.0 (c (n "ez_al") (v "0.2.0") (d (list (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "linear_model_allen") (r "^0.2.1") (d #t) (k 0)))) (h "1n634gv8gzxl794j4xjzvirip01ssg67504w6dd296xnx45k2djp")))

(define-public crate-ez_al-0.3.0 (c (n "ez_al") (v "0.3.0") (d (list (d (n "linear_model_allen") (r "^0.2.1") (d #t) (k 0)) (d (n "wavers") (r "^1.4.2") (d #t) (k 0)))) (h "1wyh2b7jg5350bdk7i5m76akkn5qi5r95gb1fj2ipm31hl2v82il")))

