(define-module (crates-io ez k- ezk-sdp-types) #:use-module (crates-io))

(define-public crate-ezk-sdp-types-0.1.0 (c (n "ezk-sdp-types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "bytesstr") (r "^1") (d #t) (k 0)) (d (n "internal") (r "^0.1") (d #t) (k 0) (p "ezk-internal")) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18igyzq7a51j2vpl43wgx9ps95r02dmc2v2980jm7fipahq2rcv9")))

(define-public crate-ezk-sdp-types-0.1.1 (c (n "ezk-sdp-types") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "bytesstr") (r "^1") (d #t) (k 0)) (d (n "internal") (r "^0.1") (d #t) (k 0) (p "ezk-internal")) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15qd0x7zbsdp5q1bfgyc8jgaw7qwvvgrvmhk3jrw4vm2lsb1ch2n")))

