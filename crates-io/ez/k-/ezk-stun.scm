(define-module (crates-io ez k- ezk-stun) #:use-module (crates-io))

(define-public crate-ezk-stun-0.1.0 (c (n "ezk-stun") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "stun-types") (r "^0.1") (d #t) (k 0) (p "ezk-stun-types")) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (d #t) (k 0)))) (h "0ygw7n3xfr1b7mxfam5xf2in1rvxj40ayhabs14gmnqq5kf1yxcx")))

(define-public crate-ezk-stun-0.1.1 (c (n "ezk-stun") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "stun-types") (r "^0.1.1") (d #t) (k 0) (p "ezk-stun-types")) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (d #t) (k 0)))) (h "193fwqs06kx3w9mk17f8ndd7h2gd5l5ckxxpmx3w2r41wcrf37jq")))

(define-public crate-ezk-stun-0.2.0 (c (n "ezk-stun") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "stun-types") (r "^0.1.1") (d #t) (k 0) (p "ezk-stun-types")) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (d #t) (k 0)))) (h "0783lpx8sqq9pzpnh3h9m9nfcwkyry58n14cq084qs4zq4vq7xca")))

