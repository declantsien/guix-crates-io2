(define-module (crates-io ez cg ezcgmath) #:use-module (crates-io))

(define-public crate-ezcgmath-0.0.1 (c (n "ezcgmath") (v "0.0.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)))) (h "1h7xv1g20ajlvdxha5x0y9dl51ws92msrz36bvanj55c8v1k1l1l")))

(define-public crate-ezcgmath-0.0.2 (c (n "ezcgmath") (v "0.0.2") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)))) (h "1lf434sfc4rn9pn0iv10icx7cwj6czw8jphv65l0c845sfkv811q")))

(define-public crate-ezcgmath-0.0.3 (c (n "ezcgmath") (v "0.0.3") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)))) (h "1ckyhw1x65dcl35bf7k0v2gn5amcxqwdi4h1f5bypmp3r4nhzbxg")))

(define-public crate-ezcgmath-0.0.4 (c (n "ezcgmath") (v "0.0.4") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)))) (h "1y039m0srwzp20ai1zrrd9nqqm97dbh0rzycm1p8f5gmy6sl46rw")))

