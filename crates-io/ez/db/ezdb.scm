(define-module (crates-io ez db ezdb) #:use-module (crates-io))

(define-public crate-EZDB-0.0.1 (c (n "EZDB") (v "0.0.1") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)))) (h "0yxwziybnlxymgx2ri74il69i2g16axmy0dm2sm8r6n7w40q5mxs")))

(define-public crate-EZDB-0.0.2 (c (n "EZDB") (v "0.0.2") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)))) (h "0y4r69hf19lh6mv77gm79ddcw92vn6cyzwrraaz22z5g57lf9xap")))

(define-public crate-EZDB-0.0.3 (c (n "EZDB") (v "0.0.3") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)))) (h "113fifwax8lkvm71j68b3iwdkh5a7wgqlzc2wi85imlkwi3481v4")))

(define-public crate-EZDB-0.0.4 (c (n "EZDB") (v "0.0.4") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)))) (h "01i8gib4m328qjh68w596ixh4hxawrgfinlj2p4i624mxa65q5jl")))

(define-public crate-EZDB-0.0.5 (c (n "EZDB") (v "0.0.5") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)))) (h "1dif6930hr4mjbxabia58j0wz68xqi44qnzwm8vsjckp3mhcm6kh")))

(define-public crate-EZDB-0.0.6 (c (n "EZDB") (v "0.0.6") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)))) (h "1lpq1jaz3f64p4aqdna51y6sh2ii88bkmg7nalydiq2ijp61v5mz")))

(define-public crate-EZDB-0.0.7 (c (n "EZDB") (v "0.0.7") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19q9bd43pk67a6ra89llwxlcml6csxwpssc2v6jz15h4w62pv023")))

(define-public crate-EZDB-0.0.8 (c (n "EZDB") (v "0.0.8") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rug") (r "^1.22.0") (d #t) (k 0)))) (h "1kpf0hkm5j4dr5m3pf3c241s28p0ady5spk1jrsvycpg5b7yz8mc")))

(define-public crate-EZDB-0.1.0 (c (n "EZDB") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rug") (r "^1.22.0") (d #t) (k 0)))) (h "01v1vvka4ya5p5ps7bfr1hb90zh6gjphs24h395b9c2ny24qxibj")))

(define-public crate-EZDB-0.1.1 (c (n "EZDB") (v "0.1.1") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.0") (f (quote ("getrandom" "static_secrets"))) (d #t) (k 0)))) (h "02mwjpkyincmfisg3dca8ifyis4jikd2qn1qc0xj6f0mxrpqxjxq")))

(define-public crate-EZDB-0.1.11 (c (n "EZDB") (v "0.1.11") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.0") (f (quote ("getrandom" "static_secrets"))) (d #t) (k 0)))) (h "1f075j37hljvp7ksnpnnsd1xk0l857yhd7n10w6118dqfq9ignbz")))

(define-public crate-EZDB-0.1.12 (c (n "EZDB") (v "0.1.12") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.0") (f (quote ("getrandom" "static_secrets"))) (d #t) (k 0)))) (h "1q86khyc91b909jmr5wyn7y9sfg1x45kfh4s4cq23y8gbd91wnrb")))

(define-public crate-EZDB-0.1.13 (c (n "EZDB") (v "0.1.13") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "brotli") (r "^3.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.0") (f (quote ("getrandom" "static_secrets"))) (d #t) (k 0)))) (h "099c673ddr01gyz4djmxi2lkv17swfrlpvbwpx8xysxkrvlyk78j")))

(define-public crate-EZDB-0.1.14 (c (n "EZDB") (v "0.1.14") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.0") (f (quote ("getrandom" "static_secrets"))) (d #t) (k 0)))) (h "0s47d217r5qvqb4p0pblwzh2ghrdghfb8s5363j7azhb8fqr6692")))

(define-public crate-EZDB-0.1.15 (c (n "EZDB") (v "0.1.15") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.0") (f (quote ("getrandom" "static_secrets"))) (d #t) (k 0)))) (h "0dzqcmgk1ylwyrvhdkig2a1i4cpv4f9xwph7qr2xsnnqh9cfn0sb")))

