(define-module (crates-io ez el ezel) #:use-module (crates-io))

(define-public crate-ezel-0.0.1 (c (n "ezel") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "piet") (r "^0.5.0") (d #t) (k 0)) (d (n "piet-common") (r "^0.5.0") (d #t) (k 0)) (d (n "piet-svg") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "polars") (r "^0.20.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "110gzfq7pvbxkafza0ilzg5xqmn22a5fw23axayqf209ja25lap7") (f (quote (("svg" "piet-svg") ("png" "piet/png" "piet-common/png") ("gg") ("default" "png" "svg" "advise") ("advise"))))))

