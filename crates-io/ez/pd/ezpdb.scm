(define-module (crates-io ez pd ezpdb) #:use-module (crates-io))

(define-public crate-ezpdb-0.1.0 (c (n "ezpdb") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pdb") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hkrfg4ik9v62g4k8qrxshywfa11199j1lgldb46asmcw59874kj")))

(define-public crate-ezpdb-0.1.1 (c (n "ezpdb") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pdb") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ycx4a0cx0kr4hfyxdcbr2fb7yyxdpf9bq9bz8k06cqjpd7zswxh")))

(define-public crate-ezpdb-0.1.2 (c (n "ezpdb") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pdb") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q58hbjnm5m22rqs0w079x1v7awh33pszlm3ilzn01i8dr21gd6p")))

(define-public crate-ezpdb-0.2.0 (c (n "ezpdb") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pdb") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0brbl8y5wkf7dy2pqzsmdjv25bdjlkghd9j3m2zb8jknf440czys")))

(define-public crate-ezpdb-0.3.0 (c (n "ezpdb") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pdb") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "05vgg24jmz9vy0dncmcs8nxp4b636248q9ijwbx6prz1bvdkpwn0")))

(define-public crate-ezpdb-0.3.2 (c (n "ezpdb") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pdb") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1vkmi1nsz2zalnmws6l5122802xpk9lm729y09xzapascxiqkm60")))

(define-public crate-ezpdb-0.3.3 (c (n "ezpdb") (v "0.3.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pdb") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1y2j27n22jnf4sidwaiax2q0572d5iknpdvmqn8cs5d7w3r7pwhy")))

(define-public crate-ezpdb-0.4.0 (c (n "ezpdb") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pdb") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "09m6ifysik7q3gk2f2rb7yqmr1nqd4259p2ljlpl70sl6xck5f88")))

(define-public crate-ezpdb-0.5.0 (c (n "ezpdb") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pdb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (d #t) (k 0)))) (h "1wkr97c1fd6vzl1a64w37ycxyxcqayd91nrz5a50ykpamha3pk2j")))

(define-public crate-ezpdb-0.6.0 (c (n "ezpdb") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pdb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (d #t) (k 0)))) (h "0kgx8x9ykflzy5ky2kalgk8h8sghp95607jrl3i9kg6mnhlwxfbp")))

