(define-module (crates-io ez _c ez_config) #:use-module (crates-io))

(define-public crate-ez_config-0.1.0 (c (n "ez_config") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1825r7z9w2a2vw5prn6fjp83g8876hjkb2c05si9plbqwyw2wwj5")))

(define-public crate-ez_config-0.1.1 (c (n "ez_config") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f1ad9chxiyxzz6insc9xg4y75c3gzwpmc8whzcd3sn9z1wxqgql")))

