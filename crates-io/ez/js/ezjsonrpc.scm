(define-module (crates-io ez js ezjsonrpc) #:use-module (crates-io))

(define-public crate-ezjsonrpc-0.1.0 (c (n "ezjsonrpc") (v "0.1.0") (d (list (d (n "ezjsonrpc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.35") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^0.1.14") (d #t) (k 2)))) (h "1l18hzzsxg05dnkj9ga5657w9xf67pkd7sb59hbmq0iiivm2kr8j")))

(define-public crate-ezjsonrpc-0.1.2 (c (n "ezjsonrpc") (v "0.1.2") (d (list (d (n "ezjsonrpc-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.35") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^0.1.14") (d #t) (k 2)))) (h "063vh9zagxb83kaff1c1pxcwkjfg8ksffyfzvy2gbl2v3bqskyg1")))

