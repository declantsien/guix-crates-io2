(define-module (crates-io ez js ezjs) #:use-module (crates-io))

(define-public crate-ezjs-0.1.0 (c (n "ezjs") (v "0.1.0") (h "1hy0msij8k65c669gjaj7k10kjh42qjay9iknqgcc5q47if7ybvv")))

(define-public crate-ezjs-0.1.1 (c (n "ezjs") (v "0.1.1") (h "1qv53jdbwfi20mljp9i34spy0nmvznrnmn1qx5j0qch74fwx1ppl")))

