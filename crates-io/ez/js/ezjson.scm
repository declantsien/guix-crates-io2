(define-module (crates-io ez js ezjson) #:use-module (crates-io))

(define-public crate-ezjson-0.1.0 (c (n "ezjson") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1dvmn117k7mk1lxc650cx172bi0dkkdapmg12p9g87kmd6gvcw1i")))

(define-public crate-ezjson-0.1.1 (c (n "ezjson") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1v0b2iaghsi5k5qx2c4bwr8cxdifr13pbmj00jnbkv05c0c0b5kg")))

(define-public crate-ezjson-0.2.0 (c (n "ezjson") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "090h2cm0x6pffjas94w50yqw6x90wh04dy3yc8xvarda4xp78rk7")))

