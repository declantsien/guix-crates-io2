(define-module (crates-io ez js ezjsonrpc-macros) #:use-module (crates-io))

(define-public crate-ezjsonrpc-macros-0.1.0 (c (n "ezjsonrpc-macros") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zc6riqr3fhvglg9mdab9qs6d9pchhk1g6gw4sgrbm73h69vm3dp")))

(define-public crate-ezjsonrpc-macros-0.1.2 (c (n "ezjsonrpc-macros") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hx8z5yv4h3favr1pd9db3vym8nzlq42qkahn2cif69gbq8rfy1p")))

