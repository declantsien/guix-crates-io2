(define-module (crates-io ez me ezmem) #:use-module (crates-io))

(define-public crate-ezmem-0.1.1 (c (n "ezmem") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("tlhelp32" "stralign" "handleapi" "winbase" "memoryapi" "wow64apiset"))) (d #t) (k 0)))) (h "0wys9y3cya5rzmb5fn7sw9xfainwx7v8nkdkjwqlv28q0zn7xrih")))

(define-public crate-ezmem-0.1.2 (c (n "ezmem") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("tlhelp32" "stralign" "handleapi" "winbase" "memoryapi" "wow64apiset"))) (d #t) (k 0)))) (h "0sksfchyxzjlj7r22zzaaf8p42jajzmlha8b3cvld9i6bkdijpz1")))

(define-public crate-ezmem-0.1.3 (c (n "ezmem") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("tlhelp32" "stralign" "handleapi" "winbase" "memoryapi" "wow64apiset"))) (d #t) (k 0)))) (h "1fm5lvc157shpf6izwgdazbasq4xx60n8c13f1xyrwnlhv4lp4wh")))

(define-public crate-ezmem-0.2.0 (c (n "ezmem") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("tlhelp32" "stralign" "handleapi" "winbase" "memoryapi" "wow64apiset" "errhandlingapi"))) (d #t) (k 0)))) (h "140rappddrpdm6rjgswx734qn3s1k7npkdvlyr8k9nga4hiwc7vn")))

