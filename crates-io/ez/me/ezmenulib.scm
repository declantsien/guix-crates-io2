(define-module (crates-io ez me ezmenulib) #:use-module (crates-io))

(define-public crate-ezmenulib-0.2.7 (c (n "ezmenulib") (v "0.2.7") (h "0fy3r0jpb0q5lpi3bdrias3ksqv89yp82vnjh7ady4z23v36jph6")))

(define-public crate-ezmenulib-0.2.9 (c (n "ezmenulib") (v "0.2.9") (h "0xnqcw9mfb3i54qms8pxblx19hiwzvnpyc87g41xk1ifhiw0q426")))

(define-public crate-ezmenulib-0.2.10 (c (n "ezmenulib") (v "0.2.10") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)))) (h "0wf1nfxsc5adcjlc8diinnahnpv3rwdiy4g1wqww2i3nchv462d7") (f (quote (("default") ("date" "chrono"))))))

