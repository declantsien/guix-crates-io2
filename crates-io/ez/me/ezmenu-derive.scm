(define-module (crates-io ez me ezmenu-derive) #:use-module (crates-io))

(define-public crate-ezmenu-derive-0.1.0 (c (n "ezmenu-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0q048n5hfr3597l5nprgcp8hb6blym6f3sdlhm1cks1f3i5wm0yl")))

(define-public crate-ezmenu-derive-0.2.0 (c (n "ezmenu-derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1rly7d0z91ljz5pj9f10isn73msx75psfwv8zz689ahva6ckbcmd") (f (quote (("custom_io"))))))

(define-public crate-ezmenu-derive-0.2.3 (c (n "ezmenu-derive") (v "0.2.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1cj54mfj572h94ghlpqx3538cx99iyllfjxgvivjapjh3ni420cy") (f (quote (("custom_io"))))))

(define-public crate-ezmenu-derive-0.2.4 (c (n "ezmenu-derive") (v "0.2.4") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1s8wdhskkpdbjwl5v7s5zrbp6dq8j9nkc8lfl0c3nr8xwnpvr0g5") (f (quote (("parsed_attr") ("derive") ("default" "derive" "parsed_attr"))))))

(define-public crate-ezmenu-derive-0.2.5 (c (n "ezmenu-derive") (v "0.2.5") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mgka2f7k79vrqq0h8v8aqgb49qdz01rlqys5kzpzgk20jpqx79p") (f (quote (("parsed_attr") ("derive") ("default" "derive" "parsed_attr"))))))

