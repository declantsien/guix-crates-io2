(define-module (crates-io ez me ezmenu-macros) #:use-module (crates-io))

(define-public crate-ezmenu-macros-0.2.7 (c (n "ezmenu-macros") (v "0.2.7") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18fifd9dilw17nxwccbr55d533x38rdy4akwrg7g83ksz7nvznfa") (f (quote (("parsed") ("derive") ("default" "derive" "parsed"))))))

