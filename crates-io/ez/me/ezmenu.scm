(define-module (crates-io ez me ezmenu) #:use-module (crates-io))

(define-public crate-ezmenu-0.1.0 (c (n "ezmenu") (v "0.1.0") (d (list (d (n "ezmenu-derive") (r "=0.1.0") (d #t) (k 0)))) (h "0g29fxrikkp5s93pnzplk8dblz2rr5a1c395d02ks9q8khd57zyf")))

(define-public crate-ezmenu-0.1.1 (c (n "ezmenu") (v "0.1.1") (d (list (d (n "ezmenu-derive") (r "=0.1.0") (d #t) (k 0)))) (h "1ahkyffdhrpwd352yr3syl5i5mmqs1ih0bzwwlvpnb4i3mh03f03")))

(define-public crate-ezmenu-0.2.0 (c (n "ezmenu") (v "0.2.0") (d (list (d (n "ezmenu-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0vvwrp4iw0wap0wjwi7wnx6yvbb376129xvng5dcwr633fzn0h49") (f (quote (("custom_io" "ezmenu-derive/custom_io"))))))

(define-public crate-ezmenu-0.2.3 (c (n "ezmenu") (v "0.2.3") (d (list (d (n "ezmenu-derive") (r "^0.2.3") (d #t) (k 0)))) (h "063p1dba34ixpxg2spjx8a7jxsqc0cwyjb6k6wh6492249raly0k") (f (quote (("derive") ("default" "derive") ("custom_io" "ezmenu-derive/custom_io"))))))

(define-public crate-ezmenu-0.2.4 (c (n "ezmenu") (v "0.2.4") (d (list (d (n "ezmenu-derive") (r "^0.2.3") (d #t) (k 0)))) (h "12h7b9pjhaiyrsp9rprn21j3nbqav8lq0i1i4yci445baq5g1cv6") (f (quote (("parsed_attr" "ezmenu-derive/parsed_attr") ("derive" "ezmenu-derive/derive") ("default" "derive" "parsed_attr"))))))

(define-public crate-ezmenu-0.2.5 (c (n "ezmenu") (v "0.2.5") (d (list (d (n "ezmenu-derive") (r "^0.2.3") (d #t) (k 0)))) (h "09p83v8kaq2i3nch6vmlghfqy0ibwkf20msv7qlps0z0j5la2c8q") (f (quote (("parsed_attr" "ezmenu-derive/parsed_attr") ("derive" "ezmenu-derive/derive") ("default" "derive" "parsed_attr"))))))

(define-public crate-ezmenu-0.2.7 (c (n "ezmenu") (v "0.2.7") (h "047qik360c720j9sm4r1ak89vp9jl0fszqhbgidqhilzj1vf5lrw")))

(define-public crate-ezmenu-0.2.8 (c (n "ezmenu") (v "0.2.8") (d (list (d (n "ezmenu") (r "=0.2.7") (d #t) (k 0)) (d (n "ezmenu-macros") (r "^0.2.5") (d #t) (k 0)))) (h "1xprvcpmp685xrbzdqlyprjll84x6a59a2yfay21zs4q8l65dyb4") (f (quote (("parsed" "ezmenu-macros/parsed") ("derive" "ezmenu-macros/derive") ("default" "derive" "parsed"))))))

(define-public crate-ezmenu-0.2.9 (c (n "ezmenu") (v "0.2.9") (d (list (d (n "ezmenu-macros") (r "^0.2.5") (d #t) (k 0)) (d (n "ezmenulib") (r "^0.2.7") (d #t) (k 0)))) (h "0wrsznm529s55b6smyn8pn73d3847zf6h86g5fj3sb7w2gbhximw") (f (quote (("parsed" "ezmenu-macros/parsed") ("derive" "ezmenu-macros/derive") ("default" "derive" "parsed"))))))

