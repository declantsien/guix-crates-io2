(define-module (crates-io ez st ezstress) #:use-module (crates-io))

(define-public crate-ezstress-2.0.3 (c (n "ezstress") (v "2.0.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "ocl") (r "^0.19.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27.6") (d #t) (k 0)))) (h "1dszl6xf87qb1m5g9pfr412byznhi54njvbf1v2sdx6wds78gyjg")))

