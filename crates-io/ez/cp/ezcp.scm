(define-module (crates-io ez cp ezcp) #:use-module (crates-io))

(define-public crate-ezcp-0.1.0 (c (n "ezcp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1iyx3hv5bqjnc31djy4zslgmyznp1apvz9c83n663kpbckb8f9ld")))

(define-public crate-ezcp-0.1.1 (c (n "ezcp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0c9qzx5wb9zlagdv9k7cvki5pd9ad9rdwk4yazdjr3cf2ipgrfg3")))

(define-public crate-ezcp-0.1.2 (c (n "ezcp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1jm0jsqa58i03zrryq73agpzdl5iirncff05kn6a5bzhvwr6c4wg")))

(define-public crate-ezcp-0.2.0 (c (n "ezcp") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "15c3f3l715ydjh9vmn7vg91lng0apis7hy2xiyrra4x3fpda29xy")))

(define-public crate-ezcp-0.2.1 (c (n "ezcp") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std" "backtrace"))) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fs_extra") (r "^1.3") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 0)) (d (n "termsize") (r "^0.1") (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "1kvk5d6m579q386s6rihb6mj3dxlzzydg5j5s7pz1iy6jgwfdb75")))

(define-public crate-ezcp-0.2.2 (c (n "ezcp") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std" "backtrace"))) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fs_extra") (r "^1.3") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 0)) (d (n "termsize") (r "^0.1") (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "0905q29xg37vb1ml1kjcyf3kvf6xkv2f6kq53z0kg6pq4a3d0frp")))

(define-public crate-ezcp-0.3.0 (c (n "ezcp") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std" "backtrace"))) (k 0)) (d (n "bincode") (r "^1.3") (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fs_extra") (r "^1.3") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snap") (r "^1.1") (k 0)) (d (n "termsize") (r "^0.1") (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "17z48a4d2816isc0db8fg0c24cckgvybz1d9g488w81dl24b2ra6")))

