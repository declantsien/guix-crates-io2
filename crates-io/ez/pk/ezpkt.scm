(define-module (crates-io ez pk ezpkt) #:use-module (crates-io))

(define-public crate-ezpkt-0.1.2 (c (n "ezpkt") (v "0.1.2") (d (list (d (n "pkt") (r "^0.1.2") (d #t) (k 0)))) (h "1bx11qb2xgzrr0fzxiwjmmzqj3xnfdvinzh7lxv4rmckwzbjmr76")))

(define-public crate-ezpkt-0.1.3 (c (n "ezpkt") (v "0.1.3") (d (list (d (n "pkt") (r "^0.1.3") (d #t) (k 0)))) (h "1j82dkycspxcicllbcchk31vz3j2r75h6kpg0nva22gcwwkj371n")))

(define-public crate-ezpkt-0.1.4 (c (n "ezpkt") (v "0.1.4") (d (list (d (n "pkt") (r "^0.1.4") (d #t) (k 0)))) (h "1xg20lxv7mi918bbw4dyl2rplmqgilwgjpvfpvms43602a0sbqch")))

(define-public crate-ezpkt-0.1.5 (c (n "ezpkt") (v "0.1.5") (d (list (d (n "pkt") (r "^0.1.5") (d #t) (k 0)))) (h "0pl45gvi6w80fcdva9gkixmhb5iyw87kdvjnil8idwgg2hxpdihq")))

(define-public crate-ezpkt-0.1.6 (c (n "ezpkt") (v "0.1.6") (d (list (d (n "pkt") (r "^0.1.6") (d #t) (k 0)))) (h "1qh452zbdhdd926hhnvgfr9sggdj7xs7v5865iy6rcf82pc2g5zm")))

(define-public crate-ezpkt-0.1.7 (c (n "ezpkt") (v "0.1.7") (d (list (d (n "pkt") (r "^0.1.7") (d #t) (k 0)))) (h "0d99d3nzp62svg9hn1mwzqs5c4kh77k1j1vpy50bagk3v03q6hg3")))

(define-public crate-ezpkt-0.1.8 (c (n "ezpkt") (v "0.1.8") (d (list (d (n "pkt") (r "^0.1.8") (d #t) (k 0)))) (h "17w5nbzm9aw31ww8par7www21wr25nzncm1rkqprin0vagv5axff")))

(define-public crate-ezpkt-0.1.9 (c (n "ezpkt") (v "0.1.9") (d (list (d (n "pkt") (r "^0.1.9") (d #t) (k 0)))) (h "1pfn9n1ghkx84ijfzaqqxylbwqws2yycngswsihiqnrww2cbq0r7")))

(define-public crate-ezpkt-0.1.10 (c (n "ezpkt") (v "0.1.10") (d (list (d (n "pkt") (r "^0.1.10") (d #t) (k 0)))) (h "1mq3758haf4bnll981dxkcgrsvk9ch9z8g99dyi6s5yas45h55md")))

(define-public crate-ezpkt-0.1.11 (c (n "ezpkt") (v "0.1.11") (d (list (d (n "pkt") (r "^0.1.11") (d #t) (k 0)))) (h "0ybr17swwsqpc3nyhrqq6lqlmiqd86v8w1k7kwrjx06yh1l2p324")))

(define-public crate-ezpkt-0.2.0 (c (n "ezpkt") (v "0.2.0") (d (list (d (n "pkt") (r "^0.2.0") (d #t) (k 0)))) (h "0wjfmd0rx4mrn5900vxaqrn1nxh8dfskzq7mszqsms1ykmb0cv87")))

