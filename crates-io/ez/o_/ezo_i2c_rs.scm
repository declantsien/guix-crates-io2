(define-module (crates-io ez o_ ezo_i2c_rs) #:use-module (crates-io))

(define-public crate-ezo_i2c_rs-0.1.0 (c (n "ezo_i2c_rs") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0h0d7jbkbgngbhnbl9jila5zpbs7jhz60wpdpxw898sf7r3mg11n")))

(define-public crate-ezo_i2c_rs-0.2.0 (c (n "ezo_i2c_rs") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0hfxpcdmqrv0jz7c2narr4nhw6cq0fc7j6q02d8mb0k8vmd1pc8g")))

