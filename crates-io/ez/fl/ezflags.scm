(define-module (crates-io ez fl ezflags) #:use-module (crates-io))

(define-public crate-ezflags-0.1.0 (c (n "ezflags") (v "0.1.0") (h "1bwwiabww8bzg2k2wy6awgk4fi78qhab2f52slk9p9gid9ixq6j1")))

(define-public crate-ezflags-0.1.1 (c (n "ezflags") (v "0.1.1") (h "0cqz4aqa4k5brc8p948m64zy6nzgm4l7gj77wjbw38jb5b60aj47")))

(define-public crate-ezflags-0.1.2 (c (n "ezflags") (v "0.1.2") (h "0brw7nzpyr209c0lga1mpz3cww8c8bbny7shwq2jgbiig5b634b5")))

(define-public crate-ezflags-0.1.3 (c (n "ezflags") (v "0.1.3") (h "1fnmg78q6xcbqip5yx1dz9c412v4ydrplacpclba46iics1fd90i")))

