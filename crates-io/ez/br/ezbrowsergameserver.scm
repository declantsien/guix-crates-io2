(define-module (crates-io ez br ezbrowsergameserver) #:use-module (crates-io))

(define-public crate-ezbrowsergameserver-0.1.0 (c (n "ezbrowsergameserver") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.20.1") (d #t) (k 0)))) (h "1jq3pxw4nw6c8rl6mhxq33d57bv3khnvxa7qkw3z48x4igclm4gm")))

(define-public crate-ezbrowsergameserver-0.2.0 (c (n "ezbrowsergameserver") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.20.1") (d #t) (k 0)))) (h "10bf4gridgnjg68xhynnfydlfa6cwn70d55v1mbs0hazhfjb8838")))

(define-public crate-ezbrowsergameserver-0.2.1 (c (n "ezbrowsergameserver") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.20.1") (d #t) (k 0)))) (h "1dg5n3k4gx7497519ihzgix7sr828d6h3ymcgs69nhiicylri5y7")))

(define-public crate-ezbrowsergameserver-0.2.2 (c (n "ezbrowsergameserver") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.20.1") (d #t) (k 0)))) (h "12fd1zl2zzbicx25b554ng5zkv3hyyy3hrlfnqym7b0chaczi04b")))

