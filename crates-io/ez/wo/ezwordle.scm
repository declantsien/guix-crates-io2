(define-module (crates-io ez wo ezwordle) #:use-module (crates-io))

(define-public crate-ezwordle-0.1.0 (c (n "ezwordle") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "0wgnf1hjnj3kp6xbni0y857hh96j36j5zv7qsdqh26sp4z6dfs9x")))

(define-public crate-ezwordle-0.1.1 (c (n "ezwordle") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "17l9m8v24xq9g2ix4n2p3miy4gyqbzswwy5dwbqv4ai3bkvnfjac")))

(define-public crate-ezwordle-0.2.1 (c (n "ezwordle") (v "0.2.1") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "0gvq4v19n456l9k6qm84jvnc2n9aj9d8nmwjfkl73ifvcmfmz08g")))

(define-public crate-ezwordle-0.3.0 (c (n "ezwordle") (v "0.3.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "08nadd6bb49xkfzqn7g4w750ks9d4a1j29y0r83ivhkgh2jzvcpw")))

(define-public crate-ezwordle-0.4.0 (c (n "ezwordle") (v "0.4.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "0i1zkxh54kwv208lwswf79wp1k83lgldc0c0z11mf1i8klvwvj9x")))

(define-public crate-ezwordle-0.4.1 (c (n "ezwordle") (v "0.4.1") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "0spa8vjhmxgrnqr8qa5fb7fvmhhw54mzprl9swjp5s6j87piwmsh")))

