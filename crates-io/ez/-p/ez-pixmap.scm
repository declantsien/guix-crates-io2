(define-module (crates-io ez -p ez-pixmap) #:use-module (crates-io))

(define-public crate-ez-pixmap-0.1.0 (c (n "ez-pixmap") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)))) (h "1yxj8zx3w6lqpxs1wxkpzmndgf1rrrrn04dxsv2vyjn645q2dqr0") (y #t)))

(define-public crate-ez-pixmap-0.1.1 (c (n "ez-pixmap") (v "0.1.1") (d (list (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)))) (h "1bry4bip579rzr07rglrgb8k2y596z25gc9260wv08f3ly4b09vz") (y #t)))

(define-public crate-ez-pixmap-0.1.2 (c (n "ez-pixmap") (v "0.1.2") (d (list (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1qksxw4n6f4dj5z407qhpjk9cq89ifljda1ccsxqnb8bsjspvl11")))

(define-public crate-ez-pixmap-0.1.3 (c (n "ez-pixmap") (v "0.1.3") (d (list (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0saixgbv86yvbz5jnbx130g1wia0abvbp7q35wchn5jfj0pnvxqm")))

(define-public crate-ez-pixmap-0.2.0 (c (n "ez-pixmap") (v "0.2.0") (d (list (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "minifb") (r "^0.19") (d #t) (k 2)))) (h "19byign9v224i0akb8d268pzs0cal875xywnzzmln4zyrjzhq2rj")))

(define-public crate-ez-pixmap-0.2.1 (c (n "ez-pixmap") (v "0.2.1") (d (list (d (n "fltk") (r "^0.15") (f (quote ("fltk-bundled"))) (d #t) (k 2)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "minifb") (r "^0.19") (d #t) (k 2)) (d (n "ufb") (r "^0.1") (d #t) (k 2)))) (h "0qn9miyvqm7zl8nvzmk5d340bg14avksgszrg5w2p54rmqn8pk12")))

(define-public crate-ez-pixmap-0.2.2 (c (n "ez-pixmap") (v "0.2.2") (d (list (d (n "color-maps") (r "^0.1") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (f (quote ("fltk-bundled"))) (d #t) (k 2)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "minifb") (r "^0.19") (d #t) (k 2)) (d (n "ufb") (r "^0.1.2") (d #t) (k 2)))) (h "0kdlx0hfg596nzbm5ymdl011ma57410bxsa90fj6c1j0q89flvnv")))

