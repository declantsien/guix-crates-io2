(define-module (crates-io ez -p ez-proc-macro) #:use-module (crates-io))

(define-public crate-ez-proc-macro-0.0.2-dev.5 (c (n "ez-proc-macro") (v "0.0.2-dev.5") (d (list (d (n "ez-internal") (r "^0.0.2-dev.5") (d #t) (k 0)))) (h "170x3n76bv479rkjap5wg26iy3a6mf6c3092qhv95nz5n926bmpi") (y #t)))

(define-public crate-ez-proc-macro-0.0.2-ez.0 (c (n "ez-proc-macro") (v "0.0.2-ez.0") (d (list (d (n "ez-internal") (r "^0.0.2-ez.0") (d #t) (k 0)))) (h "0gd72hpgilz9ldd59fh39m761fh9d4d93zs1c9z0q7lkivcfk27s") (y #t)))

(define-public crate-ez-proc-macro-0.0.2-ez.1 (c (n "ez-proc-macro") (v "0.0.2-ez.1") (d (list (d (n "ez-internal") (r "^0.0.2-ez.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1jcjrbmrlcnc2chkz4ady64bpgckvi4d6f7j760mr5l6mhxqj2jl") (y #t)))

(define-public crate-ez-proc-macro-0.0.2-ez.2 (c (n "ez-proc-macro") (v "0.0.2-ez.2") (d (list (d (n "ez-internal") (r "^0.0.2-ez.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1wrv8gcv12vmxqxc9a1x5l5kxw8v7lxh3g679868z8zfz8sr7jjp") (y #t)))

(define-public crate-ez-proc-macro-0.0.2-ez.3 (c (n "ez-proc-macro") (v "0.0.2-ez.3") (d (list (d (n "ez-internal") (r "^0.0.2-ez.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0g9396yl75zpg05dji8wk2ix6cgqqwqzz0pfr7sh4dlwi82pz80n") (y #t)))

(define-public crate-ez-proc-macro-0.0.2-ez.4 (c (n "ez-proc-macro") (v "0.0.2-ez.4") (d (list (d (n "ez-internal") (r "^0.0.2-ez.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0mja1r916c2405y9x8cyb8b8xpg5v32wrb1ddxn3k002s98khdm5") (y #t)))

(define-public crate-ez-proc-macro-0.0.2-ez.5 (c (n "ez-proc-macro") (v "0.0.2-ez.5") (d (list (d (n "ez-internal") (r "^0.0.2-ez.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit" "visit-mut" "fold" "derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "0wg43q7i0x9aspq5lc5mf7b635rxxa82g2yab60c8cdzdq2a2h8l") (y #t)))

(define-public crate-ez-proc-macro-0.0.2-ez.6 (c (n "ez-proc-macro") (v "0.0.2-ez.6") (d (list (d (n "ez-internal") (r "^0.0.2-ez.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit" "visit-mut" "fold" "derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "16fbqyf6nfaab60ncmday8scn0vyp0yzj64fgw4w0gahb59qng20") (y #t)))

(define-public crate-ez-proc-macro-0.0.2-ez.9 (c (n "ez-proc-macro") (v "0.0.2-ez.9") (d (list (d (n "ez-internal") (r "^0.0.2-ez.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit" "visit-mut" "fold" "derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "1jxh7h0ji8fjb1lkqbyf3g2rhjp12bqgz03xrh9lzqghw77wf852") (y #t)))

(define-public crate-ez-proc-macro-0.0.2 (c (n "ez-proc-macro") (v "0.0.2") (d (list (d (n "ez-internal") (r "^0.0.2-ez.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit" "visit-mut" "fold" "derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "0bpf6p9mdgvw48jg3ghahiw40i8rmn2sc2ij7d6xflkm9anim3gn") (y #t)))

(define-public crate-ez-proc-macro-0.0.3 (c (n "ez-proc-macro") (v "0.0.3") (d (list (d (n "ez-impl") (r "^0.0.3-dev") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit" "visit-mut" "fold" "derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "1saldc8m58ailc1w7hsdrgpjdd7683jixr4yv3lbiqh7vpr1l1n6") (y #t)))

(define-public crate-ez-proc-macro-0.0.4 (c (n "ez-proc-macro") (v "0.0.4") (d (list (d (n "ez-impl") (r "^0.0.4-dev") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit" "visit-mut" "fold" "derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "0ndldbi4h3il3k66ynr82cpqrn03ibyk1lkhw4p8mwb8gyjx5kck") (y #t)))

(define-public crate-ez-proc-macro-0.0.5-dev (c (n "ez-proc-macro") (v "0.0.5-dev") (d (list (d (n "ez-impl") (r "^0.0.5-dev") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit" "visit-mut" "fold" "derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "0qqvm4d05sw6611hcnyqyjly1i989v720nffsffwnd2kdikgll93") (y #t)))

(define-public crate-ez-proc-macro-0.0.5-dev.2 (c (n "ez-proc-macro") (v "0.0.5-dev.2") (d (list (d (n "ez-impl") (r "^0.0.5-dev.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit" "visit-mut" "fold" "derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "1580hwwrr167qpbimzjbh0vvbbs4i3hk77ypfscaxxl518wzj46h") (y #t)))

(define-public crate-ez-proc-macro-0.0.5 (c (n "ez-proc-macro") (v "0.0.5") (d (list (d (n "ez-impl") (r "^0.0.5-dev.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit" "visit-mut" "fold" "derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "1sic25ldxjqrdimq7pvzf4wdynbp4wf5lg1niszp3fdlnh6saqcf") (y #t)))

(define-public crate-ez-proc-macro-0.0.6 (c (n "ez-proc-macro") (v "0.0.6") (d (list (d (n "ez-impl") (r "^0.0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit" "visit-mut" "fold" "derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "0b8rzrjw3fpbg8823fa464wwx04haz1vbfj6v9qrn2ggq6pfnd7p") (y #t)))

(define-public crate-ez-proc-macro-0.0.0-- (c (n "ez-proc-macro") (v "0.0.0--") (h "1lkqa7qbxraqbkqmmksjkl136lx2yzw1v6alan43cfpwdrqqiyhc") (y #t)))

