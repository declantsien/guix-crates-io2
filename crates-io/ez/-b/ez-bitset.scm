(define-module (crates-io ez -b ez-bitset) #:use-module (crates-io))

(define-public crate-ez-bitset-0.1.0 (c (n "ez-bitset") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1ibbivsds8xpy9ld7pqi4wc9izbr2lj1jjcvfj6xqwp4v6dc1pjf")))

