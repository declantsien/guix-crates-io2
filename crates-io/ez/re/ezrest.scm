(define-module (crates-io ez re ezrest) #:use-module (crates-io))

(define-public crate-ezrest-0.0.1-beta-1 (c (n "ezrest") (v "0.0.1-beta-1") (d (list (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9.2") (d #t) (k 1)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 1)))) (h "0rvnylgg11prbmdbhyhy85hpffps6rf6z0dafrg2blapa4ma3pdy")))

(define-public crate-ezrest-0.0.1-beta-2 (c (n "ezrest") (v "0.0.1-beta-2") (d (list (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9.2") (d #t) (k 1)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 1)))) (h "02bhw139v78ffn1yi5k7i31ypavrjqzyyinx87778ijkbywyjq7b")))

