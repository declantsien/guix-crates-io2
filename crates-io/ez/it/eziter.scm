(define-module (crates-io ez it eziter) #:use-module (crates-io))

(define-public crate-eziter-0.1.0 (c (n "eziter") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "04k1qcd9j3fy29pzcwxf38aiybs8jdkn6606amc5yp03f5c2nlqs")))

(define-public crate-eziter-0.1.1 (c (n "eziter") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "1yv55jqj5a6n280yws2apxg9ih1jsqycj19wwjrgfwni9svdh9cj")))

(define-public crate-eziter-0.1.2 (c (n "eziter") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "0dbqig6q029jdqwj2pwbdlmr2zm0if4islbiqkvw4wfbh8msbz1y")))

