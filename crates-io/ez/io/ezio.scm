(define-module (crates-io ez io ezio) #:use-module (crates-io))

(define-public crate-ezio-0.1.0 (c (n "ezio") (v "0.1.0") (h "0cm7hjjyspgy7wffl0q5iin531s8knm3ysymi1difgw0fsfgilr8")))

(define-public crate-ezio-0.1.1 (c (n "ezio") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1ilhkjdzics7jxq58m4b5sgx8sagz5gkdfdm1ca6fg5gf7z31b7f")))

(define-public crate-ezio-0.1.2 (c (n "ezio") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "19xw3ldrnmf8jzx9g85fhpfzch5bmccjwrlwy5fb77l81krdyl5v")))

