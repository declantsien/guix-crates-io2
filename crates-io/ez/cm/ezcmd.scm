(define-module (crates-io ez cm ezcmd) #:use-module (crates-io))

(define-public crate-ezcmd-0.1.0 (c (n "ezcmd") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "10xkd1cjxwg6b3xrg039plg4dny6jvmni57apn2xnv0kxbb535nl")))

(define-public crate-ezcmd-0.2.0 (c (n "ezcmd") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0113c4zxgwwqakk474g2glhqzpbp1m47rhbfdqm44qw2c2ii6skm")))

