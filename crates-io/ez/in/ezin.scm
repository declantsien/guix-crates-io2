(define-module (crates-io ez in ezin) #:use-module (crates-io))

(define-public crate-ezin-0.1.0 (c (n "ezin") (v "0.1.0") (h "0lcbzw9s3hv7mk3lv11rqibhijqasmii3xcian7n10sqr7f9zl2c")))

(define-public crate-ezin-0.1.1 (c (n "ezin") (v "0.1.1") (h "012hjndw5l8shj92r1b8j8shj5vq6a9xy43nxm3gg421wjzngppx")))

(define-public crate-ezin-0.1.2 (c (n "ezin") (v "0.1.2") (h "0mgcdazg7l3h49rkdkabw9xys7q1s1ykydyr4l46c09cgqvy3cij")))

