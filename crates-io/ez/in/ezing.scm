(define-module (crates-io ez in ezing) #:use-module (crates-io))

(define-public crate-ezing-0.1.0 (c (n "ezing") (v "0.1.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "glium") (r "^0.16") (d #t) (k 2)) (d (n "glium_text") (r "^0.11") (d #t) (k 2)) (d (n "glutin") (r "^0.7") (d #t) (k 2)))) (h "1cv31l99lckinl1hflm6l63ny7s6670gxkacqsh34mvxrkgmrgwh")))

(define-public crate-ezing-0.1.1 (c (n "ezing") (v "0.1.1") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "glium") (r "^0.16") (d #t) (k 2)) (d (n "glium_text") (r "^0.11") (d #t) (k 2)) (d (n "glutin") (r "^0.7") (d #t) (k 2)))) (h "1v8vdvr2pmpa4nalaaqkmb8cdivfl4gakdjgjg25xi11adq8d2p1")))

(define-public crate-ezing-0.1.2 (c (n "ezing") (v "0.1.2") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "glium") (r "^0.16") (d #t) (k 2)) (d (n "glium_text") (r "^0.11") (d #t) (k 2)) (d (n "glutin") (r "^0.7") (d #t) (k 2)))) (h "18sgkv0zsz13s24dp96mgm89idg8mg2wh0mni0n99k0zfpaqkw1h")))

(define-public crate-ezing-0.2.0 (c (n "ezing") (v "0.2.0") (d (list (d (n "glium") (r "^0.21") (d #t) (k 2)) (d (n "glium_text") (r "^0.12") (d #t) (k 2)) (d (n "glutin") (r "^0.14") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0m76mvpmqn757xk2aig1pcqyjbhfyh4l0c7rkwgdgqkf9d8ky3wk")))

(define-public crate-ezing-0.2.1 (c (n "ezing") (v "0.2.1") (d (list (d (n "glium") (r "^0.21") (d #t) (k 2)) (d (n "glium_text") (r "^0.12") (d #t) (k 2)) (d (n "glutin") (r "^0.14") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ld731mr695kc7qp5m400fzj62a54i5ql4l4z89mc8wxk8qqhvjn")))

