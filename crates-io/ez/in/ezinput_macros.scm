(define-module (crates-io ez in ezinput_macros) #:use-module (crates-io))

(define-public crate-ezinput_macros-0.1.0 (c (n "ezinput_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lcnmi8z492vpkczygwrray7lp6g9cymh1cmicn5yh3gpz2q4hka")))

(define-public crate-ezinput_macros-0.2.0 (c (n "ezinput_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11hywlvakmn8fxi07xps4h91rjhav6f9fak16rfza9mcsks3cgr3")))

