(define-module (crates-io ez in ezinput) #:use-module (crates-io))

(define-public crate-ezinput-0.1.0 (c (n "ezinput") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6") (f (quote ("serialize"))) (k 0)) (d (n "ezinput_macros") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "1xqmwvbb2k31v9j7awvd8hm3l2qdhzisa6vcc2w1bpzwf37c7vla")))

(define-public crate-ezinput-0.1.1 (c (n "ezinput") (v "0.1.1") (d (list (d (n "bevy") (r "^0.6") (f (quote ("serialize"))) (k 0)) (d (n "ezinput_macros") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "05ly1nr1nl0hfxw79pwsv06yrs9dcdzl6pn8dgh9j4igv3xi9jkf")))

(define-public crate-ezinput-0.1.2 (c (n "ezinput") (v "0.1.2") (d (list (d (n "bevy") (r "^0.6") (f (quote ("serialize"))) (k 0)) (d (n "ezinput_macros") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "1wbg9h9qlkzg4xc7h7jchsf6xg5gxwj0ca15amgqbb25mfmx88pr")))

(define-public crate-ezinput-0.2.0 (c (n "ezinput") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "19k2xml28bdq9106y4h0cmaq7cxig6x3dif07scs0gbimajcr4v0")))

(define-public crate-ezinput-0.2.1 (c (n "ezinput") (v "0.2.1") (d (list (d (n "bevy") (r "^0.6") (f (quote ("serialize"))) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "196kaddxjag215n9s6r4fvdy023x7fghphcyk4msbqw60f3wpsrf")))

(define-public crate-ezinput-0.2.2 (c (n "ezinput") (v "0.2.2") (d (list (d (n "bevy") (r "^0.6") (f (quote ("serialize"))) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "0fm9plh1k2nwyczxb0vq6kd88cw57yb8pkngpb7na2d1dh5bws38")))

(define-public crate-ezinput-0.2.3 (c (n "ezinput") (v "0.2.3") (d (list (d (n "bevy") (r "^0.6") (f (quote ("serialize" "render" "x11" "bevy_gilrs"))) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0y2r3ck9b3aqqyai5kmbgbxk37583x2n43d04w7lrdry1dqrxawq")))

(define-public crate-ezinput-0.2.4 (c (n "ezinput") (v "0.2.4") (d (list (d (n "bevy") (r "^0.7") (f (quote ("serialize" "render" "x11" "bevy_gilrs"))) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "10311c6qmr66kfa8mq33p4i3kdjwq37zgz8lih2ywykdcylihbfn")))

(define-public crate-ezinput-0.3.0 (c (n "ezinput") (v "0.3.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("serialize" "render" "x11" "bevy_gilrs"))) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1lfci2slk100p9r4klmkjvhzp7w6z8zgn2z2jrayqi8fqwzshlp3")))

(define-public crate-ezinput-0.3.1 (c (n "ezinput") (v "0.3.1") (d (list (d (n "bevy") (r "^0.7") (f (quote ("serialize" "render" "x11" "bevy_gilrs"))) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1p1pwpgz8hzwavyi4hcd1a5v2gq5lm2qbqvgjqqqd3ig8cag9ld4")))

(define-public crate-ezinput-0.3.2 (c (n "ezinput") (v "0.3.2") (d (list (d (n "bevy") (r "^0.7") (f (quote ("serialize" "render" "x11" "bevy_gilrs"))) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d4f26zjzgjci07v3a180id4np1231vyaj9gc4sqqms0a74n0i4g")))

(define-public crate-ezinput-0.3.3 (c (n "ezinput") (v "0.3.3") (d (list (d (n "bevy") (r "^0.7") (f (quote ("serialize" "render" "x11" "bevy_gilrs"))) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jv08nrjvxgamyjc6f1cnb0j6pjsvlalwwnbjncsv0dzmlm91xkx")))

(define-public crate-ezinput-0.3.4 (c (n "ezinput") (v "0.3.4") (d (list (d (n "bevy") (r "^0.7") (f (quote ("serialize" "render" "x11" "bevy_gilrs"))) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wl39v4q6xjlqbddlpmffj0z3zj5fwk0rs8h5vifql30hi07z2bv")))

(define-public crate-ezinput-0.3.5 (c (n "ezinput") (v "0.3.5") (d (list (d (n "bevy") (r "^0.7") (f (quote ("serialize" "render" "x11" "bevy_gilrs"))) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1brmcq2k0hcg7zax6jkn9wcqca6z5p75aarjx7zlig2wmyal05ic")))

(define-public crate-ezinput-0.3.6 (c (n "ezinput") (v "0.3.6") (d (list (d (n "bevy") (r "^0.7") (f (quote ("serialize" "render" "x11" "bevy_gilrs"))) (k 0)) (d (n "ezinput_macros") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lxxwf6anqhni14897hk6dvlkfzlzcsi3pjcrjslks72wazlqjix")))

