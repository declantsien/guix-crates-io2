(define-module (crates-io ez in ezinstall) #:use-module (crates-io))

(define-public crate-ezinstall-0.1.0 (c (n "ezinstall") (v "0.1.0") (d (list (d (n "ebacktrace") (r "^0.3") (d #t) (k 0)) (d (n "ezexec") (r "^0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0bbaa0nwiyrhlv679dqxx8rhlxhl9zzbcynqan2c2p2ds725g0hr") (f (quote (("default"))))))

