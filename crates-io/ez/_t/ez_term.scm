(define-module (crates-io ez _t ez_term) #:use-module (crates-io))

(define-public crate-ez_term-0.1.0 (c (n "ez_term") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1w7wy7rhsqn40qhdlkdxhxfm1vcxil5sjxny58zcfif8jcas4slx") (r "1.61.0")))

(define-public crate-ez_term-0.1.1 (c (n "ez_term") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0h566pvh04zymz7jb99ydqc8alp1bk1m6vp3rj7s0k6xp0r18s76") (r "1.61.0")))

