(define-module (crates-io ez ty ezty) #:use-module (crates-io))

(define-public crate-ezty-0.1.0 (c (n "ezty") (v "0.1.0") (d (list (d (n "mopa") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0dmbm8349gy2f15y1fspwy3j44m0796y1xx5w41v56sn7pcidsqb") (f (quote (("layout") ("default" "any_debug")))) (s 2) (e (quote (("any_debug" "dep:mopa"))))))

(define-public crate-ezty-0.1.1 (c (n "ezty") (v "0.1.1") (d (list (d (n "mopa") (r "^0.2") (o #t) (d #t) (k 0)))) (h "07l5g7ly30ilkwrignqmrz8zzvnaqxjj4myqvjwhzvaz0kxmz5i1") (f (quote (("layout") ("default" "any_debug")))) (s 2) (e (quote (("any_debug" "dep:mopa"))))))

(define-public crate-ezty-0.1.2 (c (n "ezty") (v "0.1.2") (d (list (d (n "mopa") (r "^0.2") (o #t) (d #t) (k 0)))) (h "16zq6qrqq59cvx8c3cjwd9d7hpif4a8c5lk2rqm8m01lhz917ky6") (f (quote (("layout") ("default" "any_debug")))) (s 2) (e (quote (("any_debug" "dep:mopa"))))))

(define-public crate-ezty-0.1.3 (c (n "ezty") (v "0.1.3") (d (list (d (n "mopa") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0v2gvwfhj9fyh0450dl81n8nvwmbdadw1vyfqlbn3c30ial60rn9") (f (quote (("default" "any_debug")))) (s 2) (e (quote (("any_debug" "dep:mopa"))))))

(define-public crate-ezty-0.1.4 (c (n "ezty") (v "0.1.4") (d (list (d (n "mopa") (r "^0.2") (o #t) (d #t) (k 0)))) (h "12hbbbiwx6a6c2fd09x29w89kqycfbj1xk88dy26hyjrz172lnrh") (f (quote (("default" "any_debug")))) (s 2) (e (quote (("any_debug" "dep:mopa"))))))

