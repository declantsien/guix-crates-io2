(define-module (crates-io ez ex ezexec) #:use-module (crates-io))

(define-public crate-ezexec-0.1.0 (c (n "ezexec") (v "0.1.0") (d (list (d (n "ebacktrace") (r "^0.3") (d #t) (k 0)))) (h "0nn9a9xpzp0j3bq1d5mdcmq61lgrkial4sbln598qar0dqvhsacp") (f (quote (("default"))))))

(define-public crate-ezexec-0.1.1 (c (n "ezexec") (v "0.1.1") (d (list (d (n "ebacktrace") (r "^0.3") (d #t) (k 0)))) (h "1fa8f7x839i62rm7kpwy3q912swvm7iznfbkfwbq17bcdj5nz527") (f (quote (("default"))))))

(define-public crate-ezexec-0.2.0 (c (n "ezexec") (v "0.2.0") (d (list (d (n "ebacktrace") (r "^0.3") (d #t) (k 0)))) (h "1mc2qri8dzf1l9db0y6s8wn56cwrfkqxqxdg6wxx5xvkpkxlqvf8") (f (quote (("default"))))))

(define-public crate-ezexec-0.3.0 (c (n "ezexec") (v "0.3.0") (d (list (d (n "ebacktrace") (r "^0.5") (d #t) (k 0)))) (h "1daq6m5h6cjy3xfadwfwkkslyf8c77lkym5wajawq6m65l0gp4s5") (f (quote (("default"))))))

(define-public crate-ezexec-0.4.0 (c (n "ezexec") (v "0.4.0") (h "0mychbyvid81gh13msbpm7qf4dvy5ns5a9bm82wkqllmg835y415") (f (quote (("default"))))))

(define-public crate-ezexec-0.4.1 (c (n "ezexec") (v "0.4.1") (h "09m42cm7w4ly5jxld67mc39brv1ik2ccpqil0vr517qzmannnd7s") (f (quote (("default"))))))

