(define-module (crates-io ez oa ezoauth) #:use-module (crates-io))

(define-public crate-ezoauth-0.1.0 (c (n "ezoauth") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "oauth2") (r "^4.0.0-alpha") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04krsfyxq90p4mwbyw5xfxrpi4nx6cnvdiwpq3n4wzm24021xfar")))

(define-public crate-ezoauth-0.2.0 (c (n "ezoauth") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "oauth2") (r "^4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15dx1sg7hmi0zvia1x5h8gqg1ds4yswlffwyif7nrpymn5ljcxhc")))

