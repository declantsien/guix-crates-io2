(define-module (crates-io ez td eztd-core) #:use-module (crates-io))

(define-public crate-eztd-core-0.0.1 (c (n "eztd-core") (v "0.0.1") (h "1rzrm6x6rp0djwgwvn6vik47amwdak5kwlzg75jk9k91c96n6fk2")))

(define-public crate-eztd-core-0.0.2 (c (n "eztd-core") (v "0.0.2") (h "1lqgwaxm8y2vmsf9lhkjgpi4pss5qd96g7ryi7ql1nimjbn6q0j8")))

(define-public crate-eztd-core-0.0.3 (c (n "eztd-core") (v "0.0.3") (h "06c9jk2pfl0dqqznq8wj2xpkzxqkbjv4fkkrak7ybzznzjgrx8fj")))

