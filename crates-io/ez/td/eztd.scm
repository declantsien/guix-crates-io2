(define-module (crates-io ez td eztd) #:use-module (crates-io))

(define-public crate-eztd-0.0.1 (c (n "eztd") (v "0.0.1") (d (list (d (n "eztd-core") (r "^0.0.1") (d #t) (k 0)))) (h "0s88i334d16k9ah6jx7kz3hb5najknvqj96diwckv9k892hrxnv1")))

(define-public crate-eztd-0.0.2 (c (n "eztd") (v "0.0.2") (d (list (d (n "eztd-core") (r "^0.0.2") (d #t) (k 0)))) (h "19srmb8jvn5pv4djmzd7ahm6a0h4bys9j40xx3vg5qpma8zdvvdp")))

(define-public crate-eztd-0.0.3 (c (n "eztd") (v "0.0.3") (d (list (d (n "eztd-core") (r "^0.0.3") (d #t) (k 0)))) (h "1zrxrdf5615wqq277jhadgdhhg73znqhxvss05mhzqh4w7vzbl4x")))

