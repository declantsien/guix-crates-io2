(define-module (crates-io ez fi ezfile) #:use-module (crates-io))

(define-public crate-ezfile-0.1.0 (c (n "ezfile") (v "0.1.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "08lahwg87pmsz7vlfhcglqns240ll2c6wrdrz604n9k3yl2f1b7z")))

(define-public crate-ezfile-1.0.0 (c (n "ezfile") (v "1.0.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "18s1hrh97b7s6j4lkhswdjb182gqr4kszm7j9z19ay0jy4wf2y5w")))

