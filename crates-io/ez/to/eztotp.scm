(define-module (crates-io ez to eztotp) #:use-module (crates-io))

(define-public crate-eztotp-0.1.0 (c (n "eztotp") (v "0.1.0") (d (list (d (n "google-authenticator") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jpvxn1sl8ilx9255yp5lmsl0y63dgpdkssaz2fmq4kxqkinyd6i")))

