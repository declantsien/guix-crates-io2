(define-module (crates-io ez lo ezlogcli) #:use-module (crates-io))

(define-public crate-ezlogcli-0.2.0 (c (n "ezlogcli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 2)) (d (n "escargot") (r "^0.5.8") (d #t) (k 0)) (d (n "ezlog") (r "^0.2.0") (f (quote ("decode"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rwk2szbv35nn5gypzbgaw4qwrl84yd8mhl9q7nawxj9ipqy3f34")))

