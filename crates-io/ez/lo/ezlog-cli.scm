(define-module (crates-io ez lo ezlog-cli) #:use-module (crates-io))

(define-public crate-ezlog-cli-0.1.2 (c (n "ezlog-cli") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ezlog") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "166cpkz7hf4qqy2gd1842pmwisyr3bd62cliycf10pm6p7bi72ha")))

(define-public crate-ezlog-cli-0.1.4 (c (n "ezlog-cli") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ezlog") (r "^0.1") (f (quote ("decode"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dgv1046chrrmbwhmz4l00vds6rlpg31d3x1p0wa48ga23ggm8q3")))

(define-public crate-ezlog-cli-0.1.5 (c (n "ezlog-cli") (v "0.1.5") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ezlog") (r "^0.1") (f (quote ("decode"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00k3d4s0ky2z7n0d7paqdalr11z7j0j2vs62ywqhhm2j8zfdmr0w")))

(define-public crate-ezlog-cli-0.1.7 (c (n "ezlog-cli") (v "0.1.7") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ezlog") (r "^0.1") (f (quote ("decode"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07dsrrg3ihffn5jl5naz9xr4vkq476b3bf0p9s0n8srxlam2bmzn")))

(define-public crate-ezlog-cli-0.2.0-alpha.1 (c (n "ezlog-cli") (v "0.2.0-alpha.1") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ezlog") (r "^0.2.0-alpha.1") (f (quote ("decode"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17fqpzm6aax6a3nhp7vmg88wxjk8jwv25mp8b3bqyh595n8ac07v")))

(define-public crate-ezlog-cli-0.2.0-alpha.2 (c (n "ezlog-cli") (v "0.2.0-alpha.2") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ezlog") (r "^0.2.0-alpha.2") (f (quote ("decode"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14snnrnaqia0c6nv8irldmvv5hwl8r9av0yf3pdvd4bv3z45xmgf")))

(define-public crate-ezlog-cli-0.2.0-beta.1 (c (n "ezlog-cli") (v "0.2.0-beta.1") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ezlog") (r "^0.2.0-beta.1") (f (quote ("decode"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13hafsaq6jk9gi4bq5qm95dc4gcax28jm9xhh8pj58q7xnf25b1i")))

(define-public crate-ezlog-cli-0.2.0-beta.2 (c (n "ezlog-cli") (v "0.2.0-beta.2") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ezlog") (r "^0.2.0-beta.2") (f (quote ("decode"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gxb5jhbwx4x95zv52yi9ifvcp7862smy9dy94nks2p8r9fsj2s8")))

(define-public crate-ezlog-cli-0.2.0-beta.3 (c (n "ezlog-cli") (v "0.2.0-beta.3") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ezlog") (r "^0.2.0-beta.2") (f (quote ("decode"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ymkcwbvxiwi1bnf24mz3cpfdixppk1k0qznwkbzfid9vb64rjr3")))

