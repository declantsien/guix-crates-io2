(define-module (crates-io ez lo ezlogger) #:use-module (crates-io))

(define-public crate-ezlogger-0.1.0 (c (n "ezlogger") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1z1d7xqa0j4q31a1nhbqflyyww2z6j7w3x49r2b8xxw70ij65ca0")))

