(define-module (crates-io ez rt ezrtc-server) #:use-module (crates-io))

(define-public crate-ezrtc-server-0.2.0 (c (n "ezrtc-server") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "axum") (r "^0.6.18") (f (quote ("ws" "macros" "json"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (d #t) (k 0)) (d (n "wasm-peers-protocol") (r "^0.3") (d #t) (k 0)))) (h "13p94y64dsx4piklyb35pbc2hm0z07zd54jb7jap95zfi803dl2l")))

