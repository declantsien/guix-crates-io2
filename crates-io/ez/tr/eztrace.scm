(define-module (crates-io ez tr eztrace) #:use-module (crates-io))

(define-public crate-eztrace-0.1.0 (c (n "eztrace") (v "0.1.0") (h "0jjdm2y5nwg0hms9am5vlahb8hfmjybvdk049xaw4m04kg4ash0f")))

(define-public crate-eztrace-0.2.0 (c (n "eztrace") (v "0.2.0") (h "0yql5xx2a8happzirrz6960a0q4qhpbdxvxxwaym6l73ds8bm9n2")))

