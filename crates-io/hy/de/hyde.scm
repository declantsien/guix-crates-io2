(define-module (crates-io hy de hyde) #:use-module (crates-io))

(define-public crate-hyde-0.1.0 (c (n "hyde") (v "0.1.0") (d (list (d (n "handlebars") (r "^3.5.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1hqczci8jc9x3881hjsjirhr7ahvm3zfinmdxg5siiapn2p53ww6") (y #t)))

