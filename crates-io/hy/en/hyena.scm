(define-module (crates-io hy en hyena) #:use-module (crates-io))

(define-public crate-hyena-0.1.0 (c (n "hyena") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "1ar0j4yx9m64ki9lw831nrphwbgn63bj1l3701cc11m8nwvm06mh")))

(define-public crate-hyena-0.2.0 (c (n "hyena") (v "0.2.0") (d (list (d (n "async-channel") (r "^1.7") (d #t) (k 0)) (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "concurrent-queue") (r "^1.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "1zn2kdmh7qfyfih9vl8ir5fid2as42bwdzqg03mnky5csq6q854j")))

(define-public crate-hyena-0.2.1 (c (n "hyena") (v "0.2.1") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "concurrent-queue") (r "^1.2") (d #t) (k 0)) (d (n "event-listener") (r "^2.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)))) (h "0qy7qx6ldzbawxdzl58442bdn5ilbihyzdsdsssdri81cr1abh6y")))

(define-public crate-hyena-0.2.2 (c (n "hyena") (v "0.2.2") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "async-task") (r "^4.3") (d #t) (k 0)) (d (n "concurrent-queue") (r "^1.2") (d #t) (k 0)) (d (n "event-listener") (r "^2.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)))) (h "07x46aazmjjk4akkzscrz94fxvx9gprm05wbm14dj9hj69wm6csa")))

(define-public crate-hyena-0.2.3 (c (n "hyena") (v "0.2.3") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "async-task") (r "^4.3") (d #t) (k 0)) (d (n "concurrent-queue") (r "^2.1") (d #t) (k 0)) (d (n "event-listener") (r "^2.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)))) (h "05gipxlnbav21fxcfjqra2lmpsp1520s8jgc97qfw8jz98qp38xg")))

(define-public crate-hyena-0.2.4 (c (n "hyena") (v "0.2.4") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "async-task") (r "^4.3") (d #t) (k 0)) (d (n "concurrent-queue") (r "^2.1") (d #t) (k 0)) (d (n "event-listener") (r "^2.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)))) (h "0b6iy1gmddbcsw84p0il4cyvhcc0ax7l10j2sw6ii5iwqsnry7k4")))

