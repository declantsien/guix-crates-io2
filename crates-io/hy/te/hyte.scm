(define-module (crates-io hy te hyte) #:use-module (crates-io))

(define-public crate-hyte-0.1.0 (c (n "hyte") (v "0.1.0") (d (list (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0g66ff0b0a5iw1k1189760j031djzmlsdl77yrb8sbmbr34kfa57")))

(define-public crate-hyte-0.1.1 (c (n "hyte") (v "0.1.1") (d (list (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "13vjn1ny1zbq2pnphigsqycqpqf1xq1dhikpycjndpncyvdmnhc3")))

