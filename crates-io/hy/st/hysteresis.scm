(define-module (crates-io hy st hysteresis) #:use-module (crates-io))

(define-public crate-hysteresis-0.1.0 (c (n "hysteresis") (v "0.1.0") (d (list (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "0wkvdl9pz4hidmc3mr6b1836kdgmrwzbhj7cskbmi95w7010b3hd")))

(define-public crate-hysteresis-0.1.1 (c (n "hysteresis") (v "0.1.1") (d (list (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "0i3cvbv02m1c7xlyr3avpxly36d3409qqp5mif2fygh1qdng2i24")))

(define-public crate-hysteresis-0.2.0 (c (n "hysteresis") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "14qav7jwl8k111lgvrm246kp7vvniafj6m60h0lf51qh5a6xkkmy")))

(define-public crate-hysteresis-0.3.0 (c (n "hysteresis") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "11jcras35fzcia9jsiwlmjv9c1hlrc34lwxy8hj2wqyw192izm9i")))

(define-public crate-hysteresis-0.3.1 (c (n "hysteresis") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "1yddbfdr3rqkpcyzmyc2wrd7plb1sk2nfrb55svn7wzdikq4bizs")))

(define-public crate-hysteresis-0.4.0 (c (n "hysteresis") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "12ajra29lhnjx9vhh8d9rjprlhy6l7w7d7q7170w4z0ck7w4lnj4")))

(define-public crate-hysteresis-0.5.0 (c (n "hysteresis") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "048pyks1mxsx2d6qjwac4xn3nj6sq45s47cfl56lijacb8sqwfr2")))

