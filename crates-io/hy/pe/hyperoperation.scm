(define-module (crates-io hy pe hyperoperation) #:use-module (crates-io))

(define-public crate-hyperoperation-0.1.0 (c (n "hyperoperation") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-iter") (r "^0.1.43") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1d8ffyzkwy67fzbqghjjvpg99g4mhg8hm5fii4cjs1lyxcil0k97")))

(define-public crate-hyperoperation-0.1.1 (c (n "hyperoperation") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-iter") (r "^0.1.43") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "14c00iav8qrm53zb4ghrh4vdgn60fwsg1hyli6m2qsikvln6lcpk")))

