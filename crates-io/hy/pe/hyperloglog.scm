(define-module (crates-io hy pe hyperloglog) #:use-module (crates-io))

(define-public crate-hyperloglog-0.0.1 (c (n "hyperloglog") (v "0.0.1") (h "0l0l5j9l0kcl4i0cjn14fs6lfxwn81sspxdg2xzn3krm02zis4hr")))

(define-public crate-hyperloglog-0.0.2 (c (n "hyperloglog") (v "0.0.2") (h "02n9rsjpvzm4ib3z2p60sa5q64mjs3dk1jp5n7vj1mpb25wvf4bg")))

(define-public crate-hyperloglog-0.0.3 (c (n "hyperloglog") (v "0.0.3") (h "0gsyhkam1ixpz5zi4d7rdvbarh9a25z6yvcl975j45gmickvxzkb")))

(define-public crate-hyperloglog-0.0.4 (c (n "hyperloglog") (v "0.0.4") (h "0qnsvllrgryg6k7jnrmgi2w38zgdhchvr68rd58knycdn59nwss6")))

(define-public crate-hyperloglog-0.0.5 (c (n "hyperloglog") (v "0.0.5") (h "0386c0mczfhliw2nc5spxmk508cwamhxjdpr1zdfhyalb3m6zcc1")))

(define-public crate-hyperloglog-0.0.6 (c (n "hyperloglog") (v "0.0.6") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "1wwd56znnm493j57rni8jr0nhwsf138z02vdc8bqh38153zzxbm1")))

(define-public crate-hyperloglog-0.0.7 (c (n "hyperloglog") (v "0.0.7") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "116xyjhahyzgpzbyw8k4mz35p0cinyza7f4wlklpkr2d6ah678mf")))

(define-public crate-hyperloglog-0.0.8 (c (n "hyperloglog") (v "0.0.8") (d (list (d (n "rand") (r ">= 0.3.14") (d #t) (k 0)))) (h "0sny7xklcl26mvyzw01x9in47v7kkiyrpnzmdal3afyw8ickvgkv")))

(define-public crate-hyperloglog-0.0.9 (c (n "hyperloglog") (v "0.0.9") (d (list (d (n "rand") (r ">= 0.3.14") (d #t) (k 0)))) (h "1malcx2m504jwsflhx656xaa5m8p3qvwh1bsdrg7kckkrbjmhd45")))

(define-public crate-hyperloglog-0.0.10 (c (n "hyperloglog") (v "0.0.10") (d (list (d (n "rand") (r "~0.3") (d #t) (k 0)) (d (n "siphasher") (r "~0.1") (d #t) (k 0)))) (h "0c58j8wvij2zx5qrqrr7dq5362gxh4l26c2if98214jd12b5j8m0")))

(define-public crate-hyperloglog-0.0.11 (c (n "hyperloglog") (v "0.0.11") (d (list (d (n "rand") (r "~0.3") (d #t) (k 0)) (d (n "siphasher") (r "~0.1") (d #t) (k 0)))) (h "02a47sm1mj1xv0wd7gkgv7gvcwks4h867p6s31764vzaz244jlzb")))

(define-public crate-hyperloglog-0.0.12 (c (n "hyperloglog") (v "0.0.12") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "03bzxsrj3g8hkaz4ap04cli4q56mh47lv4537vmpcz7bwgwz432z")))

(define-public crate-hyperloglog-1.0.0 (c (n "hyperloglog") (v "1.0.0") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1cq5xszvsdmls97kbdbk4iqj81w0cyq0izibjhin0j0f7fv51bk6")))

(define-public crate-hyperloglog-1.0.1 (c (n "hyperloglog") (v "1.0.1") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1pmxvfwx6k05ilsvfg5kb4nb0n78m0jnck8vqczn0kf5b0npmfck")))

(define-public crate-hyperloglog-1.0.2 (c (n "hyperloglog") (v "1.0.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0p8lp6m6qbjrr2r97d6r6gg8c19bzgff3ivycqf4dafknm506j3b") (f (quote (("with_serde" "serde" "siphasher/serde_std") ("default"))))))

