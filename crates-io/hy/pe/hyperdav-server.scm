(define-module (crates-io hy pe hyperdav-server) #:use-module (crates-io))

(define-public crate-hyperdav-server-0.1.0 (c (n "hyperdav-server") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "1r0iwc5j8yr6i05qw3sac0v05anh6ncnxqlag143021pifvg8lv8")))

