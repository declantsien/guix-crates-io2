(define-module (crates-io hy pe hyper-request-validator) #:use-module (crates-io))

(define-public crate-hyper-request-validator-0.1.0 (c (n "hyper-request-validator") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-body") (r "^0.4") (d #t) (k 0)) (d (n "http-body-request-validator") (r "^0.1") (d #t) (k 0)) (d (n "http-request-validator") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (k 0)))) (h "0cbrcwvgxcly97plw091wprgj2jn4q6kzq8danz7a9v04cqkdkvz")))

