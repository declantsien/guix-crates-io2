(define-module (crates-io hy pe hypermud-client) #:use-module (crates-io))

(define-public crate-hypermud-client-0.0.0 (c (n "hypermud-client") (v "0.0.0") (h "1s2884d886ygyshkx0g1bn3vx4d25hp9h23mspzsjvxk8pvvvbzi") (y #t)))

(define-public crate-hypermud-client-0.0.1 (c (n "hypermud-client") (v "0.0.1") (h "1mxhx1drky59fci9rc3l85wy6ywn3r6bhgsjhfc2fskgbirhqc98") (y #t)))

