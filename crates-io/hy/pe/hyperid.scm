(define-module (crates-io hy pe hyperid) #:use-module (crates-io))

(define-public crate-hyperid-1.0.0 (c (n "hyperid") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "17zik32d7a44gnpnr67j165lcpig0if68i2bjbh4vghijln6q1in")))

(define-public crate-hyperid-1.1.0 (c (n "hyperid") (v "1.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "15c45cvw0mx6mpiylmwh0x5zzvjb7n7k72y076x2325f59mbby59")))

(define-public crate-hyperid-1.1.1 (c (n "hyperid") (v "1.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "101sfw03ifrqlg8c0p6q28x0phkgmb6qy7gvgwzl8rgis9xygjgq")))

(define-public crate-hyperid-2.0.1-alpha.0 (c (n "hyperid") (v "2.0.1-alpha.0") (d (list (d (n "base64") (r "^0") (o #t) (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0310fqj9l37zyw8gj5l45jsgxl5l06wxkfhsl9k5hp2gy9jrmv2k") (f (quote (("url_safe" "base64")))) (y #t)))

(define-public crate-hyperid-2.0.0 (c (n "hyperid") (v "2.0.0") (d (list (d (n "base64") (r "^0") (o #t) (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lv3m7abk9qyglai4djvxzg4pk117q1xp3s93hdpjzxc4xcivwfd") (f (quote (("url_safe" "base64"))))))

(define-public crate-hyperid-2.0.1-alpha.1 (c (n "hyperid") (v "2.0.1-alpha.1") (d (list (d (n "base64") (r "^0") (o #t) (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1d43n6za22ly5173r1p2zr2h6k9vq7s6s37n58d0qm36jkzqqy2s") (f (quote (("url_safe" "base64")))) (y #t)))

(define-public crate-hyperid-2.0.1-alpha.2 (c (n "hyperid") (v "2.0.1-alpha.2") (d (list (d (n "base64") (r "^0") (o #t) (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0p8cir1lk289zbypqbfrp85ckr2gawsnxks5mpqvvch4i65xsc3f") (f (quote (("url_safe" "base64")))) (y #t)))

(define-public crate-hyperid-2.0.1-alpha.3 (c (n "hyperid") (v "2.0.1-alpha.3") (d (list (d (n "base64") (r "^0") (o #t) (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rnlg7887vl39xhiwghhvc5867i8qivmsnyjnvz89d1z7wzl0bap") (f (quote (("url_safe" "base64")))) (y #t)))

(define-public crate-hyperid-2.0.1-alpha.4 (c (n "hyperid") (v "2.0.1-alpha.4") (d (list (d (n "base64") (r "^0") (o #t) (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0pllzjymf5vcv4smczmvq5ygkbw25hrxfs60bajvzhg07ca275iz") (f (quote (("url_safe" "base64")))) (y #t)))

(define-public crate-hyperid-2.0.1 (c (n "hyperid") (v "2.0.1") (d (list (d (n "base64") (r "^0") (o #t) (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1k96qps30ipksgskfzl14lqhi1jiyc3xphg3wjfr0v8wgbhyipf8") (f (quote (("url_safe" "base64"))))))

(define-public crate-hyperid-2.1.1 (c (n "hyperid") (v "2.1.1") (d (list (d (n "base64") (r "^0") (o #t) (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yhfgjrl4d41p5xcc0f549ig79i9n4vryr02lk3sfxflld4yl3rq") (f (quote (("url_safe" "base64"))))))

(define-public crate-hyperid-2.2.0 (c (n "hyperid") (v "2.2.0") (d (list (d (n "base64") (r "^0") (o #t) (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "00wlx8vv3fkbw4d4igxgn3p94kqzg4i0rclil2rg1mj7c363mdla") (f (quote (("url_safe" "base64"))))))

(define-public crate-hyperid-2.3.1 (c (n "hyperid") (v "2.3.1") (d (list (d (n "base64") (r "^0") (o #t) (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "12jh342byd63ilvhqgjhvhr5vaq76cr8c3nqhg41n5b3xdhim44x") (f (quote (("url_safe" "base64"))))))

