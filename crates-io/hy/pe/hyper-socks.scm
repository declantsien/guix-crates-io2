(define-module (crates-io hy pe hyper-socks) #:use-module (crates-io))

(define-public crate-hyper-socks-0.1.0 (c (n "hyper-socks") (v "0.1.0") (d (list (d (n "hyper") (r "^0.7") (k 0)) (d (n "hyper") (r "^0.7") (d #t) (k 2)) (d (n "socks") (r "^0.1") (d #t) (k 0)))) (h "1k51d7yzdkvar40rwxq3f5xgsf0svjhwm7146hn4a5x27l2krdbs")))

(define-public crate-hyper-socks-0.1.1 (c (n "hyper-socks") (v "0.1.1") (d (list (d (n "hyper") (r "^0.7") (k 0)) (d (n "hyper") (r "^0.7") (d #t) (k 2)) (d (n "socks") (r "^0.2") (d #t) (k 0)))) (h "1d4w4ln38pvgfd91y833arkgflainrhyrda5fqncnblbvglblzkj")))

(define-public crate-hyper-socks-0.1.2 (c (n "hyper-socks") (v "0.1.2") (d (list (d (n "hyper") (r ">= 0.7, < 0.9") (k 0)) (d (n "hyper") (r ">= 0.7, < 0.9") (d #t) (k 2)) (d (n "socks") (r "^0.2") (d #t) (k 0)))) (h "1xq9n483krq54pfbw5ljn1j9avfkl38dz2008476x6r5zaslar9d")))

(define-public crate-hyper-socks-0.2.0 (c (n "hyper-socks") (v "0.2.0") (d (list (d (n "hyper") (r "^0.8.1") (k 0)) (d (n "hyper") (r "^0.8.1") (d #t) (k 2)) (d (n "socks") (r "^0.2") (d #t) (k 0)))) (h "1wl35kxcnnmj6nvrvzb6dxsq57v7jzn842713riv2nid6993i9v5")))

(define-public crate-hyper-socks-0.2.1 (c (n "hyper-socks") (v "0.2.1") (d (list (d (n "hyper") (r ">= 0.8.1, < 0.10") (k 0)) (d (n "hyper") (r ">= 0.8.1, < 0.10") (d #t) (k 2)) (d (n "socks") (r "^0.2") (d #t) (k 0)))) (h "0f9yncppwyr2hzmbbyq8laf965fxx6y5xx5br5n3hk2sxz9hx69q")))

(define-public crate-hyper-socks-0.3.0 (c (n "hyper-socks") (v "0.3.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 2)) (d (n "socks") (r "^0.2") (d #t) (k 0)))) (h "01kainq4w4z5asilwwz4gm7j6diklysmfjr5kpz2yw55inwpjrs9")))

(define-public crate-hyper-socks-0.4.0 (c (n "hyper-socks") (v "0.4.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 2)) (d (n "socks") (r "^0.3") (d #t) (k 0)))) (h "05ljiy8w1vbirqcj85sd09adb8qib93ip5p01k45hcm8g716dgr4")))

