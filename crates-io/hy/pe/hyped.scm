(define-module (crates-io hy pe hyped) #:use-module (crates-io))

(define-public crate-hyped-0.1.0 (c (n "hyped") (v "0.1.0") (d (list (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "1cmf3320lyqc0hv8z5mf63qgysk6iad28zzf5m62lqz7abmnk37p")))

(define-public crate-hyped-0.1.1 (c (n "hyped") (v "0.1.1") (d (list (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "01c6xj9w40m8x20lrxxvsb2daqk1dd33vlx48s1333pcb4dyf6ic")))

