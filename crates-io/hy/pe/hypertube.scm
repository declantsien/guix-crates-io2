(define-module (crates-io hy pe hypertube) #:use-module (crates-io))

(define-public crate-hypertube-0.0.1 (c (n "hypertube") (v "0.0.1") (d (list (d (n "cidr") (r "^0.2.2") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zd4dzhjn2b5sqk1f82r0gn7bj1x76llhs6w5y60k02qbz3m56p3")))

(define-public crate-hypertube-0.0.2 (c (n "hypertube") (v "0.0.2") (d (list (d (n "cidr") (r "^0.2.2") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qjvkprnh29mb9k1y46pn0i50n1qrcynvd2z69l1insnimgxqlcj")))

(define-public crate-hypertube-0.1.0 (c (n "hypertube") (v "0.1.0") (d (list (d (n "cidr") (r "^0.2.2") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0630dq1qldlmdqaf4m5b6nhfbmq5k2kl1pz74q7b029yk5h3382s")))

(define-public crate-hypertube-0.1.1 (c (n "hypertube") (v "0.1.1") (d (list (d (n "cidr") (r "^0.2.2") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kc2rwm160nhg38j0mi95dkchqm3kvaj9fv7r4g2l7m3hdbyz34h")))

(define-public crate-hypertube-0.2.0 (c (n "hypertube") (v "0.2.0") (d (list (d (n "cidr") (r "^0.2.2") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hh0g7n1fph0ayypi0ywywjmv17m6ccs1k70hsvx0g246galknbw")))

(define-public crate-hypertube-0.2.1 (c (n "hypertube") (v "0.2.1") (d (list (d (n "cidr") (r "^0.2.2") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15wch76j1f8mjwlla6jf4548vhvrnlqmlh4sf9lk4m1j64p5bd0j")))

(define-public crate-hypertube-0.2.2 (c (n "hypertube") (v "0.2.2") (d (list (d (n "cidr") (r "^0.2.2") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05cccabdvqqydzhk5h3yyrag69jy5992zrl080d21hj5graypzlz")))

