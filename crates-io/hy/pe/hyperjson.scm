(define-module (crates-io hy pe hyperjson) #:use-module (crates-io))

(define-public crate-hyperjson-0.2.1 (c (n "hyperjson") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0k1p16sdjkbz1f1qkjvpwsnfvvgvpk43jrhnj2pa646wghxfs29s") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-hyperjson-0.2.2 (c (n "hyperjson") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "177mhia7qmh1am6w3ksy347q00g53mw27fh4m62m83pyiqs88l77") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-hyperjson-0.2.3 (c (n "hyperjson") (v "0.2.3") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "0mjiys51s1p9p0x1rk86yiw2ig3pr17l70i893kr84fm3808q10y") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-hyperjson-0.2.4 (c (n "hyperjson") (v "0.2.4") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "0yn69v0qiishg9bwxabv5h6pmy9n21wlfk80m5vrpvwi69jy8mrm") (f (quote (("default" "pyo3/extension-module"))))))

