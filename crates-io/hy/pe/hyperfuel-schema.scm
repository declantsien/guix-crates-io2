(define-module (crates-io hy pe hyperfuel-schema) #:use-module (crates-io))

(define-public crate-hyperfuel-schema-0.0.2 (c (n "hyperfuel-schema") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.18") (f (quote ("compute_concatenate"))) (k 0)))) (h "0v39bxrjxbm1rzlr1g3ianingysb4gyrawx6aqqdfs6rqhm2mvn4")))

(define-public crate-hyperfuel-schema-0.0.3 (c (n "hyperfuel-schema") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.18") (f (quote ("compute_concatenate"))) (k 0)))) (h "065vak5l9ypqq880cfnb7sdvhvr61a9qc0j36547qil4cxb8i1ps")))

(define-public crate-hyperfuel-schema-1.0.0 (c (n "hyperfuel-schema") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.18") (f (quote ("compute_concatenate"))) (k 0)))) (h "055l9jlgh9z73ngzzm2bgljd1pl8fc7blswng0ciyjvyyhjq015a")))

(define-public crate-hyperfuel-schema-2.0.0 (c (n "hyperfuel-schema") (v "2.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.18") (f (quote ("compute_concatenate"))) (k 0)))) (h "0b70i8afib2sjnmld6jskd7jzdr9l0pl2f5safvapsznvxsbxx1a")))

