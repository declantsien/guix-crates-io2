(define-module (crates-io hy pe hyper-native-tls) #:use-module (crates-io))

(define-public crate-hyper-native-tls-0.1.0 (c (n "hyper-native-tls") (v "0.1.0") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (k 0)) (d (n "hyper-openssl") (r "^0.1") (d #t) (k 2)) (d (n "native-tls") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 2)))) (h "0d08zxwb47jpiy5zlxlalb27mw9440spyw0whaswqnb2bkifk389")))

(define-public crate-hyper-native-tls-0.2.0 (c (n "hyper-native-tls") (v "0.2.0") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 2)) (d (n "native-tls") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 2)))) (h "05qbghv2spr2j47a8qi7dikha8knyjd4cxk6gnbn3lw10jy7cjiq")))

(define-public crate-hyper-native-tls-0.2.1 (c (n "hyper-native-tls") (v "0.2.1") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 2)) (d (n "native-tls") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 2)))) (h "1api6kdnf3kddniz6g4g0dykm239d16la6jx7fdd7hqhdvng2y0f")))

(define-public crate-hyper-native-tls-0.2.2 (c (n "hyper-native-tls") (v "0.2.2") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 2)) (d (n "native-tls") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 2)))) (h "029l920fyx1sqw462jskb25mc60pisxjc5kmbqhag5q45xvqzrmg")))

(define-public crate-hyper-native-tls-0.2.3 (c (n "hyper-native-tls") (v "0.2.3") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.2") (d #t) (k 0)))) (h "01gpqgjkljz9db1d13gdxpq9r97a20gv9aw0g5q4dzvxwvlwrzj8")))

(define-public crate-hyper-native-tls-0.2.4 (c (n "hyper-native-tls") (v "0.2.4") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.2") (d #t) (k 0)))) (h "1s2nm9apj5i8yjrzq8amdjwzhy0v967fjl1vca1ra1fk6m52wcvj")))

(define-public crate-hyper-native-tls-0.3.0 (c (n "hyper-native-tls") (v "0.3.0") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)))) (h "0s30y20qy0akzss91yxsq1x1q7rr04jy33i0cq72nx22yjc5advd")))

