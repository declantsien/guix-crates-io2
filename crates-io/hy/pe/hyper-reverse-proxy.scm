(define-module (crates-io hy pe hyper-reverse-proxy) #:use-module (crates-io))

(define-public crate-hyper-reverse-proxy-0.1.0 (c (n "hyper-reverse-proxy") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0rbwjhdhy7ji8l5k607h9m2nvm10czfb9q1zdxqwfq7ffx3nsvwl")))

(define-public crate-hyper-reverse-proxy-0.1.1 (c (n "hyper-reverse-proxy") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "1xq5way76x4h8rhdk2wldwchykxb4kxz9d2nlydj4zgpynlk6k4z")))

(define-public crate-hyper-reverse-proxy-0.2.0 (c (n "hyper-reverse-proxy") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 2)) (d (n "unicase") (r "^2.0.0") (d #t) (k 0)))) (h "0797ypivqmnwwvwxm8icyh08m0ynrnldampyn4yij51n1zgwx5h5")))

(define-public crate-hyper-reverse-proxy-0.2.1 (c (n "hyper-reverse-proxy") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 2)) (d (n "tokio-signal") (r "^0.1.2") (d #t) (k 2)) (d (n "unicase") (r "^2.0.0") (d #t) (k 0)))) (h "0rwhsan9axqhjyxjfby6lx833fmkwrw353930vm7wxmwnwhjcsmv")))

(define-public crate-hyper-reverse-proxy-0.3.0 (c (n "hyper-reverse-proxy") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "unicase") (r "^2.2") (d #t) (k 0)))) (h "0m961fg2zxrhaplapcdfvx2pd0kcnhdx2zshyqfrq8cvk1cw7s7v")))

(define-public crate-hyper-reverse-proxy-0.4.0 (c (n "hyper-reverse-proxy") (v "0.4.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "unicase") (r "^2.3") (d #t) (k 0)))) (h "1s1c962hc4zm8s0p8ysz3wvv08995am76jpw2kchac614k31jks7")))

(define-public crate-hyper-reverse-proxy-0.5.0 (c (n "hyper-reverse-proxy") (v "0.5.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0n5pf5ph7g2qvd6l1nsdpghma7vwd4lndr280b22wkwg4z5d6gxs")))

(define-public crate-hyper-reverse-proxy-0.5.1 (c (n "hyper-reverse-proxy") (v "0.5.1") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0ddr4dcmxskmc3az1z0ns7q05ydcbsrjdnhwplrrzyw3njqzj6nc")))

