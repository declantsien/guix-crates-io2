(define-module (crates-io hy pe hype) #:use-module (crates-io))

(define-public crate-hype-0.0.1 (c (n "hype") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)))) (h "08gmjg2k6jhl857fq6hvi0w7spmyis9kvbhhn6j8sr19j9z75ak7")))

