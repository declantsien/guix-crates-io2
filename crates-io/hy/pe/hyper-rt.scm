(define-module (crates-io hy pe hyper-rt) #:use-module (crates-io))

(define-public crate-hyper-rt-0.1.0 (c (n "hyper-rt") (v "0.1.0") (d (list (d (n "awak") (r "^0.2") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0nldisglqj4h1fmi805hryji1dzkpfqkgljpjs9cgp9qr0fx4lxg")))

