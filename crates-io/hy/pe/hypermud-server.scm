(define-module (crates-io hy pe hypermud-server) #:use-module (crates-io))

(define-public crate-hypermud-server-0.0.0 (c (n "hypermud-server") (v "0.0.0") (h "1nmblfczwcg9kmfp71dlybniscsc5n93zczll0l9j20gh7p2ki8j") (y #t)))

(define-public crate-hypermud-server-0.0.1 (c (n "hypermud-server") (v "0.0.1") (h "1qiyf7n1g7y5wi450xki062r6f32kl0gviak7ylwmm0cbzsvx3jb") (y #t)))

