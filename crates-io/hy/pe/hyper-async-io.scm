(define-module (crates-io hy pe hyper-async-io) #:use-module (crates-io))

(define-public crate-hyper-async-io-0.1.0 (c (n "hyper-async-io") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (f (quote ("async-await"))) (k 0)) (d (n "hyper") (r "^0.14.27") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (k 0)))) (h "0kxljjhgbr7i1fs1r1as77b8qi5qyy4qmawlc0axssmv4cs8zf6j")))

