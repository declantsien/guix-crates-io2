(define-module (crates-io hy pe hypersync-net-types) #:use-module (crates-io))

(define-public crate-hypersync-net-types-0.1.0 (c (n "hypersync-net-types") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.19") (d #t) (k 0)) (d (n "capnpc") (r "^0.19") (d #t) (k 1)) (d (n "hypersync-format") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jwxdj98kr644cpd339v8vb52gr4rlmx7m1hyc14ycvr2bd7wqzv")))

