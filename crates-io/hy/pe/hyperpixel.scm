(define-module (crates-io hy pe hyperpixel) #:use-module (crates-io))

(define-public crate-hyperpixel-0.0.0 (c (n "hyperpixel") (v "0.0.0") (d (list (d (n "js_ffi") (r "^0.0.14") (d #t) (k 0)))) (h "14jm9m4c6cjgymh8gr8hhx2lwc4q13xpl24xbbaiww8inkzz9dbr")))

(define-public crate-hyperpixel-0.0.1 (c (n "hyperpixel") (v "0.0.1") (d (list (d (n "js_ffi") (r "^0.1") (d #t) (k 0)))) (h "0p13ify9r9wkl7b39ghna284fjfwcmxv7lgmgsq7wlqppn2k5fk7")))

(define-public crate-hyperpixel-0.0.2 (c (n "hyperpixel") (v "0.0.2") (d (list (d (n "js_ffi") (r "^0.1") (d #t) (k 0)))) (h "1qy0srxb4ymrnc6b87cb2hjvsm5mamxamx98ccsd7wjyj587cf6y")))

(define-public crate-hyperpixel-0.1.0 (c (n "hyperpixel") (v "0.1.0") (d (list (d (n "js_ffi") (r "^0.1") (d #t) (k 0)))) (h "1jlh4fq03gb48hkmp33vxvy7cy626f7xk9yz23sjspxpww4f3afh")))

(define-public crate-hyperpixel-0.1.1 (c (n "hyperpixel") (v "0.1.1") (d (list (d (n "js_ffi") (r "^0.5") (d #t) (k 0)))) (h "00qfp0m5jpfrvxg4r6mc83m2b6gqzrvv0g1pjm489ixlcs87fvyw")))

(define-public crate-hyperpixel-0.1.2 (c (n "hyperpixel") (v "0.1.2") (d (list (d (n "js_ffi") (r "^0.6") (d #t) (k 0)))) (h "1mykihs4l6vmkaalf4y9ld6kcxqhy58p44jh70f33zas1b6j5vmv")))

