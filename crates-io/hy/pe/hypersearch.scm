(define-module (crates-io hy pe hypersearch) #:use-module (crates-io))

(define-public crate-hypersearch-0.1.0 (c (n "hypersearch") (v "0.1.0") (h "026px0aa2w60k0rl90zrg7mklscs230j38ss3yg4pffllawmpgvf")))

(define-public crate-hypersearch-0.2.0 (c (n "hypersearch") (v "0.2.0") (d (list (d (n "hypersearch_codegen") (r "^0.2.1") (d #t) (k 0)))) (h "0a68a1fh5cng52g5hcn9cjvzbni037zj5dh1g3blab94iwigsb59")))

