(define-module (crates-io hy pe hypertask_engine) #:use-module (crates-io))

(define-public crate-hypertask_engine-0.2.0 (c (n "hypertask_engine") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.59") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "15k4rm2yqzdxnml0l0dydpm1qkxf9l3kagxykc5j3p05l9xdl3ky")))

