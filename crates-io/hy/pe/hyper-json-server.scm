(define-module (crates-io hy pe hyper-json-server) #:use-module (crates-io))

(define-public crate-hyper-json-server-0.1.0 (c (n "hyper-json-server") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)))) (h "115xycn9vmvfzxmhbvl4xbfqib1cym06jrwvph7hdcjm5nrl67yb")))

