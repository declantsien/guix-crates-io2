(define-module (crates-io hy pe hypermud-common) #:use-module (crates-io))

(define-public crate-hypermud-common-0.0.0 (c (n "hypermud-common") (v "0.0.0") (h "0bxlxwigj8lf3d9hjb5r2xbm8jscd56iv7h1brx7rak3fka3h73i") (y #t)))

(define-public crate-hypermud-common-0.0.1 (c (n "hypermud-common") (v "0.0.1") (h "0vzgxd0v1807hbkszzq80fsjb95ilc2hh34dj1yhh1s964mkqncl") (y #t)))

