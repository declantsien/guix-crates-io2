(define-module (crates-io hy pe hyper-dns) #:use-module (crates-io))

(define-public crate-hyper-dns-0.1.0 (c (n "hyper-dns") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)) (d (n "trust-dns") (r "^0.9.3") (d #t) (k 0)))) (h "1l7q4ymdxsxh2n19kzjpy9hlh70rf0vlnxjikyr7r96gv8v9gca5")))

(define-public crate-hyper-dns-0.1.1 (c (n "hyper-dns") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)) (d (n "trust-dns") (r "^0.9.3") (d #t) (k 0)))) (h "13k00qs6pj843b8zdqfkvbypwycxfbxzzm12j8scg8z10r7khj61")))

(define-public crate-hyper-dns-0.1.2 (c (n "hyper-dns") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)) (d (n "trust-dns") (r "^0.9.3") (d #t) (k 0)))) (h "030ijphkiv6kjnwx2157nr1lilpllkmqbajr65skj2d7rjdp85sh")))

(define-public crate-hyper-dns-0.1.3 (c (n "hyper-dns") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)) (d (n "trust-dns") (r "^0.9.3") (d #t) (k 0)))) (h "05i6vyn50lfyirjx0cqyj7i4c9pgn6llkbnzrvrs90cqmswhg6y0")))

(define-public crate-hyper-dns-0.2.0 (c (n "hyper-dns") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)) (d (n "trust-dns") (r "^0.13.0") (d #t) (k 0)))) (h "1kz6gldiy0447y1z79bnkx8lq2gl0h1f48hfipfhb1y0p7g5df7c")))

(define-public crate-hyper-dns-0.2.1 (c (n "hyper-dns") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)) (d (n "trust-dns") (r "^0.13.0") (k 0)))) (h "1s6q3mnqjwrg1745ggxvp3wi94ywdxx51xwnv1s89wl0mwgdhj95")))

(define-public crate-hyper-dns-0.2.2 (c (n "hyper-dns") (v "0.2.2") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)) (d (n "trust-dns") (r "^0.13.0") (k 0)))) (h "1ybsf0wryffpa1w3g426ac0cb2gnlpd1n3ifvl97jamk4ll3ydkq")))

(define-public crate-hyper-dns-0.3.0 (c (n "hyper-dns") (v "0.3.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns") (r "^0.14.0") (k 0)))) (h "1ljdyzz2giv121v6wd8fgbfph6rmj6kvyhq88j1rr01f8c0kd8dm")))

