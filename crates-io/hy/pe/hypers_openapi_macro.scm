(define-module (crates-io hy pe hypers_openapi_macro) #:use-module (crates-io))

(define-public crate-hypers_openapi_macro-0.0.0-alpha (c (n "hypers_openapi_macro") (v "0.0.0-alpha") (d (list (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cxdr0lrla0s82z26bdi33zl9ljq82a2yqbydmkp02ir64m9y36a")))

(define-public crate-hypers_openapi_macro-0.1.0 (c (n "hypers_openapi_macro") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "=3.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "=1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.82") (d #t) (k 0)) (d (n "quote") (r "=1.0.36") (d #t) (k 0)) (d (n "regex") (r "=1.10.4") (d #t) (k 0)) (d (n "syn") (r "=2.0.64") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bacsdwh5r0nb87yib6y6sw9hs427rkw2xva678as2r077ya9h0m")))

