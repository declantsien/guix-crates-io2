(define-module (crates-io hy pe hyper-tree-router) #:use-module (crates-io))

(define-public crate-hyper-tree-router-0.1.0 (c (n "hyper-tree-router") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("http1" "server" "tcp"))) (d #t) (k 0)) (d (n "prefix_tree_map") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "18jih618gdk8hdk027rfyyj75v4kb8xqwigxkkarph08gmr8iddq") (r "1.64")))

