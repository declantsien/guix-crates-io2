(define-module (crates-io hy pe hyper-sse) #:use-module (crates-io))

(define-public crate-hyper-sse-0.1.0 (c (n "hyper-sse") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libhydrogen") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1y2ys8dmkb46idlxfqn7mh6fd0xwh2ivk94fjgi1vpf1ljif5w62")))

