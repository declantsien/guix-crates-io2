(define-module (crates-io hy pe hyper-multipart-rfc7578-mem-uploads) #:use-module (crates-io))

(define-public crate-hyper-multipart-rfc7578-mem-uploads-0.1.0-alpha2 (c (n "hyper-multipart-rfc7578-mem-uploads") (v "0.1.0-alpha2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1f38x91di1bm58b94c6j1ajy82hmmza827xfmjli3v1x8jn2di9g") (y #t)))

(define-public crate-hyper-multipart-rfc7578-mem-uploads-0.1.0-alpha3 (c (n "hyper-multipart-rfc7578-mem-uploads") (v "0.1.0-alpha3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1qcd42jnmnznyqzrdjx5cycivg1lpw6m58sz4k6f3dwmx7zficwp") (y #t)))

