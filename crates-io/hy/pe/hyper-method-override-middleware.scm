(define-module (crates-io hy pe hyper-method-override-middleware) #:use-module (crates-io))

(define-public crate-hyper-method-override-middleware-0.1.0 (c (n "hyper-method-override-middleware") (v "0.1.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1hsq1a5h738clmv6qryy1zyj9n6fza5z6kqgw38yfyx77xbpz58p")))

(define-public crate-hyper-method-override-middleware-1.0.0 (c (n "hyper-method-override-middleware") (v "1.0.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1z4n46w4mm1rslhbxlscg186552n4c66xfahxf4lhzpyz8yvk9a5")))

