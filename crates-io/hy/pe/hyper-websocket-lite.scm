(define-module (crates-io hy pe hyper-websocket-lite) #:use-module (crates-io))

(define-public crate-hyper-websocket-lite-0.3.3 (c (n "hyper-websocket-lite") (v "0.3.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (d #t) (k 0)) (d (n "websocket-codec") (r "^0.3.3") (d #t) (k 0)))) (h "1j498hy5vfa2i7p8p2xv4rn70mkpgm48fw15xg2azcyxz88krskw")))

(define-public crate-hyper-websocket-lite-0.3.4 (c (n "hyper-websocket-lite") (v "0.3.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)) (d (n "websocket-codec") (r "^0.3.4") (d #t) (k 0)))) (h "0nczjkgv9q2b2yf00visybrs37dsiagvaz292vqbahsxf5dlzjak")))

(define-public crate-hyper-websocket-lite-0.3.5 (c (n "hyper-websocket-lite") (v "0.3.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)) (d (n "websocket-codec") (r "^0.3.5") (d #t) (k 0)))) (h "10i8ki0ymdbbxnp55i9ax3n1cyyvrqhyqq5hpaffcansw4k3xrq2")))

(define-public crate-hyper-websocket-lite-0.5.0 (c (n "hyper-websocket-lite") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "server"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("tcp"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)) (d (n "websocket-codec") (r "^0.5") (d #t) (k 0)))) (h "0yi2ki6dgja5qhv1bgrp1rafg7lbn6nm4vq3xnhvlbsdd9hhqkhb")))

