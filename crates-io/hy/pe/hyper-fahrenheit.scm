(define-module (crates-io hy pe hyper-fahrenheit) #:use-module (crates-io))

(define-public crate-hyper-fahrenheit-0.1.0 (c (n "hyper-fahrenheit") (v "0.1.0") (d (list (d (n "fahrenheit") (r "^4.5") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (k 0)) (d (n "tokio") (r "^0.2") (k 0)))) (h "0w2wqhcjzgd0xqpnd6cbrmxin694aissx90i61ri5cmzpzmbfxkv")))

(define-public crate-hyper-fahrenheit-0.1.1 (c (n "hyper-fahrenheit") (v "0.1.1") (d (list (d (n "fahrenheit") (r "^4.5") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (k 0)) (d (n "tokio") (r "^0.2") (k 0)))) (h "0qg8japxy5yxw0hbkxks1wnyf5hhzaipvrvyf6jkqfk7n7rd9k1q")))

