(define-module (crates-io hy pe hyperbole_macros) #:use-module (crates-io))

(define-public crate-hyperbole_macros-0.1.0-alpha.0 (c (n "hyperbole_macros") (v "0.1.0-alpha.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "15pmg1fxv1l0biiyzjf1kiknyds2kn2x3ggdkkdjixsg5qr44jrm")))

(define-public crate-hyperbole_macros-0.1.0-alpha.1 (c (n "hyperbole_macros") (v "0.1.0-alpha.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0fy70zfpp3wvvw688krzna040s9kvbhhhkqgx2xkm9pwm2ny52zx")))

(define-public crate-hyperbole_macros-0.1.0 (c (n "hyperbole_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "09nxskqisfvzvq3743q4whlcc998ci1rsm0xjsgj2wnp9fjhcxir")))

