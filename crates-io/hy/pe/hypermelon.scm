(define-module (crates-io hy pe hypermelon) #:use-module (crates-io))

(define-public crate-hypermelon-0.1.0 (c (n "hypermelon") (v "0.1.0") (h "1ka3r9xfr7jqx0a93rxm6hfkbcvmzy70csl79ymkzwdl915zk4qx") (y #t)))

(define-public crate-hypermelon-0.1.1 (c (n "hypermelon") (v "0.1.1") (h "154lhkk7sdjh9ylqh41zfhiv5lhh4wa7yxzy487w1mqy2mjlkh1z") (y #t)))

(define-public crate-hypermelon-0.2.0 (c (n "hypermelon") (v "0.2.0") (h "0wqfqallpyqr9kqx5barad1ilc2qzl77wxsdyj64zx77m1fkxpgy") (y #t)))

(define-public crate-hypermelon-0.3.0 (c (n "hypermelon") (v "0.3.0") (h "040hja8i15nwdf0sj6vkikjd7zgss0b0pz0da4m9ygwaa9vlcl5w") (y #t)))

(define-public crate-hypermelon-0.3.1 (c (n "hypermelon") (v "0.3.1") (h "18gmd82gxj52i5n92v8iswffqcz3pn9h3zrqjyyaicdrnr99q1wy") (y #t)))

(define-public crate-hypermelon-0.3.2 (c (n "hypermelon") (v "0.3.2") (h "10xp9s7v38sqdvv4wzp2ds4aqqpkdh8vid074naayv5n6aizkff3") (y #t)))

(define-public crate-hypermelon-0.3.3 (c (n "hypermelon") (v "0.3.3") (h "08l0a9zm2cwk970c0w2dicmq4r40z16azm3zlhkgfsx235l70dy6") (y #t)))

(define-public crate-hypermelon-0.3.4 (c (n "hypermelon") (v "0.3.4") (h "1md5vzcnbyp5422skb71amjmdhar03srcpnlg3mqq8w1s7jjsksf") (y #t)))

(define-public crate-hypermelon-0.3.5 (c (n "hypermelon") (v "0.3.5") (h "00fyh20046n4ymhr2yjcd6iaygrp1yk04q1k74bw8fbqr0x2in6s") (y #t)))

(define-public crate-hypermelon-0.3.6 (c (n "hypermelon") (v "0.3.6") (h "11b3qa3n3q9k4j8p5yh5iic8hbbglpvw33h7nd8hc561cg8a2s05") (y #t)))

(define-public crate-hypermelon-0.3.7 (c (n "hypermelon") (v "0.3.7") (h "1n2pfkzv33ip3avccdb17n66jwilg001ag4iff83cdfmcrsmh3hj") (y #t)))

(define-public crate-hypermelon-0.3.8 (c (n "hypermelon") (v "0.3.8") (h "1ssvc9j1vr20d1ganycl203wfy7d7fzlxm5fqv2pdz27sms0qf00") (y #t)))

(define-public crate-hypermelon-0.3.9 (c (n "hypermelon") (v "0.3.9") (h "075m0n956avwbdpxkwmkh6q8m42j4kyjxp6cam10mcph3ll56hmv") (y #t)))

(define-public crate-hypermelon-0.3.10 (c (n "hypermelon") (v "0.3.10") (h "0qqvg9hzaaxrksj0m9mds4nnp304bclaa70vl4p7bg7ra22r1lcj") (y #t)))

(define-public crate-hypermelon-0.3.11 (c (n "hypermelon") (v "0.3.11") (h "161pdhrw20ky97w1mvgk3v6njbzrx6cc4kfhz9wwmhcn058rk771") (y #t)))

(define-public crate-hypermelon-0.3.12 (c (n "hypermelon") (v "0.3.12") (h "0bpmvrzsymwdqp8c0wnscixcjz6rr1pww8nnbn0qz9sdzi4p8v08")))

(define-public crate-hypermelon-0.3.13 (c (n "hypermelon") (v "0.3.13") (h "17k5cz3wp3k6nds3bhplfsmjdfaw2i0qkgndx0cllqbvmvf2bq28")))

(define-public crate-hypermelon-0.4.0 (c (n "hypermelon") (v "0.4.0") (h "03i044hps6xc9i8bfg1rw1kkvdcnzq8nzi36wj6zfjkqx896an0p")))

(define-public crate-hypermelon-0.5.0 (c (n "hypermelon") (v "0.5.0") (h "06gx6b5in12zrj9lndgv8rpmf46xdkjkrj7kihx3x81m49lvvi40")))

(define-public crate-hypermelon-0.5.1 (c (n "hypermelon") (v "0.5.1") (h "1cp776bwhzj1cqs9c55i311rncsk2wizan6xf2nyfx82kvj2n4ax")))

(define-public crate-hypermelon-0.5.2 (c (n "hypermelon") (v "0.5.2") (h "0g29i4h3jqxrk09788zhlz7qrxx1dhjc82slzqylmf5liy58623g")))

(define-public crate-hypermelon-0.5.3 (c (n "hypermelon") (v "0.5.3") (h "02ai8c5pc4czpmbp8yipphg40pzxwypnrv5z2jikm0a763jg7q97")))

(define-public crate-hypermelon-0.5.4 (c (n "hypermelon") (v "0.5.4") (h "1mmwmkaq35qzikiz48m9agxp8zb66c217vsjri2z6y1rl0hjkd1x")))

(define-public crate-hypermelon-0.5.5 (c (n "hypermelon") (v "0.5.5") (h "1chr0xd7b1yh7k5ilbg3rhl01fzj3hcpzhdsi3z7wp3ja6wk47d2")))

(define-public crate-hypermelon-0.6.0 (c (n "hypermelon") (v "0.6.0") (h "0h10rh32gchr33ikznvn07nzjapvcbxsdcf6la2fxd4g6sgzi67l") (y #t)))

(define-public crate-hypermelon-0.6.1 (c (n "hypermelon") (v "0.6.1") (h "1j95ax4qb8gk9230fjymx323zrhmiw0fw6rmlvhc42nhrgbp1psb")))

(define-public crate-hypermelon-1.0.0 (c (n "hypermelon") (v "1.0.0") (h "0869a6sh6lma26bhhjh7aq2hya6dh39bqfpvy5drkn3wl5xipsna")))

