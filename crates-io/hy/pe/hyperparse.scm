(define-module (crates-io hy pe hyperparse) #:use-module (crates-io))

(define-public crate-hyperparse-0.1.0 (c (n "hyperparse") (v "0.1.0") (h "1r8m401wymvfqv3m54gb7kgmdn2g4z6f0hj4d27kq62ywk34gd92")))

(define-public crate-hyperparse-0.1.1 (c (n "hyperparse") (v "0.1.1") (h "1z8hb23c8xjkg7vlicy7hfygqmdcp2glwqs4gfvdpf6g67i785xs")))

(define-public crate-hyperparse-0.1.2 (c (n "hyperparse") (v "0.1.2") (h "06bla90m6rvsz5dpx03nk8rqg54i3i8jdq1gpq9wgwgldr4lxrgl")))

