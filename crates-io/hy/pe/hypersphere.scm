(define-module (crates-io hy pe hypersphere) #:use-module (crates-io))

(define-public crate-hypersphere-0.1.0 (c (n "hypersphere") (v "0.1.0") (d (list (d (n "glam") (r "^0.24.0") (d #t) (k 0)))) (h "1rlk59qz4nc8c0xmihb3c3l7danp8nrqyp3982v0aans63b29pql")))

(define-public crate-hypersphere-0.1.1 (c (n "hypersphere") (v "0.1.1") (d (list (d (n "glam") (r "^0.24.0") (d #t) (k 0)))) (h "0xkg3m3x0fwh1sspicf6vdkc7rrfbi9mpnpba8s1yqrpjifhsy2n")))

