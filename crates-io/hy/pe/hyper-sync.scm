(define-module (crates-io hy pe hyper-sync) #:use-module (crates-io))

(define-public crate-hyper-sync-0.10.14 (c (n "hyper-sync") (v "0.10.14") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "httparse") (r "^1.0") (d #t) (k 0)) (d (n "language-tags") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime") (r "^0.3.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicase") (r "^2.1") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "15i4a2bsyaf9xxfsygs8qdm7rf43szl9bnvlnr2vmhnx3xxdnj00") (f (quote (("nightly")))) (y #t)))

