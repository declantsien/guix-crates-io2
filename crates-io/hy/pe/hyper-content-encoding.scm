(define-module (crates-io hy pe hyper-content-encoding) #:use-module (crates-io))

(define-public crate-hyper-content-encoding-0.1.0 (c (n "hyper-content-encoding") (v "0.1.0") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.1") (d #t) (k 0)) (d (n "hyper") (r "^1.3.1") (d #t) (k 0)) (d (n "hyper-util") (r "^0.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "06q7ca50bjzhmivrrr424pjrh06a5mnhx777il4xiad0farangi9")))

(define-public crate-hyper-content-encoding-0.1.1 (c (n "hyper-content-encoding") (v "0.1.1") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.1") (d #t) (k 0)) (d (n "hyper") (r "^1.3.1") (d #t) (k 0)) (d (n "hyper-util") (r "^0.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "0bgkhbpgp3wn8s318p4q9wvl133235gcliwqk99wa1a0g62pxkh0")))

