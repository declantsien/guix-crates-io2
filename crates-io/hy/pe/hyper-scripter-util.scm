(define-module (crates-io hy pe hyper-scripter-util) #:use-module (crates-io))

(define-public crate-hyper-scripter-util-0.3.2 (c (n "hyper-scripter-util") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)))) (h "04ilab3yagi2zh0dzqvq9nwf52qjm59b2rl3s6b0wfd5i25dv7ab")))

(define-public crate-hyper-scripter-util-0.4.0 (c (n "hyper-scripter-util") (v "0.4.0") (h "0r8bppg5d4zjcd8ipw7wld3ckasx7ywjwwwrkbnshkxshlyzyr5v")))

(define-public crate-hyper-scripter-util-0.4.2 (c (n "hyper-scripter-util") (v "0.4.2") (h "0i5rvfnjx4i7br3fdl87jzzckkqh770iaz007gjrz4qy6di6f855")))

(define-public crate-hyper-scripter-util-0.4.5 (c (n "hyper-scripter-util") (v "0.4.5") (h "0lhvpcsm6mg9m8637n351s7p63p62y815ni8m5d4h4ax5lp1750h")))

(define-public crate-hyper-scripter-util-0.4.6 (c (n "hyper-scripter-util") (v "0.4.6") (h "1ga90z60rnlb7dvmazvhrslg21lqgp4g9b5lgcjjx34wnrkgh1fd")))

(define-public crate-hyper-scripter-util-0.4.8 (c (n "hyper-scripter-util") (v "0.4.8") (h "1wkd2vbg9aafkm453aibmbl93a3g95w6m71ncdbp2w03wcfk1fx3")))

(define-public crate-hyper-scripter-util-0.5.1 (c (n "hyper-scripter-util") (v "0.5.1") (h "1rgx9m5rd8f4iw0a6i0cvqrps29hyn1604a4psjlzscpws0ad14r")))

(define-public crate-hyper-scripter-util-0.5.5 (c (n "hyper-scripter-util") (v "0.5.5") (h "0xfymlpjawnpl3c2rcbp9yjamg5a3nm3ykm9snswqfsfcbkk0kq7")))

(define-public crate-hyper-scripter-util-0.5.7 (c (n "hyper-scripter-util") (v "0.5.7") (h "0x80zvg7akfgph3zklcrqdfy2bxslla0v70yny4y4qjmshx8cz1l")))

(define-public crate-hyper-scripter-util-0.5.9 (c (n "hyper-scripter-util") (v "0.5.9") (h "1w971mf2k917ad370yw5wzmcm271dr1rwv4vrbgg1zc5ay2z67a0")))

(define-public crate-hyper-scripter-util-0.5.10 (c (n "hyper-scripter-util") (v "0.5.10") (h "0pczpia2zpchffn79gyzxd43sjfhw3m7b5d5b1589pg7fnaqwc69")))

(define-public crate-hyper-scripter-util-0.5.12 (c (n "hyper-scripter-util") (v "0.5.12") (h "1xavirvwqzim194zdm0lmn54yw5393iii6zywdrx4bkskss7gpsn")))

(define-public crate-hyper-scripter-util-0.5.15 (c (n "hyper-scripter-util") (v "0.5.15") (h "1yj065n2i32fjgnpbm1j2jlv6ai3dgybp08ikr35wwp75y80w3pq")))

(define-public crate-hyper-scripter-util-0.5.19 (c (n "hyper-scripter-util") (v "0.5.19") (h "0j19nqvi8b3rsz4v8y7dslb0l2ch7fbanzc7xanxdbjscffsfs9i")))

(define-public crate-hyper-scripter-util-0.5.24 (c (n "hyper-scripter-util") (v "0.5.24") (h "06qrr7f3c1i17p1gd9l2acpa0j8cnm35xdkzc6b6prx83cmsj58v")))

(define-public crate-hyper-scripter-util-0.6.0 (c (n "hyper-scripter-util") (v "0.6.0") (h "0qvxdqgmx8f0cgpr2zqs9ac411yzg2wnjyqkijg8jagi09rwcnps")))

(define-public crate-hyper-scripter-util-0.6.1 (c (n "hyper-scripter-util") (v "0.6.1") (h "14x32gvdn88cdsnjgy9frzw2ja57izr6hbdzrxkvn3qi8ghfvhqa")))

(define-public crate-hyper-scripter-util-0.6.5 (c (n "hyper-scripter-util") (v "0.6.5") (h "1a0gmh33hldic9h6zmsqih40qw25ba4cykxakmpj7jryip4qhnf8")))

(define-public crate-hyper-scripter-util-0.6.8 (c (n "hyper-scripter-util") (v "0.6.8") (h "1hx3730lbpj22amjgyf3dbxgb8sphk1zapgigxh1pm19dv1zzlqk")))

(define-public crate-hyper-scripter-util-0.7.0 (c (n "hyper-scripter-util") (v "0.7.0") (h "02giiqiil2wzxldh6ab9pxsk8y2s4aqq34h84b8fbppwsvym3yf1")))

(define-public crate-hyper-scripter-util-0.7.1 (c (n "hyper-scripter-util") (v "0.7.1") (h "1vm8llka05ax76a8h8v9g32j81wrg9nkp1axvb489mqv1z2spwlw")))

(define-public crate-hyper-scripter-util-0.7.5 (c (n "hyper-scripter-util") (v "0.7.5") (h "0ahxppr55fag999l18rq6jh04mc388ism1hymv0brlwmc406wyhh")))

