(define-module (crates-io hy pe hypersave) #:use-module (crates-io))

(define-public crate-hypersave-0.1.0 (c (n "hypersave") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0scnzmayyxs0sz4yylnfd38p1x568nc59hzhgwwvs8dmy7qxgkri")))

