(define-module (crates-io hy pe hyperderive) #:use-module (crates-io))

(define-public crate-hyperderive-0.1.0 (c (n "hyperderive") (v "0.1.0") (h "1dc1cw3vnhhkkww93k4g5w4lc20mkib9x13jpnkphjj6fr97dcq8") (y #t)))

(define-public crate-hyperderive-0.0.0 (c (n "hyperderive") (v "0.0.0") (h "17ll2a2j5vq6i4wy839baa0czrgvndnn9df1ffprjry2nax6nhar") (y #t)))

(define-public crate-hyperderive-0.0.1 (c (n "hyperderive") (v "0.0.1") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1d0vmz9gyjrjb140j783bpqyixmkmpfb1ibynw16xzdg72d9bi30")))

(define-public crate-hyperderive-0.0.2 (c (n "hyperderive") (v "0.0.2") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0gfvscblg93r5ihmcwc414p8w8qn7304k7gngpl8bn2z100nrnc4")))

(define-public crate-hyperderive-0.0.3 (c (n "hyperderive") (v "0.0.3") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)) (d (n "synstructure") (r "^0.11.0") (d #t) (k 0)))) (h "1yg4gv5cay8wca5shkkb264y70wd7ph95rcq66czs7bz4xnq0ya6")))

