(define-module (crates-io hy pe hyper-scan) #:use-module (crates-io))

(define-public crate-hyper-scan-0.1.0 (c (n "hyper-scan") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "time" "process" "macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0brv21f78gb80k40xf3h1jsay943a8jf7qizww9pbsys0l65irjv") (r "1.60.0")))

(define-public crate-hyper-scan-0.1.1 (c (n "hyper-scan") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "time" "process" "macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1956sm6axsnpm3ds5gx1q1r9ix82mmamn128lr08cn93rj9lnm60") (r "1.60.0")))

