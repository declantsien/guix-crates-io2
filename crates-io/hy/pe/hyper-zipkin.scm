(define-module (crates-io hy pe hyper-zipkin) #:use-module (crates-io))

(define-public crate-hyper-zipkin-0.1.0 (c (n "hyper-zipkin") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "zipkin") (r "^0.1.0") (d #t) (k 0)))) (h "0fmg54xh010d52r7hvnnxh1p13h31akyc2w69j0zni9ca7a9j255")))

(define-public crate-hyper-zipkin-0.2.0 (c (n "hyper-zipkin") (v "0.2.0") (d (list (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "zipkin") (r "^0.1.0") (d #t) (k 0)))) (h "0srq0mgj1mr1mfs79mfd3cgwg8givpzhz16hz438076y69y54xz6")))

(define-public crate-hyper-zipkin-0.2.1 (c (n "hyper-zipkin") (v "0.2.1") (d (list (d (n "hyper") (r "^0.11") (k 0)) (d (n "zipkin") (r "^0.1.0") (d #t) (k 0)))) (h "193ia6x5hrh5lpfha1hccgqll0hxxh3dzdsi56x6acpyl8zxvdm7")))

(define-public crate-hyper-zipkin-0.3.0 (c (n "hyper-zipkin") (v "0.3.0") (d (list (d (n "hyper") (r "^0.11") (k 0)) (d (n "zipkin") (r "^0.2.0") (d #t) (k 0)))) (h "01m34pmw19pxx2yaghigl41djj87kv4j2bfkjfkzxwdyf20d8c6s")))

(define-public crate-hyper-zipkin-0.3.1 (c (n "hyper-zipkin") (v "0.3.1") (d (list (d (n "hyper") (r "^0.11") (k 0)) (d (n "zipkin") (r "^0.2.1") (d #t) (k 0)))) (h "1xa8ny0pfl95n9npdmc80qh53zbgfx9jn5ly0da5h368cgmkq4dc")))

(define-public crate-hyper-zipkin-0.4.0 (c (n "hyper-zipkin") (v "0.4.0") (d (list (d (n "hyper") (r "^0.11") (k 0)) (d (n "zipkin") (r "^0.3.0") (d #t) (k 0)))) (h "0k1idr5fxf1vl00jq1l1gxk4c2bv25hydchfwji3x27fgdwknmn8")))

