(define-module (crates-io hy pe hyperbridge) #:use-module (crates-io))

(define-public crate-hyperbridge-0.1.0 (c (n "hyperbridge") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)))) (h "0scdzvnxbzki0lsa2b8ghnvckbhp2vi7gwmkcmj160bs45x8czfs") (y #t)))

(define-public crate-hyperbridge-0.1.1 (c (n "hyperbridge") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "1fvcp0y4rjmyvqa74wni91wnhq43m7kdjy7zpgljp8hmb2ilff5a") (f (quote (("with-futures" "futures")))) (y #t)))

(define-public crate-hyperbridge-0.1.2 (c (n "hyperbridge") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "1m2diqjmkhkzsm8ax308nd4p8j43z6ncgg26klf8khnxyk080zbj") (f (quote (("with-futures" "futures")))) (y #t)))

(define-public crate-hyperbridge-0.1.3 (c (n "hyperbridge") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "1k6rb23x1gd9xv776jfrllzs3z79m9sgz0zrjs84gq01q8zjg8if") (f (quote (("with-futures" "futures")))) (y #t)))

(define-public crate-hyperbridge-0.1.4 (c (n "hyperbridge") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "09cjkrgycsmmpsaz6zxb5j1dg0f1irgs29g6g2g3pg8ffcabv1az") (f (quote (("with-futures" "futures")))) (y #t)))

(define-public crate-hyperbridge-0.1.5 (c (n "hyperbridge") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "1rhaskkqygwzxvhirmpfh36br80s97awm5f0s0dldfzp4a3lb7mz") (f (quote (("with-futures" "futures")))) (y #t)))

(define-public crate-hyperbridge-0.1.6 (c (n "hyperbridge") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "15890ilkpkqw30s78hpqvcls68zkmizc690prmw4j1ccnpyd2lr6") (f (quote (("with-futures" "futures")))) (y #t)))

(define-public crate-hyperbridge-0.1.7 (c (n "hyperbridge") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "02i61jcp7sgj5sw71w853bzq7r31jsw25xidr6bj26lpl1rhjsn8") (f (quote (("with-futures" "futures")))) (y #t)))

(define-public crate-hyperbridge-0.1.8 (c (n "hyperbridge") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "0jymxbwfc667zlzrlk9z0yjdkqla734j7spppkzfkv5xrav0kfwn") (f (quote (("with-futures" "futures")))) (y #t)))

(define-public crate-hyperbridge-0.1.9 (c (n "hyperbridge") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "1njaxw3gdbrj0qk6lf9hzkh0bihpjwir38fr90f69f0jzhbq28mr") (f (quote (("with-futures" "futures")))) (y #t)))

(define-public crate-hyperbridge-0.2.0 (c (n "hyperbridge") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "1q54h7q6w2dh1cw7ni59ffz24a1h7sm55761hcf78hw7mlq04rgg") (f (quote (("with-futures" "futures")))) (y #t)))

(define-public crate-hyperbridge-0.2.1 (c (n "hyperbridge") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "0ckzaxkqwppzsk8f3bnpgw7mr3mr48j4xc838nkhnr1mad92rgyk") (f (quote (("with-futures" "futures")))) (y #t)))

(define-public crate-hyperbridge-0.2.2 (c (n "hyperbridge") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "0aapdv7cg1z8h3m19wfal7f9dy6zv2yxf1swrjh7aikybpzwb6qf") (f (quote (("with-futures" "futures"))))))

(define-public crate-hyperbridge-0.2.3 (c (n "hyperbridge") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "0ccr83lwcrcjg9vmnk4ixdp1vaiw60cmxc0sdm0a39x8ln66799l") (f (quote (("with-futures" "futures"))))))

(define-public crate-hyperbridge-0.2.5 (c (n "hyperbridge") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "1fwba30ww808fl3kxj71fgs37b2czz7b563d5d61iw9ixq28srly") (f (quote (("with-futures" "futures"))))))

