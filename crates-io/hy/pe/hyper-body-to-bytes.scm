(define-module (crates-io hy pe hyper-body-to-bytes) #:use-module (crates-io))

(define-public crate-hyper-body-to-bytes-0.1.0 (c (n "hyper-body-to-bytes") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("stream"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "025m7bcrvgja3f0z6h4yj39b7k6pn046nlhb8f8xbn6zcik8cjgl")))

