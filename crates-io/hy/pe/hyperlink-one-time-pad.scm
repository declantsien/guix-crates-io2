(define-module (crates-io hy pe hyperlink-one-time-pad) #:use-module (crates-io))

(define-public crate-hyperlink-one-time-pad-0.1.0 (c (n "hyperlink-one-time-pad") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "17dgkhhmbdrh5hy13bhh5p6k4dh7l3vkbw49laha4yzrv0s36kak")))

(define-public crate-hyperlink-one-time-pad-0.2.0 (c (n "hyperlink-one-time-pad") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h4p6n81nlr4yvqsdhrw3wbjyc6yw59qmzpg0fwb7x53mxgsnjgv")))

