(define-module (crates-io hy pe hypermachines_sys) #:use-module (crates-io))

(define-public crate-hypermachines_sys-0.1.0 (c (n "hypermachines_sys") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)))) (h "04drwpa58jggclvhjn730iv7bfv8wk7sxkzcslrg1pmlxigc053i")))

