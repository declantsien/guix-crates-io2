(define-module (crates-io hy pe hyperide-macro) #:use-module (crates-io))

(define-public crate-hyperide-macro-0.0.1 (c (n "hyperide-macro") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstml") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1kndxs0vlm9qpd79kz7irh6f13pi0qihcjdlrnx8ymndvx21if3y")))

(define-public crate-hyperide-macro-0.0.2 (c (n "hyperide-macro") (v "0.0.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstml") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0zp70xps8syr50m8wxg0dsc0z89ln4b0z2jhwi1mhwh32jy4pyr7")))

(define-public crate-hyperide-macro-0.0.3 (c (n "hyperide-macro") (v "0.0.3") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstml") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0ryqz9xa1cyzgr56wr595y8786fi9m1ky0aiwww8k35yj0apx9q9")))

(define-public crate-hyperide-macro-0.0.4 (c (n "hyperide-macro") (v "0.0.4") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstml") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0rjv1n9dwpk3scwichb3vhl3mcp8d5kn35dfjvdy7sdpbpnlzqma")))

