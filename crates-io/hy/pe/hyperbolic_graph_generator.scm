(define-module (crates-io hy pe hyperbolic_graph_generator) #:use-module (crates-io))

(define-public crate-hyperbolic_graph_generator-0.1.0 (c (n "hyperbolic_graph_generator") (v "0.1.0") (d (list (d (n "GSL") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0bb4dqmcxpyha1j4mprk7fb1ljn1dkwy7z1qqf8qfirm8r9hv2g3")))

(define-public crate-hyperbolic_graph_generator-0.1.1 (c (n "hyperbolic_graph_generator") (v "0.1.1") (d (list (d (n "GSL") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1h9yjia0pa6qh6rkxmhz9h9h7gn5qzv1kqg3clk7r58v3xdp9bsj")))

(define-public crate-hyperbolic_graph_generator-0.1.2 (c (n "hyperbolic_graph_generator") (v "0.1.2") (d (list (d (n "GSL") (r "^1.1.0") (d #t) (k 0)))) (h "10nw3s3rdp5jnwfp6yw27fbn84rykgsq3gmagz2pcqdnwvssgmq8") (f (quote (("debug"))))))

(define-public crate-hyperbolic_graph_generator-0.1.3 (c (n "hyperbolic_graph_generator") (v "0.1.3") (d (list (d (n "GSL") (r "^1.1.0") (d #t) (k 0)))) (h "0qxrnb3ynjdip4n4j4lgwhw4p5x0w9xlm4cjzzww2amgpqhlih4k") (f (quote (("debug"))))))

