(define-module (crates-io hy pe hyperbitbit) #:use-module (crates-io))

(define-public crate-hyperbitbit-0.0.1-alpha.1 (c (n "hyperbitbit") (v "0.0.1-alpha.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.103") (f (quote ("std" "derive"))) (o #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "02zwy8ppyh49zkzqvjfgax37419lrpryylvbxxwam50shh3kgrn9") (f (quote (("serde_support" "serde"))))))

(define-public crate-hyperbitbit-0.0.1-alpha.2 (c (n "hyperbitbit") (v "0.0.1-alpha.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.103") (f (quote ("std" "derive"))) (o #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "1a83fsvvi7gzq4yardi16gzzrq8f72ya8q7razgfszx535kxjikn") (f (quote (("serde_support" "serde"))))))

