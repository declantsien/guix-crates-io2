(define-module (crates-io hy pe hyperoperator) #:use-module (crates-io))

(define-public crate-hyperoperator-1.0.0 (c (n "hyperoperator") (v "1.0.0") (d (list (d (n "num-bigfloat") (r "^1.6.2") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)))) (h "1j9caizdqp9bk1rh1m63w6qb74baxwna2jwh5wgvl8valiyf7lks") (f (quote (("num_bigint" "num-bigint" "num-traits") ("num_bigfloat" "num-bigfloat" "num-traits")))) (r "1.62")))

(define-public crate-hyperoperator-1.0.1 (c (n "hyperoperator") (v "1.0.1") (d (list (d (n "num-bigfloat") (r "^1.6.2") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)))) (h "05mdr4vw0q95xrf6v51f45zkkgvkbx8qxxajvqsih0ajb3mdhqif") (f (quote (("num_bigint" "num-bigint" "num-traits") ("num_bigfloat" "num-bigfloat" "num-traits")))) (r "1.62")))

