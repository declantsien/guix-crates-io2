(define-module (crates-io hy pe hyperdav) #:use-module (crates-io))

(define-public crate-hyperdav-0.1.0 (c (n "hyperdav") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.3") (d #t) (k 0)) (d (n "url") (r "^1.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0ny5ahdrvikxnyyax97aazph5rd65mnppbha3xj6yx0rcssclycs")))

(define-public crate-hyperdav-0.1.1 (c (n "hyperdav") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9.3") (d #t) (k 0)) (d (n "url") (r "^1.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0mg5324nmab520izj5vlsvrp0cfx1mmq96rx41ah02cs10qj2nj5")))

(define-public crate-hyperdav-0.1.2 (c (n "hyperdav") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9.3") (d #t) (k 0)) (d (n "url") (r "^1.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "1gpsnmpda1g40ic8ysjqdqxkr68y73v9cqj8lkacr7aklv77482m")))

(define-public crate-hyperdav-0.2.0 (c (n "hyperdav") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1jlcwvjd79g9cqd707qpv9w0n9wv3cbxpsabv7cldpk8jnhbsr75")))

