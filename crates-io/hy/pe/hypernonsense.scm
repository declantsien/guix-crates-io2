(define-module (crates-io hy pe hypernonsense) #:use-module (crates-io))

(define-public crate-hypernonsense-0.1.0 (c (n "hypernonsense") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "154aq3g02a8mdv34fdq2ds6qikirj4gi22yq3l52iaxsb4f42h6b")))

(define-public crate-hypernonsense-0.2.0 (c (n "hypernonsense") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0dfj87iaxf83q2yxk2g5ggxmix5z222zqchzc9z2853b7ml21zsw")))

(define-public crate-hypernonsense-0.3.0 (c (n "hypernonsense") (v "0.3.0") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1vm7q0alkzzkmwjin8cr1ggc46ypavwxczv3cjb7kxdy4naw6wyr")))

(define-public crate-hypernonsense-0.4.0 (c (n "hypernonsense") (v "0.4.0") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1xa4l6p1b1fywdbs1mi88al1l6vfr29b22aydmzs0qbljvgrm8l6")))

(define-public crate-hypernonsense-1.0.0 (c (n "hypernonsense") (v "1.0.0") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "00nqyn8hv8dzav7cbarj0l0b1cj3xgfwr339mqm8xz300pm4nrs2")))

(define-public crate-hypernonsense-1.0.1 (c (n "hypernonsense") (v "1.0.1") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "12j6p7hann6yrq168s4a2rzgais9chc8zaa73krwn7igymd5nngh")))

(define-public crate-hypernonsense-1.0.2 (c (n "hypernonsense") (v "1.0.2") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ni0339nrp55adv970zhfgc782bhbi7p99kdgijgd8i6axy1nryi")))

(define-public crate-hypernonsense-1.1.0 (c (n "hypernonsense") (v "1.1.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "1fwb973j89wzjps4w9c07f2w7q98sfvvz5g8w4w667fh6bq4kysv")))

(define-public crate-hypernonsense-2.0.0 (c (n "hypernonsense") (v "2.0.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "1v7dsnnrjhq6zbxvka92jidb9013kh17b78syvbmqhrdwxvrzwp8")))

(define-public crate-hypernonsense-2.0.1 (c (n "hypernonsense") (v "2.0.1") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "1wmjgv840pzmhm4i8gv61nf9k4lvgricv132fn9hssy4d6y3dpqc")))

(define-public crate-hypernonsense-2.0.2 (c (n "hypernonsense") (v "2.0.2") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "13i7xsj4jwvdz6ycfcgsfa42rvcwb6jbk46rnfar93m5dwff3nci")))

(define-public crate-hypernonsense-2.1.0 (c (n "hypernonsense") (v "2.1.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "1zd1x7bykm38lwzmqx4dwmfqwxmg2f9z74qxwwfypxl8pmci6rl6")))

(define-public crate-hypernonsense-2.2.0 (c (n "hypernonsense") (v "2.2.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "0223a5bd6wcig61ik8lz8f51cbp80x77ahjn8d1p4aqpyy7fkqwq")))

