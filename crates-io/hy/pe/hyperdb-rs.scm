(define-module (crates-io hy pe hyperdb-rs) #:use-module (crates-io))

(define-public crate-hyperdb-rs-0.1.0 (c (n "hyperdb-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0y8iyhriqz731ab4yq2vdr4jjz5350mwp7l63bnfip5fjx91jvwd")))

(define-public crate-hyperdb-rs-0.1.1 (c (n "hyperdb-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1hai23az3y1i3b55l5cc1a2vm18c3sdxar43pvnzb3ydi5g3kw6q")))

(define-public crate-hyperdb-rs-0.1.2 (c (n "hyperdb-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "079m2ing4imjaq0hm63lndr9wr1j3v2zghgs5iriw82wx5b0l4rv")))

