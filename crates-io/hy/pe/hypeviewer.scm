(define-module (crates-io hy pe hypeviewer) #:use-module (crates-io))

(define-public crate-hypeviewer-1.0.0 (c (n "hypeviewer") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "05mlpv9fmf7w8q9bi96wwd5vkkcyr5j7rwqrz3xbq9jgvw7m3dq7")))

