(define-module (crates-io hy pe hyper-futures) #:use-module (crates-io))

(define-public crate-hyper-futures-0.1.0 (c (n "hyper-futures") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0") (d #t) (k 2)) (d (n "hyper") (r "^1.1.0") (d #t) (k 0)) (d (n "hyper") (r "^1.1.0") (f (quote ("client" "http1"))) (d #t) (k 2)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "05bzgy9y2yvbzcdgxmh4jyfzbpzzlidqf09f9kcb8c5wd9bv61sd")))

(define-public crate-hyper-futures-0.1.1 (c (n "hyper-futures") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0") (d #t) (k 2)) (d (n "hyper") (r "^1.1.0") (d #t) (k 0)) (d (n "hyper") (r "^1.1.0") (f (quote ("client" "http1"))) (d #t) (k 2)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "0lw0i3ilbx5ib3pv7iysxcz96zd1763z08nkb9smcga4ic8xv3nn")))

