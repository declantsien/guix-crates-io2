(define-module (crates-io hy pe hypergraph) #:use-module (crates-io))

(define-public crate-hypergraph-0.1.0 (c (n "hypergraph") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "random_color") (r "^0.5.0") (d #t) (k 0)))) (h "1c7i5m53lrdg3bvy44xc3pw02saa03r1isr514al7hz3i8s19ij7")))

(define-public crate-hypergraph-0.1.1 (c (n "hypergraph") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "random_color") (r "^0.5.0") (d #t) (k 0)))) (h "0v32aa97a83ly5n3kj7hd6ijd2bvi2bhv48jlyxz2yiw0aianj47")))

(define-public crate-hypergraph-0.1.2 (c (n "hypergraph") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "random_color") (r "^0.5.0") (d #t) (k 0)))) (h "1ralpghd96jp5dz63w3dvjm31i7kirvpjfhknxrw05hs6w3vfc1n")))

(define-public crate-hypergraph-0.1.3 (c (n "hypergraph") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "random_color") (r "^0.5.0") (d #t) (k 0)))) (h "0gfngkv4zcagj9bzmfndca41k0xdzhhd6f3jmrlnv9cfd3n5aqqz")))

(define-public crate-hypergraph-0.1.4 (c (n "hypergraph") (v "0.1.4") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "random_color") (r "^0.5.0") (d #t) (k 0)))) (h "07ch1xbfb32qxwzjn8dq6v3yxpmcvq1qz8514rj20hy62gbwby4q")))

(define-public crate-hypergraph-0.1.5 (c (n "hypergraph") (v "0.1.5") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "random_color") (r "^0.5.0") (d #t) (k 0)))) (h "1wff4hiwww4fppbqipnw3n2fi7brp3aapphf8yj4gm3r991d1a4h")))

(define-public crate-hypergraph-0.1.6 (c (n "hypergraph") (v "0.1.6") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "random_color") (r "^0.5.0") (d #t) (k 0)))) (h "0n2m68c69b05ysfiqx35d5j2480f0kn3yaq33ynl7ajg1aqvq3gx")))

(define-public crate-hypergraph-0.2.0 (c (n "hypergraph") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "random_color") (r "^0.5.0") (d #t) (k 0)))) (h "0vx53070v8zh8ssmmr4srhl3lmlqkdrskn7p73vzi9cvdvwqb7sp")))

(define-public crate-hypergraph-0.2.1 (c (n "hypergraph") (v "0.2.1") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "random_color") (r "^0.5.0") (d #t) (k 0)))) (h "1vg74cz0gnn5fnw6vxp62s3jnpa86wmx1gjpldmvzmz7yf2b3dw4")))

(define-public crate-hypergraph-1.0.0 (c (n "hypergraph") (v "1.0.0") (d (list (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "10k4bn4yjvll1jmcjndyirixfbng5hi4b05ykwqlmh19r5yp0znd")))

(define-public crate-hypergraph-1.0.1 (c (n "hypergraph") (v "1.0.1") (d (list (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1gbcvxp8fah4ib5ld2023xhm22vwprvq7p2dnnjb77ky6djnsmgx")))

(define-public crate-hypergraph-1.0.2 (c (n "hypergraph") (v "1.0.2") (d (list (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1zi06sd7svydb2hfyg57liflh23hpirj806ji8rhq7iy69mr5mzq")))

(define-public crate-hypergraph-1.0.3 (c (n "hypergraph") (v "1.0.3") (d (list (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0nxkda0yf91am6nksbp7y3m2bv6g4jyg8xm7kb6b9znx4mgg7cn4")))

(define-public crate-hypergraph-1.0.4 (c (n "hypergraph") (v "1.0.4") (d (list (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "11x81ylbq6i0y89vq324vvwpr5jld67xyq0a1mwgm5bkk0i89r5v")))

(define-public crate-hypergraph-1.0.5 (c (n "hypergraph") (v "1.0.5") (d (list (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0n008l3892vx2dxf751c5d91s2pp6393mkxvvqhbqrw47afjjy7w")))

(define-public crate-hypergraph-1.0.6 (c (n "hypergraph") (v "1.0.6") (d (list (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "19zhi29rsb49ywg5ally9g7vay1c3ykwxx57gl5sc1ss0wj8pyvh") (r "1.56")))

(define-public crate-hypergraph-1.0.7 (c (n "hypergraph") (v "1.0.7") (d (list (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04lpc4690a1j75928xlawqahf9wv0fvs4dchs0a2y87yzghfhirc") (r "1.56")))

(define-public crate-hypergraph-1.1.0 (c (n "hypergraph") (v "1.1.0") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1cy4hsg4iryxb6wjs7p0dxdai3zzy0jgzg9ajzmsm69n4532z79n") (r "1.56")))

(define-public crate-hypergraph-1.1.1 (c (n "hypergraph") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1m5cj8h4ykcviz6746j8s2msx61a7gffzwsw1zvih8ksfcar3nmv") (r "1.56")))

(define-public crate-hypergraph-1.2.0 (c (n "hypergraph") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1z1i338shzbfg39pi8kvd4icf6f12r0z705dpsbl9i9by5bgqx6k") (r "1.56")))

(define-public crate-hypergraph-1.3.0 (c (n "hypergraph") (v "1.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1nwzzqdhjm183lcl314p4jkqkqc0l285qyzdw8z7ra6l3phwqjsp") (r "1.56")))

(define-public crate-hypergraph-1.3.1 (c (n "hypergraph") (v "1.3.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1vlgxv9ai0f8bdy9y7hvijpcwgl2qmqc3znhlzmkfkjd688znqaz") (r "1.56")))

(define-public crate-hypergraph-1.3.2 (c (n "hypergraph") (v "1.3.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "06w6zvpfynh3yrc4mwk90jr4jc1wvkj2s3k2dghqim82mjybl1nr") (r "1.56")))

(define-public crate-hypergraph-1.3.3 (c (n "hypergraph") (v "1.3.3") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1qc8xh3918f0c55975afgngxna92qk7xl45grzcwxz03sa72abpc") (r "1.56")))

(define-public crate-hypergraph-1.3.4 (c (n "hypergraph") (v "1.3.4") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "1majd2f0cvsrmr0xzi2r785mvj8h8vnhlf75vjz0d122g9b13nr1") (r "1.56")))

(define-public crate-hypergraph-1.3.5 (c (n "hypergraph") (v "1.3.5") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1r9iaykarbrhj8l943mvisrzdcla6xfvbgv1x94rkgja0cakv979") (r "1.56")))

(define-public crate-hypergraph-1.3.6 (c (n "hypergraph") (v "1.3.6") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1ms21iksidaaygjlbrf0z190pcyf6kcrk8zsdilnp7m1j7qbn3mh") (r "1.56")))

(define-public crate-hypergraph-1.3.7 (c (n "hypergraph") (v "1.3.7") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1af1laa6qn7p903cj4z8723d7dnawwld9nr1vpkf8sz88srci6i1") (r "1.56")))

(define-public crate-hypergraph-1.3.8 (c (n "hypergraph") (v "1.3.8") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0ymyj9xm0vjkpd88wgnvmvc551r4bi87brr7gbi9hlmbp843hy6g") (r "1.56")))

(define-public crate-hypergraph-1.3.9 (c (n "hypergraph") (v "1.3.9") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "16m94076y0lrfl08vrqnpw159bfs07wjlid9sykszhjr31c08iv1") (r "1.56")))

(define-public crate-hypergraph-2.0.0 (c (n "hypergraph") (v "2.0.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "103f4w9g0i14v1fpwwrqy2kj6yzqdszybczs70qg0z6g3jbzjaxd") (r "1.56")))

(define-public crate-hypergraph-2.1.0 (c (n "hypergraph") (v "2.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1kmcsj712rxbapawl7lskav9wqz98n5fia0ab9rmw74cqx1bsayc") (r "1.56")))

(define-public crate-hypergraph-2.1.1 (c (n "hypergraph") (v "2.1.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ivqsn6b95ssj4adcdq3qv5cz690zx8lapkj441l8919mjpyamyh") (r "1.56")))

(define-public crate-hypergraph-2.1.2 (c (n "hypergraph") (v "2.1.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0j2l9y1s33fbfnmiz3kmwdky6cg14hj7idia9p1ld5z9z9djh304") (r "1.56")))

