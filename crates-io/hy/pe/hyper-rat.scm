(define-module (crates-io hy pe hyper-rat) #:use-module (crates-io))

(define-public crate-hyper-rat-0.1.0 (c (n "hyper-rat") (v "0.1.0") (d (list (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "ramhorns") (r "^0.10.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0fnlq7j0j7ryahdpmdcy15w4g14k39ym54k0i69j8qwpc9p1h6cn")))

(define-public crate-hyper-rat-0.2.0 (c (n "hyper-rat") (v "0.2.0") (d (list (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "ramhorns") (r "^0.10.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "1amniq7jrsvrb6hln0pq6fa00dwnxsc2n04rvx0ywgbs6nqx8l3h")))

(define-public crate-hyper-rat-0.2.1 (c (n "hyper-rat") (v "0.2.1") (d (list (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "ramhorns") (r "^0.10.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "03xh4adpzgcc2qk5a99sqyxp6lspgl3bnnr7vxfpgw5kayq4c8xk")))

