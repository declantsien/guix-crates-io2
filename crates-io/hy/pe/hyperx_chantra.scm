(define-module (crates-io hy pe hyperx_chantra) #:use-module (crates-io))

(define-public crate-hyperx_chantra-1.4.0 (c (n "hyperx_chantra") (v "1.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "httpdate") (r "^1") (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0hr6if0a3nwbq2iqvlm82z8mwgizgavicr9k81jyb34bai0k7iz1") (f (quote (("nightly") ("headers") ("compat"))))))

