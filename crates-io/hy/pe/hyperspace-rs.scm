(define-module (crates-io hy pe hyperspace-rs) #:use-module (crates-io))

(define-public crate-hyperspace-rs-0.1.0 (c (n "hyperspace-rs") (v "0.1.0") (d (list (d (n "ethers") (r "^2.0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "04j26wiiy7md18crq57zd3sra4kxzlx017sj1wcx5g8ybq2zpjq7")))

(define-public crate-hyperspace-rs-0.2.0 (c (n "hyperspace-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ethers") (r "^2.0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0gkgmapqv71wmjarmfa47xk3xxpb3x8383r95fvjqw02im334dhz")))

