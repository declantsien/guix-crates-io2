(define-module (crates-io hy pe hypertext-macros) #:use-module (crates-io))

(define-public crate-hypertext-macros-0.1.0 (c (n "hypertext-macros") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0k31h25fhhxqckbphv0ga03r0db9n9qaj08z6zf5694y9p89i3ww")))

(define-public crate-hypertext-macros-0.2.0 (c (n "hypertext-macros") (v "0.2.0") (d (list (d (n "html-escape") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "03glsmww2vcmxidyqhlcjlyip5pii3fscdbcfnkcz0srywfba205")))

(define-public crate-hypertext-macros-0.2.1 (c (n "hypertext-macros") (v "0.2.1") (d (list (d (n "html-escape") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "009ml7zf6ras05wcgziy3650km0jwiyn2az2khbmvhjgwx5zddn6")))

(define-public crate-hypertext-macros-0.3.0 (c (n "hypertext-macros") (v "0.3.0") (d (list (d (n "html-escape") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "149nafs7a0afqci8akp03bjkhpkp9cl0n1iq9i26zqpi10ja95nj")))

(define-public crate-hypertext-macros-0.3.1 (c (n "hypertext-macros") (v "0.3.1") (d (list (d (n "html-escape") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1dqx0vq1zsfarkz0ilhfvidmajwlwqavgk3jykyfniqkypyvhr1k")))

(define-public crate-hypertext-macros-0.3.2 (c (n "hypertext-macros") (v "0.3.2") (d (list (d (n "html-escape") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "15b5ifh3382qm9a69hgi2s6bwn2mwbn843c3yab0hm1hpk4bd79l")))

(define-public crate-hypertext-macros-0.4.0 (c (n "hypertext-macros") (v "0.4.0") (d (list (d (n "html-escape") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ypgx7898yyds7y8hlyy1n7hfp6ym661s6n7vv8qw7idyd1rz7xr")))

(define-public crate-hypertext-macros-0.4.1 (c (n "hypertext-macros") (v "0.4.1") (d (list (d (n "html-escape") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1zwi36dd369wmry2ir712ibrkb4rnlrqjqdv50lvprrl9adr7s91")))

(define-public crate-hypertext-macros-0.5.0 (c (n "hypertext-macros") (v "0.5.0") (d (list (d (n "html-escape") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1v8fdx12d78c9xh5x81g8h1csgjx3aac5h3mb932bv6dj3qasrwx")))

