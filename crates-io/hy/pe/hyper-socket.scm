(define-module (crates-io hy pe hyper-socket) #:use-module (crates-io))

(define-public crate-hyper-socket-0.1.0 (c (n "hyper-socket") (v "0.1.0") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "05mn9vmrn29mgmlk018dw4bbi2sf1f6kmbiy3cikhpm53pxplqp3")))

(define-public crate-hyper-socket-0.2.0-alpha.1 (c (n "hyper-socket") (v "0.2.0-alpha.1") (d (list (d (n "hyper") (r "= 0.13.0-alpha.1") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.4") (d #t) (k 0)))) (h "0fa4j80767p6bk1dihh38k0kdnq8qwkclsbj6rjnq0xxiq66r06k")))

(define-public crate-hyper-socket-0.2.0 (c (n "hyper-socket") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("uds"))) (d #t) (k 0)))) (h "1bm72kx2szzng84i76f12fqv4gd30asl4v9dv83hzjs0ryqvippw")))

