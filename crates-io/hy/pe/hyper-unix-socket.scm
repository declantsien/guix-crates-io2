(define-module (crates-io hy pe hyper-unix-socket) #:use-module (crates-io))

(define-public crate-hyper-unix-socket-0.1.0 (c (n "hyper-unix-socket") (v "0.1.0") (h "1z826hnh6anzym064yvq7k6d86havmk1591x31kivajz51ml4cia") (y #t)))

(define-public crate-hyper-unix-socket-0.0.0-development (c (n "hyper-unix-socket") (v "0.0.0-development") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0") (d #t) (k 2)) (d (n "hyper") (r "^1.0.0") (k 0)) (d (n "hyper-util") (r "^0.1.0") (f (quote ("client-legacy" "tokio"))) (k 0)) (d (n "hyper-util") (r "^0.1.0") (f (quote ("http1"))) (k 2)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1") (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "io-util"))) (d #t) (k 2)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "1sz2l1wyz4q14p9ps4pzaw8k7wsm6s6p6iqh96zz4x0h5wrfpyks") (r "1.73.0")))

