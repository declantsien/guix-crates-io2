(define-module (crates-io hy pe hypersynthetic) #:use-module (crates-io))

(define-public crate-hypersynthetic-0.1.0 (c (n "hypersynthetic") (v "0.1.0") (d (list (d (n "hypersynthetic_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "hypersynthetic_types") (r "^0.1.0") (d #t) (k 0)))) (h "0x0k53wkh43l590i5bjc71y48087lmpqk00gkhlvw8szmjshr30p")))

(define-public crate-hypersynthetic-0.1.1 (c (n "hypersynthetic") (v "0.1.1") (d (list (d (n "htmlize") (r "^1.0.3") (d #t) (k 0)) (d (n "hypersynthetic_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "hypersynthetic_types") (r "^0.1.0") (d #t) (k 0)))) (h "017rb46l5gi0n8k0rr6zh115fl9aggj78i6imklhpsyc2rj7mksf")))

(define-public crate-hypersynthetic-0.1.2 (c (n "hypersynthetic") (v "0.1.2") (d (list (d (n "htmlize") (r "^1.0.3") (d #t) (k 0)) (d (n "hypersynthetic_macros") (r "^0.1.2") (d #t) (k 0)))) (h "10cia57ablpsdhilpg0vg760aydl0rp02d1i4wx14k2rb5ygph9z")))

(define-public crate-hypersynthetic-0.1.3 (c (n "hypersynthetic") (v "0.1.3") (d (list (d (n "htmlize") (r "^1.0.3") (d #t) (k 0)) (d (n "hypersynthetic_macros") (r "^0.1.3") (d #t) (k 0)))) (h "0xw3r7xdxj0niv1r1nvhdpxvsqf9aj7d177z3c93zayb6cmyg6aj")))

(define-public crate-hypersynthetic-0.2.0 (c (n "hypersynthetic") (v "0.2.0") (d (list (d (n "htmlize") (r "^1.0.3") (d #t) (k 0)) (d (n "hypersynthetic_macros") (r "^0.2.0") (d #t) (k 0)))) (h "13jk5mx7s8j1k0n2zm9wzvgwlqlnsxgdcd3vp59l3b8xkvin8a3a")))

(define-public crate-hypersynthetic-0.2.1 (c (n "hypersynthetic") (v "0.2.1") (d (list (d (n "htmlize") (r "^1.0.3") (d #t) (k 0)) (d (n "hypersynthetic_macros") (r "^0.2.0") (d #t) (k 0)))) (h "19d13zb1cry13avy0l7rxgb3xj9j1fc0hg484ann8c5rmyrqg2vk")))

(define-public crate-hypersynthetic-0.3.0 (c (n "hypersynthetic") (v "0.3.0") (d (list (d (n "htmlize") (r "^1.0.3") (d #t) (k 0)) (d (n "hypersynthetic_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (o #t) (d #t) (k 0)))) (h "1zdk9qhrx2zgacczkfrzmlnsarcrj17mjl782pf1sk875nl8n166") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-hypersynthetic-0.3.1 (c (n "hypersynthetic") (v "0.3.1") (d (list (d (n "htmlize") (r "^1.0.3") (d #t) (k 0)) (d (n "hypersynthetic_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "15ijypjb79d2c5a3f1pcwi9mny67gw4wb9byzya1csds6fch0v3f") (s 2) (e (quote (("rocket" "dep:rocket"))))))

