(define-module (crates-io hy pe hypersynthetic_types) #:use-module (crates-io))

(define-public crate-hypersynthetic_types-0.1.0 (c (n "hypersynthetic_types") (v "0.1.0") (h "0m2pnsn17r6d4wsr9i53pmr5ik41f6w76d1gs5ii1d98qzgji8x6")))

(define-public crate-hypersynthetic_types-0.1.1 (c (n "hypersynthetic_types") (v "0.1.1") (h "17f232ym6ggd3a65w7brf6x75fq007xjmssd7cr9lxah7smfn9fk")))

