(define-module (crates-io hy pe hypermangle-py) #:use-module (crates-io))

(define-public crate-hypermangle-py-0.1.0 (c (n "hypermangle-py") (v "0.1.0") (d (list (d (n "axum") (r "0.6.*") (f (quote ("macros" "ws"))) (d #t) (k 0)) (d (n "parking_lot") (r "0.12.*") (d #t) (k 0)) (d (n "pyo3") (r "0.19.*") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pyo3-asyncio") (r "0.19.*") (f (quote ("tokio-runtime" "attributes"))) (d #t) (k 0)) (d (n "tokio") (r "1.30.*") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "1y8xwx36d0hh3l7nimn63l7fq1hfiqzj04w000rssi716ly90nvq")))

(define-public crate-hypermangle-py-0.2.0 (c (n "hypermangle-py") (v "0.2.0") (d (list (d (n "axum") (r "0.6.*") (f (quote ("macros" "ws"))) (d #t) (k 0)) (d (n "parking_lot") (r "0.12.*") (d #t) (k 0)) (d (n "pyo3") (r "0.19.*") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pyo3-asyncio") (r "0.19.*") (f (quote ("tokio-runtime" "attributes"))) (d #t) (k 0)) (d (n "tokio") (r "1.30.*") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "124vmaa8ywk1vpj6rn2qfcp87g5k92dwyzw5vv1z83v0z2l17zjn")))

