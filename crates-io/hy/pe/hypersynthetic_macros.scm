(define-module (crates-io hy pe hypersynthetic_macros) #:use-module (crates-io))

(define-public crate-hypersynthetic_macros-0.1.0 (c (n "hypersynthetic_macros") (v "0.1.0") (d (list (d (n "htmlize") (r "^1.0.3") (d #t) (k 0)) (d (n "hypersynthetic_types") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "108c114gqas1qqxl6l1zj0frfb6fyxq3ppjd5ayphmldw32lxnsd")))

(define-public crate-hypersynthetic_macros-0.1.1 (c (n "hypersynthetic_macros") (v "0.1.1") (d (list (d (n "htmlize") (r "^1.0.3") (d #t) (k 2)) (d (n "hypersynthetic_types") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w6dh9xc9ns221abi3pim9bhspc0rn5y3w09fq8lrnbh68kq7hip")))

(define-public crate-hypersynthetic_macros-0.1.2 (c (n "hypersynthetic_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vxahf6nk1ngn8i0ii3sgns6wbihbfky6hn0hqb9hg0p963zf3yv")))

(define-public crate-hypersynthetic_macros-0.1.3 (c (n "hypersynthetic_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zkwcwrkzlkc1287m09n0nklj5ygr9iv105s1aj5bi25zq5x53nx")))

(define-public crate-hypersynthetic_macros-0.2.0 (c (n "hypersynthetic_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1slipzvyypqsi48jdb4rz77pr0rx3k7zcsw21yg39w79703kacjp")))

(define-public crate-hypersynthetic_macros-0.3.0 (c (n "hypersynthetic_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pzskp5c0k78dabn0bs6fng10r2f4d17l0kjjq323fxfi1mgjsv9")))

