(define-module (crates-io hy pe hyper-stub) #:use-module (crates-io))

(define-public crate-hyper-stub-0.1.0 (c (n "hyper-stub") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "hyper") (r "^0.12.1") (d #t) (k 0)) (d (n "memsocket") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)))) (h "1m5filk2m8alb5ibi6vr8wf8hq765p3b5p3dc6xbv3kl4r4vhghi")))

