(define-module (crates-io hy pe hypercpu) #:use-module (crates-io))

(define-public crate-hypercpu-0.0.1 (c (n "hypercpu") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "0zflpzcdyr3jb4a6bmzncjc23f4andbcswqscx4qn1bkpldpjcxi")))

(define-public crate-hypercpu-0.0.2 (c (n "hypercpu") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "1g4v00kwsq4wdwgxf3f5irk4fbhpmwyzbhkpn67nc8c2ay9zvsyb")))

