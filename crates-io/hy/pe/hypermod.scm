(define-module (crates-io hy pe hypermod) #:use-module (crates-io))

(define-public crate-hypermod-0.1.0 (c (n "hypermod") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "086rjdb93zmz3lc9a0jixrdniwqgdx3r3sgkajm8vwj3ahdrimms")))

(define-public crate-hypermod-0.2.0 (c (n "hypermod") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0c6s59b5x1sgiy7sgzcks3zj4cza9cwv98b20ic5pw0mav9wgwds")))

