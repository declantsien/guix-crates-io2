(define-module (crates-io hy pe hyperpom) #:use-module (crates-io))

(define-public crate-hyperpom-0.1.0 (c (n "hyperpom") (v "0.1.0") (d (list (d (n "applevisor") (r "^0.1.1") (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "capstone") (r "^0.11.0") (d #t) (k 0)) (d (n "keystone-engine") (r "^0.1.0") (f (quote ("build-from-src"))) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rhexdump") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("local-offset" "formatting"))) (d #t) (k 0)))) (h "1chwb65j7z4wpb9a71j40sc7wl0sdq0vp4fqnh3w9i9jb98n7ah6")))

(define-public crate-hyperpom-0.1.1 (c (n "hyperpom") (v "0.1.1") (d (list (d (n "applevisor") (r "^0.1.2") (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "capstone") (r "^0.11.0") (d #t) (k 0)) (d (n "keystone-engine") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rhexdump") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("local-offset" "formatting"))) (d #t) (k 0)))) (h "0psyjklkavdczkv9m7nhd1wf2ydwzpabgs09f9npyy43ciqgw3p1")))

(define-public crate-hyperpom-0.1.2 (c (n "hyperpom") (v "0.1.2") (d (list (d (n "applevisor") (r "^0.1.2") (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "capstone") (r "^0.11.0") (d #t) (k 0)) (d (n "keystone-engine") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rhexdump") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("local-offset" "formatting"))) (d #t) (k 0)))) (h "0jnhndnyz3kbh4mqfjj14bm4h4g1m4h242q0kqh6y69r3yxglsm8")))

