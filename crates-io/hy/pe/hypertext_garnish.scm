(define-module (crates-io hy pe hypertext_garnish) #:use-module (crates-io))

(define-public crate-hypertext_garnish-0.1.0 (c (n "hypertext_garnish") (v "0.1.0") (d (list (d (n "garnish_lang_compiler") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "garnish_lang_runtime") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "garnish_lang_simple_data") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "garnish_lang_traits") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_garnish") (r "^0.1.0") (d #t) (k 0)))) (h "0587vzg80as0sb3ysvpyly9r1x1pi4mkx531sbc1806a38rj8irm")))

(define-public crate-hypertext_garnish-0.2.0 (c (n "hypertext_garnish") (v "0.2.0") (d (list (d (n "garnish_lang") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_garnish") (r "^0.2.0") (d #t) (k 0)))) (h "0a6ig8jwzdjh30qkwknw271sqgn292g9jffb49mfzhb985igbspl")))

(define-public crate-hypertext_garnish-0.3.0 (c (n "hypertext_garnish") (v "0.3.0") (d (list (d (n "garnish_lang") (r "^0.0.5-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_garnish") (r "^0.3.0") (d #t) (k 0)))) (h "10d9v81jm7djbqwfz87rm4h0fm9c9nrwgcw22ahy1i70kzqyjl9i")))

