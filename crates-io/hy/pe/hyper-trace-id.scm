(define-module (crates-io hy pe hyper-trace-id) #:use-module (crates-io))

(define-public crate-hyper-trace-id-0.1.0 (c (n "hyper-trace-id") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.19") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (f (quote ("util"))) (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "021wxvmcw6ay6snd48d9qzkn8nzqgc0332124imy66fia61zakgz") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum" "dep:axum"))))))

