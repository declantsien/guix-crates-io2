(define-module (crates-io hy pe hyper-usse) #:use-module (crates-io))

(define-public crate-hyper-usse-0.1.0 (c (n "hyper-usse") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("compat"))) (d #t) (k 2)) (d (n "futures-locks") (r "^0.5.0") (d #t) (k 2)) (d (n "hyper") (r "^0.13.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "0bp6cyr1p5wfkmqmgn9wqw5sf1f2f1r7ild3vnrv68amkyg17kdg")))

(define-public crate-hyper-usse-0.2.0 (c (n "hyper-usse") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("time" "macros" "sync" "stream"))) (d #t) (k 2)))) (h "1qfkbxnn2i0v1vnr61rgh47v7p0azm0ncx20gsrn61wbpbr8bx5p")))

(define-public crate-hyper-usse-0.3.0 (c (n "hyper-usse") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("time" "macros" "sync" "stream" "io-util" "io-std"))) (d #t) (k 2)))) (h "0m7jpha67j3rs9d7q5y95q9bvbf9hsr5slvdvkx24qx25kldblix")))

(define-public crate-hyper-usse-0.3.1 (c (n "hyper-usse") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("time" "macros" "sync" "stream" "io-util" "io-std"))) (d #t) (k 2)))) (h "0mp470glvdgbkb36zlvmxd4w205w0ldhjrlsnjg8rgsnpvax4scm")))

(define-public crate-hyper-usse-0.3.2 (c (n "hyper-usse") (v "0.3.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("time" "macros" "sync" "stream" "io-util" "io-std"))) (d #t) (k 2)))) (h "10lglqy55vwfs64hzvqdhh9m3abwx6i6p7pnn3zxqhxbfkawpmhf")))

