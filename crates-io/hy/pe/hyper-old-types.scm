(define-module (crates-io hy pe hyper-old-types) #:use-module (crates-io))

(define-public crate-hyper-old-types-0.11.0 (c (n "hyper-old-types") (v "0.11.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "httparse") (r "^1.0") (d #t) (k 0)) (d (n "language-tags") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime") (r "^0.3.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "1i69sks0bwamzqdbx8ffgkssxffv6crdmwjgl47nr5pkxi8vx5k8") (f (quote (("nightly") ("default") ("compat" "http"))))))

