(define-module (crates-io hy pe hyperdex) #:use-module (crates-io))

(define-public crate-hyperdex-0.0.1 (c (n "hyperdex") (v "0.0.1") (h "1csdppal8726hn6ia7inay7n97y1s46lixijr1dicc84syih2lav")))

(define-public crate-hyperdex-0.0.2 (c (n "hyperdex") (v "0.0.2") (h "031rmv9xn8nmmh9ayjq8qh622a8sj74hph0l8a67rg1rmk6vjymv")))

(define-public crate-hyperdex-1.0.0 (c (n "hyperdex") (v "1.0.0") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 0)))) (h "1br4nazxrrf9r4xynvvkskxraq1m2w8j39pzxl79skvvqnjcnw8j")))

(define-public crate-hyperdex-1.1.0 (c (n "hyperdex") (v "1.1.0") (d (list (d (n "errno") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "1zqdgxyyqdpn3dhn87jxz4rxjyl2la9pp82rys2bnf7vfbp17m40")))

