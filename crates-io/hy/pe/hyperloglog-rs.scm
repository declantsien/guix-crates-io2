(define-module (crates-io hy pe hyperloglog-rs) #:use-module (crates-io))

(define-public crate-hyperloglog-rs-0.1.0 (c (n "hyperloglog-rs") (v "0.1.0") (h "14msf1ckv67s9blccqg9czccrrb7y7xdji9w2aswj66rmwnpjdsf")))

(define-public crate-hyperloglog-rs-0.1.1 (c (n "hyperloglog-rs") (v "0.1.1") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "088v5gwj72ysdmj1azzkv2aqcgyy1bk5ac3pimvrb5ll82pm5xq7")))

(define-public crate-hyperloglog-rs-0.1.3 (c (n "hyperloglog-rs") (v "0.1.3") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0kc44jm1iycgm7hgnb5d89aagicq3fawa5ssaj33wacr3s0sg3cy")))

(define-public crate-hyperloglog-rs-0.1.4 (c (n "hyperloglog-rs") (v "0.1.4") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0968jnp5khcfmwy4jw1dkzyxwgppnv4wcldwc80j142g5i8fain9")))

(define-public crate-hyperloglog-rs-0.1.5 (c (n "hyperloglog-rs") (v "0.1.5") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1ygqwakq1xhkjqp6phkmyqjmxzcgdnfl65d4qa3ywfdhqa88w162")))

(define-public crate-hyperloglog-rs-0.1.6 (c (n "hyperloglog-rs") (v "0.1.6") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "00z53pvfdrwmhdksw7r8ppgzlrn0dhjgvw8r80cmvid6pqpagc74")))

(define-public crate-hyperloglog-rs-0.1.7 (c (n "hyperloglog-rs") (v "0.1.7") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0hnfnv6sla7dk9rxl8689jb35r3xkb7dxspicpadqcgy4qkf65m6")))

(define-public crate-hyperloglog-rs-0.1.8 (c (n "hyperloglog-rs") (v "0.1.8") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1rj1ajrm06ljgb81bcwsjbcpwmji5d3ifjzs0alg5yn8q14l4aij")))

(define-public crate-hyperloglog-rs-0.1.9 (c (n "hyperloglog-rs") (v "0.1.9") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1z1r7mmx1sqcsi5r93gh93rv8lgvxphj07mzq4m9ql3rl5804nic")))

(define-public crate-hyperloglog-rs-0.1.10 (c (n "hyperloglog-rs") (v "0.1.10") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "19s3519pbgvzrqbigia6xp6ppc3bkkxdcaw8p8cwmsff95d7i780")))

(define-public crate-hyperloglog-rs-0.1.11 (c (n "hyperloglog-rs") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "01wv7nlrvas88h1g834hlk38zz7dc2figmngjrmbfpass4fg0c27")))

(define-public crate-hyperloglog-rs-0.1.12 (c (n "hyperloglog-rs") (v "0.1.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1d8rlmhc0ma62qp6p7jyj8c8z2acbf7q5dr945vrfcav9w8gjql4")))

(define-public crate-hyperloglog-rs-0.1.13 (c (n "hyperloglog-rs") (v "0.1.13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0khnrzmc9wc85bfs9h68vm4df33w4lsk7bcjqp0mfrilldy2q60f")))

(define-public crate-hyperloglog-rs-0.1.14 (c (n "hyperloglog-rs") (v "0.1.14") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1x3jbfy5i8jmws9n0vn7hn53kvb97l034j4xy4jqbnih33yaiwl9")))

(define-public crate-hyperloglog-rs-0.1.15 (c (n "hyperloglog-rs") (v "0.1.15") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0h72k2xh550a8hgbk6a9xpkwdkli1017a8nyk1gvvg89079d7jnn")))

(define-public crate-hyperloglog-rs-0.1.17 (c (n "hyperloglog-rs") (v "0.1.17") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0gffxspjx11hv4wiz5ryh3kan5l0zn4adxjmabaxiifg25300665")))

(define-public crate-hyperloglog-rs-0.1.19 (c (n "hyperloglog-rs") (v "0.1.19") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0v7kg9hk0d9aiigj78pm20rqill0k322xgjkafcx2z4za6jp77k6")))

(define-public crate-hyperloglog-rs-0.1.21 (c (n "hyperloglog-rs") (v "0.1.21") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0sb0azh5qqybcbbrsclp7m9p89x582zgvxjgckswjm6smv9hv3fb")))

(define-public crate-hyperloglog-rs-0.1.22 (c (n "hyperloglog-rs") (v "0.1.22") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0b52k7pd94pwjakjdsp4l1z6ayizwrbkz8vkh10l4zsp42g37qwi")))

(define-public crate-hyperloglog-rs-0.1.23 (c (n "hyperloglog-rs") (v "0.1.23") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0sypsmqhdypyj1p33df2x4hv4854pj0m8q7zyxw9kaxvssi1l47v")))

(define-public crate-hyperloglog-rs-0.1.24 (c (n "hyperloglog-rs") (v "0.1.24") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1j44vkmpd9f93cqigi19j5x1m04xn4fllqdfzgpxi61gn2lry17d")))

(define-public crate-hyperloglog-rs-0.1.25 (c (n "hyperloglog-rs") (v "0.1.25") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1qb6dyrr6a5qmakyjx489nv8np2ld3zp8ah2isc6h61biv3h3yna")))

(define-public crate-hyperloglog-rs-0.1.26 (c (n "hyperloglog-rs") (v "0.1.26") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0kyryyw6agd8kam4chzy71h87mmlrfns87m0yczb09s0asq9mdbb")))

(define-public crate-hyperloglog-rs-0.1.27 (c (n "hyperloglog-rs") (v "0.1.27") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1h4sfyiwphzyhjzh8dpi4sf90ipn4bhbzkzykc2w4man90wxcv5v")))

(define-public crate-hyperloglog-rs-0.1.28 (c (n "hyperloglog-rs") (v "0.1.28") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1ccpdi1c6avcf5gq3gap2fnvkfy3x4ncrrb7wrm5spz1qcabxnwv") (y #t)))

(define-public crate-hyperloglog-rs-0.1.29 (c (n "hyperloglog-rs") (v "0.1.29") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "17krijkskdkwgc4r7sw77h7g3i8jp5vld3qrrjlk6qaak2azi9cg") (y #t)))

(define-public crate-hyperloglog-rs-0.1.30 (c (n "hyperloglog-rs") (v "0.1.30") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "09hyrciijfa09gvg6h4imhrgs81vmszaky5x4imh1pbv93iwr7x8") (y #t)))

(define-public crate-hyperloglog-rs-0.1.31 (c (n "hyperloglog-rs") (v "0.1.31") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1pl4pdfqq3l4r12470azg4qr0ksk7676j079dyk7xvhshk38d35c") (y #t)))

(define-public crate-hyperloglog-rs-0.1.32 (c (n "hyperloglog-rs") (v "0.1.32") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1v7fp1pk12p53yginbvmsd2crnbqzixfq2ijx7isj19519ijcwqc") (y #t)))

(define-public crate-hyperloglog-rs-0.1.33 (c (n "hyperloglog-rs") (v "0.1.33") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1ndmjqrq2wvdwlxy241bw9kx6s0nn06ngdbh7b1n8rcc32ic666c") (y #t)))

(define-public crate-hyperloglog-rs-0.1.34 (c (n "hyperloglog-rs") (v "0.1.34") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "109r49g09101i9yxzrsh96l2wxsvb58116vkm1494sm6xhkhs4sx")))

(define-public crate-hyperloglog-rs-0.1.36 (c (n "hyperloglog-rs") (v "0.1.36") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0vq7a6c27ikhbbca1p15il8svpl0bcbqhhsa9bn38aqwd0bp0ddc")))

(define-public crate-hyperloglog-rs-0.1.37 (c (n "hyperloglog-rs") (v "0.1.37") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0bvkc1rryfpnkdi4ibkkdzfb03yhs8ks2zml3g5765sqqlkmp4vg")))

(define-public crate-hyperloglog-rs-0.1.38 (c (n "hyperloglog-rs") (v "0.1.38") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0yh30ix1cnvk064ky6hbkfsv0g5ls4gdwk71l2w3x38alqqrw2zs")))

(define-public crate-hyperloglog-rs-0.1.40 (c (n "hyperloglog-rs") (v "0.1.40") (d (list (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1yfxq95ngpkcvlbafb8qwah7r8xhr135a8s6na80dy2f133l0lrj") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.41 (c (n "hyperloglog-rs") (v "0.1.41") (d (list (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0b3y6mjnrpcbyz2kbn94v310av75m91n6w5dg6nrb5l49iqj8da4") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.42 (c (n "hyperloglog-rs") (v "0.1.42") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1v0pmdglwj3bw6sw22pkygxq4prjpfixqgbz6r7920a4im686x86") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.43 (c (n "hyperloglog-rs") (v "0.1.43") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1xcpxndr5kwaadk1ixlj50mgakb32pbr0bk9a698yzfqazawzh4r") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.44 (c (n "hyperloglog-rs") (v "0.1.44") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "18iaa56lchs7hb30j72hwwza844ip0jiwik4k5zabfrzdfm9pndk") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.45 (c (n "hyperloglog-rs") (v "0.1.45") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1jszngzh6w6zncwyn7yk7753108d2vn42v1nvq18lwyf14k67ccm") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.46 (c (n "hyperloglog-rs") (v "0.1.46") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "micromath") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1knwmknaa9sgj52cfb6wha1lrsab3kxv7fxr7k5lfz5lrqb6bqlj") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.47 (c (n "hyperloglog-rs") (v "0.1.47") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "micromath") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "17lsab2kvv62p57624w1637lrz113b7qsm0ra3hgngqh9hnbn168") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.48 (c (n "hyperloglog-rs") (v "0.1.48") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "micromath") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1cdnvl3m4n5cj9kcrlnv62xk1pmwsj436s8rvs087gjxs5b96p23") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.49 (c (n "hyperloglog-rs") (v "0.1.49") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "micromath") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0q07xlrzy8vbckdqx9h1v4mffh8md55j1ql8r18d0wsdch7k17ff") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.50 (c (n "hyperloglog-rs") (v "0.1.50") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "micromath") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0h5shkfbgvx1kk3wc5wbaa6s10ihzjaj16aj30knn1zjlvxjd6hx") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.51 (c (n "hyperloglog-rs") (v "0.1.51") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "highway") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "micromath") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0f9v1inwh58z7vklvqviicbcq9zqnqicx7dz23is615h5hsp2q4f") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.52 (c (n "hyperloglog-rs") (v "0.1.52") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00nhpldh75v7hjgqw6yzfd12isxijphw5993bcn2cvf0vbbsbhnv") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.53 (c (n "hyperloglog-rs") (v "0.1.53") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0w72mlavxf3afbgz0xbnivr4hnn5rs4w7q8nfafnlmjvds4sjib9") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.54 (c (n "hyperloglog-rs") (v "0.1.54") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02piy4jvi2mpx4wgzw7242agnx5nssjfrdcy5giiwlf4kkjyd0v9") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.55 (c (n "hyperloglog-rs") (v "0.1.55") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ixd4w8bsnig5fc7sgyiz3x4m6q2gr6mbksj5bqcpg90rb1k9j9h") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-hyperloglog-rs-0.1.56 (c (n "hyperloglog-rs") (v "0.1.56") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 2)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1l55h94w4zc1glgn2l3xpf5ksz373c234ykdj481zz5l3r3f4dy6") (f (quote (("std") ("default") ("alloc"))))))

