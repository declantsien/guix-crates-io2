(define-module (crates-io hy pe hyperlocal) #:use-module (crates-io))

(define-public crate-hyperlocal-0.1.0 (c (n "hyperlocal") (v "0.1.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "026v83sk7yll5v96b0vhg0jrcgzvmybbpjcqxkqsjrzr1iyb4knc")))

(define-public crate-hyperlocal-0.2.0 (c (n "hyperlocal") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0p6vsin5cmsfd7ad586lbxidg0x3b25sgr2davq0idjzpm8v12ha")))

(define-public crate-hyperlocal-0.3.0 (c (n "hyperlocal") (v "0.3.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1dfyh23m4b929q83a6x9r1m47vwa9h3zz11zl8j51hlk8sbdjij5") (f (quote (("external_unix_socket" "unix_socket") ("default" "external_unix_socket"))))))

(define-public crate-hyperlocal-0.4.0 (c (n "hyperlocal") (v "0.4.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.1") (d #t) (k 0)))) (h "09z1z4pmmzhlrz38398iyhzfmrzbrc0y93mjbd3nhpbkyayjxypf")))

(define-public crate-hyperlocal-0.4.1 (c (n "hyperlocal") (v "0.4.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.1") (d #t) (k 0)))) (h "1ija3bhqnbilc7va9jpipd2ksyplz8n8r4lhi7j1xz9hn81zjnja")))

(define-public crate-hyperlocal-0.5.0 (c (n "hyperlocal") (v "0.5.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.2") (d #t) (k 0)))) (h "08sw8gkxf7g6glc2al48l3b8hpvzvfh6bn2gcycwi21riyzmbm9a")))

(define-public crate-hyperlocal-0.6.0 (c (n "hyperlocal") (v "0.6.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.2") (d #t) (k 0)))) (h "1yy4zgd952777xbp0x1jdlxf5ry06waf2lpl2vpwc8w6cpaxcqyh")))

(define-public crate-hyperlocal-0.7.0 (c (n "hyperlocal") (v "0.7.0") (d (list (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.13.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("uds" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("uds" "stream" "macros"))) (d #t) (k 2)))) (h "16qs2ksmv9r7k06fkvxrfwgk7sbaxd9acq4zws9iny60j6ksb4pi") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-hyperlocal-0.8.0 (c (n "hyperlocal") (v "0.8.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server" "client" "http1" "runtime"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread" "net" "macros" "io-std" "io-util"))) (d #t) (k 2)))) (h "136978rsp0wr6x28cxivxhbq2np66l4jrq3n9xwckrrd5dxxzbqg") (f (quote (("server") ("default" "client" "server") ("client"))))))

