(define-module (crates-io hy pe hyper-thread-random) #:use-module (crates-io))

(define-public crate-hyper-thread-random-0.4.0 (c (n "hyper-thread-random") (v "0.4.0") (d (list (d (n "rand") (r "^0.4") (d #t) (t "cfg(not(all(target_feature = \"rdrnd\", any(target_arch = \"x86\", target_arch = \"x86_64\"))))") (k 0)))) (h "1aa4740h8jc6hycfyv0pfz4s3z2gr47f0c9j9x9f3hiqkpfi04nb")))

(define-public crate-hyper-thread-random-0.4.2 (c (n "hyper-thread-random") (v "0.4.2") (d (list (d (n "rand") (r "^0.4") (d #t) (t "cfg(not(all(any(target_arch = \"x86\", target_arch = \"x86_64\"), target_feature = \"rdrnd\")))") (k 0)))) (h "0gk5a5v56p9cznpy7rzv3zbkl36z0ybzqy9b3v3v9rlp7f7w4lix")))

