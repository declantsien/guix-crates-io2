(define-module (crates-io hy pe hyper-function-core) #:use-module (crates-io))

(define-public crate-hyper-function-core-0.1.0 (c (n "hyper-function-core") (v "0.1.0") (h "0h3kw2rrkms70dv2ksb0chacn8y1dqqbm8l3y4g43fwzy292500r")))

(define-public crate-hyper-function-core-0.1.1 (c (n "hyper-function-core") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tungstenite") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rusty_ulid") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.16") (f (quote ("connect" "__rustls-tls"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0x8nbs0wgrifhhl64mh6gs6r3gcirwdd4n9q6ngj44kh66fldp9h")))

