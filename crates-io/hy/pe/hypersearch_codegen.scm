(define-module (crates-io hy pe hypersearch_codegen) #:use-module (crates-io))

(define-public crate-hypersearch_codegen-0.1.0 (c (n "hypersearch_codegen") (v "0.1.0") (h "119lvvj4g4xbr77hnqm2wi369ynqrr3gbvzvy4f3llc9570ky0yq")))

(define-public crate-hypersearch_codegen-0.2.0 (c (n "hypersearch_codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "0svlkij1n5z74frdy9z419nf8jvichdkkj532ab8m08x4v50x3af")))

(define-public crate-hypersearch_codegen-0.2.1 (c (n "hypersearch_codegen") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "1dhnnipbqg1h4pnpwx0bq2j7dg85dsd62d6qch7dx5p6hzzw2g2h")))

