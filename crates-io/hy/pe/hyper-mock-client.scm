(define-module (crates-io hy pe hyper-mock-client) #:use-module (crates-io))

(define-public crate-hyper-mock-client-0.1.0 (c (n "hyper-mock-client") (v "0.1.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "11jnnkn88bd8v34mkjx5camaa1jlrh7ifjm81p6lyk60pq1hgbbc")))

(define-public crate-hyper-mock-client-0.1.1 (c (n "hyper-mock-client") (v "0.1.1") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "16rl33yh2105jbhvi4msqnhcd20kgm0m625civ7kpp0vrgdnwhyf")))

(define-public crate-hyper-mock-client-0.1.2 (c (n "hyper-mock-client") (v "0.1.2") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "0k57xx3x8i8p815d45ksdhnin9g4r857iphf85wpbb8s3nd5iksc")))

(define-public crate-hyper-mock-client-0.1.3 (c (n "hyper-mock-client") (v "0.1.3") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "1vpp93dpskh3k5dzix6jnxvbpqn20ddcq119851n1qmrwc0wy7mi")))

