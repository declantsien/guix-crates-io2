(define-module (crates-io hy pe hyper_insecure_https_connector) #:use-module (crates-io))

(define-public crate-hyper_insecure_https_connector-0.1.0 (c (n "hyper_insecure_https_connector") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (d #t) (k 0)))) (h "024sdw661kxn0wa0b5jl8s5d5i4vdcdkvcdynyhpnrrhkdwk9gm2")))

