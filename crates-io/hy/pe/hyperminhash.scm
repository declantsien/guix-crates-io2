(define-module (crates-io hy pe hyperminhash) #:use-module (crates-io))

(define-public crate-hyperminhash-0.1.0 (c (n "hyperminhash") (v "0.1.0") (d (list (d (n "metrohash") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "16j8psq2j0hh5haz686qvvis53whshi2wbmffxxj1hnh160z6938")))

(define-public crate-hyperminhash-0.1.1 (c (n "hyperminhash") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "metrohash") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0blqjh5rb7ih2i1wqym98jnblpfgs1zs0gl94hj8s6fxyd2ljyp6") (f (quote (("serialize" "byteorder") ("default"))))))

(define-public crate-hyperminhash-0.1.2 (c (n "hyperminhash") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "18w899q05bl08g5rx6q00j37m3zdbslv1gg0ll8ay1lj9rjalvp4") (f (quote (("serialize" "byteorder") ("default"))))))

(define-public crate-hyperminhash-0.1.3 (c (n "hyperminhash") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "0brnhz40qnkl1lw11gwzfalvq2gfpnxx1gbk3jm4vbh156gn04kq") (f (quote (("serialize" "byteorder") ("default"))))))

