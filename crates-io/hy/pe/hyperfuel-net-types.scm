(define-module (crates-io hy pe hyperfuel-net-types) #:use-module (crates-io))

(define-public crate-hyperfuel-net-types-0.0.3 (c (n "hyperfuel-net-types") (v "0.0.3") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)) (d (n "hyperfuel-format") (r "^0.0.4") (d #t) (k 0) (p "hyperfuel-format")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gwz2y000mihr3g9i9v8w16k1i669zg3p0xjd5y35d0dyrwarxys")))

(define-public crate-hyperfuel-net-types-1.0.0 (c (n "hyperfuel-net-types") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)) (d (n "hyperfuel-format") (r "^1.0.0") (d #t) (k 0) (p "hyperfuel-format")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hcibvfzl10drrd3nrh2a5yfs6jr40msxsm6nj7xl5brnszqicnm")))

(define-public crate-hyperfuel-net-types-2.0.0 (c (n "hyperfuel-net-types") (v "2.0.0") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)) (d (n "hyperfuel-format") (r "^2.0.0") (d #t) (k 0) (p "hyperfuel-format")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nqyzqbjqbff8qj5awx4pfkk2ghzqr536bqs7yhnycddil25qvc8")))

