(define-module (crates-io hy pe hyperloglogplus) #:use-module (crates-io))

(define-public crate-hyperloglogplus-0.1.0 (c (n "hyperloglogplus") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0m7s6lrdl5zz2gg7w8mi22g82apq173fphhhdqnchfrna587infa") (f (quote (("bench-units"))))))

(define-public crate-hyperloglogplus-0.2.0 (c (n "hyperloglogplus") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0f3s0afzmx79q75a68pd88261r3g78v58srw2xj8srjaf7amvv76") (f (quote (("bench-units"))))))

(define-public crate-hyperloglogplus-0.2.1 (c (n "hyperloglogplus") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ii04cvypzmyml1k3nkbdq536mwn5j6k35lwcjpbx6dpc14vh38n") (f (quote (("bench-units"))))))

(define-public crate-hyperloglogplus-0.2.2 (c (n "hyperloglogplus") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lr0jlcip4r22nxzd7ihlmsipyfncrn24qiamipcpp763hwivpbk") (f (quote (("const-loop") ("bench-units"))))))

(define-public crate-hyperloglogplus-0.3.0 (c (n "hyperloglogplus") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3.3") (d #t) (k 2)))) (h "1qaihcn2dy4iwgmy0qqjrvkhf957fs7546a5pvd6wk51zwwjcai6") (f (quote (("std" "serde/std" "serde/derive") ("default" "std") ("const-loop") ("bench-units") ("alloc" "serde/alloc"))))))

(define-public crate-hyperloglogplus-0.4.0 (c (n "hyperloglogplus") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3.3") (d #t) (k 2)))) (h "0v5ip2ib8s477h8rvmfpfb8pi2161nrp1690xg6n2smjl9apa1dh") (f (quote (("std" "serde/std" "serde/derive") ("default" "std") ("const-loop") ("bench-units") ("alloc" "serde/alloc"))))))

(define-public crate-hyperloglogplus-0.4.1 (c (n "hyperloglogplus") (v "0.4.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3.3") (d #t) (k 2)))) (h "1qzvq6b4c0n313dvjd58c81rrsnm6inxgzbm0kjk7b6wjkgyn7b2") (f (quote (("std" "serde/std" "serde/derive") ("default" "std") ("const-loop") ("bench-units") ("alloc" "serde/alloc"))))))

