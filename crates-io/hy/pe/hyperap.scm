(define-module (crates-io hy pe hyperap) #:use-module (crates-io))

(define-public crate-hyperap-0.1.0 (c (n "hyperap") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "open") (r "^1.2.1") (d #t) (k 0)))) (h "1alw0aq9r26jbrmx0n41yflcvvxr788wgvhmg2j0v58y2x8niyzw")))

(define-public crate-hyperap-0.2.0 (c (n "hyperap") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.13") (d #t) (k 0)) (d (n "open") (r "^1.2.1") (d #t) (k 0)))) (h "1gym5i5hwad2hbaplvixwlz5fr8vizy1l4ir113s2blylw3bwyrv")))

(define-public crate-hyperap-0.3.0 (c (n "hyperap") (v "0.3.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.13") (d #t) (k 0)) (d (n "open") (r "^1.2.1") (d #t) (k 0)))) (h "1q7gsp02qm1yqr8kndd66mw8wngqankgdzq187b9jybx3i8dz9gq")))

(define-public crate-hyperap-0.4.0 (c (n "hyperap") (v "0.4.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.13") (d #t) (k 0)) (d (n "open") (r "^1.2.1") (d #t) (k 0)))) (h "0x23xqdksgcy68h6s1qydr6mwrpz3br4qbjqpydsdbi7q5mhwamp")))

