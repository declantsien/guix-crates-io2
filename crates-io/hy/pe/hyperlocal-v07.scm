(define-module (crates-io hy pe hyperlocal-v07) #:use-module (crates-io))

(define-public crate-hyperlocal-v07-0.7.0 (c (n "hyperlocal-v07") (v "0.7.0") (d (list (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.13.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("uds" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("uds" "stream" "macros"))) (d #t) (k 2)))) (h "105xxckjdmxcqazd3kfvv5i6wnisf3nzrnfq855pph8smrsyv5fb") (f (quote (("server") ("default" "client" "server") ("client")))) (y #t)))

