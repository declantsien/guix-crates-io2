(define-module (crates-io hy pe hyperhasher) #:use-module (crates-io))

(define-public crate-hyperhasher-0.1.0 (c (n "hyperhasher") (v "0.1.0") (d (list (d (n "blake2") (r "^0.10.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1qmlbazsammjh3na04rky07xm89yc1yy1s9wqiq93ga5kqiv7c3y")))

(define-public crate-hyperhasher-0.1.1 (c (n "hyperhasher") (v "0.1.1") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)))) (h "0h5p9f3p1s783m6rc2cqxr05gyyispvlzdhz2q2c4cb6d5pxq66y")))

(define-public crate-hyperhasher-0.1.2 (c (n "hyperhasher") (v "0.1.2") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)))) (h "013lw4w2n5kn6wczflrb69r3b42ng1vnvlgsxqv1cj226gq3mi6l")))

