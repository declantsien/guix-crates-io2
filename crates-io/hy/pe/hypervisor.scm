(define-module (crates-io hy pe hypervisor) #:use-module (crates-io))

(define-public crate-hypervisor-0.0.1 (c (n "hypervisor") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "0b9hp6vdsgzx23h0f8imvw9h5p9gqs00c7xg7f6s7j25xhszvxns")))

(define-public crate-hypervisor-0.0.2 (c (n "hypervisor") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "1i64wqhnw1ydap4j75mv2ffczv1xmr80ql8vmr8jdkgjcxlr6v37")))

(define-public crate-hypervisor-0.0.3 (c (n "hypervisor") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "0pxhy0g7hn2zjd4ivkpllcchv4ncv36rd7hvkn3yrmn4b9cmkv1j")))

(define-public crate-hypervisor-0.0.4 (c (n "hypervisor") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "19l23y4nxqr6lg18j73xr4nla9qd5kz82zdf1vghr4gzk8kly1w8") (y #t)))

(define-public crate-hypervisor-0.0.5 (c (n "hypervisor") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "1a8b2hyzchv1kh58pfq83b8lbaazlf3gs8xkgxmm42g7x1j1lqz1")))

(define-public crate-hypervisor-0.0.6 (c (n "hypervisor") (v "0.0.6") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "0jms5z7jmplwin5sw563wgigv5h7dy69dvgyhqy6l90rp9nwadz1")))

(define-public crate-hypervisor-0.0.8 (c (n "hypervisor") (v "0.0.8") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "1lbsgw0jfr1n4ag28jqwp986xljg7451zwwadjf6kjnr5ngxwyl2")))

