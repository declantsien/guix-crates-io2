(define-module (crates-io hy pe hyper_sardine) #:use-module (crates-io))

(define-public crate-hyper_sardine-0.1.0 (c (n "hyper_sardine") (v "0.1.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "1n233nv1v25690jb3x7xwpf3py3a31qcv67n1ijpdi5x9fqa2x5r")))

(define-public crate-hyper_sardine-0.2.0 (c (n "hyper_sardine") (v "0.2.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "hyperx") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "03j4r7pb03i6hj9qnb0kvxjn9s8fpa6pndm8xwssxqk28pyjyhjw")))

(define-public crate-hyper_sardine-0.2.1 (c (n "hyper_sardine") (v "0.2.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "hyperx") (r "~0.13.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hxijlrv66iv9ba06z6hvfxqzcdcn815zd4kypdkw24h1vj28b3x")))

(define-public crate-hyper_sardine-0.2.2 (c (n "hyper_sardine") (v "0.2.2") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "hyperx") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0l2h7wmydsskdpjv9yqs51v2j56nfrgwmrszc9425y679v4i08dl")))

(define-public crate-hyper_sardine-0.2.3 (c (n "hyper_sardine") (v "0.2.3") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "hyperx") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0k7c0z8c0na2brzq1bnia4lq68cga5lsf79p9mw2dclf05b7i352")))

