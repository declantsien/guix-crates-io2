(define-module (crates-io hy pe hyperdual) #:use-module (crates-io))

(define-public crate-hyperdual-0.3.0 (c (n "hyperdual") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0wvvsxy8q47di8alvapcsg0i834b5f6c2iya68mm6sxp9v6avlas")))

(define-public crate-hyperdual-0.3.1 (c (n "hyperdual") (v "0.3.1") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1p1i8lrz2m4fzfr5accv7i3qng7ji1wyjad2k4mgdm2hn2k1mk3p")))

(define-public crate-hyperdual-0.3.2 (c (n "hyperdual") (v "0.3.2") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1sdyhipn37hrc190cb4ylafk1lr7w6wwqw9kj8z0rb91d2kly50j")))

(define-public crate-hyperdual-0.3.3 (c (n "hyperdual") (v "0.3.3") (d (list (d (n "nalgebra") (r "^0.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1m7ci363f5i26gi37g93n710l1qkpdl86skqyac7xsmffr93fhvn")))

(define-public crate-hyperdual-0.3.4 (c (n "hyperdual") (v "0.3.4") (d (list (d (n "nalgebra") (r "^0.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0qv8j1syykqls828pkgbhjdz9sw5n8g9i2h8lpnjxhjcmavd16br")))

(define-public crate-hyperdual-0.3.5 (c (n "hyperdual") (v "0.3.5") (d (list (d (n "nalgebra") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "15q9na3s6aqgd2qqnn4a46nxphsragimk8q3mgy9835mg5zv35b4")))

(define-public crate-hyperdual-0.3.6 (c (n "hyperdual") (v "0.3.6") (d (list (d (n "nalgebra") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1w9szmiqljapig4x18b814k1803irwv8p89xrf5bi2qdfn9hfmxi")))

(define-public crate-hyperdual-0.3.7 (c (n "hyperdual") (v "0.3.7") (d (list (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07s3mfbiivyz8f2wzd94c1ia973w9wpf0kad77m0lpw52dvm9pzl")))

(define-public crate-hyperdual-0.4.0 (c (n "hyperdual") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.27") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vdsi2giad49xmi5rjd453pyzhgp91pn2c20jg7nyczglnzyhsnd")))

(define-public crate-hyperdual-0.4.1 (c (n "hyperdual") (v "0.4.1") (d (list (d (n "nalgebra") (r "^0.27") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vgj9w2i6yff0ljjacj9vfm7mxg3n7y06y87q9h7bc396wgjiwlh")))

(define-public crate-hyperdual-0.4.2 (c (n "hyperdual") (v "0.4.2") (d (list (d (n "nalgebra") (r "^0.27") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1cganxni6a8mschvw4mqaxqf3ixjchkjmma9c9y01zhg86ccbazm")))

(define-public crate-hyperdual-0.5.0 (c (n "hyperdual") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "09xpc1c99319f1ywd931c6xpcp3a22pkdfxrmhvcglsy1gfzvi38")))

(define-public crate-hyperdual-0.5.1 (c (n "hyperdual") (v "0.5.1") (d (list (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0xh35v3dhm121kwm6r0dq6wrs2dvk4pk4rwj8azxvi19kyfazrfj")))

(define-public crate-hyperdual-0.5.2 (c (n "hyperdual") (v "0.5.2") (d (list (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "11x0i5jzrnz36f3xqqqllksnmbbzyzcp9xhsqvayq382w1f08d1j")))

(define-public crate-hyperdual-1.0.0 (c (n "hyperdual") (v "1.0.0") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0w9ymsf598vnwl9bwbjd4m0ikr7wghpm6fbgwbbw3bgrnbwmv39d")))

(define-public crate-hyperdual-1.1.0 (c (n "hyperdual") (v "1.1.0") (d (list (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1sabg7ks8kcq7b7l0ix33ybrh2gibl0wbr6fm8kkwnwyfhihc5nz")))

(define-public crate-hyperdual-1.2.0 (c (n "hyperdual") (v "1.2.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "005paffwh782420qf054h6k31k64mjc7imxxhh134lj9qz88pj6y")))

