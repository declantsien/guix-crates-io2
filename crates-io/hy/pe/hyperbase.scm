(define-module (crates-io hy pe hyperbase) #:use-module (crates-io))

(define-public crate-hyperbase-0.1.1 (c (n "hyperbase") (v "0.1.1") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "equiv") (r "^0.1.1") (d #t) (k 0)) (d (n "graph_simple") (r "^0.1.1") (d #t) (k 0)) (d (n "kmer_lookup") (r "^0.1.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0z2k0hha2qpmdzaax4lmlnf8b769fqgqw8x7qg65jncvv0ck9km5")))

(define-public crate-hyperbase-0.1.2 (c (n "hyperbase") (v "0.1.2") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "equiv") (r "^0.1.2") (d #t) (k 0)) (d (n "graph_simple") (r "^0.1.1") (d #t) (k 0)) (d (n "kmer_lookup") (r "^0.1.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1wmwa91111kn0pb61ciyvjzvbl1xdkyf96b3vqclm55jm742ipap")))

(define-public crate-hyperbase-0.1.3 (c (n "hyperbase") (v "0.1.3") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "equiv") (r "^0.1.2") (d #t) (k 0)) (d (n "graph_simple") (r "^0.1.1") (d #t) (k 0)) (d (n "kmer_lookup") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1bzkr5k85rjka5y2vqs1dyamx77ppm40y396lsz337kahs7rmhf4")))

(define-public crate-hyperbase-0.1.4 (c (n "hyperbase") (v "0.1.4") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "equiv") (r "^0.1.2") (d #t) (k 0)) (d (n "graph_simple") (r "^0.1.2") (d #t) (k 0)) (d (n "kmer_lookup") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1fr6jrhgya05a2dw64x528kxbmr2dhi4gr4mpkcff5f800sgqscc")))

(define-public crate-hyperbase-0.1.5 (c (n "hyperbase") (v "0.1.5") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "equiv") (r "^0.1") (d #t) (k 0)) (d (n "graph_simple") (r "^0.1") (d #t) (k 0)) (d (n "kmer_lookup") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "09iby83gyb0b09wdms73xbdcgaah5gmv5rdgaan491w8a7k627vs")))

(define-public crate-hyperbase-0.1.6 (c (n "hyperbase") (v "0.1.6") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "equiv") (r "^0.1") (d #t) (k 0)) (d (n "graph_simple") (r "^0.1") (d #t) (k 0)) (d (n "kmer_lookup") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "0yqngwmfgb55415pzsaiz160vgk3jxrjpxr9pb7a687i0lf1zvcg")))

(define-public crate-hyperbase-0.1.7 (c (n "hyperbase") (v "0.1.7") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "equiv") (r "^0.1") (d #t) (k 0)) (d (n "graph_simple") (r "^0.1") (d #t) (k 0)) (d (n "kmer_lookup") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r ">=0.5, <0.7") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1lb08w7qks5pm6fzdh5c4i2wihqvskjf1wdf6zdid11p4cal4yvz")))

(define-public crate-hyperbase-0.1.8 (c (n "hyperbase") (v "0.1.8") (d (list (d (n "debruijn") (r "^0.3") (d #t) (k 0)) (d (n "equiv") (r "^0.1") (d #t) (k 0)) (d (n "graph_simple") (r "^0.1") (d #t) (k 0)) (d (n "kmer_lookup") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r ">=0.5, <0.7") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "0g9xa0gj9zkl6jzan632lqs2m2fz92ivzhpwr2hqlj9hnkmljh9f")))

