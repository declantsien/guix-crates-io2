(define-module (crates-io hy pe hypertask_config_file_opener) #:use-module (crates-io))

(define-public crate-hypertask_config_file_opener-0.2.0 (c (n "hypertask_config_file_opener") (v "0.2.0") (d (list (d (n "hypertask_engine") (r "^0.2.0") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0sr0h98x4pdfwgs0xlqy7ws0pyhali13rq4z80g2gi62r58yf71g")))

