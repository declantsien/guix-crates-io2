(define-module (crates-io hy po hypox) #:use-module (crates-io))

(define-public crate-hypox-0.1.0 (c (n "hypox") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1xhpqrgh27lzsyliqga1bmprxv9jhls9l9riwpcl6r5h0ih2rds9")))

