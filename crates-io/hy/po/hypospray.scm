(define-module (crates-io hy po hypospray) #:use-module (crates-io))

(define-public crate-hypospray-0.1.0 (c (n "hypospray") (v "0.1.0") (d (list (d (n "hypospray_extensions") (r "^0.1") (d #t) (k 2)))) (h "17bl7chphr3fnm07p8j38hrjj6zgdz828vlkjf959z45xsp7gyx9")))

(define-public crate-hypospray-0.1.1 (c (n "hypospray") (v "0.1.1") (d (list (d (n "hypospray_extensions") (r "^0.1") (d #t) (k 2)))) (h "0p5lby7cq2j5dnh9yi39q2rcxl6nhzsfggbcmrxjpr7laq7yssg3")))

(define-public crate-hypospray-0.1.2 (c (n "hypospray") (v "0.1.2") (d (list (d (n "hypospray_extensions") (r "^0.1") (d #t) (k 2)))) (h "02wcg7c64b4z36lvliv9mz2101bm68x1lc849w0dlna5qjdxlgzp")))

