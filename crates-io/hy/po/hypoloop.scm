(define-module (crates-io hy po hypoloop) #:use-module (crates-io))

(define-public crate-hypoloop-0.1.0 (c (n "hypoloop") (v "0.1.0") (h "1amnrn5hfzzkvwrm1r2g4z9fszbzvxrmx85dbjdxhdhn2yi8hpid")))

(define-public crate-hypoloop-0.1.1 (c (n "hypoloop") (v "0.1.1") (h "0r8ibwbm6mf9wq39h2plrm4xiyirlksih7jdxaddxzykh3ij5b1j")))

(define-public crate-hypoloop-0.1.2 (c (n "hypoloop") (v "0.1.2") (h "0g115565gxfbybxdqjmjxhyyir4vf9cdg6dc74hfvfhi5h5hx3hj")))

(define-public crate-hypoloop-0.1.3 (c (n "hypoloop") (v "0.1.3") (h "132vzs9098r4814cy273zzn34lwymll2jn67n8821l5pbgkl9127")))

(define-public crate-hypoloop-0.1.4 (c (n "hypoloop") (v "0.1.4") (h "1kr9ikd1a8rk1zjsnb13jk0pylk1pnl8w2546srcv638nsl62cmg")))

(define-public crate-hypoloop-0.1.5 (c (n "hypoloop") (v "0.1.5") (h "09lj3r64sqrh060kq6jmhck9hkmw8wnj4g61lrz5fn7a47cc93sf")))

(define-public crate-hypoloop-0.1.6 (c (n "hypoloop") (v "0.1.6") (d (list (d (n "winit") (r "^0.25.0") (d #t) (k 2)))) (h "13ld42dqhm3yl7mjbw1wzy14c0hdb47kbmbr5dm25mcjmr9ivj2n")))

(define-public crate-hypoloop-0.1.7 (c (n "hypoloop") (v "0.1.7") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "pixels") (r "^0.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "winit") (r "^0.25") (d #t) (k 2)) (d (n "winit_input_helper") (r "^0.10") (d #t) (k 2)))) (h "15aljjlz6il6nsmcj8prnpnrjzv4gs8an0zggk84i7i8xgfv14bb")))

