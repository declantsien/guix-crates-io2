(define-module (crates-io hy po hypofetch) #:use-module (crates-io))

(define-public crate-hypofetch-0.1.0 (c (n "hypofetch") (v "0.1.0") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "uptime_lib") (r "^0.2.2") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "1d8ilq126f6sj8hym7gdak81kfkw12hz4na0j9b3g1zlw2fpnfpr")))

(define-public crate-hypofetch-0.2.0 (c (n "hypofetch") (v "0.2.0") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "uptime_lib") (r "^0.2.2") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "0hyf2gn6lkidy0m2rri48474g7zhc0q57q12wqxbd4gp17vrxxfp")))

(define-public crate-hypofetch-0.2.1 (c (n "hypofetch") (v "0.2.1") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "uptime_lib") (r "^0.2.2") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "1fzawrvgw6pwfnx8l56dqw387pr7h7dscxbh0a9n2p579dm8zcf9")))

(define-public crate-hypofetch-0.2.2 (c (n "hypofetch") (v "0.2.2") (d (list (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "1kf7kd0hxl1f0bn9h46lsr3hnj4vwfkl2w317xhb7brb5m0qagmb")))

