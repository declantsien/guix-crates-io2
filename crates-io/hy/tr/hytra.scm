(define-module (crates-io hy tr hytra) #:use-module (crates-io))

(define-public crate-hytra-0.1.0 (c (n "hytra") (v "0.1.0") (d (list (d (n "atomic") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "087yns2vnc1mn2jwqmrld3rv1a3q7295c26gipjfc5vymy6i07pp")))

(define-public crate-hytra-0.1.1 (c (n "hytra") (v "0.1.1") (d (list (d (n "atomic") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "0jv5r8zqfll99xqgkxk6x5dky4m45lrakhilin0fw35m8w90f2xc")))

(define-public crate-hytra-0.1.2 (c (n "hytra") (v "0.1.2") (d (list (d (n "atomic") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "094108s21fysjbj8486fk9qvxhw9dfrsz9blcd8058i7glxf8zhx")))

