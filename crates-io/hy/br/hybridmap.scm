(define-module (crates-io hy br hybridmap) #:use-module (crates-io))

(define-public crate-hybridmap-0.1.0 (c (n "hybridmap") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "smallvec") (r "^2.0.0-alpha.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 2)) (d (n "uuid") (r "^1.7") (d #t) (k 2)))) (h "09r2sm8hvc46z7zgmbbrm2k1v6688f6kb8h65km3yz31ayxki4fm")))

(define-public crate-hybridmap-0.1.1 (c (n "hybridmap") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "smallvec") (r "^2.0.0-alpha.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 2)) (d (n "uuid") (r "^1.7") (d #t) (k 2)))) (h "012048askpr4pgywwk70aiccmv8zc6hl6bg02dii684wk56lzfjz")))

