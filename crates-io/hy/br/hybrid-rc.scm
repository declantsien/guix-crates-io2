(define-module (crates-io hy br hybrid-rc) #:use-module (crates-io))

(define-public crate-hybrid-rc-0.1.0 (c (n "hybrid-rc") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "11hhvj9gqlzy7knrfvkh48rj9wp13x6c6r7zrni5r49z8mgh4h8l")))

(define-public crate-hybrid-rc-0.1.1 (c (n "hybrid-rc") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "11w5k9gqp109r81k8kyw8ki0vdkl80mcfnvcz662fcswplc2chai")))

(define-public crate-hybrid-rc-0.2.0 (c (n "hybrid-rc") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1l37a3di71qqgx2avky8yyz9zga3cs90knspla6lqp5p53pnw7km") (f (quote (("std") ("default" "std"))))))

(define-public crate-hybrid-rc-0.3.0 (c (n "hybrid-rc") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1pgjn6qdx4vigk0n08pvsl8kh191ydbd2llpjh8vhmc5ks2ykx4z") (f (quote (("std") ("default" "std"))))))

(define-public crate-hybrid-rc-0.4.0 (c (n "hybrid-rc") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0gkk8jdc8d6pkhbangx9cwkmgjscsimbzaaliykxqvq4frz5wj3y") (f (quote (("std") ("default" "std"))))))

(define-public crate-hybrid-rc-0.5.0 (c (n "hybrid-rc") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0yw4lpj75ndfg4j6w8z5syf4ygjib1jnjag0vi7550p092ifbvan") (f (quote (("std") ("default" "std"))))))

(define-public crate-hybrid-rc-0.6.0 (c (n "hybrid-rc") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0vr6sqw1012ap1x9avnlm36nc4jzyc75ps5bmcj12y7ikvg27hvy") (f (quote (("std") ("default" "std"))))))

