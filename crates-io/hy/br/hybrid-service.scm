(define-module (crates-io hy br hybrid-service) #:use-module (crates-io))

(define-public crate-hybrid-service-0.1.0 (c (n "hybrid-service") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "0z4g68ncm8lnzq9vy1qx7jdc400sl6bm76warg4k3l7l4a851fif")))

(define-public crate-hybrid-service-0.1.1 (c (n "hybrid-service") (v "0.1.1") (d (list (d (n "axum") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "19m1vr68z0jjv8y1fa8ljihgcd03s1mwwj3524z4g6hjz0lps0xl") (s 2) (e (quote (("axum" "dep:axum"))))))

