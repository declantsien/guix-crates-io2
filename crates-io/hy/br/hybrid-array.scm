(define-module (crates-io hy br hybrid-array) #:use-module (crates-io))

(define-public crate-hybrid-array-0.1.0 (c (n "hybrid-array") (v "0.1.0") (d (list (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "0j5kw7w268c5019db8vd7w2ylh9cab4gyhhkpj0hqk62la9p5nmg") (r "1.56")))

(define-public crate-hybrid-array-0.2.0-pre.0 (c (n "hybrid-array") (v "0.2.0-pre.0") (d (list (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "0pb9bxl4x0k2d0xhqvl6pznd7cd0qwihjny27q5kkn9gys0cvbs5") (r "1.65")))

(define-public crate-hybrid-array-0.2.0-pre.1 (c (n "hybrid-array") (v "0.2.0-pre.1") (d (list (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "1z67sshq86v6fak1pw3dhbslb9wb1jk748hgzkxdwram0n4kfizv") (r "1.65")))

(define-public crate-hybrid-array-0.2.0-pre.2 (c (n "hybrid-array") (v "0.2.0-pre.2") (d (list (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "0xs2x4r6w323418apq2zmhm76mq6v9fdavbzc0za9h6pi261w7z1") (r "1.65")))

(define-public crate-hybrid-array-0.2.0-pre.3 (c (n "hybrid-array") (v "0.2.0-pre.3") (d (list (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "0afyh7mxymxd43qh6zbm4j7n5s8vfdzwyqcjmiym8qzq2d9pb3g2") (r "1.65")))

(define-public crate-hybrid-array-0.2.0-pre.4 (c (n "hybrid-array") (v "0.2.0-pre.4") (d (list (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "1xbfsina8rvcky7lnsrwfmqlvj8d874cdag27y7jixbi7q6h25lv") (r "1.65")))

(define-public crate-hybrid-array-0.2.0-pre.5 (c (n "hybrid-array") (v "0.2.0-pre.5") (d (list (d (n "typenum") (r "^1.17") (d #t) (k 0)))) (h "0g01y6wkm3j3abhpjpa210yl76arpnyb1x1qmiv9l5balsv1a5j3") (r "1.65")))

(define-public crate-hybrid-array-0.2.0-pre.6 (c (n "hybrid-array") (v "0.2.0-pre.6") (d (list (d (n "typenum") (r "^1.17") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "1qn4693bbm2s1jwbfkpk812mqgbx8fq5dxajhkqynlrwvmd8s53z") (r "1.65")))

(define-public crate-hybrid-array-0.2.0-pre.7 (c (n "hybrid-array") (v "0.2.0-pre.7") (d (list (d (n "typenum") (r "^1.17") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "01ddzi0a7i2kdkw3008a0h4xjn1hkwypkmgha4zw9pnlyfx16df2") (r "1.65")))

(define-public crate-hybrid-array-0.2.0-pre.8 (c (n "hybrid-array") (v "0.2.0-pre.8") (d (list (d (n "typenum") (r "^1.17") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0vd4w0d5kx21ssxbxn0vziqvxskaba6k9v89mw69izhq4hjazyr7") (r "1.65")))

(define-public crate-hybrid-array-0.2.0-rc.0 (c (n "hybrid-array") (v "0.2.0-rc.0") (d (list (d (n "typenum") (r "^1.17") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "18cd9chhpqijfa3b8xz1dmcw48azrj2psr5rf20yh24zq9x53idq") (r "1.65")))

(define-public crate-hybrid-array-0.2.0-rc.1 (c (n "hybrid-array") (v "0.2.0-rc.1") (d (list (d (n "typenum") (r "^1.17") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "1awrsi80ri87xzahxs06prl7gq9yhwcsdkdnhag374nrr5lhlw3v") (f (quote (("std")))) (r "1.65")))

(define-public crate-hybrid-array-0.2.0-rc.2 (c (n "hybrid-array") (v "0.2.0-rc.2") (d (list (d (n "typenum") (r "^1.17") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "10hxzxacn7bjhx544ipm1jnpicli8bfdbkc7ysdqip0m99xiwav6") (f (quote (("std")))) (r "1.65")))

(define-public crate-hybrid-array-0.2.0-rc.3 (c (n "hybrid-array") (v "0.2.0-rc.3") (d (list (d (n "typenum") (r "^1.17") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0ivrzabv0inm10a8pcz31bx6n5m3qwjyp7sq0f7k4xpbnlynp1fq") (f (quote (("std")))) (r "1.65")))

(define-public crate-hybrid-array-0.2.0-rc.4 (c (n "hybrid-array") (v "0.2.0-rc.4") (d (list (d (n "typenum") (r "^1.17") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0zmx7d1rwl1nz1z3isj3w796iz1x2547mcb9pacmbpz2mrk3prhq") (f (quote (("std")))) (r "1.65")))

(define-public crate-hybrid-array-0.2.0-rc.5 (c (n "hybrid-array") (v "0.2.0-rc.5") (d (list (d (n "typenum") (r "^1.17") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "1cm2zj33jppb7iw816i55pam7p9fqkxiq75rls3w465k012kbnnw") (f (quote (("std")))) (r "1.65")))

(define-public crate-hybrid-array-0.2.0-rc.6 (c (n "hybrid-array") (v "0.2.0-rc.6") (d (list (d (n "typenum") (r "^1.17") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0qscbzrm639w8gl8qd0gsbmmmy2lw0gsjqb7bcb0lyzkbi1np6av") (f (quote (("std")))) (r "1.65")))

(define-public crate-hybrid-array-0.2.0-rc.7 (c (n "hybrid-array") (v "0.2.0-rc.7") (d (list (d (n "typenum") (r "^1.17") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "1zwwjy1bmcbiy6g5ys3fn69m2j5i949czasa53zzzjyy18d33hl7") (f (quote (("std") ("extra-sizes")))) (r "1.65")))

(define-public crate-hybrid-array-0.2.0-rc.8 (c (n "hybrid-array") (v "0.2.0-rc.8") (d (list (d (n "typenum") (r "^1.17") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0iqhnpr1hysqhzrign7f3i76mghydpj4n1pp9fprw7d4lmfqyrjk") (f (quote (("std") ("extra-sizes")))) (r "1.65")))

