(define-module (crates-io hy pr hyprcursor) #:use-module (crates-io))

(define-public crate-hyprcursor-0.0.1 (c (n "hyprcursor") (v "0.0.1") (d (list (d (n "cairo-rs") (r "^0.19.2") (k 0)) (d (n "libhyprcursor-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1kfwwv4ydn2zvlddw74ialvc7lzdj9m291g1nkvkxxm8l27fgk5f")))

(define-public crate-hyprcursor-0.0.2 (c (n "hyprcursor") (v "0.0.2") (d (list (d (n "cairo-rs") (r "^0.19.2") (k 0)) (d (n "cairo-rs") (r "^0.19.2") (f (quote ("png"))) (d #t) (k 2)) (d (n "libhyprcursor-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1168pxk1lx4g196pcy7x63nig4hg8b0s347da5r14nqanhgvhyzm")))

