(define-module (crates-io hy pr hyprforest-installer) #:use-module (crates-io))

(define-public crate-hyprforest-installer-0.1.0 (c (n "hyprforest-installer") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0cz4932bnhj8z6v4my485yrccvzxjzxr42b5l84d9yq1w4mxmflb")))

(define-public crate-hyprforest-installer-0.1.1 (c (n "hyprforest-installer") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0paw8x5rk4x7cnxlsbawp8yyvqkwijixa653n44n7had1xakh51a")))

(define-public crate-hyprforest-installer-0.1.2 (c (n "hyprforest-installer") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1qchgmdw6crxvxa90hynwpzrdpf84s2v9i7xf9d4awjjg2ji7mxp")))

