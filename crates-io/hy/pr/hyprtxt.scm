(define-module (crates-io hy pr hyprtxt) #:use-module (crates-io))

(define-public crate-hyprtxt-0.1.0 (c (n "hyprtxt") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "113829nhjwsnir26a9n2xv9fr55nn115jm7fva17anr7ikbzvqs9")))

(define-public crate-hyprtxt-0.1.1 (c (n "hyprtxt") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c4d1fj0xhzji8k4231m4cawjn7iwq90ljfc6yzm663bcl3kyab1")))

