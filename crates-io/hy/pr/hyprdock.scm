(define-module (crates-io hy pr hyprdock) #:use-module (crates-io))

(define-public crate-hyprdock-0.1.0 (c (n "hyprdock") (v "0.1.0") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0xvz64sfva68gnvp5apw5vsppibsa548k2hb6ag2h6i8ang0y3xh")))

(define-public crate-hyprdock-0.2.0 (c (n "hyprdock") (v "0.2.0") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1d1qk9vyfwyk093h28hqhkncjddnpmrkqp82ykckr4bcayphl7zw")))

(define-public crate-hyprdock-0.2.1 (c (n "hyprdock") (v "0.2.1") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0wa8bxzjm4yhrhc984xzyj20wcxr1b18kxlrd2c2xik03412ibq0")))

(define-public crate-hyprdock-0.3.0 (c (n "hyprdock") (v "0.3.0") (d (list (d (n "adw") (r "^0.3") (d #t) (k 0) (p "libadwaita")) (d (n "gtk") (r "^0.17") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.6.1") (d #t) (k 0)) (d (n "gtk4") (r "^0.6.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.158") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1kdix0094ldy80i2snpx78m0l24sbp5c8h9lssgbdh7yziwb8fc3")))

(define-public crate-hyprdock-0.3.2 (c (n "hyprdock") (v "0.3.2") (d (list (d (n "adw") (r "^0.3") (d #t) (k 0) (p "libadwaita")) (d (n "gtk") (r "^0.17") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.6.1") (d #t) (k 0)) (d (n "gtk4") (r "^0.6.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.158") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0xgik1i4mnvyggh6khprs5d3icyjk5yq543fysvxnin3bmg442k3")))

(define-public crate-hyprdock-0.3.3 (c (n "hyprdock") (v "0.3.3") (d (list (d (n "adw") (r "^0.3") (d #t) (k 0) (p "libadwaita")) (d (n "gtk") (r "^0.17") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.6.1") (d #t) (k 0)) (d (n "gtk4") (r "^0.6.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.158") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1d0gslv5sc47p8b5k7ia4dnh98kcnv7fb8795dd6q6pmkajm0jby")))

(define-public crate-hyprdock-0.3.4 (c (n "hyprdock") (v "0.3.4") (d (list (d (n "adw") (r "^0.3") (d #t) (k 0) (p "libadwaita")) (d (n "gtk") (r "^0.17") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.6.1") (d #t) (k 0)) (d (n "gtk4") (r "^0.6.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.158") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1x4m872bcnagwz253p1lyqbm7qyrx3d35c1k4cqy4ywp7n65qcmw")))

(define-public crate-hyprdock-0.3.5 (c (n "hyprdock") (v "0.3.5") (d (list (d (n "gtk") (r "^0.17") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.6.1") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.158") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1bicpc25ys7s6d1xkmczy0dl4xb2n8dzvqima95m5sf9yk0b3aaq")))

(define-public crate-hyprdock-0.3.6 (c (n "hyprdock") (v "0.3.6") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "gtk") (r "^0.17.1") (d #t) (k 0) (p "gtk")) (d (n "gtk-layer-shell") (r "^0.6.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "19rdph5b99ayiqqad9awzys05wrb8brjw8kvc8sxqx76ms590qrr")))

(define-public crate-hyprdock-0.3.7 (c (n "hyprdock") (v "0.3.7") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "gtk") (r "^0.17.1") (d #t) (k 0) (p "gtk")) (d (n "gtk-layer-shell") (r "^0.6.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1sx3gxanlb6pgn0vjdyh0dsd9yza4x45y9xmpqk3da2k4845is9k")))

