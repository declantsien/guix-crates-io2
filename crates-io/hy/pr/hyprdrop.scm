(define-module (crates-io hy pr hyprdrop) #:use-module (crates-io))

(define-public crate-hyprdrop-0.2.0 (c (n "hyprdrop") (v "0.2.0") (d (list (d (n "hyprland") (r "^0.3.13") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "time") (r "^0.3.32") (d #t) (k 0)))) (h "18a9xd6qrh7q27d1q85jnzy8yw3l76jcxhh1fchngaiif0xs2yc4")))

