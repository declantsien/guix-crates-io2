(define-module (crates-io hy pr hypr-helper) #:use-module (crates-io))

(define-public crate-hypr-helper-0.0.7 (c (n "hypr-helper") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "194bq3d987zsbn1kgl14xn0pn41zvmfn4jy9flvd8d2ibrb4mpax")))

(define-public crate-hypr-helper-0.1.0 (c (n "hypr-helper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1nc7zi5kfi1jfmh7bh4mpk28378rpkkmhg1r4q4v0nkisjy9y40z")))

(define-public crate-hypr-helper-0.1.1 (c (n "hypr-helper") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0d79yqlfanp1g6d43zqxlfbyqhzfkmz9bp1hgps208wbclpigbs1")))

