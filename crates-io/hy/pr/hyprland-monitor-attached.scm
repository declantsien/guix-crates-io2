(define-module (crates-io hy pr hyprland-monitor-attached) #:use-module (crates-io))

(define-public crate-hyprland-monitor-attached-0.1.0 (c (n "hyprland-monitor-attached") (v "0.1.0") (h "1iggqwg219fkq2ndjmy47cxsmq4fy6nmwaadxhz5ql9xxzknpwc8")))

(define-public crate-hyprland-monitor-attached-0.1.1 (c (n "hyprland-monitor-attached") (v "0.1.1") (h "1hmhhxzgiya42gdg667a5ppl91aykfz13nfs9jjn92fvx6r1fb1k")))

(define-public crate-hyprland-monitor-attached-0.1.2 (c (n "hyprland-monitor-attached") (v "0.1.2") (h "1vpqwqjbz6zz7irysz80m88145wqz9zhp05qa5gh66y2416509vz")))

(define-public crate-hyprland-monitor-attached-0.1.3 (c (n "hyprland-monitor-attached") (v "0.1.3") (h "1h2f8sr3bdb3i7xnp9gcqjwadyndynn9l12wvs5knvv1f644pm8d")))

(define-public crate-hyprland-monitor-attached-0.1.4 (c (n "hyprland-monitor-attached") (v "0.1.4") (h "1s7csn1skaxx4sfjqv92fg54khhxhlz8v2alzbnfl7ix3iwdypnm")))

(define-public crate-hyprland-monitor-attached-0.1.5 (c (n "hyprland-monitor-attached") (v "0.1.5") (h "1863m48kp1l95d2ysh3p26n21di64j4ljbda6qilwdwn08l9dvnq")))

(define-public crate-hyprland-monitor-attached-0.1.6 (c (n "hyprland-monitor-attached") (v "0.1.6") (h "14g14y855kxfn3rhhc5ixhraykxyz22nhsw1694pw0pdkypjpcgl")))

