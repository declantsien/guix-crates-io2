(define-module (crates-io hy pr hyprland-macros) #:use-module (crates-io))

(define-public crate-hyprland-macros-0.3.1 (c (n "hyprland-macros") (v "0.3.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0qmhzxpvjz20fd34vzc2gx6lvg8ip0m18dgps7ljk3n0j4805200")))

(define-public crate-hyprland-macros-0.3.2 (c (n "hyprland-macros") (v "0.3.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0vkawh4zy46vpq2jysfrrzrar597q1p7ndjgcmkh28zcni85brid")))

(define-public crate-hyprland-macros-0.3.3 (c (n "hyprland-macros") (v "0.3.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "012c9djgl8y505rlh8bsq95b3jnwnng3gibjhbmvb7cy7723av3x")))

(define-public crate-hyprland-macros-0.3.4 (c (n "hyprland-macros") (v "0.3.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1sz4yhwi7d1s2wfpk9ql43dgmiq0j3j8skmrijpi55lp5vax6hf9")))

(define-public crate-hyprland-macros-0.4.0-alpha.1 (c (n "hyprland-macros") (v "0.4.0-alpha.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "13n3zgrfz6l6hi0g35zy8qkimray89i95x0qb547xrrc316cxn2x")))

