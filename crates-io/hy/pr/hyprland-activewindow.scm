(define-module (crates-io hy pr hyprland-activewindow) #:use-module (crates-io))

(define-public crate-hyprland-activewindow-0.5.0 (c (n "hyprland-activewindow") (v "0.5.0") (d (list (d (n "hyprland") (r "^0.3.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jp2f72cjgfh4x5k2x38v2q31r310drk514an50sbbhjljqkk3fy") (y #t)))

(define-public crate-hyprland-activewindow-1.0.0 (c (n "hyprland-activewindow") (v "1.0.0") (d (list (d (n "flexi_logger") (r "^0.28") (d #t) (k 0)) (d (n "hyprland") (r "^0.3.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14l3h6404hr34qir4r3p5d2j356vakwpb5f7bw36v1dl8ignmspc") (y #t)))

(define-public crate-hyprland-activewindow-1.0.1 (c (n "hyprland-activewindow") (v "1.0.1") (d (list (d (n "flexi_logger") (r "^0.28") (d #t) (k 0)) (d (n "hyprland") (r "^0.3.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bnkb1diwzfj7i8rdhpav8iqsipvxj878i9cimsm1i0ibpslhpn1") (y #t)))

(define-public crate-hyprland-activewindow-1.0.2 (c (n "hyprland-activewindow") (v "1.0.2") (d (list (d (n "flexi_logger") (r "^0.28") (d #t) (k 0)) (d (n "hyprland") (r "^0.3.13") (f (quote ("silent"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10k4ngg62andrkg02vbq4gzhga4mgydm8lp58aql5fp4hd5680qj")))

(define-public crate-hyprland-activewindow-1.0.3 (c (n "hyprland-activewindow") (v "1.0.3") (d (list (d (n "flexi_logger") (r "^0.28") (d #t) (k 0)) (d (n "hyprland") (r "^0.4.0-alpha.2") (f (quote ("silent"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1abacc3j0jbimcagn18dyc57fxgl62ccsbqjg6mx331i3akmlhqb")))

