(define-module (crates-io hy pr hyprland_workspace_display) #:use-module (crates-io))

(define-public crate-hyprland_workspace_display-0.2.0 (c (n "hyprland_workspace_display") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "1g56baxn9izi1qgbmhzf9fbq9835xk529m6m87wvf3m62vhr8b2n")))

(define-public crate-hyprland_workspace_display-0.2.1 (c (n "hyprland_workspace_display") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "07k8jv2k5ldn59cv38ln8bxwa8viipf7diwvbv1dzryzxgdd4b8r")))

