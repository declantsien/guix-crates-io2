(define-module (crates-io hy pr hyprsession) #:use-module (crates-io))

(define-public crate-hyprsession-0.1.0 (c (n "hyprsession") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hyprland") (r "^0.3.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0cmz1iliq06x36cviri6miqn19zl36jar7xrby7m58hay1774bpj")))

(define-public crate-hyprsession-0.1.1 (c (n "hyprsession") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hyprland") (r "^0.3.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0n038lkgw9qs1lnp6ipwwja55570kk7iisw24wnjk8gs0al8gwvw")))

(define-public crate-hyprsession-0.1.2 (c (n "hyprsession") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hyprland") (r "^0.3.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "13d4zcv6x4bk0j4x9lnb80ywk7xpdjqaxck416x9f8x6pri53akc")))

