(define-module (crates-io hy pr hyprdusk) #:use-module (crates-io))

(define-public crate-hyprdusk-0.0.0 (c (n "hyprdusk") (v "0.0.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "gdk") (r "^0.18.0") (d #t) (k 0)) (d (n "gio") (r "^0.18.3") (d #t) (k 0)) (d (n "glib") (r "^0.18.3") (d #t) (k 0)) (d (n "grass") (r "^0.13.1") (f (quote ("macro"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18.1") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.8.0") (d #t) (k 0)) (d (n "hyprland") (r "^0.3.12") (d #t) (k 0)) (d (n "mpris") (r "^2.0.1") (d #t) (k 0)) (d (n "stray") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10540p9wji6wvkfn5fcwn42l87ln0n3yc469js9b8sq23k94mi2g")))

