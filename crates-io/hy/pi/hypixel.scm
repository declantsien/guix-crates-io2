(define-module (crates-io hy pi hypixel) #:use-module (crates-io))

(define-public crate-hypixel-0.1.0 (c (n "hypixel") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.19.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d6mxjnp2ksffy2q9zf16n6qsbchygix2w5v5y3wl6xmxjhfch4i") (f (quote (("skyblock") ("resources") ("players") ("others") ("default" "players" "resources" "skyblock" "others"))))))

