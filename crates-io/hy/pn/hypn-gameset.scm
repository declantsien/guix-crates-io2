(define-module (crates-io hy pn hypn-gameset) #:use-module (crates-io))

(define-public crate-hypn-gameset-0.1.6 (c (n "hypn-gameset") (v "0.1.6") (d (list (d (n "anchor-lang") (r "^0.18.2") (d #t) (k 0)) (d (n "solrandhypn") (r "^0.1.7") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0vf5b2yy98v8y06ms1f7dyf8l9zd302hryi77wjfx3dh3ys2ylmn") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

