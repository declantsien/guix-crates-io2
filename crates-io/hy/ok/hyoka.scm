(define-module (crates-io hy ok hyoka) #:use-module (crates-io))

(define-public crate-hyoka-0.1.0 (c (n "hyoka") (v "0.1.0") (h "1vnhypv6y5by5nwfv02jlx2vdk8djgr4yi48dzhgr430hxvbm02i")))

(define-public crate-hyoka-0.1.1 (c (n "hyoka") (v "0.1.1") (h "05ranhkk72lcmqjdqifj5wgfdh796frr6m54whaqp1qndnbrh6ab")))

(define-public crate-hyoka-0.1.2 (c (n "hyoka") (v "0.1.2") (h "0y51x31826pwqwq8dn061lcn49hf7war67ybisaqm0a5nf5arwdd")))

(define-public crate-hyoka-0.1.3 (c (n "hyoka") (v "0.1.3") (h "044xpjkv24fk2lmmfd7xhr66dg3f0pq0kvz4923rvvx985yi7vrd")))

(define-public crate-hyoka-0.1.4 (c (n "hyoka") (v "0.1.4") (h "17ddwd48rl2yhbik7gjvvibvywhsjvsh6if7d0zwjmf6pa1qhyk5")))

(define-public crate-hyoka-0.1.5 (c (n "hyoka") (v "0.1.5") (h "0bix8wwal113s43ai14x1d9p86rbhyqhsv4ar42n5g62zllbs9ky")))

