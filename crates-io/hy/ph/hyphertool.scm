(define-module (crates-io hy ph hyphertool) #:use-module (crates-io))

(define-public crate-hyphertool-0.1.0 (c (n "hyphertool") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hypher") (r "^0.1.4") (d #t) (k 0)))) (h "1jbpxa25k2vbwnbmpz6b6h9c7dqb8g9ia8cbvdhii51jxx6jqfzb")))

(define-public crate-hyphertool-0.2.0 (c (n "hyphertool") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hypher") (r "^0.1.4") (d #t) (k 0)))) (h "0jalk2i6mx62lha394w34xgqssy62z0ig6srmgzpsm1d5snaxrss")))

