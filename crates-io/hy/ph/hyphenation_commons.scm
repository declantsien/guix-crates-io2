(define-module (crates-io hy ph hyphenation_commons) #:use-module (crates-io))

(define-public crate-hyphenation_commons-0.6.0 (c (n "hyphenation_commons") (v "0.6.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0xpxzakdbcgj7pnflvb75f831vpd5p6g1vs07b7v9rs7vsjh7zwl") (f (quote (("unstable" "serde_derive") ("default" "serde_codegen"))))))

(define-public crate-hyphenation_commons-0.6.1 (c (n "hyphenation_commons") (v "0.6.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0g5smjsxv9xgdk0s62i566iki60vpxyhxi4kfwdwjifqb2flpqj3") (f (quote (("unstable" "serde_derive") ("default" "serde_codegen"))))))

(define-public crate-hyphenation_commons-0.7.0 (c (n "hyphenation_commons") (v "0.7.0") (d (list (d (n "atlatl") (r "^0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yrhgq3avm4mgbpvmnznsbhwg93wji9pd74m0q6iggq2ikp6j0sf")))

(define-public crate-hyphenation_commons-0.7.1 (c (n "hyphenation_commons") (v "0.7.1") (d (list (d (n "atlatl") (r "^0.1.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pasnbk3rbdgf30jjjh1h24a9pxpdrnn0ihcivmpnzqha6mn2d4y")))

(define-public crate-hyphenation_commons-0.8.0 (c (n "hyphenation_commons") (v "0.8.0") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "089jv1pr718aq0yjfcv6x0zljw9f73jm15khdsydzfln3ci7n4hj")))

(define-public crate-hyphenation_commons-0.8.1 (c (n "hyphenation_commons") (v "0.8.1") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "001vhdnlhb1y4pdil4gprq6qj5gcmmf253mzj808x5cv7zn65085")))

(define-public crate-hyphenation_commons-0.8.3 (c (n "hyphenation_commons") (v "0.8.3") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kamm69ch1sg5zwax0qjckzsqi4ld9r8y1bdrbpqfx2xpdch70nz")))

(define-public crate-hyphenation_commons-0.8.4 (c (n "hyphenation_commons") (v "0.8.4") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gq59h9h8597k04yl53an0j56cvb0in98pxpp27dkiz5mnifgssz")))

