(define-module (crates-io hy dr hydro_template) #:use-module (crates-io))

(define-public crate-hydro_template-0.1.0 (c (n "hydro_template") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.20.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "0y4ijl0hmf08bnbwrb07is97npa4i5lfw8swf39ywacj80129dgy")))

