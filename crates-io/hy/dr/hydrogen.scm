(define-module (crates-io hy dr hydrogen) #:use-module (crates-io))

(define-public crate-hydrogen-0.1.0 (c (n "hydrogen") (v "0.1.0") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "simple-slab") (r "^0.1.0") (d #t) (k 0)) (d (n "threadpool") (r "~1.0.0") (d #t) (k 0)))) (h "187w8hgbmpx635vjlc0yxl3mngfrpjv8mz1kqxk4nwg73mzxla2c")))

(define-public crate-hydrogen-0.1.1 (c (n "hydrogen") (v "0.1.1") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "simple-slab") (r "^0.1.0") (d #t) (k 0)) (d (n "threadpool") (r "~1.0.0") (d #t) (k 0)))) (h "1gzbrdff5zcdbibjnv57q9vya3zz2pd5mg6ll0fiakpnry3cg4vw")))

(define-public crate-hydrogen-0.1.2 (c (n "hydrogen") (v "0.1.2") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "simple-slab") (r "^0.1.0") (d #t) (k 0)) (d (n "threadpool") (r "~1.0.0") (d #t) (k 0)))) (h "16k3yflcgi0dkrj8ak4nqg55rcn5r2a0rbxv59ksix7s2vpn41ni")))

(define-public crate-hydrogen-0.1.3 (c (n "hydrogen") (v "0.1.3") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "simple-slab") (r "^0.2") (d #t) (k 0)) (d (n "threadpool") (r "~1.0") (d #t) (k 0)))) (h "03swad6knhz11kamiljc469z1q6d01yj2c80mfsh0f0h4janm5gp")))

(define-public crate-hydrogen-0.1.4 (c (n "hydrogen") (v "0.1.4") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "simple-slab") (r "^0.2") (d #t) (k 0)) (d (n "threadpool") (r "~1.0") (d #t) (k 0)))) (h "1i3p3panyqnrkjbbg7km4dn8n659f21m183vglb8yqlbgg0dvvri")))

(define-public crate-hydrogen-0.1.5 (c (n "hydrogen") (v "0.1.5") (d (list (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "simple-slab") (r "^0.2") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "057gkh1ps5jx2xaiyyr45xbn9fygfgkl0abmckyabs6cwa94p7yq")))

