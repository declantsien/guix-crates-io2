(define-module (crates-io hy dr hydralite) #:use-module (crates-io))

(define-public crate-hydralite-1.0.0 (c (n "hydralite") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "19sxby0xxlfyqbdyszapzg54in90agbnfchcgw72k88pmblnrayk")))

