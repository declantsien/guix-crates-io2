(define-module (crates-io hy dr hydroflow_internalmacro) #:use-module (crates-io))

(define-public crate-hydroflow_internalmacro-0.0.0 (c (n "hydroflow_internalmacro") (v "0.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "0z5bl6mhsa6dhjlix61g164d8439g1ahagryj7f7980hgzf4iswd")))

(define-public crate-hydroflow_internalmacro-0.1.0 (c (n "hydroflow_internalmacro") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "1y12njhkaabfi2zvmpkcii7izazyxyd3823wzvv1639szl7kaijc")))

