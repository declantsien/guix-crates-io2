(define-module (crates-io hy dr hydrate-codegen) #:use-module (crates-io))

(define-public crate-hydrate-codegen-0.0.1 (c (n "hydrate-codegen") (v "0.0.1") (d (list (d (n "codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "hydrate-data") (r "^0.0.1") (d #t) (k 0)) (d (n "hydrate-pipeline") (r "^0.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1hak71x1wadrlb5svqx15sc5sr1q7gfrd5dvw4cl96rr1q2d8q8h")))

(define-public crate-hydrate-codegen-0.0.2 (c (n "hydrate-codegen") (v "0.0.2") (d (list (d (n "codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "hydrate-data") (r "^0.0.2") (d #t) (k 0)) (d (n "hydrate-pipeline") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0zp7f56g10mn3d932s2dri5m93p8fbi1grsd2iy7m7lsypcvdb51")))

