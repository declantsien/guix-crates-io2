(define-module (crates-io hy dr hydra-macros) #:use-module (crates-io))

(define-public crate-hydra-macros-0.1.1 (c (n "hydra-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v5n1js7cdj993hxksiy1q8nm4bmwx72dx0wnqw3ii91narkkv43")))

(define-public crate-hydra-macros-0.1.2 (c (n "hydra-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03hm3ajbz4q9v2bkfi3jn3chpzfa057y78czap1bw3d974lbwsyi")))

(define-public crate-hydra-macros-0.1.3 (c (n "hydra-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j0l0rnpirfsh32kf1cky8h5i28s5l0s1k30hgav5q59q6sdiaar")))

(define-public crate-hydra-macros-0.1.4 (c (n "hydra-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jwp2kmsgfk1ff4qmx14frkr78c7cgbnsqnhxg1j26yd2lixik13")))

(define-public crate-hydra-macros-0.1.5 (c (n "hydra-macros") (v "0.1.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12lsisqnxaz5s4wl96p5w6r4bg823lkwwzp3d7wmxmkx0rz8b3nb")))

(define-public crate-hydra-macros-0.1.6 (c (n "hydra-macros") (v "0.1.6") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wlplzldc3p8xqabz9v309f2r2lmk9h2ws6fqncajz8smrir12fp")))

(define-public crate-hydra-macros-0.1.7 (c (n "hydra-macros") (v "0.1.7") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h4az5svha51vfv058p4cwxc2a92ncnfwwnbgfi831sif07hjva8")))

(define-public crate-hydra-macros-0.1.8 (c (n "hydra-macros") (v "0.1.8") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vmpmj9mpk4045d06kzh4m99k9siycp29rk81lf2ss1b97h61w3m")))

(define-public crate-hydra-macros-0.1.9 (c (n "hydra-macros") (v "0.1.9") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lby1m28vdz4bl2v6vrvra0y51cyg8p2ww5yxnrx9ny9b0zcmiqr")))

(define-public crate-hydra-macros-0.1.10 (c (n "hydra-macros") (v "0.1.10") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0snsqpizazacv9rjcj8wddifbds7ndrbjjv3r19z3af5sfn22cdm")))

(define-public crate-hydra-macros-0.1.11 (c (n "hydra-macros") (v "0.1.11") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rryxdzkc2mzjw7simj8iskjmk2ivpcdi6zjbiswgqw2ny4cgsgq")))

(define-public crate-hydra-macros-0.1.12 (c (n "hydra-macros") (v "0.1.12") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xcscrr8fvdjcwcj5nb6cx9cjdb6lvjsbjlhb622lzk7iljzgi32")))

(define-public crate-hydra-macros-0.1.13 (c (n "hydra-macros") (v "0.1.13") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09cia2xxljs9xznd82qf22iv542xw8pvkjgdic0gfg2lxb4xc32n")))

(define-public crate-hydra-macros-0.1.14 (c (n "hydra-macros") (v "0.1.14") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02dcbw3c3rd22bkrm3i6a7llhvqbi4jkswllayxh6aivxd7ys7fh")))

(define-public crate-hydra-macros-0.1.15 (c (n "hydra-macros") (v "0.1.15") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11dq20jk1b9c099h5ad4n9g4vh19njk52prjs5xn9nqv55j5fj1y")))

(define-public crate-hydra-macros-0.1.16 (c (n "hydra-macros") (v "0.1.16") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qj4hbgjhv3jv3imlx25k6w76z07flc08hh8wdklf930lad5aldk")))

(define-public crate-hydra-macros-0.1.17 (c (n "hydra-macros") (v "0.1.17") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q80343g8xyzy67nmnd5f6nf3bzj6ic5bkz3r01aa1plw8wgyz1b")))

(define-public crate-hydra-macros-0.1.18 (c (n "hydra-macros") (v "0.1.18") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k614da77fn5jviziikv4ka5x51p4zh6hfv96zy7apivkmzvzsvc")))

(define-public crate-hydra-macros-0.1.19 (c (n "hydra-macros") (v "0.1.19") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gsplvkg0p2ldrvg4gq08pddrs5rlz7cqcckzvmvwzyhypivmajp")))

(define-public crate-hydra-macros-0.1.20 (c (n "hydra-macros") (v "0.1.20") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zcf7gmv90qfr9cc5583km054yngs9j5csag1g42i6nlbg6mgkzk")))

