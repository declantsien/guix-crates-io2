(define-module (crates-io hy dr hydration_context) #:use-module (crates-io))

(define-public crate-hydration_context-0.1.0 (c (n "hydration_context") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "or_poisoned") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "throw_error") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0r55hlxan5z50cg03czalyrs7jlw48ymz0nsd5vgkzvzwn96y04m") (s 2) (e (quote (("browser" "dep:wasm-bindgen" "dep:js-sys")))) (r "1.75")))

