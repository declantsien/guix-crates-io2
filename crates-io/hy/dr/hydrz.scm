(define-module (crates-io hy dr hydrz) #:use-module (crates-io))

(define-public crate-hydrz-0.1.0 (c (n "hydrz") (v "0.1.0") (d (list (d (n "minicbor") (r "^0.20.0") (f (quote ("alloc" "std" "derive"))) (d #t) (k 0)))) (h "1vn4qh05mzprf0am2zzmqd1dknlnksrivn78lw1xi3p05ap11lxr")))

(define-public crate-hydrz-0.2.0 (c (n "hydrz") (v "0.2.0") (d (list (d (n "libhydrz") (r "^0.1.0") (d #t) (k 0)))) (h "05sx66n381d4lfaxmwks0n5mdxqhkjn8dy5w0a9cdbaqvk7kc1gg")))

