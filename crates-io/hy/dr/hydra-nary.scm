(define-module (crates-io hy dr hydra-nary) #:use-module (crates-io))

(define-public crate-hydra-nary-0.0.0 (c (n "hydra-nary") (v "0.0.0") (d (list (d (n "latexify") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pex") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "1rl7g82nkq2avi1pd0ra9zrz809vzhm7vkdgma8icxvrkj0fdzbr") (f (quote (("default"))))))

