(define-module (crates-io hy dr hydroconf) #:use-module (crates-io))

(define-public crate-hydroconf-0.1.0 (c (n "hydroconf") (v "0.1.0") (h "07ak1qy74sr2yq6gqkv9bdxvwllgmniigvdkzlzp00dxlhwabwsn")))

(define-public crate-hydroconf-0.2.0 (c (n "hydroconf") (v "0.2.0") (d (list (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "dotenv-parser") (r ">=0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "05kx37vg9sb7qccr8gmpi7ham0cp81kyaj5d3ky8gc92h2wn2xi0")))

