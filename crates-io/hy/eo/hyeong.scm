(define-module (crates-io hy eo hyeong) #:use-module (crates-io))

(define-public crate-hyeong-0.1.0-beta (c (n "hyeong") (v "0.1.0-beta") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)))) (h "048vnkc8bara1d971rrwivh46dpxkpnpd1zzgracr6rj794ra6ln")))

(define-public crate-hyeong-0.1.0 (c (n "hyeong") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)))) (h "1jmiqy9ca1vp0nqhs3jzbwl8vv9hzqm59dbalbj55yab5daa2vz5")))

(define-public crate-hyeong-0.1.1 (c (n "hyeong") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "052cmxd89f5981c7p4ylhwzcs3czyf1js9r959plqv4kr4wks2fd") (f (quote (("number") ("default" "clap" "ctrlc" "termcolor"))))))

(define-public crate-hyeong-0.1.2 (c (n "hyeong") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0z98103w1px7mic85pjyw3rbzf7vagiq2dj5v8s6r994a171zby5") (f (quote (("number") ("default" "clap" "ctrlc" "termcolor"))))))

(define-public crate-hyeong-0.1.3 (c (n "hyeong") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1a4bszr17dzq1j0b6k90s0ks0pkzbb6hqlgiajniarich9pyx7v1") (f (quote (("number") ("default" "clap" "ctrlc" "termcolor"))))))

(define-public crate-hyeong-0.2.0 (c (n "hyeong") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1a9f48lwf99b0s6sbi0l9sh0y4g2zyz76fxwn60dnq9p0ka2xrq3") (f (quote (("number") ("default" "clap" "ctrlc" "termcolor"))))))

(define-public crate-hyeong-0.2.1 (c (n "hyeong") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1iq92h5kh7dczw0k31vgrdaybilzg26pnm1fh723pfvrfhffchz7") (f (quote (("number") ("default" "clap" "ctrlc" "termcolor"))))))

(define-public crate-hyeong-0.2.2 (c (n "hyeong") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0l953cxc8jpavswh6clb06x4anhwj3sjg31wlyi86hwpb2nn7cgq") (f (quote (("number") ("default" "clap" "ctrlc" "termcolor"))))))

