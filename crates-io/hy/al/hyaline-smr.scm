(define-module (crates-io hy al hyaline-smr) #:use-module (crates-io))

(define-public crate-hyaline-smr-0.1.0 (c (n "hyaline-smr") (v "0.1.0") (d (list (d (n "atomicdouble") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "loom") (r "^0.5") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0xlkby5qmff6ndsl20bmqnm7rag8wkqcwrvwhqdzbmnj2fmrznvp")))

(define-public crate-hyaline-smr-0.1.1 (c (n "hyaline-smr") (v "0.1.1") (d (list (d (n "atomicdouble") (r "^0.1.4") (d #t) (k 0)) (d (n "loom") (r "^0.5") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0fd2lvfcvj2zkd2n18lbzk6kgsywpz1rp0x4vwngmr7ixcv5jq9c")))

