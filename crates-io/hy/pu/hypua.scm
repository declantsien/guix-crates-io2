(define-module (crates-io hy pu hypua) #:use-module (crates-io))

(define-public crate-hypua-0.1.0 (c (n "hypua") (v "0.1.0") (d (list (d (n "phf") (r "^0.8.0") (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 1)))) (h "019q0xj53hpg553lcrhihnkng82i7iqz5f180c432haw6cpswr4f")))

(define-public crate-hypua-0.2.0 (c (n "hypua") (v "0.2.0") (d (list (d (n "phf") (r "^0.8.0") (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 1)))) (h "04c60yflvrimhk3gsm32yk75rmkpnhan7000wld5y536fff938mb")))

