(define-module (crates-io qw es qwest_r) #:use-module (crates-io))

(define-public crate-qwest_r-0.1.0 (c (n "qwest_r") (v "0.1.0") (h "10a5sjz9mb439ljd3m2cld0grv2p9afx40bnf1944bqmc9skcqqw")))

(define-public crate-qwest_r-0.2.0 (c (n "qwest_r") (v "0.2.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tcod") (r "^0.15") (f (quote ("serialization"))) (d #t) (k 0)))) (h "0wk235495mqcfdrj2h2ma8r9dp3nk0x6r62jwlk4x86262i6r6bd")))

