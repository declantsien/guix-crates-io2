(define-module (crates-io qw ea qweather-http-client) #:use-module (crates-io))

(define-public crate-qweather-http-client-0.1.0 (c (n "qweather-http-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json" "blocking" "gzip"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pvzs91bmgvz66spaxqxzj3c65bqvk8ccbhyc2cwgjlaa6rbzd1q") (f (quote (("reqwest-http-client" "reqwest") ("default" "reqwest-http-client"))))))

(define-public crate-qweather-http-client-0.1.1 (c (n "qweather-http-client") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json" "blocking" "gzip"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qzzgm6p14643l08l198nz1f59fv9yfcdfxa8z8fp5izrvy24plc") (f (quote (("reqwest-http-client" "reqwest") ("default" "reqwest-http-client"))))))

(define-public crate-qweather-http-client-0.2.0 (c (n "qweather-http-client") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json" "gzip"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gw4ckiczg26j8nff3f3y9qqskzaws62nacmis5lx21yi2fdb88y") (f (quote (("reqwest-http-client" "reqwest") ("default" "reqwest-http-client"))))))

(define-public crate-qweather-http-client-0.2.1 (c (n "qweather-http-client") (v "0.2.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json" "gzip"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0if9lrv380988gmf5r23cwksl2fp4yirhx9bd0sbxllmniy1qr6x") (f (quote (("reqwest-http-client" "reqwest") ("default" "reqwest-http-client"))))))

