(define-module (crates-io qw ea qweather-service) #:use-module (crates-io))

(define-public crate-qweather-service-0.1.0 (c (n "qweather-service") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "qweather-http-client") (r "^0.1.0") (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1mlygll83c4bd3db76jbicd7qf690isblchysg6c7i1mamh0wwdy")))

(define-public crate-qweather-service-0.1.1 (c (n "qweather-service") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "qweather-http-client") (r "^0.1.1") (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "time-macros") (r "^0.2.16") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0x598a786fsd3qdbs13lcysm0c0v2x7qz4k2bq4x0mp96jghjwhw")))

(define-public crate-qweather-service-0.2.0 (c (n "qweather-service") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "qweather-http-client") (r "^0.2.0") (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "time-macros") (r "^0.2.16") (f (quote ("parsing"))) (d #t) (k 0)))) (h "168amr28lkzby8dp4b8sar689438hc7gz6z677pp8vhbhpjzxvvk")))

(define-public crate-qweather-service-0.2.1 (c (n "qweather-service") (v "0.2.1") (d (list (d (n "qweather-http-client") (r "^0.2.1") (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "time-macros") (r "^0.2.16") (f (quote ("parsing"))) (d #t) (k 0)))) (h "19444mvac470g6bwjhs7fic02c5mshdyyjzr4cwkpqylzwhx61m5")))

