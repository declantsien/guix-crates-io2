(define-module (crates-io qw ea qweazsxdc) #:use-module (crates-io))

(define-public crate-qweazsxdc-0.1.0 (c (n "qweazsxdc") (v "0.1.0") (h "042qksqfzjcgim60a9w59wqh2lfq37fjb4l8kw5m0l16mdjshvgn")))

(define-public crate-qweazsxdc-0.2.0 (c (n "qweazsxdc") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ffqvmi5ipqrc4kvrim7cpxhfgq74nw04yprhv8h7hb2cdzpdva7")))

(define-public crate-qweazsxdc-0.2.1 (c (n "qweazsxdc") (v "0.2.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03a7p406l0xy9kw3v4nv66hf9s8lvj20xaqrrld3g8qn255981av")))

(define-public crate-qweazsxdc-0.2.2 (c (n "qweazsxdc") (v "0.2.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sl6xpanqrchc23yp4k44033qj341rx5lk58m2rvijx5yhb23fbq")))

(define-public crate-qweazsxdc-0.2.3 (c (n "qweazsxdc") (v "0.2.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dlpali0r1icyid8znkrz2fqnj9rdhcfm9iri2qgakj8ina5bax3")))

(define-public crate-qweazsxdc-0.2.5 (c (n "qweazsxdc") (v "0.2.5") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0imj4hq55pbzxv8ywhnqxn9hh6c48h29frc9gy2qg8dk89i175nl")))

(define-public crate-qweazsxdc-0.2.4 (c (n "qweazsxdc") (v "0.2.4") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bg0qgxqi48yy5ngpxwkxybswgplzqvqcrknx4mbv86j2d5ryqhn")))

(define-public crate-qweazsxdc-0.2.6 (c (n "qweazsxdc") (v "0.2.6") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jlk5ckxaiiiq84hhjrvkkp3835va5hyk83zcmmrds475g6v4x8a")))

(define-public crate-qweazsxdc-0.2.7 (c (n "qweazsxdc") (v "0.2.7") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z06q92jgzwpgl2hvdn70cyn6rfsmyi3s9khqjylgracv9acjayl")))

(define-public crate-qweazsxdc-0.2.8 (c (n "qweazsxdc") (v "0.2.8") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hq4yr6zigmlmchwjyqhfwqa6sbnqwhri3bkr2pix5dy13sl47w0")))

