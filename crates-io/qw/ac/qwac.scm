(define-module (crates-io qw ac qwac) #:use-module (crates-io))

(define-public crate-qwac-0.1.0 (c (n "qwac") (v "0.1.0") (h "11cfp4kxvv2ikk2539rghkqj29mvj2znnclnm6llzbnc9a8s9vch")))

(define-public crate-qwac-0.1.1 (c (n "qwac") (v "0.1.1") (h "0cdfwvkcm7jvga5nf5mjkymymfcv92ljlwilnlpdflh43xhrsc46")))

(define-public crate-qwac-0.1.2 (c (n "qwac") (v "0.1.2") (h "0hjpyicnsnj67v7vmrdfsbm2378675875p5i5wygx22l3hbf2pca") (f (quote (("alloc"))))))

(define-public crate-qwac-0.1.4 (c (n "qwac") (v "0.1.4") (h "13kgd4d3vbhkvgwjdrnndqzmmdphqlrxgm5bzpy20cg4fdxi1p8b") (f (quote (("alloc"))))))

(define-public crate-qwac-0.2.0 (c (n "qwac") (v "0.2.0") (h "0n9rc9pfrsm32bxxzqglaa42sk4hl8kb5zhj5px2i0ki75j35ni2") (f (quote (("alloc"))))))

(define-public crate-qwac-0.3.0 (c (n "qwac") (v "0.3.0") (h "0vqhn1c3f968wlv86pxmdj22hsc9gapkwr4i62hgr4qxy32gvfxk") (f (quote (("alloc"))))))

(define-public crate-qwac-0.4.0 (c (n "qwac") (v "0.4.0") (h "0fllz2r4c15aj7dwbrwnh9q1qhhzcssgj0kgnv26fz6ya2m1f683") (f (quote (("alloc"))))))

(define-public crate-qwac-0.4.1 (c (n "qwac") (v "0.4.1") (h "0n6aamannviqqqnzs2xd9854v04njydj6v1h5x1x3pmdnx21x4c0") (f (quote (("alloc"))))))

(define-public crate-qwac-0.4.2 (c (n "qwac") (v "0.4.2") (h "1y4pfnq8jb7rsnh2bl7m89hmxq1lbbkn2si6k48jnbam9j68q270") (f (quote (("alloc"))))))

(define-public crate-qwac-0.4.3 (c (n "qwac") (v "0.4.3") (h "16mwwspwssmw4qzadkjcccc23gbxf39vs5g1imga190945w27g5g") (f (quote (("alloc"))))))

(define-public crate-qwac-0.5.0 (c (n "qwac") (v "0.5.0") (h "0bganf0yy06hf5fqrhpkb0g357hpx7j1xvh4qqxwmxaqfkiz3rdz") (f (quote (("alloc"))))))

(define-public crate-qwac-0.8.0 (c (n "qwac") (v "0.8.0") (h "029wnjv99796pgnqpjl50ba4p6cnbicr3js6gsrbq4symbqmp77m") (f (quote (("f64-canvas") ("f32-canvas") ("alloc"))))))

(define-public crate-qwac-0.8.1 (c (n "qwac") (v "0.8.1") (h "0sxmsmhc2i0cc249w52kmzs091arp928simvjxnm3mpjcmcnhq6j") (f (quote (("f64-canvas") ("f32-canvas") ("alloc"))))))

(define-public crate-qwac-0.8.2 (c (n "qwac") (v "0.8.2") (h "0vwcb7z4xz3qhyjgk5l7ax6gh2dyylfzbkdb8p6haahvlkjrmdvg") (f (quote (("f64-canvas") ("f32-canvas") ("alloc"))))))

(define-public crate-qwac-0.9.0 (c (n "qwac") (v "0.9.0") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "1pqgzisf2brv1njl4xb43g5j303365hpzalmbjjszaajmrilbyv2") (f (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.9.1 (c (n "qwac") (v "0.9.1") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "0ilb18as2gc4sk6fshkph4ksy85z2nl5y3qz0phbi00qs72sjpc1") (f (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.9.2 (c (n "qwac") (v "0.9.2") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "0qdfs126shpd85kzcq7x92j05rx2vs4ligp0a1171rvppz3d0s0y") (f (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.9.3 (c (n "qwac") (v "0.9.3") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "1lxmrj68qhwcr278xb37vz6bm4hv69l1c2zmpkicnyq2k8jkg08w") (f (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.9.4 (c (n "qwac") (v "0.9.4") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "0hawc2d233lnpymz4z3krxc88gvqmxkqmp0clmnsssbz5ppr01dg") (f (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.10.0 (c (n "qwac") (v "0.10.0") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "041xq6mqgw26bgnvkf0nfyhni5s67d4p66fgazs1cn488b7f67w5") (f (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.11.0-a.1 (c (n "qwac") (v "0.11.0-a.1") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "0116wl4yiy0gxkhzbr96hhnjr57jgmi7yi674n17gy324rgyg33m") (f (quote (("std" "euclid/std" "alloc") ("f64-timings") ("f64-canvas") ("f32-timings") ("f32-canvas") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11.0-a.2 (c (n "qwac") (v "0.11.0-a.2") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "1cl2y4l503idgb09r1kxkfhdb9gma83l22xzv6nrz6zcznk0rdn2") (f (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11.0-a.3 (c (n "qwac") (v "0.11.0-a.3") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "0y06jrs243m6advc4ybv8dqzxvprbwjn53svnjbmk411kr1pq333") (f (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11.0-a.4 (c (n "qwac") (v "0.11.0-a.4") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "1h6qwabngdgx22q3jv8m4v4hbcb16kqjpk78l1v0p7p4dsi96y34") (f (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11.0-a.5 (c (n "qwac") (v "0.11.0-a.5") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "12wzjf48f50p9m1gf4wgb3vfz9bjjdpq3ns052fvcs3kki679fpm") (f (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11.0-a.6 (c (n "qwac") (v "0.11.0-a.6") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "0qqa9af0f1f65lkfgdp6h8vlc9v5axdjqs12pc1lfbb7vv0sy8bi") (f (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11.0-a.7 (c (n "qwac") (v "0.11.0-a.7") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "0zdx4agklf708kg810l7d1f6kivsfn8533rdrxx4q416ldw5v11n") (f (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11.0-a.8 (c (n "qwac") (v "0.11.0-a.8") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "0lbrmr25qm95qpjggxcs745mcspcddlgbsrs6lrv9wj0paccfl45") (f (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11.0-a.9 (c (n "qwac") (v "0.11.0-a.9") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "1i8wn23049fx1lg7h3k8l38nf19qzv5galxnmmqbm45c404sqa6p") (f (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11.0-a.10 (c (n "qwac") (v "0.11.0-a.10") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "0f767lhzc7ai37f4qkip4a6988lb5gkarzp4l3zgx97i4x3vhsyi") (f (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11.0 (c (n "qwac") (v "0.11.0") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)))) (h "1r7hfs630r0ny53v1hxhb4gr0yhp3knnsjcdjhljyf53vdx8vr66") (f (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.12.0 (c (n "qwac") (v "0.12.0") (d (list (d (n "euclid") (r "^0.22.6") (f (quote ("libm"))) (k 0)) (d (n "qwac-sys") (r "^0.11.0") (d #t) (k 0)))) (h "0g0fx6q6h91gdyrs2cp737xw5fvlyg9plmhi7g76awx9l1ia4nmp") (f (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.15.0 (c (n "qwac") (v "0.15.0") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.15.0") (d #t) (k 0)))) (h "0yl6ml6nnmxkhndp5zk5gs4vabpgm0hvan96v0j9qpvdcrcayhkh")))

(define-public crate-qwac-0.16.1 (c (n "qwac") (v "0.16.1") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.16.1") (d #t) (k 0)))) (h "0917jgbjjb6gl78k73vpin4i4d5w02vmjngnbfmhv64a7n11p1vx")))

(define-public crate-qwac-0.17.0 (c (n "qwac") (v "0.17.0") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.16.1") (d #t) (k 0)))) (h "1hwva3z9zmawva8xgyqrcfnpfmd87f2y1h1ps7fkb4r92jw2h1g8")))

(define-public crate-qwac-0.17.1 (c (n "qwac") (v "0.17.1") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.16.2") (d #t) (k 0)))) (h "0n0w14c2km7k8aqgry5kpc4i9fvfrw023jpdj99y40cin9bpwpqw")))

(define-public crate-qwac-0.18.0 (c (n "qwac") (v "0.18.0") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.18.0") (d #t) (k 0)))) (h "0nvb6kcba79qd9xjvk0sgz6z9w6svvv5a04p4nzr44kwgwch7zhw")))

(define-public crate-qwac-0.19.0 (c (n "qwac") (v "0.19.0") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.19.0") (d #t) (k 0)))) (h "15n8vbwc2cfd89lzv7apljbvdvl4wji8jvfj8jm3iq6arg19d61a")))

(define-public crate-qwac-0.21.0 (c (n "qwac") (v "0.21.0") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.21.0") (d #t) (k 0)))) (h "10048saz8nafhg619v689cm56sjsgi6ajadcm90m633c3zjlwcb4")))

(define-public crate-qwac-0.22.0 (c (n "qwac") (v "0.22.0") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.22.0") (d #t) (k 0)))) (h "1wzwm8nqqzv25lx0ahqc320p7zvq27zvjh6fv689wb131myxrszb")))

(define-public crate-qwac-0.23.0 (c (n "qwac") (v "0.23.0") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.22.0") (d #t) (k 0)))) (h "10229jvk6passaqjbsibxi9nkrv4xfpzjc58avzxszlm1q3jxzjq")))

(define-public crate-qwac-0.24.0 (c (n "qwac") (v "0.24.0") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.25.0") (d #t) (k 0)))) (h "1795j4gx289gpqf2h1g167zs74xa2fg231qlymdi6grcvkv2mrp3")))

(define-public crate-qwac-0.26.0 (c (n "qwac") (v "0.26.0") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.26.0") (d #t) (k 0)))) (h "0frpmf4fwj9iinrxjzmai4m3z3h2an09zpp16gd9d3jav3yrbpbc")))

(define-public crate-qwac-0.27.0 (c (n "qwac") (v "0.27.0") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.27.0") (d #t) (k 0)))) (h "11f59r3gh4gdmbil8jcdlwv2bbkyixw2qw90qwm44b0hzlzxz9hr")))

(define-public crate-qwac-0.28.0 (c (n "qwac") (v "0.28.0") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.28.0") (d #t) (k 0)))) (h "0wjxsc7lr6v98h915y5lrlzi1fsc37va4xx948giv5c0xfckzqip")))

(define-public crate-qwac-0.28.1 (c (n "qwac") (v "0.28.1") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.28.0") (d #t) (k 0)))) (h "0176fd1fy45g742l3pnlx66yh1ihbg423x7ivq6hpnzvbp2cpxbp")))

(define-public crate-qwac-0.28.2 (c (n "qwac") (v "0.28.2") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.28.1") (d #t) (k 0)))) (h "1ipki3nvglx27ylg0yz07i2bfi1cp3nk7i7rb5ha3v0qs5f4jx5d")))

(define-public crate-qwac-0.29.0 (c (n "qwac") (v "0.29.0") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "qwac-sys") (r "^0.28.1") (d #t) (k 0)))) (h "0gqfdd0hgyv9wq7l5pa7h6bcj3hmbj0c9lcbkwm8564jjxvpviz9")))

