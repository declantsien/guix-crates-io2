(define-module (crates-io qw ii qwiic-button-led) #:use-module (crates-io))

(define-public crate-qwiic-button-led-0.1.0 (c (n "qwiic-button-led") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1az9n5180d25li91ad3nx2qhirwjis6j1j4g9vhai1laj4gnrhkj")))

(define-public crate-qwiic-button-led-0.1.1 (c (n "qwiic-button-led") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1sk2b7ckvkcpq9wzqfnjpv6810xmyxdf9jxrv712n6h2irjbrygv")))

