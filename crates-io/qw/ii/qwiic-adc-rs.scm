(define-module (crates-io qw ii qwiic-adc-rs) #:use-module (crates-io))

(define-public crate-qwiic-adc-rs-0.1.0 (c (n "qwiic-adc-rs") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.4") (d #t) (k 0)))) (h "06nan5mws2xry0pgqvvihprl78920szlfx6qrh0zxdvnj2k7q2wk")))

(define-public crate-qwiic-adc-rs-0.1.11 (c (n "qwiic-adc-rs") (v "0.1.11") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.4") (d #t) (k 0)))) (h "1wpz4zcfxjch3awvr9gbc0k4bf6k473232knqqns62c5b8ssny3g")))

