(define-module (crates-io qw ii qwiic-relay-rs) #:use-module (crates-io))

(define-public crate-qwiic-relay-rs-0.1.0 (c (n "qwiic-relay-rs") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.4") (d #t) (k 0)))) (h "0zfxhx17d3pcn7vd6cdz5yjwax16d4xjf3cnfqbm7k9zm5nw1qk8")))

(define-public crate-qwiic-relay-rs-0.1.1 (c (n "qwiic-relay-rs") (v "0.1.1") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.4") (d #t) (k 0)))) (h "0irh378zrxkncp022knja66skj9fqmc15nzlwpfvdcb0p70d58yd")))

(define-public crate-qwiic-relay-rs-0.1.11 (c (n "qwiic-relay-rs") (v "0.1.11") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.4") (d #t) (k 0)))) (h "1drsq10vl92c9336zjpw14sjr9zjqkhdgbz3f16dvkqm7fhxh3va")))

