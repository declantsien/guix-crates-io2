(define-module (crates-io qw ii qwiic-mp3-trigger) #:use-module (crates-io))

(define-public crate-qwiic-mp3-trigger-0.1.0 (c (n "qwiic-mp3-trigger") (v "0.1.0") (d (list (d (n "i2cdev") (r "~0.3.1") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1hcylz2m5d7dgp745dvlzjj2agpsnm7psf0h7yw9k450xjc35ml2") (y #t)))

(define-public crate-qwiic-mp3-trigger-0.1.1 (c (n "qwiic-mp3-trigger") (v "0.1.1") (d (list (d (n "i2cdev") (r "~0.3.1") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0cc969abnhphfv7yvbim41hnws3x6k79qchclp4bk4fvdzpzi1qh")))

(define-public crate-qwiic-mp3-trigger-0.1.2 (c (n "qwiic-mp3-trigger") (v "0.1.2") (d (list (d (n "i2cdev") (r "~0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1mbczv99byyl9cw68cslfnhyvj9mk606h4khmmwfhpp697vl9hip")))

