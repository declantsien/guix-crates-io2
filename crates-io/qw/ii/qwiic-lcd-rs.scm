(define-module (crates-io qw ii qwiic-lcd-rs) #:use-module (crates-io))

(define-public crate-qwiic-lcd-rs-0.1.0 (c (n "qwiic-lcd-rs") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.4") (d #t) (k 0)))) (h "1lq8dkx3xdz7zdw2bnqv5nk2m90lbq5pgfg534q2iawz6d0pr1wn")))

(define-public crate-qwiic-lcd-rs-0.1.1 (c (n "qwiic-lcd-rs") (v "0.1.1") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.4") (d #t) (k 0)))) (h "0y2vm3wkzqhacd2mdzjaflqpk41h52x83jsmld8vjhb20xkn5f17")))

(define-public crate-qwiic-lcd-rs-0.1.11 (c (n "qwiic-lcd-rs") (v "0.1.11") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.4") (d #t) (k 0)))) (h "1lvf4vnp6n1x70h3zb43bcg9yyc9q3y6av4d0bmp43vwb0nyipws")))

