(define-module (crates-io qw ut qwutils) #:use-module (crates-io))

(define-public crate-qwutils-0.1.0 (c (n "qwutils") (v "0.1.0") (d (list (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "1gib8s1v21l2qz4m19s95v6d9cmhg0xxl9s8x1fa3fnh8jaami1v")))

(define-public crate-qwutils-0.1.1 (c (n "qwutils") (v "0.1.1") (d (list (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "10skfsfji2yppqzz9ymgvipr4hp95z1lml78cgvmh2zczl61bp1i")))

(define-public crate-qwutils-0.2.0 (c (n "qwutils") (v "0.2.0") (d (list (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "0wm248hnyi06ban2shr3bg8kpxwg15s46mpm0aa33q9mfq67l43c")))

(define-public crate-qwutils-0.3.0 (c (n "qwutils") (v "0.3.0") (d (list (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "0ds100dn63mxwnpvg0w77sg4g1qnm8vznk1rdbvhshc4icnd4sl1")))

(define-public crate-qwutils-0.3.1 (c (n "qwutils") (v "0.3.1") (d (list (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "0vdzvrmzclvcs4005g5sc6f5r8pdqiwgs4bd9jn81jip8vwhqg1j")))

