(define-module (crates-io qw it qwitlib) #:use-module (crates-io))

(define-public crate-qwitlib-0.1.0 (c (n "qwitlib") (v "0.1.0") (h "1fzxs05k7n08avd3x2a7c71c9sgjmgwspr99y6vwhyajk7hp9zf1")))

(define-public crate-qwitlib-0.1.1 (c (n "qwitlib") (v "0.1.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0yzs8l210mqirbvpci0hrrs0a8jwch3f4j9fis105qxvzwfccc1b")))

(define-public crate-qwitlib-0.1.2 (c (n "qwitlib") (v "0.1.2") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1h90f7dkq9gq528gf2i8wki7vy15z3f52halsvn20cqms5j0s48a")))

(define-public crate-qwitlib-0.2.2 (c (n "qwitlib") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "07q1lynqhnd9ib7mhnf3cx2k89mzlxj2g0clwy13hn82ih02h6sh")))

(define-public crate-qwitlib-0.2.3 (c (n "qwitlib") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0y0dv17wir5ipmxq5h0ygx12nj6af2pk7zj6ylq8r9f20m0n2pyb")))

(define-public crate-qwitlib-0.2.4 (c (n "qwitlib") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0zlb94004mk8x999shqqa2q5sipcfrk3w78w9wbm1a37maf5pz9y")))

(define-public crate-qwitlib-0.3.0 (c (n "qwitlib") (v "0.3.0") (d (list (d (n "calamine") (r "^0.24") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "15sghz1490vdcc4fhkcq0spl5202fln0jylsghhy3fy3bgd0rjcn")))

(define-public crate-qwitlib-0.3.1 (c (n "qwitlib") (v "0.3.1") (d (list (d (n "calamine") (r "^0.24") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "01wqz852l3dkc2fb7zfzpgry11nkkp4f9a25l7f83mf5n6swsy1s")))

(define-public crate-qwitlib-0.3.2 (c (n "qwitlib") (v "0.3.2") (d (list (d (n "calamine") (r "^0.24") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0ipa3mdkrjwfnwpbc30ar31wg1xcrp1sc95iamng3363i6y3ffjv")))

(define-public crate-qwitlib-0.3.3 (c (n "qwitlib") (v "0.3.3") (d (list (d (n "calamine") (r "^0.24") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "08fpdwpyb0bpf1ysmwlyaaynn3vpf4yr0abp123yxya3hji418sj")))

(define-public crate-qwitlib-0.3.4 (c (n "qwitlib") (v "0.3.4") (d (list (d (n "calamine") (r "^0.24") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1aggh0ca6dsw7gzbkf76xx6n61qkxw17li57f7y5610m2rp3qi24")))

(define-public crate-qwitlib-0.4.0 (c (n "qwitlib") (v "0.4.0") (d (list (d (n "calamine") (r "^0.24") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0m6cyvr6nh11rdqdxwzfiy2ss1bdvg8i8shcc7jcy779m1d6js3a")))

