(define-module (crates-io qw al qwal) #:use-module (crates-io))

(define-public crate-qwal-0.1.0 (c (n "qwal") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1ax4310msim68yilvmmn9mjnp3rgndca757hcqq15vyhjrimmpyw") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio") ("async-std" "dep:async-std"))))))

