(define-module (crates-io qw er qwerty) #:use-module (crates-io))

(define-public crate-qwerty-0.1.0 (c (n "qwerty") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.169") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0l0rvjfl2a76yfr0bh5np4yfy7p1fvf0g3rh52ymqs0krflgialz") (f (quote (("strict" "clippy") ("default"))))))

