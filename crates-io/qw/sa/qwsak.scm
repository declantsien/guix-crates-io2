(define-module (crates-io qw sa qwsak) #:use-module (crates-io))

(define-public crate-qwsak-0.1.2 (c (n "qwsak") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "quakeworld") (r "^0.0.2") (f (quote ("ascii_strings"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "1agcwrccyl3y2rrc3wcqg5nlvf05ck85s2mp88nzv0qjcfmhb9cl")))

(define-public crate-qwsak-0.1.5 (c (n "qwsak") (v "0.1.5") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "quakeworld") (r "^0.2.1") (f (quote ("ascii_strings"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "1dv652h8x8gfphab3cbs4zfcx47gkzknz4bbibazhm6splah3zx2")))

(define-public crate-qwsak-0.2.0 (c (n "qwsak") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "quakeworld") (r "^0.2.3") (f (quote ("ascii_strings" "trace"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "0f9jackdf67dv5hbwsq88lhwgjcsv9gqjk10wm49s469hr32vb88")))

