(define-module (crates-io qw ik qwiki) #:use-module (crates-io))

(define-public crate-qwiki-0.1.1 (c (n "qwiki") (v "0.1.1") (d (list (d (n "wikipedia") (r "^0.3.4") (d #t) (k 0)))) (h "10cxpgi9x7rvfjbzbbr4hhxmxhy62ni438c91vi0l26j7r9ac1dl")))

(define-public crate-qwiki-0.1.2 (c (n "qwiki") (v "0.1.2") (d (list (d (n "wikipedia") (r "^0.3.4") (d #t) (k 0)))) (h "04h5lyf7qv1rfg1dk8955w57g8hxvy7a4f4216fy3ghr0fvnh9xj")))

(define-public crate-qwiki-0.2.0 (c (n "qwiki") (v "0.2.0") (d (list (d (n "wikipedia") (r "^0.3.4") (d #t) (k 0)))) (h "105j2pxpm8jcm3nyjn182q87pxdwpv0wfvzs8crfb3fj4mzbmbhx")))

(define-public crate-qwiki-0.3.0 (c (n "qwiki") (v "0.3.0") (d (list (d (n "wikipedia") (r "^0.3.4") (d #t) (k 0)))) (h "1yl1inwfwrwp08f49zcqhfkf7vvgzfrfrp492622ksmq0dlf2xkj")))

(define-public crate-qwiki-0.3.1 (c (n "qwiki") (v "0.3.1") (d (list (d (n "wikipedia") (r "^0.3.4") (d #t) (k 0)))) (h "1r6r821ihvvmxl0gg909f1bdjjjdf9sdj9bny3k901b8zg47q8vj")))

