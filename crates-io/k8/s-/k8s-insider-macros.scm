(define-module (crates-io k8 s- k8s-insider-macros) #:use-module (crates-io))

(define-public crate-k8s-insider-macros-0.3.4 (c (n "k8s-insider-macros") (v "0.3.4") (d (list (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)))) (h "1kxnb0qvfk645cgjhw0fkjfkc3h8dp5f56rs763ghi00grg2j2v3")))

(define-public crate-k8s-insider-macros-0.4.0 (c (n "k8s-insider-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)))) (h "0mcw1q2m55x1iqn5nlpwa4dr0bi5nsdlyz1fjvvfq8a23a5337hs")))

(define-public crate-k8s-insider-macros-0.4.1 (c (n "k8s-insider-macros") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)))) (h "1vkw3ksivy16qmzja3rm96xx54b1h4spxha0bn6az0b6ihna7cm4")))

