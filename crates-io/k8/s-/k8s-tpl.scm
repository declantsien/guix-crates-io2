(define-module (crates-io k8 s- k8s-tpl) #:use-module (crates-io))

(define-public crate-k8s-tpl-0.1.0 (c (n "k8s-tpl") (v "0.1.0") (d (list (d (n "gtmpl") (r "^0.5.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "03xgb8i2dhpzdsxcx1rxn3xqaqyly4gmbibd7i81dh4h9hgwbmws")))

(define-public crate-k8s-tpl-0.2.0 (c (n "k8s-tpl") (v "0.2.0") (d (list (d (n "gtmpl") (r "^0.5.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1kgyin2nrr0fz5hxahmh2fpg5zlyfd7n9xzjm2ds399yscxhm1rl")))

(define-public crate-k8s-tpl-0.3.0 (c (n "k8s-tpl") (v "0.3.0") (d (list (d (n "gtmpl") (r "^0.5.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1b0zn4gzc5v021j52id16082jmizi8xi3s7yicf7cml3ks5iw0p0")))

(define-public crate-k8s-tpl-0.4.0 (c (n "k8s-tpl") (v "0.4.0") (d (list (d (n "gtmpl") (r "^0.5.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1ia8drcscm56xg6db2rmd4nikc9p97zy1m1s6kw5szzmknllcs0y")))

(define-public crate-k8s-tpl-0.5.0 (c (n "k8s-tpl") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "gtmpl") (r "^0.5.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0xx56flap9igdg5f7qg67dxigxx8893rpz84vvkaqmr23s2g4cdj")))

