(define-module (crates-io k8 s- k8s-openapi-derive) #:use-module (crates-io))

(define-public crate-k8s-openapi-derive-0.5.0-beta.1 (c (n "k8s-openapi-derive") (v "0.5.0-beta.1") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "= 0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lci63jn69x9y21m948gv6crs31vbdv6jkh79yzi926h80pn70ws")))

(define-public crate-k8s-openapi-derive-0.5.1-beta.1 (c (n "k8s-openapi-derive") (v "0.5.1-beta.1") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "= 0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1id8y5dmdjk217i1jh13aq0ncybz7f7kbr4mjrkhvnymc9irz4bb")))

(define-public crate-k8s-openapi-derive-0.6.0 (c (n "k8s-openapi-derive") (v "0.6.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "= 0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1k1ir0lsxc5l4g67lj3lsh7gswfnqsm44bcjz2rbfpn2db2nikg4")))

(define-public crate-k8s-openapi-derive-0.7.0 (c (n "k8s-openapi-derive") (v "0.7.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "= 0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qj6lnfisrf9x5h0pbqxsxgjr8ay1lhrkmdhxdq2gyb1kgf8yzfk")))

(define-public crate-k8s-openapi-derive-0.7.1 (c (n "k8s-openapi-derive") (v "0.7.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "= 0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1a1g364awnjfr4h09qjn7fk9qfp6af87j394nayxrabpyzm4rhvs")))

(define-public crate-k8s-openapi-derive-0.8.0 (c (n "k8s-openapi-derive") (v "0.8.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "= 0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pl8ml34vq3qs23dcx1zvba7xd3i3qv922wy3jjixjdlf22c0l24")))

(define-public crate-k8s-openapi-derive-0.9.0 (c (n "k8s-openapi-derive") (v "0.9.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zk42g2kj1ln9vdf6i63jkkbrhxqz9c3yxy9wwl15v97fx3dk7b7")))

(define-public crate-k8s-openapi-derive-0.10.0 (c (n "k8s-openapi-derive") (v "0.10.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kwjxyacr37hqgp9l8dhi1w969jf54ar7bz0y95fnwwrds4gs1zw")))

(define-public crate-k8s-openapi-derive-0.11.0 (c (n "k8s-openapi-derive") (v "0.11.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1pc8g893n9wzvymg4a0y6kmx7h5mbac40jq932n30p8r2gvxggyp")))

(define-public crate-k8s-openapi-derive-0.12.0 (c (n "k8s-openapi-derive") (v "0.12.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0apr4xa4iwm6xi20q9xm0x7i1c83agqbgx6j75mm4yc87ard460b")))

(define-public crate-k8s-openapi-derive-0.13.0 (c (n "k8s-openapi-derive") (v "0.13.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06n33h395mzf2rcwzsmd263f1bn8xhh1xswgrny2fdk4r359zr7w")))

(define-public crate-k8s-openapi-derive-0.13.1 (c (n "k8s-openapi-derive") (v "0.13.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12xr92s3cb2ffshwmiss2msg9qdaihz4qdacb118xm7870rvw0ad")))

(define-public crate-k8s-openapi-derive-0.14.0 (c (n "k8s-openapi-derive") (v "0.14.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "04qc80srwl4v916xwn1273fy4r43rpkwa62g50q0dalvih01xxly")))

(define-public crate-k8s-openapi-derive-0.15.0 (c (n "k8s-openapi-derive") (v "0.15.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.15.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "1aq0ykigc85cxy6sbay2mqdl98q3rngx7p62kc0qajlk1lb1w1d4")))

(define-public crate-k8s-openapi-derive-0.16.0 (c (n "k8s-openapi-derive") (v "0.16.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.16.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "0ks01sbk1f9c9w2pvi67zb652lbyvlqs5aywy3z9mmdglw44zdsg")))

(define-public crate-k8s-openapi-derive-0.17.0 (c (n "k8s-openapi-derive") (v "0.17.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.17.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "17m9x9xhhqw6gbasl87hfxmv2yij1xvmjqfmfcadpfimh7xwf4bw")))

(define-public crate-k8s-openapi-derive-0.18.0 (c (n "k8s-openapi-derive") (v "0.18.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.18.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "17nkmzfxa6iphnv5bz1xx8vpnvy1hsvmdi2n0adypar36mbikxd2")))

(define-public crate-k8s-openapi-derive-0.19.0 (c (n "k8s-openapi-derive") (v "0.19.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "1l251y897x66yd8xrq4vmn0987ahwa5x17cpbj0ma30nqm71s9rr")))

(define-public crate-k8s-openapi-derive-0.20.0 (c (n "k8s-openapi-derive") (v "0.20.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "k8s-openapi-codegen-common") (r "=0.20.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "08c2plcyg7m4j6br5cjqxcq3kw2ndx8dsa9nxdwhda8i4bd3f4gi")))

(define-public crate-k8s-openapi-derive-0.21.0 (c (n "k8s-openapi-derive") (v "0.21.0") (d (list (d (n "k8s-openapi-codegen-common") (r "=0.21.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "0vbcy0c3bxdf65jlvz7psn8226bmjaqjmwfmd86d1gjcdrlll52y")))

(define-public crate-k8s-openapi-derive-0.21.1 (c (n "k8s-openapi-derive") (v "0.21.1") (d (list (d (n "k8s-openapi-codegen-common") (r "=0.21.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "1mr1dlngivd9dpc4lcp1771k0ady26bfvgk9rh9gp3bn5b0ylb9k")))

(define-public crate-k8s-openapi-derive-0.22.0 (c (n "k8s-openapi-derive") (v "0.22.0") (d (list (d (n "k8s-openapi-codegen-common") (r "=0.22.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "075cv7kzj7ffdhcswj91rv8pj3hv395m84kczhd01hg9zwb16n3i")))

