(define-module (crates-io k8 s- k8s-pb) #:use-module (crates-io))

(define-public crate-k8s-pb-0.1.0 (c (n "k8s-pb") (v "0.1.0") (d (list (d (n "prost") (r "^0.8.0") (d #t) (k 0)))) (h "0v7ckd3az1d60551d9ii5z0sncanxp08y5pcf8pcgnwqn9azvr2s") (r "1.65.0")))

(define-public crate-k8s-pb-0.2.0 (c (n "k8s-pb") (v "0.2.0") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)))) (h "1vn967d68xpmdkdbnz0haz4cn2r6wv90y7jf1952xcw92m24ahkn") (r "1.65.0")))

(define-public crate-k8s-pb-0.3.0 (c (n "k8s-pb") (v "0.3.0") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)))) (h "1f01bsnn8svny555hy8yp86lly3cf24maa2l4137za1qs3nzxqql") (r "1.65.0")))

