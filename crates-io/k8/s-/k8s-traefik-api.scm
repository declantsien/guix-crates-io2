(define-module (crates-io k8 s- k8s-traefik-api) #:use-module (crates-io))

(define-public crate-k8s-traefik-api-0.1.0 (c (n "k8s-traefik-api") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.18.0") (f (quote ("v1_26" "schemars"))) (d #t) (k 0)) (d (n "kube") (r "^0.82.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "02grnyiddjb2802v3f8gxjhmld50cz1qjdsq8aaqhm20y5sjl9ri")))

