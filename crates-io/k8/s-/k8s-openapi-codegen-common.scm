(define-module (crates-io k8 s- k8s-openapi-codegen-common) #:use-module (crates-io))

(define-public crate-k8s-openapi-codegen-common-0.5.0 (c (n "k8s-openapi-codegen-common") (v "0.5.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1bkkra98xbgf1zb1a53r0h05bl88nd6czahkh5hh567zic6a0d4z")))

(define-public crate-k8s-openapi-codegen-common-0.5.1 (c (n "k8s-openapi-codegen-common") (v "0.5.1") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "154g95xx6mpsfcfvg3il7mqpnhbiffzwz3pmvl0wx5n79wkgdn0q")))

(define-public crate-k8s-openapi-codegen-common-0.6.0 (c (n "k8s-openapi-codegen-common") (v "0.6.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "0v8833njm09vm86w3q4fzsa8s51v43qfzh3qjfzwxfv1bkaqldsp")))

(define-public crate-k8s-openapi-codegen-common-0.7.0 (c (n "k8s-openapi-codegen-common") (v "0.7.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "0kn8szl3d9rd6r33mc9y7pf859lbfjy422nzrc0dzbsz6025aq0f")))

(define-public crate-k8s-openapi-codegen-common-0.8.0 (c (n "k8s-openapi-codegen-common") (v "0.8.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1cyg19h2g27y27qqvgnf7w0dib689amigdp67zvy5dab00iky3mz")))

(define-public crate-k8s-openapi-codegen-common-0.9.0 (c (n "k8s-openapi-codegen-common") (v "0.9.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1l61cjbwni57c0qaaqjrccpi8rvwjqbm57jy7wzcwam6k5rwkvpk")))

(define-public crate-k8s-openapi-codegen-common-0.10.0 (c (n "k8s-openapi-codegen-common") (v "0.10.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1i9wny3lavya7xnmsvnazz0j36b471qa8lm05bnsw87zfq3r9fhn")))

(define-public crate-k8s-openapi-codegen-common-0.11.0 (c (n "k8s-openapi-codegen-common") (v "0.11.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1mhc7pvxzy1w7jw1c4d7ig5p16s1vj653qblnl03nsg18cg3k684")))

(define-public crate-k8s-openapi-codegen-common-0.12.0 (c (n "k8s-openapi-codegen-common") (v "0.12.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1gbdavcz5j1kkqckj06hfxcwv389gvj5m6n288flgb24msdb4hmi")))

(define-public crate-k8s-openapi-codegen-common-0.13.0 (c (n "k8s-openapi-codegen-common") (v "0.13.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "0vxb23dlc10ccx35n02m8da1bp3iczl7z27x53fzzabdk258bv0n")))

(define-public crate-k8s-openapi-codegen-common-0.13.1 (c (n "k8s-openapi-codegen-common") (v "0.13.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1cr0f7wvdggvd57c3vsxy2bf2yhhi7rsy9qwg4wj6jqa20c5ihkn")))

(define-public crate-k8s-openapi-codegen-common-0.14.0 (c (n "k8s-openapi-codegen-common") (v "0.14.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)))) (h "05k168kwahppws7yami6ya4gvyjnxgbb0sq2pz2810lfbrd7fivj")))

(define-public crate-k8s-openapi-codegen-common-0.15.0 (c (n "k8s-openapi-codegen-common") (v "0.15.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)))) (h "1lj0q7siihyp9qr3jg3dzass0j456mjrsprb30pnp59fv1z5mqsx")))

(define-public crate-k8s-openapi-codegen-common-0.16.0 (c (n "k8s-openapi-codegen-common") (v "0.16.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)))) (h "0hik1pxzc7ypcajwnshqj4zd9cbn9jfwmm8x5r41rlk6974wavim")))

(define-public crate-k8s-openapi-codegen-common-0.17.0 (c (n "k8s-openapi-codegen-common") (v "0.17.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)))) (h "1cqid669yvxrkiwdvqig3x94gppaja4x6qkcbsqakwcrb9gn6ibf")))

(define-public crate-k8s-openapi-codegen-common-0.18.0 (c (n "k8s-openapi-codegen-common") (v "0.18.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)))) (h "0cw7z61xz50zl6ny15pbbv7q03qvgigpmn5ai297g18xhj9qr9ls")))

(define-public crate-k8s-openapi-codegen-common-0.19.0 (c (n "k8s-openapi-codegen-common") (v "0.19.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)))) (h "0pwsb3phz2jwc9389hcqsccssz9n9wrlvvjjgdg74s8jcw48ay2m")))

(define-public crate-k8s-openapi-codegen-common-0.20.0 (c (n "k8s-openapi-codegen-common") (v "0.20.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)))) (h "19spmshjmww56hbcwlg9yj7r3iiqs1zwffrvffq2hl3dxl0hxy3i")))

(define-public crate-k8s-openapi-codegen-common-0.21.0 (c (n "k8s-openapi-codegen-common") (v "0.21.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)))) (h "0yzkds29ndlcz6yd0hzshmsz47gq2xssa3vcggh0a65nxiyq336d")))

(define-public crate-k8s-openapi-codegen-common-0.21.1 (c (n "k8s-openapi-codegen-common") (v "0.21.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)))) (h "1mycfwvnwwl4z7pb73qg2s232bjilv1zai7na29nvxdfm99d8ycm")))

(define-public crate-k8s-openapi-codegen-common-0.22.0 (c (n "k8s-openapi-codegen-common") (v "0.22.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)))) (h "07rns55vp0ca0p4iakiynm4n9zjjw6mgfgjhmn4xg6hpmhgs40xw") (s 2) (e (quote (("serde" "dep:serde"))))))

