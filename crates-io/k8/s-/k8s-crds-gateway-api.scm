(define-module (crates-io k8 s- k8s-crds-gateway-api) #:use-module (crates-io))

(define-public crate-k8s-crds-gateway-api-0.1.0 (c (n "k8s-crds-gateway-api") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.21.0") (d #t) (k 0)) (d (n "kube") (r "^0.88.1") (k 0)) (d (n "kube-derive") (r "^0.88.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (k 0)) (d (n "typed-builder") (r "^0.18.1") (o #t) (d #t) (k 0)))) (h "11axm3x1jgdp1yg3hacrrl23ixx4flc4wkb69iam52fvl36nd69i") (f (quote (("_docs" "k8s-openapi/v1_29")))) (s 2) (e (quote (("schemars" "dep:schemars" "k8s-openapi/schemars" "kube/derive") ("builder" "dep:typed-builder"))))))

