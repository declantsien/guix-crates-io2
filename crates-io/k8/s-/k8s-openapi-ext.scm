(define-module (crates-io k8 s- k8s-openapi-ext) #:use-module (crates-io))

(define-public crate-k8s-openapi-ext-0.0.2 (c (n "k8s-openapi-ext") (v "0.0.2") (d (list (d (n "k8s-openapi") (r "^0.15") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.15") (f (quote ("v1_24"))) (d #t) (k 2)))) (h "08baf12b7ky3l9r9xs6mzf2r9gp58c4yyazzdsbsk6m96x4wax91") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.3 (c (n "k8s-openapi-ext") (v "0.0.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.15") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.15") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0r4j5r56qmx0cfnf6dj2h3j96zh6v8kqgf684b1kl6x8sxfamp7y") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.4 (c (n "k8s-openapi-ext") (v "0.0.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.15") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.15") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "054jhrpwa3z0afvp5pnbcy7zgxxhkabhx7d3022lsn25nlyaywrb") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.5 (c (n "k8s-openapi-ext") (v "0.0.5") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.15") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.15") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06n6h0yiwdhmmyjrjrl34hh4dahicmnhmd8czrhnp4ah00va5cav") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.6 (c (n "k8s-openapi-ext") (v "0.0.6") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.15") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.15") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08b2bnhjbsv1mbl3b4rvmqrqxz026356nhjjfdv1a8rj95yca7q4") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.7 (c (n "k8s-openapi-ext") (v "0.0.7") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1nmnb567bl9dvvjh1n84jl4qsxrrjg76r5if0kwlgfnm9qldhzqb") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.8 (c (n "k8s-openapi-ext") (v "0.0.8") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11ml681wvhr46v6g17as6zd4fmc5c7m48i190mxws4imkhlywbn4") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.9 (c (n "k8s-openapi-ext") (v "0.0.9") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0583j5m9c7jgjj7jrfgkhjmc32km0sg0xr7ihqzys3vff6zn3s9m") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.10 (c (n "k8s-openapi-ext") (v "0.0.10") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gr9y9gb7vz3x5wj0y7cmlhwackxdvwiqs7hyzn7a43i6icrmp43") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.11 (c (n "k8s-openapi-ext") (v "0.0.11") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "05snhhzh2arkbcmpskgkg33l4s7ivgqrjlccza7nkf0x5yr6f9zj") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.12 (c (n "k8s-openapi-ext") (v "0.0.12") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03rzczjwvaxbx47j2ix8cvzr3vw0n10y8mvwbw53v0q21xczgdsf") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.13 (c (n "k8s-openapi-ext") (v "0.0.13") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1j5bpzh12j22rzlqgzp4n12w137azzb2l1sxy1pnzrqjznkyq0rq") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.14 (c (n "k8s-openapi-ext") (v "0.0.14") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16") (f (quote ("v1_24"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16mzzys0zbyi4vch7amlf00jdna3rx7r5hmrgs7clrrvy9z4s985") (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.15 (c (n "k8s-openapi-ext") (v "0.0.15") (d (list (d (n "base64") (r "^0.20") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (f (quote ("v1_26"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0b0q5x5svg091b346ds5c6mpbqq6jpkxjksknkb4bad2l8xjjwh7") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.16 (c (n "k8s-openapi-ext") (v "0.0.16") (d (list (d (n "base64") (r "^0.20") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (f (quote ("v1_26"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "031mb5ima8mm8xhb1kan1qay3zl8wj53wbk006y4cwwzc69xs1gl") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.17 (c (n "k8s-openapi-ext") (v "0.0.17") (d (list (d (n "base64") (r "^0.20") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (f (quote ("v1_26"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1302sh006czrcp90ppsb7hpy98s9h6i4w4rvrkspx33svw7sa258") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.18 (c (n "k8s-openapi-ext") (v "0.0.18") (d (list (d (n "base64") (r "^0.20") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (f (quote ("v1_26"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jp1qbmgdy3lqzcppsr2cwnrmklqirm4vqwjm5d630dk9k200p1l") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.19 (c (n "k8s-openapi-ext") (v "0.0.19") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (f (quote ("v1_26"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0x42yh62qv0kjcqdak2rdyvxhwy1ym5maf6hyxl5s3izk5zcp1y6") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.20 (c (n "k8s-openapi-ext") (v "0.0.20") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17") (f (quote ("v1_26"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "18y9b9dync2jg3ag5389ldy8fi53ja51ak6c6w72hayhzsjr6fzw") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.21 (c (n "k8s-openapi-ext") (v "0.0.21") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.18") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.18") (f (quote ("v1_26"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qq8lswh6knmw5ryvric44hn7v041mijlqvzq1ycz4yv5r0yn4zx") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.22 (c (n "k8s-openapi-ext") (v "0.0.22") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.18") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.18") (f (quote ("v1_26"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1az5457qral0111vla8nd0kblwsa500id3zlrdsbiirsc4jmydnv") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.23 (c (n "k8s-openapi-ext") (v "0.0.23") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.18") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.18") (f (quote ("v1_26"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19axjxrmma5ykfk4wbh54cqzdd823a2vkqn2m28hyn32yaqllsjw") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.24 (c (n "k8s-openapi-ext") (v "0.0.24") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.18") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.18") (f (quote ("v1_26"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02qwcb5jg2r3qalqc22zpjax5y59djx7d6jyl73xavmj6i9hx0kj") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.25 (c (n "k8s-openapi-ext") (v "0.0.25") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.19") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.19") (f (quote ("v1_27"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ly3y8m6x2852cympbz3khspzzwwn402ycya4hkandi1yjrs1f47") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.26 (c (n "k8s-openapi-ext") (v "0.0.26") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (f (quote ("v1_28"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1pmz80l3wfrfx51rgrlfmpfx8qd0spjyblickw4sdrcppazzpla8") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.27 (c (n "k8s-openapi-ext") (v "0.0.27") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (f (quote ("v1_28"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1h4v2vj1cwhl2nljyfi4y4l08ix2qy491rj6p54s9m8s52kqmzrb") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.28 (c (n "k8s-openapi-ext") (v "0.0.28") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (f (quote ("v1_28"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0729jq4kl2zqmpqc8ikak61hbaj05ibx2igg0gc56h1bhwkzyz4f") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.29 (c (n "k8s-openapi-ext") (v "0.0.29") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (f (quote ("v1_28"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0c4bqb86fanja50hs2hdig95i7ynrrlzf5lx1qkz43xpbpsqv9ql") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.30 (c (n "k8s-openapi-ext") (v "0.0.30") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (f (quote ("v1_28"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zk4d4ibb285rbs4di1z8n1ysrpm2pgkhg5zslwnqmf9592538d7") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.31 (c (n "k8s-openapi-ext") (v "0.0.31") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (f (quote ("v1_28"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "089q2a3nwas3xs5sbh2hzj236748psxnng6hh14sg58ja7803s23") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.32 (c (n "k8s-openapi-ext") (v "0.0.32") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.20") (f (quote ("v1_28"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1mvq7r0n81wnfrxjvak28s1i5czc6pn696jf6yxqakbbfg149kp8") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.33 (c (n "k8s-openapi-ext") (v "0.0.33") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.21") (f (quote ("latest"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rl0xzzq4z5pmypwcp2lsv1v6ifz57ck8klk9qq9az9ff95ifpgr") (f (quote (("pedantic")))) (r "1.61")))

(define-public crate-k8s-openapi-ext-0.0.34 (c (n "k8s-openapi-ext") (v "0.0.34") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.21") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.21") (f (quote ("latest"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1356h2lgk0i8bgi32g9jf9viliiprwcgky6ih9n3j8m338vjal92") (f (quote (("pedantic")))) (r "1.61")))

