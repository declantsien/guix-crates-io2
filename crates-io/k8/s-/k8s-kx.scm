(define-module (crates-io k8 s- k8s-kx) #:use-module (crates-io))

(define-public crate-k8s-kx-0.2.1 (c (n "k8s-kx") (v "0.2.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "skim") (r "^0.6.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.9.5") (d #t) (k 0)))) (h "1kqhnspsddrc25h6n2swk0zjri9lfckbxznn3z2a29gdp17fh5bv")))

(define-public crate-k8s-kx-0.3.0 (c (n "k8s-kx") (v "0.3.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "os-str-generic") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "skim") (r "^0.6.8") (d #t) (k 0)))) (h "0farpczsp362z19dfznzqxla2gja461zb293my2c4g1y3hhfsl9v")))

