(define-module (crates-io k8 s- k8s-deviceplugin) #:use-module (crates-io))

(define-public crate-k8s-deviceplugin-0.1.0 (c (n "k8s-deviceplugin") (v "0.1.0") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (d #t) (k 1)))) (h "11fnzi4hkjl4svl1bxhp6hxrc08j9jpb2xj6887mgkbyd8s9l7fj")))

(define-public crate-k8s-deviceplugin-0.2.0 (c (n "k8s-deviceplugin") (v "0.2.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)))) (h "1h99xj5yvyns9ghmbcjdybapvzz1l4503bkbkzzwrds1w3g44s12")))

