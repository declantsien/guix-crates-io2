(define-module (crates-io k8 -o k8-obj-core) #:use-module (crates-io))

(define-public crate-k8-obj-core-0.1.0 (c (n "k8-obj-core") (v "0.1.0") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)))) (h "0wdfh56aw8snn93fixylqy7ppmkg5mz93z12ag61lfhpwhg8pwpj")))

(define-public crate-k8-obj-core-1.0.0 (c (n "k8-obj-core") (v "1.0.0") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)))) (h "17m5gspdbwg1zvbgx8kf35nlabjxmyhqfws6mh58vmxq4xfywdyh")))

(define-public crate-k8-obj-core-1.1.0 (c (n "k8-obj-core") (v "1.1.0") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)))) (h "1q40cn51hb805aaj1dyrxw4485irf643s2xrpdgybcl24460mlkd")))

(define-public crate-k8-obj-core-2.0.0 (c (n "k8-obj-core") (v "2.0.0") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "1mcazl5jhb2vaxdqls18a0djwics4hwvmbgkam753q2nfxqc7y8w")))

(define-public crate-k8-obj-core-2.1.0 (c (n "k8-obj-core") (v "2.1.0") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "1yq026k2znmnp5kfjmid5gazm0abnvcjgpf6000yv64jwngm3fxz")))

(define-public crate-k8-obj-core-2.1.1 (c (n "k8-obj-core") (v "2.1.1") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "1qgwxvp0q7mbggyld9vbpplqgckmb79p9bhv4lvras64hxcxkqpz")))

(define-public crate-k8-obj-core-2.2.0 (c (n "k8-obj-core") (v "2.2.0") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "1agc2gpr6i45559z4cvpqifz451g1lsi9fd4m264b72sc99124s4") (y #t)))

