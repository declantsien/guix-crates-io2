(define-module (crates-io k8 -o k8-obj-metadata) #:use-module (crates-io))

(define-public crate-k8-obj-metadata-0.1.0 (c (n "k8-obj-metadata") (v "0.1.0") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)))) (h "0m498m37dkb3jzfrz1af735ggzgbcnmil5nhvldx3nabpv6x3x7r")))

(define-public crate-k8-obj-metadata-0.1.1 (c (n "k8-obj-metadata") (v "0.1.1") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)))) (h "1z1rzq7anyxzxh2pcyr0vwkj1x1zkr339mpnpbr1ngvq0v20hi83")))

(define-public crate-k8-obj-metadata-1.0.0 (c (n "k8-obj-metadata") (v "1.0.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)))) (h "1ah2y792svp73zd12zzvrlc8rd892ns9f2bkap81c4k0shhyfyqa")))

(define-public crate-k8-obj-metadata-2.0.0 (c (n "k8-obj-metadata") (v "2.0.0") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "057kvcxz1834wc25fgvp5zh9453jhrwma5j47hnqnwmg4kyj8f1r")))

(define-public crate-k8-obj-metadata-2.0.1 (c (n "k8-obj-metadata") (v "2.0.1") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "1cdybimbkqhgxlgra5yw19w48rnjw9fnljkfc2k2c5ddc397p86w")))

(define-public crate-k8-obj-metadata-2.0.2 (c (n "k8-obj-metadata") (v "2.0.2") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "1la42y9sdfr4y6vq9i726gfnh6l9sfarha1xbrabm746qh6fbb8f")))

