(define-module (crates-io k8 -o k8-obj-storage) #:use-module (crates-io))

(define-public crate-k8-obj-storage-1.0.0 (c (n "k8-obj-storage") (v "1.0.0") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "k8-obj-core") (r "^1.0.0") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)))) (h "0gsbzv739dr15dp3qchk1y8nl1p6imfnhxq5qvv8yrxfal4h4fkj")))

(define-public crate-k8-obj-storage-1.1.0 (c (n "k8-obj-storage") (v "1.1.0") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "k8-obj-core") (r "^1.1.0") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)))) (h "1m7l2ksg29qvsrax8l8fjjgry2jnwq95n6grdamjjinwx2mbaqmf")))

(define-public crate-k8-obj-storage-2.0.0 (c (n "k8-obj-storage") (v "2.0.0") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "08vhf7csc91d5xbkdv7cav8jl5pzf2pnkir6i9rmml46awrsq92r")))

