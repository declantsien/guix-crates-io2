(define-module (crates-io k8 -m k8-metadata-core) #:use-module (crates-io))

(define-public crate-k8-metadata-core-0.1.0 (c (n "k8-metadata-core") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)))) (h "1dbssihynnn70nv8kcfciwddny92s650fq7fq461jqxbs040pnas")))

(define-public crate-k8-metadata-core-0.1.2 (c (n "k8-metadata-core") (v "0.1.2") (d (list (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)))) (h "0i4pr12mx6d4p5b5964fck93ny12wfk6j23vf9dc5xj7grz26gsp")))

