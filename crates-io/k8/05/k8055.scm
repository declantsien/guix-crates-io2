(define-module (crates-io k8 #{05}# k8055) #:use-module (crates-io))

(define-public crate-k8055-0.0.1 (c (n "k8055") (v "0.0.1") (d (list (d (n "usb") (r "~0.1.0") (d #t) (k 0)))) (h "1c8mxc5frb1cllki9kmdmbwsmqp7p3jhn28k54ny8d1y4na7pmva")))

(define-public crate-k8055-0.0.2 (c (n "k8055") (v "0.0.2") (d (list (d (n "usb") (r "~0.1.0") (d #t) (k 0)))) (h "144sb2pvzwn8y0bszmr2dzq9ddp9sxa2x1d4wmz80lp4q5zv44xc")))

(define-public crate-k8055-0.0.3 (c (n "k8055") (v "0.0.3") (d (list (d (n "usb") (r "~0.1.0") (d #t) (k 0)))) (h "13aq48zab1b5rwlz0wahrizpgpv343d1sq9xbdbl0b3wk236py99")))

(define-public crate-k8055-0.0.4 (c (n "k8055") (v "0.0.4") (d (list (d (n "usb") (r "~0.1.0") (d #t) (k 0)))) (h "18vm0svv5wjlrw3l8nd0mfjasm9mwagwfzw9nma3grdnvmpkdp3i")))

(define-public crate-k8055-0.0.5 (c (n "k8055") (v "0.0.5") (d (list (d (n "rustc-serialize") (r "~0.2.7") (d #t) (k 0)) (d (n "usb") (r "~0.1.0") (d #t) (k 0)))) (h "16xf3x00i9hadblx24mignk2spwaf3jm2dmi9sqf5brxxxav0wlw")))

(define-public crate-k8055-0.0.6 (c (n "k8055") (v "0.0.6") (d (list (d (n "rustc-serialize") (r "~0.2.7") (d #t) (k 0)) (d (n "usb") (r "~0.1.0") (d #t) (k 0)))) (h "1xplz6csk4dgg7w8vkdhfny74br83swashdfvwpb2x1gwqv6qihg")))

(define-public crate-k8055-0.1.0 (c (n "k8055") (v "0.1.0") (d (list (d (n "bitflags") (r "~0.3") (d #t) (k 0)) (d (n "error-chain") (r "~0.7") (d #t) (k 0)) (d (n "libusb") (r "~0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "0c5l1ap2ac4865m5kwmnfd4pfx4nvzyf98xsssxdld1y1i4q0shj")))

(define-public crate-k8055-0.2.0 (c (n "k8055") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "0mjwibi7kcg5qfvw26cjczgc55wr7024ydwshqm2zzc3ammsvb0h")))

(define-public crate-k8055-0.2.1 (c (n "k8055") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "10id7b494j801lb683lkj75jafckxjk1l7nnp8mj9bvp4b0c0x5i")))

(define-public crate-k8055-0.2.3 (c (n "k8055") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1v6ypsh0q7fznp60r2b9lsxwva4j3rxh24gsmzph43lmyr40zg5m")))

