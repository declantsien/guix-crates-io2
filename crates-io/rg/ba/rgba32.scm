(define-module (crates-io rg ba rgba32) #:use-module (crates-io))

(define-public crate-rgba32-0.1.0 (c (n "rgba32") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "13k69vz5aqy2qkizd2y1lcqkwnyvj2gi2p6q0g6yd167lypdpds1") (f (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1.1 (c (n "rgba32") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0npj68x17ds76vgmls0cvs0wygjln9b8g0zlsrj4aaa6bhbsr7gr") (f (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1.2 (c (n "rgba32") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1ac3s3057qijj38yspj7d24cdh0b2i8mf7caann9nh7b10m9kqnc") (f (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1.3 (c (n "rgba32") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1f9idspq3mn87v451c8ds3bd6b9yp8s39sbiq5zrl0lb5kdr5rrn") (f (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1.4 (c (n "rgba32") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0karfwil7s2xask342kg2wcnws8d8m0l0x05xlaina1xjrv0hsn8") (f (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1.5 (c (n "rgba32") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "17ymjgzzycvpyqyw8ks1g103wxnmi9r5zam2299fvhimspss58dh") (f (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1.6 (c (n "rgba32") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0xdz1dizqzvvihbvbps1r7gsngmh8mc9n0k3h2p1cr4rvxmc071n") (f (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1.7 (c (n "rgba32") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0ng4k0sa10dii2pdzmhxhr4ys7yq4147zaf94v6pjxm3qh72qrg6") (f (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1.8 (c (n "rgba32") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "12ylgyk25fzvwg7r8344zqhfjhm46vi75zpk5vw237nb8684iam0") (f (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1.9 (c (n "rgba32") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0qxz62vlgq7r1zkrw2yv83naj138hy8497jnjd4y49f20hpklf8s") (f (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1.10 (c (n "rgba32") (v "0.1.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0qxkmkkyq5hq3yq9zgbn7yg82w11m2p11lga71m8b7gfnfpbnlbg") (f (quote (("serialize" "serde"))))))

