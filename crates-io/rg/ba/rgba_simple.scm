(define-module (crates-io rg ba rgba_simple) #:use-module (crates-io))

(define-public crate-rgba_simple-0.3.2 (c (n "rgba_simple") (v "0.3.2") (d (list (d (n "gtk") (r "~0.3") (o #t) (d #t) (k 0) (p "gtk4")) (d (n "hex") (r "~0.4") (f (quote ("std"))) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gnawvx7402zhqczp7rz0grkmlzi6y82qbgdb05kndrxirh4acmj")))

(define-public crate-rgba_simple-0.3.3 (c (n "rgba_simple") (v "0.3.3") (d (list (d (n "gtk") (r "~0.3") (o #t) (d #t) (k 0) (p "gtk4")) (d (n "hex") (r "~0.4") (f (quote ("std"))) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "101lczqxgc6ql9p9fwvqs0n6ymaakain12mrn0a9wag9jjdm12g2")))

(define-public crate-rgba_simple-0.4.0 (c (n "rgba_simple") (v "0.4.0") (d (list (d (n "gdk") (r "~0.3") (o #t) (d #t) (k 0) (p "gdk4")) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jag10sbz7ji3fb3gik493a285j051vcvdh5kq8a9yavs6qy5ybn")))

(define-public crate-rgba_simple-0.4.1 (c (n "rgba_simple") (v "0.4.1") (d (list (d (n "gdk") (r "~0.3") (o #t) (d #t) (k 0) (p "gdk4")) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15w0b7rgylzcsja98j9dd22by9sbyk0pcb2gqbrzc7g2sy1h8awm")))

(define-public crate-rgba_simple-0.5.0 (c (n "rgba_simple") (v "0.5.0") (d (list (d (n "gdk") (r "~0.4") (o #t) (d #t) (k 0) (p "gdk4")) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cnnrx7k68fq7acgyqcyy7ldwm2yahk18rjxai8z3h7aga5fcjs2")))

(define-public crate-rgba_simple-0.5.1 (c (n "rgba_simple") (v "0.5.1") (d (list (d (n "gdk") (r "~0.4") (o #t) (d #t) (k 0) (p "gdk4")) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "172xfjdi7dikkzv0jkhwm5ncn72p5qahwqbhm60dm6fjwamj57f0")))

(define-public crate-rgba_simple-0.6.0 (c (n "rgba_simple") (v "0.6.0") (d (list (d (n "gdk") (r "~0.4") (o #t) (d #t) (k 0) (p "gdk4")) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06jdjyvakflvwsiwvb9jw5qmj5019afhla77q77qy5jk2syybxlk")))

(define-public crate-rgba_simple-0.6.1 (c (n "rgba_simple") (v "0.6.1") (d (list (d (n "gdk") (r "~0.4") (o #t) (d #t) (k 0) (p "gdk4")) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y52i5hx1h636ambjpgwcqmgbvp479ajgp9bw4jj3y1xbg21mbx7")))

(define-public crate-rgba_simple-0.7.0 (c (n "rgba_simple") (v "0.7.0") (d (list (d (n "gdk") (r "~0.4") (o #t) (d #t) (k 0) (p "gdk4")) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j3gyl8mcd1j6vaxg8k9gw1n989fqv7q1lb5b08lb16p0am6z41q")))

(define-public crate-rgba_simple-0.8.0 (c (n "rgba_simple") (v "0.8.0") (d (list (d (n "gdk") (r "^0.5") (o #t) (d #t) (k 0) (p "gdk4")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "150x42j5silssd2xkfnll5sviiyvhmyjl9km8zjfidiappagrb8l")))

(define-public crate-rgba_simple-0.9.0 (c (n "rgba_simple") (v "0.9.0") (d (list (d (n "gdk") (r "^0.6") (o #t) (d #t) (k 0) (p "gdk4")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18slk64bhcv1y0dnvadfgswpa5lp6kmh7dja4i0klh1n7qqq73c1")))

(define-public crate-rgba_simple-0.10.0 (c (n "rgba_simple") (v "0.10.0") (d (list (d (n "gdk") (r "^0.7") (o #t) (d #t) (k 0) (p "gdk4")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12n55b1kh72p5xl6gai10ydppywjk3rhv439gw48a5vh4danbkg6")))

