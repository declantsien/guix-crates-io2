(define-module (crates-io rg ba rgba8888-to-rgb332) #:use-module (crates-io))

(define-public crate-rgba8888-to-rgb332-0.1.0 (c (n "rgba8888-to-rgb332") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)))) (h "1lhnz0ivqd9c5snvvh9gw6bh6rdy5snr19i9ssr90pa1wcj7jli1")))

