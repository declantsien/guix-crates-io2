(define-module (crates-io rg ms rgmsh) #:use-module (crates-io))

(define-public crate-rgmsh-0.1.0 (c (n "rgmsh") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "gmsh-sys") (r "^0.1") (d #t) (k 0)))) (h "1zh27hsy22s9nldv4khpzzsxkhnr7yihyrhrlcpqkprp1gma0z72")))

(define-public crate-rgmsh-0.1.1 (c (n "rgmsh") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "gmsh-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0d0v8wyirvljxn184vrs5kjpr805c383iiflbfcn65qxvggd5l32")))

(define-public crate-rgmsh-0.1.2 (c (n "rgmsh") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "gmsh-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0s657fn52n4953qqgk351gf55hgyf2fqdkz3n8xzhzmh5msf7fhg")))

