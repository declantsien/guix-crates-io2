(define-module (crates-io rg ra rgraph) #:use-module (crates-io))

(define-public crate-rgraph-0.1.0 (c (n "rgraph") (v "0.1.0") (d (list (d (n "dot") (r "^0.1.2") (d #t) (k 0)))) (h "0gcdfxsgh4rcdrn7cfxfwa9zl6l2f6hi195bw09d7vrmr3vzxrqr")))

(define-public crate-rgraph-0.1.1 (c (n "rgraph") (v "0.1.1") (d (list (d (n "dot") (r "^0.1.2") (d #t) (k 0)))) (h "0b7inl9yhlbs0g7jmpg65mgl43yy58bhwh2pgh6rgfs8np25cz56")))

(define-public crate-rgraph-0.2.1 (c (n "rgraph") (v "0.2.1") (d (list (d (n "dot") (r "^0.1.2") (d #t) (k 0)))) (h "16ifra182zcymrrjk89bf8inhnakb05lfwsxqx3phazhsx8gnypf")))

(define-public crate-rgraph-0.2.2 (c (n "rgraph") (v "0.2.2") (d (list (d (n "dot") (r "^0.1.4") (d #t) (k 0)))) (h "0pg0579sr51jhlmyypyh56629vyp6pxh36c31qj6nlcffx53pnm0")))

