(define-module (crates-io rg b_ rgb_derivation) #:use-module (crates-io))

(define-public crate-rgb_derivation-0.1.1 (c (n "rgb_derivation") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0aqyzv0jylcsj5m5gh8b3vk9z059lariwaxfz5bpbaw5jwfxkrjk")))

(define-public crate-rgb_derivation-0.1.2 (c (n "rgb_derivation") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0zfgmpdg4vkzsilahz8ks7i1r111r3mhlp4qa0hlv141vdpwv0bn")))

(define-public crate-rgb_derivation-0.2.0 (c (n "rgb_derivation") (v "0.2.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "17zy8ain97dkv3bs19p207w68cdhbkcmgq5shdfxadd8jvc3qk9m")))

