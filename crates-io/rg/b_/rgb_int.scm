(define-module (crates-io rg b_ rgb_int) #:use-module (crates-io))

(define-public crate-rgb_int-0.1.0 (c (n "rgb_int") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1zw76g327jdlw1wr76x0spp24ff2djrivaj1fbi217rih0s89pjz") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb_int-0.1.1 (c (n "rgb_int") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0d9hzkypzbyq0lrhrycpgshx2g1fv1bxy01qjxx8h96izg5jnzs0") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb_int-0.1.2 (c (n "rgb_int") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0y1hj9d4brw774iv4yfqd7fl6bdgg5kn2ws9j12gbjfz2sdx5fjz") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb_int-0.1.3 (c (n "rgb_int") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1wgi8jpx01i7ra96g22j7vxj96w4xdg48zrfxpyyhjqpqb8jljx7") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb_int-0.1.4 (c (n "rgb_int") (v "0.1.4") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1hfs7grg1p6cpa2j8l703lpp8h80vwx18598bfdpf8814f3v9r3f") (f (quote (("serialize" "serde"))))))

