(define-module (crates-io rg b_ rgb_hsv) #:use-module (crates-io))

(define-public crate-rgb_hsv-1.0.0 (c (n "rgb_hsv") (v "1.0.0") (h "1k254qwd5jjjacz3rqs3k56r5kybp4l7hmklrrjxhln8kacb7chv")))

(define-public crate-rgb_hsv-1.0.1 (c (n "rgb_hsv") (v "1.0.1") (h "1295ax7v1w5cshvwl7in7svz8hfbqmywngigl8qvrp10hwz15h7y")))

