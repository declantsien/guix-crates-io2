(define-module (crates-io rg z_ rgz_sim) #:use-module (crates-io))

(define-public crate-rgz_sim-0.1.0 (c (n "rgz_sim") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (f (quote ("dynamic_linking"))) (d #t) (k 0)) (d (n "bevy_debug_grid") (r "^0.2") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.21.0") (d #t) (k 0)) (d (n "bevy_panorbit_camera") (r "^0.7.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "rgz_msgs") (r "^0.1.0") (d #t) (k 0)) (d (n "rgz_transport") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1286ysrzq6jjlglvpizr2c3ks2f4ph3bqsirnda1ifb6hgsl56yd")))

