(define-module (crates-io rg z_ rgz_derive) #:use-module (crates-io))

(define-public crate-rgz_derive-0.1.0 (c (n "rgz_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "10xzn7nbin131cwakf2s9pg2chi6qhzia26wwcr5wnbwx408q1jl")))

