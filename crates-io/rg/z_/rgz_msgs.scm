(define-module (crates-io rg z_ rgz_msgs) #:use-module (crates-io))

(define-public crate-rgz_msgs-0.1.0 (c (n "rgz_msgs") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)) (d (n "rgz_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1bigmzy1p6jfblrw3sy6vjik2n30imajgsq198x2dsg3mgbwfkqw")))

