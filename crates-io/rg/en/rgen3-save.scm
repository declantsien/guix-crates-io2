(define-module (crates-io rg en rgen3-save) #:use-module (crates-io))

(define-public crate-rgen3-save-0.1.0 (c (n "rgen3-save") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rgen3-string") (r "^0.1.0") (d #t) (k 0)))) (h "16rbmyfx25vmxlf8dfqarj1ls9lih83j6yg596j4filpzz6jhk3c")))

