(define-module (crates-io rg en rgen) #:use-module (crates-io))

(define-public crate-rgen-0.5.0 (c (n "rgen") (v "0.5.0") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "signifix") (r "^0.9") (d #t) (k 0)))) (h "0brwhf1z1n6cilwy2k9p5slvvczll89a9lzch4jffjc6afi898i8")))

