(define-module (crates-io rg en rgend) #:use-module (crates-io))

(define-public crate-rgend-1.3.1 (c (n "rgend") (v "1.3.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0a96m0x0brh1na5yjv9f4jmiynz3jkpd9qr9xy8ci2h34yhcvd0r")))

