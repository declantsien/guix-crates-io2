(define-module (crates-io rg -c rg-chess) #:use-module (crates-io))

(define-public crate-rg-chess-0.2.0 (c (n "rg-chess") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)) (d (n "glam") (r "^0.21.3") (f (quote ("mint"))) (d #t) (k 0)) (d (n "grid") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0yxbiv4b1z4mkc1s44ysw26v5d744s1ksz9rxn0jxyw58g9b2dl3")))

(define-public crate-rg-chess-0.2.1 (c (n "rg-chess") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "12nf77774da7jbyj8micy0syajn30sd4xmxv5i7ll5zmpdhbxl6d")))

(define-public crate-rg-chess-0.2.2 (c (n "rg-chess") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "08k97bi1cnr5xg8padcbysqrd5k2w6klb205srlsiblijfcv6xmx")))

(define-public crate-rg-chess-0.2.3 (c (n "rg-chess") (v "0.2.3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "0ccj454qwk0yqhiikldy31ifcn2yaxf2ffsz93czr33p4mda9ym7") (y #t)))

(define-public crate-rg-chess-0.2.4 (c (n "rg-chess") (v "0.2.4") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "05a0vdpz3lnjs0nzbbqbpy0gbx0m6f2q1yx8xrrmws8wzk4ag992")))

