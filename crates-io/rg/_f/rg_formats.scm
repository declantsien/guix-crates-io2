(define-module (crates-io rg _f rg_formats) #:use-module (crates-io))

(define-public crate-rg_formats-0.1.0 (c (n "rg_formats") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1qa9mrch58dhlap6ifhwq6bxdqfpa7wjpfj01c9c3xd84kj4qbkf")))

(define-public crate-rg_formats-0.1.1 (c (n "rg_formats") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0wi6bwq2v2b7i78zaf257h8jhvrr45nvf2b7vd7bp9fifx2zapj3")))

(define-public crate-rg_formats-0.1.2 (c (n "rg_formats") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1ifx9l3k2r4wk0v12arfacnvv6wa1kcya5apa8zszd260wj03jxg")))

