(define-module (crates-io rg b2 rgb24) #:use-module (crates-io))

(define-public crate-rgb24-0.1.0 (c (n "rgb24") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "00wd1mnw9w0p4d3bdghsjz1yv9303h3fl8gp1ak2n9vj42gys16i") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.1.1 (c (n "rgb24") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0wd55dxav1sdjc5pvlazkna71zq38483sfnmac4qcsc92516310m") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.1.2 (c (n "rgb24") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1wn279pfm53c30rji05gjhxqa0yj0yy1mc2an5j035jxzblynbhq") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.1.3 (c (n "rgb24") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0w293kxpfwig1l6s4vlyjz7ilg84dlbfvjhdifhzl9wz5250dmm4") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.1.4 (c (n "rgb24") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0mri4abvzi6jib8fvg54pmglj2yky3l99y8pqrwh0zjgzhxx9ffd") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.1.5 (c (n "rgb24") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1qa4s2iaaqv79s0h1dkfdgh4c3mdi2f5gpblg7kj7s692yvf88v5") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.2.0 (c (n "rgb24") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0hy15r6pxyji7ik8rshgllbrv5hp2s6c4hrbfgw0fxnq3qz4m0yn") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.2.1 (c (n "rgb24") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1l0fqsxl5y6lmj6rx1nv65hdca9nz318bpqadyv4469dzl2iflm6") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.2.2 (c (n "rgb24") (v "0.2.2") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "13sa6w04i5f35prl2wgkmxwwc928rncz7xardj29srsn2al29y9l") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.2.3 (c (n "rgb24") (v "0.2.3") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0zy33hjmxn0aj8wkmpdsaf7dp1pw6zf6zsk102sk8x10dv8vqvrq") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.2.4 (c (n "rgb24") (v "0.2.4") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0l4b82sn3q2lq67hnyvlk15paahk48siz3cdczz0vcb95xsw7lpb") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.2.5 (c (n "rgb24") (v "0.2.5") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0rdbng0yy9cp2ya6w502f9dr09mz0j501jf2pbbvibsfp33d4p5d") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.2.6 (c (n "rgb24") (v "0.2.6") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0qzahlp5l68h84xlfha9hbs7svbwhc486c5lqnr4f9dn6r62f0pi") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.2.7 (c (n "rgb24") (v "0.2.7") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0fxsyzc9a5irxxga7iz5cqyrfs4gr4v3gh6mjz1dqdbsbvy3i78g") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.2.8 (c (n "rgb24") (v "0.2.8") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand_isaac") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0l0r935xbmndzi9anhfz9k13vrs90q7gh2qm2vq4aw0ygwsy2rsa") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.2.9 (c (n "rgb24") (v "0.2.9") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0jwmx2ri8xcr2f832235yxrzig7pv437zn8h52ngnsv6a93g0r4v") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.3.0 (c (n "rgb24") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand_isaac") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "17shwrh6d50cjw0i18jcdd3ndb6jnxyg33qw7cfa14gp1iqhhm00") (f (quote (("serialize" "serde"))))))

(define-public crate-rgb24-0.3.1 (c (n "rgb24") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand_isaac") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "06fgw0vzl443c7gz7l7js6l08bx1in652k8g2npba4galv457h55") (f (quote (("serialize" "serde"))))))

