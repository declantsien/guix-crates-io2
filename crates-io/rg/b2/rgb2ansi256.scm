(define-module (crates-io rg b2 rgb2ansi256) #:use-module (crates-io))

(define-public crate-rgb2ansi256-0.1.0 (c (n "rgb2ansi256") (v "0.1.0") (d (list (d (n "ansi_colours") (r "=1.0.4") (d #t) (k 2)))) (h "0vpfp8w25mpg2y05piv3l118a7h3dwjzj2rqg3v3ggq9inirxpsp")))

(define-public crate-rgb2ansi256-0.1.1 (c (n "rgb2ansi256") (v "0.1.1") (d (list (d (n "ansi_colours") (r "=1.0.4") (d #t) (k 2)))) (h "1q8lk3pzfkpsf7gvjyvafmxbkdxmp9480jch2x9jv4853imskg0y")))

