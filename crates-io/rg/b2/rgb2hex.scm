(define-module (crates-io rg b2 rgb2hex) #:use-module (crates-io))

(define-public crate-rgb2hex-0.1.0 (c (n "rgb2hex") (v "0.1.0") (h "17v7csx2x6y4fq8dkaxhf93kj2f8kjh69sw3479pw2ivl4y7fa70")))

(define-public crate-rgb2hex-0.2.0 (c (n "rgb2hex") (v "0.2.0") (h "0mcrf9pxqg3hymdq8alpzq2l83vwp1ph927aygjzns8qa6j55ghb")))

(define-public crate-rgb2hex-0.3.0 (c (n "rgb2hex") (v "0.3.0") (h "1jb3mg5iwxdxbvg17gvw8p310ggr40xi6gzzq4gf9bwl7jnq0pic")))

