(define-module (crates-io rg b2 rgb22) #:use-module (crates-io))

(define-public crate-rgb22-0.3.0-beta.3 (c (n "rgb22") (v "0.3.0-beta.3") (d (list (d (n "amplify") (r "^3") (d #t) (k 0)) (d (n "amplify_derive") (r "^2.4.3") (d #t) (k 0)) (d (n "bitcoin") (r "^0.26") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lnpbp") (r "^0.3") (d #t) (k 0)) (d (n "rgb-core") (r "^0.3.0-beta.3") (d #t) (k 0)) (d (n "serde_crate") (r "~1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_with") (r "~1.5.1") (f (quote ("hex"))) (o #t) (d #t) (k 0)))) (h "0mk9ajgpr98r5x8sxhvllg5qvblbxrdwfm00czv9yifnh631ifka")))

