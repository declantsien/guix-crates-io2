(define-module (crates-io rg b2 rgb2spec) #:use-module (crates-io))

(define-public crate-rgb2spec-0.1.0 (c (n "rgb2spec") (v "0.1.0") (h "1ipwf95ffi6v8siaa5k6w29319pgbhclls56l3n6rp831b459df6")))

(define-public crate-rgb2spec-0.1.1 (c (n "rgb2spec") (v "0.1.1") (h "1w5aal8caiv8n8lf3jxz9mzgaqk0scg8rjarnha9acjzh1pgrbsq")))

