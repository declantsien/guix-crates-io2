(define-module (crates-io rg pa rgparse) #:use-module (crates-io))

(define-public crate-rgparse-0.1.0 (c (n "rgparse") (v "0.1.0") (h "0znni1zr08xvks7fnn3i37yhav0wpar2rnc2360fag3x8lmjpng5")))

(define-public crate-rgparse-0.1.1 (c (n "rgparse") (v "0.1.1") (h "14fysbig5a3x98ccxs8j179kgaij6vg86qrdgk09nmdd44g0swmb")))

(define-public crate-rgparse-0.1.2 (c (n "rgparse") (v "0.1.2") (h "1a0m3q1fxhl4igi7p29j5b94n79aq0svfgqljy1ikhzg346lzjbg")))

