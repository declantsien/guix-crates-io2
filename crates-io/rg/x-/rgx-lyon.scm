(define-module (crates-io rg x- rgx-lyon) #:use-module (crates-io))

(define-public crate-rgx-lyon-0.1.0 (c (n "rgx-lyon") (v "0.1.0") (d (list (d (n "lyon") (r "^0.16") (d #t) (k 0)) (d (n "rgx") (r "^0.8") (f (quote ("renderer"))) (d #t) (k 0)) (d (n "winit") (r "^0.22") (d #t) (k 2)))) (h "1ss8arxvbsnxmi8k958dkf9fbq4zg9gmw61j3s4hc54hv7v2jd5n")))

(define-public crate-rgx-lyon-0.1.1 (c (n "rgx-lyon") (v "0.1.1") (d (list (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "rgx") (r "^0.8") (f (quote ("renderer"))) (d #t) (k 0)) (d (n "winit") (r "^0.22") (d #t) (k 2)))) (h "14kafrxndhndj3ik5f8lab1v9yk6lppiis97prp1ykb7vnc3ry9y")))

(define-public crate-rgx-lyon-0.1.2 (c (n "rgx-lyon") (v "0.1.2") (d (list (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "rgx") (r "^0.8") (f (quote ("renderer"))) (d #t) (k 0)) (d (n "winit") (r "^0.22") (d #t) (k 2)))) (h "1az6w6fvlbgilv5dyk93rd3hqb1wzl5f8xwm8dhkp7z8bwpcciz3")))

