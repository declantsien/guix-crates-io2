(define-module (crates-io rg ch rgch) #:use-module (crates-io))

(define-public crate-rgch-0.1.27 (c (n "rgch") (v "0.1.27") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0g5npswlxaxi0ga93wxqi3zyq69icrldzrwdmzfzd5b28izvbxw4")))

(define-public crate-rgch-0.1.28 (c (n "rgch") (v "0.1.28") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0903cmvyxsc4jbsjqkmbfmqwmrlj7lsqc1175zn52gy8x9sg77cg")))

(define-public crate-rgch-0.1.29 (c (n "rgch") (v "0.1.29") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "105cq032qrrwh718sfhrlr823hqwp1f25g1hynmsqv7xiqbgvhy0")))

(define-public crate-rgch-0.1.30 (c (n "rgch") (v "0.1.30") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1p3mgfv08ygd5p9m34vhjf4ypyjpfmrrl5s232mamidn6vlca9nh")))

(define-public crate-rgch-0.1.31 (c (n "rgch") (v "0.1.31") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1s6jq507wmiavk62a3kcnh43liszzac8w2gjs01s923p2kkz8x5m")))

(define-public crate-rgch-0.1.32 (c (n "rgch") (v "0.1.32") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "11gi0j8yar25n5qrykppfjlkbckrmp22g99qr16g5hxs86v6iv51")))

(define-public crate-rgch-0.1.33 (c (n "rgch") (v "0.1.33") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0wl5l97yad11swq582ddn7wpfdxqa5m06kzljhj41m0r5619j5pl")))

