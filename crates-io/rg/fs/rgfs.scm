(define-module (crates-io rg fs rgfs) #:use-module (crates-io))

(define-public crate-rgfs-0.1.0 (c (n "rgfs") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("fs" "macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1kc01chigw9qwajdv4pc3zydlq037nwqrm2prr2h5ihim9sw2isz")))

