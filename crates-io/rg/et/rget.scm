(define-module (crates-io rg et rget) #:use-module (crates-io))

(define-public crate-rget-0.1.0 (c (n "rget") (v "0.1.0") (d (list (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "number_prefix") (r "^0.2.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.1.0") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)) (d (n "term_size") (r "^0.2.1") (d #t) (k 0)))) (h "01zvxp5nnrksmifv8cpvl312k53gm5lydx2vbwbjfn8idaikdv01")))

(define-public crate-rget-0.2.0 (c (n "rget") (v "0.2.0") (d (list (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)) (d (n "toml") (r "^0.2.1") (d #t) (k 0)))) (h "1hxfgza3xg4z4l344jrzhgl4pqfv18kd25ky29hm1cxs0nfkzar3")))

(define-public crate-rget-0.2.1 (c (n "rget") (v "0.2.1") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0kk6yjhnrq0nhims1b6hbkr7kyr54k9x99f7fzsli7kn8k21glwm")))

(define-public crate-rget-0.3.0 (c (n "rget") (v "0.3.0") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1z3s9ghz2srqvgmvjas8l2mzdx3rxwv2mv7l4iyzvqv8aldaavy4")))

(define-public crate-rget-0.3.1 (c (n "rget") (v "0.3.1") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1s9jd8clldhr04yx7l1mgsd2ln804w9jx9p92mmwd1pwb8jvjxyq")))

