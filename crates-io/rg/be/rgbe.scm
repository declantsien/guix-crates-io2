(define-module (crates-io rg be rgbe) #:use-module (crates-io))

(define-public crate-rgbe-0.0.1 (c (n "rgbe") (v "0.0.1") (d (list (d (n "bytemuck") (r "^1.14.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "half") (r "^2.3.1") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (f (quote ("png" "hdr"))) (k 0)))) (h "1sd4b4p6c6jnwq588bj6vszywkwsgz0j71bv20z1f7aami2g9s1d")))

