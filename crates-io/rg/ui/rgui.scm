(define-module (crates-io rg ui rgui) #:use-module (crates-io))

(define-public crate-rgui-0.1.0 (c (n "rgui") (v "0.1.0") (d (list (d (n "druid") (r "^0.7.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "err-derive") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "10j2pzidylsp3vakj13wv1df3sqfa5s83c13lvdynlr5gaiywccn")))

