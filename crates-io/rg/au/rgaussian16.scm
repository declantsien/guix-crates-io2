(define-module (crates-io rg au rgaussian16) #:use-module (crates-io))

(define-public crate-rgaussian16-0.1.0 (c (n "rgaussian16") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "0xrs52g7h8zrlwqf76988wmj1mmzj0nn5cbl6z3bw35qcj3svvga") (y #t)))

(define-public crate-rgaussian16-0.1.1 (c (n "rgaussian16") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "1v2q783yfx0ahb192l7nfvav03pwzlj4aaqy7mcfvq98vc7glym9") (y #t)))

(define-public crate-rgaussian16-0.1.2 (c (n "rgaussian16") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "04lhs40ibq9xmqa62h198gvj8pa81f9l9sypjp1lmv6jbnvmd74j") (y #t)))

(define-public crate-rgaussian16-0.1.3 (c (n "rgaussian16") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "0zmc7jgp97qxbfdklwshn985ngp7c9hwkwj66k391anl8bkxw9qy")))

