(define-module (crates-io rg lu rglua-macros) #:use-module (crates-io))

(define-public crate-rglua-macros-0.1.0 (c (n "rglua-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1a9km8pw72hqgx95g502iizd1pmbvzd6rf6g0xdg7758p8y0y9fk")))

(define-public crate-rglua-macros-0.2.0 (c (n "rglua-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0phn25zrl1br9ykab6bsbz1sjbzd1j3avlmq5fk7fi9mx93cgwvl")))

