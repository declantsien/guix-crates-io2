(define-module (crates-io rg s_ rgs_models) #:use-module (crates-io))

(define-public crate-rgs_models-0.1.0 (c (n "rgs_models") (v "0.1.0") (d (list (d (n "iso_country") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1d90c29picrdavvddfdr86yy89x2w5hj7lb1ljbiws7da1c662qr") (y #t)))

(define-public crate-rgs_models-0.1.1 (c (n "rgs_models") (v "0.1.1") (d (list (d (n "iso_country") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "19jz0fnh4zp2zv7p637ni1a0nv64kch50apg20mh1qdgiig6wamh") (y #t)))

(define-public crate-rgs_models-0.2.0 (c (n "rgs_models") (v "0.2.0") (d (list (d (n "iso_country") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1g1d7n7xfv7xi1hgzlkbryyy5hx7hxdd9z0vv47kbi0wmrxx5kcd") (y #t)))

(define-public crate-rgs_models-0.3.0 (c (n "rgs_models") (v "0.3.0") (d (list (d (n "iso_country") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0gizsqbcj6pvgj0cs736jnsz9130nnxhhfb8105fzwrnq92rir2m") (y #t)))

(define-public crate-rgs_models-0.4.0 (c (n "rgs_models") (v "0.4.0") (d (list (d (n "iso_country") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0mxprlyhzmzad87vp2kcnsi9cy5fdsdgdiacxwkw4c0f1lfz8isd") (y #t)))

(define-public crate-rgs_models-0.5.0 (c (n "rgs_models") (v "0.5.0") (d (list (d (n "iso_country") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0z245yv7q6kl0gf8d3cr2xnv4bm65rna34rj4nw2bznmvbmj75g5")))

(define-public crate-rgs_models-0.6.0 (c (n "rgs_models") (v "0.6.0") (d (list (d (n "iso_country") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0zzd7vx20835bz76rjpk4ga97dbf51x07rbzrjd3y8wvj3hy7v6n")))

(define-public crate-rgs_models-0.7.0 (c (n "rgs_models") (v "0.7.0") (d (list (d (n "iso_country") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1z2fv6j9ar9p4ipq7dbj8p84yx292mzphqnkdprcw1nvhs095xjc")))

(define-public crate-rgs_models-0.8.0 (c (n "rgs_models") (v "0.8.0") (d (list (d (n "iso_country") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "10k4g4qp353hyyx4fz1wysmcba7jipfcg4q870gp85zb3qlp694g")))

(define-public crate-rgs_models-0.9.0 (c (n "rgs_models") (v "0.9.0") (d (list (d (n "iso_country") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "058374s5a2093hszlmai9x0msbflq117a99zk8c6dxg5p3s8mdgj")))

(define-public crate-rgs_models-0.9.1 (c (n "rgs_models") (v "0.9.1") (d (list (d (n "iso_country") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18z44wmsxndm65yfpynzhf4yr8c4g509lrxfkfx5n82vsy968i1q")))

