(define-module (crates-io rg ub rgubin) #:use-module (crates-io))

(define-public crate-rgubin-0.1.0 (c (n "rgubin") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "jlogger") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0g4rsb46c04lxg5hmiq9b2qzfi5airxfzw2q32pm11sq8ylg4z2l")))

(define-public crate-rgubin-0.1.1 (c (n "rgubin") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "jlogger") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "177z6nf55xcmw3i3m9d0lxgd5zsb1lkpy0rkxmhm59car524055r")))

