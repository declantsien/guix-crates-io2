(define-module (crates-io rg bo rgbobj) #:use-module (crates-io))

(define-public crate-rgbobj-0.1.0 (c (n "rgbobj") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("std" "suggestions" "color" "wrap_help"))) (k 0)) (d (n "rgbds-obj") (r "^0.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "07hvqkv9fdvgc7cmys82picr4zp3m6i3l54nrg9qsfky95l5p62i")))

(define-public crate-rgbobj-0.2.0 (c (n "rgbobj") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("cargo" "wrap_help"))) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rgbds-obj") (r "^0.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0s77zcin50c5fn0zvxii1lc8r0wa6lsq81lp3f4yywmh8sayrqqb")))

(define-public crate-rgbobj-0.2.1 (c (n "rgbobj") (v "0.2.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("cargo" "wrap_help"))) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rgbds-obj") (r "^0.1.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0vgxi88rd4a2lkqnydzhv982n10rs1ajqnpa7058wm3im30qn93d")))

