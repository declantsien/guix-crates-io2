(define-module (crates-io rg oa rgoap) #:use-module (crates-io))

(define-public crate-rgoap-0.1.0 (c (n "rgoap") (v "0.1.0") (d (list (d (n "pathfinding") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)))) (h "0cx07smdzrnghb0mnnaw3v8jcbhf331s1718nh8y8bbnf9jkk9sf")))

