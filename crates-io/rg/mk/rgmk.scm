(define-module (crates-io rg mk rgmk) #:use-module (crates-io))

(define-public crate-rgmk-0.1.0 (c (n "rgmk") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)) (d (n "log") (r "^0.3.3") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "quick-error") (r "^0.1.4") (d #t) (k 0)))) (h "0psnb88bnh4kpnpy3yzwx94vd0z5kc3sqsr8qq8i24ckpwvgm4qh")))

(define-public crate-rgmk-0.2.0 (c (n "rgmk") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)) (d (n "log") (r "^0.3.3") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "quick-error") (r "^0.1.4") (d #t) (k 0)))) (h "18296d4r796fnf81ws729lb1n03ywdyirayn9423ksq6cv2l2syl")))

(define-public crate-rgmk-0.3.0 (c (n "rgmk") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "11g96gkfnvld2id7swc3g7hcmqzh7s82rvdw1hvrj6m617gls64k")))

