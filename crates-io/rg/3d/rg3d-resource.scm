(define-module (crates-io rg #{3d}# rg3d-resource) #:use-module (crates-io))

(define-public crate-rg3d-resource-0.1.0 (c (n "rg3d-resource") (v "0.1.0") (d (list (d (n "rg3d-core") (r "^0.17") (d #t) (k 0)))) (h "0z1pcfhb6r7hl42p7jqk4lzh6qb0ny7sp1ziz4pmrm2sc18sksj8")))

(define-public crate-rg3d-resource-0.1.1 (c (n "rg3d-resource") (v "0.1.1") (d (list (d (n "rg3d-core") (r "^0.17.1") (d #t) (k 0)))) (h "1hgcrvvd7ndqf5mk78a0fnsg6x7yibmlz2ij3f20b2lwkbpqg7w1")))

(define-public crate-rg3d-resource-0.2.0 (c (n "rg3d-resource") (v "0.2.0") (d (list (d (n "rg3d-core") (r "^0.18.0") (d #t) (k 0)))) (h "0dxn5cfdvvaam8845cx4qrmsdzg6r762bwyhamma5gg9h16f1ais")))

(define-public crate-rg3d-resource-0.3.0 (c (n "rg3d-resource") (v "0.3.0") (d (list (d (n "rg3d-core") (r "^0.19.0") (d #t) (k 0)))) (h "1gkyk3iyamzspwvasykl228xcg1z1pzjxrwpvm1lk24b03a8psar") (r "1.56")))

