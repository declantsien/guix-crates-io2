(define-module (crates-io rg #{3d}# rg3d-core-derive) #:use-module (crates-io))

(define-public crate-rg3d-core-derive-0.11.0 (c (n "rg3d-core-derive") (v "0.11.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "darling") (r "^0.12.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (d #t) (k 0)))) (h "1pv6ykfsbk3svzj08mhkz9fvc4nds2amnwnakfw3k9dy47zs2xd9")))

(define-public crate-rg3d-core-derive-0.12.0 (c (n "rg3d-core-derive") (v "0.12.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (d #t) (k 0)))) (h "0y79g1ajlb0mhyx70qsn34lry7ccwibfp38dx93ll9xspv9lx7c4")))

(define-public crate-rg3d-core-derive-0.13.0 (c (n "rg3d-core-derive") (v "0.13.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "1anj889311x3vzian81d0g3bl6b9yfjv2llivlvapxb65j695pra")))

(define-public crate-rg3d-core-derive-0.14.0 (c (n "rg3d-core-derive") (v "0.14.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "00q1ydq1l246wwqzlbbylq0jacg962ib97y66nmxbrmhz88ixmdy") (r "1.56")))

