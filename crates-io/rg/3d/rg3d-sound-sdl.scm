(define-module (crates-io rg #{3d}# rg3d-sound-sdl) #:use-module (crates-io))

(define-public crate-rg3d-sound-sdl-0.1.0 (c (n "rg3d-sound-sdl") (v "0.1.0") (d (list (d (n "rg3d-sound") (r "^0.26.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "077900rrjz5ch4gkibsdmjs2vli69ja5knf9yq8vnzxq1s13i2zr")))

(define-public crate-rg3d-sound-sdl-0.1.1 (c (n "rg3d-sound-sdl") (v "0.1.1") (d (list (d (n "rg3d-sound") (r "^0.26.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1d2j80zfaszmka9sh63xr52qamax6bx2myxifn53w9409nsy3w5y")))

