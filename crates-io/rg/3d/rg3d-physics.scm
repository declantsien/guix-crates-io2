(define-module (crates-io rg #{3d}# rg3d-physics) #:use-module (crates-io))

(define-public crate-rg3d-physics-0.1.7 (c (n "rg3d-physics") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.1.9") (d #t) (k 0)))) (h "0zffq2jfmssqbl61bzp2yfqnsbvlxwsa0n2nnb71cv5hq5mmvsr1")))

(define-public crate-rg3d-physics-0.1.8 (c (n "rg3d-physics") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.2.0") (d #t) (k 0)))) (h "1xdfqhyaq4v4fa0jfh3i63jdm8slv3k1d19j6f08dzc1f2f67dzl")))

(define-public crate-rg3d-physics-0.2.0 (c (n "rg3d-physics") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.2.1") (d #t) (k 0)))) (h "1imzpjhb6gzc3201f42q4qigk2k61ys2z2p8wnjjgswhhzjjavxq")))

(define-public crate-rg3d-physics-0.2.1 (c (n "rg3d-physics") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.4.0") (d #t) (k 0)))) (h "1vd66c5685y4xkg2v0iq71v55nncp5p2b229w6h6pd94s781mmmn")))

(define-public crate-rg3d-physics-0.3.0 (c (n "rg3d-physics") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.5.0") (d #t) (k 0)))) (h "04wf49z7240rnjg2bqrk0shqxhz0m0afcp5nnlccfvdg9j2zp0rz")))

(define-public crate-rg3d-physics-0.4.0 (c (n "rg3d-physics") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.6.0") (d #t) (k 0)))) (h "0f1ggzafs5qbwr2qs4bmn4js9bw9klraf80w0s1s5g2rvv3bmgr6")))

(define-public crate-rg3d-physics-0.5.0 (c (n "rg3d-physics") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.7.0") (d #t) (k 0)))) (h "1h9alddflfbs9cg061d9z39hsw2fjfa88x0mxk5mn7gnq900k6jv")))

(define-public crate-rg3d-physics-0.6.0 (c (n "rg3d-physics") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.8.0") (d #t) (k 0)))) (h "1zb4swciighjvdwmrrza2zcvjn9b1lyy1hm5xvm6ir768d5i0x03") (f (quote (("enable_profiler" "rg3d-core/enable_profiler"))))))

(define-public crate-rg3d-physics-0.7.0 (c (n "rg3d-physics") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.9.0") (d #t) (k 0)))) (h "1xa6yv610rzkd9009w0rb78kc6cs34hqqchhfi4wqvnps4i7vla5") (f (quote (("enable_profiler" "rg3d-core/enable_profiler"))))))

(define-public crate-rg3d-physics-0.8.0 (c (n "rg3d-physics") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.10.0") (d #t) (k 0)))) (h "0l8hic5ya5waymgsqfyz1avnp93sg3ccsm2wils2zavbawfx90hf") (f (quote (("enable_profiler" "rg3d-core/enable_profiler"))))))

