(define-module (crates-io rg b- rgb-color) #:use-module (crates-io))

(define-public crate-rgb-color-0.1.0 (c (n "rgb-color") (v "0.1.0") (h "1311jah86j6c2rdqqjyvzxi40qi7wm08vjpknpz0838x9f11vsc2")))

(define-public crate-rgb-color-0.1.1 (c (n "rgb-color") (v "0.1.1") (h "0s5njyp3sf0ynyqn557hwxhk6gfr88ldmg9a61ps0g9yrmwp3nyl")))

