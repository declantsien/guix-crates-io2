(define-module (crates-io rg b- rgb-lib-migration) #:use-module (crates-io))

(define-public crate-rgb-lib-migration-0.1.3 (c (n "rgb-lib-migration") (v "0.1.3") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.8.0") (f (quote ("sqlx-mysql" "sqlx-postgres" "sqlx-sqlite" "runtime-async-std-native-tls"))) (d #t) (k 0)))) (h "1yf0blz0m9jsamslwfi4yq9p2q8hll48ngw8lh39vh4idcsff07d") (r "1.59.0")))

(define-public crate-rgb-lib-migration-0.1.4 (c (n "rgb-lib-migration") (v "0.1.4") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.8.0") (f (quote ("sqlx-mysql" "sqlx-postgres" "sqlx-sqlite" "runtime-async-std-native-tls"))) (d #t) (k 0)))) (h "0d2vxmd2zslh8xwmm8fn4za8hjccx7wdnzs6hr6y5cr5ra97qdny") (r "1.59.0")))

(define-public crate-rgb-lib-migration-0.1.6 (c (n "rgb-lib-migration") (v "0.1.6") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.10.6") (f (quote ("sqlx-mysql" "sqlx-postgres" "sqlx-sqlite" "runtime-async-std-native-tls"))) (d #t) (k 0)))) (h "13840mnx68q7chd4px1yvqjl3rh04mbx512nr90klr4b6rdk92a4") (r "1.59.0")))

(define-public crate-rgb-lib-migration-0.1.7 (c (n "rgb-lib-migration") (v "0.1.7") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.10.6") (f (quote ("sqlx-mysql" "sqlx-postgres" "sqlx-sqlite" "runtime-async-std-native-tls"))) (d #t) (k 0)))) (h "1gzpz695dici8xy0yyll3d35lzhzrsz96m1685lg8mdwxy3l10yr") (r "1.59.0")))

(define-public crate-rgb-lib-migration-0.1.8 (c (n "rgb-lib-migration") (v "0.1.8") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.11.3") (f (quote ("sqlx-mysql" "sqlx-postgres" "sqlx-sqlite" "runtime-async-std-native-tls"))) (d #t) (k 0)))) (h "0w1j2jr0ps7ngh2mx4x3si8qrqs6y5jfsrdf63ixprb0gpsq3xjj") (r "1.59.0")))

(define-public crate-rgb-lib-migration-0.2.0-alpha.1 (c (n "rgb-lib-migration") (v "0.2.0-alpha.1") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.11.3") (f (quote ("sqlx-mysql" "sqlx-postgres" "sqlx-sqlite" "runtime-async-std-native-tls"))) (d #t) (k 0)))) (h "10rkf9hs0nwfan56i15z4iqj4ljz553s149n11msshx0ddzrfkmy") (r "1.67")))

(define-public crate-rgb-lib-migration-0.2.0-alpha.2 (c (n "rgb-lib-migration") (v "0.2.0-alpha.2") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.11.3") (f (quote ("sqlx-mysql" "sqlx-postgres" "sqlx-sqlite" "runtime-async-std-native-tls"))) (d #t) (k 0)))) (h "04vjaq1azcfvy382wdaial1r6bqwi47x94pfwmgb02wg7gxi57h0") (r "1.67")))

(define-public crate-rgb-lib-migration-0.2.0-alpha.3 (c (n "rgb-lib-migration") (v "0.2.0-alpha.3") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.12.2") (f (quote ("sqlx-mysql" "sqlx-postgres" "sqlx-sqlite" "runtime-async-std-native-tls"))) (d #t) (k 0)))) (h "1zjh45pk62c4f31gnans7c2kbjqjaalymwgb799jqw0mf671fyzl") (r "1.67")))

(define-public crate-rgb-lib-migration-0.2.0-alpha.5 (c (n "rgb-lib-migration") (v "0.2.0-alpha.5") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.12.2") (f (quote ("sqlx-mysql" "sqlx-postgres" "sqlx-sqlite" "runtime-async-std-native-tls"))) (d #t) (k 0)))) (h "0sphpfhvhzckcnwbp0xi7wsc6vpqr49kmj5ydb75bakp8gfcyb27") (r "1.67")))

(define-public crate-rgb-lib-migration-0.2.0-alpha.6 (c (n "rgb-lib-migration") (v "0.2.0-alpha.6") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.12.3") (f (quote ("sqlx-mysql" "sqlx-postgres" "sqlx-sqlite" "runtime-async-std-native-tls"))) (d #t) (k 0)))) (h "1kjahqlpwdfgb1rryh1vjfrlbd8rmiwmwjyix1zdx1hjdnmxlqqm") (r "1.67")))

(define-public crate-rgb-lib-migration-0.2.0 (c (n "rgb-lib-migration") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "=0.12.4") (f (quote ("sqlx-mysql" "sqlx-postgres" "sqlx-sqlite" "runtime-async-std-native-tls"))) (d #t) (k 0)))) (h "0j5ayz7c0barigq8a4rax994rgy87zhi7l9mi7qb8xj6xngks4c8") (r "1.67")))

(define-public crate-rgb-lib-migration-0.3.0-alpha.1 (c (n "rgb-lib-migration") (v "0.3.0-alpha.1") (d (list (d (n "async-std") (r "^1.12") (f (quote ("attributes" "tokio1"))) (k 0)) (d (n "sea-orm-migration") (r "=0.12.15") (f (quote ("cli" "runtime-async-std-native-tls" "sqlx-mysql" "sqlx-postgres" "sqlx-sqlite"))) (k 0)))) (h "134a0l4ijyvk6pqrp90szdrd833cfki49dh0cgzk19psh5s03yx1") (r "1.67")))

