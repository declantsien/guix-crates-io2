(define-module (crates-io rg b- rgb-stl) #:use-module (crates-io))

(define-public crate-rgb-stl-0.11.0-beta.2 (c (n "rgb-stl") (v "0.11.0-beta.2") (d (list (d (n "rgb-std") (r "^0.11.0-beta.2") (d #t) (k 0)) (d (n "strict_types") (r "^1.6.3") (d #t) (k 0)))) (h "07ykcmvc3g5isqvbsidw9q0m4y68lk1i8y5kaxiahhq5vx7vaqbv") (r "1.67")))

