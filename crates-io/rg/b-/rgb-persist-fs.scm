(define-module (crates-io rg b- rgb-persist-fs) #:use-module (crates-io))

(define-public crate-rgb-persist-fs-0.10.0-alpha.1 (c (n "rgb-persist-fs") (v "0.10.0-alpha.1") (d (list (d (n "amplify") (r "^4.0.0-beta.19") (d #t) (k 0)) (d (n "rgb-std") (r "^0.10.0-alpha.1") (f (quote ("fs"))) (d #t) (k 0)) (d (n "strict_encoding") (r "^2.0.0-rc.3") (d #t) (k 0)))) (h "14cpwa6kfzyhm636lk9kdznshi1amziys1hvsyc9yks6hrlkrpfy") (f (quote (("default")))) (r "1.66")))

(define-public crate-rgb-persist-fs-0.10.0-rc.1 (c (n "rgb-persist-fs") (v "0.10.0-rc.1") (d (list (d (n "amplify") (r "^4.0.0-beta.20") (d #t) (k 0)) (d (n "rgb-std") (r "^0.10.0-rc.1") (f (quote ("fs"))) (d #t) (k 0)) (d (n "strict_encoding") (r "^2.0.0") (d #t) (k 0)))) (h "07z31xw6z9szz5y7qm11hww1xjp77x7glj69bscvrqa03gilgkf9") (f (quote (("default")))) (r "1.66")))

(define-public crate-rgb-persist-fs-0.10.0 (c (n "rgb-persist-fs") (v "0.10.0") (d (list (d (n "amplify") (r "^4.0.0") (d #t) (k 0)) (d (n "rgb-std") (r "^0.10.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "strict_encoding") (r "^2.0.1") (d #t) (k 0)))) (h "0jn6635zb9pfvmpci43d9bj4p5id44r501s6zk45if3ya62xwmix") (f (quote (("default")))) (r "1.66")))

(define-public crate-rgb-persist-fs-0.11.0 (c (n "rgb-persist-fs") (v "0.11.0") (d (list (d (n "amplify") (r "^4.5.0") (d #t) (k 0)) (d (n "rgb-std") (r "^0.11.0-beta.1") (f (quote ("fs"))) (d #t) (k 0)) (d (n "strict_encoding") (r "^2.6.1") (d #t) (k 0)))) (h "14zryzmdg4crrcyvgm1p9wha6p4c3h7s05k7nrjyiswrnqfap7xx") (f (quote (("default")))) (r "1.67")))

(define-public crate-rgb-persist-fs-0.11.1 (c (n "rgb-persist-fs") (v "0.11.1") (d (list (d (n "amplify") (r "^4.6.0") (d #t) (k 0)) (d (n "rgb-std") (r "^0.11.0-beta.5") (f (quote ("fs"))) (d #t) (k 0)) (d (n "strict_encoding") (r "^2.7.0-beta.1") (d #t) (k 0)))) (h "1hw580q9ljhsyfswqpavgphwsp9wy1h702dmilgs28h869z4k58k") (f (quote (("default")))) (r "1.67")))

