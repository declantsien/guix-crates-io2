(define-module (crates-io rg b- rgb-io) #:use-module (crates-io))

(define-public crate-rgb-io-0.1.1 (c (n "rgb-io") (v "0.1.1") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1") (o #t) (d #t) (k 0)))) (h "01zllqbzbycv3ziijz0xqcnzgb7qvr7k531r4gvmvyv7806d9zqs") (f (quote (("bin" "dia-args" "termion"))))))

(define-public crate-rgb-io-0.2.0 (c (n "rgb-io") (v "0.2.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1") (o #t) (d #t) (k 0)))) (h "07mgsrvx6l2sq39124222hcgba1zyjpb9yy1hbn36jsik6ph09v7") (f (quote (("bin" "dia-args" "termion"))))))

