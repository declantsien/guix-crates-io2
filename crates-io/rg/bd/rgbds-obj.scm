(define-module (crates-io rg bd rgbds-obj) #:use-module (crates-io))

(define-public crate-rgbds-obj-0.1.0 (c (n "rgbds-obj") (v "0.1.0") (h "0cmj6wn3gm08nk7dijgm886l3p77nzfc80hg810xj76gg49dxyn1")))

(define-public crate-rgbds-obj-0.1.1 (c (n "rgbds-obj") (v "0.1.1") (h "1w7wjhzdzqh2l4w26dj07rkj9sd3rws67ki2zn7yzb6crf9dggg3")))

