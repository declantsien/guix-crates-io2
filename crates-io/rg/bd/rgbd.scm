(define-module (crates-io rg bd rgbd) #:use-module (crates-io))

(define-public crate-rgbd-0.1.0 (c (n "rgbd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "ureq") (r "^2.9.7") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (d #t) (k 0)))) (h "1z10lf5ddgp5r2yrwsmnfyhv39zrs4mgwb6q3hazc06p81bxzn7w")))

(define-public crate-rgbd-0.2.0 (c (n "rgbd") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9.7") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (d #t) (k 0)))) (h "0m6fsy9i8hs2qnxahdyarqnf1njpx1q4zkjbnddxzha6yhkcxcfb")))

(define-public crate-rgbd-0.2.1 (c (n "rgbd") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9.7") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (d #t) (k 0)))) (h "0mmasyr8xd9yrw9bn3a0ypc7jnprhkz7hi4fzbjfvx2ia9r7mh8d")))

