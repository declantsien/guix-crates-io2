(define-module (crates-io rg ep rgep) #:use-module (crates-io))

(define-public crate-rgep-0.1.0 (c (n "rgep") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)))) (h "1wm3gb19xqpa35fa52m2zch7rghfm2w6p9fhhd1gm9cvsbwfzpng")))

