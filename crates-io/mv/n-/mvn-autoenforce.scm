(define-module (crates-io mv n- mvn-autoenforce) #:use-module (crates-io))

(define-public crate-mvn-autoenforce-0.1.0 (c (n "mvn-autoenforce") (v "0.1.0") (d (list (d (n "itertools") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.5") (d #t) (k 0)))) (h "1jlg2d01pzy732s30by3mn5917aw2iv0zrn7z4h4ggjfjfv9557h")))

(define-public crate-mvn-autoenforce-0.1.1 (c (n "mvn-autoenforce") (v "0.1.1") (d (list (d (n "itertools") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.5") (d #t) (k 0)))) (h "0g3z4payz559i2ai1n9y6snpjc5qwmza2s2vbrim2w90qh5fci34")))

(define-public crate-mvn-autoenforce-0.1.2 (c (n "mvn-autoenforce") (v "0.1.2") (d (list (d (n "itertools") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.5") (d #t) (k 0)))) (h "0k0wgmpyhlp7z9jz6xmj4a7z0bwcp87hprzg9vmsh4jkjc97rlg9")))

(define-public crate-mvn-autoenforce-0.1.3 (c (n "mvn-autoenforce") (v "0.1.3") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.11") (d #t) (k 0)))) (h "0w09s5jhs5h8si406s302xn136qxfydlzx26sj68s80pxks3xwjw")))

(define-public crate-mvn-autoenforce-0.1.4 (c (n "mvn-autoenforce") (v "0.1.4") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.11") (d #t) (k 0)))) (h "18aj5ibf0gvq26g7ihlpnawjkihzc8108jic6f73wdxbjmw8r9yx")))

(define-public crate-mvn-autoenforce-0.1.5 (c (n "mvn-autoenforce") (v "0.1.5") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.11") (d #t) (k 0)))) (h "148lbhm2f6vgf69cr6vbq3hw0zz3aini67rphix936gaxdkadbzw")))

(define-public crate-mvn-autoenforce-0.1.6 (c (n "mvn-autoenforce") (v "0.1.6") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.11") (d #t) (k 0)))) (h "198wl6waj6p6jwamksmddm7a4m4yly8z8wwngld9d6d0ajqz92qr")))

(define-public crate-mvn-autoenforce-0.1.7 (c (n "mvn-autoenforce") (v "0.1.7") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.11") (d #t) (k 0)))) (h "1mj52s7x5nj8gjygyadp2qwg8l519j6dp3rdka1068qgy6dp45b8")))

(define-public crate-mvn-autoenforce-0.1.8 (c (n "mvn-autoenforce") (v "0.1.8") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.11") (d #t) (k 0)))) (h "0k7j394mxbx0s76w54ab9ah1djv9d8dap69qpcm62357isxfh93m") (y #t)))

(define-public crate-mvn-autoenforce-0.1.9 (c (n "mvn-autoenforce") (v "0.1.9") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.11") (d #t) (k 0)))) (h "0vh35jwvx14h7pvvaqfp3ckbddizj04zq1iiv3m5xalj6v742vbk") (y #t)))

(define-public crate-mvn-autoenforce-0.1.10 (c (n "mvn-autoenforce") (v "0.1.10") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.11") (d #t) (k 0)))) (h "1rcg2jib8pi548nfagx5gjfn500m3sraq5kh8chbngcaahhi6jah")))

(define-public crate-mvn-autoenforce-0.1.11 (c (n "mvn-autoenforce") (v "0.1.11") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.11") (d #t) (k 0)))) (h "1dkaj0dy8w1xf78m9z47wfkakbaxbaaa4qflhm7y062hgz7jyk09")))

(define-public crate-mvn-autoenforce-1.0.0 (c (n "mvn-autoenforce") (v "1.0.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.11") (d #t) (k 0)))) (h "1fhhbz4p8yss3nhlbhvvs85vb3k26qv78sdyki5m8jajvzwa62s7")))

