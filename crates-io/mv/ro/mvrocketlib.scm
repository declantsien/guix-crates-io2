(define-module (crates-io mv ro mvrocketlib) #:use-module (crates-io))

(define-public crate-mvrocketlib-1.0.0 (c (n "mvrocketlib") (v "1.0.0") (d (list (d (n "mvrocketlib-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "0fq85zrwmxvvccxxznbhg85avkrk2yql1l48d7f85k9m31p9n5x9")))

(define-public crate-mvrocketlib-1.1.0 (c (n "mvrocketlib") (v "1.1.0") (d (list (d (n "mvrocketlib-macro") (r "^1.1.0") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "0bp3ak6a7yh8wznjnbjzcxpcvg4j8yfjrmwd3qrpj1x82hy89hbm")))

(define-public crate-mvrocketlib-1.1.1 (c (n "mvrocketlib") (v "1.1.1") (d (list (d (n "mvrocketlib-macro") (r "^1.1.1") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "0b8nl1cgfc80ij6bz446yp7mnby50zx3vzc4np8arb1iqwvxjyz0")))

(define-public crate-mvrocketlib-1.1.2 (c (n "mvrocketlib") (v "1.1.2") (d (list (d (n "mvrocketlib-macro") (r "^1.1.1") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "0amcvl04pppl31kvpj2ig5bvs8lbk4kyyd0bi9pn6chkld1a4iip")))

(define-public crate-mvrocketlib-1.1.3 (c (n "mvrocketlib") (v "1.1.3") (d (list (d (n "mvrocketlib-macro") (r "^1.1.2") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "0bb0kzvqddcgjlb81gh56ac1lq1p6af5239qscb5vflfsw1qbxbi")))

(define-public crate-mvrocketlib-1.1.4 (c (n "mvrocketlib") (v "1.1.4") (d (list (d (n "mvrocketlib-macro") (r "^1.1.2") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "0p90mfbjpmbdzmcv30np7yfdhqi32d6ckxmi9fn1q88dq1qlqvr1")))

(define-public crate-mvrocketlib-1.1.5 (c (n "mvrocketlib") (v "1.1.5") (d (list (d (n "mvrocketlib-macro") (r "^1.1.2") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "1qmxbfcj9x4wasbc6jznl0i9apkba4977xhxnk7wdhx8bbnaw8cv")))

(define-public crate-mvrocketlib-1.1.6 (c (n "mvrocketlib") (v "1.1.6") (d (list (d (n "mvrocketlib-macro") (r "^1.1.2") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "01v5gwz2ifpsbv6ik94ax6v8cys54nqn2454wn20q00yjcd9pwnf")))

(define-public crate-mvrocketlib-1.1.7 (c (n "mvrocketlib") (v "1.1.7") (d (list (d (n "mvrocketlib-macro") (r "^1.1.3") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "134m37cb83xy01dkj43z59b022dmm9vlynj7q9d3rd8z22jc8x0z")))

(define-public crate-mvrocketlib-1.1.8 (c (n "mvrocketlib") (v "1.1.8") (d (list (d (n "mvrocketlib-macro") (r "^1.1.4") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "1vi5jvs216yn8ma92d9pbjw1hnnf15pmk5x3mkc9ygfdaqyg8mbk")))

