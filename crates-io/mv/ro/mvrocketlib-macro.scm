(define-module (crates-io mv ro mvrocketlib-macro) #:use-module (crates-io))

(define-public crate-mvrocketlib-macro-1.0.0 (c (n "mvrocketlib-macro") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "00lmqx7znqxa1gadf86j1imalq3z76q51k2x5zp1q3nzy4csyx6q")))

(define-public crate-mvrocketlib-macro-1.1.0 (c (n "mvrocketlib-macro") (v "1.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0750gzkrg0xr46jy9bm54z5pijvzbzw8kz24d1dr00qpvwr7hl96")))

(define-public crate-mvrocketlib-macro-1.1.1 (c (n "mvrocketlib-macro") (v "1.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "11d37im36cx7mcw7j5xyjwgpmyb8kr3xbhlfk801xbqq72vzvq60")))

(define-public crate-mvrocketlib-macro-1.1.2 (c (n "mvrocketlib-macro") (v "1.1.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1vs1rycj38gi09ddbkbn5awm62qrdk4jpga7w112bly37vzcp1g3")))

(define-public crate-mvrocketlib-macro-1.1.3 (c (n "mvrocketlib-macro") (v "1.1.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0h0f8s2h8yj3ggfwqplr8x2y64khi45flbbbqiwxxjklhc6iaaai")))

(define-public crate-mvrocketlib-macro-1.1.4 (c (n "mvrocketlib-macro") (v "1.1.4") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0bzfljad8glylg4dmg4ni198xp5kb05y3nl8dacrilk4c531sdc6")))

