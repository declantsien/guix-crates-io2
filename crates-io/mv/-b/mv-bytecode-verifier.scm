(define-module (crates-io mv -b mv-bytecode-verifier) #:use-module (crates-io))

(define-public crate-mv-bytecode-verifier-0.1.0 (c (n "mv-bytecode-verifier") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "invalid-mutations") (r "^0.1.0") (d #t) (k 2)) (d (n "mirai-annotations") (r "^1.10.1") (d #t) (k 0)) (d (n "move-binary-format") (r "^0.1.0") (d #t) (k 0) (p "mv-binary-format")) (d (n "move-borrow-graph") (r "^0.1.0") (d #t) (k 0) (p "mv-borrow-graph")) (d (n "move-core-types") (r "^0.1.0") (d #t) (k 0) (p "mv-core-types")) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "0r9b6s6aw1y6mfq29bzn39p8901vcwnmd016q3qfjyjdr56v4ybg") (f (quote (("default")))) (y #t)))

