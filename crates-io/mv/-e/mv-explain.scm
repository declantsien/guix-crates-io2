(define-module (crates-io mv -e mv-explain) #:use-module (crates-io))

(define-public crate-mv-explain-0.1.4 (c (n "mv-explain") (v "0.1.4") (d (list (d (n "bcs") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.1.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.1.0") (d #t) (k 0)))) (h "0gj3ia41n5y4xblp3dg77fy6q1xm9yjygcr9nsk445xh93vz4jvk") (f (quote (("default")))) (y #t)))

(define-public crate-mv-explain-0.1.5 (c (n "mv-explain") (v "0.1.5") (d (list (d (n "bcs") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.1.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.1.0") (d #t) (k 0)))) (h "13z41favypw1hg5gnq043qg4bwgq3qw2l2ik214ygv18d0g2hjwr") (f (quote (("default")))) (y #t)))

(define-public crate-mv-explain-0.1.6 (c (n "mv-explain") (v "0.1.6") (d (list (d (n "bcs") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.1.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.1.0") (d #t) (k 0)))) (h "0ml9pkq8lzq20rphfjms62rmqgpi1h2fb3c4ca78scahncvk4syw") (f (quote (("default")))) (y #t)))

(define-public crate-mv-explain-0.2.0 (c (n "mv-explain") (v "0.2.0") (d (list (d (n "bcs") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.2.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.2.0") (d #t) (k 0)))) (h "19aypc9m1s3a0irln6m30h9y7i8pbzpmw8q329j8p7wdjs83jbz6") (f (quote (("default")))) (y #t)))

(define-public crate-mv-explain-0.2.1 (c (n "mv-explain") (v "0.2.1") (d (list (d (n "bcs") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.2.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.2.0") (d #t) (k 0)))) (h "06j8f6cb1n9blzhyycvy5qjy4dsvc1b9s809z0fq95m5w0y1amz8") (f (quote (("default")))) (y #t)))

(define-public crate-mv-explain-0.3.0 (c (n "mv-explain") (v "0.3.0") (d (list (d (n "bcs") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.3.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.3.0") (d #t) (k 0)))) (h "1hvdsjvwc38h09ayyljvp40rjkg7pqr3xz6075i417rd481i1j2y") (f (quote (("default")))) (y #t)))

(define-public crate-mv-explain-0.3.1 (c (n "mv-explain") (v "0.3.1") (d (list (d (n "bcs") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.3.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.3.0") (d #t) (k 0)))) (h "0k3k9vxdr9m2f4sf8mrpw6954ji379s8vs2dwkgikjzidmvxy22h") (f (quote (("default")))) (y #t)))

(define-public crate-mv-explain-0.3.2 (c (n "mv-explain") (v "0.3.2") (d (list (d (n "bcs") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.3.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.3.0") (d #t) (k 0)))) (h "0ap5rdvx2xzi3kqpa7zcql92jsin3rc9p8hq2xaikklgpj47i8pb") (f (quote (("default")))) (y #t)))

