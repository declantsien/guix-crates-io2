(define-module (crates-io mv -s mv-symbol-pool) #:use-module (crates-io))

(define-public crate-mv-symbol-pool-0.1.0 (c (n "mv-symbol-pool") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0n0q2rvdx76lj5gxzcbgpn907cp0sig30daywh2q7s6ingiibjh7") (f (quote (("default")))) (y #t)))

