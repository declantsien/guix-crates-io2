(define-module (crates-io mv n_ mvn_version) #:use-module (crates-io))

(define-public crate-mvn_version-0.0.0 (c (n "mvn_version") (v "0.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)))) (h "1zgwp4lx3agp4p6hasxybjhv7klln1hfwclfi0sab3r6ywdp6ikq")))

(define-public crate-mvn_version-0.1.0 (c (n "mvn_version") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)))) (h "18mmi47f2pkpbm5k5laj1d6vxq1vkm2dj35hpdhs9a9bv1qkbh2g")))

