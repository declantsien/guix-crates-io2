(define-module (crates-io mv -c mv-command-line-common) #:use-module (crates-io))

(define-public crate-mv-command-line-common-0.1.0 (c (n "mv-command-line-common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0g7sric3sdhz2zarqqlg1a886c83r1hffa59jznsw32zb11qw5gx") (y #t)))

