(define-module (crates-io mv c_ mvc_views) #:use-module (crates-io))

(define-public crate-mvc_views-0.1.0 (c (n "mvc_views") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08vskhrb6wc220zg0hmx204xygb12hdqxnqydm3s20s9bcq3q0x2")))

(define-public crate-mvc_views-0.1.1 (c (n "mvc_views") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aq2w25nv8ljis33c0mvil86fqlx4sl2y44dw0dvksrdlh1q882n")))

