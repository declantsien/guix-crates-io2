(define-module (crates-io mv ca mvcapi) #:use-module (crates-io))

(define-public crate-mvcapi-3.0.11 (c (n "mvcapi") (v "3.0.11") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0hfarqryrrlz1nkfhimh21d66xlwx5mvzxlqjzdl00ncx49nihkk")))

