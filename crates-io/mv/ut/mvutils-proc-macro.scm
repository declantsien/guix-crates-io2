(define-module (crates-io mv ut mvutils-proc-macro) #:use-module (crates-io))

(define-public crate-mvutils-proc-macro-0.1.0 (c (n "mvutils-proc-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "0vhazsskr5y5d7gp54yaw4b06nirkxg2pmrpxa5mgxxackafr8p9") (r "1.69.0")))

(define-public crate-mvutils-proc-macro-0.2.0 (c (n "mvutils-proc-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "15g4kj8brrxf06yg9fk8ij0yvl570hn0jvadrsmziqnivs5rpd9v") (r "1.69.0")))

(define-public crate-mvutils-proc-macro-0.3.0 (c (n "mvutils-proc-macro") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "1hqjhl4y66q013wdcyq3g0vkjh3g2hbpnl5qbadd35n7jy3h18dq") (r "1.69.0")))

(define-public crate-mvutils-proc-macro-0.3.1 (c (n "mvutils-proc-macro") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "0yqkkv9708i5jrx1d9w9nrbcviqgzirwl4v20jjgl112d1h1bf6p") (r "1.69.0")))

(define-public crate-mvutils-proc-macro-0.3.2 (c (n "mvutils-proc-macro") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "10vmqdiyxy0z0c5z6kavd633dkdyypx88645kpi71r72mzhczvbj") (r "1.69.0")))

(define-public crate-mvutils-proc-macro-0.3.3 (c (n "mvutils-proc-macro") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1jhmn69svsd5sy845n6rqwnif3y5346nm5ipkwl9lxf1l0hn0f0a") (r "1.75.0")))

(define-public crate-mvutils-proc-macro-0.4.0 (c (n "mvutils-proc-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "134072wwzyqlnip7n7wpilrwvw5l2hhby52167k344pfa7ahw4wx") (r "1.75.0")))

