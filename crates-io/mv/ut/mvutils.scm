(define-module (crates-io mv ut mvutils) #:use-module (crates-io))

(define-public crate-mvutils-0.1.0 (c (n "mvutils") (v "0.1.0") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0g3kb9cqdv7vhsiz1cn5sgvxzrlcr3sjclyzq3ygj8c3zyk284af") (r "1.69.0")))

(define-public crate-mvutils-0.2.0 (c (n "mvutils") (v "0.2.0") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "11912gcnap6qzdvah2a8cik4a4lnzmyk62dx72l875jz1c8780wx") (r "1.69.0")))

(define-public crate-mvutils-0.2.1 (c (n "mvutils") (v "0.2.1") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1x1vdw2c7hn4sgi6cdb15ir6n5r6gvpikw3rhchkhikyqzwaxvfz") (r "1.69.0")))

(define-public crate-mvutils-0.3.0 (c (n "mvutils") (v "0.3.0") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1m4fjr8izjn0xas9qw6993afrqbnq8vi8qs7sgyj2bhviwylgbwf") (r "1.69.0")))

(define-public crate-mvutils-0.3.1 (c (n "mvutils") (v "0.3.1") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0aa8rqwkcz48pmcll4s7l89gbaiwl2mq2z9ybzsz68xy9i5m25zd") (r "1.69.0")))

(define-public crate-mvutils-0.3.2 (c (n "mvutils") (v "0.3.2") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0m59qdaf0ywp9q95g9xz6kwlh5w3llg7cnjgdw7lfn98314lxmw0") (r "1.69.0")))

(define-public crate-mvutils-0.3.3 (c (n "mvutils") (v "0.3.3") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1ngahqsqjpsl1hm2vgb4lhvsc6w04w30jp4icxzqlcysjv9df5kw") (r "1.69.0")))

(define-public crate-mvutils-0.3.4 (c (n "mvutils") (v "0.3.4") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0bz5bh5dan1r3f4r5hdckins4443cwl2nnl6ndvl9kw1ap3bq36v") (r "1.69.0")))

(define-public crate-mvutils-0.3.5 (c (n "mvutils") (v "0.3.5") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1jbsmpv9y1s1cai3703m3i9zbl0iziwn6fqc84pzb0xm3lzp5pcs") (r "1.69.0")))

(define-public crate-mvutils-0.4.0 (c (n "mvutils") (v "0.4.0") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (o #t) (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1l6g1d3msd3pspd31ilqv4rh9dc10vk1cw4rfc051xxcd9hq3nhk") (s 2) (e (quote (("xml" "dep:hashbrown")))) (r "1.75.0")))

(define-public crate-mvutils-0.4.1 (c (n "mvutils") (v "0.4.1") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (o #t) (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0661g1v9faz9fi0wrplb34vdabdml8047mwwzd1zgcdlwyj448mj") (s 2) (e (quote (("xml" "dep:hashbrown")))) (r "1.75.0")))

(define-public crate-mvutils-0.4.2 (c (n "mvutils") (v "0.4.2") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (o #t) (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "12979wh9hn2jmql7015cc6v25bks2rkkjqmwpyx01m3iv33jbpr8") (s 2) (e (quote (("xml" "dep:hashbrown")))) (r "1.75.0")))

(define-public crate-mvutils-0.4.3 (c (n "mvutils") (v "0.4.3") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (o #t) (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "01dyfhh1a8ipfhv6wff18lb3gwbs7fy99p5sv7cxwrmz9j4zhg6g") (s 2) (e (quote (("xml" "dep:hashbrown")))) (r "1.75.0")))

(define-public crate-mvutils-0.4.4 (c (n "mvutils") (v "0.4.4") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (o #t) (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0ya7mhba3f4n9y6v0a61mp14hc2zigbhvnhds62wscglpvcvkl76") (s 2) (e (quote (("xml" "dep:hashbrown")))) (r "1.75.0")))

(define-public crate-mvutils-0.5.0 (c (n "mvutils") (v "0.5.0") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1vzs9cp4ai46wpsgamrqwgswwkq5ybldllgvkcz15b2xai9nwnjr") (f (quote (("xml")))) (r "1.75.0")))

(define-public crate-mvutils-0.5.1 (c (n "mvutils") (v "0.5.1") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "04cdddglzp8yjhxbmq99r713w2fybspjagab4m9701s5x5wjm8f8") (f (quote (("xml")))) (r "1.75.0")))

(define-public crate-mvutils-0.5.2 (c (n "mvutils") (v "0.5.2") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1aa72h85j3v2k4w3bssp3p90rcqz92nnm2g2jxiz0k1rxsn7x734") (f (quote (("xml")))) (r "1.75.0")))

(define-public crate-mvutils-0.5.3 (c (n "mvutils") (v "0.5.3") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "096ya7rg4dm2zfihdazjfr5vfi089r2jqj6vbbxrc048bdxia8md") (f (quote (("xml")))) (r "1.75.0")))

(define-public crate-mvutils-0.5.4 (c (n "mvutils") (v "0.5.4") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1xzb0lyrznxc9pk9g30mgivsrya9q9vzm5xqdr7fc5qc2vrwh59c") (f (quote (("xml") ("savable_arc")))) (r "1.75.0")))

(define-public crate-mvutils-0.5.5 (c (n "mvutils") (v "0.5.5") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1hrgg0frvya60kd7047zxqbv20adrrm7a7hmdvbcwgrzgkvdckm1") (f (quote (("xml") ("savable_arc")))) (r "1.75.0")))

(define-public crate-mvutils-0.5.6 (c (n "mvutils") (v "0.5.6") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0afp8aildzfsr4xc0vcav6sd64fzl23djgphimqg2bh9a3nwg6qp") (f (quote (("xml") ("savable_arc")))) (r "1.75.0")))

(define-public crate-mvutils-0.5.7 (c (n "mvutils") (v "0.5.7") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0lxj20mf1jv58mqbanlq81sbkr69906fpdkknhf9srdpaa8mmfj2") (f (quote (("xml") ("savable_arc")))) (r "1.75.0")))

(define-public crate-mvutils-0.6.0 (c (n "mvutils") (v "0.6.0") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1v1byijmdk0zb45gsk1dd50fy7ip5qd97df70yki0kqymrk037kc") (f (quote (("xml") ("savable_arc")))) (r "1.75.0")))

(define-public crate-mvutils-0.6.1 (c (n "mvutils") (v "0.6.1") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "mvutils-proc-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0m3hj7y386rgv3ykkz3g0h2hng4fj56whsybi7wprm7rzxhy9iyq") (f (quote (("xml") ("savable_arc")))) (r "1.75.0")))

