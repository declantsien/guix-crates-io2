(define-module (crates-io mv c- mvc-rs) #:use-module (crates-io))

(define-public crate-mvc-rs-0.1.0 (c (n "mvc-rs") (v "0.1.0") (h "1y7xh9x755mk0cqqky48vnpvdqskz175csx98aw1r4m6h0qbcp1z") (y #t)))

(define-public crate-mvc-rs-0.2.0 (c (n "mvc-rs") (v "0.2.0") (h "19lijgp2gsqqd1icxky1l2hj2snlvaz2c1ygxqqq709320pqp7hl") (y #t)))

(define-public crate-mvc-rs-0.3.0 (c (n "mvc-rs") (v "0.3.0") (h "1zhkjv861l6dv4a19ank59bv8d135h0drzp4xag2g2nnzlvs1ff5") (y #t)))

(define-public crate-mvc-rs-0.3.2 (c (n "mvc-rs") (v "0.3.2") (h "093vn62xvid9cqa4111iv54gv8wc49hm60b3z757mnx6rb50j12j") (y #t)))

(define-public crate-mvc-rs-3.3.0 (c (n "mvc-rs") (v "3.3.0") (h "0r7w6h74hz4f5024wi576dq899vx96mhmqw63whbllni95ihx06q")))

