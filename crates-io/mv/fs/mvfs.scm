(define-module (crates-io mv fs mvfs) #:use-module (crates-io))

(define-public crate-mvfs-0.3.0 (c (n "mvfs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "moka") (r "^0.9.4") (d #t) (k 0)) (d (n "mvclient") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "100xbf04g8h27cplh4i6x0innscwnfvkasydr0xsp92b03m2pms2")))

