(define-module (crates-io mv lo mvlogger) #:use-module (crates-io))

(define-public crate-mvlogger-0.1.0 (c (n "mvlogger") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mvutils") (r "^0.4.1") (d #t) (k 0)))) (h "1dlxjay94l0wphgscrldd6qc2k09xypjfqrkgdjwi1xazpz34iws") (r "1.75.0")))

(define-public crate-mvlogger-0.1.1 (c (n "mvlogger") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mvutils") (r "^0.4.1") (d #t) (k 0)))) (h "0whr1aa3p1xdv45glk0dd7f3372qr97a0zlvkv4mvv6z3x7b5n12") (r "1.75.0")))

(define-public crate-mvlogger-0.1.2 (c (n "mvlogger") (v "0.1.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mvutils") (r "^0.4.1") (d #t) (k 0)))) (h "1f87p1m2hflvxpgxcpvldd7g120r307s96ghv34qbhb30vz8yb5s") (r "1.75.0")))

(define-public crate-mvlogger-0.2.0 (c (n "mvlogger") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mvutils") (r "^0.4.2") (d #t) (k 0)))) (h "0jl66xkbq4wm0yvabjzrxrs45dgdvpl883sc0738bsb0gkfqdvi1") (r "1.75.0")))

(define-public crate-mvlogger-0.2.1 (c (n "mvlogger") (v "0.2.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.0") (d #t) (k 0)))) (h "0bgjbwfmiim0wbw94ysr4prvd4bybg6mcwblgy0czas8pg305f5d") (r "1.75.0")))

(define-public crate-mvlogger-0.2.2 (c (n "mvlogger") (v "0.2.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.1") (d #t) (k 0)))) (h "01xmj6c5mxkb68vfpsb1vj7nadjifjyz45pnfh90mi84jxcqm3jx") (r "1.75.0")))

(define-public crate-mvlogger-0.2.3 (c (n "mvlogger") (v "0.2.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.2") (d #t) (k 0)))) (h "0n9mwj50gspsjcs7ravil0i0sy63qk1zmrgyr0p8ywr9fn2r8ip4") (r "1.75.0")))

(define-public crate-mvlogger-0.2.4 (c (n "mvlogger") (v "0.2.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.3") (d #t) (k 0)))) (h "1i18x1r6frjkrk6dajlwa2nqrpjb24fbx7qdvf9ym60ci2ds5a65") (r "1.75.0")))

(define-public crate-mvlogger-0.2.5 (c (n "mvlogger") (v "0.2.5") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.5") (d #t) (k 0)))) (h "10clsrd946y98v2y45hg8fkpj2pagbjs8w5977m190wlc46rkvws") (r "1.75.0")))

(define-public crate-mvlogger-0.3.0 (c (n "mvlogger") (v "0.3.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.5") (d #t) (k 0)))) (h "1qh6m6855j7rw84cf3m0y05d86n18izxz7r95jyirz69sz2fp19q") (r "1.75.0")))

