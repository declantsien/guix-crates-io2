(define-module (crates-io mv sd mvsdk) #:use-module (crates-io))

(define-public crate-mvsdk-0.1.0 (c (n "mvsdk") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (o #t) (d #t) (k 1)) (d (n "image") (r "^0.24.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.116") (d #t) (k 0)))) (h "104mnshyss0bp3nyijp0x7l602463xik5dgy3qdf9jgsqkx8pbj6") (f (quote (("polling_example" "image") ("default_callback_example" "image") ("default") ("custom_callback_example" "image") ("compile_bindings" "bindgen"))))))

(define-public crate-mvsdk-0.1.1 (c (n "mvsdk") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.116") (d #t) (k 0)))) (h "0i1rayy56xr7b55vsk6a6wihkylaq6ff4xldd7jyd57ci8mmv7rh") (f (quote (("default") ("compile_bindings" "bindgen"))))))

