(define-module (crates-io mv bi mvbitfield-macros) #:use-module (crates-io))

(define-public crate-mvbitfield-macros-0.1.0 (c (n "mvbitfield-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "175bfs090kpwv9gk5yssdbk4f707cwyazhlicrqhag4ck060491p")))

(define-public crate-mvbitfield-macros-0.2.0 (c (n "mvbitfield-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "14ch6p5k0rkcsd6kfhc5qgwph44xbcq9mar7mmixa6jw4cdf7fzc")))

