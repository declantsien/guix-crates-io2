(define-module (crates-io mv bi mvbitfield) #:use-module (crates-io))

(define-public crate-mvbitfield-0.0.0 (c (n "mvbitfield") (v "0.0.0") (h "050zkz08j6lq33c538m1lpzfp3547nh5dbwddqn3nh7b7vwqgdiq")))

(define-public crate-mvbitfield-0.1.0 (c (n "mvbitfield") (v "0.1.0") (d (list (d (n "bitint") (r "^0.1") (d #t) (k 0)) (d (n "mvbitfield-macros") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1cj8k2r3p2kffcz3nrdmiz38dlw4b5zn3xx06b28zd3k9vkpjwd1") (f (quote (("doc") ("_trybuild_tests") ("_nightly"))))))

(define-public crate-mvbitfield-0.1.1 (c (n "mvbitfield") (v "0.1.1") (d (list (d (n "bitint") (r "^0.1") (d #t) (k 0)) (d (n "mvbitfield-macros") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1xdcb7pxlwnisb1r6y4wq2svylg03c5dnpgrqbkjismsyc3xxpag") (f (quote (("doc") ("_trybuild_tests") ("_nightly"))))))

(define-public crate-mvbitfield-0.2.0 (c (n "mvbitfield") (v "0.2.0") (d (list (d (n "bitint") (r "^0.1") (d #t) (k 0)) (d (n "mvbitfield-macros") (r "^0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1xbg7vc0ssv296j1r5180s193l13zfawdl2skf434466r4ssp527") (f (quote (("doc") ("_trybuild_tests") ("_nightly"))))))

