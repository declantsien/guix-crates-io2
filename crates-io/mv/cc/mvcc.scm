(define-module (crates-io mv cc mvcc) #:use-module (crates-io))

(define-public crate-mvcc-0.0.1 (c (n "mvcc") (v "0.0.1") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.15") (d #t) (k 0)))) (h "1mglxx7lx5rsxnbjp08vnkgp1yhd4j81hhrl0sklkkg053jam8sy")))

