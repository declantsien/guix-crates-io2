(define-module (crates-io mv cc mvcc_cell) #:use-module (crates-io))

(define-public crate-mvcc_cell-0.1.0 (c (n "mvcc_cell") (v "0.1.0") (d (list (d (n "guest_cell") (r "^0.1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1p7l88w3knczqr46yv295xiqa5xlxpava2y8486yy9pj95amhf2s")))

(define-public crate-mvcc_cell-0.1.1 (c (n "mvcc_cell") (v "0.1.1") (d (list (d (n "guest_cell") (r "^0.1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1na1748jvzkv99sqjjiaym703ag2grydsgq572zglqjfmjir79z6")))

(define-public crate-mvcc_cell-0.1.2 (c (n "mvcc_cell") (v "0.1.2") (d (list (d (n "guest_cell") (r "^0.1.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "017zdaqs21gh7v4pj6jrw5xpanj0nfrfp0n66jg6d366nrvi4xnb")))

