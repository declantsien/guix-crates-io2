(define-module (crates-io mv ar mvar) #:use-module (crates-io))

(define-public crate-mvar-0.1.1 (c (n "mvar") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 2)) (d (n "shuttle") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0ba08wd5z8bd68gaynyasgwcniyvm9fqp2bl11l698z49apwwhyw")))

