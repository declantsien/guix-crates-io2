(define-module (crates-io mv sy mvsync) #:use-module (crates-io))

(define-public crate-mvsync-1.0.0 (c (n "mvsync") (v "1.0.0") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "0d4dsr0ljgcfl8lygyl2h5gf063c9xwldp3dibjlr65m0w92nhka") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

(define-public crate-mvsync-1.1.0 (c (n "mvsync") (v "1.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "mvutils") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "0r2avlq9zyyr1yras8x3145ih59yv4q8r3mddjd83agpps9m18np") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

(define-public crate-mvsync-1.1.1 (c (n "mvsync") (v "1.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "mvutils") (r "^0.4.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "031j9wp107vk7jm1qqkyr3ln1yhv2zw8qjbcr8phjqzvwhan1fki") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

(define-public crate-mvsync-1.1.2 (c (n "mvsync") (v "1.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "mvutils") (r "^0.4.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "1vnqsnz9lxqx3vlvpm5b7a8zkm4a27cmahaw9w08x563hf6ajfjj") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

(define-public crate-mvsync-1.1.3 (c (n "mvsync") (v "1.1.3") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "mvutils") (r "^0.4.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "1mmxi97n8a3ab8vn5h5zw70hy4rrap8snlgxypnpakqbp5qzslgh") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

(define-public crate-mvsync-1.1.4 (c (n "mvsync") (v "1.1.4") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "1pvs74lcsjbvwb96f6rjpj9cbkdybc7qh5rld8p72h99c0fspf99") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

(define-public crate-mvsync-1.1.5 (c (n "mvsync") (v "1.1.5") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "17ck1wrwajs3z0aygh3bw9yg3cchi6ry44g8d72dpb5kb6chprc6") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

(define-public crate-mvsync-1.1.6 (c (n "mvsync") (v "1.1.6") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "174ykdz3mnbrmpvddwbhh1a72ala79sasln8q8zkq69jljj3zv2v") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

(define-public crate-mvsync-1.1.7 (c (n "mvsync") (v "1.1.7") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "0xhjvvvlq1rnffpgy2finjl27v41zfs5bfwkyl3dpl0rl61cvrrm") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

(define-public crate-mvsync-1.1.8 (c (n "mvsync") (v "1.1.8") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "0j58ch1nyraql1xskr4y8p03fqxnj35j851igfxbicm5khnmd8qn") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

(define-public crate-mvsync-1.1.9 (c (n "mvsync") (v "1.1.9") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "0m06bvnh4ag37adgbnkya6jsmjkmcnhmx4k12yqdrq8ls9c7a33r") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

(define-public crate-mvsync-1.2.0 (c (n "mvsync") (v "1.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)) (d (n "mvutils") (r "^0.5.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "11fjl3m2fx2wqn67bqj96a0np288pc4xf7qg2kvdvwbz2d5fmqzd") (f (quote (("main-thread") ("command-buffers") ("all" "command-buffers" "main-thread"))))))

