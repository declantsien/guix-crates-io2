(define-module (crates-io mv db mvdb) #:use-module (crates-io))

(define-public crate-mvdb-0.1.0 (c (n "mvdb") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00yfk2diifgrdbjmh9qplvflaqdyvdq365gwp7sm1z2x2qyxrcp1") (f (quote (("use-hashable") ("default" "use-hashable"))))))

(define-public crate-mvdb-0.1.1 (c (n "mvdb") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1ladixyiqjq39mq55h90vdbh0lpva9ziir32q721syk0jfg1l0yk") (f (quote (("use-toml" "toml") ("use-json" "serde_json") ("use-hashable") ("default" "use-hashable" "use-json"))))))

(define-public crate-mvdb-0.1.2 (c (n "mvdb") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "04s84iz21p2sz84da6bqy4cxplfdh0kgnl77zvavinxxxizlr83q") (f (quote (("use-toml" "toml") ("use-json" "serde_json") ("use-hashable") ("default" "use-hashable" "use-json"))))))

(define-public crate-mvdb-0.2.0 (c (n "mvdb") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ns0wzv82i0sn0dv56irw5x1g2zzjlyyhv8395q2ryhanhs8wz7q")))

(define-public crate-mvdb-0.2.1 (c (n "mvdb") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jirz7zb48a35b5m71a7zgh0yaywsr0sr5a58k86qinymx71g8ap")))

(define-public crate-mvdb-0.3.0 (c (n "mvdb") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zjwjsg7798v4w23yfxiznsp5fpxz0iah4a4ivngp66m3xy8jqgp")))

