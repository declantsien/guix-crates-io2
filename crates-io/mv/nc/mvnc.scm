(define-module (crates-io mv nc mvnc) #:use-module (crates-io))

(define-public crate-mvnc-0.1.0-prealphaempty (c (n "mvnc") (v "0.1.0-prealphaempty") (h "11f9cdrd480q44b9sfvaafdvr60sj6pis5lpj0nyckgzag0y67d3")))

(define-public crate-mvnc-0.1.0-prealpha (c (n "mvnc") (v "0.1.0-prealpha") (h "1l44722vcdd581f7mwpc239ay6rw03hlffpknlfxrf7lxzxndg39")))

(define-public crate-mvnc-0.1.0 (c (n "mvnc") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "186nq9mzc4jlgjlljcd93c3yl9ll6ix14li6hzmgphj2n79lf43z")))

(define-public crate-mvnc-0.1.1 (c (n "mvnc") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0331pkkf39n50b1advqvn644h5di2ha8195hnbj4cir2w1hixxzr")))

(define-public crate-mvnc-0.1.2 (c (n "mvnc") (v "0.1.2") (h "1yv9yq145bd0rrxqanq8g3yd216cab9s32bf2sizialhcf88xl4q")))

(define-public crate-mvnc-0.1.3 (c (n "mvnc") (v "0.1.3") (d (list (d (n "half") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "189yx7f5600al1ssf5wm2b3qbi4qcwzn8nfp46riavdk3ffpc9md")))

(define-public crate-mvnc-0.2.0 (c (n "mvnc") (v "0.2.0") (d (list (d (n "half") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0x538y94gdfcf1qprf46w3dqf1lj7z9wkv41ablmaqfr6mjslvva")))

(define-public crate-mvnc-0.3.0 (c (n "mvnc") (v "0.3.0") (d (list (d (n "half") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0a3fkhkp1ismx71bx5zd9g8y3fxgc58sf1mlmqc31qxn1w0wi92w")))

