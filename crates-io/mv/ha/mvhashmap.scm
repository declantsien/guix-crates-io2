(define-module (crates-io mv ha mvhashmap) #:use-module (crates-io))

(define-public crate-mvhashmap-0.1.0 (c (n "mvhashmap") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "1lbl17nj0s7bbjg3lp68ncxl686vwyffg8gwrlrdh1b4cspdxinn") (y #t)))

(define-public crate-mvhashmap-0.1.1 (c (n "mvhashmap") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "1wwylgknkcsl2gd9ziv9icq6qswxbh577dbsj82g8id662yazc5q") (y #t)))

(define-public crate-mvhashmap-0.1.2 (c (n "mvhashmap") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "1cvmzr8cqgzaas5kw5nahsknjv2b2wwqf4x7wrzc3f417kqh5rqk") (y #t)))

(define-public crate-mvhashmap-0.1.3 (c (n "mvhashmap") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "1dr4h1d63n8l6kpqb9fhwr3775q19cy8sszfinr6ggvl8xy1i6lz") (y #t)))

(define-public crate-mvhashmap-0.1.4 (c (n "mvhashmap") (v "0.1.4") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "1nvcl5fxabz9jnz08qbmnbav6ixrxwiyx3g35drk6h2vrhynvspw") (y #t)))

(define-public crate-mvhashmap-0.1.5 (c (n "mvhashmap") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "174ng9sy5zs9z7n9yv777kcrxbvkmd3g4245kqam5m14nbgrw2bh") (y #t)))

(define-public crate-mvhashmap-0.1.6 (c (n "mvhashmap") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "1d6kbkgnmhlnkgagl8fflyd8hzy29qqc5xcbhcyqg6dwiklr0cr3") (y #t)))

(define-public crate-mvhashmap-0.1.7 (c (n "mvhashmap") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "03qn7fp6r42nfzxafvn0sazj62rf55xgjxgmaj44rlzyvcd664qg") (y #t)))

(define-public crate-mvhashmap-0.2.1 (c (n "mvhashmap") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "1dhaik6izp1kw6a8n4737n0sk44z3qw47ad9yx6mjxjnkhp3xdvc") (y #t)))

(define-public crate-mvhashmap-0.2.2 (c (n "mvhashmap") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "1q97i5mxnzyfyisrnkdw13jbmkd7kx0hdly0m5sxlgknimv5d5wj") (y #t)))

(define-public crate-mvhashmap-0.2.6 (c (n "mvhashmap") (v "0.2.6") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "0jpmvafldxzml2hk9bcjb69mamqwflridgy4n571m1fh5zpz23mh") (y #t)))

(define-public crate-mvhashmap-0.2.7 (c (n "mvhashmap") (v "0.2.7") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)))) (h "0rknkhyx0q8ar4zfq85jzlfa6im79viq1rji727xr863918ydfcd") (y #t)))

