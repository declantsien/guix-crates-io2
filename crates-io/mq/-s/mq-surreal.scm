(define-module (crates-io mq -s mq-surreal) #:use-module (crates-io))

(define-public crate-mq-surreal-0.4.1 (c (n "mq-surreal") (v "0.4.1") (d (list (d (n "mq") (r "^0.4.1") (d #t) (k 0)))) (h "0jb6wpda16x6hf9z2yr42b4g4plzykdxr106zac88jj26qqx5p4k")))

(define-public crate-mq-surreal-0.4.3 (c (n "mq-surreal") (v "0.4.3") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "mq") (r "^0.4.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.9") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "147grbhmxvp6m12c4n28vcxdj4svncfj1h47w8zj8b9isay5g9gn")))

(define-public crate-mq-surreal-0.4.4 (c (n "mq-surreal") (v "0.4.4") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "mq") (r "^0.4.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.9") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "00q2c8rzgr0is9jzzcdfa4wc2868lf1k6441y4a2z01s3ksxjv5a")))

(define-public crate-mq-surreal-0.4.5 (c (n "mq-surreal") (v "0.4.5") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "mq") (r "^0.4.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.9") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "1ms7rx9nhsf85skipgazb1iv35zgnbkac927yq16i77nadwkmanh")))

(define-public crate-mq-surreal-0.4.6 (c (n "mq-surreal") (v "0.4.6") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "mq") (r "^0.4.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.9") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "002n2zkwkysr6jd29z7gks6vy6s6dqxgx4afk6h1zl7ljfkllra5")))

(define-public crate-mq-surreal-0.4.7 (c (n "mq-surreal") (v "0.4.7") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "mq") (r "^0.4.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.9") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "1jxgw40gwpq6fj5fs3khhbxv5cz5n6zxxcwrm7yacdd7jm6dmss0")))

(define-public crate-mq-surreal-0.4.8 (c (n "mq-surreal") (v "0.4.8") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "mq") (r "^0.4.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.9") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "0pgwy7wy8za0zyhb5xzfz5xf2x09v7mghqgdjmilal1fgc2jryad")))

(define-public crate-mq-surreal-0.5.0 (c (n "mq-surreal") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "mq") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.10") (d #t) (k 0)) (d (n "time") (r "^0.3.28") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "1mnxnhw0mj3hc13gg9fn9x1ihlcwg1xic5c9s49iv37cy6r16cch")))

(define-public crate-mq-surreal-0.6.0 (c (n "mq-surreal") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "mq") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.11") (d #t) (k 0)) (d (n "time") (r "^0.3.28") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "02qllx5lbaj2wgh1bcjcdiiajyp01b9z6r0xp6zk620gl8wnw911")))

(define-public crate-mq-surreal-0.7.0 (c (n "mq-surreal") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "mq") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.3.28") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "1pw90ld5pnhdysp5ry0n1dfz0k52w1jmxr5b27pgib8j4r2l629j")))

(define-public crate-mq-surreal-0.8.0 (c (n "mq-surreal") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "mq") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.2") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "005sn75m0r9iw46qi4hgg26aw05i29rvrfiq4c43fd3cxmpvmc1b")))

(define-public crate-mq-surreal-0.9.0 (c (n "mq-surreal") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "mq") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.2") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "15kxwv0ixkg79pgiflf8whlc7wqi2ksnyxypm5y1l7qc8jfbk7js")))

(define-public crate-mq-surreal-0.10.0 (c (n "mq-surreal") (v "0.10.0") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "mq") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "surrealdb") (r "^1.4.2") (d #t) (k 0)) (d (n "time") (r "^0.3.36") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "12if9p9favvv0jqijyrirwj4sp5kq2qb3c2sbafnv9avlravwygd")))

