(define-module (crates-io mq uo mquote-impl) #:use-module (crates-io))

(define-public crate-mquote-impl-0.1.0 (c (n "mquote-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1lb4yqf7ng4sz6sij6zdh5apmqrpi8say2y556knz11vw9cbwzmw")))

