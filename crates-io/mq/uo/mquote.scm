(define-module (crates-io mq uo mquote) #:use-module (crates-io))

(define-public crate-mquote-0.1.0 (c (n "mquote") (v "0.1.0") (d (list (d (n "mquote-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "16l20v7731h9qsv4jgkh4ay45lxbgmr1fyph7mivlvwkaiyf8ckh")))

