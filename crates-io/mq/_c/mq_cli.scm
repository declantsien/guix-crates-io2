(define-module (crates-io mq _c mq_cli) #:use-module (crates-io))

(define-public crate-mq_cli-3773.0.0 (c (n "mq_cli") (v "3773.0.0") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "posix_mq") (r "^3771.0.0") (d #t) (k 0)))) (h "0z5bwgrw10pl4rv4yy4xnniblxfbvclzck3mbhccsssvrxsj0i9q")))

