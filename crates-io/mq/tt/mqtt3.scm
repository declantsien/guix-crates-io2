(define-module (crates-io mq tt mqtt3) #:use-module (crates-io))

(define-public crate-mqtt3-0.1.0 (c (n "mqtt3") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "0lnyvcnrd3w7fzm0wd5bg1zywmbibr0bxz5aj3zfnhd54nr4sdaz")))

(define-public crate-mqtt3-0.1.1 (c (n "mqtt3") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "0d42rb2wvvp0qikd987fwqcs2334vj815gdpxpnpa3bn9nkfzq35")))

(define-public crate-mqtt3-0.1.2 (c (n "mqtt3") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "12n5nz349i8x7hal89h88f4xw1mqxavbnwh9v9748d2pj845fk32")))

(define-public crate-mqtt3-0.1.3 (c (n "mqtt3") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "17g2nvbf05g7xnvzwrd5sa2mkjmgj8hpdrbzphlxf6bym7nn676d")))

(define-public crate-mqtt3-0.1.4 (c (n "mqtt3") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "0m06ff1hnr5qx203apiqy8j380faz41s4d7dvmpgc7hr318gm3b2")))

