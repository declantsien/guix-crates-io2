(define-module (crates-io mq tt mqtterminal-server) #:use-module (crates-io))

(define-public crate-mqtterminal-server-0.1.0 (c (n "mqtterminal-server") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1njl652788zhxhry2shk4bdn2iqpi8rjc5gmmas3p1g068fpzrvz") (f (quote (("vendored-openssl" "openssl/vendored"))))))

