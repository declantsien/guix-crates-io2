(define-module (crates-io mq tt mqtt311) #:use-module (crates-io))

(define-public crate-mqtt311-0.1.4 (c (n "mqtt311") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "0ci03f123g5l1qgrzvz5msnv9f7dn581nwpi2fs5x6zylp8jjsnl")))

(define-public crate-mqtt311-0.2.0 (c (n "mqtt311") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.13") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0khdxzan922s401r7y428416v0ccinck1jfkhg38j6axjvnniayn")))

