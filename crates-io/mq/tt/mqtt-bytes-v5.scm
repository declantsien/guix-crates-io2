(define-module (crates-io mq tt mqtt-bytes-v5) #:use-module (crates-io))

(define-public crate-mqtt-bytes-v5-0.1.0 (c (n "mqtt-bytes-v5") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "14g9vyanc1lcapvy35a533yssnh3v7ganqpn3zcjvq3wva6wk5vj") (f (quote (("default") ("cow_string") ("boxed_string") ("binary_string"))))))

(define-public crate-mqtt-bytes-v5-0.1.1 (c (n "mqtt-bytes-v5") (v "0.1.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0wlsxfp0czxpg5cwv8afjkc9yjfnwl0fqg2ipwm4ypsm63npldwv") (f (quote (("default") ("cow_string") ("boxed_string") ("binary_string"))))))

(define-public crate-mqtt-bytes-v5-0.1.2 (c (n "mqtt-bytes-v5") (v "0.1.2") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0w9jhw53f3qd44541pdx861cc1p6c91xkr6dq58573k3if600q5j") (f (quote (("default") ("cow_string") ("boxed_string") ("binary_string"))))))

(define-public crate-mqtt-bytes-v5-0.1.3 (c (n "mqtt-bytes-v5") (v "0.1.3") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0hjly13czm3d02xff0n8d6kxv0if8wbn2r35iycifj851q8fbwfb") (f (quote (("default") ("cow_string") ("boxed_string") ("binary_string"))))))

