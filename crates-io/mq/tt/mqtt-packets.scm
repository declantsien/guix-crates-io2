(define-module (crates-io mq tt mqtt-packets) #:use-module (crates-io))

(define-public crate-mqtt-packets-0.1.0 (c (n "mqtt-packets") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "1zj0x9fdbx178gy9gmp9a2mp6qgrqb6mgvdnmfdfqfjq53gkm2dw") (f (quote (("v_3_1_1"))))))

