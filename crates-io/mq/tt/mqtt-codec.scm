(define-module (crates-io mq tt mqtt-codec) #:use-module (crates-io))

(define-public crate-mqtt-codec-0.1.0 (c (n "mqtt-codec") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "string") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "0rpi78c4ipkj1s7sgjpd24m5d73qjy3a1xi5i2ynbsqxh4rajx79")))

(define-public crate-mqtt-codec-0.2.0 (c (n "mqtt-codec") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "string") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "0gpqpk6qkkllxryx92x33i9ib7h72sig5sm8qyq6657g47n6rhjz")))

(define-public crate-mqtt-codec-0.3.0 (c (n "mqtt-codec") (v "0.3.0") (d (list (d (n "actix-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5.2") (d #t) (k 0)) (d (n "bytestring") (r "^0.1.0") (d #t) (k 0)))) (h "1rlxrjfg6zz42bgsvgv69gy6fikkjsaciwf11zdab8akx7cq7rjs")))

