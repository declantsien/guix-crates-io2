(define-module (crates-io mq tt mqttrust) #:use-module (crates-io))

(define-public crate-mqttrust-0.0.1 (c (n "mqttrust") (v "0.0.1") (h "0gp4i26brxmm19azg9w3d1ialzrrah9f62izq8344s3nl3w1sxdb")))

(define-public crate-mqttrust-0.1.0 (c (n "mqttrust") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mqttrs") (r "^0.4.1") (k 0)))) (h "00mhnjywjl77m4illr9vn94g53byvy19xblzqzb4a084h97qs2pi")))

(define-public crate-mqttrust-0.2.0 (c (n "mqttrust") (v "0.2.0") (d (list (d (n "heapless") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mqttrs") (r "^0.4.1") (k 0)))) (h "1lsip3nq388xmn2zc0b1i5xxqdaqy8z24r33k99kk2n7jg9nraks")))

(define-public crate-mqttrust-0.3.0 (c (n "mqttrust") (v "0.3.0") (d (list (d (n "defmt") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (f (quote ("serde" "defmt-impl"))) (d #t) (k 0)))) (h "15c7vqm26hq6l3qwlh5xmr10hxff6vz1gdn3grmg3qw6kjq94n5y") (y #t)))

(define-public crate-mqttrust-0.4.0 (c (n "mqttrust") (v "0.4.0") (d (list (d (n "defmt") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p8rcz89991cj0bki6zf3qb1rky0cc33mp5yf3jwpa2pc4f4pwfx") (f (quote (("derive" "serde" "heapless/serde") ("defmt-impl" "defmt" "heapless/defmt-impl") ("default"))))))

(define-public crate-mqttrust-0.4.1 (c (n "mqttrust") (v "0.4.1") (d (list (d (n "defmt") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0z2i7f4rsvqvqxwdfpsq1b0wqkkaf0hrm2kdh7534dyq10rr9n2x") (f (quote (("derive" "serde" "heapless/serde") ("defmt-impl" "defmt" "heapless/defmt-impl") ("default"))))))

(define-public crate-mqttrust-0.5.0 (c (n "mqttrust") (v "0.5.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12zm1k4yx6hlgf5a8frq683bib24r2h1vm8393brfpficznvjmld") (f (quote (("derive" "serde" "heapless/serde") ("defmt-impl" "defmt" "heapless/defmt-impl") ("default"))))))

(define-public crate-mqttrust-0.5.1 (c (n "mqttrust") (v "0.5.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ff41cq3bl79syk2zh5d3fgb3s9xrjnx6zj55s5qhy81b4wawbf1") (f (quote (("derive" "serde" "heapless/serde") ("defmt-impl" "defmt" "heapless/defmt-impl") ("default"))))))

(define-public crate-mqttrust-0.5.2 (c (n "mqttrust") (v "0.5.2") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ggn1dbwaxkfqkb0yq3a5p5hgp8jcrbwgjs3i3xzsg16nrq573ds") (f (quote (("derive" "serde" "heapless/serde") ("defmt-impl" "defmt" "heapless/defmt-impl") ("default"))))))

(define-public crate-mqttrust-0.6.0 (c (n "mqttrust") (v "0.6.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ppd61y35qqqrncb0j3s9lkbnf1qndqwm85vfk2hy2yjh2vzfcmf") (f (quote (("derive" "serde" "heapless/serde") ("defmt-impl" "defmt" "heapless/defmt-impl") ("default"))))))

