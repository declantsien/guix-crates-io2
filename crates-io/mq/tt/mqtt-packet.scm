(define-module (crates-io mq tt mqtt-packet) #:use-module (crates-io))

(define-public crate-mqtt-packet-0.1.0 (c (n "mqtt-packet") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1jm18xj2sc9lsj47cdsnhg28ssixb7x72anpdwjbp92yii9b3fma")))

(define-public crate-mqtt-packet-0.2.0 (c (n "mqtt-packet") (v "0.2.0") (h "1gwfz33bn0akxm7vhvy20hbw77nzj672vcalzs712xlmba66x7lv")))

(define-public crate-mqtt-packet-0.3.0 (c (n "mqtt-packet") (v "0.3.0") (h "0zjsa1f1ym6fbqkz2z6nbhxq4l9nyx10c05x6bwhrplhz5gahd4c")))

