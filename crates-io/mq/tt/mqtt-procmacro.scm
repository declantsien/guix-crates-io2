(define-module (crates-io mq tt mqtt-procmacro) #:use-module (crates-io))

(define-public crate-mqtt-procmacro-1.0.0 (c (n "mqtt-procmacro") (v "1.0.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "132h21qc5a6gczdhg3vamy6017282a2za7n4985ab450cn6fxb14")))

