(define-module (crates-io mq tt mqtt-v5) #:use-module (crates-io))

(define-public crate-mqtt-v5-0.1.0 (c (n "mqtt-v5") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4") (d #t) (k 0)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "1bb5kzkxm59jszp61rh7gra18pmg270s5wm5yxfhvmswmhbbkksq") (f (quote (("default" "codec") ("codec" "tokio-util"))))))

(define-public crate-mqtt-v5-0.1.1 (c (n "mqtt-v5") (v "0.1.1") (d (list (d (n "base64") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (o #t) (d #t) (k 0)) (d (n "websocket-codec") (r "^0.3") (o #t) (d #t) (k 0)))) (h "193glmlh3dnmy7yn0dvphppc8gj417qdpxzvxawrpnbrrbkm89n2") (f (quote (("websocket" "codec" "websocket-codec" "sha1" "base64") ("default" "codec" "websocket") ("codec" "tokio-util"))))))

