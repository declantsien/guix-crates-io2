(define-module (crates-io mq tt mqtt-benchmark) #:use-module (crates-io))

(define-public crate-mqtt-benchmark-0.1.0 (c (n "mqtt-benchmark") (v "0.1.0") (d (list (d (n "awaitgroup") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gy2r4xm6zdlgxa5xs8ajm86fa5zkzy6j788m8gvn23d5q95vlm9")))

(define-public crate-mqtt-benchmark-0.1.1 (c (n "mqtt-benchmark") (v "0.1.1") (d (list (d (n "awaitgroup") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jnax5b3g9cjm271kzf4qnc7pp2fbykc53x1q59g39lakz9xjs51")))

(define-public crate-mqtt-benchmark-0.1.2 (c (n "mqtt-benchmark") (v "0.1.2") (d (list (d (n "awaitgroup") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1irdfmw54p94hqaqas8kgz3sj35rn0lv4npazka18zlp2kh1appp")))

(define-public crate-mqtt-benchmark-0.2.0 (c (n "mqtt-benchmark") (v "0.2.0") (d (list (d (n "awaitgroup") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rumqttc") (r "^0.24.0") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0a6bd44a7lfjc6gszaw1k8zqi2444i96w74pylx74zrq2lm3najx") (y #t)))

(define-public crate-mqtt-benchmark-0.2.1 (c (n "mqtt-benchmark") (v "0.2.1") (d (list (d (n "awaitgroup") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rumqttc") (r "^0.24.0") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0i66bmxh952gn7l40301bajyb29qhasf045n797rblj7db3rws67")))

(define-public crate-mqtt-benchmark-0.2.2 (c (n "mqtt-benchmark") (v "0.2.2") (d (list (d (n "awaitgroup") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rumqttc") (r "^0.24.0") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "11vpnlawdlppqyg7z2viwfmy1nvc6nv3j2hf82g53w3km6ddml5a")))

