(define-module (crates-io mq tt mqtt-sn) #:use-module (crates-io))

(define-public crate-mqtt-sn-0.1.0 (c (n "mqtt-sn") (v "0.1.0") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byte") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "16caqmgqnf5zib4qbsh6cwpxidnbw9mnjk2iyladd4xgyj9ih9zw")))

(define-public crate-mqtt-sn-0.2.0 (c (n "mqtt-sn") (v "0.2.0") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byte") (r "^0.2") (d #t) (k 0)) (d (n "defmt") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)))) (h "0ry05yygw7pbfkz2f86y009fi7gdzhwz9pypqdaqf14l52ijxf6m") (f (quote (("defmt-impl" "defmt" "heapless/defmt-impl"))))))

(define-public crate-mqtt-sn-0.2.1 (c (n "mqtt-sn") (v "0.2.1") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byte") (r "^0.2") (d #t) (k 0)) (d (n "defmt") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)))) (h "15ya9knnwfb8h003infl3yq0d9d56b3941rqqivvr6z376k76i4d") (f (quote (("defmt-impl" "defmt" "heapless/defmt-impl"))))))

(define-public crate-mqtt-sn-0.2.2 (c (n "mqtt-sn") (v "0.2.2") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byte") (r "^0.2") (d #t) (k 0)) (d (n "defmt") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)))) (h "0ayh1q4i5d526xz62lsm0vqq442csdyfhdjsbifaxvak1d1qhdsa") (f (quote (("defmt-impl" "defmt" "heapless/defmt-impl"))))))

