(define-module (crates-io mq tt mqttp) #:use-module (crates-io))

(define-public crate-mqttp-0.0.0 (c (n "mqttp") (v "0.0.0") (d (list (d (n "actix-mqtt-client") (r "^0.5.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.2.1") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "115zkpx50dj3n10wy81sbh33g5pyq923pi97d2xpcf7xyav5169p")))

