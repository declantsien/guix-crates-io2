(define-module (crates-io mq tt mqtt-proto) #:use-module (crates-io))

(define-public crate-mqtt-proto-0.0.1 (c (n "mqtt-proto") (v "0.0.1") (h "1yj8y8rzqcwvrjkz9pdrp9380ialslk08x3cnp0ffa2m0qqmjxyy")))

(define-public crate-mqtt-proto-0.1.0 (c (n "mqtt-proto") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.2.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "129skxn9sh66hrfyiykrsqjgk1zy43vjpn2l0ai09f8is4v7wp28") (f (quote (("std") ("default" "std"))))))

