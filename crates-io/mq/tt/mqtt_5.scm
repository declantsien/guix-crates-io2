(define-module (crates-io mq tt mqtt_5) #:use-module (crates-io))

(define-public crate-mqtt_5-0.1.0 (c (n "mqtt_5") (v "0.1.0") (d (list (d (n "bitreader_async") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "packattack") (r "^0.1.0") (d #t) (k 0)) (d (n "packattack-derive") (r "^0.3.1") (d #t) (k 0)))) (h "0daxjx75y633zyl85yfgz61prkz8m8dngm5nl83m5qg6ksf0dfyl")))

(define-public crate-mqtt_5-0.1.1 (c (n "mqtt_5") (v "0.1.1") (d (list (d (n "bitreader_async") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "packattack") (r "^0.1.0") (d #t) (k 0)) (d (n "packattack-derive") (r "^0.3.1") (d #t) (k 0)))) (h "07hg4cavxiciw0adqqjya83wbhcfghb4l139j9gax843kji1ygcc")))

