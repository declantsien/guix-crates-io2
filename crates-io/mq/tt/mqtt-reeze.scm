(define-module (crates-io mq tt mqtt-reeze) #:use-module (crates-io))

(define-public crate-mqtt-reeze-0.1.0 (c (n "mqtt-reeze") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rumqttc") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (d #t) (k 0)))) (h "153r1g8hfq6q947bxsmi38h2x8jikljmmw8l4qdq1qnpqm9mxvr7")))

(define-public crate-mqtt-reeze-0.2.0 (c (n "mqtt-reeze") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rumqttc") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (d #t) (k 0)))) (h "0dnslp8wn766xc8h81hnflyydbfb6hzay34pc4m2g7r5bs9zhhlr")))

