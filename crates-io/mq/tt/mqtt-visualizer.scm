(define-module (crates-io mq tt mqtt-visualizer) #:use-module (crates-io))

(define-public crate-mqtt-visualizer-0.1.0 (c (n "mqtt-visualizer") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "mqttrs") (r "^0.4") (d #t) (k 0)))) (h "11siglznhn9wqgphi1yc1qr2rfxpymgzqjp1x1k66kf5fx6kd4sy")))

(define-public crate-mqtt-visualizer-0.2.0 (c (n "mqtt-visualizer") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "mqttrs") (r "^0.4") (d #t) (k 0)))) (h "1fn8w5a57cb4h8wbz39dfnkqyzwwbmvs8ppm7dp9za2rvgjpcmwj")))

