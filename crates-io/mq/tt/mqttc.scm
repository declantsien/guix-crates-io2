(define-module (crates-io mq tt mqttc) #:use-module (crates-io))

(define-public crate-mqttc-0.1.0 (c (n "mqttc") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mqtt3") (r "^0.1") (d #t) (k 0)) (d (n "netopt") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0x6gsla9rwrn61xmdbnrnh92prf1jhjlp9byfzsfiz81xym5c2wk")))

(define-public crate-mqttc-0.1.1 (c (n "mqttc") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mqtt3") (r "^0.1") (d #t) (k 0)) (d (n "netopt") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1f80r19py8bc8k4p4vb4dlk15gh97dfd8hn3smy7il35xf3x8kgv")))

(define-public crate-mqttc-0.1.2 (c (n "mqttc") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mqtt3") (r "^0.1") (d #t) (k 0)) (d (n "netopt") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0sy1rjm0zzfqjm6lkbm73jnr0gs2hj0nw9qj7yadlbvvvg4ivnbp")))

(define-public crate-mqttc-0.1.3 (c (n "mqttc") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mqtt3") (r "^0.1") (d #t) (k 0)) (d (n "netopt") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1b87i811xpdm8rdwi7i9g1jx56pdgksrb0biv3hk796vcsg1wpzx")))

(define-public crate-mqttc-0.1.4 (c (n "mqttc") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mqtt3") (r "^0.1") (d #t) (k 0)) (d (n "netopt") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "163s1whj5wlbmgwsf968wz4ma9272dnfcfamv5zzkbf5v89wxiba")))

