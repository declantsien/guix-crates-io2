(define-module (crates-io mq tt mqtt_packet_3_5) #:use-module (crates-io))

(define-public crate-mqtt_packet_3_5-0.1.0 (c (n "mqtt_packet_3_5") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cpqihga142y0j25b6rsvl7jygwm8shjj485xskgsd73yxjyh6mg")))

(define-public crate-mqtt_packet_3_5-0.1.1 (c (n "mqtt_packet_3_5") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13fywgw9417spa8jbk0kj1hfcqizmrm63s6419nm53z4939vjiny")))

(define-public crate-mqtt_packet_3_5-0.2.1 (c (n "mqtt_packet_3_5") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14n5xavjrly99wqy3dkiglcgcnc91k3jzl30h3i9w7p5k847a8mn") (f (quote (("serde_support" "serde")))) (y #t)))

(define-public crate-mqtt_packet_3_5-0.2.2 (c (n "mqtt_packet_3_5") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0g9rn2n94jagbhpykmlrknpw947hpwymaxn60xm72xch56w49w9p") (f (quote (("serde_support" "serde"))))))

