(define-module (crates-io mq tt mqtt4bytes) #:use-module (crates-io))

(define-public crate-mqtt4bytes-0.1.0 (c (n "mqtt4bytes") (v "0.1.0") (h "10jiab9rgxmiw8bhkgl6qkrfwaibalk5rj2gb0djw5sv97hwap5h")))

(define-public crate-mqtt4bytes-0.1.1 (c (n "mqtt4bytes") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "1hf3habxyiirawifwxnsqsl9hxya0gn235qb3x6p8mzlh3nyjr03") (f (quote (("std" "thiserror" "tokio-util") ("default" "std"))))))

(define-public crate-mqtt4bytes-0.1.2 (c (n "mqtt4bytes") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "0g4wj3dhvqswp826fbw54wgw3hswysb9269lwkbsc26w7d86pmxh") (f (quote (("std" "thiserror" "tokio-util") ("default" "std"))))))

(define-public crate-mqtt4bytes-0.1.3 (c (n "mqtt4bytes") (v "0.1.3") (d (list (d (n "bytes") (r "^0.5") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "0ni37clqs2d3ws1vf2lqhv7df0wk7xwh4rwz2rwyg9iabrx884aa") (f (quote (("std" "thiserror" "tokio-util") ("default" "std"))))))

(define-public crate-mqtt4bytes-0.1.4 (c (n "mqtt4bytes") (v "0.1.4") (d (list (d (n "bytes") (r "^0.5") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "16q9mk545yyfl516p4daydwr5j20sss8i5hi7v87sc414hbillja") (f (quote (("std" "thiserror" "tokio-util") ("default" "std"))))))

(define-public crate-mqtt4bytes-0.1.5 (c (n "mqtt4bytes") (v "0.1.5") (d (list (d (n "bytes") (r "^0.5") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "05d3k6j09cwzvask2g78lwi2v5d1vfa0jlwgv57d1i8qf0310r31")))

(define-public crate-mqtt4bytes-0.1.6 (c (n "mqtt4bytes") (v "0.1.6") (d (list (d (n "bytes") (r "^0.5") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "17y9lmc35jl71zibxmdmrs08lhs4qbf29zlfysrk5lkc2mnqdpnz")))

(define-public crate-mqtt4bytes-0.1.7 (c (n "mqtt4bytes") (v "0.1.7") (d (list (d (n "bytes") (r "^0.5") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1gknxsr3w5cdvzb19w5qslhsc57xq4cjh80vxdlxfic6llhpp6x1")))

(define-public crate-mqtt4bytes-0.1.8 (c (n "mqtt4bytes") (v "0.1.8") (d (list (d (n "bytes") (r "^0.5") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0pm07mc4sj506paniqabqpny0zksp55rh39w3rlm43r8m0rbz3sy") (y #t)))

(define-public crate-mqtt4bytes-0.2.0 (c (n "mqtt4bytes") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "152gxv3wxw7x961kv821cszwwbq33j4k7xz568jlv4g4mhc9a5sc")))

(define-public crate-mqtt4bytes-0.3.0 (c (n "mqtt4bytes") (v "0.3.0") (d (list (d (n "bytes") (r "^0.5") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0xankfisiqr6fn3j4z6kk2f2ij1xp7j742ikjq7lhlcp5jvifzrz")))

(define-public crate-mqtt4bytes-0.4.0 (c (n "mqtt4bytes") (v "0.4.0") (d (list (d (n "bytes") (r "^0.6") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1bwmxbk52dziwdghi7xzbrvbva75xpqivh3wvdpvizp6r5hq160m")))

