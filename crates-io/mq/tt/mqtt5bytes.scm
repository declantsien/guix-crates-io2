(define-module (crates-io mq tt mqtt5bytes) #:use-module (crates-io))

(define-public crate-mqtt5bytes-0.1.0 (c (n "mqtt5bytes") (v "0.1.0") (h "10q8qfipcpi03x1rcd951s81dbp26shb35z2krlwsmsspbya88qr")))

(define-public crate-mqtt5bytes-0.1.1 (c (n "mqtt5bytes") (v "0.1.1") (h "0mvwkap9jh1l70g0qqgy0dyc4vhxw4daklpggjwi9nvd5sndsicx")))

