(define-module (crates-io mq tt mqtt_macro) #:use-module (crates-io))

(define-public crate-mqtt_macro-0.1.0 (c (n "mqtt_macro") (v "0.1.0") (d (list (d (n "mqtt-procmacro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bwasq1anxxpxc4kykcshgncwq6f77hil6yvz4p5w4h0z14zag72") (f (quote (("serde_default"))))))

