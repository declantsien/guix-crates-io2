(define-module (crates-io mq tt mqttbytes) #:use-module (crates-io))

(define-public crate-mqttbytes-0.1.0 (c (n "mqttbytes") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1724i4bqc0rv05i3cykb5sliqmyn0x4pdw5cp59q6fd574b81dz9") (f (quote (("v5"))))))

(define-public crate-mqttbytes-0.2.0 (c (n "mqttbytes") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0bvrcxs78rl5c9f37l0v2n72wc2nl9mq4kh28kp31qfqaqa76kv8") (f (quote (("v5"))))))

(define-public crate-mqttbytes-0.2.1 (c (n "mqttbytes") (v "0.2.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1nl1h6sv0vcdriynl5fjq0xw79iyxhccs0r14r9n2jgi6gmi3m40") (f (quote (("v5"))))))

(define-public crate-mqttbytes-0.3.0 (c (n "mqttbytes") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1h4a6b23jr8zp659gwpq3a0d8wrqaxyxjhxq9py8lwqdvds9xpqi") (f (quote (("v5"))))))

(define-public crate-mqttbytes-0.4.0 (c (n "mqttbytes") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0nnyq5706h2xakpgny2j0sp3a72a8rfdhyibp2bnsra444zmf46p") (f (quote (("v5"))))))

(define-public crate-mqttbytes-0.5.0 (c (n "mqttbytes") (v "0.5.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0a6cklf48j723vga7qznvslxgh91spgh3y6xvg2miclfyf33ly8j") (f (quote (("v5"))))))

(define-public crate-mqttbytes-0.6.0 (c (n "mqttbytes") (v "0.6.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0lr8fmhfgll3mbf7mfn8aciyaxx44bilcmzzfi6m9q989v93kgd7") (f (quote (("v5"))))))

