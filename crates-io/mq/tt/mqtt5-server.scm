(define-module (crates-io mq tt mqtt5-server) #:use-module (crates-io))

(define-public crate-mqtt5-server-0.1.0 (c (n "mqtt5-server") (v "0.1.0") (h "046yx9h49kkqk5lizrjf8wgkkz1iyrmxc8wxwz7mp731a1mdg1g2")))

(define-public crate-mqtt5-server-0.2.0 (c (n "mqtt5-server") (v "0.2.0") (h "07a18rvqzw03mpc9b3yqvv7b54h1s03p0hzf8xc9zhhm5jvcwf69")))

