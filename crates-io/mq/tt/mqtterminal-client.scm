(define-module (crates-io mq tt mqtterminal-client) #:use-module (crates-io))

(define-public crate-mqtterminal-client-0.1.0 (c (n "mqtterminal-client") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0g2p2haacmw7rrvbfk8v34lwsd1q6xljnx2bnisvwjldqghas2c9") (f (quote (("vendored-openssl" "openssl/vendored"))))))

