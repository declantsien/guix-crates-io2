(define-module (crates-io mq tt mqtt-manager) #:use-module (crates-io))

(define-public crate-mqtt-manager-1.0.0 (c (n "mqtt-manager") (v "1.0.0") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "rumqttc") (r "^0.24.0") (f (quote ("url"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt-multi-thread" "signal"))) (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1h0bw422bjjba9y6sm728s0q1ww6ynnixn44z9mdzg0jl9dqsk1v")))

(define-public crate-mqtt-manager-1.0.1 (c (n "mqtt-manager") (v "1.0.1") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "rumqttc") (r "^0.24.0") (f (quote ("url"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt-multi-thread" "signal"))) (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1rqak0hfgsqz1qfl0lzgzvwbvcxrvspqc1g5p0346zb7g9410kzh")))

