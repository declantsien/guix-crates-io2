(define-module (crates-io mq tt mqtt-v5-fork) #:use-module (crates-io))

(define-public crate-mqtt-v5-fork-0.2.0 (c (n "mqtt-v5-fork") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.11") (d #t) (k 0)) (d (n "sha1") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)) (d (n "websocket-codec") (r "^0.5") (o #t) (d #t) (k 0)))) (h "07xk8w2g08pk8bh3m8sqi8h6fwxw61wl9z7q612b2j7w5d63wwiw") (f (quote (("websocket" "codec" "websocket-codec" "sha1" "base64") ("default" "codec" "websocket") ("codec" "tokio-util"))))))

