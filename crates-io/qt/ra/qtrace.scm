(define-module (crates-io qt ra qtrace) #:use-module (crates-io))

(define-public crate-qtrace-0.1.0 (c (n "qtrace") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0pd2hidhwww384mvhc1lsb92xsfgyr6bxpji6hxlwfxjh52xsdxi")))

