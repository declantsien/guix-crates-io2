(define-module (crates-io qt ra qtranslation) #:use-module (crates-io))

(define-public crate-qtranslation-0.1.0 (c (n "qtranslation") (v "0.1.0") (d (list (d (n "qmetaobject") (r "^0.2.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.1") (d #t) (k 0)))) (h "0s7d2w2clq3k4j08c75qq9bylhj0mfz4n3873agl57r1gaji00sp") (y #t)))

(define-public crate-qtranslation-0.1.1 (c (n "qtranslation") (v "0.1.1") (d (list (d (n "qmetaobject") (r "^0.2.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.1") (d #t) (k 0)))) (h "175yi9bwj4h4c071q15xw5gay41dgwa96hznkz2ff6p2p7rn3nj0") (y #t)))

(define-public crate-qtranslation-0.1.11 (c (n "qtranslation") (v "0.1.11") (d (list (d (n "qmetaobject") (r "^0.2.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.1") (d #t) (k 0)))) (h "063cwdhiyph0ixcpr1q1zb6c8p6zfzrg70rg55095jbsdn73lk8i") (y #t)))

(define-public crate-qtranslation-0.1.2 (c (n "qtranslation") (v "0.1.2") (d (list (d (n "qmetaobject") (r "^0.2.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.1") (d #t) (k 0)))) (h "1wcqgns469kznb1v0xnssmi3wk711wflq974b6s9crh3akmi2zni")))

(define-public crate-qtranslation-0.1.3 (c (n "qtranslation") (v "0.1.3") (d (list (d (n "qmetaobject") (r "^0.2.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.1") (d #t) (k 0)))) (h "10ksln1p3xfwvna6lax2wvqslp1w5pdx1pjppqvs4992nkxc597v")))

(define-public crate-qtranslation-0.1.4 (c (n "qtranslation") (v "0.1.4") (d (list (d (n "qmetaobject") (r "^0.2.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.1") (d #t) (k 0)))) (h "14icqm6ahi2zcvkz9m51chrzyhb6yp1p9hwxd8p0bvvrml6apqr6") (y #t)))

(define-public crate-qtranslation-0.1.5 (c (n "qtranslation") (v "0.1.5") (d (list (d (n "qmetaobject") (r "^0.2.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.1") (d #t) (k 0)))) (h "1lagr1sqgc7a27djsilzg06iscpmddlk35ln5icfzlncfcqqmwib")))

(define-public crate-qtranslation-0.1.6 (c (n "qtranslation") (v "0.1.6") (d (list (d (n "qmetaobject") (r "^0.2.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.1") (d #t) (k 0)))) (h "0adsc4z3pazn1nhbcwmilcg67fcll58320bir1kzhyfd69v7civ0")))

(define-public crate-qtranslation-0.1.7 (c (n "qtranslation") (v "0.1.7") (d (list (d (n "qmetaobject") (r "^0.2.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.1") (d #t) (k 0)))) (h "0sfqgyk9h8y32f2xbbp2irlqs680pgfv4znw47r5s4xf36j2rghk")))

