(define-module (crates-io qt _b qt_build_tools) #:use-module (crates-io))

(define-public crate-qt_build_tools-0.0.0 (c (n "qt_build_tools") (v "0.0.0") (d (list (d (n "cpp_to_rust") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "00qjf47iw97hzxq5kgfq4d5h6gng351ml50b9g9l9zfn7a40rf20")))

(define-public crate-qt_build_tools-0.0.1 (c (n "qt_build_tools") (v "0.0.1") (d (list (d (n "cpp_to_rust") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0il0y1p4gwhwk04ddj5l6yw1z5kvqx2r5rqbhim73p116x5xv1x5")))

(define-public crate-qt_build_tools-0.1.0 (c (n "qt_build_tools") (v "0.1.0") (d (list (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0iwx0knf0yh74yj8bxa118018vhwrkm3sl3pmn7ywxr65bvq6hvh")))

(define-public crate-qt_build_tools-0.1.1 (c (n "qt_build_tools") (v "0.1.1") (d (list (d (n "compress") (r "^0.1.2") (d #t) (k 0)) (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.7") (d #t) (k 0)) (d (n "select") (r "^0.3.0") (d #t) (k 0)))) (h "1yrn2ipfjgq3ak0m5800lryi7dy680raxnvbw2pmgr0w9baf31q3")))

(define-public crate-qt_build_tools-0.1.2 (c (n "qt_build_tools") (v "0.1.2") (d (list (d (n "compress") (r "^0.1.2") (d #t) (k 0)) (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.7") (d #t) (k 0)) (d (n "select") (r "^0.3.0") (d #t) (k 0)))) (h "19qcrxsdz15hcx83inz638hc45hgzfr6wlgmhah3yj1ms1ddkh4v")))

(define-public crate-qt_build_tools-0.1.3 (c (n "qt_build_tools") (v "0.1.3") (d (list (d (n "compress") (r "^0.1.2") (d #t) (k 0)) (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.7") (d #t) (k 0)) (d (n "select") (r "^0.3.0") (d #t) (k 0)))) (h "0a1c14n9ajhh6zfznk912wgqyvzkajz0q617h5wjrb5b0gb6srqm")))

(define-public crate-qt_build_tools-0.2.0 (c (n "qt_build_tools") (v "0.2.0") (d (list (d (n "cpp_to_rust_build_tools") (r "^0.2.0") (d #t) (k 0)) (d (n "qt_generator_common") (r "^0.2.0") (d #t) (k 0)))) (h "02pr9ban1h1vjl25kym6az3pd3zxvdkihvbjhh2ba8r2a5cbmc5j")))

(define-public crate-qt_build_tools-0.2.1 (c (n "qt_build_tools") (v "0.2.1") (d (list (d (n "cpp_to_rust_build_tools") (r "^0.2.1") (d #t) (k 0)) (d (n "qt_generator_common") (r "^0.2.0") (d #t) (k 0)))) (h "0zlri4gx73a7f5xjd6lh6y6w9sgjnbsxspv4n0y3xwjkbpss97w4")))

(define-public crate-qt_build_tools-0.2.3 (c (n "qt_build_tools") (v "0.2.3") (d (list (d (n "cpp_to_rust_build_tools") (r "^0.2.3") (d #t) (k 0)) (d (n "qt_generator_common") (r "^0.2.3") (d #t) (k 0)))) (h "1nz2nrvv689s2aqb8chpnkcn8fgpa992hfa7s230yjgr5894hpxy")))

(define-public crate-qt_build_tools-0.2.4 (c (n "qt_build_tools") (v "0.2.4") (d (list (d (n "cpp_to_rust_build_tools") (r "^0.2.3") (d #t) (k 0)) (d (n "qt_generator_common") (r "^0.2.3") (d #t) (k 0)))) (h "1zdibdn7wk030pgap4flacjd1pmv3cv0cmck5j49vz0g9z9kbmwv")))

