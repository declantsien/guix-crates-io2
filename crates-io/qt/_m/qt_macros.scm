(define-module (crates-io qt _m qt_macros) #:use-module (crates-io))

(define-public crate-qt_macros-0.1.0-alpha.1 (c (n "qt_macros") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "152skryfcc75v5mlbqfdsripskjfqzc82zil32fvzvlrnxxqzzlg")))

(define-public crate-qt_macros-0.1.0-alpha.2 (c (n "qt_macros") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n4p8xzs6q7gma07z312jdyqh766rrxakhlf4y4z0na0qkwr00qb")))

(define-public crate-qt_macros-0.1.0 (c (n "qt_macros") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wda75gc5gsjdpy3hgbf2vrh44lg5qifvngq6zlm3md9nvlbf93d")))

(define-public crate-qt_macros-0.1.1 (c (n "qt_macros") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qxa5vmbppcvf0ac1s74a1mlnc00cnf4ksgg650dy95njphzrdj2")))

