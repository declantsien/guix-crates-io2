(define-module (crates-io qt y- qty-macros) #:use-module (crates-io))

(define-public crate-qty-macros-0.2.0 (c (n "qty-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1kwrk954rbdh5jz9a2gjc8lcdmnjnrix85lvcj0y9vfrz17wknf8")))

(define-public crate-qty-macros-0.2.1 (c (n "qty-macros") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0xk4k4s8anra94dbfgsdky1a6fhy28nymacvmfg2cn1dci7v741q")))

(define-public crate-qty-macros-0.3.0 (c (n "qty-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1sir83m2wyyj23565mqxw60jv5qr5wgp0wibcf60nk8avfhc8dly")))

(define-public crate-qty-macros-0.4.0 (c (n "qty-macros") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1lsvdcrd6888py9mpcc8wn3x6nlhs0knpjwjz39qv6km1lhp2bhj")))

(define-public crate-qty-macros-0.5.0 (c (n "qty-macros") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1k2yjv01bxan7v5wb2583z3mi8c5sja2hfj2wk5d8bza5j8lpz7g")))

(define-public crate-qty-macros-0.6.0 (c (n "qty-macros") (v "0.6.0") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1r2hcps5d19s3gdylcdqhph66837h3hxrw0jcgykqrmvrka21f4s")))

(define-public crate-qty-macros-0.7.0 (c (n "qty-macros") (v "0.7.0") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "04igjdd163mrnp04kiwh46svhvmxffrfa8y0f3jbjc4bs1vdw4kh")))

(define-public crate-qty-macros-0.9.0 (c (n "qty-macros") (v "0.9.0") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "154kp2crwnz68ibiaznaca2ina0vli62ay8fqcn5ln7rks080pn1")))

(define-public crate-qty-macros-0.9.1 (c (n "qty-macros") (v "0.9.1") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0qm9zah6pqdsw73p5jxr9n5w7zcdzgij3fm8wgp9ag39dns1jfjl")))

(define-public crate-qty-macros-0.11.0 (c (n "qty-macros") (v "0.11.0") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1yjb8537khnm4m98cjv1b16rwjja8dp87af8pavz42bcwylgj4mp")))

(define-public crate-qty-macros-0.11.3 (c (n "qty-macros") (v "0.11.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0x5vj889d5bvpvahn7az45zw1gm52l9xmvcdrg9hhfbqnqh1alni")))

(define-public crate-qty-macros-0.12.0 (c (n "qty-macros") (v "0.12.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1wy82cnq9v01p6g9ds9k1i49ciiawfsgn92z1smbx1nfln5y5vf4")))

