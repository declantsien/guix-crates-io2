(define-module (crates-io qt md qtmd) #:use-module (crates-io))

(define-public crate-qtmd-0.1.0 (c (n "qtmd") (v "0.1.0") (d (list (d (n "tqdm") (r "^0.6.0") (d #t) (k 0)))) (h "1f9g0sj1zz8if1xjwbdbzr0f005d4dv13flhq5c4dk8ci0kc1wic")))

(define-public crate-qtmd-0.1.1 (c (n "qtmd") (v "0.1.1") (d (list (d (n "tqdm") (r "^0.6.0") (d #t) (k 0)))) (h "0pzana05gd6hplaxf8l2lrzbfwdljkil48mlcmbj5dmzm1k4vk0l")))

