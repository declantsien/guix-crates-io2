(define-module (crates-io qt _c qt_core_custom_events) #:use-module (crates-io))

(define-public crate-qt_core_custom_events-0.0.1 (c (n "qt_core_custom_events") (v "0.0.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "~0.2") (d #t) (k 1)) (d (n "qt_core") (r "~0.2") (d #t) (k 0)))) (h "0wr883q5vhy0s6wwji3zmwzcq679dizmk6iaxzadpcwvhw0hl3v0")))

(define-public crate-qt_core_custom_events-0.0.2 (c (n "qt_core_custom_events") (v "0.0.2") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "~0.2") (d #t) (k 1)) (d (n "qt_core") (r "~0.2") (d #t) (k 0)))) (h "1m1jnd3wprfbqcsfa15njii6saqnbaw4r71hx5icp10lmzml8jfc")))

(define-public crate-qt_core_custom_events-0.1.0 (c (n "qt_core_custom_events") (v "0.1.0") (d (list (d (n "cpp_core") (r "~0.5") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "qt_core") (r "~0.4") (d #t) (k 0)) (d (n "qt_ritual_build") (r "~0.3") (d #t) (k 1)))) (h "0vdwrqgifvh63l8svkw05sz3qs44ffyjg3rcyjkwsb6kk1n5mjdr")))

(define-public crate-qt_core_custom_events-0.1.1 (c (n "qt_core_custom_events") (v "0.1.1") (d (list (d (n "cpp_core") (r "~0.5") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "qt_core") (r "~0.4") (d #t) (k 0)) (d (n "qt_ritual_build") (r "~0.3") (d #t) (k 1)))) (h "1m01nvzvi5ybydx9ai1qw8k2v8kmrwc3kivnqrx9r9b7imlcqm65")))

(define-public crate-qt_core_custom_events-0.2.0 (c (n "qt_core_custom_events") (v "0.2.0") (d (list (d (n "cpp_core") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.5.0-alpha.1") (d #t) (k 1)))) (h "0jbv10ha4ldk2z46pz7wgk0bcr2a3aa9sfbwcwvybgij7z26kxsa")))

