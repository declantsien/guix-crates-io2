(define-module (crates-io qt _c qt_core) #:use-module (crates-io))

(define-public crate-qt_core-0.0.0 (c (n "qt_core") (v "0.0.0") (d (list (d (n "cpp_to_rust") (r "^0.1") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j2z55cy2yjb3zpd6lk01hi8dm7i8vpx7bdl3rhkkq2wqzd1hjhs")))

(define-public crate-qt_core-0.0.1 (c (n "qt_core") (v "0.0.1") (d (list (d (n "cpp_to_rust") (r "^0.1") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06f1mdpycnmir5my3gj0b1q7yhf29b6f800i05ykrd20b52pywhd")))

(define-public crate-qt_core-0.1.0 (c (n "qt_core") (v "0.1.0") (d (list (d (n "cpp_to_rust") (r "^0.4") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.0") (d #t) (k 1)))) (h "1wmjf6vq04hjy1agk94pnw4vdjc7i57dfx4lp3ivhl2mr0miak9z")))

(define-public crate-qt_core-0.1.1 (c (n "qt_core") (v "0.1.1") (d (list (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.1") (d #t) (k 1)))) (h "13rrs6g7rqcbarcw1bvpyal17aqvql2a7zccy6kl22n0hm83pmb3")))

(define-public crate-qt_core-0.1.2 (c (n "qt_core") (v "0.1.2") (d (list (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.1") (d #t) (k 1)))) (h "02qllszlaq8k3q1kvrxh21c74dwkn90kkl67wg74ds3jj459yw82")))

(define-public crate-qt_core-0.1.3 (c (n "qt_core") (v "0.1.3") (d (list (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.1") (d #t) (k 1)))) (h "1nn2chqadi1x2affgrl2c3p1x4zcp0gw5128c8n4c3gj18bm5hgb")))

(define-public crate-qt_core-0.2.0 (c (n "qt_core") (v "0.2.0") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.0") (d #t) (k 1)))) (h "081g6c0m7bi52ng58qq1js5piv6z68cip2fwkmf86j5q4dy4wjbj")))

(define-public crate-qt_core-0.2.1 (c (n "qt_core") (v "0.2.1") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.0") (d #t) (k 1)))) (h "1cwylj93gnggh65knbcpncv4sww7i3dbgad6h5hqdxhvjwpijh9p")))

(define-public crate-qt_core-0.2.2 (c (n "qt_core") (v "0.2.2") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.1") (d #t) (k 1)))) (h "1cs25bnh2s60awf1v8gv9f1hx3rsw12wfw9fnla3819mpny6w74n")))

(define-public crate-qt_core-0.2.3 (c (n "qt_core") (v "0.2.3") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.3") (d #t) (k 1)))) (h "0rswi5dxxq73in4ydpz7jylhz94l61s8mvmk2k3wa37icrrh868z")))

(define-public crate-qt_core-0.3.0 (c (n "qt_core") (v "0.3.0") (d (list (d (n "cpp_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.1.0") (d #t) (k 1)))) (h "0g3jcy0bdskyd1ryqv4xi87vc3vgjzil1q4c8whmsg8r56rx5frl")))

(define-public crate-qt_core-0.4.0-alpha1 (c (n "qt_core") (v "0.4.0-alpha1") (d (list (d (n "cpp_core") (r "^0.4.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.2.0") (d #t) (k 1)))) (h "0kvx3dpm5wg8d7300cz7cagdc7c8y1lk8zbyli1lzbqw1hkx15dc") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_core-0.4.0-alpha2 (c (n "qt_core") (v "0.4.0-alpha2") (d (list (d (n "cpp_core") (r "^0.4.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.2.0") (d #t) (k 1)))) (h "0198c12lnbp1r10pjmj9qk6z9pzpdw9xp6hfdhijpq2v5b6flppp") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_core-0.4.0 (c (n "qt_core") (v "0.4.0") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "1qlc24v9kzf6i4whdmy4d3n2rkpl977wl30k5l2bswv6yq34pxab") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_core-0.4.1 (c (n "qt_core") (v "0.4.1") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "0byx1p1y87fl4acvfkjapx7878cn2jsqijfv035ywpw6fhldb6x0") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_core-0.5.0-alpha.1 (c (n "qt_core") (v "0.5.0-alpha.1") (d (list (d (n "cpp_core") (r "^0.5.1") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.4.0") (d #t) (k 1)))) (h "1w7h0nk724qrzbq7w3f6jl83ngpv4qx6l0brjl0dxwrphrnbb2ac") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_core-0.5.0-alpha.2 (c (n "qt_core") (v "0.5.0-alpha.2") (d (list (d (n "cpp_core") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "qt_macros") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.5.0-alpha.1") (d #t) (k 1)))) (h "0bf8ns6if00m16m0s89wma5gbnrpfca2fm732y2fr6lppjdr961d") (f (quote (("ritual_rustdoc_nightly") ("ritual_rustdoc"))))))

(define-public crate-qt_core-0.5.0 (c (n "qt_core") (v "0.5.0") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "qt_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.5.0") (d #t) (k 1)))) (h "0yisv3c5yiq4dirpki0szrcm1llwpgwl81gzhzvbr6p1kr7ai3vp") (f (quote (("ritual_rustdoc_nightly") ("ritual_rustdoc"))))))

