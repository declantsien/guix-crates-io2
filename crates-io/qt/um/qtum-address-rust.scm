(define-module (crates-io qt um qtum-address-rust) #:use-module (crates-io))

(define-public crate-qtum-address-rust-0.1.0 (c (n "qtum-address-rust") (v "0.1.0") (d (list (d (n "basex-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.7.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "01ia53h76wr3jszw7064czx0hmrxj1ac8p1w3079w2bz5f0sskwc")))

(define-public crate-qtum-address-rust-0.1.1 (c (n "qtum-address-rust") (v "0.1.1") (d (list (d (n "basex-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.7.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "02l4s00sd13hlhk2wbpfa2diqxv9ws2iy9prz005zcgyrbg80jkk")))

(define-public crate-qtum-address-rust-0.2.0 (c (n "qtum-address-rust") (v "0.2.0") (d (list (d (n "basex-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.7.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0mgnv82nzjc7m5qcxvi4ckjj52dc8hs6wpmpzri40n47swapb8w7")))

