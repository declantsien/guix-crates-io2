(define-module (crates-io qt -c qt-cb) #:use-module (crates-io))

(define-public crate-qt-cb-0.1.0 (c (n "qt-cb") (v "0.1.0") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "cpp_core") (r "^0.6.0") (d #t) (k 2)) (d (n "qt-cb-sys") (r "=0.1.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 2)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 2)))) (h "0r9a86cnkcfki4hb2jn8pvywa7lskg6zvmr27x3sdzpzp591idxv") (y #t)))

(define-public crate-qt-cb-0.1.1 (c (n "qt-cb") (v "0.1.1") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "cpp_core") (r "^0.6.0") (d #t) (k 2)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 2)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 2)))) (h "17k9pbby9npmiiq1cpcf0mrx0m2prdbwhm79a82s0asj1vnf0c9y")))

(define-public crate-qt-cb-0.1.2 (c (n "qt-cb") (v "0.1.2") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "cpp_core") (r "^0.6.0") (d #t) (k 2)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 2)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 2)))) (h "168nh7ql1ag1vkjhk3bg5admgbqli4sisk2d4jmy0sw7zms07l29")))

(define-public crate-qt-cb-0.1.3 (c (n "qt-cb") (v "0.1.3") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "cpp_core") (r "^0.6.0") (d #t) (k 2)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 2)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 2)))) (h "1k3asjqzv9cd42ibjp47v96r1h04cj7cqhkf9f6dbjddixwdh01w")))

(define-public crate-qt-cb-0.1.4 (c (n "qt-cb") (v "0.1.4") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "cpp_core") (r "^0.6.0") (d #t) (k 2)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 2)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 2)))) (h "184z71g7ny2vxqzhbk6hk64bbaqv3wbk7rzkz27gz9h8lgdi9dxl")))

(define-public crate-qt-cb-0.1.5 (c (n "qt-cb") (v "0.1.5") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "cpp_core") (r "^0.6.0") (d #t) (k 2)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 2)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 2)))) (h "15h44wmgda28wnqacxyw7kxdda3hgk72624nz51ix8p6lhjymw8j")))

(define-public crate-qt-cb-0.1.6 (c (n "qt-cb") (v "0.1.6") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "cpp_core") (r "^0.6.0") (d #t) (k 2)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 2)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_widgets") (r "^0.5.0") (d #t) (k 2)))) (h "0bi7yfjapw6cx0m14p6pb5kqxxk0vv56rbgrd8dli3q41qpsq79l")))

