(define-module (crates-io qt _g qt_generator_common) #:use-module (crates-io))

(define-public crate-qt_generator_common-0.0.0 (c (n "qt_generator_common") (v "0.0.0") (d (list (d (n "cpp_to_rust_common") (r "^0.0") (d #t) (k 0)))) (h "00h2ngd98ai3dgv0y3mh37qyrhni6axdw54h179sgfv2pf2vh0sw")))

(define-public crate-qt_generator_common-0.2.0 (c (n "qt_generator_common") (v "0.2.0") (d (list (d (n "cpp_to_rust_common") (r "^0.2.0") (d #t) (k 0)))) (h "1lhdh4ql7nc256h7az5xv7vvwpkfdbbhbmqjzsam7qgns7wi5s61")))

(define-public crate-qt_generator_common-0.2.3 (c (n "qt_generator_common") (v "0.2.3") (d (list (d (n "cpp_to_rust_common") (r "^0.2.3") (d #t) (k 0)))) (h "07pn4sgqqma35hfxw7hxxhnmj1s2an48cr7hf773ymglphj5nahm")))

