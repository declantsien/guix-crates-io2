(define-module (crates-io qt _g qt_gui) #:use-module (crates-io))

(define-public crate-qt_gui-0.0.0 (c (n "qt_gui") (v "0.0.0") (d (list (d (n "cpp_to_rust") (r "^0.1") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_core") (r "^0.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.0") (d #t) (k 1)))) (h "1x7wbdlijxm6zicjr1qjnwc514z2hgvhwdj0zdrczkb12ccxqh9k")))

(define-public crate-qt_gui-0.0.1 (c (n "qt_gui") (v "0.0.1") (d (list (d (n "cpp_to_rust") (r "^0.1") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_core") (r "^0.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.0") (d #t) (k 1)))) (h "15bal10b74nmag5asq46zr8ddaqkjiz5n1rvfrvwxbf9nwv16h6g")))

(define-public crate-qt_gui-0.1.0 (c (n "qt_gui") (v "0.1.0") (d (list (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.1") (d #t) (k 1)) (d (n "qt_core") (r "^0.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.1") (d #t) (k 1)))) (h "0a21i3syzpdkly6w966pr1w0q9bnhaixrhic25i1sgdimsdq7bgd")))

(define-public crate-qt_gui-0.1.1 (c (n "qt_gui") (v "0.1.1") (d (list (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.1") (d #t) (k 1)) (d (n "qt_core") (r "^0.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.1") (d #t) (k 1)))) (h "08x8plw09acma6kh9lp06s7ga3rhb5qh4v2xmzm6d3x9razx0qdd")))

(define-public crate-qt_gui-0.1.2 (c (n "qt_gui") (v "0.1.2") (d (list (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.1") (d #t) (k 1)) (d (n "qt_core") (r "^0.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.1") (d #t) (k 1)))) (h "06q7rnwv4pdq68ydc2zdwnf4wjq9m14wqzy7pqf1ll4w3vmlq6kf")))

(define-public crate-qt_gui-0.1.3 (c (n "qt_gui") (v "0.1.3") (d (list (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.1") (d #t) (k 1)) (d (n "qt_core") (r "^0.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.1") (d #t) (k 1)))) (h "1wkq9ha9zc8z0mcjg46i7j24rd4h51xqdqbvrshnk9izikmmh6dv")))

(define-public crate-qt_gui-0.2.0 (c (n "qt_gui") (v "0.2.0") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.0") (d #t) (k 1)) (d (n "qt_core") (r "^0.2.0") (d #t) (k 0)))) (h "0aj0llc527nrqqnplg4mph0vkn10nqc58c974acchc3h8vqv8bjd")))

(define-public crate-qt_gui-0.2.1 (c (n "qt_gui") (v "0.2.1") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.0") (d #t) (k 1)) (d (n "qt_core") (r "^0.2.1") (d #t) (k 0)))) (h "1l7lkpk1vb062yyhwahr6hvnb7rkgw96kjp62wm4m8lyn39d1bl7")))

(define-public crate-qt_gui-0.2.2 (c (n "qt_gui") (v "0.2.2") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.1") (d #t) (k 1)) (d (n "qt_core") (r "^0.2.2") (d #t) (k 0)))) (h "1rjddav1nzg04074yj59l83bpw3px9q02hcql0n9jqklnyxfbqmz")))

(define-public crate-qt_gui-0.2.3 (c (n "qt_gui") (v "0.2.3") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.3") (d #t) (k 1)) (d (n "qt_core") (r "^0.2.3") (d #t) (k 0)))) (h "1f4392vfsa6hfvnz4l0h1zi3283mixz9za0i2w9i6p4kprgzx5vf")))

(define-public crate-qt_gui-0.3.0 (c (n "qt_gui") (v "0.3.0") (d (list (d (n "cpp_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.1.0") (d #t) (k 1)))) (h "0pnqc0h9l9f19m8q7qz3rs0h7i3a56fxdnnpzwqsn59cycygl8zi")))

(define-public crate-qt_gui-0.4.0 (c (n "qt_gui") (v "0.4.0") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.4.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "0s1000wwzb1f7a0232pns9lnzbb9d5ys17x12jri4a0dfa6rm9cb") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_gui-0.4.1 (c (n "qt_gui") (v "0.4.1") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.4.1") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "0g7p28m0d23bvjw06j8nd5v53gj3a94xv8lq7p6fr0c0avismwmd") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_gui-0.5.0-alpha.1 (c (n "qt_gui") (v "0.5.0-alpha.1") (d (list (d (n "cpp_core") (r "^0.5.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0-alpha.1") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.4.0") (d #t) (k 1)))) (h "1wmmak4skw4h775sxyxcy5yxn1d67ip4qyx6q16gw82zvqs5c5q1") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_gui-0.5.0-alpha.2 (c (n "qt_gui") (v "0.5.0-alpha.2") (d (list (d (n "cpp_core") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.5.0-alpha.1") (d #t) (k 1)))) (h "1lh91ds76bnl6mnws30crsf15pa9p171rg9pwlm1pzgrd8blpa7c") (f (quote (("ritual_rustdoc_nightly" "qt_core/ritual_rustdoc_nightly") ("ritual_rustdoc" "qt_core/ritual_rustdoc"))))))

(define-public crate-qt_gui-0.5.0 (c (n "qt_gui") (v "0.5.0") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.5.0") (d #t) (k 1)))) (h "0dqqsaxcgwbb5g3qmacjyh0q8ph4jw9wbv1w52271d30d1n3dj6w") (f (quote (("ritual_rustdoc_nightly" "qt_core/ritual_rustdoc_nightly") ("ritual_rustdoc" "qt_core/ritual_rustdoc"))))))

