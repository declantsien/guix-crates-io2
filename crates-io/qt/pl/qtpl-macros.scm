(define-module (crates-io qt pl qtpl-macros) #:use-module (crates-io))

(define-public crate-qtpl-macros-0.2.0 (c (n "qtpl-macros") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "120k3k1q20fipy1pplq7s3pp13sdyp3hnq765hqjpj7fddmdzj37")))

(define-public crate-qtpl-macros-0.3.0 (c (n "qtpl-macros") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jm3153x6i9fw1lxqizjxa77c5yvnlhzcz68jz12crfmlywf5wnw")))

(define-public crate-qtpl-macros-0.4.0 (c (n "qtpl-macros") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qqnsh5shsis9aw7rl2pcp0qldbq1h6ysi728ds3k41q6cdmw88c")))

(define-public crate-qtpl-macros-0.4.1 (c (n "qtpl-macros") (v "0.4.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hnl7ljggamyzhkkl66b2g7f6paf78p0i0riqfzwlqz210nnvdia")))

(define-public crate-qtpl-macros-0.5.0 (c (n "qtpl-macros") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05m36anc3lzdhkis10w347b40pmjm9lh4y1ygm6dzl33cdd7qpkp")))

(define-public crate-qtpl-macros-0.5.1 (c (n "qtpl-macros") (v "0.5.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pkm37dv7d4d6gldyr0k3s49k9arwwbc4z4z3ms4s0m2pwcd1m67")))

(define-public crate-qtpl-macros-0.6.0 (c (n "qtpl-macros") (v "0.6.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ri1krzwfbpvxf7yanfkxgwwx2lr0kyalwd60wi8431pxy9kqqfx")))

(define-public crate-qtpl-macros-0.7.0 (c (n "qtpl-macros") (v "0.7.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nv3gy054i2mxr1fnznyrfyiczlwjis7x25mrxxbnwn3ljw5z5jz")))

(define-public crate-qtpl-macros-0.7.1 (c (n "qtpl-macros") (v "0.7.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m0gl4ccgdyaiaj5v1mkwrh0mvv40iwsg2bgpm5w1vihn5z512pw")))

