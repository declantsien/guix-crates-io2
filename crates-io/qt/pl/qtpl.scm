(define-module (crates-io qt pl qtpl) #:use-module (crates-io))

(define-public crate-qtpl-0.1.0 (c (n "qtpl") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16gf3aap6hqfmiw2bdhiydibyvak6s8d2f9zqd7q4dw0rq7wnixh")))

(define-public crate-qtpl-0.2.0 (c (n "qtpl") (v "0.2.0") (d (list (d (n "qtpl-macros") (r "^0.2.0") (d #t) (k 0)))) (h "18wxl1zzv0pzr1k5x5rcf2lk4pw693q8sl29msd25jhm3v8wcn65")))

(define-public crate-qtpl-0.3.0 (c (n "qtpl") (v "0.3.0") (d (list (d (n "qtpl-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0j194rn2svk4vxrd7b5irwx8r3zyh17bwvfpbklifailnh2j9zrp")))

(define-public crate-qtpl-0.4.0 (c (n "qtpl") (v "0.4.0") (d (list (d (n "qtpl-macros") (r "^0.4") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.8") (d #t) (k 0)))) (h "0zp3lc8jna50dmkx7b0njv9rlbcc706mf8v71w5z81p4m2j2xsn8")))

(define-public crate-qtpl-0.4.1 (c (n "qtpl") (v "0.4.1") (d (list (d (n "qtpl-macros") (r "^0.4.1") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.8") (d #t) (k 0)))) (h "1yl2adappqaryli9wx653fdphd9ygag8mjs2ccrw6d8lmhsmskr1")))

(define-public crate-qtpl-0.5.0 (c (n "qtpl") (v "0.5.0") (d (list (d (n "qtpl-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.8") (d #t) (k 0)))) (h "1jkx8qxnzhniqjz46hpyk6v4w2fimbi04pi8ssn18iwls1gfi677")))

(define-public crate-qtpl-0.5.1 (c (n "qtpl") (v "0.5.1") (d (list (d (n "qtpl-macros") (r "^0.5.1") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.8") (d #t) (k 0)))) (h "025fh3s738sl2ysjm5iwpm77f1dyj8rgrm6f0wwl7j6nwdnyshv7")))

(define-public crate-qtpl-0.6.0 (c (n "qtpl") (v "0.6.0") (d (list (d (n "qtpl-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.8") (d #t) (k 0)))) (h "1xjlfqzpb7ndh4pm0xydk9svk6q1i9bg0wwpacnkyxkpvyp567z9")))

(define-public crate-qtpl-0.7.0 (c (n "qtpl") (v "0.7.0") (d (list (d (n "qtpl-macros") (r "^0.7.0") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.8") (d #t) (k 0)))) (h "1kdimdl47ywvkv56lxnbr85pl6bn3akd37dbsg0ycx2lcgrjqnb2")))

(define-public crate-qtpl-0.7.1 (c (n "qtpl") (v "0.7.1") (d (list (d (n "qtpl-macros") (r "^0.7") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.8") (d #t) (k 0)))) (h "0vmw63d0bbg5a5zy07nm189dws09j8hh7k78l4bvxwa6cnfmp71h")))

