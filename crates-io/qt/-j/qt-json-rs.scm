(define-module (crates-io qt -j qt-json-rs) #:use-module (crates-io))

(define-public crate-qt-json-rs-1.0.0 (c (n "qt-json-rs") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1kxjjx5r4gf538fgr9x5h26ixh88n0rznca4rc3130x58caq8dvx")))

(define-public crate-qt-json-rs-1.0.1 (c (n "qt-json-rs") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "13qm2qrfjcdrv3miy6bm81d2pkqa1dvjxwqjkbfdr38mk72v3qxx")))

