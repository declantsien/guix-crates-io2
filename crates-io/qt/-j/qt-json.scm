(define-module (crates-io qt -j qt-json) #:use-module (crates-io))

(define-public crate-qt-json-1.0.2 (c (n "qt-json") (v "1.0.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1rbkng1fhvf85k2jbska0n4wvvgr3h2rpr0m8pw6b8kf5qkhdzj8")))

