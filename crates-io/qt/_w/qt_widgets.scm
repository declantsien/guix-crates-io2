(define-module (crates-io qt _w qt_widgets) #:use-module (crates-io))

(define-public crate-qt_widgets-0.0.0 (c (n "qt_widgets") (v "0.0.0") (d (list (d (n "cpp_to_rust") (r "^0.1") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_core") (r "^0.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.0") (d #t) (k 1)) (d (n "qt_gui") (r "^0.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.0") (d #t) (k 1)))) (h "0yqxqcjg59jkrcahi1wkf4ccf62clz2is719jcvlhpf9ks9wyfc5")))

(define-public crate-qt_widgets-0.1.0 (c (n "qt_widgets") (v "0.1.0") (d (list (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.1") (d #t) (k 1)) (d (n "qt_core") (r "^0.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.1") (d #t) (k 1)) (d (n "qt_gui") (r "^0.1") (d #t) (k 0)) (d (n "qt_gui") (r "^0.1") (d #t) (k 1)))) (h "0m0pi0vhl9lscq0p96x03a0bf6gx7ycjb82w56k5y40lq8p4rh30")))

(define-public crate-qt_widgets-0.1.2 (c (n "qt_widgets") (v "0.1.2") (d (list (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.1") (d #t) (k 1)) (d (n "qt_core") (r "^0.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.1") (d #t) (k 1)) (d (n "qt_gui") (r "^0.1") (d #t) (k 0)) (d (n "qt_gui") (r "^0.1") (d #t) (k 1)))) (h "0vhis4dgqcadk9n0ni01scjqyl1qvrz0c9c5023q19g4m4sadl2i")))

(define-public crate-qt_widgets-0.1.3 (c (n "qt_widgets") (v "0.1.3") (d (list (d (n "cpp_to_rust") (r "^0.5") (d #t) (k 1)) (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.1") (d #t) (k 1)) (d (n "qt_core") (r "^0.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.1") (d #t) (k 1)) (d (n "qt_gui") (r "^0.1") (d #t) (k 0)) (d (n "qt_gui") (r "^0.1") (d #t) (k 1)))) (h "17j4ry82sdva66l38mfd7xwm7g88ry7pm8vhrws9xfmdkcfqw14g")))

(define-public crate-qt_widgets-0.2.0 (c (n "qt_widgets") (v "0.2.0") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.0") (d #t) (k 1)) (d (n "qt_core") (r "^0.2.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.2.0") (d #t) (k 0)))) (h "13dkgnmbwwly7bdg8xq1avyip8rkdqyg6ldwz8fg636zqm0qr26d")))

(define-public crate-qt_widgets-0.2.1 (c (n "qt_widgets") (v "0.2.1") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.0") (d #t) (k 1)) (d (n "qt_core") (r "^0.2.1") (d #t) (k 0)) (d (n "qt_gui") (r "^0.2.1") (d #t) (k 0)))) (h "1kcxj5470m0baxxn59fvwfd942hrxpx3zdyvsz94mlrcdvma3fhv")))

(define-public crate-qt_widgets-0.2.2 (c (n "qt_widgets") (v "0.2.2") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.1") (d #t) (k 1)) (d (n "qt_core") (r "^0.2.2") (d #t) (k 0)) (d (n "qt_gui") (r "^0.2.2") (d #t) (k 0)))) (h "1gfi1arsylwcp7adigz0igqymcrhm41lbkgrzpzs6h7lczyjw30d")))

(define-public crate-qt_widgets-0.2.3 (c (n "qt_widgets") (v "0.2.3") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.3") (d #t) (k 1)) (d (n "qt_core") (r "^0.2.3") (d #t) (k 0)) (d (n "qt_gui") (r "^0.2.3") (d #t) (k 0)))) (h "1g4sg0zg9vdjjylaxsydgr645qmi600x82w93nidrv441l2510d2")))

(define-public crate-qt_widgets-0.3.0 (c (n "qt_widgets") (v "0.3.0") (d (list (d (n "cpp_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.1.0") (d #t) (k 1)))) (h "0hyx0v9nvanp4cdv792g2p8sg69bxg8dial1yi06cbm0fyl4ly1b")))

(define-public crate-qt_widgets-0.4.0 (c (n "qt_widgets") (v "0.4.0") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.4.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.4.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "1wmhzx2rin7b88vm4srzqcaq7g4h28ydxfh2b3ndrf8925r3hrfg") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_widgets-0.4.1 (c (n "qt_widgets") (v "0.4.1") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.4.1") (d #t) (k 0)) (d (n "qt_gui") (r "^0.4.1") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "0ilnifazhk6ggbrkpiaarabhh813m09zpq994dpil7pd7y50knvd") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_widgets-0.5.0-alpha.1 (c (n "qt_widgets") (v "0.5.0-alpha.1") (d (list (d (n "cpp_core") (r "^0.5.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0-alpha.1") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0-alpha.1") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.4.0") (d #t) (k 1)))) (h "0kall82vsw282lrax3qi19zv32p7cyw1mscvyiljr33yrgky8iin") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_widgets-0.5.0-alpha.2 (c (n "qt_widgets") (v "0.5.0-alpha.2") (d (list (d (n "cpp_core") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.5.0-alpha.1") (d #t) (k 1)))) (h "06i0wgv7bhva3bb14sqzn59790apl6hlykq42y093isjfnkhj852") (f (quote (("ritual_rustdoc_nightly" "qt_core/ritual_rustdoc_nightly" "qt_gui/ritual_rustdoc_nightly") ("ritual_rustdoc" "qt_core/ritual_rustdoc" "qt_gui/ritual_rustdoc"))))))

(define-public crate-qt_widgets-0.5.0 (c (n "qt_widgets") (v "0.5.0") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.5.0") (d #t) (k 1)))) (h "1xmpl0bb4q5jn54fnqza3bwrvjzkdd9xmqk40laswyx1k30xwg6r") (f (quote (("ritual_rustdoc_nightly" "qt_core/ritual_rustdoc_nightly" "qt_gui/ritual_rustdoc_nightly") ("ritual_rustdoc" "qt_core/ritual_rustdoc" "qt_gui/ritual_rustdoc"))))))

