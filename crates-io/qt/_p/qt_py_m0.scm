(define-module (crates-io qt _p qt_py_m0) #:use-module (crates-io))

(define-public crate-qt_py_m0-0.10.0 (c (n "qt_py_m0") (v "0.10.0") (d (list (d (n "atsamd-hal") (r "^0.14") (k 0)) (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)) (d (n "smart-leds") (r "^0.3") (d #t) (k 2)) (d (n "usb-device") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "usbd-serial") (r "^0.1") (d #t) (k 2)) (d (n "ws2812-timer-delay") (r "^0.3") (f (quote ("slow"))) (d #t) (k 2)))) (h "0dfgn00vff55d7wbpi0rkzfl4px2mgpdpckvfy987fvdmkvqi6py") (f (quote (("use_semihosting") ("usb" "atsamd-hal/usb" "usb-device") ("unproven" "atsamd-hal/unproven") ("rt" "cortex-m-rt" "atsamd-hal/samd21e-rt") ("default" "rt" "atsamd-hal/samd21e"))))))

