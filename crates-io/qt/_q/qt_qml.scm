(define-module (crates-io qt _q qt_qml) #:use-module (crates-io))

(define-public crate-qt_qml-0.3.0 (c (n "qt_qml") (v "0.3.0") (d (list (d (n "cpp_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.1.0") (d #t) (k 1)))) (h "12vgf76472idqlx6rwm832mkj62830scdjsdipkpjavj8avd4ds5")))

(define-public crate-qt_qml-0.4.0 (c (n "qt_qml") (v "0.4.0") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.4.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.4.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "15ahi6362x2irfqzkr1n4cfncqsgqgwi2hcqdplgampr262rp5pr") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_qml-0.4.1 (c (n "qt_qml") (v "0.4.1") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.4.1") (d #t) (k 0)) (d (n "qt_gui") (r "^0.4.1") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "11xibh6px9fgaw8j5dnhc1qwxs9jybnpsln5alxgnvd3i8piss0k") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_qml-0.5.0-alpha.1 (c (n "qt_qml") (v "0.5.0-alpha.1") (d (list (d (n "cpp_core") (r "^0.5.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0-alpha.1") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0-alpha.1") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.4.0") (d #t) (k 1)))) (h "0gd2lwm4zkdy2lnw2545d38s714w1skwsvlipricvv5z0nnkrj28") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_qml-0.5.0-alpha.2 (c (n "qt_qml") (v "0.5.0-alpha.2") (d (list (d (n "cpp_core") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.5.0-alpha.1") (d #t) (k 1)))) (h "00r7sf6dkqkw67mbbfmq565rf5pkp5rrcrwlc95298iiys80xx1i") (f (quote (("ritual_rustdoc_nightly" "qt_core/ritual_rustdoc_nightly" "qt_gui/ritual_rustdoc_nightly") ("ritual_rustdoc" "qt_core/ritual_rustdoc" "qt_gui/ritual_rustdoc"))))))

(define-public crate-qt_qml-0.5.0 (c (n "qt_qml") (v "0.5.0") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.5.0") (d #t) (k 1)))) (h "0lqp2zx0a4q6n1dx0k5dzg52h9yskc3dz3fvwahjqas1r0d8jq09") (f (quote (("ritual_rustdoc_nightly" "qt_core/ritual_rustdoc_nightly" "qt_gui/ritual_rustdoc_nightly") ("ritual_rustdoc" "qt_core/ritual_rustdoc" "qt_gui/ritual_rustdoc"))))))

