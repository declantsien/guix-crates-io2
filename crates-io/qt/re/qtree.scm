(define-module (crates-io qt re qtree) #:use-module (crates-io))

(define-public crate-qtree-0.1.0 (c (n "qtree") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ggez") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ggez") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "nalgebra") (r "^0.17") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "snowflake") (r "^1.3") (d #t) (k 0)))) (h "0dwyay136qmpm00h634af29x48x96brnnx7l7jphp041kqaq5dy2") (f (quote (("default" "ggez"))))))

