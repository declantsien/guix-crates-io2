(define-module (crates-io qt _3 qt_3d_core) #:use-module (crates-io))

(define-public crate-qt_3d_core-0.2.2 (c (n "qt_3d_core") (v "0.2.2") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.1") (d #t) (k 1)) (d (n "qt_core") (r "^0.2.2") (d #t) (k 0)) (d (n "qt_gui") (r "^0.2.2") (d #t) (k 0)))) (h "0mjz92ns7qscdqc341b19xs6r3mr1rsbqg3y2r5ly7pf4zwnp8k8")))

(define-public crate-qt_3d_core-0.2.3 (c (n "qt_3d_core") (v "0.2.3") (d (list (d (n "cpp_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qt_build_tools") (r "^0.2.3") (d #t) (k 1)) (d (n "qt_core") (r "^0.2.3") (d #t) (k 0)) (d (n "qt_gui") (r "^0.2.3") (d #t) (k 0)))) (h "1f8nb4m6z7jj6asjyxvgzssrj4n0xxfrhc69xxm7k1k3rj56qdk4")))

(define-public crate-qt_3d_core-0.3.0 (c (n "qt_3d_core") (v "0.3.0") (d (list (d (n "cpp_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.3.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.1.0") (d #t) (k 1)))) (h "1h33kqvgnvd8iqvn8pzz9hc9g03sd2ifr1yqj5wq00b90hv8cgvb")))

(define-public crate-qt_3d_core-0.4.0 (c (n "qt_3d_core") (v "0.4.0") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.4.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.4.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "15lxqdnxr9zd0nh5rrl6w03ahma3q2h88f3xsxhf1v9sbb1z965l") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_3d_core-0.4.1 (c (n "qt_3d_core") (v "0.4.1") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.4.1") (d #t) (k 0)) (d (n "qt_gui") (r "^0.4.1") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "17p1gn5arq69v9pq9ndlp6z8iyah0ns8h4rdhwslgyj617am66v5") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_3d_core-0.5.0-alpha.1 (c (n "qt_3d_core") (v "0.5.0-alpha.1") (d (list (d (n "cpp_core") (r "^0.5.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0-alpha.1") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0-alpha.1") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.4.0") (d #t) (k 1)))) (h "1ac8f7bimgxhlpw807bwzkm55nhq3aj9d18bfsnhzp8in5vfdx7m") (f (quote (("ritual_rustdoc"))))))

(define-public crate-qt_3d_core-0.5.0-alpha.2 (c (n "qt_3d_core") (v "0.5.0-alpha.2") (d (list (d (n "cpp_core") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.5.0-alpha.1") (d #t) (k 1)))) (h "1i1w5wlm9qgy0pxxjzih4hg96qmd5f7gmfybfz9qh4qycxgf7sy7") (f (quote (("ritual_rustdoc_nightly" "qt_core/ritual_rustdoc_nightly" "qt_gui/ritual_rustdoc_nightly") ("ritual_rustdoc" "qt_core/ritual_rustdoc" "qt_gui/ritual_rustdoc"))))))

(define-public crate-qt_3d_core-0.5.0 (c (n "qt_3d_core") (v "0.5.0") (d (list (d (n "cpp_core") (r "^0.6.0") (d #t) (k 0)) (d (n "qt_core") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_gui") (r "^0.5.0") (d #t) (k 0)) (d (n "qt_ritual_build") (r "^0.5.0") (d #t) (k 1)))) (h "0wjivl5xrbjf3k6b2sjzrfxvq62bgkgs54jm4shs8cmssh74xn8i") (f (quote (("ritual_rustdoc_nightly" "qt_core/ritual_rustdoc_nightly" "qt_gui/ritual_rustdoc_nightly") ("ritual_rustdoc" "qt_core/ritual_rustdoc" "qt_gui/ritual_rustdoc"))))))

