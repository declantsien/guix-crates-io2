(define-module (crates-io qt _r qt_ritual_build) #:use-module (crates-io))

(define-public crate-qt_ritual_build-0.0.0 (c (n "qt_ritual_build") (v "0.0.0") (d (list (d (n "qt_ritual_common") (r "^0.0.0") (d #t) (k 0)) (d (n "ritual_build") (r "^0.0.0") (d #t) (k 0)))) (h "0ggckdl9dpyxgc44ia3x0jyq5343qng9wfagbsfwg58lb6rgvpg7")))

(define-public crate-qt_ritual_build-0.1.0 (c (n "qt_ritual_build") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "qt_ritual_common") (r "^0.1.0") (d #t) (k 0)) (d (n "ritual_build") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1cap2hfmnd69wbrd3pvk38sh8hgnklg41ifwkwmrdnvc3xhvj2sh")))

(define-public crate-qt_ritual_build-0.2.0 (c (n "qt_ritual_build") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "qt_ritual_common") (r "^0.2.0") (d #t) (k 0)) (d (n "ritual_build") (r "^0.2.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0ycyhv3d3cy0as7dczpqkbms3q8rlpvwrcn36ab0abi3zdc46air")))

(define-public crate-qt_ritual_build-0.3.0 (c (n "qt_ritual_build") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "qt_ritual_common") (r "^0.3.0") (d #t) (k 0)) (d (n "ritual_build") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1qdxh4b1p00j8dk822j14sqr7qrkbigy6lqg1nacj7i9zphhhp7j")))

(define-public crate-qt_ritual_build-0.4.0 (c (n "qt_ritual_build") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "qt_ritual_common") (r "^0.4.0") (d #t) (k 0)) (d (n "ritual_build") (r "^0.4.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "00igmvgfpjkz2rq89cfa60bzfqf6bqyigi8l4m0lisq11a37a9ba")))

(define-public crate-qt_ritual_build-0.5.0-alpha.1 (c (n "qt_ritual_build") (v "0.5.0-alpha.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "qt_ritual_common") (r "^0.4.0") (d #t) (k 0)) (d (n "ritual_build") (r "^0.4.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0mv2ph62qfv42lwjgvj8x4mc52xcfq62hfpciyf8qlkx5lkggkj1")))

(define-public crate-qt_ritual_build-0.5.0 (c (n "qt_ritual_build") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "qt_ritual_common") (r "^0.4.0") (d #t) (k 0)) (d (n "ritual_build") (r "^0.4.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0y9rpppimw4hjcdd0y0x7km7f7d55mzrn6ad82vhmg75ckdrk71a")))

