(define-module (crates-io qt _r qt_ritual_common) #:use-module (crates-io))

(define-public crate-qt_ritual_common-0.0.0 (c (n "qt_ritual_common") (v "0.0.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.0.0") (d #t) (k 0)))) (h "0zmankfcr3icz0l3y4s5pzm1iz99arkdl02h52a3bkyxqyiybgi2")))

(define-public crate-qt_ritual_common-0.1.0 (c (n "qt_ritual_common") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1k0wpg434kzi6c1f5cgaps8nz12z1x599sna1c6y810x05l7x30w")))

(define-public crate-qt_ritual_common-0.1.1 (c (n "qt_ritual_common") (v "0.1.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1rcq85j51qhrx1zpisd5wj1l5bj615dfda3kj2jxhfwa80sil3s4")))

(define-public crate-qt_ritual_common-0.2.0 (c (n "qt_ritual_common") (v "0.2.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.2.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "02wsnx67wzcn721fpqa7mm30akq9wfaawwf1p1lp7shddf14pz8k")))

(define-public crate-qt_ritual_common-0.3.0 (c (n "qt_ritual_common") (v "0.3.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1djgxycn27xplpj9y4fr5cmg37fi7hw16idbngk6pfz2fdfhi7vg")))

(define-public crate-qt_ritual_common-0.4.0 (c (n "qt_ritual_common") (v "0.4.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.4.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0w1an6k2xmnphww8683g6v8q4nv3in8lws966j187fr4aphcg8bv")))

