(define-module (crates-io qt _r qt_ritual) #:use-module (crates-io))

(define-public crate-qt_ritual-0.0.0 (c (n "qt_ritual") (v "0.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "compress") (r "^0.1.2") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.10.3") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "qt_ritual_common") (r "^0.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "ritual") (r "^0.0.0") (d #t) (k 0)) (d (n "ritual_common") (r "^0.0.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16.0") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "0r5amh1p1sb9wdgrw6fb66apdnjhlwhn6iq1g2hn1i6plhbjl9bw")))

