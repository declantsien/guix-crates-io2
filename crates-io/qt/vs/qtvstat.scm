(define-module (crates-io qt vs qtvstat) #:use-module (crates-io))

(define-public crate-qtvstat-0.1.0 (c (n "qtvstat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.23") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)))) (h "09vdd9bfqbf7z0cnwlwqp4k8dxdwfmzb13lw4yxllr9zz5fhr6i1")))

(define-public crate-qtvstat-0.2.0 (c (n "qtvstat") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "quake_qtvinfo") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("rustls-tls"))) (k 0)) (d (n "tinyudp") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)))) (h "1a2l4bk4w8lc29w0ly8gbnjb7amnyvd3zs6v83nsmap58psfa1wa")))

