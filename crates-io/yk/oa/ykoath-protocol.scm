(define-module (crates-io yk oa ykoath-protocol) #:use-module (crates-io))

(define-public crate-ykoath-protocol-0.1.0 (c (n "ykoath-protocol") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pcsc") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1dz1gdlzrdfl0v8v1mnl6a74h3xrlss3hc5rl0n3z4m693jf8isw")))

(define-public crate-ykoath-protocol-0.2.0 (c (n "ykoath-protocol") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pcsc") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1mbfg090n2fqjd2f2q0wda1bzivyjy6s4zxyj8il80jvka0wrkv0")))

