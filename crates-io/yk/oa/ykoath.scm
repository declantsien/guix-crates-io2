(define-module (crates-io yk oa ykoath) #:use-module (crates-io))

(define-public crate-ykoath-0.1.0 (c (n "ykoath") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libappindicator") (r "^0.4.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.5.0") (d #t) (k 0)) (d (n "pcsc") (r "^1") (d #t) (k 0)))) (h "094l233dqcifj68831qzda5zxq4vakjzi9xa99cs07k9mn8yrjg4")))

(define-public crate-ykoath-0.1.1 (c (n "ykoath") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libappindicator") (r "^0.4.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.5.0") (d #t) (k 0)) (d (n "pcsc") (r "^1") (d #t) (k 0)))) (h "0y3adq2ph07d5j08czaa7nrc7a5iza0c8vsc2xlpbd3ar9c41y9z")))

