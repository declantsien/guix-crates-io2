(define-module (crates-io yk pe ykpers-rs) #:use-module (crates-io))

(define-public crate-ykpers-rs-0.1.0 (c (n "ykpers-rs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "expectest") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libykpers-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "17qqpaljlf9syyw9k8gx34zj6v2vwy7lycaxn45cfg9ldb2crgm5")))

(define-public crate-ykpers-rs-0.2.0-beta1 (c (n "ykpers-rs") (v "0.2.0-beta1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "errno") (r "^0.2.5") (d #t) (k 0)) (d (n "expectest") (r "^0.12.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "libykpers-sys") (r "^0.2.0-beta1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1kf3hsw65839csgz5h6si3y26cbamhf6mwf1dxfzpg1skkn870rk")))

(define-public crate-ykpers-rs-0.3.0 (c (n "ykpers-rs") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "expectest") (r "^0.12.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "libykpers-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "15nx80zb5kl91gk8wndmqdfi8yhvlqr02wd54rfisvzc8agpz8jr")))

(define-public crate-ykpers-rs-0.3.1 (c (n "ykpers-rs") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "expectest") (r "^0.12.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "libykpers-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "04pszjfplhi8xkmmhd5kncxsb4dd83y6hxsg7dm2a2qpwx2sjm9p")))

