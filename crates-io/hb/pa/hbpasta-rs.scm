(define-module (crates-io hb pa hbpasta-rs) #:use-module (crates-io))

(define-public crate-hbpasta-rs-0.1.0 (c (n "hbpasta-rs") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "pasta_curves") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "005rywax00kh3cm2lzx8knp085bisyaak1yvlp0kzkrjfiibw3ww")))

