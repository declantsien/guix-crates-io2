(define-module (crates-io hb -s hb-subset) #:use-module (crates-io))

(define-public crate-hb-subset-0.1.0 (c (n "hb-subset") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0dny89l4xp8rbzn15933wh1h37zw667925m3l0af2lsfax1r9w5y") (y #t)))

(define-public crate-hb-subset-0.2.0 (c (n "hb-subset") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "09sjqrf9d72myvrli7b1xhyji0f3nfcfripjipy1y5kswvxybwbw") (f (quote (("bundled"))))))

(define-public crate-hb-subset-0.2.1 (c (n "hb-subset") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1in2q2avv99fn5wvxfjdaihy391s696rdmp6d09rnnbbg0dm4v2x") (f (quote (("bundled"))))))

(define-public crate-hb-subset-0.3.0 (c (n "hb-subset") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "11rrhslyr6s5zphqbamh7q63psfxpkx57gpd0x1jah6w0d1hnd67") (f (quote (("bundled"))))))

