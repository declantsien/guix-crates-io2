(define-module (crates-io hb s- hbs-acc-pow-sys) #:use-module (crates-io))

(define-public crate-hbs-acc-pow-sys-0.2.0 (c (n "hbs-acc-pow-sys") (v "0.2.0") (d (list (d (n "hbs-builder") (r "^0.2") (d #t) (k 1)) (d (n "hbs-common-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sdawcp7l29ak53k6zh2za3f3hd0r9qn3s5rz1prz2x3ch600yms")))

(define-public crate-hbs-acc-pow-sys-0.2.1 (c (n "hbs-acc-pow-sys") (v "0.2.1") (d (list (d (n "hbs-builder") (r "^0.2") (d #t) (k 1)) (d (n "hbs-common-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00r2g77ccvq93frv279011h7pav20w68cy0s6yx1bxfz7p7nbkdc")))

