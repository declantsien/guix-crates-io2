(define-module (crates-io hb s- hbs-sys) #:use-module (crates-io))

(define-public crate-hbs-sys-0.2.0 (c (n "hbs-sys") (v "0.2.0") (d (list (d (n "hbs-builder") (r "^0.2") (d #t) (k 1)) (d (n "hbs-common-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m1vsbxxkrmpn46wc0gw1ii8zzx7lrwn5yvmy9w659k3969szzy5")))

(define-public crate-hbs-sys-0.2.1 (c (n "hbs-sys") (v "0.2.1") (d (list (d (n "hbs-builder") (r "^0.2") (d #t) (k 1)) (d (n "hbs-common-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1j30ipk9jyw5wqm9iq6lipgrdd9iq7ag81xwghi05hz4m8gzh7x0")))

