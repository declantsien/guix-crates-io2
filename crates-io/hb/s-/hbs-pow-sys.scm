(define-module (crates-io hb s- hbs-pow-sys) #:use-module (crates-io))

(define-public crate-hbs-pow-sys-0.1.0 (c (n "hbs-pow-sys") (v "0.1.0") (h "1777hhljk5q8rjaxjdlk5rga4aj17x1ykg8kl94hgi54y761vwkx")))

(define-public crate-hbs-pow-sys-0.2.0 (c (n "hbs-pow-sys") (v "0.2.0") (d (list (d (n "hbs-builder") (r "^0.2") (d #t) (k 1)) (d (n "hbs-common-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17ccfzn5x0bxs8ir2k2l4ax3njhbmi8zl4iq31k90ya28av544gg")))

(define-public crate-hbs-pow-sys-0.2.1 (c (n "hbs-pow-sys") (v "0.2.1") (d (list (d (n "hbs-builder") (r "^0.2") (d #t) (k 1)) (d (n "hbs-common-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "067jwlr87yd92a87vpkk6ln77s6kicni1rcb5sfx66w2d0nllrcx")))

