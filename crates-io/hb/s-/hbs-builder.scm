(define-module (crates-io hb s- hbs-builder) #:use-module (crates-io))

(define-public crate-hbs-builder-0.2.0 (c (n "hbs-builder") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 0)))) (h "00cs5rs13pjz6w6hn3prlaly4fyy8wicc4imp01wiqwma3m5i0p5")))

(define-public crate-hbs-builder-0.2.1 (c (n "hbs-builder") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 0)))) (h "1p3cgykpi2dnggn329sc9gqnmqy9nkfnsm4h9p6l2cizfzwkviqk")))

