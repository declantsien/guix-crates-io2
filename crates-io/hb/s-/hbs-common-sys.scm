(define-module (crates-io hb s- hbs-common-sys) #:use-module (crates-io))

(define-public crate-hbs-common-sys-0.1.0 (c (n "hbs-common-sys") (v "0.1.0") (h "1ql0c8a2rgcl38ka6g566vrcyzg5drnyck2mr45fc1b3jjqsp023")))

(define-public crate-hbs-common-sys-0.2.0 (c (n "hbs-common-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ip70jh30x02ap33gpg7qkdi62zqhz40dn9xb0bg14c7fr6472bf")))

(define-public crate-hbs-common-sys-0.2.1 (c (n "hbs-common-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dmnvqa3v27hj3i9c1cbsyhs58gnf6fyr5gb5ryg1nikbrja49a7")))

