(define-module (crates-io hb s- hbs-pow) #:use-module (crates-io))

(define-public crate-hbs-pow-0.1.0 (c (n "hbs-pow") (v "0.1.0") (h "0kmvp6h8pca0sqa400m4lxzlgxn29mrgzvjvhm1d670czjkx7x8h")))

(define-public crate-hbs-pow-0.2.0 (c (n "hbs-pow") (v "0.2.0") (d (list (d (n "hbs-pow-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c0x0dg7bp2xaiwcgll9223jf7vhd83l6hdv61g1cxddqd5razsd")))

