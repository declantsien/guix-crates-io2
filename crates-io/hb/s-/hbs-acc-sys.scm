(define-module (crates-io hb s- hbs-acc-sys) #:use-module (crates-io))

(define-public crate-hbs-acc-sys-0.2.0 (c (n "hbs-acc-sys") (v "0.2.0") (d (list (d (n "hbs-builder") (r "^0.2") (d #t) (k 1)) (d (n "hbs-common-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ana9hnj90f9kps6mwjmqhcnir7kja1w9fjjs0lxjsb9si1g2p1g")))

(define-public crate-hbs-acc-sys-0.2.1 (c (n "hbs-acc-sys") (v "0.2.1") (d (list (d (n "hbs-builder") (r "^0.2") (d #t) (k 1)) (d (n "hbs-common-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vl07yi6z4m1l9v2h0slm4lq4bihwhybcax45xwcim1zws2c4k8n")))

