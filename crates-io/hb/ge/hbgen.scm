(define-module (crates-io hb ge hbgen) #:use-module (crates-io))

(define-public crate-hbgen-0.1.0 (c (n "hbgen") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "handlebars") (r "^3.0") (d #t) (k 0)))) (h "1jnc3cnll2gv72dafx396dn511kb9b7r8zqpb330lzbkqhxf5a8p")))

