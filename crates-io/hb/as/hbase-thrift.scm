(define-module (crates-io hb as hbase-thrift) #:use-module (crates-io))

(define-public crate-hbase-thrift-0.1.0 (c (n "hbase-thrift") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "1hz5lsb6pvjp3rfb7a16bp3dzxslkcihp0zmk7kah3i6w7fvd8g3")))

(define-public crate-hbase-thrift-0.2.0 (c (n "hbase-thrift") (v "0.2.0") (d (list (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "11bipdlfpffamzvf7ljxskpf66rqyw05vvsl40yw82xn98r6rny7")))

(define-public crate-hbase-thrift-0.3.0 (c (n "hbase-thrift") (v "0.3.0") (d (list (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0ir6wxh3q5z2qfx1l1k875fghlb7gw556g1y3kc8mnhvwdni37fx")))

(define-public crate-hbase-thrift-0.4.0 (c (n "hbase-thrift") (v "0.4.0") (d (list (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "1vvi2qxk8k5br582px15hajslivlsx88wfh92y77b0vdvjygk322")))

(define-public crate-hbase-thrift-0.5.0 (c (n "hbase-thrift") (v "0.5.0") (d (list (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0h91lp7papc6pjrx01vgrkjzrmp1vcprqnwyg6ay5p4xl9w75ldr")))

(define-public crate-hbase-thrift-0.6.0 (c (n "hbase-thrift") (v "0.6.0") (d (list (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "03sljxh0xx7kig2cmhyghfp66l09a3z391416qzqnq7c2hdha8rw")))

(define-public crate-hbase-thrift-0.7.0 (c (n "hbase-thrift") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "bb8") (r "^0.7.1") (d #t) (k 2)) (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)) (d (n "thrift-pool") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "08cga0azlh0wyhx0v4filyf28x2rlh72csa9akqcw3yr3lw120aw")))

(define-public crate-hbase-thrift-0.7.1 (c (n "hbase-thrift") (v "0.7.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)) (d (n "thrift-pool") (r "^0.1.1") (d #t) (k 0)))) (h "17abwzap496136vl4bklqxc5d3yz2zc82bk0j4gnf1q77zy490s6")))

(define-public crate-hbase-thrift-0.7.2 (c (n "hbase-thrift") (v "0.7.2") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)) (d (n "thrift-pool") (r "^0.1.2") (d #t) (k 0)))) (h "07rvrmr9pjkssgjz04c9bnlsnyrqxz3rv0d6v1bb11r3pq9qzkx4")))

(define-public crate-hbase-thrift-0.7.3 (c (n "hbase-thrift") (v "0.7.3") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)) (d (n "thrift-pool") (r "^0.1.2") (k 0)))) (h "19ryd10wrzjg4rwv6sp92fydhlvw6wxqxlcw6zs7jlbjljizc0cr") (f (quote (("r2d2" "thrift-pool/enable-r2d2") ("default" "r2d2") ("bb8" "thrift-pool/enable-bb8"))))))

(define-public crate-hbase-thrift-0.7.4 (c (n "hbase-thrift") (v "0.7.4") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)) (d (n "thrift-pool") (r "^0.1.2") (k 0)))) (h "0pcnygxsbpx62iian0br3cdsyydslrafmj1693lg26f5igla8hga") (f (quote (("r2d2" "thrift-pool/enable-r2d2") ("bb8" "thrift-pool/enable-bb8"))))))

(define-public crate-hbase-thrift-0.7.5 (c (n "hbase-thrift") (v "0.7.5") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)) (d (n "thrift-pool") (r "^0.1.3") (k 0)))) (h "1l2j5b9h1x3ngi09g2556czdy3b6b5z777845ha65a8bcxp8v2av") (f (quote (("r2d2" "thrift-pool/enable-r2d2") ("bb8" "thrift-pool/enable-bb8"))))))

(define-public crate-hbase-thrift-0.7.6 (c (n "hbase-thrift") (v "0.7.6") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)) (d (n "thrift-pool") (r "^0.1.4") (k 0)))) (h "0440dgj8s3ml38z23lbim1wxckmzr3bqhxk69576qyjqz5fkxpc6") (f (quote (("r2d2" "thrift-pool/enable-r2d2") ("bb8" "thrift-pool/enable-bb8"))))))

(define-public crate-hbase-thrift-1.1.0 (c (n "hbase-thrift") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "easy-ext") (r "^1.0.0") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)) (d (n "thrift-pool") (r "^1.4.0") (o #t) (k 0)))) (h "0jkafxcx4fmha2p1a5pn0wcmrdi7xzp2a7sa3dx6yfrxcai49zzs") (f (quote (("r2d2" "pool" "thrift-pool/impl-r2d2") ("pool" "thrift-pool") ("default" "r2d2") ("bb8" "pool" "thrift-pool/impl-bb8"))))))

(define-public crate-hbase-thrift-1.2.0 (c (n "hbase-thrift") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "easy-ext") (r "^1.0.1") (d #t) (k 0)) (d (n "thrift") (r "^0.17.0") (d #t) (k 0)) (d (n "thrift-pool") (r "^1.5") (o #t) (k 0)))) (h "08c1sxc4kpp049qmh1aksxh8hy1zy3lf3l50cnihm71bxrpqblc0") (f (quote (("r2d2" "pool" "thrift-pool/impl-r2d2") ("pool" "thrift-pool") ("default" "r2d2") ("bb8" "pool" "thrift-pool/impl-bb8"))))))

