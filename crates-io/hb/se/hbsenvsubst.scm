(define-module (crates-io hb se hbsenvsubst) #:use-module (crates-io))

(define-public crate-hbsenvsubst-0.0.1 (c (n "hbsenvsubst") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.15.9") (d #t) (k 0)))) (h "1ln6blqh419pbj8ynf7s0s00byl16lzw2n8j8ipi05fq8drh7dn2")))

(define-public crate-hbsenvsubst-0.0.2 (c (n "hbsenvsubst") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.15.9") (d #t) (k 0)))) (h "1kxjcbz2x9m85hk5ydxbryd062c0x40g33d3ira5vf9xvkjbivs7")))

