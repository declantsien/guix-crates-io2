(define-module (crates-io hb _m hb_macros) #:use-module (crates-io))

(define-public crate-hb_macros-0.1.0 (c (n "hb_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1iiqci66fzjv09xplf2zavcrcmp1f6r1i8pdgsvb4k5pkkqvip9a")))

(define-public crate-hb_macros-0.1.1 (c (n "hb_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0cifhh256s6x4024vlf44irfsy6hmrdkn0gfh8ckbzzrqi1wzgz8")))

(define-public crate-hb_macros-0.1.2 (c (n "hb_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0wcjliphvzx4fiaaaqnpnyzhwbl3nnadv20cfw707k1kq6qrq751")))

(define-public crate-hb_macros-0.1.3 (c (n "hb_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0bwsd6wy5lwvkll03w4d2abqkh0rih9kbs4ra327bmh6hm2f4yp0")))

