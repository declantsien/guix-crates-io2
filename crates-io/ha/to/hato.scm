(define-module (crates-io ha to hato) #:use-module (crates-io))

(define-public crate-hato-0.1.0 (c (n "hato") (v "0.1.0") (d (list (d (n "aligned-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (k 2)) (d (n "dyn-clone") (r "^1.0") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 2)) (d (n "unscrupulous") (r "^0.1.0") (d #t) (k 0)))) (h "01svagwa0jhwydvfdqgv65rbzniiv6i54zrhzvwl1scfcqbq2rzc")))

(define-public crate-hato-0.2.0 (c (n "hato") (v "0.2.0") (d (list (d (n "aligned-vec") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (k 2)) (d (n "dyn-clone") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 2)) (d (n "unscrupulous") (r "^0.1.0") (d #t) (k 0)))) (h "0snvf2csrn9kmpwvyz7ljj09qsk6jalf02pk85nvc24bgwcil47p")))

(define-public crate-hato-0.2.1 (c (n "hato") (v "0.2.1") (d (list (d (n "aligned-vec") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (k 2)) (d (n "dyn-clone") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 2)) (d (n "unscrupulous") (r "^0.1.0") (d #t) (k 0)))) (h "10j81dlg85sj1ivmxkqk5ig6s5pkj413hzss3v486qb1ld2y6wwz")))

