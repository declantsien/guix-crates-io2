(define-module (crates-io ha x- hax-cli-options) #:use-module (crates-io))

(define-public crate-hax-cli-options-0.1.0-pre.1 (c (n "hax-cli-options") (v "0.1.0-pre.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hax-frontend-exporter-options") (r "=0.1.0-pre.1") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00a8zy014ab70vqv6k46dixwla2mj5pfjr4amwzr2gwhy6q83n51")))

