(define-module (crates-io ha x- hax-adt-into) #:use-module (crates-io))

(define-public crate-hax-adt-into-0.1.0-pre.1 (c (n "hax-adt-into") (v "0.1.0-pre.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive" "printing" "extra-traits" "parsing" "full"))) (d #t) (k 0)))) (h "17f054c1dw3qqq1ib9vy7c76jifz6kn76m7c4y0m6cnasynas2px")))

