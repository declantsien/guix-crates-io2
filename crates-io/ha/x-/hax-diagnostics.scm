(define-module (crates-io ha x- hax-diagnostics) #:use-module (crates-io))

(define-public crate-hax-diagnostics-0.1.0-pre.1 (c (n "hax-diagnostics") (v "0.1.0-pre.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "104gd9nj107q35lkznvln04h7dhzsj3cbjf9jhllnpjpzsp0d3dz")))

