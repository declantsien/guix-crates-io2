(define-module (crates-io ha nz hanzzok) #:use-module (crates-io))

(define-public crate-hanzzok-0.1.0 (c (n "hanzzok") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "libhanzzok") (r "^0.1.1") (d #t) (k 0)))) (h "17hmkvgylaqbfdhn7a89lw6p5qpzvgxmvrx28rsvl2dl861sb9xh") (r "1.56.0")))

