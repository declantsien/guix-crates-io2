(define-module (crates-io ha dv hadvent) #:use-module (crates-io))

(define-public crate-hadvent-0.1.0 (c (n "hadvent") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0sld4211d50zpy6xzdq6vqz270fv8f3wrcq43h3521v8r326nd2n")))

(define-public crate-hadvent-0.1.1 (c (n "hadvent") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0y6gwxyjiiybpimmwzfjg0jkz8fb74mgsvb4v8l4c5icbbs8wdp2")))

(define-public crate-hadvent-0.2.0 (c (n "hadvent") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1zm8lx4rw8jfzqz3zdnm9j040gyscrz7s8dzsyahlp9v7qbqnmi2")))

(define-public crate-hadvent-0.2.1 (c (n "hadvent") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "05crzg6gxyb218q54hpdvgv2hlvgwfkk9564lnchp40r2f9qpi7j")))

