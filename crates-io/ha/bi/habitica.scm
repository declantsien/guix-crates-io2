(define-module (crates-io ha bi habitica) #:use-module (crates-io))

(define-public crate-habitica-0.1.0 (c (n "habitica") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "habitica_rust_client") (r "^0.1.5") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "07dn05vf24an07h9mac0x230gai1b9nrmbc29n0as7w9f7bgkmk0") (y #t)))

