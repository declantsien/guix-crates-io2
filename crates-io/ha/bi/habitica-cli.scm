(define-module (crates-io ha bi habitica-cli) #:use-module (crates-io))

(define-public crate-habitica-cli-0.1.0 (c (n "habitica-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "habitica_rust_client") (r "^0.1.5") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "0qznf06gg3qll5bflc5cna4rfp1f2ij3g1dh4qcpfi1mvll7wa45")))

(define-public crate-habitica-cli-0.1.1 (c (n "habitica-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "habitica_rust_client") (r "^0.1.5") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "0jqlb9cy2ib632zjwxy767gs51sf0dvp7z6pa1y40c1spi1vwq6g")))

(define-public crate-habitica-cli-0.1.2 (c (n "habitica-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "habitica_rust_client") (r "^0.1.5") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "13lkns837jhhxj87b5s5p2v2cvxmfs1hsc6aviv8ywcy5i96hyh2")))

(define-public crate-habitica-cli-0.1.3 (c (n "habitica-cli") (v "0.1.3") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "habitica_rust_client") (r "^0.1.5") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "0d3j1rp352kvrp72ndn1w3z1mziza0n55b19nwh28y7wjzs3nick")))

(define-public crate-habitica-cli-0.1.4 (c (n "habitica-cli") (v "0.1.4") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "habitica_rust_client") (r "^0.1.5") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "0lblxwg79yahg0galwr41s2f22gdwyldjvbd45p2lyyh2k3xllk3")))

