(define-module (crates-io ha bi habitica_weekly_report) #:use-module (crates-io))

(define-public crate-habitica_weekly_report-0.1.0 (c (n "habitica_weekly_report") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "figment") (r "^0.10") (f (quote ("toml" "json"))) (d #t) (k 0)) (d (n "lettre") (r "^0.10.0-rc.3") (f (quote ("rustls-tls" "builder" "smtp-transport" "serde"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (f (quote ("json"))) (d #t) (k 0)))) (h "07q74v5fjhrcsvk7g507cssbs1g8yqh86pw36v1hppmfn06h19yf")))

