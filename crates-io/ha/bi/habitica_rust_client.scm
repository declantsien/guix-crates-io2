(define-module (crates-io ha bi habitica_rust_client) #:use-module (crates-io))

(define-public crate-habitica_rust_client-0.1.0 (c (n "habitica_rust_client") (v "0.1.0") (d (list (d (n "mocktopus") (r "^0.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "0ld3fsgcs2jaaisll16w36gns0cxq3ipm5cbxrkwrsslf229xr5j")))

(define-public crate-habitica_rust_client-0.1.1 (c (n "habitica_rust_client") (v "0.1.1") (d (list (d (n "mocktopus") (r "^0.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "19gzk0g41kr9hq2sy5d2cxpymbdpfbas12qnmrw04j0g7xinbcpi")))

(define-public crate-habitica_rust_client-0.1.2 (c (n "habitica_rust_client") (v "0.1.2") (d (list (d (n "mocktopus") (r "^0.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "07jrbzynq8rlr8p2s6pw1cy6039w7nb958vvqzkg1pq9vl661p56")))

(define-public crate-habitica_rust_client-0.1.3 (c (n "habitica_rust_client") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "10jkxddcapvqrazjalgq1cl5pqzibdcfxr3apr3qvp69pfvcqr93")))

(define-public crate-habitica_rust_client-0.1.4 (c (n "habitica_rust_client") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "19x9z6d02pddyjqi4bvpg3vc9mzj3wa69f1xhbfpslkqbr4pj267")))

(define-public crate-habitica_rust_client-0.1.5 (c (n "habitica_rust_client") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "1x951v7vkp1ba75bn5fymfylkrfgc4lqp743548w611zhnrdxwmb")))

(define-public crate-habitica_rust_client-0.1.6 (c (n "habitica_rust_client") (v "0.1.6") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)))) (h "0m1c0cz0i5z8xw1x7vw6j3p3zj52xh8242brjx77jbpa0dk3zcav")))

(define-public crate-habitica_rust_client-0.1.7 (c (n "habitica_rust_client") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)))) (h "1g7ark4lfsr8c3ifbll1133pl7y20pn4mmr9hs55a9i00x1fjv3h")))

(define-public crate-habitica_rust_client-0.1.8 (c (n "habitica_rust_client") (v "0.1.8") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)))) (h "1ivf2v0rkm734k0svjiy2h4mw7xja36mdnfdaz63p2myhg7w8fr1")))

