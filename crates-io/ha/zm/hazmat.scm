(define-module (crates-io ha zm hazmat) #:use-module (crates-io))

(define-public crate-hazmat-0.0.0 (c (n "hazmat") (v "0.0.0") (h "0b0mz86dgy4465xcj4s543nsrj77gpkq5y7h30ay53hrzmy7gwk8")))

(define-public crate-hazmat-0.1.0 (c (n "hazmat") (v "0.1.0") (d (list (d (n "hazmat-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0c4j5lk62v7dm7ii27m4ng6qp05b00243krbz24dnpk1hw6lryny")))

