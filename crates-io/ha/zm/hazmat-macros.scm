(define-module (crates-io ha zm hazmat-macros) #:use-module (crates-io))

(define-public crate-hazmat-macros-0.0.0 (c (n "hazmat-macros") (v "0.0.0") (h "10pi74db3sa95fv6dwqjznfydbpmfamhhq6rx6j09j8hkcg081x7")))

(define-public crate-hazmat-macros-0.1.0 (c (n "hazmat-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kpsyn5bbfjjkf8h7ssfxgc44l2r08jsgq49f3klq1x4sbwr52k3")))

