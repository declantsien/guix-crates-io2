(define-module (crates-io ha l_ hal_sensor_dht) #:use-module (crates-io))

(define-public crate-hal_sensor_dht-0.1.0 (c (n "hal_sensor_dht") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0b88m41sw6w2sdinkyrp7fhg4439vysjbdj8jqrbv7y4a9ns8kdb") (f (quote (("floats"))))))

