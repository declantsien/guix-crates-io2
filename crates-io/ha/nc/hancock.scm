(define-module (crates-io ha nc hancock) #:use-module (crates-io))

(define-public crate-hancock-0.1.0 (c (n "hancock") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "18x2f6frlxc9a5n7qjszb6c7dxhxfnzpaydnv7yc7dy18v2kbbz5")))

(define-public crate-hancock-0.1.1 (c (n "hancock") (v "0.1.1") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0wn183wb13gv3ii3sk6hfss4yvrs8q41lz7sm6i15bi4kgqqrckc")))

(define-public crate-hancock-0.2.0-alpha.1 (c (n "hancock") (v "0.2.0-alpha.1") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "sfv") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1xnc12pzkvvdz1val7zab4zxll47ayj58hb6sggp50j7vj6wbrdb")))

