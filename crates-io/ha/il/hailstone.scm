(define-module (crates-io ha il hailstone) #:use-module (crates-io))

(define-public crate-hailstone-0.1.0 (c (n "hailstone") (v "0.1.0") (d (list (d (n "beetle-collatz") (r "^0.3.5") (f (quote ("threaded"))) (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "16dsxh130934jxa2jkyhx77bwp4h5gv6i35xvwslg2dxf5cm0a8c")))

