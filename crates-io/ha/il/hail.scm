(define-module (crates-io ha il hail) #:use-module (crates-io))

(define-public crate-hail-0.1.0 (c (n "hail") (v "0.1.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11.9") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1aia00901j70qww148nif4gylc19vicxrviiprfzg7xc45fv4my8")))

