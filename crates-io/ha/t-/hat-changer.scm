(define-module (crates-io ha t- hat-changer) #:use-module (crates-io))

(define-public crate-hat-changer-0.1.0 (c (n "hat-changer") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "go-parse-duration") (r "^0.1.1") (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "pretty-duration") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "120nrw2y5p4shib0yji4s329aksk98sd0j2v0a017j8xr49jwab2")))

