(define-module (crates-io ha zp hazptr) #:use-module (crates-io))

(define-public crate-hazptr-0.1.0 (c (n "hazptr") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "cfg-if") (r "^0.1.7") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "reclaim") (r "^0.2.0") (k 0)))) (h "0sgzhryxfv4pi0h16i1xifghzwp7hzm8rrac5z1z0hfip1w1rmmp") (f (quote (("std" "arrayvec/std" "reclaim/std") ("sanitize-threads") ("default" "std") ("count-release"))))))

(define-public crate-hazptr-0.1.1 (c (n "hazptr") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "cfg-if") (r "^0.1.7") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "reclaim") (r "^0.2.1") (k 0)))) (h "1ssa70cdfh7jvvk4wvjdjdpivrz81wz2xgm59wwd2kvrd8hpcvhh") (f (quote (("std" "arrayvec/std" "reclaim/std") ("sanitize-threads") ("default" "std") ("count-release"))))))

