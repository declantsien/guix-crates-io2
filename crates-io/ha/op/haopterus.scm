(define-module (crates-io ha op haopterus) #:use-module (crates-io))

(define-public crate-haopterus-0.0.1 (c (n "haopterus") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1kxrg66kdpslrzyp00ijgrpgxvda606cngn8nj13vc4029iw6b37")))

(define-public crate-haopterus-0.0.2 (c (n "haopterus") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "134d2lr44l1v86ziv4rlz18p6r24acsxql3l2abz393g74qifp2n")))

