(define-module (crates-io ha cs hacspec-attributes) #:use-module (crates-io))

(define-public crate-hacspec-attributes-0.1.0-beta.1 (c (n "hacspec-attributes") (v "0.1.0-beta.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "hacspec-util") (r "^0.1.0-beta.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1z13q41kznw3x8n50yjnl3cf6i38snynnxgd6am2sx4ipxs2kdfj") (f (quote (("update_allowlist" "print_attributes") ("print_attributes") ("default"))))))

