(define-module (crates-io ha pi hapi-sys) #:use-module (crates-io))

(define-public crate-hapi-sys-0.1.0 (c (n "hapi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "heck") (r "^0.3.1") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 1)))) (h "0sljg60bp9hj90jh39qz15j8lm93r9bpwyc31pv2phfiwlfa0g39") (f (quote (("rustify") ("default"))))))

(define-public crate-hapi-sys-0.1.1 (c (n "hapi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "heck") (r "^0.3.1") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 1)))) (h "0yxblq12gy2m0x6052b6fpmv0vfy0ind3p7vbwnhsi9vffz2xam8") (f (quote (("rustify") ("default"))))))

(define-public crate-hapi-sys-0.1.2 (c (n "hapi-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "heck") (r "^0.3.1") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 1)))) (h "159znhvrvjd3n31zmp117pcdfihpwb341hszjm3hw28ja9024rdh") (f (quote (("rustify") ("default"))))))

(define-public crate-hapi-sys-0.1.3 (c (n "hapi-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "heck") (r "^0.3.1") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 1)))) (h "137kagds34dxvs0rf7lr89n5466j3ana195wybwnxr3hglggwzmp") (f (quote (("rustify") ("default" "rustify"))))))

(define-public crate-hapi-sys-0.2.0 (c (n "hapi-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "heck") (r "^0.4.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 1)))) (h "046p6x2afw7im626pliivl2akgj998piacydnnzdx5fvyqysqk5l") (f (quote (("rustify") ("default" "rustify"))))))

