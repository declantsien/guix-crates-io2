(define-module (crates-io ha pi hapi-core-solana) #:use-module (crates-io))

(define-public crate-hapi-core-solana-0.3.0 (c (n "hapi-core-solana") (v "0.3.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (d #t) (k 0)))) (h "06qh6nw1wxlm7rrmcfkq6kxqhs66nir3k455gxg1v3k1726h6yg3") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

