(define-module (crates-io ha pi hapi-core) #:use-module (crates-io))

(define-public crate-hapi-core-0.1.0 (c (n "hapi-core") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1dcw8yzy5p8acz8np965z594lpibaw974yzzi2iijd4fy2ql4j0z") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hapi-core-0.1.1 (c (n "hapi-core") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "06jvyzkyblbh41rj3wy542r33d3n423z94nabafnpwynrd3vqpgk") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hapi-core-0.1.2 (c (n "hapi-core") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "028kkh1chjya6zivi2vskkg3k5ig16nl4nicw8ld34wgn5p2dlyd") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hapi-core-0.2.0 (c (n "hapi-core") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.26.0") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1kjvrsxw64a0v43pn055ma6ifn56sx7924cgbfhrrs9dw9sfc8aq") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

