(define-module (crates-io ha pi hapi) #:use-module (crates-io))

(define-public crate-hapi-0.1.0 (c (n "hapi") (v "0.1.0") (d (list (d (n "near-contract-standards") (r "^4.0.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1l6fc71w9r7cd10yidr69b952yhg12gqsgrwmksipv3dnh5n2x1n")))

(define-public crate-hapi-0.1.1 (c (n "hapi") (v "0.1.1") (d (list (d (n "near-contract-standards") (r "^4.0.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0w4z02p602q8l6mbdzqa85yl4y00nx9xag4m6jrfv2pw80n5fjna")))

(define-public crate-hapi-0.1.2 (c (n "hapi") (v "0.1.2") (d (list (d (n "near-contract-standards") (r "^4.0.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1kjjgfnxb07a3gd9ifa59as1rd9mymk2m3vjcyw70aj8kkq4k5sj")))

(define-public crate-hapi-0.1.3 (c (n "hapi") (v "0.1.3") (d (list (d (n "near-contract-standards") (r "^4.0.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1zfwyd5h0fdg5qwvlc44amvsgm5kysifq3hzn34bs8cg8nvqhyq2")))

(define-public crate-hapi-0.1.4 (c (n "hapi") (v "0.1.4") (d (list (d (n "near-contract-standards") (r "^4.0.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "01jxfqdm27qk4c5m7zrmizmis4xwla5w5lzh9y7g3dxbpbl0c36m")))

(define-public crate-hapi-0.1.5 (c (n "hapi") (v "0.1.5") (d (list (d (n "near-contract-standards") (r "^4.0.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0ljnw0xvb8ks4y0skdx91bx64yqvbf7g7njzbw4f89i5xz6xqhwk")))

(define-public crate-hapi-0.1.6 (c (n "hapi") (v "0.1.6") (h "0zr9biz8h881ghm9kapw1ly4hjagspg2jymlngagvy8dbdr17856")))

