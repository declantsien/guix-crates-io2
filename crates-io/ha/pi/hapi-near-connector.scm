(define-module (crates-io ha pi hapi-near-connector) #:use-module (crates-io))

(define-public crate-hapi-near-connector-0.1.1 (c (n "hapi-near-connector") (v "0.1.1") (d (list (d (n "near-contract-standards") (r "^4.0.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0xdp1gzyv16w4idi63q159miwhdd0w2chm0gmzxii7g7xbpzpg9m")))

(define-public crate-hapi-near-connector-0.2.0 (c (n "hapi-near-connector") (v "0.2.0") (d (list (d (n "near-contract-standards") (r "^4.0.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0ypv10l0r2ji1s5ppmf2qnmf3qg37dlkla5hx6ppxmk2czq5s3wn")))

(define-public crate-hapi-near-connector-0.3.0 (c (n "hapi-near-connector") (v "0.3.0") (d (list (d (n "near-contract-standards") (r "^4.0.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0hrj151xa8bgsj3dfm9km8hkgnrki86jxd0p7a5a6vacmik31akw")))

