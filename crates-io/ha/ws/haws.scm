(define-module (crates-io ha ws haws) #:use-module (crates-io))

(define-public crate-haws-0.1.0 (c (n "haws") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1iwdig5g3m46nda22x2089349ph2cnwfgas1jd5fg9v4bk18p5l0") (y #t)))

(define-public crate-haws-0.1.1 (c (n "haws") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0062k4hw5d828cs4953bc4zdnxnjrir6d5b6k2asy4vjq3pgq3nb") (y #t)))

(define-public crate-haws-0.1.2 (c (n "haws") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "135r9p9zlfnw237xsqnwgd1ar9ws4rf0ain9gzas5jhsb234w2mg")))

(define-public crate-haws-0.1.3 (c (n "haws") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0w433lcbfccvyl9zqjpydh5ssdrwnwf5qr4sh9apdc0h02w7yi6c")))

