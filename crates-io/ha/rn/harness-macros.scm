(define-module (crates-io ha rn harness-macros) #:use-module (crates-io))

(define-public crate-harness-macros-0.0.0 (c (n "harness-macros") (v "0.0.0") (h "1vxchh1n6bq7gilvynmanpyf66xlswks8miq5zqz59b9yd8a388r")))

(define-public crate-harness-macros-0.0.1 (c (n "harness-macros") (v "0.0.1") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1sdn3i2qjzfrxd83iijpm1bmbm12pci1cilzrn7r2z7m9ib46l2v")))

(define-public crate-harness-macros-0.0.2 (c (n "harness-macros") (v "0.0.2") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "harness") (r "^0.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "15k7pi093cq2x22gfhmf700pl1k3g4vwg1sk3lrissxlms8i8bqc")))

