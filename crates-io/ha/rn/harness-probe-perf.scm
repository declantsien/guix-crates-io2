(define-module (crates-io ha rn harness-probe-perf) #:use-module (crates-io))

(define-public crate-harness-probe-perf-0.0.0 (c (n "harness-probe-perf") (v "0.0.0") (h "07fvv3iqahq1b3w750yk8myxfwxf39gx6ymh22p2cfw83894rqqn")))

(define-public crate-harness-probe-perf-0.0.1 (c (n "harness-probe-perf") (v "0.0.1") (d (list (d (n "harness") (r "^0.0.1") (d #t) (k 0)) (d (n "pfm") (r "^0.1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1s9gvrb3cfs0x9x8s05qpkvyqbvdvmildjshlpr81lrni3x564iw")))

(define-public crate-harness-probe-perf-0.0.2 (c (n "harness-probe-perf") (v "0.0.2") (d (list (d (n "harness") (r "^0.0.4") (d #t) (k 0)) (d (n "pfm") (r "^0.1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1ff86d4mjxa2q0izp6s6kw0w874sda32jrxqjv7ks3pq34mpbx1s")))

(define-public crate-harness-probe-perf-0.0.3 (c (n "harness-probe-perf") (v "0.0.3") (d (list (d (n "harness") (r "^0.0.5") (d #t) (k 0)) (d (n "pfm") (r "^0.1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "15zci45ljkfbpbcn37agiaqxfldlga5ic0ajf871my0jva1mjhxz")))

(define-public crate-harness-probe-perf-0.0.4 (c (n "harness-probe-perf") (v "0.0.4") (d (list (d (n "harness") (r "^0.0") (d #t) (k 0)) (d (n "pfm") (r "^0.1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "04iic45syvg0hasr3rspwyxs548cscqnqz2j4li7vc17i54y7s0g")))

(define-public crate-harness-probe-perf-0.0.5 (c (n "harness-probe-perf") (v "0.0.5") (d (list (d (n "harness") (r "^0.0") (d #t) (k 0)) (d (n "pfm") (r "^0.1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "04qk69sag7qvi9a8gk6jcr7xg5bx84wh2z73h4jzcpahkanz6jj5")))

(define-public crate-harness-probe-perf-0.0.6 (c (n "harness-probe-perf") (v "0.0.6") (d (list (d (n "harness") (r "^0.0") (d #t) (k 0)) (d (n "pfm") (r "^0.1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0qr534f5a005iygzswzgk6rdzkm9hbm32k4zmziwz0xqsbgw9zqk")))

