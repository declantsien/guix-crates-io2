(define-module (crates-io ha c- hac-config) #:use-module (crates-io))

(define-public crate-hac-config-0.1.0 (c (n "hac-config") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "12gj57fv3k743v1nzssn12ixhh8x4dhf9zssz5a54jkrr3b2i0j0")))

