(define-module (crates-io ha c- hac-colors) #:use-module (crates-io))

(define-public crate-hac-colors-0.1.0 (c (n "hac-colors") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (f (quote ("all-widgets" "crossterm"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0ms8s5gca1ylqzqkm4nas0gp3nrm1mmz5kd482bcsd852dy9wak9")))

