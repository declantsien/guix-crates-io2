(define-module (crates-io ha wk hawktracer-converter) #:use-module (crates-io))

(define-public crate-hawktracer-converter-0.0.1 (c (n "hawktracer-converter") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hawktracer-parser") (r "^0.2.2") (d #t) (k 0)))) (h "09zv7d8i74f3vbwhdrxd461b94yjpm4mczvfk0zzm2ivq94ky0x0")))

(define-public crate-hawktracer-converter-0.1.0 (c (n "hawktracer-converter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.1") (d #t) (k 0)) (d (n "hawktracer-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)))) (h "1y2hm1iyjyk59702p53041k91j9wzc4k848dlx03cmygqxpcvvd1")))

(define-public crate-hawktracer-converter-0.1.1 (c (n "hawktracer-converter") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.1") (d #t) (k 0)) (d (n "hawktracer-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)))) (h "043sqwpi62g8j1kr87qk0cz5pflqqm392b2sh3dd9xg07022vnzz")))

(define-public crate-hawktracer-converter-0.2.0 (c (n "hawktracer-converter") (v "0.2.0") (d (list (d (n "assert-json-diff") (r "^1.0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "hawktracer-parser") (r "^0.2.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 2)))) (h "06ldpvcy9qzbljd4hwv7gwb05wrnc4a6i7da32fjr6rjwdnjrjyk")))

