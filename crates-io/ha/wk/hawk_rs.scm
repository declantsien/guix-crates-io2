(define-module (crates-io ha wk hawk_rs) #:use-module (crates-io))

(define-public crate-hawk_rs-0.0.1 (c (n "hawk_rs") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1bx3jqwi9a7pmz01lna5vfww3a9fzdb8wj1fsc1sw10m4rm45138")))

