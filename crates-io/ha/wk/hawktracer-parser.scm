(define-module (crates-io ha wk hawktracer-parser) #:use-module (crates-io))

(define-public crate-hawktracer-parser-0.1.0 (c (n "hawktracer-parser") (v "0.1.0") (h "0azzrjqy8hzgy0jjzjcy92f6cm9y4gjbhxgv9rgfbwp2mrkkqczi") (y #t)))

(define-public crate-hawktracer-parser-0.2.0 (c (n "hawktracer-parser") (v "0.2.0") (h "02v1ili58h01vcqz8xhs18zc2fzhz2vlqvv0xv5235piwzrvpiw0")))

(define-public crate-hawktracer-parser-0.2.1 (c (n "hawktracer-parser") (v "0.2.1") (h "0mnhhamhrf0fjf3bi6afpr0a1jc2p17ir577kin6b6r9a3f01kg4")))

(define-public crate-hawktracer-parser-0.2.2 (c (n "hawktracer-parser") (v "0.2.2") (h "0d6bwd4f7x2ckrjglxdp0ahb8h06c16wzp2l0kjz1lkc5a018071")))

(define-public crate-hawktracer-parser-0.2.3 (c (n "hawktracer-parser") (v "0.2.3") (h "1yybmrwwgl752k1slj8snj1fkq32imz1vvnzbgrqwwfghvjy7xdh")))

