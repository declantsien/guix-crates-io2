(define-module (crates-io ha wk hawk-cli) #:use-module (crates-io))

(define-public crate-hawk-cli-0.1.4 (c (n "hawk-cli") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "16vs9b3kdfj20g2wpp937bmql99iy5f81c1ic06brdjvw6pn1x2x")))

