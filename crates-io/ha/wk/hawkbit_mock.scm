(define-module (crates-io ha wk hawkbit_mock) #:use-module (crates-io))

(define-public crate-hawkbit_mock-0.1.0 (c (n "hawkbit_mock") (v "0.1.0") (d (list (d (n "hawkbit") (r "^0.1") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16ndwfy4592gnma333vl429aw5r2aiflxj2x0a4ixgdgf2aff7x0")))

(define-public crate-hawkbit_mock-0.1.1 (c (n "hawkbit_mock") (v "0.1.1") (d (list (d (n "hawkbit") (r "^0.1") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x1hsj3370s9wcgxbwj71fkmx8qi739s7z6xlmv4lybwjkmllbfq")))

(define-public crate-hawkbit_mock-0.2.0 (c (n "hawkbit_mock") (v "0.2.0") (d (list (d (n "hawkbit") (r "^0.2") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rh7idbmicxbz8l2wbcg50y1abrdnrfz7g88iixjjdpxcap88yrk")))

(define-public crate-hawkbit_mock-0.3.0 (c (n "hawkbit_mock") (v "0.3.0") (d (list (d (n "hawkbit") (r "^0.3") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "082kasnkzyc3kmiz3kcnh1r66k0bgsbka374j5k235xm556k3zbi")))

(define-public crate-hawkbit_mock-0.4.0 (c (n "hawkbit_mock") (v "0.4.0") (d (list (d (n "hawkbit") (r "^0.4") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14cqbpb3l0dk7i4hczidi8yd0rd8w8sr4hd8bg5dx1rl7mci6ci5")))

(define-public crate-hawkbit_mock-0.5.0 (c (n "hawkbit_mock") (v "0.5.0") (d (list (d (n "hawkbit") (r "^0.5.0") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lckh14xqxdayc2shg0bf49w0ij6v86ny3jifis6p18kpxf3bqih")))

(define-public crate-hawkbit_mock-0.6.0 (c (n "hawkbit_mock") (v "0.6.0") (d (list (d (n "hawkbit") (r "^0.6.0") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0prsscgdwgdqx3p4jikfpq5awkjdx5b8qx7pn2p88ih7454449zb")))

