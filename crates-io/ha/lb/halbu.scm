(define-module (crates-io ha lb halbu) #:use-module (crates-io))

(define-public crate-halbu-0.1.0 (c (n "halbu") (v "0.1.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.1.0") (d #t) (k 0)))) (h "1d1k4y2fsaw26kb7lklhc3bqqz9y7ffgfslfj9n33qxdqsar9gqn")))

