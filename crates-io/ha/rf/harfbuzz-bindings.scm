(define-module (crates-io ha rf harfbuzz-bindings) #:use-module (crates-io))

(define-public crate-harfbuzz-bindings-0.1.0 (c (n "harfbuzz-bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "0mgb5g4aag0da2lwj7yarfqsb4hlb8vqza75bc2hf7n2fss8s85q") (l "harfbuzz")))

(define-public crate-harfbuzz-bindings-0.1.1 (c (n "harfbuzz-bindings") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "1m1s1a7dpimlfxknm265ldhwr82ip2zm0fr1yn63kslfpyyxjj2d") (l "harfbuzz")))

(define-public crate-harfbuzz-bindings-0.1.2 (c (n "harfbuzz-bindings") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "0zhaijbj53wfyw86x4xyrvxkr2ha7cqiyq9b2619c1rchp0lpv3l") (l "harfbuzz")))

(define-public crate-harfbuzz-bindings-0.2.0 (c (n "harfbuzz-bindings") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "10sdg3y06ac5xhnl7acdbb73brr6n5n4mzx5cfgmr4xq61hvy9zb") (l "harfbuzz")))

