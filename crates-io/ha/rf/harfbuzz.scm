(define-module (crates-io ha rf harfbuzz) #:use-module (crates-io))

(define-public crate-harfbuzz-0.1.0 (c (n "harfbuzz") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1zgv1xcd57bx2gplra9418kpk2kra0v9hwbh8x6j6f1brpmphl66")))

(define-public crate-harfbuzz-0.1.1 (c (n "harfbuzz") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "173kjd3j3allgvi7z4q66kgwcwfnn9v9hrb2y6mzj9xahq4mv9kg")))

(define-public crate-harfbuzz-0.1.2 (c (n "harfbuzz") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0cr9y45gvdpv5jy2rvas74vn2s6ggh8yjlprcn0w1xxnfl8hgh0z")))

(define-public crate-harfbuzz-0.2.0 (c (n "harfbuzz") (v "0.2.0") (d (list (d (n "harfbuzz-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1ha4jinxlp98zgwgf59xzagmgbm10alf56kjn1wv7vl8xma4fi9w")))

(define-public crate-harfbuzz-0.3.0 (c (n "harfbuzz") (v "0.3.0") (d (list (d (n "harfbuzz-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0k8wihi25g9p0ni7jq8v4arsdhdl616qf5w0mwm9p5wh1x5pg493")))

(define-public crate-harfbuzz-0.3.1 (c (n "harfbuzz") (v "0.3.1") (d (list (d (n "harfbuzz-sys") (r "^0.3.1") (k 0)))) (h "1np8rmyl61lwmiz88jim2ycw7y600bk4hvxfkvjf7v55cri45xs6") (f (quote (("default" "build-native-harfbuzz" "build-native-freetype") ("build-native-harfbuzz" "harfbuzz-sys/build-native-harfbuzz") ("build-native-freetype" "harfbuzz-sys/build-native-freetype"))))))

(define-public crate-harfbuzz-0.4.0 (c (n "harfbuzz") (v "0.4.0") (d (list (d (n "harfbuzz-sys") (r "^0.5.0") (k 0)))) (h "14cdcjrzky5p41v7kbl4bhk644mxxrvng2spgmlkzddy36aslv3p") (f (quote (("default" "build-native-harfbuzz" "build-native-freetype") ("build-native-harfbuzz" "harfbuzz-sys/build-native-harfbuzz") ("build-native-freetype" "harfbuzz-sys/build-native-freetype"))))))

(define-public crate-harfbuzz-0.6.0 (c (n "harfbuzz") (v "0.6.0") (d (list (d (n "harfbuzz-sys") (r "^0.6.0") (k 0)) (d (n "harfbuzz-traits") (r "^0.6.0") (k 0)))) (h "1dlry7jjbni7spjb01rgb1bfy16a773njqpbrd23l4b4r0a8rryw") (f (quote (("std") ("freetype" "harfbuzz-sys/freetype") ("directwrite" "harfbuzz-sys/directwrite") ("default" "coretext" "directwrite" "freetype" "std") ("coretext" "harfbuzz-sys/coretext") ("bundled" "harfbuzz-sys/bundled"))))))

