(define-module (crates-io ha rf harfbuzz-ft-sys) #:use-module (crates-io))

(define-public crate-harfbuzz-ft-sys-0.1.0 (c (n "harfbuzz-ft-sys") (v "0.1.0") (d (list (d (n "freetype") (r "^0.4.1") (d #t) (k 0)) (d (n "harfbuzz-sys") (r "^0.3.1") (d #t) (k 0)))) (h "1in0j83008b5jmkxm1w3l3sv7hkjnfqmhhazis21q2mfr7414y7r")))

