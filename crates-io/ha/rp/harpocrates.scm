(define-module (crates-io ha rp harpocrates) #:use-module (crates-io))

(define-public crate-harpocrates-0.0.1 (c (n "harpocrates") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "170l42323kv8rqd17lmyg8n7ri7ic1dmh457mv1dhn27sfds2p8w")))

