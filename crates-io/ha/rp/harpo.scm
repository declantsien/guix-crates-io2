(define-module (crates-io ha rp harpo) #:use-module (crates-io))

(define-public crate-harpo-0.6.0 (c (n "harpo") (v "0.6.0") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "1nf8n9rpp2qwjw40n8wgiyp4pn0qw83zpd4pqj5c8prami0r8a85")))

(define-public crate-harpo-0.6.1 (c (n "harpo") (v "0.6.1") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "1ggkizl7sgigpky9yggzaqv2011sr2zx3zyi6lwznijxl40vf9dg")))

(define-public crate-harpo-0.6.2 (c (n "harpo") (v "0.6.2") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "19hbh6hmb1709np8zvhqdldvr361j3fdykyckz0m74wm587w69af")))

(define-public crate-harpo-0.6.3 (c (n "harpo") (v "0.6.3") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "137w04yxjy810fj0khi37zl6jjhpwd1p2v96p66z4ih85xkjnbj4")))

(define-public crate-harpo-0.6.4 (c (n "harpo") (v "0.6.4") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "05mq31dwa2qxsf6g1k9nkk2xypagvlgnsc4j1bvbm7s6rvh1czgf")))

(define-public crate-harpo-0.7.0 (c (n "harpo") (v "0.7.0") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "1zd0w5c9g1yzgdwascj8md567yy9ship9l7pyismp9sjwh1w0g22")))

