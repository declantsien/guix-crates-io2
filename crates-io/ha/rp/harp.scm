(define-module (crates-io ha rp harp) #:use-module (crates-io))

(define-public crate-harp-0.0.0 (c (n "harp") (v "0.0.0") (h "0il34j3p7cp4qdm1vmcrxmcl0p5hq5fw7r8s3n7128ar61l9m1xv") (y #t)))

(define-public crate-harp-0.1.0 (c (n "harp") (v "0.1.0") (d (list (d (n "slog") (r "^2") (o #t) (d #t) (k 0)))) (h "1aqiwnnawb9j0hj3knpss7ja77sfqdvl7bh92vw9cppd4p315gv0") (f (quote (("std" "alloc") ("logger" "slog" "std") ("default" "std") ("alloc")))) (y #t)))

