(define-module (crates-io ha rp harperdb) #:use-module (crates-io))

(define-public crate-harperdb-0.1.0 (c (n "harperdb") (v "0.1.0") (d (list (d (n "assert-json-diff") (r "^1.1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "10avfsjsbbizp4md7ffdlza8rb2q5sxmj3kzcgf7zqylbwr101h4")))

(define-public crate-harperdb-1.0.0 (c (n "harperdb") (v "1.0.0") (d (list (d (n "assert-json-diff") (r "^1.1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0wrxmf3xpd2h0if89wgkbp6f0bba6jal66caxg3n8ca6nrp8c56v")))

