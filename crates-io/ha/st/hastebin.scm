(define-module (crates-io ha st hastebin) #:use-module (crates-io))

(define-public crate-hastebin-0.1.0 (c (n "hastebin") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)))) (h "193q44dfffr3swp8nkgs850p415hzy0gxpxp00p3s7f5rylzqjpn")))

(define-public crate-hastebin-0.2.0 (c (n "hastebin") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)))) (h "146zf2hi98nlq1dv47jhgv3zqaljivrq00jnfwpk6x20wy8bmhc2")))

