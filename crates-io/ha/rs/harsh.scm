(define-module (crates-io ha rs harsh) #:use-module (crates-io))

(define-public crate-harsh-0.1.0 (c (n "harsh") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.85") (o #t) (d #t) (k 0)))) (h "13jp3i8xbhji90f5jg36fkwmf02m8vcqd0v3b0rqcp2m91268q2h") (f (quote (("default"))))))

(define-public crate-harsh-0.1.1 (c (n "harsh") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.85") (o #t) (d #t) (k 0)))) (h "12d8mxdmlcjjxc3iq85f6xzrg1j8f0br01g8mdfdsfqwn1cn0gw0") (f (quote (("default"))))))

(define-public crate-harsh-0.1.2 (c (n "harsh") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.95") (o #t) (d #t) (k 0)))) (h "0i14n5nsk3dapjn0raz64rgl32v9jnp8xkn449r27a28c86wgczf") (f (quote (("default"))))))

(define-public crate-harsh-0.1.3 (c (n "harsh") (v "0.1.3") (h "1v9djz6cm5cmadjxjx8jqfsdg6vnz65nc503hb9vqslddrxvhahp")))

(define-public crate-harsh-0.1.4 (c (n "harsh") (v "0.1.4") (h "0gk0mg6ls7xqgr2srywppa9pq44dvjlbz84hawn8fiy2lhcv2s9m")))

(define-public crate-harsh-0.1.5 (c (n "harsh") (v "0.1.5") (h "060vzypacjv55b306kggpfd0fxxp73712bcfmdpq957ic2db7qw5")))

(define-public crate-harsh-0.1.6 (c (n "harsh") (v "0.1.6") (h "0fihgw3zygiyjrgngigaqjszh2wgvsq5k0lklq63lj46shz6dpkh")))

(define-public crate-harsh-0.1.7 (c (n "harsh") (v "0.1.7") (h "07q7qgzhk71kp6fsnxn5ri08cnck4sb2n3mb8xz71prw5yc3ni0h")))

(define-public crate-harsh-0.2.0 (c (n "harsh") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1n80n94j7xz65lgnfxd35c6swzfqpp8ymb56xfjyzi0w6p0inyal")))

(define-public crate-harsh-0.2.1 (c (n "harsh") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "0nflv2arxg8f70af7amikwhbldf08yx8c00ccakrklxh54a8djw4")))

(define-public crate-harsh-0.2.2 (c (n "harsh") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1yzdqln2vzcdvk04jrpvgji6bb0r2zmqwzcdl4q2b0j970lf5z56")))

