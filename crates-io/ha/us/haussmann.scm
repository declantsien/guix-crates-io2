(define-module (crates-io ha us haussmann) #:use-module (crates-io))

(define-public crate-haussmann-0.0.1 (c (n "haussmann") (v "0.0.1") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 2)) (d (n "svg") (r "^0.13.0") (d #t) (k 2)))) (h "0ii310zny2f7f449wpfsc4vnixw7kfhk0kp3p11bw62pz9v7b38x") (y #t)))

(define-public crate-haussmann-0.0.2 (c (n "haussmann") (v "0.0.2") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 2)) (d (n "svg") (r "^0.13.0") (d #t) (k 2)))) (h "1i33irxw30ixk87sri5i6zv76fwv5jxrfwiqd1nxf9jmblzpci8z")))

(define-public crate-haussmann-0.0.3 (c (n "haussmann") (v "0.0.3") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 2)))) (h "18lbzjlscjbdpf3f4knjxkm7x2ya3aapv4bdx12rzvsw878jriav")))

