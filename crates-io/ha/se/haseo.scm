(define-module (crates-io ha se haseo) #:use-module (crates-io))

(define-public crate-haseo-0.1.0 (c (n "haseo") (v "0.1.0") (d (list (d (n "colorize") (r "^0.1.0") (d #t) (k 0)))) (h "1i6x6dnibj0sv5gdmvs3y2zzcd5h358s8mp5c81807m9rlhbyafd")))

(define-public crate-haseo-0.1.1 (c (n "haseo") (v "0.1.1") (d (list (d (n "colorize") (r "^0.1.0") (d #t) (k 0)))) (h "0c2p978sv7b4hbx9gprk0pjfkvhfkgss03iwjqc0mszhmkxyhj3m")))

(define-public crate-haseo-0.1.3 (c (n "haseo") (v "0.1.3") (d (list (d (n "colorize") (r "^0.1.0") (d #t) (k 0)))) (h "036aq1aw1ncbsmbaiyhbix82b2dqvys8kaxffldgy2vbzbg55jw0")))

(define-public crate-haseo-0.1.5 (c (n "haseo") (v "0.1.5") (d (list (d (n "colorize") (r "^0.1.0") (d #t) (k 0)))) (h "1zd90wxwq1c25gnfhp9v8ly9qmd4qi2r5h9dwx45q3v1vllv7gcp")))

