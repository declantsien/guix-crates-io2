(define-module (crates-io ha ya hayami-im-rc) #:use-module (crates-io))

(define-public crate-hayami-im-rc-0.1.0 (c (n "hayami-im-rc") (v "0.1.0") (d (list (d (n "ahash") (r "^0.3") (d #t) (k 0)) (d (n "im-rc") (r "^15") (d #t) (k 0)) (d (n "symbolmap-trait") (r "^0.1.1") (d #t) (k 0)) (d (n "symbolmap-trait") (r "^0.1.1") (f (quote ("testing"))) (d #t) (k 2)))) (h "1xwdi8li8spx13ph9fwpvw0qx37vgssdppzkmqw55h0p5mrr5xri")))

(define-public crate-hayami-im-rc-0.1.1 (c (n "hayami-im-rc") (v "0.1.1") (d (list (d (n "ahash") (r "^0.3") (d #t) (k 0)) (d (n "im-rc") (r "^15") (d #t) (k 0)) (d (n "symbolmap-trait") (r "^0.1.1") (d #t) (k 0)) (d (n "symbolmap-trait") (r "^0.1.1") (f (quote ("testing"))) (d #t) (k 2)))) (h "0pzg0gq0as515n462ykkksx2rqkv5713m3xgphnladsh9z0hh0ir") (f (quote (("pool" "im-rc/pool") ("default" "pool"))))))

