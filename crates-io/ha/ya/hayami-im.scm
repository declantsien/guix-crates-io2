(define-module (crates-io ha ya hayami-im) #:use-module (crates-io))

(define-public crate-hayami-im-0.1.0 (c (n "hayami-im") (v "0.1.0") (d (list (d (n "ahash") (r "^0.3") (d #t) (k 0)) (d (n "elysees") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "im") (r "^15") (d #t) (k 0)) (d (n "symbolmap-trait") (r "^0.1.1") (d #t) (k 0)) (d (n "symbolmap-trait") (r "^0.1.1") (f (quote ("testing"))) (d #t) (k 2)))) (h "042il2nw8rq9x20pg7yglww1pzsann0jk91ifjjb43zmx3r9rsqc") (f (quote (("default" "elysees"))))))

