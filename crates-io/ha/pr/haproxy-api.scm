(define-module (crates-io ha pr haproxy-api) #:use-module (crates-io))

(define-public crate-haproxy-api-0.0.0-alpha.1 (c (n "haproxy-api") (v "0.0.0-alpha.1") (d (list (d (n "mlua") (r "^0.3.2") (f (quote ("async"))) (d #t) (k 0)))) (h "0s7ya0f7sp0p1z764inh9n1a5sd0l7kqarkm3qnq0szw9xmhliwq")))

(define-public crate-haproxy-api-0.1.0-alpha.1 (c (n "haproxy-api") (v "0.1.0-alpha.1") (d (list (d (n "mlua") (r "^0.4.1") (f (quote ("async" "send" "lua53"))) (d #t) (k 0)))) (h "1wpcgi2y565k7c2mwrzszk50f87d3fhncimkqpyjl71d55g3b9r4")))

(define-public crate-haproxy-api-0.1.1-alpha.1 (c (n "haproxy-api") (v "0.1.1-alpha.1") (d (list (d (n "mlua") (r "^0.4.1") (f (quote ("async" "send" "lua53"))) (d #t) (k 0)))) (h "05jnxz6wg01vrbhl6kwh4ncswyyhx5vqsplskhg9iyrplrln4z8m")))

(define-public crate-haproxy-api-0.2.0 (c (n "haproxy-api") (v "0.2.0") (d (list (d (n "mlua") (r "^0.5.3") (f (quote ("async" "send" "serialize"))) (d #t) (k 0)))) (h "0fmqldaxrqq8kdvnl4rhc601dqhsp1i0fggfvanip254r6jmjf6p") (f (quote (("vendored" "mlua/vendored") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53"))))))

(define-public crate-haproxy-api-0.2.1 (c (n "haproxy-api") (v "0.2.1") (d (list (d (n "mlua") (r "^0.5.3") (f (quote ("async" "send" "serialize"))) (d #t) (k 0)))) (h "1c2f5imbgm3h2ga04fh1lv9r3jmx2qgqdp5zhdsi2z6zhgjrgpc6") (f (quote (("vendored" "mlua/vendored") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53"))))))

(define-public crate-haproxy-api-0.2.2 (c (n "haproxy-api") (v "0.2.2") (d (list (d (n "mlua") (r "^0.5.3") (f (quote ("async" "send" "serialize"))) (d #t) (k 0)))) (h "0lvm9b31629d9j70ilwzk0vs1lp7g61wgfknvbaygc1ps51qwvsl") (f (quote (("vendored" "mlua/vendored") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53"))))))

(define-public crate-haproxy-api-0.3.0-beta.1 (c (n "haproxy-api") (v "0.3.0-beta.1") (d (list (d (n "mlua") (r "^0.6.0-beta.3") (f (quote ("async" "send" "serialize"))) (d #t) (k 0)))) (h "0nkk5lyqz6zkz5r3ckhwciffw73j24qd5fyfps20xpa7b0c0ibfm") (f (quote (("vendored" "mlua/vendored") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53"))))))

(define-public crate-haproxy-api-0.3.0 (c (n "haproxy-api") (v "0.3.0") (d (list (d (n "mlua") (r "^0.6.0") (f (quote ("async" "send" "serialize"))) (d #t) (k 0)))) (h "0a779y1rnjdblagdn20hdrcmrvdi6x1nbca6k2sjngh8qnkp39qa") (f (quote (("vendored" "mlua/vendored") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53"))))))

(define-public crate-haproxy-api-0.4.0 (c (n "haproxy-api") (v "0.4.0") (d (list (d (n "mlua") (r "^0.7") (f (quote ("async" "send" "serialize"))) (d #t) (k 0)))) (h "012s6b09vl9mlhw64s0xccry0mrqmhlfc63wsh76zzg07fz1hxsf") (f (quote (("vendored" "mlua/vendored") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53"))))))

(define-public crate-haproxy-api-0.5.0 (c (n "haproxy-api") (v "0.5.0") (d (list (d (n "mlua") (r "^0.8") (f (quote ("async" "serialize"))) (d #t) (k 0)))) (h "1aj4cbp47dhzjl9ca4m433xm9j2y6afsvanai3ybgns8y1ilm7wp") (f (quote (("vendored" "mlua/vendored") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53"))))))

(define-public crate-haproxy-api-0.6.0 (c (n "haproxy-api") (v "0.6.0") (d (list (d (n "mlua") (r "^0.8") (f (quote ("async" "serialize"))) (d #t) (k 0)))) (h "0aa4947ap3a8kr3k3ih06yq9z9r2j6kjc5bawa3bvcyr28q3y69x") (f (quote (("vendored" "mlua/vendored") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53"))))))

(define-public crate-haproxy-api-0.7.0 (c (n "haproxy-api") (v "0.7.0") (d (list (d (n "mlua") (r "^0.9") (f (quote ("async" "serialize" "module"))) (d #t) (k 0)))) (h "1d6wcc1xbsyjry35jgsgn9a889dbla607lkf7vzd2fi4xhkk1678") (f (quote (("lua54" "mlua/lua54") ("lua53" "mlua/lua53"))))))

