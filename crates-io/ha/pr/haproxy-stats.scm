(define-module (crates-io ha pr haproxy-stats) #:use-module (crates-io))

(define-public crate-haproxy-stats-0.1.0 (c (n "haproxy-stats") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "csv") (r "^1.1") (k 0)) (d (n "duration-str") (r "^0.3") (f (quote ("serde"))) (k 0)) (d (n "semver") (r "^1.0") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)))) (h "11vnd1vlx90yvh4jl733ws51karc76wyb226x1kkf9wir428ic7w")))

