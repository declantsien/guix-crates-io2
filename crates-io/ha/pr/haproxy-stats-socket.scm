(define-module (crates-io ha pr haproxy-stats-socket) #:use-module (crates-io))

(define-public crate-haproxy-stats-socket-0.1.0 (c (n "haproxy-stats-socket") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-util-either") (r "^0.1") (f (quote ("std" "tokio_io"))) (k 0)) (d (n "haproxy-stats") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1.17") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1j4am9sz94axbvr1nag9xspdqm4yi4b6wgdfyfca0qxlf82fq39s") (f (quote (("default") ("_integration_tests"))))))

