(define-module (crates-io ha nj hanjie) #:use-module (crates-io))

(define-public crate-hanjie-0.1.0 (c (n "hanjie") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)))) (h "1cnp7p4r4gm3nh41a14mgmhy4sp7sjmjhfqsp59hc12xq45972pj") (r "1.75")))

(define-public crate-hanjie-0.1.1 (c (n "hanjie") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "1dljsg8qxjja3r8bxrl7r9k6awvag9pmwk3bgq2kilqicig8pj97") (r "1.75")))

(define-public crate-hanjie-0.1.2 (c (n "hanjie") (v "0.1.2") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "102w3vpn7d6gcibikn8i4ad1zwprv8ly48paj7y5c4ch1sn47lss") (r "1.75")))

