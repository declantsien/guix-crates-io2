(define-module (crates-io ha nj hanja) #:use-module (crates-io))

(define-public crate-hanja-0.1.0 (c (n "hanja") (v "0.1.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.21") (d #t) (k 1)))) (h "06i54syf9vj7mxyxnnhzxnj2cx3n58q0qgcs838yxblcdf5sgcnj")))

(define-public crate-hanja-0.1.1 (c (n "hanja") (v "0.1.1") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.21") (d #t) (k 1)))) (h "1r6xg4v746bhdhrrxv6xz7xnbdfq6kzlh0wgi75lzd1pdn7h6cgj")))

