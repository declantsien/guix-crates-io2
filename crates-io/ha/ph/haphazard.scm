(define-module (crates-io ha ph haphazard) #:use-module (crates-io))

(define-public crate-haphazard-0.0.0 (c (n "haphazard") (v "0.0.0") (h "1v0d7qgr1z56v1l9zvsic8nhfs6nz74pf3kbwsywdd75nab583xr")))

(define-public crate-haphazard-0.1.0 (c (n "haphazard") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "16b7mc03d6gabsrji3z134x1pnich2f3gw4klqx7yaxsc48crfwl") (f (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1.1 (c (n "haphazard") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1mdnsdx0ig3qgr984w2bhgfqrs99cnkm2wavxq1a0nv7kdbqfkbb") (f (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1.2 (c (n "haphazard") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1k8m82ayn0qnkai6r43d67l8v9vm52cnm8lr3yszndfkwqzykci4") (f (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1.3 (c (n "haphazard") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "0r8yfgznzq0zig99g523rq2z67zph7jc5piav615sp9k8qmkk786") (f (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1.4 (c (n "haphazard") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1nsnf7v17adp7a45zjr4yjk4ig7xbwy5gbd5q1n421ccncwrgd89") (f (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1.5 (c (n "haphazard") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1abk5s8x3qxyffxz28wxl6239dk81hignhsw8gsknidi9dc7d1gp") (f (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1.6 (c (n "haphazard") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1zfgvb6db2pvc7cw73icwqw00yqkrs82qfzr4rd6707pbhrgb5lb") (f (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1.7 (c (n "haphazard") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "0vqhybsrqcgmxny9vxf9wqxb8ka0ikr395wn8nnr6zv0l028ysnp") (f (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1.8 (c (n "haphazard") (v "0.1.8") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "loom") (r "^0.7.1") (d #t) (t "cfg(loom)") (k 0)) (d (n "regex") (r "^1.6") (o #t) (d #t) (t "cfg(any())") (k 0)) (d (n "serde") (r "^1.0.100") (o #t) (d #t) (t "cfg(any())") (k 0)))) (h "0djwq3sz26vh671jzj0m7xadnzv10fkda20bmkhw38dal1kg9jzf") (f (quote (("std") ("default" "std")))) (r "1.60.0")))

