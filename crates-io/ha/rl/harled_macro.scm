(define-module (crates-io ha rl harled_macro) #:use-module (crates-io))

(define-public crate-harled_macro-0.1.0 (c (n "harled_macro") (v "0.1.0") (d (list (d (n "harled_core") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r1nb9w08icyi3m83zb6yr90dyq1hmagcg7i21xic2017vcz5z4d") (y #t)))

(define-public crate-harled_macro-0.2.0 (c (n "harled_macro") (v "0.2.0") (d (list (d (n "harled_core") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ilq5x04w6932q9k4bfjjx2i355wsp5x7xxmsc6jy3wjqampzvcb")))

(define-public crate-harled_macro-0.3.0 (c (n "harled_macro") (v "0.3.0") (d (list (d (n "harled_core") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13s7pdzx0r8x0xhraarcazhfnhnn4dhhf8la156b67cpi93m8ysb")))

(define-public crate-harled_macro-0.4.1 (c (n "harled_macro") (v "0.4.1") (d (list (d (n "harled_core") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s3820j4lz57bykfspcismkdk901h5w0bcrd28xf944f6wcn36j7")))

