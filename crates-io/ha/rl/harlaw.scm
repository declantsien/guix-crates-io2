(define-module (crates-io ha rl harlaw) #:use-module (crates-io))

(define-public crate-harlaw-1.0.0 (c (n "harlaw") (v "1.0.0") (d (list (d (n "insta") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0whqw11fxgfwbphq24k5iwrv4izc2ni5cq3qacxds0zzhz262h7c")))

(define-public crate-harlaw-1.1.0 (c (n "harlaw") (v "1.1.0") (d (list (d (n "insta") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f11p8f6dg5qz4j3730yvajvcj6prhd9naw5740ncxdi3f1wxihm")))

(define-public crate-harlaw-1.1.1 (c (n "harlaw") (v "1.1.1") (d (list (d (n "insta") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n4abqgmzcp0c051k6arrsk99cd0wmkr8n29mx8nyxdc7b4ills2")))

