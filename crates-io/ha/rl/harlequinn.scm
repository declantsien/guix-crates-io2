(define-module (crates-io ha rl harlequinn) #:use-module (crates-io))

(define-public crate-harlequinn-0.1.0 (c (n "harlequinn") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "quinn") (r "^0.5.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.7.0") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.1") (f (quote ("rt-threaded" "sync" "time"))) (d #t) (k 0)))) (h "0dvh71cfins7zz1y0c1ax8zp8jf11dwka2zj7a0p1gxgs964raba")))

(define-public crate-harlequinn-0.1.1 (c (n "harlequinn") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "quinn") (r "^0.5.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.7.0") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.1") (f (quote ("rt-threaded" "sync" "time"))) (d #t) (k 0)))) (h "0qkyivvlb77by19wzg5anm0abvrrigha6ssx8665m0p7wb7w1shj")))

