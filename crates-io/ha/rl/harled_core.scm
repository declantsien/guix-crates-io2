(define-module (crates-io ha rl harled_core) #:use-module (crates-io))

(define-public crate-harled_core-0.1.0 (c (n "harled_core") (v "0.1.0") (d (list (d (n "giftwrap") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cpswn7khmx3lv0hszdxg3lkpysaw24ggxc456i6rvbwfyq2aplq") (y #t)))

(define-public crate-harled_core-0.2.0 (c (n "harled_core") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00hywgyclbjjp3wrsyjis60ggi0lynb9ff6s5rwr8ckna9zkfrgb")))

(define-public crate-harled_core-0.3.0 (c (n "harled_core") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lk8qi9vslr3r9bhlsj3xl7dk9b1qdklbzw27rfcq95khdg3l6x5")))

(define-public crate-harled_core-0.4.0 (c (n "harled_core") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l9db4gbn3kfb4dsx3kc2wqn8f461lfrkrhlppzzx1mp6pff6r6f") (y #t)))

(define-public crate-harled_core-0.4.1 (c (n "harled_core") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z4i6li1381w0mawkp675yd9n1gnbq4fa0dri6p5bhj0n34cib30")))

