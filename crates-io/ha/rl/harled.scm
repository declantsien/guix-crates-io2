(define-module (crates-io ha rl harled) #:use-module (crates-io))

(define-public crate-harled-0.1.0 (c (n "harled") (v "0.1.0") (d (list (d (n "harled_core") (r "^0.1") (d #t) (k 0)) (d (n "harled_macro") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pk3kghhvp3rrigqbw5z2p0k6lgl605gvzyz57jxpsvyrmvry89q") (y #t)))

(define-public crate-harled-0.2.0 (c (n "harled") (v "0.2.0") (d (list (d (n "harled_core") (r "^0.2") (d #t) (k 0)) (d (n "harled_macro") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k73capm02phx3f6dhnym1n42fz9v0hq54g2lnydyv4j1jmcigd2")))

(define-public crate-harled-0.3.0 (c (n "harled") (v "0.3.0") (d (list (d (n "harled_core") (r "^0.3") (d #t) (k 0)) (d (n "harled_macro") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zdg9vdp8wxp2c3g25vy5bi4zwwd1m744ckdg30n9qamghzj3vqh")))

(define-public crate-harled-0.4.0 (c (n "harled") (v "0.4.0") (d (list (d (n "harled_core") (r "^0.3") (d #t) (k 0)) (d (n "harled_macro") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bk9wvrjvczdcjac0nqmnrlmaskd3yhzlchnxlg8rhcnig32spfb") (y #t)))

(define-public crate-harled-0.4.1 (c (n "harled") (v "0.4.1") (d (list (d (n "harled_core") (r "^0.4") (d #t) (k 0)) (d (n "harled_macro") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1clsbd1p0rjk799kblcqpcybzbc7kzjg2ymp6mqdwpialmf5yvpr")))

