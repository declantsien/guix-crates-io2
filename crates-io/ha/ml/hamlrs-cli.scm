(define-module (crates-io ha ml hamlrs-cli) #:use-module (crates-io))

(define-public crate-hamlrs-cli-0.1.0 (c (n "hamlrs-cli") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "hamlrs") (r "^0.3.0") (d #t) (k 0)))) (h "0sydmk2qb779rxia9r9b7llc536dphqd1zmghpjp3jya7rb38da3")))

(define-public crate-hamlrs-cli-0.1.1 (c (n "hamlrs-cli") (v "0.1.1") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "hamlrs") (r "^0.3.0") (d #t) (k 0)))) (h "14mhn83lk063yiylzkrxg2y3agdw66q0bql17x8pp2m5cw7091xr")))

(define-public crate-hamlrs-cli-0.1.2 (c (n "hamlrs-cli") (v "0.1.2") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "hamlrs") (r "^0.4.0") (d #t) (k 0)))) (h "0m255kvdix2hnkbc6zkqn06vpwvsdak3ypqcggrxmhgy60l581dm")))

(define-public crate-hamlrs-cli-0.2.0 (c (n "hamlrs-cli") (v "0.2.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "hamlrs") (r "^0.4.1") (d #t) (k 0)))) (h "1j0z8zqz2bb33yypmvrnrzqdmyd35br6sxy9qwnwx5a1x1mvmgvr")))

(define-public crate-hamlrs-cli-0.4.2 (c (n "hamlrs-cli") (v "0.4.2") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "hamlrs") (r "^0.4.2") (d #t) (k 0)))) (h "16cwrs0s3palcwdbqhvnhqis29c3kpmvbmnz3fxw3kjr8yhi952q")))

