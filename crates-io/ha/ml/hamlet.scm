(define-module (crates-io ha ml hamlet) #:use-module (crates-io))

(define-public crate-hamlet-0.1.0 (c (n "hamlet") (v "0.1.0") (h "0sv4ds64yijx3kh1180fjlzs0j9knaii314kk1mvnn7jzxfvczpx")))

(define-public crate-hamlet-0.2.0 (c (n "hamlet") (v "0.2.0") (h "0qalkimk32s3xdic2h2n8c1qx0rw6mp4lv8mrhil5hgbnyymqkf5")))

(define-public crate-hamlet-0.2.1 (c (n "hamlet") (v "0.2.1") (h "0jdn817nkynfh8n7zd6qkv8x4dl6zj4ay7qci49hr7dsjg8657zj")))

