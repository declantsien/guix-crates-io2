(define-module (crates-io ha ml haml) #:use-module (crates-io))

(define-public crate-haml-0.1.0 (c (n "haml") (v "0.1.0") (h "1dijim5qrg8zy842q20x6awkvwml9fxjy8323liz73rbw2p34fph")))

(define-public crate-haml-0.1.1 (c (n "haml") (v "0.1.1") (h "0wcasmcvva55dhqnxkkb5dqn5yxz5azr6sda60x1i8438vnhll39")))

(define-public crate-haml-0.1.2 (c (n "haml") (v "0.1.2") (h "0biwxa0240a08ab2d4a3np2jlp0s3lb0nvyaqjhbw147d5jkax6b")))

