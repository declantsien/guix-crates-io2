(define-module (crates-io ha ml hamlrs) #:use-module (crates-io))

(define-public crate-hamlrs-0.0.1 (c (n "hamlrs") (v "0.0.1") (h "0399a79z1wd1qxf9ixcd8dhx360i7f5lcj2zc25y5nz5c0zrjawm") (y #t)))

(define-public crate-hamlrs-0.1.0 (c (n "hamlrs") (v "0.1.0") (h "11qjr7c91afid6c85558s06q1hg3f9cqs49vhhw9lx1zdzyi5djh")))

(define-public crate-hamlrs-0.1.1 (c (n "hamlrs") (v "0.1.1") (h "1kar5gvrm6kxmq82m3lnca4raxzz96p14fdz8s6p1dq52c366vlv")))

(define-public crate-hamlrs-0.1.2 (c (n "hamlrs") (v "0.1.2") (h "15pfmdwdd6krpm1xysxzy9i16hfxlk6l4q7f6n5pjb1p6286syg3")))

(define-public crate-hamlrs-0.2.0 (c (n "hamlrs") (v "0.2.0") (h "1vsj61n2ls9rxydz5kmlkx3j7wf11dmaf6zcrwgn9a9k5q6cdwq0")))

(define-public crate-hamlrs-0.2.1 (c (n "hamlrs") (v "0.2.1") (h "0lf4xgfzsvgmscsbd4sald9dcayz1j6kqxkskslwsw433vk4hkbv")))

(define-public crate-hamlrs-0.3.0 (c (n "hamlrs") (v "0.3.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)))) (h "1ssyx3vr1fy0npfmp7jk0i6xplnzsw504p39khiq103jjilfinp2")))

(define-public crate-hamlrs-0.4.0 (c (n "hamlrs") (v "0.4.0") (h "1hv4xi5z98wcihb3p9r3q3b31n6839jfcih11l0k5310znphyp12")))

(define-public crate-hamlrs-0.4.1 (c (n "hamlrs") (v "0.4.1") (h "0nvk21njz6m8yby2dq8n61qzphk8gwwy2q13n12sy9g9zx7knkap")))

(define-public crate-hamlrs-0.4.2 (c (n "hamlrs") (v "0.4.2") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)))) (h "13yw7sky6akm120w8qazhy0v7k2a6n6cq44h5xls8yb74hxp60dw")))

(define-public crate-hamlrs-0.4.3 (c (n "hamlrs") (v "0.4.3") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)))) (h "138pj1lh8ki54ngpj4jpi3gp5m1yzr42jyli3zl5m12lmg6401jw")))

