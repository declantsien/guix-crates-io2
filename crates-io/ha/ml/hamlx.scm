(define-module (crates-io ha ml hamlx) #:use-module (crates-io))

(define-public crate-hamlx-0.1.0 (c (n "hamlx") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rapid-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "rapid-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "1c2cv51wr1z4pfc10h2x6rdx0cs2ys0abigyc3r9zffydnkw8pz7")))

(define-public crate-hamlx-0.1.1 (c (n "hamlx") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rapid-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "rapid-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "0vkybnq771zldndrqlv9qjdiskrnzwkgh1qzzwwfisw9wkc2zqy7")))

(define-public crate-hamlx-0.1.2 (c (n "hamlx") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rapid-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "rapid-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "11vgvkkj24b0f7lsa3l858b4658f3x3ddlbcc0d0zyb7q043rh1g")))

(define-public crate-hamlx-0.1.3 (c (n "hamlx") (v "0.1.3") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rapid-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "rapid-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "0hb3arvl06vch11wz8f7s282grcvivmbv2kzli9d98gwz9gr62af")))

(define-public crate-hamlx-0.1.4 (c (n "hamlx") (v "0.1.4") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rapid-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "rapid-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "1m8jp5vfb2b94c1gd7wrqwj14pcd4plykyikjpwvay02z64zqwmn")))

(define-public crate-hamlx-0.1.5 (c (n "hamlx") (v "0.1.5") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rapid-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "rapid-utils") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "1fr65v77v3f2yy07n2a71fvcmcvd3dxk4w0s04knxz5azzplg8my")))

(define-public crate-hamlx-0.1.6 (c (n "hamlx") (v "0.1.6") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rapid-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "rapid-utils") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "12l7lhn8ddi6aksffqr17h7lvamlav3fdaxr5jjc7ldz60mr8364")))

(define-public crate-hamlx-0.1.7 (c (n "hamlx") (v "0.1.7") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rapid-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "rapid-utils") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "0wqk6j608qigyj2f55klxyk529cpdrqnf0lrx4d58s9cl9sjd3xk")))

(define-public crate-hamlx-0.1.8 (c (n "hamlx") (v "0.1.8") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rapid-fs") (r "^0.1.1") (d #t) (k 0)) (d (n "rapid-utils") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "196ppw6l9r8q210misnipdwikz38w1ad6kz8rf0vqqw02q5b5ivi")))

(define-public crate-hamlx-0.1.10 (c (n "hamlx") (v "0.1.10") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rapid-fs") (r "^0.1.1") (d #t) (k 0)) (d (n "rapid-utils") (r "^0.1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "0as3sgi81gwd1zfv5avdk2swzazzr4i1zlfc7dr2rind9a7pqa53")))

