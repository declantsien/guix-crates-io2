(define-module (crates-io ha ve haversine-rs) #:use-module (crates-io))

(define-public crate-haversine-rs-0.1.0 (c (n "haversine-rs") (v "0.1.0") (h "11700rghx3l5qlcvbk4ailffkncpalppfmsd4ry4scmn548c633d")))

(define-public crate-haversine-rs-0.2.0 (c (n "haversine-rs") (v "0.2.0") (h "09d6r59zm02q2prs8wjn0scxi1cmxz2vjsaqvlqkqfrfr1d8h1vb")))

(define-public crate-haversine-rs-0.2.1 (c (n "haversine-rs") (v "0.2.1") (h "0dwznhrz1jmlgnqhjn0wgbqw7zh5s51ap94bb5qb2wlkrmf7p6wh")))

(define-public crate-haversine-rs-0.2.2 (c (n "haversine-rs") (v "0.2.2") (h "1cf7ii2hvxhc2m9hvnp1i4s3j6gkjv9yrp07p9an2c6mxciwk3ak")))

(define-public crate-haversine-rs-0.3.0 (c (n "haversine-rs") (v "0.3.0") (h "07pp1xn0wf2drd26k5cm9nl2j7scazv89b01acqz5cmsk1igjf59")))

