(define-module (crates-io ha ve haveibeenrusted) #:use-module (crates-io))

(define-public crate-haveibeenrusted-0.1.0 (c (n "haveibeenrusted") (v "0.1.0") (d (list (d (n "base16ct") (r "^0.1.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("gzip" "brotli"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mibs8r4mcixvp58xdwil6wa084apsm4i458q24vsp9nrdqwm44m")))

(define-public crate-haveibeenrusted-0.1.1 (c (n "haveibeenrusted") (v "0.1.1") (d (list (d (n "base16ct") (r "^0.1.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("gzip" "brotli"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gzjb9wzjzv0wvp2lhhdckhhyykiz087rq7f6xc7c61yr6vwhdim")))

(define-public crate-haveibeenrusted-0.1.2 (c (n "haveibeenrusted") (v "0.1.2") (d (list (d (n "base16ct") (r "^0.1.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("gzip" "brotli"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kww7x7036vl0dh8v1jcn9r3x0bgjw7z84kamj5nq8rvgifk5sm5")))

