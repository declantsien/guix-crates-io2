(define-module (crates-io ha ve haversine) #:use-module (crates-io))

(define-public crate-haversine-0.2.0 (c (n "haversine") (v "0.2.0") (h "0lbj770dvj7sz7hragdnyb6lawkj0qrz82fd9kv24vk17gd2v5w7")))

(define-public crate-haversine-0.2.1 (c (n "haversine") (v "0.2.1") (h "0q56ysqi99ay29krrn751gfd3484d8ry4pzbraiqjz91idh8557g")))

