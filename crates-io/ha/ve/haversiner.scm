(define-module (crates-io ha ve haversiner) #:use-module (crates-io))

(define-public crate-haversiner-0.1.0 (c (n "haversiner") (v "0.1.0") (h "0q8zb72jhmwl0qksx4ya8l0cvdgizny0437a4pipb899hl0yzsgr")))

(define-public crate-haversiner-0.2.0 (c (n "haversiner") (v "0.2.0") (h "1862675s0l7kz21ag4i9k9sz0vygsrc65kigfmfp8338m6q0fl9l")))

(define-public crate-haversiner-0.2.1 (c (n "haversiner") (v "0.2.1") (h "034zzhiagmcl1w2kp6qbhrknwkv25616drs8l5lvgmqwxv7b16s5")))

(define-public crate-haversiner-0.3.0 (c (n "haversiner") (v "0.3.0") (h "0d5fwm0rjfxi3fb139ybbq8fw3kp6m6yd7af4dyxv099i6c88jwl")))

