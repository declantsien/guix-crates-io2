(define-module (crates-io ha ve have) #:use-module (crates-io))

(define-public crate-have-0.1.0 (c (n "have") (v "0.1.0") (h "1mzkgp1f0q5wpmaddyvzj0j1sdllbdhkh04fngvpxpjhzzwb5iwy")))

(define-public crate-have-0.1.1 (c (n "have") (v "0.1.1") (h "0akhbgfi1r4w17d0c2n4hrl41gn3czmcjma1lgg5adazq3q0d4qj")))

