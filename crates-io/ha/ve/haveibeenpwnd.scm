(define-module (crates-io ha ve haveibeenpwnd) #:use-module (crates-io))

(define-public crate-haveibeenpwnd-0.1.0 (c (n "haveibeenpwnd") (v "0.1.0") (d (list (d (n "error-chain") (r "0.4.*") (d #t) (k 0)) (d (n "hyper") (r "0.9.*") (d #t) (k 0)) (d (n "serde") (r "0.8.*") (d #t) (k 0)) (d (n "serde_json") (r "0.8.*") (d #t) (k 0)) (d (n "url") (r "1.2.*") (d #t) (k 0)))) (h "0gs5sc0f2i7h1qnhx3iqd8210ing1g7n6zyqza5l74sbxb9rmsnn")))

(define-public crate-haveibeenpwnd-0.2.0 (c (n "haveibeenpwnd") (v "0.2.0") (d (list (d (n "error-chain") (r "0.4.*") (d #t) (k 0)) (d (n "hyper") (r "0.9.*") (d #t) (k 0)) (d (n "serde") (r "0.8.*") (d #t) (k 0)) (d (n "serde_json") (r "0.8.*") (d #t) (k 0)) (d (n "url") (r "1.2.*") (d #t) (k 0)))) (h "1qyvfdw69l953ywdbz4fg2ikhh4fkwxhdsx1g90xhc5ig0s4nvas")))

(define-public crate-haveibeenpwnd-0.2.1 (c (n "haveibeenpwnd") (v "0.2.1") (d (list (d (n "error-chain") (r "0.4.*") (d #t) (k 0)) (d (n "hyper") (r "0.10.*") (d #t) (k 0)) (d (n "hyper-rustls") (r "0.3.*") (d #t) (k 0)) (d (n "serde") (r "0.8.*") (d #t) (k 0)) (d (n "serde_json") (r "0.8.*") (d #t) (k 0)) (d (n "url") (r "1.2.*") (d #t) (k 0)))) (h "06j9vdf36yrgjh83pmkhk1dpwv5b90nh6664dl7z30s2bm27h3qx")))

