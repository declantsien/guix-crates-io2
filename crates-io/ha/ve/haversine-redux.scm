(define-module (crates-io ha ve haversine-redux) #:use-module (crates-io))

(define-public crate-haversine-redux-0.1.0 (c (n "haversine-redux") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "035a7nak49pf47w73d3ykri6l6s9w2w4qgdqvr4c0qg7vlpyy10x") (f (quote (("no_std" "libm") ("default"))))))

(define-public crate-haversine-redux-0.2.0 (c (n "haversine-redux") (v "0.2.0") (d (list (d (n "libm") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "1xfn0piwa1ak6r6yblb3p45bakcwxyk366510gk85jid171i3prr") (f (quote (("no_std" "libm") ("default"))))))

(define-public crate-haversine-redux-0.2.1 (c (n "haversine-redux") (v "0.2.1") (d (list (d (n "libm") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "0zn036cw6wi1p39nj563wlbx3x97dlpnjwnfkhv24a6fvl8zmgra") (f (quote (("no_std" "libm") ("default"))))))

