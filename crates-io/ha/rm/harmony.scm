(define-module (crates-io ha rm harmony) #:use-module (crates-io))

(define-public crate-harmony-0.1.0 (c (n "harmony") (v "0.1.0") (d (list (d (n "itertools") (r "^0.4.10") (d #t) (k 0)))) (h "009p0inanj3bnbmx8x2qkfciyk7qhhhp835m3m2xrixk90waw2gi")))

(define-public crate-harmony-0.1.1 (c (n "harmony") (v "0.1.1") (d (list (d (n "itertools") (r "^0.4.10") (d #t) (k 0)))) (h "08zc8a6fq0axqk2rx9nhpb2cdcxbz5jm98l81j89744prjkalscs")))

(define-public crate-harmony-0.1.2 (c (n "harmony") (v "0.1.2") (d (list (d (n "itertools") (r "^0.4.10") (d #t) (k 0)))) (h "0x9qv61vhrq8bnn8zyz0165i2m900ijpqabv2h07ckx8k7kfwi89")))

(define-public crate-harmony-0.1.3 (c (n "harmony") (v "0.1.3") (d (list (d (n "itertools") (r "^0.4.10") (d #t) (k 0)))) (h "157vpm85gpgg0i57bqf8s21fmqpkqsx7nxxcd2rmvsy5scrd17h0")))

(define-public crate-harmony-0.2.0 (c (n "harmony") (v "0.2.0") (d (list (d (n "itertools") (r "^0.4.10") (d #t) (k 0)))) (h "15g9yr5cmcxi2y7q7366jv6dpccz5ycsn61xrc33cvddvcbnibaf")))

(define-public crate-harmony-0.2.1 (c (n "harmony") (v "0.2.1") (d (list (d (n "itertools") (r "^0.4.10") (d #t) (k 0)))) (h "0rdvcfilzk8d2jjdq0wq4fjpha49412v0238wpzzndsxawbk02rj")))

