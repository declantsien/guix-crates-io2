(define-module (crates-io ha rm harmony_derive) #:use-module (crates-io))

(define-public crate-harmony_derive-0.1.0 (c (n "harmony_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cfb21ynq06bjsf2wpv0nasx8q8sjlckmsy9qf1x1rf55vi8ivaq")))

(define-public crate-harmony_derive-0.1.1 (c (n "harmony_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kwqza9xsdq1zhaf0vbnm80afjpm9mnakx0l2pcqkdvy9sxylv9j")))

(define-public crate-harmony_derive-0.1.2 (c (n "harmony_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rmbzj5859xdmgrjsydmaszkgrydf4wyfwa67sckdxyc3zj7idwd")))

(define-public crate-harmony_derive-0.1.3 (c (n "harmony_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xsycvw4wqwwr48w8iyb4ycz64r3ad9vnpzrqncn4d5v6axqn4rd") (f (quote (("client"))))))

