(define-module (crates-io ha z- haz-alloc-core) #:use-module (crates-io))

(define-public crate-haz-alloc-core-0.1.0 (c (n "haz-alloc-core") (v "0.1.0") (d (list (d (n "haz-alloc-internal") (r "^0.0.1") (d #t) (k 0)))) (h "0qiadww6yffyf40l6vjpdzlrx18s9ra0v5kwss9f58wc2imls2w1")))

(define-public crate-haz-alloc-core-0.1.1 (c (n "haz-alloc-core") (v "0.1.1") (d (list (d (n "haz-alloc-internal") (r "^0.0.1") (d #t) (k 0)))) (h "02qwjf5sjpn6kanbarmi8d3r44kvqrp0skbbq934z3ckgbzm3kmk")))

(define-public crate-haz-alloc-core-0.2.0 (c (n "haz-alloc-core") (v "0.2.0") (d (list (d (n "haz-alloc-internal") (r "^0.0.1") (d #t) (k 0)))) (h "0gn34lw1ffhkqfzlikg34mpjc2vsj0yp0ws51kmp5pg5bn85sqd5")))

(define-public crate-haz-alloc-core-0.3.0 (c (n "haz-alloc-core") (v "0.3.0") (h "1pzijzfb52g5j8khblfhalnsiac354bww5zj7wlafrrbc1jnp7f3")))

(define-public crate-haz-alloc-core-0.4.0 (c (n "haz-alloc-core") (v "0.4.0") (h "1mnvc2mc51bs3zkl36lczfqmai2cy5qnjbj0h0grwlbdvdv9pj29")))

