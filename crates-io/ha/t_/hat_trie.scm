(define-module (crates-io ha t_ hat_trie) #:use-module (crates-io))

(define-public crate-hat_trie-0.1.0 (c (n "hat_trie") (v "0.1.0") (d (list (d (n "ahtable") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "0lr0fhbhj2qw7nqpbymhipg2k8hbl845mwmlgi0b08jkxhrnf5vy")))

(define-public crate-hat_trie-0.1.1 (c (n "hat_trie") (v "0.1.1") (d (list (d (n "ahtable") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "0xp615a8fi77p7wakr34jfb1snx6dvzfw4vygnhk3v6iyxnvc4df")))

(define-public crate-hat_trie-0.1.2 (c (n "hat_trie") (v "0.1.2") (d (list (d (n "ahtable") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "1zr8shpw4rf8xlk478xjngxicxjsfg13ydqpv6by73jjnjz6ww2f")))

(define-public crate-hat_trie-0.1.3 (c (n "hat_trie") (v "0.1.3") (d (list (d (n "ahtable") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "1b29y2i5b8b6qrxg316x098zx88bq6mw4g9fbcbg9zhcnja5z9wz")))

(define-public crate-hat_trie-0.1.4 (c (n "hat_trie") (v "0.1.4") (d (list (d (n "ahtable") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "0c5rkx6l28286mybh5pr7vjp4zzk0j044wsrpn0ancbwzxyi0cr7")))

(define-public crate-hat_trie-0.1.5 (c (n "hat_trie") (v "0.1.5") (d (list (d (n "ahtable") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "0hghy5an3mfadn0gfr841z7nbr5802mjmckmj5jhdis1mz7swwdy")))

(define-public crate-hat_trie-0.2.0 (c (n "hat_trie") (v "0.2.0") (d (list (d (n "ahtable") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "1xvkji84ny9b3mbfv1kmaybc9fw70yadb0kavwyf34kgj68cscy8")))

(define-public crate-hat_trie-0.2.1 (c (n "hat_trie") (v "0.2.1") (d (list (d (n "ahtable") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "0zrsgp0gi6fwplf4p1fmcr198p1iqyf79iz9w9f0wcwqky4k2w1w")))

(define-public crate-hat_trie-0.2.2 (c (n "hat_trie") (v "0.2.2") (d (list (d (n "ahtable") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "1j43iynlpnz9v9i7k4dzmk7wh02jchjhf009czp9v5mxfbkm7376")))

(define-public crate-hat_trie-0.2.3 (c (n "hat_trie") (v "0.2.3") (d (list (d (n "ahtable") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "1gv1gjp4pad1fmsl7gk4rqjzy2s62ld0ibp653zhkncsdv2hd0zn")))

(define-public crate-hat_trie-0.2.4 (c (n "hat_trie") (v "0.2.4") (d (list (d (n "ahtable") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "02m7km41lf1wpvy0lpvc4mk2zyz08hnp3zf00fkz0fyxi33022kz")))

