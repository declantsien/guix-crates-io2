(define-module (crates-io ha mm hamming_rs) #:use-module (crates-io))

(define-public crate-hamming_rs-0.1.0 (c (n "hamming_rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hamming") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1yx319hq9c58ara5ibzdxgsbs24pvi0fszrcidr5p25y980h13bk")))

(define-public crate-hamming_rs-0.1.1 (c (n "hamming_rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hamming") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1zm6lrc4in71w2rjynw0l580sd0fz40nm58mzf0181dzxp4p71rj")))

(define-public crate-hamming_rs-0.2.0 (c (n "hamming_rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hamming") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nmczmv4bjs4azp9cizmhydzl905d8vcrym0shhbz926wrycacb7")))

(define-public crate-hamming_rs-0.2.1 (c (n "hamming_rs") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hamming") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1d3qjq69cz4caqkzpinvfpr112fn0d22dps38w66d8703wl99ych")))

(define-public crate-hamming_rs-0.2.2 (c (n "hamming_rs") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hamming") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1dmglbqi6bkl2mfp9c8aj29cks0lsnxv2b5gfq1cxkdml2k558nf")))

(define-public crate-hamming_rs-0.2.21 (c (n "hamming_rs") (v "0.2.21") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hamming") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "strsim") (r "^0.10.0") (d #t) (k 2)))) (h "13crkjfjhi92qd0iivgcivixqnj0y671imkrhzjw317fv885ijyg")))

(define-public crate-hamming_rs-0.2.22 (c (n "hamming_rs") (v "0.2.22") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hamming") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "strsim") (r "^0.10.0") (d #t) (k 2)))) (h "1ncgpfwpg0kff3n8frfkwmw4hxyxrspb4cvcbywd1md3k6kyg3cm")))

