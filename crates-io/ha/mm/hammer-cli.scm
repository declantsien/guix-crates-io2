(define-module (crates-io ha mm hammer-cli) #:use-module (crates-io))

(define-public crate-hammer-cli-0.1.0 (c (n "hammer-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0bbc3rgqlv8s8689p97lihydm7ivmwcaxmcm1c7zs4svzbhw64jc")))

(define-public crate-hammer-cli-0.2.0 (c (n "hammer-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "18z227p89rgxalxci2wl0rmgpk2lgvi1fwzbpqjlmf9nj9hdzyxc")))

(define-public crate-hammer-cli-0.2.1 (c (n "hammer-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1b9d2xbwxnmk215rqajm75icvkpjsydhfcl91g2wg1039gayy7ji")))

(define-public crate-hammer-cli-0.2.2 (c (n "hammer-cli") (v "0.2.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "10fqkfh4rdfyyg4gmd839g62kmjjgk4dskcrxpkwmpj3581x33dg")))

(define-public crate-hammer-cli-0.2.3 (c (n "hammer-cli") (v "0.2.3") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0g792lphjzdxbcpxi677fww6nbckhr7dljaxp0lq6xg666s2avaz")))

(define-public crate-hammer-cli-0.3.0 (c (n "hammer-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "177b1vz7n56515g5r12i464bfnd4ibyjz5dln7m42i2i2h8i5n21")))

(define-public crate-hammer-cli-0.3.1 (c (n "hammer-cli") (v "0.3.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1pk4i527jzzy58z1nkxim39s7yhrrzg65mfdr4c0cf6vq7m7i0kd")))

