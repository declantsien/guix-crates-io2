(define-module (crates-io ha mm hammer) #:use-module (crates-io))

(define-public crate-hammer-0.1.0 (c (n "hammer") (v "0.1.0") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "mount") (r "^0.3") (d #t) (k 0)) (d (n "staticfile") (r "^0.4.0") (d #t) (k 0)))) (h "0mmmxw5ha7i69kxwdxrgrawpfbg5h0f6bnwc7a07gsaln2a380zv")))

