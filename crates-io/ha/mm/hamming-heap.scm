(define-module (crates-io ha mm hamming-heap) #:use-module (crates-io))

(define-public crate-hamming-heap-0.1.0 (c (n "hamming-heap") (v "0.1.0") (h "1fcjd8bdq2ckld212nh08pdlq6p10w3hyn5hil2fy56dd4yrzmkr")))

(define-public crate-hamming-heap-0.2.0 (c (n "hamming-heap") (v "0.2.0") (h "1vabz8w405rp8xwp0s2yyh66lbzg322a02dad6r9a2prnmy5crpy")))

(define-public crate-hamming-heap-0.3.0 (c (n "hamming-heap") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.13.2") (d #t) (k 0)))) (h "1kbkq0wmczhm8nvkqgxlk71rapy77nah3fmjwk1vrb29v4pvfcba")))

(define-public crate-hamming-heap-0.4.0 (c (n "hamming-heap") (v "0.4.0") (h "09bl7grkql9394qrlrjsjh9jk4a89xapmi0ckv189jj9k5vipx3i")))

(define-public crate-hamming-heap-0.4.1 (c (n "hamming-heap") (v "0.4.1") (h "1gwj4l35cjgz0dkzkc5jh69zzscjw6r6p2dg1f9wx9ly1946f42i")))

