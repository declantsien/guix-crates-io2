(define-module (crates-io ha mm hamming-lsh) #:use-module (crates-io))

(define-public crate-hamming-lsh-0.1.0 (c (n "hamming-lsh") (v "0.1.0") (d (list (d (n "bitarray") (r "^0.5.1") (k 0)) (d (n "hamming-dict") (r "^0.4.0") (f (quote ("alloc"))) (k 0)) (d (n "rand") (r "^0.8.4") (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (k 0)))) (h "0gxaa90717bkj0h6ds1h0vkp7qn6r08k16y8j5c8aind3dgndyb7")))

(define-public crate-hamming-lsh-0.2.0 (c (n "hamming-lsh") (v "0.2.0") (d (list (d (n "bitarray") (r "^0.5.1") (k 0)) (d (n "hamming-dict") (r "^0.4.0") (f (quote ("alloc"))) (k 0)) (d (n "rand") (r "^0.8.4") (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1n9h2gvikfvmcz0h06f91gbpm7s5ghhmwvzhl8i8h0h8fs0qgkqp") (f (quote (("serde1" "serde" "bitarray/serde"))))))

(define-public crate-hamming-lsh-0.3.0 (c (n "hamming-lsh") (v "0.3.0") (d (list (d (n "bitarray") (r "^0.9.1") (k 0)) (d (n "hamming-dict") (r "^0.5.0") (f (quote ("alloc"))) (k 0)) (d (n "rand") (r "^0.8.4") (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1vl7wbcvpw5c053z9dkz5fd21ml6ypmm0r767v1nmjv7lkf464b9") (f (quote (("serde1" "serde" "bitarray/serde"))))))

(define-public crate-hamming-lsh-0.3.1 (c (n "hamming-lsh") (v "0.3.1") (d (list (d (n "bitarray") (r "^0.9.1") (k 0)) (d (n "hamming-dict") (r "^0.5.0") (f (quote ("alloc"))) (k 0)) (d (n "rand") (r "^0.8.4") (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1njy6vp494kcgc02hq99gynjy4vdgdlx15hpj30zb4vw11837zns") (f (quote (("serde1" "serde" "bitarray/serde"))))))

(define-public crate-hamming-lsh-0.3.2 (c (n "hamming-lsh") (v "0.3.2") (d (list (d (n "bitarray") (r "^0.9.1") (k 0)) (d (n "hamming-dict") (r "^0.5.0") (f (quote ("alloc"))) (k 0)) (d (n "rand") (r "^0.8.4") (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0q5r6wqvfjv9lm91xs29806b7yn9clnrxhpq6frrsp7pwpfvxmls") (f (quote (("serde1" "serde" "bitarray/serde"))))))

