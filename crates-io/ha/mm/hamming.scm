(define-module (crates-io ha mm hamming) #:use-module (crates-io))

(define-public crate-hamming-0.1.0 (c (n "hamming") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0lk1cqp3v4xifjvynnmlqyilqm8khadwrhy2c5ahj01bp44k71y9") (f (quote (("unstable"))))))

(define-public crate-hamming-0.1.1 (c (n "hamming") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "07idbj404hmy6kdzvz0b9v3i90i74n5alr4pwc27qpxga1n1xilm") (f (quote (("unstable"))))))

(define-public crate-hamming-0.1.2 (c (n "hamming") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "141nc0gcdf5zs1vazy12s0sqw2znqjjwyypzvbv6vc4lpakcxv00") (f (quote (("unstable"))))))

(define-public crate-hamming-0.1.3 (c (n "hamming") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1q9hri1l1x1y7vv153kvdw9lkqslmbwgia5r3qj6i39pfji3s135") (f (quote (("unstable"))))))

