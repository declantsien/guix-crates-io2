(define-module (crates-io ha mm hammer-and-sample) #:use-module (crates-io))

(define-public crate-hammer-and-sample-0.1.0 (c (n "hammer-and-sample") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1vgvck6lljdlylsnfra7a2s9mjy3grijdvc970l91ghwx4wqdmr9")))

(define-public crate-hammer-and-sample-0.1.1 (c (n "hammer-and-sample") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "18kacvy2b1z0zpjgv1lc5gv3rfw9i944yn46c5s4pv3acqzrbip3")))

(define-public crate-hammer-and-sample-0.1.2 (c (n "hammer-and-sample") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "03z7zpzfhqa10z4wk9vxhbrvwawy79xlzya2p007wmy02h9rhj7s")))

(define-public crate-hammer-and-sample-0.1.3 (c (n "hammer-and-sample") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1yzns70lzkr5g1x687d0wrcds3nhyhy0brx7xp4pd5gxxvhizpck")))

(define-public crate-hammer-and-sample-0.1.4 (c (n "hammer-and-sample") (v "0.1.4") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1z1cxwx3xz8fl5x4lpzkn1nf6vy51b0wfwh7dazk58jxr7rgbw7m")))

(define-public crate-hammer-and-sample-0.2.0 (c (n "hammer-and-sample") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)))) (h "0kqcl5231b6w1z30d8j2k3vzmqn6cdkr38nhbqh2fkkpdq5ry63l")))

