(define-module (crates-io ha mm hamming-bow) #:use-module (crates-io))

(define-public crate-hamming-bow-0.1.0 (c (n "hamming-bow") (v "0.1.0") (d (list (d (n "bitarray") (r "^0.9.1") (k 0)) (d (n "hamming-dict") (r "^0.5.0") (f (quote ("alloc"))) (k 0)) (d (n "rand") (r "^0.8.4") (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0a44ln2rl6688gq7ljx12qbhnlyplfzy5l8mnb8id4qb0qnl65rw") (f (quote (("serde1" "serde" "bitarray/serde"))))))

