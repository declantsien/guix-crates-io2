(define-module (crates-io ha ik haiku-sys) #:use-module (crates-io))

(define-public crate-haiku-sys-0.1.0 (c (n "haiku-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1z5lx93ag86xw5l7br3pm21d1k2kypgyyfv9q6rc9djw7hgrwhvg")))

(define-public crate-haiku-sys-0.2.0 (c (n "haiku-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "1shdychk07naijjaxaaiyj2985iy1qq3bhchp0wgv440rvbirx12")))

