(define-module (crates-io ha ik haiku) #:use-module (crates-io))

(define-public crate-haiku-0.1.0 (c (n "haiku") (v "0.1.0") (h "0lrrphzkqrwlhc7i54c4f9ys51ghy1bddkqqiz9k69q72j14b4zn")))

(define-public crate-haiku-0.1.1 (c (n "haiku") (v "0.1.1") (d (list (d (n "haiku-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.6") (d #t) (k 2)))) (h "0dnndii50y5a9ywk52df2n83yrxjc54ihyxa4r4ywhsvf0jdlkvp")))

(define-public crate-haiku-0.2.0 (c (n "haiku") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "haiku-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1ks4l2r22mbjafp9wqcs63nahckdlk7iky5pv2kgy070gmhlbm56")))

