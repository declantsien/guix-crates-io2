(define-module (crates-io ha ik haikunator) #:use-module (crates-io))

(define-public crate-haikunator-0.1.0 (c (n "haikunator") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 2)))) (h "07wj74m5xrdcfyv2pd3bxwlbix7ivw8glzxm26h43gf6zjybmkr1") (f (quote (("nightly"))))))

(define-public crate-haikunator-0.1.1 (c (n "haikunator") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 2)))) (h "11cfjxp5nrvkj94vpb6jnhhj0h9mrjfniz89zby1p2pxw2bsp89i") (f (quote (("nightly"))))))

(define-public crate-haikunator-0.1.2 (c (n "haikunator") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 2)))) (h "0bxz0rm3y8hms21rdgngvz3hkbsw08h8mb2pcanqx8lj0p3rjvjh") (f (quote (("nightly"))))))

