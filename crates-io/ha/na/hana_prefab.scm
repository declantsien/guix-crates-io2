(define-module (crates-io ha na hana_prefab) #:use-module (crates-io))

(define-public crate-hana_prefab-0.1.1 (c (n "hana_prefab") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("file_watcher"))) (d #t) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "12x6m4zm7aa6w2lirg619wy9nkmrsd17wc9s296py7fspqxhq4cz")))

