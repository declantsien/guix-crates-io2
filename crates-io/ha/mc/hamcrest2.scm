(define-module (crates-io ha mc hamcrest2) #:use-module (crates-io))

(define-public crate-hamcrest2-0.1.6 (c (n "hamcrest2") (v "0.1.6") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0lnkdqmwd4ca88fw9fcvyr66mj2scyiiav6fcs6jp9bk6z4bbk3r")))

(define-public crate-hamcrest2-0.2.0 (c (n "hamcrest2") (v "0.2.0") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1s8dc5qfvk7apapfm4n0mbpklkg2zpg02kgvwaq8mbb319zqr30y")))

(define-public crate-hamcrest2-0.2.1 (c (n "hamcrest2") (v "0.2.1") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0vkzdpwy3qcv5p2b4jzzr6gxh687xsm0i0bhahvsls3iyz27zg28")))

(define-public crate-hamcrest2-0.2.2 (c (n "hamcrest2") (v "0.2.2") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "19iv59wfqk8fr1jki7ik76xm9bvkdirby8vp77hk1sswkf2jpyr3")))

(define-public crate-hamcrest2-0.2.3 (c (n "hamcrest2") (v "0.2.3") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1zvx22h5sh7m0ircnla3z0ajj8xxjipdzhh2vwcm4vxmszp7yszv")))

(define-public crate-hamcrest2-0.2.4 (c (n "hamcrest2") (v "0.2.4") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0ldlaisy22syf13zaj2nrd1wc3a6vjdrpgyhzap9li1v59n4xhps")))

(define-public crate-hamcrest2-0.2.5 (c (n "hamcrest2") (v "0.2.5") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0yq7vjhnm8p6y1ynx409f6a0amhlmraa4ank5cyclvhf98dyskj7")))

(define-public crate-hamcrest2-0.2.6 (c (n "hamcrest2") (v "0.2.6") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1v8g6w2b1mgf7y6rl9cq2f4js1pl2jdynb5jamrkwlbbj9xh16qy")))

(define-public crate-hamcrest2-0.3.0 (c (n "hamcrest2") (v "0.3.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0x8hx7jyzz2bl0wf6nir62imd26yhp6qcr7zf76cjpg05p33gy29")))

