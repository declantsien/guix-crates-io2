(define-module (crates-io ha mc hamcrest) #:use-module (crates-io))

(define-public crate-hamcrest-0.1.0 (c (n "hamcrest") (v "0.1.0") (d (list (d (n "num") (r "^0.1.25") (d #t) (k 0)))) (h "1aq4dq2d6xck7cfy8dh2yj3pxmibh7qbap4f0457d25r16s81h97")))

(define-public crate-hamcrest-0.1.1 (c (n "hamcrest") (v "0.1.1") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)))) (h "1m49rf7bnkx0qxja56slrjh44zi4z5bjz5x4pblqjw265828y25z")))

(define-public crate-hamcrest-0.1.2 (c (n "hamcrest") (v "0.1.2") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)))) (h "0yjkj21xnyskhph937zvlsbf2k2c9226pjn637k8qxvp339hglik")))

(define-public crate-hamcrest-0.1.3 (c (n "hamcrest") (v "0.1.3") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)))) (h "0lrvh63gpfaqjbarl5m6rhjfgjzkrb4zbqiy64vqjz4437sygnd6")))

(define-public crate-hamcrest-0.1.4 (c (n "hamcrest") (v "0.1.4") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0manrp74hbjvmz4x8hmpnzq5mhfpx86jc9kn15rcbvcx681pawgq")))

(define-public crate-hamcrest-0.1.5 (c (n "hamcrest") (v "0.1.5") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1xnpdvha6wigr7gbsr6p36lx62ampiwyjp5m9rq0p9fmdzdnc3d7")))

