(define-module (crates-io ha mc hamcall) #:use-module (crates-io))

(define-public crate-hamcall-0.1.0 (c (n "hamcall") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1kz45kkb6z4cw0jbi7rnzsja4j390linp53h5qvxnf3nss1iq5ag")))

(define-public crate-hamcall-0.2.0 (c (n "hamcall") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0wdmy6mjjh42974119xfv3h7b8d1146g28i9arrfn67wpa8p03qn")))

(define-public crate-hamcall-0.2.1 (c (n "hamcall") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1zrh49rzjnjqvp8fc5aqaa2ja4abxgm4wljyrbrsdf326fwfv72q")))

