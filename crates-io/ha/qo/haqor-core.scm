(define-module (crates-io ha qo haqor-core) #:use-module (crates-io))

(define-public crate-haqor-core-0.1.0 (c (n "haqor-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "gzip"))) (d #t) (k 0)) (d (n "sqlite") (r "^0.30.4") (d #t) (k 0)))) (h "0p455waz61z3vz8zfqsxh9bz204pw6ipkr15nrxvbz44zlyi1i1s")))

