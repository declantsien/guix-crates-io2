(define-module (crates-io ha nb hanbun) #:use-module (crates-io))

(define-public crate-hanbun-0.1.0 (c (n "hanbun") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "font8x8") (r "^0.2") (d #t) (k 2)) (d (n "meval") (r "^0.2") (d #t) (k 2)))) (h "04nm2rfbz4pah4rnf1pqdh4y1k7ki6d3mbcn26qa8f6h64cldcm9")))

(define-public crate-hanbun-0.2.0 (c (n "hanbun") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "font8x8") (r "^0.2") (d #t) (k 2)) (d (n "meval") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1p46yi0pvm5g51im2xisbdbcbk8zqbnr364znfrymc3npj2yzlk5")))

(define-public crate-hanbun-0.3.0 (c (n "hanbun") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "font8x8") (r "^0.2") (d #t) (k 2)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "meval") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1wklm4262zn6dx2bwpc3gbvq36bahx0wj3p1yq0wwhx05wjrypgg")))

(define-public crate-hanbun-0.4.0 (c (n "hanbun") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "font8x8") (r "^0.2") (d #t) (k 2)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "meval") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0kpldbld273ps0lkwlybag3f6iqlfq2p46f736syjzds0gbpya1y")))

(define-public crate-hanbun-0.4.1 (c (n "hanbun") (v "0.4.1") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "font8x8") (r "^0.2") (d #t) (k 2)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "meval") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1qd85yxxxiqk9xj93dm13q8ncyiigqagwymz5g7npby1q7c1vmrq")))

