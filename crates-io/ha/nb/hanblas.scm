(define-module (crates-io ha nb hanblas) #:use-module (crates-io))

(define-public crate-hanblas-0.0.1 (c (n "hanblas") (v "0.0.1") (d (list (d (n "blas") (r "^0.20.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "01iln0bnvkxhc4fv5jlwhsvgpkhpqg6v53c1mddi686xmmjmfj5s") (f (quote (("thread") ("naive") ("dynamic_arch") ("default" "dynamic_arch") ("api_lapack") ("api_cblas"))))))

