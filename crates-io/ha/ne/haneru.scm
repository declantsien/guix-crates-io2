(define-module (crates-io ha ne haneru) #:use-module (crates-io))

(define-public crate-haneru-0.0.0 (c (n "haneru") (v "0.0.0") (d (list (d (n "async-rwlock") (r "^1.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "flume") (r "^0.10.4") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rio") (r "^0.9.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0lrlh3yhhsjc6564ichdgvkawd250vvdcmnxw83148ilbgkgbnf4")))

