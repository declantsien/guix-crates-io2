(define-module (crates-io ha za hazard) #:use-module (crates-io))

(define-public crate-hazard-0.1.0 (c (n "hazard") (v "0.1.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "1dnn19fz4fg8k6l9ncxf1q7hn8gvwb3g7wabw1xbm7d4an0fysf6")))

(define-public crate-hazard-0.2.0 (c (n "hazard") (v "0.2.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "12fs5fmqy7hhpimmzmbqnw2q9fz0bbmhj4b4ja0rwgyy4q12fl1v")))

(define-public crate-hazard-0.3.0 (c (n "hazard") (v "0.3.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "0xxshp28ah20sknsi06x7m3kwpjg4rmb7hfgbj1849c5l66k1c0x")))

(define-public crate-hazard-0.3.1 (c (n "hazard") (v "0.3.1") (h "1w82p263ywrfmsias0ydnncj08fg1vjbiikcdnql9nfcn93imqhd")))

