(define-module (crates-io ha lt haltia) #:use-module (crates-io))

(define-public crate-haltia-0.0.0 (c (n "haltia") (v "0.0.0") (h "00jyri4sgknny8fq6ff3hq5s87gmd68wl51a07p0pbhnfh2lk6q3") (f (quote (("default")))) (r "1.70")))

(define-public crate-haltia-0.0.1 (c (n "haltia") (v "0.0.1") (d (list (d (n "haltia-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0qlmg7h7dws0k1k0cf7kxbhrzix7cwrv2lwj25wms3vs0c968wvr") (f (quote (("default")))) (r "1.70")))

