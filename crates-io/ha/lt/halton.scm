(define-module (crates-io ha lt halton) #:use-module (crates-io))

(define-public crate-halton-0.1.0 (c (n "halton") (v "0.1.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)))) (h "1m8gc4zwp9zhx8jcar3v0pqi575kip0shqzahs745kgb12hrcalh")))

(define-public crate-halton-0.2.0 (c (n "halton") (v "0.2.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)))) (h "12am07mbjmhyi267i85930ilpaykgs5rdv0nifkclcgkn6c2i7n2")))

(define-public crate-halton-0.2.1 (c (n "halton") (v "0.2.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)))) (h "1bmc33v9iqh3v0a3w51zxjlgwma6abgr1wb853hinnnqwd498qmy")))

