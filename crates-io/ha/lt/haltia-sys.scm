(define-module (crates-io ha lt haltia-sys) #:use-module (crates-io))

(define-public crate-haltia-sys-0.0.0 (c (n "haltia-sys") (v "0.0.0") (h "168njd21a3fqyj001pr4if0kv2b0jnlwykfvz293c4l8l23jh99a") (f (quote (("default")))) (r "1.70")))

(define-public crate-haltia-sys-0.0.1 (c (n "haltia-sys") (v "0.0.1") (h "0v92qs2sk1qmknq9ja7rm0hfx5wlw67kwj3bhrhnmkg9a8gb8ryr") (f (quote (("default")))) (r "1.70")))

