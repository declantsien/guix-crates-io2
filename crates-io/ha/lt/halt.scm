(define-module (crates-io ha lt halt) #:use-module (crates-io))

(define-public crate-halt-0.1.0 (c (n "halt") (v "0.1.0") (h "11cybflp3q3kc2fh45c5s23q860lql7mrxc2bhpqw180ins5wy6i")))

(define-public crate-halt-0.2.0 (c (n "halt") (v "0.2.0") (h "0ahhv7q5b932358n4wh10abmbpr1a0bgqbd6g34rdbsis1hlqp9r")))

(define-public crate-halt-0.3.0 (c (n "halt") (v "0.3.0") (h "0lfqz7bhnz3c4z46f6laj1icxdsyy4w2bbl7vad2l66bwr5vv3y8")))

(define-public crate-halt-0.4.0 (c (n "halt") (v "0.4.0") (h "14q3v1wx6av2p4izinijqnw13pmd5057rzcvzgffkspflh3z95am")))

(define-public crate-halt-0.5.0 (c (n "halt") (v "0.5.0") (h "1mxv5q9c3hi0fb3h9kzdcimiyby0w9kcx5v5s2ns0j5qa6fcky8i")))

(define-public crate-halt-0.5.1 (c (n "halt") (v "0.5.1") (h "14z5bqkrmnl5lxmc9znwrswspzlyxshwk8zalkwajmms8291cs4g")))

(define-public crate-halt-1.0.0 (c (n "halt") (v "1.0.0") (h "1fwl0ng7kj8c3vc1fphjrjx55rlg76vnhrq0g6kf3lq9zwwijgc0")))

(define-public crate-halt-2.0.0 (c (n "halt") (v "2.0.0") (h "04zh4cxg8ilvllcg3q7kb0kfikxx4smi3dgd1gkrzw2iawk2mgsg")))

(define-public crate-halt-2.0.1 (c (n "halt") (v "2.0.1") (h "0kln2r21c5g4bvjjq7iqkgd303bca8n4cmkavl6jr2d4byz8zgib")))

(define-public crate-halt-2.0.2 (c (n "halt") (v "2.0.2") (h "1ygddi36b7sh6lrq5sz24fwhmq2anwwvzaiinpvfr3xs95zvwh3y")))

