(define-module (crates-io ha nn hann-rs) #:use-module (crates-io))

(define-public crate-hann-rs-0.1.0 (c (n "hann-rs") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0fkdkdx3pa7l06ygh9y48mz38drix7zgjzqp6vwfpd8slabvjww9")))

