(define-module (crates-io ha nn hannibal-derive) #:use-module (crates-io))

(define-public crate-hannibal-derive-0.8.0 (c (n "hannibal-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "04wk85g8p7skfibx9nbbfwpnibxi392mhhrnh6ibrzrq3449437r")))

(define-public crate-hannibal-derive-0.9.0 (c (n "hannibal-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0dpck1qsdwy2x8psqj0m6h2inngapblj4qmp33ivxgdbmxdi38zw")))

(define-public crate-hannibal-derive-0.10.0 (c (n "hannibal-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rx5izn0hpins4wn1rg8syfif32l61sw9jd81g8vl69vixrab85p")))

