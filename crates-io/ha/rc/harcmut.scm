(define-module (crates-io ha rc harcmut) #:use-module (crates-io))

(define-public crate-HArcMut-1.0.0 (c (n "HArcMut") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0aplz8f81mvf1g7wrpgydjx5m7dgbajmsfd18dq7dzxciq5v7ssc")))

(define-public crate-HArcMut-1.0.1 (c (n "HArcMut") (v "1.0.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "181cdnryxxn53wmmi53viv073di1fy4i42zyh8rdij5i6dgd8wd1")))

(define-public crate-HArcMut-1.0.2 (c (n "HArcMut") (v "1.0.2") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1lxjwr1ixdys3r5c53axw2qsl3hi1aklwrd9y46d3xqpqhxyba2b")))

(define-public crate-HArcMut-1.1.0 (c (n "HArcMut") (v "1.1.0") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1lk4rjznka1xd140mg7vdhmlck99dgd2gx5hscf0v41rq8r503nr")))

(define-public crate-HArcMut-1.1.1 (c (n "HArcMut") (v "1.1.1") (d (list (d (n "arc-swap") (r "^1.7.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.2") (d #t) (k 0)))) (h "0n5qr2q77r91wj131m52zaw8fp4xgwnq4wwvqyyrfxnlxw6315gp")))

