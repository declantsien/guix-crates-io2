(define-module (crates-io ha lf half_matrix) #:use-module (crates-io))

(define-public crate-half_matrix-0.1.0 (c (n "half_matrix") (v "0.1.0") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)))) (h "0983rxsg31030m4py2jwza7il8vynn4zf0ihfz2k488i02fvhxd0")))

(define-public crate-half_matrix-0.1.1 (c (n "half_matrix") (v "0.1.1") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)))) (h "1km6rzxl1q3ipzzqlmmfamvq2l86phbf0gdpkfyk8g8pzngrzlsz")))

(define-public crate-half_matrix-0.2.0 (c (n "half_matrix") (v "0.2.0") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)))) (h "1c7phf9n8xyc52h0r0y2dsc0s5pz215l7wnbwc0sy55q9km55g95")))

(define-public crate-half_matrix-0.2.1 (c (n "half_matrix") (v "0.2.1") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)))) (h "18l3anmqjgl8zfa5kn2y395cx7y1wqwidr50f9za41z4gay1riwm")))

