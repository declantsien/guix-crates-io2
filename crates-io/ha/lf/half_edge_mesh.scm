(define-module (crates-io ha lf half_edge_mesh) #:use-module (crates-io))

(define-public crate-half_edge_mesh-1.0.3 (c (n "half_edge_mesh") (v "1.0.3") (d (list (d (n "cgmath") (r "^0.7.0") (d #t) (k 0)))) (h "0yq7cc9lk2am4iyyq3q6s9k9faf3jvfgppz51zgk3dj5bcn5pw6v")))

(define-public crate-half_edge_mesh-1.0.4 (c (n "half_edge_mesh") (v "1.0.4") (d (list (d (n "cgmath") (r "^0.7.0") (d #t) (k 0)))) (h "10xr9fprvfkkmrg0b3awkmqza8zhyaayzmliadpmrzab0092jy9k")))

(define-public crate-half_edge_mesh-1.0.5 (c (n "half_edge_mesh") (v "1.0.5") (d (list (d (n "cgmath") (r "^0.9.1") (d #t) (k 0)))) (h "0nz0fb07dxv1wcrfz88hry68xvclkfmh3nb47bhhk4baldjxlg10")))

(define-public crate-half_edge_mesh-1.0.6 (c (n "half_edge_mesh") (v "1.0.6") (d (list (d (n "cgmath") (r "^0.9.1") (d #t) (k 0)))) (h "1dv73ivawrks3gfs0x4slxkq3hmd4s3nciyf8xhgxqb7hvdsxwfw")))

(define-public crate-half_edge_mesh-1.0.7 (c (n "half_edge_mesh") (v "1.0.7") (d (list (d (n "cgmath") (r "^0.10.0") (d #t) (k 0)))) (h "1k9ik824wg5w3bz5n08155ki6ax8jkcadgz5l7qnln6clm4ldngx")))

(define-public crate-half_edge_mesh-1.0.8 (c (n "half_edge_mesh") (v "1.0.8") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.10.0") (d #t) (k 0)))) (h "0njy9f17lqjbjxvnn7md46dsfj7grwq16hycdj2ql4s3cwlmms39")))

(define-public crate-half_edge_mesh-1.1.8 (c (n "half_edge_mesh") (v "1.1.8") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.16.0") (d #t) (k 0)))) (h "0dn37yy5f1bh3cm2d69p0cp6nckcgqljkv98715l362lpzbw7pja")))

