(define-module (crates-io ha lf halfbit) #:use-module (crates-io))

(define-public crate-halfbit-0.0.0 (c (n "halfbit") (v "0.0.0") (d (list (d (n "num") (r ">=0.1") (d #t) (k 0)))) (h "1prrbify2278d5gm2app6dmmp7mzwv1sinzmhr1vl1jyrcjy6fdb")))

(define-public crate-halfbit-0.0.1 (c (n "halfbit") (v "0.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)))) (h "0q9q36idqws9skb4rlzfwh1l49d1qc0adkjr35crv4vd66w37wb1") (f (quote (("use-std") ("use-libc" "libc") ("default"))))))

(define-public crate-halfbit-0.0.2 (c (n "halfbit") (v "0.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)))) (h "0p8z4hm3sk90a618cgaks8idanp22d00na0r6drgsys248xsj67c") (f (quote (("use-std") ("use-libc" "libc") ("nightly") ("default"))))))

(define-public crate-halfbit-0.0.3 (c (n "halfbit") (v "0.0.3") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)))) (h "06gq3z036qvvd3hiyl2v2s2hzqajvs68rdi1qll9s3p593spz7g3") (f (quote (("use-std") ("use-libc" "libc") ("nightly") ("default"))))))

(define-public crate-halfbit-0.0.4 (c (n "halfbit") (v "0.0.4") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)))) (h "0nppn91n26zx1i8b0x2pwqlrkbrb86z2zv0il1a8x23ay819jdxl") (f (quote (("use-std") ("use-libc" "libc") ("nightly") ("default"))))))

(define-public crate-halfbit-0.0.5 (c (n "halfbit") (v "0.0.5") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)))) (h "1zbjk65xhkszhh16s492277na8syvhml3gkifivqh2h5h7b9c4kg") (f (quote (("use-std") ("use-libc" "libc") ("nightly") ("default"))))))

