(define-module (crates-io ha lf halfling) #:use-module (crates-io))

(define-public crate-halfling-0.1.0 (c (n "halfling") (v "0.1.0") (d (list (d (n "nonmax") (r "^0.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1gig0zhfjs96qrimfczimphwn98i8dbjcqiibs03m03nzqg2mkc8")))

(define-public crate-halfling-0.1.1 (c (n "halfling") (v "0.1.1") (d (list (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0s8mf3vv1kz02fxfbixgwjb9cxvj34cy82k07l7vl024fcnjwn0s")))

(define-public crate-halfling-0.1.2 (c (n "halfling") (v "0.1.2") (d (list (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0cf6vvigygz2d1nnrmvjxi4p93zx31ifzdiq7agbjjhw4vn12cnf")))

(define-public crate-halfling-0.1.3 (c (n "halfling") (v "0.1.3") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "14ddr1019s41rmbpp1z9kjlgr57mgyv7jrmqmv28ndpnv18z27p2")))

(define-public crate-halfling-0.1.4 (c (n "halfling") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "03h0w2kxjnqakmq9gfcnsx1k431fna2g81hd2rb4gghq63a565a1")))

(define-public crate-halfling-0.1.5 (c (n "halfling") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0cdlnr4807wbdxv64hzqp7bdljy7cyl5wdbfkz86jwl1x5xk0m1p")))

(define-public crate-halfling-0.2.0 (c (n "halfling") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1i2jsngch4px8yikbgxyzmygjjqs263kw6z6l1ijvn1lbn2q2as7")))

(define-public crate-halfling-0.2.1 (c (n "halfling") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "105np5dx9w1r8cc5f8384cc3bzr6ddgly5r06nrpxmgwwr4ajfwd")))

(define-public crate-halfling-0.3.0 (c (n "halfling") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1hi6ax41f7rnbkinpk5jlbs0m48q8mvv9vdrpn7mfgix8b4vbh0k")))

(define-public crate-halfling-0.4.0 (c (n "halfling") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "17fxwhgaq40055nh4d45ni9zgmjs3lsjjk7873b4bz2zqii1g59j")))

(define-public crate-halfling-0.4.1 (c (n "halfling") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "19c7vpwwlj8sf2cxf3sx57pk75w16hk3ix110mc6qyxdprs9vnm3")))

