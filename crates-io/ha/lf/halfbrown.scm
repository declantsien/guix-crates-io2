(define-module (crates-io ha lf halfbrown) #:use-module (crates-io))

(define-public crate-halfbrown-0.1.0 (c (n "halfbrown") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.2") (d #t) (k 0)))) (h "05h3i2v7zdicf2r37fy338sjzxb04dqdyzvh4z30qzxpq402ync4")))

(define-public crate-halfbrown-0.1.1 (c (n "halfbrown") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0scaj8gn4cbki9f9iz69an5zqkjrfj83273z41ciwv09vnw7w2sr")))

(define-public crate-halfbrown-0.1.2 (c (n "halfbrown") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "15w7w89m7jysbyl3x3hwv8r9lxbs4sc81rks5k36pznp9p4r8c6f")))

(define-public crate-halfbrown-0.1.3 (c (n "halfbrown") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "17q7as1r70y0vbi7by88x3ys6xnm6bpiwfzk67fn597cyzmcbjnr")))

(define-public crate-halfbrown-0.1.4 (c (n "halfbrown") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1z1akvjkysgbx3pv41jd9yansv45mf9vqilc27h5c9wc0p5jcvy8")))

(define-public crate-halfbrown-0.1.5 (c (n "halfbrown") (v "0.1.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "01rvdk62pq1vhv5lvvamjj9w1w40lwq6fh9vjy3dhz7qpml5mh6n")))

(define-public crate-halfbrown-0.1.6 (c (n "halfbrown") (v "0.1.6") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1yjhj4brc68sl3wfkwq7h233bxjarrwy6205cyfmb34bffxd55ph")))

(define-public crate-halfbrown-0.1.7 (c (n "halfbrown") (v "0.1.7") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1wq8a3c6yjgbfb3m996h4ii35fzgch7crkkzm4pcbkhr0k1fq44i")))

(define-public crate-halfbrown-0.1.8 (c (n "halfbrown") (v "0.1.8") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1a3lyczybhgqp2p2bxys7jbk9xh1w9n2prfcaidxw5p6pb3wz8vh")))

(define-public crate-halfbrown-0.1.9 (c (n "halfbrown") (v "0.1.9") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0r9lz687yghh189kl8a48s7sgpacc37cwr61lqk62wgha7fr3394")))

(define-public crate-halfbrown-0.1.10 (c (n "halfbrown") (v "0.1.10") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0yjcyl04xp3bflzx0aal55gjqm87x3i6vckdc18k4fmvlldy73pj") (f (quote (("default"))))))

(define-public crate-halfbrown-0.1.11 (c (n "halfbrown") (v "0.1.11") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "040a4qm1984qxg0862jqffin68l4kfhla5gmnad431am9d99j961") (f (quote (("default"))))))

(define-public crate-halfbrown-0.1.12 (c (n "halfbrown") (v "0.1.12") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "09f8hgz913650fbqhqxldiwf46r7fcka6xjil60rnccx4mvrblry") (f (quote (("default"))))))

(define-public crate-halfbrown-0.1.13 (c (n "halfbrown") (v "0.1.13") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1d3gs947iyhbl16aqcqwhl983s1cliznm74vji7xp7qblchndqj9") (f (quote (("default"))))))

(define-public crate-halfbrown-0.1.14 (c (n "halfbrown") (v "0.1.14") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0q2c9mqf14qff93jdcp1s42nh1269xn9m784ansm8rmh6sh6ha8h") (f (quote (("default"))))))

(define-public crate-halfbrown-0.1.15 (c (n "halfbrown") (v "0.1.15") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1gyr0vmcsfd2dyz3ggzhsjdqpa0c690k6vqyl3aa65gl5lhfssff") (f (quote (("default"))))))

(define-public crate-halfbrown-0.1.16 (c (n "halfbrown") (v "0.1.16") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "16f1v573j5r2n796pbs8n34ksn2mypj1r8nr7mw32niwh4vs92zz") (f (quote (("default"))))))

(define-public crate-halfbrown-0.1.17 (c (n "halfbrown") (v "0.1.17") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1gsr0yvcnkpk3sgvhhjzwkq23l6milx7zrxvwr5hgqnfkz89y6l3") (f (quote (("default"))))))

(define-public crate-halfbrown-0.1.18 (c (n "halfbrown") (v "0.1.18") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0ks4hpw8xfk2lk4ja6rzsqbvnwsza3wqjkmmhzpc2360m5q3qaly") (f (quote (("default"))))))

(define-public crate-halfbrown-0.2.0 (c (n "halfbrown") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1jrrrhrnq5y45c75p4f8grr5l9yrb6mdv2abanvwvj5wq9f7b79q") (f (quote (("fxhash" "rustc-hash") ("default") ("arraybackend" "arrayvec"))))))

(define-public crate-halfbrown-0.2.1 (c (n "halfbrown") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0n3ssqmb3jxpr8pb1fbfgjq7jhzwj2hagi749avx6qij4gcvg6kd") (f (quote (("fxhash" "rustc-hash") ("default") ("arraybackend" "arrayvec"))))))

(define-public crate-halfbrown-0.2.2 (c (n "halfbrown") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1gzc7aq8lnak0sxjsacak7dkry50fdl76fy18m0ihqgqj17651gr") (f (quote (("fxhash" "rustc-hash") ("default") ("arraybackend" "arrayvec"))))))

(define-public crate-halfbrown-0.2.3 (c (n "halfbrown") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1i79laf85axyw26zpy7yjfakml8gd6fb4shhrclmisihb34agjjg") (f (quote (("fxhash" "rustc-hash") ("default") ("arraybackend" "arrayvec"))))))

(define-public crate-halfbrown-0.2.4 (c (n "halfbrown") (v "0.2.4") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1v5h9hhyx29ac18rr8csvl0m7m39qy99h52zdqwl9zyxaisi70an") (f (quote (("fxhash" "rustc-hash") ("default") ("arraybackend" "arrayvec"))))))

(define-public crate-halfbrown-0.2.5 (c (n "halfbrown") (v "0.2.5") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0vzg46039pd730vc2hdhl09h86j4cd007awwlrf8l407hqd6d245") (f (quote (("fxhash" "rustc-hash") ("default") ("arraybackend" "arrayvec"))))))

