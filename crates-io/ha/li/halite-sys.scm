(define-module (crates-io ha li halite-sys) #:use-module (crates-io))

(define-public crate-halite-sys-0.1.0 (c (n "halite-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "081fys4f0wd3n7f32lya18vml9xbdk16kwzw8jch69cdnkbmb3d8")))

(define-public crate-halite-sys-0.1.1 (c (n "halite-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0g00bx9zh56rb6vg9hgdkwgpvfykk23nidlf9zhf90hs8wlqczhx")))

(define-public crate-halite-sys-0.1.2 (c (n "halite-sys") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "tempfile") (r "^3.3") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0ifnkkc35kf43lnsbalsf8n2mb5gsvhi8s4bbnhnsj0as5ibdxs9")))

(define-public crate-halite-sys-0.1.3 (c (n "halite-sys") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "tempfile") (r "^3.3") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "00c02a3vhq92wm1swlwiz944ibq8p50a8ka703sl88392ggv48yk")))

(define-public crate-halite-sys-0.1.4 (c (n "halite-sys") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "tempfile") (r "^3.5") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "07bckkgn78k1a8xw5jfcz0mxwriskrb2bqf3kfxahh6s99mfwy02")))

(define-public crate-halite-sys-0.1.5 (c (n "halite-sys") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "tempfile") (r "^3.6") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1622sn77qqsk7slk3j2fpznqx29dbaygxkp87b7bnjpzv4b8z2h7")))

(define-public crate-halite-sys-0.1.6 (c (n "halite-sys") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "tempfile") (r "^3.7") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1hqx2dmv8s9ksl6r161z7szq6vjdfmkdvqfd87hha91gfpvv5gl8")))

(define-public crate-halite-sys-0.1.7 (c (n "halite-sys") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "tempfile") (r "^3.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0c66xzig23csd4wlwnnlspsmcqi4sr35qn7wp9zbxkwp8wzfkjgc")))

(define-public crate-halite-sys-0.1.8 (c (n "halite-sys") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "tempfile") (r "^3.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.4") (d #t) (k 1)))) (h "1hwv7x5mw0ymljy58k8kx8a6d8z11vccrvc6nsi2d92bc7hplnw0")))

(define-public crate-halite-sys-0.1.9 (c (n "halite-sys") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "tempfile") (r "^3.10") (d #t) (k 1)) (d (n "walkdir") (r "^2.5") (d #t) (k 1)))) (h "1dz6zxw3bw3nq4n5xhd8jf0gxrckpm5q9lpp9mr8mliaxf7knfak")))

(define-public crate-halite-sys-0.1.10 (c (n "halite-sys") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "tempfile") (r "^3.10") (d #t) (k 1)) (d (n "walkdir") (r "^2.5") (d #t) (k 1)))) (h "0m39601979l7j7na33jy8xrx467qyjii0hwbg1halcm3qy35244x")))

(define-public crate-halite-sys-0.1.11 (c (n "halite-sys") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "tempfile") (r "^3.10") (d #t) (k 1)) (d (n "walkdir") (r "^2.5") (d #t) (k 1)))) (h "06nys23hv3j3753wjb7r0ybsfibchzb6r03vdpq49np6rajqc06s")))

