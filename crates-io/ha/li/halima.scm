(define-module (crates-io ha li halima) #:use-module (crates-io))

(define-public crate-halima-0.1.0 (c (n "halima") (v "0.1.0") (h "0gp35j4s7s92nmy3zmd647ss92xfk6ndanc29qfpjnmbzrhkrbzw")))

(define-public crate-halima-0.2.0 (c (n "halima") (v "0.2.0") (h "1m9ncd9fa7d9avp54jgmva1rmry7f7slxrmfr5y5mijn5vdn80jj")))

