(define-module (crates-io ha li halite3bdk) #:use-module (crates-io))

(define-public crate-halite3bdk-0.1.0 (c (n "halite3bdk") (v "0.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "simplelog") (r "^0.8.0") (d #t) (k 0)))) (h "1512cbffnl6qanckzvzcc1alzmahyf0jn2g555cmvr32d00j4pmb")))

