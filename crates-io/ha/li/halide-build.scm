(define-module (crates-io ha li halide-build) #:use-module (crates-io))

(define-public crate-halide-build-0.1.0 (c (n "halide-build") (v "0.1.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)))) (h "0jlm08jkjhjlsg0dj2m02b814wk1vdxwr4inx8ggk1wcrx0g52f5") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.2.0 (c (n "halide-build") (v "0.2.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)))) (h "06ps202xx75p28460d2w3rp331skwy88vwiqdilc6sb8hpijznpc") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3.0 (c (n "halide-build") (v "0.3.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)))) (h "08z3fn81ixjyw856qr5bjzgyjnw8chk6rsc5422qpjyar1ngmxy7") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3.1 (c (n "halide-build") (v "0.3.1") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)))) (h "1lkx1v68sjxnzfnpjv50410q2r22x7ysswg5qsgkq76js3hi3q8i") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3.2 (c (n "halide-build") (v "0.3.2") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)))) (h "1psf1pmh89nnqhkb0jv39rcvkcyh316iz406pli3lx4fjfdv6lvw") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3.3 (c (n "halide-build") (v "0.3.3") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)))) (h "0f9s82jjny8p4bswmgwyhhf42zilipm8dk9jlqbf9hhgd9f1w2ws") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3.4 (c (n "halide-build") (v "0.3.4") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)))) (h "0kjdafpp2ynxrsx0v1h2b39s51c401phl499a8mk8l2h5ah8sbvv") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3.5 (c (n "halide-build") (v "0.3.5") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)))) (h "0vx7f7gjq34yb28vh00wzv8ip6rj3y7rpv9j1il977lw8gczcywr") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.4.0 (c (n "halide-build") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (o #t) (d #t) (k 0)))) (h "18xj0nqkrh769kq7mkdyhgq0jsk1aj8a36x187p3vchpqaz7fswn") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.4.1 (c (n "halide-build") (v "0.4.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (o #t) (d #t) (k 0)))) (h "1fck2svxd9l6n9jr6ygxfn4ml1s3hmcv1prmnh4ag0r4ijz5jyqq") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.5.0 (c (n "halide-build") (v "0.5.0") (d (list (d (n "clap") (r "^3") (f (quote ("env"))) (o #t) (d #t) (k 0)))) (h "1xcw2sl3k48ynaglsldajkh17b2zv9mqf395xclc430bk20kaa5a") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.5.1 (c (n "halide-build") (v "0.5.1") (d (list (d (n "clap") (r "^3") (f (quote ("env"))) (o #t) (d #t) (k 0)))) (h "1xpylrajg225hq53q4xkf35wwsqwl8znz0w9s8k3xzh0isynjzrm") (f (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.6.0 (c (n "halide-build") (v "0.6.0") (d (list (d (n "clap") (r "^3") (f (quote ("env"))) (o #t) (d #t) (k 0)))) (h "0mjw4wlc3vzay2xzmld6lkcd8vdmfygzjrh81bh51gx2xn2vgfqp") (f (quote (("default") ("bin" "clap"))))))

