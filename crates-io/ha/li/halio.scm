(define-module (crates-io ha li halio) #:use-module (crates-io))

(define-public crate-halio-0.1.0-alpha-1 (c (n "halio") (v "0.1.0-alpha-1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nb") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "taskio") (r "^0.1.0-alpha-1") (f (quote ("full"))) (d #t) (k 0)))) (h "0grmalgfsjw61ky2d1s7hpwi5hrad6899bmbhijh7pq80c0b4rwx") (f (quote (("full" "embedded-hal") ("embedded-hal"))))))

