(define-module (crates-io ha li halide-runtime) #:use-module (crates-io))

(define-public crate-halide-runtime-0.1.0 (c (n "halide-runtime") (v "0.1.0") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)))) (h "1kr9kxqjzl8iza6ic9nhgx3m4n1md7wn7c85d2g92xc5yzppvgx5")))

(define-public crate-halide-runtime-0.2.0 (c (n "halide-runtime") (v "0.2.0") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)))) (h "0swpdp8wqrvhydszsf20p1q116li7hzbk70zvl732k4vjrf3zg6y")))

(define-public crate-halide-runtime-0.2.1 (c (n "halide-runtime") (v "0.2.1") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)))) (h "1r3lp2r9pa0kjyrkyc5gfwr5i9wkg517pmwc9x4827jf15aylccy")))

(define-public crate-halide-runtime-0.3.0 (c (n "halide-runtime") (v "0.3.0") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)))) (h "1hcxf7xfb06pwhga571gx7b2v4sx6z1aadr62yh8bl86l7n57098")))

(define-public crate-halide-runtime-0.4.0 (c (n "halide-runtime") (v "0.4.0") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)))) (h "1g8casy5cwm7cs2xc0w7jgk0xbz0ycnimn33kn1pzgx2sjdaz6n9") (f (quote (("gpu"))))))

(define-public crate-halide-runtime-0.5.0 (c (n "halide-runtime") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "halide-build") (r "^0.4") (d #t) (k 1)))) (h "0xi25bsn8rsklwzfzzfiqhj28179y5s1rfsd77xv33b4rimxaavj") (f (quote (("gpu") ("default" "gpu"))))))

(define-public crate-halide-runtime-0.5.1 (c (n "halide-runtime") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "halide-build") (r "^0.4") (d #t) (k 1)))) (h "0zzdl3bk5951zan4py2rbl2jf04bvcn59zl7gjc64n2iqk138v5g") (f (quote (("gpu") ("default" "gpu"))))))

(define-public crate-halide-runtime-0.6.0 (c (n "halide-runtime") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "halide-build") (r "^0.5") (d #t) (k 1)))) (h "1dkmx74wk986dfvkl1f04706v1gi5jhcdicvxshdh2aqhp3jm3ah") (f (quote (("gpu") ("default" "gpu"))))))

(define-public crate-halide-runtime-0.6.1 (c (n "halide-runtime") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "halide-build") (r "^0.6") (d #t) (k 1)))) (h "1j62d3zl2drzjp855rdbafsxynpjmbr3bg64vzdwffsf63bwlsrr") (f (quote (("gpu") ("default" "gpu"))))))

