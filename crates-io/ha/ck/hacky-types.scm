(define-module (crates-io ha ck hacky-types) #:use-module (crates-io))

(define-public crate-hacky-types-0.1.0 (c (n "hacky-types") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "00l8amhnyz7c2r5477wqznzfv4xlmpqbkbidpgq2yjfv46g6j6w7")))

