(define-module (crates-io ha ck hackmd-rs) #:use-module (crates-io))

(define-public crate-hackmd-rs-0.1.0 (c (n "hackmd-rs") (v "0.1.0") (h "0idz52ij2j6sbslrg9qxz531yv5p5x5cykq6s8xidym0m7nx76km")))

(define-public crate-hackmd-rs-0.2.0 (c (n "hackmd-rs") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1jl89prl7gad38z2yi58m60l9v5wy04d7dxh1lfymhzinr0kpiw9")))

