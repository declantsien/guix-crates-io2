(define-module (crates-io ha ck hacksaw) #:use-module (crates-io))

(define-public crate-hacksaw-1.0.0 (c (n "hacksaw") (v "1.0.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (f (quote ("xkb" "shape"))) (d #t) (k 0)))) (h "0xpfbs55hbgv2v6z2grfsznd5cdx5gmg18vb1s4hv143wg1njmpf")))

(define-public crate-hacksaw-1.0.1 (c (n "hacksaw") (v "1.0.1") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (f (quote ("xkb" "shape"))) (d #t) (k 0)))) (h "1ayqljyxk5zdjy0ydl4gfsy72hjm3h3d59g76pnczbhm0fv54lgy")))

(define-public crate-hacksaw-1.0.2 (c (n "hacksaw") (v "1.0.2") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (f (quote ("xkb" "shape"))) (d #t) (k 0)))) (h "0akq565x2rdvaip3vljbszf7wykx3i6gfawc4164wqpxmdgzmp06")))

(define-public crate-hacksaw-1.0.3 (c (n "hacksaw") (v "1.0.3") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (f (quote ("xkb" "shape"))) (d #t) (k 0)))) (h "1z6f1nkwikz189njlkyhvl9qj84ri2ysyb0i8v4xncq6j7wc3hbb")))

(define-public crate-hacksaw-1.0.4 (c (n "hacksaw") (v "1.0.4") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("xkb" "shape"))) (d #t) (k 0)))) (h "0xvcfx0mrbab3hs0lmcrh5grxdsr3y0awv2inwqgv3c35dj5278p")))

