(define-module (crates-io ha ck hackfn) #:use-module (crates-io))

(define-public crate-hackfn-0.1.0 (c (n "hackfn") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0zyqp0qd5s45sl3m5lm5ng2cisa8n21gvzf47l9dbshksi3rfrrw")))

(define-public crate-hackfn-0.1.1 (c (n "hackfn") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1qc31mbily70vvr457vi073xw466q619zfzz5sva311wfmdh7jbz")))

(define-public crate-hackfn-0.1.2 (c (n "hackfn") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01ljdjn4z3z0yinqi72lp7nkiq6f3x00ddpirrdnx9kgwhxwji5h")))

(define-public crate-hackfn-0.1.3 (c (n "hackfn") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zhnw7vqp8zzvffvl6yg71mnm0w6jgbr5d6rgpcm352ppb5ll2l3")))

(define-public crate-hackfn-0.1.4 (c (n "hackfn") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kn0h6lvyhnd8p8i09s69xfq9acslickdh1al6b9b0xhpybg6j40")))

(define-public crate-hackfn-0.1.5 (c (n "hackfn") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08n78apznj2mypylg7p688pxgaxkajdcgmbqkjyhdpx5by79jk11") (r "1.36")))

(define-public crate-hackfn-0.1.6 (c (n "hackfn") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ifa45byz5a9bzssblj73wp20l9ljwbl194wf2mklzr7lzkz03sp") (r "1.36")))

(define-public crate-hackfn-0.1.7 (c (n "hackfn") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zdp058ascpqkz3p602k6cy66ji33nx6spvds4rkiyynhrpv3kn7") (r "1.36")))

(define-public crate-hackfn-0.1.8 (c (n "hackfn") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jd75s85d7ghx4d2p440ymm5cb1sg4fq45apfnpr1jjszs9fm6xg") (r "1.36")))

