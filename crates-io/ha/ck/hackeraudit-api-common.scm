(define-module (crates-io ha ck hackeraudit-api-common) #:use-module (crates-io))

(define-public crate-hackeraudit-api-common-0.1.0 (c (n "hackeraudit-api-common") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0yzc5i78arxy99wjfjljdj3k85cvhh8j7agzma74f5dcqwxkyc61")))

