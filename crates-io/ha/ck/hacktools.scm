(define-module (crates-io ha ck hacktools) #:use-module (crates-io))

(define-public crate-hacktools-0.1.0 (c (n "hacktools") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustscan") (r "^2.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "145rkzbipaaxfzmzlyw2hqxvv25b28aj5iszzn6mh2wadk63iiqw")))

(define-public crate-hacktools-0.1.1 (c (n "hacktools") (v "0.1.1") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustscan") (r "^2.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "0h4hcj9fq10dx4gafcgydcyrc05ypn6yi606pp0mv6ggc4chprrv")))

(define-public crate-hacktools-0.1.2 (c (n "hacktools") (v "0.1.2") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustscan") (r "^2.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "0m4l71h3ygi44wjzqih7ll5acvpfd841159ndx9vgm0zk8qxw2nz")))

(define-public crate-hacktools-0.1.3 (c (n "hacktools") (v "0.1.3") (d (list (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustscan") (r "^2.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "0fb5bvd4v4r4ww8vyhm5aya31zg10gh60dc8ncdgy7dndif1h19r")))

(define-public crate-hacktools-1.0.0 (c (n "hacktools") (v "1.0.0") (d (list (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustscan") (r "^2.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "1yigyvqj2xlz31s2c480f6cg6cpagbi6s319wxmcnzzha1xr5ad4")))

(define-public crate-hacktools-1.1.0 (c (n "hacktools") (v "1.1.0") (d (list (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustscan") (r "^2.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "1cc9dxq1z1pvplj0ffdzibidf4dcgr71559pxqrfl8lfxbr2j4s4")))

