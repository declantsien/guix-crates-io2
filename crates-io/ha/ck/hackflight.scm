(define-module (crates-io ha ck hackflight) #:use-module (crates-io))

(define-public crate-hackflight-0.1.0 (c (n "hackflight") (v "0.1.0") (h "05v8lhd98sr6iw7hvihrdcf2d26cp761cjyxh6wvkjb530nhmijf")))

(define-public crate-hackflight-0.1.1 (c (n "hackflight") (v "0.1.1") (h "1lfgxj34wyg3v3nk98g7m895zrrln631k34gzc5kp7n03wla04k7")))

(define-public crate-hackflight-0.1.2 (c (n "hackflight") (v "0.1.2") (h "0y5j3vycj3civz9cg1zwc82c7ydkbad9xvijf7c3zqsgn3413x42")))

(define-public crate-hackflight-0.1.3 (c (n "hackflight") (v "0.1.3") (h "0q06f69g47chs6daicjkss9frx5890plxl24gi1ayvlcsl9487zb")))

(define-public crate-hackflight-0.1.4 (c (n "hackflight") (v "0.1.4") (h "05s84zsdmz1f0vgwwbi6rm9lrz5wkydn8x8a9dzw08xk1chm7234")))

(define-public crate-hackflight-0.1.5 (c (n "hackflight") (v "0.1.5") (h "1dh8yga92j7rl3668s403n8f2bgfsik8092h4mlxyzv6b96qsfsp")))

(define-public crate-hackflight-0.1.6 (c (n "hackflight") (v "0.1.6") (h "0n3bryj6ml0qhhf5kbrj045q7hfvnwr56gh67li9iqmm6ki906nl")))

(define-public crate-hackflight-0.1.7 (c (n "hackflight") (v "0.1.7") (h "0m2vg09zc8sg1pyrimdxsgkdhjz4gh475c7wiyqgw02n57xx0451")))

