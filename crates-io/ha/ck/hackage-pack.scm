(define-module (crates-io ha ck hackage-pack) #:use-module (crates-io))

(define-public crate-hackage-pack-0.1.0 (c (n "hackage-pack") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1hnb37s705d2wkyhymqdqf1wnw5li7jwwmkl216hicbyfjq4w3fp") (y #t)))

(define-public crate-hackage-pack-0.1.1 (c (n "hackage-pack") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0bzjgw2z8qlk3832y77z57j1irfxcq0gcsa28m9zm3yl923xf5hh") (y #t)))

(define-public crate-hackage-pack-0.1.2 (c (n "hackage-pack") (v "0.1.2") (h "1x86j9k3mrxi9iy3y06h5zg7vjj9ybqf9g7scibq9xa4yw52lgaq") (y #t)))

(define-public crate-hackage-pack-0.1.3 (c (n "hackage-pack") (v "0.1.3") (h "17grls491v7k56b9imsqp5p7ppmp5xvawpajzbsgal6xmaxia6cn") (y #t)))

(define-public crate-hackage-pack-0.1.4 (c (n "hackage-pack") (v "0.1.4") (h "14qcsczcnymy5bcyrginxfxxl5sqakvliak2wwvbsciq6wpkssac") (y #t) (r "1.62.1")))

(define-public crate-hackage-pack-0.1.5 (c (n "hackage-pack") (v "0.1.5") (h "0rnnvzxv3b0fd52j476chl395ladh31a7hy21ms134dbxky8y85w") (r "1.62.1")))

