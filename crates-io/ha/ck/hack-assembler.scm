(define-module (crates-io ha ck hack-assembler) #:use-module (crates-io))

(define-public crate-hack-assembler-0.1.0 (c (n "hack-assembler") (v "0.1.0") (d (list (d (n "im") (r "^14.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0r5pihfd4smzxw9sv7qbzbkmyq5b53589c9xgkss8hd74s75ga33")))

