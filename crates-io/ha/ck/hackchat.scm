(define-module (crates-io ha ck hackchat) #:use-module (crates-io))

(define-public crate-hackchat-0.1.1 (c (n "hackchat") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12.2") (d #t) (k 0)))) (h "01vhw6krg0aay55gmc7k6pxj4ljhwci78a6zhfdm8pb74gj56sm9")))

(define-public crate-hackchat-0.1.2 (c (n "hackchat") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12.2") (d #t) (k 0)))) (h "05wn8qcy5xmkgwa1y1i821c3f1hpnbrqns4bjs73fg9455q39zr0")))

(define-public crate-hackchat-0.1.3 (c (n "hackchat") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "websocket") (r "^0.17") (d #t) (k 0)))) (h "0j1bp20m6b699gph2xwr8n989z0hcksla7phlqz725j9klyiznx6")))

(define-public crate-hackchat-0.1.4 (c (n "hackchat") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "websocket") (r "^0.17") (d #t) (k 0)))) (h "1jhabm5l655n01v1mn0z84ij6dzaln0ix0hfp85k7pw6pk0y6kxa")))

