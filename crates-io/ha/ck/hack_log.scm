(define-module (crates-io ha ck hack_log) #:use-module (crates-io))

(define-public crate-hack_log-0.1.0 (c (n "hack_log") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "081scdxp3h3vc368qr0wfaldnspisiyd2n5a5ilg29nn8ayqw2fm") (f (quote (("default") ("color" "ansi_term"))))))

