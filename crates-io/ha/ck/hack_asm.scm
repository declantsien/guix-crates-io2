(define-module (crates-io ha ck hack_asm) #:use-module (crates-io))

(define-public crate-hack_asm-0.1.0-dev (c (n "hack_asm") (v "0.1.0-dev") (d (list (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ddrwcb45h6fdgw5rly2qqjfwsvhhs1rrbmaj5j4nymsgcfnsg3f")))

(define-public crate-hack_asm-0.1.1-dev (c (n "hack_asm") (v "0.1.1-dev") (d (list (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1y7ycykmkgdv5js7cgxqmhja7fgcb2ymrj2j0yb9dr43m5yb8xmy")))

(define-public crate-hack_asm-0.1.2-dev (c (n "hack_asm") (v "0.1.2-dev") (d (list (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "17iqbnjlblb45rndr27vmrl5lgi2cq61icgq9xq4y6qi2zqibhkp")))

(define-public crate-hack_asm-0.1.3-dev (c (n "hack_asm") (v "0.1.3-dev") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "125knnpcpxwsyrp7j183w7mf29drzbxmlfddyib62ycv4q2khss4") (y #t)))

(define-public crate-hack_asm-0.2.0-dev (c (n "hack_asm") (v "0.2.0-dev") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0v1dlw9nb09gh8q6viz17gr2748d4r2y9vi4qd32c7x9wsb8np27")))

(define-public crate-hack_asm-0.3.0-dev (c (n "hack_asm") (v "0.3.0-dev") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "07lawx86izmlg3dmmzs8biw3cdllvwsb7r8qwjl0wbfj8ncbncbn")))

(define-public crate-hack_asm-1.0.0 (c (n "hack_asm") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1y0hsy2jrascdwszr5kiwar6yhsv8szv1bkkpmj3x5sf95fscvy4")))

(define-public crate-hack_asm-1.0.1 (c (n "hack_asm") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ll7b9q89cimfxsbxg2wib808s7aq1g5nk7aj791k2x5kl782diw")))

(define-public crate-hack_asm-1.0.2 (c (n "hack_asm") (v "1.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02h56a41k3gzk3ar4q19d2dr0dndsqpql4l1vvl8yrqlnmvmlnma")))

