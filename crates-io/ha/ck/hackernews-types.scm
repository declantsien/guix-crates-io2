(define-module (crates-io ha ck hackernews-types) #:use-module (crates-io))

(define-public crate-hackernews-types-0.1.0 (c (n "hackernews-types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j0lygj18ggfqhblqz9lb605375552kz8831p9q22p65a9k3yvqn")))

(define-public crate-hackernews-types-0.1.1 (c (n "hackernews-types") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0d5cg05xrxs1fjlq03vprn1v6h1br74d077dw2yixg3yb7xf0ysd")))

(define-public crate-hackernews-types-0.1.2 (c (n "hackernews-types") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0vvznnjmi9d4zamc245dcrdq0138mfpg70xpx0x42yn722hdammh")))

