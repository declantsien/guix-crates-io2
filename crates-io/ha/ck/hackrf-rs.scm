(define-module (crates-io ha ck hackrf-rs) #:use-module (crates-io))

(define-public crate-hackrf-rs-0.3.0 (c (n "hackrf-rs") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "eusb") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0lxf7dszggmbnyf87g8qlbiddfq1qmv9l0wwj8lv709m692ky578")))

(define-public crate-hackrf-rs-0.3.1 (c (n "hackrf-rs") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "eusb") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1a45r0vhxi8fj0axbh3rsq08gsr5dg7gq23xg167fll460vcmswv")))

(define-public crate-hackrf-rs-0.4.0 (c (n "hackrf-rs") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "eusb") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "106cz1bj5xy5g2zmkg6vqkmfiyryfvci395jwybg2dfw5ixmzisi")))

(define-public crate-hackrf-rs-0.5.0 (c (n "hackrf-rs") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "eusb") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1dwwniin4pyk2rxdsr767k2gvmhh9ndyiyi3wwc8fp2fadd0jyzr")))

(define-public crate-hackrf-rs-0.6.0 (c (n "hackrf-rs") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "eusb") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0x0qzgmli5mysws21sgzy9avvckx1x3br6wjv9qxj16q6fs2v3w8")))

(define-public crate-hackrf-rs-0.7.0 (c (n "hackrf-rs") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "eusb") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1qy7xnc05hcdsrg140xlyb2cfy3kl00v1851chkmgwq1k16q3c68")))

