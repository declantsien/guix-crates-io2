(define-module (crates-io ha ck hackrfone) #:use-module (crates-io))

(define-public crate-hackrfone-0.1.0 (c (n "hackrfone") (v "0.1.0") (d (list (d (n "num-complex") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "rusb") (r "~0.8") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "1604nmjhb4w027qjjg5cqbj04vzbh2gq41dsqryxs3vj6hripwz0")))

(define-public crate-hackrfone-0.2.0 (c (n "hackrfone") (v "0.2.0") (d (list (d (n "num-complex") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "rusb") (r "~0.8") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "05bd4pdkmhc0v26fnq48gpfyvgk1h7azrajvrx3ahpd26rpdbbzj")))

(define-public crate-hackrfone-0.2.1 (c (n "hackrfone") (v "0.2.1") (d (list (d (n "num-complex") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "rusb") (r "~0.8") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0hgspwf4qqk6xkrd3jf9n0w76qdwr0fz2p7z9x9c4qwyrsaqww6j")))

(define-public crate-hackrfone-0.2.2 (c (n "hackrfone") (v "0.2.2") (d (list (d (n "num-complex") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "rusb") (r "~0.8") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "1v1r54xkvcxj2wxbyip2lg8rcm4jlb9cj3p6bmxfl6s6idc8g985")))

(define-public crate-hackrfone-0.2.3 (c (n "hackrfone") (v "0.2.3") (d (list (d (n "num-complex") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "rusb") (r "~0.8") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "03di8v1ijkq34pvdzqvk9khm4f1qr4ibdfxc4vzj6nyq2dxvn6z8")))

