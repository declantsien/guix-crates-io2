(define-module (crates-io ha ck hacker_text) #:use-module (crates-io))

(define-public crate-hacker_text-1.0.0 (c (n "hacker_text") (v "1.0.0") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)))) (h "1i6l4xpxwf5vp9i8r1ixk3fah3qg0gv5sq18n61skpgr7sbvmf07")))

(define-public crate-hacker_text-1.1.0 (c (n "hacker_text") (v "1.1.0") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)))) (h "1h3g30i8j0f0iwik69z3bxd2f4dqai4vgh2brcd2bdmnfmbi9d42")))

