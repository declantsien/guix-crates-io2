(define-module (crates-io ha ck hackathon_question1) #:use-module (crates-io))

(define-public crate-hackathon_question1-0.1.0 (c (n "hackathon_question1") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (d #t) (k 0)))) (h "110d4q8cfqb1wka7zvcmrc5ih8g2hafc52sf9mh7fjxhaz55x27z")))

