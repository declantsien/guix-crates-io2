(define-module (crates-io ha ck hackaton) #:use-module (crates-io))

(define-public crate-hackaton-0.1.0 (c (n "hackaton") (v "0.1.0") (d (list (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)))) (h "1icx65lxfkx54530k3dyd3xwabxzcx01lvham4sn490y46q2dci9") (y #t)))

(define-public crate-hackaton-0.1.1 (c (n "hackaton") (v "0.1.1") (d (list (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)))) (h "076shrp29kffk85dl3w2zqk0pakxbj6wh4lh4pdzjlmc898x3jxl") (y #t)))

