(define-module (crates-io ha ck hacksaw-x11rb) #:use-module (crates-io))

(define-public crate-hacksaw-x11rb-1.0.5 (c (n "hacksaw-x11rb") (v "1.0.5") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "x11rb") (r "^0.5") (f (quote ("xkb" "shape"))) (k 0)))) (h "1axaj1j2j6cxqswld5kzzn0xzmplvyapqb2v7yc5zvj8ycz57yzx")))

