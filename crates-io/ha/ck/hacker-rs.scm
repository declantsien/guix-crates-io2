(define-module (crates-io ha ck hacker-rs) #:use-module (crates-io))

(define-public crate-hacker-rs-0.1.0 (c (n "hacker-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde-well-known"))) (d #t) (k 0)))) (h "1q4r8bxndhycm6s0mqi22lnj8s1jz7dlhggz26fl3sma8ll0prx5")))

(define-public crate-hacker-rs-0.1.1 (c (n "hacker-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde-well-known"))) (d #t) (k 0)))) (h "16y845prkl3ss39k4akrcfhz6cm7kmisxkv7f51jszbgax1mxw0r")))

(define-public crate-hacker-rs-0.1.3 (c (n "hacker-rs") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde-well-known"))) (d #t) (k 0)))) (h "1vk2mldrqr5rbnmq55h2c48ll8r9p7zgs0hxscz4axhlx59ypz0r")))

(define-public crate-hacker-rs-0.1.5 (c (n "hacker-rs") (v "0.1.5") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde-well-known"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "12xbyay3pvr2qgs3xhkp0zqj8vmbrv75d6gqpy52bz8syjbfr0rm")))

