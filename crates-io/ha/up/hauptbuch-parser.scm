(define-module (crates-io ha up hauptbuch-parser) #:use-module (crates-io))

(define-public crate-hauptbuch-parser-0.0.1 (c (n "hauptbuch-parser") (v "0.0.1") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "07h7fv8pwnbsdcs0chhvsxd7gnfdqgnmssn0qhcpkkrh4vfzdnzi")))

(define-public crate-hauptbuch-parser-0.0.2 (c (n "hauptbuch-parser") (v "0.0.2") (d (list (d (n "combine") (r "^3.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "0c80gr2fpr1bh8sm9z3ima4a73hkfc8s808r11bn73jbsxs18raq")))

(define-public crate-hauptbuch-parser-0.0.3 (c (n "hauptbuch-parser") (v "0.0.3") (d (list (d (n "combine") (r "^3.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "0zmiaf71b8xmfi41kg2l6y6shl2wx47pr7a2vswjnynw3lgy9z36")))

(define-public crate-hauptbuch-parser-0.0.4 (c (n "hauptbuch-parser") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "combine") (r "^3.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10") (d #t) (k 0)))) (h "0lylmmvhk9vady0a3yz8qbywzssw2gil56iwfhisx3kwjjnb4b7m")))

(define-public crate-hauptbuch-parser-0.1.0 (c (n "hauptbuch-parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "combine") (r "^3.8") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^4.2") (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (d #t) (k 0)))) (h "1mn7q0z30xy70gz8l10f6cwqjv70wmij2a9h9na9gf2xbzfns67x") (f (quote (("default" "nom" "combine"))))))

(define-public crate-hauptbuch-parser-0.1.1 (c (n "hauptbuch-parser") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "combine") (r "^3.8") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^4.2") (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (d #t) (k 0)))) (h "0yg9hpknri5hvkhin34dfi8w6x8n0i4lk22rnc38b6vmlfb2vpq1") (f (quote (("default" "nom" "combine"))))))

(define-public crate-hauptbuch-parser-0.2.0 (c (n "hauptbuch-parser") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "combine") (r "^3.8") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (d #t) (k 0)))) (h "1fq28mpg558924i9zjwb4g4ami2gks5s5dvkr95ygs6b19mlxrb4") (f (quote (("default" "nom" "combine"))))))

(define-public crate-hauptbuch-parser-0.3.0 (c (n "hauptbuch-parser") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1gkkybbplhabxmx4zv6k8kwlfzipcy6y1vfkbsfb4mgk8rvg56fz")))

(define-public crate-hauptbuch-parser-0.4.0 (c (n "hauptbuch-parser") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0r7746hyblvf6vj0rbw5pgcsq28w8c7ygjkkfn2gm4rw5q1ygr3r")))

