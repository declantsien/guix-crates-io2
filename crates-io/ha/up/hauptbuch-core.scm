(define-module (crates-io ha up hauptbuch-core) #:use-module (crates-io))

(define-public crate-hauptbuch-core-0.0.1 (c (n "hauptbuch-core") (v "0.0.1") (d (list (d (n "hauptbuch-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10") (d #t) (k 0)))) (h "1i21dr17p07vkxjci9xvai5cc4vn6xm4byq2mx72xdbf9d5q64d6")))

(define-public crate-hauptbuch-core-0.0.2 (c (n "hauptbuch-core") (v "0.0.2") (d (list (d (n "hauptbuch-parser") (r "^0.0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10") (d #t) (k 0)))) (h "0fw81y113y9sqykf7l7g37qzb8pz9c13fgcmzjhlm2cs0fr3vr7x")))

