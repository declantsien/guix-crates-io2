(define-module (crates-io ha up hauptbuch) #:use-module (crates-io))

(define-public crate-hauptbuch-0.0.1 (c (n "hauptbuch") (v "0.0.1") (d (list (d (n "hauptbuch-core") (r "^0.0.1") (d #t) (k 0)) (d (n "hauptbuch-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tabular") (r "^0.1") (d #t) (k 0)))) (h "1vg5gbnz20phmbjhn052pyp6r7mkpq9121z7ndc453dmij28q06g")))

