(define-module (crates-io ha no hanower) #:use-module (crates-io))

(define-public crate-hanower-0.2.1 (c (n "hanower") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0w7j7pfswv40jvc5wckkrh1ps896d40ficz8795mmqg4ci5lr9ig")))

(define-public crate-hanower-0.2.2 (c (n "hanower") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "05xgba71mjw92b070ys5528cic5fiycnm0lrc6i5sdfy0gqqzz7g")))

(define-public crate-hanower-0.2.4 (c (n "hanower") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1vb68r51k0drayyhy4rc924sn9qy9k7lsp32q2hr5p55758yz3bj")))

