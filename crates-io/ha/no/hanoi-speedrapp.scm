(define-module (crates-io ha no hanoi-speedrapp) #:use-module (crates-io))

(define-public crate-hanoi-speedrapp-0.1.0 (c (n "hanoi-speedrapp") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eframe") (r "^0.27.2") (f (quote ("persistence"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "pretty-duration") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "04nnlinqdk2sblx3z2s28rn33qi62ik2j0ha8cqkn2dwmpphdpir")))

