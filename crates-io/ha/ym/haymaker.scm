(define-module (crates-io ha ym haymaker) #:use-module (crates-io))

(define-public crate-haymaker-0.0.1 (c (n "haymaker") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.6") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.6") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1jqvhqsihi77ii7v2x5pxbapdapsr49bmzzda63a2jmi5zlxhmsf")))

