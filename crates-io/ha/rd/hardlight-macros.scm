(define-module (crates-io ha rd hardlight-macros) #:use-module (crates-io))

(define-public crate-hardlight-macros-0.1.1 (c (n "hardlight-macros") (v "0.1.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "014vm5m9s56fysd34w1xhk7m6sj18hqg09arf5x1qx58aqa7s70x")))

(define-public crate-hardlight-macros-0.1.2 (c (n "hardlight-macros") (v "0.1.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cjrh37bndqlsf3hi4m4rnj2kg682lakf8qp5zid9znaf5jlga9x")))

(define-public crate-hardlight-macros-1.0.0 (c (n "hardlight-macros") (v "1.0.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hw9libivwdwr0kk3s5h0djd7fdg2vc9g3ny3ckpybv26sj3qzma") (f (quote (("disable-self-signed"))))))

(define-public crate-hardlight-macros-1.2.3 (c (n "hardlight-macros") (v "1.2.3") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12dd9zlhwhpfpbgfj17vx5m0q3wvv3bbwc8ma1ih6p5qm70lclzc") (f (quote (("disable-self-signed"))))))

(define-public crate-hardlight-macros-1.2.4 (c (n "hardlight-macros") (v "1.2.4") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ib822mh3sfhw4s2fj52w8k3rbyb6lq9a69l7iggrlys5wkrf9dd") (f (quote (("disable-self-signed"))))))

(define-public crate-hardlight-macros-1.3.0 (c (n "hardlight-macros") (v "1.3.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02gr62yjg5yqmqpi89vk6anrm2kni7vqpcp026q1qdg2am96hh33") (f (quote (("disable-self-signed"))))))

(define-public crate-hardlight-macros-1.4.0 (c (n "hardlight-macros") (v "1.4.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13m4sfwb0xzbp0lphck8j4mj4bf1scqk1hh7zf808p96ib08nxl6") (f (quote (("disable-self-signed"))))))

(define-public crate-hardlight-macros-1.4.1 (c (n "hardlight-macros") (v "1.4.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00900vq4v1533h11n5gg8a8wdrz9kf4988g5xa557flmqn933llp") (f (quote (("disable-self-signed"))))))

(define-public crate-hardlight-macros-2.0.0-beta0 (c (n "hardlight-macros") (v "2.0.0-beta0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fjhifb063pcr46cqmfrzl92bdlnn0yxss2scfqcs319vksmbry7") (f (quote (("disable-self-signed"))))))

(define-public crate-hardlight-macros-2.0.0 (c (n "hardlight-macros") (v "2.0.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zjfzk0dmdg2myxkk97imj9mdl29lw3v3il2vv42wiwry95a0n7m") (f (quote (("disable-self-signed"))))))

