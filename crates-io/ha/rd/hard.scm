(define-module (crates-io ha rd hard) #:use-module (crates-io))

(define-public crate-hard-0.1.0 (c (n "hard") (v "0.1.0") (d (list (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2.7") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1v97w7bm757nwbkhhacy8r8f5rp8cql0qyvxd37rw0cljc1bwwn2")))

(define-public crate-hard-0.1.1 (c (n "hard") (v "0.1.1") (d (list (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2.7") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0i9fmrmj0zinwrgff85lgr2hbqw1kp3q8gwh8la3s08v4di0j7rv")))

(define-public crate-hard-0.2.0 (c (n "hard") (v "0.2.0") (d (list (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2.7") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0qmp7b0f0bmnrd1d58i2xghnbrlvfi5n75za9q2s58ba0mx4i9yg")))

(define-public crate-hard-0.2.1 (c (n "hard") (v "0.2.1") (d (list (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2.7") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1wmg9x79byia6an3rdq15saph4ikjb4m11i37x4ak51gx47g7fnb")))

(define-public crate-hard-0.2.2 (c (n "hard") (v "0.2.2") (d (list (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libsodium-sys-stable") (r "^1.19.19") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1bclwvyp6b3qg7slc37wrk6xmsh5bwjnx64582y8kbbpw8lk8aws")))

(define-public crate-hard-0.3.0 (c (n "hard") (v "0.3.0") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libsodium-sys-stable") (r "^1.19.19") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1bxly9wkkasq034qsf3kqj65qgq30h748826jf6vlhiqs0cwixck")))

(define-public crate-hard-0.3.1 (c (n "hard") (v "0.3.1") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libsodium-sys-stable") (r "^1.19.19") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gg6ddyb87wsfa4yqc9xjfc1cyn9ij5f0bl4nsshcxgpxsymaw5p")))

(define-public crate-hard-0.4.0 (c (n "hard") (v "0.4.0") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libsodium-sys-stable") (r "^1.19.19") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ylldc07sxnb2kbzqmg8m0n32hwsp8n84y9fqjmfdxl583h3x9bh") (f (quote (("restricted-types"))))))

(define-public crate-hard-0.5.0 (c (n "hard") (v "0.5.0") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libsodium-sys-stable") (r "^1.19.19") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00mq2bqlk3baz54s3j8y72ycgwjvcg6ng9iw4phcsjy5qsqcjix9") (f (quote (("restricted-types"))))))

