(define-module (crates-io ha rd hardened_malloc-rs) #:use-module (crates-io))

(define-public crate-hardened_malloc-rs-0.1.0+12 (c (n "hardened_malloc-rs") (v "0.1.0+12") (h "0gjrd0svqp363bd9ka0yfn01c56pd32z756q5fj35xk72nsj8scc") (f (quote (("static") ("standard") ("light") ("gcc") ("dynamic") ("default" "static" "gcc" "light") ("clang")))) (r "1.63.0")))

(define-public crate-hardened_malloc-rs-0.1.1+12 (c (n "hardened_malloc-rs") (v "0.1.1+12") (h "0c6l6mgjl9v8mvinknxfy8hmnjs83wy42fwbckndxhj0wgy5sl20") (f (quote (("static") ("standard") ("light") ("gcc") ("dynamic") ("default" "static" "gcc" "light") ("clang")))) (r "1.63.0")))

(define-public crate-hardened_malloc-rs-0.1.2+12 (c (n "hardened_malloc-rs") (v "0.1.2+12") (h "1djcg67khxv0n11rhb45wdkd7vdn41pn5zzkhl7ickdihcaynzb4") (f (quote (("static") ("standard") ("light") ("gcc") ("dynamic") ("default" "static" "gcc" "light") ("clang")))) (r "1.63.0")))

