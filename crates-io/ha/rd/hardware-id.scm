(define-module (crates-io ha rd hardware-id) #:use-module (crates-io))

(define-public crate-hardware-id-0.3.0 (c (n "hardware-id") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "winreg") (r "^0.10.1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1ryb0y7xs71yxiyp0h4li9cmmi7cdfb1p5jkwf3gh4ffh195n5nk")))

