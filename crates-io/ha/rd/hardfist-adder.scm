(define-module (crates-io ha rd hardfist-adder) #:use-module (crates-io))

(define-public crate-hardfist-adder-0.1.0 (c (n "hardfist-adder") (v "0.1.0") (d (list (d (n "hardfist-add-one") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "07crf7f6xj4p1qd57m1mpgzgni4nkiy1pipdv6ir5n3g4bq2nykc")))

