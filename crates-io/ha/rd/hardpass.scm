(define-module (crates-io ha rd hardpass) #:use-module (crates-io))

(define-public crate-hardpass-0.1.0 (c (n "hardpass") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "sha-crypt") (r "^0.5.0") (d #t) (k 0)))) (h "05i5111mccxz540f85nk5gr1v3mqm4wzv83riryi82q8j7cm7imb")))

