(define-module (crates-io ha rd hard-xml-derive) #:use-module (crates-io))

(define-public crate-hard-xml-derive-0.6.3 (c (n "hard-xml-derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13349rkf9dxjr0kcjgh5flirmnlq9vy92dw1i6b27fp56w00k0nd")))

(define-public crate-hard-xml-derive-0.6.4 (c (n "hard-xml-derive") (v "0.6.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mdlm9ylxllbs55yfpawh8zj9w37pigg18rv3dr6ph4i8x5ms348")))

(define-public crate-hard-xml-derive-0.0.0 (c (n "hard-xml-derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d6h923sv8amsbqip5m43hlqqba9hj3pjrkpsp85y00rbl8kdiml")))

(define-public crate-hard-xml-derive-1.0.1 (c (n "hard-xml-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07nbnlaqa8503kd6ga63dz1xz69144wyklhgji275l5w23z4ybz2")))

(define-public crate-hard-xml-derive-1.1.0 (c (n "hard-xml-derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ghr1d8mdsafs75khvrgjxlg3bpql9lfsj6qxilp1901bah26qx7")))

(define-public crate-hard-xml-derive-1.4.0 (c (n "hard-xml-derive") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09bp0fzjc2dxnz6r4s9q4giqci4k873hdr96gzg2v7zk719ajqpl")))

(define-public crate-hard-xml-derive-1.5.0 (c (n "hard-xml-derive") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1if0ji2lh74l57xz74rs4dvaw34wvcpm66yvrf8d5hn3iw7gn940")))

(define-public crate-hard-xml-derive-1.7.0 (c (n "hard-xml-derive") (v "1.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07n32w8qddr6lkp3ch4cf609gkla0bc11xj7k6b8h8y0akqfahzx")))

(define-public crate-hard-xml-derive-1.9.0 (c (n "hard-xml-derive") (v "1.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1301kxqh6rbbr6npb5cqaf3yadmbgxfqpn7xx7w4vljk50shjd1z")))

(define-public crate-hard-xml-derive-1.10.0 (c (n "hard-xml-derive") (v "1.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09cpyhym237jvc7mr0iagiyxyb8gpbsvbg95hhaj695x2dpyqk6j")))

(define-public crate-hard-xml-derive-1.12.0 (c (n "hard-xml-derive") (v "1.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15hnbijpadrihw565l2v0y1nhsg0pp97xf1faajxgpmg1a87szmy")))

(define-public crate-hard-xml-derive-1.13.0 (c (n "hard-xml-derive") (v "1.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jlpnzvsf4db9wvxn35l0k6388lpfsyzw0rzzxwj8g8k5dg5i91s")))

(define-public crate-hard-xml-derive-1.15.0 (c (n "hard-xml-derive") (v "1.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vmh1i2b25h3djw5dbznb0d1j2k6w51qrxckl45a8pvjzxr9cqk1")))

(define-public crate-hard-xml-derive-1.17.0 (c (n "hard-xml-derive") (v "1.17.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cs9fzryy09scch80gh2g6g1piy5sxcrxsz9mbbgjzlh142x4sy0")))

(define-public crate-hard-xml-derive-1.19.0 (c (n "hard-xml-derive") (v "1.19.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "128rk2c60mvw4krxl7lc4k4hz45hkwklmhw2fgiq0n4mqjw3r6rs")))

(define-public crate-hard-xml-derive-1.21.0 (c (n "hard-xml-derive") (v "1.21.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "108qlbk6lds0llf02rd8gqcl59pay5bhjviff5ggd05mdrcl9rjj")))

(define-public crate-hard-xml-derive-1.23.0 (c (n "hard-xml-derive") (v "1.23.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jx7a897dxpg5632xqaiz0vx9qq5fxgrpq6f8wkaf7pl7z4wm4cx")))

(define-public crate-hard-xml-derive-1.25.0 (c (n "hard-xml-derive") (v "1.25.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pb13v2yxz6014bn4mlzww18f4g2gdgm7qs7v3rnvaraz5iq9aas")))

(define-public crate-hard-xml-derive-1.27.0 (c (n "hard-xml-derive") (v "1.27.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1azk42xmgpw87r3y42av51fwfz29jvm7bpfqlhcbyh6h438yh37m")))

(define-public crate-hard-xml-derive-1.34.0 (c (n "hard-xml-derive") (v "1.34.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xwxh65iw6cb3f5bs4r7cg3r5lmjnsfiv12gr6bvjlfs4yrlb8sp")))

(define-public crate-hard-xml-derive-1.36.0 (c (n "hard-xml-derive") (v "1.36.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xm4sdip896ywmhn8makhdhdiqcsinag3k19d6lhxr93zv6ygyhv")))

