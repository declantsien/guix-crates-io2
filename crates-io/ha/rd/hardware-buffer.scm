(define-module (crates-io ha rd hardware-buffer) #:use-module (crates-io))

(define-public crate-hardware-buffer-0.1.0 (c (n "hardware-buffer") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.1") (d #t) (k 0)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1ska6ml06h11mv3g5wv0mcjzcd2i7kwyr225cmvmn5zdg6sb5l1n")))

(define-public crate-hardware-buffer-0.1.1 (c (n "hardware-buffer") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.1") (d #t) (k 0)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1ghc3mcdqzlvj6zhq28w3y44mvq6cx05j86zk5pnaw92j2xy5z5h")))

(define-public crate-hardware-buffer-0.1.2 (c (n "hardware-buffer") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.1") (d #t) (k 0)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "016hkriw9qhsk9j3dj0c7yxwybcgsi11bpc367lw309kzzlbr65n")))

