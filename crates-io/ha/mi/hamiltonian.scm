(define-module (crates-io ha mi hamiltonian) #:use-module (crates-io))

(define-public crate-hamiltonian-0.0.0 (c (n "hamiltonian") (v "0.0.0") (h "0w99k1hc5zwzmv4aia0pqvgrh8052nl1d6y0xv53nncwijyzzbhg") (f (quote (("default"))))))

(define-public crate-hamiltonian-0.0.1 (c (n "hamiltonian") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0p1qdxljbccn0l7rs95snmii7lwh1y2jz76q6d2ghzb2ffmnd0py") (f (quote (("default"))))))

