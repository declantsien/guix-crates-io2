(define-module (crates-io ha ts hatsuon) #:use-module (crates-io))

(define-public crate-hatsuon-0.1.0 (c (n "hatsuon") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cosmic-text") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.9.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)))) (h "0vs90wsn7ycy1gv0isv9arvi4j2jqpl87zn6bf8y1202rslj6jq6")))

