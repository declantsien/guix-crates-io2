(define-module (crates-io ha ms hamster) #:use-module (crates-io))

(define-public crate-hamster-0.0.3 (c (n "hamster") (v "0.0.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "git_info") (r "^0.1") (d #t) (k 0)) (d (n "gitlab_ci_parser") (r "^0.0.4") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "0s0mzbx2pfvmsjgqnjv9cm8hn4h0bhwrywclp2kjii7vnv7j24np")))

(define-public crate-hamster-0.0.4 (c (n "hamster") (v "0.0.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "git_info") (r "^0.1") (d #t) (k 0)) (d (n "gitlab_ci_parser") (r "^0.0.5") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "13j97dwm6ynz6d10475wmwjigqzjm2f7y5xvzlzii85v9ask2zij")))

