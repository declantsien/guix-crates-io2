(define-module (crates-io ha rr harriet) #:use-module (crates-io))

(define-public crate-harriet-0.1.0 (c (n "harriet") (v "0.1.0") (d (list (d (n "cookie-factory") (r "^0.3.2") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "nom") (r "^6.2.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "00621lbcf5223mqrk2l8kq9k7xb7wk5zcwn43ynnmx15a83wldy7")))

(define-public crate-harriet-0.1.1 (c (n "harriet") (v "0.1.1") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1fmmi36dav13rndzknc0dmdqcfi4lc1pf1w6y37ahz8ifivm7i6m")))

(define-public crate-harriet-0.2.0 (c (n "harriet") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "snowflake") (r "^1") (d #t) (k 0)))) (h "09cbczamhdfwx0wi4bwdxxq6wjqiqhxry93rx26xhbmmqgvl94jy")))

(define-public crate-harriet-0.2.1 (c (n "harriet") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "snowflake") (r "^1") (d #t) (k 0)))) (h "0cy3n3hrbrbjmk1j69yid598hmw8yfzm08rcxl9apf5lmjazwhlz")))

(define-public crate-harriet-0.2.2 (c (n "harriet") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "oxiri") (r "^0.2.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "snowflake") (r "^1") (d #t) (k 0)))) (h "1rf829fjq2salvdpnx53y4lsgnrjyi4aa7kij7qzrmcyx383qc6z")))

(define-public crate-harriet-0.2.3 (c (n "harriet") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "oxiri") (r "^0.2.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "snowflake") (r "^1") (d #t) (k 0)))) (h "0wjigipbg124jhh6g7043chqf37shq6myy9z5mv03h6gf05bvc43")))

(define-public crate-harriet-0.3.0 (c (n "harriet") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "oxiri") (r "^0.2.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "snowflake") (r "^1") (d #t) (k 0)))) (h "1jf1swhfxxsbw9fv6z4bz8av13prr3frxlvcp7hykgdgz8ibx9al")))

(define-public crate-harriet-0.3.1 (c (n "harriet") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "oxiri") (r "^0.2.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "snowflake") (r "^1") (d #t) (k 0)))) (h "1vl94r07mg98fj5lyvfpp68laaanrw0s92c5hqydakvh84y36m6l") (r "1.65.0")))

