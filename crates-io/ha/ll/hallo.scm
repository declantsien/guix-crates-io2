(define-module (crates-io ha ll hallo) #:use-module (crates-io))

(define-public crate-hallo-0.1.0 (c (n "hallo") (v "0.1.0") (h "03np9dr8qzy3vi7z8z7wgby98rvhxk2fdxvc8r3qqs0wm2cci6zy")))

(define-public crate-hallo-0.1.1 (c (n "hallo") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "0jv0zs3517cw5501gx19z35ll6hg849hdxix6a70ahs2b4p1y5z9")))

(define-public crate-hallo-0.1.2 (c (n "hallo") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "18714mj9jnrjxd067vbcsgf925y2gsip7kaqs0nd6iiqzm8cfi23")))

