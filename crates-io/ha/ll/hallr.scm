(define-module (crates-io ha ll hallr) #:use-module (crates-io))

(define-public crate-hallr-0.1.0 (c (n "hallr") (v "0.1.0") (d (list (d (n "hronn") (r "^0.1.0") (f (quote ("glam"))) (d #t) (k 0)) (d (n "krakel") (r "^0.2.0") (f (quote ("glam"))) (d #t) (k 0)) (d (n "linestring") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "vector-traits") (r "^0.2.0") (f (quote ("glam"))) (d #t) (k 0)))) (h "1cpgn5bqd91plyxi2sj3s9q964fy1ha6j53nd84254zcrxh5ss0h")))

