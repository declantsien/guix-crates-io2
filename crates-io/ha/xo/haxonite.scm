(define-module (crates-io ha xo haxonite) #:use-module (crates-io))

(define-public crate-haxonite-0.1.0 (c (n "haxonite") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "logger") (r "^0.2.0") (d #t) (k 0)) (d (n "mount") (r "^0.2") (d #t) (k 0)) (d (n "random_choice") (r "^0.3") (d #t) (k 0)) (d (n "router") (r "^0.4") (d #t) (k 0)) (d (n "rustache") (r "^0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "simplelog") (r "^0.4.2") (d #t) (k 0)) (d (n "staticfile") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "1yfllg9cfacmcdrlab2zhk92kbyp09117qvijx87hcb1bn5ww8pj")))

