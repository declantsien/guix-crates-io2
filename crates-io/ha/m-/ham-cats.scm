(define-module (crates-io ha m- ham-cats) #:use-module (crates-io))

(define-public crate-ham-cats-0.1.0 (c (n "ham-cats") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (k 0)) (d (n "half") (r "^2.3.1") (k 0)) (d (n "labrador-ldpc") (r "^1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (k 0)))) (h "1d4sjzm5xyf6yh4wpc82xmkisk1kryh0m4j31pa044ij416mclr1")))

(define-public crate-ham-cats-0.2.0 (c (n "ham-cats") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (k 0)) (d (n "half") (r "^2.3.1") (k 0)) (d (n "labrador-ldpc") (r "^1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (k 0)))) (h "1n7za17pi8mdrb98ladm07nwa3bpfzg2d2r6pbhg2fdrxc9l4ajy")))

(define-public crate-ham-cats-0.2.1 (c (n "ham-cats") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (k 0)) (d (n "half") (r "^2.3.1") (k 0)) (d (n "labrador-ldpc") (r "^1.2.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (k 0)))) (h "10rkhhpc3hl9n8drsgr0v4m5vmay9alijlys9rnfp3pcympbfr4c")))

