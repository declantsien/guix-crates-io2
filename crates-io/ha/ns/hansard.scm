(define-module (crates-io ha ns hansard) #:use-module (crates-io))

(define-public crate-hansard-0.1.0 (c (n "hansard") (v "0.1.0") (d (list (d (n "atom_syndication") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)))) (h "0y0dsil8lf3lan000lsm9sqiqza7za7b7gxi71inhfagh4w2lyxj")))

(define-public crate-hansard-0.1.1 (c (n "hansard") (v "0.1.1") (d (list (d (n "atom_syndication") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)))) (h "05wh20nphg505fd19djxc7ry268525b387g5awilywipn36m42p6")))

(define-public crate-hansard-0.1.2 (c (n "hansard") (v "0.1.2") (d (list (d (n "atom_syndication") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "039qsjkwjkvnjpngcrv87ggbl9bxklmw37j89kpkdms523ccky0b")))

(define-public crate-hansard-0.1.3 (c (n "hansard") (v "0.1.3") (d (list (d (n "atom_syndication") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "0jwwmx4kv858af6mdr05w82h7k64d64vlm70i01vvbczicd65iy0")))

