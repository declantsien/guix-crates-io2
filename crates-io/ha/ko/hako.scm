(define-module (crates-io ha ko hako) #:use-module (crates-io))

(define-public crate-hako-0.0.0 (c (n "hako") (v "0.0.0") (d (list (d (n "colored") (r "^3.0.0") (d #t) (k 2) (p "owo-colors")) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.0") (d #t) (k 0)))) (h "0aa9q9ql08mir8s504q5ckv8h24xgrw3bp7sh3sfiiwy5cnagwm0") (r "1.56.0")))

