(define-module (crates-io ha pp happynum) #:use-module (crates-io))

(define-public crate-happynum-1.4.1 (c (n "happynum") (v "1.4.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "080fmnqhv5f6zl2zbxwa15xy4v3nphyqmzrpfwd3nkzgzbv93dpw")))

(define-public crate-happynum-1.4.2 (c (n "happynum") (v "1.4.2") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "1575vgc7q6h2wfdifr7x4xsj7jsd57qg3paj4l791kb65zf8bg4q")))

