(define-module (crates-io ha pp happi) #:use-module (crates-io))

(define-public crate-happi-0.0.1 (c (n "happi") (v "0.0.1") (h "111ys80587j5zsqpcm664f8acb2q1gp7gzzj2njqyqd65ywhwmd1")))

(define-public crate-happi-0.0.2 (c (n "happi") (v "0.0.2") (h "0wwggcmzxm9gzcn840fmpls5vzkdx7225i266bq7vjxp98sshknc")))

(define-public crate-happi-0.0.4 (c (n "happi") (v "0.0.4") (d (list (d (n "happi-derive") (r ">=0") (d #t) (k 0)))) (h "160lssg2sgdsm9mw968mdxrvznsgllnaj4isvhki5yrzaplfmcm9")))

(define-public crate-happi-0.0.5 (c (n "happi") (v "0.0.5") (d (list (d (n "happi-derive") (r ">=0") (d #t) (k 0)))) (h "11vjdclqppi0dxa343kf6n8ryzy2skswv6sinsj519bjfjc46ff5")))

(define-public crate-happi-0.0.6 (c (n "happi") (v "0.0.6") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 0)) (d (n "happi-derive") (r ">=0") (d #t) (k 0)) (d (n "hyper") (r "^0.14.9") (f (quote ("client" "tcp" "http1" "http2"))) (d #t) (k 0)) (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "18azj797blddmmykww2njaiqmbix7ij8apwq4ikssy4bjifdrrab")))

