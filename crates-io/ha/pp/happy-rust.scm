(define-module (crates-io ha pp happy-rust) #:use-module (crates-io))

(define-public crate-happy-rust-0.1.0 (c (n "happy-rust") (v "0.1.0") (d (list (d (n "log") (r "~0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "~1.2.0") (d #t) (k 0)) (d (n "logtest") (r "~2.0.0") (d #t) (k 2)))) (h "086alv8ga0dl01mf77r1lhjh9bbhi4pk422xv4dciv0zxwjs3fy5")))

(define-public crate-happy-rust-0.1.1 (c (n "happy-rust") (v "0.1.1") (d (list (d (n "log") (r "~0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "~1.2.0") (d #t) (k 0)) (d (n "logtest") (r "~2.0.0") (d #t) (k 2)))) (h "04lw2ffgy4aggm4gv5hvqzk1bm4ka4izra0ymy68b0w91wacycsr")))

(define-public crate-happy-rust-0.1.2 (c (n "happy-rust") (v "0.1.2") (d (list (d (n "log") (r "~0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "~1.2.0") (d #t) (k 0)) (d (n "logtest") (r "~2.0.0") (d #t) (k 2)))) (h "06jksaf0zif0c4gzjl2shx9jicapq7i2x4h4hp4cgkagabdx8q7a")))

(define-public crate-happy-rust-0.1.3 (c (n "happy-rust") (v "0.1.3") (d (list (d (n "log") (r "~0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "~1.2.0") (d #t) (k 0)) (d (n "logtest") (r "~2.0.0") (d #t) (k 2)))) (h "0a30ydc8fsj295sbygfgbpg3ihjfywfalzrj1xhf5hsm619mjgb9")))

