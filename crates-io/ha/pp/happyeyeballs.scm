(define-module (crates-io ha pp happyeyeballs) #:use-module (crates-io))

(define-public crate-happyeyeballs-0.1.0 (c (n "happyeyeballs") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("net" "time" "macros"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "io-util"))) (k 2)))) (h "0ygj9dnnq6bq4b3qdl2zbl9h63mci4c9k14g5x4yn7ipgma7dijf")))

(define-public crate-happyeyeballs-0.1.1 (c (n "happyeyeballs") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("net" "time" "macros"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "io-util"))) (k 2)))) (h "1fwg1lnldmq4xk5cw64r7ysyw1ijwpxn19jm5p7wgf7i4ycp86kd")))

(define-public crate-happyeyeballs-0.1.2 (c (n "happyeyeballs") (v "0.1.2") (d (list (d (n "tokio") (r "^1") (f (quote ("net" "time" "macros"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "io-util"))) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-test") (r "^0.1") (d #t) (k 2)))) (h "03k74v7h779ic0ld1qzqz4if2wz71zia4qgan95sr7fi8pk764m5")))

