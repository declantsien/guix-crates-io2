(define-module (crates-io ha pp happy-tweet) #:use-module (crates-io))

(define-public crate-happy-tweet-0.1.0 (c (n "happy-tweet") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "114rr110xflqbd27vnnnvsyvj2bnn18f7y9v7rjqkjs12ay57x55")))

