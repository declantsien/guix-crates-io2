(define-module (crates-io ha pp happiness_cli) #:use-module (crates-io))

(define-public crate-happiness_cli-0.1.0 (c (n "happiness_cli") (v "0.1.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0asp7lhggpkx7md14j5i5a9dbjgwy9jipd03yznb1ksp8l738hvw")))

(define-public crate-happiness_cli-0.1.1 (c (n "happiness_cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0hbd18sr7xff1vg6hmrlb6qcdmcrjx71kl52p8p8ismmmm1bfvkp")))

(define-public crate-happiness_cli-0.1.2 (c (n "happiness_cli") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0r5rwk0rbiddgvwi2xgvqwryss5z95sxyhkyr6bd833wyqczddqi")))

(define-public crate-happiness_cli-0.1.21 (c (n "happiness_cli") (v "0.1.21") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1dpwphmviidwspwkmcbz3hkslz4i8iv33kc1fnikf17d03v1m9n3")))

(define-public crate-happiness_cli-0.1.22 (c (n "happiness_cli") (v "0.1.22") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0q3a7giqk0nj2vb901scxsxlbj8spiq4zphqlhmy6pa3gr4n5z0f")))

(define-public crate-happiness_cli-0.1.23 (c (n "happiness_cli") (v "0.1.23") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1gbhhxvgzs39cxd6sxhm7v5jshsjhncvx3pwfjmqyccfgq13n9q0")))

(define-public crate-happiness_cli-0.1.3 (c (n "happiness_cli") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1wykkfhpnw178xw3iwv4n74dzgr2abm4fr7c1kn69lprcfmp39hq")))

(define-public crate-happiness_cli-0.1.31 (c (n "happiness_cli") (v "0.1.31") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0nsqq9135zi1k2cvfakvgmgfhh94kj3s7q5c09xnnza9d1fdk0z7")))

