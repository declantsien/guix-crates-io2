(define-module (crates-io ha pp happylog) #:use-module (crates-io))

(define-public crate-happylog-0.1.0 (c (n "happylog") (v "0.1.0") (d (list (d (n "console") (r ">=0.9") (d #t) (k 0)) (d (n "indicatif") (r ">=0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "structopt") (r ">=0.2") (o #t) (d #t) (k 0)))) (h "18risdvyk8f8kalbn0kp35l2kb99b4vg2p7zds84bqj92ic0hhc9") (f (quote (("default" "structopt"))))))

(define-public crate-happylog-0.2.0 (c (n "happylog") (v "0.2.0") (d (list (d (n "console") (r ">=0.9") (d #t) (k 0)) (d (n "indicatif") (r ">=0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "structopt") (r ">=0.2") (o #t) (d #t) (k 0)))) (h "155qxx59jrybsd1ibfk8g42qanglcrqlafiqg2ynfzqyjfy323sk") (f (quote (("default" "structopt"))))))

(define-public crate-happylog-0.3.0-b1 (c (n "happylog") (v "0.3.0-b1") (d (list (d (n "console") (r ">=0.9") (d #t) (k 0)) (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "structopt") (r ">=0.2") (o #t) (d #t) (k 0)))) (h "0s5rb1mp0csrpj92qdd1avp85qb0qk0cgjaalvcjklfalj5qyqk9") (f (quote (("default" "structopt" "colored") ("colored" "fern/colored"))))))

(define-public crate-happylog-0.3.0-b2 (c (n "happylog") (v "0.3.0-b2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r ">=0.2") (o #t) (d #t) (k 0)))) (h "09sydw114c9yqdvrkr7qkb5l3l39f6aa9cj589daydsg45k02iyx") (f (quote (("default" "colored") ("colored" "fern/colored"))))))

(define-public crate-happylog-0.3.0-b3 (c (n "happylog") (v "0.3.0-b3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r ">=0.2") (o #t) (d #t) (k 0)))) (h "0drv9yic1f4kr9rk5246why36vj9540nn6s01hnq87amd7igfvcd") (f (quote (("default" "colored") ("colored" "fern/colored"))))))

(define-public crate-happylog-0.3.0-b4 (c (n "happylog") (v "0.3.0-b4") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r ">=0.2") (o #t) (d #t) (k 0)))) (h "0xq5wbn5pam0cr4hjm853w584j000cj6qbdkdhy4vaqvpniz65wa") (f (quote (("default" "colored") ("colored" "fern/colored")))) (y #t)))

(define-public crate-happylog-0.3.0-b6 (c (n "happylog") (v "0.3.0-b6") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r ">=0.2") (o #t) (d #t) (k 0)))) (h "0fmivwccp1pv6bv7hgn0rpa0mhmpcma7z1m0bpg7clj6xv2libqf") (f (quote (("default" "colored") ("colored" "fern/colored"))))))

(define-public crate-happylog-0.3.0-b7 (c (n "happylog") (v "0.3.0-b7") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r ">=0.2") (o #t) (d #t) (k 0)))) (h "10s99cc016iyz1hk276g3c6ila0mvskjgfkak20mm3n91zz1cgi5") (f (quote (("default" "colored") ("colored" "fern/colored"))))))

