(define-module (crates-io ha pp happv) #:use-module (crates-io))

(define-public crate-happv-0.1.0 (c (n "happv") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0fi1shrgqc9lc35df10cdkril276p913n2bfv8f4q451wfxbxwra") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-happv-0.1.1 (c (n "happv") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "15p1fbdzmhfh83gqlw6skpamvpwy74pc6x9ph7vz6lhiv7lnjhrr") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-happv-0.1.2 (c (n "happv") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1b1f3alyrkw5cfz2hh5fxj93yfxdn1lk4fhs0xdmqmdcknzpcimi") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

