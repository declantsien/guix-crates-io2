(define-module (crates-io ha pp happi-derive) #:use-module (crates-io))

(define-public crate-happi-derive-0.0.1 (c (n "happi-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)))) (h "1kqx49yj0893y909psj6jdq2bnxmqiq460la22y5wyi4idzrg8rj")))

(define-public crate-happi-derive-0.0.2 (c (n "happi-derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)))) (h "0amv62ml6xgm94lmsnzk0bzgxr8vxl371lzqhxfrkzpvwyxmcrw5")))

(define-public crate-happi-derive-0.0.3 (c (n "happi-derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)))) (h "07z6nbgx85ad5q5k86l5dwdpyzxbg1vfyl9rgi4ywb5qyr5lw5s9")))

(define-public crate-happi-derive-0.0.4 (c (n "happi-derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)))) (h "1a5yyh50fqwy8faryrh4gxs0p83cwld9yrjf9hmaapnr4012lq8g")))

(define-public crate-happi-derive-0.0.5 (c (n "happi-derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)))) (h "117fpkn9p3km6q816pxafq7jaralv7pv12fwd3hidc3irr2hydcl")))

(define-public crate-happi-derive-0.0.6 (c (n "happi-derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)))) (h "17g5v0zbflgf7npbmmwdmkvhbmxkiy0cqyfhqhxgvyp0r365g56n")))

