(define-module (crates-io ha pp happylock) #:use-module (crates-io))

(define-public crate-happylock-0.1.0 (c (n "happylock") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "17ywb4zwkjbg6wfzwgl08wb8fzzccb2vayksd23nmsh6j3jlv048") (f (quote (("default" "parking_lot")))) (y #t)))

(define-public crate-happylock-0.1.1 (c (n "happylock") (v "0.1.1") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "0gd5zhllr71z1ijxkzrhni8nvbfyv0drri31s74w10n73h9kcr8a") (f (quote (("default" "parking_lot"))))))

(define-public crate-happylock-0.1.2 (c (n "happylock") (v "0.1.2") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "1xhrz6j0n8mfvi4p238wp05p03rdwsi1fw90v4z1459rc7jpmnqk") (f (quote (("default" "parking_lot")))) (y #t)))

(define-public crate-happylock-0.1.3 (c (n "happylock") (v "0.1.3") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "1w44j8gnl3asrql10kbhs86z0pwyh86hwjnf85cpci3zc8v067ri") (f (quote (("default" "parking_lot")))) (y #t)))

(define-public crate-happylock-0.1.4 (c (n "happylock") (v "0.1.4") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "1jl3kxbrw50wk064lg48949b9z6k7gwmh1sf7j7vclrl2ax91sdx") (f (quote (("default" "parking_lot"))))))

(define-public crate-happylock-0.1.5 (c (n "happylock") (v "0.1.5") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "0l2wxhql43a2idikpasfjp4chy0a0haw5lbpadj2dkl4zispmbbg") (f (quote (("default" "parking_lot"))))))

(define-public crate-happylock-0.2.0 (c (n "happylock") (v "0.2.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "01gdqbxjhk6xa3rmb2ivjx77iz2m5lbddig25i4wzski56viby0k") (f (quote (("default" "parking_lot"))))))

