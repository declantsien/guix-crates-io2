(define-module (crates-io ha ku haku-client) #:use-module (crates-io))

(define-public crate-haku-client-0.1.0 (c (n "haku-client") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "16z5842i6m53z7p52y4vy4m9k3lzw425klas8y9p33kxm8d15wrb") (y #t)))

