(define-module (crates-io ha la hala-pprof-derive) #:use-module (crates-io))

(define-public crate-hala-pprof-derive-0.1.4 (c (n "hala-pprof-derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)) (d (n "uuid") (r "^1.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cm5hanms46dd9xcfbyi8xy4i6fz008ngf4zqk8bsmi3sl0frb3h")))

(define-public crate-hala-pprof-derive-0.1.5 (c (n "hala-pprof-derive") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)) (d (n "uuid") (r "^1.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "08xrw8gvvimsmvr8v4xg67xwkzq13rrbv9siis43ffmplkzcpxfk")))

(define-public crate-hala-pprof-derive-0.1.6 (c (n "hala-pprof-derive") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)) (d (n "uuid") (r "^1.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lya6gc999rjia1gap19cr5b1klb9f42va2pfxndn0xdm1rmlcny")))

