(define-module (crates-io ha la hala-io-util) #:use-module (crates-io))

(define-public crate-hala-io-util-0.1.2 (c (n "hala-io-util") (v "0.1.2") (d (list (d (n "bytes") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (f (quote ("executor" "thread-pool"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (f (quote ("executor" "thread-pool"))) (d #t) (k 2)) (d (n "hala-io-driver") (r "^0.1") (d #t) (k 0)) (d (n "hala-io-test") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)))) (h "1hgjdvl7yifdk8d9z324bddyl5zggrx90ga816skaq86pkgqfmv2") (f (quote (("timeout") ("read_buf" "bytes") ("mux") ("io_group") ("default" "async_io" "io_group" "mux" "timeout" "read_buf") ("async_io"))))))

