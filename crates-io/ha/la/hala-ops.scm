(define-module (crates-io ha la hala-ops) #:use-module (crates-io))

(define-public crate-hala-ops-0.1.2 (c (n "hala-ops") (v "0.1.2") (h "1g1hg79lrm8xqszhqcg27qx2xjqbprl46kdc5kgb6311cy9691cx")))

(define-public crate-hala-ops-0.1.3 (c (n "hala-ops") (v "0.1.3") (h "1vsp8vrpq8j393qcaqbh49rjsm0s7igx9i46lgmbl6qnrh2k72ld")))

(define-public crate-hala-ops-0.1.4 (c (n "hala-ops") (v "0.1.4") (h "0mv0mkh1xjygzb3f3zpkyqzf9nymqpphhq56gyiaw67skw8fs82i")))

(define-public crate-hala-ops-0.1.5 (c (n "hala-ops") (v "0.1.5") (h "0yqs7qgyfl15k48zk01wyp0m6m80xp6nkfjqrssrfp1g7y0c3qfq")))

(define-public crate-hala-ops-0.1.6 (c (n "hala-ops") (v "0.1.6") (h "1lmnb2yzld8ba7a3gr9an5hmbky0m2rd7ng9jyjxx5pqvblakgcs")))

