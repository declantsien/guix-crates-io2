(define-module (crates-io ha la hala-io-test) #:use-module (crates-io))

(define-public crate-hala-io-test-0.1.2 (c (n "hala-io-test") (v "0.1.2") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (f (quote ("executor" "thread-pool"))) (d #t) (k 0)) (d (n "hala-io-driver") (r "^0.1") (d #t) (k 0)) (d (n "hala-io-test-derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)))) (h "09ag4iychcdrwg5hwxfj3qa7ddzgbhj1phsjlrmyssg34jlr442b") (f (quote (("debug"))))))

