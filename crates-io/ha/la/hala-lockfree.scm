(define-module (crates-io ha la hala-lockfree) #:use-module (crates-io))

(define-public crate-hala-lockfree-0.1.2 (c (n "hala-lockfree") (v "0.1.2") (d (list (d (n "boxcar") (r "^0.2.4") (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "divan") (r "^0.1") (d #t) (k 2)))) (h "09a3cv3xi2l9wi78gywfcfqs0wb99hm7lgjlmk92a80whq75lakk")))

(define-public crate-hala-lockfree-0.1.3 (c (n "hala-lockfree") (v "0.1.3") (d (list (d (n "boxcar") (r "^0.2.4") (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "divan") (r "^0.1") (d #t) (k 2)))) (h "19byypfbi03lzcfzm6pi1yfv3im6z95crqlxbfp9dg2cqn0lvj98")))

(define-public crate-hala-lockfree-0.1.4 (c (n "hala-lockfree") (v "0.1.4") (d (list (d (n "boxcar") (r "^0.2.4") (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "divan") (r "^0.1") (d #t) (k 2)))) (h "10m40w1xaxc6pcp8a45yq0licpdi68n1lyvgyiyl0b0vff4aqmba")))

(define-public crate-hala-lockfree-0.1.5 (c (n "hala-lockfree") (v "0.1.5") (d (list (d (n "boxcar") (r "^0.2.4") (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "divan") (r "^0.1") (d #t) (k 2)))) (h "0cm053zpjjqrc6akfbpm405ly2g4yjf9a46x8iavnjdyg0y9n3dz")))

(define-public crate-hala-lockfree-0.1.6 (c (n "hala-lockfree") (v "0.1.6") (d (list (d (n "boxcar") (r "^0.2.4") (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "divan") (r "^0.1") (d #t) (k 2)))) (h "0ns1cgmk2hkgkjmnmvpz76qmmcf67nsk9f2prlxajn4qqk4mdi3n")))

