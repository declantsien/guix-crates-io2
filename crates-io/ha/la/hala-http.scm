(define-module (crates-io ha la hala-http) #:use-module (crates-io))

(define-public crate-hala-http-0.1.4 (c (n "hala-http") (v "0.1.4") (h "0falyw408cqwsmp153kq8jk68jbqlln36sjrp5abm0gjr62qd57l")))

(define-public crate-hala-http-0.1.5 (c (n "hala-http") (v "0.1.5") (h "1cml7h4yi07ia14bh5csffaa0iwcnms3ba3lr6z47n3bpwnyqwl2")))

(define-public crate-hala-http-0.1.6 (c (n "hala-http") (v "0.1.6") (h "0fb85y05m58lvsw75mnaah4hfvlxd52dv1l61fjihkpkb3z8xdk9")))

