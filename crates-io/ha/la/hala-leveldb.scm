(define-module (crates-io ha la hala-leveldb) #:use-module (crates-io))

(define-public crate-hala-leveldb-0.1.4 (c (n "hala-leveldb") (v "0.1.4") (d (list (d (n "leveldb-sys") (r "^2.0.9") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0w9pav10h0jr2n06sl4lqgd02qxnw6hifzmnl7hvzanl5xvvzy67")))

(define-public crate-hala-leveldb-0.1.5 (c (n "hala-leveldb") (v "0.1.5") (d (list (d (n "leveldb-sys") (r "^2.0.9") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ds7jn7cr66b3mb03kzifw5i1ahv9xqc20yj9q64kif9pjczvn34")))

(define-public crate-hala-leveldb-0.1.6 (c (n "hala-leveldb") (v "0.1.6") (d (list (d (n "leveldb-sys") (r "^2.0.9") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1s5jvjjclamawvdc5ib1imiq2f1hr9gsq445jym5ii2pkk7lx5ia")))

