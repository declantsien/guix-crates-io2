(define-module (crates-io ha la hala-test) #:use-module (crates-io))

(define-public crate-hala-test-0.1.2 (c (n "hala-test") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "02g9mxsbp5s5x0478bqwkx3z2msibw9yw009la1hix07jwnf2g6i")))

(define-public crate-hala-test-0.1.3 (c (n "hala-test") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "19il4pbabvf95zmm8wzgymx9rfln49mi4449041crziwglp0ywr5")))

(define-public crate-hala-test-0.1.4 (c (n "hala-test") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "03b89wskcq18ngyd5jngy124f5g7ji32hbalb7b6cqsiinn284bi")))

(define-public crate-hala-test-0.1.5 (c (n "hala-test") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1r8pmzp4n6m7rljihvhb4bq2sp6zlmx1v0h86ci5mrmwck0mddbx")))

(define-public crate-hala-test-0.1.6 (c (n "hala-test") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "06ib30yzclhi2738vkzn0mz4qc6y8b25dp764d0h0qg34l2dm2z6")))

