(define-module (crates-io ha la hala-net) #:use-module (crates-io))

(define-public crate-hala-net-0.1.2 (c (n "hala-net") (v "0.1.2") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "divan") (r "^0.1") (d #t) (k 2)) (d (n "future-mediator") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (f (quote ("executor" "thread-pool"))) (d #t) (k 0)) (d (n "hala-io-driver") (r "^0.1") (d #t) (k 0)) (d (n "hala-io-test") (r "^0.1") (d #t) (k 2)) (d (n "hala-io-util") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.8.9") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)) (d (n "quiche") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "ring") (r "^0.17.6") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0swg98wxaif86yn2izv8wzmxi59sw9f24b2xgrq7k9csvffjd0xl") (f (quote (("quice" "quiche" "rand" "ring") ("default" "quice"))))))

