(define-module (crates-io ha de hadean-std) #:use-module (crates-io))

(define-public crate-hadean-std-0.1.0 (c (n "hadean-std") (v "0.1.0") (d (list (d (n "hadean") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1qmfhfqbgnmxqnf4yriqffpk6xajynzyj29pinsqbia7acf3b23n")))

(define-public crate-hadean-std-0.1.1 (c (n "hadean-std") (v "0.1.1") (d (list (d (n "hadean") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1s5h980qsi337hyib00c8py2vj8s54g3jk0ap9w6bddp4w4bzb5x")))

(define-public crate-hadean-std-0.1.2 (c (n "hadean-std") (v "0.1.2") (d (list (d (n "hadean") (r "^0.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.1") (d #t) (k 0)) (d (n "odds") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0yghhy98g21j7ma0w8k56jda5jdsrk0aw04cf5v1lsxf310yhm47")))

(define-public crate-hadean-std-0.1.3 (c (n "hadean-std") (v "0.1.3") (d (list (d (n "hadean") (r "^0.1.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.1.0") (d #t) (k 0)) (d (n "odds") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1n8mnrs5zqwxwbh3hf8mzdfj3aj2zhbq41vy8d7k2vwk90l1i4px")))

(define-public crate-hadean-std-0.1.4 (c (n "hadean-std") (v "0.1.4") (d (list (d (n "hadean") (r "^0.1.4") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.1.0") (d #t) (k 0)) (d (n "odds") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1w7mzmiwa373dd45x9srvm48yiqxwm1gawkpc56v68wl44x99xrf")))

(define-public crate-hadean-std-0.1.5 (c (n "hadean-std") (v "0.1.5") (d (list (d (n "hadean") (r "^0.1.4") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.1.0") (d #t) (k 0)) (d (n "odds") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1d7j3dpfkwn3wx4q2g9khs8q7zvfmz8cljf7m74iiclmzanv42br")))

(define-public crate-hadean-std-0.1.6 (c (n "hadean-std") (v "0.1.6") (d (list (d (n "hadean") (r "^0.1.4") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.1.0") (d #t) (k 0)) (d (n "odds") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1d6vby36w65y68bzmi3jkmw9mlglr8j3zw0qvbbnazblad61gwni")))

(define-public crate-hadean-std-0.2.0 (c (n "hadean-std") (v "0.2.0") (d (list (d (n "hadean") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1azkyc7r6madzq6rl65fh9gm18l7azndcla0b690cb776gpkdbkf")))

