(define-module (crates-io ha de hadean) #:use-module (crates-io))

(define-public crate-hadean-0.1.0 (c (n "hadean") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0brs4299lwdbvg46pilmyqf8ga4f0fp3raaqzpfh5b0x8ljxvl9l")))

(define-public crate-hadean-0.1.3 (c (n "hadean") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18ss4pbqbkf3aiv6byp4rcamv34m6vhgrvkqad6crdwzxpwgfbbv")))

(define-public crate-hadean-0.1.4 (c (n "hadean") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "010gazwhq4vfy7b2pcpw68hdw2z0a8ghr33nzkjxrjcdbx2qrss1")))

(define-public crate-hadean-0.2.0 (c (n "hadean") (v "0.2.0") (d (list (d (n "aidanhs-tmp-parse-generics-shim") (r "^0.1.3") (d #t) (k 0)) (d (n "bincode") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)))) (h "1v50zp958n7i0lyqnxv83kjrbln32j4y8w6k8qw90cjknfqk5n1s")))

(define-public crate-hadean-0.2.1 (c (n "hadean") (v "0.2.1") (d (list (d (n "aidanhs-tmp-parse-generics-shim") (r "^0.1.3") (d #t) (k 0)) (d (n "bincode") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)))) (h "0nwx7d3hkhp8wbvz6zq191rx1m7frqnlnpmnbpqbrrcirn99gmi5")))

(define-public crate-hadean-0.2.2 (c (n "hadean") (v "0.2.2") (d (list (d (n "aidanhs-tmp-parse-generics-shim") (r "^0.1.3") (d #t) (k 0)) (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1781paq6vv2gmaw5bk0kwkihsrd5r518yyncjdx2npxlrilxg3r4")))

(define-public crate-hadean-0.2.3 (c (n "hadean") (v "0.2.3") (d (list (d (n "aidanhs-tmp-parse-generics-shim") (r "^0.1.3") (d #t) (k 0)) (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1d1g754za1xhcw3931q5swflj8p3l0wmnrfgs0dfxq1f8fpicgin")))

(define-public crate-hadean-0.2.4 (c (n "hadean") (v "0.2.4") (d (list (d (n "aidanhs-tmp-parse-generics-shim") (r "^0.1.3") (d #t) (k 0)) (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0bn9bhqyiacdd7ghd48p63hwvj89lwzb42g6hg7dzzblqzfq5ldd")))

