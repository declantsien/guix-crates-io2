(define-module (crates-io ha nd handwriting-scan-tool) #:use-module (crates-io))

(define-public crate-handwriting-scan-tool-0.1.0 (c (n "handwriting-scan-tool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "imageproc") (r "^0.24.0") (d #t) (k 0)))) (h "1jdfz00lbxvmska2gkqpg5qvc7l62vq44h7lixd302xgfvhwz77n")))

