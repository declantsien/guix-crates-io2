(define-module (crates-io ha nd handy_async) #:use-module (crates-io))

(define-public crate-handy_async-0.2.0 (c (n "handy_async") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "10nk0a4pzvpnpljvr1gm25riyqxig2j9jpsg0xspx60dlksndj47")))

(define-public crate-handy_async-0.2.1 (c (n "handy_async") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0hk0xdwlw0mp5sxzgbdfaidzjqkck2jsd6hk76ldniaqhnn48f26")))

(define-public crate-handy_async-0.2.2 (c (n "handy_async") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1phd253gcslrbrwnccgqspha6xkgnv3c78mg7kbv1na4mw5m3n52")))

(define-public crate-handy_async-0.2.3 (c (n "handy_async") (v "0.2.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1fls8q56qcyxl3xipsp0yh27qxy28dbqfc799a0clv47slbig2x2")))

(define-public crate-handy_async-0.2.4 (c (n "handy_async") (v "0.2.4") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "034mk42jn2s0wzr56a0x4xbc008mc46hs0mlqsyy8fi4ji6vvpmm")))

(define-public crate-handy_async-0.2.5 (c (n "handy_async") (v "0.2.5") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0b7z1d0z7bpva5s0znydrqfcc00s0788ia8mfjjs2h6khkpqnasa")))

(define-public crate-handy_async-0.2.6 (c (n "handy_async") (v "0.2.6") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "08gyzg2lnv69i35awjpaf5zkijyj8pha2zmcg5scf4l3xrnr9ayi")))

(define-public crate-handy_async-0.2.7 (c (n "handy_async") (v "0.2.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1gbrys8gk630qa0wqp831d4ym8r3m4xnlp00v51xki9hxpzqw7yf")))

(define-public crate-handy_async-0.2.8 (c (n "handy_async") (v "0.2.8") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "04plmdh705mrqp5dpq9vy065fv9h1sx7c99akwyi9px81hik0qfa")))

(define-public crate-handy_async-0.2.9 (c (n "handy_async") (v "0.2.9") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "09c185fy8gf8c3f5ffjkbmsjaw2nq6236r6zysph3r2xf6py1f15")))

(define-public crate-handy_async-0.2.10 (c (n "handy_async") (v "0.2.10") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0nq593an1sw3vgfy5chjnjb2gbpz232vg6z6sx2fipj3yq3w23wx")))

(define-public crate-handy_async-0.2.11 (c (n "handy_async") (v "0.2.11") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0gf1wmk93bilh5h9lwhw83wa6md4q8qjlizzw1n8ip7algkk650v")))

(define-public crate-handy_async-0.2.12 (c (n "handy_async") (v "0.2.12") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0j6v4c4gpjnjvg1l26idjqy7lkzv86aig8q99jrwc5kxfik629zw")))

(define-public crate-handy_async-0.2.13 (c (n "handy_async") (v "0.2.13") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0ckjpr2ca2mw2dfkwbx4k97y6gpijnl5ibd77wxrnfyx6ia5qp8n")))

