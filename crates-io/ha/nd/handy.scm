(define-module (crates-io ha nd handy) #:use-module (crates-io))

(define-public crate-handy-0.1.0 (c (n "handy") (v "0.1.0") (h "1rn8n9icycyy3wwis74d7pyn12gd75fcm92cqw1i0pcpv9lwxznr")))

(define-public crate-handy-0.1.1 (c (n "handy") (v "0.1.1") (h "0d1rnr1p144gg6nqm6m0b3czqqab25l0sja8ppbrky8r6ifas1kq")))

(define-public crate-handy-0.1.2 (c (n "handy") (v "0.1.2") (h "0b5jarn17k32cz7340yhlhm0skwx9m0dx84dv04m9z3di7b9x4kn")))

(define-public crate-handy-0.1.3 (c (n "handy") (v "0.1.3") (h "18b1p6qmg23rk006i6zd7fzvfga28qv2cy3aq5agvdcw2rllpwqn")))

(define-public crate-handy-0.1.4 (c (n "handy") (v "0.1.4") (h "023fm1j529kqhfwrhmsiy0c99d1ncq62b3g41rkx149lm2sxpdjq")))

