(define-module (crates-io ha nd handyman) #:use-module (crates-io))

(define-public crate-handyman-0.1.0 (c (n "handyman") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1m52kycskwdpsq91v1v9xx8hwqqha0xppzfk4wpd9bsi6brda9k8")))

