(define-module (crates-io ha nd handtrack-rs) #:use-module (crates-io))

(define-public crate-handtrack-rs-0.1.0 (c (n "handtrack-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "downloader") (r "^0.2.6") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "serial_test") (r "^0.10.0") (d #t) (k 0)) (d (n "tensorflow") (r "^0.19.1") (d #t) (k 0)))) (h "0lp5lvcyk9sdnw1im4gvr27mq10cczw057ykkx2c5sn8g224p12d")))

