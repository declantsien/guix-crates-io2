(define-module (crates-io ha nd handshake) #:use-module (crates-io))

(define-public crate-handshake-0.1.0 (c (n "handshake") (v "0.1.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10") (d #t) (k 0)) (d (n "hkdf") (r "^0.12") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "snow") (r "^0.9") (d #t) (k 2)) (d (n "x25519-dalek") (r "^1.1") (d #t) (k 0)))) (h "1i09b0dxvnflijxlakgkqw4pm89hwivfx6cfnzpvnim3c2i4fhkd") (y #t)))

(define-public crate-handshake-0.1.1 (c (n "handshake") (v "0.1.1") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10") (d #t) (k 0)) (d (n "hkdf") (r "^0.12") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "snow") (r "^0.9") (d #t) (k 2)) (d (n "x25519-dalek") (r "^1.1") (d #t) (k 0)))) (h "0if8g46ajxr5jn9x60qw062bsn2i1c2c3ip0qd36g3h7i3nj95pc") (y #t)))

(define-public crate-handshake-0.1.2 (c (n "handshake") (v "0.1.2") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10") (d #t) (k 0)) (d (n "hkdf") (r "^0.12") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "snow") (r "^0.9") (d #t) (k 2)) (d (n "x25519-dalek") (r "^1.1") (d #t) (k 0)))) (h "06j4m2f75pjr74rzy20qz30im7dm1hi5jjwbp2i1mync5q9j00wf") (y #t)))

(define-public crate-handshake-0.1.3 (c (n "handshake") (v "0.1.3") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10") (d #t) (k 0)) (d (n "hkdf") (r "^0.12") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "snow") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.1") (d #t) (k 0)))) (h "0n2dkywwc94pnzkzkgwma5n57d0kngm19d2c2d25fybb20xqhgfy") (y #t)))

(define-public crate-handshake-0.1.4 (c (n "handshake") (v "0.1.4") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10") (d #t) (k 0)) (d (n "hkdf") (r "^0.12") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "snow") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1.1") (d #t) (k 0)))) (h "0jq3hb2a2yf66jmrjvypwzd55r6kl00p15wlnm99gn97qckbk75l") (y #t)))

