(define-module (crates-io ha nd handlebars-template-handler) #:use-module (crates-io))

(define-public crate-handlebars-template-handler-0.1.0 (c (n "handlebars-template-handler") (v "0.1.0") (d (list (d (n "handlebars-iron") (r "^0.17.0") (f (quote ("serde_type"))) (d #t) (k 0)) (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "params") (r "^0.3.0") (d #t) (k 0)) (d (n "router") (r "^0.2.0") (d #t) (k 2)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "1w79vv1wsvcw9r1li7x2y7wbff8bjcsi8ma9wwjhj799frinr7yd")))

