(define-module (crates-io ha nd handlebars_sprig) #:use-module (crates-io))

(define-public crate-handlebars_sprig-0.1.0 (c (n "handlebars_sprig") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.1") (f (quote ("dir_source" "script_helper"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r8fj1f39qd1szdd4arnvlfva71rj6f30zrl27fzz3y906wys432")))

(define-public crate-handlebars_sprig-0.2.0 (c (n "handlebars_sprig") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.1") (f (quote ("dir_source" "script_helper"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r69il22bjn4jx41yb0bvrc8z0pk66k1rq8b33zyzjq6nnkjyf1a")))

(define-public crate-handlebars_sprig-0.3.0 (c (n "handlebars_sprig") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.1") (f (quote ("dir_source" "script_helper"))) (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasi-experimental-http") (r "^0.7.0") (d #t) (k 0)))) (h "1skq7p0r0qzlpv3f60lv71wx5l1zg70i0wmj0bzv52p4f2cqjv1k")))

