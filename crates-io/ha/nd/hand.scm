(define-module (crates-io ha nd hand) #:use-module (crates-io))

(define-public crate-hand-0.1.0 (c (n "hand") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0510zcyb42jsbj2nmd0mz1cqhi4svbslhyrayq2m75xqjp3m9yxx")))

(define-public crate-hand-0.1.1 (c (n "hand") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1y3g91gqh2hkpnjl8jxclzhvzq9hkj7lvv4w997f5m8pmf3gj38h")))

