(define-module (crates-io ha nd handleengine) #:use-module (crates-io))

(define-public crate-HandleEngine-0.1.0 (c (n "HandleEngine") (v "0.1.0") (h "1h3ybbhp6nmsvnch3fxs0z92glysbnfxaw6snlhbs1h479929d63") (y #t)))

(define-public crate-HandleEngine-0.1.1 (c (n "HandleEngine") (v "0.1.1") (h "1dbwwync0fbzj820x0yrngpygl0g8dqgxrzh3qvj9fqyz828gz3j") (y #t)))

(define-public crate-HandleEngine-0.1.2 (c (n "HandleEngine") (v "0.1.2") (h "0jz908cb81p57vdvr39yxmlgfhfdlr8g2d0vd2pg6xk7wh51pwqg") (y #t)))

(define-public crate-HandleEngine-0.1.3 (c (n "HandleEngine") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0mdqjlk9wcbzl0wvi2px2d6a3g8g2gdaqfg1klm9gqxm62vyi1pv") (y #t)))

(define-public crate-HandleEngine-0.1.4 (c (n "HandleEngine") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "122nmi9q5fzbrbpvfj1v32qqb9rka2ynylggdkz469d4c7g19yls") (y #t)))

(define-public crate-HandleEngine-0.1.5 (c (n "HandleEngine") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "19vp9b8r606sm29xkhj2a0h13ydisa9v25v12hgvrw00y4i97g0m") (y #t)))

(define-public crate-HandleEngine-0.1.6 (c (n "HandleEngine") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1wpmqqc80w529d1lcijxgqihyd86q9yb81lg66g8zahdsr050wpv") (y #t)))

(define-public crate-HandleEngine-0.1.7 (c (n "HandleEngine") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0z1r577gw4kn03wbdsp2006qnj61nc5vhn50w73c7rizbi2b9qky") (y #t)))

