(define-module (crates-io ha nd handoff_counter) #:use-module (crates-io))

(define-public crate-handoff_counter-0.8.4 (c (n "handoff_counter") (v "0.8.4") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0gavwz3vrz6q1ay215isp9c3nll6c1kvbp5nqn8cqd1szi0daj6h")))

(define-public crate-handoff_counter-0.8.5 (c (n "handoff_counter") (v "0.8.5") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "14p0bxkqlfyhkzjvpqhqp5vj3r9ki1dlqxp27grgxl4ry26k3kfn")))

