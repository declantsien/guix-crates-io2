(define-module (crates-io ha nd handmade) #:use-module (crates-io))

(define-public crate-handmade-0.1.0 (c (n "handmade") (v "0.1.0") (d (list (d (n "image") (r "^0.23.10") (d #t) (k 0)))) (h "1kk66r67yjbckygzw95kwvd71x4vrhs2d9j5gkwnpvlh68kw23r8")))

(define-public crate-handmade-0.1.1 (c (n "handmade") (v "0.1.1") (d (list (d (n "image") (r "^0.23.10") (d #t) (k 0)))) (h "1g9qd2zr4afjr2jwazgjdx8hbkhx90l6a4k5sjfz0j0sg08zgy39")))

(define-public crate-handmade-0.1.2 (c (n "handmade") (v "0.1.2") (d (list (d (n "image") (r "^0.23.10") (d #t) (k 0)))) (h "0vw1a96ws49ds3c5k625wf7244xs4z5x9788lc7szb9cav7fn8mq")))

(define-public crate-handmade-0.1.3 (c (n "handmade") (v "0.1.3") (d (list (d (n "image") (r "^0.23.10") (d #t) (k 0)))) (h "0alyq1xka1rw7ggpxbscsxwclr14ljaxqz2j524shr7r5i5p9bg6")))

(define-public crate-handmade-0.1.4 (c (n "handmade") (v "0.1.4") (d (list (d (n "image") (r "^0.23.10") (d #t) (k 0)))) (h "008jkid1f0ychdxnj859j55v9lyazigbif5jm5wyfpkbc0xji754")))

