(define-module (crates-io ha nd handlebars-chrono) #:use-module (crates-io))

(define-public crate-handlebars-chrono-0.1.0 (c (n "handlebars-chrono") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "handlebars") (r "^5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09l1sh43p4c3hlgsa001fc4yiwmqj2kzj5xamyqbxkh2aqr0m37z") (f (quote (("locale" "chrono/unstable-locales")))) (s 2) (e (quote (("timezone" "dep:chrono-tz"))))))

(define-public crate-handlebars-chrono-0.1.1 (c (n "handlebars-chrono") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "handlebars") (r "^5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0z2iw5jnbvhs7kfhaapc6vaqaxp4vkb220adn1rsl3qk0d2mfyvw") (f (quote (("locale" "chrono/unstable-locales")))) (s 2) (e (quote (("timezone" "dep:chrono-tz"))))))

