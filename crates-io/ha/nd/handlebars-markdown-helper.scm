(define-module (crates-io ha nd handlebars-markdown-helper) #:use-module (crates-io))

(define-public crate-handlebars-markdown-helper-0.1.0 (c (n "handlebars-markdown-helper") (v "0.1.0") (d (list (d (n "handlebars") (r "^0.11.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)))) (h "10n8lznq8fb29c9adjvmmkyipyz2k78xqkc3qdk2rjdbjfhic9sf")))

(define-public crate-handlebars-markdown-helper-0.1.1 (c (n "handlebars-markdown-helper") (v "0.1.1") (d (list (d (n "handlebars") (r "^0.11.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)))) (h "0wbc7bqr884q8s0b96vrz0v8hy344hdi26ib2k3w9szpbc4zlsnx")))

(define-public crate-handlebars-markdown-helper-0.1.2 (c (n "handlebars-markdown-helper") (v "0.1.2") (d (list (d (n "handlebars") (r "^0.11.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)))) (h "1f07vfxs7axs0rkz8ibyx86gy1nrzxizxk2hzjx3hfk7vg7zi0fw")))

(define-public crate-handlebars-markdown-helper-0.1.3 (c (n "handlebars-markdown-helper") (v "0.1.3") (d (list (d (n "handlebars") (r "^0.11.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)))) (h "0vpm002r9p0rvhlpnslaswl0123z9zm9fkak1zx8hls1gj3dsmcl")))

(define-public crate-handlebars-markdown-helper-0.2.0 (c (n "handlebars-markdown-helper") (v "0.2.0") (d (list (d (n "handlebars") (r "^0.17.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)))) (h "1c27c3ij41dvrakvnv9w401n9bi9b647fpn08gpw3l27ap00ajnx")))

(define-public crate-handlebars-markdown-helper-0.3.0 (c (n "handlebars-markdown-helper") (v "0.3.0") (d (list (d (n "handlebars") (r "^0.20.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)))) (h "09sv19bc9m9hph8n4d2l40kgpdxmqs6anb652k7kczx7cw1g411n")))

(define-public crate-handlebars-markdown-helper-0.4.0 (c (n "handlebars-markdown-helper") (v "0.4.0") (d (list (d (n "handlebars") (r "^0.22.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)))) (h "17c4q2vhi7yjh04pcrzgsmw30axbj6zj9zwd55han2zsax6hsbv2")))

(define-public crate-handlebars-markdown-helper-0.5.0 (c (n "handlebars-markdown-helper") (v "0.5.0") (d (list (d (n "handlebars") (r "^0.23.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)))) (h "15cw8w19waz9idva7bm1qvx8gcmhviqh75j89rjzvpjx0y4iwmrd")))

(define-public crate-handlebars-markdown-helper-0.6.0 (c (n "handlebars-markdown-helper") (v "0.6.0") (d (list (d (n "handlebars") (r "^0.26.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)))) (h "0fpnjrx74nrp167r27143f7ksfsnwf3khwg9r8vgi9av1kva6rmj")))

(define-public crate-handlebars-markdown-helper-0.6.1 (c (n "handlebars-markdown-helper") (v "0.6.1") (d (list (d (n "handlebars") (r "^0.26.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.14") (d #t) (k 0)))) (h "1p2yp6lg9rxpg5wz1xrvaw4vjr9cnrix1gc86xzl3ydmw71fjasw")))

(define-public crate-handlebars-markdown-helper-0.7.0 (c (n "handlebars-markdown-helper") (v "0.7.0") (d (list (d (n "handlebars") (r "^1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.2") (d #t) (k 0)))) (h "1g45af8r5n1a4byznjbwa134f9p9ix56k7jpdbm8sx5zqhxpzlnr")))

