(define-module (crates-io ha nd handwritten-json) #:use-module (crates-io))

(define-public crate-handwritten-json-0.1.0 (c (n "handwritten-json") (v "0.1.0") (d (list (d (n "property") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vdzk05r2hmffvn78fbsddi708ibrgx5l6fb17kci8gk926mvc8x")))

