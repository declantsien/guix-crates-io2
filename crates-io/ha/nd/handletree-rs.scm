(define-module (crates-io ha nd handletree-rs) #:use-module (crates-io))

(define-public crate-handletree-rs-0.1.0 (c (n "handletree-rs") (v "0.1.0") (h "17p7n8awqhai1fmx5fbhzqf52n17wh0lni99mv27j6iv92ly1zaq")))

(define-public crate-handletree-rs-0.2.0 (c (n "handletree-rs") (v "0.2.0") (h "1fksgy7z3hvlp7qbcm8x42wwm4vv4nh35a4yh8fl6agkwi143b4b")))

