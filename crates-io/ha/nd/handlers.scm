(define-module (crates-io ha nd handlers) #:use-module (crates-io))

(define-public crate-handlers-0.1.0 (c (n "handlers") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)))) (h "1ghzpbmjyrq62qs215swm5c8wzhng63076dkiwn5qvkx3xb2j61z")))

(define-public crate-handlers-0.2.0 (c (n "handlers") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)))) (h "05vm4x1rgpbckdvpnixiqi3zwin2cvhdjkv1xkplwyxxyqqf3cyy")))

(define-public crate-handlers-0.3.0 (c (n "handlers") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)))) (h "1m7rk7is1pwszn5w1x305m8vjfb8zj7npmk6bdqsxb847s7rjmyl")))

(define-public crate-handlers-0.4.0 (c (n "handlers") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)))) (h "1jrpp3a7f6ac8z4bvr2b4g4yncxafwnxgp97g1jy490i09q7d1i5")))

(define-public crate-handlers-0.5.0 (c (n "handlers") (v "0.5.0") (d (list (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)))) (h "1l3gk3l72ggc12zlbanm5a2arp6yh9mm2ngx5vzjs95gjniqv9nw")))

(define-public crate-handlers-0.6.0 (c (n "handlers") (v "0.6.0") (d (list (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)))) (h "1gfghy1sw69b985hkxqxaz70pbi03688nfswg3kvr5p4v26qy2y4")))

(define-public crate-handlers-0.7.0 (c (n "handlers") (v "0.7.0") (d (list (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)))) (h "05wnvm4zhggm1b6c67nsxfslya81vqiz9d7nia25rv5lpzb05lrx")))

(define-public crate-handlers-0.7.1 (c (n "handlers") (v "0.7.1") (d (list (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)))) (h "0yzc11can9041wkg8wahdrzbf748y4q2bjcvrmhzhmbdr3niy0hm")))

(define-public crate-handlers-0.8.0 (c (n "handlers") (v "0.8.0") (d (list (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)))) (h "10kc65z1rwq1wfzczpbcac2rx1zkkaczra5zn9zri331sp6y7ydj")))

(define-public crate-handlers-0.9.0 (c (n "handlers") (v "0.9.0") (d (list (d (n "interpolate_idents") (r "^0.0.10") (d #t) (k 0)))) (h "015whnx97nx7rli0n7032gmds1kqvgvf5wsxd7gawcz6b36mrgx3")))

(define-public crate-handlers-0.10.0 (c (n "handlers") (v "0.10.0") (d (list (d (n "paste") (r "^0.1.3") (d #t) (k 0)))) (h "1lp1ifyi7x8sklnb64xwynxp9k1r3ds6910h1ysgx4afpcqr1scr")))

