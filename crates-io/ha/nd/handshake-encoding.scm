(define-module (crates-io ha nd handshake-encoding) #:use-module (crates-io))

(define-public crate-handshake-encoding-0.1.1 (c (n "handshake-encoding") (v "0.1.1") (d (list (d (n "encodings") (r "^0.1.0") (d #t) (k 0)) (d (n "extended-primitives") (r "^0.3.1") (d #t) (k 0)))) (h "1vkkg2g54rrfgq09dqixir1j07fnnlgcdvy7i89pp18ipx1l3ai0")))

