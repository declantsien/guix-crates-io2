(define-module (crates-io ha nd handle) #:use-module (crates-io))

(define-public crate-handle-0.1.0 (c (n "handle") (v "0.1.0") (h "1iapy7aa8g31daljyl9c86m9s6rvvnpm9rbyssjrr7syzzdcfsp4")))

(define-public crate-handle-0.2.0 (c (n "handle") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0hq1cf5mj0fd1r3v8rh3q51fa2fkak3jksz6jphkznkyngjx7n2v")))

(define-public crate-handle-0.2.1 (c (n "handle") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "060plxdx8wmh9jgn1b6q043acgi4a4afcniskp16pzv980maxn9p")))

(define-public crate-handle-0.2.2 (c (n "handle") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "09zydn10l61cbrwcbxwp4ls2lbbpbvs2mg49fhxc459nqmkdcy66")))

(define-public crate-handle-0.2.3 (c (n "handle") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0616qmsx5r7irp1qmx4i4gknz2fbgj6irp2hrxxslbg9kv8ylsgv")))

(define-public crate-handle-0.2.4 (c (n "handle") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "1xviwm8vy9r06cs74pgbfqqggwhz6ywwnpy94vvdxn7cvgynmwzh")))

(define-public crate-handle-0.2.5 (c (n "handle") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "1qqi10rhz5jh674kfxryjfvrkpvdyfnghz53xfy8xilfrpxb7ncq")))

(define-public crate-handle-1.0.0 (c (n "handle") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "1y8i4r2p7r35g9ljicxf9l7lhlj9f7f0ig0za2l8ry7dzivbmmhp")))

(define-public crate-handle-1.0.1 (c (n "handle") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "04lga3b5j4c9yhxk7yfrha66l4n0ncz4r8aa2qraw605xj3vf5ap")))

(define-public crate-handle-1.0.2 (c (n "handle") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0ilfzwjrr5j24b724p1ksgi5mwx3rfzhffaa12k17ma14qap5vaj")))

