(define-module (crates-io ha nd handy_io) #:use-module (crates-io))

(define-public crate-handy_io-0.1.0 (c (n "handy_io") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1b78rfa73rafqllm7q7np6gmz1rnfas2h618n8bddy8j9flbk7r0")))

(define-public crate-handy_io-0.1.1 (c (n "handy_io") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "106igmwj6s4l9xx482z5bff4gp1nln34c6gqdq9ywyaymif4yxwp")))

(define-public crate-handy_io-0.1.2 (c (n "handy_io") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "110jazp2dccybnis1qjjwdq8mgk7rzx2pm3xn2adfgfl19jp994i")))

