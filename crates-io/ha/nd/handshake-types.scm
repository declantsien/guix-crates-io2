(define-module (crates-io ha nd handshake-types) #:use-module (crates-io))

(define-public crate-handshake-types-0.1.1 (c (n "handshake-types") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.1.1") (d #t) (k 0)) (d (n "encodings") (r "^0.1.0") (d #t) (k 0)) (d (n "extended-primitives") (r "^0.3.1") (d #t) (k 0)) (d (n "handshake-encoding") (r "^0.1.1") (d #t) (k 0)) (d (n "pds") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0505dai48g9k0znagvq33q2m84hpb18f9j21rfjyxciag774c9i7") (f (quote (("default") ("bloom" "pds"))))))

