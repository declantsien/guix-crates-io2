(define-module (crates-io ha nd handlebars-concat) #:use-module (crates-io))

(define-public crate-handlebars-concat-0.1.0 (c (n "handlebars-concat") (v "0.1.0") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "11lgn23la7ai14ka9c3kajdczb74ssmxjv8p6yx4v7yjzrqinrm2") (r "1.72.1")))

(define-public crate-handlebars-concat-0.1.1 (c (n "handlebars-concat") (v "0.1.1") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18g7xsc5wny5wnpjkar96p3q24nr7bv8l9jw0kysjkzp33s0bsld") (r "1.72.1")))

(define-public crate-handlebars-concat-0.1.2 (c (n "handlebars-concat") (v "0.1.2") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0admcbvhlwz09ipzjjxlipbaq7lqvz52hsbskbsdp997xnr4yzsd")))

(define-public crate-handlebars-concat-0.1.3 (c (n "handlebars-concat") (v "0.1.3") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0mw7xh84lijl5xd7adlcx9fy1xc5jvr8rnz8a02mldsj3jwf9fwl")))

(define-public crate-handlebars-concat-0.1.4 (c (n "handlebars-concat") (v "0.1.4") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ccmd0zjqi27l8l5p73m7sfgslka3pxjisdriqy8izwjr36mmlg5")))

(define-public crate-handlebars-concat-0.2.0 (c (n "handlebars-concat") (v "0.2.0") (d (list (d (n "handlebars") (r "^5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1m49xaq2wfpsl87z5k2w2sb1vsfiy22px41s1wr2v4psbz4zgqxd")))

(define-public crate-handlebars-concat-0.2.1 (c (n "handlebars-concat") (v "0.2.1") (d (list (d (n "handlebars") (r "^5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "053z5lfqv0pp26v3z1m7q4h6ffxasjp295zc2hdp1hmqhvq0mifc")))

(define-public crate-handlebars-concat-0.1.5 (c (n "handlebars-concat") (v "0.1.5") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0vlsi6m1pc06ln6d9y2c8fy0fx44v11fyi35wwl7dpr121vlgvz7")))

