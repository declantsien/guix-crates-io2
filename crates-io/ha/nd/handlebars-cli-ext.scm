(define-module (crates-io ha nd handlebars-cli-ext) #:use-module (crates-io))

(define-public crate-handlebars-cli-ext-0.0.0 (c (n "handlebars-cli-ext") (v "0.0.0") (d (list (d (n "execute") (r "^0.2.9") (d #t) (k 0)) (d (n "handlebars") (r "^4.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)))) (h "0nqdax67wxa1wvllw5ldiq9vc3dgzkh8q6r54573xvadas0fwkr8")))

