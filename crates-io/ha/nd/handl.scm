(define-module (crates-io ha nd handl) #:use-module (crates-io))

(define-public crate-handl-0.1.0 (c (n "handl") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.11") (f (quote ("js"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("macro-diagnostics" "v4" "fast-rng"))) (d #t) (k 0)))) (h "0c6qjrhlyjyhs4hjvf7g250p94qfawa4as1k7yc0cdnf16cp41yz") (y #t)))

(define-public crate-handl-0.1.1 (c (n "handl") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.2.11") (f (quote ("js"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("macro-diagnostics" "v4" "fast-rng"))) (d #t) (k 0)))) (h "0awkr7bljfv5l4fvghhky0zsas4x09axzlxvyz7rvvcx6756igp6") (y #t)))

(define-public crate-handl-0.1.10 (c (n "handl") (v "0.1.10") (d (list (d (n "getrandom") (r "^0.2.11") (f (quote ("js"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("macro-diagnostics" "v4" "fast-rng"))) (d #t) (k 0)))) (h "0gcv7mh3v57p4nandizvm602sxhx4cw4g4l8bkkfrihvbngiaijw") (y #t)))

