(define-module (crates-io ha nd handlebars_switch) #:use-module (crates-io))

(define-public crate-handlebars_switch-0.1.0 (c (n "handlebars_switch") (v "0.1.0") (d (list (d (n "handlebars") (r "^0.29") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fqdkkqr999cahyivj8z966qwrbzy5n6qysn5kag6l9grjkkzxzd")))

(define-public crate-handlebars_switch-0.2.0 (c (n "handlebars_switch") (v "0.2.0") (d (list (d (n "handlebars") (r "^0.31") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "105d8a39ywzvnrbv6cawwjpci8w0aqgz06h67nwrs3lja4lhm1p8")))

(define-public crate-handlebars_switch-0.3.0 (c (n "handlebars_switch") (v "0.3.0") (d (list (d (n "handlebars") (r "^0.32") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s64d3szgnid0kkv95hd3i1mvs9k2w10zm91fm7934kgz6kfqhkc")))

(define-public crate-handlebars_switch-0.4.0 (c (n "handlebars_switch") (v "0.4.0") (d (list (d (n "handlebars") (r "^3.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y55farvby9l3pd1ivrz7ii7w98k7k8dz1b6alv1njvmrqv87njn")))

(define-public crate-handlebars_switch-0.5.0 (c (n "handlebars_switch") (v "0.5.0") (d (list (d (n "handlebars") (r "^4.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k3y5wj5z34nxc2sfh9d50xqdlqswmsyzb1j17nh3wrml9qmdqsp")))

(define-public crate-handlebars_switch-0.6.0 (c (n "handlebars_switch") (v "0.6.0") (d (list (d (n "handlebars") (r "^5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vvaggvhba4kc0n5b3ifzq3k5hhkpy4bw08gpsxl62i9bj6y0330")))

