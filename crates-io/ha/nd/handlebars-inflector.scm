(define-module (crates-io ha nd handlebars-inflector) #:use-module (crates-io))

(define-public crate-handlebars-inflector-0.1.0 (c (n "handlebars-inflector") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "handlebars") (r "^4") (d #t) (k 0)))) (h "1608bkxwmi4h12jzgi20d2ns6i0g73ri2dg4isalgvpivqx0pfbp") (r "1.72.1")))

(define-public crate-handlebars-inflector-0.1.1 (c (n "handlebars-inflector") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "handlebars") (r "^4") (d #t) (k 0)))) (h "0g0jg63z71pzr3z4n13y85mkyirzdfshmcl2jwkgf3lf0cwyjln6") (r "1.72.1")))

(define-public crate-handlebars-inflector-0.1.2 (c (n "handlebars-inflector") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "handlebars") (r "^4") (d #t) (k 0)))) (h "13a3mr2r614fclp0gq15a6gimy7z2l31szv8zbbn4l49cp8pqj2h")))

(define-public crate-handlebars-inflector-0.2.0 (c (n "handlebars-inflector") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "handlebars") (r "^5") (d #t) (k 0)))) (h "0zmkb7gs2ny9b44y0flizllwd2blx494jr8wgpw16q7qpi4ysypi")))

