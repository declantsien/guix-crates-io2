(define-module (crates-io ha nd handlebars-repeat) #:use-module (crates-io))

(define-public crate-handlebars-repeat-0.1.0 (c (n "handlebars-repeat") (v "0.1.0") (d (list (d (n "handlebars") (r "^4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jb84a2k9rs4gcfh7i62gsn7nvrkr5nan3jvz7nwz54fna4kagbn")))

