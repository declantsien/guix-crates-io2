(define-module (crates-io ha nd handlebox) #:use-module (crates-io))

(define-public crate-handlebox-0.2.0 (c (n "handlebox") (v "0.2.0") (h "0vgv5xik15c6aywb1jvscrywr7wmzn5x8fsq00b60i11fji267ab")))

(define-public crate-handlebox-0.3.0 (c (n "handlebox") (v "0.3.0") (h "1bm0qkv6nnzr1mklnyyxyi69yw4ssmfjgw8l9093iirnfrm169j7")))

