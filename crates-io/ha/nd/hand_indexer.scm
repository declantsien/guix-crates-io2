(define-module (crates-io ha nd hand_indexer) #:use-module (crates-io))

(define-public crate-hand_indexer-0.1.0 (c (n "hand_indexer") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1hdkfhnvirzpfpsax6ka6aam8z3bxy57gwkq3bhq9dz1q73g8mnw")))

(define-public crate-hand_indexer-0.1.1 (c (n "hand_indexer") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "14pjzcbl98ak5y4cmn4qnp85i4gb623gpwra0czip104fa84w7fp")))

(define-public crate-hand_indexer-0.1.2 (c (n "hand_indexer") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0nzkjmbs5lfmwffjsra8j854l0ws3xxpa6j0mk88nqw15packrmi")))

