(define-module (crates-io ha nd handle-error) #:use-module (crates-io))

(define-public crate-handle-error-0.1.0 (c (n "handle-error") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)))) (h "1aq80bphn26w94xj4y1x7pjh292xdgm6hkc9aiadr022k58pkxsr")))

(define-public crate-handle-error-0.1.1 (c (n "handle-error") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)))) (h "1al24rbky5hxf7zv7h93qx488ffawwfniky592s14vjx69b22n51")))

(define-public crate-handle-error-0.1.2 (c (n "handle-error") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)))) (h "10www560av8k80jvx4iym3n1l3yhz1xk9kx4j5fzxif3b9lm8c3b")))

