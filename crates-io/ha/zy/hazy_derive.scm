(define-module (crates-io ha zy hazy_derive) #:use-module (crates-io))

(define-public crate-hazy_derive-0.1.0 (c (n "hazy_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0wrhq24d71yzbgg9g72vanj1zw895d9wy289a4g5nx6klbb15dyp")))

(define-public crate-hazy_derive-0.1.1 (c (n "hazy_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0v9w2l7q0vnif5r9a0il8g0a55zpa6gn45g7wr1v63qql4nn3kry")))

