(define-module (crates-io ha zy hazy) #:use-module (crates-io))

(define-public crate-hazy-0.1.0 (c (n "hazy") (v "0.1.0") (h "1dpq1jcz68lvj2ng812x9dmw251wgxjsxncas37zadqszpjwqvnc")))

(define-public crate-hazy-0.1.1 (c (n "hazy") (v "0.1.1") (d (list (d (n "hazy_derive") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "089fwgw8jcb548bvx28kw84a8xvmhwvsk7m57r1bfpvadnw3r3ha")))

