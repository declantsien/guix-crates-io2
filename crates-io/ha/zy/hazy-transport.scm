(define-module (crates-io ha zy hazy-transport) #:use-module (crates-io))

(define-public crate-hazy-transport-0.0.1 (c (n "hazy-transport") (v "0.0.1") (d (list (d (n "datagram-transport") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0gvr6lmzw1b635gz4f329sw6hnlg26c8mbbd4j53xjj7a2j5fv51")))

