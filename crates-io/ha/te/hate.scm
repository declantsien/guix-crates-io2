(define-module (crates-io ha te hate) #:use-module (crates-io))

(define-public crate-hate-0.1.0 (c (n "hate") (v "0.1.0") (d (list (d (n "android_glue") (r "^0.2") (d #t) (t "arm-linux-androideabi") (k 0)) (d (n "cgmath") (r "^0.14") (d #t) (k 0)) (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.16") (d #t) (k 0)) (d (n "glutin") (r "^0.8") (d #t) (k 0)) (d (n "png") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "04463azv40xslykngmdkd0xah4wiif74wf07avqi1didkb7brfcy")))

