(define-module (crates-io ha tm hatmel) #:use-module (crates-io))

(define-public crate-hatmel-0.1.0 (c (n "hatmel") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cssparser") (r "^0.32") (d #t) (k 0)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "string_cache") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1554xa21x8s5pb8j7cfdsjiyszxbam4bgjdisfj1lrn2iwa28zfx")))

(define-public crate-hatmel-0.2.0 (c (n "hatmel") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cssparser") (r "^0.32") (d #t) (k 0)) (d (n "fashion") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "string_cache") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "047pcypwd6v13npzp9i7jlh5306w1r43q88l6ziky5z09xih7vgs") (f (quote (("default" "fashion"))))))

