(define-module (crates-io ha ox haoxue-dict) #:use-module (crates-io))

(define-public crate-haoxue-dict-0.1.0 (c (n "haoxue-dict") (v "0.1.0") (d (list (d (n "cedict") (r "^0.3.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "prettify_pinyin") (r "^2.0.0") (d #t) (k 0)))) (h "11fbi1cfsbljqwa0gnkyl9q19wc87k6p49g605d5ax1vvancks52")))

(define-public crate-haoxue-dict-0.1.1 (c (n "haoxue-dict") (v "0.1.1") (d (list (d (n "cedict") (r "^0.3.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettify_pinyin") (r "^2.0.0") (d #t) (k 0)))) (h "1xyv0l8l599rk69dhblx6pbp34k3vckx3nayn3wzj4lghah63kmm")))

(define-public crate-haoxue-dict-0.1.2 (c (n "haoxue-dict") (v "0.1.2") (d (list (d (n "cedict") (r "^0.3.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettify_pinyin") (r "^2.0.0") (d #t) (k 0)))) (h "16x6x07b6wy8mqbkylq20plqg3kll97cb7nwbh0h297w2nzndr25")))

(define-public crate-haoxue-dict-0.1.3 (c (n "haoxue-dict") (v "0.1.3") (d (list (d (n "cedict") (r "^0.3.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettify_pinyin") (r "^2.0.0") (d #t) (k 0)))) (h "1dnz4hg25f75xfb8ki8qhb18dhdl6cl52yiykhyjswbsaq677kmb")))

(define-public crate-haoxue-dict-0.1.4 (c (n "haoxue-dict") (v "0.1.4") (d (list (d (n "cedict") (r "^0.3.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettify_pinyin") (r "^2.0.0") (d #t) (k 0)))) (h "1ri4m49zg0h3wr7wzay1246n9s6zxqv8g1pw9p33c2jfzsigdidx")))

(define-public crate-haoxue-dict-0.1.5 (c (n "haoxue-dict") (v "0.1.5") (d (list (d (n "cedict") (r "^0.3.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettify_pinyin") (r "^2.0.0") (d #t) (k 0)))) (h "0m0a4iv2i410z0h108i440wbdjb1mipi0r8dbbsbal42wgrblxfv")))

