(define-module (crates-io ha mp hampel) #:use-module (crates-io))

(define-public crate-hampel-0.1.0 (c (n "hampel") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0bkggh7viq8g24bing445fq02abqqnjsyyhhai0ncf7s9k58205v")))

(define-public crate-hampel-0.1.1 (c (n "hampel") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1laryn2kx98acnnxshn83gbjq807kkbcy93a39lblxi38r9pmwp2")))

