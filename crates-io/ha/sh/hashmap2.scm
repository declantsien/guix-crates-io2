(define-module (crates-io ha sh hashmap2) #:use-module (crates-io))

(define-public crate-hashmap2-0.1.0 (c (n "hashmap2") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "15iri3sfwrq9s9xb5y6ibdpb86xb2bxgkma572l08vk43qswrsw8")))

(define-public crate-hashmap2-0.2.0 (c (n "hashmap2") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "03xzv1mrcqmczd64scwyydsxxcadkc6hq0d4lzycmw6vcx5l4vha")))

