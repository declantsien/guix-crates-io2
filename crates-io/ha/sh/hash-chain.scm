(define-module (crates-io ha sh hash-chain) #:use-module (crates-io))

(define-public crate-hash-chain-0.0.0 (c (n "hash-chain") (v "0.0.0") (h "0kxqmn2p987b5c1m6ndc8w9mzfvlcvjnx01746swwfpi3yfyyz1s")))

(define-public crate-hash-chain-0.1.0 (c (n "hash-chain") (v "0.1.0") (h "1hgdyvg2gmnf91bhrrslg3pzp06qx3f6zp9agphi4aq6xn6nc70n")))

(define-public crate-hash-chain-0.2.0 (c (n "hash-chain") (v "0.2.0") (h "192jy10s3pz4afvrnbxr0jjfwz9bj9jca1ph4m3hb9vpp82g6sqd")))

(define-public crate-hash-chain-0.2.1 (c (n "hash-chain") (v "0.2.1") (h "0b6vs0im345wq34zxg2nskz2s40q5nsh2fsxk8vjfq9s7ny8k6ls")))

(define-public crate-hash-chain-0.2.2 (c (n "hash-chain") (v "0.2.2") (h "0f0jwygm56km713c62qmdy8hrhc9vr8rdglzl03jshz8548pib46")))

(define-public crate-hash-chain-0.2.3 (c (n "hash-chain") (v "0.2.3") (h "0wwkp3wxsc6yf6igzppx9319qp8xfimb4zvmsjirpgha0mq6h953")))

(define-public crate-hash-chain-0.2.4 (c (n "hash-chain") (v "0.2.4") (h "1id40c9i673lg9vhrr7hyxic1kyxhkjx1gyyacn55y0g5h789adw")))

(define-public crate-hash-chain-0.2.5 (c (n "hash-chain") (v "0.2.5") (h "057djanzl4r9jfwqmrx54h5h792cx0xhvbg175j5m4dkq25gikh7")))

(define-public crate-hash-chain-0.2.6 (c (n "hash-chain") (v "0.2.6") (h "0w1hz1hi17x96kil869ns6gn8b68mm1dxaxsa7k84bknxjgrmy23") (y #t)))

(define-public crate-hash-chain-0.3.0 (c (n "hash-chain") (v "0.3.0") (h "13l2cgzpvj44740xmz6623p91i8hkc1hvws9d6a7smpwka6c61i1")))

(define-public crate-hash-chain-0.3.1 (c (n "hash-chain") (v "0.3.1") (h "0yhqvvd6hzplrdxbb98zapj471ifhm29bbzsy9yh1kyn4qlmnzyd")))

(define-public crate-hash-chain-0.3.2 (c (n "hash-chain") (v "0.3.2") (d (list (d (n "hashers") (r "^1") (d #t) (k 2)) (d (n "im-rc") (r "^14") (o #t) (d #t) (k 0)))) (h "1cl8x7p5mqywg07qkvl248yq1md7k4n68cg73gxy3f7grb3fkqaa") (f (quote (("imutable" "im-rc") ("default"))))))

