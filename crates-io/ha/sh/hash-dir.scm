(define-module (crates-io ha sh hash-dir) #:use-module (crates-io))

(define-public crate-hash-dir-0.1.0 (c (n "hash-dir") (v "0.1.0") (h "1275css4riqwg67sihxx6dcf6kwsgy5ka6x0cb4g73pzj7b5al8h") (y #t)))

(define-public crate-hash-dir-0.2.0 (c (n "hash-dir") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "13xs4gj4wwxnfhggpzyviyn12clylybx7q87296909bak47jns9r")))

(define-public crate-hash-dir-0.2.1 (c (n "hash-dir") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "09bl8wi4hv7kxafq5hbrpdnwqvb7aywyi7ypn0l0xgg744blssrq")))

