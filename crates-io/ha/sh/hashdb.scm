(define-module (crates-io ha sh hashdb) #:use-module (crates-io))

(define-public crate-hashdb-0.1.0 (c (n "hashdb") (v "0.1.0") (d (list (d (n "elastic-array") (r "^0.9") (d #t) (k 0)) (d (n "ethcore-bigint") (r "^0.1.3") (d #t) (k 0)))) (h "1bfnpgfgrvwkkrs51rn52w04ipnw85mmvyjccwi6a9ahm3nnkrdx")))

(define-public crate-hashdb-0.1.1 (c (n "hashdb") (v "0.1.1") (d (list (d (n "elastic-array") (r "^0.9") (d #t) (k 0)) (d (n "ethcore-bigint") (r "^0.2.1") (d #t) (k 0)))) (h "1x9w019057v38gbqrgflrxdgmi9480q61kmld094cnwc6myf0yyr")))

(define-public crate-hashdb-0.1.2 (c (n "hashdb") (v "0.1.2") (d (list (d (n "elastic-array") (r "^0.10") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.3") (d #t) (k 0)))) (h "1dynly99hrqp0cnv6sv8dfyahxnnklyz1px2pxl8fxf7sz34q2v8")))

(define-public crate-hashdb-0.2.0 (c (n "hashdb") (v "0.2.0") (d (list (d (n "elastic-array") (r "^0.10") (d #t) (k 0)) (d (n "heapsize") (r "^0.4") (d #t) (k 0)))) (h "1wz8gwddf1jppzry13wywppanldhiizrllv1wyrad4n50nsdv40w")))

(define-public crate-hashdb-0.2.1 (c (n "hashdb") (v "0.2.1") (d (list (d (n "elastic-array") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1cfxlldszgbr291ryqbcx4r2limgxh7qiwnm8lrrps6dfz2izizi") (f (quote (("std" "elastic-array") ("default" "std"))))))

(define-public crate-hashdb-0.3.0-beta.0 (c (n "hashdb") (v "0.3.0-beta.0") (h "1ckzpkkw9k8x6c87c6725ar62ynzhpnx62s6b5nmqfxlmfns67w0") (f (quote (("std") ("default" "std"))))))

(define-public crate-hashdb-0.3.0 (c (n "hashdb") (v "0.3.0") (h "1ir8nq55xvglayd5xqhspll74ykv575hd0zjgnn4dl3d6gp624nr") (f (quote (("std") ("default" "std"))))))

