(define-module (crates-io ha sh hash_cons) #:use-module (crates-io))

(define-public crate-hash_cons-0.1.0 (c (n "hash_cons") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0dsi2pixqd162qzsn70qcb2my2lmzwm306lwam31wvmw1rivk22x") (f (quote (("thread-safe") ("single-threaded") ("default" "single-threaded"))))))

(define-public crate-hash_cons-0.1.1 (c (n "hash_cons") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ddwvgzisxy0j14siiy6b74z2fbhp8rp2s3cfq18zlw8smizgl2l") (f (quote (("thread-safe") ("single-threaded") ("default" "single-threaded"))))))

(define-public crate-hash_cons-0.1.2 (c (n "hash_cons") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "12qp8rqghmfqgl94swprflzwkwcn16qqzx9nzkba70269qhcs8bd") (f (quote (("thread-safe") ("default" "auto-cleanup") ("auto-cleanup"))))))

(define-public crate-hash_cons-0.1.3 (c (n "hash_cons") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1lm9mlw75waxabmyqymhgdh3478r1khnkb9shq3bnp1dnm89y08j") (f (quote (("thread-safe") ("default" "auto-cleanup") ("auto-cleanup"))))))

(define-public crate-hash_cons-0.1.4 (c (n "hash_cons") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "15dmwcwffak7knv1scfiz9pnhch9sqn5bp0x50853ga80zn8waw8") (f (quote (("thread-safe") ("default" "auto-cleanup") ("auto-cleanup"))))))

