(define-module (crates-io ha sh hashy) #:use-module (crates-io))

(define-public crate-hashy-0.1.0 (c (n "hashy") (v "0.1.0") (d (list (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "0va443d3spaismisi3yd9i112g77jwxsamd3hmnwdjk727vwcjrc")))

(define-public crate-hashy-0.2.0 (c (n "hashy") (v "0.2.0") (d (list (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "1zdv6dv4xgdpkj9z3ijiqwyl7wv6k3q6qv725vd705xiiz8dmkr0")))

