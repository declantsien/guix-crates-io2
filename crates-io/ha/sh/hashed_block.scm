(define-module (crates-io ha sh hashed_block) #:use-module (crates-io))

(define-public crate-hashed_block-0.1.0 (c (n "hashed_block") (v "0.1.0") (h "1s37f1210whybfn818vzwicpmchfnbgqprhidg7wqham6l2n0idp")))

(define-public crate-hashed_block-0.1.1 (c (n "hashed_block") (v "0.1.1") (h "0lg9spvl5j5hca0b7sbxyvpz0sghlhgjw6fl05rhin4rifl7dihp")))

