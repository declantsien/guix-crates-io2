(define-module (crates-io ha sh hashheap) #:use-module (crates-io))

(define-public crate-hashheap-0.1.0 (c (n "hashheap") (v "0.1.0") (h "1iw3hnb6s7g6sv0p44ajgd91ss6pdv4lappxkzawkymd227ry6wf")))

(define-public crate-hashheap-0.1.1 (c (n "hashheap") (v "0.1.1") (h "1vff45344rgj37z1wgkbsj2qy30naqb02sx8pk8yv1bfc0qcnmgj") (y #t)))

(define-public crate-hashheap-0.1.2 (c (n "hashheap") (v "0.1.2") (h "06f7824a0x45grinzd0y1qwykncyzzf77s5b5llpj6n10gcdag99")))

