(define-module (crates-io ha sh hashlink) #:use-module (crates-io))

(define-public crate-hashlink-0.1.0 (c (n "hashlink") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.5.0") (d #t) (k 0)))) (h "15isnz174vvgw6qw2f9jqxbhx58dd6hq588nmxhbqwy2d9dc2zhc")))

(define-public crate-hashlink-0.2.0 (c (n "hashlink") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.5.0") (d #t) (k 0)))) (h "05jqwh583bc4ljvskpl99y91m9xs5krildvbkfajnmqwic4v8hhy")))

(define-public crate-hashlink-0.2.1 (c (n "hashlink") (v "0.2.1") (d (list (d (n "hashbrown") (r "^0.5.0") (d #t) (k 0)))) (h "0czngdn742lckjjb5b74apfam09vifsnnkznn377hm3rayry1kgk")))

(define-public crate-hashlink-0.3.0 (c (n "hashlink") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.5.0") (d #t) (k 0)))) (h "1m53ymvbg59mx5bsw05f1i9yj84zg9wa1fay4w5ifqjw1v7xajr6")))

(define-public crate-hashlink-0.4.0 (c (n "hashlink") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.6.0") (d #t) (k 0)))) (h "08bfcmn4hrwn9f2jjmwdbrrci678b1p47qgm8qv45hl77a2wqxa6")))

(define-public crate-hashlink-0.5.0 (c (n "hashlink") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.7.0") (d #t) (k 0)))) (h "1fpd7jd2c2r97qrhnr163r92ng7gf5ssyvn226415mgfq74xijsy")))

(define-public crate-hashlink-0.5.1 (c (n "hashlink") (v "0.5.1") (d (list (d (n "hashbrown") (r "^0.7.0") (d #t) (k 0)))) (h "1xq2ciny1cdq9b4xb2rnljpsm935fn4qwbs68998rikna550qp0c")))

(define-public crate-hashlink-0.6.0 (c (n "hashlink") (v "0.6.0") (d (list (d (n "hashbrown") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1a2gi4737lmqq1i48b9w13gvbkh4g3gc7gj6d3974hywy21gg76r") (f (quote (("serde_impl" "serde"))))))

(define-public crate-hashlink-0.7.0 (c (n "hashlink") (v "0.7.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1kzs54xq1g41zph39cfdfchiafij99382zw5fk6zq7xwkh9a6jbj") (f (quote (("serde_impl" "serde"))))))

(define-public crate-hashlink-0.8.0 (c (n "hashlink") (v "0.8.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "11ihliil4pmwy5raqymxcch2qi4fbmxxagd70bxwvzlkrdaw2lnl") (f (quote (("serde_impl" "serde"))))))

(define-public crate-hashlink-0.8.1 (c (n "hashlink") (v "0.8.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1yhgpv6k8pr7d3gp89gqcgxk2diai6gk4j05mmhdhy22ig7izzk9") (f (quote (("serde_impl" "serde"))))))

(define-public crate-hashlink-0.8.2 (c (n "hashlink") (v "0.8.2") (d (list (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1ap6ar5jlqq6ln7d9r2j5079mbx0zg8643xacqyjwkqw96ws2q87") (f (quote (("serde_impl" "serde"))))))

(define-public crate-hashlink-0.8.3 (c (n "hashlink") (v "0.8.3") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0vyjzyrqzy4rzj10s7dancg9zv9q64hpnjsgxzzqjxrdi9qncbri") (f (quote (("serde_impl" "serde"))))))

(define-public crate-hashlink-0.8.4 (c (n "hashlink") (v "0.8.4") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1xy8agkyp0llbqk9fcffc1xblayrrywlyrm2a7v93x8zygm4y2g8") (f (quote (("serde_impl" "serde"))))))

(define-public crate-hashlink-0.9.0 (c (n "hashlink") (v "0.9.0") (d (list (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1vi5nxldy84raw4jy0fq059xq7mnfha0y2gg7kfihxb0yzvslbk9") (f (quote (("serde_impl" "serde"))))))

(define-public crate-hashlink-0.9.1 (c (n "hashlink") (v "0.9.1") (d (list (d (n "hashbrown") (r "^0.14.3") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1byq4nyrflm5s6wdx5qwp96l1qbp2d0nljvrr5yqrsfy51qzz93b") (f (quote (("serde_impl" "serde"))))))

