(define-module (crates-io ha sh hash-rings) #:use-module (crates-io))

(define-public crate-hash-rings-0.1.0 (c (n "hash-rings") (v "0.1.0") (d (list (d (n "extended-collections") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1b94zrw5nl7spb6gamchz8a8hr87hl5lgk262hy69fsad47rh1ii")))

(define-public crate-hash-rings-0.2.0 (c (n "hash-rings") (v "0.2.0") (d (list (d (n "extended-collections") (r "^0.1") (d #t) (k 0)) (d (n "primal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "0sia43d3hdn6n9q7aqai6wbmymq655xypjad97rgjdw78qs8hbzr")))

(define-public crate-hash-rings-0.2.1 (c (n "hash-rings") (v "0.2.1") (d (list (d (n "extended-collections") (r "^0.1") (d #t) (k 0)) (d (n "primal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "1lsvndpr9mwhn8wzq7pl50pwnbzlc6ny3x3impg73szfwkxi7lly")))

(define-public crate-hash-rings-0.2.2 (c (n "hash-rings") (v "0.2.2") (d (list (d (n "primal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "0zvs933hakp8sry8isa68bks88caw4gb5wnvky40s86qj5a1z3rk") (y #t)))

(define-public crate-hash-rings-0.2.3 (c (n "hash-rings") (v "0.2.3") (d (list (d (n "primal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "04gnpjssxh82v5xp42dq9kdmfhslrszj593r5548rhz2nb7gx6q2")))

(define-public crate-hash-rings-0.3.0 (c (n "hash-rings") (v "0.3.0") (d (list (d (n "primal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "1wadn1khwzif82makvsqvj9brdp77bk4fyxdvy8nbisym02f8iza")))

(define-public crate-hash-rings-1.0.0 (c (n "hash-rings") (v "1.0.0") (d (list (d (n "primal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "00dggymr4xy5vj14zd0yd74yfdff33gsdw22j5r9qp2y26nv73qn")))

(define-public crate-hash-rings-1.1.0 (c (n "hash-rings") (v "1.1.0") (d (list (d (n "primal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "1jzwbl95xmbs2iqdppsq80mwzv8v27vvcij4apgkzl0w63cvr9ak")))

