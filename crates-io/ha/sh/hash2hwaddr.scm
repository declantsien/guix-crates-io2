(define-module (crates-io ha sh hash2hwaddr) #:use-module (crates-io))

(define-public crate-hash2hwaddr-0.0.0 (c (n "hash2hwaddr") (v "0.0.0") (d (list (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "0kzgpls7g7pf1rkz11zg2i4zp2f4r0l5kjz97kivbp86scrhz15l")))

(define-public crate-hash2hwaddr-0.0.1 (c (n "hash2hwaddr") (v "0.0.1") (d (list (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "1vygabipxm8fx8h1xnr739vc787fdlmj3mra7fkw6rz7x5ggnyl5")))

