(define-module (crates-io ha sh hash_events) #:use-module (crates-io))

(define-public crate-hash_events-0.1.0 (c (n "hash_events") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)))) (h "1b00hp440fi619ip2zr4ywxlm8wq1mvq541praa3cka93vfnszzz")))

(define-public crate-hash_events-0.1.1 (c (n "hash_events") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)))) (h "0wgsw4x614y17zi3ms4lba91xbqhqdmc8al9lbpdan254fa414qs")))

