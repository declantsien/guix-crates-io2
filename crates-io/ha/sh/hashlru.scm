(define-module (crates-io ha sh hashlru) #:use-module (crates-io))

(define-public crate-hashlru-0.1.0 (c (n "hashlru") (v "0.1.0") (h "0hgbjjykzrkpa0d638gmqhrnvbrhq7bfr03pv4034zzm4bs2qz97")))

(define-public crate-hashlru-0.2.0 (c (n "hashlru") (v "0.2.0") (h "1jcx7v72r6g8lgrgy3dmcl976gda7bg3z23b3v91ha5q66yq4396")))

(define-public crate-hashlru-0.2.1 (c (n "hashlru") (v "0.2.1") (h "1195bg3fqbvi116phhxaxpy60jg0wr74bql3i19smklnrvkih7gx")))

(define-public crate-hashlru-0.3.0 (c (n "hashlru") (v "0.3.0") (h "1f9mzx4pq6nk6dv7vlaq5ha3n3kqaa51d8pkhn3af79q4h9c2c30")))

(define-public crate-hashlru-0.3.2 (c (n "hashlru") (v "0.3.2") (h "0krm7h8s9aii1pa90fix5splhc94xpb0m871kgh8mkh1myv0fc08")))

(define-public crate-hashlru-0.4.0 (c (n "hashlru") (v "0.4.0") (h "02hmf8rcayplwfr5lncc16jr0g0qgkdrm51qa3fdc22xr3bqlziw")))

(define-public crate-hashlru-0.5.0 (c (n "hashlru") (v "0.5.0") (h "0frgf7l861nswncbvlhyacx4jpwvq2j8gbs0x4a2mgy85m4npwpy")))

(define-public crate-hashlru-0.5.1 (c (n "hashlru") (v "0.5.1") (h "0i04afqbjvpgkd20414ynliv5l60493zm3vlz74rxc8j9w4w8976")))

(define-public crate-hashlru-0.6.0 (c (n "hashlru") (v "0.6.0") (h "0dz82qbww3gkcd5snknq98jva6j5cyg44c761nrr92fl2lg7wddm")))

(define-public crate-hashlru-0.6.1 (c (n "hashlru") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0m3j546r7hr5b2vyifsbinjkpnyjvs4cg6cbv4p13xbcj2ypa9bd")))

(define-public crate-hashlru-0.7.0 (c (n "hashlru") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1gpy64an40kn0wfji4yp3bfqh57z1xn1z5lncwp6jbp8vpaybba9")))

(define-public crate-hashlru-0.7.1 (c (n "hashlru") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "05gmbjs952y6mqkvkn5sql0ppwfxxn17d6kfbbilb04c66ihaln1")))

(define-public crate-hashlru-0.7.2 (c (n "hashlru") (v "0.7.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1qj0hkmix37jgqixyxyinz2i1j9a13gxqk805xrrvd6asq2z2q35")))

(define-public crate-hashlru-0.7.3 (c (n "hashlru") (v "0.7.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1whdndgapb9gsh23m6bd5h6zchbazpprpgn32qhajmhq96m6p9df")))

(define-public crate-hashlru-0.8.0 (c (n "hashlru") (v "0.8.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1z4ljsr5ri5sd6lbb1hzhf8mf4ksj9wnfwz7x9x97gq0ii24c15z")))

(define-public crate-hashlru-0.9.0 (c (n "hashlru") (v "0.9.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06gskgsrsv6n7mygdj1x1200s0yhw8jycp638j8310hdsa7ikcsr") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-hashlru-0.10.0 (c (n "hashlru") (v "0.10.0") (d (list (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "postcard") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "13xkzihfj9lwl91hpnqnw7gjzhlp9341cbz7s5v1bmz4y5cagpm5") (f (quote (("default" "hashbrown")))) (s 2) (e (quote (("serde" "dep:serde") ("hashbrown" "dep:hashbrown")))) (r "1.60")))

(define-public crate-hashlru-0.11.0 (c (n "hashlru") (v "0.11.0") (d (list (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "postcard") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "0njciz9692clns92mm3cx6r66hrcavxylcfwsr8wc602kzi3n97c") (f (quote (("default" "hashbrown")))) (s 2) (e (quote (("serde" "dep:serde") ("hashbrown" "dep:hashbrown")))) (r "1.60")))

(define-public crate-hashlru-0.11.1 (c (n "hashlru") (v "0.11.1") (d (list (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "postcard") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "1arrl9bfvhqy97xqy4vaj0ys8d8531y7rb5v5yscqrk3cpgq5iyj") (f (quote (("default" "hashbrown")))) (s 2) (e (quote (("serde" "dep:serde") ("hashbrown" "dep:hashbrown")))) (r "1.60")))

