(define-module (crates-io ha sh hashindexed) #:use-module (crates-io))

(define-public crate-hashindexed-0.1.0 (c (n "hashindexed") (v "0.1.0") (h "046aligb7602ikivyqb9l3l5qdxbcpljgwz0sq29ph0hlws78i3i")))

(define-public crate-hashindexed-0.1.1 (c (n "hashindexed") (v "0.1.1") (h "0ma6n26vrwqzgzdr1bxwrvap4s2q4pdxcy71nn7g9j4crm9z2lfm")))

