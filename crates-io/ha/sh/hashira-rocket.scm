(define-module (crates-io ha sh hashira-rocket) #:use-module (crates-io))

(define-public crate-hashira-rocket-0.0.1-alpha (c (n "hashira-rocket") (v "0.0.1-alpha") (d (list (d (n "hashira") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("io"))) (d #t) (k 0)))) (h "09a7sw7msa5pml7hf2pzy6ipmvvvcq6ps94yynv77c8m2jfl3ipc")))

(define-public crate-hashira-rocket-0.0.2-alpha (c (n "hashira-rocket") (v "0.0.2-alpha") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashira") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("io"))) (d #t) (k 0)))) (h "1nylnrpz4zckqcqb5jvxzmgnb06ydpvbkwcgrqsidghn3z1jz3k7")))

