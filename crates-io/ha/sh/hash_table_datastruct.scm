(define-module (crates-io ha sh hash_table_datastruct) #:use-module (crates-io))

(define-public crate-hash_table_datastruct-0.1.0 (c (n "hash_table_datastruct") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "0lmmziqmp5w2xhapn0zlqg7z7pr9pnj13n7lrllcsn6dv52cixzf")))

(define-public crate-hash_table_datastruct-0.2.0 (c (n "hash_table_datastruct") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "0m75f8yi93b4cg6vfdgnldx5rnjmkb9b6s3smb5pjjqx0jgd4gws")))

(define-public crate-hash_table_datastruct-0.3.0 (c (n "hash_table_datastruct") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "1m7dryxdp2n7m3j17yrd6sg55riq2j3ghpqriiw8p3ri2zpdrbfy")))

(define-public crate-hash_table_datastruct-0.3.1 (c (n "hash_table_datastruct") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "0gcnda1zxr99mh0x4jf22j3afrnw1v75a4qgngp4f98j1zcjb3sn")))

(define-public crate-hash_table_datastruct-0.4.0 (c (n "hash_table_datastruct") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "05gdbqcl8c5w5ppvm01r2j9r62ibf18ifa6dcxnj21vf15sz3cl7")))

(define-public crate-hash_table_datastruct-0.5.0 (c (n "hash_table_datastruct") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "1vk0w5f890h8r8imd0172x7km1b8q1an4fpf3adays373iv7izjx")))

(define-public crate-hash_table_datastruct-0.6.0 (c (n "hash_table_datastruct") (v "0.6.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jvn0wfadj7in87k9pdwzma79dkwjrnrkfa8ahdijh9rsyq2d9z5") (f (quote (("default" "serde"))))))

(define-public crate-hash_table_datastruct-0.6.1 (c (n "hash_table_datastruct") (v "0.6.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04flp3gcc76qcmc5ip0iw17hjd9kpi2ih7lak4gp2xkjawa9dkhc") (f (quote (("default" "serde"))))))

(define-public crate-hash_table_datastruct-0.6.2 (c (n "hash_table_datastruct") (v "0.6.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dwx1jhw6ripfj5wip9fgw13w5ldgx6cwn2g1rs1i62gaakab8kd") (f (quote (("hashbrown-serde" "serde" "hashbrown" "hashbrown/serde") ("default" "serde"))))))

(define-public crate-hash_table_datastruct-0.6.3 (c (n "hash_table_datastruct") (v "0.6.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bfjahsxk5p9wrgp3pp2fkvpr1s90a5da4sd0c54ar59yjhm2v5h") (f (quote (("hashbrown-serde" "serde" "hashbrown" "hashbrown/serde") ("default" "serde"))))))

