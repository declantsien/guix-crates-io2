(define-module (crates-io ha sh hashlogs) #:use-module (crates-io))

(define-public crate-hashlogs-1.0.0 (c (n "hashlogs") (v "1.0.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0r6zlna834aflqpwaz4fmfr0zv4fsyvln48rlljfrqp6vkh91jzw")))

(define-public crate-hashlogs-1.0.1 (c (n "hashlogs") (v "1.0.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0icqia3rgjmdvzxkaxivc0mllqcl40hlymdhrbndbi7j3fama0cg")))

(define-public crate-hashlogs-1.0.2 (c (n "hashlogs") (v "1.0.2") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1f1prk8s764l05x6msknknchr2m7x6xrjm9c4jcrw642p2brz5qb")))

