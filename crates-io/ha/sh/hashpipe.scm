(define-module (crates-io ha sh hashpipe) #:use-module (crates-io))

(define-public crate-hashpipe-0.1.0 (c (n "hashpipe") (v "0.1.0") (d (list (d (n "chan") (r "~0.1.0") (d #t) (k 0)) (d (n "chan-signal") (r "~0.1.0") (d #t) (k 0)) (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "irc") (r "^0.11.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "11vx537lswdnv0m459fgigw9pf4xxb2b9cc8djlsp7cd4bjczhs4")))

(define-public crate-hashpipe-0.1.1 (c (n "hashpipe") (v "0.1.1") (d (list (d (n "chan") (r "~0.1.0") (d #t) (k 0)) (d (n "chan-signal") (r "~0.1.0") (d #t) (k 0)) (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "irc") (r "^0.11.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1y44yswph2skgaf8s9ii7sg210llprgp9x307zvv5v4g6fssjlg7")))

(define-public crate-hashpipe-0.1.2 (c (n "hashpipe") (v "0.1.2") (d (list (d (n "chan") (r "~0.1.0") (d #t) (k 0)) (d (n "chan-signal") (r "~0.1.0") (d #t) (k 0)) (d (n "clap") (r "~2.21.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "irc") (r "^0.11.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1jzlxaz4yzggp4fy68mhjzx8liga6qfxi0gr8hfffd9fqyghbzmp")))

