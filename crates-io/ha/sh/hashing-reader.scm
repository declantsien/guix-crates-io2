(define-module (crates-io ha sh hashing-reader) #:use-module (crates-io))

(define-public crate-hashing-reader-0.1.0 (c (n "hashing-reader") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports" "async_tokio" "async_futures"))) (d #t) (k 2)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "pin-project") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "05bx6xvzm605nd16m5ww3c43dqpddwcxflbrh830aw3dikm802s9") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

