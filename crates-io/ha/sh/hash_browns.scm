(define-module (crates-io ha sh hash_browns) #:use-module (crates-io))

(define-public crate-hash_browns-0.1.0 (c (n "hash_browns") (v "0.1.0") (h "1fgl40cjs7vpg2nxpc4z2g33n7r8bvgwpc7pasak74bjybv6hs4y")))

(define-public crate-hash_browns-0.1.1 (c (n "hash_browns") (v "0.1.1") (h "02jczv3m9lssv1gd76v77ii7ik4yvwqznwx44mb5lbw4qv1mq2ja")))

