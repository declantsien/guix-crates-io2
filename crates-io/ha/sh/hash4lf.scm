(define-module (crates-io ha sh hash4lf) #:use-module (crates-io))

(define-public crate-hash4lf-0.1.0 (c (n "hash4lf") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "md-5") (r "^0.10.5") (d #t) (k 2)) (d (n "murmur3") (r "^0.5.2") (d #t) (k 0)))) (h "1ykfrkislgimgm6f50p8495wqgfr52gyrww7mzswshp825bbbxcl") (y #t)))

