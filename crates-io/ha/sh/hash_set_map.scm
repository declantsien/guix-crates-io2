(define-module (crates-io ha sh hash_set_map) #:use-module (crates-io))

(define-public crate-hash_set_map-0.2.0 (c (n "hash_set_map") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)))) (h "0bs33qx2z4nvaxjp51ff165jpgp0sydc69mxja5plqpjckk2fsyx")))

(define-public crate-hash_set_map-0.3.0 (c (n "hash_set_map") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)))) (h "00q6jb557k7vxiqj488dh00gwbhzdjfy6ly0xm3zq59lm9di4j14")))

(define-public crate-hash_set_map-0.3.1 (c (n "hash_set_map") (v "0.3.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)))) (h "1gx2grbr83yk8i6w20i0s4mhj7j7j17vqa6v8k704libzinw764l")))

(define-public crate-hash_set_map-0.3.2 (c (n "hash_set_map") (v "0.3.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)))) (h "1ma4ygg5b6lmnhg4wn38gs4zl86sw86y5rargyywrrwxjabp0iyx")))

(define-public crate-hash_set_map-0.3.3 (c (n "hash_set_map") (v "0.3.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)))) (h "0k0qcxy6si6hzimyrxhg4y36rxmf34wqgfpxr4v1hak94b6hj49w")))

(define-public crate-hash_set_map-0.4.0 (c (n "hash_set_map") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)))) (h "0pv7vmz6269q9154hcwz4529ssy75zirhj948i7rjxh8s1qj30sl")))

