(define-module (crates-io ha sh hash_combine) #:use-module (crates-io))

(define-public crate-hash_combine-0.3.2 (c (n "hash_combine") (v "0.3.2") (h "1rix990whkwkgnffp4idhmf6z1c1d2aiwvdrp7yg0i48gxx40s1s")))

(define-public crate-hash_combine-0.3.4 (c (n "hash_combine") (v "0.3.4") (h "1d1c8acy5wybklficzqij6bclsingi6hxyzp1f500f25xb4x94cr")))

(define-public crate-hash_combine-0.3.5 (c (n "hash_combine") (v "0.3.5") (h "1pca2vbgwn9s6alqwrp5ppk13kkkcpa3q9ha2pyra6xnbzc97w7f")))

(define-public crate-hash_combine-0.3.6 (c (n "hash_combine") (v "0.3.6") (h "0670b7qap0193vaf15myzcfn15xnzxk9vfn3fa5i197mraywpp8g")))

(define-public crate-hash_combine-0.3.7 (c (n "hash_combine") (v "0.3.7") (h "0v5xgkzagqndxm7bdx9wzbzvfrfrbkb92qjl24anaslwnrw0gfci")))

(define-public crate-hash_combine-0.3.8 (c (n "hash_combine") (v "0.3.8") (h "18ndpmi2sa1zf1l3a6yd3dg3zfw1gw3w272pjy3p5342ancipqlf")))

(define-public crate-hash_combine-0.3.9 (c (n "hash_combine") (v "0.3.9") (h "1rxfwgjwnm97734fnrf52vxv06g4apm08qawzlsdflilyxc9w3rj")))

