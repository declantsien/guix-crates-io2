(define-module (crates-io ha sh hash-roll) #:use-module (crates-io))

(define-public crate-hash-roll-0.1.1 (c (n "hash-roll") (v "0.1.1") (d (list (d (n "histogram") (r "0.*") (d #t) (k 2)) (d (n "rand") (r "0.*") (d #t) (k 2)))) (h "0whd8x5s0fbkw2kp9jg27sjsfp0z3s42gnvmhz79axxlwyi5bbg7") (f (quote (("nightly"))))))

(define-public crate-hash-roll-0.2.0 (c (n "hash-roll") (v "0.2.0") (d (list (d (n "fmt-extra") (r "^0.1") (d #t) (k 0)) (d (n "histogram") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rollsum") (r "^0.2") (d #t) (k 2)))) (h "1ab17gwj83vxn5szjm1rdyzgvmz41wzrib8cq2gxmv4nsrphv90s") (f (quote (("nightly"))))))

(define-public crate-hash-roll-0.3.0 (c (n "hash-roll") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fmt-extra") (r "^0.2") (d #t) (k 0)) (d (n "histogram") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 2)) (d (n "rollsum") (r "^0.2") (d #t) (k 2)))) (h "04gs4whfighgjbwk0xdswsnbbipf7r960graxn8dy9mmlh1piqm9") (f (quote (("zstd") ("zpaq") ("ram") ("pigz") ("mii") ("gzip") ("gear") ("fastcdc") ("default" "bup" "buzhash" "fastcdc" "gear" "gzip" "mii" "pigz" "ram" "zpaq" "zstd") ("buzhash") ("bup"))))))

