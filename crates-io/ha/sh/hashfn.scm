(define-module (crates-io ha sh hashfn) #:use-module (crates-io))

(define-public crate-hashfn-0.1.0 (c (n "hashfn") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.93") (f (quote ("full"))) (d #t) (k 0)))) (h "0jd5phgy1vqvkj9pxvsrmf13fnb2axl7458zb63v8lkrdbdy93hz")))

(define-public crate-hashfn-0.2.0 (c (n "hashfn") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.93") (f (quote ("full"))) (d #t) (k 0)))) (h "1pbi10f2qfw66wmngacxz8g5hv4s4qf027jv8cn1ivg6dy6ilhdx")))

