(define-module (crates-io ha sh hashira-axum) #:use-module (crates-io))

(define-public crate-hashira-axum-0.0.1-alpha (c (n "hashira-axum") (v "0.0.1-alpha") (d (list (d (n "axum") (r "^0.6.15") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashira") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "hyper") (r "^0.14.26") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)) (d (n "tower-http") (r "^0.4.0") (f (quote ("fs"))) (d #t) (k 0)))) (h "0j6xkmkalfl15js87sxmfa6w0z8ar8pa0i50x3fs9cbpsrpviqyh")))

(define-public crate-hashira-axum-0.0.2-alpha (c (n "hashira-axum") (v "0.0.2-alpha") (d (list (d (n "axum") (r "^0.6.15") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashira") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "hyper") (r "^0.14.26") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)) (d (n "tower-http") (r "^0.4.0") (f (quote ("fs"))) (d #t) (k 0)))) (h "160wgckq5x1b9na21l4an3ap9s5qy9r7gbz84rxrbv4777rq5wwf")))

