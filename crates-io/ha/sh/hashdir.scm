(define-module (crates-io ha sh hashdir) #:use-module (crates-io))

(define-public crate-hashdir-0.1.2 (c (n "hashdir") (v "0.1.2") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "custom_error") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "02mam7vqdjzfwzj71h6glhxdx99451i0hmnixf38mrq9bp1jf5yx")))

(define-public crate-hashdir-0.1.3 (c (n "hashdir") (v "0.1.3") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "custom_error") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "16x1hjlrykg20x87ipfv3y5xcnkd9jiyin1rcmh76bdlh408hrnq")))

(define-public crate-hashdir-0.1.4 (c (n "hashdir") (v "0.1.4") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "custom_error") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "11vi9r7knyxamwwhl3vbfxb0xqxq1vpw4bmb9d6bimic28wx66ma")))

(define-public crate-hashdir-0.2.0 (c (n "hashdir") (v "0.2.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "custom_error") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1x12gp30fnwagsxj03qmb0czfjsm1jdkhh7rgd47lw1c4wk507ym")))

