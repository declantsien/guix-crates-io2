(define-module (crates-io ha sh hashing-copy) #:use-module (crates-io))

(define-public crate-hashing-copy-0.1.0 (c (n "hashing-copy") (v "0.1.0") (d (list (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "sha2") (r "^0.7") (d #t) (k 2)))) (h "0nf1km1a1gkc4frsas0n0sfziwc8pm0239dl8x49fsc5a45k695r")))

(define-public crate-hashing-copy-0.1.1 (c (n "hashing-copy") (v "0.1.1") (d (list (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "sha2") (r "^0.7") (d #t) (k 2)))) (h "14ba9cf784b40flv79m3zzd4lkanynj6d3v47phj04np1zk03rl4")))

(define-public crate-hashing-copy-0.2.0 (c (n "hashing-copy") (v "0.2.0") (d (list (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "sha2") (r "^0.7") (d #t) (k 2)))) (h "0vmzirfyjd66kgaapgizmcw9xjdyk2f2a4n1dw4z7dcdc0nwwdyz")))

(define-public crate-hashing-copy-0.3.0 (c (n "hashing-copy") (v "0.3.0") (d (list (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1.1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)))) (h "1jlyfs5cjvahxcknpx7bs659jbgmd9im32brwfazwi0jpbdnkgvj")))

