(define-module (crates-io ha sh hashgood) #:use-module (crates-io))

(define-public crate-hashgood-0.4.0 (c (n "hashgood") (v "0.4.0") (d (list (d (n "copypasta") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "06fqqsmblv8z0c8prhqy1vmgac87hvy9p6ky2grksq40x1y8rps6") (f (quote (("paste" "copypasta"))))))

