(define-module (crates-io ha sh hash-list) #:use-module (crates-io))

(define-public crate-hash-list-0.1.0 (c (n "hash-list") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json-writer") (r "^0.2.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.5") (d #t) (k 0)))) (h "1z9k5mz4lmvvryxawgwvmjn4zy51xp2q0l8dj6qb09jb5dwlmk9v")))

