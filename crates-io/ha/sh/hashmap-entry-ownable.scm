(define-module (crates-io ha sh hashmap-entry-ownable) #:use-module (crates-io))

(define-public crate-hashmap-entry-ownable-0.1.0 (c (n "hashmap-entry-ownable") (v "0.1.0") (h "06by3kyw05nnvgds5hm7kwgz9mapc62y7d0jal9i3jg56v39q112")))

(define-public crate-hashmap-entry-ownable-0.2.0 (c (n "hashmap-entry-ownable") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0rspk692gazpjwaf6vh21y7rnpwgfrlray68wyd0xgqnxihvkrwl") (f (quote (("nightly") ("default"))))))

