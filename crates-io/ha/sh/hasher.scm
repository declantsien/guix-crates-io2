(define-module (crates-io ha sh hasher) #:use-module (crates-io))

(define-public crate-hasher-0.1.0 (c (n "hasher") (v "0.1.0") (d (list (d (n "blake2b-rs") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libsm") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0b1l50kkwfp1yppbn4pwf1j3jyvzvv5nqnjiqvkv02s4pc1lw81w") (f (quote (("hash-sm3" "libsm") ("hash-keccak" "tiny-keccak") ("hash-blake2b" "blake2b-rs") ("default"))))))

(define-public crate-hasher-0.1.1 (c (n "hasher") (v "0.1.1") (d (list (d (n "blake2b-rs") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libsm") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1hcgmdcpw642i8a6l1yrc5d5fzj2jb78y3xn88w3jqs5sq8vxd3i") (f (quote (("hash-sm3" "libsm") ("hash-keccak" "tiny-keccak") ("hash-blake2b" "blake2b-rs") ("default"))))))

(define-public crate-hasher-0.1.2 (c (n "hasher") (v "0.1.2") (d (list (d (n "blake2b-rs") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libsm") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (o #t) (d #t) (k 0)))) (h "08ylzhs6aw87vn4316fxc7h1562fbx5sxbpx6syafw894b01lvs7") (f (quote (("hash-sm3" "libsm") ("hash-keccak" "tiny-keccak") ("hash-blake2b" "blake2b-rs") ("default"))))))

(define-public crate-hasher-0.1.3 (c (n "hasher") (v "0.1.3") (d (list (d (n "blake2b-rs") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "cryptape-sm") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (o #t) (d #t) (k 0)))) (h "06npxxhazcfkvvnkd6yshgd8fc63fqpbbddw2a02ns9swxnq9s0h") (f (quote (("hash-sm3" "cryptape-sm") ("hash-keccak" "tiny-keccak") ("hash-blake2b" "blake2b-rs") ("default"))))))

(define-public crate-hasher-0.1.4 (c (n "hasher") (v "0.1.4") (d (list (d (n "blake2b-rs") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "cryptape-sm") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (o #t) (d #t) (k 0)))) (h "1am3z1z4j78vaba3p6b4xwpxrq8888gra3c74b72fzsnnrwadfxv") (f (quote (("hash-sm3" "cryptape-sm") ("hash-keccak" "tiny-keccak") ("hash-blake2b" "blake2b-rs") ("default"))))))

