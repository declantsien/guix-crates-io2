(define-module (crates-io ha sh hash40) #:use-module (crates-io))

(define-public crate-hash40-0.1.0 (c (n "hash40") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "047igyaijj7dnkq0cgwg997fb6ymmychh8yxv7gaqlfvy7n4p7v4")))

(define-public crate-hash40-0.2.0 (c (n "hash40") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "compile-time-crc32") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "0yi2xsxzvnmryav7v009w5m6cjx7dg2mb9s93pccpsjxlkx8chz5")))

(define-public crate-hash40-0.3.0 (c (n "hash40") (v "0.3.0") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "compile-time-crc32") (r "^0.1.2") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "1jalh0341xzqgdg5j54r6dwdmvcg3k1lh5k97dpr46pkvdsfy2jc")))

(define-public crate-hash40-0.4.0 (c (n "hash40") (v "0.4.0") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "compile-time-crc32") (r "^0.1.2") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "1h2wzv8i0f2p3g28nd7hbyc44a0khwvfqa45c4pgaz6aw26snr3x")))

(define-public crate-hash40-0.4.1 (c (n "hash40") (v "0.4.1") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "compile-time-crc32") (r "^0.1.2") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "095z63j6glzr2492anq2apiiamff6dda0ikc46wgfd2kbw548w11")))

(define-public crate-hash40-0.4.2 (c (n "hash40") (v "0.4.2") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "compile-time-crc32") (r "^0.1.2") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "0la7xlc6mx2k1f69i3ypd1irg02j3cg9jsv25701444clkd68rji")))

(define-public crate-hash40-0.4.3 (c (n "hash40") (v "0.4.3") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "compile-time-crc32") (r "^0.1.2") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "diff-struct") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "0dgp9607nbgcjcji2gcv7pxhzqjgq4wh3csvx2xkyknj3fvczcll")))

(define-public crate-hash40-0.4.4 (c (n "hash40") (v "0.4.4") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "compile-time-crc32") (r "^0.1.2") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "diff-struct") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "0h6lhj4233g2240ghbiw9zbrclkd24ba5z2w0h27rg5v3xklwn70")))

(define-public crate-hash40-1.0.0 (c (n "hash40") (v "1.0.0") (d (list (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "diff-struct") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1mjypr047gy056s5fckqy3pw8dpsdrxdnqh8idab546ncfmjw4bs")))

(define-public crate-hash40-1.1.0 (c (n "hash40") (v "1.1.0") (d (list (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "diff-struct") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "05a6f574vhpvjjz03j9bjiqcpc02iw4yxyncaikd923f0jr97qb6")))

(define-public crate-hash40-1.2.0 (c (n "hash40") (v "1.2.0") (d (list (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "binrw") (r "^0.9.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "diff-struct") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0cc3fw25k80xw7ajzaqyhc99ggyb8zdllxlvxc1dhqwjil3grx3g") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-hash40-1.3.0 (c (n "hash40") (v "1.3.0") (d (list (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "binrw") (r "^0.11.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "diff-struct") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1nllk1hf3rn2bbgayx4790d13dmzjcfrjy5gy4zqm1jx6p604hws") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-hash40-1.3.1 (c (n "hash40") (v "1.3.1") (d (list (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "binrw") (r "^0.11.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "diff-struct") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1daj2flaq30y4f8cwhmj5h5igf9sjby7asdqx0xb83f58zra8vy7") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

