(define-module (crates-io ha sh hash-rs) #:use-module (crates-io))

(define-public crate-hash-rs-0.0.1 (c (n "hash-rs") (v "0.0.1") (h "1pckgrysng8agib83604swnj5z2l9w5vbr0cav0srk0ymjgcfbkg")))

(define-public crate-hash-rs-0.0.2 (c (n "hash-rs") (v "0.0.2") (h "1f2ph7h427mssxldy9hl2q4zyhdhngyv2m6qwwb52ydhgaks29n6")))

