(define-module (crates-io ha sh hash-lib) #:use-module (crates-io))

(define-public crate-hash-lib-0.0.1 (c (n "hash-lib") (v "0.0.1") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0fjr1pzzg8nhmn09vlx7gsk9xcnblxskqpildssnl1mckcq3b8xi") (y #t)))

(define-public crate-hash-lib-0.0.2 (c (n "hash-lib") (v "0.0.2") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1hjnws9mmpg7mg9gz5jm1wc4jip8v61832inbfd4mv66mc8nasgh") (y #t) (r "1.66.1")))

