(define-module (crates-io ha sh hashtag) #:use-module (crates-io))

(define-public crate-hashtag-0.1.0 (c (n "hashtag") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "10c912qwyb9c9l1ai2hilscc4cqv958m66z32x6ngsjm4f0c725d")))

(define-public crate-hashtag-0.1.1 (c (n "hashtag") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "0ndrli49dwcz3syzbisjnj646wz97g2p4fhxazp6fk7yc2klhryr")))

(define-public crate-hashtag-0.1.2 (c (n "hashtag") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1jzdqw89lrp5fmvh1f9s01va119jng2xqhs2wp2c8hyqz5c535da")))

(define-public crate-hashtag-0.1.3 (c (n "hashtag") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "06hszvz90whr1idsy9vbq2sdbi7v0kadyk26ylfjgilki2zhji40")))

(define-public crate-hashtag-0.1.4 (c (n "hashtag") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "12rlisskc348kp4a4jsjfwz7b2iqrsci65460rin10i6zhy4bc2y")))

(define-public crate-hashtag-0.1.5 (c (n "hashtag") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1937n1i5c07gviyzl83shpsl7symhsy96vq6bvp0h0aisffmdzdg")))

(define-public crate-hashtag-0.1.6 (c (n "hashtag") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "050a14m85k7fa8h78arj2j562dxfjnxw7b86kdxjzj90s6a9y57d")))

(define-public crate-hashtag-0.1.7 (c (n "hashtag") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "07vcc6gaiwdayrj0p19mcxalgslacirkbh81r2xv9f9b8hp8r5vh")))

(define-public crate-hashtag-0.1.8 (c (n "hashtag") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "05dssx8xyz61rfjy4bbwsl72kwklxbn6hwzwnlwzmhhnprhgpg48")))

(define-public crate-hashtag-0.1.9 (c (n "hashtag") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1brmdnvjx5r68ap8c6ma0k9wh9mfd09lj87dcadlzv1p9vq6v16a")))

(define-public crate-hashtag-0.1.10 (c (n "hashtag") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1indp000pvihxbppmisnp1sl3fixxlqzq3qpnmds479fji4nblbf")))

(define-public crate-hashtag-0.1.11 (c (n "hashtag") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1ic72wjlh7a72xpf14gg0g63a213js2bkwkqyv75rgiksnkvc6fw")))

(define-public crate-hashtag-1.0.0 (c (n "hashtag") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0z0f068hgg26wqpxklqpfyw76icwy9xcl762zf23q83jzf3p4hjb") (f (quote (("default"))))))

(define-public crate-hashtag-1.0.1 (c (n "hashtag") (v "1.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ydq9jjy35y0sa78pmz3n3db89fj2q3c5m7ihw370001fnf39qqw") (f (quote (("default"))))))

