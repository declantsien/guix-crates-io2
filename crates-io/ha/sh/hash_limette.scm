(define-module (crates-io ha sh hash_limette) #:use-module (crates-io))

(define-public crate-hash_limette-0.1.2 (c (n "hash_limette") (v "0.1.2") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)))) (h "1dbhrra7035p5rwa37wj2wz2gibm7152qncpv5284jrxpwk47y76") (y #t)))

(define-public crate-hash_limette-0.1.3 (c (n "hash_limette") (v "0.1.3") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)))) (h "1gjl20c8sxyi7gm9czfscj0xyf6fjrflwj2mn11cxzidpnjmmvbk") (y #t)))

(define-public crate-hash_limette-0.1.4 (c (n "hash_limette") (v "0.1.4") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)))) (h "03xhyj99fd02r53yi8ckkavgcjvflgxhawk689c7157lh1dfrv39")))

