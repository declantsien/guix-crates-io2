(define-module (crates-io ha sh hashira-warp) #:use-module (crates-io))

(define-public crate-hashira-warp-0.0.2-alpha (c (n "hashira-warp") (v "0.0.2-alpha") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashira") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1yj506n1rblis1igacvs0gz3pa9qf3y8m5q3ymgdf98wm5fj67wf")))

