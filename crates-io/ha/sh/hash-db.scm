(define-module (crates-io ha sh hash-db) #:use-module (crates-io))

(define-public crate-hash-db-0.9.0 (c (n "hash-db") (v "0.9.0") (h "0f6kk9inm46spz4bs14zb2zdp7sz9d6sp7706y1vqsw64hvw8pnw") (f (quote (("std") ("default" "std"))))))

(define-public crate-hash-db-0.11.0 (c (n "hash-db") (v "0.11.0") (h "10qls65n6nc8wb2319gs8wlcl53g2nisnyc8c7qrfahsdqgm00qv") (f (quote (("std") ("default" "std"))))))

(define-public crate-hash-db-0.12.0 (c (n "hash-db") (v "0.12.0") (h "17c2fd9lpdvzn1zdf0lwkanr6w205qq6vpg7fpafh34xf8s3hih7") (f (quote (("std") ("default" "std"))))))

(define-public crate-hash-db-0.12.2 (c (n "hash-db") (v "0.12.2") (h "1q2w6h39yh6jvz4zcv43f0p9cr8fgmv7k306c7fsqw64wlbv8zxs") (f (quote (("std") ("default" "std"))))))

(define-public crate-hash-db-0.12.3 (c (n "hash-db") (v "0.12.3") (h "0pk3lfdvaw0vvzgs0izj21ma83biinlvr8gr9w49d5fxsppq2zjq") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-hash-db-0.12.4 (c (n "hash-db") (v "0.12.4") (h "0hssrvnmpa3lgvq33s81v83ajix1w3wpxq437mix8vn852j9ag0b") (f (quote (("std") ("default" "std"))))))

(define-public crate-hash-db-0.14.0 (c (n "hash-db") (v "0.14.0") (h "0q7jr0pjl4g6j7gafzar67c1m8rkna39d3flnr9qxhmw0q2p38n4") (f (quote (("std") ("default" "std"))))))

(define-public crate-hash-db-0.15.0 (c (n "hash-db") (v "0.15.0") (h "19139j5ii9852dr76zfrk5wprbih36n47y1y91jd58n4jgn7zj1j") (f (quote (("std") ("default" "std"))))))

(define-public crate-hash-db-0.15.1 (c (n "hash-db") (v "0.15.1") (h "0flikvibn1mk5371v7vw4m4dy961xcq9c4dw26b7n7y22xn8pigi") (f (quote (("std") ("default" "std"))))))

(define-public crate-hash-db-0.15.2 (c (n "hash-db") (v "0.15.2") (h "0jinaqwwr960a0i93gkgy8091fn8zn0v7s07lgrx187dnpkx8fyj") (f (quote (("std") ("default" "std"))))))

(define-public crate-hash-db-0.16.0 (c (n "hash-db") (v "0.16.0") (h "1d3abk1l1301rh4x24jj1y2a1yhfxc3y9yg45yp2ax0x6s37fzcf") (f (quote (("std") ("default" "std"))))))

