(define-module (crates-io ha sh hashporn) #:use-module (crates-io))

(define-public crate-hashporn-0.1.0 (c (n "hashporn") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "metadata") (r "^0.1.6") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)))) (h "11fbbgndbppiw5rry4msbymrkgz020c5adz3yazh53jkblinasxa")))

