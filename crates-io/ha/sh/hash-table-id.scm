(define-module (crates-io ha sh hash-table-id) #:use-module (crates-io))

(define-public crate-hash-table-id-0.1.0 (c (n "hash-table-id") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1w854xzw3ppp62gqvb5qr82qfv8g59xvy3hl9zalxla5ksgw67m4")))

