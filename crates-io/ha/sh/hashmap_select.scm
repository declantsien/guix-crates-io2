(define-module (crates-io ha sh hashmap_select) #:use-module (crates-io))

(define-public crate-hashmap_select-0.1.0 (c (n "hashmap_select") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)))) (h "1lhlml1kkhcpyifibarnx3ncf0i326jk3f6b10hx4n4z0rqkf36g")))

(define-public crate-hashmap_select-0.1.1 (c (n "hashmap_select") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)))) (h "0c22n4p8qr19v0a5knphyghygxkdxdrkckaabc65579nziar1ww4")))

(define-public crate-hashmap_select-0.1.2 (c (n "hashmap_select") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)))) (h "0bdhd5xsvgz3ixhq01lkhiayn5qdly5k09qkh303dnwpy4czn3b3")))

