(define-module (crates-io ha sh hashlife) #:use-module (crates-io))

(define-public crate-hashlife-0.0.0 (c (n "hashlife") (v "0.0.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 2)) (d (n "cached") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.12") (d #t) (k 2)))) (h "14dqy49c7kawm3m0d857lh6myhjid3j6dny07z5i1g211pj2hvlb")))

