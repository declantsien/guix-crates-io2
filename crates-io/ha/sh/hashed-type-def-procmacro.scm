(define-module (crates-io ha sh hashed-type-def-procmacro) #:use-module (crates-io))

(define-public crate-hashed-type-def-procmacro-0.0.0 (c (n "hashed-type-def-procmacro") (v "0.0.0") (h "1cvvmw78758jlq481j5j9sl0k5i77j1clqqmj6a1idrwc7vqh6zq") (y #t)))

(define-public crate-hashed-type-def-procmacro-0.1.0 (c (n "hashed-type-def-procmacro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "167j75b87ji8gz69ckks3dhqcsqcwri1ih3vw00m4dkqqi475213")))

(define-public crate-hashed-type-def-procmacro-0.1.1 (c (n "hashed-type-def-procmacro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)))) (h "18vf47g53jrgbxp8x0bmjfr13n0zipz39hmy6jraslffnbx25b35")))

(define-public crate-hashed-type-def-procmacro-0.1.2 (c (n "hashed-type-def-procmacro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1p65cfn3xvd60kayjvpfkzgf3bki5qax0gp062gaxmm02rzmg97g")))

