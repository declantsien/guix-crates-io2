(define-module (crates-io ha sh hashmap_ext) #:use-module (crates-io))

(define-public crate-hashmap_ext-0.0.1 (c (n "hashmap_ext") (v "0.0.1") (h "1w5bbgrlkc9x2323h99qva74hllms5pqaphwy5g7z84ndj1csjh2") (f (quote (("unstable"))))))

(define-public crate-hashmap_ext-0.0.2 (c (n "hashmap_ext") (v "0.0.2") (h "0zxr6qzk9dvx1bd8wfhjcc3dvzqa8k78l85vaf2mn11szlbdhvzg") (f (quote (("unstable"))))))

(define-public crate-hashmap_ext-0.0.3 (c (n "hashmap_ext") (v "0.0.3") (h "0dys8v1if4c8scna2avqybxx608as86bd9rvirhvs67hv4lplq5c") (f (quote (("unstable"))))))

