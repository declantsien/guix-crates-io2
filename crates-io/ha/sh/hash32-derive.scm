(define-module (crates-io ha sh hash32-derive) #:use-module (crates-io))

(define-public crate-hash32-derive-0.1.0 (c (n "hash32-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "18lrlxycq45kaz0l688shxnhgh3ryjp3zn0n6vfcs5sa2nyyzh7b")))

(define-public crate-hash32-derive-0.1.1 (c (n "hash32-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1zy60cdqrccd9kc8w4hvk1q584b4gjr4d48n3dff42xn6alapljr")))

