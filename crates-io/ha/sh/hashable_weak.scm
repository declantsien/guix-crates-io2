(define-module (crates-io ha sh hashable_weak) #:use-module (crates-io))

(define-public crate-hashable_weak-0.1.0 (c (n "hashable_weak") (v "0.1.0") (h "1a31ijncz3ffwinvns9a7j9prwvs722igcg7nf717nhz9p4az989")))

(define-public crate-hashable_weak-0.1.1 (c (n "hashable_weak") (v "0.1.1") (h "1fhdqcf5pbaxplnm0ah0j8s9dhbg5dlvlwbl5ssklrd8jl91jfp0")))

