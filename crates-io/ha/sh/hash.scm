(define-module (crates-io ha sh hash) #:use-module (crates-io))

(define-public crate-hash-0.0.1 (c (n "hash") (v "0.0.1") (h "1iqjp46z46h9ingkv1vd252hsjb2k3g0y7azlzid13mw0wrdi8q9")))

(define-public crate-hash-0.1.0 (c (n "hash") (v "0.1.0") (h "1r8c6argil977cmw9w549xknv5g3h2x0z0jxgbhl0l1g4m33c7d1")))

(define-public crate-hash-0.2.0 (c (n "hash") (v "0.2.0") (d (list (d (n "md5") (r "*") (d #t) (k 0)))) (h "1jsigy4lkfq8zyrxdxy3saqd89qcdg1fgnkgfhcs3bzvjw4idflr")))

(define-public crate-hash-0.3.0 (c (n "hash") (v "0.3.0") (h "1479dl4jy9lcac4zl3iykbig5wrg8m2b78bdnijamnmgnyyljz6x")))

