(define-module (crates-io ha sh hash_histogram) #:use-module (crates-io))

(define-public crate-hash_histogram-0.5.0 (c (n "hash_histogram") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hyh8cbs420p2qxjk0z5r3i816n44xmjrssz07q8x1b2vlmi9nsn")))

(define-public crate-hash_histogram-0.5.1 (c (n "hash_histogram") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y9zq0555kimswmipyvjfslz656x9f6w6cfnn2a4j1vsg18bb76g")))

(define-public crate-hash_histogram-0.5.2 (c (n "hash_histogram") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b5p6wbzi8bdq8g3sg8jy104r1k617cpq7620md08afajgxfwv2j")))

(define-public crate-hash_histogram-0.6.0 (c (n "hash_histogram") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lk2smdpvgxqpds95asrdl690zyic6mkbxp8yw956hf0y55hrcl8")))

(define-public crate-hash_histogram-0.6.1 (c (n "hash_histogram") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1c997va3zvn5m1gg0hik7mgdjdhg00gdm2rzplwwab3kiywhkv8j")))

(define-public crate-hash_histogram-0.6.2 (c (n "hash_histogram") (v "0.6.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0737zsccrqb8krxb3aclij9ky5kmph1vipdcxnnfb5rndpg71sxk")))

(define-public crate-hash_histogram-0.7.0 (c (n "hash_histogram") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09gd5bwc3mnz9myykrjf50xw7z33sjlbih578nhqrg1dhv7gzhbh")))

(define-public crate-hash_histogram-0.8.0 (c (n "hash_histogram") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "178x8r87zf7v3pa7x7as6d6sp5xr3w3w79fqn3dm04xfshb5f873")))

