(define-module (crates-io ha sh hashfind) #:use-module (crates-io))

(define-public crate-hashfind-0.1.0 (c (n "hashfind") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "libhashfind") (r "^0.1.0") (d #t) (k 0)))) (h "1qg7fh519qh2zhma7a402isy11qmh47jmhcyvdjxg18gmfyn9fxk")))

