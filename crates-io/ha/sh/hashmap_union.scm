(define-module (crates-io ha sh hashmap_union) #:use-module (crates-io))

(define-public crate-hashmap_union-0.1.0 (c (n "hashmap_union") (v "0.1.0") (h "0w3vlffk39k67m2waapdqzgyr2l07jqaf4lanmz3qyj2w248ivx7")))

(define-public crate-hashmap_union-0.2.0 (c (n "hashmap_union") (v "0.2.0") (h "0ap11bqynd604x8gx1f23dmk84v0fcb68pgi27pmzhxiamp0zgb7")))

(define-public crate-hashmap_union-0.3.0 (c (n "hashmap_union") (v "0.3.0") (h "1x9xf47m3p1qy2z2v17rq42kjvqypiza0r1hbcf76xj2nq3gh4ls")))

