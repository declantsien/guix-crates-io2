(define-module (crates-io ha sh hashvec) #:use-module (crates-io))

(define-public crate-hashvec-0.1.0 (c (n "hashvec") (v "0.1.0") (h "0y9n1k71zp8mi8lb616hg20rnbiil48l7r13x4c56gvk5caqfbgh")))

(define-public crate-hashvec-0.1.1 (c (n "hashvec") (v "0.1.1") (h "0xibw8fghc3gkjd1sl4sh91l8rzi432wl89sq44vmn5b2nz7xkcx")))

(define-public crate-hashvec-0.1.2 (c (n "hashvec") (v "0.1.2") (h "04nr957vlcic2ymlshqbbnnbcia8j2gsf0kfw64ldmvvhl91bghr")))

(define-public crate-hashvec-0.1.3 (c (n "hashvec") (v "0.1.3") (h "1rhzcf6brvblg1frdpk5v1xgxc1mfll8nbjgdg11la99f873npj8")))

(define-public crate-hashvec-0.1.4 (c (n "hashvec") (v "0.1.4") (h "0rckac592xmqqrzvkd4xp8k8fiaczkabhzr5fbjybhxlbpiwngjl")))

(define-public crate-hashvec-0.1.5 (c (n "hashvec") (v "0.1.5") (h "1g5861hbw0ygvp7snxzl39qwwfraf2mzrzwj7cbb2ln243k9pkj2")))

(define-public crate-hashvec-0.1.6 (c (n "hashvec") (v "0.1.6") (h "0vmf1q37p55kv4h0yw2lgsq8w7q03hp0dzn7vphc70sgy8c7gml7")))

