(define-module (crates-io ha sh hashira-tide) #:use-module (crates-io))

(define-public crate-hashira-tide-0.0.2-alpha (c (n "hashira-tide") (v "0.0.2-alpha") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashira") (r "^0.0.2-alpha") (f (quote ("internal"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "1r9kw3nz7i3f2v47pjwig0jb6ylcbpxlgkb2qz7d3nagz51zv7kg")))

