(define-module (crates-io ha sh hash_map_diff) #:use-module (crates-io))

(define-public crate-hash_map_diff-0.1.0 (c (n "hash_map_diff") (v "0.1.0") (h "0nd9xi2hggdp77y8wg3mfxpzsmjvf9kqgpkjng5bcdm85s9va5k9")))

(define-public crate-hash_map_diff-0.2.0 (c (n "hash_map_diff") (v "0.2.0") (h "041663fzbw1z8hsc3i3b0zc8q11aqf04c042x22f47hhpkw6i4gz")))

