(define-module (crates-io ha sh hashbag) #:use-module (crates-io))

(define-public crate-hashbag-0.1.0 (c (n "hashbag") (v "0.1.0") (h "1p1jp83p2iqgzwh672dg6gjvvhplli3ij942ra8n0lk6ggwld30c")))

(define-public crate-hashbag-0.1.1 (c (n "hashbag") (v "0.1.1") (h "1cdsf453r9916f3mwgkmdhmc78ag37zy4d3agdxmrl0pvj7rfzfj")))

(define-public crate-hashbag-0.1.2 (c (n "hashbag") (v "0.1.2") (h "01h7msmlb3wsqzbhij2km6amkj016d4sxvqyagg17kz7zyrk2as5")))

(define-public crate-hashbag-0.1.3 (c (n "hashbag") (v "0.1.3") (d (list (d (n "griddle") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1fvzh7hxax4balc9vdz1q3zvqv5i53zjnsmxypq8w3zkh4b6dgm9") (f (quote (("amortize" "griddle"))))))

(define-public crate-hashbag-0.1.4 (c (n "hashbag") (v "0.1.4") (d (list (d (n "griddle") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1lvkfsff1m3bydx62ki484fwlpdccrlw9d3qdhswqzz3v9m147a6") (f (quote (("amortize" "griddle"))))))

(define-public crate-hashbag-0.1.5 (c (n "hashbag") (v "0.1.5") (d (list (d (n "griddle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06n0j5k4ka02smiw2sk6x0pg5bgvy14vhn9767wzd7vy1s7if6h2") (f (quote (("amortize" "griddle"))))))

(define-public crate-hashbag-0.1.6 (c (n "hashbag") (v "0.1.6") (d (list (d (n "griddle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1cwqbcwmiiwx0wjnfksircsfbjf1r3wqsi8qfbk2wxg4fjz2qbxd") (f (quote (("amortize" "griddle"))))))

(define-public crate-hashbag-0.1.7 (c (n "hashbag") (v "0.1.7") (d (list (d (n "griddle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ap3f26pikcxxrc8lzh308v4rn8ivngknfzcvnvavxavi1c0qafv") (f (quote (("amortize" "griddle"))))))

(define-public crate-hashbag-0.1.8 (c (n "hashbag") (v "0.1.8") (d (list (d (n "griddle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zz8lbfya1bcya31hnq2ynbxm658xs01qlg71x3hzi9lr927r6jp") (f (quote (("amortize" "griddle"))))))

(define-public crate-hashbag-0.1.9 (c (n "hashbag") (v "0.1.9") (d (list (d (n "griddle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1cyfmn4761avbkndq6lkdqja3n8ndfid7cini541y0kg4add6nf7") (f (quote (("amortize" "griddle"))))))

(define-public crate-hashbag-0.1.10 (c (n "hashbag") (v "0.1.10") (d (list (d (n "griddle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0av297mcr3vwb47blgaxk0n93ch1imrmcjn1mmzf3jm767q2fqfy") (f (quote (("amortize" "griddle"))))))

(define-public crate-hashbag-0.1.11 (c (n "hashbag") (v "0.1.11") (d (list (d (n "griddle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1g4n8d6a86agikr071iq28d2qb1bgglk4353jf4kwbdpihilrm5k") (f (quote (("amortize" "griddle"))))))

(define-public crate-hashbag-0.1.12 (c (n "hashbag") (v "0.1.12") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "griddle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14wx5rx11h0nb1rmlwmiw7719q2qf941x7ipcdg8yahb0sr99x4q") (f (quote (("amortize" "griddle"))))))

