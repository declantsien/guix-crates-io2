(define-module (crates-io ha sh hash_hasher) #:use-module (crates-io))

(define-public crate-hash_hasher-0.1.0 (c (n "hash_hasher") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1hr0915q0avmxi6wgnsyskzvlz6q1p5s9389fdjcr0q8y09nkxr9") (y #t)))

(define-public crate-hash_hasher-0.2.0 (c (n "hash_hasher") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1f7f4xiv99ihxb3pqwwsphis1ls1jkq79gg2ichfhnf8gbpxn1wq") (y #t)))

(define-public crate-hash_hasher-0.3.0 (c (n "hash_hasher") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1x7z2lvikc1xfnjkpyvwqx0fxq82cfa16sxhbp07qpx0i2b409hb") (y #t)))

(define-public crate-hash_hasher-1.0.0 (c (n "hash_hasher") (v "1.0.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1i029grfq2124hxhvrlnj61hfx970w7bdgmzm0xbhh138j4n21lb")))

(define-public crate-hash_hasher-1.1.0 (c (n "hash_hasher") (v "1.1.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1ppb637b6pk5ibifazxl6a17sqagg2kpxkc2c4awxmmxbwrmnga3")))

(define-public crate-hash_hasher-2.0.0 (c (n "hash_hasher") (v "2.0.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0dv36f4n9friqjm2b8wxf5j8vzk2llwv267lbg971gjjk80lcv4g")))

(define-public crate-hash_hasher-2.0.1 (c (n "hash_hasher") (v "2.0.1") (d (list (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "03rfy71x08dvvfsq4j60bh6x8g1klwxz5lwflilyqzcrk7251gdc")))

(define-public crate-hash_hasher-2.0.2 (c (n "hash_hasher") (v "2.0.2") (d (list (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1k16vx6c7kdkcvnky4y5l4jf2mq613cj9rjjxr2fj596cwl8iivs")))

(define-public crate-hash_hasher-2.0.3 (c (n "hash_hasher") (v "2.0.3") (d (list (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "034cd4m3znwff3cd1i54c40944y999jz086d70rwpl0jfl01swkl")))

