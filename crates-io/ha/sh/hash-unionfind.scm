(define-module (crates-io ha sh hash-unionfind) #:use-module (crates-io))

(define-public crate-hash-unionfind-0.1.0 (c (n "hash-unionfind") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (d #t) (k 0)))) (h "1rg7mw4sk4v5bp42ldm7xx8zhavg8rvcwir78brak6k0xlywipxy")))

(define-public crate-hash-unionfind-0.1.1 (c (n "hash-unionfind") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (d #t) (k 0)))) (h "00pfzqmipg59c1yyz2msa21yaix1zyf67cy3yxpgkpff17vk244p")))

