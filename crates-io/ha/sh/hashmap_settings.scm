(define-module (crates-io ha sh hashmap_settings) #:use-module (crates-io))

(define-public crate-hashmap_settings-0.1.0 (c (n "hashmap_settings") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wp93vd0fcbdix1k7v1l03968jskphgxky2k9cjmrrbq9hj3wqds")))

(define-public crate-hashmap_settings-0.2.0 (c (n "hashmap_settings") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03q0zc5b7ciyr64870zrq82j45nfhn7vcnd1pmfxwfrh7m4dwnf0")))

(define-public crate-hashmap_settings-0.3.0 (c (n "hashmap_settings") (v "0.3.0") (d (list (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)) (d (n "dyn_ord") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "typetag") (r "^0.2") (d #t) (k 0)))) (h "0hy4v500zl4aw9azhnym5r9qfs8l31crm4fp1l1pvz4djvqdlh0d")))

(define-public crate-hashmap_settings-0.4.0 (c (n "hashmap_settings") (v "0.4.0") (d (list (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)) (d (n "dyn_ord") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "typetag") (r "^0.2") (d #t) (k 0)))) (h "1xlw4pipzp5lpqz5v242yz16gl8dxbmlh03f7hxnxy7zq84gdr8k") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-hashmap_settings-0.4.1 (c (n "hashmap_settings") (v "0.4.1") (d (list (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)) (d (n "dyn_ord") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "typetag") (r "^0.2") (d #t) (k 0)))) (h "1dhvvk1vqrmm4b3j0gayy92p6nc70fd5yb8h8hm5p7xfbv1hl075") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-hashmap_settings-0.5.0 (c (n "hashmap_settings") (v "0.5.0") (d (list (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)) (d (n "dyn_ord") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "typetag") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1znwlzc1jxqx2d7r0cx6v6g89dg2l7p1q5ibkbw1i321fky95nh0") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:typetag"))))))

(define-public crate-hashmap_settings-0.5.1 (c (n "hashmap_settings") (v "0.5.1") (d (list (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)) (d (n "dyn_ord") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "typetag") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0nq9xx61w6h2mr2ppi7wd1mfscj7rr4v37yd354rcjnrvq256dd3") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:typetag"))))))

