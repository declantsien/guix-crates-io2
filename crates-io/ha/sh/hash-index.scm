(define-module (crates-io ha sh hash-index) #:use-module (crates-io))

(define-public crate-hash-index-0.1.0 (c (n "hash-index") (v "0.1.0") (d (list (d (n "blake3") (r "^0.1.0") (d #t) (k 0)))) (h "181jyyravzdfjcs9yy67xiaspnb2cs2ci6i864fwi4ra8y9iq62w")))

(define-public crate-hash-index-0.1.1 (c (n "hash-index") (v "0.1.1") (d (list (d (n "blake3") (r "^0.1.0") (d #t) (k 0)))) (h "0danqyx08jwcghlxk2kc4n2l61llmq0vb6m6dbrbhb2sf2bs03qw")))

