(define-module (crates-io ha sh hashcons) #:use-module (crates-io))

(define-public crate-hashcons-0.1.1 (c (n "hashcons") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h1w4sx6s9blzh1kdkqh872cljzlhmkima7gdv45z87ln51wb3pi")))

(define-public crate-hashcons-0.1.2 (c (n "hashcons") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fga6fkx0rs9ikh99d887crvlkwpg518cra5f7qslcanhc58n1aq")))

