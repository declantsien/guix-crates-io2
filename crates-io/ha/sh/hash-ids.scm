(define-module (crates-io ha sh hash-ids) #:use-module (crates-io))

(define-public crate-hash-ids-0.1.0 (c (n "hash-ids") (v "0.1.0") (h "14srrl281gw0gk8r2rqgv1469gyz8pyl3rb0zybrc53iqp7jnd3b")))

(define-public crate-hash-ids-0.2.0 (c (n "hash-ids") (v "0.2.0") (h "0fvinf3l6mc95qx4x0lk8vzg091g7rnh5np2s0vynwldglppx2pg")))

(define-public crate-hash-ids-0.2.1 (c (n "hash-ids") (v "0.2.1") (h "17dwd5021g9ng7p9rh98fx40s1g5gxzx3m4jbi82y72a0v4cp2cj")))

