(define-module (crates-io ha sh hashers) #:use-module (crates-io))

(define-public crate-hashers-1.0.0 (c (n "hashers") (v "1.0.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 2)))) (h "0nbqmx9w2rzpc8bhcdysgsxxpg5r3qdp1pzqqfw4qd2syjvp9zbi")))

(define-public crate-hashers-1.0.1 (c (n "hashers") (v "1.0.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 2)))) (h "0c1w7sanj02r4sgdz0xpx9jn2g77f5zmhmhf49pp8npa2lxskg5j")))

