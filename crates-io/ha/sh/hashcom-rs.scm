(define-module (crates-io ha sh hashcom-rs) #:use-module (crates-io))

(define-public crate-hashcom-rs-0.1.0 (c (n "hashcom-rs") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (d #t) (k 0)))) (h "1vfc9srgk8fk58jqxaw1lakbmdklsbv5zgzjxy3j9yqz4ydvpn0i")))

(define-public crate-hashcom-rs-0.2.0 (c (n "hashcom-rs") (v "0.2.0") (d (list (d (n "base16ct") (r "^0.1.1") (d #t) (k 2)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1z8scvb3kgclg7h30h42ip0bwq8pzfav00rnnv4bh7ca8zsx7ayn")))

