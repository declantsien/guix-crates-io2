(define-module (crates-io ha sh hashira-macros) #:use-module (crates-io))

(define-public crate-hashira-macros-0.0.1-alpha (c (n "hashira-macros") (v "0.0.1-alpha") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "167wnxmmak931sagzsfz3vmkpgbbjfcwwi8wc2b0avmv6s5gzfhs")))

(define-public crate-hashira-macros-0.0.2-alpha (c (n "hashira-macros") (v "0.0.2-alpha") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1zw37sznpf05s96rh1rpl6xvhjymwj5s20wp4qm941407chrkj2b")))

