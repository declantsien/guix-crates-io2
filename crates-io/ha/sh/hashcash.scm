(define-module (crates-io ha sh hashcash) #:use-module (crates-io))

(define-public crate-hashcash-0.1.0 (c (n "hashcash") (v "0.1.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0q99qmqyibx1qfj4rqmd1wrygib6sxfb8zw2z4cd0lzhz7fpjmg9")))

(define-public crate-hashcash-0.1.1 (c (n "hashcash") (v "0.1.1") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0h1fznp8is9q2dyzp8w1jhyx2zfh80sqa212899ysw45s5xkv7g3")))

