(define-module (crates-io ha sh hash_router) #:use-module (crates-io))

(define-public crate-hash_router-0.1.0 (c (n "hash_router") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "supabase_rs") (r "^0.2.2") (d #t) (k 0)))) (h "1510764d9dmi33jinz38m82y71n6gfzsyk9g2sw5qc7298xhdwxx")))

(define-public crate-hash_router-0.1.1 (c (n "hash_router") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "supabase_rs") (r "^0.2.6") (d #t) (k 0)))) (h "08g6pl7d04lyjhz57ysjlcfpva430sbkdhzrbzds79y18i6kbry9")))

(define-public crate-hash_router-0.1.2 (c (n "hash_router") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "supabase_rs") (r "^0.2.7") (d #t) (k 0)))) (h "1qmdjj45y30kv7ybpkhijnrmwqiafk528j7mzxllv09wi14w9fxj")))

