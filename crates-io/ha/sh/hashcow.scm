(define-module (crates-io ha sh hashcow) #:use-module (crates-io))

(define-public crate-hashcow-0.1.0 (c (n "hashcow") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.1.8") (f (quote ("rayon"))) (d #t) (k 0)))) (h "1i8vahb5l2g0dry51yf1hxl0hl5vxaphr6xnak2h90if4f3zbcwv")))

(define-public crate-hashcow-0.2.0 (c (n "hashcow") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.1.8") (f (quote ("rayon"))) (d #t) (k 0)))) (h "0ihc5cb3psgxnjap6hnj17spqpkr55janjfy94z2jrac0fxrbzm0")))

