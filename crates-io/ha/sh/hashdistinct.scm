(define-module (crates-io ha sh hashdistinct) #:use-module (crates-io))

(define-public crate-hashdistinct-0.1.0 (c (n "hashdistinct") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "11hxv0zfk40vwi81j2p3h3nb6lxqvl3majdkx745lh50p3njax6b")))

(define-public crate-hashdistinct-0.2.0 (c (n "hashdistinct") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "0fgf3mcadjkysm4s8iaiin28267vvc8d8al86gfhhs6lqvv4bag2")))

(define-public crate-hashdistinct-0.3.0 (c (n "hashdistinct") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "1ialdx1mqijvmzx023k6mkccl5dk2cm6hg7fxdif7q5bjlxg7zyy")))

(define-public crate-hashdistinct-0.3.1 (c (n "hashdistinct") (v "0.3.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "08pqrcpm2hc98r6ygmy4vljxn7lgdmlm3cmqcyl20689i3qxvfjg")))

(define-public crate-hashdistinct-0.3.3 (c (n "hashdistinct") (v "0.3.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "050398qak3k8jf713law4hddriv8mc425qsyifzb7b451bpr62v5")))

(define-public crate-hashdistinct-0.4.0 (c (n "hashdistinct") (v "0.4.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "00hlyis92mp8zm3nqvyl7ayvx4gwb31v567lhzj9ppw2g4jav8l3")))

