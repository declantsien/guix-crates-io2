(define-module (crates-io ha sh hashira-actix-web) #:use-module (crates-io))

(define-public crate-hashira-actix-web-0.0.1-alpha (c (n "hashira-actix-web") (v "0.0.1-alpha") (d (list (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashira") (r "^0.0.1-alpha") (d #t) (k 0)))) (h "05mvwd4rsjdpvkmz4fjidhpksirfwkxzkj8fqa4zn63wl6kwgqvd")))

(define-public crate-hashira-actix-web-0.0.2-alpha (c (n "hashira-actix-web") (v "0.0.2-alpha") (d (list (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hashira") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1bd63aarykx0q636x88g5pmhj2j9xcmf3gzk3xsvbaikqsdjm2vh")))

