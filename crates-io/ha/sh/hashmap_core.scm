(define-module (crates-io ha sh hashmap_core) #:use-module (crates-io))

(define-public crate-hashmap_core-0.1.0 (c (n "hashmap_core") (v "0.1.0") (h "1352lq9rjp8xwhgn3jw3ynsmp9y3z8jzv63vllczl933rm419agc")))

(define-public crate-hashmap_core-0.1.1 (c (n "hashmap_core") (v "0.1.1") (h "1wlhzb8hnfzfj4hyp46c4qd8p700ydp7xx3rhrk09n9r6wm7zn71") (f (quote (("disable"))))))

(define-public crate-hashmap_core-0.1.2 (c (n "hashmap_core") (v "0.1.2") (h "12fah9nlhqjldl7qmv3f2rfm5x8c1jy8bhlijkmcj7a2hlb9v4bm") (f (quote (("disable"))))))

(define-public crate-hashmap_core-0.1.3 (c (n "hashmap_core") (v "0.1.3") (h "0irblp4100pirny99gh38b6irqc83xxlkrr23vy1va2gzqhl98hq") (f (quote (("disable"))))))

(define-public crate-hashmap_core-0.1.4 (c (n "hashmap_core") (v "0.1.4") (h "0vi2vlyga9wijmb7fwl8ijxj7iqr8kq2755bipl9fy317azba3y6") (f (quote (("disable"))))))

(define-public crate-hashmap_core-0.1.5 (c (n "hashmap_core") (v "0.1.5") (h "0r2jizp6xisbpribcj1vi9n0sbag3fxfnsf3valmfwz7vaqsqksm") (f (quote (("disable"))))))

(define-public crate-hashmap_core-0.1.6 (c (n "hashmap_core") (v "0.1.6") (h "1ba6hmpqfyy555b8h6gp5c6djp2gp854q9bb9n46380fl47s1j7c") (f (quote (("disable"))))))

(define-public crate-hashmap_core-0.1.7 (c (n "hashmap_core") (v "0.1.7") (h "1k65wdi9d7b16z9xc2ldf5n01spkm1m2hqx777p9l7w614fba7mg") (f (quote (("disable"))))))

(define-public crate-hashmap_core-0.1.8 (c (n "hashmap_core") (v "0.1.8") (h "161lsdwxfwyf1k87ccyqkhlgk8b474mm25cm5pkv3hz1jpvcfamv") (f (quote (("disable"))))))

(define-public crate-hashmap_core-0.1.9 (c (n "hashmap_core") (v "0.1.9") (h "04cd57vgqsap6850dyjhlz5hwgz651jrpynyrv5sgqjmvvhli5aa") (f (quote (("disable"))))))

(define-public crate-hashmap_core-0.1.10 (c (n "hashmap_core") (v "0.1.10") (h "06l8np242mg4aksp83jj7nkwy7jq9mhcgy3rzbrhw9sia1xcn14f") (f (quote (("disable"))))))

(define-public crate-hashmap_core-0.1.11 (c (n "hashmap_core") (v "0.1.11") (h "0g4pfx0s06jgw3bhija702f8lv8nfybd7hdhfccm4l32m3jm4s1d") (f (quote (("disable"))))))

