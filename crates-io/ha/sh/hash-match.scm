(define-module (crates-io ha sh hash-match) #:use-module (crates-io))

(define-public crate-hash-match-1.0.0 (c (n "hash-match") (v "1.0.0") (d (list (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("const_xxh3" "xxh3"))) (d #t) (k 0)))) (h "12ch8kc2i34f7vjkkr0n2z7bcld6fw63vxy61szvb1xzzlsylqbl")))

(define-public crate-hash-match-1.0.1 (c (n "hash-match") (v "1.0.1") (d (list (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("const_xxh3" "xxh3"))) (d #t) (k 0)))) (h "1xzjm15csmkxqb8k658sy1zlrvknfj88jjmpbw11k4w837gm8aqw")))

