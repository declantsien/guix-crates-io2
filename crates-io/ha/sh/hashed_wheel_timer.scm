(define-module (crates-io ha sh hashed_wheel_timer) #:use-module (crates-io))

(define-public crate-hashed_wheel_timer-0.1.0 (c (n "hashed_wheel_timer") (v "0.1.0") (h "19pksq9gbxj6nvgvb35kcjrdibps4zcv5wkfjb2sj18rnv7b7w6d")))

(define-public crate-hashed_wheel_timer-0.1.1 (c (n "hashed_wheel_timer") (v "0.1.1") (h "032lyrwcnccb3r29hpv6l22vv8ralx4d422yqc9s4rkaq58n7y74")))

