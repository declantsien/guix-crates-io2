(define-module (crates-io ha sh hash-compress) #:use-module (crates-io))

(define-public crate-hash-compress-0.1.0 (c (n "hash-compress") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)))) (h "05y2cbv3y1ajyvwkfrlz7zsv8irnwgx82x4csljyhqqwg3y6hmfj")))

