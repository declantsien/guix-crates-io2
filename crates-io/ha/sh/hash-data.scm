(define-module (crates-io ha sh hash-data) #:use-module (crates-io))

(define-public crate-hash-data-0.1.0 (c (n "hash-data") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (f (quote ("serde_impl"))) (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 1)))) (h "1161jbnk6dfpgnlcapgzpal55r1zfssk3ysdzr2pilyma43mzj7a")))

(define-public crate-hash-data-0.2.0 (c (n "hash-data") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (f (quote ("serde_impl"))) (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 1)))) (h "10qk0hn6zmhaqpxh5j5dlhm069qw9w3w9g622g9r4ck52h79j3z2")))

(define-public crate-hash-data-0.3.0 (c (n "hash-data") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "0hfqa3jw6lq1f5vf34c4cs12y5kggk8a185r0hcan8qjrwpvngwi")))

