(define-module (crates-io ha sh hash32) #:use-module (crates-io))

(define-public crate-hash32-0.1.0 (c (n "hash32") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.2") (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 2)))) (h "1bw60v0ghk4vldq2zf8mz5z5n95f67iy3ycag5xnmh1rar1r1mqj") (f (quote (("const-fn"))))))

(define-public crate-hash32-0.1.1 (c (n "hash32") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.2") (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 2)))) (h "1k7lv7hsbzv14pz90cxay6v7avh6d6kcrra0rsc45b33dvw1l16l") (f (quote (("const-fn"))))))

(define-public crate-hash32-0.2.0 (c (n "hash32") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.2") (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 2)))) (h "1f57gpjhy138yrxpjphm7wwr579xvhph5bv6ysac54p7ydn4glzj")))

(define-public crate-hash32-0.2.1 (c (n "hash32") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.2") (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 2)))) (h "0rrbv5pc5b1vax6j6hk7zvlrpw0h6aybshxy9vbpgsrgfrc5zhxh")))

(define-public crate-hash32-0.3.0 (c (n "hash32") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2.2") (k 0)))) (h "08fcspxlkdz5wadg6lbmhvhn56mjmrkvbsyn5995j1qpijbriaib")))

(define-public crate-hash32-0.3.1 (c (n "hash32") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.2.2") (k 0)))) (h "01h68z8qi5gl9lnr17nz10lay8wjiidyjdyd60kqx8ibj090pmj7")))

