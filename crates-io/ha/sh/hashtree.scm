(define-module (crates-io ha sh hashtree) #:use-module (crates-io))

(define-public crate-hashtree-0.1.0 (c (n "hashtree") (v "0.1.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)))) (h "1z0dwvcy1dza40z22xp6ka3b1zg5jys6jn12skk36bpbi98zlwb7")))

(define-public crate-hashtree-0.1.1 (c (n "hashtree") (v "0.1.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)))) (h "0sxyn8n6x1bka5cydgskjmihdbk3fkc5nr3hj1bcjc88a5jma80g")))

(define-public crate-hashtree-0.2.1 (c (n "hashtree") (v "0.2.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 2)))) (h "0vdclwzmkxfpn6mlk5m2bnmzyvrax95qzrx12asa1bm7bqr7icdi")))

(define-public crate-hashtree-0.3.1 (c (n "hashtree") (v "0.3.1") (d (list (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "sha2") (r "^0.10.0") (d #t) (k 0)))) (h "0snpyxkdbnw5y0vyd0ncn2xdd1jw8y0cdg4k59z3077mya04aba1")))

(define-public crate-hashtree-0.4.1 (c (n "hashtree") (v "0.4.1") (d (list (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "sha2") (r "^0.10.0") (d #t) (k 0)))) (h "194rkzd2a1vpinwsxdbq2aqnwd8i2hvm0vnpp6228bpcjdwz5ja7")))

(define-public crate-hashtree-0.4.2 (c (n "hashtree") (v "0.4.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.0") (d #t) (k 0)))) (h "1fqw9vsl53439idfvrav0as1lj2i1zfyi0cpmxrd4b567h1j19yk")))

