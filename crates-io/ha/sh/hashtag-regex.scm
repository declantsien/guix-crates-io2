(define-module (crates-io ha sh hashtag-regex) #:use-module (crates-io))

(define-public crate-hashtag-regex-0.1.0 (c (n "hashtag-regex") (v "0.1.0") (d (list (d (n "emojic") (r "^0.4.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "18np2h8kddpx1isbamjspgmkwx7whjxf9f49ngcl68kv8ark5da8")))

(define-public crate-hashtag-regex-0.1.1 (c (n "hashtag-regex") (v "0.1.1") (d (list (d (n "emojic") (r "^0.4.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "167bhv738ni1kha1nfxyayj7f0fhy8h2dz3v98yd1mwv9bvzvcnr")))

