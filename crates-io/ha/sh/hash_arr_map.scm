(define-module (crates-io ha sh hash_arr_map) #:use-module (crates-io))

(define-public crate-hash_arr_map-0.1.0 (c (n "hash_arr_map") (v "0.1.0") (d (list (d (n "mjb_gc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1h7kih31yqyq7n65fs56h95yh1jl9hs7949mb61hlvyr7vcvjmp2") (f (quote (("std") ("gc" "mjb_gc") ("default" "std"))))))

(define-public crate-hash_arr_map-0.2.0 (c (n "hash_arr_map") (v "0.2.0") (d (list (d (n "mjb_gc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "15p8c22w7zma0czav2ndskq8hb8xjaarm8687v6kl63m6796w9j2") (f (quote (("std") ("gc" "mjb_gc") ("default" "std"))))))

(define-public crate-hash_arr_map-0.3.0 (c (n "hash_arr_map") (v "0.3.0") (d (list (d (n "fastrand") (r "^1.4") (d #t) (k 0)) (d (n "mjb_gc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1d6i2vfmgqwi56iq188dyscj4pz27da50vpj976mkj0l857ygp1h") (f (quote (("std") ("gc" "mjb_gc") ("default" "std"))))))

(define-public crate-hash_arr_map-0.3.1 (c (n "hash_arr_map") (v "0.3.1") (d (list (d (n "fastrand") (r "^1") (d #t) (k 0)) (d (n "mjb_gc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0pfmm2vhzd5nd1h3yq1d0zylz6sxw46x3rdacm5svp7xg366aa7m") (f (quote (("std") ("gc" "mjb_gc") ("default" "std"))))))

(define-public crate-hash_arr_map-0.4.0 (c (n "hash_arr_map") (v "0.4.0") (d (list (d (n "bacon_rajan_cc") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "fastrand") (r "^1") (o #t) (d #t) (k 0)) (d (n "imm_gc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "17jvhaanai4gdp8gqhlncns54armwcvjb9wq9i189ywc117mjwy6") (f (quote (("std" "fastrand") ("default" "std"))))))

