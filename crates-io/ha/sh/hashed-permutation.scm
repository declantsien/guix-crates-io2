(define-module (crates-io ha sh hashed-permutation) #:use-module (crates-io))

(define-public crate-hashed-permutation-1.0.0 (c (n "hashed-permutation") (v "1.0.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "13smxxjmrfhhc0s3xswbwp3232w37lw6qbavv5n4ndq32b6jb140")))

(define-public crate-hashed-permutation-2.0.0 (c (n "hashed-permutation") (v "2.0.0") (d (list (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1bjvhmxy7k8myb40jaipg5czfmjbicgzyq2nc1xx17yyj950p18v") (f (quote (("failure-crate" "failure") ("default"))))))

(define-public crate-hashed-permutation-2.1.0 (c (n "hashed-permutation") (v "2.1.0") (d (list (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)))) (h "16h7l8yh04crgi977kpv2m35a7b0ir7y0c4s18yy6893lzgq98af") (f (quote (("use-rand" "rand") ("failure-crate" "failure") ("default"))))))

(define-public crate-hashed-permutation-2.1.1 (c (n "hashed-permutation") (v "2.1.1") (d (list (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0qcxdd136v60gmagd2d9dq4lqiry6177ig5b907k9p2556ch3p3f") (f (quote (("use-rand" "rand") ("failure-crate" "failure") ("default"))))))

(define-public crate-hashed-permutation-2.2.0 (c (n "hashed-permutation") (v "2.2.0") (d (list (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0qj0rhs8yczynikbqak5gbnncf3zmil9lz6vhn9dc93lyk0dn226") (f (quote (("use-rand" "rand") ("failure-crate" "failure") ("default"))))))

(define-public crate-hashed-permutation-3.0.0 (c (n "hashed-permutation") (v "3.0.0") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rz921h7zz5l14g9d7x9di56c56y2yn6sc052j6m1gl4d4kmsp2j") (f (quote (("use-rand" "rand") ("default"))))))

(define-public crate-hashed-permutation-3.0.1 (c (n "hashed-permutation") (v "3.0.1") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g6igf04xdv7ywhby1kad31q0jhv34km8bn6p5blq1i33wbgji00") (f (quote (("use-rand" "rand") ("default"))))))

(define-public crate-hashed-permutation-3.0.2 (c (n "hashed-permutation") (v "3.0.2") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l5j819qa9v9ysh5jn9bl3kfhbh7jmzjziz8g7s6bxls1mskfy41") (f (quote (("use-rand" "rand") ("default"))))))

