(define-module (crates-io ha sh hashes) #:use-module (crates-io))

(define-public crate-hashes-0.1.0 (c (n "hashes") (v "0.1.0") (h "12667wvswp0nw777dzhsmywbpg6sfvdnaximjhr4n6k9cq103ad3") (f (quote (("test_big_data"))))))

(define-public crate-hashes-0.1.1 (c (n "hashes") (v "0.1.1") (h "1l7il2l50mxp8hjaj4mj38xz7h85r911g5dcqqx9km9wj1rcprph") (f (quote (("test_big_data"))))))

(define-public crate-hashes-0.1.2 (c (n "hashes") (v "0.1.2") (h "06s2njc5gp7102nqad56z35kaf6wlibiy5zrz41vxy9b85qwzxwf") (f (quote (("test_big_data"))))))

(define-public crate-hashes-0.1.3 (c (n "hashes") (v "0.1.3") (h "1lg0rkd0gzax9a6s6vf2562zll8gnri3n8ncv4isjz5pgdivrm9j") (f (quote (("test_big_data"))))))

(define-public crate-hashes-0.1.4 (c (n "hashes") (v "0.1.4") (h "0vi1s2vz2yarf05qwr0drz35lc2xsjmbhci3pinxqia8gdrnpkm6") (f (quote (("test_big_data"))))))

(define-public crate-hashes-0.1.5 (c (n "hashes") (v "0.1.5") (h "1b1lwawgvsjdvrlkl65vh3hfxqq9cw9535c5pkmcap4xfag79yak") (f (quote (("test_big_data"))))))

(define-public crate-hashes-0.1.6 (c (n "hashes") (v "0.1.6") (h "1nnnkczy3janq922j2qsd5c1lsi8lhpx14x5lgdh4bmw7pb91bn3") (f (quote (("test_big_data") ("std"))))))

(define-public crate-hashes-0.1.7 (c (n "hashes") (v "0.1.7") (h "0vz09jm7ibk02wb78wgx6wh776g069whww13r5yc9xznlh5jzmwm") (f (quote (("test_big_data") ("std"))))))

(define-public crate-hashes-0.1.8 (c (n "hashes") (v "0.1.8") (h "0nmjk77h8q0dmxby70s6apwbjygamfqrx3z97gwhlmpw8mlnk4jh") (f (quote (("test_big_data") ("std"))))))

(define-public crate-hashes-0.1.9 (c (n "hashes") (v "0.1.9") (h "0wr8dz747kawfybrsrhkiaay5q443hid0p6mzzs7f6x5rg5mrqha") (f (quote (("test_big_data") ("std"))))))

