(define-module (crates-io ha sh hashmap_vec) #:use-module (crates-io))

(define-public crate-hashmap_vec-0.1.0 (c (n "hashmap_vec") (v "0.1.0") (h "03hv87bv9cpx7x00wnqdrz82a55l44h5bc6qw59qvi374yh23ixf") (y #t)))

(define-public crate-hashmap_vec-0.1.1 (c (n "hashmap_vec") (v "0.1.1") (h "0jwm270194ayidz7dlfws6lqmn429dx5d44kjn6zwk7gvsljlf71")))

