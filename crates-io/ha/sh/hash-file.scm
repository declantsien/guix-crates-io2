(define-module (crates-io ha sh hash-file) #:use-module (crates-io))

(define-public crate-hash-file-0.0.1 (c (n "hash-file") (v "0.0.1") (d (list (d (n "sha256") (r "^1.1.1") (d #t) (k 0)))) (h "1g52bv9mqscim0n7i5cfzk1akln3mfxanwxpmfxmn3xxb1il1f2w")))

(define-public crate-hash-file-0.0.2 (c (n "hash-file") (v "0.0.2") (d (list (d (n "sha256") (r "^1.1.1") (d #t) (k 0)))) (h "0wxckagki2fn5s6353k1zhci3a1pmjx6qqg1b83vd7vwwmfgs91p")))

(define-public crate-hash-file-0.0.3 (c (n "hash-file") (v "0.0.3") (d (list (d (n "sha256") (r "^1.1.1") (d #t) (k 0)))) (h "122mqr3bmnl9rzpa807wbd5z791khlkmw6xz6n1pn9g5dc2wqzpb")))

(define-public crate-hash-file-0.0.4 (c (n "hash-file") (v "0.0.4") (d (list (d (n "sha256") (r "^1.1.1") (d #t) (k 0)))) (h "06ss9nra2zbh228q4fd3vyqcsv4brg59c4pv7vfxyxlvpv7jp0wd")))

