(define-module (crates-io ha sh hash-trie) #:use-module (crates-io))

(define-public crate-hash-trie-0.0.1 (c (n "hash-trie") (v "0.0.1") (h "1zcl6dlz1ak2bdisasmikr49klh63bwv707skb82qbh4bmjzl52x") (y #t)))

(define-public crate-hash-trie-0.1.0 (c (n "hash-trie") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "11h0vbaq1if837rc153cc0xiccvv3ndlfr2k7sdhsv7vd04jzh8p") (y #t)))

(define-public crate-hash-trie-0.2.0 (c (n "hash-trie") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "13069sigqg7hx5anvhgkxjk5frj12zxqdh1xn22s3a93l3fvgnqj") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-hash-trie-0.3.0 (c (n "hash-trie") (v "0.3.0") (d (list (d (n "im") (r "^15.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1pv6mdkpzp4dh2dahn8gpd89ajyadzvxhd1wfdng9yhjhyd50j5i") (f (quote (("std") ("default"))))))

(define-public crate-hash-trie-0.4.0 (c (n "hash-trie") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)))) (h "1cvk4rwk6zdw027myx0a894nnycbscgivanw9gi1rfam0lgiqbay") (f (quote (("parallel" "rayon") ("default"))))))

