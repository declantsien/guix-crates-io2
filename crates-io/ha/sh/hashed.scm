(define-module (crates-io ha sh hashed) #:use-module (crates-io))

(define-public crate-hashed-0.1.0 (c (n "hashed") (v "0.1.0") (h "1nkyv5h146xr2p470fj3x3j1ivbfmhq4xh47ll486bd3i9irwpk7") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-hashed-0.1.1 (c (n "hashed") (v "0.1.1") (h "13i22za11vy768fg83pjmv05c408myi567awyx4ic4nawdm61jra") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-hashed-0.2.0 (c (n "hashed") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0bng0yx3dqfdw30415zw15652fvszzss7fl7gb4ddv3r0lvw1yp2") (f (quote (("truncate" "num-traits") ("std") ("nightly") ("default" "std"))))))

(define-public crate-hashed-0.2.1 (c (n "hashed") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "1b14hcw9ymcdgxd5za9prlsbsvmsx0nk0n1v6sffs51ibsxjvk2w") (f (quote (("truncate" "num-traits") ("std") ("nightly") ("default" "std"))))))

