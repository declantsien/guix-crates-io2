(define-module (crates-io ha sh hashconsing) #:use-module (crates-io))

(define-public crate-hashconsing-0.2.1 (c (n "hashconsing") (v "0.2.1") (h "16g8kgdzqx927r9lx7iifmirk4ca8idjfxridsy3wfl4vz084y99") (y #t)))

(define-public crate-hashconsing-0.3.0 (c (n "hashconsing") (v "0.3.0") (h "1hkhjn5gd9w14d0f0jg9wgmjdillhxb6drz4bydvz00ih8vx6df0") (y #t)))

(define-public crate-hashconsing-0.4.0 (c (n "hashconsing") (v "0.4.0") (h "0f1jgyg5z5h4p38p8r29c893w690s4bdrlxs50imfiirhb5pirvc") (y #t)))

(define-public crate-hashconsing-0.5.0 (c (n "hashconsing") (v "0.5.0") (h "0sxp5lhxdc76fr5lxgk2h5ramw4f0ccslhxiz8hqp91njm6fmai9") (y #t)))

(define-public crate-hashconsing-0.5.1 (c (n "hashconsing") (v "0.5.1") (h "0j9840ivaah9gqbj8s8nw01biw16szyrgjigdag738axdfb6z0v7")))

(define-public crate-hashconsing-0.9.0 (c (n "hashconsing") (v "0.9.0") (h "0hfflr06s0syr9vfap3dxflw272gddg8mv5w1i67dhjba3ynzs0m")))

(define-public crate-hashconsing-0.9.9 (c (n "hashconsing") (v "0.9.9") (h "1qqix8xlk558km1zka8rkwfbcm9zbrjqw39fhpwbapalpv0rqm7g")))

(define-public crate-hashconsing-0.9.11 (c (n "hashconsing") (v "0.9.11") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "0md02vr1kmyim4kji7a6y3vxwj2k0x8d1d8273zx3yiiw2lf0cic")))

(define-public crate-hashconsing-0.10.0 (c (n "hashconsing") (v "0.10.0") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "09iq4l01xglacsz0llnwfzwp840z6rkfs36ngwpksvr4vb67g7h1")))

(define-public crate-hashconsing-0.10.1 (c (n "hashconsing") (v "0.10.1") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "034vh85fmx08karpgpwamqqgxp1rwa3k65f1yid837jn73h43hmi")))

(define-public crate-hashconsing-0.10.3 (c (n "hashconsing") (v "0.10.3") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "16816b0ymjlkp1pg7ppaqsqnqjq1a96174vlicicsi8vyn5hff8g")))

(define-public crate-hashconsing-1.0.0 (c (n "hashconsing") (v "1.0.0") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "1a99cgc7fa3whqdr33ljqy6h3037zln1fiwyxk41d52qv2qb70dk")))

(define-public crate-hashconsing-1.0.1 (c (n "hashconsing") (v "1.0.1") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "0drxgg0w29gbgl5h0gd8bdcplbfj8mfy8wnm88valqawk3nr3fq7")))

(define-public crate-hashconsing-1.1.0 (c (n "hashconsing") (v "1.1.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ipma0qb6xwqn546y6bk86kgwkhdc87j2is2vfhxpwkk1fk7m5cd")))

(define-public crate-hashconsing-1.2.0 (c (n "hashconsing") (v "1.2.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0gpmjnd8yk8dlhdx4kdf18kk2nrd6sqkn4qczbmi63h0ynpgp0q1")))

(define-public crate-hashconsing-1.3.0 (c (n "hashconsing") (v "1.3.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0x90nyfaamfrr9mbfdb4g3g2248scxhac1b00xksiq995ak98igc")))

(define-public crate-hashconsing-1.5.0 (c (n "hashconsing") (v "1.5.0") (d (list (d (n "ahash") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0m46imfqwm0gfyzbff2d91zrcvyzj273ppibyhi8jgaybrhsmnna") (f (quote (("with_ahash" "ahash"))))))

(define-public crate-hashconsing-1.5.1 (c (n "hashconsing") (v "1.5.1") (d (list (d (n "ahash") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ha66wzxvn8l9azdppiz69q8rfp0cnf1g7dz9xiy9scic2lnqrx7") (f (quote (("with_ahash" "ahash") ("unstable_docrs" "with_ahash"))))))

(define-public crate-hashconsing-1.6.0 (c (n "hashconsing") (v "1.6.0") (d (list (d (n "ahash") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "08w2cislrvkm4zy6lhxldmbwi9fcr3jk0mw0zzrvrp6cmm4wzdax") (f (quote (("with_ahash" "ahash") ("unstable_docrs" "with_ahash"))))))

