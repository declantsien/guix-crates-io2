(define-module (crates-io ha sh hash-checker) #:use-module (crates-io))

(define-public crate-hash-checker-0.1.4 (c (n "hash-checker") (v "0.1.4") (d (list (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "0b5vsbwk2aqkx9m5ssvyknyb4iyk4b3277x81l5vr6wbgp407cv6")))

