(define-module (crates-io ha sh hash_avatar) #:use-module (crates-io))

(define-public crate-hash_avatar-0.1.1 (c (n "hash_avatar") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1zln1paw6bn7fjzb4jvv26qjphc5216f844mh0lfnmx72m03w3dq")))

(define-public crate-hash_avatar-0.1.2 (c (n "hash_avatar") (v "0.1.2") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0nkzp149jkvl1rv6p8ddjbb6j14zpiz0hrp49xz3cj16xivgns0y")))

(define-public crate-hash_avatar-0.1.3 (c (n "hash_avatar") (v "0.1.3") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1qcjsxf4n3148slvcdnvwxgn8xr34sszr3n7xwhwpivmnwqd4rk0")))

(define-public crate-hash_avatar-0.1.5 (c (n "hash_avatar") (v "0.1.5") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0wm5xmw9vynwk0pfda4hcfl4ilx407hiav2y839nw6419lzavkcl")))

