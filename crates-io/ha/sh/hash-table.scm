(define-module (crates-io ha sh hash-table) #:use-module (crates-io))

(define-public crate-hash-table-0.1.0 (c (n "hash-table") (v "0.1.0") (d (list (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "05v1mhdjhljydw114iz8d2jda56ngqswgfvgyqzca696n7ikip7i")))

(define-public crate-hash-table-0.2.0 (c (n "hash-table") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)) (d (n "slot") (r "^0.2") (d #t) (k 0)))) (h "0h3wpig117giks9pyhna5b36q8zxgn1q2411wpm04x2xbfiyz7c9")))

(define-public crate-hash-table-0.2.1 (c (n "hash-table") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)) (d (n "slot") (r "^0.2") (d #t) (k 0)))) (h "1nwb6r4nnadydwa00s951hh5ir0i4h662wh0lx1rcnvlr8z7vldr")))

(define-public crate-hash-table-0.2.2 (c (n "hash-table") (v "0.2.2") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)) (d (n "slot") (r "^0.2") (d #t) (k 0)))) (h "0lc37ykc3ixa0wk7xiw6idqkz85z1z7bgf8s3hcl0cjb366zcl7b")))

(define-public crate-hash-table-0.2.3 (c (n "hash-table") (v "0.2.3") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "1iwzyjhw801bkcnwcczdf1xsb6yz6w3iqnz2maiiqfk0jvlpd9sn")))

(define-public crate-hash-table-0.2.4 (c (n "hash-table") (v "0.2.4") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "1v8k70bmbim7f8wcy6nd11zvv5mrbfzzk47yz598cc40vj9b7wb7")))

(define-public crate-hash-table-0.2.5 (c (n "hash-table") (v "0.2.5") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "191y5sw6mf0d9kyssqjyaik7z3r7flr4dq63mz025d79b1f6dyn7")))

