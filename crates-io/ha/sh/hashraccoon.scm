(define-module (crates-io ha sh hashraccoon) #:use-module (crates-io))

(define-public crate-hashraccoon-0.0.1 (c (n "hashraccoon") (v "0.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)))) (h "0fwkcxf17bhj7d5nsq5b90p754ih49hxic3y2384bir11grrb0fp") (y #t)))

(define-public crate-hashraccoon-0.0.2 (c (n "hashraccoon") (v "0.0.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)))) (h "0gxljmmdbz61kzvb9gvgkkq66dfl8aj17ds3zslg1lq7fzc62pg3")))

