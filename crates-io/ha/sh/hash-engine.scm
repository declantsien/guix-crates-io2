(define-module (crates-io ha sh hash-engine) #:use-module (crates-io))

(define-public crate-hash-engine-0.2.0-alpha.0 (c (n "hash-engine") (v "0.2.0-alpha.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "engula-journal") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "engula-kernel") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "engula-storage") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "01b3pkgf5xnsv5n0rsmac6rwa3i569v0zsi4vbslff1d741i0dcw")))

(define-public crate-hash-engine-0.2.0 (c (n "hash-engine") (v "0.2.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "engula-journal") (r "^0.2") (d #t) (k 0)) (d (n "engula-kernel") (r "^0.2") (d #t) (k 0)) (d (n "engula-storage") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)))) (h "02bywrnpc29nas20qq4j8l883cr8yglawi6dcq95w4qw1snbar9q")))

