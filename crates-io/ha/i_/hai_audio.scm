(define-module (crates-io ha i_ hai_audio) #:use-module (crates-io))

(define-public crate-hai_audio-0.6.0 (c (n "hai_audio") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arc-swap") (r "^1.6") (d #t) (k 2)) (d (n "hai_core") (r "^0.6") (f (quote ("js_runtime"))) (k 0)) (d (n "hai_pal") (r "^0.6") (d #t) (k 0)) (d (n "kira") (r "^0.8.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0318i82rs864va2z4ac0jwwqxpyawgw51rmx6cn9psa2yi0figsm") (f (quote (("v8" "hai_core/v8") ("quickjs" "hai_core/quickjs") ("default" "quickjs"))))))

