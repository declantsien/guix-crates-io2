(define-module (crates-io ha i_ hai_macros) #:use-module (crates-io))

(define-public crate-hai_macros-0.1.0 (c (n "hai_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i37xp5brf2g2kyibk478lyx55h7d2rqwszrj1ydippxxryc9ljk")))

(define-public crate-hai_macros-0.1.1 (c (n "hai_macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00a2cfgcpzbdv17cj8lrsbhsib46midvcn6pzwp8my790dx4phd6")))

(define-public crate-hai_macros-0.2.0 (c (n "hai_macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "19c7b9msf04y2vf66il26prazlm988hpr4n52ya9w97rddfa952d") (f (quote (("v8") ("quickjs") ("default"))))))

(define-public crate-hai_macros-0.3.0 (c (n "hai_macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "11cijcs0277y7gxp8cnkbfxj4w282lsdmz1ypabdyvn9nsgsy3a8") (f (quote (("v8") ("quickjs") ("default"))))))

(define-public crate-hai_macros-0.4.0 (c (n "hai_macros") (v "0.4.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1wpfaxbylid00cic5xzwvwjxfmyqldxyfhhp41n4mx8grw40c53h") (f (quote (("v8") ("quickjs") ("default"))))))

