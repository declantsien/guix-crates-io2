(define-module (crates-io ha lo halo2_summarizer) #:use-module (crates-io))

(define-public crate-halo2_summarizer-0.1.0 (c (n "halo2_summarizer") (v "0.1.0") (h "1i9amvxv7xiiw55xcynazq528795xvbxrx562d3cw7kgr91n1wyp")))

(define-public crate-halo2_summarizer-0.1.1 (c (n "halo2_summarizer") (v "0.1.1") (h "0m9c9hmsny4dxi5yjsh4klqg3xddi6r3rf8gn0bkcaiwlhp3adbi")))

