(define-module (crates-io ha lo halo2) #:use-module (crates-io))

(define-public crate-halo2-0.0.0 (c (n "halo2") (v "0.0.0") (h "027kq5d7l6d712yl7zy4mm8va15mf15cy8b6vpk2rqwbi5yaxqzp")))

(define-public crate-halo2-0.1.0-beta.1 (c (n "halo2") (v "0.1.0-beta.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ff") (r "^0.11") (d #t) (k 0)) (d (n "group") (r "^0.11") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 2)) (d (n "pasta_curves") (r "^0.2.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tabbycat") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "1agkf3jpbl1q5clivijakbl1aagh240jpmarryqjy241xn2nn60g") (f (quote (("sanity-checks") ("gadget-traces" "backtrace") ("dev-graph" "plotters" "tabbycat"))))))

(define-public crate-halo2-0.1.0-beta.2 (c (n "halo2") (v "0.1.0-beta.2") (d (list (d (n "halo2_proofs") (r "^0.1.0-beta.2") (d #t) (k 0)))) (h "15armrhslknn33pc9lk1b94phlxxsm5a8b8h70azwlw2ndwwf8ra")))

