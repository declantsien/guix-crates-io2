(define-module (crates-io ha lo halo2_legacy_pdqsort) #:use-module (crates-io))

(define-public crate-halo2_legacy_pdqsort-0.0.0 (c (n "halo2_legacy_pdqsort") (v "0.0.0") (h "0h5rgzxdwa0g14k1yx5p153d3238ya8xv3db295y8b5w5fa08p9l")))

(define-public crate-halo2_legacy_pdqsort-0.1.0 (c (n "halo2_legacy_pdqsort") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1iayayshc7cmq6iilpijprrbxhxq5prjdy1f1dg9r5k7mvhnywa7")))

