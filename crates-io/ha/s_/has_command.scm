(define-module (crates-io ha s_ has_command) #:use-module (crates-io))

(define-public crate-has_command-0.1.0 (c (n "has_command") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0asf9gahps8kbx910j048cf0rh4f4wrwbq97wbx8zk36daf8pwh5")))

