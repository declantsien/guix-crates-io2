(define-module (crates-io ha s_ has_fields) #:use-module (crates-io))

(define-public crate-has_fields-0.1.0 (c (n "has_fields") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01l8bs35i3vphv79qcx0lf98ym8glrc1ngwhrviddicc8h3rqidn")))

(define-public crate-has_fields-0.1.1 (c (n "has_fields") (v "0.1.1") (d (list (d (n "has_fields_macros") (r "^0.1.0") (d #t) (k 0)))) (h "139p40n9r8ajg7kr5ibd9blwzj3x2gkc86s3miy62i8i7wmvw93p")))

