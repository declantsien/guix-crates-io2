(define-module (crates-io ha s_ has_flag) #:use-module (crates-io))

(define-public crate-has_flag-0.1.0 (c (n "has_flag") (v "0.1.0") (h "1yk9kfxlw2ww4hlwq5vcz6bmc6n34miismk6mw42y90b5105vlhi") (y #t)))

(define-public crate-has_flag-0.1.1 (c (n "has_flag") (v "0.1.1") (h "02dnpqi6wznj6mr979p59j3a7dwcpjqk1klkz5wpg8lfc08sxax4")))

