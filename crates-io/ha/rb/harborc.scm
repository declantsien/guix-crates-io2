(define-module (crates-io ha rb harborc) #:use-module (crates-io))

(define-public crate-harborc-0.1.0 (c (n "harborc") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "comment") (r "^0.1.1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)))) (h "0xsl7vm4kdz9ba6dkl0vkfy7hr14vbgk3dm21bb5240z1z6v9d43")))

(define-public crate-harborc-0.1.1 (c (n "harborc") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "comment") (r "^0.1.1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)))) (h "0llwfxx83l0scq3knlfr0l0d95nxlx2yh1vrzndg9j9as8sid8r1")))

