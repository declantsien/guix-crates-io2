(define-module (crates-io ha nk hankblminigrep) #:use-module (crates-io))

(define-public crate-hankblminigrep-0.1.0 (c (n "hankblminigrep") (v "0.1.0") (h "182kyfp7icn5vf7rbh12zqgvka916h3mv0vlz7w9xi6g5s69zgng")))

(define-public crate-hankblminigrep-0.1.1 (c (n "hankblminigrep") (v "0.1.1") (h "1zmb5d651hi1p6izg4l49qqb0pbl67c9k8a839i1dnjmvsp3qnaz")))

