(define-module (crates-io ha do hadoop) #:use-module (crates-io))

(define-public crate-hadoop-0.0.3 (c (n "hadoop") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "atomic") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "iref") (r "^3.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.3") (d #t) (k 1)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "016payaqajq4aicj8kxaxhqvq4xvvpyly5a8z4wpapm48f4jmhj3")))

