(define-module (crates-io ha do hadolint-sarif) #:use-module (crates-io))

(define-public crate-hadolint-sarif-0.2.10 (c (n "hadolint-sarif") (v "0.2.10") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.10") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1wmb3isn9s9vxp24xsv7pazky23dhi28k4w0ljasrz52gap8hq84")))

(define-public crate-hadolint-sarif-0.2.11 (c (n "hadolint-sarif") (v "0.2.11") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.11") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "011jd7zwg24wxyn657rl6010l8d122699l275qs7gdc12d14z0sv")))

(define-public crate-hadolint-sarif-0.2.12 (c (n "hadolint-sarif") (v "0.2.12") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.12") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ip4lijyh7ygr0vc5h94v28n4g3ddga8vxkchrsvayk619i8mhga")))

(define-public crate-hadolint-sarif-0.2.14 (c (n "hadolint-sarif") (v "0.2.14") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.14") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0vhzi16dpf447iz9c75fqc0jbqb1d9diffzgnvb9rafizqxrg8zj")))

(define-public crate-hadolint-sarif-0.2.15 (c (n "hadolint-sarif") (v "0.2.15") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.15") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "06rp652nijp6q7fib4bz1cbs6jrc6b648lk5ifipw6kgxqxkwqmz")))

(define-public crate-hadolint-sarif-0.2.17 (c (n "hadolint-sarif") (v "0.2.17") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.17") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "19ri0k4rd4wvih4j6y6vw1ss94zpgws29gr44766k34ada8kji3y")))

(define-public crate-hadolint-sarif-0.2.19 (c (n "hadolint-sarif") (v "0.2.19") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.19") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "12si2yhy200glp40azn3hgzcm6psi03gax4gmjyzzczp9hpgfm9h")))

(define-public crate-hadolint-sarif-0.2.20 (c (n "hadolint-sarif") (v "0.2.20") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.20") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0r0apgydpzkhn1cpz5bsakfwjxw9j7sz75mf8msqr2adrjjfyplm")))

(define-public crate-hadolint-sarif-0.2.21 (c (n "hadolint-sarif") (v "0.2.21") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.7") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.21") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0qj9p12mcdvrlvxr1n720z3i3jcgb7xwd7fzxn8xn5r33phlx0m8")))

(define-public crate-hadolint-sarif-0.2.22 (c (n "hadolint-sarif") (v "0.2.22") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.22") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "01hnndh5jdvyr4ap4ha0c3wwg3y1ixsnl1hgzams0261f155q42m")))

(define-public crate-hadolint-sarif-0.2.24 (c (n "hadolint-sarif") (v "0.2.24") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.24") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0vslrl9nipgjsamkz1j0i76xgg3ffxvbn3xpf18g55i3npmfyfs2")))

(define-public crate-hadolint-sarif-0.2.25 (c (n "hadolint-sarif") (v "0.2.25") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.25") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ixjpjh3232mcpfxp336indp4bir1k14sa9yniwl7bgpm7qmp7gg")))

(define-public crate-hadolint-sarif-0.3.0 (c (n "hadolint-sarif") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.10") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.0") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1c97519zy5zvzjhwnzpizng88k2vi0q8al2xn2jx4pfm75nrkzzq")))

(define-public crate-hadolint-sarif-0.3.1 (c (n "hadolint-sarif") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.1") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "09x8yj80dbhhzvpq2swc70nqxky3wdmndxiccl5lz5rlrljvp4l9")))

(define-public crate-hadolint-sarif-0.3.2 (c (n "hadolint-sarif") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.2") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0rwdg5c254yj0plss1f01kai2z7nm0jzrya66imng3w0y1nc6833")))

(define-public crate-hadolint-sarif-0.3.3 (c (n "hadolint-sarif") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.3") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "024d1b711yf5vhgwp1i2ifih1sglfvhzr7v82v3jfjmjd9lj2xyc")))

(define-public crate-hadolint-sarif-0.3.4 (c (n "hadolint-sarif") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.4") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1rim8pk0adkdsp3mbpn4ymv56hkyfig68lh4vb4pg0kj8hhlpnyv")))

(define-public crate-hadolint-sarif-0.3.5 (c (n "hadolint-sarif") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.5") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0k30p536qncgf0cnf4z5bjm00gqcn3zjqsbp6amqwnfrn7fjpyjs")))

(define-public crate-hadolint-sarif-0.3.6 (c (n "hadolint-sarif") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.6") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1kfs98g1w4alk2apvjn7889vngvzpfa5pwakzaa479cws7fqbxy4")))

(define-public crate-hadolint-sarif-0.3.7 (c (n "hadolint-sarif") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.7") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0b4kl07d4qcg7i8cnjkd8bx8pclv9i8zlg2srlhii1zzq5gni47q")))

(define-public crate-hadolint-sarif-0.4.0 (c (n "hadolint-sarif") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.0") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "05rcxrizzs3p02bi8zazq3zkazxzpkvqlqsfhpchzl65963b8859")))

(define-public crate-hadolint-sarif-0.4.1 (c (n "hadolint-sarif") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.1") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0k7bvc27khky1qpd1vhjmq5qwbxcgpbv8zw2d05j6nv9skckh0ql")))

(define-public crate-hadolint-sarif-0.4.2 (c (n "hadolint-sarif") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.2") (f (quote ("hadolint-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "16rmzxx9alxsgwncrbfxa66q0qscqsqmzc6c7rj8i4m0lifgbwaa")))

