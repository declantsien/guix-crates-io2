(define-module (crates-io ha do hadoop-hdfs) #:use-module (crates-io))

(define-public crate-hadoop-hdfs-0.0.2 (c (n "hadoop-hdfs") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hadoop-common") (r "^0.0.2") (d #t) (k 0)) (d (n "hadoop-hdfs-client") (r "^0.0.2") (d #t) (k 0)) (d (n "iref") (r "^2.2.3") (d #t) (k 0)))) (h "0cdl816scgjl12q5f3dxjx6ym6qfw2ak43v0ynvr1rm7cl0xn872")))

