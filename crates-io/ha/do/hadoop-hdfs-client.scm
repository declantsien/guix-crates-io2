(define-module (crates-io ha do hadoop-hdfs-client) #:use-module (crates-io))

(define-public crate-hadoop-hdfs-client-0.0.2 (c (n "hadoop-hdfs-client") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "atomic") (r "^0.6.0") (d #t) (k 0)) (d (n "hadoop-common") (r "^0.0.2") (d #t) (k 0)) (d (n "hadoop-proto") (r "^0.0.2") (d #t) (k 0)) (d (n "iref") (r "^2.2.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0rhbfqwhi4jqqn9cyl50h6kzac74gxjvxr3w9wi2xh1jd6rn4nnh")))

