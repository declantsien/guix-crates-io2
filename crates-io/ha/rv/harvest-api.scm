(define-module (crates-io ha rv harvest-api) #:use-module (crates-io))

(define-public crate-harvest-api-0.1.0 (c (n "harvest-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "054m6g9w5z9qs2jqdr0ryhyf7xzdjdq2dn6rw6anbrhsj6qcb5c6")))

(define-public crate-harvest-api-1.0.0 (c (n "harvest-api") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1mdvb0p4vckcckxrhkqcna15fv3ncjhakfs878nxs579qadnlgdk")))

(define-public crate-harvest-api-2.0.0 (c (n "harvest-api") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "17mnys5mc0f2g4if3dab3j82g24y8zyz75s7hkipyfqpwsy5s2ma")))

(define-public crate-harvest-api-3.0.0 (c (n "harvest-api") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "02ka841mhbfssw9myrs2cs66g4w1iv1c1cpalnb9n9xdyml2qzpw")))

(define-public crate-harvest-api-4.0.0 (c (n "harvest-api") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "111j1dcn4r6x01ylvkjn2jq05dj7433kr86dap6xn3viwb5is2h7")))

(define-public crate-harvest-api-5.0.0 (c (n "harvest-api") (v "5.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1lq1qjg5qf1b465p1zskv3x3h80kp7njiizxbb750mywwjrc2m4h")))

