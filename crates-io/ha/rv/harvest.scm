(define-module (crates-io ha rv harvest) #:use-module (crates-io))

(define-public crate-harvest-0.1.0 (c (n "harvest") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1s86gggvg6qwcgvr4rni87fq8dp6yi3b3l5mz20acqfgpiggi5nn")))

(define-public crate-harvest-0.1.1 (c (n "harvest") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "036ky2gizr2110whnr1l5qsbaach3my0w97rm9r912x2mvapqcfy")))

(define-public crate-harvest-0.1.2 (c (n "harvest") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1w45kg9sxlj9c0m8mx9pzs4mk3k7wzs3gwdjxp5w060wgchs9j47")))

(define-public crate-harvest-0.2.0 (c (n "harvest") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "pager") (r "^0.15.0") (d #t) (k 0)))) (h "0n4fnfbbxjg7w149gx3i3rnjr7s8x86d35p6l6gfyd07iyiim11w")))

(define-public crate-harvest-0.2.1 (c (n "harvest") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "pager") (r "^0.15.0") (d #t) (k 0)))) (h "03q9nlhcnym0g5y31sa2vzjgh6zq4gccy8gz840qm4aai012wz4r")))

(define-public crate-harvest-0.2.2 (c (n "harvest") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "pager") (r "^0.15.0") (d #t) (k 0)))) (h "1cgr7rypr3hz07gmg7dhyyjkl9d1ghpq1iab04832g7665jbhxng")))

