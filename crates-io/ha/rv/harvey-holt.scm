(define-module (crates-io ha rv harvey-holt) #:use-module (crates-io))

(define-public crate-harvey-holt-0.1.0 (c (n "harvey-holt") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "csv") (r "^0.15.0") (d #t) (k 0)) (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "1yfqld0xsc76v2ap84d4jq3m0lqjxcxkwf1pcnqrsyq6fq5r5iyc")))

