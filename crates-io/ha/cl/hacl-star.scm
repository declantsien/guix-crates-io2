(define-module (crates-io ha cl hacl-star) #:use-module (crates-io))

(define-public crate-hacl-star-0.0.0 (c (n "hacl-star") (v "0.0.0") (d (list (d (n "hacl-star-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "17cvnpxm9jj3k0pplmk4hy597nx74p8fh4kp7di7k357rfws6gv9")))

(define-public crate-hacl-star-0.0.2 (c (n "hacl-star") (v "0.0.2") (d (list (d (n "hacl-star-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "0azcrhfgdsiqi5fy8ww5k2ahsdrz2v620s8msd0d4h7hwbz7g86s")))

(define-public crate-hacl-star-0.0.3 (c (n "hacl-star") (v "0.0.3") (d (list (d (n "hacl-star-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "178saj7jz079jyrzlf7y95ha4zmmphrlc4xy6n0z4gvpz8lmha0q") (f (quote (("bindgen" "hacl-star-sys/bindgen"))))))

(define-public crate-hacl-star-0.0.5 (c (n "hacl-star") (v "0.0.5") (d (list (d (n "hacl-star-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "1p030v9ssmn39hlvg3jzn9glaysqk99y3m3qyr5qsydjgmnl03lc") (f (quote (("bindgen" "hacl-star-sys/bindgen"))))))

(define-public crate-hacl-star-0.0.6 (c (n "hacl-star") (v "0.0.6") (d (list (d (n "hacl-star-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "rand") (r "^0.4") (k 0)))) (h "0hr27l1i45s73z6xlspgdmvmk7236ppwimv8a5d4ay74x85sbyhz") (f (quote (("bindgen" "hacl-star-sys/bindgen"))))))

(define-public crate-hacl-star-0.0.7 (c (n "hacl-star") (v "0.0.7") (d (list (d (n "hacl-star-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "rand") (r "^0.4") (k 0)))) (h "0l129lc8727c5q31k804glqkx96h3nc5bnixgynqg5cfr8ii4h7j") (f (quote (("bindgen" "hacl-star-sys/bindgen"))))))

(define-public crate-hacl-star-0.0.8 (c (n "hacl-star") (v "0.0.8") (d (list (d (n "hacl-star-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2") (k 0)))) (h "00dmwybxv9nhcyz0ks0l8wbsb135f5y567zfai5npkbsvfc6yynd") (f (quote (("std" "rand_core/std" "libc/use_std") ("serde1" "rand_core/serde1") ("bindgen" "hacl-star-sys/bindgen") ("alloc" "rand_core/alloc"))))))

(define-public crate-hacl-star-0.0.9 (c (n "hacl-star") (v "0.0.9") (d (list (d (n "hacl-star-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2") (k 0)))) (h "117cx1scmas919c5c8m71c6aaqnm9zm0pfrqdcma1kf06xxajydr") (f (quote (("std" "rand_core/std" "libc/use_std") ("serde1" "rand_core/serde1") ("bindgen" "hacl-star-sys/bindgen") ("alloc" "rand_core/alloc"))))))

(define-public crate-hacl-star-0.0.10 (c (n "hacl-star") (v "0.0.10") (d (list (d (n "hacl-star-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2") (k 0)))) (h "1pfpa7pjd52cl9y98gcicslx13vs282nczrypjpqagd3hvn625ld") (f (quote (("std" "rand_core/std" "libc/use_std") ("serde1" "rand_core/serde1") ("bindgen" "hacl-star-sys/bindgen") ("alloc" "rand_core/alloc"))))))

(define-public crate-hacl-star-0.0.11 (c (n "hacl-star") (v "0.0.11") (d (list (d (n "hacl-star-sys") (r "= 0.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2") (k 0)))) (h "141rba15lrrh2nlhv2p1j2wh1wr855yfnngafdd2438vrnb5b27z") (f (quote (("std" "hacl-star-sys/use_std" "rand_core/std" "libc/use_std") ("serde1" "rand_core/serde1") ("bindgen" "hacl-star-sys/bindgen") ("alloc" "rand_core/alloc"))))))

(define-public crate-hacl-star-0.0.12 (c (n "hacl-star") (v "0.0.12") (d (list (d (n "hacl-star-sys") (r "= 0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2") (k 0)))) (h "0qxqaq5a5fpds51nrbvi7hjq7k5q0wj69257s6ixj73dcki5j2l4") (f (quote (("std" "hacl-star-sys/use_std" "rand_core/std") ("serde1" "rand_core/serde1") ("bindgen" "hacl-star-sys/bindgen") ("alloc" "rand_core/alloc"))))))

(define-public crate-hacl-star-0.0.13 (c (n "hacl-star") (v "0.0.13") (d (list (d (n "hacl-star-sys") (r "= 0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2") (k 0)))) (h "1hhrp36hfpz300sq2d8v7wrsix6qkm2gp15vj216szxw22n3ajz9") (f (quote (("std" "hacl-star-sys/use_std" "rand_core/std") ("serde1" "rand_core/serde1") ("bindgen" "hacl-star-sys/bindgen") ("alloc" "rand_core/alloc"))))))

(define-public crate-hacl-star-0.0.14 (c (n "hacl-star") (v "0.0.14") (d (list (d (n "hacl-star-sys") (r "= 0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_core") (r "^0.4") (k 0)))) (h "1n2gkrmph2ivc4bam4x058x857gi6261glmsfv8arrgzbp4f3xw9") (f (quote (("bindgen" "hacl-star-sys/bindgen"))))))

(define-public crate-hacl-star-0.1.0 (c (n "hacl-star") (v "0.1.0") (d (list (d (n "hacl-star-sys") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_core") (r "^0.4") (k 0)))) (h "1jzv0m5pb43djj79naxs9lrgq1n463xay5asa6v77a95ybg9bnjw") (f (quote (("bindgen" "hacl-star-sys/bindgen"))))))

