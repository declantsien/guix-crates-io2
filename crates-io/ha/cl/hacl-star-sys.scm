(define-module (crates-io ha cl hacl-star-sys) #:use-module (crates-io))

(define-public crate-hacl-star-sys-0.0.1 (c (n "hacl-star-sys") (v "0.0.1") (h "1p3adbqlw2mbjj1qkyz5bxv0w6yvxacsv4ha3kn9r6q5ff1xa24w")))

(define-public crate-hacl-star-sys-0.0.2 (c (n "hacl-star-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0jpcs8i2zha4443xydfh3jmkjgd173qzg50i9sss3mpvy9kyma6v")))

(define-public crate-hacl-star-sys-0.0.3 (c (n "hacl-star-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.32") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0nb1qv523pnyf76r643zvsd74cp75wj0nk49z1wl44kd3pgnhkjm")))

(define-public crate-hacl-star-sys-0.0.5 (c (n "hacl-star-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.32") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1d4392ilklgpxnhslc7zamy3w8d252vyp0nddgd1sc9gmxv0bnnz")))

(define-public crate-hacl-star-sys-0.0.6 (c (n "hacl-star-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.37") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "09sg9xz8a12w68bqff3r8b98ww8lzr4y2yas9f9zwchhfmidk38n") (f (quote (("use_std" "libc/use_std"))))))

(define-public crate-hacl-star-sys-0.0.7 (c (n "hacl-star-sys") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.40") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0vfl7ds0756r3b2nbqrlfq4n7wnz1i9fn33lnvjs4nmyxzcwvlk3") (f (quote (("use_std" "libc/use_std"))))))

(define-public crate-hacl-star-sys-0.1.0 (c (n "hacl-star-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.40") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "10svyhi7njpf9jww91d43mz9f0vnh8ip56xhigx1720zxcqrja6l") (f (quote (("use_std" "libc/use_std") ("overwrite" "bindgen"))))))

