(define-module (crates-io ha xc haxcel) #:use-module (crates-io))

(define-public crate-haxcel-0.1.0 (c (n "haxcel") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("winuser" "libloaderapi" "debugapi" "processthreadsapi" "handleapi" "namedpipeapi" "winbase" "fileapi" "errhandlingapi" "wincon"))) (d #t) (k 0)) (d (n "xladd") (r "^0.1.2") (d #t) (k 0)))) (h "1msd4q51d41d8gn0gv9xk7p5lp5as9hs1qkvhlrxvwg1xmkh4prl")))

