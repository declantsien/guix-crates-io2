(define-module (crates-io ha ze haze_core) #:use-module (crates-io))

(define-public crate-haze_core-1.3.0 (c (n "haze_core") (v "1.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "miette") (r "^5.3.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "08bvwgmypgpc260q2dzqmq3p3km6vgnv6i8r54k90n81vz7v874q")))

(define-public crate-haze_core-1.4.0 (c (n "haze_core") (v "1.4.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "miette") (r "^5.3.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "19xrb685n4vc289y4zz02dcfjzb00gmnl385rg5kacgh4g9alr55")))

