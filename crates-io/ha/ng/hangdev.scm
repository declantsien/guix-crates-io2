(define-module (crates-io ha ng hangdev) #:use-module (crates-io))

(define-public crate-hangdev-0.1.0 (c (n "hangdev") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0xb538sw6gwi5yik2jk6fx5ljrxlnfi21zpzwd9h32i4c92frw6n")))

