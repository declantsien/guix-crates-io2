(define-module (crates-io ha ng hangeul) #:use-module (crates-io))

(define-public crate-hangeul-0.1.0 (c (n "hangeul") (v "0.1.0") (h "0aygd7whvl08k8izwrb59na0qbp1381939m4ggwwq36vfmd8r9m9")))

(define-public crate-hangeul-0.1.1 (c (n "hangeul") (v "0.1.1") (h "1dd5zx58hcjrj3804zrgmj7invwipaxy4g1cc09fm8g1jca16l7b") (y #t)))

(define-public crate-hangeul-0.1.2 (c (n "hangeul") (v "0.1.2") (h "0av1ycljvhrh32aafmqis5cwc8qwd0486m4dww9xd56xl9b3xaga")))

(define-public crate-hangeul-0.1.3 (c (n "hangeul") (v "0.1.3") (h "1b9bg3hcn9hbvifhi7xf7fy258ljfin33qjj0fxx93q7h8ym6gln")))

(define-public crate-hangeul-0.2.0 (c (n "hangeul") (v "0.2.0") (h "1hwm2a3q1jjkw2c28b5rznp84my4ixpf8db9w73k4hd3k4hdwg6i")))

(define-public crate-hangeul-0.3.0 (c (n "hangeul") (v "0.3.0") (h "1j3q451r6ipvjidy5mgb1qdnb360m3lkbplfp3p37fl1gv5aaxhf")))

(define-public crate-hangeul-0.4.0 (c (n "hangeul") (v "0.4.0") (h "0mz8rmi73c9zkn34p4cysvc1myamhsigbk3h01chskppwl8cw8r7")))

