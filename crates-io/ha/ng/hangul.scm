(define-module (crates-io ha ng hangul) #:use-module (crates-io))

(define-public crate-hangul-0.1.0 (c (n "hangul") (v "0.1.0") (h "1d6k8nha3d4gd2lvf1inblwi1j7v8gwd5439z8m8q48xzhs852ni")))

(define-public crate-hangul-0.1.1 (c (n "hangul") (v "0.1.1") (h "0a7m3jxmcg91sz0k37drhh3bmi513l9wpz0h0845yjj14yjxhhyj")))

(define-public crate-hangul-0.1.2 (c (n "hangul") (v "0.1.2") (h "0xw1ky0azihap2bcvxc3qahb7v7hc76yslc3c9hmp14jadn0knlw")))

(define-public crate-hangul-0.1.3 (c (n "hangul") (v "0.1.3") (h "1bdf14sl8cr4jcl8par59d830ljag6kbgbp8647xjwgfvadnh7qj")))

