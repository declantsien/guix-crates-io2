(define-module (crates-io ha ng hangmanrs) #:use-module (crates-io))

(define-public crate-hangmanrs-0.1.0 (c (n "hangmanrs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1gc6d1214qfhrsgwrccb87x4kb4v4gw5lmhifbbxlzk1sx2hxwb1")))

(define-public crate-hangmanrs-0.1.1 (c (n "hangmanrs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "11h277zhbyklm9bcpcgs834rbchgwnvqpr6bk7p1vhji3x9gklw6")))

(define-public crate-hangmanrs-0.1.2 (c (n "hangmanrs") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0dvifcccipdps50ph87dzm1n5adi4n20v8y0gpd56z5igxd930pv")))

(define-public crate-hangmanrs-0.1.3 (c (n "hangmanrs") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0p057xkipnmj5zcv91080y2jg4xryda19d06na4myamjw729142l")))

(define-public crate-hangmanrs-0.1.4 (c (n "hangmanrs") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "167fh12ls4siv8gavx3pxwxa7rk26369jarq8fq94ml0g595313j")))

