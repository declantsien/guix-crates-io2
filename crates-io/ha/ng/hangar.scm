(define-module (crates-io ha ng hangar) #:use-module (crates-io))

(define-public crate-hangar-0.0.1 (c (n "hangar") (v "0.0.1") (d (list (d (n "cargo-shim") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "1i8rq5dcjwac00ni41kczm93m89clnl82744lnz83gh6d0niizlf")))

(define-public crate-hangar-0.0.2 (c (n "hangar") (v "0.0.2") (d (list (d (n "cargo-shim") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "14niihnhsinzd4gkqc0q287f1qgpfqakdsfkcc432vdqv63byxg9")))

(define-public crate-hangar-0.0.3 (c (n "hangar") (v "0.0.3") (d (list (d (n "cargo-shim") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "1wj1q7dsf2n2v4wrcz03wrgdsgkr8d10bvbaxp6cm0c9x3k7k8nf")))

(define-public crate-hangar-0.0.4 (c (n "hangar") (v "0.0.4") (d (list (d (n "cargo-shim") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "0m4bap2kn33hkamz4d4jj01gf1vw2gflmdq18av5phnqbj8rxvni")))

