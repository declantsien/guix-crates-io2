(define-module (crates-io ha ng hanger) #:use-module (crates-io))

(define-public crate-hanger-0.1.0 (c (n "hanger") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "04pz4pj5wc52zyslgzzbdhv1227bp4sqzwy7mvxm3w9qrcv0piwy")))

