(define-module (crates-io ha tc hatch_result) #:use-module (crates-io))

(define-public crate-hatch_result-0.1.0 (c (n "hatch_result") (v "0.1.0") (h "0mnl65a05hmlyncqfn33kqnhqw7mxk9y646wbk4agcja5l84nx32")))

(define-public crate-hatch_result-0.1.1 (c (n "hatch_result") (v "0.1.1") (h "1mqmkp6skbvdwnci5j3spphl6ixmpmfkvhpsgv2m6mp78krwmnrd")))

(define-public crate-hatch_result-0.1.2 (c (n "hatch_result") (v "0.1.2") (h "0hwrmpsv7hvx0yl5g0vb14c1zvd0zbsqz4pg8wk697xgqdcz3ss8")))

(define-public crate-hatch_result-0.1.3 (c (n "hatch_result") (v "0.1.3") (h "1dfyh6rnwxmbhmldqd7ylipwg37zx783cf0faha20rv87qf0f7n4")))

(define-public crate-hatch_result-0.1.4 (c (n "hatch_result") (v "0.1.4") (h "0hgbddylpgj2nxjb2pamnidbj7nwc0cx69ym8vmylw2hxdi5v84j")))

(define-public crate-hatch_result-0.1.5 (c (n "hatch_result") (v "0.1.5") (h "1mm937hbvp7l6rqklqmjv1vak8q714pk8164bihlmc78mv2gm621")))

(define-public crate-hatch_result-0.1.6 (c (n "hatch_result") (v "0.1.6") (h "11y5g0ljawhjydwh9qqmj84lxf7db81flc8jdk68n6mrdzav0shs")))

(define-public crate-hatch_result-0.1.7 (c (n "hatch_result") (v "0.1.7") (h "06vg9v6z86waspy2m68am3nkkr87ywih128kcj9dgc04lvg0lxar")))

(define-public crate-hatch_result-0.1.8 (c (n "hatch_result") (v "0.1.8") (h "0v3n12n0nzv4xr7rh1wcvy3cdb4c624hhs9yz05ffjn88pn81jd2")))

