(define-module (crates-io ha tc hatch) #:use-module (crates-io))

(define-public crate-hatch-0.1.0 (c (n "hatch") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.5") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.42.0") (d #t) (k 0)) (d (n "rusoto_ssm") (r "^0.42.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1r551480vr7iccxa1p53631vd7bdk97fv1hwjbgny9hhsa8b33sp") (y #t)))

(define-public crate-hatch-0.1.2 (c (n "hatch") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.5") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.42.0") (d #t) (k 0)) (d (n "rusoto_ssm") (r "^0.42.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0fki4f64ki2drnmv8hmy279iyqdiz9xv717kqg6c2nc0hbq2cv16")))

