(define-module (crates-io ha tc hatchling) #:use-module (crates-io))

(define-public crate-hatchling-0.1.0 (c (n "hatchling") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rdf") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0120bajnckwcksh71wsc6whf1y6ppqdhl38a5ljc325w4l2z9fff")))

