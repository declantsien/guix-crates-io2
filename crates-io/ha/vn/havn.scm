(define-module (crates-io ha vn havn) #:use-module (crates-io))

(define-public crate-havn-0.1.0 (c (n "havn") (v "0.1.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "1ck5k7wkxkl87g6p47358pr6qy2738kn0ihza8jkn659r708azzm")))

(define-public crate-havn-0.1.1 (c (n "havn") (v "0.1.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "0ig5i58n5vbi3dm37d7zbfffvisc78ikcy93027xcsfymw0qqi8a")))

(define-public crate-havn-0.1.2 (c (n "havn") (v "0.1.2") (d (list (d (n "clap") (r "^4.3") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "1iir1xyiqianzvq88wgbv7jlxcchh5y4kpydjxv4xblw77qzxksp")))

(define-public crate-havn-0.1.3 (c (n "havn") (v "0.1.3") (d (list (d (n "clap") (r "^4.3") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "os_info") (r "^3.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "15ss1z75a41x8ydbjc9bf2kz15ywlf8dkdbzy59jg6x80dmcr2cq")))

(define-public crate-havn-0.1.4 (c (n "havn") (v "0.1.4") (d (list (d (n "clap") (r "^4.4") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "os_info") (r "^3.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "038218ijs3pp22183bqv56smlgpwim3p180hvmjvbrvwpvm7s16n")))

(define-public crate-havn-0.1.5 (c (n "havn") (v "0.1.5") (d (list (d (n "clap") (r "^4.4") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "os_info") (r "^3.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "1lpahnan39lssmkfswcfib2747sas6nyk5671d721jmymxwp17zy")))

(define-public crate-havn-0.1.6 (c (n "havn") (v "0.1.6") (d (list (d (n "clap") (r "^4.4") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "os_info") (r "^3.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "0csik65zlpa69jr3jxchfqpszirhy2ia3wvvzklw5vb7lycrymlx")))

(define-public crate-havn-0.1.7 (c (n "havn") (v "0.1.7") (d (list (d (n "clap") (r "^4.4") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "os_info") (r "^3.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "165k327qgmdv4g5skxa3vs70f8rf8s8s9pi0k2mvflgqjvjz6g79")))

(define-public crate-havn-0.1.8 (c (n "havn") (v "0.1.8") (d (list (d (n "clap") (r "^4.5") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "os_info") (r "^3.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "1n89kkgnlp5igban0qahfjyrmfkmcins9g64rng6n7g924a5c72p")))

(define-public crate-havn-0.1.9 (c (n "havn") (v "0.1.9") (d (list (d (n "clap") (r "^4.5") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "os_info") (r "^3.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "0l2f2m3k57kxvzncf469dm0yzimh4wyzsxg06rbb2dy92r2kqs3a")))

(define-public crate-havn-0.1.10 (c (n "havn") (v "0.1.10") (d (list (d (n "clap") (r "^4.5") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "os_info") (r "^3.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "1l8f47f3kijzb4j37l82p8icbay8hlysl580vcykv2h56fxm6s1r")))

(define-public crate-havn-0.1.11 (c (n "havn") (v "0.1.11") (d (list (d (n "clap") (r "^4.5") (f (quote ("color" "derive" "unicode"))) (d #t) (k 0)) (d (n "os_info") (r "^3.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("macros" "net" "parking_lot" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "0702yw5nyr9lw5iqv2i14lhjd43krj6g7cn4wxsdvfcdbdw08lj9")))

