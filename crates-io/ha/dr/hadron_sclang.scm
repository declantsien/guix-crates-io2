(define-module (crates-io ha dr hadron_sclang) #:use-module (crates-io))

(define-public crate-hadron_sclang-0.1.0-alpha.1 (c (n "hadron_sclang") (v "0.1.0-alpha.1") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1ifyq76kjn1rzvhp1222lj1b2k0nrsmxyhgyrsqa51rw8s7sps2p")))

(define-public crate-hadron_sclang-0.1.0-alpha.2 (c (n "hadron_sclang") (v "0.1.0-alpha.2") (d (list (d (n "const_format") (r "^0.2.31") (d #t) (k 0)) (d (n "mmap-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "099dicl0cs97m95pc395kwmdv1pyyd4xfssxbh4ikbdmj8jwgnv6")))

(define-public crate-hadron_sclang-0.1.0-alpha.3 (c (n "hadron_sclang") (v "0.1.0-alpha.3") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "duct") (r "^0.13.7") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mmtk") (r "^0.25.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.10") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "1yr7qsyyg9zj1xhyfz9idb0fn96mjhyx904ll5jklnq3jmibzxmh")))

