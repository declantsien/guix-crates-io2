(define-module (crates-io ha dr hadron) #:use-module (crates-io))

(define-public crate-hadron-0.1.0 (c (n "hadron") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.3.12") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.65") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.65") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.19") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.4") (o #t) (d #t) (k 0)))) (h "1kbvr71vi0ak15kdmif7nxys3w2zz1hc8h3ix3dsy032gnv9c8ap")))

