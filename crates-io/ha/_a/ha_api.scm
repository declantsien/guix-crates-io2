(define-module (crates-io ha _a ha_api) #:use-module (crates-io))

(define-public crate-ha_api-0.1.0 (c (n "ha_api") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05yyhqdjxb704y60mk19v30w3bam2xs7si8hg8i433zbwcbhymc1")))

