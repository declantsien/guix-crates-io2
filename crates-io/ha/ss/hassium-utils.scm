(define-module (crates-io ha ss hassium-utils) #:use-module (crates-io))

(define-public crate-hassium-utils-0.1.0 (c (n "hassium-utils") (v "0.1.0") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jrjdq4v47lxiqncywpncw20gj80p8zmj146rpa935ggdalqc0qq") (f (quote (("parallel" "rayon"))))))

(define-public crate-hassium-utils-0.1.1 (c (n "hassium-utils") (v "0.1.1") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wy0l0n2blkb6nhravckdxr22rs9zpk1g4gnn0kqhfzcynfcwky9") (f (quote (("parallel" "rayon"))))))

(define-public crate-hassium-utils-0.1.2 (c (n "hassium-utils") (v "0.1.2") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y8bw0klq95wqr8qv03pg41g9xaig6sskd8xb3hfkgm2kqd7lhvr") (f (quote (("parallel" "rayon"))))))

(define-public crate-hassium-utils-0.1.3 (c (n "hassium-utils") (v "0.1.3") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1syfh3n6iyn6kmzcb6cbp3v1ygb54zsk790rwr674wdic0xqr7sd") (f (quote (("parallel" "rayon"))))))

