(define-module (crates-io ha ss hass-mqtt-discovery-macros) #:use-module (crates-io))

(define-public crate-hass-mqtt-discovery-macros-0.0.0 (c (n "hass-mqtt-discovery-macros") (v "0.0.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1afwd24v0zkf3gfqv853pk5fpfylxllv7mxp4pkkiwv3ig4rs7f1")))

