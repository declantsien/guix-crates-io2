(define-module (crates-io ha ss hassium-ignite) #:use-module (crates-io))

(define-public crate-hassium-ignite-0.1.0 (c (n "hassium-ignite") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0iicrwi7y4z1qcpbrh4zdafsmqiwjmgd9ygmxdw3dc8mih96agm1")))

(define-public crate-hassium-ignite-0.1.1 (c (n "hassium-ignite") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1gl8pxz0nfg5158lxj017bdgy6az8ja14lyy0998m7nfd6vv7y8i")))

(define-public crate-hassium-ignite-0.1.2 (c (n "hassium-ignite") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "087p1q1fcz38ha68b63a8418hww8z5rkf90paks8s1cnhcvmhpdk")))

(define-public crate-hassium-ignite-0.1.3 (c (n "hassium-ignite") (v "0.1.3") (d (list (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0zg4imf8rzzln38p9n01lgsi0vbgmh24fgsrhddbg55hyx0gr6rw")))

