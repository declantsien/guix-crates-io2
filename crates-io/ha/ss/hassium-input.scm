(define-module (crates-io ha ss hassium-input) #:use-module (crates-io))

(define-public crate-hassium-input-0.1.0 (c (n "hassium-input") (v "0.1.0") (d (list (d (n "hassium-core") (r "^0.1") (d #t) (k 0)))) (h "1kyb3jspv7lnaazl3s25c8lvx5c6waiv1sk3wdynsbqy4ll219p8") (f (quote (("parallel" "hassium-core/parallel"))))))

(define-public crate-hassium-input-0.1.1 (c (n "hassium-input") (v "0.1.1") (d (list (d (n "hassium-core") (r "^0.1.1") (d #t) (k 0)))) (h "13a99kypy713cmp46r9a605an4vaska2k633c9cirxiqh5lmkvc6") (f (quote (("parallel" "hassium-core/parallel"))))))

(define-public crate-hassium-input-0.1.2 (c (n "hassium-input") (v "0.1.2") (d (list (d (n "hassium-core") (r "^0.1.2") (d #t) (k 0)))) (h "0sfm5l0qkjwh2ickyw10lh110sc2qzdrdjdw4aipqa9wi3r7ad21") (f (quote (("parallel" "hassium-core/parallel"))))))

(define-public crate-hassium-input-0.1.3 (c (n "hassium-input") (v "0.1.3") (d (list (d (n "hassium-core") (r "^0.1.3") (d #t) (k 0)))) (h "18ikb02lxyn1ymdla16z3badx4w5dkbirzvnaszvywrzi54ckgf3") (f (quote (("parallel" "hassium-core/parallel"))))))

