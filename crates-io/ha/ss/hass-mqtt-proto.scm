(define-module (crates-io ha ss hass-mqtt-proto) #:use-module (crates-io))

(define-public crate-hass-mqtt-proto-0.1.0 (c (n "hass-mqtt-proto") (v "0.1.0") (d (list (d (n "nameof") (r "^1") (d #t) (k 0)) (d (n "semval") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0md293b8l5kagjwbabvyw9b3nydlnmpgl52xmzjhjmfagq11zap3")))

