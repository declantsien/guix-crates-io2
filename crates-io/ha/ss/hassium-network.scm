(define-module (crates-io ha ss hassium-network) #:use-module (crates-io))

(define-public crate-hassium-network-0.1.0 (c (n "hassium-network") (v "0.1.0") (d (list (d (n "hassium-core") (r "^0.1") (d #t) (k 0)))) (h "1w0ypwr6gvl5sgg1ajnn5dszhqz981p1rab1vgqpgjvpr8n4z69a") (f (quote (("parallel" "hassium-core/parallel"))))))

(define-public crate-hassium-network-0.1.1 (c (n "hassium-network") (v "0.1.1") (d (list (d (n "hassium-core") (r "^0.1.1") (d #t) (k 0)))) (h "1hy4nmr7qqj3xcn79g3ap4qg56knlr7mv2dwf6x5lpq472qfz481") (f (quote (("parallel" "hassium-core/parallel"))))))

(define-public crate-hassium-network-0.1.2 (c (n "hassium-network") (v "0.1.2") (d (list (d (n "hassium-core") (r "^0.1.2") (d #t) (k 0)))) (h "1al3qkp58xclfckv4hnk5q974g3lxb5rh7y1942wlnapvpjkkf73") (f (quote (("parallel" "hassium-core/parallel"))))))

(define-public crate-hassium-network-0.1.3 (c (n "hassium-network") (v "0.1.3") (d (list (d (n "hassium-core") (r "^0.1.3") (d #t) (k 0)))) (h "1g0cs6mkm2k1w3yaj7ykawpc44wsnmihprg76mnyyb05qrg78jq5") (f (quote (("parallel" "hassium-core/parallel"))))))

