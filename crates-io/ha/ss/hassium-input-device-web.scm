(define-module (crates-io ha ss hassium-input-device-web) #:use-module (crates-io))

(define-public crate-hassium-input-device-web-0.1.0 (c (n "hassium-input-device-web") (v "0.1.0") (d (list (d (n "hassium-core") (r "^0.1") (d #t) (k 0)) (d (n "hassium-input") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "Element" "Window" "MouseEvent" "KeyboardEvent" "EventTarget"))) (d #t) (k 0)))) (h "0lnnbhqak8rpi8ily0d9194xz34dndfqxpv3i07dzyff6wyl6m01")))

(define-public crate-hassium-input-device-web-0.1.1 (c (n "hassium-input-device-web") (v "0.1.1") (d (list (d (n "hassium-core") (r "^0.1.1") (d #t) (k 0)) (d (n "hassium-input") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "Element" "Window" "MouseEvent" "KeyboardEvent" "EventTarget"))) (d #t) (k 0)))) (h "1pmiyv0n3cklwzi8yl4d6fb11rdpd1c6bfg23lj0wpkydyvldczd")))

(define-public crate-hassium-input-device-web-0.1.2 (c (n "hassium-input-device-web") (v "0.1.2") (d (list (d (n "hassium-core") (r "^0.1.2") (d #t) (k 0)) (d (n "hassium-input") (r "^0.1.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "Element" "Window" "MouseEvent" "KeyboardEvent" "EventTarget"))) (d #t) (k 0)))) (h "1rnziyw8psqzp7fd9dwnkmflmwim7j21zc2j8v8s4nsx6zak0r74")))

(define-public crate-hassium-input-device-web-0.1.3 (c (n "hassium-input-device-web") (v "0.1.3") (d (list (d (n "hassium-core") (r "^0.1.3") (d #t) (k 0)) (d (n "hassium-input") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "Element" "Window" "MouseEvent" "KeyboardEvent" "EventTarget"))) (d #t) (k 0)))) (h "1s4r3as7gq36w01b1ni9siqva8xcslqlr04yl8pv4nl2s7l6x71v")))

