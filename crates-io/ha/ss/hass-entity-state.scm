(define-module (crates-io ha ss hass-entity-state) #:use-module (crates-io))

(define-public crate-hass-entity-state-0.0.0 (c (n "hass-entity-state") (v "0.0.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0k07wbwp75iypzk7ywmdgcdc8m3izkvy1clajpq7hq5p4h1ca3qx")))

(define-public crate-hass-entity-state-0.1.0 (c (n "hass-entity-state") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "04l0nvl46q5i670z3z8yw9cllnlca3f97wjsf5nmp9nvfd0acgd9")))

(define-public crate-hass-entity-state-0.2.0 (c (n "hass-entity-state") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0mhphxp7bsj0dng9aas8xlkb126ana6hqrfs80ij0016fxr4kx8f")))

