(define-module (crates-io ha ss hassium-network-backend-native) #:use-module (crates-io))

(define-public crate-hassium-network-backend-native-0.1.0 (c (n "hassium-network-backend-native") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "hassium-core") (r "^0.1") (d #t) (k 0)) (d (n "hassium-network") (r "^0.1") (d #t) (k 0)))) (h "1maiq6r1k24pb6wphldhmlhfhplbavlg4llblx2275406skj50rd") (f (quote (("parallel" "hassium-core/parallel"))))))

(define-public crate-hassium-network-backend-native-0.1.1 (c (n "hassium-network-backend-native") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "hassium-core") (r "^0.1.1") (d #t) (k 0)) (d (n "hassium-network") (r "^0.1.1") (d #t) (k 0)))) (h "10xcmng6jhmv4qv89bpv2m205n3arrvbi74q8bjw32f3mhm0kdmj") (f (quote (("parallel" "hassium-core/parallel"))))))

(define-public crate-hassium-network-backend-native-0.1.2 (c (n "hassium-network-backend-native") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "hassium-core") (r "^0.1.2") (d #t) (k 0)) (d (n "hassium-network") (r "^0.1.2") (d #t) (k 0)))) (h "05qjd4ig0gfaq7hf7gs5lyss85dd3rvxyyifxk3mjwlbnwimd3yr") (f (quote (("parallel" "hassium-core/parallel"))))))

(define-public crate-hassium-network-backend-native-0.1.3 (c (n "hassium-network-backend-native") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "hassium-core") (r "^0.1.3") (d #t) (k 0)) (d (n "hassium-network") (r "^0.1.3") (d #t) (k 0)))) (h "0nczmzk08zpzdraz9jxmsqhz2cld4scc6ip86f73ycka4bijisj0") (f (quote (("parallel" "hassium-core/parallel"))))))

