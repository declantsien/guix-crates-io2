(define-module (crates-io ha mt hamt) #:use-module (crates-io))

(define-public crate-hamt-0.1.0 (c (n "hamt") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)))) (h "0ph3z95dsykpbpg4alsj1d8ql8g58jlq2vkbz8w2b7np2gy5g9ay")))

(define-public crate-hamt-0.1.1 (c (n "hamt") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)))) (h "0jysy28ghxg4q5zy4fddgyagcznyddryf7hkxz1s052qv4cbb1rw")))

(define-public crate-hamt-0.1.2 (c (n "hamt") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)))) (h "07halnj5s1m27ipck1cyrk7xm2g6wjrf4qsi44bx37pg3qb0fp4k")))

(define-public crate-hamt-0.1.3 (c (n "hamt") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)))) (h "137w7sld1jc6j3mlm8d8hip0238m4nyhn7hqj251f7cs20r0833g")))

(define-public crate-hamt-0.1.4 (c (n "hamt") (v "0.1.4") (d (list (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)))) (h "1i2rxri7xaxb2m0k8d718csw9bfix8c8y20d7wsrf9s180y8vm6a")))

(define-public crate-hamt-0.1.6 (c (n "hamt") (v "0.1.6") (d (list (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)))) (h "0rq3cxpy2lzg8v4vq9xr5z4jzh9cwshrrrz8ah9288n0z7dfx5fv")))

(define-public crate-hamt-0.1.7 (c (n "hamt") (v "0.1.7") (d (list (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)))) (h "0qwz4iy08lla1i1m92fphi18nlrdg9mxbl06zn0bllrbh6ap025y")))

(define-public crate-hamt-0.2.0 (c (n "hamt") (v "0.2.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0l6b45zayrl6ds3ry4hzwnskdwpa29frypkgzvwyyjms0jacyj53") (f (quote (("default"))))))

