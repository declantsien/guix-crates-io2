(define-module (crates-io ha mt hamt-sync) #:use-module (crates-io))

(define-public crate-hamt-sync-0.1.0 (c (n "hamt-sync") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "046ilmvlw25xvf6cjgvbxkvw5qz83djx8hn6sjwd6z19ps0qsczw")))

(define-public crate-hamt-sync-0.1.1 (c (n "hamt-sync") (v "0.1.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1g500bb6hygh4qa43apqsjaxiryw7n4acvkl5fwp3b2j2hdg3bhb")))

(define-public crate-hamt-sync-0.1.2 (c (n "hamt-sync") (v "0.1.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1h4xzrpy9l63mbi00dxs8gv9xihpq7p2r0cza4ax4bw8jnhrqi5d")))

(define-public crate-hamt-sync-0.2.0 (c (n "hamt-sync") (v "0.2.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "17qy5n5ki9ni0vada6rvh3gc31s47qacqf765s9i3vaz4wc3zq0x")))

(define-public crate-hamt-sync-0.2.1 (c (n "hamt-sync") (v "0.2.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0h3gm3cnw7z9dnafd92az9v3wgpw824ijsw5fshr0s036ic0mw1c")))

(define-public crate-hamt-sync-0.2.2 (c (n "hamt-sync") (v "0.2.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "00fca1b6qmck34vsqifwfw5xhvsm0ab0aaiaz0yv8f9cv9nqc75y")))

(define-public crate-hamt-sync-0.2.3 (c (n "hamt-sync") (v "0.2.3") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0vklljmvfsgihrfnc6xqcvz83n2xyf57h75xm20yz8zjn32d2kaw")))

(define-public crate-hamt-sync-0.2.4 (c (n "hamt-sync") (v "0.2.4") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1r00233jhldmr1phgj1x4qkjg88027nyg64ji8cp1xl5cqsjb8vv")))

(define-public crate-hamt-sync-0.2.5 (c (n "hamt-sync") (v "0.2.5") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0z13ym8gly53h605lclqxkdg0w99why7ihyflglk5wf2inl6syqr")))

