(define-module (crates-io ha mt hamt-rs) #:use-module (crates-io))

(define-public crate-hamt-rs-0.2.0 (c (n "hamt-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 0)))) (h "025hpynf5yicc7xdsv9b9mp3lzlycfkznfdzjzcjasvbbhfnlb3i") (f (quote (("hashmap_default_hasher") ("default"))))))

(define-public crate-hamt-rs-0.2.1 (c (n "hamt-rs") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 0)))) (h "1303sax6f46bqkbvdvpiai0kfg41381z4rsfdacka7277zrfvvka") (f (quote (("hashmap_default_hasher") ("default"))))))

(define-public crate-hamt-rs-0.2.2 (c (n "hamt-rs") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 0)))) (h "0q0wyylwinmhqzdn4r4wm3dsgzdjpib5vd606a4pkvpz0y3y78yz") (f (quote (("rust_alloc") ("hashmap_default_hasher"))))))

(define-public crate-hamt-rs-0.2.3 (c (n "hamt-rs") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 0)))) (h "0yby45rz9k1p3hjcg8a2rsmg84m19ib1vs3rscfz1xqp7yxm8kz1") (f (quote (("rust_alloc") ("hashmap_default_hasher"))))))

(define-public crate-hamt-rs-0.3.0 (c (n "hamt-rs") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 0)))) (h "1ran344p39lz0blg4ghjw24i67zgiymdqics313mafmfsydgb8v0")))

