(define-module (crates-io ha sk haskell_bits) #:use-module (crates-io))

(define-public crate-haskell_bits-0.1.0 (c (n "haskell_bits") (v "0.1.0") (d (list (d (n "is_type") (r "^0.2.0") (d #t) (k 0)))) (h "0gs4c2nb11hc4rc353ii4sdawihp4znyiwriymaa1c64vbbny1f2")))

(define-public crate-haskell_bits-0.2.0 (c (n "haskell_bits") (v "0.2.0") (d (list (d (n "is_type") (r "^0.2.0") (d #t) (k 0)))) (h "0irxil9d8kglcjp3sm6xl4h90hic3cvdf94zk3k82jlyicb1mfkm")))

(define-public crate-haskell_bits-0.3.0 (c (n "haskell_bits") (v "0.3.0") (d (list (d (n "is_type") (r "^0.2.1") (d #t) (k 0)))) (h "0lb9qb9zsx6034x0652238qwa6q7bkg053bcj7mzmp4a6rwhajqy")))

