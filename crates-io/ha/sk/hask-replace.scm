(define-module (crates-io ha sk hask-replace) #:use-module (crates-io))

(define-public crate-hask-replace-0.1.0 (c (n "hask-replace") (v "0.1.0") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0864y58yh7c5bi7ypd98y4pmbv6pkqg14j89zgqf3y15biyjp4wp")))

(define-public crate-hask-replace-0.1.1 (c (n "hask-replace") (v "0.1.1") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1qwpfpv6pk0j543zcjpwrs2a0vkx8k0pa5vfzpr4klmkhwl3n7ys")))

(define-public crate-hask-replace-0.1.2 (c (n "hask-replace") (v "0.1.2") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0z7gd9gjxyxpcd0yc5ff9csky5rgbnqwr3rsab9pi2s0n5h5rbs1")))

(define-public crate-hask-replace-0.1.3 (c (n "hask-replace") (v "0.1.3") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "16n426g9infqiwgihlnhgn26bnib27jgy1d4sbj9gsi7iyd0g22q")))

(define-public crate-hask-replace-0.1.4 (c (n "hask-replace") (v "0.1.4") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1vkryrn3cml3sz7pw60cl7xb0gzmvb7yfpq5pbvq962g7l2p69yz")))

(define-public crate-hask-replace-0.1.5 (c (n "hask-replace") (v "0.1.5") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0842l7xdp0viw2spwzxn8sj5nl2xab9ix0zqad8y7c5h0ax4h8n3")))

(define-public crate-hask-replace-0.1.6 (c (n "hask-replace") (v "0.1.6") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1lankryasa82ngq02zricz6nim24mk5fri8jw026z6xvjq777mi0")))

(define-public crate-hask-replace-0.2.0 (c (n "hask-replace") (v "0.2.0") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1sx2x9dnbl5n8wrvd8xvxjbfj5xd22faqmydfk6cwkc2khx4kwpm")))

(define-public crate-hask-replace-0.2.1 (c (n "hask-replace") (v "0.2.1") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1vxjbz6kkzjiy08dfpvj7b341i93a2v01lcw0ivq1lj1rpybmmkp")))

(define-public crate-hask-replace-0.3.0 (c (n "hask-replace") (v "0.3.0") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0z7ail4xhbzk3jfnaqv8j00lpbhirvpj982nk35cncv2qwh93jrd")))

(define-public crate-hask-replace-0.3.1 (c (n "hask-replace") (v "0.3.1") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1q02wss64nj6iab45sjnn85d6wlcp56klrhg91m3lcqiy50wkmyf")))

(define-public crate-hask-replace-0.3.2 (c (n "hask-replace") (v "0.3.2") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "07yxysda1i81y6zlzmjbz1bv4ks0rzaylvss6qajxcx6gksljy5z")))

(define-public crate-hask-replace-0.3.3 (c (n "hask-replace") (v "0.3.3") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1vb9nzkglj5nlcgfv6j8257f5q244l403dqgsm0cbc92vfi7wx3j")))

(define-public crate-hask-replace-0.3.4 (c (n "hask-replace") (v "0.3.4") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1liai7krw9l20d484z9q5ij6y0rwdrm8pg5ri3k3l622ldpkqnm4")))

(define-public crate-hask-replace-0.3.5 (c (n "hask-replace") (v "0.3.5") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1354f85h8hz9yh2xnysm2l8a908z2fx55wyspbvyqqizfxw4p2p8")))

(define-public crate-hask-replace-0.4.0 (c (n "hask-replace") (v "0.4.0") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1m0v3nahdypyzp18bhy29ivz1d1yzkyvpypzh9dk65hv9hh43br9")))

(define-public crate-hask-replace-0.4.1 (c (n "hask-replace") (v "0.4.1") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1sva9ryp7iqdvq60bnhzwn1lxh2jvgafm1afi7rdnaxqyapigy29")))

(define-public crate-hask-replace-0.4.2 (c (n "hask-replace") (v "0.4.2") (d (list (d (n "clap") (r "^2.26.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "0mcm3xx5rm4w6b72k8nzch32rh0cjj02xm27plxj24i8vhhlac77")))

(define-public crate-hask-replace-0.4.3 (c (n "hask-replace") (v "0.4.3") (d (list (d (n "clap") (r "^2.26.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (f (quote ("simd-accel"))) (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "1wgpfz4v9w402b6z150l6c1igmk5mwx10pr77x3f09yxj3vl0gg4")))

(define-public crate-hask-replace-0.4.4 (c (n "hask-replace") (v "0.4.4") (d (list (d (n "clap") (r "^2.26.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (f (quote ("simd-accel"))) (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "0l2mibn7gfx8xrhamxvrj2asngb16fvykqfl02hmq9fyw5c42h4k")))

(define-public crate-hask-replace-0.4.5 (c (n "hask-replace") (v "0.4.5") (d (list (d (n "clap") (r "^2.26.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (f (quote ("simd-accel"))) (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "1jqf1k7ivfxal4dj9z9ciya4h0ww8b622z2krmfmngg87a301rx2")))

(define-public crate-hask-replace-0.4.6 (c (n "hask-replace") (v "0.4.6") (d (list (d (n "clap") (r "^2.26.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (f (quote ("simd-accel"))) (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "0qkyi487vy3k518qgfqpzg2mjzl3lv4jw6ia1lwdxmz59m2d9xgk")))

(define-public crate-hask-replace-0.6.0 (c (n "hask-replace") (v "0.6.0") (d (list (d (n "clap") (r "^2.26.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (f (quote ("simd-accel"))) (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "0id7jxfq9mgxynfr8346h18h59l3c2sq1a0bwgmgbq95alchg85n")))

