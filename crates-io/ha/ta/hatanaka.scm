(define-module (crates-io ha ta hatanaka) #:use-module (crates-io))

(define-public crate-hatanaka-0.0.1 (c (n "hatanaka") (v "0.0.1") (d (list (d (n "rinex") (r "^0.0.12") (d #t) (k 0)))) (h "0apclginqnxmkq99rr848cf3fapfc1bp66lcri3cnkfxcx5p2xgr") (y #t)))

(define-public crate-hatanaka-0.0.2 (c (n "hatanaka") (v "0.0.2") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rinex") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vnkl2pvgwjpplvyjpi8g9jy2wmq16ydh5g4gfy071vikjgg6ljd") (y #t)))

(define-public crate-hatanaka-0.1.0 (c (n "hatanaka") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "~2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "rinex") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wa5x0pmnrgsf6xs40kz5nkv9k57pbq6yclm2dv4nr3yms1vrnzx") (y #t)))

(define-public crate-hatanaka-1.0.0 (c (n "hatanaka") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "~2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "rinex") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rr6n4jkg4vp1wprzq5brz0i38ff1541pj1mlf2j2gz66i67qcmx") (y #t)))

