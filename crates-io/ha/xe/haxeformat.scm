(define-module (crates-io ha xe haxeformat) #:use-module (crates-io))

(define-public crate-haxeformat-0.1.0 (c (n "haxeformat") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "dtoa") (r "^0.4.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0hhgq52bjz3d6g0daj6h9sxypnjmbip4xrspp6172swi2q6g9xqr")))

(define-public crate-haxeformat-0.2.0 (c (n "haxeformat") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "dtoa") (r "^0.4.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0gllb6v3rl5bv4ra8xy6561pwxfa449vq0bgik0qljc5nmi9nwpg")))

(define-public crate-haxeformat-0.2.1 (c (n "haxeformat") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "dtoa") (r "^0.4.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0bjhmc3sda26ksd1b22j60jd5ybh371ry4xkwp42a8p6iq01qa3g")))

(define-public crate-haxeformat-0.2.2 (c (n "haxeformat") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "dtoa") (r "^0.4.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1dznkqj0bdffw520yfadpbbs4mn13a13vn6iddv7nxy1k0lpnj38")))

(define-public crate-haxeformat-0.2.3 (c (n "haxeformat") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "dtoa") (r "^1.0.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "itoa") (r "^1.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "14v5b3f9lvcs6z1bzr6hbdq7cbzhxy4kb49dg68fpm900hws5fim")))

