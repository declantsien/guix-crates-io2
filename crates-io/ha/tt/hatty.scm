(define-module (crates-io ha tt hatty) #:use-module (crates-io))

(define-public crate-hatty-0.1.0 (c (n "hatty") (v "0.1.0") (d (list (d (n "macaddr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "04i4f4lz46i235kxqyjxk0x9f1kpjrhn086kgxqvpwjiw9y1mkg7")))

(define-public crate-hatty-1.0.0 (c (n "hatty") (v "1.0.0") (d (list (d (n "macaddr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1g0ancyl55anpf79f3yb0x07s6wd3l98lppqf09lm9dqbchxa1n0")))

(define-public crate-hatty-1.0.1 (c (n "hatty") (v "1.0.1") (d (list (d (n "macaddr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "10zgara799nbldrs9jy8m45ab4y8qm9wrlcri494k3qbf5kpz1rq")))

