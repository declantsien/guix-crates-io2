(define-module (crates-io ha tt hatter) #:use-module (crates-io))

(define-public crate-hatter-0.0.1 (c (n "hatter") (v "0.0.1") (h "0lgp05n9i9fw1v0vdq3nv0qpzk93w09l12jr8yrpjqs0k586hif8")))

(define-public crate-hatter-0.0.2 (c (n "hatter") (v "0.0.2") (d (list (d (n "rustyline") (r "^6.2.0") (o #t) (d #t) (k 0)))) (h "0i8p6z0ha12alpl315n9k45b3lvch70d8ly0gmsxylczska2vj4g") (f (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.0.3 (c (n "hatter") (v "0.0.3") (d (list (d (n "rustyline") (r "^6.2.0") (o #t) (d #t) (k 0)))) (h "14a1giak5h00kcvkpx4rw0sgcp78snkl03fdmgxbl2i79kyaskcy") (f (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.1.0 (c (n "hatter") (v "0.1.0") (d (list (d (n "rustyline") (r "^6.2.0") (o #t) (d #t) (k 0)))) (h "1c95b88alsg05ikg5wxyx8fn14llpsj9xid8rnqk5vhjarrnn8cw") (f (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.1.1 (c (n "hatter") (v "0.1.1") (d (list (d (n "rustyline") (r "^6.2.0") (o #t) (d #t) (k 0)))) (h "0hg6xs2rkp8f1k424yxchqqk8cpplwisp15d8ibkddwb1bf4chsp") (f (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.1.2 (c (n "hatter") (v "0.1.2") (d (list (d (n "rustyline") (r "^6.2.0") (o #t) (d #t) (k 0)))) (h "05glaqyq37zq0j5bzh6wkh6cmm3f8x2b01wb2mprl1pxd04qqagf") (f (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.1.3 (c (n "hatter") (v "0.1.3") (d (list (d (n "rustyline") (r "^6.2.0") (o #t) (d #t) (k 0)))) (h "12hf4xx0601j6bbqw9zhn6b06316ljrpkmhz8z9i9f3sqqqhlzkx") (f (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.1.4 (c (n "hatter") (v "0.1.4") (d (list (d (n "rustyline") (r "^6.2.0") (o #t) (d #t) (k 0)))) (h "0wiv8kir9d656a2fyy60n0vyjrbkwcw7bh6sbc5575igprczhacc") (f (quote (("repl" "rustyline"))))))

