(define-module (crates-io ha tt hatto) #:use-module (crates-io))

(define-public crate-hatto-0.1.0 (c (n "hatto") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "minidom") (r "^0.15.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.5") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "spdx-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0nc6sz60yvhwwscnnzcq1bv8pgsr0fmk14yjvk3kfkarijsf8c4j")))

