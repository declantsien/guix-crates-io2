(define-module (crates-io ha ud haudit) #:use-module (crates-io))

(define-public crate-haudit-0.1.0 (c (n "haudit") (v "0.1.0") (d (list (d (n "app_dirs") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "hackeraudit-api-common") (r "^0.1") (d #t) (k 0)) (d (n "ignore") (r "^0.3") (d #t) (k 0)) (d (n "pem") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0xhv5f74g3glraccklacgxga8nb9r3skh2pj4ks98v2cmdh0x2z4")))

