(define-module (crates-io ha ru haru-decorator) #:use-module (crates-io))

(define-public crate-haru-decorator-0.1.0 (c (n "haru-decorator") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xarfh4g75cjf9iiidqv02m5v34q81h3ldgxcb6fikcbilnxx76c")))

(define-public crate-haru-decorator-0.2.0 (c (n "haru-decorator") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15w99g2c13zvabn6dis8ibc7zb4605zsn052vd0499chnxw8b6q3")))

(define-public crate-haru-decorator-0.2.1 (c (n "haru-decorator") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19zhpmkwvpa89sh7ybrsg7yv3x4mr2f06m6g1q96jwrcbhymb5fl")))

(define-public crate-haru-decorator-0.2.2 (c (n "haru-decorator") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mlz0sqrgana26y3pagkylsr2hnjfb123nzyals6wsg5a6528fla")))

(define-public crate-haru-decorator-0.2.3 (c (n "haru-decorator") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mv8hd1a0xxh62q6bgvi2yciv30f6m0d25m1sapg7ajhy8m86l1f")))

(define-public crate-haru-decorator-0.21.0 (c (n "haru-decorator") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fpyc81zx275a2dk9jx8qw8mv8j3l5xnvsc6pw09v9c4v7win1k8")))

