(define-module (crates-io kh ai khaiii-sys) #:use-module (crates-io))

(define-public crate-khaiii-sys-0.1.0+0.4 (c (n "khaiii-sys") (v "0.1.0+0.4") (d (list (d (n "bindgen") (r "^0.62.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 1)))) (h "1rq0iqcxjnabk427w3v7sy3nkywymy6wm8xwrbx50cxd9id2gd37") (f (quote (("vendored")))) (l "khaiii")))

