(define-module (crates-io kh ai khaiii-rs) #:use-module (crates-io))

(define-public crate-khaiii-rs-0.1.0 (c (n "khaiii-rs") (v "0.1.0") (d (list (d (n "khaiii-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0mr25fx4d0k4dbgxblpddbfmpa6zk12i4cl1pkcparjcc5fqgq4g") (f (quote (("vendored-khaiii" "khaiii-sys/vendored"))))))

(define-public crate-khaiii-rs-0.1.1 (c (n "khaiii-rs") (v "0.1.1") (d (list (d (n "khaiii-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1630c9gjksfkd31l802clyrjk49g10jjrbap2ksvkz3yjpwafbz3") (f (quote (("vendored-khaiii" "khaiii-sys/vendored"))))))

(define-public crate-khaiii-rs-0.1.2 (c (n "khaiii-rs") (v "0.1.2") (d (list (d (n "khaiii-sys") (r "^0.1.0") (d #t) (k 0)))) (h "14sdzcxyash9hzls7rlfqg1ns8jfnjnpzg9f0zi23nmc79bnlxih") (f (quote (("vendored-khaiii" "khaiii-sys/vendored"))))))

(define-public crate-khaiii-rs-0.1.3 (c (n "khaiii-rs") (v "0.1.3") (d (list (d (n "khaiii-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0qk4v5bw8m5cs28ikk7bm320v7zxb66br48p3r7rkiyasw3bbcfh") (f (quote (("vendored-khaiii" "khaiii-sys/vendored"))))))

(define-public crate-khaiii-rs-0.1.4 (c (n "khaiii-rs") (v "0.1.4") (d (list (d (n "khaiii-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (o #t) (d #t) (k 0)))) (h "1vsfx2gg1zk3k8gs3xbdk0n458qy4kihya2paik9yik3l7v87if9") (f (quote (("vendored-khaiii" "khaiii-sys/vendored") ("serde_support" "serde" "serde_json"))))))

