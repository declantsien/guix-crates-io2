(define-module (crates-io kh at khat) #:use-module (crates-io))

(define-public crate-khat-0.1.0 (c (n "khat") (v "0.1.0") (h "0kdfyzfy7spvn8vk532lv8ay0h4fia9in24gbq5zch8jwygw66mi")))

(define-public crate-khat-0.1.1 (c (n "khat") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z5zc10hcivlbfidypn8mw7yj4smib0gcx9xyglb4j8n9db5r0ic")))

(define-public crate-khat-0.1.2 (c (n "khat") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k4y4p1gph91ywapc2mwvmkr2k3qw3fm5a0qig92c33817m6np6b")))

(define-public crate-khat-0.1.3 (c (n "khat") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0205f122ni4cnr830daln0pyh44kdf02hkzhfnwz5f28v3ki3qpk")))

(define-public crate-khat-0.1.4 (c (n "khat") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "014ybbvjan7glilbzhvnsyv9775pxaiw53iamxd65gia6wmysc7q")))

