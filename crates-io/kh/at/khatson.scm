(define-module (crates-io kh at khatson) #:use-module (crates-io))

(define-public crate-khatson-0.1.0 (c (n "khatson") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tch") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ka66n29x7lkxkrrvp2dkn9g22wf834xy80myw4aak1mxjha7wrq")))

(define-public crate-khatson-0.2.0 (c (n "khatson") (v "0.2.0") (d (list (d (n "chawuek") (r "^0.1.0") (d #t) (k 0)))) (h "1r6ik5chrjn4ilwxzr7vm7fkjgqirxdiqn23q8l37nivzf7q0v3s")))

