(define-module (crates-io kh ak khaki) #:use-module (crates-io))

(define-public crate-khaki-0.1.0 (c (n "khaki") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1.2") (d #t) (k 0)))) (h "13vf4mh7a0xzwldpijbcbpn7zl6kp8kbgh4x7b83xpq7zqgb0xy3")))

(define-public crate-khaki-0.2.0 (c (n "khaki") (v "0.2.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1.2") (d #t) (k 0)))) (h "1afhq4gshigxrx9k3sjbd2gf164bfk63997d0w7zsi9a4dlb48c1")))

