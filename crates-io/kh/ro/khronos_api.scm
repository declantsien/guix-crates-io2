(define-module (crates-io kh ro khronos_api) #:use-module (crates-io))

(define-public crate-khronos_api-0.0.3 (c (n "khronos_api") (v "0.0.3") (h "1mcz8iyvc10klp6ylxps82m7b6fz27ipvlp3y2aqw335d7iqgzn2")))

(define-public crate-khronos_api-0.0.4 (c (n "khronos_api") (v "0.0.4") (h "1vmm6wvsbfl3nrc16vvy5wyrksjqp2ld2vkjwzzxc9m02dlyq6c9")))

(define-public crate-khronos_api-0.0.5 (c (n "khronos_api") (v "0.0.5") (h "11crnkc9vkbjilqc36jn7s1mq68pvh16kwm1amxmcy4chwmsxd66")))

(define-public crate-khronos_api-0.0.6 (c (n "khronos_api") (v "0.0.6") (h "1ihxp61y2gl2b8drwgikb12ki7swz0x5rq60zajv9gdc2prqs4x0")))

(define-public crate-khronos_api-0.0.7 (c (n "khronos_api") (v "0.0.7") (h "0759rsa6r0hyqj71wlkr3zcix5qzp7rlhis73dx5k1igpsvknqly")))

(define-public crate-khronos_api-0.0.8 (c (n "khronos_api") (v "0.0.8") (h "0yc58wmdzvh6jf4ljs0a6z3994skr0zpa9y4fklzf4g8n77jf8i9")))

(define-public crate-khronos_api-1.0.0 (c (n "khronos_api") (v "1.0.0") (h "0c368d96q2v1vhkdyjm8qmmjx9c4187kb84idza2gi3k0rvd7j89")))

(define-public crate-khronos_api-1.0.1 (c (n "khronos_api") (v "1.0.1") (h "19wib9dybhlgxpwp77sl5bbck4qas2vfldx43j7syrfn64m8x86m")))

(define-public crate-khronos_api-1.1.0 (c (n "khronos_api") (v "1.1.0") (h "0v8cgxbcqqncny7mkljii65ixfpbxn3n4z69avgxv439c321p373") (y #t)))

(define-public crate-khronos_api-2.0.0 (c (n "khronos_api") (v "2.0.0") (h "1pbacdqm5vpqp0fag9as22vi3b03mjg6fc9pa3n7z2pbrx2wcryq")))

(define-public crate-khronos_api-2.1.0 (c (n "khronos_api") (v "2.1.0") (h "0mcgsnbpzfhc2cfnd9ig2z5qgz7b0nc8qqvgjg2n14h5qky27vyr")))

(define-public crate-khronos_api-2.2.0 (c (n "khronos_api") (v "2.2.0") (h "0m5mpi5zyzzbsjkfymfkzib577ii8lk3l5p9sgxvarrzqdrb8yh3")))

(define-public crate-khronos_api-3.0.0 (c (n "khronos_api") (v "3.0.0") (h "0jgbb8rjay6qynx2rn4cc7qq3plny0xk4s8ls8f8gmbb69npw8v2")))

(define-public crate-khronos_api-3.1.0 (c (n "khronos_api") (v "3.1.0") (h "1p0xj5mlbagqyvvnv8wmv3cr7l9y1m153888pxqwg3vk3mg5inz2")))

