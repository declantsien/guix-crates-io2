(define-module (crates-io kh tm khtml) #:use-module (crates-io))

(define-public crate-khtml-0.1.0 (c (n "khtml") (v "0.1.0") (h "1gqq0im32vcdjqw75vsq6djfcybkxsa2f75h2ryc626lxn8b4rsk")))

(define-public crate-khtml-0.1.1 (c (n "khtml") (v "0.1.1") (h "0r33smb8vsfn7cs3p6hbm5g0bfrpcs2pzg5k38m3rkh3qgp12fxs")))

(define-public crate-khtml-0.1.2 (c (n "khtml") (v "0.1.2") (h "1swc5inij8i2rf8rma2s8kycnv4ysq5dzmilrg0brffvadj4zg9q")))

(define-public crate-khtml-0.1.3 (c (n "khtml") (v "0.1.3") (h "0ka6i8q7bas6k8g15hs0d5j09a467ljng10fv9dwh5wq02idi0k5")))

(define-public crate-khtml-0.1.4 (c (n "khtml") (v "0.1.4") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "10l3wz3lzhbbs8pjs5gm7pdhb1mrfnxwaj9nj4in3nih8kwiidb1")))

