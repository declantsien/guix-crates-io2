(define-module (crates-io kh al khalzam) #:use-module (crates-io))

(define-public crate-khalzam-0.1.0 (c (n "khalzam") (v "0.1.0") (d (list (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)))) (h "140hdk5alc5q3rm611w8hmdmmpdmixfz4g3h7rk0n8y37k2nc8vy")))

(define-public crate-khalzam-0.2.0 (c (n "khalzam") (v "0.2.0") (d (list (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)))) (h "1pphf6byzbkigzawrhqmjvqf930ypw9vwl67pgxym9nj3wwx0npv")))

(define-public crate-khalzam-0.2.1 (c (n "khalzam") (v "0.2.1") (d (list (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)))) (h "0qnvqrz3a0c0595ar1xammk7wc0hc78qa9naiyrbmmjh7v833wpy")))

(define-public crate-khalzam-0.2.2 (c (n "khalzam") (v "0.2.2") (d (list (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)))) (h "1a0q9yq7338iximshn3nmbwyd5463nyl0gqs8dry0qxzxjrqg128")))

(define-public crate-khalzam-0.3.2 (c (n "khalzam") (v "0.3.2") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.17") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.12") (d #t) (k 2)))) (h "15d0fff39i0nprq7zhhzlyc9gzf6d2gq7vpp7rnarn0fz4l606ms")))

(define-public crate-khalzam-0.3.3 (c (n "khalzam") (v "0.3.3") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)) (d (n "shrust") (r "^0.0.6") (d #t) (k 2)) (d (n "tokio") (r "^0.1.17") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.12") (d #t) (k 2)))) (h "01ccsn90z7bhmf4qgzrzaclzf3y05gldw84l0d8gmnwjfiwdzsl4")))

(define-public crate-khalzam-0.3.4 (c (n "khalzam") (v "0.3.4") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)) (d (n "shrust") (r "^0.0.6") (d #t) (k 2)) (d (n "tokio") (r "^0.1.17") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.12") (d #t) (k 2)))) (h "0d8ggxci6lvxbyh02j27my1ggc9xl3g4yd6fr1wsnj11rr6nlrn5")))

(define-public crate-khalzam-0.3.5 (c (n "khalzam") (v "0.3.5") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)) (d (n "shrust") (r "^0.0.6") (d #t) (k 2)) (d (n "tokio") (r "^0.1.17") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.12") (d #t) (k 2)))) (h "1ia5db502dz03f7v5qihqffdsh18h9kkf8cvrw9arqwl3d01nprd")))

(define-public crate-khalzam-0.3.6 (c (n "khalzam") (v "0.3.6") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)) (d (n "shrust") (r "^0.0.6") (d #t) (k 2)) (d (n "tokio") (r "^0.1.17") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.12") (d #t) (k 2)))) (h "0srpqqxjzp0rxb82z2fh1209m28rk8iq8klzrqfxl2nm82ddn0gp")))

(define-public crate-khalzam-0.3.7 (c (n "khalzam") (v "0.3.7") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.17") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.12") (d #t) (k 2)))) (h "1idjajlmyhfr8visbykc5h3lz4rw558rnf81fv0r5kb7prh47xx7")))

(define-public crate-khalzam-0.3.8 (c (n "khalzam") (v "0.3.8") (d (list (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 2)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)))) (h "1n1pr809rn39b2fck5hjd7fcg9jkmw69fw9p0w7ck152x6wzrc3h")))

(define-public crate-khalzam-0.3.9 (c (n "khalzam") (v "0.3.9") (d (list (d (n "minimp3") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.16.0-rc.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.5") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.15.0-rc.1") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 2)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)))) (h "0k5xd9rn8ng0kh56wgyld5racz964358wxahm5illz31rbbryy1n")))

