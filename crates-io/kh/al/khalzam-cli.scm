(define-module (crates-io kh al khalzam-cli) #:use-module (crates-io))

(define-public crate-khalzam-cli-0.1.0 (c (n "khalzam-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "khalzam") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio-threadpool") (r "^0.1.12") (d #t) (k 0)))) (h "04sxlch1hkdp989zwqfdd6zf7aiwa64j459h7fdhcgdci5j2mz4n")))

(define-public crate-khalzam-cli-0.1.1 (c (n "khalzam-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "khalzam") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio-threadpool") (r "^0.1.12") (d #t) (k 0)))) (h "04hby3dc458hc919pv2lgwbr888d5k6dhnsv2zkgs55knz4vaab2")))

(define-public crate-khalzam-cli-0.1.2 (c (n "khalzam-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "khalzam") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio-threadpool") (r "^0.1.12") (d #t) (k 0)))) (h "0mdki5a9vz1fn54m46fp7zvqm61v2djs4f7s57h2sv9i81wqrj38")))

(define-public crate-khalzam-cli-0.1.3 (c (n "khalzam-cli") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "khalzam") (r "^0.3.7") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0mzv8prfxcywprgjs2z2xyipns429sxyx743kaq37lqaxvh4m5zy")))

(define-public crate-khalzam-cli-0.1.4 (c (n "khalzam-cli") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "khalzam") (r "^0.3.8") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)))) (h "14pd6n999hl7whcll7g2h8pnrhpfjil0v1y9br9agaiza9s5qbvc")))

(define-public crate-khalzam-cli-0.1.5 (c (n "khalzam-cli") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "khalzam") (r "^0.3.9") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)))) (h "0vmdk3jkj950fwwai1dkgfhb2acp96jbh2v5zxiqssgirnc022n9")))

