(define-module (crates-io kh me khmercut) #:use-module (crates-io))

(define-public crate-khmercut-0.1.5 (c (n "khmercut") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crfs") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)))) (h "0w567lalgcfrrblvd5px3vk432znx7bdsqiz6igrcawyryvi5s3m")))

