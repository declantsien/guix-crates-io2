(define-module (crates-io ay ti aytina) #:use-module (crates-io))

(define-public crate-aytina-1.0.0 (c (n "aytina") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nlkxw4vasl6m3p73jj5p8y0k1dh045l2zk8q9v3ly6sq32d8zmy")))

