(define-module (crates-io ay a_ aya_poker) #:use-module (crates-io))

(define-public crate-aya_poker-0.1.0 (c (n "aya_poker") (v "0.1.0") (d (list (d (n "aya_base") (r "^0.1.0") (d #t) (k 0)) (d (n "aya_codegen") (r "^0.1.0") (d #t) (k 1)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "quickdiv") (r "^0.1.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0nr6zpvqmwzndalm13y9s916fjaja2w6ypv3fflijsydis1yaaxc") (f (quote (("std" "aya_base/std") ("colored-4color" "aya_base/colored" "aya_base/colored-4color") ("colored" "aya_base/colored")))) (r "1.64")))

