(define-module (crates-io ay a_ aya_base) #:use-module (crates-io))

(define-public crate-aya_base-0.1.0 (c (n "aya_base") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (o #t) (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0a92kipqlhzr6pjbyagr70nwk6zsl00x2fjxm7223bxnmm74aspm") (f (quote (("std" "fastrand/std") ("colored-4color")))) (r "1.64")))

