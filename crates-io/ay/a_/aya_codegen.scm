(define-module (crates-io ay a_ aya_codegen) #:use-module (crates-io))

(define-public crate-aya_codegen-0.1.0 (c (n "aya_codegen") (v "0.1.0") (d (list (d (n "aya_base") (r "^0.1.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "miniphf") (r "^0.1.0") (d #t) (k 0)))) (h "1kycbgvfal0il6dh08qrsirw4nj21dqjifp39aamnc2ifnaf9xiq") (r "1.64")))

