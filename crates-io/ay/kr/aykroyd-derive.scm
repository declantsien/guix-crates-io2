(define-module (crates-io ay kr aykroyd-derive) #:use-module (crates-io))

(define-public crate-aykroyd-derive-0.1.0 (c (n "aykroyd-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "169w7s893x9hvpirdkq3bnvilkfncxbkxcqqxy1zrmklsmjaxnf2")))

(define-public crate-aykroyd-derive-0.2.0-rc.1 (c (n "aykroyd-derive") (v "0.2.0-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "07sx08x21kva72pb7gby5yd2417b4z2y96dkgjscfv8d67finaqq")))

(define-public crate-aykroyd-derive-0.2.0 (c (n "aykroyd-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0r49zx6idkpq76d97b3xfmapx06m2bkc9mcm26aqvp8rpf2myasr")))

(define-public crate-aykroyd-derive-0.2.1 (c (n "aykroyd-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "15j3c7079agzdir53xlhb29sih0pgd5xgrshb18ndwyxwbaxls69")))

