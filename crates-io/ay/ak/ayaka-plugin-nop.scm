(define-module (crates-io ay ak ayaka-plugin-nop) #:use-module (crates-io))

(define-public crate-ayaka-plugin-nop-0.2.0 (c (n "ayaka-plugin-nop") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ayaka-plugin") (r "^0.2.0") (d #t) (k 0)))) (h "1dcypflbbw67nhbyrhqz8iic0w0lzph67nbxkv8lbj8n12vm45an")))

