(define-module (crates-io ay ak ayaka-model) #:use-module (crates-io))

(define-public crate-ayaka-model-0.2.0 (c (n "ayaka-model") (v "0.2.0") (d (list (d (n "ayaka-runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stream-future") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trylog") (r "^0.3") (d #t) (k 0)))) (h "0f53ilmj3nz7is4hsaph9zckzy9c5vbxkgngnwniw6961dwbjmyx")))

(define-public crate-ayaka-model-0.3.0 (c (n "ayaka-model") (v "0.3.0") (d (list (d (n "ayaka-plugin") (r "^0.3.0") (d #t) (k 0)) (d (n "ayaka-plugin-wasmi") (r "^0.3.0") (d #t) (k 2)) (d (n "ayaka-runtime") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "stream-future") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trylog") (r "^0.3") (d #t) (k 0)))) (h "1555p24q97bc1ma9hy66xqs7n9hjm195bbc91mxi8yh3wj83dhzf")))

