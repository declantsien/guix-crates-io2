(define-module (crates-io ay ak ayaka-plugin-wasmi) #:use-module (crates-io))

(define-public crate-ayaka-plugin-wasmi-0.2.0 (c (n "ayaka-plugin-wasmi") (v "0.2.0") (d (list (d (n "ayaka-plugin") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmi") (r "^0.28") (d #t) (k 0)))) (h "109hxkiy6l25f9f41kd7avwhby38g6xlyflgvcwjnqpnyr6q4njr")))

(define-public crate-ayaka-plugin-wasmi-0.3.0 (c (n "ayaka-plugin-wasmi") (v "0.3.0") (d (list (d (n "ayaka-plugin") (r "^0.3.0") (d #t) (k 0)) (d (n "wasmi") (r "^0.31") (d #t) (k 0)))) (h "1accnffa86kyq4vyh5xjip9rizfqjwj9fw0qcd8krl6vxk1q7anp")))

