(define-module (crates-io ay ak ayaka-plugin) #:use-module (crates-io))

(define-public crate-ayaka-plugin-0.2.0 (c (n "ayaka-plugin") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1571dwjckjzfwzn5cv439y1r03dsmslsiaanbm2jh64bghv5w4dx")))

(define-public crate-ayaka-plugin-0.3.0 (c (n "ayaka-plugin") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "10habx8p27pbsy1lja677rzxprz3jhfhvmbkj0qfff1dp4s69h3v")))

