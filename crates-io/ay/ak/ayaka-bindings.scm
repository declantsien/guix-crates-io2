(define-module (crates-io ay ak ayaka-bindings) #:use-module (crates-io))

(define-public crate-ayaka-bindings-0.2.0 (c (n "ayaka-bindings") (v "0.2.0") (d (list (d (n "ayaka-bindings-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "ayaka-bindings-types") (r "^0.2.0") (d #t) (k 0)) (d (n "ayaka-primitive") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "vfs") (r "^0.9") (d #t) (k 0)))) (h "1lwx4qja8c94f31j90f7bjn6dc1rx0bxjs53fbzx0cfzqb7rrsxq")))

(define-public crate-ayaka-bindings-0.3.0 (c (n "ayaka-bindings") (v "0.3.0") (d (list (d (n "ayaka-bindings-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "ayaka-bindings-types") (r "^0.3.0") (d #t) (k 0)) (d (n "ayaka-primitive") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "vfs") (r "^0.10") (d #t) (k 0)))) (h "1j3fxcx1syzfkyqvlgr1g9dqvrdhhs0z7gwq3iiwj3vy45qib9sh")))

