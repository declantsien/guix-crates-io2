(define-module (crates-io ay ak ayaka-bindings-impl) #:use-module (crates-io))

(define-public crate-ayaka-bindings-impl-0.2.0 (c (n "ayaka-bindings-impl") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y1wjhwdw448yjzk55f7f8j71pr0fn44d95xidk8lnpqzx25q3d4")))

(define-public crate-ayaka-bindings-impl-0.3.0 (c (n "ayaka-bindings-impl") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1r7cc8i049dkbd34nvjdrg0j6iahfbhnkv2dg45z6avxz4rch351")))

