(define-module (crates-io ay ak ayaka-primitive) #:use-module (crates-io))

(define-public crate-ayaka-primitive-0.2.0 (c (n "ayaka-primitive") (v "0.2.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^2.2") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "1l1d5cgkcpaf1m2b7h48vybsyplc0i7dhwidna4c8zfyhw3x3c4y")))

(define-public crate-ayaka-primitive-0.3.0 (c (n "ayaka-primitive") (v "0.3.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "1w0i42sdkrbq1v9286v7aa4ifxag4ad9vc6cjdzs9lv0s1q5fzm3")))

