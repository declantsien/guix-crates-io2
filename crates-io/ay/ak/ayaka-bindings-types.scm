(define-module (crates-io ay ak ayaka-bindings-types) #:use-module (crates-io))

(define-public crate-ayaka-bindings-types-0.2.0 (c (n "ayaka-bindings-types") (v "0.2.0") (d (list (d (n "ayaka-primitive") (r "^0.2.0") (d #t) (k 0)) (d (n "fallback") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vfs") (r "^0.9") (d #t) (k 0)))) (h "1bjsb4v5kzinhd6dbxvj4h7p50n18yq1jb0cmvd06cdlcwzz4y6n")))

(define-public crate-ayaka-bindings-types-0.3.0 (c (n "ayaka-bindings-types") (v "0.3.0") (d (list (d (n "ayaka-primitive") (r "^0.3.0") (d #t) (k 0)) (d (n "fallback") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vfs") (r "^0.10") (d #t) (k 0)))) (h "0db1s1c1wvnfcdari051fiadl5rvs4rk0zpf6sjprzwanly207j9")))

