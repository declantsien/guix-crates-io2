(define-module (crates-io ay ak ayaka-plugin-wasmer) #:use-module (crates-io))

(define-public crate-ayaka-plugin-wasmer-0.2.0 (c (n "ayaka-plugin-wasmer") (v "0.2.0") (d (list (d (n "ayaka-plugin") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmer") (r "^3.1") (d #t) (k 0)))) (h "008ac0qc7w0br0xb8n4wqb9dixq218ya4rck2pmw6jv3qjyqjsjw")))

(define-public crate-ayaka-plugin-wasmer-0.3.0 (c (n "ayaka-plugin-wasmer") (v "0.3.0") (d (list (d (n "ayaka-plugin") (r "^0.3.0") (d #t) (k 0)) (d (n "wasmer") (r "^4") (d #t) (k 0)))) (h "0nz9m988nkpr3ca3sinyd3x3mfqisk1c0i9j306bxr5xgkd0jlfh")))

