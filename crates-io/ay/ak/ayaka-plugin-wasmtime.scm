(define-module (crates-io ay ak ayaka-plugin-wasmtime) #:use-module (crates-io))

(define-public crate-ayaka-plugin-wasmtime-0.2.0 (c (n "ayaka-plugin-wasmtime") (v "0.2.0") (d (list (d (n "ayaka-plugin") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmtime") (r "^6.0") (d #t) (k 0)))) (h "0hlyqsyab4wg020k4jmb928anrwwp4k85caknhb5s0i5jifr046i")))

(define-public crate-ayaka-plugin-wasmtime-0.3.0 (c (n "ayaka-plugin-wasmtime") (v "0.3.0") (d (list (d (n "ayaka-plugin") (r "^0.3.0") (d #t) (k 0)) (d (n "wasmtime") (r "^15") (d #t) (k 0)))) (h "0il817m8zh6n77b8ds7225nyy1yvv6197v4m0c89c6ygxcwaasbz")))

