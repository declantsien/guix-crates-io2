(define-module (crates-io ay ch aych-delay) #:use-module (crates-io))

(define-public crate-aych-delay-0.1.0 (c (n "aych-delay") (v "0.1.0") (d (list (d (n "rodio") (r "^0.16.0") (d #t) (k 2)))) (h "1z8w8qriyl18s88hwr76n6zpvkv1109mgypn51xvc47jk8jnwjcc")))

(define-public crate-aych-delay-0.1.1 (c (n "aych-delay") (v "0.1.1") (d (list (d (n "rodio") (r "^0.16.0") (d #t) (k 2)))) (h "1w5w28zgg81cq9qdvpm0k4vhckai0ykvl80zz1qxjg0yq81frl62")))

