(define-module (crates-io ay a- aya-log-common) #:use-module (crates-io))

(define-public crate-aya-log-common-0.1.1 (c (n "aya-log-common") (v "0.1.1") (d (list (d (n "aya") (r "^0.10.5") (o #t) (d #t) (k 0)))) (h "0ipnkz4s1br5splg8vpswhbybv32q7v04rly8ysnh90542bjjvy1") (f (quote (("userspace" "aya") ("default"))))))

(define-public crate-aya-log-common-0.1.2 (c (n "aya-log-common") (v "0.1.2") (d (list (d (n "aya") (r "^0.10.7") (o #t) (d #t) (k 0)))) (h "1k0sdk290r5iv83in09441xvw5a8qpdbx6rclbyyw4bra44jhk78") (f (quote (("userspace" "aya") ("default")))) (y #t)))

(define-public crate-aya-log-common-0.1.3 (c (n "aya-log-common") (v "0.1.3") (d (list (d (n "aya") (r "^0.10.7") (o #t) (d #t) (k 0)))) (h "1lggzdpp11q84ya35rsd4ki7sdpjz4z0isac4vz0dqw7vrjp67y2") (f (quote (("userspace" "aya") ("default")))) (y #t)))

(define-public crate-aya-log-common-0.1.5 (c (n "aya-log-common") (v "0.1.5") (d (list (d (n "aya") (r "^0.10.7") (o #t) (d #t) (k 0)))) (h "1mkb4s004gam4krrpjk2hazq6xi3dlwmn91ccly75jdk30d7d8fj") (f (quote (("userspace" "aya") ("default")))) (y #t)))

(define-public crate-aya-log-common-0.1.7 (c (n "aya-log-common") (v "0.1.7") (d (list (d (n "aya") (r "^0.10.7") (o #t) (d #t) (k 0)))) (h "1s29mh0s0rfxsx60np540h70szwvl920aiy2ghkhnsgv4c0rvckn") (f (quote (("userspace" "aya") ("default")))) (y #t)))

(define-public crate-aya-log-common-0.1.8 (c (n "aya-log-common") (v "0.1.8") (d (list (d (n "aya") (r "^0.10.7") (o #t) (d #t) (k 0)))) (h "14v16qr81a74746cv9vvm8nayczpxr1fjdznp0rz747wg1gl7jgb") (f (quote (("userspace" "aya") ("default")))) (y #t)))

(define-public crate-aya-log-common-0.1.9 (c (n "aya-log-common") (v "0.1.9") (d (list (d (n "aya") (r "^0.10.7") (o #t) (d #t) (k 0)))) (h "1d9xnk8rjrg2qx5rcwcyx2s1l0ikn4z7wlfphfdi1xnlhwqm070n") (f (quote (("userspace" "aya") ("default"))))))

(define-public crate-aya-log-common-0.1.10 (c (n "aya-log-common") (v "0.1.10") (d (list (d (n "aya") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "133knqy8plmdcs5csn7i5vr9gfb9f76vmn57bdsiqp1x4dr1ifd3") (f (quote (("userspace" "aya") ("default"))))))

(define-public crate-aya-log-common-0.1.11 (c (n "aya-log-common") (v "0.1.11") (d (list (d (n "aya") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "1kihpj61108l2l9gpzkx2mrimfrwfvanzibz3qjbjfha4fsd8al7") (f (quote (("userspace" "aya") ("default"))))))

(define-public crate-aya-log-common-0.1.12 (c (n "aya-log-common") (v "0.1.12") (d (list (d (n "aya") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "1868b5iwzv92dvscdjmn5f5l73wnj8f53dw0xllfhdy79p9fm2na") (f (quote (("userspace" "aya") ("default")))) (y #t)))

(define-public crate-aya-log-common-0.1.13 (c (n "aya-log-common") (v "0.1.13") (d (list (d (n "aya") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "0p4l7bb7l4zxii1wv22vmffv69zbdr5d6yf4srp1bndkyizjj4q2") (f (quote (("userspace" "aya") ("default"))))))

(define-public crate-aya-log-common-0.1.14 (c (n "aya-log-common") (v "0.1.14") (d (list (d (n "num_enum") (r "^0.7") (k 0)))) (h "1nsfvpw4d4f9j9c5i2ws5mj0i55kn6263b2cw0jdrmg23qsqmlxn")))

