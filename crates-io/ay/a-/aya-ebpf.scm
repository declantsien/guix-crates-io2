(define-module (crates-io ay a- aya-ebpf) #:use-module (crates-io))

(define-public crate-aya-ebpf-0.1.0 (c (n "aya-ebpf") (v "0.1.0") (d (list (d (n "aya-ebpf-bindings") (r "^0.1.0") (d #t) (k 0)) (d (n "aya-ebpf-cty") (r "^0.2.1") (d #t) (k 0)) (d (n "aya-ebpf-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "const-assert") (r "^1.0.1") (o #t) (k 0)) (d (n "rustversion") (r "^1.0.0") (k 1)))) (h "1lqfsy37v6vygdnpxsz20h6v6kzp4b5k2f3nh8qqf5wsqkh8scdp") (f (quote (("default") ("const_assert" "const-assert"))))))

