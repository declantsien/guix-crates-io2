(define-module (crates-io ay a- aya-log-nightly) #:use-module (crates-io))

(define-public crate-aya-log-nightly-0.2.0 (c (n "aya-log-nightly") (v "0.2.0") (d (list (d (n "aya-log-common-nightly") (r "^0.1.14") (k 0)) (d (n "aya-nightly") (r "^0.12.0") (f (quote ("async_tokio"))) (d #t) (k 0)) (d (n "bytes") (r "^1") (k 0)) (d (n "env_logger") (r "^0.11") (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "testing_logger") (r "^0.1.1") (k 2)) (d (n "thiserror") (r "^1") (k 0)) (d (n "tokio") (r "^1.24.0") (f (quote ("rt"))) (k 0)))) (h "03lm32kip1rhlhwfxs5r0fcf25wk8iy2nas060l040g1l12j5vf2") (y #t)))

