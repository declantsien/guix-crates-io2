(define-module (crates-io ay a- aya-ebpf-macros) #:use-module (crates-io))

(define-public crate-aya-ebpf-macros-0.1.0 (c (n "aya-ebpf-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0p71z39zm17iygnwzl6rw9z7slmb8vfzn3ql98l84xalhg620y6f")))

