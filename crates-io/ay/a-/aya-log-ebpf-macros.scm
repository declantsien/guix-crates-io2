(define-module (crates-io ay a- aya-log-ebpf-macros) #:use-module (crates-io))

(define-public crate-aya-log-ebpf-macros-0.1.0 (c (n "aya-log-ebpf-macros") (v "0.1.0") (d (list (d (n "aya-log-common") (r "^0.1.14") (k 0)) (d (n "aya-log-parser") (r "^0.1.13") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (k 0)))) (h "1i0vy2r015wp22idfi9qfbr0xivpdfm42849a7dpfq7mfld2bn7n")))

