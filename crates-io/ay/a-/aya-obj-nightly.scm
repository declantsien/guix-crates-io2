(define-module (crates-io ay a- aya-obj-nightly) #:use-module (crates-io))

(define-public crate-aya-obj-nightly-0.1.0 (c (n "aya-obj-nightly") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (k 2)) (d (n "bytes") (r "^1") (k 0)) (d (n "core-error") (r "^0.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "object") (r "^0.32") (f (quote ("elf" "read_core"))) (k 0)) (d (n "rbpf") (r "^0.2.0") (k 2)) (d (n "thiserror") (r "^1") (k 0)))) (h "0r79z4mkk4wp0x57g2yc687rrh65c62qrlw3lygwhvykhk3bkg9f") (f (quote (("std")))) (y #t)))

