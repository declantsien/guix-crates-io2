(define-module (crates-io ay a- aya-log-ebpf) #:use-module (crates-io))

(define-public crate-aya-log-ebpf-0.1.0 (c (n "aya-log-ebpf") (v "0.1.0") (d (list (d (n "aya-ebpf") (r "^0.1.0") (d #t) (k 0)) (d (n "aya-log-common") (r "^0.1.14") (d #t) (k 0)) (d (n "aya-log-ebpf-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1cyvcpzbj36npq93iw4781q4bwbs5nq2pkdi3slrb642s2nvn41a")))

