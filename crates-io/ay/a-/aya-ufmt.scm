(define-module (crates-io ay a- aya-ufmt) #:use-module (crates-io))

(define-public crate-aya-ufmt-0.1.0 (c (n "aya-ufmt") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "ufmt-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "0wvwgkm1i0ra127s2p4z1i85aw11lrcys8ssynhkp1ahzvyc7h9n") (f (quote (("std" "ufmt-write/std"))))))

