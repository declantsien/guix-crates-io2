(define-module (crates-io ay ay ayaya) #:use-module (crates-io))

(define-public crate-ayaya-1.0.0 (c (n "ayaya") (v "1.0.0") (h "1fv03ps5accihxvf16d9dddjdkiaqlafxflk1kw0cbv7sir3cff2")))

(define-public crate-ayaya-2.0.0 (c (n "ayaya") (v "2.0.0") (d (list (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)))) (h "0px1mv14s1cx61dmqjspy4g01ripbl312rxrk019h98bn0wac62i")))

(define-public crate-ayaya-2.0.1 (c (n "ayaya") (v "2.0.1") (d (list (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)))) (h "1frvx6sanpb6wcjirn770lzcq005x2qcq94jlsg9cb6lyhr9bywf")))

(define-public crate-ayaya-2.0.2 (c (n "ayaya") (v "2.0.2") (d (list (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)))) (h "0lb9xshh2czkbqichgmvnq43xkycp4madmmy9ybgq42cb7z5kq55")))

