(define-module (crates-io ay mr aymr) #:use-module (crates-io))

(define-public crate-aymr-0.0.1 (c (n "aymr") (v "0.0.1") (d (list (d (n "sled") (r "^0.34.7") (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)))) (h "00jg57hrknjq8ib3vrbq5dw3f6vr0zgm14jqlgd8rczl48cgr4z4") (f (quote (("sled_pre" "sled") ("hashmap") ("default" "hashmap") ("btreemap"))))))

