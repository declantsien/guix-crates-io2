(define-module (crates-io ut -d ut-dialog) #:use-module (crates-io))

(define-public crate-ut-dialog-0.1.0 (c (n "ut-dialog") (v "0.1.0") (d (list (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)))) (h "1a2hv1lgz708bx3b4zngj8zzrll06n2kdnd2cba7nhs2pf325gv1")))

