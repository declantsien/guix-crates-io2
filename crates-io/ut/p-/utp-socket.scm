(define-module (crates-io ut p- utp-socket) #:use-module (crates-io))

(define-public crate-utp-socket-0.1.0 (c (n "utp-socket") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("time" "sync" "macros"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (f (quote ("bytes" "bytes"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.10") (d #t) (k 2)))) (h "1f7b576mggn5inmasc9amgc670c30lz8w9ylqqa1p0ps1354mni8")))

