(define-module (crates-io ut ct utctimestamp) #:use-module (crates-io))

(define-public crate-utctimestamp-0.1.0 (c (n "utctimestamp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gy8q42hhkvv472601w69752pbvibc5pw65zjp0q05v6yikbncb1") (f (quote (("serde-support" "serde") ("default"))))))

(define-public crate-utctimestamp-0.1.1 (c (n "utctimestamp") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03rr3plfcvzw5d6kxm4q5cj4a9jnfn6hgkwsqf4asrrf7y11cwpa") (f (quote (("serde-support" "serde") ("default"))))))

(define-public crate-utctimestamp-0.1.2 (c (n "utctimestamp") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zd6pf118bl8x9imi602q02z5bdh0i4dysjxf3b02gsnnl2023qp") (f (quote (("serde-support" "serde") ("default"))))))

(define-public crate-utctimestamp-0.1.3 (c (n "utctimestamp") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hhxdk0cgp5mf1nr8j4i2y2iyqdgs7j6bmnywg1cyxs93lxfn6qa") (f (quote (("serde-support" "serde") ("default"))))))

(define-public crate-utctimestamp-0.1.4 (c (n "utctimestamp") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mw1l8jz5lwj4iwbk3gjp1v0zkd0ilj2c1xwgnh39d0i1nkfngff") (f (quote (("serde-support" "serde") ("default"))))))

