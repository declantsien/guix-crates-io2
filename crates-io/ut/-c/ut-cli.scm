(define-module (crates-io ut -c ut-cli) #:use-module (crates-io))

(define-public crate-ut-cli-0.1.3 (c (n "ut-cli") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0grws6lchkzicz7nd3q9lp2r61zbkrkagslpgp4xwwpcz56gx316")))

(define-public crate-ut-cli-0.1.4 (c (n "ut-cli") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "12jx9cvalv7gg03lcjyrq4k854wsh3dini7nk6qkrvnlp3mdqqxk")))

(define-public crate-ut-cli-0.1.5 (c (n "ut-cli") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0nc7i6sgsf5b0jqf46iy7bi50fqaf6r94g3dzk8kh80l0x58a1jn")))

(define-public crate-ut-cli-0.1.6 (c (n "ut-cli") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.17") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17") (d #t) (k 0)))) (h "1c4f8hj7flbgw216draypg0b7z6mncv9npg4ydjyk5696967y84g")))

(define-public crate-ut-cli-0.1.7 (c (n "ut-cli") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "0wm3l1caqmv7rmjz6h4fj14al4xqwikypgwxrc14kykid050npjs")))

