(define-module (crates-io ut un utuntap) #:use-module (crates-io))

(define-public crate-utuntap-0.1.0 (c (n "utuntap") (v "0.1.0") (d (list (d (n "etherparse") (r "~0.9") (d #t) (k 2)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.17") (d #t) (k 0)) (d (n "serial_test") (r "~0.4") (d #t) (k 2)))) (h "02vv6x3y91gyp4889xn17lf9l47i543d4ja4yzb8gwilvljxn2y6")))

(define-public crate-utuntap-0.1.1 (c (n "utuntap") (v "0.1.1") (d (list (d (n "etherparse") (r "~0.9") (d #t) (k 2)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.17") (d #t) (k 0)) (d (n "serial_test") (r "~0.4") (d #t) (k 2)))) (h "0sjxfi4vbzv9d0783v53ldhrjlx030nsksj2kp6mx2zbqscnj0xw")))

(define-public crate-utuntap-0.2.0 (c (n "utuntap") (v "0.2.0") (d (list (d (n "etherparse") (r "~0.9") (d #t) (k 2)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.17") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "serial_test") (r "~0.4") (d #t) (k 2)))) (h "0z4m1c13vz38bw4bf034ciywy3wwy0djlk9hw4xkck8s9nkhy1zm")))

(define-public crate-utuntap-0.2.1 (c (n "utuntap") (v "0.2.1") (d (list (d (n "etherparse") (r "~0.9") (d #t) (k 2)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.17") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "serial_test") (r "~0.4") (d #t) (k 2)))) (h "1g6cjjgiwskc6iw7mcrlmr62cjbcwrl2zgn4s74glsbl1cf5jxyn") (y #t)))

(define-public crate-utuntap-0.2.2 (c (n "utuntap") (v "0.2.2") (d (list (d (n "etherparse") (r "~0.9") (d #t) (k 2)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.17") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "serial_test") (r "~0.4") (d #t) (k 2)))) (h "0nqmsa09iq3r8vscpw6gbvvj4qn0nxs3y0qddjjayxxv79in84jv")))

(define-public crate-utuntap-0.2.3 (c (n "utuntap") (v "0.2.3") (d (list (d (n "etherparse") (r "~0.9") (d #t) (k 2)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.17") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "serial_test") (r "~0.4") (d #t) (k 2)))) (h "1dz6qwhjlhs752amwqipij02rlbqr208mjvwrpy3v4990lfx6p3z")))

(define-public crate-utuntap-0.2.4 (c (n "utuntap") (v "0.2.4") (d (list (d (n "etherparse") (r "~0.9") (d #t) (k 2)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.17") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "serial_test") (r "~0.4") (d #t) (k 2)))) (h "0p6xipa1wy6b5l9diafr3zqfz3v4hrq0qb1i4rgwgmnv4dk4skpw")))

(define-public crate-utuntap-0.2.5 (c (n "utuntap") (v "0.2.5") (d (list (d (n "etherparse") (r "~0.10.1") (d #t) (k 2)) (d (n "libc") (r "~0.2.126") (d #t) (k 0)) (d (n "nix") (r "~0.24.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "serial_test") (r "~0.7") (d #t) (k 2)))) (h "1iamykxpq8lr66sm8lbvbdvfm108g9wi4lyzshc4w7c4zn69kvx2")))

(define-public crate-utuntap-0.3.0 (c (n "utuntap") (v "0.3.0") (d (list (d (n "etherparse") (r "~0.10.1") (d #t) (k 2)) (d (n "libc") (r "~0.2.126") (d #t) (k 0)) (d (n "nix") (r "~0.26.1") (f (quote ("ioctl"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "serial_test") (r "~0.7") (d #t) (k 2)))) (h "1k40cakxbcgc1cybzkx8jqbzv38a9h8ws1kh1bk2gjw05x3i6jla")))

(define-public crate-utuntap-0.3.1 (c (n "utuntap") (v "0.3.1") (d (list (d (n "etherparse") (r "~0.10.1") (d #t) (k 2)) (d (n "libc") (r "~0.2.126") (d #t) (k 0)) (d (n "nix") (r "~0.26.1") (f (quote ("ioctl"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "serial_test") (r "~0.7") (d #t) (k 2)))) (h "1v5hmifgvl4w5dv4nfr6nr8xpi3r8s15xcskg5y45akyir6jf6yb")))

