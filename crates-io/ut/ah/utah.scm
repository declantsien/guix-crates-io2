(define-module (crates-io ut ah utah) #:use-module (crates-io))

(define-public crate-utah-0.1.0 (c (n "utah") (v "0.1.0") (d (list (d (n "csv") (r "^0.14.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "itertools") (r "^0.5.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.7.2") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "19hm7ni6jshl3phjjmciigx3v063pz9xs9hrwj0p1ypzmbx2wszy") (y #t)))

(define-public crate-utah-0.1.1 (c (n "utah") (v "0.1.1") (d (list (d (n "csv") (r "^0.14.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "itertools") (r "^0.5.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.7.2") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "1d48prfbpn211gm7076vzmz06ghhnag4ss1mqlaa7qrxbb2bwnwx") (y #t)))

(define-public crate-utah-0.1.2 (c (n "utah") (v "0.1.2") (d (list (d (n "csv") (r "^0.14.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "itertools") (r "^0.5.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.7.2") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "1jsscgd5xnbg4556pc61pxzpxnq0sybapwzm3f10izfnlr7m32f2")))

