(define-module (crates-io ut ah utah2) #:use-module (crates-io))

(define-public crate-utah2-0.1.0 (c (n "utah2") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1zi31yarkmz0d8znkzm28y0s2nnga6fkicg0p4w319g9m488mzns")))

(define-public crate-utah2-0.1.1 (c (n "utah2") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "0g92zdjavmc51wh8gnvjzwddppx6nzb4v71xdsq0pw7qd9hsfn34")))

