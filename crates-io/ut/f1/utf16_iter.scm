(define-module (crates-io ut f1 utf16_iter) #:use-module (crates-io))

(define-public crate-utf16_iter-1.0.0 (c (n "utf16_iter") (v "1.0.0") (h "08gnxq0651i7p5dscmlalv07kqs6wgqmcpqlc70x9iirbc5nkzdq")))

(define-public crate-utf16_iter-1.0.1 (c (n "utf16_iter") (v "1.0.1") (h "0a8y1rwk2rrm9gj2px7r6nbshs3145rqrqnhs3lzsw02xw1vbnm0")))

(define-public crate-utf16_iter-1.0.2 (c (n "utf16_iter") (v "1.0.2") (h "00aqjxflzh2y3ckpqklv4zfa269riwv2wxpmdk2crqmh4mckdwvx")))

(define-public crate-utf16_iter-1.0.3 (c (n "utf16_iter") (v "1.0.3") (h "1da72yp5y9dbx4zz0y71h5vmswy0xdg1d9x5phx9j0v4wrcnf93a")))

(define-public crate-utf16_iter-1.0.4 (c (n "utf16_iter") (v "1.0.4") (h "14m6v10i9jw3s7dv4zlfbvah8dmgrr12xkzwfvbi0ycfnxzqppsj")))

(define-public crate-utf16_iter-1.0.5 (c (n "utf16_iter") (v "1.0.5") (h "0ik2krdr73hfgsdzw0218fn35fa09dg2hvbi1xp3bmdfrp9js8y8")))

