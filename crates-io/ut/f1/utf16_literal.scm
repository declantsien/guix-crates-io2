(define-module (crates-io ut f1 utf16_literal) #:use-module (crates-io))

(define-public crate-utf16_literal-0.1.0 (c (n "utf16_literal") (v "0.1.0") (h "0db883lvppyliyzkp4np7b312b5l4bmnva0d6zbl875v0vw99jjr")))

(define-public crate-utf16_literal-0.2.0 (c (n "utf16_literal") (v "0.2.0") (h "1qks2hbvflcyyacxydv6vhxd0vzqjabkgha4wc59vprv6r466ziy")))

(define-public crate-utf16_literal-0.2.1 (c (n "utf16_literal") (v "0.2.1") (h "0j1z5qirlgszh9qyybd4am8m26kg72knp05nw0f99svv9bz90vri")))

