(define-module (crates-io ut f1 utf16-ext) #:use-module (crates-io))

(define-public crate-utf16-ext-0.0.1 (c (n "utf16-ext") (v "0.0.1") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)))) (h "0svi007sfh943flg8hhg0ygl33r1vawq3xmkkx4dzlncxq386s1j")))

(define-public crate-utf16-ext-0.0.2 (c (n "utf16-ext") (v "0.0.2") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)))) (h "0ks81nl0n412x49nni4zs1irvvnrfh3ail22rr1nymsh5mldlmcp")))

(define-public crate-utf16-ext-0.0.3 (c (n "utf16-ext") (v "0.0.3") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)))) (h "1vkxhal2qspi2838dwdqyj5xk3c8h7qny0kxwws823pazawzmddv")))

(define-public crate-utf16-ext-0.1.0 (c (n "utf16-ext") (v "0.1.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)))) (h "0xnsg8f477r90d04ycpnih05zywnms08v3pw6bx5di30mrv9qzxr")))

