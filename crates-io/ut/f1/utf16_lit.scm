(define-module (crates-io ut f1 utf16_lit) #:use-module (crates-io))

(define-public crate-utf16_lit-0.0.1 (c (n "utf16_lit") (v "0.0.1") (h "0gxcvlab52h415q5wsahhhrb985rklyc6ivcf6kassc4y2ymafyw")))

(define-public crate-utf16_lit-0.0.2 (c (n "utf16_lit") (v "0.0.2") (h "0007qpyqi9qy1h7z1gb017d6xknmsx5mgp7zz3dv9zgb5lbjrir4")))

(define-public crate-utf16_lit-0.0.3 (c (n "utf16_lit") (v "0.0.3") (h "1w2hlv55n0c8ri0n35kbllf3gn5v9mdxm1xh04xxahmw4dnn11nx")))

(define-public crate-utf16_lit-1.0.0 (c (n "utf16_lit") (v "1.0.0") (h "0wgsgang5a7xczb94942fndddgv99n7d2phi376dh5h929gaghhp")))

(define-public crate-utf16_lit-1.0.1 (c (n "utf16_lit") (v "1.0.1") (h "01vsgvzzdqxs5f1w367sw1bbh6zwyiqkbl2cybd5a3wgzg932cj0")))

(define-public crate-utf16_lit-2.0.0 (c (n "utf16_lit") (v "2.0.0") (h "1d8h62sa9qhgnq02lqzbhgswhy0xchdmi6i2im9cyipkmr4nsmd8")))

(define-public crate-utf16_lit-2.0.1 (c (n "utf16_lit") (v "2.0.1") (h "10f475pipkf7jkdd7kk8isg005k8z820sicz20zpy2z4k8mx1778")))

(define-public crate-utf16_lit-2.0.2 (c (n "utf16_lit") (v "2.0.2") (h "1bm10sdzakmldc20sz7bb7m72sb1rmrvivfkq4wgzs0fh0m6sw0l")))

