(define-module (crates-io ut f1 utf16string) #:use-module (crates-io))

(define-public crate-utf16string-0.1.0 (c (n "utf16string") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "17py1wwyf34ws6d68ajraj2arimvf6798r60vn6bkdaqf458ybbk")))

(define-public crate-utf16string-0.2.0 (c (n "utf16string") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "05njx6b3bpq461gxx40as3i07lvkdd15za27pw9dgm8jbvla2qhb")))

