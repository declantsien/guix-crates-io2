(define-module (crates-io ut wt utwt) #:use-module (crates-io))

(define-public crate-utwt-0.4.0 (c (n "utwt") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "utmp-raw") (r "^0.3.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6") (d #t) (k 0)))) (h "1sia8w6x8vc8ibd397g4fspjqw6vqcy25vnb1wb1pwb394d1qdd4")))

