(define-module (crates-io ut oi utoipa-scalar) #:use-module (crates-io))

(define-public crate-utoipa-scalar-0.1.0 (c (n "utoipa-scalar") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (o #t) (k 0)) (d (n "axum") (r "^0.7") (o #t) (k 0)) (d (n "rocket") (r "^0.5") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^4") (d #t) (k 0)))) (h "1s7sww8ramqxlammvlqkgp5ra28v5whxzzhbddi96kfid5r4paxk")))

