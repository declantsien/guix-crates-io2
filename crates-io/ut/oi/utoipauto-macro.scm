(define-module (crates-io ut oi utoipauto-macro) #:use-module (crates-io))

(define-public crate-utoipauto-macro-0.0.1 (c (n "utoipauto-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.0.1") (d #t) (k 0)))) (h "0v98w2v81x6nq2bqkkyhqaw7jn8j6f8jjnhi7clrrbmpxqr8kjiy")))

(define-public crate-utoipauto-macro-0.0.2 (c (n "utoipauto-macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.0.2") (d #t) (k 0)))) (h "1hkk50ls1a2s4w8jh99jbwbxkvi6nfvj3iha2szs0p6flmb74aa3")))

(define-public crate-utoipauto-macro-0.0.3 (c (n "utoipauto-macro") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.0.3") (d #t) (k 0)))) (h "19r8xvw0lv8ya2kmv5yqxqx6a7igskxrqqgnil4hl478k5b0k6pn")))

(define-public crate-utoipauto-macro-0.0.4 (c (n "utoipauto-macro") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.0.4") (d #t) (k 0)))) (h "10vr6zyb10qw0qklqwl1jc9kbwi578va5960bzfsrzm80b6hdxs6")))

(define-public crate-utoipauto-macro-0.1.0 (c (n "utoipauto-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.1.0") (d #t) (k 0)))) (h "0psvaa34kka6nh13fy7dp9q7mn816iq1gx16wspqih6x0byk20gj")))

(define-public crate-utoipauto-macro-0.1.1 (c (n "utoipauto-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.1.1") (d #t) (k 0)))) (h "06fxx2i40pxc4mxqkx4xfq7a9kcqnp8dl8dyq4jncrlldjhx9rkk")))

(define-public crate-utoipauto-macro-0.1.2 (c (n "utoipauto-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.1.2") (d #t) (k 0)))) (h "0yfxlinp0dqy0rsr5w8q125sqhgp0gmjcmpakrpgslrijplsggvs")))

(define-public crate-utoipauto-macro-0.1.3 (c (n "utoipauto-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.1.3") (d #t) (k 0)))) (h "1w64aqg1kcvycjzhrqhcklrk4jbw0n6535ailw3vyz7xq81czj6b")))

(define-public crate-utoipauto-macro-0.1.4 (c (n "utoipauto-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.1.4") (d #t) (k 0)))) (h "1z1dk98jzy3gyfgrj1ag7ak2rjhxamrcwsgxs7a0fdd99y5dxppf")))

(define-public crate-utoipauto-macro-0.1.5 (c (n "utoipauto-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.1.5") (d #t) (k 0)))) (h "10mx4bkx6469rclx38hw5pkzq3g4296w7sc83sdbw8pr9y7ms8zz")))

(define-public crate-utoipauto-macro-0.1.6 (c (n "utoipauto-macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.1.6") (d #t) (k 0)))) (h "0qykdfqgx9aic5d0bajn0qr2977hdlgppchi6sabv0gmg428szqx")))

(define-public crate-utoipauto-macro-0.1.7 (c (n "utoipauto-macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.1.7") (d #t) (k 0)))) (h "09lhc7rin5m25jnwrmk56x5ykz5m21x7hjkr8abrzrlj68g1avvh")))

(define-public crate-utoipauto-macro-0.1.8 (c (n "utoipauto-macro") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.1.8") (d #t) (k 0)))) (h "1jphmn8zp326c05d9car4gxpdqg203lfwhvbly5s6vdiqi1wqg05")))

(define-public crate-utoipauto-macro-0.1.9 (c (n "utoipauto-macro") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.1.9") (d #t) (k 0)))) (h "08dw9awmh5jia7n3dza03db37bmz75w1zza6a5209r44x6vg6rpm")))

(define-public crate-utoipauto-macro-0.1.10 (c (n "utoipauto-macro") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipauto-core") (r "^0.1.10") (d #t) (k 0)))) (h "06vrskybj31hjiw30g8fja2jw62mvawhi3bi1rajh8g2x3lvchc4") (f (quote (("generic_full_path" "utoipauto-core/generic_full_path"))))))

