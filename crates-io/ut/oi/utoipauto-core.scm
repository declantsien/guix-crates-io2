(define-module (crates-io ut oi utoipauto-core) #:use-module (crates-io))

(define-public crate-utoipauto-core-0.0.1 (c (n "utoipauto-core") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "1qz54x1mf0j79skdxbn13xxzcyd7xqplgaqqhdi7sg62k65193a4")))

(define-public crate-utoipauto-core-0.0.2 (c (n "utoipauto-core") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "0f4jwc5yzqbn2jshxbyilba1gj4vk0wphcxrszdmclqx122a72w5")))

(define-public crate-utoipauto-core-0.0.3 (c (n "utoipauto-core") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "1y2vp6wi0v3s0c7m1v2czix8fqfphc03wqsxaaf656xp20ifwnpq")))

(define-public crate-utoipauto-core-0.0.4 (c (n "utoipauto-core") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "0d5nsnlm24zgah82iscvhr59rqfx0z5rp17fvqa9vy90j952jsrc")))

(define-public crate-utoipauto-core-0.1.0 (c (n "utoipauto-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "0kkfp59i21vxvqr0i96j2679cwq7sip3mm4i4xihnblq8cf5n033")))

(define-public crate-utoipauto-core-0.1.1 (c (n "utoipauto-core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "0v0nmbgwggknrhav5rb7lis9jz6dmy7qbpjsv949lf28i8h2b5n0")))

(define-public crate-utoipauto-core-0.1.2 (c (n "utoipauto-core") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "1y1d32m7kgqbgpc5rjshipdid9bx9vbvxhl1hjm70hv69rjcflly")))

(define-public crate-utoipauto-core-0.1.3 (c (n "utoipauto-core") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "125fglk22dkk0fbbgn2snpj4a48d66adn8wlrmw2sv7zzkiakd7p")))

(define-public crate-utoipauto-core-0.1.4 (c (n "utoipauto-core") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "1jrf9al9rc0cvy91s1yp9x3afcqxgy4wbjdyqmbh4m2fykb2n8q3")))

(define-public crate-utoipauto-core-0.1.5 (c (n "utoipauto-core") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "11q1j0nawmdw04zjal079cj4jqbxnf110g5r672w0gvbq1jwrpi7")))

(define-public crate-utoipauto-core-0.1.6 (c (n "utoipauto-core") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "07yjbc5pi87zj5y1qlsljk7djvwczxc54xrvascanjj2xyjjdbc7")))

(define-public crate-utoipauto-core-0.1.7 (c (n "utoipauto-core") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "1yxwpg4kvvkx0zsvvzz39vmazmfrh5igd5l7ajskwz0r9354fmi9")))

(define-public crate-utoipauto-core-0.1.8 (c (n "utoipauto-core") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "1ym7b1i7zpc28mg1db72pzl7ipnlvdxplk72gqpbq80ggf7lv3f1")))

(define-public crate-utoipauto-core-0.1.9 (c (n "utoipauto-core") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "1bz9hp6f8d6jc792ivs3hkvp2rrjk494881527h36fxj777zf8ad")))

(define-public crate-utoipauto-core-0.1.10 (c (n "utoipauto-core") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)) (d (n "utoipa") (r "^4.2.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)))) (h "003x1vknrm6nssfca0z45b88m6h48pwj1i1alsa8cysjhd3v327i") (f (quote (("generic_full_path"))))))

