(define-module (crates-io ut oi utoipa-rapidoc) #:use-module (crates-io))

(define-public crate-utoipa-rapidoc-0.1.0 (c (n "utoipa-rapidoc") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (o #t) (k 0)) (d (n "axum") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (d #t) (k 0)))) (h "0hr8lqfqv34dr3hzsg795m5yibfaymayganx3478drd8w2x9wnrf")))

(define-public crate-utoipa-rapidoc-1.0.0 (c (n "utoipa-rapidoc") (v "1.0.0") (d (list (d (n "actix-web") (r "^4") (o #t) (k 0)) (d (n "axum") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^4") (d #t) (k 0)))) (h "0qp1060nbn8prr4mqyxi14y3zj2yssizy472ny6dkkr38clrljnm")))

(define-public crate-utoipa-rapidoc-2.0.0 (c (n "utoipa-rapidoc") (v "2.0.0") (d (list (d (n "actix-web") (r "^4") (o #t) (k 0)) (d (n "axum") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^4") (d #t) (k 0)))) (h "0y4y0ragm3zpq74f96ilcax1bmb872vki08cy9jlhn6yyqwihcrx")))

(define-public crate-utoipa-rapidoc-3.0.0 (c (n "utoipa-rapidoc") (v "3.0.0") (d (list (d (n "actix-web") (r "^4") (o #t) (k 0)) (d (n "axum") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^4") (d #t) (k 0)))) (h "1422ws27dx2lmklba76d8j2c5fgwx1kmldsr2gj4yvj9jr9sfyaf")))

(define-public crate-utoipa-rapidoc-4.0.0 (c (n "utoipa-rapidoc") (v "4.0.0") (d (list (d (n "actix-web") (r "^4") (o #t) (k 0)) (d (n "axum") (r "^0.7") (f (quote ("json"))) (o #t) (k 0)) (d (n "rocket") (r "^0.5") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^4") (d #t) (k 0)))) (h "1n9rgamy3m9vm3368ya4qwgdpczbb21czvvqxdc5s4qwd65lhcha")))

