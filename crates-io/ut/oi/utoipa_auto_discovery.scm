(define-module (crates-io ut oi utoipa_auto_discovery) #:use-module (crates-io))

(define-public crate-utoipa_auto_discovery-0.1.0 (c (n "utoipa_auto_discovery") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1bp7msh3rk40k25hr4dp1ayqhi5f85zplpfca38vfgyk046pgx6w") (y #t) (r "1.69")))

(define-public crate-utoipa_auto_discovery-0.2.0 (c (n "utoipa_auto_discovery") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "19gz7rzbrapa90d4xavb69rv40yzagg0jagqxd2hn29567mjm4gv") (r "1.69")))

(define-public crate-utoipa_auto_discovery-0.3.0 (c (n "utoipa_auto_discovery") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "11i86gx6djpbk1iv0mm1931zh42nja0iyn47kifc2gw22i9c5fxb") (r "1.69")))

