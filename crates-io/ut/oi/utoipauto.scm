(define-module (crates-io ut oi utoipauto) #:use-module (crates-io))

(define-public crate-utoipauto-0.0.1 (c (n "utoipauto") (v "0.0.1") (d (list (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.0.4") (d #t) (k 0)))) (h "1k8aqcm9hgcvd8bh0zfm55iylb2s55xjliirk5jb2wlsj5vjwasc")))

(define-public crate-utoipauto-0.1.0 (c (n "utoipauto") (v "0.1.0") (d (list (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.1.0") (d #t) (k 0)))) (h "08bdvn3g3af95y28zl4n66m7m455yk1qrgykn39cd78xwgamz0a7")))

(define-public crate-utoipauto-0.1.1 (c (n "utoipauto") (v "0.1.1") (d (list (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.1.1") (d #t) (k 0)))) (h "020rz3l5i7bxvkzp00zqbn3b0h0sy5w3qsc0y76jk9vx0jg3wm0x")))

(define-public crate-utoipauto-0.1.2 (c (n "utoipauto") (v "0.1.2") (d (list (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.1.2") (d #t) (k 0)))) (h "1xvi51rpzhlh9n339lxjbrsqksflffj7vap3xwmc70hvww57y68f")))

(define-public crate-utoipauto-0.1.3 (c (n "utoipauto") (v "0.1.3") (d (list (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.1.3") (d #t) (k 0)))) (h "0qd0k5ibc3ccjg2xwq9jqynw2yfa9lx9jkc6rcbbbwrhqj5pib64")))

(define-public crate-utoipauto-0.1.4 (c (n "utoipauto") (v "0.1.4") (d (list (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.1.4") (d #t) (k 0)))) (h "092y7xcavaamvqvlx208jm795n1bg894wg5nrm9whpxadfn9hgqr")))

(define-public crate-utoipauto-0.1.5 (c (n "utoipauto") (v "0.1.5") (d (list (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.1.5") (d #t) (k 0)))) (h "0v27x2hdavwmlnwkak0zmjjz22gmgd09awn9fd4zg7yp8igwz6m6")))

(define-public crate-utoipauto-0.1.6 (c (n "utoipauto") (v "0.1.6") (d (list (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.1.6") (d #t) (k 0)))) (h "1qg15xnnvvzzs2j94s2ma5fkmdx0av21p2v3rmvvgj93cq98zy1k")))

(define-public crate-utoipauto-0.1.7 (c (n "utoipauto") (v "0.1.7") (d (list (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.1.7") (d #t) (k 0)))) (h "1vzj8ifk7mf339v2134ij2d5hba9f09y4w5jpc596nd4ms512cy4")))

(define-public crate-utoipauto-0.1.8 (c (n "utoipauto") (v "0.1.8") (d (list (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.1.8") (d #t) (k 0)))) (h "08gdf3hl3n29qkbwhdsx8idcpmfb73j5an6jds2j6gq9kw34msxa")))

(define-public crate-utoipauto-0.1.9 (c (n "utoipauto") (v "0.1.9") (d (list (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.1.9") (d #t) (k 0)))) (h "0pn0yp3y5c6g23br6ps4q5z6kr1m3r32jkkfk1s9sn05iqgnm09w")))

(define-public crate-utoipauto-0.1.10 (c (n "utoipauto") (v "0.1.10") (d (list (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.2.0") (f (quote ("preserve_path_order"))) (d #t) (k 2)) (d (n "utoipauto-macro") (r "^0.1.10") (d #t) (k 0)))) (h "0zlrr5mljr1s2333qahjw4600har07sg1jxyzdjcjlkcsh3052qc") (f (quote (("generic_full_path" "utoipauto-macro/generic_full_path"))))))

