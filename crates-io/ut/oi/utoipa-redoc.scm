(define-module (crates-io ut oi utoipa-redoc) #:use-module (crates-io))

(define-public crate-utoipa-redoc-0.1.0 (c (n "utoipa-redoc") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (f (quote ("macros"))) (o #t) (k 0)) (d (n "axum") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (d #t) (k 0)))) (h "1ggk7izki90ja394grbhdm6szpdi8aqqz92jn23hsn66mkjcq3id")))

(define-public crate-utoipa-redoc-1.0.0 (c (n "utoipa-redoc") (v "1.0.0") (d (list (d (n "actix-web") (r "^4") (o #t) (k 0)) (d (n "axum") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^4") (d #t) (k 0)))) (h "0j85bdz3w74r7sv18hcdb5n02qhafx2nik3xyyp6lcgwz1f954cc")))

(define-public crate-utoipa-redoc-2.0.0 (c (n "utoipa-redoc") (v "2.0.0") (d (list (d (n "actix-web") (r "^4") (o #t) (k 0)) (d (n "axum") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^4") (d #t) (k 0)))) (h "0bvqyrzg94jjcmbi6g6590jkp2grzxlkbsd8xmlw0m0ya7r0ylh1")))

(define-public crate-utoipa-redoc-3.0.0 (c (n "utoipa-redoc") (v "3.0.0") (d (list (d (n "actix-web") (r "^4") (o #t) (k 0)) (d (n "axum") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^4") (d #t) (k 0)))) (h "045fmzqz98r3gk34m54pgijfa775l82m4z5w8zbq260s34l3zaxm")))

(define-public crate-utoipa-redoc-4.0.0 (c (n "utoipa-redoc") (v "4.0.0") (d (list (d (n "actix-web") (r "^4") (o #t) (k 0)) (d (n "axum") (r "^0.7") (o #t) (k 0)) (d (n "rocket") (r "^0.5") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^4") (d #t) (k 0)))) (h "03zghwkddlvg4sjcpypmxzwjl1hvy3958na5gvcylsxs013a4i6a")))

