(define-module (crates-io ut c- utc-datetime) #:use-module (crates-io))

(define-public crate-utc-datetime-0.1.0 (c (n "utc-datetime") (v "0.1.0") (h "1jlmsz7xyicwa57b34ikhcg6qlzpsjzk9mr0h390m88ws5f5xbkm") (y #t)))

(define-public crate-utc-datetime-0.1.1 (c (n "utc-datetime") (v "0.1.1") (h "121lfw8vzm206y0sy6zdyhbhcr2c6kn5kiw79bvma9chqcjb6p8p") (y #t)))

(define-public crate-utc-datetime-0.1.2 (c (n "utc-datetime") (v "0.1.2") (h "1hx94dzi1slpd57wh84fw84lgmi1rkkk06wkrbxv0nzvk202gix9")))

(define-public crate-utc-datetime-0.1.3 (c (n "utc-datetime") (v "0.1.3") (h "0n8ljl5zgjvgql3jig6vcmh8jjnmxn6y7m0ijvbi2q4c9dpkvxx8")))

(define-public crate-utc-datetime-0.1.4 (c (n "utc-datetime") (v "0.1.4") (h "0qpq16hsjypgc0bhhngz32zdws8gs4rgc90gwg2a3sanrvqgl2zc")))

(define-public crate-utc-datetime-0.1.5 (c (n "utc-datetime") (v "0.1.5") (h "1hh05mfkdxssr2vycr1w5pqvvny0fwx5n4xxbwnhdhvrjid8z5lw")))

(define-public crate-utc-datetime-0.1.6 (c (n "utc-datetime") (v "0.1.6") (h "0phf6qg91i42dccv2chplbx6rwkv71x0zpln7mp4lbq810i60hag")))

(define-public crate-utc-datetime-0.1.7 (c (n "utc-datetime") (v "0.1.7") (h "0pi5nnlr3dhrjbl3xfmj16v99qp3i62lqx88gihzychzbgfv6qn3")))

(define-public crate-utc-datetime-0.1.8 (c (n "utc-datetime") (v "0.1.8") (h "031qxzd84xm9g2n5g8bb5b27h12msli90kd9v8dbd0ybgbpirazs") (y #t)))

(define-public crate-utc-datetime-0.1.9 (c (n "utc-datetime") (v "0.1.9") (h "1hcxli3q429bph4s3fljny0d1za2r6bagf0izi198af0l98v4bnd")))

(define-public crate-utc-datetime-0.1.10 (c (n "utc-datetime") (v "0.1.10") (h "0nm6486gwnwzplqa8p7dnm0arc46z78pnmyrj72z84j4wf47z1xf")))

(define-public crate-utc-datetime-0.1.11 (c (n "utc-datetime") (v "0.1.11") (h "1xh4j9zyjypjdddwx32mp4bs0k8crddbh02zsdr1jz3fp460n980")))

(define-public crate-utc-datetime-0.1.12 (c (n "utc-datetime") (v "0.1.12") (h "076zzmbhn64ddwxw93bysw0mgf5qai6jbsxz40n9lw0sf6yqj0fx")))

(define-public crate-utc-datetime-0.1.13 (c (n "utc-datetime") (v "0.1.13") (h "0lisn92rb4bi94q7r5k31h344hdqfql5sk4ch6yv7hii5gk1m3m2")))

(define-public crate-utc-datetime-0.1.14 (c (n "utc-datetime") (v "0.1.14") (h "00cs6md4wcpy3s0kkn8af3l42a5g7l4gfvy8671jwgjwhygsxm8d")))

(define-public crate-utc-datetime-0.1.15 (c (n "utc-datetime") (v "0.1.15") (h "0c23hmzfyjg7kxsb4rbiw2wmksrsrzqdynvq5xw631qvb8cam0xd")))

(define-public crate-utc-datetime-0.1.16 (c (n "utc-datetime") (v "0.1.16") (h "13m97sjlak4pq3xrmwhx840ljrvn8slrh57a090bxnfh77cmvlzm")))

(define-public crate-utc-datetime-0.1.17 (c (n "utc-datetime") (v "0.1.17") (h "0533bda9xp2i0mljs0jhlb3b1s0cvns647rcmv1zdz318rkkijrx")))

