(define-module (crates-io ut c- utc-dt) #:use-module (crates-io))

(define-public crate-utc-dt-0.1.1 (c (n "utc-dt") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("add" "mul" "from" "into"))) (d #t) (k 0)))) (h "16z26gykisb65j6i5zhw71pkgpb2jmf71im9sfrb0jp3vim151rl") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.65.0")))

(define-public crate-utc-dt-0.1.2 (c (n "utc-dt") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("add" "mul" "from" "into"))) (d #t) (k 0)))) (h "02xwcs1vg95r8bzjhdxq5sixnqcfkdhwfylgfvcxg9dy3nvc6bba") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.65.0")))

(define-public crate-utc-dt-0.1.3 (c (n "utc-dt") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("add" "mul" "from" "into"))) (d #t) (k 0)))) (h "10g4b9xd8axxvkds4syvvy6z2v8s7s5c1wmlqzvdzwn2998lv57c") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.65.0")))

(define-public crate-utc-dt-0.1.4 (c (n "utc-dt") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("add" "mul" "from" "into"))) (d #t) (k 0)))) (h "0072wzjgfvkyl0g431x0qkrvj6i2chaxdl0qyq3ipg9n79szqpw4") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.65.0")))

(define-public crate-utc-dt-0.2.0 (c (n "utc-dt") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (k 0)))) (h "1137kl47g5xxrqbpxn7d2fbfrdbpc63m87f8kc8adidisdya35bw") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.65.0")))

(define-public crate-utc-dt-0.2.1 (c (n "utc-dt") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (k 0)))) (h "029wy7ryharccic8wsjicjsxmnw3kwiycrhc3y8q3f95gf1h0a2i") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.65.0")))

