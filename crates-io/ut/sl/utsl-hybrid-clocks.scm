(define-module (crates-io ut sl utsl-hybrid-clocks) #:use-module (crates-io))

(define-public crate-utsl-hybrid-clocks-0.1.0 (c (n "utsl-hybrid-clocks") (v "0.1.0") (d (list (d (n "suppositions") (r "^0.1.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)) (d (n "time") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0kslwzsp3hn7n01fpjzyq82w4w7hx42snk5rkv8060aj2yqywka7") (f (quote (("pretty-print" "time"))))))

