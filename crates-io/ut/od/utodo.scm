(define-module (crates-io ut od utodo) #:use-module (crates-io))

(define-public crate-utodo-0.0.0 (c (n "utodo") (v "0.0.0") (h "0dxb8b8dl1926avai1z7lw9djxjs3ramq6ibfg0a1lrj1mhd32p2") (y #t)))

(define-public crate-utodo-1.0.0 (c (n "utodo") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09ppkfm91kcsaxnn8njn90z7p7q6gjqrwzcd8miwk5w5nk191x04")))

(define-public crate-utodo-2.0.0 (c (n "utodo") (v "2.0.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (o #t) (d #t) (k 0)))) (h "0j31r2jq1qdn06rr8gdarvfizlwn04a56fixy4vx4mk001hlqsdj") (f (quote (("full" "serde" "toml"))))))

(define-public crate-utodo-2.0.1 (c (n "utodo") (v "2.0.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (o #t) (d #t) (k 0)))) (h "0nvwczfk2i5m43brph0gv956hm65db0wh3260qk8hkz4q7hpwav5") (f (quote (("full" "serde" "toml"))))))

