(define-module (crates-io ut p2 utp2) #:use-module (crates-io))

(define-public crate-utp2-0.1.0 (c (n "utp2") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 2)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)))) (h "0ijixvm63baijwzgl8ydp9r9999bg1fbylib4yb5qm890x5006q9")))

