(define-module (crates-io ut im utimeseries) #:use-module (crates-io))

(define-public crate-utimeseries-0.1.0 (c (n "utimeseries") (v "0.1.0") (d (list (d (n "byte_conv") (r "^0.1.1") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.1") (d #t) (k 2)))) (h "02cd8cmyh99zx5981h0swhx7dgmk6waimsjczqg0wwrq3i2rj53r") (y #t)))

(define-public crate-utimeseries-0.1.1 (c (n "utimeseries") (v "0.1.1") (d (list (d (n "byte_conv") (r "^0.1.1") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.1") (d #t) (k 2)))) (h "0cz0llm6mj5cnbvp8mr8dsmkv9bxy1cv7g21ddwdcay0j8p3n43p")))

