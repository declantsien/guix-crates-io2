(define-module (crates-io ut im utime) #:use-module (crates-io))

(define-public crate-utime-0.1.0 (c (n "utime") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1hc5mr29avgwhaymb0474aspd1bh0wm2521q21xj4s32a0y8ssam")))

(define-public crate-utime-0.0.0 (c (n "utime") (v "0.0.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "00h2d5dj88wyzqqarcd6mm4gghvknbyn2s93iq7yknc6yxxf2ykp")))

(define-public crate-utime-0.0.1 (c (n "utime") (v "0.0.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1nlv3giqnx7p42n4346kqr990czvc3p1wlwnr6dnc92l81rdsmgi")))

(define-public crate-utime-0.1.1 (c (n "utime") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0hn0zln84jhcszsp700a9yx8cz5idfp3cd27fnaxrx33his3a4yp")))

(define-public crate-utime-0.1.2 (c (n "utime") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1jp0m0ka2wjlar47yskv54p6kbnckqpap49sbvs5d4fv8lpw50sg")))

(define-public crate-utime-0.1.3 (c (n "utime") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0i6g8kakdgzv8njga8zyg7863q1i1k5xracn2r4p2s6hh6c56yk3")))

(define-public crate-utime-0.2.0 (c (n "utime") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "16xxyz3f3qrvdia2fw30z2bs27yp0d1i5b3d646csfasgbghv72a")))

(define-public crate-utime-0.2.1 (c (n "utime") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0akr32rd2qiiwdbnkwd6hs4zfy76791xlqgwh9h5cp515iamhl05")))

(define-public crate-utime-0.2.2 (c (n "utime") (v "0.2.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)))) (h "12xpv6dqi53ng3q59q9mf8zrs278haipm668fdadcax3n6xrpczx") (y #t)))

(define-public crate-utime-0.3.0 (c (n "utime") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "151l62dbhxkfkvxmi5r5clii7zfsp5q1ia4g0m26c515v5w4bayg")))

(define-public crate-utime-0.3.1 (c (n "utime") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1vrw36v1s9yhsqi04vg9raasp70m2vzkbk68vb5jzldbbv3a1fli")))

