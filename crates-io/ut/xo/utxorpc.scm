(define-module (crates-io ut xo utxorpc) #:use-module (crates-io))

(define-public crate-utxorpc-1.0.0-alpha.0 (c (n "utxorpc") (v "1.0.0-alpha.0") (d (list (d (n "utxorpc-spec-cardano") (r "^1.0.0-alpha.0") (d #t) (k 0)) (d (n "utxorpc-spec-sync") (r "^1.0.0-alpha.0") (d #t) (k 0)))) (h "0nv0acr7azihjpv5kgg2kbjva575dlc33q7s15fwvr6mpkfnj8wk")))

(define-public crate-utxorpc-1.0.0-alpha.1 (c (n "utxorpc") (v "1.0.0-alpha.1") (d (list (d (n "utxorpc-spec-cardano") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "utxorpc-spec-sync") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "1h1nkn2v9xi7h2c882dmc9pizbn8m3hmdgkqx3a0b92wgx34265d")))

(define-public crate-utxorpc-0.1.0 (c (n "utxorpc") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.9") (f (quote ("tls-roots"))) (d #t) (k 0)) (d (n "utxorpc-spec") (r "^0.3.0") (d #t) (k 0)))) (h "13mz009z2cfzdcns55kmw69s465i61q086jkbi13znfjvxz8b61s")))

