(define-module (crates-io ut xo utxorpc-spec-ledger) #:use-module (crates-io))

(define-public crate-utxorpc-spec-ledger-1.0.0-alpha.0 (c (n "utxorpc-spec-ledger") (v "1.0.0-alpha.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.5") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1j8a0z2zbm00ki79jks7s9zsclv34r0ljqzn646f9vk2aq67ajs9") (f (quote (("default")))) (y #t)))

