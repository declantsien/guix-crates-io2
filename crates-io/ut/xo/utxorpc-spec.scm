(define-module (crates-io ut xo utxorpc-spec) #:use-module (crates-io))

(define-public crate-utxorpc-spec-0.1.0 (c (n "utxorpc-spec") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.5") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "160qwgbzsf0yax031mgpmcxsswy0f8dx9ssw50q1h38y6pc22mjl") (f (quote (("default"))))))

(define-public crate-utxorpc-spec-0.2.0 (c (n "utxorpc-spec") (v "0.2.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.5") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0f1k16qvk1wdih2lg50d13b4wb8pil5s4k907izlrv6kniyydqzz") (f (quote (("default"))))))

(define-public crate-utxorpc-spec-0.3.0 (c (n "utxorpc-spec") (v "0.3.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.5") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0ksiy1fc7al307ydwb7d3p4bc2gzj845rl864jbx0473pwj0xy48") (f (quote (("default"))))))

(define-public crate-utxorpc-spec-0.4.0 (c (n "utxorpc-spec") (v "0.4.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.6") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.6") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)))) (h "0cf2sjdyx0mfkwid2bd6nfncqqg4spcnb3s1w9hwaan7xzdbyzpx") (f (quote (("default"))))))

(define-public crate-utxorpc-spec-0.4.1 (c (n "utxorpc-spec") (v "0.4.1") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.6") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.6") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)))) (h "18kr0lishpm8vf860m8ckyff1m05w4fa3msaps630qy8fqkcy95v") (f (quote (("default"))))))

(define-public crate-utxorpc-spec-0.4.2 (c (n "utxorpc-spec") (v "0.4.2") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.6") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.6") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)))) (h "1ks05i77kpi43kxaii42xg9ijg82c0mb82vbhwylm4j4j2q8j1jk") (f (quote (("default"))))))

(define-public crate-utxorpc-spec-0.4.3 (c (n "utxorpc-spec") (v "0.4.3") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.6") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.6") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)))) (h "0asap3vngk1p7c5m9vf9fyyizzc09cs877lghb1fg7bin86vkm4j") (f (quote (("default"))))))

(define-public crate-utxorpc-spec-0.4.4 (c (n "utxorpc-spec") (v "0.4.4") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.6") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.6") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)))) (h "1gvjayyvkw6a5x2w56mp27avp3cligvzvk8dpw3g24asc87fvaqw") (f (quote (("default"))))))

(define-public crate-utxorpc-spec-0.5.0 (c (n "utxorpc-spec") (v "0.5.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "pbjson") (r "^0.6") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.6") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)))) (h "0bppl595vw3drvfb3cxiil8z8pkb6nn82bri8hid4zgdvk10m4m5") (f (quote (("default"))))))

(define-public crate-utxorpc-spec-0.5.1 (c (n "utxorpc-spec") (v "0.5.1") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "pbjson") (r "^0.6") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.6") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)))) (h "0avpd57rjb5d4l1hyqsx2rv82lcnv2kmvam9b02038ky0alivvg6") (f (quote (("default"))))))

