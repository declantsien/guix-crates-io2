(define-module (crates-io ut xo utxo) #:use-module (crates-io))

(define-public crate-utxo-0.1.0 (c (n "utxo") (v "0.1.0") (h "13pghx27ilihwfm47684cdafffyy570wls6wga5mg7bw903fwvw3") (y #t)))

(define-public crate-utxo-0.1.1 (c (n "utxo") (v "0.1.1") (h "1cl28h2pa4nqz4kmzp0988cyg9jb98al34r21xpx6if4h7xx8s0k") (y #t)))

(define-public crate-utxo-0.1.2 (c (n "utxo") (v "0.1.2") (h "0pcqscwsbbg18ly3zm2ls816ifw6fjm32g8sxmq7y4lrar8cndvv")))

(define-public crate-utxo-0.1.3 (c (n "utxo") (v "0.1.3") (h "0p8qlzvrkpljz5k3z60885wag5f8ji4rbkdm1isgf9xsgbcnsg2x")))

(define-public crate-utxo-0.1.4 (c (n "utxo") (v "0.1.4") (h "15xszd5sdh9b1k0icpfamvkfxfxl1qzqg0v5jy8sy7k4rjsxvk6s")))

(define-public crate-utxo-0.2.0 (c (n "utxo") (v "0.2.0") (h "151cs1igkl4i8a8byg4llzb13ynh2x3wllkyfjdpnpz5dp6y8w1b")))

(define-public crate-utxo-0.2.1 (c (n "utxo") (v "0.2.1") (h "0dr9cbwsqfc0q5fl03qkjkpcy3blki7jiiw3i19997z5lgkyggcw")))

(define-public crate-utxo-0.2.2 (c (n "utxo") (v "0.2.2") (h "04hsp8q9rnwvn8srymy2f7rg0z8lmli9rxspz49h0l7lyf14hby0")))

(define-public crate-utxo-0.3.0 (c (n "utxo") (v "0.3.0") (h "0pskhnf6hgl9bny5flbji3c5kw5fsl5kkycgz4qwwr4mswnl27gn")))

(define-public crate-utxo-0.3.1 (c (n "utxo") (v "0.3.1") (h "1fd4wsx8sqynk6psxswnr1909y87i8csd21frcdhc4hscdfmzw2a")))

