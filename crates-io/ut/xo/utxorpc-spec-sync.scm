(define-module (crates-io ut xo utxorpc-spec-sync) #:use-module (crates-io))

(define-public crate-utxorpc-spec-sync-1.0.0-alpha.0 (c (n "utxorpc-spec-sync") (v "1.0.0-alpha.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.5") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "utxorpc-spec-cardano") (r "^1.0.0-alpha.0") (d #t) (k 0)))) (h "157jydzk8aqyg0p9kxk6mkbk2mv8qd0d4nzpawzwdcl57fpz3wfh") (f (quote (("default"))))))

(define-public crate-utxorpc-spec-sync-1.0.0-alpha.1 (c (n "utxorpc-spec-sync") (v "1.0.0-alpha.1") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.5") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "utxorpc-spec-cardano") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "181ffgms56bzm1ch4ajzmbffzf9hljd9zj552qp8k5kzsdrspxrj") (f (quote (("default"))))))

