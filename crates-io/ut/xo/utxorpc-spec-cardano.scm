(define-module (crates-io ut xo utxorpc-spec-cardano) #:use-module (crates-io))

(define-public crate-utxorpc-spec-cardano-1.0.0-alpha.0 (c (n "utxorpc-spec-cardano") (v "1.0.0-alpha.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.5") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1qx804lnz6y81393l826c93v38fq8dlbr2vzgw6bnkas2zw65ihc") (f (quote (("default"))))))

(define-public crate-utxorpc-spec-cardano-1.0.0-alpha.1 (c (n "utxorpc-spec-cardano") (v "1.0.0-alpha.1") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "pbjson") (r "^0.5") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0538bcfc4ibf17f0v3s6kz4iiavgk8f8gzh4mraa22pnmq1qq6mv") (f (quote (("default"))))))

