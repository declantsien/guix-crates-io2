(define-module (crates-io ut f7 utf7-imap) #:use-module (crates-io))

(define-public crate-utf7-imap-0.1.0 (c (n "utf7-imap") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0mc1gm6c58bhw1vqyxl8k7iilppi32m91x3iw4iv20407m77pk9n")))

(define-public crate-utf7-imap-0.2.0 (c (n "utf7-imap") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0s7fh6mjwnfngy6jqnib37841bnxm9snpmlih0f9n6ppil5fz76p")))

(define-public crate-utf7-imap-0.3.0 (c (n "utf7-imap") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1spzf5m7rwmg0s41qmqi73xdkwnnycwdlpsmy2lzz6qzrjms79s5")))

(define-public crate-utf7-imap-0.3.1 (c (n "utf7-imap") (v "0.3.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1jlpyc2zp7wxz3c87p2flaljyp2flhc260n1fncxxl0njgbn65z8")))

(define-public crate-utf7-imap-0.3.2 (c (n "utf7-imap") (v "0.3.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0pczz3pazxj6ypnz1n5v4w24llqh501vdpq910gpdhhz4rjn6cjy")))

