(define-module (crates-io ut #{18}# ut181a) #:use-module (crates-io))

(define-public crate-ut181a-0.1.0 (c (n "ut181a") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "cp211x_uart") (r "^0.1.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "hidapi") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.0") (d #t) (k 0)))) (h "1fllkq5i46n307jr9lv9qp3zxm41j3g8abqdh0g8bd7jd7k53mac")))

(define-public crate-ut181a-0.2.0 (c (n "ut181a") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "cp211x_uart") (r "^0.2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "hid") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "0wlrcivc66x3hnbfmk3ssng6nndd6c1d850imxiq8d8mrbyz5nqc")))

(define-public crate-ut181a-0.2.1 (c (n "ut181a") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "cp211x_uart") (r "^0.2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "hid") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "1lyba2hkn6szg6wcjrhh2cl14ad8yyznc3sm8ixf1xvjfg9d90x8")))

