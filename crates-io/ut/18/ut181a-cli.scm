(define-module (crates-io ut #{18}# ut181a-cli) #:use-module (crates-io))

(define-public crate-ut181a-cli-0.1.0 (c (n "ut181a-cli") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "ut181a") (r "^0.1.0") (d #t) (k 0)))) (h "15w3c758rjagyi80x8avxv65qjpvc1kw6pyp8l5hhszxlnyf3bws")))

(define-public crate-ut181a-cli-0.2.0 (c (n "ut181a-cli") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "clap") (r "~2.29.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "hid") (r "^0.4.1") (d #t) (k 0)) (d (n "ut181a") (r "^0.2.0") (d #t) (k 0)))) (h "0g6wi5v7qxb969wrrx41lzwc9xzzidf93lr8rjw7hwhr65z7pijw") (f (quote (("static" "hid/static") ("default") ("build" "static" "hid/build"))))))

