(define-module (crates-io ut f6 utf64) #:use-module (crates-io))

(define-public crate-utf64-0.1.0 (c (n "utf64") (v "0.1.0") (h "0ahv42nmgw596x52fvjh9slia16ghz51yyl0myyng4qjpimsjls6")))

(define-public crate-utf64-1.0.0 (c (n "utf64") (v "1.0.0") (d (list (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1x8q6fa7fbk1rs6a1rcyzp6qccg1p3lzllzm2qaa1c2dffs050ih")))

(define-public crate-utf64-1.0.1 (c (n "utf64") (v "1.0.1") (d (list (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1nm4nn97r2g6rm4d8fjf75pvjdh1diib77mbqaq6rjpw68fvr5ix")))

