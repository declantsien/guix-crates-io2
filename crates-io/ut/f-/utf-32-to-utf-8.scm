(define-module (crates-io ut f- utf-32-to-utf-8) #:use-module (crates-io))

(define-public crate-utf-32-to-utf-8-1.0.0 (c (n "utf-32-to-utf-8") (v "1.0.0") (h "05wpw8xhqm72scpizwkw700vzc1inhc57p62b0dgf59vl2ag8020")))

(define-public crate-utf-32-to-utf-8-1.0.1 (c (n "utf-32-to-utf-8") (v "1.0.1") (h "12dkpklh9hwbqzlq66l1s47072r0mqlgbqfm9cdfxm5d97l6rjdh")))

(define-public crate-utf-32-to-utf-8-1.0.2 (c (n "utf-32-to-utf-8") (v "1.0.2") (h "09a673f9n2ymhvjdff0xr16dgnd76akymkqrf15pamf46jmvpkqv")))

(define-public crate-utf-32-to-utf-8-1.0.3 (c (n "utf-32-to-utf-8") (v "1.0.3") (h "1nh1hmkk7h493qgwj2r60hg49v1cacx7fyapy9rqq4msmlxxwifk")))

(define-public crate-utf-32-to-utf-8-1.0.4 (c (n "utf-32-to-utf-8") (v "1.0.4") (h "1yxdw4nvnygwxa34kdfnlz9vlq5zpz5pl03jcdasf5yhmfxkyd4x")))

