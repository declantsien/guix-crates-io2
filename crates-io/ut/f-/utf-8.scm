(define-module (crates-io ut f- utf-8) #:use-module (crates-io))

(define-public crate-utf-8-0.1.0 (c (n "utf-8") (v "0.1.0") (d (list (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "string-wrapper") (r "^0.1.0") (d #t) (k 0)))) (h "1j6ybsb89dqy2wahdvj3ibjy5njskg93p144dw9wrck3x3hqi5k9")))

(define-public crate-utf-8-0.2.0 (c (n "utf-8") (v "0.2.0") (d (list (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "string-wrapper") (r "^0.1.0") (d #t) (k 0)))) (h "1kikn027547fvmyqwnqhidngpc8qcb9jm70zw568jbsakwag9y0z")))

(define-public crate-utf-8-0.3.0 (c (n "utf-8") (v "0.3.0") (d (list (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "string-wrapper") (r "^0.1.0") (d #t) (k 0)))) (h "0k1np73cy6ll3m9wlsppn1sk9zpfpb834jgmihrfv5n4f0dp3vh6")))

(define-public crate-utf-8-0.4.0 (c (n "utf-8") (v "0.4.0") (d (list (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "string-wrapper") (r "^0.1.3") (d #t) (k 0)))) (h "0l0xy7scxncabnkwbim4a6vwwkkbc3vil8h9lnvfcccbyfw4xnw7")))

(define-public crate-utf-8-0.5.0 (c (n "utf-8") (v "0.5.0") (d (list (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "string-wrapper") (r "^0.1.3") (d #t) (k 0)))) (h "17kmql0w8vwaig1jl7c4fy0j34dg785qz6cs85y5bq1izhdvv9c0")))

(define-public crate-utf-8-0.6.0 (c (n "utf-8") (v "0.6.0") (d (list (d (n "matches") (r "^0.1.2") (d #t) (k 0)))) (h "005lknlmmmvqnbqfzvhiz1bz83sg17r2jlxw3rnvaf0452xfkbm9")))

(define-public crate-utf-8-0.7.0 (c (n "utf-8") (v "0.7.0") (d (list (d (n "matches") (r "^0.1.2") (d #t) (k 0)))) (h "02jpn2gm75f0byb02502ddqr6gd0x7mchqhghyxg4vvig4a4cn4x")))

(define-public crate-utf-8-0.7.1 (c (n "utf-8") (v "0.7.1") (d (list (d (n "matches") (r "^0.1.2") (d #t) (k 0)))) (h "0mjs304h73fz7mdbffika6fbm9yrnasxbxv63pplib6707327ydn")))

(define-public crate-utf-8-0.7.2 (c (n "utf-8") (v "0.7.2") (h "0p5pbm73a3qnm4203p3z0bsnqfilxq0bw9khq2vmq3f3nkx2s9pi")))

(define-public crate-utf-8-0.7.3 (c (n "utf-8") (v "0.7.3") (h "0y2ibazjpqp8mb5npcd9wr2izp618q4fm111cf97dqzhb1xm0a1z")))

(define-public crate-utf-8-0.7.4 (c (n "utf-8") (v "0.7.4") (h "0hail9ds825c0ai28w824phcvrw0syyg5q6waccvnc1nd5qmzcxs")))

(define-public crate-utf-8-0.7.5 (c (n "utf-8") (v "0.7.5") (h "1iw5rp4i3mfi9k51picbr5bgjqhjcmnxx7001clh5ydq31y2zr05")))

(define-public crate-utf-8-0.7.6 (c (n "utf-8") (v "0.7.6") (h "1a9ns3fvgird0snjkd3wbdhwd3zdpc2h5gpyybrfr6ra5pkqxk09")))

