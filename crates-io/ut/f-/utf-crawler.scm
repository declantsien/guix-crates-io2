(define-module (crates-io ut f- utf-crawler) #:use-module (crates-io))

(define-public crate-utf-crawler-0.1.1 (c (n "utf-crawler") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1dbklp5m15mwv87alr9lq0jfhvw61jg2nr7wfslh82ipm7a000kq")))

(define-public crate-utf-crawler-0.1.2 (c (n "utf-crawler") (v "0.1.2") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "14h9v57r0zk1xjmayks5pj275wjv8g2h0rgm1jwwywpc775292wg")))

(define-public crate-utf-crawler-0.1.3 (c (n "utf-crawler") (v "0.1.3") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "11dhyi6m5cvawb9mr5lakk7qaqys555wppy53h7r5dy0w0fjd7s9")))

