(define-module (crates-io ut f- utf-railroad) #:use-module (crates-io))

(define-public crate-utf-railroad-1.0.0 (c (n "utf-railroad") (v "1.0.0") (h "1r9y3nr5r9799d3y3wjaylca5fzqk9iv7rykh809vwcipjxm8kdi")))

(define-public crate-utf-railroad-1.1.0 (c (n "utf-railroad") (v "1.1.0") (h "07ksbaawgc0ja7zrxqcvqha29d9xqiiq5w5yqmdgh9lq3xjvkqxl")))

