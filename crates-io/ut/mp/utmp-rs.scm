(define-module (crates-io ut mp utmp-rs) #:use-module (crates-io))

(define-public crate-utmp-rs-0.1.0 (c (n "utmp-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)) (d (n "utmp-raw") (r "^0.1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2.8") (d #t) (k 0)))) (h "02l1z8r6z8i729rqvq1s9lnrm4dwdrlzwq9kvxgmkdc5hhpqc7ik")))

(define-public crate-utmp-rs-0.1.1 (c (n "utmp-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)) (d (n "utmp-raw") (r "^0.1.2") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2.8") (d #t) (k 0)))) (h "0zjc81sy05r7whdiqyfq8h2c1h420df70gsrb3zbwmnjb7445lyn")))

(define-public crate-utmp-rs-0.1.2 (c (n "utmp-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)) (d (n "utmp-raw") (r "^0.2.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6") (d #t) (k 0)))) (h "0pwzivdzxxi6rd5gdd4pf2rs9pbizfic9j687yw4327lxy33m9gf")))

(define-public crate-utmp-rs-0.2.0 (c (n "utmp-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "utmp-raw") (r "^0.2.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6") (d #t) (k 0)))) (h "02zqahirggpbl0vhcxqjmz6m4z7pmnfph620ffyq0zqfvx69dzsn")))

(define-public crate-utmp-rs-0.2.1 (c (n "utmp-rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "utmp-raw") (r "^0.2.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6") (d #t) (k 0)))) (h "0wd6fa90xv21kcqvpkawbp4fplyzxh6v5p86yzvjcjax194g29bp")))

(define-public crate-utmp-rs-0.3.0 (c (n "utmp-rs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "utmp-raw") (r "^0.3.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6") (d #t) (k 0)))) (h "084wik4dd5lfbzjg8ich17c8kx0qgr60ig8m0sjfh87ppbj9g74a")))

