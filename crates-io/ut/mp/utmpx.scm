(define-module (crates-io ut mp utmpx) #:use-module (crates-io))

(define-public crate-utmpx-0.1.0 (c (n "utmpx") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.147") (f (quote ("extra_traits"))) (d #t) (k 0)))) (h "0asqvis4vhz6vlmpfn3d20ksvdzbvrjm0w7vsd8yr049m17wxba1")))

(define-public crate-utmpx-0.2.0 (c (n "utmpx") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.147") (f (quote ("extra_traits"))) (d #t) (k 0)))) (h "0nw3qrlqdhihffqss6claa0rwqsph2xknv0f70zd8wc4vk1ddji4")))

