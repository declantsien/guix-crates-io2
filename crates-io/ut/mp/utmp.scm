(define-module (crates-io ut mp utmp) #:use-module (crates-io))

(define-public crate-utmp-0.0.1 (c (n "utmp") (v "0.0.1") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "05rngh44gw2hjwpcr98kv779kx2qc1m24v4dv83z7pf00b6ml7fb")))

(define-public crate-utmp-0.0.2 (c (n "utmp") (v "0.0.2") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "005hff8hrx0k7xhnbrvf0gc99cm0n1l3l5ijlszhd5658ibf5938")))

