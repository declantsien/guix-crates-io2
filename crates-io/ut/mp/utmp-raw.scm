(define-module (crates-io ut mp utmp-raw) #:use-module (crates-io))

(define-public crate-utmp-raw-0.1.0 (c (n "utmp-raw") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2.8") (d #t) (k 0)))) (h "1ixg74pswk7pz1n2rpqi1ni9scngb5d0xl8w53h7jaqyqmiz1ry5")))

(define-public crate-utmp-raw-0.1.1 (c (n "utmp-raw") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2.8") (d #t) (k 0)))) (h "00slvx0z340y6kgg0g8q8zmmfys098l8m01clmcw0cibmdcxl47m")))

(define-public crate-utmp-raw-0.1.2 (c (n "utmp-raw") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2.8") (d #t) (k 0)))) (h "19505606x42dakwdcw7q375dw8pjl0hi1p09fmqrrbmrcmn16s2h")))

(define-public crate-utmp-raw-0.2.0 (c (n "utmp-raw") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6") (d #t) (k 0)))) (h "05laynijd7bbdwl8d3bjg65jrgs0bga546wgxqyp2pna12armdfw")))

(define-public crate-utmp-raw-0.3.0 (c (n "utmp-raw") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6") (d #t) (k 0)))) (h "07zrwqjjr0q55mim1gxp26xv1ivadb0di1g5n2i8r06qp8c9n9fr")))

