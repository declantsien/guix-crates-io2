(define-module (crates-io ut mp utmp-classic-raw) #:use-module (crates-io))

(define-public crate-utmp-classic-raw-0.1.1 (c (n "utmp-classic-raw") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6") (d #t) (k 0)))) (h "0q9q0643y6br0515lpa2wlhc73sx4w8r1r6r8gbnbkj070mnsd7a")))

(define-public crate-utmp-classic-raw-0.1.2 (c (n "utmp-classic-raw") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6") (d #t) (k 0)))) (h "1dfh3v409rz0d4r63djrsryvgi8irdranzj9n9yx9n489snzq8k7")))

(define-public crate-utmp-classic-raw-0.1.3 (c (n "utmp-classic-raw") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.34") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cfq7m4scqs8zkkfsa92kg8y5gkd4nh6r4n183202vixg99jdhi2")))

