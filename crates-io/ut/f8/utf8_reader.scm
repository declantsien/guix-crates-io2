(define-module (crates-io ut f8 utf8_reader) #:use-module (crates-io))

(define-public crate-utf8_reader-0.1.0 (c (n "utf8_reader") (v "0.1.0") (h "180jbjs98xlwarzzwc43qy5zk9zq0lkiay0p2raszl19wsk5ap6j")))

(define-public crate-utf8_reader-0.2.0 (c (n "utf8_reader") (v "0.2.0") (h "1rlc0iw2116fax9g314g3sg7yjr2xr6gir36fjnix89mrsalw8cf")))

(define-public crate-utf8_reader-0.3.0 (c (n "utf8_reader") (v "0.3.0") (h "1yj2q2a49r5a8wa93vvq68nl4wmbz9x445ymvjcbcm7vxmlwb14q")))

(define-public crate-utf8_reader-0.4.0 (c (n "utf8_reader") (v "0.4.0") (h "09fnfz42r83xl2ghr67cis71fj9qnl7xpi39mbw1y72vlwmq2fnd")))

(define-public crate-utf8_reader-0.5.0 (c (n "utf8_reader") (v "0.5.0") (h "0k2ipkdgnznkqmkwxg4r8jflsk8f488lgvxjdxs5qpmbdkn19ydm")))

(define-public crate-utf8_reader-0.6.0 (c (n "utf8_reader") (v "0.6.0") (h "0lcgakphd9al5n6f5hqfpxk4g94ny8c8shcjxqghvnfggnpiafin")))

(define-public crate-utf8_reader-0.7.0 (c (n "utf8_reader") (v "0.7.0") (h "094i792985bdclqpi8fni3k7darqkr19ak5w51jbn0ky4p6hqs19")))

