(define-module (crates-io ut f8 utf8-width) #:use-module (crates-io))

(define-public crate-utf8-width-0.1.0 (c (n "utf8-width") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0is2bhwnshfyv07gniyn5pxpbks5qczsl5kc1qqf5d6fbi4rka0x")))

(define-public crate-utf8-width-0.1.1 (c (n "utf8-width") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1zmq1hp8n02h9xhg7zjl6pnk5jaxymg8fx9nbns5wvlfpcjf9rry")))

(define-public crate-utf8-width-0.1.2 (c (n "utf8-width") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0bri0rnaw49al897ardzbwxsy3354nhh6s5pa47xhhl4yrj6f6hc")))

(define-public crate-utf8-width-0.1.3 (c (n "utf8-width") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "192b27g65d0c975jckpzjxfj8ac5rhrabyy61p30fscdbvz58b3g")))

(define-public crate-utf8-width-0.1.4 (c (n "utf8-width") (v "0.1.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1ylf5mvzck81iszchxyqmhwimkcdqv7jhazvd454g911cchsqwch")))

(define-public crate-utf8-width-0.1.5 (c (n "utf8-width") (v "0.1.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0ax3y087ga8r2aap2h36jhmcapfvbly97mac3shxzy3y8mzxgxvw")))

(define-public crate-utf8-width-0.1.6 (c (n "utf8-width") (v "0.1.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "18fbr6bbkfprs0hb2pdz3ckfb6i1gm0j0x7ka3fhvbyd5m2ck42i")))

(define-public crate-utf8-width-0.1.7 (c (n "utf8-width") (v "0.1.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1qwj8c0fg8cpn8hq7c9xzz26kdz6ci32bf0madz57a2xi578vgc6") (r "1.56")))

