(define-module (crates-io ut f8 utf8-ranges) #:use-module (crates-io))

(define-public crate-utf8-ranges-0.1.0 (c (n "utf8-ranges") (v "0.1.0") (h "1qfsl396n06jzvnsxqirk96gv0jgl5xj30xdgr22rcz6j2spviap")))

(define-public crate-utf8-ranges-0.1.1 (c (n "utf8-ranges") (v "0.1.1") (h "0ibszmxc75n0w0lcqbylaclqhkv4pcg7ckhfy7lnc4jbj8mlnkc3")))

(define-public crate-utf8-ranges-0.1.2 (c (n "utf8-ranges") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1dlf2mn6x3z8n4mvy9cai3v91rwcnpqa9phccgv4af4kkniq1m0m")))

(define-public crate-utf8-ranges-0.1.3 (c (n "utf8-ranges") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "03xf604b2v51ag3jgzw92l97xnb10kw9zv948bhc7ja1ik017jm1")))

(define-public crate-utf8-ranges-1.0.0 (c (n "utf8-ranges") (v "1.0.0") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "08j129anwcbdwvcx1izb4nsf0nbxksin2zqxjbrfz2x94mjsnbv6")))

(define-public crate-utf8-ranges-1.0.1 (c (n "utf8-ranges") (v "1.0.1") (d (list (d (n "quickcheck") (r "^0.7") (k 2)))) (h "1r7gp4jmxkrv1266l0pbmibp3s6h3gpf1z72d14hj438vxkz8w7x")))

(define-public crate-utf8-ranges-1.0.2 (c (n "utf8-ranges") (v "1.0.2") (d (list (d (n "quickcheck") (r "^0.7") (k 2)))) (h "0drp4j55bz7gzk5sbrk6g1nd0p3xm2an9q77mpvhjxpqpr47wvvr")))

(define-public crate-utf8-ranges-1.0.3 (c (n "utf8-ranges") (v "1.0.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)))) (h "1ppzjsxmv1p1xfid8wwn07ciikk84k30frl28bwsny6za1vall4x")))

(define-public crate-utf8-ranges-1.0.4 (c (n "utf8-ranges") (v "1.0.4") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)))) (h "1fpc32znar5v02nwsw7icl41jzzzzhy0si6ngqjylzrbxxpi3bml")))

(define-public crate-utf8-ranges-1.0.5 (c (n "utf8-ranges") (v "1.0.5") (d (list (d (n "quickcheck") (r "^1") (k 2)))) (h "1fk46654sqis2dqamihlj9b1sv162kp3brgmmqpa0lqfz4kwikvz")))

