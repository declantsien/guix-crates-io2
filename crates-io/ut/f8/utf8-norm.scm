(define-module (crates-io ut f8 utf8-norm) #:use-module (crates-io))

(define-public crate-utf8-norm-1.0.0 (c (n "utf8-norm") (v "1.0.0") (d (list (d (n "lapp") (r "^0.4") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1zmdc904j3cvd137j3dz7r1s2lk8bi99sbgpmwlw4sq5l5kblcar")))

(define-public crate-utf8-norm-1.0.1 (c (n "utf8-norm") (v "1.0.1") (d (list (d (n "lapp") (r "^0.4") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0lvkw7q23xal4acp48493gz0zdfgx20nm01g8syp098nfgdpsghs")))

(define-public crate-utf8-norm-1.1.1 (c (n "utf8-norm") (v "1.1.1") (d (list (d (n "lapp") (r "^0.4") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1g51gz2sy4p8j06bcra8rcjrkv7c2rj3lg7cy5vpqynwjy0w72zx")))

