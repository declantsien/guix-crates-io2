(define-module (crates-io ut f8 utf8conv) #:use-module (crates-io))

(define-public crate-utf8conv-0.0.7 (c (n "utf8conv") (v "0.0.7") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (k 2)) (d (n "stackfmt") (r "^0.1.2") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0zqlm19c1w3xi3193c43m81nr1i9asanb68zmv4j11vcd4zrp8qg") (f (quote (("std" "alloc" "core") ("docsrs") ("default" "core") ("core") ("alloc" "core"))))))

(define-public crate-utf8conv-0.0.8 (c (n "utf8conv") (v "0.0.8") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (k 2)) (d (n "stackfmt") (r "^0.1.2") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1npjgshfwvsabvgha1chypvxkygbjmvagh2cjy2jhxcv7qwx843h") (f (quote (("std" "alloc" "core") ("docsrs") ("default" "core") ("core") ("alloc" "core"))))))

(define-public crate-utf8conv-0.0.9 (c (n "utf8conv") (v "0.0.9") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (k 2)) (d (n "stackfmt") (r "^0.1.2") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1mspnfg01za0yfv4058x537l1s38pr22w4gb3zic8kf0jnvqpzg3") (f (quote (("std" "alloc" "core") ("docsrs") ("default" "core") ("core") ("alloc" "core"))))))

(define-public crate-utf8conv-0.1.0 (c (n "utf8conv") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (k 2)) (d (n "stackfmt") (r "^0.1.2") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1nsf4vrb8glab8796lhjx9wdczyffc6n0jq5zi980viqx6r7hdrk") (f (quote (("std" "alloc" "core") ("docsrs") ("default" "core") ("core") ("alloc" "core"))))))

