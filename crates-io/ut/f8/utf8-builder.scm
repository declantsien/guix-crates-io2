(define-module (crates-io ut f8 utf8-builder) #:use-module (crates-io))

(define-public crate-utf8-builder-0.1.0 (c (n "utf8-builder") (v "0.1.0") (d (list (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "01b62pahqh8xrv6nm0va64r0k30zvy8sg5ksv2lcabcaadkvgbma") (f (quote (("std") ("default" "std"))))))

(define-public crate-utf8-builder-0.1.1 (c (n "utf8-builder") (v "0.1.1") (d (list (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0bp3yig9qksrfjdj2pqdmsf7jpv4kimsm64gya6i3wzpvkmw1lrs") (f (quote (("std") ("default" "std"))))))

(define-public crate-utf8-builder-0.1.2 (c (n "utf8-builder") (v "0.1.2") (d (list (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1wfb2s22lan2fhcknhx5d48mlym8fqagcr9zp6gacigx88bv545y") (f (quote (("std") ("default" "std"))))))

