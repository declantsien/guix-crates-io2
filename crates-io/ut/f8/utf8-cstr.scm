(define-module (crates-io ut f8 utf8-cstr) #:use-module (crates-io))

(define-public crate-utf8-cstr-0.1.0 (c (n "utf8-cstr") (v "0.1.0") (h "1zwhc5xy4dhjjvw995ysv02imshs47n9cvvmnns3hfm2kyymdgac")))

(define-public crate-utf8-cstr-0.1.1 (c (n "utf8-cstr") (v "0.1.1") (h "12sfgnx97cand56h0rq97b1s7jv28lg42bnkmbk97p60xgqbskw7")))

(define-public crate-utf8-cstr-0.1.2 (c (n "utf8-cstr") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)))) (h "1pzjsmqgp2y0wlixjqrdzivc9lick4isgndgzbcg0yg6c5h0np4z")))

(define-public crate-utf8-cstr-0.1.3 (c (n "utf8-cstr") (v "0.1.3") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)))) (h "1slbxcw0jrmnihxmiwd6h42c8aafm97kz6c0fhrka7h2rwjdvyfl")))

(define-public crate-utf8-cstr-0.1.4 (c (n "utf8-cstr") (v "0.1.4") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)))) (h "1b1ranziwx8ziv3l7krhk32shipkiak9m0vh941cmpffrj0srqdb")))

(define-public crate-utf8-cstr-0.1.5 (c (n "utf8-cstr") (v "0.1.5") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)))) (h "16pqcgirw6129q3mck396aps0c9qkbssa5zxis5011970nv1j6x5")))

(define-public crate-utf8-cstr-0.1.6 (c (n "utf8-cstr") (v "0.1.6") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)))) (h "0a5n94agdn7z54lzqqwk05fp9hsi1farac39sl82n5a1a51bpg2m")))

