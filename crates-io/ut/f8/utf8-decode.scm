(define-module (crates-io ut f8 utf8-decode) #:use-module (crates-io))

(define-public crate-utf8-decode-0.1.0 (c (n "utf8-decode") (v "0.1.0") (h "1hiwlb3cp47c00lj12pkwan0g6amkxg6najqbk6ff3bwqzla7vgc")))

(define-public crate-utf8-decode-1.0.0 (c (n "utf8-decode") (v "1.0.0") (h "1ymbkqanqaja7sngqcibgyfrckrm118k4kzmz3h26311s2a7ws7w")))

(define-public crate-utf8-decode-1.0.1 (c (n "utf8-decode") (v "1.0.1") (h "1674kmm1pfv6s9pk0m92zkqdid4rggl077x24s4a16ikz8kynqfa")))

