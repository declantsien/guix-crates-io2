(define-module (crates-io ut f8 utf8-command) #:use-module (crates-io))

(define-public crate-utf8-command-1.0.0 (c (n "utf8-command") (v "1.0.0") (h "0c29dcz3k7yy5im458dkzicg2i11ddlwhj9d1b2408f4iwy0a8p8")))

(define-public crate-utf8-command-1.0.1 (c (n "utf8-command") (v "1.0.1") (h "0ny5204sckcrycdwqamhcf9zz4ihn0h6v7mj8s8a9kcl9i953cg4")))

