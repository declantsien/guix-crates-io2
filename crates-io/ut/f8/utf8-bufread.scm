(define-module (crates-io ut f8 utf8-bufread) #:use-module (crates-io))

(define-public crate-utf8-bufread-0.1.1 (c (n "utf8-bufread") (v "0.1.1") (h "1fbwpzvf7kbhg8bn9gwzj1iy8wcxm1s23wblqb8qy3wy49dkrwxi") (y #t)))

(define-public crate-utf8-bufread-0.1.2 (c (n "utf8-bufread") (v "0.1.2") (h "0mihc6rpp0yygscczmq60fxdhaia5h7pwbg7w6qffk673yqzzldn") (y #t)))

(define-public crate-utf8-bufread-0.1.3 (c (n "utf8-bufread") (v "0.1.3") (h "1ivd18dhnk7a52n2nly6lbwy466qhmj6ap9kzpmkh173avpwwsyk") (y #t)))

(define-public crate-utf8-bufread-0.1.4 (c (n "utf8-bufread") (v "0.1.4") (h "1r69881c7byldmhg1dha66ri36pxlj54ihly0m12bjkdianqx5sx") (y #t)))

(define-public crate-utf8-bufread-0.1.5 (c (n "utf8-bufread") (v "0.1.5") (h "1rr7d9gsnhshqmksngszmrr1g7nidpkygsm283h3bphhzin1cj0a") (y #t)))

(define-public crate-utf8-bufread-1.0.0 (c (n "utf8-bufread") (v "1.0.0") (d (list (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "040cir45ki9n3xs138xqb5aiay6znsz6p405f9iviw6n0qxi7jbf") (y #t)))

