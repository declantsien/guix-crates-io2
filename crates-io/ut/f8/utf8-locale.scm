(define-module (crates-io ut f8 utf8-locale) #:use-module (crates-io))

(define-public crate-utf8-locale-0.2.0 (c (n "utf8-locale") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r ">=1, <3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0kc1dyhhihrkw21n70rnj66qchvndjz4b22yk35nf897mllygpfa")))

(define-public crate-utf8-locale-0.3.0 (c (n "utf8-locale") (v "0.3.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "quick-error") (r ">=1, <3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0myjbc9fj96vjv9hjn0p4m6krnid2wqh1mbczjnl1h40f6kli07s")))

(define-public crate-utf8-locale-1.0.0 (c (n "utf8-locale") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "16zbs9wbrv873sscs4fj4n4yz8w2dqdglwr81fbngbcmlmd5k1ay")))

(define-public crate-utf8-locale-1.0.1 (c (n "utf8-locale") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "05zjz8w5l5avw5zj421g296klppyd40qrp945q2fmdq2rxx1k29f") (r "1.58")))

(define-public crate-utf8-locale-1.0.3 (c (n "utf8-locale") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "02nrdp0yv205kld6f7gvwx7dynwxv5mhl776zlk7k2239sssdbic") (r "1.58")))

