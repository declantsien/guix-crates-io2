(define-module (crates-io ut f8 utf8parse) #:use-module (crates-io))

(define-public crate-utf8parse-0.1.0 (c (n "utf8parse") (v "0.1.0") (h "08q37jm94j5ypxmvhv6vdnazd1ay9wmhhycdqxaa98wl65zshpm1")))

(define-public crate-utf8parse-0.1.1 (c (n "utf8parse") (v "0.1.1") (h "0zamsj2986shm4x9zncjf2m5qy9scaw7qnxw4f89b2afpg6a8wl7")))

(define-public crate-utf8parse-0.2.0 (c (n "utf8parse") (v "0.2.0") (h "0wjkvy22cxg023vkmvq2wwkgqyqam0d4pjld3m13blfg594lnvlk") (f (quote (("nightly") ("default"))))))

(define-public crate-utf8parse-0.2.1 (c (n "utf8parse") (v "0.2.1") (h "02ip1a0az0qmc2786vxk2nqwsgcwf17d3a38fkf0q7hrmwh9c6vi") (f (quote (("nightly") ("default"))))))

