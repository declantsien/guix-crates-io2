(define-module (crates-io ut f8 utf8-parser) #:use-module (crates-io))

(define-public crate-utf8-parser-0.0.3 (c (n "utf8-parser") (v "0.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "01nsf6fwbcm9mbx3qigdax6j16mv991dia6cm9bxx3l0kinjbxn8") (f (quote (("std") ("default-feature" "std")))) (r "1.60")))

(define-public crate-utf8-parser-0.0.4 (c (n "utf8-parser") (v "0.0.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0bj8hhkf6l0ww5ki0rrzylnrky4kgwmpschg0gglahkshrg3rbi1") (f (quote (("std") ("error_in_core") ("default-feature" "std")))) (r "1.60")))

