(define-module (crates-io ut f8 utf8reader) #:use-module (crates-io))

(define-public crate-utf8reader-0.0.1 (c (n "utf8reader") (v "0.0.1") (h "02s31zf0dzf97d0ld04n6my5725rfh7yl825izj9bvqrc9wvi6qg")))

(define-public crate-utf8reader-0.0.2 (c (n "utf8reader") (v "0.0.2") (h "06kd41l5nrnwzp3y8m7m5zfjza13j53a4x3qn8cx730l86ybmywd")))

(define-public crate-utf8reader-0.0.3 (c (n "utf8reader") (v "0.0.3") (h "0winrmvd93pxih9kx1njkq233pia5lz8ydsxdzx1il2wb5mb3d0y")))

(define-public crate-utf8reader-0.0.4 (c (n "utf8reader") (v "0.0.4") (h "0zmrv4iiisa1j6vsvkwb9bpk1ixxxi8a36rn3rmb01nng3iwsnh8")))

(define-public crate-utf8reader-0.1.0 (c (n "utf8reader") (v "0.1.0") (h "036gcznivbm7xgqnxspd8xy17snfnvkx1k8zwdifjsmxfsrxgb9i")))

