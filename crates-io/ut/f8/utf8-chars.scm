(define-module (crates-io ut f8 utf8-chars) #:use-module (crates-io))

(define-public crate-utf8-chars-0.0.1 (c (n "utf8-chars") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)))) (h "1fn195nyc21a6p7gnnhpff1m4mgf9gl7x3y2wm4w2ibarzrfrnpw")))

(define-public crate-utf8-chars-0.0.2 (c (n "utf8-chars") (v "0.0.2") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)))) (h "0izqn8jqm5jlkcf3clyayfryn3qgx6wbmdjsrnj1gd1w6bifhkcl") (f (quote (("docs-rs"))))))

(define-public crate-utf8-chars-0.0.3 (c (n "utf8-chars") (v "0.0.3") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)))) (h "067zd04lxrbfj00r4yycjd7k8bk23zblk17llbnkpc3k22y520jw")))

(define-public crate-utf8-chars-0.0.4 (c (n "utf8-chars") (v "0.0.4") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)))) (h "19rk4spinpnyymxwndxb9jx9rbc4qfzf6wv3z62v2ig6gx946yd8") (f (quote (("docs-rs"))))))

(define-public crate-utf8-chars-0.0.5 (c (n "utf8-chars") (v "0.0.5") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)))) (h "1c8crjbx7wr3idma2hfpbmaw09gxxj31na3mrix5335rjgnb0p37")))

(define-public crate-utf8-chars-0.0.6 (c (n "utf8-chars") (v "0.0.6") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)))) (h "00yf6sii6h0a8v5zj7fqjq6wfjrx39z7fxhpzk9i6xg84ldsmqsw")))

(define-public crate-utf8-chars-0.0.7 (c (n "utf8-chars") (v "0.0.7") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)))) (h "0sq05fz843h8znrcnrgbvpnkf7kspp1ykw4y7hhzmrh0sgymqnfz")))

(define-public crate-utf8-chars-0.0.8 (c (n "utf8-chars") (v "0.0.8") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)))) (h "1qkcrbk4nkkkgzs071d992l0gmfii4a2g06hvp928pjb8sv6irvp")))

(define-public crate-utf8-chars-0.0.9 (c (n "utf8-chars") (v "0.0.9") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)))) (h "0zbxg9bcgvnxrnn1svk1dlazl45h5h8lkjf4fz4gb7k7pyn3shn6")))

(define-public crate-utf8-chars-0.0.10 (c (n "utf8-chars") (v "0.0.10") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "08961hbcfsvi4arfrcqq5sb6cjxxxk092p27vk64f1r5zc8y3n0n")))

(define-public crate-utf8-chars-0.1.0 (c (n "utf8-chars") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "177lkvz1mmf4f7r3gzyqkvi75v8xp0bar84fvsxzgzcrvl5bdyp4")))

(define-public crate-utf8-chars-0.1.1 (c (n "utf8-chars") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "1yigpws25n4z8f189bmq9r3rm1i9p92y7xsy8kmpq2164d8x881k")))

(define-public crate-utf8-chars-0.1.2 (c (n "utf8-chars") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "0wnckqzgmmzz9z10yxpfig4kn0cbh50xy5qhggkwid4bvskm7w04")))

(define-public crate-utf8-chars-0.1.3 (c (n "utf8-chars") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "1hh507syf0l44cx4bs416j0zw9x93nzw6gdwpm236xhrvk3h05fx")))

(define-public crate-utf8-chars-0.1.4 (c (n "utf8-chars") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.0") (d #t) (k 2)))) (h "11pjij0n20jknzk08wdg7h5ir18pwwwsavq75i5r0ya7jfdyr2lp")))

(define-public crate-utf8-chars-0.2.0 (c (n "utf8-chars") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0dk87jmpjzj8qz24yz6f3znrxxg1qzlj4s97brjn2zi888sfqnzj") (y #t)))

(define-public crate-utf8-chars-0.2.1 (c (n "utf8-chars") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0r5fksmsfn1zw5jbildh9yc5v3amxbb11wxp8vcipjcwxi4qnacc") (y #t)))

(define-public crate-utf8-chars-0.2.2 (c (n "utf8-chars") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0gavlia3jq8sy6vhxhr5m4zyrlp56vjrh3z2z98xbikq2937n4xd") (y #t)))

(define-public crate-utf8-chars-0.2.3 (c (n "utf8-chars") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "066d6zmbwcc88wpiz7gy3c7qr5h7mp0b21kfazj23x5rg07d2mdj")))

(define-public crate-utf8-chars-0.3.0 (c (n "utf8-chars") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0c09rw5mpwlsfx8g54vs4hwafrjmi9wjaxmwjp6z06zayafcz9x0")))

(define-public crate-utf8-chars-0.4.0 (c (n "utf8-chars") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1na771kgxlh9i2x1f7q53c7vwjawvwdfgndkk1i0cgh7v9svz478")))

(define-public crate-utf8-chars-0.5.0 (c (n "utf8-chars") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1i6acpan7cwrpvrq7s81r07nsfx8k5bdjf0xhrrk1y0c4yy3bq84")))

(define-public crate-utf8-chars-0.5.1 (c (n "utf8-chars") (v "0.5.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1f9dfivfi5kg1ii6rgy59lhalz6ri9al77c691cbfkmkpnbb7557")))

(define-public crate-utf8-chars-1.0.0 (c (n "utf8-chars") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1p59yq113py5w36fkhw19x1kw8lbqns1rw0igvw0hd7dpbc5qzh9")))

(define-public crate-utf8-chars-1.0.1 (c (n "utf8-chars") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0s4m3ag4k80k3izx6z52pfhqk2p9h48d4wlzynf5gq4qbfnhla3x")))

(define-public crate-utf8-chars-1.0.2 (c (n "utf8-chars") (v "1.0.2") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0hf8krxn54lm9jsppamzvmhf34xz6s71kh6bwydh37g7mj7qsd61")))

(define-public crate-utf8-chars-2.0.0 (c (n "utf8-chars") (v "2.0.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0awq3i04hn77lyvj82mqvxa5pgx6fn475in7missn22qnq8zz3b7") (r "1.59")))

(define-public crate-utf8-chars-2.0.1 (c (n "utf8-chars") (v "2.0.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "06638bmc6psy32vqfvfh434rls4bqpxhi7v45nm1sih6jdr3grzs") (r "1.59")))

(define-public crate-utf8-chars-2.0.2 (c (n "utf8-chars") (v "2.0.2") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1bvqq8qx2m1gwvkm8x252kbvgif5kmc50xs8zsh96gbp9c0s9w3f") (r "1.59")))

(define-public crate-utf8-chars-2.0.3 (c (n "utf8-chars") (v "2.0.3") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0fpw9mgrwp7c84ii3nnpy7ckd99i44ffq68v96cjmnxclfkc8ys3") (r "1.59")))

(define-public crate-utf8-chars-2.1.0 (c (n "utf8-chars") (v "2.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1azamg1y7q9q3vmsr00zkdvicr891kcl5bynh2mwzzxaqpqncpms") (f (quote (("default") ("bench")))) (r "1.59")))

(define-public crate-utf8-chars-3.0.0 (c (n "utf8-chars") (v "3.0.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0qmaqa2c8wi5hk2b4p666siv666252jsdjgdh0awmw3ac9sxhr02") (f (quote (("default") ("bench")))) (r "1.70")))

(define-public crate-utf8-chars-3.0.1 (c (n "utf8-chars") (v "3.0.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0xy2qa4xdk2qls6p7zl2iw3sj9zc8r85crq9pkc35b7r1575wih8") (f (quote (("default") ("bench")))) (r "1.70")))

(define-public crate-utf8-chars-3.0.2 (c (n "utf8-chars") (v "3.0.2") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kivq9pyxp84ap97yd2q0c48dv6krrhxmlp96farpcxgrazxka2j") (f (quote (("default") ("bench")))) (r "1.70")))

(define-public crate-utf8-chars-3.0.3 (c (n "utf8-chars") (v "3.0.3") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1v2wyj6s06hy0x0r5r3b55bciww0zzd6mghbf407rxrlij7632xv") (f (quote (("default") ("bench")))) (r "1.70")))

