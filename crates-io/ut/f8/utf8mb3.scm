(define-module (crates-io ut f8 utf8mb3) #:use-module (crates-io))

(define-public crate-utf8mb3-0.1.1 (c (n "utf8mb3") (v "0.1.1") (h "0y8hsgbvyc3ck2c5gvqlrqbzbza21hqkb5iq8lncaxd0d77qdh7s")))

(define-public crate-utf8mb3-0.1.2 (c (n "utf8mb3") (v "0.1.2") (h "1qdp6cyqwf877yigrm17bwabrw7nzzh314sfbywkaw8xzf40ff15")))

