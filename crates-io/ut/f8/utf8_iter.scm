(define-module (crates-io ut f8 utf8_iter) #:use-module (crates-io))

(define-public crate-utf8_iter-1.0.0 (c (n "utf8_iter") (v "1.0.0") (h "1nj1ghjyw9qq0ig9qggi0mnvfcm2n1w012mj8vfjzvnh6q300105")))

(define-public crate-utf8_iter-1.0.1 (c (n "utf8_iter") (v "1.0.1") (h "120ijgdin1w08n8v8wd604k21zc9wg8011f7i1xksniyjb2dprjr")))

(define-public crate-utf8_iter-1.0.2 (c (n "utf8_iter") (v "1.0.2") (h "0rnj1nwwc07lmchd1304dnjjlddyvbjl54cm7j6rgph0sgamqw7q")))

(define-public crate-utf8_iter-1.0.3 (c (n "utf8_iter") (v "1.0.3") (h "0cwzrh2w7n0i6cd3y842pxydcb0p629x3bjwhqyhwl5raljr5a34")))

(define-public crate-utf8_iter-1.0.4 (c (n "utf8_iter") (v "1.0.4") (h "1gmna9flnj8dbyd8ba17zigrp9c4c3zclngf5lnb5yvz1ri41hdn")))

