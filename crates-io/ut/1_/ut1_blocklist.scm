(define-module (crates-io ut #{1_}# ut1_blocklist) #:use-module (crates-io))

(define-public crate-ut1_blocklist-0.1.0 (c (n "ut1_blocklist") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1nayzkn5ybi26j1yxm5c2hd4qlyda3vb9fpadba1v5gafxbpn90f")))

(define-public crate-ut1_blocklist-0.1.1 (c (n "ut1_blocklist") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1ggcsb5zaijcgbk5vslq6khxkqaaxa2yx4l0j9r694bpbbdz8rw7") (y #t)))

(define-public crate-ut1_blocklist-0.2.0 (c (n "ut1_blocklist") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0r2hc7cq2d3747fbapd5y7l8n1g1hdy2j51hgrm2nglx7zb54j4v")))

(define-public crate-ut1_blocklist-0.3.0 (c (n "ut1_blocklist") (v "0.3.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1q2awjyg5ymm1lpm2famkzlm6g298jq7lc8cgq0davddcvki4fyf")))

(define-public crate-ut1_blocklist-0.3.1 (c (n "ut1_blocklist") (v "0.3.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1k63p23mfdb6f69ijbbqk8crwcl21fmqhcffcra1drjm1l247y6w")))

(define-public crate-ut1_blocklist-0.3.2 (c (n "ut1_blocklist") (v "0.3.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0z2hs29nl14xv176dwg7b9pcaha2f9y2d9lm4gq04vpf87hn7bbk")))

