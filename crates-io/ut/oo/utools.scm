(define-module (crates-io ut oo utools) #:use-module (crates-io))

(define-public crate-utools-0.1.0 (c (n "utools") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jf74w89zmqxiwjndsigdmizb4cywxz02ycsm8llsir0lzl7asdg")))

(define-public crate-utools-0.1.1 (c (n "utools") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g65y0fq9k4cz5kxcf062mafill70i3058pdzz89mgc97g6flab4")))

(define-public crate-utools-0.1.2 (c (n "utools") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0iidlcq5sj8bg08861jyg9959j51pnq06r7wrmvl9dry7rxw86wz")))

