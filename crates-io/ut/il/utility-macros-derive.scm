(define-module (crates-io ut il utility-macros-derive) #:use-module (crates-io))

(define-public crate-utility-macros-derive-0.0.1 (c (n "utility-macros-derive") (v "0.0.1") (d (list (d (n "_um") (r "^0.0.1") (d #t) (k 0) (p "utility-macros-internals")) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1pwlvidrah9020cw59wi44agndl469l17n495spng7w9cn4brcph")))

(define-public crate-utility-macros-derive-0.0.2 (c (n "utility-macros-derive") (v "0.0.2") (d (list (d (n "_um") (r "^0.0.1") (d #t) (k 0) (p "utility-macros-internals")) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1kxaar0mhv3l3dwl2wl9b6pskig8kxy819claqbrhalhv8f9k468")))

(define-public crate-utility-macros-derive-0.0.3 (c (n "utility-macros-derive") (v "0.0.3") (d (list (d (n "_um") (r "^0.0.2") (d #t) (k 0) (p "utility-macros-internals")) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1g37yy71kzs79d9f936ksabz76ibircdc3r7z9xsqld81p9z4r52")))

(define-public crate-utility-macros-derive-0.0.4 (c (n "utility-macros-derive") (v "0.0.4") (d (list (d (n "_um") (r "^0.0.4") (d #t) (k 0) (p "utility-macros-internals")) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0kpyg795fn245v25vx545nhr6v84nlwffc4y4pk2fpgpfhm5ww1b") (y #t)))

(define-public crate-utility-macros-derive-0.0.5 (c (n "utility-macros-derive") (v "0.0.5") (d (list (d (n "_um") (r "^0.0.5") (d #t) (k 0) (p "utility-macros-internals")) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1v0xpmk9qph76l2n5kw2bby96vi30fb4ldgrbysvq6zfk91pbyn2")))

