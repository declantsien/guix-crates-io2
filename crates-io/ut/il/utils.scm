(define-module (crates-io ut il utils) #:use-module (crates-io))

(define-public crate-utils-0.0.1 (c (n "utils") (v "0.0.1") (d (list (d (n "commandext") (r "*") (d #t) (k 0)))) (h "19l7fisq3c5xpkds06md03iqq28sammpjpd8isizxzc22drr5g4s")))

(define-public crate-utils-0.0.2 (c (n "utils") (v "0.0.2") (d (list (d (n "commandext") (r "*") (d #t) (k 0)))) (h "0f87ldhw902q8hckr15r55yi2asij02cin2v51z3hbwrwbriq9j0")))

(define-public crate-utils-0.0.3 (c (n "utils") (v "0.0.3") (d (list (d (n "commandext") (r "*") (d #t) (k 0)))) (h "06k5d2rypvg2xg0ia0pcag3kw4dm6h294kdzx76pksccbc244cvx")))

