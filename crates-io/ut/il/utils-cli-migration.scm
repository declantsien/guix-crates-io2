(define-module (crates-io ut il utils-cli-migration) #:use-module (crates-io))

(define-public crate-utils-cli-migration-1.0.0 (c (n "utils-cli-migration") (v "1.0.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.12.0") (f (quote ("runtime-tokio-native-tls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "0fa6cjf8l0avjqsx77p6rpazgzvkfr1al3d8i07zk6xxrbyci3hz")))

(define-public crate-utils-cli-migration-1.0.9 (c (n "utils-cli-migration") (v "1.0.9") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.12.0") (f (quote ("runtime-tokio-native-tls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "0pmcbbb7y0hl12ahz6228c12k06b73958d2mdv4kir095n3pacdl")))

