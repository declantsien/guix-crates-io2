(define-module (crates-io ut il utility-macros-internals) #:use-module (crates-io))

(define-public crate-utility-macros-internals-0.0.1 (c (n "utility-macros-internals") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19pjszy3wclfxghha5y8dvzzrbrfshqgcdwp641zcdhw67166ryj")))

(define-public crate-utility-macros-internals-0.0.2 (c (n "utility-macros-internals") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x4cvqi8zgjm6xh33pyv0s37mzc67z62fdfsx880v90wxz8896y7")))

(define-public crate-utility-macros-internals-0.0.4 (c (n "utility-macros-internals") (v "0.0.4") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18hgpddh77y6b7gfnr1l3lsr7rx55h144k98zgrz1x498qaphmbb") (y #t)))

(define-public crate-utility-macros-internals-0.0.5 (c (n "utility-macros-internals") (v "0.0.5") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yqdi1vb2g631bxb7p13sp2aidz74fcfy2ggsr1cjd942mmnmrw8")))

