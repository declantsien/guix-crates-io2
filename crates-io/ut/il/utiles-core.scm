(define-module (crates-io ut il utiles-core) #:use-module (crates-io))

(define-public crate-utiles-core-0.3.0 (c (n "utiles-core") (v "0.3.0") (d (list (d (n "fast_hilbert") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1m1lwkfkdxdnh87x0lf5bf2j2js0clq1n8n0kdl7ank3ylw7p9sf")))

(define-public crate-utiles-core-0.3.1 (c (n "utiles-core") (v "0.3.1") (d (list (d (n "fast_hilbert") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "185j2bwjf2pd7l7fvz0ly3a8y4yyy82l16pp5k2qgsqkyaa03ykl")))

(define-public crate-utiles-core-0.4.0 (c (n "utiles-core") (v "0.4.0") (d (list (d (n "fast_hilbert") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1cvm6hn5l1c0l00mhdnkxjbl4sy0a58qwwxf6g2qbddv9w41dkzd")))

(define-public crate-utiles-core-0.4.1 (c (n "utiles-core") (v "0.4.1") (d (list (d (n "fast_hilbert") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1f0qslak8s04kj9qz9sq77b3kxp246xk4ncw8qhzxa5ikwjcpiy5")))

