(define-module (crates-io ut il utility-types) #:use-module (crates-io))

(define-public crate-utility-types-0.0.1 (c (n "utility-types") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04zlh18mhqn0hx47xxg013ihx0pz0myrvhsg02ykdqpi4j0zy5ha")))

(define-public crate-utility-types-0.0.2 (c (n "utility-types") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1rxslbqvch37lpc7arccxxh1ymxcq1wp5nk2c626d176a3d5s2zj")))

