(define-module (crates-io ut il utility) #:use-module (crates-io))

(define-public crate-utility-0.1.0 (c (n "utility") (v "0.1.0") (h "1qxrfikfr1irc4fi34f1h489c2wx5vljs4mwkjvd57djaay06f8f") (y #t)))

(define-public crate-utility-0.0.0 (c (n "utility") (v "0.0.0") (h "10919801r1lvw734cjiqdj9wzraa0mhyj497pbsydr7d2s3vzic0")))

