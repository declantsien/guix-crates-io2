(define-module (crates-io ut il utils_did) #:use-module (crates-io))

(define-public crate-utils_did-0.1.0 (c (n "utils_did") (v "0.1.0") (d (list (d (n "bip39") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libsm") (r "^0.4.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0wfhipvnpyjfcprzifpnfgaq15xx7kz0agvnnay0n376gcbrjddb")))

