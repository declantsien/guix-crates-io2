(define-module (crates-io ut il utils-plugs-proc) #:use-module (crates-io))

(define-public crate-utils-plugs-proc-0.1.0 (c (n "utils-plugs-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "1fkc4nylv1665904q0db34jdqvj8sd980gb8ix7xv2d63fxyamv2")))

