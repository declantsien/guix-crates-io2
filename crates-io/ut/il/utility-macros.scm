(define-module (crates-io ut il utility-macros) #:use-module (crates-io))

(define-public crate-utility-macros-0.0.1 (c (n "utility-macros") (v "0.0.1") (d (list (d (n "_derive") (r "^0.0.1") (d #t) (k 0) (p "utility-macros-derive")) (d (n "_um") (r "^0.0.1") (d #t) (k 0) (p "utility-macros-internals")))) (h "18ydqj37lky3fpng9jk5qvja7gw3g2iw9dhryism02mn2gm68yiz")))

(define-public crate-utility-macros-0.0.2 (c (n "utility-macros") (v "0.0.2") (d (list (d (n "_derive") (r "^0.0.2") (d #t) (k 0) (p "utility-macros-derive")) (d (n "_um") (r "^0.0.1") (d #t) (k 0) (p "utility-macros-internals")))) (h "10x53lg50zqy10c8xbaj7a05brckcy148w8zppwdgq7phmcx90q3")))

(define-public crate-utility-macros-0.0.3 (c (n "utility-macros") (v "0.0.3") (d (list (d (n "_derive") (r "^0.0.3") (d #t) (k 0) (p "utility-macros-derive")) (d (n "_um") (r "^0.0.2") (d #t) (k 0) (p "utility-macros-internals")))) (h "1yx1cv54m1rd8g1diym7wwzh7zp5c7dw50d5z0lhvdm56y1rbxlz")))

(define-public crate-utility-macros-0.0.4 (c (n "utility-macros") (v "0.0.4") (d (list (d (n "_derive") (r "^0.0.4") (d #t) (k 0) (p "utility-macros-derive")) (d (n "_um") (r "^0.0.4") (d #t) (k 0) (p "utility-macros-internals")))) (h "1pfj6y1jrmyb539vjwdybk4bmhlq19g9wk2n046r17k32ckdhnfp") (y #t)))

(define-public crate-utility-macros-0.0.5 (c (n "utility-macros") (v "0.0.5") (d (list (d (n "_derive") (r "^0.0.5") (d #t) (k 0) (p "utility-macros-derive")) (d (n "_um") (r "^0.0.5") (d #t) (k 0) (p "utility-macros-internals")))) (h "0p18c2jgdsw3l3bzwlrpdasvmi5v5q4cnnz2nzm54b5x9ywl4rd8")))

