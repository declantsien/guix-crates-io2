(define-module (crates-io ut il utile_cli) #:use-module (crates-io))

(define-public crate-utile_cli-0.1.0 (c (n "utile_cli") (v "0.1.0") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "0kric9vxrasm7kscfb95fbilwxhfqxvyyb0jvx6rw6d4hv5lvqrs")))

(define-public crate-utile_cli-0.1.1 (c (n "utile_cli") (v "0.1.1") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "1wnhb81219d3zxwj32gbq5iffc9p4fw8hz1j3l1kpxnhz1a6nqw1")))

(define-public crate-utile_cli-0.1.2 (c (n "utile_cli") (v "0.1.2") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "1lc25761ww9p8i7vasxxp4jqgrd2ghz1m8gbvlwgdr142bwii0v0")))

(define-public crate-utile_cli-0.2.0 (c (n "utile_cli") (v "0.2.0") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "0n7i7w54xg2kzzj1bhv3g5l545nwdw18v0kqga6xs5n1x7fl91ns")))

(define-public crate-utile_cli-0.2.1 (c (n "utile_cli") (v "0.2.1") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "16ncfg8vf509ha8xjz5isznl4kix96dd09w5w7lhr69b09xcbrir")))

(define-public crate-utile_cli-0.3.0 (c (n "utile_cli") (v "0.3.0") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "1ai4mj8235grp67ha1vlh0xvxkhf5jxa6ndn40553bag7863s23g")))

(define-public crate-utile_cli-0.3.1 (c (n "utile_cli") (v "0.3.1") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "1f1zmmfz9gvh3rpvmhaxckw6yn2hid3dr735q5j902yz9msg8yl7")))

(define-public crate-utile_cli-0.3.2 (c (n "utile_cli") (v "0.3.2") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "0mvz0cgy2zqrfvznjshs2fspcf9yaw4i91s2jmzfam6affzng02v")))

