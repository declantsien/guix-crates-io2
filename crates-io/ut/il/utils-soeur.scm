(define-module (crates-io ut il utils-soeur) #:use-module (crates-io))

(define-public crate-utils-soeur-0.1.0 (c (n "utils-soeur") (v "0.1.0") (h "0pdxzjhnfs2cy476g3nqi44cv4vhpqn2cdfdazb0lziz40pkrw9s")))

(define-public crate-utils-soeur-0.1.1 (c (n "utils-soeur") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)))) (h "0riv1ry6bnd17ffwbrdsnxadl50jc18lm83wvc6f8n91dr4cqahh")))

