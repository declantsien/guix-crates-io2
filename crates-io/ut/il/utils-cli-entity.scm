(define-module (crates-io ut il utils-cli-entity) #:use-module (crates-io))

(define-public crate-utils-cli-entity-1.0.5 (c (n "utils-cli-entity") (v "1.0.5") (d (list (d (n "sea-orm") (r "^0.12") (d #t) (k 0)))) (h "06wxhz82pk6f34pp8vik0rs79vq5af7ky1b6bamx7ig1qf2f3bb2")))

(define-public crate-utils-cli-entity-1.0.9 (c (n "utils-cli-entity") (v "1.0.9") (d (list (d (n "sea-orm") (r "^0.12") (d #t) (k 0)))) (h "03wrflhzlafaij8vzs06gr9rz0k255c2gmmrkpqrc31l6m2lf65y")))

