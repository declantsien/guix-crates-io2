(define-module (crates-io ut il utility_games) #:use-module (crates-io))

(define-public crate-utility_games-0.1.0 (c (n "utility_games") (v "0.1.0") (d (list (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "03mv5vklx1z004d4vcsyqa79kf0hsa38xjpdh8fy48d6rw14skma") (f (quote (("pos" "mint"))))))

(define-public crate-utility_games-0.1.1 (c (n "utility_games") (v "0.1.1") (d (list (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0y8wxp2ps9bc41nazlldiqmharbm0w0l4wj4rsr5ypn8j344mqcd") (f (quote (("pos" "mint" "rand"))))))

(define-public crate-utility_games-0.2.0 (c (n "utility_games") (v "0.2.0") (d (list (d (n "ggez") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0pydfqi234g6caysd4qgpb2qq0c3szg15x0y3hivivmfy276wk2s") (f (quote (("pos" "ggez" "rand") ("interface" "ggez"))))))

(define-public crate-utility_games-0.2.1 (c (n "utility_games") (v "0.2.1") (d (list (d (n "ggez") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1kaswivi6cpibh411b2r7xrgv0vqhpkamk0jp70cj04mzp6issza") (f (quote (("pos" "ggez" "rand") ("interface" "ggez")))) (y #t)))

(define-public crate-utility_games-0.2.2 (c (n "utility_games") (v "0.2.2") (d (list (d (n "ggez") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1k6icm5mibrbzmpqinpzn94zq3m2ga1j1vicg8qcs1in6hwhvgky") (f (quote (("pos" "ggez" "rand") ("interface" "ggez")))) (y #t)))

(define-public crate-utility_games-0.2.3 (c (n "utility_games") (v "0.2.3") (d (list (d (n "ggez") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1xd65sv641alp96nm2z9a1kkxbfhhyk494q2i42r1v18lcxazjf2") (f (quote (("pos" "ggez" "rand") ("interface" "ggez"))))))

