(define-module (crates-io ut il utilz) #:use-module (crates-io))

(define-public crate-utilz-0.1.0 (c (n "utilz") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xjlgmrjx5sq61x71d8rxrq1ifp3sqrd0m9r0rf1j9wda5rgwnp8")))

(define-public crate-utilz-0.1.1 (c (n "utilz") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v1bskdhkplx1lagw998zflynfhjzya5k4hs6jlpl8zq2jbnyiyy")))

(define-public crate-utilz-0.2.0 (c (n "utilz") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1353s2mwlr4cyzzdw3ib12cvja91c1sq77zxs04733a1wx7vwg56")))

(define-public crate-utilz-0.3.0 (c (n "utilz") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hhxa6rmgs2njhlgr78l18c1czm60dlg0fbplrqqf596z0df64h4") (f (quote (("default" "binaries") ("binaries"))))))

