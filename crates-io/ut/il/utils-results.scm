(define-module (crates-io ut il utils-results) #:use-module (crates-io))

(define-public crate-utils-results-1.0.0 (c (n "utils-results") (v "1.0.0") (h "0wf8fgs0cz8pszcw7shk1pmbv2b7aw6bxnrhs70kksbncxy5fli7") (y #t)))

(define-public crate-utils-results-1.0.1 (c (n "utils-results") (v "1.0.1") (h "1d6f2dyj3s9wrzi4iqj1vf9my45zj7m8rykxxv0y79lnp8sskvpz") (y #t)))

(define-public crate-utils-results-1.1.0 (c (n "utils-results") (v "1.1.0") (h "13x7g1jxk8k9bgcvbha3300z1l69j0iws3v2gs028nndq0h15c6p") (y #t)))

(define-public crate-utils-results-1.1.1 (c (n "utils-results") (v "1.1.1") (h "198sp69ldp47fxx6gn7im370jrr5mml91mpfks5hm96g9phsrf2n") (y #t)))

(define-public crate-utils-results-1.1.2 (c (n "utils-results") (v "1.1.2") (h "0fnxq2pg0l6d7a6ph03dj87kzifvbqd0r2af66a87h24kbcggjkj")))

(define-public crate-utils-results-2.1.2 (c (n "utils-results") (v "2.1.2") (h "1na15s195iz36286gg9nm7rycn511m485kkw9mh1k0yycgks35az")))

(define-public crate-utils-results-2.2.0 (c (n "utils-results") (v "2.2.0") (h "0mr1y0g4gs4fq81sxkjh8wxgxli06al0mgm3ma21jx7hjn6zj8c8") (y #t)))

(define-public crate-utils-results-2.2.1 (c (n "utils-results") (v "2.2.1") (h "0dxa20nnvpjcxb3nf4q2gw66dgbq7px63q09m5vskwy3i37qv6pd") (y #t)))

(define-public crate-utils-results-2.2.2 (c (n "utils-results") (v "2.2.2") (h "0zc67l9v4kl86351s1pr3lpfwqkkwlnwq35854czyz3cd0svx9mq")))

(define-public crate-utils-results-3.0.0 (c (n "utils-results") (v "3.0.0") (h "145k0spa17qqk2cc6q21010qgbf94alllvycksnp0b4vh1yir4g3")))

(define-public crate-utils-results-3.1.0 (c (n "utils-results") (v "3.1.0") (h "1941mg6gljgkhl2300bffikfgk6gx24km6is65856dl3cbmgsqpw")))

(define-public crate-utils-results-4.0.0 (c (n "utils-results") (v "4.0.0") (h "1wqb91fsbaq3ldazkkvfy94jnyqbqz8d6v8wjkbrns6257msk8y7")))

(define-public crate-utils-results-4.1.0 (c (n "utils-results") (v "4.1.0") (h "1mqp98c0chk24qdcrczk4ww51422g3xqrfc2b8w33vpibklmmwb3")))

(define-public crate-utils-results-4.2.0 (c (n "utils-results") (v "4.2.0") (h "1vm3bjal0fpz1bamhp0a0iy3xcpfyd6f93cp8m9zrrxm9scv7k9p")))

(define-public crate-utils-results-4.2.1 (c (n "utils-results") (v "4.2.1") (h "057r5mpw4kldls4qrj9ff45hhvwv96dc57i027yac5h6wr86y1iw")))

(define-public crate-utils-results-4.3.0 (c (n "utils-results") (v "4.3.0") (h "0yf8y1h4h2sd74d3l9nypxbfh82w4vkwpqbf3n2l7ynqw3zkv043")))

(define-public crate-utils-results-4.3.1 (c (n "utils-results") (v "4.3.1") (h "1ar13a5fzwbgf8aylqy7w68cn37y8yhd1w8ipabhp48hr2vrilha")))

(define-public crate-utils-results-5.0.0 (c (n "utils-results") (v "5.0.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (k 0)))) (h "1128407zhh048vm72272893vk8v4bdcizs8z0dg8na9f3h892vww") (f (quote (("std" "anyhow/default") ("default" "anyhow"))))))

(define-public crate-utils-results-5.1.0 (c (n "utils-results") (v "5.1.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (k 0)))) (h "1s8sdps26fv71wkwbzrkgj4d849q8il3i8jp1r36bazqry8v2i9l") (f (quote (("std" "anyhow/default") ("default" "anyhow"))))))

(define-public crate-utils-results-5.1.1 (c (n "utils-results") (v "5.1.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (k 0)))) (h "1jbkw5qps5fdiz9w33b62nyqd3r9djv2pssa8676wxl4ld2di0bl") (f (quote (("std" "anyhow/default") ("default" "anyhow"))))))

(define-public crate-utils-results-5.2.0 (c (n "utils-results") (v "5.2.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)))) (h "0cxmh1vz2qjhai4q2w19agjjhhv7xx2f0lx5xv80ic6hrmjwx8cs") (f (quote (("std" "anyhow/default") ("default" "std"))))))

(define-public crate-utils-results-5.2.1 (c (n "utils-results") (v "5.2.1") (d (list (d (n "anyhow") (r "^1.0") (k 0)))) (h "1l9kc20mrj29sgbzxvkzqqkc6g37iadxza5h5v65rqzfz1bzcg93") (f (quote (("std" "anyhow/default") ("default" "std"))))))

(define-public crate-utils-results-5.3.0 (c (n "utils-results") (v "5.3.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)))) (h "08gihc9j9mqvz3gldxpnkm217djf4lpzarnyvg7z5lkcpl3ik1px") (f (quote (("std" "anyhow/default") ("default" "std"))))))

