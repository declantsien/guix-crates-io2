(define-module (crates-io ut il util-rs) #:use-module (crates-io))

(define-public crate-util-rs-0.1.0 (c (n "util-rs") (v "0.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1ymgs0w5wx2q6nbsg89s61p9gmflx4w5vynbvsjichsdwlmra86h")))

(define-public crate-util-rs-0.2.0 (c (n "util-rs") (v "0.2.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0q6asz03q2m0hkmdmfjzxs18dbv8syp4yjpix7k7f2gncbf1awvv")))

(define-public crate-util-rs-0.3.1 (c (n "util-rs") (v "0.3.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1jbq7zq4x9dcaj3iwkv5xm2xrp6l0j6h66sbl45s6x3xblz8yzas")))

(define-public crate-util-rs-0.3.2 (c (n "util-rs") (v "0.3.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1wsshgvsj7pw5fvd0v9bqq1l8d58dysyf8lsl6fkkhcs91kgfdnk")))

