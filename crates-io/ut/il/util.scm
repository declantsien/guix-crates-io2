(define-module (crates-io ut il util) #:use-module (crates-io))

(define-public crate-util-0.0.0 (c (n "util") (v "0.0.0") (h "1b37xixaayabqpvz4fgz4yh33ms2yld09gwwbgs6r0gblkp6m8bx")))

(define-public crate-util-0.1.0 (c (n "util") (v "0.1.0") (h "1caskw84km6wxsi7vi9wp82bhsp0far49vl896jlfh8mab9b27bv")))

(define-public crate-util-0.1.1 (c (n "util") (v "0.1.1") (h "0jwv0w188hm3bkg15g1lff75a0brjx4pq8i461y02a1hqzf0pnyg")))

(define-public crate-util-0.1.2 (c (n "util") (v "0.1.2") (h "004fxjl0hvyvwz2qjl78c1j7nyfmfy5y99sw4npvw4z8nmwfyfdn")))

(define-public crate-util-0.1.3 (c (n "util") (v "0.1.3") (h "02gd3gszzhidynkpvvr2qg583x2pdc24dsl2x0px4nmjxf4gg0qq")))

