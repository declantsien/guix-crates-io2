(define-module (crates-io ut re utreexo) #:use-module (crates-io))

(define-public crate-utreexo-0.0.0 (c (n "utreexo") (v "0.0.0") (d (list (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "blake2b_simd") (r "^0.5") (k 0)))) (h "1kybjg2h49svnvkww93spxcmlflhfrrssds0m93r467wzzsnl5k5") (f (quote (("std" "blake2b_simd/std" "bit-vec/std") ("default"))))))

