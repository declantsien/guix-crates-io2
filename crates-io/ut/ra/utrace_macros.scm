(define-module (crates-io ut ra utrace_macros) #:use-module (crates-io))

(define-public crate-utrace_macros-0.1.0 (c (n "utrace_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)) (d (n "utrace_core") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0)))) (h "1hxc1mnida5x79iwlhkz4vdsh44jmv7p22hh0jl8pjgwkjh0jmmn") (y #t)))

(define-public crate-utrace_macros-0.1.1 (c (n "utrace_macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)) (d (n "utrace_core") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "15p84a32cd322v8z8yl504js194ixz14xxi6king4mkzi37xkby5")))

