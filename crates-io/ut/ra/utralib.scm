(define-module (crates-io ut ra utralib) #:use-module (crates-io))

(define-public crate-utralib-0.1.0 (c (n "utralib") (v "0.1.0") (d (list (d (n "svd2utra") (r "^0.1.0") (d #t) (k 1)))) (h "0wqjkpm2wac7vq3207h099fpx3l6yp3d0chczrqa7c63cqisw659") (f (quote (("std") ("precursor-c809403-perflib") ("precursor-c809403") ("default" "precursor-c809403"))))))

(define-public crate-utralib-0.1.1 (c (n "utralib") (v "0.1.1") (d (list (d (n "svd2utra") (r "^0.1.0") (d #t) (k 1)))) (h "05fza78504igdz4m2rmxka8gnsjzfhxnzv0yhfz1z05hz6g78qm7") (f (quote (("std") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor") ("hosted") ("default" "precursor-c809403" "precursor"))))))

(define-public crate-utralib-0.1.2 (c (n "utralib") (v "0.1.2") (d (list (d (n "svd2utra") (r "^0.1.0") (d #t) (k 1)))) (h "18w0c0gk07j1qjm9s6vb38rg3rz4y36xys3h5rqdlgs43kdcl929") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor") ("hosted") ("default" "precursor-c809403" "precursor"))))))

(define-public crate-utralib-0.1.3 (c (n "utralib") (v "0.1.3") (d (list (d (n "svd2utra") (r "^0.1.1") (d #t) (k 1)))) (h "0yd5f2c356mz2pwflk1fzgxb5i6w9shrbn4ii2cdyqwcmzxp3ncr") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor") ("hosted") ("default" "precursor-c809403" "precursor"))))))

(define-public crate-utralib-0.1.4 (c (n "utralib") (v "0.1.4") (d (list (d (n "svd2utra") (r "^0.1.2") (d #t) (k 1)))) (h "1lsp56i1nikap32rj4xqff672266sycb8226si8pv88v3cpqfqzi") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor") ("hosted") ("default" "precursor-c809403" "precursor"))))))

(define-public crate-utralib-0.1.5 (c (n "utralib") (v "0.1.5") (d (list (d (n "svd2utra") (r "^0.1.3") (d #t) (k 1)))) (h "1bgrqbipra4l9zgckmr4qa9r8mzb9raakg9bcjcisvk05jjbvixn") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor") ("hosted") ("default" "precursor-c809403" "precursor"))))))

(define-public crate-utralib-0.1.6 (c (n "utralib") (v "0.1.6") (d (list (d (n "svd2utra") (r "^0.1.4") (d #t) (k 1)))) (h "0k71zwjp417i7hlx134zv0j60aw0frjyx8a7ags1r67j2dvy9l63") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor-a0912d6") ("precursor-2753c12-dvt") ("precursor") ("hosted") ("default" "precursor-c809403" "precursor"))))))

(define-public crate-utralib-0.1.7 (c (n "utralib") (v "0.1.7") (d (list (d (n "svd2utra") (r "^0.1.5") (d #t) (k 1)))) (h "0xvnihl996979q2bihwyiyv78323kxjc2389k5nygqfssdjhiq6d") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor-a0912d6") ("precursor-2753c12-dvt") ("precursor") ("hosted") ("default" "precursor-c809403" "precursor"))))))

(define-public crate-utralib-0.1.8 (c (n "utralib") (v "0.1.8") (d (list (d (n "svd2utra") (r "^0.1.6") (d #t) (k 1)))) (h "0d5aa8b27wa358py062z1v15iaxyrfrr84jjv0x9hhv08hnm583z") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor-a0912d6") ("precursor-2753c12-dvt") ("precursor") ("hosted") ("default" "precursor-c809403" "precursor"))))))

(define-public crate-utralib-0.1.9 (c (n "utralib") (v "0.1.9") (d (list (d (n "svd2utra") (r "^0.1.7") (d #t) (k 1)))) (h "1b5k5bv9dll7zbsrnyxqy3wcpxc2p73z61brzz862pr3dka82a9a") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor-a0912d6") ("precursor-2753c12-dvt") ("precursor") ("hosted") ("default" "precursor-c809403" "precursor"))))))

(define-public crate-utralib-0.1.10 (c (n "utralib") (v "0.1.10") (d (list (d (n "svd2utra") (r "^0.1.8") (d #t) (k 1)))) (h "129mw2230dpdskpx93nzddss1ilb25n39pf9cw63yrz4ixgcv425") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor-b414f2b") ("precursor-a0912d6") ("precursor-2753c12-dvt") ("precursor") ("hosted") ("default" "precursor-b414f2b" "precursor"))))))

(define-public crate-utralib-0.1.11 (c (n "utralib") (v "0.1.11") (d (list (d (n "svd2utra") (r "^0.1.9") (d #t) (k 1)))) (h "1dca4wirrmx9wh14hmhp93sharpsziqlla2q9zsw7amrkbq6x1nd") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor-a0912d6") ("precursor-7447604") ("precursor-2753c12-dvt") ("precursor") ("hosted") ("default" "precursor-7447604" "precursor"))))))

(define-public crate-utralib-0.1.12 (c (n "utralib") (v "0.1.12") (d (list (d (n "svd2utra") (r "^0.1.10") (d #t) (k 1)))) (h "0hy33zjab0wnqibjddzp0gkbqc9q4n6md4h80cnfk1fkq2h8f8w1") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor-a0912d6") ("precursor-7447604") ("precursor-2753c12-dvt") ("precursor") ("hosted") ("default" "precursor-7447604" "precursor"))))))

(define-public crate-utralib-0.1.13 (c (n "utralib") (v "0.1.13") (d (list (d (n "svd2utra") (r "^0.1.11") (d #t) (k 1)))) (h "0dj9v1z5r0m77pa45gpvvzwyh0ylq0zgf6hz55mngpf75yyi4lxc") (f (quote (("std") ("renode") ("precursor-c809403-perflib") ("precursor-c809403") ("precursor-a0912d6") ("precursor-70190e2") ("precursor-2753c12-dvt") ("precursor") ("hosted") ("default" "precursor-70190e2" "precursor"))))))

(define-public crate-utralib-0.1.14 (c (n "utralib") (v "0.1.14") (d (list (d (n "svd2utra") (r "^0.1.12") (d #t) (k 1)))) (h "1r2xpnk08i7rmr3az5j6qv1vm12nkcggsh3ihfmfc1a66axy5vrl") (f (quote (("std") ("renode") ("precursor-pvt") ("precursor-perflib") ("precursor-dvt") ("precursor") ("hosted") ("default" "precursor-pvt" "precursor") ("cramium-soc") ("cramium-fpga") ("atsama5d27"))))))

(define-public crate-utralib-0.1.15 (c (n "utralib") (v "0.1.15") (d (list (d (n "svd2utra") (r "^0.1.13") (d #t) (k 1)))) (h "0r7qwppdar9sj8gcnlynhlipwf05xwfxci566f0nn6zqw81j5hww") (f (quote (("std") ("renode") ("precursor-pvt") ("precursor-perflib") ("precursor-dvt") ("precursor") ("hosted") ("default" "precursor-pvt" "precursor") ("cramium-soc") ("cramium-fpga") ("atsama5d27"))))))

(define-public crate-utralib-0.1.16 (c (n "utralib") (v "0.1.16") (d (list (d (n "svd2utra") (r "^0.1.14") (d #t) (k 1)))) (h "1mli3q89cap3xphvn574k3csdzklph4kp5xzv8fs6j8vna43n8q5") (f (quote (("std") ("renode") ("precursor-pvt") ("precursor-perflib") ("precursor-dvt") ("precursor") ("hosted") ("default" "precursor-pvt" "precursor") ("cramium-soc") ("cramium-fpga") ("atsama5d27"))))))

(define-public crate-utralib-0.1.17 (c (n "utralib") (v "0.1.17") (d (list (d (n "svd2utra") (r "^0.1.15") (d #t) (k 1)))) (h "0w26jdvlzhkwxdz1gfbpsh5643cvmcjx7647wpqnb10j9l2gb52w") (f (quote (("std") ("renode") ("precursor-pvt") ("precursor-perflib") ("precursor-dvt") ("precursor") ("hosted") ("default" "precursor-pvt" "precursor") ("cramium-soc") ("cramium-fpga") ("atsama5d27"))))))

(define-public crate-utralib-0.1.18 (c (n "utralib") (v "0.1.18") (d (list (d (n "svd2utra") (r "^0.1.16") (d #t) (k 1)))) (h "0p357ammdr0i6r404waam83xfsms3nsw5d2synqvxhmly5rhxb00") (f (quote (("std") ("renode") ("precursor-pvt") ("precursor-perflib") ("precursor-dvt") ("precursor") ("hosted") ("default" "precursor-pvt" "precursor") ("cramium-soc") ("cramium-fpga") ("atsama5d27"))))))

(define-public crate-utralib-0.1.19 (c (n "utralib") (v "0.1.19") (d (list (d (n "svd2utra") (r "^0.1.17") (d #t) (k 1)))) (h "1ycj6d1qdw0m2i3k9gy48maa6m489nyqvdaawlnpw8qj4qmc0cvl") (f (quote (("std") ("renode") ("precursor-pvt") ("precursor-perflib") ("precursor-dvt") ("precursor") ("hosted") ("default" "precursor-pvt" "precursor") ("cramium-soc") ("cramium-fpga") ("atsama5d27"))))))

(define-public crate-utralib-0.1.20 (c (n "utralib") (v "0.1.20") (d (list (d (n "svd2utra") (r "^0.1.18") (d #t) (k 1)))) (h "10rnar0m3jh1qx24h2ad7f9b5shcf69kdyshk3w8ysv1v2ar1rlr") (f (quote (("std") ("renode") ("precursor-pvt") ("precursor-perflib") ("precursor-dvt") ("precursor") ("hosted") ("default" "precursor-pvt" "precursor") ("cramium-soc") ("cramium-fpga") ("atsama5d27"))))))

(define-public crate-utralib-0.1.21 (c (n "utralib") (v "0.1.21") (d (list (d (n "svd2utra") (r "^0.1.19") (d #t) (k 1)))) (h "1b8km9bn9j9qmjh400i6rvifmyknp0z36i72qi6lyx7bnnxbnczi") (f (quote (("std") ("renode") ("precursor-pvt") ("precursor-perflib") ("precursor-dvt") ("precursor") ("hosted") ("default" "precursor-pvt" "precursor") ("cramium-soc") ("cramium-fpga") ("atsama5d27"))))))

(define-public crate-utralib-0.1.22 (c (n "utralib") (v "0.1.22") (d (list (d (n "svd2utra") (r "^0.1.20") (d #t) (k 1)))) (h "07iirxj1n6nkpsm8niqnzbsrlyx5j3941vl0y1kn9ch4q8nlm5c4") (f (quote (("std") ("renode") ("precursor-pvt") ("precursor-perflib") ("precursor-dvt") ("precursor") ("hosted") ("default" "precursor-pvt" "precursor") ("cramium-soc") ("cramium-fpga") ("atsama5d27"))))))

(define-public crate-utralib-0.1.23 (c (n "utralib") (v "0.1.23") (d (list (d (n "svd2utra") (r "^0.1.21") (d #t) (k 1)))) (h "027jm7l65bwjdzra5sqz1g1iz0rfb39x3xsd5ryygzfmaq7yk0ra") (f (quote (("std") ("renode") ("precursor-pvt") ("precursor-perflib") ("precursor-dvt") ("precursor") ("hosted") ("default" "precursor-pvt" "precursor") ("cramium-soc") ("cramium-fpga") ("atsama5d27"))))))

(define-public crate-utralib-0.1.24 (c (n "utralib") (v "0.1.24") (d (list (d (n "svd2utra") (r "^0.1.22") (d #t) (k 1)))) (h "0vqv1mniy4r5f1fh8k884m1sb078y38yklvhajwd4ni863k743sz") (f (quote (("std") ("renode") ("precursor-pvt") ("precursor-perflib") ("precursor-dvt") ("precursor") ("hosted") ("default" "hosted") ("cramium-soc") ("cramium-fpga") ("atsama5d27"))))))

