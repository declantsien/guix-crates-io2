(define-module (crates-io ut ra utrace) #:use-module (crates-io))

(define-public crate-utrace-0.1.1 (c (n "utrace") (v "0.1.1") (d (list (d (n "critical-section") (r "^1.1.2") (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (d #t) (k 0)) (d (n "utrace_core") (r "^0.1.1") (d #t) (k 0)) (d (n "utrace_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1iqs5bzhkas04q9jp7sl86zc0jridyzi81g2c4m8l2lzmc5w5vc3")))

