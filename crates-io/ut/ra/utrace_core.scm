(define-module (crates-io ut ra utrace_core) #:use-module (crates-io))

(define-public crate-utrace_core-0.1.0 (c (n "utrace_core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (o #t) (d #t) (k 0)))) (h "0hni5pc08z2i091nsgh528xswyjmy2iryanmgzxcabnv9r5y1d3y") (y #t) (s 2) (e (quote (("std" "dep:anyhow" "dep:serde" "dep:serde_json"))))))

(define-public crate-utrace_core-0.1.1 (c (n "utrace_core") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (o #t) (d #t) (k 0)))) (h "0qa4j6zs94q7hvwz5pkyh7grxj5x2679v799ka5h4c3wgskzbdfg") (s 2) (e (quote (("std" "dep:anyhow" "dep:serde" "dep:serde_json"))))))

