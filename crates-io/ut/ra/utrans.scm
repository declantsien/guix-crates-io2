(define-module (crates-io ut ra utrans) #:use-module (crates-io))

(define-public crate-utrans-0.1.0 (c (n "utrans") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("color" "derive" "help" "suggestions" "usage" "wrap_help" "error-context"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)) (d (n "realpath-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1jrss1s8dikljv150yd7g7rgjzzi3mh6fqnqlxgf6g5zhrjsni78") (f (quote (("err-notify") ("copy") ("chae"))))))

