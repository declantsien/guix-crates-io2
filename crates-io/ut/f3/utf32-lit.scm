(define-module (crates-io ut f3 utf32-lit) #:use-module (crates-io))

(define-public crate-utf32-lit-1.0.0 (c (n "utf32-lit") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "00h932p5wwwdkc52vk6jazzr0h23hfap7yspr04hd6733i3964v7")))

(define-public crate-utf32-lit-1.1.0 (c (n "utf32-lit") (v "1.1.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0y1s26kkcbdwsxsj3cip8vn57j4qlcqisnzw20lajxr07nxivy8j")))

(define-public crate-utf32-lit-1.1.1 (c (n "utf32-lit") (v "1.1.1") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1zw9ik4crj2nyjgmljw6vz7v6p39lpnc71662qmwwkyrb71bidid")))

(define-public crate-utf32-lit-1.2.0 (c (n "utf32-lit") (v "1.2.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0vykydsrgjiha9yy1cmplgqw08sxbzwb9jnx8bljgwmdw3s2gdyw")))

