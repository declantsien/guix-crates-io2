(define-module (crates-io hr to hrtor) #:use-module (crates-io))

(define-public crate-hrtor-0.1.0 (c (n "hrtor") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rlua") (r "^0.19.7") (d #t) (k 0)))) (h "0skhm3mq18ycf69lc79fdhxzvfj46cisqrdqb02pvhr69c08s2qm")))

