(define-module (crates-io hr ti hrtime) #:use-module (crates-io))

(define-public crate-hrtime-0.1.0 (c (n "hrtime") (v "0.1.0") (h "1pw49d3i6fcicbgzfg7q9amb3fvrrs78r0g3iyczfpg7nl4gijld")))

(define-public crate-hrtime-0.2.0 (c (n "hrtime") (v "0.2.0") (h "0v434kzy78d65fjm8ca29j7fzcfic66lmc2y4ribi9kmww78g01a")))

