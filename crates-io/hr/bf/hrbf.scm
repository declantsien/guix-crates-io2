(define-module (crates-io hr bf hrbf) #:use-module (crates-io))

(define-public crate-hrbf-0.2.1 (c (n "hrbf") (v "0.2.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "autodiff") (r "^0.1") (d #t) (k 2)) (d (n "na") (r "^0.18") (d #t) (k 0) (p "nalgebra")) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "12ylq9sxrnf4nq4as122fzs6c44sbif2rzdw3q70k6h7g13br8ds")))

(define-public crate-hrbf-0.3.0 (c (n "hrbf") (v "0.3.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "autodiff") (r "^0.1") (d #t) (k 2)) (d (n "na") (r "^0.20") (d #t) (k 0) (p "nalgebra")) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1xdmlqgik98rhwfaagyhxdb37w1ddcn8s85hyc31js95vvv5p1m3")))

(define-public crate-hrbf-0.4.0 (c (n "hrbf") (v "0.4.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "autodiff") (r "^0.3") (d #t) (k 2)) (d (n "na") (r "^0.24") (d #t) (k 0) (p "nalgebra")) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "181vybyv206labdjif2gcjn1nqarrk3rg0yyr3g4agqq77ky0k33")))

(define-public crate-hrbf-0.5.0 (c (n "hrbf") (v "0.5.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "autodiff") (r "^0.3") (d #t) (k 2)) (d (n "na") (r "^0.25") (d #t) (k 0) (p "nalgebra")) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1rm2l4sdmmkf2iyfi2944psbnanxfdklh62p98ja1r2hqgfyhkv9")))

(define-public crate-hrbf-0.6.0 (c (n "hrbf") (v "0.6.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "autodiff") (r "^0.3") (d #t) (k 2)) (d (n "na") (r "^0.29") (d #t) (k 0) (p "nalgebra")) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1nfz9jlhf85jw0kkb3z5k2c2d0hkc6igb0bkgzdyg6wzp4cdvca4")))

(define-public crate-hrbf-0.7.0 (c (n "hrbf") (v "0.7.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "autodiff") (r "^0.4") (d #t) (k 2)) (d (n "na") (r "^0.30") (d #t) (k 0) (p "nalgebra")) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1zgdpssap5gv64lzjwq2n6xxg9l1w2ykdzv3f4p0dmwsflhjxd1m")))

(define-public crate-hrbf-0.8.0 (c (n "hrbf") (v "0.8.0") (d (list (d (n "na") (r "^0.32") (d #t) (k 0) (p "nalgebra")) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "autodiff") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "08pgi8j0n90pnb8v92vkjanpindly2wh1v9dijqvywy58wmqnix8")))

