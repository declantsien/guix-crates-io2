(define-module (crates-io hr st hrstopwatch) #:use-module (crates-io))

(define-public crate-hrstopwatch-0.0.1 (c (n "hrstopwatch") (v "0.0.1") (d (list (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_Performance"))) (d #t) (k 0)))) (h "1zjhwk55ibpqwqvp01lmf84rhb5hpgb197py082c9ayc3vk82qy0")))

(define-public crate-hrstopwatch-0.0.2 (c (n "hrstopwatch") (v "0.0.2") (d (list (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_Performance"))) (d #t) (k 0)))) (h "15s17fh0aybr25d64j6sdyr94gc6if3wswvlqqj43frk16icdxvj")))

(define-public crate-hrstopwatch-0.0.3 (c (n "hrstopwatch") (v "0.0.3") (d (list (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_Performance"))) (d #t) (k 0)))) (h "1xqmhrzi1dvwpaxksalcg9h675n4nyf1cbw63qh3n8yj2khxd0mv")))

(define-public crate-hrstopwatch-0.1.0 (c (n "hrstopwatch") (v "0.1.0") (d (list (d (n "snafu") (r "^0.7.1") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_Performance"))) (d #t) (k 0)))) (h "13awxi801c289z4b02rhlmgq04dws25db6clmzi5y52mza03spzq")))

