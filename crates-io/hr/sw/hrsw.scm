(define-module (crates-io hr sw hrsw) #:use-module (crates-io))

(define-public crate-hrsw-0.1.0 (c (n "hrsw") (v "0.1.0") (d (list (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1fgdqx6k682vbwabvdlqbxwl2s05p0ldn7qbbm9zi8mvsm0191nn")))

(define-public crate-hrsw-0.1.1 (c (n "hrsw") (v "0.1.1") (h "1gfzcczp8087v17ar3af5v7h7xqmmj2p25i9s1pglr0h965f2g8n") (y #t)))

(define-public crate-hrsw-0.1.2 (c (n "hrsw") (v "0.1.2") (h "1zpp9zb97jirlm5s6b82x3iw9iz6imm6y68lvxjh41vji8ypbr0m")))

