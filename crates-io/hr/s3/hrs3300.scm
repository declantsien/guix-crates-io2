(define-module (crates-io hr s3 hrs3300) #:use-module (crates-io))

(define-public crate-hrs3300-0.1.0 (c (n "hrs3300") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0qbzc68vq5wl22rvbfxx57vvr4inwbc57j8l8ab10309j60kvw51")))

