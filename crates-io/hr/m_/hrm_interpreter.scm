(define-module (crates-io hr m_ hrm_interpreter) #:use-module (crates-io))

(define-public crate-hrm_interpreter-0.2.0 (c (n "hrm_interpreter") (v "0.2.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "1iy64ajwvg5kysblm6mhbwz93q5gn3snh83khdmbj4j57qlv0qxz")))

