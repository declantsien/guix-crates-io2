(define-module (crates-io hr an hrana-client-proto) #:use-module (crates-io))

(define-public crate-hrana-client-proto-0.1.0 (c (n "hrana-client-proto") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "09vp30rhz2jci5b7n1i90mwas5lxxrmnlmlx513cc5v3fydzjw1z")))

(define-public crate-hrana-client-proto-0.1.1 (c (n "hrana-client-proto") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "026v7m80z1vmkmiz705kw1vhccfaylccmg9iy606p6h3hmip8apv")))

(define-public crate-hrana-client-proto-0.1.2 (c (n "hrana-client-proto") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "00k1dmaml6a5hmd45g4fkq9n3y26wxxasz5rik5z5xq7lr85vw96")))

(define-public crate-hrana-client-proto-0.2.1 (c (n "hrana-client-proto") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0cz5ypcyffkv2kccd21pgfmqzrwpd8j4bwk41vb3znl9w90lwszi")))

