(define-module (crates-io hr an hrandr) #:use-module (crates-io))

(define-public crate-hrandr-0.1.1 (c (n "hrandr") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "14a84qkd5v8svhnli5j3mwi19hrmgjv7nr8m9dn6w7z2i6kslznc")))

(define-public crate-hrandr-0.1.2 (c (n "hrandr") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xckcg3483n2svm5qyxc2f3jm54vr6lvhrphip3mb27738l4b6wm")))

(define-public crate-hrandr-0.1.3 (c (n "hrandr") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vgkzwllf075p285yfb2xksb55hqn9yd89qilminavdzgr1gspnn")))

(define-public crate-hrandr-0.1.4 (c (n "hrandr") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "19xyyqvn01dky3j1p3dv08wcl5p7d14mmhf4pzkbjsxwgrqs1d9y")))

(define-public crate-hrandr-0.1.5 (c (n "hrandr") (v "0.1.5") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "01ab09a3ip76kpmk24b5jv98praz87q3rwl7ymnflqljw350gfbz")))

