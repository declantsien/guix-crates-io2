(define-module (crates-io hr x- hrx-get) #:use-module (crates-io))

(define-public crate-hrx-get-0.1.0 (c (n "hrx-get") (v "0.1.0") (h "0qi09fg6f63cg7162vy5qjq5q5qakbd2n576h57an1ak4wxvcsmb")))

(define-public crate-hrx-get-0.1.1 (c (n "hrx-get") (v "0.1.1") (h "0zs6fhhp394rjphdrbifvrcp1lvca4kycyhwa9f75m9sjq4h92vl")))

(define-public crate-hrx-get-0.1.2 (c (n "hrx-get") (v "0.1.2") (h "1kr65hmfbkk00bnm5x43r0fq2f4rr12lj51la3za03wkrd6jc9w1")))

(define-public crate-hrx-get-0.1.4 (c (n "hrx-get") (v "0.1.4") (h "1xfh9da2qc53mipq3fs61jna5pabk12m1h3nhnwj2p5485v5cqcc")))

(define-public crate-hrx-get-0.2.0 (c (n "hrx-get") (v "0.2.0") (h "0dpc88bfvdhg36w6654bkvfqkczkqx54rxdr6943j82423f9pjbb")))

