(define-module (crates-io hr pc hrpc-proc-macro) #:use-module (crates-io))

(define-public crate-hrpc-proc-macro-0.1.0 (c (n "hrpc-proc-macro") (v "0.1.0") (h "14963jhw24bjjrpl2mb3541p5gf3f4mflz0g1k91mfmg9310q178")))

(define-public crate-hrpc-proc-macro-0.26.0 (c (n "hrpc-proc-macro") (v "0.26.0") (h "1h396i1362qs4wxx7i8bbh0b6anv4y35n895lflrj7bpcgd2l4q1")))

(define-public crate-hrpc-proc-macro-0.27.0 (c (n "hrpc-proc-macro") (v "0.27.0") (h "10cvssjw32b0q8kqb4128wp4za7zy4y5zla0a8vb6m7vfx4xmr7r")))

(define-public crate-hrpc-proc-macro-0.28.0 (c (n "hrpc-proc-macro") (v "0.28.0") (h "1mc839raz2rl1bqvb922zwivfl16d1y3w76ypx79cad93lx2rss3")))

(define-public crate-hrpc-proc-macro-0.29.0 (c (n "hrpc-proc-macro") (v "0.29.0") (h "0gkq2f4fqkr2x30yp0gfkz26fpipycmpjkx7pmv2wp6drjhv85x1")))

(define-public crate-hrpc-proc-macro-0.30.0 (c (n "hrpc-proc-macro") (v "0.30.0") (h "179bjmkj2m0k7817zc37ybcyqj9whyf3m2g81gh2p72f4r2za7yw")))

(define-public crate-hrpc-proc-macro-0.31.0 (c (n "hrpc-proc-macro") (v "0.31.0") (h "0gwgiwa34q5703mgps77d4fvra2byczwki84zaaj5447my4w1z97")))

(define-public crate-hrpc-proc-macro-0.32.0 (c (n "hrpc-proc-macro") (v "0.32.0") (h "0l5g8x1ig8bjx9s6s3q10ipnfx7qad0y83qbc75fajnz9zgkjz0k")))

(define-public crate-hrpc-proc-macro-0.33.0 (c (n "hrpc-proc-macro") (v "0.33.0") (h "0qg47s26f3zvfdqgmzrwiv4mivwdra9rji0q08nqjin5i9893d64")))

