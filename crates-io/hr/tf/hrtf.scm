(define-module (crates-io hr tf hrtf) #:use-module (crates-io))

(define-public crate-hrtf-0.1.0 (c (n "hrtf") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.10.0") (d #t) (k 0)) (d (n "rubato") (r "^0.5.0") (d #t) (k 0)) (d (n "rustfft") (r "^4.0.0") (d #t) (k 0)))) (h "1lf9xpv02wm8fpcfly4qam1sgxh41vjykizkdncjrbfjrm5i8h66") (f (quote (("enable_profiler" "rg3d-core/enable_profiler"))))))

(define-public crate-hrtf-0.2.0 (c (n "hrtf") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rg3d-core") (r "^0.10.0") (d #t) (k 0)) (d (n "rubato") (r "^0.5.0") (d #t) (k 0)) (d (n "rustfft") (r "^4.0.0") (d #t) (k 0)))) (h "16ll35ww5wba3x5hs4d652023bif0ip3n0s6hfbv7myclmbn2za4") (f (quote (("enable_profiler" "rg3d-core/enable_profiler"))))))

(define-public crate-hrtf-0.3.0 (c (n "hrtf") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rubato") (r "^0.5.0") (d #t) (k 0)) (d (n "rustfft") (r "^4.0.0") (d #t) (k 0)))) (h "1xzyfrfjy7ayljgmznqllv7599vwv0a2amh2hvxfj372igxc2fvm")))

(define-public crate-hrtf-0.4.0 (c (n "hrtf") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rubato") (r "^0.5.0") (d #t) (k 0)) (d (n "rustfft") (r "^4.0.0") (d #t) (k 0)))) (h "0gldmjcl95crrwv1fwqq6k09indfdm5rv44pip6mmg5d9m9cx8k3")))

(define-public crate-hrtf-0.5.0 (c (n "hrtf") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rubato") (r "^0.5.0") (d #t) (k 0)) (d (n "rustfft") (r "^5.0.1") (d #t) (k 0)))) (h "0wn0h0q8xmnpnhskg1pzh0fgiyfvg3pp251q716y4f6csj3g7m18")))

(define-public crate-hrtf-0.6.0 (c (n "hrtf") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rubato") (r "^0.6.0") (d #t) (k 0)) (d (n "rustfft") (r "^5.0.1") (d #t) (k 0)))) (h "1k0zbsk9yrq1f28dkgb9zh0nm0qpm4xn0vijvhp9vc7nm66rnjdb")))

(define-public crate-hrtf-0.7.0 (c (n "hrtf") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rubato") (r "^0.8.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "1nlmpqh93bvgpxgdalg289kdbbscfsrpb2gi77j6xm7x2wc0i2yr")))

(define-public crate-hrtf-0.8.0 (c (n "hrtf") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rubato") (r "^0.10.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "1gkmgs2vc2f4z0djdsc5zjxc8izjjqb62m1ajkp237bwbm58q23k")))

(define-public crate-hrtf-0.8.1 (c (n "hrtf") (v "0.8.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rubato") (r "^0.14.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "1rj808lzl8jkif4yf8xdzmlwiz8ld00kgwsym8rzlmgxhixf8k8g")))

