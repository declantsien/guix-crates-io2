(define-module (crates-io pm s- pms-7003) #:use-module (crates-io))

(define-public crate-pms-7003-0.1.0 (c (n "pms-7003") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)))) (h "0kx43l72n6ffvr417w0vknr1hxd8k5jav2baqx5335i3xgawjwmy")))

(define-public crate-pms-7003-0.2.0 (c (n "pms-7003") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)))) (h "1fxq904ml1f48zlsvk4ar6i4qzrr04ib5h5rh2fbkqhw6z4syg4f")))

(define-public crate-pms-7003-0.3.0 (c (n "pms-7003") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)))) (h "1pga3j8z0fpgbldfw212ghv6vf7y0vhk8m1l60qzb7fixay56wkj")))

