(define-module (crates-io pm x_ pmx_parser) #:use-module (crates-io))

(define-public crate-pmx_parser-0.1.0 (c (n "pmx_parser") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vnrf0r7vzx7zf8w46skr2fm1fhd7vywzvihns1yi465dvjg9xbb")))

(define-public crate-pmx_parser-0.1.1 (c (n "pmx_parser") (v "0.1.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "018hhz55bas9biyp6smfjn4dg7c7a7rb9igcfwfwrq0chzvrg3vd")))

(define-public crate-pmx_parser-0.2.0 (c (n "pmx_parser") (v "0.2.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a7zs5bifq1axc3qadsyf3032z3022c78abc9gfpiamig2prjzz6")))

