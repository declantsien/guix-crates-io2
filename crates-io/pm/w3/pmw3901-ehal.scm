(define-module (crates-io pm w3 pmw3901-ehal) #:use-module (crates-io))

(define-public crate-pmw3901-ehal-0.1.0 (c (n "pmw3901-ehal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-rtt-core") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "040g6gbj8yzp13nhwbwvdjyppjql54j4s8gkhn8kwslgvbhgv6y7") (f (quote (("rttdebug" "panic-rtt-core") ("default"))))))

