(define-module (crates-io pm w3 pmw3901) #:use-module (crates-io))

(define-public crate-pmw3901-1.0.0 (c (n "pmw3901") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "1pwazbr9myvk70n36lcb6zqjn3crc5ryszyqldgz8l5nsld6w69f")))

