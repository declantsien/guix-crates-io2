(define-module (crates-io pm s7 pms700x) #:use-module (crates-io))

(define-public crate-pms700x-0.1.0 (c (n "pms700x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1.0") (d #t) (k 0)))) (h "01hwgargbypapvgkv05f9k85m1vwx0ibznqljdb84dd16iilakzn")))

(define-public crate-pms700x-0.1.1 (c (n "pms700x") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1.0") (d #t) (k 0)))) (h "1hah1rimxfknpxi3835vp5xrjql4l1wy2k5whlam3nw3lgd7qsxp")))

