(define-module (crates-io pm w1 pmw1) #:use-module (crates-io))

(define-public crate-pmw1-0.1.0 (c (n "pmw1") (v "0.1.0") (h "15hbsx9x38bcwqyrlbmp8pyaazpjpch2wcpbi81zn5m4x7bg84h1")))

(define-public crate-pmw1-0.2.0 (c (n "pmw1") (v "0.2.0") (h "0mqxard43brk512i4a5xsc2qlf1qlxbvasj13q0xx77g59ynz1sd")))

(define-public crate-pmw1-0.2.1 (c (n "pmw1") (v "0.2.1") (h "08nvwf1kpvszav1jcn2mfgs4y4lk74m2v6y3fn44a8qnx0drf687")))

(define-public crate-pmw1-0.2.2 (c (n "pmw1") (v "0.2.2") (h "0ca4bnrhamq7wxjb2yi2vr0v8rgqc77i22vyk9dw5iw8ii2mfrs5")))

