(define-module (crates-io pm sa pmsa) #:use-module (crates-io))

(define-public crate-pmsa-0.1.0 (c (n "pmsa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 2)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1bklsh8dcz05rs410jrrja35c0f2jxfz0g9z5z6614xbv6p0gpzh")))

