(define-module (crates-io pm wi pmwiki-rss-discord-bot) #:use-module (crates-io))

(define-public crate-pmwiki-rss-discord-bot-1.0.0 (c (n "pmwiki-rss-discord-bot") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.20") (d #t) (k 0)) (d (n "rss") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("tls"))) (k 0)))) (h "1xyf7p0dijzlv8czgzi2skihryh47i4llq5wwarz4hsv5sm632qg")))

