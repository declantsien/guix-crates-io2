(define-module (crates-io pm ac pmac) #:use-module (crates-io))

(define-public crate-pmac-0.0.0 (c (n "pmac") (v "0.0.0") (h "0kk0j094wci38r05qasrhyn44a08ffrvikdx5w1kdmn9qz9vc8cj")))

(define-public crate-pmac-0.1.0 (c (n "pmac") (v "0.1.0") (d (list (d (n "aesni") (r "^0.2") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.6") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "dbl") (r "^0.1") (d #t) (k 0)))) (h "053pp5bg4bb819nhc67n2vpl3h5f95qj6b1lm5a97jad8l9cr0ha")))

(define-public crate-pmac-0.2.0 (c (n "pmac") (v "0.2.0") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.7") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "dbl") (r "^0.2") (d #t) (k 0)))) (h "1rb1x9s6h7k5bws9fmpadq1hnwxq7gcz0y9h0sys8rxahqzdcxmx")))

(define-public crate-pmac-0.3.0 (c (n "pmac") (v "0.3.0") (d (list (d (n "aes") (r "^0.4") (d #t) (k 2)) (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.8") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "dbl") (r "^0.3") (d #t) (k 0)))) (h "1fr6a6zyspi7fkjxf4am7g2bhhs75dfk05mg9f4xx5wm7jkxm6wp")))

(define-public crate-pmac-0.4.0 (c (n "pmac") (v "0.4.0") (d (list (d (n "aes") (r "^0.5") (d #t) (k 2)) (d (n "crypto-mac") (r "^0.9.1") (f (quote ("block-cipher"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.9.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "dbl") (r "^0.3") (d #t) (k 0)))) (h "0zkj78lprlbdw1r2k64k9fqf8cr210znhwlfaaa75bzs8va7hy31") (f (quote (("std" "crypto-mac/std"))))))

(define-public crate-pmac-0.5.0 (c (n "pmac") (v "0.5.0") (d (list (d (n "aes") (r "^0.6") (d #t) (k 2)) (d (n "crypto-mac") (r "^0.10") (f (quote ("cipher"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "dbl") (r "^0.3") (d #t) (k 0)))) (h "0vs7cpr4nxrjv80d1mqs9i4qvqcqbv14inad0mkg3rbfc0ycfylh") (f (quote (("std" "crypto-mac/std"))))))

(define-public crate-pmac-0.5.1 (c (n "pmac") (v "0.5.1") (d (list (d (n "aes") (r "^0.6") (d #t) (k 2)) (d (n "crypto-mac") (r "^0.10") (f (quote ("cipher"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "dbl") (r "^0.3") (d #t) (k 0)))) (h "1jq5wlfqrivpskv3h6qax7wyq5hqdp5483gl2mhpx6hhi7mmghp1") (f (quote (("std" "crypto-mac/std"))))))

(define-public crate-pmac-0.6.0 (c (n "pmac") (v "0.6.0") (d (list (d (n "aes") (r "^0.7") (f (quote ("force-soft"))) (d #t) (k 2)) (d (n "crypto-mac") (r "^0.11") (f (quote ("cipher"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.11") (f (quote ("dev"))) (d #t) (k 2)) (d (n "dbl") (r "^0.3") (d #t) (k 0)))) (h "04z0w16ni1ln86brihv3f430rhj5wmcy4yp2icd17dxkzamcdn22") (f (quote (("std" "crypto-mac/std"))))))

(define-public crate-pmac-0.7.0 (c (n "pmac") (v "0.7.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "dbl") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.2") (f (quote ("mac"))) (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)))) (h "0avg0856miq872p5mhj08kp2np9blnl4wz8cx5nig2ral26aaj7v") (f (quote (("zeroize" "cipher/zeroize") ("std" "digest/std")))) (r "1.56")))

(define-public crate-pmac-0.7.1 (c (n "pmac") (v "0.7.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "dbl") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("mac"))) (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "01z4dmfnbch8x3l5i4q6gwqaisddyrsk90sp5dr6lc7hxxvzf8ps") (f (quote (("zeroize" "cipher/zeroize") ("std" "digest/std")))) (r "1.56")))

