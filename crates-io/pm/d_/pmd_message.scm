(define-module (crates-io pm d_ pmd_message) #:use-module (crates-io))

(define-public crate-pmd_message-1.0.0 (c (n "pmd_message") (v "1.0.0") (d (list (d (n "binread") (r "^1.4.1") (d #t) (k 0)) (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "pmd_sir0") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "19naz2bylq69fzxpzzj5zi6m8pych2lr7b8scildij0ss78gzwwv")))

(define-public crate-pmd_message-1.0.1 (c (n "pmd_message") (v "1.0.1") (d (list (d (n "binread") (r "^1.4.1") (d #t) (k 0)) (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "pmd_sir0") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1f52qwh1k9wwxvisr00f092d4rr7yp6zdrcxiyr2k236g1dwvdk0")))

(define-public crate-pmd_message-1.0.2 (c (n "pmd_message") (v "1.0.2") (d (list (d (n "binread") (r "^1.4.1") (d #t) (k 0)) (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "pmd_sir0") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "08nygvlvp4frzm438j9dhsxg4kznnf4kqq8h5bgr5j7bhid8ng6z")))

(define-public crate-pmd_message-2.0.0 (c (n "pmd_message") (v "2.0.0") (d (list (d (n "binread") (r "^2.1.1") (d #t) (k 0)) (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "pmd_code_table") (r "^0.1.0") (d #t) (k 0)) (d (n "pmd_sir0") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1gv4hxcq031w5djljzpzh3xvrx7gkdhj88jc4njq18z2snpblvs4")))

