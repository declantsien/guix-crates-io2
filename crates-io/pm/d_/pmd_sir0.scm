(define-module (crates-io pm d_ pmd_sir0) #:use-module (crates-io))

(define-public crate-pmd_sir0-1.0.0 (c (n "pmd_sir0") (v "1.0.0") (d (list (d (n "io_partition") (r "^1.0.0") (d #t) (k 0)))) (h "18iwnfmfb99ihy8v3bj9d4q8crc7as8xaakqh8p2lhxrja8vwpa2")))

(define-public crate-pmd_sir0-1.1.0 (c (n "pmd_sir0") (v "1.1.0") (d (list (d (n "io_partition") (r "^1.0.0") (d #t) (k 0)))) (h "1gpf47gjyvcrl71cv1nhmcqdcv9280l4z4lszf7xfsp3ia27l1ns")))

(define-public crate-pmd_sir0-1.1.1 (c (n "pmd_sir0") (v "1.1.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "io_partition") (r "^1.0.0") (d #t) (k 0)))) (h "0k3fgb8iji8xj71lsr6x9j2pn0gq1rs1slpjwl0y389w61j1pyk6")))

(define-public crate-pmd_sir0-1.2.1 (c (n "pmd_sir0") (v "1.2.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "io_partition") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "105c1a6ajq7dd2mn6dhwb8ypmz78wgjd3vwm82darm0ha375jnmf")))

(define-public crate-pmd_sir0-1.2.2 (c (n "pmd_sir0") (v "1.2.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "io_partition") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1im0j5vp3w8y47rx97swjg6rgk5g1044gsb1ypgkw959w2k6z5rk")))

