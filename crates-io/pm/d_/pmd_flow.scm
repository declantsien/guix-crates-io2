(define-module (crates-io pm d_ pmd_flow) #:use-module (crates-io))

(define-public crate-pmd_flow-1.0.0 (c (n "pmd_flow") (v "1.0.0") (d (list (d (n "pmd_sir0") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d4xlj9g8d9n8zplsf36yy3ixzglbwmr4ni4i92sj538njwl021m")))

(define-public crate-pmd_flow-1.0.1 (c (n "pmd_flow") (v "1.0.1") (d (list (d (n "pmd_sir0") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "066skd4bcr1zaxg28c0ik13024mgll5jlryj11n9g36q4b5nrbbi")))

(define-public crate-pmd_flow-1.0.2 (c (n "pmd_flow") (v "1.0.2") (d (list (d (n "pmd_sir0") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j4ji80c4fx4hixwqb3822asccp2dnrha4qyrx0wg5kdg834xhjb")))

