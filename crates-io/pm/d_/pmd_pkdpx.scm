(define-module (crates-io pm d_ pmd_pkdpx) #:use-module (crates-io))

(define-public crate-pmd_pkdpx-1.0.0 (c (n "pmd_pkdpx") (v "1.0.0") (d (list (d (n "io_partition") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0m5678w2r4ahfvrngx1arhaf7yyry7zgryn8l594p4kvb281pafw")))

(define-public crate-pmd_pkdpx-1.1.0 (c (n "pmd_pkdpx") (v "1.1.0") (d (list (d (n "io_partition") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0s7m8aid12y0a0zn3xzjm2wybkwrzybsiia3n5kq9r5ijxbw173m")))

