(define-module (crates-io pm d_ pmd_farc) #:use-module (crates-io))

(define-public crate-pmd_farc-0.1.0 (c (n "pmd_farc") (v "0.1.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "io_partition") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pmd_sir0") (r "^1.0.0") (d #t) (k 0)))) (h "0rxwirid89nz61vnivhvs4hqxrldszbnhdw2gg87cr38p8rfvswb")))

(define-public crate-pmd_farc-1.0.0 (c (n "pmd_farc") (v "1.0.0") (d (list (d (n "binread") (r "^1.4.1") (d #t) (k 0)) (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "io_partition") (r "^1.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pmd_sir0") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0mpxr76lia1jjx2j45xh03qzs9dkhx4fdzsd4gly5gfdbzdmnq34")))

(define-public crate-pmd_farc-1.0.1 (c (n "pmd_farc") (v "1.0.1") (d (list (d (n "binread") (r "^1.4.1") (d #t) (k 0)) (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "io_partition") (r "^1.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pmd_sir0") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0wq4i01iqqc4pbblwgj0y618yrgqzgq6rx9gng24fhdi2ypzz5si")))

