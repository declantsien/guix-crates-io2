(define-module (crates-io pm d_ pmd_cte) #:use-module (crates-io))

(define-public crate-pmd_cte-1.0.0 (c (n "pmd_cte") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0lw4xnvj3drx91fiwk4dqq73cn298nw4yc8b7wgzlcqhshbp021p")))

