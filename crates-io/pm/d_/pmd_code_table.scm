(define-module (crates-io pm d_ pmd_code_table) #:use-module (crates-io))

(define-public crate-pmd_code_table-0.1.0 (c (n "pmd_code_table") (v "0.1.0") (d (list (d (n "binread") (r "^2.1.1") (d #t) (k 0)) (d (n "pmd_sir0") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1pxynfi8jvwwjkpaj3wkg66psfhyn09jqgv4d4x4qr43v7n286wq")))

