(define-module (crates-io pm d_ pmd_dic) #:use-module (crates-io))

(define-public crate-pmd_dic-1.0.0 (c (n "pmd_dic") (v "1.0.0") (d (list (d (n "binread") (r "^1.4.1") (d #t) (k 0)) (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1lb08pbf9y560dk0wjm0bmk7h5x03jbvncmv0vvi7ax4kwplmnim")))

(define-public crate-pmd_dic-1.1.1 (c (n "pmd_dic") (v "1.1.1") (d (list (d (n "binread") (r "^2.1.1") (d #t) (k 0)) (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1v68149qz27ka4j6xcqsbpc6lcgnrzlpvdskdxj9ny0gp22cdn9n")))

