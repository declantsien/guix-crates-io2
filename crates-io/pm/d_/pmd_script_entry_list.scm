(define-module (crates-io pm d_ pmd_script_entry_list) #:use-module (crates-io))

(define-public crate-pmd_script_entry_list-1.0.0 (c (n "pmd_script_entry_list") (v "1.0.0") (d (list (d (n "pmd_sir0") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h6fxdl8wwpjcxxbm644dgv5nrndxj4zgw8iwalmzy3bsqpzq37y")))

