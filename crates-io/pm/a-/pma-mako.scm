(define-module (crates-io pm a- pma-mako) #:use-module (crates-io))

(define-public crate-pma-mako-0.1.0 (c (n "pma-mako") (v "0.1.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "flume") (r "^0.10.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xnz96g207vb2nrsqnfs6sdjyls9a2216q9c592z2j4b9jyj1k0s")))

