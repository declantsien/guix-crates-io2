(define-module (crates-io pm ap pmap) #:use-module (crates-io))

(define-public crate-pmap-0.0.4 (c (n "pmap") (v "0.0.4") (h "0w0659pczb72k46b7yy0dqmyf65x2z8v8rhhlxjichy62rakjvcm")))

(define-public crate-pmap-0.0.5 (c (n "pmap") (v "0.0.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1988bjpnmrz4j2jy41rcbh3kv8lr8nl1awil2546wfhj3j0q6v5x")))

