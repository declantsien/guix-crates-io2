(define-module (crates-io pm c- pmc-rs) #:use-module (crates-io))

(define-public crate-pmc-rs-0.1.0 (c (n "pmc-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pmc-sys") (r "^0.1") (d #t) (k 0)))) (h "0dvarjvp2g9l1f71hqc9c993qwhhvbn22nzy7iwnnmj7xvgm8cnx")))

(define-public crate-pmc-rs-0.1.1 (c (n "pmc-rs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pmc-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0wdhy3zvw36j05p49ym15yq860clpga4yqm839nni8wd926ap8b8")))

(define-public crate-pmc-rs-0.2.0 (c (n "pmc-rs") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pmc-sys") (r "^0.1.2") (d #t) (t "cfg(target_os = \"freebsd\")") (k 0)))) (h "1j65gkxw8sh0v0b3w4wa185fqs3kfh7mpkc5arqi12v0vkkqzwxj")))

(define-public crate-pmc-rs-0.2.1 (c (n "pmc-rs") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pmc-sys") (r "^0.1.2") (d #t) (t "cfg(target_os = \"freebsd\")") (k 0)))) (h "0v1vmdn839vkhliam5fsqf8xw0n11y41vh4lvv2ws1vfzr1156pa")))

(define-public crate-pmc-rs-0.2.2 (c (n "pmc-rs") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pmc-sys") (r "^0.1.3") (d #t) (t "cfg(target_os = \"freebsd\")") (k 0)))) (h "1kf2zxsy9ydc9hw4w8h97v6rgm43zjdnrscxfbs2zgijqspv4cwx")))

