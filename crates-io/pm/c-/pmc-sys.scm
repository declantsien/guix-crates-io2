(define-module (crates-io pm c- pmc-sys) #:use-module (crates-io))

(define-public crate-pmc-sys-0.1.0 (c (n "pmc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wyng87qb66y16qb3jlkmrdfxgqqvdvimvlnrfcnwdfn8xy5dgl3")))

(define-public crate-pmc-sys-0.1.1 (c (n "pmc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sv11zdw897npxfhdwfn1dbj6i15x6pb68rfh2r6srqa7lzs8vdg")))

(define-public crate-pmc-sys-0.1.2 (c (n "pmc-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10jinx70fxw9cx5g27ng4df90nycmghmnjzdlglqdrgy1ijq8qqq") (l "pmc")))

(define-public crate-pmc-sys-0.1.3 (c (n "pmc-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s4h84ji0iy16cvs7q263izf9nwfbhnb0ygxplb4m837d62wmb22") (l "pmc")))

