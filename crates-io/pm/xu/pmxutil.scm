(define-module (crates-io pm xu pmxutil) #:use-module (crates-io))

(define-public crate-PMXUtil-0.1.0 (c (n "PMXUtil") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)))) (h "1pj5kbnci7bi8qz3ynlmcm49g3sz4hnvmnmywrfg3pm0i8kip8g1") (y #t)))

(define-public crate-PMXUtil-0.1.1 (c (n "PMXUtil") (v "0.1.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)))) (h "09pg81jpwib2g94cvchli8fnbfagg2959b0zi1knr74hffaag4fc")))

(define-public crate-PMXUtil-0.1.2 (c (n "PMXUtil") (v "0.1.2") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)))) (h "13564q2haa5q1j2pn8igrhn9j742divrz9fplh9jn29p8vcb7vg5")))

(define-public crate-PMXUtil-0.2.0 (c (n "PMXUtil") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "1w8wk3aj3lhkc1q72whh07sy9xpy4wk4hzrqzmgypk3bfa5ayq4a")))

(define-public crate-PMXUtil-0.2.3 (c (n "PMXUtil") (v "0.2.3") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "0ppwb6mvb5dk42a62hzmc98hf6bhlmgvby5cn0n59vn1hfd1gh0j")))

(define-public crate-PMXUtil-0.3.0 (c (n "PMXUtil") (v "0.3.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "1441wvr8126r7hrz2zavknfsxfyyk8qgk9swxzh769i035kg7hih")))

(define-public crate-PMXUtil-0.3.1 (c (n "PMXUtil") (v "0.3.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "09mkjvyw971mm5n0bc7a1ri1lv7cvcrxvflclpq9a6ismdnjsclz")))

(define-public crate-PMXUtil-0.3.2 (c (n "PMXUtil") (v "0.3.2") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "0hvafaqpxbl8cga1mj7ywycpfy5xb2h7i19gm8nf26w3fqyh3br6")))

(define-public crate-PMXUtil-0.3.3 (c (n "PMXUtil") (v "0.3.3") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "1gblrfwrs6gwq4fak1z2ilsgbw3b818hlqn3j5fwr9qsfgh15nnp")))

(define-public crate-PMXUtil-0.4.0 (c (n "PMXUtil") (v "0.4.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "1n4xj3nl887sqjyvb10g3h2nw1j71223zvkqkcn330b1llmhxja1")))

(define-public crate-PMXUtil-0.5.0 (c (n "PMXUtil") (v "0.5.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "00k649f9sin6xx851nyx67rcxm3cmi4mmb04jsbs6s2n2cy83iaw")))

(define-public crate-PMXUtil-0.6.0 (c (n "PMXUtil") (v "0.6.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "10zhdxsqi2npa3dsp75n6rcgg0zc7zvi4mcvxdf403kvm410fya6")))

(define-public crate-PMXUtil-0.6.1 (c (n "PMXUtil") (v "0.6.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "1zjjphm4wmwlkyawjwshn3qkc8phz93xmdb36wxjzk78cixycvfn")))

(define-public crate-PMXUtil-0.7.0 (c (n "PMXUtil") (v "0.7.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "0wvav8p3j67avlrkzydgli921m5a16zs7mp226kyrlq2isy8zl6j")))

(define-public crate-PMXUtil-0.8.0 (c (n "PMXUtil") (v "0.8.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1shjnvd0l8a7ixja06f6w29jygjfvccyhlgj232w77xsm6ri2f80")))

(define-public crate-PMXUtil-0.8.2 (c (n "PMXUtil") (v "0.8.2") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1nnnjx94560df0py8g7lsa1wd6rhcia1rrvhr4crxggl2qiynbrf")))

(define-public crate-PMXUtil-0.9.0 (c (n "PMXUtil") (v "0.9.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1y16qv2b6zafb2dizcj80764xayb1dp2fx1q3zr2q9ni6agm4kl8")))

