(define-module (crates-io pm ft pmftree) #:use-module (crates-io))

(define-public crate-pmftree-0.1.0 (c (n "pmftree") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libpolymesh") (r "^1") (d #t) (k 0)))) (h "0zq0pcw60wmnskl42sd967jmgbdixrdgya8mycv3yjnn0wq3mbdx")))

