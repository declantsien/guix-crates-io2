(define-module (crates-io pm tr pmtree) #:use-module (crates-io))

(define-public crate-pmtree-1.0.0 (c (n "pmtree") (v "1.0.0") (d (list (d (n "ark-serialize") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 2)))) (h "1wcf2fkl9v0b4rm2ql77bsk6ri7jaa5cnwzb7ws702gxynsxba6q")))

(define-public crate-pmtree-2.0.0 (c (n "pmtree") (v "2.0.0") (d (list (d (n "ark-serialize") (r "=0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "=0.3.4") (d #t) (k 2)) (d (n "rayon") (r "=1.7.0") (d #t) (k 0)) (d (n "sled") (r "=0.34.7") (d #t) (k 2)) (d (n "tiny-keccak") (r "=2.0.2") (f (quote ("keccak"))) (d #t) (k 2)))) (h "047m4s6ncr58x941qmv7bmgy90k62sbpgf27rn3csb3dx4p34m70")))

