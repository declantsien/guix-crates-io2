(define-module (crates-io pm #{10}# pm1006) #:use-module (crates-io))

(define-public crate-pm1006-0.0.1 (c (n "pm1006") (v "0.0.1") (d (list (d (n "embedded-io") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1sk3gvjcrd7lmwdzd4i9h2an8dz5y9lwp3pl381kk1fw5sp0z9k5") (f (quote (("default" "log"))))))

(define-public crate-pm1006-0.0.2 (c (n "pm1006") (v "0.0.2") (d (list (d (n "embedded-io") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1r1918895bmac53j4hxfw7c8kvpg007bnf3rl2sk9jvs3xkxjjyr") (f (quote (("default" "log"))))))

