(define-module (crates-io pm df pmdfonttool) #:use-module (crates-io))

(define-public crate-pmdfonttool-1.0.0 (c (n "pmdfonttool") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "pmd_cte") (r "^1.0.0") (d #t) (k 0)) (d (n "pmd_dic") (r "^1.0.0") (d #t) (k 0)))) (h "1mvy95qcylwkgb3dn83bhwcpij18zzm8clkjw55hcsm9j3rkn8hk")))

(define-public crate-pmdfonttool-1.1.0 (c (n "pmdfonttool") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "fontdue") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "pmd_cte") (r "^1.0.0") (d #t) (k 0)) (d (n "pmd_dic") (r "^1.1.1") (d #t) (k 0)))) (h "1c4nxf2g5qajvbpcwj136insrf1dab5qyja4d0fan2prwillmksb")))

