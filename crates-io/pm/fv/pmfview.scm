(define-module (crates-io pm fv pmfview) #:use-module (crates-io))

(define-public crate-pmfview-0.1.0 (c (n "pmfview") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libpolymesh") (r "^1") (d #t) (k 0)) (d (n "raylib") (r "^3.5.0") (d #t) (k 0)))) (h "0d3v7acv6pc7mrl3qgzssaa73zxmbsq7g7y6vs44mqlrl1scwm0p")))

