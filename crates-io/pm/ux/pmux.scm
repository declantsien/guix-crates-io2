(define-module (crates-io pm ux pmux) #:use-module (crates-io))

(define-public crate-pmux-0.0.0 (c (n "pmux") (v "0.0.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "0l2rfyikz5k0lkhy3i9c96n36xhain4bzldd6wigwx15lrc4rj4m") (y #t)))

(define-public crate-pmux-0.0.1 (c (n "pmux") (v "0.0.1") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "1fwfbkzp18bv548rpin2maxg53lr766ilmfq6zqvajn51na75wi8") (y #t)))

(define-public crate-pmux-0.0.2 (c (n "pmux") (v "0.0.2") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "166jypnlwaw0pc2kzgl9xsh3nv15p4hmxc11pd6bwyaf08xhljh2") (y #t)))

