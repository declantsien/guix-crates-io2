(define-module (crates-io pm dk pmdk-sys) #:use-module (crates-io))

(define-public crate-pmdk-sys-0.0.2 (c (n "pmdk-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sri55mc0h71i99k6v9lk6ibgyz4aw4gwwhiz8h3afs3rpb6dnsr")))

(define-public crate-pmdk-sys-0.0.3 (c (n "pmdk-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a23v5wdrdcyfz8j24nc012k73vx2p6jc9b68dxk2kgnd3f76z5h")))

(define-public crate-pmdk-sys-0.0.4 (c (n "pmdk-sys") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0px0p9fscr2nzp2363k7izcjl3qdzcywhmd57341qzlagfm49siq")))

(define-public crate-pmdk-sys-0.0.5 (c (n "pmdk-sys") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10vmddd19g3wl9ahqagkkkavbnrkpb3x00iwr3f0g9s0bwri5k9i") (y #t)))

(define-public crate-pmdk-sys-0.0.6 (c (n "pmdk-sys") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qvxaj61wj2dzw3a2ldammap25nc3rmgglaxg8s1i7n4sv2hxw1i")))

(define-public crate-pmdk-sys-0.1.0 (c (n "pmdk-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hzxrdxln0rhpz0a1i99bxdrpwv0z20nxk3yb1wnb25ha5ilk0qr") (l "pmemobj")))

(define-public crate-pmdk-sys-0.1.1 (c (n "pmdk-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "144574jnhgsm0k7yijsgx3l078jkn7hnr9lzprgd6c45acj7mvlq") (y #t) (l "pmemobj")))

(define-public crate-pmdk-sys-0.2.0 (c (n "pmdk-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v64jbckwj6nil6x6namx1lj5havmi3c3a8v3g54srmi1p4kai10") (l "pmemobj")))

(define-public crate-pmdk-sys-0.3.0 (c (n "pmdk-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17rylz9q63y9jhskw4qhcvzmkbbv5mh8ds6wrjlfm3dj89hknq5v") (l "pmemobj")))

(define-public crate-pmdk-sys-0.4.0 (c (n "pmdk-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "057f2jpd5ppg0k80yaiqlz670lnk22kx4wb6nz2cdn28zldgmi3b") (l "pmemobj")))

(define-public crate-pmdk-sys-0.5.0 (c (n "pmdk-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09hbp4bzic73vrx50y00mkb2r4xmw3hx0bypapl22m9lzrd58lq5") (l "pmemobj")))

(define-public crate-pmdk-sys-0.6.0 (c (n "pmdk-sys") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y4akjby53w8sqp3brn41v3s0iwm4c8fgp9zwrsl0a2akvyhr65q") (l "pmemobj")))

(define-public crate-pmdk-sys-0.7.0 (c (n "pmdk-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cxmy8g5m3a70zk8wdwim3350ajkw2lw9k5qv34k2pfl2cjfsg98") (l "pmemobj")))

(define-public crate-pmdk-sys-0.8.0 (c (n "pmdk-sys") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rcnw93sqy1fddn2xmnay023zsrg1szjhaha58izxxkzpqmg740x") (l "pmemobj")))

(define-public crate-pmdk-sys-0.9.0 (c (n "pmdk-sys") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iczmhnarf2lv15l2v129m7994fk6i0d0rgrzkfn6zb2vnklwg4s") (l "pmemobj")))

(define-public crate-pmdk-sys-0.9.1 (c (n "pmdk-sys") (v "0.9.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1549v5vas1ya32xh9g95ggidqixfj7y79ifk7snpd7achqnwvjyl") (l "pmemobj")))

(define-public crate-pmdk-sys-0.9.2 (c (n "pmdk-sys") (v "0.9.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0isrpgpl82dil15qq3csckcn6w8lasdb7475bwfd9ynx71mdq8f4") (l "pmemobj")))

(define-public crate-pmdk-sys-0.9.4 (c (n "pmdk-sys") (v "0.9.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0khbicy3j9fgmckipscqskh6s19581a1a8wy4kyvwqdfp53wgdnv") (l "pmemobj")))

