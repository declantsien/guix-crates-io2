(define-module (crates-io pm em pmem) #:use-module (crates-io))

(define-public crate-pmem-0.0.0 (c (n "pmem") (v "0.0.0") (h "0jih98adbx31az4jvadzzvy5li3bj063dr3ldba9ibp9q157xpqm")))

(define-public crate-pmem-0.0.1 (c (n "pmem") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pxk57dxx008wfyhbx4032km23yzxh9pvzjgvzf4xr8pjnkrv6hi")))

(define-public crate-pmem-0.0.2 (c (n "pmem") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pmem-sys") (r "^0.0") (d #t) (k 0)))) (h "1jkdcac2aax6xyhv1bfakzi2nqk4apg9v4pipx2ffyd04fyc03hn")))

(define-public crate-pmem-0.1.0 (c (n "pmem") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pmem-sys") (r "^0.0") (d #t) (k 0)))) (h "1fvffvvk68hnyyq3v1z6ab2c1zf5cc3r6rcq6f2mkh61qy5lgfy9")))

