(define-module (crates-io pm em pmemkv) #:use-module (crates-io))

(define-public crate-pmemkv-0.1.0-alpha.0 (c (n "pmemkv") (v "0.1.0-alpha.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "pmemkv-sys") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "1ga0r3h0kyxxfshmbsj7lkv359f7klqgsz75ljp9zs51675vm9wf")))

(define-public crate-pmemkv-0.1.1-alpha.0 (c (n "pmemkv") (v "0.1.1-alpha.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "pmemkv-sys") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "1dk6q91hayq4d3y8pbwin0m8jql31ldz3sv9gi8r5ng0v143ib9g")))

(define-public crate-pmemkv-0.1.2-alpha.0 (c (n "pmemkv") (v "0.1.2-alpha.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "pmemkv-sys") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "06jmxh5pnlr5n4m29rzd0g7j7b41qrnzb8g49qzb6az44sd9nm1a")))

(define-public crate-pmemkv-0.1.3-alpha.0 (c (n "pmemkv") (v "0.1.3-alpha.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "pmemkv-sys") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "1fp2kvzpd2j757nqc7wpjcjjp1b52dmia8pwg5zvpm31dqbg5l59")))

