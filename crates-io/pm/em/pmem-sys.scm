(define-module (crates-io pm em pmem-sys) #:use-module (crates-io))

(define-public crate-pmem-sys-0.0.1 (c (n "pmem-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xmwzbnxialj1lcjxrsdlvq2k4qmr6qnsblliqh5wd2695iik5d8")))

(define-public crate-pmem-sys-0.0.3 (c (n "pmem-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hnqppdhvd396mp8lsvh1vyylii3gdjdq26b4ls4ysn8pdf5q8cs")))

