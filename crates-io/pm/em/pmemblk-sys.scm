(define-module (crates-io pm em pmemblk-sys) #:use-module (crates-io))

(define-public crate-pmemblk-sys-0.0.1 (c (n "pmemblk-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cdb50arg9k56igpsphp4wbih74f5lsppn2421bxhqfljlzzw891")))

(define-public crate-pmemblk-sys-0.0.2 (c (n "pmemblk-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ngyy47pqjm6zkcnkc43xl7nfqvzhcd3rgy1ikfcbh04wcryhm4v")))

