(define-module (crates-io pm em pmem-blk) #:use-module (crates-io))

(define-public crate-pmem-blk-0.0.1 (c (n "pmem-blk") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pmemblk-sys") (r "^0.0") (d #t) (k 0)))) (h "1jz1fjngd0jz3j7il43w6ajqfmsb9350gmh033vvq73sam4cq296")))

(define-public crate-pmem-blk-0.0.2 (c (n "pmem-blk") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pmemblk-sys") (r "^0.0") (d #t) (k 0)))) (h "1hhk13ma5f6lfc6i82jcgfhm0isv4c84811vcb5rvccq3r6vz312")))

