(define-module (crates-io pm fe pmfextract) #:use-module (crates-io))

(define-public crate-pmfextract-0.1.0 (c (n "pmfextract") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libpolymesh") (r "^1") (d #t) (k 0)))) (h "0p035ck2f0jvr3s7lmhzka95j8702bmqgw3zhllzb5lk3p4w0lcf")))

