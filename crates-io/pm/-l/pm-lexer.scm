(define-module (crates-io pm -l pm-lexer) #:use-module (crates-io))

(define-public crate-pm-lexer-0.0.1 (c (n "pm-lexer") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0l90r6505hlw5lfwb52di6bh4i68p04xdzj08ikr8qza6xmlb3rr")))

(define-public crate-pm-lexer-0.1.0 (c (n "pm-lexer") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0w7xz55rdwcsqvm5r9k0pq75ig0d044hdvyx2c8lbwqh89nk10xf")))

(define-public crate-pm-lexer-0.1.1 (c (n "pm-lexer") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0y9cz0wdl9r3f9bkaz76wrg48ch30dynxyv0l5zkk0aymb5m1wg8")))

(define-public crate-pm-lexer-0.1.2 (c (n "pm-lexer") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1nx5w0vlpvk0h3gxxlbzrmwbsyiaawxxsxrxbj561kf266ir2bmi")))

(define-public crate-pm-lexer-0.1.3 (c (n "pm-lexer") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1gilidjyjajhxhrc77r100w2fqvykpsvwx9iz0fr91h0wy28v23f")))

(define-public crate-pm-lexer-0.1.4 (c (n "pm-lexer") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "00wnwybjwxgylp0yb8xmi5rgy5z675j5dc4j4vdbyrnb0ijnwg38")))

