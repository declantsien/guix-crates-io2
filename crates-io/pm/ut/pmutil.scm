(define-module (crates-io pm ut pmutil) #:use-module (crates-io))

(define-public crate-pmutil-0.1.0 (c (n "pmutil") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2.1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.3") (f (quote ("derive" "parsing"))) (k 0)))) (h "1mjlkmzyvcxs6627d97qjlba2a8af3y7lziy04aawdm3s4mq30sl")))

(define-public crate-pmutil-0.2.0 (c (n "pmutil") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("derive" "parsing"))) (k 0)))) (h "1d384hhaq1ga564b5wgvdvbrxhj61c2q0kc2n6w69qrq5kqbfn1z")))

(define-public crate-pmutil-0.3.0 (c (n "pmutil") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("derive" "parsing"))) (k 0)))) (h "1z1686k2dj281b7slra8ghh8zd1ciryp7mflv3hacjqxpncri36w")))

(define-public crate-pmutil-0.4.0 (c (n "pmutil") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (k 0)))) (h "00z0960r92zhmz2dy83sm7zxnavhb0mfq162i4x9k7qybsggb201")))

(define-public crate-pmutil-0.5.0 (c (n "pmutil") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (k 0)))) (h "1wffpfq2n8vqmqsq4bdvmal0lsk7x43b9fk0gwxmg23lqgk8z7l8")))

(define-public crate-pmutil-0.5.1 (c (n "pmutil") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (k 0)))) (h "00r85njpj1784k5kghihw07izhpcs73fzk1nxf23b433rm2xskaz")))

(define-public crate-pmutil-0.5.2 (c (n "pmutil") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (k 0)))) (h "1bsqni870j30ycrmhlgr2skcd1x8mwmhnm497znl5jiv8047xfrl")))

(define-public crate-pmutil-0.5.3 (c (n "pmutil") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (k 0)))) (h "0170zgziivri4qsch682pga3qq3z4wpr4wngzr5f9jyc97ayb51q")))

(define-public crate-pmutil-0.6.0 (c (n "pmutil") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (k 0)))) (h "0zb0x3bdh09paqy7gpxhjr7na0nm34fgksnb9lq5sg1137lw1lkc")))

(define-public crate-pmutil-0.6.1 (c (n "pmutil") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (k 0)))) (h "1ih9gbgcygfyrxqlgm3smffzqngzlnlpn5lb5l6h8n1c1k3hp92j")))

