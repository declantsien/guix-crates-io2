(define-module (crates-io pm he pmhelp-internal-macros) #:use-module (crates-io))

(define-public crate-pmhelp-internal-macros-0.0.1 (c (n "pmhelp-internal-macros") (v "0.0.1") (d (list (d (n "pmhelp-internal") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0s10h28jlx1z8z953vwmfgzn0a2sxr351yb6svxr1150s0xmdmdw")))

(define-public crate-pmhelp-internal-macros-0.0.2 (c (n "pmhelp-internal-macros") (v "0.0.2") (d (list (d (n "pmhelp-internal") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rpmhwv2b9ab36vyyjbhay8aiwc3gk5z721iiji7kdsj8z6pddfn")))

