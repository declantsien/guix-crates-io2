(define-module (crates-io pm he pmhelp) #:use-module (crates-io))

(define-public crate-pmhelp-0.0.1 (c (n "pmhelp") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10k7d3n18in6n3kkvb4s0mpcs08my4jxw4mswvbjmssgny8lh51w")))

(define-public crate-pmhelp-0.0.2 (c (n "pmhelp") (v "0.0.2") (d (list (d (n "pmhelp-internal") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1akpivgdrp5hwrxg56zmppf091cf5ahh97gwd3v8c1gac70y86wz")))

