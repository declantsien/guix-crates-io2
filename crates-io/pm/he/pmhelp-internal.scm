(define-module (crates-io pm he pmhelp-internal) #:use-module (crates-io))

(define-public crate-pmhelp-internal-0.0.1 (c (n "pmhelp-internal") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0c4nxyz6jjsanr44f89h2i6g66v4c2i920rfxgmsmj8y6jcwyx1j")))

(define-public crate-pmhelp-internal-0.0.2 (c (n "pmhelp-internal") (v "0.0.2") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rk0vpz2qdxxq2v3kmxcl1vhdpg712c5gp1bj7d6xd6khab1hwwf") (f (quote (("from-base"))))))

