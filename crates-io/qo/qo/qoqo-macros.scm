(define-module (crates-io qo qo qoqo-macros) #:use-module (crates-io))

(define-public crate-qoqo-macros-0.1.0 (c (n "qoqo-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k7j0vqxifinv0n2mzz7flfraaly5pv757g0l5bcd1rp77bmklic")))

(define-public crate-qoqo-macros-0.1.2 (c (n "qoqo-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1l0hzz52whz0qxf5jghrqzjbjf4d121aq2mgjc9mic377kx7zn3r")))

(define-public crate-qoqo-macros-0.1.4 (c (n "qoqo-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "02lmwl50z4x9v276nbgchd6ld4kknh9bgz8g4m2zixcfsgi7zdci")))

(define-public crate-qoqo-macros-0.5.0 (c (n "qoqo-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "151gsbshi9qy706a5h8dw1nsq97cx45j0agfxm17a1nndzdvjq8c")))

(define-public crate-qoqo-macros-0.5.1 (c (n "qoqo-macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1vw86z4frn7a473nbcggklhpmlyjdlcc70fqgvm4gh6dzavrby5v")))

(define-public crate-qoqo-macros-0.6.0 (c (n "qoqo-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1dci7dp9hny9310vciiwrxrsvilk570g2ydc0y4wk35sxiab05vi")))

(define-public crate-qoqo-macros-0.6.1 (c (n "qoqo-macros") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "12dk2mcg8x9jxikyg7k6s37v3qpgji6wy0pw999a9h5pqp03m735")))

(define-public crate-qoqo-macros-0.6.2 (c (n "qoqo-macros") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1y10hfqa1z2ajx5ybdigl2mk0ljrq2d9z1ymwmvnx9jhs956hlwr")))

(define-public crate-qoqo-macros-0.6.3 (c (n "qoqo-macros") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "09r974x5nsd3pmci5c4krf3j1qfam68f95wb6vffmkd6yf9yp6ps") (r "1.56")))

(define-public crate-qoqo-macros-0.7.0 (c (n "qoqo-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0d7c40zld5fi30pf1kpxxs3v2z0fai5q4bqb02d1qp7zxwbygv58") (r "1.56")))

(define-public crate-qoqo-macros-0.8.0 (c (n "qoqo-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1q3xcsdyrba7wclz8yfm105xnjvlfv1bq3z050wc5dqxwdraidrr") (r "1.56")))

(define-public crate-qoqo-macros-0.8.1 (c (n "qoqo-macros") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "04408gjm1ky45mvcqwflc90f9hpl6kp1m2vk7jd3kak0kj2lk44x") (r "1.56")))

(define-public crate-qoqo-macros-0.8.2 (c (n "qoqo-macros") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1zq9l0b8z2k5n7sl1yqhgqjavq76702i63lxwalqvj87dsnqq92p") (r "1.56")))

(define-public crate-qoqo-macros-0.9.0 (c (n "qoqo-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1ayzc5qlj1phd8c2jky7gxqa5bd4drknhlzqqq9nl8wwqja8l85q") (r "1.56")))

(define-public crate-qoqo-macros-0.10.0 (c (n "qoqo-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "15zh0hc182cwdk6n59csna3ymd3152vaffl96rxpdbqad1vk4rr6") (r "1.56")))

(define-public crate-qoqo-macros-0.11.3 (c (n "qoqo-macros") (v "0.11.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0a6hfrgcxcgxdbalxq0n9b97vm8bd7zrbcnj28ps08f1xl59d4s2") (r "1.56")))

(define-public crate-qoqo-macros-1.0.0-alpha (c (n "qoqo-macros") (v "1.0.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1aygzq105lc9riz57svqsdi21gryzlxin7wygycqlz6cib2lv54l") (r "1.56")))

(define-public crate-qoqo-macros-1.0.0-alpha.1 (c (n "qoqo-macros") (v "1.0.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1n9f7s9r5bxqd3sczqbpi5s60aq95islyif0dmm681lsi7kkcsfy") (r "1.56")))

(define-public crate-qoqo-macros-1.0.0-alpha.2 (c (n "qoqo-macros") (v "1.0.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1gv0alqi8l1s7q4j8i6d11d6ksazxxs0z5mh6apjnxhnc4ly20lj") (r "1.56")))

(define-public crate-qoqo-macros-1.0.0-alpha.3 (c (n "qoqo-macros") (v "1.0.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0bi87m9y4h1j2fb248sjsx0il8694srljz54y32nblmpryfhqfg1") (r "1.56")))

(define-public crate-qoqo-macros-1.0.0-alpha.4 (c (n "qoqo-macros") (v "1.0.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0kvg95jgzqg43kvxvvwlg59l0m2xv1q3pyacm7p35h5jvlb5q546") (r "1.56")))

(define-public crate-qoqo-macros-1.0.0-alpha.5 (c (n "qoqo-macros") (v "1.0.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "11hmncaxj6cqb1pf4fk77dmlzcv7qw6f176gf55l23vly4nd718x") (r "1.56")))

(define-public crate-qoqo-macros-1.0.0-beta.1 (c (n "qoqo-macros") (v "1.0.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "084c71pji26xm4ps667fk0jclibpy6mbvj13792m5p8zbdak4c91") (r "1.56")))

(define-public crate-qoqo-macros-1.0.0-beta.2 (c (n "qoqo-macros") (v "1.0.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "12qg6qd5ggvlq98i24c55316fmpfmm9mrx2l54fk5mvww1smvimw") (r "1.56")))

(define-public crate-qoqo-macros-1.0.0 (c (n "qoqo-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1ikvn3wqmq9y1xx1jmgkp6rrdrjp14z84d1cvrrid7r2r0h5n90l") (r "1.56")))

(define-public crate-qoqo-macros-1.1.0-beta.1 (c (n "qoqo-macros") (v "1.1.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1pj5hh35yzazqnqndhg07m4lzm5vq2jn1p3ndj8ihwwsscwj6z95") (r "1.56")))

(define-public crate-qoqo-macros-1.1.0-beta.2 (c (n "qoqo-macros") (v "1.1.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "168s4bgdpb5cc8c0g065qhdg5856njia37js0xydgjlp0ikjl0dp") (r "1.56")))

(define-public crate-qoqo-macros-1.1.0-beta.10 (c (n "qoqo-macros") (v "1.1.0-beta.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0vrhs3m8myp54xmqc6vvqbx7dkqg9kfhjmn1swl7f98wf5yb86z4") (r "1.56")))

(define-public crate-qoqo-macros-1.1.0-beta.12 (c (n "qoqo-macros") (v "1.1.0-beta.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1l7flh3xypppchy6ych3rpdjxssc6hmc890j9yvk4kirfqg1y6n8") (r "1.56")))

(define-public crate-qoqo-macros-1.1.0 (c (n "qoqo-macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0p0ga4y4gi3b3zq462n7sr56zkwnmkwqqjh62va1zly97z8xhm98") (r "1.56")))

(define-public crate-qoqo-macros-1.2.0-alpha.1 (c (n "qoqo-macros") (v "1.2.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0imfbwyh88b964v24q7gsqp43qq87sd7rmfr0a3hprnkd4gb85fy") (r "1.56")))

(define-public crate-qoqo-macros-1.2.0-alpha.2 (c (n "qoqo-macros") (v "1.2.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0zi2i44lm0g7aw9885si4xnjlaribkzsciag664229dkgfzwzjrk") (r "1.56")))

(define-public crate-qoqo-macros-1.2.0-alpha.3 (c (n "qoqo-macros") (v "1.2.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "00vaw2pnds9l72zcj4s4s82rg5sqhnmq5wrx1cybaxz159lj8ck0") (r "1.56")))

(define-public crate-qoqo-macros-1.2.0 (c (n "qoqo-macros") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0a68dpbln9pnd86k1blr8j7gsbd6znn4npq6m7fxwz1gmwgwprsb") (r "1.56")))

(define-public crate-qoqo-macros-1.2.1 (c (n "qoqo-macros") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "19ynrwpz0v8z77zjkllh84fvkqkc8bcdbmigmm6l6gz3rblqh5ij") (r "1.56")))

(define-public crate-qoqo-macros-1.2.2 (c (n "qoqo-macros") (v "1.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1sc415bz8z5airnjbsh3v0kngpdq36gfdfjy6fal4fx6al5dnsaq") (r "1.56")))

(define-public crate-qoqo-macros-1.2.3 (c (n "qoqo-macros") (v "1.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1nd3piv9lx1g4vmzv16y90a547fajn8mzwp5v5dj9f90c4qxv5a0") (r "1.56")))

(define-public crate-qoqo-macros-1.2.4 (c (n "qoqo-macros") (v "1.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0svhmqbwzs1ij9hj4zzhbpl0kgpw2hbpjm2b1q8lf1kag18hmixj") (r "1.56")))

(define-public crate-qoqo-macros-1.2.5 (c (n "qoqo-macros") (v "1.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1vksvpab4n46c7808fqj3k0f13mz4nv4zinr3cyhp13fmmcnsvbw") (r "1.56")))

(define-public crate-qoqo-macros-1.3.0 (c (n "qoqo-macros") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1f57kvpqhm4sakjsi1jh7mh3ba7wj4b3c6hfa6awnplgm1rdi75g") (r "1.56")))

(define-public crate-qoqo-macros-1.3.1 (c (n "qoqo-macros") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "18d3by3ai4lxmdv4613268shr1pjvi56y7ynmk4h4sdkml58z85f") (r "1.56")))

(define-public crate-qoqo-macros-1.3.2 (c (n "qoqo-macros") (v "1.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0vpskjngc7b4my1qsll1cbi6lbq937v62hmwlxfzjabxxyvzwm0x") (r "1.56")))

(define-public crate-qoqo-macros-1.4.0 (c (n "qoqo-macros") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "14p4a3ll4m123b80wz5brv5lijwc5qci95rldlfsjmma9fn4j86r") (r "1.56")))

(define-public crate-qoqo-macros-1.5.0 (c (n "qoqo-macros") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0q979zij2h92najahsc4kmzwflqczv74cs1diqzcnika2rv1nh83") (r "1.56")))

(define-public crate-qoqo-macros-1.6.0-alpha.0 (c (n "qoqo-macros") (v "1.6.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1za4pfjk8yw5q2r2mzy8dpsx02lv9fksb8id85rbli7sb8acpv6c") (r "1.56")))

(define-public crate-qoqo-macros-1.5.1 (c (n "qoqo-macros") (v "1.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0s8dmi2pilnva1vafk7v1bkay4v23p9idcj0azgz8xhwmvyg4p5a") (r "1.56")))

(define-public crate-qoqo-macros-1.6.0-alpha.1 (c (n "qoqo-macros") (v "1.6.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1b33ppfi0hcs48c2z4hik3r74ydzqgv6dblw2fgn6lvpzn1bssjs") (r "1.56")))

(define-public crate-qoqo-macros-1.6.0-alpha.2 (c (n "qoqo-macros") (v "1.6.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0cagxrk8qzrna48dfk689p0ix1cmmf3cnba071278m7v1jfckxwq") (r "1.56")))

(define-public crate-qoqo-macros-1.6.0 (c (n "qoqo-macros") (v "1.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0a5zrifp1hw8wivr69cicnh8z0vwp084cy70mgnn0mwrq8k3q4zp") (r "1.56")))

(define-public crate-qoqo-macros-1.6.1 (c (n "qoqo-macros") (v "1.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1xkszad5jsiz6la0nndjn1nq4h5lp56sbfjgwn75kq2n0bqgsjq6") (r "1.56")))

(define-public crate-qoqo-macros-1.6.2 (c (n "qoqo-macros") (v "1.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0vhx6gqyz9kmw7wn7gsq6qsj1fhfvhs2r047siffjrhb75q65zg0") (r "1.56")))

(define-public crate-qoqo-macros-1.7.0 (c (n "qoqo-macros") (v "1.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0by6vgamnrl8pgn2q2y4kqg8sliyy9vkvvkl3lwabdbn6bh727zk") (r "1.56")))

(define-public crate-qoqo-macros-1.7.1 (c (n "qoqo-macros") (v "1.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1iicbmc4dp1h0yk6lpk3k0fxcs54rbngd6zdx451ysd5k7r1wdp0") (r "1.56")))

(define-public crate-qoqo-macros-1.8.0-alpha.0 (c (n "qoqo-macros") (v "1.8.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0v4a6wr0yd7ig3aikqyw0zpmpxqhj56kk08fwi3432a17mvxnwgj") (r "1.56")))

(define-public crate-qoqo-macros-1.8.0-alpha.1 (c (n "qoqo-macros") (v "1.8.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "114wsl5fapvp56dya3zsk97khd056s3xv802bwib0p5r3y5gs2m5") (r "1.56")))

(define-public crate-qoqo-macros-1.8.0-alpha.2 (c (n "qoqo-macros") (v "1.8.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0djpyf95yy72h8k3nghxd2665zchpkri555vkli4vsp1xc2w5j55") (r "1.56")))

(define-public crate-qoqo-macros-1.8.0 (c (n "qoqo-macros") (v "1.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0xhvq5mk5g2smw0ls8xrcbckwhf062wp1n89j4m2hnjfhzhpysix") (r "1.56")))

(define-public crate-qoqo-macros-1.9.0 (c (n "qoqo-macros") (v "1.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1kkb7gcgnp34wik17y5319mi56mdcibbdfi5zw3w21w5x4ycim2z") (r "1.56")))

(define-public crate-qoqo-macros-1.9.1 (c (n "qoqo-macros") (v "1.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1ad66x2dbck0a8rv88i5jvkzj802api8nppxw2k8z5ilshq7v89h") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

(define-public crate-qoqo-macros-1.10.0 (c (n "qoqo-macros") (v "1.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "18glb519v9ifh5i0pjychwg0h31qz83glgn66h6g1yv1q4yhb657") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

(define-public crate-qoqo-macros-1.11.0-alpha.0 (c (n "qoqo-macros") (v "1.11.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struqture") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0xidkm5ii1kppsyw72bi7d2h2ym41sympq6q9yz8c4v5ymq1qg71") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

(define-public crate-qoqo-macros-1.11.0-alpha.1 (c (n "qoqo-macros") (v "1.11.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struqture") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0akay6zh1wfk0aizc4dc27dpv2psvnc07waqfb0792mq5lzv9h1k") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

(define-public crate-qoqo-macros-1.11.0-alpha.2 (c (n "qoqo-macros") (v "1.11.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struqture") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "01571nbr32hq6wm9cb4vw0x3i7hq1a6vydqzgr5b15rc5jl0kvk1") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

(define-public crate-qoqo-macros-1.11.0 (c (n "qoqo-macros") (v "1.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struqture") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0w6w19nyp72dciwv560s6675hf5q8miyhasd14l0wvqhnyw5fhjb") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

(define-public crate-qoqo-macros-1.12.0 (c (n "qoqo-macros") (v "1.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struqture") (r "^1.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "018bqm7vdp35z5dhdrsd35in3s8ax39sgh13rad3mlvkc7fimc53") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

(define-public crate-qoqo-macros-1.11.1 (c (n "qoqo-macros") (v "1.11.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struqture") (r "~1.6") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1sf61d8ig2njc5vs1kh9c721b0rlwd7bwcw29vvhhh2d4xyvpspv") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

(define-public crate-qoqo-macros-1.10.1 (c (n "qoqo-macros") (v "1.10.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0rp9y78smi5ilqp9kfqpb9zc62sawy2dpyybr613jdj7ka14hs64") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

(define-public crate-qoqo-macros-1.9.2 (c (n "qoqo-macros") (v "1.9.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "055a15rdsrq9kjy3vzbf9vsg0gzsy02gjjskxzbn6q56c6p2wf1d") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

(define-public crate-qoqo-macros-1.12.1 (c (n "qoqo-macros") (v "1.12.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struqture") (r "^1.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1f8j91q1y3p5f036fwvzkz3s7ny006v73g9cpaiv8xdbm18vlplv") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

(define-public crate-qoqo-macros-1.13.0 (c (n "qoqo-macros") (v "1.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struqture") (r "^1.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "10ija4l2rpxv7xdd7g2apyd505km5bkvmppmwr921wsb78p9lnmx") (f (quote (("unstable_chain_with_environment")))) (r "1.56")))

