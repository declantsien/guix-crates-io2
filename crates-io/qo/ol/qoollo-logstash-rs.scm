(define-module (crates-io qo ol qoollo-logstash-rs) #:use-module (crates-io))

(define-public crate-qoollo-logstash-rs-0.2.0 (c (n "qoollo-logstash-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls-crate") (r "^0.20") (o #t) (d #t) (k 0) (p "rustls")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (o #t) (d #t) (k 0)))) (h "0cc9xrgfihabgkq93pc6v5g909nfkkqk207wji5g0883pq7x5cf1") (f (quote (("tls" "native-tls") ("rustls" "rustls-crate" "webpki-roots") ("default"))))))

