(define-module (crates-io qo ol qool) #:use-module (crates-io))

(define-public crate-qool-0.1.0 (c (n "qool") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "fmtlog") (r "^0.1") (d #t) (k 0)) (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "qr2term") (r "^0.2") (d #t) (k 0)) (d (n "staticfile") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1x917wvclan0wd1rj7k4x5dj35xa649jp8caha9wg6n9xi2l2nwb")))

