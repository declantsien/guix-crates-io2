(define-module (crates-io qo i_ qoi_rs) #:use-module (crates-io))

(define-public crate-qoi_rs-0.1.0 (c (n "qoi_rs") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 2)))) (h "062s01516f8545af5s4sa3argqapw1fs8fnxrv42dn6p88xc0ri3")))

(define-public crate-qoi_rs-0.1.1 (c (n "qoi_rs") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 2)))) (h "0sannrmwcdgz21bq64rv9kxrji25x1bj56v9ba337v4c5bql9b7p")))

