(define-module (crates-io qo ic qoiconv-rs) #:use-module (crates-io))

(define-public crate-qoiconv-rs-0.5.1 (c (n "qoiconv-rs") (v "0.5.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "0k5mng6vdfk62b05a8gwivygy98x73bjl4hgzbdqf80miczljckf")))

