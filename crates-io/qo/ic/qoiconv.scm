(define-module (crates-io qo ic qoiconv) #:use-module (crates-io))

(define-public crate-qoiconv-0.1.0 (c (n "qoiconv") (v "0.1.0") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "libqoi") (r "^0.2.1") (d #t) (k 0)))) (h "00g53hh62zz28sjkjj2j655smn6ir0q6pfs6pkkk2s59gcnimcbm")))

