(define-module (crates-io qo l- qol-rs) #:use-module (crates-io))

(define-public crate-qol-rs-0.1.0 (c (n "qol-rs") (v "0.1.0") (h "1ajgr7f1mybpbphdlyw9mscn7qqspagqc7ysa83yrhb3ip16gm48") (f (quote (("type_aliases") ("sae") ("macros") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases") ("default" "full")))) (y #t)))

(define-public crate-qol-rs-0.1.1 (c (n "qol-rs") (v "0.1.1") (h "1mxnfh22aadjmvch7rj74xkljxazszdkc1v18klcm257yjm500z3") (f (quote (("type_aliases") ("sae") ("macros") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases") ("default" "full")))) (y #t)))

(define-public crate-qol-rs-0.1.2 (c (n "qol-rs") (v "0.1.2") (h "04w9cfyyfcqb2xvync20wm2vizzgzhcykia7qgx76rvj0689ahav") (f (quote (("type_aliases") ("sae") ("macros") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases") ("default" "full")))) (y #t)))

(define-public crate-qol-rs-0.1.21 (c (n "qol-rs") (v "0.1.21") (h "140rm8qsis0akgw8shw619qaxw475f2jrd9cmw2z9ap7b263pzvv") (f (quote (("type_aliases") ("sae") ("macros") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases") ("default" "full")))) (y #t)))

(define-public crate-qol-rs-0.1.3 (c (n "qol-rs") (v "0.1.3") (h "1icjk9iaylwv4svrvim9f1qz5vvj4abl3zyirwdsc89wp1j773cd") (f (quote (("type_aliases") ("sae") ("macros") ("generics") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases" "generics") ("default" "full")))) (y #t)))

(define-public crate-qol-rs-0.1.4 (c (n "qol-rs") (v "0.1.4") (h "0zlrj5p1q749wflfvl67265628v36dy99snrlsdfwfn0qs9ay2nk") (f (quote (("type_aliases") ("sae") ("macros") ("generics") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases" "generics") ("default" "full")))) (y #t)))

(define-public crate-qol-rs-0.1.5 (c (n "qol-rs") (v "0.1.5") (h "1zi8q87c1qm956c05rgcklllf94zli7xlkacp8mg09vzg81qv3wk") (y #t)))

(define-public crate-qol-rs-0.1.41 (c (n "qol-rs") (v "0.1.41") (h "0pswq7v8asa79jja3srgwc0yqa2xxzvdfvhmlf9hxvxk0f6c69s7") (f (quote (("type_aliases") ("sae") ("macros") ("generics") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases" "generics") ("default" "full")))) (y #t)))

