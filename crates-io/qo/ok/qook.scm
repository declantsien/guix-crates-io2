(define-module (crates-io qo ok qook) #:use-module (crates-io))

(define-public crate-qook-0.1.0 (c (n "qook") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0l4h5wrhmjn70f57vbixkc25z5vmb7n6lbkh3d0g5v6k3g6rph2a") (l "qrack_invoke")))

(define-public crate-qook-0.1.1 (c (n "qook") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "080manjhpqkn5cl3b58xrzaqm7hn8kvcvb4qrhypn93szrahb4b4") (l "qrack_invoke")))

(define-public crate-qook-0.1.2 (c (n "qook") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "01p2lnfaw68bxxd33gv76ww52zm13ppbcsjkj4pj3b86jgqm2n17") (l "qrack_invoke")))

(define-public crate-qook-0.1.3 (c (n "qook") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1w3b59vzk2pwcnd68hz07gh35dz7sc2w4qg9fs68zhb72i1cm7cc") (l "qrack_invoke")))

(define-public crate-qook-0.2.0 (c (n "qook") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "09vw49d5v79mmacy3pssk983a6sjdf1vc66k3dmarcqzl8gqgyri") (l "qrack_invoke")))

(define-public crate-qook-0.3.0 (c (n "qook") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "11c6i43hg1nzrkmdsa4g2cx45xq5c1sdidn68v984bhzszfz1h9f") (l "qrack_invoke")))

(define-public crate-qook-0.4.0 (c (n "qook") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1rry6ffs8q3dc1knsgy0lq36sjm50qcx4hly7ydm5g7mxyq82hpg") (l "qrack_invoke")))

(define-public crate-qook-0.5.0 (c (n "qook") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1jlrvr4q78892pwxc72l4bdifcsbhibd3knbdbww2gb6cwrlpwy7") (l "qrack_invoke")))

(define-public crate-qook-0.6.0 (c (n "qook") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0v80n9d8fdhsp3cq72lxs4vbfamd6kdpcdi233rb7y56mx63k2ly") (l "qrack_invoke")))

(define-public crate-qook-0.7.0 (c (n "qook") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0ziy1m2gk176pj1p98dvqxil3rpxzzpm5irp05yn7xci73z2j9gp") (l "qrack_invoke")))

(define-public crate-qook-0.8.0 (c (n "qook") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1g835dn2ds2gslchlfk4dd98fpd4dbivf1cn2nnd73fa7gc3kl0q") (l "qrack_invoke")))

(define-public crate-qook-0.9.0 (c (n "qook") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0gj6ccdfdl31i20fs4m4aj2js2x9lalpgz8lsnaa78m68bqi7sc7") (l "qrack_invoke")))

(define-public crate-qook-0.10.0 (c (n "qook") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "1a32r080vhqc950h4pfx5xflxfxgv9aqvnx9dgwwx6nk9g8mnvb1") (l "qrack_pinvoke")))

