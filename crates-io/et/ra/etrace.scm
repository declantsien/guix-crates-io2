(define-module (crates-io et ra etrace) #:use-module (crates-io))

(define-public crate-etrace-0.2.7 (c (n "etrace") (v "0.2.7") (h "0r5mlg1iwx3258b81p9ydair2ihl3nwv6bfiwaq2mgaxijwm604r")))

(define-public crate-etrace-0.2.8 (c (n "etrace") (v "0.2.8") (h "1jnqjvvrqcgryjgph2rf40z3my69fa02x0da7k18rs779adv8gjs")))

(define-public crate-etrace-1.0.0 (c (n "etrace") (v "1.0.0") (h "0azafbwz8zwmjymfyf649gbnik50wgkp6vw6i2f0r3yj9vaw3ihg")))

(define-public crate-etrace-1.1.0 (c (n "etrace") (v "1.1.0") (h "18fxjk8z1vyfsszlkhn65628498fiyrsv4g8ckhdfbjmsvfjax0f")))

(define-public crate-etrace-1.1.1 (c (n "etrace") (v "1.1.1") (h "0h3gylk3k30zqac1ss3k070qnlarqbv171cvh3p4cw50ivk12wzi")))

