(define-module (crates-io et f_ etf_balancer) #:use-module (crates-io))

(define-public crate-etf_balancer-1.0.0 (c (n "etf_balancer") (v "1.0.0") (d (list (d (n "actix-web") (r "^0.6.10") (d #t) (k 0)) (d (n "cute") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)) (d (n "streaming-stats") (r "^0.2") (d #t) (k 0)))) (h "0sphwlil2ry2yiycikg5ih1qkm8c3n4xbnsm21qrwwrbpysgw0zq")))

(define-public crate-etf_balancer-1.1.0 (c (n "etf_balancer") (v "1.1.0") (d (list (d (n "actix-rt") (r "^1") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.0-alpha.3") (d #t) (k 0)) (d (n "cute") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)) (d (n "streaming-stats") (r "^0.2") (d #t) (k 0)))) (h "1cc18y17lxv9pz9sxnpy8xl55172dv5hswlx0qllx631qj6nc9q0")))

