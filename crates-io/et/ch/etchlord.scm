(define-module (crates-io et ch etchlord) #:use-module (crates-io))

(define-public crate-etchlord-0.1.0 (c (n "etchlord") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "005imjw31f2c0i4aqy1gsb198849fadib8wxvygkz9p52bwhn7a4")))

