(define-module (crates-io et ch etch) #:use-module (crates-io))

(define-public crate-etch-0.1.1 (c (n "etch") (v "0.1.1") (d (list (d (n "glue") (r "^0.6") (d #t) (k 0)) (d (n "syntect") (r "^3") (d #t) (k 0)))) (h "0mp0pb07pwvk3br80knd6xfk56dj8rkzw555lr0ifvc00swk03mf")))

(define-public crate-etch-0.2.1 (c (n "etch") (v "0.2.1") (d (list (d (n "glue") (r "^0.6") (d #t) (k 0)) (d (n "syntect") (r "^3") (o #t) (d #t) (k 0)))) (h "16n57ixlvxa72n39165ssawxbs9c6042pk6s8rczyb4a80s5vq0g") (f (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

(define-public crate-etch-0.3.1 (c (n "etch") (v "0.3.1") (d (list (d (n "glue") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syntect") (r "^3") (o #t) (d #t) (k 0)))) (h "1v567rj8f5knqxlyynsmbmp3p327fhnzgz6ws186dp2x92l6rsig") (f (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

(define-public crate-etch-0.4.1 (c (n "etch") (v "0.4.1") (d (list (d (n "bork") (r "^0.2") (d #t) (k 0)) (d (n "glue") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syntect") (r "^3") (o #t) (d #t) (k 0)))) (h "0nswm7jzg30a7qd3n56h8jfiqagilxn7klsy5c7cvpdq71nsixnw") (f (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

(define-public crate-etch-0.1.2 (c (n "etch") (v "0.1.2") (d (list (d (n "glue") (r "^0.6") (d #t) (k 0)) (d (n "syntect") (r "^3") (d #t) (k 0)))) (h "174z43kks3mf7pr4c2kv4i6kb22vkmaxr3n7mvh6lqzibn64z0bc")))

(define-public crate-etch-0.2.2 (c (n "etch") (v "0.2.2") (d (list (d (n "glue") (r "^0.6") (d #t) (k 0)) (d (n "syntect") (r "^3") (o #t) (d #t) (k 0)))) (h "05wlfihhah9zq49dihcp5pqxqg20659qxbwjcrlfijm4rixq84ph") (f (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

(define-public crate-etch-0.3.2 (c (n "etch") (v "0.3.2") (d (list (d (n "glue") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syntect") (r "^3") (o #t) (d #t) (k 0)))) (h "0cfhb1jgkdc092k4fdh8wi8kmmdfr91r22aczwl7zawlzf42187h") (f (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

(define-public crate-etch-0.4.2 (c (n "etch") (v "0.4.2") (d (list (d (n "bork") (r "^0.2") (d #t) (k 0)) (d (n "glue") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syntect") (r "^3") (o #t) (d #t) (k 0)))) (h "044hwrd01v1yjj8djpk3c01jbjsc0z039kkcf60980wklbvm9fdy") (f (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

