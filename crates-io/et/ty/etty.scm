(define-module (crates-io et ty etty) #:use-module (crates-io))

(define-public crate-etty-0.0.0 (c (n "etty") (v "0.0.0") (d (list (d (n "crossbeam") (r "^0.8.2") (f (quote ("crossbeam-channel"))) (d #t) (k 0)) (d (n "etty-macros") (r "^0.0.0") (d #t) (k 0)) (d (n "itoa") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("sync"))) (d #t) (k 0)))) (h "0f3zmx2zcrpayyybmjrn1mm379izr0jzbqlgjm3cv8zl90ccflbj")))

