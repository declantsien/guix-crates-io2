(define-module (crates-io et ty etty-macros) #:use-module (crates-io))

(define-public crate-etty-macros-0.0.0 (c (n "etty-macros") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0vc7gps1sa424ayikxcdbvnp1xg8pzjdfcbpcqd3z3ah6zf9z73n")))

