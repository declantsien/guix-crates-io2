(define-module (crates-io et hn ethnum-intrinsics) #:use-module (crates-io))

(define-public crate-ethnum-intrinsics-1.0.0 (c (n "ethnum-intrinsics") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.55") (d #t) (k 1)))) (h "1zjdqql6p8kwikwp0jw2jmsgfckgcl28ydz8ksk5ifrg2xlmm4yg") (l "ethnum")))

(define-public crate-ethnum-intrinsics-1.0.1 (c (n "ethnum-intrinsics") (v "1.0.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "1vjndp4wcrzsn597z2qdhbhsq4krhzy8g36cnqwhj07qaxmamrl7") (l "ethnum")))

(define-public crate-ethnum-intrinsics-1.1.0 (c (n "ethnum-intrinsics") (v "1.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0q54arblnkanigjxynl52zn4rhss7drx5hn9vsm93glc1742qnc2") (l "ethnum")))

(define-public crate-ethnum-intrinsics-1.2.0 (c (n "ethnum-intrinsics") (v "1.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1qvb1r3vmnk5nplz6x1014rn6b9nfnig2qmlj8hi3jpq75j8cgh9") (l "ethnum")))

