(define-module (crates-io et hn ethnum) #:use-module (crates-io))

(define-public crate-ethnum-1.0.0 (c (n "ethnum") (v "1.0.0") (d (list (d (n "ethnum-intrinsics") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0ibg50fx3fp09iicvnpiqj2kccr0bdifwil3fz3z94nxzciffrf1") (f (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.0.1 (c (n "ethnum") (v "1.0.1") (d (list (d (n "ethnum-intrinsics") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0zvzlnj4wqjrrq3z78jsy5xbcczy08hd2z3ycd87m209x5yabhbm") (f (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.0.2 (c (n "ethnum") (v "1.0.2") (d (list (d (n "ethnum-intrinsics") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0gp08ywpj5x7f10ad587nphq58wmzz282kxqkyvhyzqkivhq8fci") (f (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.0.3 (c (n "ethnum") (v "1.0.3") (d (list (d (n "ethnum-intrinsics") (r "^1") (o #t) (d #t) (k 0)))) (h "12p7f256qavq62844j2vqns2098ac771k7k4aan2bvzrw6qwczc8") (f (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.0.4 (c (n "ethnum") (v "1.0.4") (d (list (d (n "ethnum-intrinsics") (r "^1") (o #t) (d #t) (k 0)))) (h "08j8dv8zmmihrp17gdvpqlsc8rpw9r83ydqznm6g3j0zd8cxln0g") (f (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.1.0 (c (n "ethnum") (v "1.1.0") (d (list (d (n "ethnum-intrinsics") (r "^1") (o #t) (d #t) (k 0)))) (h "1fhad33a8pyps96dmzzb9w5wssbi6sw1ff3rqlcy3wqjp2k5nil0") (f (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.1.1 (c (n "ethnum") (v "1.1.1") (d (list (d (n "ethnum-intrinsics") (r "^1") (o #t) (d #t) (k 0)))) (h "1liylhsw0kb1fidsbnznvpd1cd2h90fc8rcpxggv94mdvi3h7d33") (f (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.2.0 (c (n "ethnum") (v "1.2.0") (d (list (d (n "ethnum-intrinsics") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "11jxz7jvbf6pdf2pm3z9d5dr0yhgbm2dif39n51x64gfncxxjqn3") (f (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.2.1 (c (n "ethnum") (v "1.2.1") (d (list (d (n "ethnum-intrinsics") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0iycphsq0r2d508y3m8r8wr5l3q1igzwnq40d0mh9m8bfhsfmx7n") (f (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.2.2 (c (n "ethnum") (v "1.2.2") (d (list (d (n "ethnum-intrinsics") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1lwa4h1rj3dnrmqm1b059cx73c9gs9s79pdlivnnmvg9vnvsgr47") (f (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.3.0 (c (n "ethnum") (v "1.3.0") (d (list (d (n "ethnum-intrinsics") (r "^1") (o #t) (d #t) (k 0)) (d (n "ethnum-macros") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "049k6y7ndvbgxypra9vdlq1d24127sxc0hmvbqjpbsx6kw5krb1f") (f (quote (("macros" "ethnum-macros") ("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.3.1 (c (n "ethnum") (v "1.3.1") (d (list (d (n "ethnum-intrinsics") (r "^1") (o #t) (d #t) (k 0)) (d (n "ethnum-macros") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1vfi5pcqiq906ixi8mr0v0xqs8v7knks82dni25b8dwgpivyfzbd") (f (quote (("macros" "ethnum-macros") ("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.3.2 (c (n "ethnum") (v "1.3.2") (d (list (d (n "ethnum-intrinsics") (r "=1.1.0") (o #t) (d #t) (k 0)) (d (n "ethnum-macros") (r "=1.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "010w5k5ri2k5500igvp3zb48z0sfjwfb5jvsvkg303wf0z8bk601") (f (quote (("macros" "ethnum-macros") ("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.4.0 (c (n "ethnum") (v "1.4.0") (d (list (d (n "ethnum-intrinsics") (r "=1.2.0") (o #t) (d #t) (k 0)) (d (n "ethnum-macros") (r "=1.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1b5d4j4al7f40sq717bbrkiifgxvbgyfpvh6zfvpylpsna1g73vc") (f (quote (("macros" "ethnum-macros") ("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1.5.0 (c (n "ethnum") (v "1.5.0") (d (list (d (n "ethnum-intrinsics") (r "=1.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0b68ngvisb0d40vc6h30zlhghbb3mc8wlxjbf8gnmavk1dca435r") (f (quote (("macros") ("llvm-intrinsics" "ethnum-intrinsics"))))))

