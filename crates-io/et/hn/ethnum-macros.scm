(define-module (crates-io et hn ethnum-macros) #:use-module (crates-io))

(define-public crate-ethnum-macros-1.0.0 (c (n "ethnum-macros") (v "1.0.0") (h "0wn53nxc5sfb93x9dqs3zzg1qz99wm9lnb1mhljp7z8b0shlsiaj")))

(define-public crate-ethnum-macros-1.1.0 (c (n "ethnum-macros") (v "1.1.0") (h "0haq14b9jzs7qxi7mj6d7vx80rmd62b1jsmldfa2z786mc8vlsds")))

