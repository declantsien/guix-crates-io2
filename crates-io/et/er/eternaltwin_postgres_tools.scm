(define-module (crates-io et er eternaltwin_postgres_tools) #:use-module (crates-io))

(define-public crate-eternaltwin_postgres_tools-0.13.0 (c (n "eternaltwin_postgres_tools") (v "0.13.0") (h "0imsbcxr45dalzx4ycj76pg93m82vpvssa1vdwxabqj21dwc9yrp") (r "1.61.0")))

(define-public crate-eternaltwin_postgres_tools-0.14.0 (c (n "eternaltwin_postgres_tools") (v "0.14.0") (h "1ywlv8hwzilpzhs7d3wjylgl7ng3f8m91zk684mgc3k5hlak7v15") (r "1.77.0")))

(define-public crate-eternaltwin_postgres_tools-0.14.2 (c (n "eternaltwin_postgres_tools") (v "0.14.2") (h "1p197ldkhlg1a78m0f4sjv8hzg9vaybhjqn23mg5xdmq0vaic1px") (r "1.77.0")))

