(define-module (crates-io et er eternaltwin_mt_dns) #:use-module (crates-io))

(define-public crate-eternaltwin_mt_dns-0.13.0 (c (n "eternaltwin_mt_dns") (v "0.13.0") (d (list (d (n "eternaltwin_core") (r "^0.13.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "neon") (r "^0.10.1") (f (quote ("napi-6"))) (o #t) (k 0)))) (h "1jhqi8m7jx9pjnaw4n7ais4qwq9fz8m59nik9lhai5q85slzz84s") (r "1.61.0")))

(define-public crate-eternaltwin_mt_dns-0.14.0 (c (n "eternaltwin_mt_dns") (v "0.14.0") (d (list (d (n "eternaltwin_core") (r "^0.14.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "neon") (r "^0.10.1") (f (quote ("napi-6"))) (o #t) (k 0)))) (h "04c6g9ngiqd53v4pnb0hy5iawddq3bkkmymhxyl6lslhchdinc10") (r "1.77.0")))

(define-public crate-eternaltwin_mt_dns-0.14.2 (c (n "eternaltwin_mt_dns") (v "0.14.2") (d (list (d (n "eternaltwin_core") (r "^0.14.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "neon") (r "^0.10.1") (f (quote ("napi-6"))) (o #t) (k 0)))) (h "0w76apq43qk4cmg2kcsdglvj2kw1i5jsif1s33gq199shkkpfn96") (r "1.77.0")))

