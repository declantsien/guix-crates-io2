(define-module (crates-io et er eternaltwin_app) #:use-module (crates-io))

(define-public crate-eternaltwin_app-0.13.0 (c (n "eternaltwin_app") (v "0.13.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0nm82bq099pbarmp7r5m117xbm92gjpbpas22lbd6pjdkxiam17d") (r "1.61.0")))

(define-public crate-eternaltwin_app-0.14.0 (c (n "eternaltwin_app") (v "0.14.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "148s6pmbq3yj1zffq9px60hckd4hzw91acrp7i9j4f2yw2908jd2") (r "1.77.0")))

(define-public crate-eternaltwin_app-0.14.2 (c (n "eternaltwin_app") (v "0.14.2") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1771skr4d5hk41bi2an78cim0giylqx8lnn6r54ilsb25mkg2dlr") (r "1.77.0")))

