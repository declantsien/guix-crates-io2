(define-module (crates-io et er eternity) #:use-module (crates-io))

(define-public crate-eternity-0.1.0 (c (n "eternity") (v "0.1.0") (d (list (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "strum") (r "^0.20") (d #t) (k 2)) (d (n "strum_macros") (r "^0.20") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (k 2)) (d (n "tokio_compat") (r "^0.2") (f (quote ("time" "sync"))) (o #t) (k 0) (p "tokio")))) (h "08234sw5k3g0bh580vjvk68h6xp6sbjp0kl0jsxdzj34gm2a0yfx") (f (quote (("tokio_0_2" "tokio_compat") ("default" "tokio" "cache") ("cache"))))))

