(define-module (crates-io et er eternity-rs) #:use-module (crates-io))

(define-public crate-eternity-rs-0.1.0 (c (n "eternity-rs") (v "0.1.0") (h "12g02la1mnrirv5g7cydhc4w9gh25kb0y8aavsr0z5nd0a1ays5g")))

(define-public crate-eternity-rs-0.2.0 (c (n "eternity-rs") (v "0.2.0") (h "1y3fifxla1ay7jrlq75k14m78pffcbca29piv5hg56cw02ac5m10")))

