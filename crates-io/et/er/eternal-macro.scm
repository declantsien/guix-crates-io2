(define-module (crates-io et er eternal-macro) #:use-module (crates-io))

(define-public crate-eternal-macro-0.1.0 (c (n "eternal-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "09l7y9pj1g5bnpcbpxw9fg2133qxapqc4br2xf08r5siza3hxqrj")))

(define-public crate-eternal-macro-0.2.0 (c (n "eternal-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0b3snh643arxrb4387paik3br98gpbcy3b0clrwm3p0m414xnnjv")))

(define-public crate-eternal-macro-0.2.1 (c (n "eternal-macro") (v "0.2.1") (d (list (d (n "eternal") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "183sjqywm5mp6xw7ad5i5cm9a0rwa1mmak7z2x1j7j1dqniz3w17")))

