(define-module (crates-io et er eternaltwin_constants) #:use-module (crates-io))

(define-public crate-eternaltwin_constants-0.13.0 (c (n "eternaltwin_constants") (v "0.13.0") (d (list (d (n "eternaltwin_core") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0ijjy1icmrf0jixhn2zb6swf02vjflzl3w98zjdihf58q499g8g2") (r "1.61.0")))

(define-public crate-eternaltwin_constants-0.14.0 (c (n "eternaltwin_constants") (v "0.14.0") (d (list (d (n "eternaltwin_core") (r "^0.14.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "17n6bl8w9khmw0yv22g7qqjsla97zx3yrrz1iv00kadkjg8yj8wm") (r "1.77.0")))

(define-public crate-eternaltwin_constants-0.14.2 (c (n "eternaltwin_constants") (v "0.14.2") (d (list (d (n "eternaltwin_core") (r "^0.14.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0myh0b0z4bfnbf5a0kbddfnnh0xvnb6z0a497m8zd8x2q5pivi38") (r "1.77.0")))

