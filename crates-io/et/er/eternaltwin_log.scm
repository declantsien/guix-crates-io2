(define-module (crates-io et er eternaltwin_log) #:use-module (crates-io))

(define-public crate-eternaltwin_log-0.13.0 (c (n "eternaltwin_log") (v "0.13.0") (h "0w3c3dgy0lnvfxr08j0zr5h5xy6jhgbiw4yc3lrnym5pxic20qh2") (r "1.61.0")))

(define-public crate-eternaltwin_log-0.14.0 (c (n "eternaltwin_log") (v "0.14.0") (h "1p53ax22g8azvhi2lyzvxznmg3i7a6k8288l5d3h4ww7aj74kaff") (r "1.77.0")))

(define-public crate-eternaltwin_log-0.14.2 (c (n "eternaltwin_log") (v "0.14.2") (h "1iqp5lgxnh12rndihg30ck1vxpfhamg56bsmkpzifh9xxkxv491a") (r "1.77.0")))

