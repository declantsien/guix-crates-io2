(define-module (crates-io et er eternal_iterator) #:use-module (crates-io))

(define-public crate-eternal_iterator-0.1.0 (c (n "eternal_iterator") (v "0.1.0") (h "0a6k3pm0l154iq15977jr8zbn0x5cmmnp56dvyssdnmsn0lcipkc")))

(define-public crate-eternal_iterator-0.1.1 (c (n "eternal_iterator") (v "0.1.1") (h "0gkjw2r2ixghi11mg8wfkhl5wr1g2adr9hyd64czxwald44qnd7y")))

(define-public crate-eternal_iterator-0.1.2 (c (n "eternal_iterator") (v "0.1.2") (h "0pmvr6jx4rl84fwl3m5casafv489jvdn6p014krk2sabxc81jjmy")))

(define-public crate-eternal_iterator-0.1.3 (c (n "eternal_iterator") (v "0.1.3") (h "0na44hchv4wlq75icz81ijjcq4h764km3is4636m6vbgv01h49z2")))

(define-public crate-eternal_iterator-0.1.4 (c (n "eternal_iterator") (v "0.1.4") (h "1pfyi6gpmm798cj5lklz48lhrml8ad89g102rk2nnnm9a8g8sk1i") (r "1.54")))

