(define-module (crates-io et er eternalfest_config) #:use-module (crates-io))

(define-public crate-eternalfest_config-0.16.0 (c (n "eternalfest_config") (v "0.16.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0hz8mhhw4b4db50f2ngh5s4ha93d08mr79gv8yx6sqvm6zwjy33m")))

(define-public crate-eternalfest_config-0.17.0 (c (n "eternalfest_config") (v "0.17.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1aqa13j0d0gc2jwnf18qxcdvyk0zv39r40h6djmxzzl60m25325k")))

(define-public crate-eternalfest_config-0.18.0 (c (n "eternalfest_config") (v "0.18.0") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "060qh963y3qaylj6g71p3ra9zvygpha2s6r5kv9cmyrm3p12d8nc")))

(define-public crate-eternalfest_config-0.18.2 (c (n "eternalfest_config") (v "0.18.2") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0h1jiihmgs7azjmsk2k6zy8inyz2w7ixhdpm01js77dx5sp4bwzr")))

