(define-module (crates-io et er eternals_password_generator) #:use-module (crates-io))

(define-public crate-eternals_password_generator-0.1.0 (c (n "eternals_password_generator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "14bdcrd3nd161649b5wprrp8bkrd4nzmnvbfsl85gczs5ms2asd4")))

(define-public crate-eternals_password_generator-0.1.1 (c (n "eternals_password_generator") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1xvcfp8pwf2wyrhxmxi9ha45v5dfzf0cy1d3s33886fgkim44v7n")))

