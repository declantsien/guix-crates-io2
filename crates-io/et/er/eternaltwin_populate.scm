(define-module (crates-io et er eternaltwin_populate) #:use-module (crates-io))

(define-public crate-eternaltwin_populate-0.13.0 (c (n "eternaltwin_populate") (v "0.13.0") (d (list (d (n "eternaltwin_constants") (r "^0.13.0") (d #t) (k 0)) (d (n "eternaltwin_core") (r "^0.13.0") (f (quote ("sqlx"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.6.2") (f (quote ("macros" "chrono" "offline" "postgres" "runtime-tokio-rustls" "uuid"))) (k 0)))) (h "0k5bw2jqifgxb6p49k5lhb8vw98d3cmwdw1fy5xrk36mach95hjx") (r "1.61.0")))

(define-public crate-eternaltwin_populate-0.14.0 (c (n "eternaltwin_populate") (v "0.14.0") (d (list (d (n "eternaltwin_constants") (r "^0.14.0") (d #t) (k 0)) (d (n "eternaltwin_core") (r "^0.14.0") (f (quote ("sqlx"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("macros" "chrono" "postgres" "runtime-tokio-rustls" "uuid"))) (k 0)))) (h "0zbmmjqm74k2vnfx57zvylkyybfp47y0jhjan91q3kx1jmrxgfkb") (r "1.77.0")))

(define-public crate-eternaltwin_populate-0.14.2 (c (n "eternaltwin_populate") (v "0.14.2") (d (list (d (n "eternaltwin_constants") (r "^0.14.2") (d #t) (k 0)) (d (n "eternaltwin_core") (r "^0.14.2") (f (quote ("sqlx"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("macros" "chrono" "postgres" "runtime-tokio-rustls" "uuid"))) (k 0)))) (h "0z0dsmpxk3g0kla074x05d65ajaavazsfnihp0iji93x7i3f7wzf") (r "1.77.0")))

