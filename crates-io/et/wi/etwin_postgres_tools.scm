(define-module (crates-io et wi etwin_postgres_tools) #:use-module (crates-io))

(define-public crate-etwin_postgres_tools-0.1.0 (c (n "etwin_postgres_tools") (v "0.1.0") (h "0jqqqx1lcfqpj81h2brw7v8gab1l9v578bmrl3lr32x1flkz30m5")))

(define-public crate-etwin_postgres_tools-0.6.0 (c (n "etwin_postgres_tools") (v "0.6.0") (h "08avp2zzsxa9d4zzi58vrb753dhnvna0myp4i0yf2rmqwf4l3rza")))

(define-public crate-etwin_postgres_tools-0.7.0 (c (n "etwin_postgres_tools") (v "0.7.0") (h "0ms5c3ylnzibyjk6syfbz62cfbyyyrq4jlg4ia6cji39nvc49l81")))

(define-public crate-etwin_postgres_tools-0.7.2 (c (n "etwin_postgres_tools") (v "0.7.2") (h "13x1mms84npg2zyw4cncf04xsp4p7yabba1s6zyj3xyc44kfs9lw")))

(define-public crate-etwin_postgres_tools-0.8.0 (c (n "etwin_postgres_tools") (v "0.8.0") (h "1hw6rv7pgasnr3q5r6i2dq64b6gysbyh2jiz3vigr2zs635mq1wh")))

(define-public crate-etwin_postgres_tools-0.8.1 (c (n "etwin_postgres_tools") (v "0.8.1") (h "1r3m2yh5w0pwyxnj5mwj1cncdryqbxw9vpd7dskgb6dgn0c0c8qv")))

(define-public crate-etwin_postgres_tools-0.9.0 (c (n "etwin_postgres_tools") (v "0.9.0") (h "110mh5j55dg62z5l7gl760v2rgmx4rmigllh0d1agx4p70zjfwhx")))

(define-public crate-etwin_postgres_tools-0.9.1 (c (n "etwin_postgres_tools") (v "0.9.1") (h "1kgyq2szp0x8d6cwbd7gv0j75hypqv6fqngwdmxaaprj7izvjk1l")))

(define-public crate-etwin_postgres_tools-0.9.2 (c (n "etwin_postgres_tools") (v "0.9.2") (h "14m92a3f01ycpl5rrn84141k89fh6ccwkb8b4qybqmhsmgmv83pp")))

(define-public crate-etwin_postgres_tools-0.10.0 (c (n "etwin_postgres_tools") (v "0.10.0") (h "0bfvzc6zbmxmffmx95kaq9p9n90ngzvlnfd54h3ibbnn4z15w4l7")))

(define-public crate-etwin_postgres_tools-0.10.2 (c (n "etwin_postgres_tools") (v "0.10.2") (h "11sp2xcgskv5g3q6bnpdxk8qlh880lq80lr7lphli917mfcy4jx6")))

(define-public crate-etwin_postgres_tools-0.11.0 (c (n "etwin_postgres_tools") (v "0.11.0") (h "0jj5ng2jn1cv4q2mf9ri0w3br0wfkz671l69vv9bxl9qc2hcl5ih") (r "1.61.0")))

(define-public crate-etwin_postgres_tools-0.12.0 (c (n "etwin_postgres_tools") (v "0.12.0") (h "0nma9dmg07bsw1j0dfs4c6ckbwj49ffqn5z9lcvbgw8ixz94nnh0") (r "1.61.0")))

(define-public crate-etwin_postgres_tools-0.12.3 (c (n "etwin_postgres_tools") (v "0.12.3") (h "112h13himm3g547wd2hyqzc8xy7gqr8pcqv632qvd95xng5n325r") (r "1.61.0")))

