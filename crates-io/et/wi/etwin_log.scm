(define-module (crates-io et wi etwin_log) #:use-module (crates-io))

(define-public crate-etwin_log-0.7.0 (c (n "etwin_log") (v "0.7.0") (h "1s4h5p2jp3dii9y4kxsk7fhrdsj78b325zq8gpxgg4lx5n66f96w")))

(define-public crate-etwin_log-0.7.2 (c (n "etwin_log") (v "0.7.2") (h "1087nvqi74rsfd7dc051wphl38hjgyf65pg60r27hnnix2k10slg")))

(define-public crate-etwin_log-0.8.0 (c (n "etwin_log") (v "0.8.0") (h "1d238d65namxxfpm7pxkh9xwvg126aaakdi72ws92nhjz7fn0l7y")))

(define-public crate-etwin_log-0.8.1 (c (n "etwin_log") (v "0.8.1") (h "1wlm8pjhvfnzqi7i8hapk8bw6dq9yjy9cvjfqd12gqyjk8p05l6g")))

(define-public crate-etwin_log-0.9.0 (c (n "etwin_log") (v "0.9.0") (h "18grragska66dyz0pwdd18v22a5lz8cqwhkxipg0d4jl7qnfs2ac")))

(define-public crate-etwin_log-0.9.1 (c (n "etwin_log") (v "0.9.1") (h "0ri17ycqkv2b0rkdy0g8wvkn07xc8h2khgzmg9cgz1qxqfsvcrs7")))

(define-public crate-etwin_log-0.9.2 (c (n "etwin_log") (v "0.9.2") (h "1qr7xj5scpihbyh13bmipn2v0lrf1ymp5r7y0l3f3lkjf1gxlalb")))

(define-public crate-etwin_log-0.10.0 (c (n "etwin_log") (v "0.10.0") (h "1rny92zcz30zb507b6k0bmm2nappgnpf39hq19h3a9dv19ids70m")))

(define-public crate-etwin_log-0.10.2 (c (n "etwin_log") (v "0.10.2") (h "1g1ky6sbq9rnxjy2w5rdhpb6k2iz2iskh7ng3jmsaslkx5z094gb")))

(define-public crate-etwin_log-0.11.0 (c (n "etwin_log") (v "0.11.0") (h "0hy2k11r722d307dk8xzh4lz7p050v4i7qlh7bx8x6ql0fcj9w2i") (r "1.61.0")))

(define-public crate-etwin_log-0.12.0 (c (n "etwin_log") (v "0.12.0") (h "038iv2mcjhfh0j848833cyywz86mjr0gl2v620kacb20kfmn8382") (r "1.61.0")))

(define-public crate-etwin_log-0.12.3 (c (n "etwin_log") (v "0.12.3") (h "0djx6zi1fa46by8lx6iabpfjps9m3mx96qdcijxkm832ymmghd9h") (r "1.61.0")))

