(define-module (crates-io et wi etwin_constants) #:use-module (crates-io))

(define-public crate-etwin_constants-0.1.0 (c (n "etwin_constants") (v "0.1.0") (d (list (d (n "etwin_core") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1mamapgrykpw0cgr1ndd04wbn771ir8plwrlj8zaz6yrkma040jj")))

(define-public crate-etwin_constants-0.6.0 (c (n "etwin_constants") (v "0.6.0") (d (list (d (n "etwin_core") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0qd778hz9l6w2xhbnvn1cmdcbc6xvf3hh24vkng4sw3ryf61my2a")))

(define-public crate-etwin_constants-0.7.0 (c (n "etwin_constants") (v "0.7.0") (d (list (d (n "etwin_core") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0fdpdxpqli36l9dgchzg4c18dann8y624plgna3rfc6wzbz0a6cb")))

(define-public crate-etwin_constants-0.7.2 (c (n "etwin_constants") (v "0.7.2") (d (list (d (n "etwin_core") (r "^0.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1ivgzh9h5apd18zn28cikw7izxzvgpagy12fja4mayfld3mj362r")))

(define-public crate-etwin_constants-0.8.0 (c (n "etwin_constants") (v "0.8.0") (d (list (d (n "etwin_core") (r "^0.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1agqjz12v5hyfpxg6gsj8jq7pqw9aywvimdhi8qkz7ma299iccpr")))

(define-public crate-etwin_constants-0.8.1 (c (n "etwin_constants") (v "0.8.1") (d (list (d (n "etwin_core") (r "^0.8.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0wnq91jk6nmkmchpfk7b5k5r39wh7yq1s8mfhhnd1z41g4b4jrjl")))

(define-public crate-etwin_constants-0.9.0 (c (n "etwin_constants") (v "0.9.0") (d (list (d (n "etwin_core") (r "^0.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0bb16ym0bky0f8n7yb83wndwjvvnmw460iwqi7ryrqc0bzr44zkc")))

(define-public crate-etwin_constants-0.9.1 (c (n "etwin_constants") (v "0.9.1") (d (list (d (n "etwin_core") (r "^0.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1i831s6imqn2bsnikhpn17h5wk5yrxd5m380w9xxq6pcc9xq0vaz")))

(define-public crate-etwin_constants-0.9.2 (c (n "etwin_constants") (v "0.9.2") (d (list (d (n "etwin_core") (r "^0.9.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "07n8b7izisd9syh31vb8wgsl1vhvbpgz4mn2fagabaq0zv4pjy64")))

(define-public crate-etwin_constants-0.10.0 (c (n "etwin_constants") (v "0.10.0") (d (list (d (n "etwin_core") (r "^0.10.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "0nmrrr7ds5m7abrv5infqgzr9xj2x5vzh5i9prwgaqgp3q7cb1kd")))

(define-public crate-etwin_constants-0.10.2 (c (n "etwin_constants") (v "0.10.2") (d (list (d (n "etwin_core") (r "^0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "04pgrx9z4sq22bsd376nv0yqppazy0pwxia5nb5dq8a9gpf3q3ha")))

(define-public crate-etwin_constants-0.11.0 (c (n "etwin_constants") (v "0.11.0") (d (list (d (n "etwin_core") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)))) (h "0badvsvjjlrgyzxf222p2sg4a2l2k8a8zvyag224qz47wzqa8hxz") (r "1.61.0")))

(define-public crate-etwin_constants-0.12.0 (c (n "etwin_constants") (v "0.12.0") (d (list (d (n "etwin_core") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)))) (h "1yxkkxw7vsfmr08xq1dbalh27lpwhwdk8nd5jb7dj4qnlyyykr6w") (r "1.61.0")))

(define-public crate-etwin_constants-0.12.3 (c (n "etwin_constants") (v "0.12.3") (d (list (d (n "etwin_core") (r "^0.12.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)))) (h "0jkgppjw8p6r00qldw5klifv4p61fagnsjrj7ibib4zhbmzc30l9") (r "1.61.0")))

