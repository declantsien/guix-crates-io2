(define-module (crates-io et wi etwin_mt_querystring) #:use-module (crates-io))

(define-public crate-etwin_mt_querystring-0.10.2 (c (n "etwin_mt_querystring") (v "0.10.2") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (o #t) (d #t) (k 0)))) (h "1l7mk5wkqrlbplrzaxbzfwzi2vrg2cdw3wm0kixqi2jax8hmagfp") (f (quote (("default" "url"))))))

(define-public crate-etwin_mt_querystring-0.11.0 (c (n "etwin_mt_querystring") (v "0.11.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (o #t) (d #t) (k 0)))) (h "12b5i7qj3h0q0qgvl9bg2dvb59ndvbsnb8yagbdj317g7qkxw6m7") (f (quote (("default" "url")))) (r "1.61.0")))

(define-public crate-etwin_mt_querystring-0.12.0 (c (n "etwin_mt_querystring") (v "0.12.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (o #t) (d #t) (k 0)))) (h "1chjpbac894559lix6p1wib77afdh9lby6w5lhck20jq2kjjnc9w") (f (quote (("default" "url")))) (r "1.61.0")))

(define-public crate-etwin_mt_querystring-0.12.3 (c (n "etwin_mt_querystring") (v "0.12.3") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (o #t) (d #t) (k 0)))) (h "1i1hqpjpwn4jw3qhm0rqp6cl3s6d9jw5pllzbkh95a3vmrvzaz02") (f (quote (("default" "url")))) (r "1.61.0")))

