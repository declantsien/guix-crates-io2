(define-module (crates-io et wi etwin_scraper_tools) #:use-module (crates-io))

(define-public crate-etwin_scraper_tools-0.1.0 (c (n "etwin_scraper_tools") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.2") (d #t) (k 0)))) (h "0qsfn3hyymsjy3skkglymsay2jlc68rqffngsgilh3ilsswnjav2")))

(define-public crate-etwin_scraper_tools-0.6.0 (c (n "etwin_scraper_tools") (v "0.6.0") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.2") (d #t) (k 0)))) (h "1r8yi5mkbpz47735h430w1qw91rnw3xm5nw80a0w5qhm74n7rnap")))

(define-public crate-etwin_scraper_tools-0.7.0 (c (n "etwin_scraper_tools") (v "0.7.0") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.2") (d #t) (k 0)))) (h "07w69ahdva8b860s4z8dj4h4crwrpgpd8lbxkmzmqyw00shp48z3")))

(define-public crate-etwin_scraper_tools-0.7.2 (c (n "etwin_scraper_tools") (v "0.7.2") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.2") (d #t) (k 0)))) (h "0r159v6g4si4gh9zc1y0llqim34jv4r6vy0c4kpnssjixr6kf6xh")))

(define-public crate-etwin_scraper_tools-0.8.0 (c (n "etwin_scraper_tools") (v "0.8.0") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "08d8j0bll2rjh1dcksr3hhikxam0axxwbz2zakfyn5j70j3qr1j4")))

(define-public crate-etwin_scraper_tools-0.8.1 (c (n "etwin_scraper_tools") (v "0.8.1") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0mf0xd6h1vkhqz49gzf86229byr6slfnq7mjasc4ggv8kpqzclh2")))

(define-public crate-etwin_scraper_tools-0.9.0 (c (n "etwin_scraper_tools") (v "0.9.0") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "138sar35xg9kfbc4lzi1f4fvlkbcx7dcvcn5b3sm8rfj6d09jjpw")))

(define-public crate-etwin_scraper_tools-0.9.1 (c (n "etwin_scraper_tools") (v "0.9.1") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0hwpxwncslc91854hg39za69q4pjaggn8i7j41byrl5v3p3s12am")))

(define-public crate-etwin_scraper_tools-0.9.2 (c (n "etwin_scraper_tools") (v "0.9.2") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0ak9a5b9m0vrbmm120sn1jvhh0mqpz8b0n33p5fhshdybbgaprf4")))

(define-public crate-etwin_scraper_tools-0.10.0 (c (n "etwin_scraper_tools") (v "0.10.0") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "16rlm46gngj3x9akna8skrv4fgk3v0gww8hgrx7wrdcz3ffqhn9g")))

(define-public crate-etwin_scraper_tools-0.10.2 (c (n "etwin_scraper_tools") (v "0.10.2") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0c45iv5gv1p0w0gxg2ck65by1vkz3jnwhy3bl0zi749gyjg5ixb0")))

(define-public crate-etwin_scraper_tools-0.11.0 (c (n "etwin_scraper_tools") (v "0.11.0") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0iy34qgadlj5f5j10i99kzdd7dlr6yn4yr2glzlrcv3y81cd9b3d") (r "1.61.0")))

(define-public crate-etwin_scraper_tools-0.12.0 (c (n "etwin_scraper_tools") (v "0.12.0") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "033kfh7bwwhlfcmdyz3g5q6r11kcw5h4gg3cdxyiil43msxwcwpq") (r "1.61.0")))

(define-public crate-etwin_scraper_tools-0.12.3 (c (n "etwin_scraper_tools") (v "0.12.3") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "08kryjayyhrpff9kapdf3pm02dvp6gdybds3dra2arckssv9ymfx") (r "1.61.0")))

