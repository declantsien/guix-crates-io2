(define-module (crates-io et wi etwin_mt_dns) #:use-module (crates-io))

(define-public crate-etwin_mt_dns-0.9.2 (c (n "etwin_mt_dns") (v "0.9.2") (d (list (d (n "etwin_core") (r "^0.9.2") (f (quote ("_serde"))) (d #t) (k 0)) (d (n "neon") (r "^0.9.1") (f (quote ("napi-6"))) (o #t) (k 0)))) (h "05kfbpkxkrdcd01b9mhj74gc6pf03l7aza6y2lgd18zf66xfm4v3")))

(define-public crate-etwin_mt_dns-0.10.0 (c (n "etwin_mt_dns") (v "0.10.0") (d (list (d (n "etwin_core") (r "^0.10.0") (f (quote ("_serde"))) (d #t) (k 0)) (d (n "neon") (r "^0.9.1") (f (quote ("napi-6"))) (o #t) (k 0)))) (h "0y19apgdgkni361si0wl7phb3pch459jcyimpyk831lrbq2l9jlr")))

(define-public crate-etwin_mt_dns-0.10.2 (c (n "etwin_mt_dns") (v "0.10.2") (d (list (d (n "etwin_core") (r "^0.10.2") (f (quote ("_serde"))) (d #t) (k 0)) (d (n "neon") (r "^0.10.1") (f (quote ("napi-6"))) (o #t) (k 0)))) (h "1hvrabg1k5mmfqzx18ql8dm03mi1s51ah7v8y4djwlrms4aby8zl")))

(define-public crate-etwin_mt_dns-0.11.0 (c (n "etwin_mt_dns") (v "0.11.0") (d (list (d (n "etwin_core") (r "^0.11.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "neon") (r "^0.10.1") (f (quote ("napi-6"))) (o #t) (k 0)))) (h "1pfc0r6f1a3w82r0gl58qfphc207ab94w3cyzy57zl6sb4q639cv") (r "1.61.0")))

(define-public crate-etwin_mt_dns-0.12.0 (c (n "etwin_mt_dns") (v "0.12.0") (d (list (d (n "etwin_core") (r "^0.12.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "neon") (r "^0.10.1") (f (quote ("napi-6"))) (o #t) (k 0)))) (h "1rmm8a71rqq3m9kh0cwmhgzjrb980mdv03wlhgggq4xhxrm3g7wn") (r "1.61.0")))

(define-public crate-etwin_mt_dns-0.12.3 (c (n "etwin_mt_dns") (v "0.12.3") (d (list (d (n "etwin_core") (r "^0.12.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "neon") (r "^0.10.1") (f (quote ("napi-6"))) (o #t) (k 0)))) (h "1m5xhqndkfnfwpb1y1svq9ph3pyxjlncl1yrpdid563d7pcg8z0a") (r "1.61.0")))

