(define-module (crates-io et wi etwin_config) #:use-module (crates-io))

(define-public crate-etwin_config-0.1.0 (c (n "etwin_config") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "09da9dp79b8clbqj3vsgbgb7k09iz8vl8y5y0i3ymypxxkvd3z69")))

(define-public crate-etwin_config-0.6.0 (c (n "etwin_config") (v "0.6.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0dcm92ma00qr75ajcrvzw0fx9ia58cwfvsl0hv50p3d1j0lpgv4j")))

(define-public crate-etwin_config-0.7.0 (c (n "etwin_config") (v "0.7.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1b0571gl73z3k1w3aghqpixz5frq9zjzk1g3mdm90g6h8zjc6y6d")))

(define-public crate-etwin_config-0.7.2 (c (n "etwin_config") (v "0.7.2") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1fwjlbxlf8kwf4kfxgs9ap92yky8p7sawnf1nwajjy6lpr34mmcm")))

(define-public crate-etwin_config-0.8.0 (c (n "etwin_config") (v "0.8.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1r3hzjvgfxqhnb0rrc223dc4hvz6b9kil5w8wjpdhyc4hri61klr")))

(define-public crate-etwin_config-0.8.1 (c (n "etwin_config") (v "0.8.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0rnjil9jwgqx4b32gbhj536v9h1irxb4yr5rr708whyfm16700q6")))

(define-public crate-etwin_config-0.9.0 (c (n "etwin_config") (v "0.9.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0jd5fhm10w3rq3nb38yv3x12lp5rcmwhbpvvl4lbx2dmbvpks97g")))

(define-public crate-etwin_config-0.9.1 (c (n "etwin_config") (v "0.9.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1kclvyg5vg9hfakxl3bf9hx2k665hfnlr8lym5sp64iwsdqvnxya")))

(define-public crate-etwin_config-0.9.2 (c (n "etwin_config") (v "0.9.2") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0rda1phxxik0icq03qncf9hffszxqp0jhzjslnmqbwlswhynfyv6")))

(define-public crate-etwin_config-0.10.0 (c (n "etwin_config") (v "0.10.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "054gn56ib1dzhyq5w9dbzawdn37s4cprk2w6yk9461zzabwhm1rl")))

(define-public crate-etwin_config-0.10.2 (c (n "etwin_config") (v "0.10.2") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1a88aa5fgjlv9qa8n75k1a408vyc6r45afr38c24sm98pwxcgiw0")))

(define-public crate-etwin_config-0.11.0 (c (n "etwin_config") (v "0.11.0") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1n8w2lhagihcbqmym89vdbf168fqkj1jlzngn0znmywzrrrma0ga") (r "1.61.0")))

(define-public crate-etwin_config-0.12.0 (c (n "etwin_config") (v "0.12.0") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0rh3s48gkrx1my64ly6dwiif63a989g5iqwxfd288n3w6ly0r0q0") (r "1.61.0")))

(define-public crate-etwin_config-0.12.3 (c (n "etwin_config") (v "0.12.3") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1437yga2aibp7d7qlgfaj3qqamxc3y0yr5mrhwvgdr4mcbdkahg8") (r "1.61.0")))

