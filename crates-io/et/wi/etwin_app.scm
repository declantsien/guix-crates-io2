(define-module (crates-io et wi etwin_app) #:use-module (crates-io))

(define-public crate-etwin_app-0.12.3 (c (n "etwin_app") (v "0.12.3") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "16rm9v9kqddxd19m8s5vql0i67pw9vc7dzachw5wi8pviwsgyfmj") (r "1.61.0")))

(define-public crate-etwin_app-0.12.4 (c (n "etwin_app") (v "0.12.4") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "07dic9a1ingan0xn9rpprk5mxrqmqq676lxd53a12fvzhlkv7abw") (r "1.61.0")))

