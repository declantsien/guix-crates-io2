(define-module (crates-io et ha ethabi-contract) #:use-module (crates-io))

(define-public crate-ethabi-contract-4.0.0 (c (n "ethabi-contract") (v "4.0.0") (h "0mr7qakkv85pfcxdha00ix18yn79rifg9kcvgvqh7lv6fy4b7y4q")))

(define-public crate-ethabi-contract-5.0.0 (c (n "ethabi-contract") (v "5.0.0") (h "03llyri5489v4qajh4qr4j0r9jgb3ldw8mr4d3lzmnynfszy4pl7") (y #t)))

(define-public crate-ethabi-contract-5.0.1 (c (n "ethabi-contract") (v "5.0.1") (h "15biv0y2rax6f8pg7zfxyjlrk8s7lx841y00a61142xf1nah0lm4")))

(define-public crate-ethabi-contract-5.0.2 (c (n "ethabi-contract") (v "5.0.2") (h "1b8jdpn98lmibpyj1x2m9k24qfnjkpicr9a0jjykjj5kpm773zgi")))

(define-public crate-ethabi-contract-5.0.3 (c (n "ethabi-contract") (v "5.0.3") (h "0wy0x6lyhb95z90kkf60ssqbm9cbwbra36n9i8s2gs2r8g1668na")))

(define-public crate-ethabi-contract-5.1.0 (c (n "ethabi-contract") (v "5.1.0") (h "1v92ppnm6gj67275bvym66h88g1aw7h03rjgyrp5phb4s4hrw311")))

(define-public crate-ethabi-contract-5.1.1 (c (n "ethabi-contract") (v "5.1.1") (h "1ygjm9drf6mxaqd04c6fszccnmwsrs6vyis001jbb926597ka4bk")))

(define-public crate-ethabi-contract-6.0.0 (c (n "ethabi-contract") (v "6.0.0") (h "09qcdq9f0dcmn8w998yncy7vl7lgrkvf3nrmlafaa4lfhvyjapkr")))

(define-public crate-ethabi-contract-7.0.0 (c (n "ethabi-contract") (v "7.0.0") (h "1qm71fiz8cp254wmckg4bmmwklq35aqb395msqq472wrng8367xm")))

(define-public crate-ethabi-contract-8.0.0 (c (n "ethabi-contract") (v "8.0.0") (h "17rzb9fv6mjyh7hpmg1dflkcivq5nn2k6sbm3miv5nd8mp7pyar7")))

(define-public crate-ethabi-contract-8.0.1 (c (n "ethabi-contract") (v "8.0.1") (h "1y21pvv5pcdn3a8m5m35vw58gkxs0c4z95infn91cid5ax7k3mp0")))

(define-public crate-ethabi-contract-9.0.0 (c (n "ethabi-contract") (v "9.0.0") (h "1jf3rvidhqk2nyjp6sw5flznc3ij2jy9712jn7xlndwh0b77sh6g")))

(define-public crate-ethabi-contract-10.0.0 (c (n "ethabi-contract") (v "10.0.0") (h "0xq9991sqc589nkbly7fxhh2xjvi07ivfvkiprkp0lwhsima92ys")))

(define-public crate-ethabi-contract-11.0.0 (c (n "ethabi-contract") (v "11.0.0") (h "0cl0yv3sqwj32c8nrji17skdzjg1xy4qbzfshlv27n3p3wph1m48")))

(define-public crate-ethabi-contract-16.0.0 (c (n "ethabi-contract") (v "16.0.0") (h "10qw0irf91vbcaxzbbh0f1zpnwhhmbmyfhbsxdr9ixgvcsvv2cj6")))

