(define-module (crates-io et ha ethanol-derive) #:use-module (crates-io))

(define-public crate-ethanol-derive-0.1.0 (c (n "ethanol-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "100ayv7fxwbsakc40mdp7w76vx13gblqnplyhj6fhrwxh23gxwgk")))

