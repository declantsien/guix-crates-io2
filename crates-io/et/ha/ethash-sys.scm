(define-module (crates-io et ha ethash-sys) #:use-module (crates-io))

(define-public crate-ethash-sys-0.1.0 (c (n "ethash-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1skx9wsqwry5sxjd8c0vfphz9prhdjipqz9qbwgvziq0b72q3n35")))

(define-public crate-ethash-sys-0.1.1 (c (n "ethash-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0x6bh4gmcpq9g5fdkgpl2cngnnz4jayh4mn6svhzb2dm0baa7c6f")))

(define-public crate-ethash-sys-0.1.2 (c (n "ethash-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "05j7jjzn9mi7xhxihlpydwifz4r5zi5saqr8cbi01s3grxq611n3")))

(define-public crate-ethash-sys-0.1.3 (c (n "ethash-sys") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1z79lgnf5g9scychh5vcfqgzgl9lyxdqqk4wk7rqxwkx0gfxvryq")))

