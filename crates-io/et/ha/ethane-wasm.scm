(define-module (crates-io et ha ethane-wasm) #:use-module (crates-io))

(define-public crate-ethane-wasm-1.0.2 (c (n "ethane-wasm") (v "1.0.2") (d (list (d (n "ethane") (r "^1.0.2") (f (quote ("non-blocking"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.45") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "1vjv8wbl277yw63rksk2qab3vzkxa1kz3ppvpxxcj11al4ygndp3")))

