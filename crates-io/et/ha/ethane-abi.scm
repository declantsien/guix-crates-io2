(define-module (crates-io et ha ethane-abi) #:use-module (crates-io))

(define-public crate-ethane-abi-1.0.0 (c (n "ethane-abi") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.11") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07akqgbp0rcqnpylj4kydgllq9j26ilblk1rpi4p00qyffq824fp")))

(define-public crate-ethane-abi-1.0.1 (c (n "ethane-abi") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.11") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dqzy3s29ah4632xp08f43h5892sjsqx11adva4pwiiwhgh0i8nl")))

(define-public crate-ethane-abi-1.0.2 (c (n "ethane-abi") (v "1.0.2") (d (list (d (n "ethane-types") (r "^1.0.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1m9zgslrjwscmsxld9bn35gh0b8w91px3xn2bpsqj8723lbhx3my")))

