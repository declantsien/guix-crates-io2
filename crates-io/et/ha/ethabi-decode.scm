(define-module (crates-io et ha ethabi-decode) #:use-module (crates-io))

(define-public crate-ethabi-decode-1.0.0 (c (n "ethabi-decode") (v "1.0.0") (d (list (d (n "ethereum-types") (r "^0.14.1") (k 0)) (d (n "hex") (r "^2.0") (d #t) (k 2) (p "rustc-hex")) (d (n "hex-literal") (r "^0.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)) (d (n "uint") (r "^0.9.5") (k 2)))) (h "05rljm035zms6167w13a75rl91rgjs5yb0bagmr0m0k5imj9ilq9") (f (quote (("std" "ethereum-types/std") ("default" "std"))))))

