(define-module (crates-io et ha ethaddr-macros) #:use-module (crates-io))

(define-public crate-ethaddr-macros-0.1.0 (c (n "ethaddr-macros") (v "0.1.0") (d (list (d (n "sha3") (r "^0.10") (k 0)))) (h "16zx0gp6i6wl3bf87w9z86y8i9gdir1nmg9s9ii0gwykp0f5g828")))

(define-public crate-ethaddr-macros-0.1.1 (c (n "ethaddr-macros") (v "0.1.1") (d (list (d (n "sha3") (r "^0.10") (k 0)))) (h "0rvhzljqpzx76brlfhi1l3dc06xcrgksfln50xbkfwpzd7qrqr06")))

(define-public crate-ethaddr-macros-0.1.2 (c (n "ethaddr-macros") (v "0.1.2") (d (list (d (n "sha3") (r "^0.10") (k 0)))) (h "0045zfxkksb920fji84a0wv7m14z70csv9gyk4y4zakinr6107zy")))

