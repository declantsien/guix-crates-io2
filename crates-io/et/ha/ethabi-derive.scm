(define-module (crates-io et ha ethabi-derive) #:use-module (crates-io))

(define-public crate-ethabi-derive-4.0.0 (c (n "ethabi-derive") (v "4.0.0") (d (list (d (n "ethabi") (r "^4.0") (d #t) (k 0)) (d (n "heck") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1n1sw9jkbcmq4qhcc8qrhx6qzn1qmck7a8n3v285zgqqjm3anqbi")))

(define-public crate-ethabi-derive-4.1.0 (c (n "ethabi-derive") (v "4.1.0") (d (list (d (n "ethabi") (r "^4.1") (d #t) (k 0)) (d (n "heck") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "16xf7ymh9d7g4a9yp1qvkrbavqni9wacfjfa0rj4wwxpavf88g0f")))

(define-public crate-ethabi-derive-4.2.0 (c (n "ethabi-derive") (v "4.2.0") (d (list (d (n "ethabi") (r "^4.2") (d #t) (k 0)) (d (n "heck") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "073sgb6z1m6a1sdb67nx9dhxll675fln19lisvpb60rmhlhk6zp1")))

(define-public crate-ethabi-derive-5.0.0 (c (n "ethabi-derive") (v "5.0.0") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1i6l3snpqhdpp8rvrwghfbrjspnqy2ihg47xn21xi2v29gaq4r6b") (y #t)))

(define-public crate-ethabi-derive-5.0.1 (c (n "ethabi-derive") (v "5.0.1") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "09yavqqymr62dc6q9hhs8hn3zlv3nfbfz2bgi0r11fwpzv5l8lxb")))

(define-public crate-ethabi-derive-5.0.2 (c (n "ethabi-derive") (v "5.0.2") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1g16igy7x3cmfpbpqrnk4lfw0h7424gilbw3k0gl0ximy3i59g12")))

(define-public crate-ethabi-derive-5.0.3 (c (n "ethabi-derive") (v "5.0.3") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0jq2yi0if295zaaasipmcqk2n9ayhspr5gajq2cijy9kf70xv4a5") (y #t)))

(define-public crate-ethabi-derive-5.0.4 (c (n "ethabi-derive") (v "5.0.4") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0dppp276dcjqd64k9fdhfrsqmikvay5ngfh44a3yp68majiim8pl")))

(define-public crate-ethabi-derive-5.0.5 (c (n "ethabi-derive") (v "5.0.5") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "04xsv2wip17g7yxx69b8c0l96hcsnd9in94binphi5149wzpsdcs")))

(define-public crate-ethabi-derive-5.0.6 (c (n "ethabi-derive") (v "5.0.6") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (d #t) (k 0)))) (h "0nksxh7yvi2pj2znw5mhyy87g53131ab61nkqmp7w25khb1g44hc")))

(define-public crate-ethabi-derive-5.0.7 (c (n "ethabi-derive") (v "5.0.7") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (d #t) (k 0)))) (h "075i12pka0977nax5misscgd3gjhvg8sf8wckgxrr1s0yag0q0lf")))

(define-public crate-ethabi-derive-5.0.8 (c (n "ethabi-derive") (v "5.0.8") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (d #t) (k 0)))) (h "1qxwb7ibk8xs4j1wqg77284yh5jqswlj2vndgwp1n68lng77czq5")))

(define-public crate-ethabi-derive-5.1.0 (c (n "ethabi-derive") (v "5.1.0") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (d #t) (k 0)))) (h "1bmp0hmx7h16mc21f3gv1g024nsaq4n7jdi6iyabfdhq4n7gniyq")))

(define-public crate-ethabi-derive-5.1.1 (c (n "ethabi-derive") (v "5.1.1") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0isqkrds22inz07c4092j86f4srfgx8hzsclgvqh5d81bmdm8i5k")))

(define-public crate-ethabi-derive-5.1.2 (c (n "ethabi-derive") (v "5.1.2") (d (list (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0x1xk0mzw039rcbz6gy45g4421bc9bh9zkdfxmx1hix1pacp1g6j")))

(define-public crate-ethabi-derive-5.1.3 (c (n "ethabi-derive") (v "5.1.3") (d (list (d (n "ethabi") (r "^5.1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (d #t) (k 0)))) (h "1aqz130n8rd8df6vfj4yk4fayrz2wpqxwv56wh5rfcvhsgjr3bvw")))

(define-public crate-ethabi-derive-6.0.1 (c (n "ethabi-derive") (v "6.0.1") (d (list (d (n "ethabi") (r "^6.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1f8gdhzz1lwhzn63vbp9js3zashf13ry7isjjbd8rvm9796yllv8")))

(define-public crate-ethabi-derive-6.0.2 (c (n "ethabi-derive") (v "6.0.2") (d (list (d (n "ethabi") (r "^6.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0xd0ajq3ln9078bp5qp2xswprnbyjnw6g3g2dafvx4411hjqg9b6")))

(define-public crate-ethabi-derive-7.0.0 (c (n "ethabi-derive") (v "7.0.0") (d (list (d (n "ethabi") (r "^7.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1gykvn07j8qhl8ylv6q3blznkv9dhyj8fr68lwgajw4jmlbmapxf")))

(define-public crate-ethabi-derive-8.0.0 (c (n "ethabi-derive") (v "8.0.0") (d (list (d (n "ethabi") (r "^8.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1lsqmqgxdncm62c66yldwar16fl55ycspdakvhhihpdzk4qyzkb5")))

(define-public crate-ethabi-derive-9.0.1 (c (n "ethabi-derive") (v "9.0.1") (d (list (d (n "ethabi") (r "^9.0.1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "114qac2x6ff2yyjhj0yfx0m709q5n8028pysa2aaknz1z7a561xx")))

(define-public crate-ethabi-derive-10.0.0 (c (n "ethabi-derive") (v "10.0.0") (d (list (d (n "ethabi") (r "^10.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1jisd56jnyyn7xydl9qpj3ijrbbkfywddgnfrkkq9mjr4hl76imy")))

(define-public crate-ethabi-derive-11.0.0 (c (n "ethabi-derive") (v "11.0.0") (d (list (d (n "ethabi") (r "^11.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "02n7znr5k3xby8ajvnz7q9m9398smra9dv6gms6g694bfkwkyyjq")))

(define-public crate-ethabi-derive-12.0.0 (c (n "ethabi-derive") (v "12.0.0") (d (list (d (n "ethabi") (r "^12.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "09g0ggz0c4n6ijrn7ks0halg44q4jkkn7j5g27qzq34fc5kgxh5p")))

(define-public crate-ethabi-derive-13.0.0 (c (n "ethabi-derive") (v "13.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethabi") (r "^13.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "05djvv7pqqgbw72l0jdyywjylaqdiwzjfcmsyl1ngnh4r675vc6r")))

(define-public crate-ethabi-derive-14.0.0 (c (n "ethabi-derive") (v "14.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethabi") (r "^14.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1p838bxinpbh9h6h3imwfvmrlm6wlnbvfjkagbmf859h239vgn6z")))

(define-public crate-ethabi-derive-14.1.0 (c (n "ethabi-derive") (v "14.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethabi") (r "^14.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "17nxcsw6y0bi1ff51l7c85bnflwamk48q3xvac67h89hvp3k2li2")))

(define-public crate-ethabi-derive-16.0.0 (c (n "ethabi-derive") (v "16.0.0") (d (list (d (n "ethabi") (r "^16.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0rzl408ayzqlb7y6fplpjrvnr137n1xwdr90582d2k5yad2lfrk7")))

