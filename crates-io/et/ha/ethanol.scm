(define-module (crates-io et ha ethanol) #:use-module (crates-io))

(define-public crate-ethanol-0.1.0 (c (n "ethanol") (v "0.1.0") (d (list (d (n "ethanol-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "05vnai4snax5sbpjmhacgz73hm6k5mf434zqw8xxvnkxdnlpqd4j")))

