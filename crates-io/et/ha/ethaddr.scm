(define-module (crates-io et ha ethaddr) #:use-module (crates-io))

(define-public crate-ethaddr-0.1.0 (c (n "ethaddr") (v "0.1.0") (d (list (d (n "ethaddr-macros") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "1sdzbi7h0377g6fd83aqrkr1yw7a1hhm180agl54dd9pcblhz76g") (f (quote (("macros" "ethaddr-macros") ("default" "checksum" "std") ("checksum" "sha3")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethaddr-0.1.1 (c (n "ethaddr") (v "0.1.1") (d (list (d (n "ethaddr-macros") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "1n3ir9zpwfm9qhpkd4lab09kyigpzmqw06i8v0r2627fzx5nvhb1") (f (quote (("macros" "ethaddr-macros") ("default" "checksum" "std") ("checksum" "sha3")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethaddr-0.1.2 (c (n "ethaddr") (v "0.1.2") (d (list (d (n "ethaddr-macros") (r "=0.1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "0n5r1czlk584hi7197gx5fbavnnhv8vxcbyswq3nl8wr2mibfnxr") (f (quote (("macros" "ethaddr-macros") ("default" "checksum" "std") ("checksum" "sha3")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethaddr-0.2.0 (c (n "ethaddr") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "11zkblwr1blir7z78ap0xn1i29n921m1fbplgr9y7fz484fc9gww") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethaddr-0.2.1 (c (n "ethaddr") (v "0.2.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "1lmib8xcqfsqa9fsdmc3icvp6vkbv029kf5ah0388rcy60ssbg53") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethaddr-0.2.2 (c (n "ethaddr") (v "0.2.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "1j33hd7jfh7y2410x9248nbf3yqfl53hrj0m76w3ybhv1mvw3g7c") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

