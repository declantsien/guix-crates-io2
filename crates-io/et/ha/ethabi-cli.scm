(define-module (crates-io et ha ethabi-cli) #:use-module (crates-io))

(define-public crate-ethabi-cli-1.1.0 (c (n "ethabi-cli") (v "1.1.0") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "ethabi") (r "^1.1") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1mm31qq8y7d4wcqwmnbb96jdlhm12w919p1qa12scc94wacw6a58")))

(define-public crate-ethabi-cli-1.2.0 (c (n "ethabi-cli") (v "1.2.0") (d (list (d (n "docopt") (r "= 0.6.85") (d #t) (k 0)) (d (n "ethabi") (r "^1.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1kd0ryy6s5fp67zii4a840y8xxb0vqdlxryhgm8pnaz7qh9siikz") (y #t)))

(define-public crate-ethabi-cli-2.0.0 (c (n "ethabi-cli") (v "2.0.0") (d (list (d (n "docopt") (r "= 0.6.85") (d #t) (k 0)) (d (n "ethabi") (r "^2.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0zbyck1byyrkn3wl7lpwbxkwal4amcp08fky2jfslz5h9rq0yy0p")))

(define-public crate-ethabi-cli-2.0.1 (c (n "ethabi-cli") (v "2.0.1") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "ethabi") (r "^2.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0w70mgm5dr516fzwic2vwz49fhibjwv52rsjd2iw0r1kaiilhila")))

(define-public crate-ethabi-cli-4.0.0 (c (n "ethabi-cli") (v "4.0.0") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (k 0)) (d (n "ethabi") (r "^4.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "01m8k9343gx0riq72yhgl4v333d4lbnk9zp40537nqc8l6k6qy2y") (f (quote (("backtrace" "error-chain/backtrace"))))))

(define-public crate-ethabi-cli-5.0.0 (c (n "ethabi-cli") (v "5.0.0") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1m0qdq4dcp3wz8s5x45hk8dvqs4i6yb5lqs4bfhln7hcpji3rbz8") (f (quote (("backtrace" "error-chain/backtrace")))) (y #t)))

(define-public crate-ethabi-cli-5.0.1 (c (n "ethabi-cli") (v "5.0.1") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "ethabi") (r "^5.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0qhw9qii0ja8va2sk46za6yjkjdarpnpa7s2sw61nar82bv61jv3") (f (quote (("backtrace" "error-chain/backtrace"))))))

(define-public crate-ethabi-cli-5.1.0 (c (n "ethabi-cli") (v "5.1.0") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "ethabi") (r "^5") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1bp77qwsb791kinyrr0h5aas0qm9m65vb7gxfdcjiga4pspxqnjz") (f (quote (("backtrace" "error-chain/backtrace"))))))

(define-public crate-ethabi-cli-6.0.0 (c (n "ethabi-cli") (v "6.0.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "ethabi") (r "^6.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "04rn7km3sqdcl1hfidxxd2700d5dyk51ijsw5rh6ba7zgmq7mjas") (f (quote (("backtrace" "error-chain/backtrace"))))))

(define-public crate-ethabi-cli-7.0.0 (c (n "ethabi-cli") (v "7.0.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "ethabi") (r "^7.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0fn90b04fr4j4v3l2jrlzqggg8nnagar057l1lww0nia6h6kwflf") (f (quote (("backtrace" "error-chain/backtrace"))))))

(define-public crate-ethabi-cli-8.0.0 (c (n "ethabi-cli") (v "8.0.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.1") (k 0)) (d (n "ethabi") (r "^8.0.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.0") (d #t) (k 0)))) (h "0y3zyba9vjazzhlng59z9m2ssmqgi2rijxkc12v2pzv7zpsyip1l") (f (quote (("backtrace" "error-chain/backtrace"))))))

(define-public crate-ethabi-cli-9.0.1 (c (n "ethabi-cli") (v "9.0.1") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.1") (k 0)) (d (n "ethabi") (r "^9.0.1") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.0") (d #t) (k 0)))) (h "1gf0jjx0741fdjsdfg5fs3gxmz32a0ry63m3liv0zajwj3xkc2n4") (f (quote (("backtrace" "error-chain/backtrace"))))))

(define-public crate-ethabi-cli-10.0.0 (c (n "ethabi-cli") (v "10.0.0") (d (list (d (n "error-chain") (r "^0.12.1") (k 0)) (d (n "ethabi") (r "^10.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.0") (d #t) (k 0)))) (h "0a2ifdqk26ndps9vcyrpdkmyxxl79l32hi53nj1rgda01x8p7rvd") (f (quote (("backtrace" "error-chain/backtrace"))))))

(define-public crate-ethabi-cli-11.0.0 (c (n "ethabi-cli") (v "11.0.0") (d (list (d (n "ethabi") (r "^11.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "1skhrzfycr3pw5xc0vy31rd327q28kfag05ms61mnsw3kibp5qid")))

(define-public crate-ethabi-cli-13.0.0 (c (n "ethabi-cli") (v "13.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethabi") (r "^13.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "02dlgrxb07813ndfhbrv2ya0b30sgjshza3l8aripxyd2gqjxmc5")))

(define-public crate-ethabi-cli-14.0.0 (c (n "ethabi-cli") (v "14.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethabi") (r "^14.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1dyz1606zr2fhinhazmww1sb46clrfd7rwsajk0v7m25jsy3fmxs")))

(define-public crate-ethabi-cli-14.1.0 (c (n "ethabi-cli") (v "14.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethabi") (r "^14.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0s046z3x447qvh24cjw0vdk2kr6m1hc9ar2csz9mz5x091lrmlqj")))

(define-public crate-ethabi-cli-16.0.0 (c (n "ethabi-cli") (v "16.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethabi") (r "^16.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0h8r7afkzhnjn67xxpg678bg0w6fdc0dl6m8rvibypaqr1nb35am")))

