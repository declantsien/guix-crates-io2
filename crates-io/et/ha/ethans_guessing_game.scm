(define-module (crates-io et ha ethans_guessing_game) #:use-module (crates-io))

(define-public crate-ethans_guessing_game-0.1.0 (c (n "ethans_guessing_game") (v "0.1.0") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "emojis") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09nrqs67z03b069zxvsx816l8wfgv6s8fdia6l3w83348ydxbh2c") (y #t)))

