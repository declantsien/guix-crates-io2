(define-module (crates-io et ha ethabi-fork-ethcontract) #:use-module (crates-io))

(define-public crate-ethabi-fork-ethcontract-13.0.0 (c (n "ethabi-fork-ethcontract") (v "13.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.10.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uint") (r "^0.9.0") (d #t) (k 0)))) (h "1bavwda1n1bw5zwqm44lgdcbmx6zl0d5vlh7hzsb3brny6lfl52x")))

