(define-module (crates-io et h- eth-secp256k1) #:use-module (crates-io))

(define-public crate-eth-secp256k1-0.5.7 (c (n "eth-secp256k1") (v "0.5.7") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1q3rkwmglys7j3kqk3byvb42ah3d6dn91y7532cwinz8cxszplyb") (f (quote (("unstable") ("dev" "clippy") ("default"))))))

