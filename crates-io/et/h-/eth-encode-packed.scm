(define-module (crates-io et h- eth-encode-packed) #:use-module (crates-io))

(define-public crate-eth-encode-packed-0.1.0 (c (n "eth-encode-packed") (v "0.1.0") (d (list (d (n "ethabi") (r "^14.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1gdgygzmjz4nimj54krwc9padri5ffp9pxg9rhyc2y60fjyy5535")))

