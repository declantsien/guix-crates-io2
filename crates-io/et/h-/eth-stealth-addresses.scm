(define-module (crates-io et h- eth-stealth-addresses) #:use-module (crates-io))

(define-public crate-eth-stealth-addresses-0.1.0 (c (n "eth-stealth-addresses") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.14.7") (d #t) (k 0)) (d (n "k256") (r "^0.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "secp256k1") (r "^0.27.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "15iwlbgrmfkqpfyfwpr435ybmabnkbpkkwxn4x2mhm9ajpxc3a2i")))

