(define-module (crates-io et h- eth-jsonrpc-types-internals) #:use-module (crates-io))

(define-public crate-eth-jsonrpc-types-internals-0.1.0 (c (n "eth-jsonrpc-types-internals") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0506c942rpny4nfqic54kr1caja7sdscmlpyvk61vkh7m6smmway")))

