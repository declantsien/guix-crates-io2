(define-module (crates-io et h- eth-blockies) #:use-module (crates-io))

(define-public crate-eth-blockies-0.9.0 (c (n "eth-blockies") (v "0.9.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 2)))) (h "0mycnpd9fppxfiwl6zckhvnraqjsp3z2saqhz2j909wq8cxh4hzy") (y #t)))

(define-public crate-eth-blockies-0.9.1 (c (n "eth-blockies") (v "0.9.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1fb8d93y5043ny243kzzms4rjknbk209jhkxwp4m2jb61n44w3qr") (y #t)))

(define-public crate-eth-blockies-0.9.2 (c (n "eth-blockies") (v "0.9.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1x9d4amm38wxym1v4xdic93hg6cwxb1vk14pfpvv66sg7sz2hwbn")))

(define-public crate-eth-blockies-1.0.0 (c (n "eth-blockies") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)))) (h "0v93y169qxcssglksb2dw0k1p0yh1mhiq87mx6jgyv0qb4wy8cl9") (r "1.56")))

(define-public crate-eth-blockies-1.1.0 (c (n "eth-blockies") (v "1.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "deflate") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1vb4xh8s69slmawgki5zc676g09k3gb6qpr3ldhvbgv9mnh1mjn3") (f (quote (("default" "compressed_png")))) (s 2) (e (quote (("compressed_png" "dep:deflate")))) (r "1.63")))

