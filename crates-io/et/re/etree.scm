(define-module (crates-io et re etree) #:use-module (crates-io))

(define-public crate-etree-0.1.0 (c (n "etree") (v "0.1.0") (d (list (d (n "eval") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0798r4nppqzzlqmhxvmkxgp6pcwd7cj77121a78wq14lnvb4i8y1")))

(define-public crate-etree-0.1.1 (c (n "etree") (v "0.1.1") (d (list (d (n "eval") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1l5qsza9pz5bp8my5wqf5lsx6frggkz2wzg71k76fng6k5a7i7yg")))

(define-public crate-etree-0.2.0 (c (n "etree") (v "0.2.0") (d (list (d (n "eval") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09ihgx6rhx9b7yqlr7inl9ffiji6wgawavf6hsnblg11mn7mlnha")))

(define-public crate-etree-0.2.1 (c (n "etree") (v "0.2.1") (d (list (d (n "eval") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0sfx9zs6xing21qszkgf45msbfl6spr1d72qyqk7m3lbi4fjfcbv")))

(define-public crate-etree-0.2.2 (c (n "etree") (v "0.2.2") (d (list (d (n "eval") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17a7xf8gk3r7nb1yxc56j55db6y76yf9h9n7jlcwr33lxbjly4i7")))

(define-public crate-etree-0.3.0 (c (n "etree") (v "0.3.0") (d (list (d (n "eval") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ipyswk7b5clg32kq3nvksflr5wcms0yin6b4q6vgihy3f9klg1m")))

(define-public crate-etree-0.3.1 (c (n "etree") (v "0.3.1") (d (list (d (n "eval") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wbahsr7s59anq2g727gfbhgx4m3qdlwphya7a4kzyz2q8bk6vsr")))

(define-public crate-etree-0.3.2 (c (n "etree") (v "0.3.2") (d (list (d (n "eval") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "03iyj6rgszzibb78p2h9bqajsgb7xpracdv2g59rbz4w8p9hacpj")))

