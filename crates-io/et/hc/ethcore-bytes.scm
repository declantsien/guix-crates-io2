(define-module (crates-io et hc ethcore-bytes) #:use-module (crates-io))

(define-public crate-ethcore-bytes-0.1.0 (c (n "ethcore-bytes") (v "0.1.0") (h "1mas041j2swsk70fx3kc79nbsimpqgj0i8ngqzhj4p3crmrcfxrr")))

(define-public crate-ethcore-bytes-0.1.1 (c (n "ethcore-bytes") (v "0.1.1") (h "1sg25f0nrcmlp107f69603hkcdzyv5gn2q1n4m7fry17wvhyymz9")))

