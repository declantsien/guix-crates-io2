(define-module (crates-io et hc ethcontract-generate-fork) #:use-module (crates-io))

(define-public crate-ethcontract-generate-fork-0.25.6 (c (n "ethcontract-generate-fork") (v "0.25.6") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "curl") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ethcontract-common") (r "^0.25.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1sgf2z843sy3ns36j4iqmwm1q5dv7ycamci95q8wc2638qfba2b3") (f (quote (("http" "curl") ("default" "http"))))))

