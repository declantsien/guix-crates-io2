(define-module (crates-io et hc ethcontract-derive-fork) #:use-module (crates-io))

(define-public crate-ethcontract-derive-fork-0.25.6 (c (n "ethcontract-derive-fork") (v "0.25.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ethcontract-common") (r "^0.25.6") (d #t) (k 0)) (d (n "ethcontract-generate-fork") (r "^0.25.6") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1aj0rhk4p5s9w63xahjj2i76fik1yj0ab7hl2iljzg9kyys7vgfi") (f (quote (("http" "ethcontract-generate-fork/http") ("default" "http"))))))

