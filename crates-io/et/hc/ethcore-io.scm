(define-module (crates-io et hc ethcore-io) #:use-module (crates-io))

(define-public crate-ethcore-io-1.12.0 (c (n "ethcore-io") (v "1.12.0") (d (list (d (n "crossbeam-deque") (r "^0.6") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6.19") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1hnnzybmspnmklgwr1j930cbfwnzviz7v9nypgvj9i9p6swb8d7h")))

