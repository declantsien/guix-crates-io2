(define-module (crates-io et hc ethcore-bigint) #:use-module (crates-io))

(define-public crate-ethcore-bigint-0.1.0 (c (n "ethcore-bigint") (v "0.1.0") (d (list (d (n "heapsize") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "1sy6gj46p1xs4flwlb0dlbgxqbbq6ajyinh8d1wlnlsk7dkfm26z") (f (quote (("x64asm_arithmetic") ("rust_arithmetic"))))))

(define-public crate-ethcore-bigint-0.1.1 (c (n "ethcore-bigint") (v "0.1.1") (d (list (d (n "heapsize") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "1gqlck3vncgaabhm3y3fdwg8ryrnxc2wqwvyv8741cs2n0qzfbs0") (f (quote (("x64asm_arithmetic") ("rust_arithmetic"))))))

(define-public crate-ethcore-bigint-0.1.2 (c (n "ethcore-bigint") (v "0.1.2") (d (list (d (n "bigint") (r "^1.0") (d #t) (k 0)) (d (n "heapsize") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "17ffgas9qv1i0grwq70lq6r8m8xmfd69rpmdxwgkxlyz22yl653f") (f (quote (("x64asm_arithmetic") ("rust_arithmetic"))))))

(define-public crate-ethcore-bigint-0.1.3 (c (n "ethcore-bigint") (v "0.1.3") (d (list (d (n "bigint") (r "^1.0.4") (d #t) (k 0)) (d (n "heapsize") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "180c7qbjl5hyv64a0jnzx43s4xmb1rdq7p60yiw8snl2mw0768sx") (f (quote (("x64asm_arithmetic") ("rust_arithmetic"))))))

(define-public crate-ethcore-bigint-0.2.0 (c (n "ethcore-bigint") (v "0.2.0") (d (list (d (n "bigint") (r "^4.0") (d #t) (k 0)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "plain_hasher") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "00mpp1mrk5rmvkg1xfqm1zggmn92jpczrzydywvi1fd4v84m35hy") (f (quote (("x64asm_arithmetic") ("rust_arithmetic") ("heapsizeof" "heapsize" "bigint/heapsizeof"))))))

(define-public crate-ethcore-bigint-0.2.1 (c (n "ethcore-bigint") (v "0.2.1") (d (list (d (n "bigint") (r "^4.0") (d #t) (k 0)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "plain_hasher") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "1d8y53allg7pj4hc0ndly671g0mw6yf0czikqglp13sawxvszddw") (f (quote (("x64asm_arithmetic") ("std" "bigint/std") ("rust_arithmetic") ("heapsizeof" "heapsize" "bigint/heapsizeof"))))))

