(define-module (crates-io et hc ethcore-logger) #:use-module (crates-io))

(define-public crate-ethcore-logger-1.9.0 (c (n "ethcore-logger") (v "1.9.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0nscv89hakrxx687nqb0mgs8ysymi187fkhkgnz30q2l94z83m8z")))

(define-public crate-ethcore-logger-1.12.0 (c (n "ethcore-logger") (v "1.12.0") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1v4zlh7d8z9f4drf7z99ign4rrkm8aqf0vavdvhayvgd2k64nvhf")))

