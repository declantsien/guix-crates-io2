(define-module (crates-io et ho ethox-io-uring) #:use-module (crates-io))

(define-public crate-ethox-io-uring-0.0.2 (c (n "ethox-io-uring") (v "0.0.2") (d (list (d (n "ethox") (r "^0.0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "io-uring") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (k 0)))) (h "1x478zq1m8dv51nhiw55ghiigbprxsgbkb0h7810fbj0invqkm33")))

