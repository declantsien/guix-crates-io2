(define-module (crates-io et ho ethock) #:use-module (crates-io))

(define-public crate-ethock-0.0.3 (c (n "ethock") (v "0.0.3") (d (list (d (n "rouille") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vg0fdnv809lbrhnb0cwi9g94hm03ba61sixmiq80gh3yfz8pbsi")))

(define-public crate-ethock-0.1.0 (c (n "ethock") (v "0.1.0") (d (list (d (n "jsonrpc-core") (r "^17.0.0") (d #t) (k 0)) (d (n "jsonrpc-http-server") (r "^17.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 2)))) (h "03d46wwk31k87fbbmr24hybf0vjsxrz32h392283a55r21gnlpx7")))

