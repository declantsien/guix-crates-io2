(define-module (crates-io et ho ethox) #:use-module (crates-io))

(define-public crate-ethox-0.0.1-wip (c (n "ethox") (v "0.0.1-wip") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "05j4s8h2v6w3w9h6x98fsdb90wv55mb3ykgnlgs7jqvrjs4kgxly")))

(define-public crate-ethox-0.0.1 (c (n "ethox") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "structopt") (r "^0.2") (k 2)))) (h "1kdsxl06r1c14x40vy2yfpwfxz1a4z2vc0gndlmi99fwc4ni6b0j") (f (quote (("sys" "libc") ("std" "alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-ethox-0.0.2 (c (n "ethox") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "structopt") (r "^0.2") (k 2)))) (h "1n77vpl1mz1vw85mf965sff20yr8w2mvwr0d8mf2rg0kawyc9rcg") (f (quote (("sys" "libc") ("std" "alloc") ("default" "alloc") ("alloc"))))))

