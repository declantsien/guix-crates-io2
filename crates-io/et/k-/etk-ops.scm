(define-module (crates-io et k- etk-ops) #:use-module (crates-io))

(define-public crate-etk-ops-0.3.0 (c (n "etk-ops") (v "0.3.0") (d (list (d (n "educe") (r "^0.4.19") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 1)) (d (n "quote") (r "^1.0.18") (d #t) (k 1)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 1)) (d (n "snafu") (r "^0.7.1") (f (quote ("std"))) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 1)))) (h "128kjik0jq1f8ca003cdq9r2gv9zg92s6ilkb8zjfy92bisqp2nh") (f (quote (("backtraces" "snafu/backtraces"))))))

