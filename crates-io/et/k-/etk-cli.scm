(define-module (crates-io et k- etk-cli) #:use-module (crates-io))

(define-public crate-etk-cli-0.1.0 (c (n "etk-cli") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0d1ifgk3rrcgmrn620429z70xwzinzb2mifz7bzv4rq5110lqkcy")))

(define-public crate-etk-cli-0.2.1 (c (n "etk-cli") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "snafu") (r "^0.7.1") (f (quote ("std"))) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0gcf47bxq6y2vxcfsvpy9g19f03jgq45adgb8npd65w54z3zn3v0")))

(define-public crate-etk-cli-0.3.0 (c (n "etk-cli") (v "0.3.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "snafu") (r "^0.7.1") (f (quote ("std"))) (k 0)))) (h "0985il78jkqj5a31d1p6kzd558i2mw93mrwnwz22i894182kla05")))

