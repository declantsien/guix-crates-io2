(define-module (crates-io et ym etym) #:use-module (crates-io))

(define-public crate-etym-0.0.5 (c (n "etym") (v "0.0.5") (d (list (d (n "clap") (r "^3.1.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (f (quote ("terminal_size"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0x6vjw61qmg5lv4134nssm5v7lwzn2r0d93jbwdj1sxg9wsszqan")))

(define-public crate-etym-0.0.6 (c (n "etym") (v "0.0.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scraper") (r "^0.13") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "0rdm5nl4szbmvr89dpykjrjaw4nhp6j2fy1r7pfyjcqkxsxxkngn")))

(define-public crate-etym-0.0.7 (c (n "etym") (v "0.0.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "scraper") (r "^0.15") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "10a0nynbn8z6d6ggw0xi9djic287iqsdw7s37ahm64dnd025cfsc")))

