(define-module (crates-io et h2 eth2_serde_utils) #:use-module (crates-io))

(define-public crate-eth2_serde_utils-0.1.0 (c (n "eth2_serde_utils") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 2)))) (h "0988w4id8p1477y8fw9kl5v9d1y68iizjww251kcipwhak1gyzs7")))

