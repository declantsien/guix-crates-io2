(define-module (crates-io et h2 eth2_ssz_derive) #:use-module (crates-io))

(define-public crate-eth2_ssz_derive-0.1.0 (c (n "eth2_ssz_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0rcq3rcxpiqsry71fbmc1fnb4117vvw4k9p62ai52v6cz67mr9mk")))

(define-public crate-eth2_ssz_derive-0.2.0 (c (n "eth2_ssz_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "1v25bbal9wmdx0gh9wmmfiyycyckdmzwb0xkbh1mdn8s6i6b8rdr")))

(define-public crate-eth2_ssz_derive-0.2.1 (c (n "eth2_ssz_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "13c06k7h5rzaj414b2d1ysw545kbgk56qlnz5nmlcwkvpsg2p08j")))

(define-public crate-eth2_ssz_derive-0.3.0 (c (n "eth2_ssz_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "1jczympyyfrf6l0c9bqv3bjr6p6nccbiwmws87kp3fs1r798cnv3")))

