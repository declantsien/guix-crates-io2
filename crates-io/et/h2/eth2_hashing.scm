(define-module (crates-io et h2 eth2_hashing) #:use-module (crates-io))

(define-public crate-eth2_hashing-0.1.0 (c (n "eth2_hashing") (v "0.1.0") (d (list (d (n "ring") (r "^0.14.6") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.2.47") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1bhzfl0yv7vqkqs4wd48nnkjp9hjh7j4cpbiks539spdy9lw9y67")))

(define-public crate-eth2_hashing-0.2.0 (c (n "eth2_hashing") (v "0.2.0") (d (list (d (n "cpufeatures") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.19") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "05w55yh8gwjfjv8p99kwz4p4pk0bssskwn4x7n19wxp3yxyp6rsv") (f (quote (("zero_hash_cache" "lazy_static") ("default" "zero_hash_cache"))))))

