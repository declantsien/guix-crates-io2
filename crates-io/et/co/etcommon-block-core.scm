(define-module (crates-io et co etcommon-block-core) #:use-module (crates-io))

(define-public crate-etcommon-block-core-0.1.0 (c (n "etcommon-block-core") (v "0.1.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "13l90bznj9yhqal1glijbi86cbhjgzrpmrjhm88p9b8a2b28w72c") (f (quote (("std" "etcommon-bigint/std" "etcommon-rlp/std") ("default" "std"))))))

(define-public crate-etcommon-block-core-0.2.0 (c (n "etcommon-block-core") (v "0.2.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (f (quote ("rlp" "string"))) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "06ms4vijdps6ajj23s3qnlab9dkj81gik3jvw97v0r8v6gsdqsr4") (f (quote (("std" "etcommon-bigint/std" "etcommon-rlp/std") ("default" "std"))))))

(define-public crate-etcommon-block-core-0.3.0 (c (n "etcommon-block-core") (v "0.3.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (f (quote ("rlp" "string"))) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0x3kbj0ddxf0m49ra0wwy9m4kcp72ckkw7dxqkwisbj60q745vny") (f (quote (("std" "etcommon-bigint/std" "etcommon-rlp/std") ("default" "std"))))))

(define-public crate-etcommon-block-core-0.3.1 (c (n "etcommon-block-core") (v "0.3.1") (d (list (d (n "etcommon-bigint") (r "^0.2") (f (quote ("rlp" "string"))) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0iqxvd7l13cz5cqqy7s33x19y8v3cnqyq2kwjc2smgm6yszq8mj9") (f (quote (("std" "etcommon-bigint/std" "etcommon-rlp/std") ("default" "std"))))))

