(define-module (crates-io et co etcommon-hexutil) #:use-module (crates-io))

(define-public crate-etcommon-hexutil-0.2.0 (c (n "etcommon-hexutil") (v "0.2.0") (h "0bw3d5j6mvajbadvq5k1wd2la1s3w6h22wmda8kc0j5n70i9zbq1")))

(define-public crate-etcommon-hexutil-0.2.1 (c (n "etcommon-hexutil") (v "0.2.1") (h "1ybnavafhy0njh6fc6r30rw6kn32kcr1girkfk4z0zjijg2c3m6g")))

(define-public crate-etcommon-hexutil-0.2.2 (c (n "etcommon-hexutil") (v "0.2.2") (h "1cwv9wzh2jgmbkhxq0ngwzb94gc435gfzc6mcl4nyji0knqxvkf9")))

(define-public crate-etcommon-hexutil-0.2.3 (c (n "etcommon-hexutil") (v "0.2.3") (h "12lz576lqdw2fxidnp86wyva12mp17g0lr3nmzh11lh5c20xw17h") (f (quote (("std") ("default" "std"))))))

(define-public crate-etcommon-hexutil-0.2.4 (c (n "etcommon-hexutil") (v "0.2.4") (h "0k02304abwhnayv775fz89g21vxln609h66rl9mq12zq7f9x3d10") (f (quote (("std") ("default" "std"))))))

