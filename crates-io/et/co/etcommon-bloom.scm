(define-module (crates-io et co etcommon-bloom) #:use-module (crates-io))

(define-public crate-etcommon-bloom-0.1.0 (c (n "etcommon-bloom") (v "0.1.0") (d (list (d (n "etcommon-bigint") (r "^0.1") (d #t) (k 0)) (d (n "etcommon-crypto") (r "^0.1") (d #t) (k 0)) (d (n "etcommon-rlp") (r "^0.1.1") (d #t) (k 0)) (d (n "etcommon-util") (r "^0.1") (d #t) (k 0)))) (h "0x9f25wfkhm9vcpk59v8rffjg6imkgmkywcwh9yp47cz9r0zk3fk")))

(define-public crate-etcommon-bloom-0.1.1 (c (n "etcommon-bloom") (v "0.1.1") (d (list (d (n "etcommon-bigint") (r "^0.1") (d #t) (k 0)) (d (n "etcommon-crypto") (r "^0.1") (d #t) (k 0)) (d (n "etcommon-rlp") (r "^0.1.1") (d #t) (k 0)) (d (n "etcommon-util") (r "^0.1") (d #t) (k 0)))) (h "1vpxm59ksncq5v7a7gcngjqiybr203rrx2b7mnil7v8y7shcgsa1")))

(define-public crate-etcommon-bloom-0.2.0 (c (n "etcommon-bloom") (v "0.2.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "03wah9ws1hii84a2blx12zmqc0nzwldk117ywq1jpmn6z3swhzsr")))

(define-public crate-etcommon-bloom-0.2.1 (c (n "etcommon-bloom") (v "0.2.1") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1bvq298j59dpcid8gxp789fhgm7gkd8bs6p1z48w178kxdw4g123")))

