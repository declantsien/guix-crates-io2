(define-module (crates-io et co etcommon-hash) #:use-module (crates-io))

(define-public crate-etcommon-hash-0.1.0 (c (n "etcommon-hash") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "secp256k1") (r "^0.6") (d #t) (k 0)))) (h "03878cc27539cvd5606w1q62acyg088k7dqz9m93nlqi8696srg3")))

(define-public crate-etcommon-hash-0.1.1 (c (n "etcommon-hash") (v "0.1.1") (d (list (d (n "devp2p-secp256k1") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "0b20nrigl0pqdb12869prd34dk3lll145cn87c1sq8xs4qy0d83v")))

