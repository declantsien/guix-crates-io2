(define-module (crates-io et co etcommon-trie-test) #:use-module (crates-io))

(define-public crate-etcommon-trie-test-0.3.8 (c (n "etcommon-trie-test") (v "0.3.8") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0akaj5qagfmzgza4vfglaqxl2jm5b3z3qls3w436ih4fw7y3022j")))

(define-public crate-etcommon-trie-test-0.3.9 (c (n "etcommon-trie-test") (v "0.3.9") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "10l4wy57cf5b6lq0l99h5i0043py9q3abs7nfyjb89ni600nxzr4")))

(define-public crate-etcommon-trie-test-0.3.11 (c (n "etcommon-trie-test") (v "0.3.11") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0mr4a1d8dwir7zrjbkl1wis4pwjnss9m4xxhv8ibzp3ai9l4irl8")))

(define-public crate-etcommon-trie-test-0.3.12 (c (n "etcommon-trie-test") (v "0.3.12") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "107ms1b7rk4703bamzdzjllm56mp6zk3msy9v65fi7prd0ry5mkm")))

