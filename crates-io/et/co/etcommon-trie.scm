(define-module (crates-io et co etcommon-trie) #:use-module (crates-io))

(define-public crate-etcommon-trie-0.2.0 (c (n "etcommon-trie") (v "0.2.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0livxyiacs8lzgpya1fdg0nx4031qacpyxkk4mh7bcrhp4ifm111")))

(define-public crate-etcommon-trie-0.2.1 (c (n "etcommon-trie") (v "0.2.1") (d (list (d (n "blockchain") (r "^0.1") (d #t) (k 0)) (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "10506ha5iw8x4lrhm9g089183x2bcmiicfaz4cwk5f91jcz3cr9k")))

(define-public crate-etcommon-trie-0.2.2 (c (n "etcommon-trie") (v "0.2.2") (d (list (d (n "blockchain") (r "^0.1") (d #t) (k 0)) (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1wj3lswd90fn4m4knnxs63nn007ixkzaljrzhkn5ddbf2kz24crr")))

(define-public crate-etcommon-trie-0.2.3 (c (n "etcommon-trie") (v "0.2.3") (d (list (d (n "blockchain") (r "^0.1") (d #t) (k 0)) (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1pp0q5gx5dw6wqqfwzwl1fnvwv8h4vsphi8aw6mvkfhg3fw65scc")))

(define-public crate-etcommon-trie-0.2.4 (c (n "etcommon-trie") (v "0.2.4") (d (list (d (n "blockchain") (r "^0.1") (d #t) (k 0)) (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0gfp1b5swwxiwa3gf1iba6bsj5xp1d2996wqjy613qb3ca9qj5fn")))

(define-public crate-etcommon-trie-0.2.5 (c (n "etcommon-trie") (v "0.2.5") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "08bc25g9fdc4midp3xfcnjwnsfskcmb14xl7ccgb5ngmprd0k3bx")))

(define-public crate-etcommon-trie-0.2.6 (c (n "etcommon-trie") (v "0.2.6") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "058ax45556d0svzcps2wh1yjs22xjz2mwwflq2qa59fgz3k1sggb")))

(define-public crate-etcommon-trie-0.2.7 (c (n "etcommon-trie") (v "0.2.7") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1hgx00r8jd325yy8mdyfx55i6z0g9s2la29rk3cs2j8585pxd0f2")))

(define-public crate-etcommon-trie-0.2.8 (c (n "etcommon-trie") (v "0.2.8") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0jl69czy757h07gjvdldx449kq7dsmpzvpj3w5klkk28ydib0jq1")))

(define-public crate-etcommon-trie-0.3.0 (c (n "etcommon-trie") (v "0.3.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1ndy0v0hz83x4prhfr3xp1ac0s91j63g15jfl0ip31s8flbfi2sa")))

(define-public crate-etcommon-trie-0.3.1 (c (n "etcommon-trie") (v "0.3.1") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "02iwflm6qq8r05jkjg5dhiv6snc9pzd7v7p7xlhnjp5zymrhblxf")))

(define-public crate-etcommon-trie-0.3.2 (c (n "etcommon-trie") (v "0.3.2") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1ipjb6nssllhq7ys8ng2f9n30yc0fb8pfbvbq76f93b8w5krzkg2")))

(define-public crate-etcommon-trie-0.3.3 (c (n "etcommon-trie") (v "0.3.3") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0davi0szzb3xmap8azgiby82jnkjklxihsm6ns8hd59h9q3knf3d")))

(define-public crate-etcommon-trie-0.3.4 (c (n "etcommon-trie") (v "0.3.4") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1wy7ydavck5gcxfi6fr17y239a7bb96dpglibwpvvb47f4afp62f")))

(define-public crate-etcommon-trie-0.3.5 (c (n "etcommon-trie") (v "0.3.5") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0whx2bz4nxszwdfpfka2afvga1gyldvb58kih2znpnadd87rhb66")))

(define-public crate-etcommon-trie-0.3.6 (c (n "etcommon-trie") (v "0.3.6") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1jccfar0wjfjdi2473iay7bwcrblsk4nnl1mhv877qayxkrwyd59")))

(define-public crate-etcommon-trie-0.3.7 (c (n "etcommon-trie") (v "0.3.7") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0mkaznm8zkjw066brsc01i664pnd6dfxba1qbqylq60h3l41gxrh")))

(define-public crate-etcommon-trie-0.3.8 (c (n "etcommon-trie") (v "0.3.8") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1d0nmkwdpcf2dfm0z3mfy4ygmv3mwsnpah9zdqspf5r8wwji56ka")))

(define-public crate-etcommon-trie-0.3.10 (c (n "etcommon-trie") (v "0.3.10") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1a0zq3nqlafsykk8qgd8cayak2jg3wxr5j2w68y6h53afaycm4x7") (y #t)))

(define-public crate-etcommon-trie-0.3.11 (c (n "etcommon-trie") (v "0.3.11") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0n8by2b8380pfx1725whiw3s3fq0r6zdaf18x7b9gv91xfccfz26")))

(define-public crate-etcommon-trie-0.4.0 (c (n "etcommon-trie") (v "0.4.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (d #t) (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "etcommon-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1jrhchqibbx698fmh7g36xc404qjly365zzfvz41qfg56hkizxf2")))

