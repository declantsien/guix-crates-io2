(define-module (crates-io et co etcommon-rlp) #:use-module (crates-io))

(define-public crate-etcommon-rlp-0.1.0 (c (n "etcommon-rlp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0qfw4zr4b017h713hkm55j7r44sy2misznckiklcwz44j3qp64mj")))

(define-public crate-etcommon-rlp-0.1.1 (c (n "etcommon-rlp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "19s76yx345dc4s0pan9dyr4bxmrnmlmqrczmqllnbgcbqa92xdfq")))

(define-public crate-etcommon-rlp-0.2.0 (c (n "etcommon-rlp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "06dva0nrjs4v259hyg09f4iyfgfpf5li0qx5vj3xz2ndp27bwjbc")))

(define-public crate-etcommon-rlp-0.2.1 (c (n "etcommon-rlp") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "179a0jfgcv360c6nx7flrvh01qihgjvnqxhxsgkdl58qls846pmg")))

(define-public crate-etcommon-rlp-0.2.2 (c (n "etcommon-rlp") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array-plus") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1bf51m89ihlvvbj32wf07cn01hf6xb9nzw790n6m37nyi3v0kpp8")))

(define-public crate-etcommon-rlp-0.2.3 (c (n "etcommon-rlp") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "elastic-array-plus") (r "^0.9.1") (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (k 0)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0m3j65l8dsfinj52p2396b1f21ay2fck8h29g8y9czvi0cdsd3jd") (f (quote (("std" "etcommon-hexutil/std" "byteorder/std" "lazy_static" "elastic-array-plus/std") ("default" "std"))))))

(define-public crate-etcommon-rlp-0.2.4 (c (n "etcommon-rlp") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "elastic-array-plus") (r "^0.10.0") (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (k 0)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)))) (h "11gym9q39ln5vj9jzqpsdmjad0znaxn5f1jwqa72n0kqxlpr6jl0") (f (quote (("std" "etcommon-hexutil/std" "byteorder/std" "lazy_static" "elastic-array-plus/std") ("default" "std"))))))

(define-public crate-etcommon-rlp-0.2.5 (c (n "etcommon-rlp") (v "0.2.5") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "elastic-array-plus") (r "^0.10.0") (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1jwvyc0h9h6k9kx3jbk9p42bw4r36765apd1ld2dm5ydaks8x5wc") (f (quote (("std" "etcommon-hexutil/std" "byteorder/std" "lazy_static" "elastic-array-plus/std") ("default" "std"))))))

