(define-module (crates-io et co etcommon) #:use-module (crates-io))

(define-public crate-etcommon-0.1.0 (c (n "etcommon") (v "0.1.0") (h "1p00nnzdg3254bgmphh7v9q3r6sm3vrad45nfig2cxk81ypn8f4j")))

(define-public crate-etcommon-0.1.1 (c (n "etcommon") (v "0.1.1") (d (list (d (n "etcommon-bigint") (r "^0.1.0") (d #t) (k 0)) (d (n "etcommon-rlp") (r "^0.1.0") (d #t) (k 0)) (d (n "etcommon-util") (r "^0.1.0") (d #t) (k 0)))) (h "0fqcqy6x5qpjjiv8yhkbim2857zzgp56hfa3bqa9xyffzc8pjmm8")))

