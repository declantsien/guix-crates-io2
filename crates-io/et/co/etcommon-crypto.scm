(define-module (crates-io et co etcommon-crypto) #:use-module (crates-io))

(define-public crate-etcommon-crypto-0.1.1 (c (n "etcommon-crypto") (v "0.1.1") (d (list (d (n "devp2p-secp256k1") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1sg8nwkk6j4ah4hm6ak2kia67qjx94n6iipzrkszrwhxg4gqibdy")))

(define-public crate-etcommon-crypto-0.1.2 (c (n "etcommon-crypto") (v "0.1.2") (d (list (d (n "devp2p-secp256k1") (r "^0.5") (d #t) (k 0)) (d (n "digest") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "etcommon-bigint") (r "^0.1.14") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0g3ksrl30zmvwc72p7wsxwdpgc737frhhcvcsj9f3s21pcngr096")))

