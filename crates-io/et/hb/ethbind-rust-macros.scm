(define-module (crates-io et hb ethbind-rust-macros) #:use-module (crates-io))

(define-public crate-ethbind-rust-macros-0.1.2 (c (n "ethbind-rust-macros") (v "0.1.2") (d (list (d (n "ethbind-rust") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "07fq11kd4p0w9as2pb3jgxllf7y7jbhwd7l5l4pz2m74wmq4qkff")))

(define-public crate-ethbind-rust-macros-0.1.3 (c (n "ethbind-rust-macros") (v "0.1.3") (d (list (d (n "ethbind-rust") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "15pjf87pjc5f6as5kr5zrvjxw5l2nj84mqkx3bx5msanc1xjr462")))

(define-public crate-ethbind-rust-macros-0.1.4 (c (n "ethbind-rust-macros") (v "0.1.4") (d (list (d (n "ethbind-rust") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0x0dkzvl5kn3j4p0j94njmwm4n5dyhbq4axil7053xw8kkjfai45")))

(define-public crate-ethbind-rust-macros-0.1.5 (c (n "ethbind-rust-macros") (v "0.1.5") (d (list (d (n "ethbind-rust") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "139ficpsx2g9j5qnhv75gkm5ns43yl7vcrz567hjkxfn4ch7qpnc")))

(define-public crate-ethbind-rust-macros-0.1.6 (c (n "ethbind-rust-macros") (v "0.1.6") (d (list (d (n "ethbind-rust") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0c9m64apk0pkwlrxzvv81hghnpj2s5zcfgdfd0912iwa4bwqr530")))

