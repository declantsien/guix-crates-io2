(define-module (crates-io et hb ethbind-gen) #:use-module (crates-io))

(define-public crate-ethbind-gen-0.1.0 (c (n "ethbind-gen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethbind-json") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g8xmvkwrjn59hbi2p45baj1lyag3miyc78vk11nbwamqs9388ng")))

(define-public crate-ethbind-gen-0.1.1 (c (n "ethbind-gen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethbind-json") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19ymy2hqxlrsqb7r728wp1qr8vf72p1hxlfm2vki0kkbdc5yd9zf")))

(define-public crate-ethbind-gen-0.1.2 (c (n "ethbind-gen") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethbind-json") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gy9y102r9zb2fv80g6w57pcrx67k4z04bzchx81lcmxbgb9b8r0")))

(define-public crate-ethbind-gen-0.1.3 (c (n "ethbind-gen") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethbind-json") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11rys6zs4y3j82ch6q0v0v8wlkg4nk9k3pzmryr40g78wdnkhpc4")))

(define-public crate-ethbind-gen-0.1.4 (c (n "ethbind-gen") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethbind-json") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "140g7f4wfp5ini3ys4g1hmb1pk1ccscxgny68lm9l87k3nkprif7")))

(define-public crate-ethbind-gen-0.1.5 (c (n "ethbind-gen") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethbind-json") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vlk017vjdqs11zqq1nlqxiim20klbklhrvvwsfg0bknp1g48lmn")))

(define-public crate-ethbind-gen-0.1.6 (c (n "ethbind-gen") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethbind-json") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0yykdgkhl13xgpalzkzk6qg72vrnjhdl4g0iw6ys1r6fvk3308mz")))

