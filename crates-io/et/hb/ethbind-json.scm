(define-module (crates-io et hb ethbind-json) #:use-module (crates-io))

(define-public crate-ethbind-json-0.1.0 (c (n "ethbind-json") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "050nb34ja97180q5xyqdz7qflz95ai0px1j91mmf640795gzr3zp")))

(define-public crate-ethbind-json-0.1.1 (c (n "ethbind-json") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "1i6iwc7c12mfzmap66g0z10jgld3vh6fh7hyp6fpc7shd4pmxxxq")))

(define-public crate-ethbind-json-0.1.2 (c (n "ethbind-json") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "1qw83bs754xp975z8pfvlkf71p34l4m57fv4hlilkm27crm5d4dx")))

(define-public crate-ethbind-json-0.1.3 (c (n "ethbind-json") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "017alacsw0w1v0drfh0vblwmxfd7g2al1izzhzzd5zyf8cairngf")))

(define-public crate-ethbind-json-0.1.4 (c (n "ethbind-json") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "13a8ik609rp1ryk4dvq2i135fq3gjrj1c0mycw9js96kjhh3pjni")))

(define-public crate-ethbind-json-0.1.5 (c (n "ethbind-json") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "04zqqz56y4iiq9j8bxlqifhdr254gvwhli8f3ymcbpkhf8zs5kks")))

(define-public crate-ethbind-json-0.1.6 (c (n "ethbind-json") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "0p23z8b7akc64ryiyaf5743bqnzigj06gmz4fby143cjxv086ffi")))

