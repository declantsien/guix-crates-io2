(define-module (crates-io et f- etf-sdk) #:use-module (crates-io))

(define-public crate-etf-sdk-0.1.0-dev (c (n "etf-sdk") (v "0.1.0-dev") (d (list (d (n "ark-bls12-381") (r "^0.4.0") (f (quote ("curve"))) (k 0)) (d (n "ark-ec") (r "^0.4.0") (k 0)) (d (n "ark-serialize") (r "^0.4.0") (k 0)) (d (n "ark-std") (r "^0.4.0") (k 0)) (d (n "etf-crypto-primitives") (r "^0.2.0") (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "1c31vgp1wkb322wq8iwv22kgbl3v165hja7zh1xm453ijmij569n")))

