(define-module (crates-io et ag etag) #:use-module (crates-io))

(define-public crate-etag-0.1.0 (c (n "etag") (v "0.1.0") (h "17hc09qqvilvhafbxaw8hg4l3fnjmjlib5q7hcj3myws95k3nxg9")))

(define-public crate-etag-0.2.0 (c (n "etag") (v "0.2.0") (h "0jry4b8b4g6k36c91wayzx56snb0b0sqhl09sgmw31h7vfv836v6")))

(define-public crate-etag-1.0.0 (c (n "etag") (v "1.0.0") (h "0v06lrv9is7s89ylicdz05kzdcqwkdazdmasxj23msfbsfkkc6xg")))

(define-public crate-etag-1.0.1 (c (n "etag") (v "1.0.1") (h "0i7gbyvqi14zz8whfpmk5p9njpbp4yg59wv7l15d7g1hyada0q6j")))

(define-public crate-etag-1.0.2 (c (n "etag") (v "1.0.2") (h "1c7xgcimz48q0nrcncc0h1v232ljjf951mll6ld8viihz5cpk1s7")))

(define-public crate-etag-1.0.3 (c (n "etag") (v "1.0.3") (h "02ffy0cgp7f9aphlpvd2qcy0nsq5g1kw6xpyh5z8r0062f50agck")))

(define-public crate-etag-2.0.0 (c (n "etag") (v "2.0.0") (d (list (d (n "wy") (r "^1") (d #t) (k 0)))) (h "0vcma93a0nv9y4zm4y3cgpk50afbqp5agglk11cgr0b9p03kw4xy") (f (quote (("std"))))))

(define-public crate-etag-2.0.1 (c (n "etag") (v "2.0.1") (d (list (d (n "wy") (r "^1") (d #t) (k 0)))) (h "0dymr1dhb1rc4s3yp50pr39gy5gkk34larr95pw2rbqdq7j1qvif") (f (quote (("std"))))))

(define-public crate-etag-2.0.2 (c (n "etag") (v "2.0.2") (d (list (d (n "wy") (r "^1") (d #t) (k 0)))) (h "1568qb4py1iw82shlpwwjjbvmpwlb34pl3rq7v7dxrr4ng88k2ih") (f (quote (("std"))))))

(define-public crate-etag-2.0.3 (c (n "etag") (v "2.0.3") (d (list (d (n "wy") (r "^1") (d #t) (k 0)))) (h "0r3nzxs62fhzjm3wk3qpb2kn7jvjh2z03mjl1abdh5s6kk4kfd2y") (f (quote (("std"))))))

(define-public crate-etag-2.0.4 (c (n "etag") (v "2.0.4") (d (list (d (n "str-buf") (r "^1") (d #t) (k 0)) (d (n "wy") (r "^1") (d #t) (k 0)))) (h "0j67mc7hvwn9ra2idi4bmp89z5bdskwm55agihx3idqrfm7i7iw2") (f (quote (("std")))) (y #t)))

(define-public crate-etag-2.0.5 (c (n "etag") (v "2.0.5") (d (list (d (n "str-buf") (r "^1") (d #t) (k 0)) (d (n "wy") (r "^1") (d #t) (k 0)))) (h "0dnh0ily8nxy91nbf3rhfvxxk3hgzlwmh9gi0q1kg0hyb6dzs2bc") (f (quote (("std"))))))

(define-public crate-etag-2.0.6 (c (n "etag") (v "2.0.6") (d (list (d (n "str-buf") (r "^2") (d #t) (k 0)) (d (n "wy") (r "^1") (d #t) (k 0)))) (h "0a00mn0nnlm5w45hzh3mmvddzylzxq66va735yvqk6q4qm41q3nb") (f (quote (("std"))))))

(define-public crate-etag-3.0.0 (c (n "etag") (v "3.0.0") (d (list (d (n "str-buf") (r "^2") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("const_xxh3" "xxh3"))) (d #t) (k 0)))) (h "13yqr7pgwvxmj01cpakvian9b8kj3fh3vqv861gx66y9is8zijdf") (f (quote (("std"))))))

(define-public crate-etag-4.0.0 (c (n "etag") (v "4.0.0") (d (list (d (n "str-buf") (r "^3") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("const_xxh3" "xxh3"))) (d #t) (k 0)))) (h "0i5zfnl71harw6g15ivnvmpysnbrf6dlx0qbp9nc5pfcl9hhcgab") (f (quote (("std"))))))

