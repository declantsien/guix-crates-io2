(define-module (crates-io et ag etagere) #:use-module (crates-io))

(define-public crate-etagere-0.1.0 (c (n "etagere") (v "0.1.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4") (d #t) (k 0)))) (h "1vicw04bic1whmbw3kwmdh40yd20zhifihkghw6nnwp28fb6fqi6") (f (quote (("serialization" "serde" "euclid/serde"))))))

(define-public crate-etagere-0.2.0 (c (n "etagere") (v "0.2.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4") (d #t) (k 0)))) (h "0gk3a3qgg17xdmphylhkif8h6024gygvsgj2jjzwr82kcp7lghzi") (f (quote (("serialization" "serde" "euclid/serde"))))))

(define-public crate-etagere-0.2.1 (c (n "etagere") (v "0.2.1") (d (list (d (n "euclid") (r ">=0.22.0, <0.23.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r ">=0.4.0, <0.5.0") (d #t) (k 0)))) (h "0c9a3vix6m2x7zvmfjdvhx718iy5r5d0mfdqcczvdfbi37bsd9rp") (f (quote (("serialization" "serde" "euclid/serde"))))))

(define-public crate-etagere-0.2.2 (c (n "etagere") (v "0.2.2") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4") (d #t) (k 0)))) (h "1sp6z3573g1fa7mfk5yh2vxdijq65h5xmk4gyw1yxqci7af4gwsp") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2.3 (c (n "etagere") (v "0.2.3") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4") (d #t) (k 0)))) (h "0yzvns4jgz9zip6fpj2p82wpwi0pbja32yg3ssj0829jcwd0r2y0") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2.4 (c (n "etagere") (v "0.2.4") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4") (d #t) (k 0)))) (h "0ml3xy4903d69x2kyxmas4vzzr3w1balffn026dx0kwh83jps3aj") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2.5 (c (n "etagere") (v "0.2.5") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4") (d #t) (k 0)))) (h "0b7skbnh6xbqxk15v726vdpc61sa4k8lyrm9152lysrvh6axkc3s") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2.6 (c (n "etagere") (v "0.2.6") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4") (d #t) (k 0)))) (h "0wk2amzr0pq6wv3pnqiy8kb134q7i1lxnm14l6s2lsxvsv1nvdjy") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2.7 (c (n "etagere") (v "0.2.7") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4") (d #t) (k 0)))) (h "128wc2wjfjrsvfq1skf4mgcwr9gv3kmmnf9i5hwpydlg64d1a0b3") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2.8 (c (n "etagere") (v "0.2.8") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4") (d #t) (k 0)))) (h "0i4ngn8gbnp8nmlszg32bm54b4pf6n9h7q42w0c2jdalhxs2zwpw") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2.9 (c (n "etagere") (v "0.2.9") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4") (d #t) (k 0)))) (h "1sdk0vfqb2cvn1giyk42zsr4ybayh5a1hj2g5d1sfdd2lfg0pxsv") (f (quote (("serialization" "serde" "euclid/serde") ("checks")))) (y #t)))

(define-public crate-etagere-0.2.10 (c (n "etagere") (v "0.2.10") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4") (d #t) (k 0)))) (h "0kiy6k3683qb126kvrnks048qha1li179w5psq6vsikc3n460s9h") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

