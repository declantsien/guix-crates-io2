(define-module (crates-io et pt etptypes) #:use-module (crates-io))

(define-public crate-etptypes-1.0.0+1.2 (c (n "etptypes") (v "1.0.0+1.2") (d (list (d (n "avro-rs") (r "^0.13.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1zn0cwcc5w71nq620aqh7cnbk4j40qmmiza1f7ys2zdnairz5gk4")))

(define-public crate-etptypes-1.0.1+1.2 (c (n "etptypes") (v "1.0.1+1.2") (d (list (d (n "apache-avro") (r "^0.15.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0q9x7q25w8f98457aal0sxpnlv00nqxa0cjhrgymldvnirh6b44x")))

