(define-module (crates-io et om etoml) #:use-module (crates-io))

(define-public crate-etoml-0.1.0 (c (n "etoml") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.9.2") (f (quote ("pem"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0ih912agrg5iv9wfgzrl71564mkqvgryc747975kb3yacz38fybs")))

(define-public crate-etoml-0.2.0 (c (n "etoml") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crypto_box") (r "^0.9.1") (f (quote ("chacha20" "serde"))) (d #t) (k 0)) (d (n "generic-array") (r "^0.14.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0a9ikph0ygyhhv195m7sx04d38q7h4zpilxg949h7gv14k9059xy")))

(define-public crate-etoml-0.2.1 (c (n "etoml") (v "0.2.1") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crypto_box") (r "^0.9.1") (f (quote ("chacha20" "serde"))) (d #t) (k 0)) (d (n "generic-array") (r "^0.14.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1j04x6f6fdn4i72r4fql6pmzgrnxkghwicpw5s3h2iwzk214xfj5")))

