(define-module (crates-io et im etime) #:use-module (crates-io))

(define-public crate-etime-0.1.0 (c (n "etime") (v "0.1.0") (h "1f6izngafhzmk7rzqhz5yr9cvk4sfw1z54s4mc9p7c7bry5rj6l5")))

(define-public crate-etime-0.1.1 (c (n "etime") (v "0.1.1") (h "0ki44lq9ic3fzqhh57bkl4gjbdfxx2d8nvamk8xfi573c9h3ikbz")))

(define-public crate-etime-0.1.2 (c (n "etime") (v "0.1.2") (h "06nf6n3a7m4smfk06pm71pzj7n975a8lvi7sssmkvywg2xrhk406")))

(define-public crate-etime-0.1.3 (c (n "etime") (v "0.1.3") (h "0lcsgxr8pvk7ra9ilwgsr6dzhkjxzljg0bnwxb4p7a2m74x0vd2g")))

(define-public crate-etime-0.1.4 (c (n "etime") (v "0.1.4") (h "1vaichsqbk7wx34ih0mgiqdg2ni36xxwn26dq9afm9w31wvzvi2c")))

(define-public crate-etime-0.1.5 (c (n "etime") (v "0.1.5") (d (list (d (n "clock_source") (r "^0.1") (d #t) (k 0)))) (h "0b1663kwnb31z8dxdj75iivnxrj40knbx5bwx6mm8xb3bi0ybv7v")))

(define-public crate-etime-0.1.6 (c (n "etime") (v "0.1.6") (d (list (d (n "clock_source") (r "^0.2") (d #t) (k 0)))) (h "0wqm0ixx9frlzjmi1q053a4sx3hmz2mh07f854vs2576jcxzl8y3")))

(define-public crate-etime-0.1.7 (c (n "etime") (v "0.1.7") (d (list (d (n "clock_source") (r "^0.2.1") (d #t) (k 0)))) (h "0fsg4sa7ij8j3nkky2xkcq31b2g7136x2j9jad0kawq4rcwzj4j1")))

(define-public crate-etime-0.1.8 (c (n "etime") (v "0.1.8") (d (list (d (n "clock_source") (r "^0.2.1") (d #t) (k 0)))) (h "1vjrg105kfdmp5yl99cqzbpnpw74yw6j0ziadwi5l0kb45j2fc7a")))

