(define-module (crates-io et w- etw-reader) #:use-module (crates-io))

(define-public crate-etw-reader-0.1.0 (c (n "etw-reader") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_System_Diagnostics_Etw" "Win32_System_Diagnostics_Debug" "Win32_System_SystemInformation" "Win32_Security_Authorization" "Win32_System_Memory" "Win32_System_Time" "Win32_Foundation"))) (d #t) (k 0)))) (h "0cjjbzmj6afvqfqnb60421d7n85gxm23nlbirql37bfl6wabzqfa")))

