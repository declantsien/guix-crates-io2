(define-module (crates-io et hj ethjson) #:use-module (crates-io))

(define-public crate-ethjson-0.1.0 (c (n "ethjson") (v "0.1.0") (d (list (d (n "ethereum-types") (r "^0.8.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m5hwwmxryjjbcmhfkns4mywbgdkwqmvsa3jr0sgmkwdshrfqqkq") (f (quote (("test-helpers"))))))

