(define-module (crates-io et hd ethdigest) #:use-module (crates-io))

(define-public crate-ethdigest-0.1.0 (c (n "ethdigest") (v "0.1.0") (d (list (d (n "ethdigest-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "12gcx916c8nh5qqiqsq2w1y78d784hv9vzfvvf2n858ckanhrmj4") (f (quote (("macros" "ethdigest-macros") ("keccak" "sha3") ("default" "std")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.1.1 (c (n "ethdigest") (v "0.1.1") (d (list (d (n "ethdigest-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "14vfs8zkqca24r3fr3lc5s41nlk266d4vdlap0hz8hqfkjpbapli") (f (quote (("macros" "ethdigest-macros") ("keccak" "sha3") ("default" "std")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.2.0 (c (n "ethdigest") (v "0.2.0") (d (list (d (n "ethdigest-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "11vcfb8kaq11525l247b0iqfc5zipnd34wxy27x8brvzy6ml0yas") (f (quote (("macros" "ethdigest-macros") ("keccak" "sha3") ("default" "std")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.2.1 (c (n "ethdigest") (v "0.2.1") (d (list (d (n "ethdigest-macros") (r "=0.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "06rqckls523xcj3rdd2gxcmaipxnrlpkdiqcjgznzs2g90ydbf6p") (f (quote (("macros" "ethdigest-macros") ("keccak" "sha3") ("default" "std")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.3.0 (c (n "ethdigest") (v "0.3.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "036s5v2z96iyykmq6xwilxrkzpsb63rmfxpls0l2fgcjw5q27gfw") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.3.1 (c (n "ethdigest") (v "0.3.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "1a96sizwar7kkzsrb69lpdp38sa4l6ysqfyh42pacqkizv95rkw6") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.3.2 (c (n "ethdigest") (v "0.3.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "0qmkg9kx3q62c8gj33dhc5zagpmvl237igb224f22kmvayvhhyd3") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.4.0 (c (n "ethdigest") (v "0.4.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "sha3") (r "^0.10") (o #t) (k 0)))) (h "1k0imqkxg40pkbwarhck8d59phb6js77xa441ybwzy8zpqisvb9w") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std" "sha3?/std"))))))

