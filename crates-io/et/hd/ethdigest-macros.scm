(define-module (crates-io et hd ethdigest-macros) #:use-module (crates-io))

(define-public crate-ethdigest-macros-0.1.0 (c (n "ethdigest-macros") (v "0.1.0") (d (list (d (n "sha3") (r "^0.10") (k 0)))) (h "0w22wg194735k5b6zmlf2b86y9fbmvanv3hw5k67r949i9fg5dzb")))

(define-public crate-ethdigest-macros-0.1.1 (c (n "ethdigest-macros") (v "0.1.1") (d (list (d (n "sha3") (r "^0.10") (k 0)))) (h "1w64k9rwhff59wb5cin056vvzdn729wkwdxsl7jaid1kmd8k0lmz")))

(define-public crate-ethdigest-macros-0.2.0 (c (n "ethdigest-macros") (v "0.2.0") (d (list (d (n "sha3") (r "^0.10") (k 0)))) (h "19k3fmjg0pgw4k07c6wgbwp4s1r4gmswvbm1r4k9h8yg3w1ffbrw")))

(define-public crate-ethdigest-macros-0.2.1 (c (n "ethdigest-macros") (v "0.2.1") (d (list (d (n "sha3") (r "^0.10") (k 0)))) (h "1a0m9sci4kw8yzj9lvb1vznw1m1jvmv9pbwy58g76l09k204hhxx")))

