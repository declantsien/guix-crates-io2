(define-module (crates-io et hi ethiopic-calendar) #:use-module (crates-io))

(define-public crate-ethiopic-calendar-0.1.0 (c (n "ethiopic-calendar") (v "0.1.0") (h "1mhb3qn4yvw0wdqah6nlmj3yn8rx1m6r2cvqxwh3sdx6nl3ild5i")))

(define-public crate-ethiopic-calendar-0.1.1 (c (n "ethiopic-calendar") (v "0.1.1") (h "1xbkyiqx41dssf37ng7mdkfb9j6g65gq59kbz1xl53q33szj57qb")))

(define-public crate-ethiopic-calendar-0.1.2 (c (n "ethiopic-calendar") (v "0.1.2") (h "0w14v018qa6vxsh5s8ybn9vs6kvzbs0nyqygmly8x5z1q0k8scfv")))

(define-public crate-ethiopic-calendar-0.1.3 (c (n "ethiopic-calendar") (v "0.1.3") (h "0j16vd3lc2j2wypad4r9n50xqvdmzdmhjdlhq5kmnd5qan363zxz")))

(define-public crate-ethiopic-calendar-0.1.4 (c (n "ethiopic-calendar") (v "0.1.4") (d (list (d (n "hifitime") (r "^3.8.0") (d #t) (k 0)))) (h "0smxdayjpa78hf2ifrvf31yz8wgq261776rsgizdslkk299gc9qp")))

