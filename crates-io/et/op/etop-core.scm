(define-module (crates-io et op etop-core) #:use-module (crates-io))

(define-public crate-etop-core-0.1.0 (c (n "etop-core") (v "0.1.0") (d (list (d (n "cryo_cli") (r "^0.3.0") (d #t) (k 0)) (d (n "cryo_freeze") (r "^0.3.0") (d #t) (k 0)) (d (n "etop-format") (r "^0.1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "polars") (r "^0.35.4") (f (quote ("parquet" "string_encoding" "polars-lazy" "lazy" "binary_encoding" "json" "dtype-struct" "mode"))) (d #t) (k 0)))) (h "0783rvnlphp4kr3l6wa46ik0kf7ff00jzklxj1mbfmcysdvqvpfp")))

(define-public crate-etop-core-0.1.1 (c (n "etop-core") (v "0.1.1") (d (list (d (n "cryo_cli") (r "^0.3.0") (d #t) (k 0)) (d (n "cryo_freeze") (r "^0.3.0") (d #t) (k 0)) (d (n "etop-format") (r "^0.1.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "polars") (r "^0.35.4") (f (quote ("parquet" "string_encoding" "polars-lazy" "lazy" "binary_encoding" "json" "dtype-struct" "mode"))) (d #t) (k 0)))) (h "071lwfxs4aviy774nisr16z1dkcavy5wafh9wi0p4h6121h73rsg")))

