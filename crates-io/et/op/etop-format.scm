(define-module (crates-io et op etop-format) #:use-module (crates-io))

(define-public crate-etop-format-0.1.0 (c (n "etop-format") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "polars") (r "^0.35.4") (f (quote ("parquet" "string_encoding" "polars-lazy" "lazy" "binary_encoding" "json" "dtype-struct" "mode"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicode-truncate") (r "^0.2.0") (d #t) (k 0)))) (h "0rclyk9rgxmjk3vxwja0dshml9y2509j0yd869jxpsrxaa0gr72p")))

(define-public crate-etop-format-0.1.1 (c (n "etop-format") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "polars") (r "^0.35.4") (f (quote ("parquet" "string_encoding" "polars-lazy" "lazy" "binary_encoding" "json" "dtype-struct" "mode"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicode-truncate") (r "^0.2.0") (d #t) (k 0)))) (h "0mc8rifbvfa6ayz05n3hrj7xymhjhnriy0fh5i4mzyndqyibqygf")))

