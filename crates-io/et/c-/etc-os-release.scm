(define-module (crates-io et c- etc-os-release) #:use-module (crates-io))

(define-public crate-etc-os-release-0.1.0 (c (n "etc-os-release") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (k 0)) (d (n "indexmap") (r "^2.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (o #t) (k 0)))) (h "0aqagz5hi7k1ap423sszk6qhws2iw39qm4jn5bdmavz01ib3payk") (f (quote (("default")))) (s 2) (e (quote (("url" "dep:url") ("date" "dep:chrono")))) (r "1.70.0")))

