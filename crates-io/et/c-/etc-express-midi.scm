(define-module (crates-io et c- etc-express-midi) #:use-module (crates-io))

(define-public crate-etc-express-midi-0.2.1 (c (n "etc-express-midi") (v "0.2.1") (d (list (d (n "midir") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ahidv0dc4g1ppmjnf1494bji4xillkjk6w0bzyxdhi4yyck5x84")))

