(define-module (crates-io et c- etc-passwd) #:use-module (crates-io))

(define-public crate-etc-passwd-0.1.0 (c (n "etc-passwd") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.73") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0n3pgnga3gdi25xv4xa4l3zpp4ggh483af81gfy49pjqrdknv4il")))

(define-public crate-etc-passwd-0.1.1 (c (n "etc-passwd") (v "0.1.1") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "169kr4b9x7r44bgaw4ph080s7mbpxpl0agp7jmsp88xxqpl2vqvk")))

(define-public crate-etc-passwd-0.2.0 (c (n "etc-passwd") (v "0.2.0") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0sfahsmljgxw7b9cz2k7jaa3f8l7ipwyyb38l21nrcgf070agg0a") (r "1.56.1")))

