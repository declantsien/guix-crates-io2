(define-module (crates-io et h_ eth_merkle_tree) #:use-module (crates-io))

(define-public crate-eth_merkle_tree-0.1.0 (c (n "eth_merkle_tree") (v "0.1.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dypwknrh561k391kic1dvspb813qvbh5xk52kk1gm9cpj1xl9z4")))

(define-public crate-eth_merkle_tree-0.1.1 (c (n "eth_merkle_tree") (v "0.1.1") (d (list (d (n "colored") (r "^2.0") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "csv") (r "^1.1") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "0vgqm1rprwr6chfd0yn3w2xjf2cd7v6vm1wdpzkcbf00scbsdq74")))

