(define-module (crates-io et h_ eth_gas_bot) #:use-module (crates-io))

(define-public crate-eth_gas_bot-0.1.0 (c (n "eth_gas_bot") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "megalodon") (r "^0.8.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.179") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1c9by0m0y5203iy6yzykwv2m180ikpwc34h8wfsddzqy2ml4cqf3")))

(define-public crate-eth_gas_bot-0.1.1 (c (n "eth_gas_bot") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "megalodon") (r "^0.8.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.179") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0giy7b1cm25n0rdgk70cdb579iha1bga55a27r5qrs4dgmxh08xx")))

