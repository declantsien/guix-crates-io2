(define-module (crates-io et h_ eth_event_macro) #:use-module (crates-io))

(define-public crate-eth_event_macro-0.1.0 (c (n "eth_event_macro") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ethabi") (r "^16.0.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "web3") (r "^0.18.0") (d #t) (k 0)))) (h "0mk43cs56zjzlj3741b0h8nbl1l517k9wjplfn4c2ij3phg1wjj5")))

