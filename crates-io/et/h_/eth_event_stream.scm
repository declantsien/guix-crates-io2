(define-module (crates-io et h_ eth_event_stream) #:use-module (crates-io))

(define-public crate-eth_event_stream-0.1.0 (c (n "eth_event_stream") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.9") (f (quote ("derive"))) (d #t) (k 2)) (d (n "eth_event_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "ethabi") (r "^16.0.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.12.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (d #t) (k 0)) (d (n "tokio-retry") (r "^0.3") (d #t) (k 0)) (d (n "web3") (r "^0.18.0") (d #t) (k 0)))) (h "143h2y5q8gll5h4rwn3b3zhx397x971laad2wvv27m84jwhxizzi")))

