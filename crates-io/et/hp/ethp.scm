(define-module (crates-io et hp ethp) #:use-module (crates-io))

(define-public crate-ethp-0.1.0 (c (n "ethp") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0prpkiaw51jpdjajp2z49glwwbw32g809whcalmf7pm5l0whvbmi")))

