(define-module (crates-io et hp ethpm) #:use-module (crates-io))

(define-public crate-ethpm-0.1.0-alpha.1 (c (n "ethpm") (v "0.1.0-alpha.1") (h "0p602jckn48bs4j1bcz2syspg3f0vwbmm41xcdbl9x5i3z8xzf2j")))

(define-public crate-ethpm-0.1.0-alpha.2 (c (n "ethpm") (v "0.1.0-alpha.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "web3") (r "^0.7.0") (d #t) (k 2)))) (h "02vvm78zmbm6ds03ykg75c4q4ip94gp3xryl8dr4givykindmnij")))

(define-public crate-ethpm-0.1.0-alpha.3 (c (n "ethpm") (v "0.1.0-alpha.3") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-json-core") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "web3") (r "^0.8.0") (d #t) (k 2)))) (h "08zn5zls7yc2r1wjhqimgk45szw031s0hinmhfwszria8zdxm5ms") (f (quote (("std" "serde" "serde_json") ("no-std" "serde-json-core") ("default" "serde" "serde_json"))))))

