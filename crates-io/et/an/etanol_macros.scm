(define-module (crates-io et an etanol_macros) #:use-module (crates-io))

(define-public crate-etanol_macros-0.1.0 (c (n "etanol_macros") (v "0.1.0") (d (list (d (n "etanol_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ypzn00rpmrc88i3yfxdq8n90dmpd76cx2ibk1c9d2i7ffgz01gw")))

(define-public crate-etanol_macros-0.1.1 (c (n "etanol_macros") (v "0.1.1") (d (list (d (n "etanol_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "008w051fyb79xay44ljzjgkyprnivykp49a82vpwyv5sk7j10gbf")))

(define-public crate-etanol_macros-0.1.2 (c (n "etanol_macros") (v "0.1.2") (d (list (d (n "etanol_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1agy6ifq2a0bjw3j57z7c9gm4pwvcg1fxhmd1i2vlj25s3s584mc")))

