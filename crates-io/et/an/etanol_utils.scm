(define-module (crates-io et an etanol_utils) #:use-module (crates-io))

(define-public crate-etanol_utils-0.0.1 (c (n "etanol_utils") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "0b86al79r1k6fq6qz4q8gkdaa6p6aar7982p23lc8xa3mr8kk69w")))

(define-public crate-etanol_utils-0.0.2 (c (n "etanol_utils") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "etanol_compiler") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1n5myxc7dwi859dqr8psliya4zm7c6dfvmsxcaa7ci2cbbaphg53")))

(define-public crate-etanol_utils-0.1.0 (c (n "etanol_utils") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "etanol_compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "02xdp0ahzc8zspvh17yapnwbqfrqc26ji04awh5ngfpq0gbhrz6h")))

(define-public crate-etanol_utils-0.1.1 (c (n "etanol_utils") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "etanol_compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0sy8prgza0x120b04npfh5l3pszwdp1n2sndka7z8gvwfwdadpiw")))

(define-public crate-etanol_utils-0.1.2 (c (n "etanol_utils") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "etanol_compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "13rga43nbj38c80dkw76vpg19fz37pfmf5p8fj1rk4yybd897y9s")))

