(define-module (crates-io et an etanol) #:use-module (crates-io))

(define-public crate-etanol-0.0.1 (c (n "etanol") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "etanol_compiler") (r "^0.0.1") (d #t) (k 0)) (d (n "etanol_databases") (r "^0.0.1") (d #t) (k 0)) (d (n "etanol_utils") (r "^0.0.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0iba60nmgv532ck15dlbl7c3mk7xxp20nsfcaykz5zpcc8zscbi0") (y #t)))

(define-public crate-etanol-0.1.0 (c (n "etanol") (v "0.1.0") (d (list (d (n "etanol_compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_databases") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_utils") (r "^0.1.0") (d #t) (k 0)))) (h "0nhx5g6i1yx93sjyjb6hgmbpyi38xmhkq91y52w3wxrvvhckcyvr")))

(define-public crate-etanol-0.1.1 (c (n "etanol") (v "0.1.1") (d (list (d (n "etanol_compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_databases") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1hv8ynka8rlfd41xzzd0brxyjm4shv4bmlgci0s7az5zsgdcvfsv")))

(define-public crate-etanol-0.1.2 (c (n "etanol") (v "0.1.2") (d (list (d (n "etanol_compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_databases") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_utils") (r "^0.1.0") (d #t) (k 0)))) (h "01wfbjynyigb1zas25b20ql4i5cfk1zs97xrxjm2c7di70spbkvp")))

