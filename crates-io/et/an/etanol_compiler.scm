(define-module (crates-io et an etanol_compiler) #:use-module (crates-io))

(define-public crate-etanol_compiler-0.0.1 (c (n "etanol_compiler") (v "0.0.1") (d (list (d (n "diacritics") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0wffgvrnjxc8q8lrdgpxpzwr39y90m5ypk58bl3zr767mhrzbcpx")))

(define-public crate-etanol_compiler-0.1.0 (c (n "etanol_compiler") (v "0.1.0") (d (list (d (n "diacritics") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1jm5br368rk82jd4s6wkvw83b869k1gnkd4a0mcxg5g5x4ny04jz")))

(define-public crate-etanol_compiler-0.1.2 (c (n "etanol_compiler") (v "0.1.2") (d (list (d (n "diacritics") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "066169rvar5x04h7g9v0djwaq87x047hklyzicrgfbqs8icrm9ma")))

