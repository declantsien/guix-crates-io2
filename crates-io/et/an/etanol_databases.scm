(define-module (crates-io et an etanol_databases) #:use-module (crates-io))

(define-public crate-etanol_databases-0.0.1 (c (n "etanol_databases") (v "0.0.1") (d (list (d (n "etanol_utils") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "126dax1g57srxqpdsfvdkzf2x0f45lbjmh9lq254crcy9j8l6qhx")))

(define-public crate-etanol_databases-0.0.2 (c (n "etanol_databases") (v "0.0.2") (d (list (d (n "etanol_utils") (r "^0.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0033ylaa7dpri3snjq0k0wgkjjjllpi51g63v6cqz78rsqbpxhvw")))

(define-public crate-etanol_databases-0.1.0 (c (n "etanol_databases") (v "0.1.0") (d (list (d (n "etanol_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "11fkspkww2508blw7sd2nx184kscyz27qzkd6dkwfv66swplkrf2")))

(define-public crate-etanol_databases-0.1.1 (c (n "etanol_databases") (v "0.1.1") (d (list (d (n "etanol_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "120sm0j1n8wis98zfa3j0zqzwdrc44wb768px49d6xq98xjy7xl9")))

(define-public crate-etanol_databases-0.1.2 (c (n "etanol_databases") (v "0.1.2") (d (list (d (n "etanol_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "etanol_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "12n24bi71nir4l4gqc3gbm76kpb8bk631fbzzfyibbx9jpnz7r7a")))

