(define-module (crates-io et te etternaonline_api) #:use-module (crates-io))

(define-public crate-etternaonline_api-0.1.0 (c (n "etternaonline_api") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.3") (f (quote ("json"))) (d #t) (k 0)))) (h "023jfy5kismnwdfd0kiqsk9ssgv2wx2shgzd0mh5fr8h3d0dpa65")))

(define-public crate-etternaonline_api-0.2.0 (c (n "etternaonline_api") (v "0.2.0") (d (list (d (n "etterna") (r "^0.1.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror_lite") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.3") (f (quote ("json"))) (d #t) (k 0)))) (h "08b1031n41wkiadyjiw622d2vbwsv4zlpp7shrchz4swdap8x6yv") (f (quote (("serde" "serde_" "etterna/serde"))))))

