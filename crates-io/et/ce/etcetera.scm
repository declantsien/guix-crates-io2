(define-module (crates-io et ce etcetera) #:use-module (crates-io))

(define-public crate-etcetera-0.1.0 (c (n "etcetera") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zjn0n1xim2v59rpk04abi66ajv487f6c8w8ygp6r33yjsvzjksl")))

(define-public crate-etcetera-0.2.0 (c (n "etcetera") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ygkh8dvb8zql86vjc9kpidkmfardg1sk3p1ghab7dbvk2flk334")))

(define-public crate-etcetera-0.2.1 (c (n "etcetera") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1naw0crxkp1pfhcbcdi6ns4lk202kanwhi2hr7hi7ijcqzqq357h")))

(define-public crate-etcetera-0.3.0 (c (n "etcetera") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vrjw5p32n6hx32gl1pfkr0hlay1bhxy58w1rsz03jwmmr1kw7k8")))

(define-public crate-etcetera-0.3.1 (c (n "etcetera") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs-next") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jb0ax1fvbpq6mm4971hqh21r25qaanmchdzxnb1bfqdf00fi7kr")))

(define-public crate-etcetera-0.3.2 (c (n "etcetera") (v "0.3.2") (d (list (d (n "cfg-if") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.22, <2.0.0") (d #t) (k 0)))) (h "1v1d2z2h1ay3skks0s4n833z275nkdf28d168cyq7ywl3vyh8sq1")))

(define-public crate-etcetera-0.4.0 (c (n "etcetera") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1zb2x5p9ajj78r5b42gmgn53zgc78jgz8lfvw5szm6sfivhzq5yh")))

(define-public crate-etcetera-0.5.0 (c (n "etcetera") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0zcycp4hgsx586ncn18glpn38502r1gcxpkpjbgl34f9z7gry2l7")))

(define-public crate-etcetera-0.6.0 (c (n "etcetera") (v "0.6.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "06wnpzpnd443i5hd0ikpgmny3073pa2xylz6sxzrkla1kyh9yfa4")))

(define-public crate-etcetera-0.7.0 (c (n "etcetera") (v "0.7.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1p38yzs2dbiaklnqqdr97q7n4k478sl10bdipgb0haldaq2db5p7")))

(define-public crate-etcetera-0.7.1 (c (n "etcetera") (v "0.7.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1827gydakhp0jaw9zkn98gwq7sc5nxb6vj6fjr6qr78jqvnjx0ji")))

(define-public crate-etcetera-0.8.0 (c (n "etcetera") (v "0.8.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_UI_Shell"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0hxrsn75dirbjhwgkdkh0pnpqrnq17ypyhjpjaypgax1hd91nv8k")))

