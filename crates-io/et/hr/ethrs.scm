(define-module (crates-io et hr ethrs) #:use-module (crates-io))

(define-public crate-ethrs-0.1.0 (c (n "ethrs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m5z481cmxx4j616bygvwvds001q7ysid7djza4csmpw3b3ljsih") (r "1.67.0")))

(define-public crate-ethrs-0.1.1 (c (n "ethrs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vayzcjjrz6g0qa41w61ipmvs7kdm758pg0vdci1w8865ln1wxg1") (r "1.67.0")))

