(define-module (crates-io et he ethers_eip2718) #:use-module (crates-io))

(define-public crate-ethers_eip2718-0.2.2 (c (n "ethers_eip2718") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethers_primitives") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_ethrlp") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1bkzsp93q546kyjzldgj66qqa45mwk88dhzj9bxakz3pjfqc90pb")))

