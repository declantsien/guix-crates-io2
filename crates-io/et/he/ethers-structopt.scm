(define-module (crates-io et he ethers-structopt) #:use-module (crates-io))

(define-public crate-ethers-structopt-0.1.0 (c (n "ethers-structopt") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "ethers") (r "^0.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "05dam9wb5ch0scwxir3ijaln7rx6xi9qjj7k1gnh85vdx4lffysx")))

