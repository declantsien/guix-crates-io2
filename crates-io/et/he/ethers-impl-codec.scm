(define-module (crates-io et he ethers-impl-codec) #:use-module (crates-io))

(define-public crate-ethers-impl-codec-0.6.0 (c (n "ethers-impl-codec") (v "0.6.0") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("max-encoded-len"))) (k 0)))) (h "0c494ik7jirky6vqb9zmlhn62xzdnd1wlhsx3lh10pjzbc5zhrw8") (f (quote (("std" "parity-scale-codec/std") ("default" "std")))) (r "1.56.1")))

