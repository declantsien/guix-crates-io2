(define-module (crates-io et he ethereumvm-precompiled-modexp) #:use-module (crates-io))

(define-public crate-ethereumvm-precompiled-modexp-0.11.0 (c (n "ethereumvm-precompiled-modexp") (v "0.11.0") (d (list (d (n "ethereum-bigint") (r "^0.2") (k 0)) (d (n "ethereum-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "ethereumvm") (r "^0.11") (k 0)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)))) (h "0p2ldxcd2fv1a2pxq02kw5gycz3m9b9rmi93cliyd4d03yb5mrz8") (f (quote (("std" "ethereumvm/std") ("rust-secp256k1" "ethereumvm/rust-secp256k1") ("rlp" "ethereum-bigint/rlp") ("default" "std" "rust-secp256k1") ("c-secp256k1" "ethereumvm/c-secp256k1"))))))

