(define-module (crates-io et he ethers-tx-handler) #:use-module (crates-io))

(define-public crate-ethers-tx-handler-0.1.0 (c (n "ethers-tx-handler") (v "0.1.0") (d (list (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)))) (h "1wasxfvmw8a0kxxhk0rmmas56157vnls6csdldir20k2phh8kyrr")))

(define-public crate-ethers-tx-handler-0.1.1 (c (n "ethers-tx-handler") (v "0.1.1") (d (list (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)))) (h "08n1v0xra9i3xcn48nrn1891kxj9zgs6w20s6hnzprrx5jy4nvq0")))

(define-public crate-ethers-tx-handler-0.1.101 (c (n "ethers-tx-handler") (v "0.1.101") (d (list (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "16ms5mpm1j53mmaiqwlr997rwbdnsiycncrpq5lspxca73rhi0yx")))

(define-public crate-ethers-tx-handler-0.1.102 (c (n "ethers-tx-handler") (v "0.1.102") (d (list (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0jh35vjcb53hws087s34p5s0clqar4x0b7a7zcc5lfnj0kakdyim")))

(define-public crate-ethers-tx-handler-0.1.103 (c (n "ethers-tx-handler") (v "0.1.103") (d (list (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0bvwcvl0jwzv5gn07zgdq8hniy1144bavrbmk2b527yzrg1pw1v7")))

(define-public crate-ethers-tx-handler-0.1.105 (c (n "ethers-tx-handler") (v "0.1.105") (d (list (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0ll14fi3dn1i0kahb80pspky1ply3ff5da99258mgas0alvc901a")))

(define-public crate-ethers-tx-handler-0.1.106 (c (n "ethers-tx-handler") (v "0.1.106") (d (list (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0pdqs7wrm6jpxdzdc5zn83cw38cw3jibyj9cb0151z0nf8bfcj0c")))

(define-public crate-ethers-tx-handler-0.1.107 (c (n "ethers-tx-handler") (v "0.1.107") (d (list (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "04fx19sfx0rfcf9qja8kh91ibvzmfjl4f6w13y0b82v31cz59whz")))

