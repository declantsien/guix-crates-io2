(define-module (crates-io et he ethers-addressbook) #:use-module (crates-io))

(define-public crate-ethers-addressbook-0.7.1 (c (n "ethers-addressbook") (v "0.7.1") (d (list (d (n "ethers-core") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kz1clzi0s5lhlkz7xp8pwwqrx07hrw0k80glzckfaz4n6v9fd44")))

(define-public crate-ethers-addressbook-0.9.0 (c (n "ethers-addressbook") (v "0.9.0") (d (list (d (n "ethers-core") (r "^0.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04yrn8i1321lr35i29nn9xgk8vv5z8k8y9n66aiil2mvg2flvpyi")))

(define-public crate-ethers-addressbook-0.10.0 (c (n "ethers-addressbook") (v "0.10.0") (d (list (d (n "ethers-core") (r "^0.10.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yap3ykn60iki2ihr3k82k16667i45h6rq9gagq0q19p6hscpwgx")))

(define-public crate-ethers-addressbook-0.1.1-alpha.1 (c (n "ethers-addressbook") (v "0.1.1-alpha.1") (d (list (d (n "ethers-core") (r "^0.6.4-alpha.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0had309x7rqip52y6l177hkrdiarih4xb7g4bnxfgj7ajvx7mvxh")))

(define-public crate-ethers-addressbook-0.11.0 (c (n "ethers-addressbook") (v "0.11.0") (d (list (d (n "ethers-core") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1940fg4vhyvf64fzlih6m6m5a4yqdq4ib5lhin96vzr7363rxm2y")))

(define-public crate-ethers-addressbook-0.12.0 (c (n "ethers-addressbook") (v "0.12.0") (d (list (d (n "ethers-core") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "100as94m8kkp7r8b0zpj5pshygx29b62c6q9f6d7r5r4c5xl9ddj")))

(define-public crate-ethers-addressbook-0.13.0 (c (n "ethers-addressbook") (v "0.13.0") (d (list (d (n "ethers-core") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00j5pxiib7d6876aggwq9ka4ijy0ygwjlnw7xgxi15ijdn1jmbwq")))

(define-public crate-ethers-addressbook-0.13.1-alpha.1 (c (n "ethers-addressbook") (v "0.13.1-alpha.1") (d (list (d (n "ethers-core") (r "^0.13.1-alpha.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nfwlgjxrqkhxiqpbx7g635b8cfdgiymzb2icns4iyrvbprl2dqy")))

(define-public crate-ethers-addressbook-0.14.0 (c (n "ethers-addressbook") (v "0.14.0") (d (list (d (n "ethers-core") (r "^0.14.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s3yqjvc660dmd0vwrfi4y03k12s186pirn8kn753liln58armmw")))

(define-public crate-ethers-addressbook-0.15.0 (c (n "ethers-addressbook") (v "0.15.0") (d (list (d (n "ethers-core") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15fpj9dyvgym1vdf49mq2wbzjrp0pvfs6s6aqmiczsk1m6sqszmf")))

(define-public crate-ethers-addressbook-0.17.0 (c (n "ethers-addressbook") (v "0.17.0") (d (list (d (n "ethers-core") (r "^0.17.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nyka97b5vy9r1fpzw456mr0ihb6qnmnlsc357fsjpplxj98jgz2")))

(define-public crate-ethers-addressbook-0.17.1-alpha.1 (c (n "ethers-addressbook") (v "0.17.1-alpha.1") (d (list (d (n "ethers-core") (r "^0.17.1-alpha.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10knnmcbizr0gw0afnskgjma6ickshg9xzkdvb0qg2yf9m3mfskz")))

(define-public crate-ethers-addressbook-1.0.0 (c (n "ethers-addressbook") (v "1.0.0") (d (list (d (n "ethers-core") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04bybyr75w80vg1q8lfdc07mliz4xlqmv6hglmcqs5sx6zdckf5l") (r "1.62")))

(define-public crate-ethers-addressbook-1.0.1 (c (n "ethers-addressbook") (v "1.0.1") (d (list (d (n "ethers-core") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fkfwk7wb43i8s6aqdh18gx2wmhh5zzjkliq2dygs095q0n09yy3") (r "1.62")))

(define-public crate-ethers-addressbook-1.0.2 (c (n "ethers-addressbook") (v "1.0.2") (d (list (d (n "ethers-core") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02xabr48bf0s7bhwwf0qakqyhwymbbmwsv70hkbla296s96yajzy") (r "1.62")))

(define-public crate-ethers-addressbook-1.5.0 (c (n "ethers-addressbook") (v "1.5.0") (d (list (d (n "ethers-core") (r "^1.0.0") (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16q6zn2v9l71fizpbwk027j0khcc3x6im94zbipi57zgbqvz1zm6") (y #t) (r "1.64")))

(define-public crate-ethers-addressbook-2.0.0 (c (n "ethers-addressbook") (v "2.0.0") (d (list (d (n "ethers-core") (r "^2.0.0") (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15cwf04xy8frx322rahs0ad0fvsrknybh2rwlhzjm2n0cl0h27ly") (r "1.64")))

(define-public crate-ethers-addressbook-2.0.1-alpha.1 (c (n "ethers-addressbook") (v "2.0.1-alpha.1") (d (list (d (n "ethers-core") (r "^2.0.1-alpha.1") (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p7126dl02p3irf5kh0c7s57h4bv6720gsisq8a51swjjdmlvf02") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.1 (c (n "ethers-addressbook") (v "2.0.1") (d (list (d (n "ethers-core") (r "^2.0.1") (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fj11f2h3b3ldpmcxwhl3rb163di1c1qrdr7z336i9dp3f4pn48j") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.2 (c (n "ethers-addressbook") (v "2.0.2") (d (list (d (n "ethers-core") (r "^2.0.2") (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g3gryh7z7b3jijnrs8ydblpjf3l6hgvq7393i966kay5s0h7dn0") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.3 (c (n "ethers-addressbook") (v "2.0.3") (d (list (d (n "ethers-core") (r "^2.0.3") (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xfv0qfmw22mh7gpddr8iq0m97mg9kd5hh9jr2a6fh3wrf91qvym") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.4 (c (n "ethers-addressbook") (v "2.0.4") (d (list (d (n "ethers-core") (r "^2.0.4") (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i9yfq389wnwc8v4rqg19v22dzdd05mlpmqs6v8zd814p0ka8rhw") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.5 (c (n "ethers-addressbook") (v "2.0.5") (d (list (d (n "ethers-core") (r "^2.0.5") (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0970a1hbg1ifsij0fiq5kcilzwym7pll1dblpfy1a2h29gadza4s") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.6 (c (n "ethers-addressbook") (v "2.0.6") (d (list (d (n "ethers-core") (r "^2.0.6") (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "067vjgvq7n96sxx7ws5jjgsvm8rqasd08wka12m3x0x66aspz9dl") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.7 (c (n "ethers-addressbook") (v "2.0.7") (d (list (d (n "ethers-core") (r "^2.0.7") (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y6mrhqsa4fyh43y474la6s29ryczd8y3vxq7h4n3jgmixxnp1bv") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.8 (c (n "ethers-addressbook") (v "2.0.8") (d (list (d (n "ethers-core") (r "^2.0.8") (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p38m9mrshs84vwzva171gjqvfqxq66v4lachx18q393zkz6zjzd") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.9 (c (n "ethers-addressbook") (v "2.0.9") (d (list (d (n "ethers-core") (r "^2.0.9") (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11hrvwpczpip4jzchqkb24wrzijaqgi37r2j1fjqy5xq25zn2i82") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.10 (c (n "ethers-addressbook") (v "2.0.10") (d (list (d (n "ethers-core") (r "^2.0.10") (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02i4nqx6nxh336jc33dr81n258xb9p1719kkrh1q8d7ds2nfisf6") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.11 (c (n "ethers-addressbook") (v "2.0.11") (d (list (d (n "ethers-core") (r "^2.0.11") (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02cdgz67sbd3l7hb3wws42qpwjnw8df3i1brpacphl9sx8j5yh4c") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.12 (c (n "ethers-addressbook") (v "2.0.12") (d (list (d (n "ethers-core") (r "^2.0.12") (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1764vppx4llg3d7rr8nv8aqpgh67ivh52jaq3za2l2g2savmxwwv") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.13 (c (n "ethers-addressbook") (v "2.0.13") (d (list (d (n "ethers-core") (r "^2.0.13") (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gs04q5pc0f0p0y7dw67jb7n6qa02r0jmxbzjkl1fv86khj9mp1m") (r "1.65")))

(define-public crate-ethers-addressbook-2.0.14 (c (n "ethers-addressbook") (v "2.0.14") (d (list (d (n "ethers-core") (r "^2.0.14") (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n8p08xmr5qs0mvj60jm2wyfb0s9ifwj27xs7dn5bajgdg8sz5al") (r "1.65")))

