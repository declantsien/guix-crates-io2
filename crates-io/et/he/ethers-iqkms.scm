(define-module (crates-io et he ethers-iqkms) #:use-module (crates-io))

(define-public crate-ethers-iqkms-0.0.0 (c (n "ethers-iqkms") (v "0.0.0") (h "0v3c1472mg8q1zywiayc1yi7d9cl41mnsgc5rijh0df3h9qpnhgf")))

(define-public crate-ethers-iqkms-0.0.1 (c (n "ethers-iqkms") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "ethers-core") (r "^1") (d #t) (k 0)) (d (n "ethers-signers") (r "^1") (d #t) (k 0)) (d (n "iqkms") (r "^0.0.1") (f (quote ("ethereum"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1rbkgk0lmqmd08z1s786lyh1aaddwz94cag8f7b9sr45gy5igqmb") (r "1.64")))

