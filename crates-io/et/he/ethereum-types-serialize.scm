(define-module (crates-io et he ethereum-types-serialize) #:use-module (crates-io))

(define-public crate-ethereum-types-serialize-0.2.0 (c (n "ethereum-types-serialize") (v "0.2.0") (d (list (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "114k9i1b8iwqhlnsdvbyrc1xqak8mg2sgq40cxgi3w25n8905p4l")))

(define-public crate-ethereum-types-serialize-0.2.1 (c (n "ethereum-types-serialize") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "00n0kmkm4y42rcc75g3zxikkqjkcgav9xknsyf4f366fm4hrmiaa")))

(define-public crate-ethereum-types-serialize-0.2.2 (c (n "ethereum-types-serialize") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0l53rvzvwpxwkl7i3f9q5fafw663rcm5z4mdknkr265w69xxfwqq")))

