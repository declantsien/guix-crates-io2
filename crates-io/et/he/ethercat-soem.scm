(define-module (crates-io et he ethercat-soem) #:use-module (crates-io))

(define-public crate-ethercat-soem-0.0.0 (c (n "ethercat-soem") (v "0.0.0") (h "1f553yk8kqkvass6yz15y4y0cbjrjrmiyh8wn2jrarpr6bqpingc")))

(define-public crate-ethercat-soem-0.1.0 (c (n "ethercat-soem") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "ethercat-soem-ctx") (r "^0.1.1") (d #t) (k 0)) (d (n "ethercat-types") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sxn8j5vsvja2rhlnyhwqivqn96w7f01piri01f4r1qa6yb46q8n") (f (quote (("issue-224-workaround" "ethercat-soem-ctx/issue-224-workaround"))))))

(define-public crate-ethercat-soem-0.2.0 (c (n "ethercat-soem") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "ethercat-soem-ctx") (r "^0.2") (d #t) (k 0)) (d (n "ethercat-types") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1n2y93spf6xy8hmqzz53pwhpyy885rr7fiq74kdkfyz9qwzr4rll") (f (quote (("issue-224-workaround" "ethercat-soem-ctx/issue-224-workaround"))))))

