(define-module (crates-io et he ethercat-soem-sys) #:use-module (crates-io))

(define-public crate-ethercat-soem-sys-0.0.0 (c (n "ethercat-soem-sys") (v "0.0.0") (h "1fxaamm4x11wjsvi93dyh7w8255spq7h8svdy1bgsz1yyg1a8g1z")))

(define-public crate-ethercat-soem-sys-0.1.0 (c (n "ethercat-soem-sys") (v "0.1.0") (d (list (d (n "bindgen") (r ">=0.55.0, <0.56.0") (d #t) (k 1)) (d (n "cmake") (r ">=0.1.0, <0.2.0") (d #t) (k 1)))) (h "0xhf60gbbifvfrdjxpyzdbkj8bvmwimwssfkgfbrz71mnx7scfc0") (f (quote (("issue-224-workaround"))))))

(define-public crate-ethercat-soem-sys-0.2.0 (c (n "ethercat-soem-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "173fa2q7zk0ckyrjgw4x8h8ihwz9867airh6wwp1a5zidp7z59ic") (f (quote (("issue-224-workaround"))))))

