(define-module (crates-io et he ethereum-block-core) #:use-module (crates-io))

(define-public crate-ethereum-block-core-0.1.0 (c (n "ethereum-block-core") (v "0.1.0") (d (list (d (n "ethereum-bigint") (r "^0.2") (f (quote ("rlp" "string"))) (k 0)) (d (n "ethereum-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "ethereum-rlp") (r "^0.2") (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "08w2p83mifdv6cky39cajv82v2vvnkxj4yhdpbnf80idxbbzmprl") (f (quote (("std" "ethereum-bigint/std" "ethereum-rlp/std") ("default" "std"))))))

(define-public crate-ethereum-block-core-0.2.0 (c (n "ethereum-block-core") (v "0.2.0") (d (list (d (n "ethereum-bigint") (r "^0.2") (f (quote ("rlp" "string"))) (k 0)) (d (n "ethereum-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "ethereum-rlp") (r "^0.2") (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0mby6xfgli03ag54aflcaagz4bsmym9idb2n0c4rllxg6c4abnvh") (f (quote (("std" "ethereum-bigint/std" "ethereum-rlp/std") ("default" "std"))))))

