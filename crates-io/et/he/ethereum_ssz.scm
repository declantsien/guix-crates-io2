(define-module (crates-io et he ethereum_ssz) #:use-module (crates-io))

(define-public crate-ethereum_ssz-1.0.0-beta.0 (c (n "ethereum_ssz") (v "1.0.0-beta.0") (d (list (d (n "ethereum-types") (r "^0.12.1") (d #t) (k 0)) (d (n "ethereum_ssz_derive") (r "^1.0.0-beta.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "021589ywibwl8zbgxcngpr7jlp7s75d2z8k7kkh52imqbnfwz8jr") (f (quote (("arbitrary" "ethereum-types/arbitrary"))))))

(define-public crate-ethereum_ssz-1.0.0-beta.2 (c (n "ethereum_ssz") (v "1.0.0-beta.2") (d (list (d (n "ethereum-types") (r "^0.12.1") (d #t) (k 0)) (d (n "ethereum_ssz_derive") (r "^1.0.0-beta.2") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "076077b6vi7qfb7fqhqqi7li3ih7p428fij7q4fbnkkkvg0jzfc2") (f (quote (("arbitrary" "ethereum-types/arbitrary"))))))

(define-public crate-ethereum_ssz-0.5.0 (c (n "ethereum_ssz") (v "0.5.0") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "ethereum_ssz_derive") (r "^0.5.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "12fqrnc3ngf7cprchzjxy7ya9k5iv8svds56chbnjdflfimbbmi9") (f (quote (("arbitrary" "ethereum-types/arbitrary"))))))

(define-public crate-ethereum_ssz-0.5.1 (c (n "ethereum_ssz") (v "0.5.1") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "ethereum_ssz_derive") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "12d4ixywi81v7cwz2dq7j3hj8hfalabs6wn6hif93x5lqnbx04l0") (f (quote (("arbitrary" "ethereum-types/arbitrary"))))))

(define-public crate-ethereum_ssz-0.5.2 (c (n "ethereum_ssz") (v "0.5.2") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "ethereum_ssz_derive") (r "^0.5.2") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "08f2adqvrnf8g2mq2jprkz86hk66qjj8xvm7sx0ayxjk62b9wx1j") (f (quote (("arbitrary" "ethereum-types/arbitrary"))))))

(define-public crate-ethereum_ssz-0.5.3 (c (n "ethereum_ssz") (v "0.5.3") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "ethereum_ssz_derive") (r "^0.5.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "1jqgm3vgxaixbrg5zskr074gpm1vipn852hj6nfj9s16kyigw7z6") (f (quote (("arbitrary" "ethereum-types/arbitrary"))))))

