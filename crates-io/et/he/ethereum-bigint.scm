(define-module (crates-io et he ethereum-bigint) #:use-module (crates-io))

(define-public crate-ethereum-bigint-0.2.9 (c (n "ethereum-bigint") (v "0.2.9") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "ethereum-hexutil") (r "^0.2") (o #t) (k 0)) (d (n "ethereum-rlp") (r "^0.2") (o #t) (k 0)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (o #t) (d #t) (k 0)))) (h "1riqx7gxpwba7yxwyykszda7mlqis40k6d56hxp42r67xqdz6ac7") (f (quote (("x64asm_arithmetic") ("string" "ethereum-hexutil") ("std" "byteorder/std" "rand" "libc" "ethereum-rlp/std" "ethereum-hexutil/std") ("rust_arithmetic") ("rlp" "ethereum-rlp") ("heapsizeof" "heapsize") ("default" "std" "rlp" "string"))))))

