(define-module (crates-io et he ethereum_ssz_derive) #:use-module (crates-io))

(define-public crate-ethereum_ssz_derive-1.0.0-beta.0 (c (n "ethereum_ssz_derive") (v "1.0.0-beta.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "1fxyjkcp635cjja9yv6g2srz6152xgsy1n4y9rff0g41szcnx9ga")))

(define-public crate-ethereum_ssz_derive-1.0.0-beta.2 (c (n "ethereum_ssz_derive") (v "1.0.0-beta.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "03x30rrqagmndpm88y17cif29m2mpc1ai4gpzq7w5yjzw9znls4b")))

(define-public crate-ethereum_ssz_derive-0.5.0 (c (n "ethereum_ssz_derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "1z5ifal0yk0jl87yacbk4bf4d24liq3ygq585vpcj0rvlvkirxa4")))

(define-public crate-ethereum_ssz_derive-0.5.1 (c (n "ethereum_ssz_derive") (v "0.5.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "1f2zaqqx1r1pxxlj0a2mhf4z8dg2akq4l4iqghnxa9cw0i112hlz")))

(define-public crate-ethereum_ssz_derive-0.5.2 (c (n "ethereum_ssz_derive") (v "0.5.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "1w2c2xcjw78milw85y7fhlh4s4xvz5b401n06km6r4h747pwgjnr")))

(define-public crate-ethereum_ssz_derive-0.5.3 (c (n "ethereum_ssz_derive") (v "0.5.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "0f7fl19zcd2mgih61i5rilflkyv7hi6dal61zswd4jzq7kyxg1b0")))

