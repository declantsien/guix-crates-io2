(define-module (crates-io et he ethernet) #:use-module (crates-io))

(define-public crate-ethernet-0.1.0 (c (n "ethernet") (v "0.1.0") (d (list (d (n "bin-utils") (r "^0.1.0") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "ether-type") (r "^0.1.0") (k 0)) (d (n "mac-parser") (r "^0.1.0") (k 0)))) (h "1vlpza4ayqlny8izw41ndsz3dkmjf230r2212z7y6msfnfrv0k07") (f (quote (("default" "debug") ("debug" "mac-parser/debug" "ether-type/debug"))))))

(define-public crate-ethernet-0.1.1 (c (n "ethernet") (v "0.1.1") (d (list (d (n "bin-utils") (r "^0.1.0") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "ether-type") (r "^0.1.0") (k 0)) (d (n "mac-parser") (r "^0.1.0") (k 0)))) (h "0jyg2sgjz7z3ggpwcs2s6xv384dj6ic17j1cac244yy78p9xln1q") (f (quote (("default" "debug") ("debug" "mac-parser/debug" "ether-type/debug"))))))

(define-public crate-ethernet-0.1.3 (c (n "ethernet") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "ether-type") (r "^0.1.2") (d #t) (k 0)) (d (n "mac-parser") (r "^0.1.4") (d #t) (k 0)) (d (n "scroll") (r "^0.12.0") (d #t) (k 0)))) (h "1y2nbcg066zkrk9cjlqk31b5c36m04bbnmsklkiv4j0pwl68f45s") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-ethernet-0.1.4 (c (n "ethernet") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "ether-type") (r "^0.1.3") (d #t) (k 0)) (d (n "mac-parser") (r "^0.1.4") (d #t) (k 0)) (d (n "scroll") (r "^0.12.0") (d #t) (k 0)))) (h "13lgm0phlyyn4433hzvg94x77byq78gbi71fyzk3id8f0zv0snhs") (f (quote (("default" "alloc") ("alloc"))))))

