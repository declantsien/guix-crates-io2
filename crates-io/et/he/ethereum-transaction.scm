(define-module (crates-io et he ethereum-transaction) #:use-module (crates-io))

(define-public crate-ethereum-transaction-0.1.0 (c (n "ethereum-transaction") (v "0.1.0") (d (list (d (n "ethereum-types") (r "^0.7") (d #t) (k 0)) (d (n "impl-serde") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rlp") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.5") (d #t) (k 0)))) (h "1wifzycvny3nbf9gayaxfpnh2swd9pdxsm299m47p5wk9z9dvk24")))

(define-public crate-ethereum-transaction-0.2.0 (c (n "ethereum-transaction") (v "0.2.0") (d (list (d (n "ethereum-types") (r "^0.8") (d #t) (k 0)) (d (n "impl-serde") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rlp") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.5") (d #t) (k 0)))) (h "13gkbnrj292n99gga9alx04k0v53jracpf0n2a68g89mfqvwi2b6")))

(define-public crate-ethereum-transaction-0.3.0 (c (n "ethereum-transaction") (v "0.3.0") (d (list (d (n "ethereum-types") (r "^0.8") (d #t) (k 0)) (d (n "impl-serde") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rlp") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.5") (d #t) (k 0)))) (h "0blpfjf7mwvyck5xw9zi3gv7phndwwv444f1hnphszr6dnd21kx1")))

(define-public crate-ethereum-transaction-0.4.0 (c (n "ethereum-transaction") (v "0.4.0") (d (list (d (n "ethereum-types") (r "^0.9") (d #t) (k 0)) (d (n "impl-serde") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rlp") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.5") (d #t) (k 0)))) (h "1gklma30casq21nvsmymgbnbrg3ihyrhi88i354kk67a3c7b1nbl")))

(define-public crate-ethereum-transaction-0.5.0 (c (n "ethereum-transaction") (v "0.5.0") (d (list (d (n "ethereum-types") (r "^0.9") (d #t) (k 0)) (d (n "impl-serde") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rlp") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.5") (d #t) (k 0)))) (h "0n6377jn4jlwg63cr6ysf21z0avm5lngllqsykb9l856czcrahdg")))

(define-public crate-ethereum-transaction-0.6.0 (c (n "ethereum-transaction") (v "0.6.0") (d (list (d (n "ethereum-types") (r "^0.11") (d #t) (k 0)) (d (n "impl-serde") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rlp") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (d #t) (k 0)))) (h "1hb1akg5ffkfk1dcn3cvlhna35prkx3vw73gb0bzyk1xf1xwyc2y")))

