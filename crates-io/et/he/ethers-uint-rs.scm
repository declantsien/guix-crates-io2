(define-module (crates-io et he ethers-uint-rs) #:use-module (crates-io))

(define-public crate-ethers-uint-rs-0.9.5 (c (n "ethers-uint-rs") (v "0.9.5") (d (list (d (n "arbitrary") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (k 0)) (d (n "hex") (r "^0.4") (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "rug") (r "^1.6.0") (f (quote ("integer"))) (t "cfg(all(unix, target_arch = \"x86_64\"))") (k 2)) (d (n "static_assertions") (r "^1.0.0") (d #t) (k 0)))) (h "1w6hf07zv708nxz769zxyslbfqdy96m772kmwmmk7vj8bwn20180") (f (quote (("std" "byteorder/std" "crunchy/std" "hex/std") ("default" "std")))) (r "1.56.1")))

