(define-module (crates-io et he ethers_eip712) #:use-module (crates-io))

(define-public crate-ethers_eip712-0.2.2 (c (n "ethers_eip712") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethers_primitives") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_eip712") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11bdxn8n37lq5vgs470l5bvz4y35hy011lxyc9mam1chzizhizws")))

