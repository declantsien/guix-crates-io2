(define-module (crates-io et he etherparse) #:use-module (crates-io))

(define-public crate-etherparse-0.1.0 (c (n "etherparse") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)))) (h "11hlr3j57v0qjr9p68xgxnz4jk6kr79lra0v2825pq8x75qv1n6a")))

(define-public crate-etherparse-0.1.1 (c (n "etherparse") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)))) (h "1202rz9448dc62h7icwy9w1wz4hj1v0nm699d9ylc6998hqljdz4")))

(define-public crate-etherparse-0.1.2 (c (n "etherparse") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)))) (h "1a1dagnzjh7zki602n327ka6kw3h8q2xwbxf3nk1qjn7z1qwhf82")))

(define-public crate-etherparse-0.1.3 (c (n "etherparse") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)))) (h "1apn73xbwrlpf7lq8x43x69x1r6dqccdy963nxc5876c92wi6yw4")))

(define-public crate-etherparse-0.1.4 (c (n "etherparse") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)))) (h "1kgkir03z83z4bpl2vn2sfxffgjja589sgg5c04bdr37kxfv44q7")))

(define-public crate-etherparse-0.2.0 (c (n "etherparse") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)))) (h "0vp599xdjdx7xl6f4vclf613hxd0a0jis30jrmrhkhxgjm02wn0v")))

(define-public crate-etherparse-0.3.0 (c (n "etherparse") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)))) (h "1sj047afj942ga9k6y31mz2za1shn13w30x2cp8yyp3l9chdva6v")))

(define-public crate-etherparse-0.3.1 (c (n "etherparse") (v "0.3.1") (d (list (d (n "assert_matches") (r "^1.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)))) (h "0i9wjppgry8krwmp0n7ygmpdvkr7j3xznjm8w6bb82s1z2wh6zvd")))

(define-public crate-etherparse-0.4.0 (c (n "etherparse") (v "0.4.0") (d (list (d (n "assert_matches") (r "^1.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "proptest") (r "^0.7.1") (d #t) (k 2)))) (h "0nkmafbppm93b0ga3pxf4v073h27qg5jrcs969137b0jhkhrfq24")))

(define-public crate-etherparse-0.5.0 (c (n "etherparse") (v "0.5.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "proptest") (r "^0.8.3") (d #t) (k 2)))) (h "0rr4h82d3fja2wgj4vp3g7v9lzxpq3slazbwyf5gqg5vc07girw2")))

(define-public crate-etherparse-0.6.0 (c (n "etherparse") (v "0.6.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 2)))) (h "0blmj9vzd5asmx2mmpjcm7a25rjpvk0kapx1lp22fbynxh5whl5c")))

(define-public crate-etherparse-0.7.0 (c (n "etherparse") (v "0.7.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 2)))) (h "1is9lnyr1w8njb5xsx7q1lnhpgvmhn8xc92zp661jmk8fxvnma7w")))

(define-public crate-etherparse-0.7.1 (c (n "etherparse") (v "0.7.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 2)))) (h "13pwrmx95j27ghnayzr7jlf747l09ivyqhx675vgl8kj0vimh3bk")))

(define-public crate-etherparse-0.8.0 (c (n "etherparse") (v "0.8.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "0bl1kcgfx6964wyabm3nisv5dqzn4r7zmqlz60i2mmaldvdmkvfl")))

(define-public crate-etherparse-0.8.2 (c (n "etherparse") (v "0.8.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)))) (h "1pikxznr7xgkx657d20ab75ccr7k7h7flmp92kdfrk5c6w81277z")))

(define-public crate-etherparse-0.8.3 (c (n "etherparse") (v "0.8.3") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "1drmlh4n19w1vpylacxhqnl4da42mpw2x9fcx6bc8r2sw31pqv3h")))

(define-public crate-etherparse-0.9.0 (c (n "etherparse") (v "0.9.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "01s86nj0k663mgxpj3r7y5wr50l5c3aq0pm4rpzyb7hz50i0k8ig")))

(define-public crate-etherparse-0.10.0 (c (n "etherparse") (v "0.10.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0wwlvci4hqrab3q0nym620j9yhff882cj28vz4rlzgadivf9399m") (y #t)))

(define-public crate-etherparse-0.10.1 (c (n "etherparse") (v "0.10.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "11k14i5l5xrq89sgvmagxz9fzak9vqpjnlcbwwr2jq4zbbmdmvlz")))

(define-public crate-etherparse-0.11.0 (c (n "etherparse") (v "0.11.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1af6w1bp1670pcvahy3hnrhbgqnhvz6n1iji561w67n9cniijy2x")))

(define-public crate-etherparse-0.12.0 (c (n "etherparse") (v "0.12.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "04d6fxw35c3zv0f4xg1nn6vg2hvb29kiwm85ac28aaafmd58rc5w")))

(define-public crate-etherparse-0.13.0 (c (n "etherparse") (v "0.13.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "146rcbnhlpcbl6c6csfhvz0227wbiwhk13md6acq8211b7m94wl2")))

(define-public crate-etherparse-0.14.0 (c (n "etherparse") (v "0.14.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "05wswnn78kgxwvhwyzjzqyqspa49j1zll0923nfjgdkdcbyayysz") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-etherparse-0.14.1 (c (n "etherparse") (v "0.14.1") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "041sfjibb2lc7k3ilyg21fzx4yr63chzr1ar4sfsl4q2fn0yxl5l") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-etherparse-0.14.2 (c (n "etherparse") (v "0.14.2") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1cf9qf089c8a5xlr7az2wcrr0i0l2zi1q9h2ixwalhsbxc1hd294") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-etherparse-0.14.3 (c (n "etherparse") (v "0.14.3") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0pzsnsii34x079wxi8zls0hadw48mwc8lm946j0yaas5rx4banh9") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-etherparse-0.15.0 (c (n "etherparse") (v "0.15.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1mvh6bym97f40cg6wgh0w4passm4i5xx5ij2l1ka2mqhzrnnws91") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

