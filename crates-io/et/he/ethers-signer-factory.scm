(define-module (crates-io et he ethers-signer-factory) #:use-module (crates-io))

(define-public crate-ethers-signer-factory-1.0.0 (c (n "ethers-signer-factory") (v "1.0.0") (d (list (d (n "ethers-core") (r "^2.0.8") (f (quote ("legacy"))) (d #t) (k 0)) (d (n "ethers-signers") (r "^2.0.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (f (quote ("std"))) (d #t) (k 0)))) (h "1wdyrv8gfzq1rjhir5y4wngndk4l3qf35nd5v17hb2bq6gny2clr") (y #t)))

(define-public crate-ethers-signer-factory-1.0.1 (c (n "ethers-signer-factory") (v "1.0.1") (d (list (d (n "ethers-core") (r "^2.0.8") (f (quote ("legacy"))) (d #t) (k 0)) (d (n "ethers-signers") (r "^2.0.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (f (quote ("std"))) (d #t) (k 0)))) (h "0f4vffnckgmcpkxlk447b33y7hmm2g6zjmgg92zah8q24f9p7j1k")))

(define-public crate-ethers-signer-factory-2.0.0 (c (n "ethers-signer-factory") (v "2.0.0") (d (list (d (n "ethers-contract-derive") (r "^2.0.10") (d #t) (k 2)) (d (n "ethers-core") (r "^2.0.8") (f (quote ("legacy"))) (d #t) (k 0)) (d (n "ethers-signers") (r "^2.0.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "14gb3nf6km21rhz56zsql90si1nnyf726w5li155yv0fqyjw76x8")))

