(define-module (crates-io et he ethereum_serde_utils) #:use-module (crates-io))

(define-public crate-ethereum_serde_utils-1.0.0-beta.0 (c (n "ethereum_serde_utils") (v "1.0.0-beta.0") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1xkrzc6zk5f0xkravzj01myv1csqq4nrfd54vdjjp5vfjqync9y9")))

(define-public crate-ethereum_serde_utils-0.5.0 (c (n "ethereum_serde_utils") (v "0.5.0") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0c3mqdqkq9v0xb15v8s2f71ygzqmzp5bxh6f13lxz1h50ac19gmd")))

(define-public crate-ethereum_serde_utils-0.5.1 (c (n "ethereum_serde_utils") (v "0.5.1") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0sk873h78xlyccz5y8r8jzhxwqrddzgzi9cz4sf3x8w0ld7b130g")))

(define-public crate-ethereum_serde_utils-0.5.2 (c (n "ethereum_serde_utils") (v "0.5.2") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0ksjib871cr2pvv378nk5kzrvws47l9lqx784hy9cil88r8mjkfy")))

