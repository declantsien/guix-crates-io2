(define-module (crates-io et he etherscan_explorer_cmd_line) #:use-module (crates-io))

(define-public crate-etherscan_explorer_cmd_line-0.0.1 (c (n "etherscan_explorer_cmd_line") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "web3-unit-converter") (r "^0.1.1") (d #t) (k 0)))) (h "1xmi382r2hfhhwhsja15gipkadl4gvgdkvr4i8ybayxqq26w9xvs")))

