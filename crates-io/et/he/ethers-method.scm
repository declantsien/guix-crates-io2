(define-module (crates-io et he ethers-method) #:use-module (crates-io))

(define-public crate-ethers-method-0.1.0 (c (n "ethers-method") (v "0.1.0") (d (list (d (n "ethers-contract-derive") (r "^2.0.10") (k 0)) (d (n "ethers-core") (r "^2.0.10") (d #t) (k 0)) (d (n "ethers-providers") (r "^2.0.10") (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1d0sfjnd0b4incd08f61dab4dhr47606ng8k54hs5c2hcsiiwaii")))

