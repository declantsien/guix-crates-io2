(define-module (crates-io et he ethers_macros) #:use-module (crates-io))

(define-public crate-ethers_macros-0.2.2 (c (n "ethers_macros") (v "0.2.2") (d (list (d (n "ethbind") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "15zfv13xcjc3allvyf1iyy4zknxq99ndc56gjswjqfajgqyn7h32")))

