(define-module (crates-io et he ethereum-trie-test) #:use-module (crates-io))

(define-public crate-ethereum-trie-test-0.3.8 (c (n "ethereum-trie-test") (v "0.3.8") (d (list (d (n "ethereum-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ethereum-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "ethereum-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "1sswrsaya54svg0m7874dvzqylxw9wj25c70vxnb7xh3q7avn8q7")))

(define-public crate-ethereum-trie-test-0.3.9 (c (n "ethereum-trie-test") (v "0.3.9") (d (list (d (n "ethereum-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ethereum-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "ethereum-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "00ic0k3fy2cx8yidx80cpanjkfrdxz44zq7935400qapd8zi3yhx")))

