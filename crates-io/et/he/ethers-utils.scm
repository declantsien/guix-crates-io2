(define-module (crates-io et he ethers-utils) #:use-module (crates-io))

(define-public crate-ethers-utils-0.1.0 (c (n "ethers-utils") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "ethers") (r "^1.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 2)))) (h "1a7ls9lnjnf8cj7059jirc4jngr15i0v1qp7g53rdpq0gwb63pag")))

(define-public crate-ethers-utils-0.1.1 (c (n "ethers-utils") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "ethers") (r "^1.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 2)))) (h "1q1lyq12b022sbcj2ybkajs0q2qk7v4i8szwncijd87y8xm4ycf7") (f (quote (("erc20"))))))

(define-public crate-ethers-utils-0.1.3 (c (n "ethers-utils") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "ethers") (r "^1.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 2)))) (h "08zphnnwdf5xqxx34f2i0vxxrw397l6l7h5f9g9gkqjjjb9xpz3r") (f (quote (("erc20"))))))

(define-public crate-ethers-utils-0.1.4 (c (n "ethers-utils") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "ethers") (r "^1.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 2)))) (h "070nk83dy6vr4qrwcsv5irklfmlvapcf7sb0izd2lg289fdram2y") (f (quote (("erc20"))))))

(define-public crate-ethers-utils-1.0.0 (c (n "ethers-utils") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "ethers") (r "^1.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 2)))) (h "09qykpaak9266hgdxcdyhf5n5a5hmm9a25m0nmgqcqrq8svb34fp") (f (quote (("erc20")))) (y #t)))

(define-public crate-ethers-utils-0.2.0 (c (n "ethers-utils") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "ethers") (r "^1.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 2)))) (h "14h2vg0n3xf58yizsfyp9j51czf18p10iqc45wwq049iqmkfa1wm") (f (quote (("erc20"))))))

