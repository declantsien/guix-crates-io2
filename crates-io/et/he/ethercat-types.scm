(define-module (crates-io et he ethercat-types) #:use-module (crates-io))

(define-public crate-ethercat-types-0.1.0 (c (n "ethercat-types") (v "0.1.0") (h "09vbln58h7i377ywkj5yi233nmbhmkpb6nsljyxq1qh4zb2088nm")))

(define-public crate-ethercat-types-0.1.1 (c (n "ethercat-types") (v "0.1.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1f24nvr4fkckq4j7h3xjjnqxaj2irrlmcdp6jgmsqj251ppnhzwd")))

(define-public crate-ethercat-types-0.2.0 (c (n "ethercat-types") (v "0.2.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "10d47acvlvpn9d1wqzb3w0kjim6zixnklrkhh6h4xdnfb5cphsm2")))

(define-public crate-ethercat-types-0.3.0 (c (n "ethercat-types") (v "0.3.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "03k8izibsgaj6aq4bfkb5p8y7dmpfcciq1p3w6s5jwpvdyw7w8h5")))

(define-public crate-ethercat-types-0.3.1 (c (n "ethercat-types") (v "0.3.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0gl4wihf31hli9ligc0mwdfb6z42vkgz2z5fp18m3ibsfggf0qkb")))

(define-public crate-ethercat-types-0.3.2 (c (n "ethercat-types") (v "0.3.2") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0wic4iad2aj1mva6q2zjv2ipkxp7vxkwz2i35jnw5mz8yqfnm9yv")))

(define-public crate-ethercat-types-0.3.3 (c (n "ethercat-types") (v "0.3.3") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0334rn86pf4q87sm5jrskb8kxzqamprspmsxw2fcpsq9lxqix6xx")))

(define-public crate-ethercat-types-0.3.4 (c (n "ethercat-types") (v "0.3.4") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ypyg5973fw2vk088jalcg0hnj0l00x0ysldspc695aw00sh3pyd")))

(define-public crate-ethercat-types-0.3.5 (c (n "ethercat-types") (v "0.3.5") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1q6lnwr66zhhm7xjf81drxjcckm043a5jlj1jcs3n1ag85klv9ph")))

