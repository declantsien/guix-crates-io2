(define-module (crates-io et he ethercat-esi) #:use-module (crates-io))

(define-public crate-ethercat-esi-0.1.0 (c (n "ethercat-esi") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)))) (h "0c6flp9wh7p9w2iadzk4g2037waiwsiymn2zld3iar1r493rhbic")))

(define-public crate-ethercat-esi-0.2.0 (c (n "ethercat-esi") (v "0.2.0") (d (list (d (n "ethercat-types") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)))) (h "16b6cqv5k0y6lh18fx881fq9jcff557dgpq39dzib96j0cjdcvwm")))

