(define-module (crates-io et he ethers-impl-rlp) #:use-module (crates-io))

(define-public crate-ethers-impl-rlp-0.3.0 (c (n "ethers-impl-rlp") (v "0.3.0") (d (list (d (n "rlp") (r "^0.5.2") (f (quote ("std"))) (k 0)))) (h "07dw4v3dwrb47bymzlvf8gj0mq6s4jlqlpnihfbiw9jq6jsmkd36") (f (quote (("std" "rlp/std") ("default" "std")))) (r "1.56.1")))

