(define-module (crates-io et he ether) #:use-module (crates-io))

(define-public crate-ether-0.0.1 (c (n "ether") (v "0.0.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "132268rng9s74140vc2ngswdwi7791plph5bhrs6zi2vwgjx72jj")))

(define-public crate-ether-0.0.2 (c (n "ether") (v "0.0.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0xcv0cdzwsmamh4phq2mkzpdjn886h8yn6q4c2sy09lhd9mk564p")))

(define-public crate-ether-0.0.3 (c (n "ether") (v "0.0.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0yxjfzx57pv2p7a9h0p0p3f3y1aavnbz7hi2b51rqg2zilkbich6")))

(define-public crate-ether-0.0.4 (c (n "ether") (v "0.0.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0b45x35f9d8yjlbxyni4yph1h1mwp08ch7dpsip17qmf8gc69lz1")))

(define-public crate-ether-0.1.0 (c (n "ether") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "02656dsalrgg30ag5qgxr0bm5kjpimj05ads3p2fqs576br6rf9l")))

(define-public crate-ether-0.1.1 (c (n "ether") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1dvw5ibm3knwscqb2hzicll2ycagxngcgwjsn881llbsj97hsg25")))

(define-public crate-ether-0.1.2 (c (n "ether") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0prvj5d7i7zchv8vz6pm8s994mhmk5sqi4096pm0dymp2yam20ci")))

(define-public crate-ether-0.1.3 (c (n "ether") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "11r222fc933dx8bc2206nwcaz869hh7x41x1z51hrynrin3n701a")))

(define-public crate-ether-0.1.4 (c (n "ether") (v "0.1.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1wbmfwz09k3cfmmjwgy730wi6z0r2g99k6grj9930x556sp880dh")))

