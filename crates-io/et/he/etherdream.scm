(define-module (crates-io et he etherdream) #:use-module (crates-io))

(define-public crate-etherdream-0.0.2 (c (n "etherdream") (v "0.0.2") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "ilda") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)))) (h "1vg4903f068s1sw6vf174fwwspaakq5dnk5xbybg9y4inr1srwrm")))

(define-public crate-etherdream-0.0.3 (c (n "etherdream") (v "0.0.3") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "ilda") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)))) (h "0rgs5ql75rlhbq2gchykxd1v17zz3c4nz217b66n7virg68g2ln4")))

(define-public crate-etherdream-0.0.4 (c (n "etherdream") (v "0.0.4") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "ilda") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)))) (h "1ja4xwzp0qcfagifhbphcp8zdzpr8di94wms39wwa7rpgc6zlhb4")))

(define-public crate-etherdream-0.0.5 (c (n "etherdream") (v "0.0.5") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "ilda") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)))) (h "1w7h1ax8yl40d8l2p9nda4zd7snkh50bwp7w4fxyj0y02rg29qav")))

(define-public crate-etherdream-0.0.6 (c (n "etherdream") (v "0.0.6") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "ilda") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)))) (h "1g739gk2p7zq3880p7nmy5ybvziyfg88bssj04175wx0xzdsgskb")))

(define-public crate-etherdream-0.0.7 (c (n "etherdream") (v "0.0.7") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "ilda") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)))) (h "0znp1b1gha8mpfng7025cg5gwvwjslyfb16ngyiwrz8jhp759kx5")))

(define-public crate-etherdream-0.0.8 (c (n "etherdream") (v "0.0.8") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "camera_capture") (r "^0.3") (d #t) (k 2)) (d (n "ilda") (r "^0.0.2") (d #t) (k 0)) (d (n "image") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)))) (h "1r75ngy0wyar8yrmlmjw93pdjlcnvisnmi634q8aanwpz039af2w")))

(define-public crate-etherdream-0.1.0 (c (n "etherdream") (v "0.1.0") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "camera_capture") (r "^0.3") (d #t) (k 2)) (d (n "ilda") (r "^0.0.2") (d #t) (k 0)) (d (n "image") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)) (d (n "point") (r "0.3.*") (d #t) (k 0)))) (h "0jwgi9x4zmf1bchl7a8q1s4hcfqs1xmkyl4krk8mh20kqkj6cmlw")))

(define-public crate-etherdream-0.1.1 (c (n "etherdream") (v "0.1.1") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "ilda") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)) (d (n "point") (r "0.3.*") (d #t) (k 0)))) (h "0i7sjav7v0xfa5pw50zjwpnkh29xnj8l9rr8wj38yz35mqm04q65")))

