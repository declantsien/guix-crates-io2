(define-module (crates-io et he ethercat-sys) #:use-module (crates-io))

(define-public crate-ethercat-sys-0.1.0 (c (n "ethercat-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.3") (d #t) (k 1)) (d (n "ioctl-sys") (r "^0.5.2") (d #t) (k 0)))) (h "1j1v6y3f8gddg9c6mgy3a8a7nxbchmqxbrggcwsg9i7gg6wl5qgw")))

(define-public crate-ethercat-sys-0.1.1 (c (n "ethercat-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "ioctl-sys") (r "^0.5.2") (d #t) (k 0)))) (h "11mfxvjf419xrnbp87q7mmy9v2p14sl8dzrpjq4q1af9mgdfvn7p")))

(define-public crate-ethercat-sys-0.1.2 (c (n "ethercat-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "ioctl-sys") (r "^0.5.2") (d #t) (k 0)))) (h "0ngjl2026bj3w8ky7pdsnk87bdfyg0gs9jna71rh64r5x4c6b1pw")))

(define-public crate-ethercat-sys-0.1.3 (c (n "ethercat-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "ioctl-sys") (r "^0.5.2") (d #t) (k 0)))) (h "156ws1rixdfzcmhjpx3xr9l8vfy5q5i7gwl2qn8xvpfz21xzd0wp")))

(define-public crate-ethercat-sys-0.2.0 (c (n "ethercat-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "ioctl-sys") (r "^0.5.2") (d #t) (k 0)))) (h "1yh3j3a2f66gwmi2xm941mv7j397ww8r29yky4lkwkrmcllpki07") (f (quote (("sncn") ("pregenerated-bindings") ("default"))))))

(define-public crate-ethercat-sys-0.3.0 (c (n "ethercat-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "ioctl-sys") (r "^0.5.2") (d #t) (k 0)))) (h "145ci0vwn6j401f87cxvjjwc5b8aw6ar05h3xpq0w2a3xwxnsqfk") (f (quote (("sncn") ("pregenerated-bindings") ("default"))))))

(define-public crate-ethercat-sys-0.3.1 (c (n "ethercat-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "ioctl-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 1)))) (h "1r44bz2vzdqg9b661fy9yn2jrbcpfxb9774vw35zc6z19fyrfk4g") (f (quote (("sncn") ("pregenerated-bindings") ("default"))))))

