(define-module (crates-io et he ether-dream) #:use-module (crates-io))

(define-public crate-ether-dream-0.1.0 (c (n "ether-dream") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "17qilp3dgjcm24dpjkx526y4zw2wqrvxdgkrz8fv5ajlh87jx0x2")))

(define-public crate-ether-dream-0.2.0 (c (n "ether-dream") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "0ivx6znqra3vz9jc22igghc6aa3vma1sc2rjmfi6w28qvnzjcp2s")))

(define-public crate-ether-dream-0.2.1 (c (n "ether-dream") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "1z59p0fg4qgm95b6zj1vcrkdpzfjhqisa678xy67cpnbdm1s13pr")))

(define-public crate-ether-dream-0.2.2 (c (n "ether-dream") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "0imvy88i6bhqr8r9i1gn9fininlmwxi1vfl23zjn5zvx53kkyrda")))

(define-public crate-ether-dream-0.2.3 (c (n "ether-dream") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "1wyd42d6yj1g52hbxjbm7ccy3vb80mb3i2izdp693ixjc3ayir4g")))

(define-public crate-ether-dream-0.2.4 (c (n "ether-dream") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "1b0lj72qjlm8yv49yil5l7gqsy44lx19w8v9d3i8ym8j1phf5fqa")))

(define-public crate-ether-dream-0.2.5 (c (n "ether-dream") (v "0.2.5") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "0wjp65l9apmdwxyzga3xq8a3p9vhikqjjwkq350jpmygi3f2g8aq")))

