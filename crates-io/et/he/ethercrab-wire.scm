(define-module (crates-io et he ethercrab-wire) #:use-module (crates-io))

(define-public crate-ethercrab-wire-0.1.0 (c (n "ethercrab-wire") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "ethercrab-wire-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (k 0)))) (h "0zanbk8g94h6dzm6knvc5rk0rn0100b8rpwhxmirqzqmkhw1k3sy") (f (quote (("std")))) (s 2) (e (quote (("defmt-03" "dep:defmt" "heapless/defmt-03")))) (r "1.75")))

(define-public crate-ethercrab-wire-0.1.1 (c (n "ethercrab-wire") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "ethercrab-wire-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (k 0)))) (h "176bwlfnfzglkf64v1xg13rx7ipv18d2pmm3z70fdp7hfzcvw9bc") (f (quote (("std")))) (s 2) (e (quote (("defmt-03" "dep:defmt" "heapless/defmt-03")))) (r "1.75")))

(define-public crate-ethercrab-wire-0.1.2 (c (n "ethercrab-wire") (v "0.1.2") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "ethercrab-wire-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (k 0)))) (h "11dprgvkv3k2grbcqjcrjn2h7y9gnh66avjz9afyvr3602fvg1sh") (f (quote (("std")))) (s 2) (e (quote (("defmt-03" "dep:defmt" "heapless/defmt-03")))) (r "1.75")))

(define-public crate-ethercrab-wire-0.1.3 (c (n "ethercrab-wire") (v "0.1.3") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "ethercrab-wire-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (k 0)))) (h "1484n7x6rbx8cpm4k6jxif82vx7hkpr5x3rjq866p3l8ii93ngr1") (f (quote (("std")))) (s 2) (e (quote (("defmt-03" "dep:defmt" "heapless/defmt-03")))) (r "1.75")))

(define-public crate-ethercrab-wire-4.0.0-rc.2 (c (n "ethercrab-wire") (v "4.0.0-rc.2") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "ethercrab-wire-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (k 0)))) (h "1ixjwwifapx088p1qkgq4lmwcmp6paqn38f5iq8d5h1iswfhyi0n") (f (quote (("std")))) (y #t) (s 2) (e (quote (("defmt-03" "dep:defmt" "heapless/defmt-03")))) (r "1.75")))

(define-public crate-ethercrab-wire-0.1.4 (c (n "ethercrab-wire") (v "0.1.4") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "ethercrab-wire-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (k 0)))) (h "1dic2zg0pp02iw4n1xaxg7g61w8lrsjl4827zb9x904cpd0n2g0p") (f (quote (("std")))) (s 2) (e (quote (("defmt-03" "dep:defmt" "heapless/defmt-03")))) (r "1.75")))

