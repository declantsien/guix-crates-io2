(define-module (crates-io et he ether-dream-dac-emulator) #:use-module (crates-io))

(define-public crate-ether-dream-dac-emulator-0.1.0 (c (n "ether-dream-dac-emulator") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "ether-dream") (r "^0.1") (d #t) (k 0)) (d (n "nannou") (r "^0.5") (d #t) (k 2)))) (h "1781pp83lndwd0y4r8gzg3k49d0kdbnp900sccickpl4rp1g4mnc")))

(define-public crate-ether-dream-dac-emulator-0.2.0 (c (n "ether-dream-dac-emulator") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "ether-dream") (r "^0.2") (d #t) (k 0)) (d (n "nannou") (r "^0.5") (d #t) (k 2)))) (h "0v9011wrnl4l6ivzy2nkcvynsjls8iy3r7hx2r0i037a02rfbz37")))

(define-public crate-ether-dream-dac-emulator-0.2.1 (c (n "ether-dream-dac-emulator") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "ether-dream") (r "^0.2") (d #t) (k 0)) (d (n "nannou") (r "^0.13") (d #t) (k 2)))) (h "0rk5jcdi4xchls8cmpc6cwdcwan9qcx2nbxqgq7jj34ccg3xc1yb")))

(define-public crate-ether-dream-dac-emulator-0.3.0 (c (n "ether-dream-dac-emulator") (v "0.3.0") (d (list (d (n "ether-dream") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nannou") (r "^0.14") (d #t) (k 2)) (d (n "piper") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.1") (d #t) (k 0)))) (h "098wnx1wbx4f8wda118mkfagd78m8da7zifg93zm7fwas3d0dx49")))

