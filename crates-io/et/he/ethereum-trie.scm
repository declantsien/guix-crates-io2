(define-module (crates-io et he ethereum-trie) #:use-module (crates-io))

(define-public crate-ethereum-trie-0.3.8 (c (n "ethereum-trie") (v "0.3.8") (d (list (d (n "ethereum-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ethereum-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "ethereum-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "12gwq64ndz8ah2960v7r4wrdxxc3bmiplm9fx7b0v2f6j2hh830d")))

(define-public crate-ethereum-trie-0.4.0 (c (n "ethereum-trie") (v "0.4.0") (d (list (d (n "ethereum-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ethereum-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "ethereum-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "03bilakkfc8r41vjhhd2ip1g8jxbciqhxgf3f93csyz7v33hz9zi")))

(define-public crate-ethereum-trie-0.5.0 (c (n "ethereum-trie") (v "0.5.0") (d (list (d (n "ethereum-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ethereum-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "ethereum-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0ax6a0i36ai153h0wb6x5g6pn6ifkklygkqq1r7nf0zl7id2hbay")))

