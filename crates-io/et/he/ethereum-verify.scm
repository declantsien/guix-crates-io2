(define-module (crates-io et he ethereum-verify) #:use-module (crates-io))

(define-public crate-ethereum-verify-0.21.8 (c (n "ethereum-verify") (v "0.21.8") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "12lvyf4pgzfvk72416nb585zjkxia7571iin5rmkq6djsjldb5zz")))

(define-public crate-ethereum-verify-0.21.10 (c (n "ethereum-verify") (v "0.21.10") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0smw1hv3d3zzip89xj02bryvnr7qvpkav59rpv6m7byqm4nzv7j2")))

(define-public crate-ethereum-verify-0.21.12 (c (n "ethereum-verify") (v "0.21.12") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0jn3q84pj1hpvb1vmcq987fjmz9ickrk6bmlm25fibb2lmmm3ara")))

(define-public crate-ethereum-verify-0.22.1 (c (n "ethereum-verify") (v "0.22.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0563wc6yqy27ngpwwhgi8xfjmzz6ynf0wmjr0rjjly4m15zh1n4a")))

(define-public crate-ethereum-verify-0.22.2 (c (n "ethereum-verify") (v "0.22.2") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1x97jnq0q9mqkgk4s6nhmcc892jxhqiksh3rk231wfchq6yriq57")))

(define-public crate-ethereum-verify-0.22.3 (c (n "ethereum-verify") (v "0.22.3") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "112is1wmssm95szy2bvx8r74znbhsivgyql9fjmkymwwpbw3iq4c")))

(define-public crate-ethereum-verify-0.22.4 (c (n "ethereum-verify") (v "0.22.4") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "00np5l8j1zkvq7rzyvqb2k41gdzqncy6x6m37ypnhy1bv00yrn6l")))

(define-public crate-ethereum-verify-0.22.5 (c (n "ethereum-verify") (v "0.22.5") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "02qhqr5klw4ya649r9d0bvwf3xc7fp39gbx256nsi8rz57vjwp15")))

(define-public crate-ethereum-verify-0.22.6 (c (n "ethereum-verify") (v "0.22.6") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0m28wfqv93bnjmib85cc1kpdawf21v2rkbgyvql6g2lh75dg0rgl")))

(define-public crate-ethereum-verify-0.22.7 (c (n "ethereum-verify") (v "0.22.7") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "02aag02d52973c83ppizl2hj0izmx6z8nbwzgdd2w1d2k7d397rh")))

(define-public crate-ethereum-verify-0.22.8 (c (n "ethereum-verify") (v "0.22.8") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1qfds7n003zwd55njkvpd7isirlfsjphns52j5b4fbgb15m4sacf")))

(define-public crate-ethereum-verify-0.22.9 (c (n "ethereum-verify") (v "0.22.9") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0wqzbs1fqbfymqgi16hl7laxxndnyancz9wbni213n9xdqaqvz6i")))

(define-public crate-ethereum-verify-0.22.10 (c (n "ethereum-verify") (v "0.22.10") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0cpzg8m00a347hhq3mz2x8q5p411av00hdlawr7n40j7dr3g62wn")))

(define-public crate-ethereum-verify-0.22.11 (c (n "ethereum-verify") (v "0.22.11") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1398jd1i8pnrjsn7khpyq1vxwalv1qn12l10gvqrzn3x8vv68h56")))

(define-public crate-ethereum-verify-0.23.0 (c (n "ethereum-verify") (v "0.23.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0drh2g36krb4y92ns7aid14r8lz5zrrrzbzn58mbs9f611jy1k11")))

(define-public crate-ethereum-verify-0.23.1 (c (n "ethereum-verify") (v "0.23.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "13zsdazi4y0kfil2fdg36a3is9ziaakyvvqpf455hfw68g0fhcrv")))

(define-public crate-ethereum-verify-0.24.0 (c (n "ethereum-verify") (v "0.24.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1rkj9lk2w8jl8gfblgv3wq2f82y8b6khim833f6dg18zg4zxxxwp")))

(define-public crate-ethereum-verify-0.24.1 (c (n "ethereum-verify") (v "0.24.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0acix6smsi1yvrqkabn29zf2wjc02s08x8nkbvz509sq57cpg9ws")))

(define-public crate-ethereum-verify-0.25.0 (c (n "ethereum-verify") (v "0.25.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1xrqls89qfaisllqbhgsr7aqcrf3xwcb42z9kf4kqksn93jjf4z4")))

(define-public crate-ethereum-verify-2.1.0 (c (n "ethereum-verify") (v "2.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0pi24xhr55pz7620lgspx14lwn5sal8wn7c9b0avriwk85fjrhni")))

(define-public crate-ethereum-verify-2.2.0 (c (n "ethereum-verify") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0yzwp829v2g6x805c7mda4m1g8rbbb8yknylf7faapmh98yvr5k7")))

(define-public crate-ethereum-verify-2.3.0 (c (n "ethereum-verify") (v "2.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0wckwrbrji63im1lk8qhanxm4c1npr0bm02da7nymc1yxx59vlc3")))

(define-public crate-ethereum-verify-2.3.1 (c (n "ethereum-verify") (v "2.3.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "09najda5d4qr181jdxkcrk04l802wqls2mhb4h2rxc3139knkn9v")))

(define-public crate-ethereum-verify-3.0.0 (c (n "ethereum-verify") (v "3.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0qx1fz7vqn2p1lh3804bxfp8h3yn3dfbsnmd676d0vgbx5asfjgh")))

(define-public crate-ethereum-verify-2.4.0 (c (n "ethereum-verify") (v "2.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1n77kkpyzqs4xw8difq0xd4nw9f9n3armf2fs25ih9g31zb5bnpg")))

(define-public crate-ethereum-verify-3.1.0 (c (n "ethereum-verify") (v "3.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0kwxh3910j0dqcrr3bqyrp1z3q3gzrjvrgszgz53sk5cvn0z35vv")))

(define-public crate-ethereum-verify-3.2.0 (c (n "ethereum-verify") (v "3.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0pk3j4m4ilrpxvm91686ghsma260w5505l3g0jxq1i12ng3ml842")))

(define-public crate-ethereum-verify-3.2.2 (c (n "ethereum-verify") (v "3.2.2") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0gbm1q74ld9lrqvin03p8dyic7h5gardpxgk012zv6668v3gw79s")))

(define-public crate-ethereum-verify-3.2.3 (c (n "ethereum-verify") (v "3.2.3") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1jvnw6ylyvjclnwlxza8giqnx380w6hxj4z8lc1anvr73ahnn20x")))

(define-public crate-ethereum-verify-3.2.4 (c (n "ethereum-verify") (v "3.2.4") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "147r89qakyn0wzs8ssc72ks5dbpi4czi3y029pv81c1sk6vi72zj")))

(define-public crate-ethereum-verify-3.2.5 (c (n "ethereum-verify") (v "3.2.5") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0g82lvl1c5lyprfhv6w3mj09sn6zygy9x989m7jgzja8syy4kqra")))

(define-public crate-ethereum-verify-3.2.6 (c (n "ethereum-verify") (v "3.2.6") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1py8gqb75ks9qfc9g1417qkiam0dyl873qa14fkinaw8mkxvjffg")))

(define-public crate-ethereum-verify-3.2.7 (c (n "ethereum-verify") (v "3.2.7") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "16frywxzi05vqrkhn2c2aza87gx28v1n26j64bsv7f17gdv82ssw")))

(define-public crate-ethereum-verify-3.2.8 (c (n "ethereum-verify") (v "3.2.8") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0dp37j21mfcg9yb0307ajn2v0215j3dcj8khbmg4b6ywvg3zl8z1")))

(define-public crate-ethereum-verify-3.2.9 (c (n "ethereum-verify") (v "3.2.9") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1j5c9j5nmdjzgkcgg2v5n4mf2bkygmnl025lqmag7amfs74g0sza")))

(define-public crate-ethereum-verify-3.3.0 (c (n "ethereum-verify") (v "3.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "179mmb28687x4ci76681cmy3ih80hcpn992bs0vfmpqbrr2ydbc1")))

(define-public crate-ethereum-verify-3.4.0 (c (n "ethereum-verify") (v "3.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1044g91lmc1azpc8fcgncx53h3cqpxargyykrf0q095440mzb23a")))

(define-public crate-ethereum-verify-3.5.0 (c (n "ethereum-verify") (v "3.5.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0ipkzwm67rp486cxq1804wbbqscm7mbrjkf8nvb9ms9y72fj3ncj")))

(define-public crate-ethereum-verify-3.6.0 (c (n "ethereum-verify") (v "3.6.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0ijx50wiqwsxcl1wc3rvrpga1y63h384nvbrbck853fawd14vxpm")))

(define-public crate-ethereum-verify-3.7.0 (c (n "ethereum-verify") (v "3.7.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1bqgykk0zm7wikrnky7k344ri72sdcwwrh77lax9pkr3ww492yfc")))

(define-public crate-ethereum-verify-3.8.0 (c (n "ethereum-verify") (v "3.8.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0lvl6jl6v92a5gfkm1ahh4136551w9khliixqz32fc1q48bhcavc")))

(define-public crate-ethereum-verify-3.9.0 (c (n "ethereum-verify") (v "3.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "09wr1n0849ldy82i608ki4k1dj7035jqbiw0f1d5im93x13z4n3b")))

(define-public crate-ethereum-verify-3.10.0 (c (n "ethereum-verify") (v "3.10.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "03ydc5ny6awv2ww3ka262nz0512k5ib8r0cacsp08lh3a2v5pay7")))

(define-public crate-ethereum-verify-3.11.0 (c (n "ethereum-verify") (v "3.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0nca6sbyjd9awsdi5gcr688gxclshnid9b1rmzh01qbmb77d3f4x")))

(define-public crate-ethereum-verify-3.12.0 (c (n "ethereum-verify") (v "3.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0c4a9fiq4pj3hfaiq0cy01j194dkshr78rn3zard36196m84rcv3")))

(define-public crate-ethereum-verify-3.13.0 (c (n "ethereum-verify") (v "3.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1cr5r4rzyb9wfcj7ngd3xz5m8ai4j2axv50r927hj3fr0i4yni3c")))

