(define-module (crates-io et he ether_convert) #:use-module (crates-io))

(define-public crate-ether_convert-0.1.0 (c (n "ether_convert") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15q4aqgwcdx9fbjd3laialrkn6v64z1c78ndi9dg7wgsd5rmd05p")))

(define-public crate-ether_convert-0.1.1 (c (n "ether_convert") (v "0.1.1") (d (list (d (n "bigdecimal") (r "^0.0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "16pp9vh55ld7mb804k15b8na8r8mfk5cx7jvbf8m11m6pmr6wixk")))

(define-public crate-ether_convert-0.1.2 (c (n "ether_convert") (v "0.1.2") (d (list (d (n "bigdecimal") (r "^0.0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "019033vwn77gjyfv2y59maarm1g7z8hvjrsyg98i1m8bq081rfr0")))

