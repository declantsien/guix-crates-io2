(define-module (crates-io et he ethernet-info) #:use-module (crates-io))

(define-public crate-ethernet-info-0.0.1-alphav1 (c (n "ethernet-info") (v "0.0.1-alphav1") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "0jxxr8v7qyy4rrm6v7ibm3hqydzpm3wv7pljii9dd3q2kp129qqc")))

(define-public crate-ethernet-info-0.0.1-alphav2 (c (n "ethernet-info") (v "0.0.1-alphav2") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "1px71zb14y67lmss7y02wr4k055a9cjik29ygs65qbxn46x1x74a")))

(define-public crate-ethernet-info-0.0.1 (c (n "ethernet-info") (v "0.0.1") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "1x2iq2dnsjxybkaazvr7lc6byq6250scwg6yg8bz95jzq4gq63vk") (y #t)))

(define-public crate-ethernet-info-0.0.2 (c (n "ethernet-info") (v "0.0.2") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "13lhliwyqhfm75px5dwzvhfwyaqhp0y8ad5cxakc1mbcsafi67yb") (y #t)))

(define-public crate-ethernet-info-0.0.3 (c (n "ethernet-info") (v "0.0.3") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "0idv89h6nv9nyjvk6yqg3dmp004v44k3g5zaz2nlfjqls8ry088q") (y #t)))

(define-public crate-ethernet-info-0.0.4 (c (n "ethernet-info") (v "0.0.4") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "1lsr0pqqira2id14ws0mskycp2rz21am6rqr6jfwx10s2iwaxn33")))

