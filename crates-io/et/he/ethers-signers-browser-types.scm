(define-module (crates-io et he ethers-signers-browser-types) #:use-module (crates-io))

(define-public crate-ethers-signers-browser-types-0.1.0 (c (n "ethers-signers-browser-types") (v "0.1.0") (d (list (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06szq81a3kh53rk42j0ymhc3z45p5kw19rghfqxbgbs0797l88yq") (y #t) (r "1.65")))

