(define-module (crates-io et he ethercat-soem-ctx) #:use-module (crates-io))

(define-public crate-ethercat-soem-ctx-0.1.0 (c (n "ethercat-soem-ctx") (v "0.1.0") (d (list (d (n "ethercat-soem-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04cr38xlivqxq9bp5w24h867h2n6v2yfbyfcycr9pbxmppp6ysx9") (f (quote (("issue-224-workaround" "ethercat-soem-sys/issue-224-workaround"))))))

(define-public crate-ethercat-soem-ctx-0.1.1 (c (n "ethercat-soem-ctx") (v "0.1.1") (d (list (d (n "ethercat-soem-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yxi1yjm6iq0232s7827gr3wx7l6jfx9rg25kcxaccbrlps98x6r") (f (quote (("issue-224-workaround" "ethercat-soem-sys/issue-224-workaround"))))))

(define-public crate-ethercat-soem-ctx-0.2.0 (c (n "ethercat-soem-ctx") (v "0.2.0") (d (list (d (n "ethercat-soem-sys") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "180z9jrjq0dznjh2xn1fk5nd990ql6gzgn8c3flpn21cr6kdcg9h") (f (quote (("issue-224-workaround" "ethercat-soem-sys/issue-224-workaround"))))))

