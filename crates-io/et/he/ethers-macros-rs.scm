(define-module (crates-io et he ethers-macros-rs) #:use-module (crates-io))

(define-public crate-ethers-macros-rs-0.1.0 (c (n "ethers-macros-rs") (v "0.1.0") (d (list (d (n "ethers-hardhat-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vpsrlyjizh2b81i36zh4p9zwimmmw0h384ayml6wgl848dxagjw")))

