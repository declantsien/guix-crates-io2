(define-module (crates-io et he ethereumvm-precompiled-bn128) #:use-module (crates-io))

(define-public crate-ethereumvm-precompiled-bn128-0.11.0 (c (n "ethereumvm-precompiled-bn128") (v "0.11.0") (d (list (d (n "bn-plus") (r "^0.4") (d #t) (k 0)) (d (n "ethereum-bigint") (r "^0.2") (k 0)) (d (n "ethereumvm") (r "^0.11") (k 0)))) (h "0n0r6z5wrxhcqyivi6186k208qhdp6p9686sdr05296m3saal7gq") (f (quote (("std" "ethereumvm/std") ("rust-secp256k1" "ethereumvm/rust-secp256k1") ("rlp" "ethereum-bigint/rlp") ("default" "std" "rust-secp256k1") ("c-secp256k1" "ethereumvm/c-secp256k1"))))))

