(define-module (crates-io et he ethercat-derive) #:use-module (crates-io))

(define-public crate-ethercat-derive-0.1.0 (c (n "ethercat-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (d #t) (k 0)))) (h "09b9qz0lriskrlg7rhq65ryzkirc3gg9z0s9mksyzxpkbvi2k4k3")))

(define-public crate-ethercat-derive-0.1.1 (c (n "ethercat-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (d #t) (k 0)))) (h "0jj92nclwrkw3wd9lha83pxik9b14wf40l7hl85yhiv7x504vrgq")))

(define-public crate-ethercat-derive-0.1.3 (c (n "ethercat-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04jpx6zgbj0z980n4ff4vvdjay2icpjk13fndadz5v6ys8kqcq6r")))

(define-public crate-ethercat-derive-0.1.4 (c (n "ethercat-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m64c911bjbsbxqi1zhdbnkj7b5x1qz5alf7r2gfvxv59if49qz2")))

(define-public crate-ethercat-derive-0.2.0 (c (n "ethercat-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0c8g44ml1bfn71wcj447mbvbb8gppql58iqwskl95phfll96b10v")))

