(define-module (crates-io et he ethereum-bloom) #:use-module (crates-io))

(define-public crate-ethereum-bloom-0.2.1 (c (n "ethereum-bloom") (v "0.2.1") (d (list (d (n "ethereum-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ethereum-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "ethereum-rlp") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.6") (d #t) (k 0)))) (h "0ca2hrrg9b5hy14a1ksp96pn5dic2xgkddfpivdfixk1zwhq27x9")))

