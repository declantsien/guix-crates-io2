(define-module (crates-io et he ether-type) #:use-module (crates-io))

(define-public crate-ether-type-0.1.0 (c (n "ether-type") (v "0.1.0") (d (list (d (n "bin-utils") (r "^0.1.0") (k 0)))) (h "17v7y8dhzv7s269hvpqh4pf6r9byjjwxqhdblqb4isdxxy1jd141") (f (quote (("default" "debug") ("debug"))))))

(define-public crate-ether-type-0.1.1 (c (n "ether-type") (v "0.1.1") (d (list (d (n "bin-utils") (r "^0.2.1") (k 0)))) (h "1dqawlj4ryq3nqfki5a1b06q6s8y57crm497s7989zqnlvgk7704") (f (quote (("default" "debug") ("debug"))))))

(define-public crate-ether-type-0.1.2 (c (n "ether-type") (v "0.1.2") (d (list (d (n "macro-bits") (r "^0.1.4") (d #t) (k 0)))) (h "0p52p5kkn5nl28v7ahvs80xgg4rzzkl4dfcd7avvhg8ig2s8135h")))

(define-public crate-ether-type-0.1.3 (c (n "ether-type") (v "0.1.3") (d (list (d (n "macro-bits") (r "^0.1.5") (d #t) (k 0)))) (h "0mg1q50agyp480149l1gfz1xaj2g1bxbsb0rkkbkiwngi076bb0b")))

