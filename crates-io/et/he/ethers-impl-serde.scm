(define-module (crates-io et he ethers-impl-serde) #:use-module (crates-io))

(define-public crate-ethers-impl-serde-0.4.0 (c (n "ethers-impl-serde") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "uint") (r "^0.9.5") (d #t) (k 2) (p "ethers-uint-rs")))) (h "1ld4l0yrvcs6w0hjxgm1xvf7rj0brv4qzkbb1lwn2n994z4gi824") (f (quote (("std" "serde/std") ("default" "std")))) (r "1.56.1")))

