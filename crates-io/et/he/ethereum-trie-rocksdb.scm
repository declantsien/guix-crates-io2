(define-module (crates-io et he ethereum-trie-rocksdb) #:use-module (crates-io))

(define-public crate-ethereum-trie-rocksdb-0.5.0 (c (n "ethereum-trie-rocksdb") (v "0.5.0") (d (list (d (n "ethereum-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ethereum-trie") (r "^0.5") (d #t) (k 0)) (d (n "ethereum-trie-memory") (r "^0.5") (d #t) (k 0)) (d (n "parity-rocksdb") (r "^0.5.0") (d #t) (k 0)))) (h "0b31gq3hsbc84fvysmbb9ylx6bbx6hj4hyv56b768pi8x87z98vz")))

