(define-module (crates-io et he ethereum-private-key-to-address) #:use-module (crates-io))

(define-public crate-ethereum-private-key-to-address-0.1.0 (c (n "ethereum-private-key-to-address") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "1yrcspplkhm7yp0dps7i0y070xi3ig2141j0jd6ry3ghml2lvidp")))

