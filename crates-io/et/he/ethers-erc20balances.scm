(define-module (crates-io et he ethers-erc20balances) #:use-module (crates-io))

(define-public crate-ethers-erc20balances-0.1.0 (c (n "ethers-erc20balances") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "auto_impl") (r "^1.1") (d #t) (k 0)) (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.172") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0s21yy8c61dmph10561zcakwam1z4bpsdjlz5qipmazsg803nx3y")))

(define-public crate-ethers-erc20balances-0.1.1 (c (n "ethers-erc20balances") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "auto_impl") (r "^1.1") (d #t) (k 0)) (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.172") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1khj2h3hm28agsqsq8cyhnjxi0crdv7d9147dh2llrhxqm2xaq9j")))

(define-public crate-ethers-erc20balances-0.1.2 (c (n "ethers-erc20balances") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "auto_impl") (r "^1.1") (d #t) (k 0)) (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.172") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1923srwwc1i60w8cc5aa63l6hg9glxghz6z2zh4i11y3xyc1x5c6")))

(define-public crate-ethers-erc20balances-0.1.3 (c (n "ethers-erc20balances") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "auto_impl") (r "^1.1") (d #t) (k 0)) (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.172") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "141jsc0kw15ysqiyiiv4626az8xgi2q03csjq9nygnl17jwn4ppb")))

