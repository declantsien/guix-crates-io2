(define-module (crates-io et he etherscan) #:use-module (crates-io))

(define-public crate-etherscan-0.1.0 (c (n "etherscan") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.28") (d #t) (k 0)))) (h "1mwfm3jdyxqqh79mihx33abi92829jjvrzk732syjq07qnls5x0p")))

