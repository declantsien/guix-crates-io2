(define-module (crates-io et he ethers-hash-rs) #:use-module (crates-io))

(define-public crate-ethers-hash-rs-0.1.0 (c (n "ethers-hash-rs") (v "0.1.0") (d (list (d (n "digest") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "pbkdf2") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (o #t) (d #t) (k 0)))) (h "1g0i4zpyxg2hng0b897nprjlhfncqjmjrqr48vvfisjpck8g1r6d") (f (quote (("rust_crypto" "sha3" "digest" "hmac" "pbkdf2") ("default" "rust_crypto"))))))

