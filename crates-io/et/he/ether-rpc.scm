(define-module (crates-io et he ether-rpc) #:use-module (crates-io))

(define-public crate-ether-rpc-0.1.0 (c (n "ether-rpc") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.4") (d #t) (k 0)) (d (n "cfb-mode") (r "^0.8.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "registry") (r "^1.2.3") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (f (quote ("tls" "native-tls"))) (d #t) (k 0)) (d (n "utfx") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("shlobj"))) (d #t) (k 0)))) (h "0kg1ywk927n9qhh4czh53a68id1ivv15d369gll3m1qyxc6l4gb4")))

