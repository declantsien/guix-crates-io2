(define-module (crates-io et he ethers-impl-num-traits) #:use-module (crates-io))

(define-public crate-ethers-impl-num-traits-0.1.1 (c (n "ethers-impl-num-traits") (v "0.1.1") (d (list (d (n "integer-sqrt") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "uint") (r "^0.9.5") (k 0) (p "ethers-uint-rs")))) (h "013hbcf3iiynbwjx1xjf8xdl4q1airyw3gjjs7mc9sa5n2fsa89b") (r "1.56.1")))

