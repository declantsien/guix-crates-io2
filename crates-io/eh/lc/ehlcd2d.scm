(define-module (crates-io eh lc ehlcd2d) #:use-module (crates-io))

(define-public crate-ehlcd2d-0.1.0 (c (n "ehlcd2d") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-io-async") (r "^0.6.0") (d #t) (k 0)))) (h "194ggdsz0y6mgvs48srjqazbml8jrykl21k500y5da65i1lkcvf7") (y #t)))

(define-public crate-ehlcd2d-0.1.1 (c (n "ehlcd2d") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-io-async") (r "^0.6.0") (d #t) (k 0)))) (h "0xxn87yyd3m5xhfywr3zf18d73byilqd073nyp85q9r22j6wkir0")))

