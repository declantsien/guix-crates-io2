(define-module (crates-io eh l- ehl-rsa-fdh) #:use-module (crates-io))

(define-public crate-ehl-rsa-fdh-0.5.0 (c (n "ehl-rsa-fdh") (v "0.5.0") (d (list (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "fdh") (r "^0.8") (d #t) (k 0) (p "ehl-fdh")) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.7") (f (quote ("zeroize"))) (d #t) (k 0) (p "num-bigint-dig")) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rsa") (r "^0.5.0") (f (quote ("expose-internals"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.9.2") (d #t) (k 2)) (d (n "sha2") (r "^0.9.2") (d #t) (k 2)) (d (n "subtle") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "13330mx45qhhm5z85q4cgyvgdn976zz0y5a8gjvfgfzkwqr0qn1v") (y #t)))

