(define-module (crates-io eh el ehelply) #:use-module (crates-io))

(define-public crate-ehelply-1.1.108 (c (n "ehelply") (v "1.1.108") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "02jfvc4zpjmdrzqibi6czpaf616b0qcbh30212fv15fa495ccr91")))

(define-public crate-ehelply-1.1.109 (c (n "ehelply") (v "1.1.109") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "07vx75wcbxqlw7z3jvkjy0szc0afs2m3agwz35hxqg5v173miggl")))

(define-public crate-ehelply-1.1.110 (c (n "ehelply") (v "1.1.110") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0ryys9fspkpvp13xx5b7zwn8cjm1y7bgrlfmw4y1jgfx0kl8jacv")))

(define-public crate-ehelply-1.1.111 (c (n "ehelply") (v "1.1.111") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "10bbgcxfys6h9pmzc55h9jm639ri247xzvmb2mjahqsy2nazhkf3")))

(define-public crate-ehelply-1.1.112 (c (n "ehelply") (v "1.1.112") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0qzlzxhmki8gn3h7zwfksni54az6yfhcvlx24dh188xksa0kjpss")))

(define-public crate-ehelply-1.1.113 (c (n "ehelply") (v "1.1.113") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "17f213vsd0k3q3s2zcqnv7y1b8ln5sniq72yrmx2nwjgs51cx87v")))

(define-public crate-ehelply-1.1.114 (c (n "ehelply") (v "1.1.114") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0ck7q3flv7b7fqmw7djkc6lj1k06zgcabb3sn76xda723lip71i8")))

(define-public crate-ehelply-1.1.115 (c (n "ehelply") (v "1.1.115") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1rg4x69ybsax66sp7bpcrzvjkh57s0jl301qf5lmaxqm6y7mg61m")))

(define-public crate-ehelply-1.1.116 (c (n "ehelply") (v "1.1.116") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0g9n8kn686gjfmmdzjhpnnih2x9cbjczpv0jm0g5r1bxr8c0sdip")))

(define-public crate-ehelply-1.1.117 (c (n "ehelply") (v "1.1.117") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1jy5xnnysx31lfwbq17ymrx06qvz1s33hsz396h0gw1mjjd38s55")))

(define-public crate-ehelply-1.1.118 (c (n "ehelply") (v "1.1.118") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "00hli5lam2mqp5sq377fqq2mvvrm8cgrgy6r64ii1sfm5jl3pd5c")))

