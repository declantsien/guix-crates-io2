(define-module (crates-io eh ko ehko) #:use-module (crates-io))

(define-public crate-ehko-0.1.0 (c (n "ehko") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y0l5mi1amhmpwp7ahjfw23xa7q7dwjgczp7lcliymip0azj5z8b")))

(define-public crate-ehko-0.1.1 (c (n "ehko") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "02fn8q93mk8fqb6jy4kwm5j941ajhw726632y5n4mh0m0njxc950")))

(define-public crate-ehko-0.1.2 (c (n "ehko") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "04ka920xh123a6fp151snj9baxi0qkdrhbc2isj0j9hrccpcny42")))

