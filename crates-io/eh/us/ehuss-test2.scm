(define-module (crates-io eh us ehuss-test2) #:use-module (crates-io))

(define-public crate-ehuss-test2-0.1.0 (c (n "ehuss-test2") (v "0.1.0") (d (list (d (n "ehuss-test1") (r "^0.1") (d #t) (k 0)))) (h "1ckh95wadbxlzr67vhqbjsk89z6arxi2n36ykj9i7xhry98glz3l")))

(define-public crate-ehuss-test2-0.1.1 (c (n "ehuss-test2") (v "0.1.1") (d (list (d (n "ehuss-test1") (r "^0.1") (d #t) (k 0)))) (h "0inbif5r4qxspkvkqj316lzdya8zcync1w6c25p0z5k9m6frka9z")))

(define-public crate-ehuss-test2-0.1.2 (c (n "ehuss-test2") (v "0.1.2") (d (list (d (n "ehuss-test1") (r "^0.1.2") (d #t) (k 0)))) (h "02h8pyj9c5f6jkraqpzl7dsr9fr15fymj7hhnjwmxmvcyfza7nsw")))

(define-public crate-ehuss-test2-0.1.4 (c (n "ehuss-test2") (v "0.1.4") (d (list (d (n "ehuss-test1") (r "^0.1.4") (d #t) (k 0)))) (h "0mk3gi9lp2zpbs5r2rznf59b0pxizv6nz3rj432nvimrps2iklka")))

(define-public crate-ehuss-test2-0.1.5 (c (n "ehuss-test2") (v "0.1.5") (d (list (d (n "ehuss-test1") (r "^0.1.11") (d #t) (k 0)))) (h "1a2kgmy04bh1hi77hdr9n38j60p71qsfswnwn4hn9am5scg0b3cz")))

