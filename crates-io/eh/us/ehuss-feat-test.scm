(define-module (crates-io eh us ehuss-feat-test) #:use-module (crates-io))

(define-public crate-ehuss-feat-test-0.1.0 (c (n "ehuss-feat-test") (v "0.1.0") (d (list (d (n "rgb") (r "^0.8.25") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (o #t) (d #t) (k 0)))) (h "01rwqw3cfr7gwran4bj8jk6kamdfkjhmzaym46cp350n4xq3p961") (s 2) (e (quote (("serde" "dep:serde" "rgb?/serde"))))))

(define-public crate-ehuss-feat-test-0.2.0 (c (n "ehuss-feat-test") (v "0.2.0") (d (list (d (n "rgb") (r "^0.8.25") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (o #t) (d #t) (k 0)))) (h "0wgfqal3b7rymy27g88kk61xlyjvdx0s297snqs1zm892ccv5s88") (s 2) (e (quote (("serde2" "dep:serde" "rgb?/serde"))))))

