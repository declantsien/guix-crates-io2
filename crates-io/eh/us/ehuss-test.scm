(define-module (crates-io eh us ehuss-test) #:use-module (crates-io))

(define-public crate-ehuss-test-0.1.0 (c (n "ehuss-test") (v "0.1.0") (h "0hqwf4cbqjhcqf8m3mdf7gm7c5qdr9s9adb08lgzch20yz98dwgk")))

(define-public crate-ehuss-test-0.1.1 (c (n "ehuss-test") (v "0.1.1") (h "1kc5md2v6nfbsdp2zh9885ri20raggygr4v2n65yvpw4hfr6r9dn")))

