(define-module (crates-io eh us ehuss-test1) #:use-module (crates-io))

(define-public crate-ehuss-test1-0.1.0 (c (n "ehuss-test1") (v "0.1.0") (h "0l8mgnrf6nxybx49aiqifkw0f1v6qprvsjhq0ijiin1bc384022s")))

(define-public crate-ehuss-test1-0.1.1 (c (n "ehuss-test1") (v "0.1.1") (h "11kfq6m2x0cvnd4srg2i6x2inwbm36s1s09di2dg3ax7q873qivh")))

(define-public crate-ehuss-test1-0.1.2 (c (n "ehuss-test1") (v "0.1.2") (h "0pr9ls61rdfff370g980dwl35aj45rdjgqgafm92h2yp65cmfy6w")))

(define-public crate-ehuss-test1-0.1.3 (c (n "ehuss-test1") (v "0.1.3") (h "0azdvr2362l0nxf7b789bww5sl3x1f9l1mfy47fhh99kvc9np420")))

(define-public crate-ehuss-test1-0.1.4 (c (n "ehuss-test1") (v "0.1.4") (h "08dk4isdl420qgz3z5xcxi252haavrf5vkd9r80kpymsk3dqim0x")))

(define-public crate-ehuss-test1-0.1.5 (c (n "ehuss-test1") (v "0.1.5") (h "14rhx3pxj0zjfpicmnpk1399yw692bcwl0p18x8314na1abwwyrd")))

(define-public crate-ehuss-test1-0.1.6 (c (n "ehuss-test1") (v "0.1.6") (h "08i4fjibyq1fp50mshczlqk01lvqhq222i0nj6vm738l0ch27jpi")))

(define-public crate-ehuss-test1-0.1.7 (c (n "ehuss-test1") (v "0.1.7") (h "0szr6av6fnklhgw0iqnqkf42wa3hr4lb54czh11mcpql32l76r36")))

(define-public crate-ehuss-test1-0.1.8 (c (n "ehuss-test1") (v "0.1.8") (h "01n91d8kaypsxqfsl9k0yq4w1xiv4m9qyjnw31czvnigqz3jnrdi")))

(define-public crate-ehuss-test1-0.1.9 (c (n "ehuss-test1") (v "0.1.9") (h "12v86a1c459haq2wjswdlzmvqkrfzfnxhpzp8apvlrydid6yla00")))

(define-public crate-ehuss-test1-0.1.10 (c (n "ehuss-test1") (v "0.1.10") (h "06qdfkhkc30xcg50s76hlv5dw32rgblm4lkbzx628nf0xjzphnlb")))

(define-public crate-ehuss-test1-0.1.11 (c (n "ehuss-test1") (v "0.1.11") (h "1p359pxn0dcr69fys6lnhigx0da7xk3z8b3bn7v2dyh5mx3wm0q7")))

