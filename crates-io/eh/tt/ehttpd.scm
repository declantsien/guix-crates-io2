(define-module (crates-io eh tt ehttpd) #:use-module (crates-io))

(define-public crate-ehttpd-0.1.0 (c (n "ehttpd") (v "0.1.0") (h "1c5qrn9q47xgx0ddvf0jddpnzmsglyq0w7ax7k3gmjb98wn8bgyg") (f (quote (("default"))))))

(define-public crate-ehttpd-0.2.0 (c (n "ehttpd") (v "0.2.0") (h "0xmpqrp61i06nr4g3kpyy6mh05wal8hwssih3qdz287cbwy0hw90") (f (quote (("default")))) (y #t)))

(define-public crate-ehttpd-0.2.1 (c (n "ehttpd") (v "0.2.1") (h "0mhz3jdvq0vkw78nn9yq7pg7xk2iggn19ca78ad13wrgrx0xfcx3") (f (quote (("default"))))))

(define-public crate-ehttpd-0.3.0 (c (n "ehttpd") (v "0.3.0") (d (list (d (n "flume") (r "^0.10.14") (k 0)))) (h "1jyh1k9m6zr25bhygb4q735zk7kci64nkixp2glpsihmxwzln4nn") (f (quote (("default"))))))

(define-public crate-ehttpd-0.3.1 (c (n "ehttpd") (v "0.3.1") (d (list (d (n "flume") (r "^0.10.14") (k 0)))) (h "1gasgramqkj87da89r1kf64cvlx5gmdwr3szmdyng67ihb1wv38g") (f (quote (("default"))))))

(define-public crate-ehttpd-0.4.0 (c (n "ehttpd") (v "0.4.0") (d (list (d (n "flume") (r "^0.10.14") (k 0)))) (h "1i9wwc5rzblwk2jf0rlakpk5ma15nc0kszlspsc07jy6pqx57ang") (f (quote (("default"))))))

(define-public crate-ehttpd-0.4.1 (c (n "ehttpd") (v "0.4.1") (d (list (d (n "flume") (r "^0.10.14") (k 0)))) (h "05qyvhca3xlx88vhk46khxfp9qc4pgypvaqx44027g3ry1qq5y7q") (f (quote (("default"))))))

(define-public crate-ehttpd-0.4.2 (c (n "ehttpd") (v "0.4.2") (d (list (d (n "flume") (r "^0.10.14") (k 0)))) (h "07i8n4pq4j2klg19krcf01p7ddzw624cxfmrv1v0zqarn0nw524d") (f (quote (("default"))))))

(define-public crate-ehttpd-0.4.3 (c (n "ehttpd") (v "0.4.3") (d (list (d (n "flume") (r "^0.10.14") (k 0)))) (h "01ayrs7k58ndhx2k78kwh64x2ni0ra42n1pyb4il2nzjnwz8wj41") (f (quote (("default"))))))

(define-public crate-ehttpd-0.5.0 (c (n "ehttpd") (v "0.5.0") (d (list (d (n "flume") (r "^0.10.14") (k 0)))) (h "0d05fg31r44jbh6l7gbr9ky9yizrf9zb7247jvci2245fcps3hxm") (f (quote (("default"))))))

(define-public crate-ehttpd-0.6.0 (c (n "ehttpd") (v "0.6.0") (d (list (d (n "flume") (r "^0.10.14") (k 0)))) (h "0aq4zad9dy22akdakrsgq11bzf1bk62zjzm1aaik4074w50yxpy6") (f (quote (("default"))))))

(define-public crate-ehttpd-0.6.1 (c (n "ehttpd") (v "0.6.1") (d (list (d (n "flume") (r "^0.11.0") (k 0)))) (h "05yd4fvl939bfmxr2knyvdyr326p8lb5fkj38491wk655rq8ngd3") (f (quote (("default"))))))

(define-public crate-ehttpd-0.6.2 (c (n "ehttpd") (v "0.6.2") (d (list (d (n "flume") (r "^0.11.0") (k 0)))) (h "0f9s92h9pnrm3cajfgy1y9cj1q8riyb0p8jl9mly7gkcm2l81mri") (f (quote (("default"))))))

(define-public crate-ehttpd-0.7.0 (c (n "ehttpd") (v "0.7.0") (d (list (d (n "flume") (r "^0.11.0") (k 0)))) (h "00bbcd9ps2457cmidrcz1kim361raqva777bdp97jp0rd2q7x5kw") (f (quote (("default"))))))

(define-public crate-ehttpd-0.7.1 (c (n "ehttpd") (v "0.7.1") (d (list (d (n "flume") (r "^0.11.0") (k 0)))) (h "01n3ysn8s367p79l6hx1bfy64csn6vbkqg0vsgbnsfpxihmqqiy4") (f (quote (("default"))))))

(define-public crate-ehttpd-0.7.2 (c (n "ehttpd") (v "0.7.2") (d (list (d (n "flume") (r "^0.11.0") (k 0)))) (h "0d40rj3ixdv48y049bpzm95p3z6np70q02x0m4pff9p31zfsf3f7") (f (quote (("default"))))))

(define-public crate-ehttpd-0.8.0 (c (n "ehttpd") (v "0.8.0") (d (list (d (n "flume") (r "^0.11.0") (k 0)))) (h "00268fjsvjgvnlb2kiiib7ymh5cwz6s4q912wc65l8a66cmfhsm0") (f (quote (("default"))))))

(define-public crate-ehttpd-0.8.1 (c (n "ehttpd") (v "0.8.1") (d (list (d (n "flume") (r "^0.11.0") (k 0)))) (h "111968d7hq8sk18rpa6zsx9dlh2lwibx2m3sh23wpgnl9qspfn8a") (f (quote (("default"))))))

