(define-module (crates-io eh tt ehttpd-range) #:use-module (crates-io))

(define-public crate-ehttpd-range-0.1.0 (c (n "ehttpd-range") (v "0.1.0") (d (list (d (n "ehttpd") (r "^0.4.1") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 2)))) (h "1yl046g0lclbxdsrb2w1yll7h6mcqgih6l53c770wj4ij2r98hw4") (f (quote (("default"))))))

(define-public crate-ehttpd-range-0.2.0 (c (n "ehttpd-range") (v "0.2.0") (d (list (d (n "ehttpd") (r "^0.4.3") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 2)))) (h "13i04b61qp1x7xdknm7r96v7r824yzfb3xakzblasar4x0czl6jq") (f (quote (("default"))))))

(define-public crate-ehttpd-range-0.2.1 (c (n "ehttpd-range") (v "0.2.1") (d (list (d (n "ehttpd") (r "^0.5.0") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 2)))) (h "1dxspfcfn8v7vji1yh52ayazhd5ymmz4ilf36wvqa0vsyrh9w30j") (f (quote (("default"))))))

(define-public crate-ehttpd-range-0.3.0 (c (n "ehttpd-range") (v "0.3.0") (d (list (d (n "ehttpd") (r "^0.6.0") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 2)))) (h "1ncyi00ji2vc5arkafxrkj25k3y5xf8y9f8sv4iggx464240d3yw") (f (quote (("default"))))))

(define-public crate-ehttpd-range-0.4.0 (c (n "ehttpd-range") (v "0.4.0") (d (list (d (n "ehttpd") (r "^0.7.0") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 2)))) (h "0p3dh6fljdr01xj7lcdsgc1aj2gsy8ypcc35zjra5gv8q7bmjypf") (f (quote (("default"))))))

(define-public crate-ehttpd-range-0.5.0 (c (n "ehttpd-range") (v "0.5.0") (d (list (d (n "ehttpd") (r "^0.8.0") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 2)))) (h "0mxai09c5lid22c3iapqrlh0ph2ziyw3kcn7lqhns61isky69ww8") (f (quote (("default"))))))

(define-public crate-ehttpd-range-0.6.0 (c (n "ehttpd-range") (v "0.6.0") (d (list (d (n "ehttpd") (r "^0.8.1") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 2)))) (h "05lrpx5bhzyi937k6f8h6464k7qag6bg29i49a7ivsi652d142b3") (f (quote (("default"))))))

