(define-module (crates-io eh ru ehrust) #:use-module (crates-io))

(define-public crate-ehrust-0.1.0 (c (n "ehrust") (v "0.1.0") (d (list (d (n "libeh") (r "^0.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v3bd3diz8phfini2zwardslvy7sigc5z917mwvkawlk7hgz86kn")))

(define-public crate-ehrust-0.1.1 (c (n "ehrust") (v "0.1.1") (d (list (d (n "libeh") (r "^0.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h19q3l94jjg3kfp2y64lwfy3cbhi4nmc3w51lrqqnwdiak0lczk")))

