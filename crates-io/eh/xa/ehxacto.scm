(define-module (crates-io eh xa ehxacto) #:use-module (crates-io))

(define-public crate-ehxacto-0.1.0 (c (n "ehxacto") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "bio") (r "^1.6.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "09l7n6q039n8zln9188npzc5b8j0sh81mi22f9ghc2r2x4i9fd6v")))

