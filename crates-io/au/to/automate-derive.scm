(define-module (crates-io au to automate-derive) #:use-module (crates-io))

(define-public crate-automate-derive-0.1.0 (c (n "automate-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0j9fi8sjxxrl4x9az0kk79z7p0pr9jx1lw4hbg6aj2q0xb34zpzb")))

(define-public crate-automate-derive-0.1.1 (c (n "automate-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0l1qakcvxg6hkny2klnw7aziili8gdpc18sa5g7q6qzy1kjqvp31")))

(define-public crate-automate-derive-0.1.2 (c (n "automate-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1llydv719byfs0zm96bbxi11hdc5q09p7pdnk5dczvfwz7br52gd")))

(define-public crate-automate-derive-0.1.3 (c (n "automate-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0d6iwda3jjjij6xf42bz6845gzh3z0hfaq9sk8f5m6l44j3b6z08")))

(define-public crate-automate-derive-0.1.4 (c (n "automate-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0m86xliclld1fzzqv5pixn5x83j0gi8z76n9cjixclgvfbp27901")))

(define-public crate-automate-derive-0.2.0 (c (n "automate-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0f3qny0izk7hc0c1dca1czzsqljz39lq0iv8a2yz629nr892m7fv")))

(define-public crate-automate-derive-0.2.1 (c (n "automate-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05n73yna1qfm5ifqjwwd2hc2mb6416acx0ikxlbh4yszc8l720yk")))

(define-public crate-automate-derive-0.3.0 (c (n "automate-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "07wfsa0ka1qffc6f4v62h0qs3f1fi1mnklppb5blbkiwkj19iz1d")))

(define-public crate-automate-derive-0.3.1 (c (n "automate-derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1w4ifhibcm9zz24r4iwwwg0b8fdc81678x0yll5a7q4ss9nysa9b") (f (quote (("strict-deserializer") ("default"))))))

(define-public crate-automate-derive-0.4.0 (c (n "automate-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04bs5abbsq87ywv576zlvd8bwbgkj0xsv7m4lb9a9hrcnd3dw8mx") (f (quote (("trace-endpoints") ("strict-deserializer") ("storage") ("default"))))))

