(define-module (crates-io au to auto_builder_derive) #:use-module (crates-io))

(define-public crate-auto_builder_derive-0.1.0 (c (n "auto_builder_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0w8vgql27frvng2577c809dap0ldwpmw9f1sf5x5w1kqp0nv3hwq")))

