(define-module (crates-io au to autoimpl-derive) #:use-module (crates-io))

(define-public crate-autoimpl-derive-0.1.0 (c (n "autoimpl-derive") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1ag9060g5c06xynafm3crw8014wgp8z4z9gy84y1w5gbq7k4qbjj")))

