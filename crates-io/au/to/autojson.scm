(define-module (crates-io au to autojson) #:use-module (crates-io))

(define-public crate-autojson-0.1.0 (c (n "autojson") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0s7lh7rs5kgi0b3j7aa9b3m99vli5bf7dc4hyvr99bm13yqmmr97")))

