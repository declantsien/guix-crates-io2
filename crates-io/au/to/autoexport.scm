(define-module (crates-io au to autoexport) #:use-module (crates-io))

(define-public crate-autoexport-0.1.0 (c (n "autoexport") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vc1jqzbn54n8lkd0dx10qrx813fsy8asxk01bxg26hmqj9clm2s")))

