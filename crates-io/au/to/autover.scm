(define-module (crates-io au to autover) #:use-module (crates-io))

(define-public crate-autover-0.1.6 (c (n "autover") (v "0.1.6") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)))) (h "1vwd6swm8zcl8fib8ah862vlngb5ksv11ba3xgixrdwv03kvhcg2")))

(define-public crate-autover-0.2.0 (c (n "autover") (v "0.2.0") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "~3.2.0") (d #t) (k 0)))) (h "13ys0cqqg6l5akpvcf06cmzglwzkh1fiy0lpq0n9wn6f37dzlsya")))

