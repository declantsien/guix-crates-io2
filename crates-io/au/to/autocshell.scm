(define-module (crates-io au to autocshell) #:use-module (crates-io))

(define-public crate-autocshell-0.1.0 (c (n "autocshell") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0316qh8lr3k4pslfbs3wpz3xdwal4yvj656liab6lxgrl4zhnzpq") (y #t)))

(define-public crate-autocshell-0.1.1 (c (n "autocshell") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1wqsiyvdhdzcha895ypg4brgajvaqbibw9r8qizrf00npva3p2xb") (y #t)))

(define-public crate-autocshell-0.1.2 (c (n "autocshell") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "10clhhc9208n5psnm6y9mp2kc716ar3cdqrq67ka332h5kdq93wa") (y #t)))

(define-public crate-autocshell-0.1.3 (c (n "autocshell") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "184v32kch4pc4458kx1lcib7k13pzkhq8jhizjh9i52z5npsrg01") (y #t)))

(define-public crate-autocshell-0.1.4 (c (n "autocshell") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1dlpw81g8bsg0fj77k250w3g15x3dmrx3w1gxrnvcjciglfihrmy") (y #t)))

(define-public crate-autocshell-0.2.0 (c (n "autocshell") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "19csr74lkld0h0alx1bxvrcwh5gqbix5c0cjm3x40yvhi2jn9g01") (y #t)))

(define-public crate-autocshell-0.2.1 (c (n "autocshell") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1v8nyrgnpnznqyv0sfmwkb4jdvkjxasf9xg266drf4qid2cjcxzl") (y #t)))

(define-public crate-autocshell-0.2.2 (c (n "autocshell") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "195dj9j5aq3022vfr263ckndgln4bwy613zpwnxc2x44pwf2d24x") (y #t)))

(define-public crate-autocshell-0.3.0 (c (n "autocshell") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0ngra5ylynqnpga6sbwjbjxypglif5nd82vvjik230fw156jh2zm") (y #t)))

(define-public crate-autocshell-0.4.0 (c (n "autocshell") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "04nhrxpvvzvfb24wh149irwff9i2inn8djkbj52cnjms3kcq4anl") (y #t)))

(define-public crate-autocshell-0.5.0 (c (n "autocshell") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1s0zdf6qsbi63nlfiji2nlasqj2rly98wpg3zqsdk3c348w1jig0") (y #t)))

(define-public crate-autocshell-0.5.1 (c (n "autocshell") (v "0.5.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1z4i1wva2gd4mbx99hrawbqf1pkphlc89xhbnki9qy1wi9bb2g8m") (y #t)))

(define-public crate-autocshell-0.5.2 (c (n "autocshell") (v "0.5.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0frnmvfd56xrcccqnvknx8v5s72xfmk85405w7946fjhbcjis4ml")))

(define-public crate-autocshell-0.5.3 (c (n "autocshell") (v "0.5.3") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0c17by6jphncnq1b8jlanvn3mayrai7qyh3dp2v2fik5mgzq5n1x")))

