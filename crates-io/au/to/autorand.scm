(define-module (crates-io au to autorand) #:use-module (crates-io))

(define-public crate-autorand-0.1.1 (c (n "autorand") (v "0.1.1") (d (list (d (n "autorand-derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "15kz4ksnww3l3cn2q2d39b3z4xz4flfswgqfnd633b6g9ilrmlba")))

(define-public crate-autorand-0.2.0 (c (n "autorand") (v "0.2.0") (d (list (d (n "autorand-derive") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1vhh8icmkqq09x5z9l9b9dw91kzfyg7njvy0lkkvmkrja5pj44m9") (f (quote (("json-value-always-null" "json") ("json" "serde_json") ("default")))) (y #t)))

(define-public crate-autorand-0.2.1 (c (n "autorand") (v "0.2.1") (d (list (d (n "autorand-derive") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0nhhs9n8fn0frdngqr9s4jq4d05h9w0dln9c8jf7gq98idzbp3m8") (f (quote (("json-value-always-null" "json") ("json" "serde_json") ("default")))) (y #t)))

(define-public crate-autorand-0.2.2 (c (n "autorand") (v "0.2.2") (d (list (d (n "autorand-derive") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1a5psj03bxls7f4lrlf259lpikziwmafcprwrdgcnqlmjfgzz7b7") (f (quote (("limited-integers") ("json-value-always-null" "json") ("json" "serde_json") ("default")))) (y #t)))

(define-public crate-autorand-0.2.3 (c (n "autorand") (v "0.2.3") (d (list (d (n "autorand-derive") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0bc58ldics1pjnqm8mnfqq8ywzi53m3px8qsznwambjbk5vxx7xn") (f (quote (("limited-integers") ("json-value-always-null" "json") ("json" "serde_json") ("default"))))))

