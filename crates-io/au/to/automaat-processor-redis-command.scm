(define-module (crates-io au to automaat-processor-redis-command) #:use-module (crates-io))

(define-public crate-automaat-processor-redis-command-0.1.0 (c (n "automaat-processor-redis-command") (v "0.1.0") (d (list (d (n "automaat-core") (r "^0.1") (d #t) (k 0)) (d (n "juniper") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "juniper") (r "^0.12") (d #t) (k 2)) (d (n "redis") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1q2kyj67rrjhh2lgacw9idapkg2zmfnlz2lw405lb22scy6l5ajl")))

