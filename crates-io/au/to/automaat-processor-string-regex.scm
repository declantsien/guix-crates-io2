(define-module (crates-io au to automaat-processor-string-regex) #:use-module (crates-io))

(define-public crate-automaat-processor-string-regex-0.1.0 (c (n "automaat-processor-string-regex") (v "0.1.0") (d (list (d (n "automaat-core") (r "^0.1") (d #t) (k 0)) (d (n "juniper") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "juniper") (r "^0.12") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0n39h6jw5dhg8smb9mrgl8g5y4q79fzzrzzclx5idwxmxkp2h2dp")))

