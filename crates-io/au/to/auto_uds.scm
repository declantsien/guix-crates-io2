(define-module (crates-io au to auto_uds) #:use-module (crates-io))

(define-public crate-auto_uds-0.1.0 (c (n "auto_uds") (v "0.1.0") (h "0sxsdj7zhzac74s1z8vlld9n3r0l4pf4ym83d6ykf84ijyjqqqpc") (f (quote (("std") ("default"))))))

(define-public crate-auto_uds-0.2.0 (c (n "auto_uds") (v "0.2.0") (h "0svi4r47if6krbivdw9xd0hm0lxzsx8flqcfwindvdww3ykrrkyc") (f (quote (("std") ("default"))))))

(define-public crate-auto_uds-0.2.1 (c (n "auto_uds") (v "0.2.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "056r2wq8wz7nc32l87i2q4qcxxagk5ajp6gp475gmrsnv05zb2vr") (f (quote (("std") ("default"))))))

(define-public crate-auto_uds-0.2.2 (c (n "auto_uds") (v "0.2.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "0p12wbbwn0izsdyjy935vxwlnax5i07sknhds0a7bx0dc1mika3d") (f (quote (("std") ("default"))))))

(define-public crate-auto_uds-0.3.0 (c (n "auto_uds") (v "0.3.0") (d (list (d (n "bytenum") (r "^0.1.9") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "1zhmcb5b3dzwb5qxrcbciik7z2487fb0i0ll1sjhrpf51lv8p1a6") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.3.1 (c (n "auto_uds") (v "0.3.1") (d (list (d (n "bytenum") (r "^0.1.9") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "17s2gmjllnv8k5hlmr4jsfcgnxc3sbha27lfzlrjscaw6dz07xab") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.4.0 (c (n "auto_uds") (v "0.4.0") (d (list (d (n "bytenum") (r "^0.1.9") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "05yk3cfnjhv312lqs9p8g0kmdc0ghrpz5nj3qzwiygda4ay98i5q") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.4.1 (c (n "auto_uds") (v "0.4.1") (d (list (d (n "bytenum") (r "^0.1.9") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "0v6q96cw9416r134rvzjkmp1yxqlad0hckhczjpmqy9x9anslj0n") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.4.2 (c (n "auto_uds") (v "0.4.2") (d (list (d (n "bytenum") (r "^0.1.9") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "0860cmilk0p9hmcgm5nw3px0j2d7p5x5pqsrzxy9rxmxfjkd5r2m") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.4.3 (c (n "auto_uds") (v "0.4.3") (d (list (d (n "enum2repr") (r "^0.1") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "05rcwv2d8mfkx3k49x137zqbsvnqlls1f59nq40msgv2j9avhlc8") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.4.4 (c (n "auto_uds") (v "0.4.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "enum2repr") (r "^0.1") (d #t) (k 0)))) (h "1pyhzkjv7zpy9plg97xxgj09ysa2zfhmpaq0bnsv242vj7926qzv") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.5.0 (c (n "auto_uds") (v "0.5.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "enum2repr") (r "^0.1") (d #t) (k 0)))) (h "1smd0lzzhp17qnhzfdbk93n6a1zhsarsm9wm0p6qhq44hx4s8gqx") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.5.1 (c (n "auto_uds") (v "0.5.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "enum2repr") (r "^0.1") (d #t) (k 0)))) (h "14kf9kh703fydxnlzgk5jp5znh1fy1zfz44gn1ql64dk3fw45jia") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.5.2 (c (n "auto_uds") (v "0.5.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "enum2repr") (r "^0.1") (d #t) (k 0)))) (h "17ik8d16gfanpxgp6wqfml2rvk2cbf0n2kmljix26m92lg9y7zv8") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.5.3 (c (n "auto_uds") (v "0.5.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "enum2repr") (r "^0.1") (d #t) (k 0)))) (h "1bsh26ii0nrpc6gq6mcn2rr9dhax45i42axsp85ig4v13558wdrb") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.5.4 (c (n "auto_uds") (v "0.5.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "enum2repr") (r "^0.1") (d #t) (k 0)))) (h "1xggj8z837lkm6xdnwrgxsm5giq56px68xayz9v9darrffjf7dpp") (f (quote (("std") ("default" "std"))))))

(define-public crate-auto_uds-0.5.5 (c (n "auto_uds") (v "0.5.5") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "enum2repr") (r "^0.1") (d #t) (k 0)))) (h "10y5y08wkbkd52lc3mh7q8x43z4mrc8aq70yc2mmcv2caq7jpc0r") (f (quote (("std") ("default" "std"))))))

