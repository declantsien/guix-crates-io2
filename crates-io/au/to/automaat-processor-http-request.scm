(define-module (crates-io au to automaat-processor-http-request) #:use-module (crates-io))

(define-public crate-automaat-processor-http-request-0.1.0 (c (n "automaat-processor-http-request") (v "0.1.0") (d (list (d (n "automaat-core") (r "^0.1") (d #t) (k 0)) (d (n "juniper") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "juniper") (r "^0.12") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1q9mkpwlxmb9ghl9zrcfqc300ia647r1kiqyhdpmk1z4jyplr65s")))

