(define-module (crates-io au to automata_tools) #:use-module (crates-io))

(define-public crate-automata_tools-0.1.0 (c (n "automata_tools") (v "0.1.0") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0n7pp5n3bqknl19pc147rcdagckwrg69xr7s2n1lp3xh0n00jr00")))

(define-public crate-automata_tools-0.1.1 (c (n "automata_tools") (v "0.1.1") (d (list (d (n "graphs_tools") (r "~0.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "wtools") (r "~0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0z665hg0xnrfzx5a65yd1lg05a8asadvqfayiskdzd8khi9kr2kn") (f (quote (("use_std") ("use_alloc") ("full" "use_std") ("default" "use_std"))))))

