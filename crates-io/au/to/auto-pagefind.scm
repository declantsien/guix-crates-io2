(define-module (crates-io au to auto-pagefind) #:use-module (crates-io))

(define-public crate-auto-pagefind-0.1.4 (c (n "auto-pagefind") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lipkdph4rzaq6yc0yf3p3q7ripak75myaazrfi04q4i0b6gr708")))

(define-public crate-auto-pagefind-0.1.5 (c (n "auto-pagefind") (v "0.1.5") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dg4gm7gawmy9bmj2bz7z8sak2ml2z47kvyfnsnjqgc6xqflr3cl")))

