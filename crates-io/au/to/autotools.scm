(define-module (crates-io au to autotools) #:use-module (crates-io))

(define-public crate-autotools-0.1.0 (c (n "autotools") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "0dx1fdc7vq17j2ax3b84sf1b138zp2gd2rs4lvgjhwkbhhdypgak")))

(define-public crate-autotools-0.1.1 (c (n "autotools") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "04y8j4a53z2z8xls0xkzkidicyb718b2pv6sfli2vq7pfrvgpsa1")))

(define-public crate-autotools-0.1.2 (c (n "autotools") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "09xzvvv8k1f08wcn1j2fvg0139ym95dl20s8rc3jdzk93rps2061")))

(define-public crate-autotools-0.1.3 (c (n "autotools") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "1dypmmqq2q1rqalm4h024hf89p3v35wvzrdi597nz00h0kqd8i56")))

(define-public crate-autotools-0.1.4 (c (n "autotools") (v "0.1.4") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "034yx3g5n7gxxd88232p518wxz89c44cg5dvmragvkssdhrsmz96")))

(define-public crate-autotools-0.2.0 (c (n "autotools") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "04728v27smzai7yi6ha2dpqrqzf1lzg5srclrps45b074xklnbs6")))

(define-public crate-autotools-0.2.1 (c (n "autotools") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "150hff6w3xmirqlgrjw7byyvhxp17ld67b8b58q3kaarsjrd2kvp")))

(define-public crate-autotools-0.2.2 (c (n "autotools") (v "0.2.2") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "0hvr0apcjf3zrmwkxrys3r5yv12g3kvjy4nn472k0jibh6a097rv")))

(define-public crate-autotools-0.2.3 (c (n "autotools") (v "0.2.3") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "0hmc7vmlnc2ifn6b5fb3ldmy0aw7507ay0niqi9gpfyxxsn5x1vv")))

(define-public crate-autotools-0.2.4 (c (n "autotools") (v "0.2.4") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "105vpfp5dwjl8bnhrz821c6x44s7wfxj0vg25a4qv9dcbg3f7j2y")))

(define-public crate-autotools-0.2.5 (c (n "autotools") (v "0.2.5") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 0)))) (h "008pgv61dzici4cll6xclwk4p45gxvb3pa7v7gkx5rd3zkg8l4y8")))

(define-public crate-autotools-0.2.6 (c (n "autotools") (v "0.2.6") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 0)))) (h "0xxdlk097n635vpzl8kn6br2c4g7jc7nhfsc6dra2a700lcdmy5f")))

(define-public crate-autotools-0.2.7 (c (n "autotools") (v "0.2.7") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 0)))) (h "1kwmqzdpgmy50dr8pzx0029f5iszrma826ji93fw03qvqhkib57g")))

