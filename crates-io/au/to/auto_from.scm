(define-module (crates-io au to auto_from) #:use-module (crates-io))

(define-public crate-auto_from-0.1.0 (c (n "auto_from") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11wm5b6fxvvg4anzbn9xfn8v4xci6p28dxrms4vdy5cmj95sk0bw")))

(define-public crate-auto_from-0.2.0 (c (n "auto_from") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07735c9g0qcwzd819p4n72m5qm4gfwwqmwb6nib3d8gll7bfi4gq")))

(define-public crate-auto_from-0.3.0 (c (n "auto_from") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0427w6g960q0cs31lv1jgim4mlcy5nvp2kinsc4byzra8h6cqv93")))

