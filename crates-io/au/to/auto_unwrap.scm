(define-module (crates-io au to auto_unwrap) #:use-module (crates-io))

(define-public crate-auto_unwrap-1.0.0 (c (n "auto_unwrap") (v "1.0.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f64sf96fb46vm0b6dj17wfski64wmvqk7s2wfx79y929hn1if9s")))

(define-public crate-auto_unwrap-1.0.1 (c (n "auto_unwrap") (v "1.0.1") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v67sjxw57ws6av7c6cc2h5ax3pskmbwb6q55zwqnxq57mr9kpmz") (y #t)))

(define-public crate-auto_unwrap-1.0.2 (c (n "auto_unwrap") (v "1.0.2") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0glymzkdmb24kz7i31zvn2kavm37fjvayf77zhy1qk4y9q6kj2zb") (y #t)))

(define-public crate-auto_unwrap-1.1.0 (c (n "auto_unwrap") (v "1.1.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w2dkqrj5viywa9qyjmy53h60l95vfpgbpkhblxav6zsxgdvj9lj")))

