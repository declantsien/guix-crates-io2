(define-module (crates-io au to auto_downloader) #:use-module (crates-io))

(define-public crate-auto_downloader-0.1.0 (c (n "auto_downloader") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1543xa0lzdfxbm8356m8p1r1n9vr2y9wsshlsbgzqzi87ikbx28b")))

