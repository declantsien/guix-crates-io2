(define-module (crates-io au to automerge-persistent) #:use-module (crates-io))

(define-public crate-automerge-persistent-0.1.0 (c (n "automerge-persistent") (v "0.1.0") (d (list (d (n "automerge") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "17bidiqj2jafbh8yzfs58ji16xiibqwj6h9jg3bg3dnsa1nqaqzj")))

(define-public crate-automerge-persistent-0.2.0 (c (n "automerge-persistent") (v "0.2.0") (d (list (d (n "automerge") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0g2is3ihfn8ag9arpc8ixfr39dk3l1zfk7pzfi9gmczjryxwbp0z")))

(define-public crate-automerge-persistent-0.3.0 (c (n "automerge-persistent") (v "0.3.0") (d (list (d (n "automerge") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "12vx8glfhrg08ys9lv4adirswjlnv6w0dr2nqvk3biafaj7viydc")))

(define-public crate-automerge-persistent-0.3.1 (c (n "automerge-persistent") (v "0.3.1") (d (list (d (n "automerge") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "07919m7apb3r6xvq13xdgb7d7x1zvzi2ns2w3nnwr9d7xnrmnj94")))

(define-public crate-automerge-persistent-0.4.0 (c (n "automerge-persistent") (v "0.4.0") (d (list (d (n "automerge") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "13mvbcgkz1crwrxmdgj9nq1a9m51awkadjbdsw41lxcvdca91i70")))

