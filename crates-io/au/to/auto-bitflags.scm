(define-module (crates-io au to auto-bitflags) #:use-module (crates-io))

(define-public crate-auto-bitflags-1.0.0 (c (n "auto-bitflags") (v "1.0.0") (h "1fr6zivkxdg526xw1hs9zcvn2cfzdx5pdl3bn4wzacy52d1lm86v") (y #t)))

(define-public crate-auto-bitflags-1.0.1 (c (n "auto-bitflags") (v "1.0.1") (h "025gj78w0mm4fszh7f2fl4ignagcyzqk7v9gf061s7z090875rw6") (y #t)))

(define-public crate-auto-bitflags-1.0.2 (c (n "auto-bitflags") (v "1.0.2") (h "006sn8ybnhzs2dcp6lm8vc9fj32dxl5nrhjzpsibq61yh4hvggvm") (y #t)))

(define-public crate-auto-bitflags-1.0.3 (c (n "auto-bitflags") (v "1.0.3") (h "1j0lji6g8r6k4bhhvd940cmq8mn8px5hdvr08l6dm4p93688153h")))

