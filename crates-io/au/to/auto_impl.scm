(define-module (crates-io au to auto_impl) #:use-module (crates-io))

(define-public crate-auto_impl-0.1.0 (c (n "auto_impl") (v "0.1.0") (d (list (d (n "quote") (r "~0.3") (d #t) (k 0)) (d (n "syn") (r "~0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0916vqm95gwvw2j6k7paik57h7blbbri9crfzgi7ka3gb951rgdv")))

(define-public crate-auto_impl-0.1.1 (c (n "auto_impl") (v "0.1.1") (d (list (d (n "quote") (r "~0.3") (d #t) (k 0)) (d (n "syn") (r "~0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "13zydnn7x2ijmfvzlrqhk3wgn3z823q7spf5lvaxx1gcxrs45wv2")))

(define-public crate-auto_impl-0.1.2 (c (n "auto_impl") (v "0.1.2") (d (list (d (n "quote") (r "~0.3") (d #t) (k 0)) (d (n "syn") (r "~0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "03y1n8sgmialwmcjd3djd2vnkw7mg2d5nqi685gf64gvbphzl8nj")))

(define-public crate-auto_impl-0.2.0 (c (n "auto_impl") (v "0.2.0") (d (list (d (n "quote") (r "~0.3") (d #t) (k 0)) (d (n "syn") (r "~0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "08b52a8xvmzzk57lqns04xicdbb1mjx6gbgd4ni6n2d2p3kbfmwm")))

(define-public crate-auto_impl-0.3.0 (c (n "auto_impl") (v "0.3.0") (d (list (d (n "build-plan") (r "^0.1.1") (d #t) (k 2)) (d (n "libtest-mimic") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.6") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "037568qigfhqkg1nqc1jy4lnansrj2ln6iris7rjbm7fyis51y37") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-auto_impl-0.4.0 (c (n "auto_impl") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^0.4.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "08dsz31i4lbf7nw2k3vnicb72xd99ciiqy15nrdg2qsgfjm3d0kg") (f (quote (("nightly"))))))

(define-public crate-auto_impl-0.4.1 (c (n "auto_impl") (v "0.4.1") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0f0aildq7rl7imgl6x6xw8jg4m08xz9q1bpcrmf5xnhar23gbjs2") (f (quote (("nightly"))))))

(define-public crate-auto_impl-0.5.0 (c (n "auto_impl") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "195d6s9bfcchwizf3km1g69l74f6xvm5gl9505js2r9xi4ff4qkq") (f (quote (("nightly")))) (r "1.37")))

(define-public crate-auto_impl-1.0.0 (c (n "auto_impl") (v "1.0.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1j5rgvcw52m3kapic3z7sqby1i4qhbcc5vbdzfsblcj484hk8fkb") (r "1.37")))

(define-public crate-auto_impl-1.0.1 (c (n "auto_impl") (v "1.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0cvff3ib008qzc27mm5w5k6ycfyiqy683rvxb36vlpr897w1v34a") (r "1.37")))

(define-public crate-auto_impl-1.1.0 (c (n "auto_impl") (v "1.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "12bv6bhn0md22lqk4iqlh0qzvzyq2202biyibpp0nsr7y67dmqzy") (r "1.56")))

(define-public crate-auto_impl-1.1.1 (c (n "auto_impl") (v "1.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1qg1q7g4x887krbcpkzybf8jz3cb9ghbw4w7z4429axmw8ak4bcp") (r "1.56")))

(define-public crate-auto_impl-1.1.2 (c (n "auto_impl") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1ajnnycwasf7qwi315730fs0qj72nff8f9bsmki4840nfnr8nfw2") (r "1.56")))

(define-public crate-auto_impl-1.2.0 (c (n "auto_impl") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0hmfcahj0vrnzq7rayk7r428zp54x9a8awgw6wil753pbvqz71rw") (r "1.56")))

