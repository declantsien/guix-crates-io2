(define-module (crates-io au to automated-rust-releases) #:use-module (crates-io))

(define-public crate-automated-rust-releases-0.1.0 (c (n "automated-rust-releases") (v "0.1.0") (h "07ihkk0kbv3jrai00c82ljn9iia0bicq4519cx20j4680g98yljm")))

(define-public crate-automated-rust-releases-0.1.1 (c (n "automated-rust-releases") (v "0.1.1") (h "1wrms96vd6fz0ij5dpnzjvhq1q7iifspwz507npqywj7hy55i888")))

(define-public crate-automated-rust-releases-0.1.2 (c (n "automated-rust-releases") (v "0.1.2") (h "0cvx5pcfhpbwig6r61imrqcqm3xwfhjyg2my8ccjg4309wg1sw0w")))

