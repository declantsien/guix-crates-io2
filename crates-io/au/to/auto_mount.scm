(define-module (crates-io au to auto_mount) #:use-module (crates-io))

(define-public crate-auto_mount-0.1.0 (c (n "auto_mount") (v "0.1.0") (d (list (d (n "sysinfo") (r "^0.25") (d #t) (k 0)))) (h "0xz18k6qyhkn70ngd9p78b60cgqzdcbsj2gcfp4fxcgv4n6mz4fl") (y #t) (r "1.63")))

(define-public crate-auto_mount-0.1.1 (c (n "auto_mount") (v "0.1.1") (d (list (d (n "sysinfo") (r "^0.25") (d #t) (k 0)))) (h "1737n511q44lipgrb50z2w46ali35n64vax4j5cn0bg698yzfhdh") (y #t) (r "1.63")))

(define-public crate-auto_mount-0.1.2 (c (n "auto_mount") (v "0.1.2") (d (list (d (n "sysinfo") (r "^0.25") (d #t) (k 0)))) (h "11irdhmc5pyfpxvhbm3lrfw98940fjpx9m3y7xzxk70jqkavlzn2") (y #t) (r "1.63")))

(define-public crate-auto_mount-0.1.3 (c (n "auto_mount") (v "0.1.3") (d (list (d (n "sysinfo") (r "^0.25") (d #t) (k 0)))) (h "0miii2iyfgz2mfmn7miwvc49kbqa3h563b1wmnm7snl6q7wz8fsj") (y #t) (r "1.63")))

(define-public crate-auto_mount-0.1.5 (c (n "auto_mount") (v "0.1.5") (d (list (d (n "sysinfo") (r "^0.25") (d #t) (k 0)))) (h "03sdnl89p9lscnr6dnpcsk72x9f930yfim1ihs9yvqjd1b06w7k7") (r "1.63")))

