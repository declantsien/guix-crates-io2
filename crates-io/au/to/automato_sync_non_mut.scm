(define-module (crates-io au to automato_sync_non_mut) #:use-module (crates-io))

(define-public crate-automato_sync_non_mut-0.0.1 (c (n "automato_sync_non_mut") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ak559l7d09vfl90xim55y61by9v3z1jsihxp4hgzi7lmklp75z6")))

