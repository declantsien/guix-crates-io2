(define-module (crates-io au to auto-delegate-macros) #:use-module (crates-io))

(define-public crate-auto-delegate-macros-0.0.1 (c (n "auto-delegate-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "02759jkjkq2kdm00fd0la2hwkl9n2v9lvafpa1xq0xyby8aikdcv")))

(define-public crate-auto-delegate-macros-0.0.2 (c (n "auto-delegate-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "197cffqkhqa3gjsphbqygaw8wqyxhrcilxn1wcwx3cwb1c9l5bkl")))

(define-public crate-auto-delegate-macros-0.0.3 (c (n "auto-delegate-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "10gdm8ja766j5f5haj5fx8y1rscj1flwy880a716j05hlvz638ng")))

(define-public crate-auto-delegate-macros-0.0.4 (c (n "auto-delegate-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "1azc2dhf082fy1v074kgqwv1rj9bz2xqsrv6yvbw1ggfx4rc24dc")))

(define-public crate-auto-delegate-macros-0.0.5 (c (n "auto-delegate-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "1pq0c6gpdjcjrk7mg78qfmvxg45s8sq6mxq6xhq5rflqldl6in2y")))

(define-public crate-auto-delegate-macros-0.0.6 (c (n "auto-delegate-macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "0n8yrbwxnsqqrv981mlk1rzyzsnhcmlbv5lz4y1hp52qyg6yf629")))

