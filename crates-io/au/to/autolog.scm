(define-module (crates-io au to autolog) #:use-module (crates-io))

(define-public crate-autolog-0.1.0 (c (n "autolog") (v "0.1.0") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1nycfh9hk5fxmn8dlwx9csnn4ggh5z70d141d5hpi6g0lav1qa4b") (f (quote (("default" "tracing"))))))

