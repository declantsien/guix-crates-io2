(define-module (crates-io au to auto-palette-wasm) #:use-module (crates-io))

(define-public crate-auto-palette-wasm-0.3.0 (c (n "auto-palette-wasm") (v "0.3.0") (d (list (d (n "auto-palette") (r "^0.3.0") (f (quote ("wasm"))) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 2)) (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "1dbljdcpgz6mn71v3z5n4i8jcykjp1j4plwv9i16nbk5gf6krh1f") (r "1.75.0")))

