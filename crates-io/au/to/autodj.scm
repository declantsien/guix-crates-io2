(define-module (crates-io au to autodj) #:use-module (crates-io))

(define-public crate-autodj-0.0.1 (c (n "autodj") (v "0.0.1") (h "1j14dwxby6rmymwlw30a453vb7wzzzc9mk8yg736sxwvq5b4aa0x") (y #t)))

(define-public crate-autodj-0.0.2 (c (n "autodj") (v "0.0.2") (h "1ama0kjkmkhh14ib673swyk9sxvhla3inrk749sxr04gwwlbygl0") (y #t)))

(define-public crate-autodj-0.0.3 (c (n "autodj") (v "0.0.3") (h "0rcn8qgy0gwx8mfzlymxqvz65q02xjmgv8f9a7hfk8dg7jhndplg") (y #t)))

(define-public crate-autodj-0.1.0 (c (n "autodj") (v "0.1.0") (h "1kq2v93d0iz2iyrp00m8aldgglfqh5zg6xbjaz53wkm7b1x6q3x8") (y #t)))

(define-public crate-autodj-0.2.0 (c (n "autodj") (v "0.2.0") (h "1agf558hwlndjbc4w5hy7zkkkq4rhslh75qlhyv6l09y82fkxxs5") (y #t)))

(define-public crate-autodj-0.2.1 (c (n "autodj") (v "0.2.1") (h "1i2lflmki0aigjjgczixlyqf7pc3ai8h8vhl4yfwy9r8nkjx6hdg") (y #t)))

(define-public crate-autodj-0.2.2 (c (n "autodj") (v "0.2.2") (h "1mr9l1yqmbpdl5179khr9iz65y1bdhh4cp6v34jg78km5fgb9am3") (y #t)))

(define-public crate-autodj-0.2.3 (c (n "autodj") (v "0.2.3") (h "16dly223gzxkxnv13y6fgsqmsglxmxiw7msl14scng48pmmxyakp") (y #t)))

(define-public crate-autodj-0.2.4 (c (n "autodj") (v "0.2.4") (h "0zch7ipv8sl864r9ninj3wnirmh9rcndb74a91a5gragjhl827xq")))

(define-public crate-autodj-0.2.5 (c (n "autodj") (v "0.2.5") (h "1z73z0x1ipkcszvh4wnvw2y6lq699mbg7kasidkqg1404cwn03h7") (y #t)))

(define-public crate-autodj-0.2.6 (c (n "autodj") (v "0.2.6") (h "0bz10mpdrs2kgciz06kl0b07r44gk2b9k8y2fg8vax2qq76r8ig2") (y #t)))

(define-public crate-autodj-0.2.7 (c (n "autodj") (v "0.2.7") (h "1dwi6ncpk9l6z79k2w95f6y7fl5dn0l77x7451q8ailg1brzvg4q") (y #t)))

(define-public crate-autodj-0.2.8 (c (n "autodj") (v "0.2.8") (h "0rg4kbjfrdm0yzrf2wfhp7xcn3c2w5rhk0b4ipw9p5xcz6wrnxxj") (y #t)))

(define-public crate-autodj-0.2.9 (c (n "autodj") (v "0.2.9") (h "1nz2275836nxqfb4ar2bp2l5fa97yqmvqxr1cn21j114pz8mdgqy") (y #t)))

(define-public crate-autodj-0.2.10 (c (n "autodj") (v "0.2.10") (h "12nrhh0m0v8fhkp10drbr2hhdx42dh0d6zq2zbfhsmd8x2fgyj37")))

(define-public crate-autodj-0.3.0 (c (n "autodj") (v "0.3.0") (h "02yz0kraqv3f103y3mrd70bh7wjm97dnjq1l2xdlnij7lshrv3xq")))

(define-public crate-autodj-0.3.1-alpha.1 (c (n "autodj") (v "0.3.1-alpha.1") (h "1d3jhkpash3zm2zmssp98r4apbcwks9p21a4kqkjfgyfgpdjn0k2")))

(define-public crate-autodj-0.4.1 (c (n "autodj") (v "0.4.1") (d (list (d (n "autodiff") (r ">=0") (d #t) (k 2)))) (h "1mx2i0z5maz76cld5xyhlqb0fbjqr6b1988vjb90i3m0144xh5iz") (f (quote (("default"))))))

(define-public crate-autodj-0.4.2 (c (n "autodj") (v "0.4.2") (d (list (d (n "autodiff") (r ">=0") (d #t) (k 2)))) (h "15kgrj41z6b4crcj4jy6x8zs31wmzprsyiz0ppm3k7kv0gcr85a1") (f (quote (("default"))))))

(define-public crate-autodj-0.4.3 (c (n "autodj") (v "0.4.3") (d (list (d (n "autodiff") (r ">=0") (d #t) (k 2)))) (h "0s21i7j9msblbdr47ppncjzkg2fmg3zvx7kkv0za61r8bw4gj22a") (f (quote (("default"))))))

(define-public crate-autodj-0.4.4 (c (n "autodj") (v "0.4.4") (d (list (d (n "autodiff") (r ">=0") (d #t) (k 2)))) (h "03a0bf1cmasmv7nvy6i74cjqb9lv9n39dipg5hjpgp440dj42j3r") (f (quote (("default"))))))

(define-public crate-autodj-0.4.5-rc.1 (c (n "autodj") (v "0.4.5-rc.1") (d (list (d (n "autodiff") (r ">=0") (d #t) (k 2)) (d (n "nalgebra") (r ">=0") (d #t) (k 2)) (d (n "num-traits") (r ">=0") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "06jvniyn8vpkdqjzsfzkca5g5p009r47cgl65cvw7m95dj17wr5j") (f (quote (("default" "uuid")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-autodj-0.5.0 (c (n "autodj") (v "0.5.0") (d (list (d (n "autodiff") (r "^0.7.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "1aa224z7barwsz4s8728l05h2plw7l4r60dv5nhm728qrhj513c0") (f (quote (("default" "uuid")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-autodj-0.5.1 (c (n "autodj") (v "0.5.1") (d (list (d (n "autodiff") (r "^0.7.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "1g2z18q3wwl7jc4rpcrx60q1jmd8kidb2pri32kix5vzrb2x3sc4") (f (quote (("default" "uuid")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-autodj-0.5.2 (c (n "autodj") (v "0.5.2") (d (list (d (n "autodiff") (r "^0.7.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "01fw0dwrm838xnin2m2v5pkzwf5fix0h1gddipc5k86kjlzgh3il") (f (quote (("default" "uuid")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-autodj-0.5.3 (c (n "autodj") (v "0.5.3") (d (list (d (n "autodiff") (r "^0.7.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (k 0)) (d (n "num-traits") (r "^0.2.18") (f (quote ("libm"))) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (o #t) (k 0)))) (h "1d12a7rl37a3npg7brrbmdl9dazm371npipxvpxcr895196g4rkl") (f (quote (("sparse" "no-std-compat/compat_hash") ("default" "std" "uuid")))) (s 2) (e (quote (("uuid" "sparse" "dep:uuid") ("std" "num-traits/std" "uuid?/std" "no-std-compat/std"))))))

