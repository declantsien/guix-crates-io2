(define-module (crates-io au to autoclap) #:use-module (crates-io))

(define-public crate-autoclap-0.0.1 (c (n "autoclap") (v "0.0.1") (h "1myxqd21lhg19p685kckd6y04jjk4pmrjj8ks8qq6h3g89agin77") (y #t)))

(define-public crate-autoclap-0.0.2 (c (n "autoclap") (v "0.0.2") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)))) (h "03g2jmv2zga6ppzkc9mynvpwa4p4sfs3f89z9dr97j4q5i2pkcrf") (y #t)))

(define-public crate-autoclap-0.0.3 (c (n "autoclap") (v "0.0.3") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)))) (h "15m9diqb2mf6k6j5cgpsqn7hmwkp1vp7w8hsi25vvaza6zy923qb") (y #t)))

(define-public crate-autoclap-0.1.0 (c (n "autoclap") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)))) (h "0rj4k3y490wkjq53ysb0ssfqilda4lkzb2mgsr1zax3svr7n7w4x") (y #t)))

(define-public crate-autoclap-0.1.1 (c (n "autoclap") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)))) (h "1s1zpsymwpyjjlmgr82xnxriqyjhry6kvhp8jbl81is46gzcqjzk") (y #t)))

(define-public crate-autoclap-0.1.2 (c (n "autoclap") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)))) (h "0cvkrf5s3ajkpbk5kdksl2mh2jb04id2navs92zs742nsrnm32ym") (y #t)))

(define-public crate-autoclap-0.2.0 (c (n "autoclap") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)))) (h "05d8cfiwrbk9qqc66wb28lhdavi59fgng0l4cwazmnd6qzaxni59") (y #t)))

(define-public crate-autoclap-0.2.1 (c (n "autoclap") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)))) (h "1lrjs6zd4bibwxy5qbmf0s14xnjq7ak88qpqj6zmky7g2zcw0jqh")))

(define-public crate-autoclap-0.2.2 (c (n "autoclap") (v "0.2.2") (d (list (d (n "clap") (r "^3.1.8") (d #t) (k 0)))) (h "1irn1r4m2d1js449r81wf88b08nivgks1ypxrz6n6g7hhxqqnbsw")))

(define-public crate-autoclap-0.2.3 (c (n "autoclap") (v "0.2.3") (d (list (d (n "clap") (r "^4.0.2") (d #t) (k 0)))) (h "1w19kpsp76miyv47naf67bwrsvi5x1scvjm2khq3k33wq40a3y9p")))

(define-public crate-autoclap-0.2.5 (c (n "autoclap") (v "0.2.5") (d (list (d (n "clap") (r "^4.0.4") (f (quote ("string"))) (d #t) (k 0)))) (h "1gspdyl3is141gz9agi14wr1cdhadi8qg3bjvxzw0xhfg9d04ph1")))

(define-public crate-autoclap-0.2.7 (c (n "autoclap") (v "0.2.7") (d (list (d (n "clap") (r "^4.0.7") (f (quote ("string"))) (d #t) (k 0)))) (h "1vvn22znacd184kbq6l51i09m69k0kl68lhqn6zg8b5cx6xi9yqs")))

(define-public crate-autoclap-0.2.8 (c (n "autoclap") (v "0.2.8") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("string"))) (d #t) (k 0)))) (h "0ww19m2nkbfbwcys5g54qbv75kl14x9zldgx6mvddc663y5sn3di")))

(define-public crate-autoclap-0.2.10 (c (n "autoclap") (v "0.2.10") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("string"))) (d #t) (k 0)))) (h "18dmxx76l4ksnbid0sys8jq6pxkj13a46v9rvzjdryr6mqdn9vja")))

(define-public crate-autoclap-0.3.0 (c (n "autoclap") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("string"))) (d #t) (k 0)))) (h "19h01j1khk6nqhblirdj7vv4kqmrw15i1vw3jswjqx98gnaib80x")))

(define-public crate-autoclap-0.3.1 (c (n "autoclap") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("string"))) (d #t) (k 0)))) (h "04nscx5n09chnss9mxmlzn3749vw9gp2ql4539z67r5rapgz7axk")))

(define-public crate-autoclap-0.3.2 (c (n "autoclap") (v "0.3.2") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("string"))) (d #t) (k 0)))) (h "0sfnn7bp8i08y1llxydcwkn4lm7kpf49g5h1c2ix6c85s35jd2ff")))

(define-public crate-autoclap-0.3.3 (c (n "autoclap") (v "0.3.3") (d (list (d (n "clap") (r "^4.0.11") (f (quote ("string"))) (d #t) (k 0)))) (h "13vhrhjxkflpcgzqp55zsmk9b0h724rczkvz07disj6gi1g8q0sb")))

(define-public crate-autoclap-0.3.4 (c (n "autoclap") (v "0.3.4") (d (list (d (n "clap") (r "^4.0.12") (f (quote ("string"))) (d #t) (k 0)))) (h "10fj22j9rfks8fqcc1yhxqppnysal33kadbh4g0bhp7my4xs91jp")))

(define-public crate-autoclap-0.3.5 (c (n "autoclap") (v "0.3.5") (d (list (d (n "clap") (r "^4.0.13") (f (quote ("string"))) (d #t) (k 0)))) (h "1hn7y4628lx2w5pbnyil87v70w9fg6p9ss8a28ql1qw3gnjshrs2")))

(define-public crate-autoclap-0.3.6 (c (n "autoclap") (v "0.3.6") (d (list (d (n "clap") (r "^4.0.14") (f (quote ("string"))) (d #t) (k 0)))) (h "14rjwgymp1icfwwq3y7jdg9blf2ih2njxm7dmml9qghjb93cgsdi")))

(define-public crate-autoclap-0.3.7 (c (n "autoclap") (v "0.3.7") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("string"))) (d #t) (k 0)))) (h "17gksvq7cd3hf2myigpdrqdpqmkxmjyq7w48a1nhmrcdl46w1a7q")))

(define-public crate-autoclap-0.3.8 (c (n "autoclap") (v "0.3.8") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("string"))) (d #t) (k 0)))) (h "1bf8afwkf4dbc8fn37mn7ypsg1886lc56jdyq0kb556ah1hmzsqr")))

(define-public crate-autoclap-0.3.9 (c (n "autoclap") (v "0.3.9") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("string"))) (d #t) (k 0)))) (h "06lqrbvrkmsqbdcs1li2pylh1p5l5fcd1a1y7kvywnmi5shf8qzg")))

(define-public crate-autoclap-0.3.10 (c (n "autoclap") (v "0.3.10") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("string"))) (d #t) (k 0)))) (h "0fdl84scm7n37fk81dxy61x7rmq6y1ph7fmpjbmnd4wa4fd9vq2k")))

(define-public crate-autoclap-0.3.11 (c (n "autoclap") (v "0.3.11") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("string"))) (d #t) (k 0)))) (h "03m408aqis09n1w1dlz26s47bnq3ff2q4dwhn38m3i425v8kw7wg")))

(define-public crate-autoclap-0.3.12 (c (n "autoclap") (v "0.3.12") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("string"))) (d #t) (k 0)))) (h "19wgz9s4mk2xxfzgmppv5yzibpgfv3in9bdrkl3i0cqh115ibmmv")))

(define-public crate-autoclap-0.3.13 (c (n "autoclap") (v "0.3.13") (d (list (d (n "clap") (r "^4.1.3") (f (quote ("string"))) (d #t) (k 0)))) (h "1ii90m5rq2rq3r8svhi5x8n2b3cfihxi1v5aqvfsqkwxng4x74w9")))

(define-public crate-autoclap-0.3.14 (c (n "autoclap") (v "0.3.14") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("string"))) (d #t) (k 0)))) (h "0y2khq2m0719966rmdm5bslfmr25g6skyvgd77zh595qflyjvdr4")))

(define-public crate-autoclap-0.3.15 (c (n "autoclap") (v "0.3.15") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("string"))) (d #t) (k 0)))) (h "038kyvis8qya72hln1k3b2hv2m6m2hnpvns96zc92mlhnik67vy5")))

