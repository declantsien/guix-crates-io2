(define-module (crates-io au to auto_activity) #:use-module (crates-io))

(define-public crate-auto_activity-0.2.0 (c (n "auto_activity") (v "0.2.0") (d (list (d (n "winput") (r "^0.2.3") (d #t) (k 0)))) (h "1n52klaxmdrw5nbrmhvvlv5w9w9x9lkpw6ynkd0kh4gxmdqvfbka") (y #t)))

(define-public crate-auto_activity-0.2.1 (c (n "auto_activity") (v "0.2.1") (d (list (d (n "winput") (r "^0.2.3") (d #t) (k 0)))) (h "0cjx1xvbd1ghr6azic1z5a42gm1gfm597rinihwdi9sf9fbjcwfp") (y #t)))

