(define-module (crates-io au to auto-delegate-impl) #:use-module (crates-io))

(define-public crate-auto-delegate-impl-0.0.7 (c (n "auto-delegate-impl") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "1a332bhpxilk8p32p9r5mf7r3cgb0s9q5190p6y80i4lf7alxg7s")))

(define-public crate-auto-delegate-impl-0.0.8 (c (n "auto-delegate-impl") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "1yd9p6798wpzdd1skxic2bknz0f28mikz7y3wb6yv98jmvvwkkcv")))

(define-public crate-auto-delegate-impl-0.0.9 (c (n "auto-delegate-impl") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "0z8k4515nb7i6c0jga8dan07b2ba3jp4mivp99ld7gazqlcjis4z")))

(define-public crate-auto-delegate-impl-0.1.0 (c (n "auto-delegate-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive" "parsing"))) (d #t) (k 0)))) (h "0yqjqhvwwfxwaj9l1liavb3py714vzcr6acv2fh38hwva2r4p24i")))

(define-public crate-auto-delegate-impl-0.1.1 (c (n "auto-delegate-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive" "parsing"))) (d #t) (k 0)))) (h "0ybglqn18nl6ccv0jvaha7nsdhgwrz86c14f89jn53ajma4215jr") (y #t)))

(define-public crate-auto-delegate-impl-0.1.2 (c (n "auto-delegate-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "derive" "parsing"))) (d #t) (k 0)))) (h "0y6lxpgmjfpc9kqyxysiljz41q1s3frfd39r07j9bphq5c0dqx98")))

