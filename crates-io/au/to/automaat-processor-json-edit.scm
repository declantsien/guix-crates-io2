(define-module (crates-io au to automaat-processor-json-edit) #:use-module (crates-io))

(define-public crate-automaat-processor-json-edit-0.1.0 (c (n "automaat-processor-json-edit") (v "0.1.0") (d (list (d (n "automaat-core") (r "^0.1") (d #t) (k 0)) (d (n "json-query") (r "^0.3") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "juniper") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "juniper") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0yqsjkh9chw46z3pj3nd4j5cdswaf6ckx1907zcc5zc2bq3hgxrq")))

