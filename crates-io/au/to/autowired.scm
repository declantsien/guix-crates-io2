(define-module (crates-io au to autowired) #:use-module (crates-io))

(define-public crate-autowired-0.0.0 (c (n "autowired") (v "0.0.0") (h "0zklk9kg4yn1c7q99pn3ad0yngysj7n7d18pba2vkbd86drfvp7l")))

(define-public crate-autowired-0.1.0 (c (n "autowired") (v "0.1.0") (d (list (d (n "autowired-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0glk50ly4l7g6ifjxdawrvnd3gmfwz46107l1pkmyhmnc72xhg4b")))

(define-public crate-autowired-0.1.1 (c (n "autowired") (v "0.1.1") (d (list (d (n "autowired-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "09g05hjc6sil4wiqjc4fbmz961pwjf617dqjh9g4nhs6w8z17hk5")))

(define-public crate-autowired-0.1.2 (c (n "autowired") (v "0.1.2") (d (list (d (n "autowired-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0phc3miw0r4f7yg8wr8cjkdp54fl200hvgg37qnwhil01dzmz329")))

(define-public crate-autowired-0.1.3 (c (n "autowired") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 2)) (d (n "autowired-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "140zzrp0mgdh7rk0kmanp87cc37147ilcfv53r9jwrqwcr58dxz8")))

(define-public crate-autowired-0.1.4 (c (n "autowired") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 2)) (d (n "autowired-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "1kr2lxl808rf7v8gjybwrqpnxqay054702wzk2g48s30nc79386d")))

(define-public crate-autowired-0.1.5 (c (n "autowired") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 2)) (d (n "autowired-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0wpcwc77c85d99c94nijadmsh1jmil12f8xmk3nb4h8jyjhmr1ym")))

(define-public crate-autowired-0.1.6 (c (n "autowired") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 2)) (d (n "autowired-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "01g32h9hc6z5qsm3i1fy1jhabqbjy1gn2iz3fci7p0da3xsdgr6g")))

(define-public crate-autowired-0.1.7 (c (n "autowired") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 2)) (d (n "autowired-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "1sym7kw99m0xb7505dc6yj230hzjkh8pm57zwbm2qn1a0i598ydl")))

(define-public crate-autowired-0.1.8 (c (n "autowired") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 2)) (d (n "autowired-derive") (r "^0.1.8") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "0vz3hbvhqn78wv43ljlsazn4xl9l8h0sqn3dngv3fl8ppanmw7m3")))

