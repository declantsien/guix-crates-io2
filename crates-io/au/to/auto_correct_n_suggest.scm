(define-module (crates-io au to auto_correct_n_suggest) #:use-module (crates-io))

(define-public crate-auto_correct_n_suggest-0.1.0 (c (n "auto_correct_n_suggest") (v "0.1.0") (h "1yhpxcvxixrlylkk4akm440iaw4cbi7nkk3pcsv9jbhv5qxapd7b")))

(define-public crate-auto_correct_n_suggest-0.2.0 (c (n "auto_correct_n_suggest") (v "0.2.0") (h "04xf0d48iirhc2bkq8iaflfazqxbr138kng0rr02agipxd3pya0x")))

(define-public crate-auto_correct_n_suggest-1.0.0 (c (n "auto_correct_n_suggest") (v "1.0.0") (h "1w6wgi51c29b6129cw6gjs4pda96jghiq028xpmh6sc7c7mfxaiq")))

