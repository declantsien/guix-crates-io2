(define-module (crates-io au to autour_core) #:use-module (crates-io))

(define-public crate-autour_core-0.1.0 (c (n "autour_core") (v "0.1.0") (d (list (d (n "graphviz_dot_builder") (r "^0.1.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1vx82j6zngjp9mbmyagidlnpnwmgn1i07nfch34698l5zy2xc6ff")))

(define-public crate-autour_core-0.1.1 (c (n "autour_core") (v "0.1.1") (d (list (d (n "graphviz_dot_builder") (r "^0.1.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1fbs4wf6aghhkdwsnpzg58i2nrf636ywm1m4ar7adf4k40s9h5js")))

(define-public crate-autour_core-0.1.2 (c (n "autour_core") (v "0.1.2") (d (list (d (n "graphviz_dot_builder") (r "^0.1.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0q33la979n91gyqvkdsbv4bq4yb3hk6g89ds7lf515dxy14mffnc")))

(define-public crate-autour_core-0.1.3 (c (n "autour_core") (v "0.1.3") (d (list (d (n "graphviz_dot_builder") (r "^0.1.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "14851nfz8n81j6nl1arvx4269v2vrbhr6l6b9imw3qas07y535gc")))

(define-public crate-autour_core-0.1.4 (c (n "autour_core") (v "0.1.4") (d (list (d (n "graphviz_dot_builder") (r "^0.1.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0lrz6rv7y7yb508rlsgvv0mym0niqb17zc84wxiz13acidajgz09")))

(define-public crate-autour_core-0.1.5 (c (n "autour_core") (v "0.1.5") (d (list (d (n "graphviz_dot_builder") (r "^0.1.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0npkf2r31k94pqw8flpjapzxpyxd8x1vc6zblggmg6y84kj8n69q")))

(define-public crate-autour_core-0.1.6 (c (n "autour_core") (v "0.1.6") (d (list (d (n "graphviz_dot_builder") (r "^0.1.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0ddyacjf42y0syw71ixbn05xa92a2yn2nwz2yznwg187qzj89wd2")))

(define-public crate-autour_core-0.1.7 (c (n "autour_core") (v "0.1.7") (d (list (d (n "graphviz_dot_builder") (r "^0.1.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1j4hbahwsgm84qw2jnpajkarbvdfz3ljca2kl1k2hkgjl6yp8bxi")))

(define-public crate-autour_core-0.1.8 (c (n "autour_core") (v "0.1.8") (d (list (d (n "graphviz_dot_builder") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "11ws0r3n4z55vj6lsqr9ymifrd66d4pa2yzb0ykf88p6619c86wa")))

(define-public crate-autour_core-0.1.9 (c (n "autour_core") (v "0.1.9") (d (list (d (n "graphviz_dot_builder") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "01pyb81dpa07radh7jjylh6z2aiaaznl3006yfq8v3x4akjzk0ks")))

(define-public crate-autour_core-0.1.10 (c (n "autour_core") (v "0.1.10") (d (list (d (n "graphviz_dot_builder") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0n3hagrkvdb1nhgrp89d35skpdblbi5i6d0dp0ybqskax34f98z5")))

