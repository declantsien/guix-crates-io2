(define-module (crates-io au to auto-rust) #:use-module (crates-io))

(define-public crate-auto-rust-0.1.0 (c (n "auto-rust") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.162") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1hdn43dm4cnxlikwdrwydm7qy0a9iqblw2515vn3if04z4r0fgw6")))

(define-public crate-auto-rust-0.1.1 (c (n "auto-rust") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.162") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0i5bx594d7msp8spn5c3mnkl6jpfj8zqcylzsc46sw9283w2ias8")))

(define-public crate-auto-rust-0.1.2 (c (n "auto-rust") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1ynvsja8rb0ixc97cvyzrj1f83ladyq6h7xb8mwkyjk8flxy2lgz")))

(define-public crate-auto-rust-0.1.3 (c (n "auto-rust") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "17l3b12s785v466zl620q764gln12vxf0wfjfwsxbm1766r1whx3")))

