(define-module (crates-io au to auto_enumerate_core) #:use-module (crates-io))

(define-public crate-auto_enumerate_core-0.1.0 (c (n "auto_enumerate_core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "visit" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0r98148h8cymflhh3iywdp3c9jnifcg9b672wbhsgrdgqnrj025z") (f (quote (("unstable" "smallvec/union" "smallvec/may_dangle") ("type_analysis") ("default")))) (y #t)))

(define-public crate-auto_enumerate_core-0.1.1 (c (n "auto_enumerate_core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "visit" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0f8sblp6xjpdgq8dm4f0v27pjsbxgiqabgicv8rqjbf4vj94ilfn") (f (quote (("unstable" "smallvec/union" "smallvec/may_dangle") ("type_analysis") ("default")))) (y #t)))

(define-public crate-auto_enumerate_core-0.1.2 (c (n "auto_enumerate_core") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "visit" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0pr3ff895xm6r0qc544a1i45fyn3fckj83h5hb6czy8n47zdmddr") (f (quote (("unstable" "smallvec/union" "smallvec/may_dangle") ("type_analysis") ("default"))))))

(define-public crate-auto_enumerate_core-0.2.0 (c (n "auto_enumerate_core") (v "0.2.0") (h "0gwvgq11qnmvnp3aadh6bim9jpyhcvmbz5nl3wa2bnb7fjic26n6")))

