(define-module (crates-io au to autowired-derive) #:use-module (crates-io))

(define-public crate-autowired-derive-0.1.0 (c (n "autowired-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1zc1vjqww2jzqqri89wjyc4wn6zjjk29fblknh670ifhhbk29a2z")))

(define-public crate-autowired-derive-0.1.2 (c (n "autowired-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "01kb3ndkd3dp71yax60xs64jdjswqbmzfgw7zqnzmsijm40bl396")))

(define-public crate-autowired-derive-0.1.3 (c (n "autowired-derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0zv019mf6pl7c5b07d311s8n30zph9q5jg2cca77lxwlmgg54l98")))

(define-public crate-autowired-derive-0.1.4 (c (n "autowired-derive") (v "0.1.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0znfi1ms8c6bdrfi69gd18nl3b63nyys8nh33vlrxrl323zp5pav")))

(define-public crate-autowired-derive-0.1.5 (c (n "autowired-derive") (v "0.1.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1q55j3zqi7hifabbl5hygazpkk1bs25pylj82npkqmiy1k1gp3rs")))

(define-public crate-autowired-derive-0.1.8 (c (n "autowired-derive") (v "0.1.8") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qnfrlxn8ssajs8sihq1cbyfcbi69vyzf0wzrkyjhhpp9ypz1qh7")))

