(define-module (crates-io au to automap) #:use-module (crates-io))

(define-public crate-automap-0.1.0 (c (n "automap") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3") (d #t) (k 0)))) (h "04ngiaqmqszfpmz1g1mcccw59zq7mcgi5a0klysa3y3681zqi7dr")))

