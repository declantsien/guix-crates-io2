(define-module (crates-io au to automafish) #:use-module (crates-io))

(define-public crate-automafish-0.1.0 (c (n "automafish") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-env-log") (r "^0.2") (d #t) (k 2)))) (h "0ndywqygi8p18f4awfcm3yi4xs8fzq121kjjniwrz0jiwgvhh0dz")))

