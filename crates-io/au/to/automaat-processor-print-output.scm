(define-module (crates-io au to automaat-processor-print-output) #:use-module (crates-io))

(define-public crate-automaat-processor-print-output-0.1.0 (c (n "automaat-processor-print-output") (v "0.1.0") (d (list (d (n "automaat-core") (r "^0.1") (d #t) (k 0)) (d (n "juniper") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "juniper") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0d4lajf6x1g355xdk3nhr1phwhw1y74xjriiw1sscb5bcc45r2va")))

