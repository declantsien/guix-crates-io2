(define-module (crates-io au to auto_curry) #:use-module (crates-io))

(define-public crate-auto_curry-0.1.0 (c (n "auto_curry") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y66hbw8w05r1sz31f5amzmz6aqdlz8l986232bnym66pmr1knm8")))

(define-public crate-auto_curry-0.1.1 (c (n "auto_curry") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "115mjd1iwl1kday6k8gs4213xg0qdb8p8ap6iwyb4gx147gp3q2n")))

(define-public crate-auto_curry-1.0.0 (c (n "auto_curry") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "1g9fysy5d24p41isv16m7mxqvbrzvc9dfnf5mv4fmg3lqiypb1b0")))

