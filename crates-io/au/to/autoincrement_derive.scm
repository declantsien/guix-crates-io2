(define-module (crates-io au to autoincrement_derive) #:use-module (crates-io))

(define-public crate-autoincrement_derive-1.0.0 (c (n "autoincrement_derive") (v "1.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c784nssmg22w7wnv1zmp5wgf6i4m4rxwywflj0z5cwskpkfjsrz")))

(define-public crate-autoincrement_derive-1.0.1 (c (n "autoincrement_derive") (v "1.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a520kbisjh8b10srny3sjq4d90aml06h8rxv9d3ysjqkadwwn8j")))

