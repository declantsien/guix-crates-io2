(define-module (crates-io au to auto-builder-macro) #:use-module (crates-io))

(define-public crate-auto-builder-macro-0.1.0 (c (n "auto-builder-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0rpfghhhjl9qmjbgy1fh297c5vzdvw20bjs9c4gn32ygj2i0valw")))

(define-public crate-auto-builder-macro-0.2.0 (c (n "auto-builder-macro") (v "0.2.0") (d (list (d (n "auto-builder-core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0kv9f7f1gpwgb7ibdkb99qgghnw7f96kvgbdkwxdy6xa1y3f9b5d")))

