(define-module (crates-io au to autoshutdown) #:use-module (crates-io))

(define-public crate-autoshutdown-0.1.0 (c (n "autoshutdown") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "simple-error") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0cf3n0iw0gzc8h0rmp37c42qli9ghm0qklx8lzbdbfs5g7a2q9fs")))

(define-public crate-autoshutdown-0.1.1 (c (n "autoshutdown") (v "0.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "simple-error") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0ja04z2n223svxjwsmlsa6qkwnr4d9iq9krnpkd1hb9j3ys4zp7y")))

