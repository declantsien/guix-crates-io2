(define-module (crates-io au to auto-const-array) #:use-module (crates-io))

(define-public crate-auto-const-array-0.1.0 (c (n "auto-const-array") (v "0.1.0") (h "1r6va6b9l1wb6fqspprsqwh5iib8r6sywkcdsawf5nlv26jz1vyc")))

(define-public crate-auto-const-array-0.1.1 (c (n "auto-const-array") (v "0.1.1") (h "0jgqcz0s071azysf5sl6nzzyz877111ayysgsm7zjgn03h9ja6bb")))

(define-public crate-auto-const-array-0.2.0 (c (n "auto-const-array") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "010f3hbczd8a90qpg0y544n7v8mzi2kx4nia32parfqjprqlfv1k")))

(define-public crate-auto-const-array-0.2.1 (c (n "auto-const-array") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fylx07l043nwfv611qqmimxbmpymss32jzfa0vf07ksjwcdzxv2")))

