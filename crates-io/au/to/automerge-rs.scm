(define-module (crates-io au to automerge-rs) #:use-module (crates-io))

(define-public crate-automerge-rs-0.0.1 (c (n "automerge-rs") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (d #t) (k 0)))) (h "0afm79fbbp5j8xccaqkcwjf86kv70dq7w9pq8kyy1nbdp4d5hyb7") (y #t)))

(define-public crate-automerge-rs-0.0.2 (c (n "automerge-rs") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03wr70dxq4xyp817c6mi0750n07mvcpfdakkhnrgpjwh0x8ml56v") (y #t)))

