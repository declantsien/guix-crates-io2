(define-module (crates-io au to automatons) #:use-module (crates-io))

(define-public crate-automatons-0.1.0 (c (n "automatons") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (o #t) (d #t) (k 0)))) (h "11ynhnbkvqd23z8b8qspk7s9x1ka2bpwqdv9vdn04lv456mva642")))

(define-public crate-automatons-0.2.0 (c (n "automatons") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0rsrb6v48kb7fm8jw72vb6zwnkp9h9rxvd0kwnjqds1mg9jnfv9b")))

(define-public crate-automatons-0.3.0 (c (n "automatons") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1h6skqqwj85w4gqkwr18pvk7y9gghz5129jgdyn1dyrpn1kfsl3s")))

