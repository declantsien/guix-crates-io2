(define-module (crates-io au to auto-delete-path) #:use-module (crates-io))

(define-public crate-auto-delete-path-0.1.0 (c (n "auto-delete-path") (v "0.1.0") (h "003r5zmflnvg71hh7fv6c7yi2pyfn9axijvqhbc6mx7yx492bmwj")))

(define-public crate-auto-delete-path-0.2.0 (c (n "auto-delete-path") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.7") (d #t) (k 0)))) (h "1ymla3n0v6gwgnal8q1a6s05xw5z5k4r2q3g1z6rw9flx23gidwy")))

