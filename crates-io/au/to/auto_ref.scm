(define-module (crates-io au to auto_ref) #:use-module (crates-io))

(define-public crate-auto_ref-0.1.0 (c (n "auto_ref") (v "0.1.0") (d (list (d (n "auto_ref_impl") (r "^0.1.0") (d #t) (k 0)))) (h "1x2nbfj45pxrpcz1j36v738zzcf4lz4bp501jxppzpsq1ha4wzxg")))

(define-public crate-auto_ref-0.1.1 (c (n "auto_ref") (v "0.1.1") (d (list (d (n "auto_ref_impl") (r "^0.1.1") (d #t) (k 0)))) (h "1vyrcmfc99divpkz07qnr7s3x58s6jzi6ajbg9qb59fqrlmzjgzg")))

(define-public crate-auto_ref-0.1.2 (c (n "auto_ref") (v "0.1.2") (d (list (d (n "auto_ref_impl") (r "^0.1.2") (d #t) (k 0)))) (h "1s4l89fgm4h4i96k0sw953cyw0m040i7hlm92qjbxpfz2y51jlnz")))

