(define-module (crates-io au to auto_worlds) #:use-module (crates-io))

(define-public crate-auto_worlds-0.1.0 (c (n "auto_worlds") (v "0.1.0") (d (list (d (n "auto_cellular") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xakpc0daabjj5a99vfxmg640wxfn9q9axx4254dbfl8xphz26w3")))

