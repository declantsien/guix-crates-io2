(define-module (crates-io au to auto-version) #:use-module (crates-io))

(define-public crate-auto-version-0.1.0 (c (n "auto-version") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1kjihcfpxj7h3knm08mlyhbs4p9gyxpna2fxwxbdi8rq5qclh1qq")))

(define-public crate-auto-version-0.2.0 (c (n "auto-version") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0g8bz7sx97r8dxwrix83cn2vmv2ij65r5rqqr4awdvv6jnvcl6k2")))

(define-public crate-auto-version-0.2.1 (c (n "auto-version") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "14x4vinm71givlzg4xz12m84b1jmsk2a04m6bdhg74ylfqff0hgf")))

