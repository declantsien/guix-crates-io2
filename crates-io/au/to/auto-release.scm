(define-module (crates-io au to auto-release) #:use-module (crates-io))

(define-public crate-auto-release-0.4.0 (c (n "auto-release") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.0") (f (quote ("derive" "help" "std"))) (k 0)) (d (n "release-utils") (r "^0.4.0") (d #t) (k 0)))) (h "1f8jz1krkdq2c5mr6w2xs6sxnqar1ygcj9mp0a9djfjdva9mmjdm") (r "1.70")))

(define-public crate-auto-release-0.4.1 (c (n "auto-release") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.0") (f (quote ("derive" "help" "std"))) (k 0)) (d (n "release-utils") (r "^0.4.0") (d #t) (k 0)))) (h "09ngny30n5mmpd468cvjyanp4v988nzidi6h3rkv4kiq61ymccbb") (r "1.70")))

(define-public crate-auto-release-0.5.0 (c (n "auto-release") (v "0.5.0") (d (list (d (n "release-utils") (r "^0.5.0") (d #t) (k 0)))) (h "1pgp6slrx9i7k3p9w5q5fisi7hv9p4vwvbc8amjicg746839r5kq") (r "1.70")))

