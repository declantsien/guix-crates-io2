(define-module (crates-io au to automata) #:use-module (crates-io))

(define-public crate-automata-0.0.1 (c (n "automata") (v "0.0.1") (h "1j1fx7kqxmks0fi4rq6v8kgzzqfd95kxlx4syll1p0s26z24p1mz")))

(define-public crate-automata-0.0.2 (c (n "automata") (v "0.0.2") (h "1ihnwk2wg5p6pfr3069g90jkmjqwnsac344ccad913grlk871d23")))

(define-public crate-automata-0.0.3 (c (n "automata") (v "0.0.3") (h "0idrw48vadav106dfm1hcdnlp1ghs3ps0v6w9q864jxg191lnp3c")))

(define-public crate-automata-0.0.4 (c (n "automata") (v "0.0.4") (h "0z5gkrc1ddxc433ycdm7jwkpwzjm4p114ld2ss8bgi7aqnlr35h0")))

