(define-module (crates-io au to auto-image-cropper) #:use-module (crates-io))

(define-public crate-auto-image-cropper-0.1.0 (c (n "auto-image-cropper") (v "0.1.0") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 0)))) (h "071q8spb5khyf2051c1mz44kp95a2s4ng48z5vfg36rsfg4q217l")))

(define-public crate-auto-image-cropper-0.1.1 (c (n "auto-image-cropper") (v "0.1.1") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 0)))) (h "07139887ampk4z3yr8s4ziw53fs8fzlpxh4lj9x2iy63kqjal1hk")))

(define-public crate-auto-image-cropper-0.1.2 (c (n "auto-image-cropper") (v "0.1.2") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)))) (h "1caqfmkdddih2hc28n3zfgifc54c72d0q03z752b27ki3ppi0ncb")))

(define-public crate-auto-image-cropper-0.1.3 (c (n "auto-image-cropper") (v "0.1.3") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)))) (h "1n3ld0xqrx2sng18hd14vjfmhabd04ms8pq84i5lxj2msbma0mww")))

(define-public crate-auto-image-cropper-0.1.4 (c (n "auto-image-cropper") (v "0.1.4") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)))) (h "189ryw6mzicd4qjflbi8gy91593wmig709d8xyg5zjwr73kl7l5r")))

(define-public crate-auto-image-cropper-0.1.5 (c (n "auto-image-cropper") (v "0.1.5") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rayon") (r "^0.7") (d #t) (k 0)))) (h "09fkvarjlq2j06f99p5wnwbxiv13h5wvasvmn6akqxzzxzg9nbdh")))

