(define-module (crates-io au to auto-git) #:use-module (crates-io))

(define-public crate-auto-git-0.1.0 (c (n "auto-git") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustygit") (r "^0.5.0") (d #t) (k 0)))) (h "1y2lwhfmbgl895944qwfzlb3c4y8hbi5j2w775xycdahqqf5qk6w")))

