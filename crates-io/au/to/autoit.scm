(define-module (crates-io au to autoit) #:use-module (crates-io))

(define-public crate-autoit-0.1.0 (c (n "autoit") (v "0.1.0") (d (list (d (n "libbindgen") (r "^0.1.2") (d #t) (k 1)))) (h "01k9p45i2flgfb9sccc83f236sbznc2wjnij7ybxiai7jw2bmab4")))

(define-public crate-autoit-0.1.1 (c (n "autoit") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.20") (d #t) (k 1)))) (h "0wn6g70fg12gfblf7y28yzkxnhbxwz23l4ssg65d99r29fli83aq")))

(define-public crate-autoit-0.1.2 (c (n "autoit") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.39") (d #t) (k 1)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "06ryvbc6w5gp7ci8z8q0wnijmard9b9k6drhg4wpkbz9c8v9zg1d")))

