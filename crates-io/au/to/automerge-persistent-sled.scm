(define-module (crates-io au to automerge-persistent-sled) #:use-module (crates-io))

(define-public crate-automerge-persistent-sled-0.1.0 (c (n "automerge-persistent-sled") (v "0.1.0") (d (list (d (n "automerge") (r "^0.1.0") (d #t) (k 0)) (d (n "automerge-persistent") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "035xaypj7c1fi4f0cgvidzymb1ilspn8j19w1629nn13ym85rs37")))

(define-public crate-automerge-persistent-sled-0.2.0 (c (n "automerge-persistent-sled") (v "0.2.0") (d (list (d (n "automerge") (r "^0.2") (d #t) (k 0)) (d (n "automerge-persistent") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1a9c08h5jra0nz9shkbgbhkxqi1qvx9mkh1wnb241kxmnvfdjdr0")))

(define-public crate-automerge-persistent-sled-0.3.0 (c (n "automerge-persistent-sled") (v "0.3.0") (d (list (d (n "automerge") (r "^0.3") (d #t) (k 0)) (d (n "automerge-persistent") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0jrhbxf2yzwn45lxlwr4jyl3r5mmih1y22k18chj5gyfv30i8pqp")))

(define-public crate-automerge-persistent-sled-0.4.0 (c (n "automerge-persistent-sled") (v "0.4.0") (d (list (d (n "automerge") (r "^0.4") (d #t) (k 0)) (d (n "automerge-persistent") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1gklxngibqcv376xgw854h92i4ml4lv979aqgm74m37hm7y14p88")))

