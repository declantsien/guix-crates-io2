(define-module (crates-io au to auto-add-lifetimes-to-impl) #:use-module (crates-io))

(define-public crate-auto-add-lifetimes-to-impl-0.1.0 (c (n "auto-add-lifetimes-to-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("printing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1zsi4zx25dnm8v4rmic4vlqk25kmsr6qdxgyvwdhkfjyr239pfrc")))

