(define-module (crates-io au to auto-builder-core) #:use-module (crates-io))

(define-public crate-auto-builder-core-0.1.0 (c (n "auto-builder-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1l1925qbg71kymn2lzmjpplx3n48qygw2dckwwdlcj6i1v18r3gx")))

(define-public crate-auto-builder-core-0.2.0 (c (n "auto-builder-core") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0hb5szf81yxsbzcjb94drj2yiakz0cykp3vh0pavlwv1i3h7hlsy")))

