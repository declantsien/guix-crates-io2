(define-module (crates-io au to auto-cc) #:use-module (crates-io))

(define-public crate-auto-cc-0.1.0 (c (n "auto-cc") (v "0.1.0") (d (list (d (n "bacon_rajan_cc") (r "^0.3.0") (d #t) (k 0)))) (h "0fa7iay6mkr5saf5dbpnwgpzpp0w2a3p63sg849v6yzjimpqyvid")))

(define-public crate-auto-cc-0.2.0 (c (n "auto-cc") (v "0.2.0") (d (list (d (n "bacon_rajan_cc") (r "^0.3.0") (d #t) (k 0)))) (h "1fvrs8fvm8wwjchsdm7zjkd6hskc9slr4kxflq8fs2b92876wskq")))

