(define-module (crates-io au to auto-palette-cli) #:use-module (crates-io))

(define-public crate-auto-palette-cli-0.3.0 (c (n "auto-palette-cli") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "auto-palette") (r "^0.3.0") (f (quote ("image"))) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)))) (h "0ydffx8ndxf8ivda4zj07f72gskh9sgv3fq5jxlgpqwairardvq6") (r "1.75.0")))

