(define-module (crates-io au to autoconf) #:use-module (crates-io))

(define-public crate-autoconf-0.1.0 (c (n "autoconf") (v "0.1.0") (d (list (d (n "autoconf_core") (r "^0.1.0") (d #t) (k 0)) (d (n "autoconf_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "config") (r "^0.14.0") (f (quote ("toml"))) (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "smart-default") (r "^0.7.1") (d #t) (k 2)))) (h "18xpnjpkn0ihrsa6y52czqmspqi1rs72dzaz3ffb0i8f0jnvisdd") (y #t)))

