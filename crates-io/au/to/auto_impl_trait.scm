(define-module (crates-io au to auto_impl_trait) #:use-module (crates-io))

(define-public crate-auto_impl_trait-0.1.0 (c (n "auto_impl_trait") (v "0.1.0") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1db9czpny3h7hlicwmcicvxyrdnqvq22r854rnlq44y8ss5jsinw") (y #t)))

(define-public crate-auto_impl_trait-0.2.0 (c (n "auto_impl_trait") (v "0.2.0") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vlshhlk2svfm099m67c5ylsskq3wqbv4l7w716120aiyzv7snyd") (y #t)))

(define-public crate-auto_impl_trait-0.3.0 (c (n "auto_impl_trait") (v "0.3.0") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n25k9s677jldxsdg65w3sfnx7ylcqn0kak4605phqizpcjhm3k8")))

(define-public crate-auto_impl_trait-0.3.1 (c (n "auto_impl_trait") (v "0.3.1") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y606dsgi8gnmny338mp7y0cpkx4q9m5r02hnmyc39vl3ap5s5s8")))

(define-public crate-auto_impl_trait-0.4.0 (c (n "auto_impl_trait") (v "0.4.0") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zkbw543y7b1hhv642w1irxxaxzc4xn95ax70nzbahddnxh9na1k")))

(define-public crate-auto_impl_trait-0.5.0 (c (n "auto_impl_trait") (v "0.5.0") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1knzz7lny9sbh2x37m9qnz9wwdh0bv34z804gv9k00zydxhd2yx2")))

(define-public crate-auto_impl_trait-0.5.1 (c (n "auto_impl_trait") (v "0.5.1") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.2") (d #t) (k 0)))) (h "1mxpq30wavxmmlp326x250sz98pnrlaysmf54m3kwwprb6gr4a58")))

(define-public crate-auto_impl_trait-0.6.0 (c (n "auto_impl_trait") (v "0.6.0") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.2") (d #t) (k 0)))) (h "0i13znnhcilim0kdv7vm8rrax6yycdcsa2xi0c67gxc6as5078s5") (f (quote (("v1alpha2") ("v1") ("default" "v1"))))))

(define-public crate-auto_impl_trait-0.6.1 (c (n "auto_impl_trait") (v "0.6.1") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.2") (d #t) (k 0)))) (h "1qz8gjyqrl4ggsfznrq24zbp3gcnin6mcwmp0i44ybjmnmgj4y9m") (f (quote (("v1alpha2") ("v1") ("default" "v1"))))))

(define-public crate-auto_impl_trait-0.6.2 (c (n "auto_impl_trait") (v "0.6.2") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.2") (d #t) (k 0)))) (h "0iab72dgl0sfc41gv1z2k66x6b85jljb5zbr4a21348xh79m6p0m") (f (quote (("v1alpha2") ("v1") ("default" "v1"))))))

(define-public crate-auto_impl_trait-0.7.0 (c (n "auto_impl_trait") (v "0.7.0") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.2") (f (quote ("codegen"))) (d #t) (k 0)))) (h "1vxsxip05d6l1kzvvwgymj7shk32m6h54pzx9jl6apk31hgh658s") (f (quote (("v1alpha2") ("v1") ("default" "v1"))))))

(define-public crate-auto_impl_trait-0.7.1 (c (n "auto_impl_trait") (v "0.7.1") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.2") (f (quote ("codegen"))) (d #t) (k 0)))) (h "1nz1mvi8aiv3gybyi98rwsjr5jd4hz2qw4y9acv7fcp3p18i3n08") (f (quote (("v1alpha2") ("v1") ("default" "v1"))))))

