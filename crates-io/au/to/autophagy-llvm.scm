(define-module (crates-io au to autophagy-llvm) #:use-module (crates-io))

(define-public crate-autophagy-llvm-0.1.0 (c (n "autophagy-llvm") (v "0.1.0") (d (list (d (n "autophagy") (r "^0.1") (d #t) (k 0)) (d (n "inkwell") (r "^0.2.0") (f (quote ("llvm16-0"))) (d #t) (k 0)))) (h "11yjh6wjh728whnma7ssjzqmawbwpys6wign58hdr3f8xg93m9rc")))

(define-public crate-autophagy-llvm-0.1.1 (c (n "autophagy-llvm") (v "0.1.1") (d (list (d (n "autophagy") (r "^0.1") (d #t) (k 0)) (d (n "inkwell") (r "^0.2.0") (f (quote ("llvm16-0"))) (d #t) (k 0)))) (h "1w4qnq3p4zsbvgiki8vbj7gj7bsgh3v6knnvl5wp8pdp80g4c8g2")))

