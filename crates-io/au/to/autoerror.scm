(define-module (crates-io au to autoerror) #:use-module (crates-io))

(define-public crate-autoerror-1.0.0 (c (n "autoerror") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0dislpmfq3sikgxxzr7h58hfmjcmy4ziw8fhk1aj09z0xr8ydikz")))

