(define-module (crates-io au to autolink) #:use-module (crates-io))

(define-public crate-autolink-0.1.0 (c (n "autolink") (v "0.1.0") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "10d59jr9nnqpj6y6ar6is73xn3nsk3jndadwi0mji8ac2k9z5562")))

(define-public crate-autolink-0.2.0 (c (n "autolink") (v "0.2.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1nj6z0773qqn23sx7mirxgjz7k0isvcp2y7x1xkcm92xrq7il6rn")))

(define-public crate-autolink-0.3.0 (c (n "autolink") (v "0.3.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "18df1srvgxyxq5vcvy2rzcdidclbmixzxnmbz9cpvf7jhjckrg2v")))

