(define-module (crates-io au to autophagy-macro) #:use-module (crates-io))

(define-public crate-autophagy-macro-0.1.0 (c (n "autophagy-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ink3ydd03qd6sxl7rdjvjvdw1pa70xpi8f3y8qw8xs8h1xm5x18")))

(define-public crate-autophagy-macro-0.1.1 (c (n "autophagy-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0kzl2n1ccrxzbjmzkyc25w5ckp0z791zz20iqib9qhz2lwq8lr7k")))

