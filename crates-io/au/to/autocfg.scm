(define-module (crates-io au to autocfg) #:use-module (crates-io))

(define-public crate-autocfg-0.0.1 (c (n "autocfg") (v "0.0.1") (h "13xyd0djk5130s9qywkysid5xq2hyii4cq5wi3jpv5qgkll66xpa")))

(define-public crate-autocfg-0.1.0 (c (n "autocfg") (v "0.1.0") (h "1y3n0dm6vy31qflvnl1z0q4wnqj5h1raglkkky92s3qzc3vkl893")))

(define-public crate-autocfg-0.1.1 (c (n "autocfg") (v "0.1.1") (h "09s7azy42g6vrvnbmzbb3lm1mrgbpyi28znwigxp7581gbgk8psf")))

(define-public crate-autocfg-0.1.2 (c (n "autocfg") (v "0.1.2") (h "16fprz5qi7paij5swyxb2hjris6d7bjzm9v8805gcjfswaz41mm6")))

(define-public crate-autocfg-0.1.3 (c (n "autocfg") (v "0.1.3") (h "0mav0ch1gicr56v8xc7q2hgyxf38hg9cifh8ipqqi79x5aiprcyz") (y #t)))

(define-public crate-autocfg-0.1.4 (c (n "autocfg") (v "0.1.4") (h "1gvpf03pdl08k29j9hjv7r7cs5zn39ib97f7wwvzv9992fjyyj8f")))

(define-public crate-autocfg-0.1.5 (c (n "autocfg") (v "0.1.5") (h "0asl6fnc35yk5l2rxwhp25v128jgm45dp754h9z8x51b6n90w4r2")))

(define-public crate-autocfg-0.1.6 (c (n "autocfg") (v "0.1.6") (h "0x8q946yy321rlpxhqf3mkd965x8kbjs2jwcw55dsmxlf7xwhwdn")))

(define-public crate-autocfg-0.1.7 (c (n "autocfg") (v "0.1.7") (h "1chwgimpx5z7xbag7krr9d8asxfqbh683qhgl9kn3hxk2l0djj8x")))

(define-public crate-autocfg-1.0.0 (c (n "autocfg") (v "1.0.0") (h "17cv6pwb4q08s0ynpr4n8hv5299hcmhdgvdchzixfpw8y5qcgapq")))

(define-public crate-autocfg-1.0.1 (c (n "autocfg") (v "1.0.1") (h "0jj6i9zn4gjl03kjvziqdji6rwx8ykz8zk2ngpc331z2g3fk3c6d")))

(define-public crate-autocfg-1.1.0 (c (n "autocfg") (v "1.1.0") (h "1ylp3cb47ylzabimazvbz9ms6ap784zhb6syaz6c1jqpmcmq0s6l")))

(define-public crate-autocfg-0.1.8 (c (n "autocfg") (v "0.1.8") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 0)))) (h "0y4vw4l4izdxq1v0rrhvmlbqvalrqrmk60v1z0dqlgnlbzkl7phd")))

(define-public crate-autocfg-1.2.0 (c (n "autocfg") (v "1.2.0") (h "102c77is3pii4rsqfsc5vrbk6qabjy0yqc0gwqzmjjb9fp3spzgi") (r "1.0")))

(define-public crate-autocfg-1.3.0 (c (n "autocfg") (v "1.3.0") (h "1c3njkfzpil03k92q0mij5y1pkhhfr4j3bf0h53bgl2vs85lsjqc") (r "1.0")))

