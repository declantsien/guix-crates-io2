(define-module (crates-io au to autopad) #:use-module (crates-io))

(define-public crate-autopad-1.0.0 (c (n "autopad") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "memoffset") (r "^0.8.0") (d #t) (k 2)))) (h "1f33g7vrb5w1ja12s8mkaljwrp9q5y6p7r2lspsq8wb87v40br7g")))

