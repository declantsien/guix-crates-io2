(define-module (crates-io au to autosar-data-specification) #:use-module (crates-io))

(define-public crate-autosar-data-specification-0.2.0 (c (n "autosar-data-specification") (v "0.2.0") (h "0nrvvjdc4h8llnzhb15mrf544d4853qhk1i3xz7ahyhg3yvynnvh")))

(define-public crate-autosar-data-specification-0.3.0 (c (n "autosar-data-specification") (v "0.3.0") (h "14h1m7x3kpzd3i6z41i83mji77mdg9knlkx4d0cvris789x8y0j7")))

(define-public crate-autosar-data-specification-0.4.0 (c (n "autosar-data-specification") (v "0.4.0") (h "10yj12piwpvds54gkq6cb28l4lrrm2bxnj7g5081nq3aiw9900sn")))

(define-public crate-autosar-data-specification-0.5.0 (c (n "autosar-data-specification") (v "0.5.0") (h "05vdhs27lz9isgr3c1wa1rhg42jhvnwc95m2fr1jyshs4345xbqd")))

(define-public crate-autosar-data-specification-0.6.0 (c (n "autosar-data-specification") (v "0.6.0") (h "0527829md0hrpfrk38ra1axdilpiw7ibzdvbgakp6v8ayni0c166")))

(define-public crate-autosar-data-specification-0.7.0 (c (n "autosar-data-specification") (v "0.7.0") (d (list (d (n "pyo3") (r "^0.19.2") (o #t) (d #t) (k 0)))) (h "14pj7p55zs5n7bcnacmynsks328nnhr84v41wgv9gz4hnwffz754") (s 2) (e (quote (("pylib" "dep:pyo3"))))))

(define-public crate-autosar-data-specification-0.8.0 (c (n "autosar-data-specification") (v "0.8.0") (d (list (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "11nfhl2b4w8lbvmaq43anms562q71d6b3sc25z9gxlxxw37x2wy5")))

(define-public crate-autosar-data-specification-0.9.0 (c (n "autosar-data-specification") (v "0.9.0") (d (list (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1dq6hc0flkl62g7hwcdwqbb8rj10c3d901ccp8fnfzpx92c205ll")))

(define-public crate-autosar-data-specification-0.9.1 (c (n "autosar-data-specification") (v "0.9.1") (d (list (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0bc5afq8za59hxbakila6xlka1kdxzj7278v4r3g0b4wbrfbj42b")))

(define-public crate-autosar-data-specification-0.10.0 (c (n "autosar-data-specification") (v "0.10.0") (d (list (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1d7ck1g1h29aj8xdzs7ncpnbzw7vq295szgwvgvqq8ph7ishz4l6")))

(define-public crate-autosar-data-specification-0.11.0 (c (n "autosar-data-specification") (v "0.11.0") (d (list (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0zxfxjpwm33jfpsfscifladnrmw1l6zd7ym5nd854ichds54vdan")))

(define-public crate-autosar-data-specification-0.11.1 (c (n "autosar-data-specification") (v "0.11.1") (d (list (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0dmv46hhfyaksz2apj1m36z2hv0iwlrry67fnijhjfv1bzvckkl5")))

(define-public crate-autosar-data-specification-0.12.0 (c (n "autosar-data-specification") (v "0.12.0") (d (list (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "14rmi3dva1dj47hiaww4jwdi7qx0xs4a1v2mvkajriyw38nrvlx4") (f (quote (("docstrings"))))))

(define-public crate-autosar-data-specification-0.13.0 (c (n "autosar-data-specification") (v "0.13.0") (d (list (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "14l5s817qp53s762bzhwrm00dvbr9vijz85bim6szm449dr42cj3") (f (quote (("docstrings"))))))

