(define-module (crates-io au to automato_sync) #:use-module (crates-io))

(define-public crate-automato_sync-0.0.1 (c (n "automato_sync") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s5dpxq3ylc2nyd12lw6ryywrb8s7qnc9pxcf0jlb1mfhqgahw35")))

(define-public crate-automato_sync-0.0.2 (c (n "automato_sync") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fajdqqap4q84x3wzamrzm0nh0h8pdpd98c3zia3vp4fpy5aclik")))

