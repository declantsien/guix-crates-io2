(define-module (crates-io au to autotiler) #:use-module (crates-io))

(define-public crate-autotiler-1.0.0 (c (n "autotiler") (v "1.0.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "059b2k8cczrs3b8hf3jddhdsxbdb3nr3chs6521b3v5r6bfn5gwd")))

(define-public crate-autotiler-1.1.0 (c (n "autotiler") (v "1.1.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1v4hpkvzw63dj6jq4xsb9cr8xklrrnq3pzgklns1fmspyflbzy11")))

