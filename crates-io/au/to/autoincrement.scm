(define-module (crates-io au to autoincrement) #:use-module (crates-io))

(define-public crate-autoincrement-1.0.0 (c (n "autoincrement") (v "1.0.0") (d (list (d (n "autoincrement_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1b4p3wzm64dl00sb8j4rrv473sqsri81bm7d5vd8zykk0j3mk2rr") (f (quote (("sync") ("full" "sync" "async" "derive" "serde") ("derive" "autoincrement_derive") ("default" "sync" "async" "derive") ("async"))))))

(define-public crate-autoincrement-1.0.1 (c (n "autoincrement") (v "1.0.1") (d (list (d (n "autoincrement_derive") (r "=1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zkvhyva406zsxnihvnq3lxp4gr8pabzn4qifif728li1jnvd0fw") (f (quote (("sync") ("full" "sync" "async" "derive" "serde") ("derive" "autoincrement_derive") ("default" "sync" "async" "derive") ("async"))))))

