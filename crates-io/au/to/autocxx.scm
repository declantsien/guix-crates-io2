(define-module (crates-io au to autocxx) #:use-module (crates-io))

(define-public crate-autocxx-0.1.0 (c (n "autocxx") (v "0.1.0") (d (list (d (n "autocxx-engine") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "038r0di858c63jz3gd3qc9xv5rwnvjd3i8p9p7s3b1n8fs5j1brq")))

(define-public crate-autocxx-0.2.0 (c (n "autocxx") (v "0.2.0") (d (list (d (n "autocxx-engine") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bb8sd5wj1vjw8allsxpzqf3wp6jbkbfyplqx3kjafrfza5ylqqz")))

(define-public crate-autocxx-0.3.0 (c (n "autocxx") (v "0.3.0") (d (list (d (n "autocxx-engine") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kr5k6df8ddix8sfmq4c72sjp2k3ia5xgnpb8f27n29gkia2pkwr")))

(define-public crate-autocxx-0.3.1 (c (n "autocxx") (v "0.3.1") (d (list (d (n "autocxx-engine") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f6kai6ihnla2mfvmpyy4g63jspfvgs6ddli2yygq1rm0v6f21qw")))

(define-public crate-autocxx-0.3.2 (c (n "autocxx") (v "0.3.2") (d (list (d (n "autocxx-engine") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r0r5k7dw0g5c48qlfz5698c565wf3wrlzxllydbrmxsz7v63lxw")))

(define-public crate-autocxx-0.3.3 (c (n "autocxx") (v "0.3.3") (d (list (d (n "autocxx-engine") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x7ry2gq3z8gs2di56r5gsibnpcb19dw9hpng4mni5q239vykx2a")))

(define-public crate-autocxx-0.4.0 (c (n "autocxx") (v "0.4.0") (d (list (d (n "autocxx-engine") (r "^0.4.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.4.0") (d #t) (k 0)))) (h "0ynq4c576hkvmpj6ifkjczl6ynk775a2g04csaycidfrj0axggw4")))

(define-public crate-autocxx-0.4.1 (c (n "autocxx") (v "0.4.1") (d (list (d (n "autocxx-engine") (r "^0.4.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.4.1") (d #t) (k 0)))) (h "0ivp0r2slk6hhq069n4jz12f02k8vrs6wpb786b1784974mkky4z")))

(define-public crate-autocxx-0.5.0 (c (n "autocxx") (v "0.5.0") (d (list (d (n "autocxx-engine") (r "^0.5.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.20") (d #t) (k 0)))) (h "0644z3vihf09s8bw63kapck3cp1ad07kb1xvxc13d55g8snpqwfv")))

(define-public crate-autocxx-0.5.1 (c (n "autocxx") (v "0.5.1") (d (list (d (n "aquamarine") (r "^0.1.5") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.5.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.5.1") (d #t) (k 0)) (d (n "cxx") (r "^1.0.29") (d #t) (k 0)))) (h "0cg318qblvkhrswd5v39rbizdrbi4lqwj4fhkjd4b0x5vg510p74")))

(define-public crate-autocxx-0.5.2 (c (n "autocxx") (v "0.5.2") (d (list (d (n "aquamarine") (r "^0.1.5") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.5.2") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.5.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.29") (d #t) (k 0)))) (h "0bpgnqp8hzi4f7n79nxpy7ny985ga605dm8r1awlflx3kh6hv494") (f (quote (("pointers" "autocxx-engine/pointers"))))))

(define-public crate-autocxx-0.5.3 (c (n "autocxx") (v "0.5.3") (d (list (d (n "aquamarine") (r "^0.1.5") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.5.3") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.5.3") (d #t) (k 0)) (d (n "cxx") (r "^1.0.34") (d #t) (k 0)))) (h "1vq2r09xpbr7pbpzvgv4jhbn36x1i5wr0cnr41ga9gykfkwjc621")))

(define-public crate-autocxx-0.5.4 (c (n "autocxx") (v "0.5.4") (d (list (d (n "aquamarine") (r "^0.1.5") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.5.4") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.5.4") (d #t) (k 0)) (d (n "cxx") (r "^1.0.38") (d #t) (k 0)))) (h "0zn5j97g46im71mihn0nk00ck5iwiz70b886ymf1na1jyscclzqa")))

(define-public crate-autocxx-0.6.0 (c (n "autocxx") (v "0.6.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.6.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.44") (d #t) (k 0)))) (h "1jdfx275q6hby01jqm4ch21xxi0la7adaapg9q0gsdkg2ipicgfz")))

(define-public crate-autocxx-0.7.0 (c (n "autocxx") (v "0.7.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.7.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.7.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.44") (d #t) (k 0)))) (h "01z5qfrjl48s4bzy6w6srrm3vw8gp2ijbxil12356llxlabygkm2")))

(define-public crate-autocxx-0.7.1 (c (n "autocxx") (v "0.7.1") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.7.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.7.1") (d #t) (k 0)) (d (n "cxx") (r "^1.0.44") (d #t) (k 0)))) (h "1v4bbxfhcg0n6vw1m9sj3jm9dsj42f0g7v7gix9ywghfnv26gl5h")))

(define-public crate-autocxx-0.8.0 (c (n "autocxx") (v "0.8.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.8.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.44") (d #t) (k 0)))) (h "0m942xr88yvb47r12wlfdgfphnnd6z0pvmhwn2nlz2x91b6mw6l0")))

(define-public crate-autocxx-0.9.0 (c (n "autocxx") (v "0.9.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.9.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.9.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.49") (d #t) (k 0)))) (h "0068zs0b7wsmkj5l1g7q3is0rp9vb2pqkbp86drfzhi8hxbs7f01")))

(define-public crate-autocxx-0.10.0 (c (n "autocxx") (v "0.10.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.10.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.10.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.49") (d #t) (k 0)))) (h "0s61karmaj8jz56gwq1ysfkdr5g6mczk1wsyvipiiv0vj4s0fx4y")))

(define-public crate-autocxx-0.11.0 (c (n "autocxx") (v "0.11.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.11.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.11.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.49") (d #t) (k 0)))) (h "0rsxsls9gp0q23dymacngsinhiyf0bzi4bgvp64i7m07lx1lzvya")))

(define-public crate-autocxx-0.11.1 (c (n "autocxx") (v "0.11.1") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.11.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.11.1") (d #t) (k 0)) (d (n "cxx") (r "^1.0.52") (d #t) (k 0)))) (h "1ab3ril2mj2s8xnx22200pzxdhkjp4flkdmfxg801799scv5rrr7")))

(define-public crate-autocxx-0.11.2 (c (n "autocxx") (v "0.11.2") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.11.2") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.11.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)))) (h "0z31880x6a143hy3mdzkdw1hs93981manm7156j59k7lpkysmh5c")))

(define-public crate-autocxx-0.12.0 (c (n "autocxx") (v "0.12.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.12.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.12.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)))) (h "166prsk33c8ixxrrj4hghkkpmzdk4kr53g45myclfsp2ma219kvk")))

(define-public crate-autocxx-0.13.0 (c (n "autocxx") (v "0.13.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.13.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.13.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)))) (h "1mgdllxa5dmp44l4yv55s3dg92s3bfynzgr7srg68qsd9d89rakc")))

(define-public crate-autocxx-0.13.1 (c (n "autocxx") (v "0.13.1") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.13.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.13.1") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)))) (h "06ppw1pv7rf952f6hk1fqp6pagc9x57d8kdn386vswr2snza7c2g")))

(define-public crate-autocxx-0.13.2 (c (n "autocxx") (v "0.13.2") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.13.2") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.13.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)))) (h "08frhic6lvw6wmvvi8dk3q5n3ixfrngki4ykpb7l6gbsm36vzzk0")))

(define-public crate-autocxx-0.14.0 (c (n "autocxx") (v "0.14.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.14.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.14.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)))) (h "0immfq5k63mzi3dy3qksg39zky0vnzgbkyvq6qzd4md14i8plz3r")))

(define-public crate-autocxx-0.15.0 (c (n "autocxx") (v "0.15.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.15.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.15.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.3") (d #t) (k 0)))) (h "1srlv1n62xmhk31hkx7r0rm176z23hyd5mr9czyh1zmsvh70rxyi")))

(define-public crate-autocxx-0.16.0 (c (n "autocxx") (v "0.16.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.16.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.16.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.4") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1md7nsnkvlhm96bigajpdx1j50w503w4wxl3bf6bwj2vp563irhk")))

(define-public crate-autocxx-0.17.0 (c (n "autocxx") (v "0.17.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-engine") (r "^0.17.0") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.17.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.4") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1wbhqnljvxv653ny3zgsrgaq8kcmxgfwydv1mi4drvpl0qj92da9")))

(define-public crate-autocxx-0.17.1 (c (n "autocxx") (v "0.17.1") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.17.1") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.4") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1chsw4wwx0sgsimiqh30qh1h4507hqq55r0zmrlpykn3fwccq7js")))

(define-public crate-autocxx-0.17.2 (c (n "autocxx") (v "0.17.2") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.17.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.4") (f (quote ("cxx"))) (d #t) (k 0)))) (h "0zii9j0id7f7bi41844ha10p38s39a3nkrbri0j2p0mj5b3lzc9v")))

(define-public crate-autocxx-0.17.3 (c (n "autocxx") (v "0.17.3") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.17.3") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.4") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1h1yl77wi1czxw39vggz13zhjh77wm2l3qzzkkbhny625rjz5pxp")))

(define-public crate-autocxx-0.17.4 (c (n "autocxx") (v "0.17.4") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.17.4") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.4") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1rinyj23nbbgfhvk1h45x3mh60jkk1vzsy472rh66zan0q6zai3h")))

(define-public crate-autocxx-0.17.5 (c (n "autocxx") (v "0.17.5") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.17.5") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.4") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1fx6k75468yrq4x4fnplza8fnvjva2mqd9af9wyjb1d60a70931z")))

(define-public crate-autocxx-0.18.0 (c (n "autocxx") (v "0.18.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.18.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1a9xq2wlkrs6i4i2k7n1pkih4qjwaj8a0jf60m4zkcsjs0sha9dk")))

(define-public crate-autocxx-0.19.0 (c (n "autocxx") (v "0.19.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.19.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1dx7v2g0m50vxb29rbjp6c7lzll8glg7v4lks8b4aiiymknz4nv7")))

(define-public crate-autocxx-0.19.1 (c (n "autocxx") (v "0.19.1") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.19.1") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1ljyywsqwj0grac7knj796ccsl18gp118y4h3khzw5cb7ag4px58")))

(define-public crate-autocxx-0.20.0 (c (n "autocxx") (v "0.20.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.20.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "056m9dri05rpp80msj3jzxxm8cw043y5kgql7g7klfa8680y75ww")))

(define-public crate-autocxx-0.20.1 (c (n "autocxx") (v "0.20.1") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.20.1") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "06pmncg4cjxdcviky9wki4kc7v70vm965w7m0lqnkrbkhgl3fzdb")))

(define-public crate-autocxx-0.21.0 (c (n "autocxx") (v "0.21.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.21.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "0kpkv0ibqqm2zg2qsi79xya9ldp09rx3s5ll2bh7baj8i7sbnb6y")))

(define-public crate-autocxx-0.21.1 (c (n "autocxx") (v "0.21.1") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.21.1") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "01g00di9bwwwmixrr9bmf1a6l95d7127k1d8p7ql058k66dhh300")))

(define-public crate-autocxx-0.21.2 (c (n "autocxx") (v "0.21.2") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.21.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "0wjcvpwy4g8r4lqdpnxclbkmdshx6allndska95nj1m1n5wr4jm8")))

(define-public crate-autocxx-0.22.0 (c (n "autocxx") (v "0.22.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.22.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "0dv4lz7llkk3h29mw73ccsy42bwl9qniv90vjanfc6kiyjr4lkaq")))

(define-public crate-autocxx-0.22.1 (c (n "autocxx") (v "0.22.1") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.22.1") (d #t) (k 0)) (d (n "cxx") (r "^1.0.68") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "0h2z0gafaa3izg211p6fv1q9p28kiqm7k765nlrdvkwnqrfhipkl")))

(define-public crate-autocxx-0.22.2 (c (n "autocxx") (v "0.22.2") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.22.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.68") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "0pnyvdhrq791pcin1lf4hpzqqr2lp4xhfjzaqk6z6rkidq8f5jsm")))

(define-public crate-autocxx-0.22.3 (c (n "autocxx") (v "0.22.3") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.22.3") (d #t) (k 0)) (d (n "cxx") (r "^1.0.68") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1znjf5dykvxw141w0yzx8sy90mdlk3l5pdxql0wfy4k3d8qd5hbl")))

(define-public crate-autocxx-0.22.4 (c (n "autocxx") (v "0.22.4") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.22.4") (d #t) (k 0)) (d (n "cxx") (r "^1.0.68") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "0rwf0ywdy16r7l5vyn1kqcj5p01kip2w2wq1nr6l39msa2q8525d")))

(define-public crate-autocxx-0.23.0 (c (n "autocxx") (v "0.23.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.23.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "0i6rp5wm3m32jnv1qr5kn095rv6mkszics5rkra7p9277qi4hfil")))

(define-public crate-autocxx-0.23.1 (c (n "autocxx") (v "0.23.1") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.23.1") (d #t) (k 0)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "02q0aw6wg9ah8ssaany89cfpy4r39pcnw5sv0a74mzxybry29hg0")))

(define-public crate-autocxx-0.24.0 (c (n "autocxx") (v "0.24.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.24.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "09l0k8d3hcvlacpy55n20hjndvdbg1jrbzjp68j945v0vzd22djc")))

(define-public crate-autocxx-0.25.0 (c (n "autocxx") (v "0.25.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.25.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "moveit") (r "^0.5") (f (quote ("cxx"))) (d #t) (k 0)))) (h "12ivpljxvarp0v3qmyvszniqjacwh8ijaii423qi2krqm05wh6cs")))

(define-public crate-autocxx-0.26.0 (c (n "autocxx") (v "0.26.0") (d (list (d (n "aquamarine") (r "^0.1") (d #t) (k 0)) (d (n "autocxx-macro") (r "^0.26.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "moveit") (r "^0.6") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1p27mf7rjk26sfni3yzlabpbrbj83fwmmm2385r0k3zx7v9lv9hv")))

