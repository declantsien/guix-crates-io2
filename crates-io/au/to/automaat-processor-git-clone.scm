(define-module (crates-io au to automaat-processor-git-clone) #:use-module (crates-io))

(define-public crate-automaat-processor-git-clone-0.1.0 (c (n "automaat-processor-git-clone") (v "0.1.0") (d (list (d (n "automaat-core") (r "^0.1") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "juniper") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "juniper") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0ib7jly2jlrny7i78d37zpq86vz2bp0fjdc8ix6pgbdz2mbp1yq4")))

