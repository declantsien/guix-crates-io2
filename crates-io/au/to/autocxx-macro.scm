(define-module (crates-io au to autocxx-macro) #:use-module (crates-io))

(define-public crate-autocxx-macro-0.4.0 (c (n "autocxx-macro") (v "0.4.0") (d (list (d (n "autocxx-engine") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vvidhlb7nfmp8ss4800616ajq1hwqab96i78jg6r6qmdlj5wzxg")))

(define-public crate-autocxx-macro-0.4.1 (c (n "autocxx-macro") (v "0.4.1") (d (list (d (n "autocxx-engine") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aifjfmphnm0cpxb55gvrww9m9bvp810yvd4g9dipifzbr9g44y8")))

(define-public crate-autocxx-macro-0.5.0 (c (n "autocxx-macro") (v "0.5.0") (d (list (d (n "autocxx-engine") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "022i5ylbsmr7mwa2ya0284gi6p3c380iykhaccf3pjz6v1c3ys9d")))

(define-public crate-autocxx-macro-0.5.1 (c (n "autocxx-macro") (v "0.5.1") (d (list (d (n "autocxx-parser") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w3is9ddgvqavg22sacfds5l6kf5h0jnch1ywvccqln9zn0ffnx9")))

(define-public crate-autocxx-macro-0.5.2 (c (n "autocxx-macro") (v "0.5.2") (d (list (d (n "autocxx-parser") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ydql1hr437rrbm9ihxa9a1acaynmxgkq5iadr6k4wlh9fm8w9l6")))

(define-public crate-autocxx-macro-0.5.3 (c (n "autocxx-macro") (v "0.5.3") (d (list (d (n "autocxx-parser") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lkddjspmmkwvdm699awcy8fhcbj5yc3f2jqxijxwdk8j9sxp8hz")))

(define-public crate-autocxx-macro-0.5.4 (c (n "autocxx-macro") (v "0.5.4") (d (list (d (n "autocxx-parser") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "135alggh3idwkzyyh01niwqnbc6g1n95014b4d5xbygxj9vif2k2")))

(define-public crate-autocxx-macro-0.6.0 (c (n "autocxx-macro") (v "0.6.0") (d (list (d (n "autocxx-parser") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kc1bbmf7fx1gvwfsj5n5jm98lwvd2hh83ilzrjxn0bw5jhn3aky")))

(define-public crate-autocxx-macro-0.7.0 (c (n "autocxx-macro") (v "0.7.0") (d (list (d (n "autocxx-parser") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08n5kh488xjc5zs16h8dn69mwsbn7qy8f5gwnb97gkizvr2mqihk")))

(define-public crate-autocxx-macro-0.7.1 (c (n "autocxx-macro") (v "0.7.1") (d (list (d (n "autocxx-parser") (r "^0.7.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rdapca87jrmbrppxyjgwkggcj3ki30k3xqvcwpmqd5g0m2sn6bh")))

(define-public crate-autocxx-macro-0.8.0 (c (n "autocxx-macro") (v "0.8.0") (d (list (d (n "autocxx-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03jsw8yxmma12bdkcqg2r09yf1133qh2mj942wbd7vlrrl8nsnqs")))

(define-public crate-autocxx-macro-0.9.0 (c (n "autocxx-macro") (v "0.9.0") (d (list (d (n "autocxx-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a97qs8khjahhb4s646zbk75ysqafpf0mnhk48si14yc00fbcbaz")))

(define-public crate-autocxx-macro-0.10.0 (c (n "autocxx-macro") (v "0.10.0") (d (list (d (n "autocxx-parser") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pxg0pvf7qydb88drm4aa1gmmxc0k6sjpy5w3s9c95nhz9wwcd0h")))

(define-public crate-autocxx-macro-0.11.0 (c (n "autocxx-macro") (v "0.11.0") (d (list (d (n "autocxx-parser") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13r5jmb1mvi17rk869gzn36v7rz7nbzgl3xhkbyxlmkgpjmcxwy5")))

(define-public crate-autocxx-macro-0.11.1 (c (n "autocxx-macro") (v "0.11.1") (d (list (d (n "autocxx-parser") (r "^0.11.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j8c41nrw8k0p2lg6sb8611y7g0i636vsf93i596xkhv55kkcf10")))

(define-public crate-autocxx-macro-0.11.2 (c (n "autocxx-macro") (v "0.11.2") (d (list (d (n "autocxx-parser") (r "^0.11.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xxgi5srmbm87v1lxn3f2cx08sffr8hlfikxfrqibzbgf45w6i0r")))

(define-public crate-autocxx-macro-0.12.0 (c (n "autocxx-macro") (v "0.12.0") (d (list (d (n "autocxx-parser") (r "=0.12.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s1is8bg23ggpkjzjmwr9m98l57l4bq1sqp03bma5bn0032waw0y")))

(define-public crate-autocxx-macro-0.13.0 (c (n "autocxx-macro") (v "0.13.0") (d (list (d (n "autocxx-parser") (r "=0.13.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k79fqvmlhwzds4v5wkiyi8zm3znm57hq97cbk97x9qqpsc09hkq")))

(define-public crate-autocxx-macro-0.13.1 (c (n "autocxx-macro") (v "0.13.1") (d (list (d (n "autocxx-parser") (r "=0.13.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kxpblzcvz01iww67mvcf0db2j31nxa72cjhsbcm1jm9577lhar5")))

(define-public crate-autocxx-macro-0.13.2 (c (n "autocxx-macro") (v "0.13.2") (d (list (d (n "autocxx-parser") (r "=0.13.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1shidb3fcxb8dyhm61y8fm1gqa4s877kgkjdry006hia8bns1li7")))

(define-public crate-autocxx-macro-0.14.0 (c (n "autocxx-macro") (v "0.14.0") (d (list (d (n "autocxx-parser") (r "=0.14.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fnprhhq6x9mxfk786kbzjv6q41r0k0hyymisdykp67pbcay3ydz")))

(define-public crate-autocxx-macro-0.15.0 (c (n "autocxx-macro") (v "0.15.0") (d (list (d (n "autocxx-parser") (r "=0.15.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "187vd9ap6lg3k6y4250zwmfkvcvgnpxb75l52mrwbnq34f9z1lxb")))

(define-public crate-autocxx-macro-0.16.0 (c (n "autocxx-macro") (v "0.16.0") (d (list (d (n "autocxx-parser") (r "=0.16.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05amhw8in7f341qwwj0pqiiz86xq0rbf1whihccdyvmygi3wl1l1")))

(define-public crate-autocxx-macro-0.17.0 (c (n "autocxx-macro") (v "0.17.0") (d (list (d (n "autocxx-parser") (r "=0.17.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pgy3q07iy8a9rxj6k289hx2ndb3n3y4b3gldsi5bshzglvgsyyw")))

(define-public crate-autocxx-macro-0.17.1 (c (n "autocxx-macro") (v "0.17.1") (d (list (d (n "autocxx-parser") (r "=0.17.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13wgmlxylwg8clz0lg0f67alcmf5r2s6r4vp0xkrs9lkwdjbr5xb")))

(define-public crate-autocxx-macro-0.17.2 (c (n "autocxx-macro") (v "0.17.2") (d (list (d (n "autocxx-parser") (r "=0.17.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q7i18gc6nflcv5nicb9ak5g906h3sqnd5r0n0fkmmx470asp0kb")))

(define-public crate-autocxx-macro-0.17.3 (c (n "autocxx-macro") (v "0.17.3") (d (list (d (n "autocxx-parser") (r "=0.17.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lnp6r436qrhp9r7xiv3pwici9r6flp4rwh153q9hgmq46blraa1")))

(define-public crate-autocxx-macro-0.17.4 (c (n "autocxx-macro") (v "0.17.4") (d (list (d (n "autocxx-parser") (r "=0.17.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04f8irhm008lr4zj8rjc06264h40jph81drzd05y6fx9bngvpzx2")))

(define-public crate-autocxx-macro-0.17.5 (c (n "autocxx-macro") (v "0.17.5") (d (list (d (n "autocxx-parser") (r "=0.17.5") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dzrcrygnsv29zjz9hvx9g9pabr37q38wvaam9848f6x39l6vy72")))

(define-public crate-autocxx-macro-0.18.0 (c (n "autocxx-macro") (v "0.18.0") (d (list (d (n "autocxx-parser") (r "=0.18.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0frrqdmc7ikbp0qbgngwjd239dkh3m9c7nzklg8hndrgak6j0skx")))

(define-public crate-autocxx-macro-0.19.0 (c (n "autocxx-macro") (v "0.19.0") (d (list (d (n "autocxx-parser") (r "=0.19.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08vcjl7bdzrwmz77axdzdfp067d9r39d474kgna4j1innp0gpl0n")))

(define-public crate-autocxx-macro-0.19.1 (c (n "autocxx-macro") (v "0.19.1") (d (list (d (n "autocxx-parser") (r "=0.19.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q0c52in0qm3ixmlgkn0ff2r1s6gi5yvfcq76g60hfw2k0g78idc")))

(define-public crate-autocxx-macro-0.20.0 (c (n "autocxx-macro") (v "0.20.0") (d (list (d (n "autocxx-parser") (r "=0.20.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12sji94a82w6aqksm6ra8ginbll8hyhvsd02fk8795j7c03imj21")))

(define-public crate-autocxx-macro-0.20.1 (c (n "autocxx-macro") (v "0.20.1") (d (list (d (n "autocxx-parser") (r "=0.20.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00nj9zjz7w20vx19r4q0rny4z79agday7r8vl3nn57bd2v93k8x2")))

(define-public crate-autocxx-macro-0.21.0 (c (n "autocxx-macro") (v "0.21.0") (d (list (d (n "autocxx-parser") (r "=0.21.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0czq8zz3fsldq7xwyr974nwwsn4zfraz1x5v69f82bvhrmiqrhi9")))

(define-public crate-autocxx-macro-0.21.1 (c (n "autocxx-macro") (v "0.21.1") (d (list (d (n "autocxx-parser") (r "=0.21.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m8f5cwy1zq9w294gn26nv641f99h7pyn0i8h6mlfc8yvy8k71md")))

(define-public crate-autocxx-macro-0.21.2 (c (n "autocxx-macro") (v "0.21.2") (d (list (d (n "autocxx-parser") (r "=0.21.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pl8x5j4i89513241lywdmz26mnllplazqdp6fvxi83r67sj85qb")))

(define-public crate-autocxx-macro-0.22.0 (c (n "autocxx-macro") (v "0.22.0") (d (list (d (n "autocxx-parser") (r "=0.22.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fclzxj6828kijw5hjr8ghl51yjx72n6aqhl6sl4pb58i2k76qkf")))

(define-public crate-autocxx-macro-0.22.1 (c (n "autocxx-macro") (v "0.22.1") (d (list (d (n "autocxx-parser") (r "=0.22.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lb44wmjxf80md16nr13w8gfbli7k8k78k6kb6skwnglygmic19l")))

(define-public crate-autocxx-macro-0.22.2 (c (n "autocxx-macro") (v "0.22.2") (d (list (d (n "autocxx-parser") (r "=0.22.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dcirw757w1incqv2fbbqx1nrgwlv3xl6x8wb9afd6mfprq06n32")))

(define-public crate-autocxx-macro-0.22.3 (c (n "autocxx-macro") (v "0.22.3") (d (list (d (n "autocxx-parser") (r "=0.22.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gpnnz01hqwscx03hhxk0wmr0qbnvhwiyaaxkgvilrpb8r2lgycb")))

(define-public crate-autocxx-macro-0.22.4 (c (n "autocxx-macro") (v "0.22.4") (d (list (d (n "autocxx-parser") (r "=0.22.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19ry503chjw14zp28xk1pwvjyqcda75slfbwvqz4vf6vvq6fzxqp")))

(define-public crate-autocxx-macro-0.23.0 (c (n "autocxx-macro") (v "0.23.0") (d (list (d (n "autocxx-parser") (r "=0.23.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02p61aznad6cg8g2zs4ac2pn5y6jk9a2gwjc1p0fljg377zwv2w5")))

(define-public crate-autocxx-macro-0.23.1 (c (n "autocxx-macro") (v "0.23.1") (d (list (d (n "autocxx-parser") (r "=0.23.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12kwqvhs8dcnrbx4bl8xs3p4qmxqm75pjn5jx67cla0qf3sybz4w")))

(define-public crate-autocxx-macro-0.24.0 (c (n "autocxx-macro") (v "0.24.0") (d (list (d (n "autocxx-parser") (r "=0.24.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dkzxxica2yx0dwr7czsngnbxp0v9jssngbzyd6gap2xghxgy8k0")))

(define-public crate-autocxx-macro-0.25.0 (c (n "autocxx-macro") (v "0.25.0") (d (list (d (n "autocxx-parser") (r "=0.25.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kvppkkjwcdgw44cvqmp6dmvhk4fsydls9a61gw7q0byrlizh4vk")))

(define-public crate-autocxx-macro-0.26.0 (c (n "autocxx-macro") (v "0.26.0") (d (list (d (n "autocxx-parser") (r "=0.26.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "103xqi0qs5zqp5v6viw8qm873bplyic0kd72wwffpdihs1l4wncf")))

