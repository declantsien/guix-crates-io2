(define-module (crates-io au to autogui) #:use-module (crates-io))

(define-public crate-autogui-0.1.0 (c (n "autogui") (v "0.1.0") (d (list (d (n "core-graphics") (r "^0.13.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "066kx7dvaw8arn5rdbx6f31ljhbhd8gzyz0iyrqjwpqgh0d6gr2y")))

(define-public crate-autogui-0.2.0 (c (n "autogui") (v "0.2.0") (d (list (d (n "core-foundation") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "core-graphics") (r "^0.13.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w26r4rzqv5pvskpscxz5s2ammrw203df2lp27ww2mmx408wdn75")))

(define-public crate-autogui-0.3.0 (c (n "autogui") (v "0.3.0") (d (list (d (n "core-foundation") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "core-graphics") (r "^0.13.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dsdsw1z34wjwkrfprq23f5jwig1mjnrasckycdngwh3qwbfcazh")))

(define-public crate-autogui-0.3.1 (c (n "autogui") (v "0.3.1") (d (list (d (n "core-foundation") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "core-graphics") (r "^0.13.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1swm7ybm6dfcgsq3c1p1fmgw1rsbmpf8z64zhpfw3w88bs4gbz7w")))

(define-public crate-autogui-0.3.2 (c (n "autogui") (v "0.3.2") (d (list (d (n "core-foundation") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "core-graphics") (r "^0.13.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zydp4xll88c4wabc2pybm7fvsvcxcrmn91yv8dm6cvl050z3mss")))

(define-public crate-autogui-0.4.0 (c (n "autogui") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "core-graphics") (r "^0.13.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0fpz2jcvnqxrlvyl9hiibkn21jw3r8ni9jnrp6kbmlq54qh4v0ix")))

(define-public crate-autogui-0.4.1 (c (n "autogui") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "core-graphics") (r "^0.13.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "05z7yqc25dbqgwrw7jmxn4dv2ncm9zwxzzb3583nm61j2iaagzzd")))

