(define-module (crates-io au to autoconf_derive) #:use-module (crates-io))

(define-public crate-autoconf_derive-0.1.0 (c (n "autoconf_derive") (v "0.1.0") (d (list (d (n "autoconf_core") (r "^0.1.0") (d #t) (k 0)) (d (n "config") (r "^0.14.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0dmpd3glb22d4v9h8gprfj9zzmii8mfcd84pps9vic160m2gmk0w") (y #t)))

