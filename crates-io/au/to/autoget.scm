(define-module (crates-io au to autoget) #:use-module (crates-io))

(define-public crate-autoget-0.1.0 (c (n "autoget") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0yvwjjhh2vnk9qir1m80lsv26wc45ayby4lhh74m8c347fqcnhpf")))

(define-public crate-autoget-0.1.1 (c (n "autoget") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1x1vk09f73ch8sk8f7jah4a4fx3334wf96s8w91nfasi4f62y5gw")))

(define-public crate-autoget-0.1.2 (c (n "autoget") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "135q92mgajhm6cgy7b6c8njb922x4by1ngzr3dc20ayqcynlpgwl")))

