(define-module (crates-io au to autodiff_derive) #:use-module (crates-io))

(define-public crate-autodiff_derive-0.1.0 (c (n "autodiff_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (d #t) (k 0)))) (h "1fwr1ab50bqh4cykswf1vml7cbg0sfif4rxj7b046x9yjxp1j731")))

(define-public crate-autodiff_derive-0.1.1 (c (n "autodiff_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (d #t) (k 0)))) (h "0pwvxqwci2cippf19q6hwlvfmkqh1yik54j3zjphiynbf3nj5wa4")))

