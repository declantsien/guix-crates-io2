(define-module (crates-io au to automod) #:use-module (crates-io))

(define-public crate-automod-0.1.0 (c (n "automod") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0788sdwc08zjrk7r04zgrfqv5b2wfknp66fgph3vlc7aq1zfvi56")))

(define-public crate-automod-0.1.1 (c (n "automod") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0pld582piq2d55z0j96zcs8izw3ml46f8h9y7sdyxg093yfvxl2h")))

(define-public crate-automod-0.1.2 (c (n "automod") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17am5i7z7jpsrq9bm0wyhf4q9850g2kqvzl3ik900x5gc7brwv2a")))

(define-public crate-automod-0.2.0 (c (n "automod") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13r6a3gim9dl7pda4x790v8lv3ck9d01sjbvn406sfy9a7l95ian")))

(define-public crate-automod-0.2.1 (c (n "automod") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i0lsa6cfcmskbyf7s789sp82jv2skqcy20cf3rgshr5pgq4qyj1")))

(define-public crate-automod-0.2.2 (c (n "automod") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v7akanp8mli0apzifqkbj50r7a9mcasx6zvv4mjyr0jgmnih09z")))

(define-public crate-automod-1.0.0 (c (n "automod") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z8kdbvvz0k8mfs45mvs16lr9xj59cdcp0sm45fawfh93gai4mhg")))

(define-public crate-automod-1.0.1 (c (n "automod") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kx2819lp0s6alc1z5ch8s15s37gmhzpa4lgck42s42562bs24ld")))

(define-public crate-automod-1.0.2 (c (n "automod") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "033x62yain234hb072n3j1jhkd83lqcvhajwmpcgaqhrpj2r1617")))

(define-public crate-automod-1.0.3 (c (n "automod") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k9hvdlnh6ymdafqsv6xn3z589j5kmzjg55ahjrvyb6yzi1g4zhi") (r "1.31")))

(define-public crate-automod-1.0.4 (c (n "automod") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kiwsiy8kpzi0r5x89ns7vpzaqm3q2wimgh17qg6gvhq9hbarzm7") (r "1.31")))

(define-public crate-automod-1.0.5 (c (n "automod") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05xkqzabv0pgn4vkp3p2yv97wp6z7gxnnqhf0ilg60ygc67c0r1z") (r "1.31")))

(define-public crate-automod-1.0.6 (c (n "automod") (v "1.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xl4a2m6mlb2dxkiz5y3fyrq198911brllv4gss3yqbrqgh2di3j") (r "1.31")))

(define-public crate-automod-1.0.7 (c (n "automod") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dv8hrcrhvysjk0hhrfblybmwrpf58jr7h3j4qqdjvxk9af6idjs") (r "1.31")))

(define-public crate-automod-1.0.8 (c (n "automod") (v "1.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1mj2x5k2pcz0r3r9cq3cia30f8b9jx76rabp89a52wi0163yq818") (r "1.56")))

(define-public crate-automod-1.0.9 (c (n "automod") (v "1.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1dzrlfak5zjdq4m14djkcmjkq8cr2g57cksvzw2j23krz0x7704j") (r "1.56")))

(define-public crate-automod-1.0.10 (c (n "automod") (v "1.0.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1wh0sznf8fj623cg73xq96s1fq0xxlb9j9jj486ym0wrapfwwsl5") (r "1.56")))

(define-public crate-automod-1.0.11 (c (n "automod") (v "1.0.11") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "057sa45859nb8arbshkqc6va8b8jf5a8vx6zr739viibqbj989md") (r "1.56")))

(define-public crate-automod-1.0.12 (c (n "automod") (v "1.0.12") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1d03yi8zqhkhn61y6ix45np35d9vi0yslb9hc5nrk9kh71gvm8kv") (r "1.56")))

(define-public crate-automod-1.0.13 (c (n "automod") (v "1.0.13") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "065csmhzr7m93k9577kvm2kmd0hm4c26sx6yz01dm7darn334rv8") (r "1.56")))

(define-public crate-automod-1.0.14 (c (n "automod") (v "1.0.14") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "12rsa5barxi8v916hlvvpjyh43y5x2yjc2bg1xs6v960vccyxwzd") (r "1.56")))

