(define-module (crates-io au to autodiff_injector) #:use-module (crates-io))

(define-public crate-autodiff_injector-0.0.1 (c (n "autodiff_injector") (v "0.0.1") (d (list (d (n "autodiff_injector_core") (r "^0.0.1") (d #t) (k 0)) (d (n "autodiff_injector_core_derive") (r "^0.0.1") (d #t) (k 0)))) (h "0qlnbqybjgb26n2842249sqwcn2jw3cdnk5f8mnaq8acvc690fzs")))

