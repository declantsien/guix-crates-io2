(define-module (crates-io au to autofolder) #:use-module (crates-io))

(define-public crate-autofolder-0.1.0 (c (n "autofolder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)))) (h "1wanhpsjczsf62i0cdj6slr63gc0lya8yj14j4l24hf86wslm52c")))

(define-public crate-autofolder-0.2.0 (c (n "autofolder") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)))) (h "162nhf5pvs9ncj5wgqrpfcwbwldn5sh26jylxq4sqdbsbkvlnpc1")))

(define-public crate-autofolder-0.3.0 (c (n "autofolder") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)))) (h "0vjp2hn0k54znnspzadjsvb861q1312qnp42ds6phwgjmcq7r2hn")))

(define-public crate-autofolder-0.4.0 (c (n "autofolder") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)))) (h "0x1jkapw75ijd94mk3fdahwxf8im9n2g1pqrab29q7vqn6q6cysf")))

(define-public crate-autofolder-0.5.0 (c (n "autofolder") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)))) (h "0ji7igznj23wmbqv1g4l1h162s5dp0hg96yfqxwx4rfvjaj00l2g")))

