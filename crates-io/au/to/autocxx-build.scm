(define-module (crates-io au to autocxx-build) #:use-module (crates-io))

(define-public crate-autocxx-build-0.1.0 (c (n "autocxx-build") (v "0.1.0") (d (list (d (n "autocxx-engine") (r "^0.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "19jl1cvfr1vsid8d75jx82qhyryblnv96xdkwv0zpjlcy8nswznb")))

(define-public crate-autocxx-build-0.2.0 (c (n "autocxx-build") (v "0.2.0") (d (list (d (n "autocxx-engine") (r "^0.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0g6awh552x0ks2r1rr3lwqxgniinwjggb7gv6nzjqjbk3r90q5dr")))

(define-public crate-autocxx-build-0.3.0 (c (n "autocxx-build") (v "0.3.0") (d (list (d (n "autocxx-engine") (r "^0.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "03sgyv6b298qcrfk6m642r6x1c956l68dz2afbqqaywqcgxf0wrp")))

(define-public crate-autocxx-build-0.3.1 (c (n "autocxx-build") (v "0.3.1") (d (list (d (n "autocxx-engine") (r "^0.3.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0j9q2ag71xy84bj5rv33fi71nzkj12wmhjp2h9h9fdfbjy92xqda")))

(define-public crate-autocxx-build-0.3.2 (c (n "autocxx-build") (v "0.3.2") (d (list (d (n "autocxx-engine") (r "^0.3.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1715jqb7dnqclx80hv4yjpq5lmx74zmc0wz5qdbm6s2l9z6b8dlq")))

(define-public crate-autocxx-build-0.3.3 (c (n "autocxx-build") (v "0.3.3") (d (list (d (n "autocxx-engine") (r "^0.3.3") (f (quote ("build"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07m2pmzzvjhmk8anbksc4b2pd2xl3v9g0lyhpgmn6hyl6did386h")))

(define-public crate-autocxx-build-0.4.0 (c (n "autocxx-build") (v "0.4.0") (d (list (d (n "autocxx-engine") (r "^0.4.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i6dqlxrnr03gk9lxcdwfbf4acj66kl4s0xb8684mnx5sp9rq9i3")))

(define-public crate-autocxx-build-0.4.1 (c (n "autocxx-build") (v "0.4.1") (d (list (d (n "autocxx-engine") (r "^0.4.1") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lqbajibj2phjdswz1mwvf08i6mivnglvkxdii4vd06611vm9qmb")))

(define-public crate-autocxx-build-0.5.0 (c (n "autocxx-build") (v "0.5.0") (d (list (d (n "autocxx-engine") (r "^0.5.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zc5kqx94agwsbvhax43468frya83q2806y5d0mlfh86rjdv2cq3")))

(define-public crate-autocxx-build-0.5.1 (c (n "autocxx-build") (v "0.5.1") (d (list (d (n "autocxx-engine") (r "^0.5.1") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y8gnb7v33ijn1iai0fj4iyx18cjijxhawhmvdc79dw5fxlsl574")))

(define-public crate-autocxx-build-0.5.2 (c (n "autocxx-build") (v "0.5.2") (d (list (d (n "autocxx-engine") (r "^0.5.2") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1763pc6h5ndpifir49gqqhc4vvrvxq9dd2cjb2wiw13cwi7097lz") (f (quote (("pointers" "autocxx-engine/pointers"))))))

(define-public crate-autocxx-build-0.5.3 (c (n "autocxx-build") (v "0.5.3") (d (list (d (n "autocxx-engine") (r "^0.5.3") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fchv64m9z4gnzyi2dinf6rrzzx39sypznp2r017k1rfpyfd0q6b")))

(define-public crate-autocxx-build-0.5.4 (c (n "autocxx-build") (v "0.5.4") (d (list (d (n "autocxx-engine") (r "^0.5.4") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d9wrfg81ykd6n00kpc5ly7r54njqklisqlg77f19bnkx9ndgl49")))

(define-public crate-autocxx-build-0.6.0 (c (n "autocxx-build") (v "0.6.0") (d (list (d (n "autocxx-engine") (r "^0.6.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g3y0w7w5wlckh3dq3kchy7kg8cb6fc9kvg6n55ky408dv86rmws")))

(define-public crate-autocxx-build-0.7.0 (c (n "autocxx-build") (v "0.7.0") (d (list (d (n "autocxx-engine") (r "^0.7.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vxvbwl44103anm157qhq1xyk2naw71w3qjdl748m4ywnvp88143")))

(define-public crate-autocxx-build-0.7.1 (c (n "autocxx-build") (v "0.7.1") (d (list (d (n "autocxx-engine") (r "^0.7.1") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cyp2z86sz23418ij7wgfq08dhfwr7gw3ardhj603iis9h0lqxzq")))

(define-public crate-autocxx-build-0.8.0 (c (n "autocxx-build") (v "0.8.0") (d (list (d (n "autocxx-engine") (r "^0.8.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0csw0v4jc79k6hfygfc68rs36kkjqi75ys948ja18g8a86lqvcli")))

(define-public crate-autocxx-build-0.10.0 (c (n "autocxx-build") (v "0.10.0") (d (list (d (n "autocxx-engine") (r "^0.10.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g7qvh69w1fnn5dwm637dissjvpw2lqb53w4bhdl56jaq11ihczp")))

(define-public crate-autocxx-build-0.11.0 (c (n "autocxx-build") (v "0.11.0") (d (list (d (n "autocxx-engine") (r "^0.11.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "092shkj4iqlp39m57790vbq9f6c9cn66y1qcw165mvb41s20c8q1")))

(define-public crate-autocxx-build-0.11.1 (c (n "autocxx-build") (v "0.11.1") (d (list (d (n "autocxx-engine") (r "^0.11.1") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06pq6ak4p1z8s2myyzmllfj1cyjq3qaa69gd5zd4gzwjx930gkqc")))

(define-public crate-autocxx-build-0.11.2 (c (n "autocxx-build") (v "0.11.2") (d (list (d (n "autocxx-engine") (r "^0.11.2") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r1s91gh7ikx2xhc5nskykskxhrmhcgkyjcyzp9ahh9iy0lqpaps")))

(define-public crate-autocxx-build-0.12.0 (c (n "autocxx-build") (v "0.12.0") (d (list (d (n "autocxx-engine") (r "=0.12.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wdh1638llmnay0nw8pz7cp92cqj9bhkrvkb1zbdlk6rhfbll7nq")))

(define-public crate-autocxx-build-0.13.1 (c (n "autocxx-build") (v "0.13.1") (d (list (d (n "autocxx-engine") (r "=0.13.1") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gsgr0q7x0bhr2aqm5nsypf3abp9q82xdj2pf965qbrin9wzlp31")))

(define-public crate-autocxx-build-0.13.2 (c (n "autocxx-build") (v "0.13.2") (d (list (d (n "autocxx-engine") (r "=0.13.2") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vzvhmf0grsnkfryfxc7z881m40pbc613g2fq2yvv0zm89whccsr")))

(define-public crate-autocxx-build-0.14.0 (c (n "autocxx-build") (v "0.14.0") (d (list (d (n "autocxx-engine") (r "=0.14.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c5fdklr2x45knbyvwnh18xrzlq8gd8djphzy7ngl16w09gyh0gh")))

(define-public crate-autocxx-build-0.15.0 (c (n "autocxx-build") (v "0.15.0") (d (list (d (n "autocxx-engine") (r "=0.15.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mq74zill8sc4i6xyarajmn8hsf6jz0vcxizlwwzz16bcwhs3i7s") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.16.0 (c (n "autocxx-build") (v "0.16.0") (d (list (d (n "autocxx-engine") (r "=0.16.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cvakck62yg2zxzfykgc8q87jk75mc8zvd6qd2nd6wgyli2mnpsa") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.17.0 (c (n "autocxx-build") (v "0.17.0") (d (list (d (n "autocxx-engine") (r "=0.17.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02am1nlj7nkyblkdhx6msvmzi3grs9vyca07g6x22jzwf6cr2kq7") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.17.1 (c (n "autocxx-build") (v "0.17.1") (d (list (d (n "autocxx-engine") (r "=0.17.1") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fys9xhlmm164hgxq11414alv03g7klw1m6d5bq8iw34g78sj5n1") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.17.2 (c (n "autocxx-build") (v "0.17.2") (d (list (d (n "autocxx-engine") (r "=0.17.2") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y9b8fskxvxw4im7i5rz1wqv1lh26v0ksifgv3j3l6q2fas4adz1") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.17.3 (c (n "autocxx-build") (v "0.17.3") (d (list (d (n "autocxx-engine") (r "=0.17.3") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mlnvslr80wz6ma0a5spsbj54fa4hr7rh4aqncnyr04h43y8x6pc") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.17.4 (c (n "autocxx-build") (v "0.17.4") (d (list (d (n "autocxx-engine") (r "=0.17.4") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00npma2fciba7f6v8zm48nn05sqjrrpbb9hvla3rqsn91aarcg9k") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.17.5 (c (n "autocxx-build") (v "0.17.5") (d (list (d (n "autocxx-engine") (r "=0.17.5") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1czr2b8lniwkwfznkk4178dkrx18py7n5anzg69am14dyd2dr0ys") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.18.0 (c (n "autocxx-build") (v "0.18.0") (d (list (d (n "autocxx-engine") (r "=0.18.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0szvxv8lcn84ryr5cy9k9qjfyxv27nxxvphjphxnh6xr2dgish32") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.19.0 (c (n "autocxx-build") (v "0.19.0") (d (list (d (n "autocxx-engine") (r "=0.19.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14mzz2qr5fwrsx00xzaqsxy28qnninpsw9sk852wgqzzlv173s45") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.19.1 (c (n "autocxx-build") (v "0.19.1") (d (list (d (n "autocxx-engine") (r "=0.19.1") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hvj2mlzf1mz1a69v557pggmfczb15f77sjbqvy9sms3af60n8qg") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.20.0 (c (n "autocxx-build") (v "0.20.0") (d (list (d (n "autocxx-engine") (r "=0.20.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15q7xjr68a5pgyxibg5ghzkxgp8nhgzss7fhyq1s4kai9249wq4n") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.20.1 (c (n "autocxx-build") (v "0.20.1") (d (list (d (n "autocxx-engine") (r "=0.20.1") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04d8pjbsn2sls75118sj00krfdmh62df49isi88k59qikcxsf9yv") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.21.0 (c (n "autocxx-build") (v "0.21.0") (d (list (d (n "autocxx-engine") (r "=0.21.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13q7cdnrhvlgdbjh20i1iygxld1a5whqzq0dyb7qr52a17h6rpnw") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.21.1 (c (n "autocxx-build") (v "0.21.1") (d (list (d (n "autocxx-engine") (r "=0.21.1") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c3vj53dhfdvnzniqkq6zrk6idq0i5w8vkkszsj2r72fihhn73w7") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.21.2 (c (n "autocxx-build") (v "0.21.2") (d (list (d (n "autocxx-engine") (r "=0.21.2") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s69wsaj28hnzq14svlrcjnp1lyx4bhychbwccfqlh1cibs7hxw0") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.22.0 (c (n "autocxx-build") (v "0.22.0") (d (list (d (n "autocxx-engine") (r "=0.22.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vsxh0p4m07nrzq2lwfqzsla479ffw6bzl54im3p6d4nxjb3s671") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.22.1 (c (n "autocxx-build") (v "0.22.1") (d (list (d (n "autocxx-engine") (r "=0.22.1") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dkz3isxa2883ixdrkhccpsj3pc610y4rck82426vqs37vz1r8rr") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.22.2 (c (n "autocxx-build") (v "0.22.2") (d (list (d (n "autocxx-engine") (r "=0.22.2") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1259djgb0y00729nhrralldxf681m8d7ky8mr416ra1f0rs0p8r5") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.22.3 (c (n "autocxx-build") (v "0.22.3") (d (list (d (n "autocxx-engine") (r "=0.22.3") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zx35rgifdfwd18qz77fx3bdqznh462ds6c7rsbdkxwzjmdlmb6m") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.22.4 (c (n "autocxx-build") (v "0.22.4") (d (list (d (n "autocxx-engine") (r "=0.22.4") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hjx572qgli159ps0ldfsvk036gxa1882r1ml3ca8ymirsgb102d") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.23.0 (c (n "autocxx-build") (v "0.23.0") (d (list (d (n "autocxx-engine") (r "=0.23.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "132gx7lyw3phlsgrjp512agx6jqypdjy1s77rgphna15vk535h9d") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.23.1 (c (n "autocxx-build") (v "0.23.1") (d (list (d (n "autocxx-engine") (r "=0.23.1") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ivqna66flshqp061nyqbsqwvkr40n35kd0gq9k80y205s6zq6xq") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.24.0 (c (n "autocxx-build") (v "0.24.0") (d (list (d (n "autocxx-engine") (r "=0.24.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15z26cn7bjpx64b94ggzy54rq71h6jb900qfk4dj9z7jfm2x8pnc") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.25.0 (c (n "autocxx-build") (v "0.25.0") (d (list (d (n "autocxx-engine") (r "=0.25.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "016qqbmyjxrix3kw9q8frw6idczibmwahyxdm84qs6iwlh3kr0ip") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

(define-public crate-autocxx-build-0.26.0 (c (n "autocxx-build") (v "0.26.0") (d (list (d (n "autocxx-engine") (r "=0.26.0") (f (quote ("build"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k9qdzbkq2lyd48r1ncshs6jrbag4dcl563lssjrrdv85lnn0plm") (f (quote (("static" "autocxx-engine/static") ("runtime" "autocxx-engine/runtime"))))))

