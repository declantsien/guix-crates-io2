(define-module (crates-io au to auto-check-rs) #:use-module (crates-io))

(define-public crate-auto-check-rs-0.1.0 (c (n "auto-check-rs") (v "0.1.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "14x4isn6hs9251gw6z9zzzi799kpdqv5zy2l9hwmb6jr3y2wxxcx")))

(define-public crate-auto-check-rs-0.1.1 (c (n "auto-check-rs") (v "0.1.1") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "1xk8abnnhyb5ir0q6qf9pq0yazd3fbi7h4j55abcpbwd5i026xxq")))

(define-public crate-auto-check-rs-0.2.0 (c (n "auto-check-rs") (v "0.2.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "1lispz598han1516ifbz6icb4qk4ks96l8613apqbrrp79mgvbg0")))

(define-public crate-auto-check-rs-0.2.1 (c (n "auto-check-rs") (v "0.2.1") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "1a72fcj5ky7b49pfss2ww1f709n0qpjmbfwyshyiyqyjxfv62jli")))

(define-public crate-auto-check-rs-0.3.0 (c (n "auto-check-rs") (v "0.3.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "1nkcj11i667jlz9001vyknm5wqigwsn3c255l2cfiy4sjz3wg4qy")))

(define-public crate-auto-check-rs-0.3.1 (c (n "auto-check-rs") (v "0.3.1") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "0ry12qjmzpmz9543d70r13z6qm6jlwha0gcdcmblmr5d2k6x8paw")))

(define-public crate-auto-check-rs-0.3.2 (c (n "auto-check-rs") (v "0.3.2") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "14dh93yxbxwmjv6kjycpgqlrakjfgx3kqkb9xk4a9jmf1pvd9qi9")))

(define-public crate-auto-check-rs-0.3.3 (c (n "auto-check-rs") (v "0.3.3") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "15r6aylw7xja62dvi50bfwjnkmk8ib93g5pgh04a5idhjagjpyd9")))

