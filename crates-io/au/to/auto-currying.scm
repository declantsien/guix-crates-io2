(define-module (crates-io au to auto-currying) #:use-module (crates-io))

(define-public crate-auto-currying-0.1.0 (c (n "auto-currying") (v "0.1.0") (h "09avhcjdh4fax704lizbkx4pfmpkm2w73hr402k6gpqayszrfdyq")))

(define-public crate-auto-currying-0.1.1 (c (n "auto-currying") (v "0.1.1") (h "1kici23mqbp8h0cwfq3lw4wn97hgxa23lyr8khcp8yzcn65gafvv")))

