(define-module (crates-io au to autorand-derive) #:use-module (crates-io))

(define-public crate-autorand-derive-0.1.1 (c (n "autorand-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0ihqxii0c4zwvvcni4kawbsq66apdr64390n27vlnyplsrj5z3z4")))

(define-public crate-autorand-derive-0.2.0 (c (n "autorand-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0k92lm6nm2kf9vj6ph5ycybd78ib2slgvl2mbs7g3rpyrn360vn7")))

(define-public crate-autorand-derive-0.2.1 (c (n "autorand-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0lfrc46m53s4j0iqypna50gz7ckmng7shwic6bpxs6b382kb5fak")))

(define-public crate-autorand-derive-0.2.2 (c (n "autorand-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1a3wpq66caq2src15s1vic24c6rhr62rn4fczqcvd28zqisrp8zw")))

