(define-module (crates-io au to autoproxy) #:use-module (crates-io))

(define-public crate-autoproxy-0.1.0 (c (n "autoproxy") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "14yd9c7wvivy4p06y7zskn85fnhiq4gv2lymldwdw5anlvajn0xn")))

