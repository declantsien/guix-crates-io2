(define-module (crates-io au to autotex) #:use-module (crates-io))

(define-public crate-autotex-1.2.0 (c (n "autotex") (v "1.2.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0chhkx6hf1w378hqf7bgsnz42c2n4i0ivsjm1rh0k3adj9b804bn") (y #t)))

(define-public crate-autotex-1.2.1 (c (n "autotex") (v "1.2.1") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0z7d00815a7zwlh4lskcflk8y3ki0xw43m9bwpmf1371kw6zyi7a")))

(define-public crate-autotex-1.2.2 (c (n "autotex") (v "1.2.2") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "12zd78ziv8i6khz4xcmx6zsm7vawswk0mss3nmvrvpbi3q4frdyy")))

(define-public crate-autotex-1.2.3 (c (n "autotex") (v "1.2.3") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "13pn07x7ahpld4q19gzvsapaf11byg8yipdaq2gm9ab0ykphlh3a")))

(define-public crate-autotex-1.3.0 (c (n "autotex") (v "1.3.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "powershell_script") (r "^1.1.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1hhmwhmzic5vira3lgb24r4sw04pa9hwimd9d27lnps0qzcg47q4")))

(define-public crate-autotex-1.4.0 (c (n "autotex") (v "1.4.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "powershell_script") (r "^1.1.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1vjz5jf11nik0221x64rcq2smg7ysn99b45j81xbgnzsmycqbvgm")))

