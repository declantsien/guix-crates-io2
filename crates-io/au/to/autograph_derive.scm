(define-module (crates-io au to autograph_derive) #:use-module (crates-io))

(define-public crate-autograph_derive-0.0.1 (c (n "autograph_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0anxx9y7avyx80iagllr59zhbf20j8s7cfcpg0yib4m14ki55gz0")))

(define-public crate-autograph_derive-0.1.0 (c (n "autograph_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "00q1ssll00303sss959xl758xhg54xgqkd75n9j0g365z8n5rlda")))

(define-public crate-autograph_derive-0.2.0 (c (n "autograph_derive") (v "0.2.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "01775vx4p6szw7g3b9xqc4dfhiybxfq1sbv9jyn1xwkan9pg2j1q")))

