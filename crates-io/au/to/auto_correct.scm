(define-module (crates-io au to auto_correct) #:use-module (crates-io))

(define-public crate-auto_correct-0.1.0 (c (n "auto_correct") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "threads_pool") (r "^0.1.0") (d #t) (k 0)))) (h "04qirrs7rbakx7gx65mp9fh1qpny8479z7jijpk53vkfwa52lqic")))

(define-public crate-auto_correct-0.1.1 (c (n "auto_correct") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "threads_pool") (r "^0.1.0") (d #t) (k 0)))) (h "1b2wxbavi4h9wkp63hpcv8l0a2hjnxq07r51gxyhzrwm8qq00j28")))

(define-public crate-auto_correct-0.1.2 (c (n "auto_correct") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "threads_pool") (r "^0.1.0") (d #t) (k 0)))) (h "1nlgbyxlng36b88yxzrim5gpiyrb7qddrhrqg5lc9n32qr0zdw4v")))

(define-public crate-auto_correct-0.1.4 (c (n "auto_correct") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "threads_pool") (r "^0.1.0") (d #t) (k 0)))) (h "0an08sy86ysn5p29hv369vvqp919k1kwa2cm27zkvihkcryvgh7y")))

(define-public crate-auto_correct-0.1.5 (c (n "auto_correct") (v "0.1.5") (d (list (d (n "crossbeam-channel") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "threads_pool") (r "^0.1.0") (d #t) (k 0)))) (h "0ya2y49yz9fm47xzc26iapnqb7sbvz6s4zqhay44yhgxz0bpydkq")))

(define-public crate-auto_correct-0.1.8 (c (n "auto_correct") (v "0.1.8") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "threads_pool") (r "^0.1.16") (d #t) (k 0)))) (h "0q12gpivksmdddgg7daq9lx369fmia8a6abx4sik0jghnc870h60")))

(define-public crate-auto_correct-0.1.9 (c (n "auto_correct") (v "0.1.9") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "threads_pool") (r "^0.1.16") (d #t) (k 0)))) (h "1cxslm9796xa95dj22gcx00n6z81hf0ifnsxiw1mnv7zcmhgw046")))

