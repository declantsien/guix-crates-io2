(define-module (crates-io au to automato-p) #:use-module (crates-io))

(define-public crate-automato-p-0.1.0 (c (n "automato-p") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nn51lrbm61djb3ag5k2zsbdcm01asvd5m61z8l1c4506n7jkb53")))

