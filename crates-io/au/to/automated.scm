(define-module (crates-io au to automated) #:use-module (crates-io))

(define-public crate-automated-0.1.0 (c (n "automated") (v "0.1.0") (d (list (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "rust-bert") (r "^0.20.0") (d #t) (k 0)) (d (n "rust_tokenizers") (r "^8.1.0") (d #t) (k 0)) (d (n "tch") (r "^0.10.0") (d #t) (k 0)) (d (n "terminal-spinners") (r "^0.3.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1wrpd3w8wcmq83pc7jf6hhx8rgd06816wf567smb724v8kk0s7fs")))

(define-public crate-automated-0.0.2 (c (n "automated") (v "0.0.2") (d (list (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "rust-bert") (r "^0.20.0") (d #t) (k 0)) (d (n "rust_tokenizers") (r "^8.1.0") (d #t) (k 0)) (d (n "tch") (r "^0.10.0") (d #t) (k 0)) (d (n "terminal-spinners") (r "^0.3.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "0vwvkl5jh4g531pvicww0pdcns3cjg9n6lrdbgl4zpa7air7hpda")))

(define-public crate-automated-0.0.3 (c (n "automated") (v "0.0.3") (d (list (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "rust-bert") (r "^0.20.0") (d #t) (k 0)) (d (n "rust_tokenizers") (r "^8.1.0") (d #t) (k 0)) (d (n "tch") (r "^0.10.0") (d #t) (k 0)) (d (n "terminal-spinners") (r "^0.3.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1q1dvvxj1i3lii2p6dc6zyklchcjnc62cnrrgld798r346p50i7l")))

