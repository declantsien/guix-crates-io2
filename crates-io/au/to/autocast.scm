(define-module (crates-io au to autocast) #:use-module (crates-io))

(define-public crate-autocast-0.1.0 (c (n "autocast") (v "0.1.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (k 0)) (d (n "expectrl") (r "^0.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6") (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 0)))) (h "1p5payanm31sfbs89c86i0wpkf9f1hfdz6zxp59zwvrjyj8r20m2")))

