(define-module (crates-io au to auto-diff-data-pipe) #:use-module (crates-io))

(define-public crate-auto-diff-data-pipe-0.5.8 (c (n "auto-diff-data-pipe") (v "0.5.8") (d (list (d (n "auto-diff") (r "^0.5.8") (d #t) (k 0)) (d (n "openblas-src") (r "^0.10") (d #t) (k 2)))) (h "1gc147sc5yjfwryzm6v1wrnsd134lxylz5qv8rbv28sarrip75a8")))

(define-public crate-auto-diff-data-pipe-0.5.9 (c (n "auto-diff-data-pipe") (v "0.5.9") (d (list (d (n "auto-diff") (r "^0.5.9") (d #t) (k 0)) (d (n "openblas-src") (r "^0.10") (d #t) (k 2)))) (h "0nbvibnicyddxc4kflwnb577xs34il9y0lbvbc89wwh8cfdgi9gi")))

