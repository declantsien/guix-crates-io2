(define-module (crates-io au to autodeck) #:use-module (crates-io))

(define-public crate-autodeck-0.2.0 (c (n "autodeck") (v "0.2.0") (d (list (d (n "humantime") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "streamdeck") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.11") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.1.6") (d #t) (k 0)))) (h "1q8mnjicrv7jqrpymag58lkcgy42620fwck22s4k3zkb248nyxcl")))

(define-public crate-autodeck-0.3.0 (c (n "autodeck") (v "0.3.0") (d (list (d (n "humantime") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "streamdeck") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.11") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.1.6") (d #t) (k 0)))) (h "19ny1pjd2p164nm43q8cac4fb1z7q1rnq03lrvm58w5aaq8vzpqg")))

(define-public crate-autodeck-0.4.0 (c (n "autodeck") (v "0.4.0") (d (list (d (n "humantime") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "streamdeck") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.11") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.1.6") (d #t) (k 0)))) (h "09fxjvsrrzlrki8mhjy319gpwqw23pfjq7mkwgj306ik9yxd5p8v")))

