(define-module (crates-io au to autohide-tdrop) #:use-module (crates-io))

(define-public crate-autohide-tdrop-1.0.0 (c (n "autohide-tdrop") (v "1.0.0") (d (list (d (n "breadx") (r "^3.1.0") (d #t) (k 0)))) (h "1dwif8p2qjsc3gax4bxw6mffava4m8sgj9l8845h38samcsy1pk6")))

(define-public crate-autohide-tdrop-1.0.1 (c (n "autohide-tdrop") (v "1.0.1") (d (list (d (n "breadx") (r "^3.1.0") (d #t) (k 0)))) (h "0n9vrx3d7z8zldrzlch6v5c2q1swg086mbalxsnm0s1w9zaq4rws")))

(define-public crate-autohide-tdrop-1.0.2 (c (n "autohide-tdrop") (v "1.0.2") (d (list (d (n "breadx") (r "^3.1.0") (d #t) (k 0)))) (h "1im12nmxh3gnw9c84v8z78vsj0afzv4slkpg1myi3yzkpjd5078p")))

(define-public crate-autohide-tdrop-1.0.3 (c (n "autohide-tdrop") (v "1.0.3") (d (list (d (n "x11rb") (r "^0.12.0") (d #t) (k 0)))) (h "197776iwax1yfkljwc04g746zs6cvpvy5yh5g0jw6rnpngnj5a57")))

