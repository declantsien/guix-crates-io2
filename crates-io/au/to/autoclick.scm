(define-module (crates-io au to autoclick) #:use-module (crates-io))

(define-public crate-autoclick-0.1.0 (c (n "autoclick") (v "0.1.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)))) (h "06ralz27dwmd8lgsmbnkk9kjd4q2v729265gy874mrnalrg2gzjq")))

(define-public crate-autoclick-0.1.1 (c (n "autoclick") (v "0.1.1") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)))) (h "0x4cn43w2dcv5jgjqkks7szix259b7d7lg09nqqvvpd84nj7wmrp")))

(define-public crate-autoclick-0.1.2 (c (n "autoclick") (v "0.1.2") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)))) (h "1x25nwa2257qml7jqpjvqsviprlrwym8ng1w92593q2vjll0il0q")))

(define-public crate-autoclick-1.0.0 (c (n "autoclick") (v "1.0.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)))) (h "0al4lr3r0a9793mpxa1wn2hi1h8s8sqz77v9nx1rsmac0bkzmpip")))

(define-public crate-autoclick-1.0.2 (c (n "autoclick") (v "1.0.2") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)))) (h "1zfs5wv352qqqym8jw27gqlg4z66k4kpgmmvskhn0k1j5cl64gpm")))

