(define-module (crates-io au to auto-builder) #:use-module (crates-io))

(define-public crate-auto-builder-0.1.0 (c (n "auto-builder") (v "0.1.0") (d (list (d (n "auto-builder-macro") (r "=0.1.0") (d #t) (k 0)))) (h "10jsh3w9sw59zvcji1vlxq5nhirpk0vlmkkbxfvfs4vjikcp4f3i")))

(define-public crate-auto-builder-0.2.0 (c (n "auto-builder") (v "0.2.0") (d (list (d (n "auto-builder-macro") (r "=0.2.0") (d #t) (k 0)))) (h "114g4ggwlgswhw2gaqp0407c8y0p4xh0n5c8rffc6c2nzs8cpqbj")))

