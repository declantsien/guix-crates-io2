(define-module (crates-io au to autotask-macros) #:use-module (crates-io))

(define-public crate-autotask-macros-0.1.0 (c (n "autotask-macros") (v "0.1.0") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1b8n9yw0wq58p3702dpw43xq7p217wjm9hmv7dsb601sj2v454hn")))

