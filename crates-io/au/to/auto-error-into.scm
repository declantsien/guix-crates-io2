(define-module (crates-io au to auto-error-into) #:use-module (crates-io))

(define-public crate-auto-error-into-0.1.0 (c (n "auto-error-into") (v "0.1.0") (d (list (d (n "auto-error-into-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1xdcchllwxlmcvnhrygqdvkq0v1wlj5g9pq98cyin71cig60cd4l")))

(define-public crate-auto-error-into-0.1.1 (c (n "auto-error-into") (v "0.1.1") (d (list (d (n "auto-error-into-macro") (r "^0.1.1") (d #t) (k 0)))) (h "1h0620gb92wizpwxmrfb572w52bh3lw5vjqzmh3xbk8g0w5f8gmn")))

