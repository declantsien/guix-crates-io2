(define-module (crates-io au to automatic) #:use-module (crates-io))

(define-public crate-automatic-0.1.0 (c (n "automatic") (v "0.1.0") (h "1gjja66q3wiadlsl5khwkp1xl1x85wa238875blwjfm56qi0i53c") (y #t)))

(define-public crate-automatic-0.0.1 (c (n "automatic") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0li960a6chah8fs5h02dgd9113vmx6nx248vib8g2rqibab6a59w") (y #t)))

(define-public crate-automatic-0.0.2 (c (n "automatic") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0chhq14439iahhq3bk7g53wj8jws0wi4gzrxp23v04dgp6lva5l5") (y #t)))

(define-public crate-automatic-0.0.3 (c (n "automatic") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "100qz56n2bbgxmjr693rgippyjdqpzjl86kjxbkmn4pag7ikrd5s") (y #t)))

(define-public crate-automatic-0.0.4 (c (n "automatic") (v "0.0.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "os_pipe") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0grak6w48ykpd8whnk3282l6nc8bavgcglzi8p775mfmb9xgfz3n") (y #t)))

(define-public crate-automatic-0.0.5 (c (n "automatic") (v "0.0.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "os_pipe") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1966s14x15yjf379c7ssav55y3cqxyd20agsfzgdr1a5vza6s2mn") (y #t)))

