(define-module (crates-io au to automatica) #:use-module (crates-io))

(define-public crate-automatica-0.8.0 (c (n "automatica") (v "0.8.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "03rwchpf996sxk55qqhbx3pcdyfvja1s5hr3plxfjfqlsk2mp1sn")))

(define-public crate-automatica-0.9.0 (c (n "automatica") (v "0.9.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "12rkvw6c4hp3cximjmvpi8ixi5jgv20cl89fvlh2x3p4fj8ad34i")))

(define-public crate-automatica-0.10.0 (c (n "automatica") (v "0.10.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)))) (h "1507a560f9cl1aw9w8zdj10afp0p3wsycw5x49z83apvsvn2ymsv")))

(define-public crate-automatica-0.10.1 (c (n "automatica") (v "0.10.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)))) (h "1vnk55ysq8apfmfsp6m0rj2w2bd9zz4lbhglxx1nz99g9a1xm64z")))

(define-public crate-automatica-1.0.0 (c (n "automatica") (v "1.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "complex-division") (r "^1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "polynomen") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0h4iz0wgjnxzjimjgny72xzr68w4f59zy16c6h996ypq8wha83sq")))

(define-public crate-automatica-1.1.0 (c (n "automatica") (v "1.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "complex-division") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "polynomen") (r "^1.1") (d #t) (k 0)) (d (n "proptest") (r "^1.2") (d #t) (k 2)))) (h "1iipyzlw8y2yj6c3npqsdvi3x57ncs5b96g582ld6vh1nmsnw62k") (r "1.56")))

