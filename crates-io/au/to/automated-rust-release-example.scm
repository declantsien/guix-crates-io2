(define-module (crates-io au to automated-rust-release-example) #:use-module (crates-io))

(define-public crate-automated-rust-release-example-0.0.0 (c (n "automated-rust-release-example") (v "0.0.0") (h "17h1dzphrkyv0kvsh31cs60cnkv4w1mpwami1mypr4j5v8qwhfhr")))

(define-public crate-automated-rust-release-example-0.0.21 (c (n "automated-rust-release-example") (v "0.0.21") (h "1lhx7l4s66wk7sr9d2qpxgll9gdf9mzzpily3bgn6yk5wvykjigj")))

(define-public crate-automated-rust-release-example-0.0.22 (c (n "automated-rust-release-example") (v "0.0.22") (h "0bllc36lp8b5x1f24cpidd4w183dx1a2s07xl8if49afl8a1yjch")))

(define-public crate-automated-rust-release-example-0.0.23 (c (n "automated-rust-release-example") (v "0.0.23") (h "0kfpx92f19k6sr6wy6z9grq252qcyz1nh49z6006brar6n2wa9vr")))

(define-public crate-automated-rust-release-example-0.0.24 (c (n "automated-rust-release-example") (v "0.0.24") (h "1wcnibp3vj62d6hdrj9pq2k5jz680xw8pjqkfcdzjxii1x3pvyhk")))

(define-public crate-automated-rust-release-example-0.0.25 (c (n "automated-rust-release-example") (v "0.0.25") (h "1sgzyijs6sqa29bpnxx0bcv0y2jff0lgfkj2kzvzw9vvvajif10h")))

(define-public crate-automated-rust-release-example-0.0.26 (c (n "automated-rust-release-example") (v "0.0.26") (h "04fzm0py1jbfk88arfkw4rxzgswikcjfic8aynm7mm682gkhl534")))

