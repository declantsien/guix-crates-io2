(define-module (crates-io au to autoperm) #:use-module (crates-io))

(define-public crate-autoperm-0.1.4 (c (n "autoperm") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0q5kwvq54l6pjhy2fqinphn1ib8p9sj5j1fh56clhp0ajrhn6j31")))

(define-public crate-autoperm-0.1.5 (c (n "autoperm") (v "0.1.5") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1jx00gmkbbhhz4dk0f0cj1n5bc6x6lw7axgj4ip37k1bb8nhnyrb") (f (quote (("bin" "clap"))))))

(define-public crate-autoperm-0.2.0 (c (n "autoperm") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0wvwc78w57a72zdb8jhacww69yg3ry3vk7m0mqn79nghf7j4p6g0") (y #t)))

(define-public crate-autoperm-0.2.1 (c (n "autoperm") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0wihxlibidckqb80f1q92vk0yg1nhmkkqyll6z2hr7civ3rc3yqc") (y #t)))

(define-public crate-autoperm-0.3.0 (c (n "autoperm") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0kk1qbh2zr6dir4w6l5cdy49a2prfqzvjr1ymxn3ccfv36z35vz5")))

