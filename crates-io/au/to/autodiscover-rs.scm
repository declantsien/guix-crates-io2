(define-module (crates-io au to autodiscover-rs) #:use-module (crates-io))

(define-public crate-autodiscover-rs-0.1.0 (c (n "autodiscover-rs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "socket2") (r "^0.3.12") (d #t) (k 0)))) (h "05vnxj0nj7bmkiw9qy74f0rbqphkhl6m35k1s017nmcfzy3h555i")))

(define-public crate-autodiscover-rs-0.1.1 (c (n "autodiscover-rs") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "socket2") (r "^0.3.12") (d #t) (k 0)))) (h "14p2j3n9xjgf89j1nv9k5nrbyvcz64309455lhqkwvda4bpmjxzk")))

