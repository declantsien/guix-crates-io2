(define-module (crates-io au to autocomplete) #:use-module (crates-io))

(define-public crate-autocomplete-0.1.0 (c (n "autocomplete") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "015ycpc6184wd8ifp3w2d220m22hj0ym7qqjciaq99qk7aqg7m8s")))

(define-public crate-autocomplete-0.1.1 (c (n "autocomplete") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "16vcq9jbd36yyri157c3nhschjg0n1mi28zgvqiynxj7n8skwk36")))

(define-public crate-autocomplete-0.1.2 (c (n "autocomplete") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0lgg1bb47bkhcgb1rmbymd429ipwnwigiflfw2nzmlnnybcjhw09")))

(define-public crate-autocomplete-0.1.3 (c (n "autocomplete") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "10d9l7jcrpbqmcrm1kzq746vr8wvpl5lpcb2qaz39li58sh10r05")))

