(define-module (crates-io au to auto-http-derive) #:use-module (crates-io))

(define-public crate-auto-http-derive-0.1.0 (c (n "auto-http-derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "027gjkwb8lbr6idxs1py94kgfm9gkqxa2frk06vwp8b9p1y6vpyd")))

