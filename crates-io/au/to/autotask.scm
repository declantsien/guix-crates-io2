(define-module (crates-io au to autotask) #:use-module (crates-io))

(define-public crate-autotask-0.1.0 (c (n "autotask") (v "0.1.0") (d (list (d (n "autotask-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "0z46zg552dq1p0xdfqasg7yg8l1a7jyw0kzlnkgbldg1lhl4yrry")))

