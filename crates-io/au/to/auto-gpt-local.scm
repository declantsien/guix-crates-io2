(define-module (crates-io au to auto-gpt-local) #:use-module (crates-io))

(define-public crate-auto-gpt-local-0.0.1 (c (n "auto-gpt-local") (v "0.0.1") (d (list (d (n "config") (r "^0.14.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)) (d (n "simple-fs") (r "^0.1.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tauri") (r "^1") (f (quote ("dialog-all" "path-all" "fs-all" "shell-open"))) (d #t) (k 0)) (d (n "tauri-build") (r "^1") (d #t) (k 1)))) (h "1ai4g424wd8qw10ah8dlmjbrrbnsa9fd2fxp5jbdjnrwssx5l43h") (f (quote (("custom-protocol" "tauri/custom-protocol"))))))

