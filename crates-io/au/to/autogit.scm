(define-module (crates-io au to autogit) #:use-module (crates-io))

(define-public crate-autogit-0.1.0 (c (n "autogit") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "1wbqddw1fgwzfc1lz9c6lk1h38v00jdx62vwqciamx3mbcf2miwd")))

(define-public crate-autogit-0.1.1 (c (n "autogit") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "1j2zi4gnyi82cl06b8d3sw9cqc2s9rwz5873m6fcq0d2llq5b4qg")))

(define-public crate-autogit-0.1.2 (c (n "autogit") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "003zac3av8kn7n5amjdwb0zrjgi2lgn91i2zl0i888225lsi5zfq")))

(define-public crate-autogit-0.1.3 (c (n "autogit") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "1jncpcisvnpi1cqx49ihm8b3wbw9k8ig3452rp0ip41zx72v84qf")))

(define-public crate-autogit-0.1.4 (c (n "autogit") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "07zr8axab076yx648x1l5ngfkpii8xcik2z6a71b0sfpyg2qjagv")))

(define-public crate-autogit-1.0.0 (c (n "autogit") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "open") (r "^3.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rqpj4qi1rss0h2nm9k12zhkmaaifa5kq49pcszsbvgfvmdwklfw")))

(define-public crate-autogit-1.1.0 (c (n "autogit") (v "1.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "open") (r "^3.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qf121s0gpi4agb3a54i9k4cf0r1fi6ms5aaz6vkjkxrb66wynl5")))

