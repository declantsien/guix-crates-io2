(define-module (crates-io au to automato) #:use-module (crates-io))

(define-public crate-automato-0.1.0 (c (n "automato") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11x068anxbmw4766knnv2riz5dgic34lnwcx97xn2lwiwj8j3gqd")))

(define-public crate-automato-0.2.0 (c (n "automato") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ia5k38676iswzks87hrzq3gpccwg4l1abhfwhkyn5kw5xkf889s")))

(define-public crate-automato-0.3.0 (c (n "automato") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dn5q8j1i8r406mw2d71w2gdkm51hx5gddggfdlazxaqhzfp3dhh")))

(define-public crate-automato-0.4.0 (c (n "automato") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08w48yy09r8ayl5idkiqji9xhajxzgskf45fgc74fd1zs8fz38jg")))

(define-public crate-automato-0.4.1 (c (n "automato") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1li7iz9932b9dmbi14p58ic3x1vp8p3rnagac7r85igm8wla29rk")))

(define-public crate-automato-0.5.0 (c (n "automato") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "166qapk5ps6dhbf53ava07d0lmczp1qi6pmy5bsbfvl15aynxpmj")))

(define-public crate-automato-0.6.0 (c (n "automato") (v "0.6.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bq65zplsg335mpzv06mshx6zh8j965gxi25dps042qaip3a6cp3")))

(define-public crate-automato-0.7.0 (c (n "automato") (v "0.7.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hw1f2wkfjrn95bj74a81nd3fz6awhb0dfjgam1xbb17z05csvns")))

(define-public crate-automato-0.8.0 (c (n "automato") (v "0.8.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mz1gma0fyhd6gxagiw7rvmgiadb0mqhs5163cwljrrzxypglywk")))

(define-public crate-automato-0.9.0 (c (n "automato") (v "0.9.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wp3gd8wnjk8ljc6msv4zg49yw32iflglz5q2zyaidbsz8f8bd15")))

(define-public crate-automato-0.10.0 (c (n "automato") (v "0.10.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16vyjzqf4xwqz9b480kr8vqya283y9micwd66yvpammbp5ai63ky")))

(define-public crate-automato-2.0.0 (c (n "automato") (v "2.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02xk41k54w336hln5hr6281nidx8mclsa2vy86yshbfbwkjbvwrj")))

(define-public crate-automato-3.0.0 (c (n "automato") (v "3.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09825x1aipj7zzw51jdwnms6zcilvildj77lpkmsbg4qan6aznzd")))

(define-public crate-automato-3.1.0 (c (n "automato") (v "3.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kgdmabjkcbgplkgdygjhgz1v7apmrdcm1wrwzmkbl88rpnnyx0h")))

(define-public crate-automato-4.0.0 (c (n "automato") (v "4.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d4h0z9x0l3v4zvgwqqsmfq1xadqx4zfjvq9q01x4v5fd541gsca")))

(define-public crate-automato-5.0.0 (c (n "automato") (v "5.0.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "003hfzlddr4xf92pdmi2z5kkjhpl0smb6ns2lvf4fadl7048gjmx")))

(define-public crate-automato-5.0.1 (c (n "automato") (v "5.0.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jszjm29cw3xzhvj72ck6s5ngkyhwsr10w169f0wzh3kil9m9x3i")))

(define-public crate-automato-5.0.2 (c (n "automato") (v "5.0.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jj5x7kb7hs91ysjykjkpr6hd9ga8bsmch5j2wxjzmi2k59h2j6n")))

(define-public crate-automato-5.1.0 (c (n "automato") (v "5.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01ifciwb4m054nw8g7042n53n1cbk31j04axjp0yr2j287ha9zi6")))

(define-public crate-automato-6.0.0 (c (n "automato") (v "6.0.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1iz5xg33ycv6097yv2vabjqpqph5swznz4zlnnccvskpxxvvn2ff")))

(define-public crate-automato-6.1.0 (c (n "automato") (v "6.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xzmvkywr3vyrxg6b196jc0nx3w346h1l80zlyisl1abf195lkjn")))

(define-public crate-automato-6.2.0 (c (n "automato") (v "6.2.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nzdvw3krrbrl7zkk4z5pnkafj8nwiwjjpwm69fq9k87i5jl7a95")))

(define-public crate-automato-6.3.0 (c (n "automato") (v "6.3.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vyx3c0nrfz776ppayb0aiq6spimxv4hsj4a651d4s94rqci8m2w")))

(define-public crate-automato-7.0.0 (c (n "automato") (v "7.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yy06f3l7lk77pibc3a4gr1v0v1krny4ph087wd11p4y1fbzi715")))

(define-public crate-automato-8.0.0 (c (n "automato") (v "8.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z6j5d6vh767lc7rscj53l9pb1n4vckh5d8nyhhjprfqaijp78xg")))

(define-public crate-automato-9.0.0 (c (n "automato") (v "9.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dwzsv8wnb2qiwrkjfkh36m8b1sc0iwj160xhn8jxpn9vfn8bxs4")))

(define-public crate-automato-10.0.0 (c (n "automato") (v "10.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q7nw053kn878v1s29lpz36cvzgms0d4qivgxxx5dlrxg3cqs802")))

(define-public crate-automato-11.0.0 (c (n "automato") (v "11.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18dg7s51f251wf59shqkapqa479rzg5vaw0vf8lhn71yfdrwwwlv")))

(define-public crate-automato-11.1.0 (c (n "automato") (v "11.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "155b4xx77704c5rd1sl3f7pw24802zy1x3qh6npn6qfdrn22jm9k")))

(define-public crate-automato-12.0.0 (c (n "automato") (v "12.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05abiqchjvv72af8ng29rr4v932001q2xijcvfrnv3i4x6fdvwwl")))

(define-public crate-automato-13.0.0 (c (n "automato") (v "13.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ac8kzpsqyf86mvqlgl716ss41js238g7r40qg856w33k7m5m061")))

(define-public crate-automato-14.0.0 (c (n "automato") (v "14.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lys27i7d9dx9n7q2n0z53airpa15pl12izn543p4vd0qnbi3z47")))

(define-public crate-automato-15.0.0 (c (n "automato") (v "15.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1diknygd7hkj1sdq2vv16908qpdikszjg75ihi4mi3jk32xzd3hv")))

(define-public crate-automato-16.0.0 (c (n "automato") (v "16.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xsv0wygn9vdw9y46glcciz7i0n9378x60qv0ryidn83z1spcmbk")))

(define-public crate-automato-17.0.0 (c (n "automato") (v "17.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y65819l2gyx871ryhl1nr8y1p8b52d4nq3sh68xhq3h2wkk8bx4")))

(define-public crate-automato-18.0.0 (c (n "automato") (v "18.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "184nzqy99gvibvjr9r1q47sdwvx3ddx37fb7506hvb5ha815009g")))

(define-public crate-automato-19.0.0 (c (n "automato") (v "19.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14kw4mwwgfl7jhmjn307rg266dxkncd5as1ifrrj95a14cv0jww9")))

(define-public crate-automato-20.0.0 (c (n "automato") (v "20.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14mx0rma1mqmw6c9z9p0xzkh18h7chm5h409sixj1jsaf2dqh2f0")))

(define-public crate-automato-21.0.0 (c (n "automato") (v "21.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0a1x4zrvqadvwcvyzgq30d1liq2hxxddn2v19mkl00xnl1d35xmd")))

(define-public crate-automato-22.0.0 (c (n "automato") (v "22.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qzcdphj22g5ylikdsfcvkxx5acjja6wmskz0cf3qb5gq43fsics")))

(define-public crate-automato-23.0.0 (c (n "automato") (v "23.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02hvr70in4gbfqrcipqbfy8i0zw6k0jv108cbbsvr6955h5acgmw")))

(define-public crate-automato-23.1.1 (c (n "automato") (v "23.1.1") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xj314zhqam733wlkqiz34amjsr2a524h0bylly835rdd3l1nvsx")))

