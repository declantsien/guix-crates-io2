(define-module (crates-io au to autom) #:use-module (crates-io))

(define-public crate-autom-0.1.0 (c (n "autom") (v "0.1.0") (h "0v4pvl90mmizmz1dc68lq1n6kipfzmsycfnr0ciciz9crqz66aac")))

(define-public crate-autom-0.1.1 (c (n "autom") (v "0.1.1") (h "1r1f5nrrdfqajf3an9y6nffinvapw0688km412lcikfrfid5mla8")))

(define-public crate-autom-0.1.2 (c (n "autom") (v "0.1.2") (h "1p3n7zyq4v4ashcxccyaaawsaynlm0mj9biqh1vdar1h964bgb33")))

(define-public crate-autom-0.1.3 (c (n "autom") (v "0.1.3") (h "08cy97ja4byq5xli3pa6hqpn2yjhqd1kgjf7ybb3xfhz21a5h80n")))

(define-public crate-autom-0.1.4 (c (n "autom") (v "0.1.4") (h "03v5yk5nmh77273hyw2p91x16mgyr7np87zq3kd5fxv49l4dm0zj")))

(define-public crate-autom-0.1.5 (c (n "autom") (v "0.1.5") (h "18iznb3ay00hqspczshy2c1ff63lfqwsm29h1kgbb1rkaggg634p")))

