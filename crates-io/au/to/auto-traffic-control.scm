(define-module (crates-io au to auto-traffic-control) #:use-module (crates-io))

(define-public crate-auto-traffic-control-0.2.0 (c (n "auto-traffic-control") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "prost") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.7.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.0") (d #t) (k 1)))) (h "0xy7hqdy3jk76666ngx2gj9x5bd3vhl871sdvdy5i8fp35kyx8lf") (f (quote (("server")))) (r "1.56")))

(define-public crate-auto-traffic-control-0.3.0 (c (n "auto-traffic-control") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "prost") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.7.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.0") (d #t) (k 1)))) (h "1pypw1fpdmw26p8himbswd0pjrfkqwkmq4a49bhl5mi0vdkn16ly") (f (quote (("server")))) (r "1.56")))

(define-public crate-auto-traffic-control-0.3.1 (c (n "auto-traffic-control") (v "0.3.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1hbacqxcrkp18vnximwizjwpzbb4qjzc7hifdb9z6y8x080frmv2") (f (quote (("server")))) (r "1.56")))

(define-public crate-auto-traffic-control-0.3.2 (c (n "auto-traffic-control") (v "0.3.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "09idl675839jc42brw9xliwkkq6s4sa7vr4d3n567221ldx2bdjm") (f (quote (("server")))) (r "1.56")))

