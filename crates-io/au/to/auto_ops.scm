(define-module (crates-io au to auto_ops) #:use-module (crates-io))

(define-public crate-auto_ops-0.1.0 (c (n "auto_ops") (v "0.1.0") (h "0iark6sgw5q67krslmpcn7rvw0adkkvrjs6bdbvqbjf2kxahsiyp")))

(define-public crate-auto_ops-0.3.0 (c (n "auto_ops") (v "0.3.0") (h "0ndgb35m2jg1f9rbm43bwcg26v7b40dcmbv35aw4f08hivfzfq3l")))

