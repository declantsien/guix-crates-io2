(define-module (crates-io au to autodefault) #:use-module (crates-io))

(define-public crate-autodefault-1.0.0 (c (n "autodefault") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (f (quote ("full" "parsing" "visit-mut" "printing"))) (k 0)))) (h "0ykp7g1mvi0simjwmragwbw33564lvr58v8wrssjip4kx4qdjiyy")))

(define-public crate-autodefault-1.1.0 (c (n "autodefault") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (f (quote ("full" "parsing" "visit-mut" "printing"))) (k 0)))) (h "0xbxnlfnw777dbxh09cr91ybvdh389dq9qd7jflk7y6wr1pinywh")))

(define-public crate-autodefault-2.0.0 (c (n "autodefault") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (f (quote ("full" "parsing" "visit-mut" "printing"))) (k 0)))) (h "11wh0b0nc7afdywq17r0m1l54r5wcw8dl356znw6qsri93j4fs4p")))

