(define-module (crates-io au to autodiff_rs) #:use-module (crates-io))

(define-public crate-autodiff_rs-0.1.0 (c (n "autodiff_rs") (v "0.1.0") (d (list (d (n "autodiff_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)) (d (n "ndarray_einsum_beta") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5.4") (d #t) (k 0)))) (h "0lcp4rif6zs13qmxf892w4mqgjlnlcmnzcfiz3j5jhwwkv83hknz") (s 2) (e (quote (("ndarray" "dep:ndarray" "dep:ndarray_einsum_beta"))))))

(define-public crate-autodiff_rs-0.1.1 (c (n "autodiff_rs") (v "0.1.1") (d (list (d (n "autodiff_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)) (d (n "ndarray_einsum_beta") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5.4") (d #t) (k 0)))) (h "0v5wlx60s00qm1d3yrvac7vxd583hdfxlmj6mv0xp53bzj884620") (s 2) (e (quote (("ndarray" "dep:ndarray" "dep:ndarray_einsum_beta"))))))

