(define-module (crates-io au to autoregressive) #:use-module (crates-io))

(define-public crate-autoregressive-0.1.0 (c (n "autoregressive") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "13f1gzv3z7szqih0h8cxy5i6by135lwhmjb4wjs59nyzzr5m4ba5")))

(define-public crate-autoregressive-0.1.1 (c (n "autoregressive") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "0i8ssp0jki0dlxgcsycqmpb8q9ias3r7bwfgf3mykggj0iinqnw8")))

(define-public crate-autoregressive-0.1.2 (c (n "autoregressive") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "0wby2ss447d33r07kxypwkjs4l0600xj65pw004b6z07ynhm6qsj")))

(define-public crate-autoregressive-0.1.3 (c (n "autoregressive") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "1j54h7qirxygmidmg522rsm017lqvnilxjqz62q7d59bp3a64yfz")))

(define-public crate-autoregressive-0.1.4 (c (n "autoregressive") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "0pay2zh7050f6kqj9dzahkwy6wi3qbjxprrf2yyhg2i6172xyh8m") (y #t)))

(define-public crate-autoregressive-0.1.5 (c (n "autoregressive") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "0bys55sdl241kdnc819kmm7kj0d7w3782z766k6v0128i6m6y608")))

