(define-module (crates-io au to automaton) #:use-module (crates-io))

(define-public crate-automaton-0.0.1 (c (n "automaton") (v "0.0.1") (d (list (d (n "regex") (r "^0.1.29") (d #t) (k 2)))) (h "10hrxd9lrrrsl8db0j9q4ky2a95wpjr5ksvzwqbasyf937s87q32") (f (quote (("bench"))))))

(define-public crate-automaton-0.0.2 (c (n "automaton") (v "0.0.2") (d (list (d (n "regex") (r "^0.1.29") (d #t) (k 2)))) (h "0b73x81451kxiyh2nv5kvai8y4ndk6lsn101whrxc7zq0i9d128y") (f (quote (("bench"))))))

