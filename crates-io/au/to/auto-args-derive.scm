(define-module (crates-io au to auto-args-derive) #:use-module (crates-io))

(define-public crate-auto-args-derive-0.1.0 (c (n "auto-args-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1g4ag17p8khqq8563lcblnbdnpgkfzmrzkd526nhwiwprsc7jra7")))

(define-public crate-auto-args-derive-0.1.1 (c (n "auto-args-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0m8z3mqm87850v86yqdlcg8inl9rkspyvgi9qv776887nhw4r89v")))

(define-public crate-auto-args-derive-0.1.2 (c (n "auto-args-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0igvw554s70yzr5miq557nrv6cvk0zkixygpgi2bg4syvmrw7mzd")))

(define-public crate-auto-args-derive-0.1.3 (c (n "auto-args-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1c8dpk9dv84z462291257srpk875jnfvra2v31za4pjw1p6vzx1s")))

(define-public crate-auto-args-derive-0.1.4 (c (n "auto-args-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0jvi0dsnhr0p5llqrcfjd9m7fmwv2b5sl5xwrnnfm0k0s48y5vpk")))

(define-public crate-auto-args-derive-0.1.5 (c (n "auto-args-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0kw9nl3r4c84rjhyvnnihw0r2npyg1xrn06idjq80k2d9ilbqk3d")))

