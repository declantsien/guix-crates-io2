(define-module (crates-io au to auto_ref_impl) #:use-module (crates-io))

(define-public crate-auto_ref_impl-0.1.0 (c (n "auto_ref_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1b2f27cjw7nbc3vsynfb6b6zrscrxalwid4xfjcyzp2fqqmbnpqv")))

(define-public crate-auto_ref_impl-0.1.1 (c (n "auto_ref_impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1vmiv2c5kkyvm1r3c3add3l0g192a6j2s3dsnlav2k3sffs4z38k")))

(define-public crate-auto_ref_impl-0.1.2 (c (n "auto_ref_impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "15s35h1rkbjwhzf5is3f0dy0hr91gg0wwwzavnvm7y85f953irqy")))

