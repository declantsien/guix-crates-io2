(define-module (crates-io au to autocall) #:use-module (crates-io))

(define-public crate-autocall-0.1.0 (c (n "autocall") (v "0.1.0") (h "16qz01iyr5djmljday0j90qcbx23rwsz62rbsgkw8p6a0rcjfv48") (y #t)))

(define-public crate-autocall-0.1.1 (c (n "autocall") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "singlemap") (r "^0.1.5") (d #t) (k 0)))) (h "0rq5k49c2pdyfs2b37ab7dc6a4clriqmq3nbg7awz8i0922rf85r") (y #t)))

(define-public crate-autocall-0.1.2 (c (n "autocall") (v "0.1.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "singlemap") (r "^0.1.5") (d #t) (k 0)))) (h "1y2zsrs9qqhz0bphj9i9ala474dzkizfnmzh64rivxikz2m1ipn0") (y #t)))

(define-public crate-autocall-0.1.3 (c (n "autocall") (v "0.1.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "singlemap") (r "^0.1.5") (d #t) (k 0)))) (h "15bkyq1zcis1gqhp3h8x2fscsix9iarax202jv1ncjc2zszp6m5z") (y #t)))

(define-public crate-autocall-0.1.6 (c (n "autocall") (v "0.1.6") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "singlemap") (r "^0.1.5") (d #t) (k 0)))) (h "0ryvi35aydnsq64yfkk61hf39hi3ki3wphx3yfx9bjais4xqkkz4")))

