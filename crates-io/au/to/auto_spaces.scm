(define-module (crates-io au to auto_spaces) #:use-module (crates-io))

(define-public crate-auto_spaces-0.1.0 (c (n "auto_spaces") (v "0.1.0") (d (list (d (n "auto_cellular") (r "^0.1") (d #t) (k 0)) (d (n "cursive") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.81") (o #t) (d #t) (k 0)))) (h "16kxz08ah4bpsxskdbzbjazf0dfnbrhaq3j7b2n7420iz8r4f5dj") (s 2) (e (quote (("wasm" "dep:wasm-bindgen" "getrandom/js") ("sdl2" "dep:sdl2") ("cursive" "dep:cursive"))))))

