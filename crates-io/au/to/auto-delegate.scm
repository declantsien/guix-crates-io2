(define-module (crates-io au to auto-delegate) #:use-module (crates-io))

(define-public crate-auto-delegate-0.0.1 (c (n "auto-delegate") (v "0.0.1") (d (list (d (n "auto-delegate-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1rynqd0fj906vs9srm1bpvv2r6ij9q13mmvn8j64y75q576n7w9d")))

(define-public crate-auto-delegate-0.0.2 (c (n "auto-delegate") (v "0.0.2") (d (list (d (n "auto-delegate-macros") (r "^0.0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0hx061kxki9al6y8nh9pvd3bvfhh008zb0d5br896b46scfljidj")))

(define-public crate-auto-delegate-0.0.3 (c (n "auto-delegate") (v "0.0.3") (d (list (d (n "auto-delegate-macros") (r "^0.0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0yzhjp5d82fm4m9nc621x2i7frgjpcqqyx1733pds4qhma7wsfxb")))

(define-public crate-auto-delegate-0.0.4 (c (n "auto-delegate") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "auto-delegate-macros") (r "^0.0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0kcak9l0qaz2vwz1bcfl735mjmsni8fisa5smfk3faxwrknmhx4a")))

(define-public crate-auto-delegate-0.0.5 (c (n "auto-delegate") (v "0.0.5") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "auto-delegate-macros") (r "^0.0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "09mnlwbx8kkgnbpmq48xr8pqkdmx5n4ry03ndpm40wyrymprfg5z")))

(define-public crate-auto-delegate-0.0.6 (c (n "auto-delegate") (v "0.0.6") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "auto-delegate-macros") (r "^0.0.6") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "16kzz895naa466giprxjb2fmp24ls8dymdjfybxlvi65kjcimbl8")))

(define-public crate-auto-delegate-0.0.7 (c (n "auto-delegate") (v "0.0.7") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "auto-delegate-impl") (r "^0.0.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0z5yi192fkdhkslkmnbhmsqyaaaynwxbw2sh3ykx411i8yl58dbn")))

(define-public crate-auto-delegate-0.0.8 (c (n "auto-delegate") (v "0.0.8") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "auto-delegate-impl") (r "^0.0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0g5rd4w8fy9dymvfd9ypd9rilhl8kkh996qshr2wp66pvsk48nch") (y #t)))

(define-public crate-auto-delegate-0.0.9 (c (n "auto-delegate") (v "0.0.9") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "auto-delegate-impl") (r "^0.0.9") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0bp0l9r7857pk34m27pirhcr02v62hj9p9gpby71v2kxhxv9j7hh")))

(define-public crate-auto-delegate-0.1.0 (c (n "auto-delegate") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "auto-delegate-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0cddnlzjh434q8k5m1660r240xk35cil27z7xq48z4i4z491vxv0")))

(define-public crate-auto-delegate-0.1.1 (c (n "auto-delegate") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "auto-delegate-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1a1c8hx615fcj028gahh76d2fjy5x2lcyi29x206cd0a6v6vzdqd") (y #t)))

(define-public crate-auto-delegate-0.1.2 (c (n "auto-delegate") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 2)) (d (n "auto-delegate-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1fjyyy8j2pnwyfj9a839hbf67xh3z3b3al119p9yx77vq3svmhjg")))

