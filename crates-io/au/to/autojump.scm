(define-module (crates-io au to autojump) #:use-module (crates-io))

(define-public crate-autojump-0.2.1 (c (n "autojump") (v "0.2.1") (d (list (d (n "atomicwrites") (r "^0.0.14") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 1)))) (h "03xbh3cdz1s2xb0drp6hrr5ljw654j6r9sqa2ywrwa39jxw62qw2")))

(define-public crate-autojump-0.3.1 (c (n "autojump") (v "0.3.1") (d (list (d (n "atomicwrites") (r "^0.1.4") (d #t) (k 0)) (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "strsim") (r "^0.6") (d #t) (k 0)))) (h "005llibbrrghiqwrgfjafdnhgi9i659mb3kwjaxz2d6d797l3d3g") (f (quote (("nightly") ("default"))))))

(define-public crate-autojump-0.4.0 (c (n "autojump") (v "0.4.0") (d (list (d (n "atomicwrites") (r "^0.1.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "strsim") (r "^0.6") (d #t) (k 0)))) (h "1r5rnpizsrai9j3wg18ji6i336kah9jgsjrvjw81r8rkfa96gcnj") (f (quote (("nightly") ("default"))))))

(define-public crate-autojump-0.5.1 (c (n "autojump") (v "0.5.1") (d (list (d (n "atomicwrites") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo" "wrap_help"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "0bdqn2sdz24brcnmvqr1l6516vy1gyhiv679ja3rhwy7g0yhgj3a") (f (quote (("nightly") ("default"))))))

