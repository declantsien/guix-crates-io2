(define-module (crates-io au to auto_vec) #:use-module (crates-io))

(define-public crate-auto_vec-0.1.0 (c (n "auto_vec") (v "0.1.0") (h "0m2dvh5r70pqcch1g3xwapvlwbkijhfn0jw4cnr1gvhwais41lp4") (y #t)))

(define-public crate-auto_vec-0.1.1 (c (n "auto_vec") (v "0.1.1") (h "0rpwxrihvz1mshafjajhbkcs4way8w3kywczjgij8ianb46ydisq") (y #t)))

(define-public crate-auto_vec-0.1.2 (c (n "auto_vec") (v "0.1.2") (h "05ay9cvbszsrgab41qr5gcvynvd5nvmqi0mywz2i1zawbkf1dfda") (y #t)))

(define-public crate-auto_vec-0.1.3 (c (n "auto_vec") (v "0.1.3") (h "11i4059gb4bm71j3n95zir0k5frys7bx8m1sqjq48pgyy2rl1p0a") (y #t)))

(define-public crate-auto_vec-0.1.4 (c (n "auto_vec") (v "0.1.4") (h "1qqlpq3fnx6c78xyl802nqszls7vag559hsqnpjjvjl3x8vk02fr") (y #t)))

(define-public crate-auto_vec-0.1.5 (c (n "auto_vec") (v "0.1.5") (h "0853q0bhgwymrx651qdpg9rx0l22zx84lmj77mfrvfxk5k22cmaw") (y #t)))

(define-public crate-auto_vec-0.1.6 (c (n "auto_vec") (v "0.1.6") (h "1ml8dpm15d4d9mlm806w3mckyls89zbii45nlwpnw3h294h9c19q") (y #t)))

(define-public crate-auto_vec-0.1.7 (c (n "auto_vec") (v "0.1.7") (h "1h7lqzsk0j5zcj3arzq283zqsdaz6m5v11hh8r8lrchdk3iljrmm") (y #t)))

(define-public crate-auto_vec-0.1.8 (c (n "auto_vec") (v "0.1.8") (h "0bimyxdmj7b143b9z2fnhkm40678c5cay6vk1a4s1yh9l2rm42as") (y #t)))

(define-public crate-auto_vec-0.1.9 (c (n "auto_vec") (v "0.1.9") (h "07r3zvs857764a0w06jhnvn0gld4fbas85dd80q8fxfyi7wj14wm")))

(define-public crate-auto_vec-0.2.0 (c (n "auto_vec") (v "0.2.0") (h "00dj7xi0ksw2wfv6y1agkm52ks1hh7jm7qs3dyrfrc2qgszx3rld")))

