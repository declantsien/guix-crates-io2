(define-module (crates-io au to autobahn-hash) #:use-module (crates-io))

(define-public crate-autobahn-hash-0.1.0 (c (n "autobahn-hash") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("nightly_portable_simd"))) (k 0)) (d (n "criterion") (r "^0.4") (k 2)) (d (n "highway") (r "^1") (d #t) (k 2)) (d (n "multiversion") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1aj7isxdhrdbdwmzpw70rlkcbi5pwxj6s3drys7h8vaxmqc0v6d7") (f (quote (("std") ("default" "multiversion")))) (s 2) (e (quote (("multiversion" "dep:multiversion" "std"))))))

