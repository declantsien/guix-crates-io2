(define-module (crates-io au to auto-args) #:use-module (crates-io))

(define-public crate-auto-args-0.1.0 (c (n "auto-args") (v "0.1.0") (d (list (d (n "auto-args-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)))) (h "1ik2kpkgnhkzm5iicvggni5chwxydj59b95fifd14vrmv2m4s3y0")))

(define-public crate-auto-args-0.2.0 (c (n "auto-args") (v "0.2.0") (d (list (d (n "auto-args-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)))) (h "1dmvc8p584gxgwfsybxjvqsy2skwihxivi7clxjy5mhw2nqknqvf")))

(define-public crate-auto-args-0.2.1 (c (n "auto-args") (v "0.2.1") (d (list (d (n "auto-args-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)))) (h "033dl3gmm8ji3c61wi12vhbw5ws31f1n0v9fgkdv3qdj7nzag66m")))

(define-public crate-auto-args-0.2.2 (c (n "auto-args") (v "0.2.2") (d (list (d (n "auto-args-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)))) (h "0mmbrcmk59v5qkjqbqbsg25mhpv0ah6ffzqy28ggyfksqzys8xvr")))

(define-public crate-auto-args-0.2.3 (c (n "auto-args") (v "0.2.3") (d (list (d (n "auto-args-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)))) (h "10qqq73a2zmw2qlvw9jkwb8ibwh77yvqggccy2nx9pp473z52vl5")))

(define-public crate-auto-args-0.2.4 (c (n "auto-args") (v "0.2.4") (d (list (d (n "auto-args-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)))) (h "0xld1wszcik15v945r3jv1rk71ff99px4qp007bnmay40wd21l2b")))

(define-public crate-auto-args-0.2.5 (c (n "auto-args") (v "0.2.5") (d (list (d (n "auto-args-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)))) (h "074r8zsns2sf5x5igznfzk0xilp1fbv3wc4yww8r3rl5jg9pjnn4")))

(define-public crate-auto-args-0.2.6 (c (n "auto-args") (v "0.2.6") (d (list (d (n "auto-args-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)))) (h "0a4k18dka3253syll0331lfjqflml8nhmj5x96xdycrf0ar26cz0")))

(define-public crate-auto-args-0.2.7 (c (n "auto-args") (v "0.2.7") (d (list (d (n "auto-args-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)))) (h "0nlfcd119jxsxpqv6z48vlvk8g2n1b9igcs2sj8cma3c2rdx2gz1")))

(define-public crate-auto-args-0.2.9 (c (n "auto-args") (v "0.2.9") (d (list (d (n "auto-args-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)))) (h "00xdxnfpv9nb1hgkgzpy16pd4c54ylvihkhfjghnxxiyd6w0znpc")))

(define-public crate-auto-args-0.3.0 (c (n "auto-args") (v "0.3.0") (d (list (d (n "auto-args-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0h8qg5dqnbh8rvba3f8wnc2czjmq02w5lmf3bhwxs4vgbqbsn2ja")))

