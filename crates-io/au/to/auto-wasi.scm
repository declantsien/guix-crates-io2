(define-module (crates-io au to auto-wasi) #:use-module (crates-io))

(define-public crate-auto-wasi-0.1.0 (c (n "auto-wasi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "wasi-common") (r "^0.22.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.71.0") (d #t) (k 0)) (d (n "wasmtime") (r "^0.22.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.22.0") (d #t) (k 0)) (d (n "wat") (r "^1.0.31") (d #t) (k 2)))) (h "134wg9444y9cav6qzna8a6lrsgmv7ad0g9cm0f1pbqhpn9r39zgp")))

