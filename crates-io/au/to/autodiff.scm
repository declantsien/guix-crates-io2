(define-module (crates-io au to autodiff) #:use-module (crates-io))

(define-public crate-autodiff-0.1.0 (c (n "autodiff") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "00641zjravlpkkmg7ajc6v2ldxzgqi1nyf0qk1bdrck4b8mli1l7")))

(define-public crate-autodiff-0.1.1 (c (n "autodiff") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "18jsb3q8ffzyibrghzhs5slnxpxvb7d031wjfn9q6clxca2ynkl3")))

(define-public crate-autodiff-0.1.2 (c (n "autodiff") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0nykq5di5w8ad25h5rqn32m55dxm57yng2f5xfcpmh3f33hlr67g")))

(define-public crate-autodiff-0.1.3 (c (n "autodiff") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "07xwhb36073aixaqyrzlnpir2xvknxq7izbm56dfz0zayljj0va6")))

(define-public crate-autodiff-0.1.4 (c (n "autodiff") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0knl1q39m4ys7s03lrjfrj9wn8sqramyhz1zalq92x2g334w3gi0")))

(define-public crate-autodiff-0.1.5 (c (n "autodiff") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1qcs612d62zlkhg868pp5rk9grhxqh75lakkq6gqrnf2lglrgacz")))

(define-public crate-autodiff-0.1.6 (c (n "autodiff") (v "0.1.6") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1m8zzd30sc4ydybpj8dah2sc1fcj7avgsz439vw09870j2adbdz2")))

(define-public crate-autodiff-0.1.7 (c (n "autodiff") (v "0.1.7") (d (list (d (n "cgmath") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0m54704dqgzfc462wcs55c28ryn2fnyj14pkcqnjgxkds8n9ckc1")))

(define-public crate-autodiff-0.1.8 (c (n "autodiff") (v "0.1.8") (d (list (d (n "cgmath") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "06g3bjlfw36qzjwgaf3b67s6swxyicnbnmhva8g3ybqli1ni1m3h")))

(define-public crate-autodiff-0.1.9 (c (n "autodiff") (v "0.1.9") (d (list (d (n "cgmath") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0dg63yvsjf6h06wr3aidwwira763kj0kxpp7an8djlzmqcpz9fwy")))

(define-public crate-autodiff-0.2.0 (c (n "autodiff") (v "0.2.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "cgmath") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0jl58af0qqkk8vxk61qfp35f918922dy82z9kilcw7b6cg1h4ihr") (y #t)))

(define-public crate-autodiff-0.2.1 (c (n "autodiff") (v "0.2.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "06zv9c4wvx158f1c77m5d13787hmsivm0g2r948cmmm7iicis2qz") (y #t)))

(define-public crate-autodiff-0.2.2 (c (n "autodiff") (v "0.2.2") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "19cki8zmh1pdv46jhhkarfiwkfck5vdk4b230rackiz5i3i8p9n4")))

(define-public crate-autodiff-0.3.0 (c (n "autodiff") (v "0.3.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1c6fim1wzagv2g2g5m5kg964k4ca8418gd717yl318zvpx1f5d2m")))

(define-public crate-autodiff-0.3.1 (c (n "autodiff") (v "0.3.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "04fzcqpm2i37yaw820xc2ky9zcvv9pfk2pdrzgx7924k782hi4zf")))

(define-public crate-autodiff-0.3.2 (c (n "autodiff") (v "0.3.2") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "01agz7cwxbxzcn1rgz6gkjd8pnrc0bkns9np390fsml28khz4l1d")))

(define-public crate-autodiff-0.4.0 (c (n "autodiff") (v "0.4.0") (d (list (d (n "approx") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bytemuck") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simba") (r "^0.6") (o #t) (d #t) (k 0)))) (h "15ri4x7dypqmfmyxfaapbdwwbbda7a0662gc8rsc8rgfz5xfw0yr") (f (quote (("na" "nalgebra" "approx" "simba") ("default" "cwise") ("cwise"))))))

(define-public crate-autodiff-0.5.1 (c (n "autodiff") (v "0.5.1") (d (list (d (n "approx") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 2)) (d (n "nalgebra") (r "^0.30.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simba") (r "^0.7") (o #t) (d #t) (k 0)))) (h "12anjpzivy7fvxc8kzf7ybyg8r82hyqhr6j1kj8i5lnvlynm5lpd") (f (quote (("na" "nalgebra" "approx" "simba") ("default" "cwise") ("cwise"))))))

(define-public crate-autodiff-0.5.2 (c (n "autodiff") (v "0.5.2") (d (list (d (n "approx") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 2)) (d (n "nalgebra") (r "^0.30.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simba") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0djrn14rfq5336c7an6i3yagmh45njvbw18g6j2f7i0mcnycbzlr") (f (quote (("na" "nalgebra" "approx" "simba") ("default" "cwise") ("cwise"))))))

(define-public crate-autodiff-0.6.0 (c (n "autodiff") (v "0.6.0") (d (list (d (n "approx") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 2)) (d (n "nalgebra") (r "^0.31") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simba") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1zjaq84wqm4wmr3bkbw7gg7kccdqn8arjhq1ym9vdjfz84w9fsxs") (f (quote (("na" "nalgebra" "approx" "simba") ("default" "cwise") ("cwise"))))))

(define-public crate-autodiff-0.7.0 (c (n "autodiff") (v "0.7.0") (d (list (d (n "approx") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "simba") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "cgmath") (r "^0.18") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qf1skr01srsvfqw8ji42ajrj3md0xr5q56v0n7rs23rgjnir72r") (f (quote (("na" "nalgebra" "approx" "simba") ("default" "cwise") ("cwise"))))))

