(define-module (crates-io au to automove) #:use-module (crates-io))

(define-public crate-automove-1.0.0 (c (n "automove") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1aznvmnadyr5120qn22inj8m7pbhlrny8fl4cjd00a14mfij0lkl")))

(define-public crate-automove-1.0.1 (c (n "automove") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1vcb4wzgj9180fnr79shykm6khqdwknr20c87bxdw34d093lbk1d")))

(define-public crate-automove-1.0.2 (c (n "automove") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1i5gjacqimx7y8z3lqh1dngmrzcvzw20w7rlsb68al44jbdvwi0r")))

(define-public crate-automove-1.0.3 (c (n "automove") (v "1.0.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1pq9h27f557j4rdlkf839flpw478w3giyf7jd05wc1v4n93fkj1m")))

(define-public crate-automove-1.0.4 (c (n "automove") (v "1.0.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1l9w1n68p7dc200y92k3krx1vnnr0m5x5g9ch1rlnzkchxw6nsg5")))

(define-public crate-automove-1.0.5 (c (n "automove") (v "1.0.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0h0m8lymllg2a0d052xjdkgpnffrivslpkh8khhczppw8j5d0r29")))

(define-public crate-automove-1.0.6 (c (n "automove") (v "1.0.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "043b26ycd20l3wrg59h00l934zd2i2w086ryvpyhdc17dglj2sf9")))

(define-public crate-automove-1.0.7 (c (n "automove") (v "1.0.7") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "07yac1hjjp55vs2acifq34vffqa4zdgwzk58cz7s6yskhd7aadn3")))

(define-public crate-automove-1.1.0 (c (n "automove") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0shjngh83smwmbbbb22vw2f29gqsgw9s13fhcildsfpz1bkxyad3")))

(define-public crate-automove-1.2.0 (c (n "automove") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1zmza14p8ha8x69q2y0id60pljwvbkv93x30ilanihvmfvxrvcri")))

