(define-module (crates-io au to auto-enum) #:use-module (crates-io))

(define-public crate-auto-enum-0.1.0 (c (n "auto-enum") (v "0.1.0") (d (list (d (n "checked-enum") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kprja4xf8yz4g2mha87629pplg4qlhlw9xapabd8s7k3ykph0di")))

(define-public crate-auto-enum-0.1.1 (c (n "auto-enum") (v "0.1.1") (d (list (d (n "checked-enum") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mw4xb8fg0wzx118k2hdvhv3lvxffd3rwrmm5mnjhj4gy3frrjgb")))

(define-public crate-auto-enum-0.1.2 (c (n "auto-enum") (v "0.1.2") (d (list (d (n "checked-enum") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qbmdgh2qjjnfhjnj88faqksq4kzdjp18qrnsyw1wsixmpvz62y7")))

(define-public crate-auto-enum-0.2.0-alpha1 (c (n "auto-enum") (v "0.2.0-alpha1") (d (list (d (n "checked-enum") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "164lb5pmgmr57db6zn78blpa8939wgv6ykj27050r3hmmvyvi0cd")))

