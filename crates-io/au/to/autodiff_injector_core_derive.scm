(define-module (crates-io au to autodiff_injector_core_derive) #:use-module (crates-io))

(define-public crate-autodiff_injector_core_derive-0.0.1 (c (n "autodiff_injector_core_derive") (v "0.0.1") (d (list (d (n "autodiff_injector_core") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0xsadzbgyv6cxj54bzpx0bcs0k9zdqzqxc50nhmcvscahc067vb7")))

