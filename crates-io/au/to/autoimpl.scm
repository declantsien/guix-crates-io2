(define-module (crates-io au to autoimpl) #:use-module (crates-io))

(define-public crate-autoimpl-0.1.0 (c (n "autoimpl") (v "0.1.0") (d (list (d (n "autoimpl-derive") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "0pqqm89q2wxww76mad5a57gsmgcz4bj3aw325galmk25fspfmazj")))

