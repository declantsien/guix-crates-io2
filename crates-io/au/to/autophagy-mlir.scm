(define-module (crates-io au to autophagy-mlir) #:use-module (crates-io))

(define-public crate-autophagy-mlir-0.1.0 (c (n "autophagy-mlir") (v "0.1.0") (d (list (d (n "autophagy") (r "^0.1") (d #t) (k 0)) (d (n "melior") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "train-map") (r "^0.1.0") (d #t) (k 0)))) (h "0am650vpip1pc40n7w1mmkaf9bcindgcmmc7hzd27yyfi890ci9c")))

(define-public crate-autophagy-mlir-0.1.1 (c (n "autophagy-mlir") (v "0.1.1") (d (list (d (n "autophagy") (r "^0.1") (d #t) (k 0)) (d (n "melior") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "train-map") (r "^0.1.0") (d #t) (k 0)))) (h "1k2xiyax69p5d00qggmg5xag2vi6g3a6shakkvgb1zapippglriq")))

