(define-module (crates-io au to automatic_file_sorter) #:use-module (crates-io))

(define-public crate-automatic_file_sorter-1.4.0 (c (n "automatic_file_sorter") (v "1.4.0") (h "0gagh8n09hgqz2yn8q243yr6gjnkmdla15a23r5bw0hbfw5lyaqs") (y #t)))

(define-public crate-automatic_file_sorter-0.1.0 (c (n "automatic_file_sorter") (v "0.1.0") (h "08skrzxzp6pys1ni60knkikhxrbl0nxwr34kw7zilhaxf4aa64ia")))

