(define-module (crates-io au to autodo_helper) #:use-module (crates-io))

(define-public crate-autodo_helper-0.1.0 (c (n "autodo_helper") (v "0.1.0") (h "12sjmd1xyk1bcanlsaqmqlc0xll970k3a8mc7y58gkayzz9h59fx")))

(define-public crate-autodo_helper-0.1.1 (c (n "autodo_helper") (v "0.1.1") (h "164ll1m7zvn7kd1z4ybcz76f8plsgbjvssip1bgi5x90bpihzqsi")))

