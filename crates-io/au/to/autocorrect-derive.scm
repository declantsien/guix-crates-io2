(define-module (crates-io au to autocorrect-derive) #:use-module (crates-io))

(define-public crate-autocorrect-derive-0.1.0 (c (n "autocorrect-derive") (v "0.1.0") (d (list (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "11x6wcr6gmx8iqpn6lkzydcvgizb7rjnn9j55z02jbccwpms00hn")))

(define-public crate-autocorrect-derive-0.2.0 (c (n "autocorrect-derive") (v "0.2.0") (d (list (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0s9d7v7szcnky4n402yaa6iyighk5saazs66384x82lk8ja05gfy")))

(define-public crate-autocorrect-derive-0.3.0 (c (n "autocorrect-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0ypiy95l51jbj6f3smhj7c4cs2h09jf1prvbs2nldi5c7wr8j25q")))

