(define-module (crates-io au to automaat-processor-shell-command) #:use-module (crates-io))

(define-public crate-automaat-processor-shell-command-0.1.0 (c (n "automaat-processor-shell-command") (v "0.1.0") (d (list (d (n "automaat-core") (r "^0.1") (d #t) (k 0)) (d (n "juniper") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "juniper") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "14w9mjszh4z0gwnlz6386h5qf9yl75wsy1fr96cmn81i3hi2dfiy")))

