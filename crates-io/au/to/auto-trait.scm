(define-module (crates-io au to auto-trait) #:use-module (crates-io))

(define-public crate-auto-trait-1.0.0 (c (n "auto-trait") (v "1.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q94np4c3dpcav8nx7zq00q4v763svldpdwlrnzk4sahz645sq71") (y #t)))

(define-public crate-auto-trait-1.0.1 (c (n "auto-trait") (v "1.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w1l7wkdc8pa21m719b9bh196n0rz5jvmr7zjgqyqm2pxr34dbgh")))

(define-public crate-auto-trait-2.0.0 (c (n "auto-trait") (v "2.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0af304kk6a4ja75p80ad6gd9wg8ii7ih6lnjxbkvfrfgdafdv5lj")))

