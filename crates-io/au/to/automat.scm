(define-module (crates-io au to automat) #:use-module (crates-io))

(define-public crate-automat-0.0.1 (c (n "automat") (v "0.0.1") (h "1rdg3218v50b8bn5w9wdds6k7c4mzwg92vdjln5blq1sl91y83f5")))

(define-public crate-automat-0.0.2 (c (n "automat") (v "0.0.2") (h "1nwq1bfkh8ril0s7xd0a7g9rv9d47wdqvcyh9kvxyv38brx12srg")))

(define-public crate-automat-0.0.3 (c (n "automat") (v "0.0.3") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1p1a6jyi25laz3wcfhhj98ywsi6r7llpygpq5wdd0yrxwqissnxk")))

(define-public crate-automat-0.0.4 (c (n "automat") (v "0.0.4") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "19hcnwlf8yvbaa6gd2zam9ikh05wnm50mdvn7xa90kyp9ysqxhaq")))

(define-public crate-automat-0.0.5 (c (n "automat") (v "0.0.5") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0w884m30a3anpf0n5jlrb5plrayb4vxvwbpzzpn0svlnvgz3kcbn")))

(define-public crate-automat-0.0.6 (c (n "automat") (v "0.0.6") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "19s6hm9igzv9rm38mi5vnpaigrscf857hlhdafq095wy9q7qpiik")))

(define-public crate-automat-0.0.7 (c (n "automat") (v "0.0.7") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1v5bigm95jgr0f8fzb40jxw0vgp08jxpn9rx23yl96r83mbyq7my")))

(define-public crate-automat-0.0.8 (c (n "automat") (v "0.0.8") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ib09cy9bpwkccy6khz5a0m2w8n1zgm3v14yl8hs0pdjmm0jv87s")))

