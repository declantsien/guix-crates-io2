(define-module (crates-io au x- aux-logid) #:use-module (crates-io))

(define-public crate-aux-logid-0.1.0 (c (n "aux-logid") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0w4pav0slhsrqjf1vhkqd84n4gy6b46xwq6axwcz2w4dnysy0ph2")))

