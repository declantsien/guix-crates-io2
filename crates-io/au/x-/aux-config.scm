(define-module (crates-io au x- aux-config) #:use-module (crates-io))

(define-public crate-aux-config-0.1.0 (c (n "aux-config") (v "0.1.0") (d (list (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0j0yhs5x7l82gkqaaia3y6nc23vypybhpwysnsjifvz2l47y5a4y")))

