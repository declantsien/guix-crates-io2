(define-module (crates-io au gm augmented-playhead) #:use-module (crates-io))

(define-public crate-augmented-playhead-0.1.0-alpha.3 (c (n "augmented-playhead") (v "0.1.0-alpha.3") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.3") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.0-alpha.3") (d #t) (k 0)))) (h "1pj52chq0jsrar53lbmmfpii95kxd22krqpv9mj2pmhh9jacpgi8")))

(define-public crate-augmented-playhead-0.1.0-alpha.4 (c (n "augmented-playhead") (v "0.1.0-alpha.4") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.4") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^1.0.0-alpha.4") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "026cvg7nyfrma69b3443irh6sgqg9sz1gjs6zza531lgcirg9dcm")))

(define-public crate-augmented-playhead-0.1.0-alpha.5 (c (n "augmented-playhead") (v "0.1.0-alpha.5") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.5") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^1.0.0-alpha.5") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.0-alpha.6") (d #t) (k 0)))) (h "1aw24c8bca5k5nqrg4vnx0lbvlqh6hg8ym0znqklbkdypg8gsbah")))

(define-public crate-augmented-playhead-0.1.0-alpha.6 (c (n "augmented-playhead") (v "0.1.0-alpha.6") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.0-alpha.7") (d #t) (k 0)))) (h "0b0b9yclp3xy14i5cma2k41nz7nhcsshsjs5n6ngpk7s04f0iv3x")))

(define-public crate-augmented-playhead-0.1.0-alpha.7 (c (n "augmented-playhead") (v "0.1.0-alpha.7") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.7") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^1.0.0-alpha.7") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.0-alpha.8") (d #t) (k 0)))) (h "02c15h0y3i4db5550ss9iyjfp9y88h1c1q205qp83fzb0ri4dxxp")))

(define-public crate-augmented-playhead-0.1.0-alpha.8 (c (n "augmented-playhead") (v "0.1.0-alpha.8") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.8") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^1.0.0-alpha.8") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.0-alpha.8") (d #t) (k 0)))) (h "1w3inj22cmfpm05x68lblwaphayqjdxzmfb3a1qdbmb3934lynbq")))

(define-public crate-augmented-playhead-0.2.0 (c (n "augmented-playhead") (v "0.2.0") (d (list (d (n "audio-processor-testing-helpers") (r "^1.1.0") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^1.1.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.1") (d #t) (k 0)))) (h "0ivmx15cm50js34a1hpzzq8pq93vc40y3gl4fzx15cqr6m5pga8a")))

(define-public crate-augmented-playhead-0.3.0 (c (n "augmented-playhead") (v "0.3.0") (d (list (d (n "audio-processor-testing-helpers") (r "^1.2.0") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^2.1.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.1") (d #t) (k 0)))) (h "0p4ibmw7diyrvbgl6slig3955lxhzy0k5x6b8qifmzcvsnkxd8p9")))

(define-public crate-augmented-playhead-0.4.0 (c (n "augmented-playhead") (v "0.4.0") (d (list (d (n "audio-processor-testing-helpers") (r "^1.3.0") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^2.2.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.2") (d #t) (k 0)))) (h "051b5qm5ay5y89n0j9ai7yak6h24wn97w0hyiv7gvfqj42l6alf6")))

(define-public crate-augmented-playhead-0.5.0 (c (n "augmented-playhead") (v "0.5.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.3.0") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^3.2.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.2") (d #t) (k 0)))) (h "0pspw98zg8rkvn4s4xc6g7zqi5y4bb4ycx76q80bicc6jlc72kdw")))

(define-public crate-augmented-playhead-0.6.0 (c (n "augmented-playhead") (v "0.6.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.4.0") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^4.0.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.2") (d #t) (k 0)))) (h "0mmcw1h0wf245f58bsbzdi5skv22hmrzd0s303vhz9rxkqgyvvky")))

(define-public crate-augmented-playhead-0.7.0 (c (n "augmented-playhead") (v "0.7.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.5.0") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^4.1.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.2.0") (d #t) (k 0)))) (h "11wkv80ycm8c7jl01amrnziqhl3yi1vpk1ls4c4shvcwkqxvsngp")))

(define-public crate-augmented-playhead-0.8.0 (c (n "augmented-playhead") (v "0.8.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.6.0") (d #t) (k 2)) (d (n "audio-processor-traits") (r "^4.2.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.2.0") (d #t) (k 0)))) (h "1kan1a2l4h0ph0bnrq0f8qd9095jqjj0afm5cygr9gq9jgmrmf68")))

