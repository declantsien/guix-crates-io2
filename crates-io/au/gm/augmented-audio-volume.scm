(define-module (crates-io au gm augmented-audio-volume) #:use-module (crates-io))

(define-public crate-augmented-audio-volume-0.1.0-alpha.2 (c (n "augmented-audio-volume") (v "0.1.0-alpha.2") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.2") (d #t) (k 2)))) (h "04nn9kj1ji5d589h7mb21wqabn8px44ljc99jq1iq8rcsb8kbs40") (f (quote (("f64") ("default"))))))

(define-public crate-augmented-audio-volume-0.1.0-alpha.3 (c (n "augmented-audio-volume") (v "0.1.0-alpha.3") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.3") (d #t) (k 2)))) (h "1hxk17sijcmwrc7x93cryi99v8lfaw9w12qg4lsmhmd1jik2h67c") (f (quote (("f64") ("default"))))))

(define-public crate-augmented-audio-volume-0.1.0-alpha.4 (c (n "augmented-audio-volume") (v "0.1.0-alpha.4") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.4") (d #t) (k 2)))) (h "0bvpk09b2wbhf1i9drpphfcm34db97pwdvzskjdssqxsfm6aqssb") (f (quote (("f64") ("f32") ("default"))))))

(define-public crate-augmented-audio-volume-0.1.0-alpha.5 (c (n "augmented-audio-volume") (v "0.1.0-alpha.5") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.5") (d #t) (k 2)))) (h "1l6ndijn95pn92xxiq7kzisg222nwia4szd3w5fg9xc7d19aqn78") (f (quote (("f64") ("f32") ("default"))))))

(define-public crate-augmented-audio-volume-0.1.0-alpha.6 (c (n "augmented-audio-volume") (v "0.1.0-alpha.6") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.6") (d #t) (k 2)))) (h "0n6fym67r670s52wp74l8xfj4inpw4r48sb5r6inraby46zbrm62") (f (quote (("f64") ("f32") ("default"))))))

(define-public crate-augmented-audio-volume-0.1.0-alpha.7 (c (n "augmented-audio-volume") (v "0.1.0-alpha.7") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.7") (d #t) (k 2)))) (h "02zik8i3h1h6a2mnidhz21k72pk45bk3yld2p75zfx5wi1hqlh0d") (f (quote (("f64") ("f32") ("default"))))))

(define-public crate-augmented-audio-volume-0.1.0-alpha.8 (c (n "augmented-audio-volume") (v "0.1.0-alpha.8") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.8") (d #t) (k 2)))) (h "1lmrpq17iljqr4gqlv9ijv3qi21qb7qgd1rrpkiqxzhwa98i65ha") (f (quote (("f64") ("f32") ("default"))))))

(define-public crate-augmented-audio-volume-0.2.0 (c (n "augmented-audio-volume") (v "0.2.0") (d (list (d (n "audio-processor-testing-helpers") (r "^1.1.0") (d #t) (k 2)))) (h "11mfd7pf8hi7sbw3k4qrfic68ks93lzx7pyn6s31kkgslbkqpfs2") (f (quote (("f64") ("f32") ("default"))))))

(define-public crate-augmented-audio-volume-0.3.0 (c (n "augmented-audio-volume") (v "0.3.0") (d (list (d (n "audio-processor-testing-helpers") (r "^1.2.0") (d #t) (k 2)))) (h "1y9vzfa4n1biasyyzn98ml5w89kfg6jh3sdwnfjricbz4cn1qkvs") (f (quote (("f64") ("f32") ("default"))))))

(define-public crate-augmented-audio-volume-0.4.0 (c (n "augmented-audio-volume") (v "0.4.0") (d (list (d (n "audio-processor-testing-helpers") (r "^1.3.0") (d #t) (k 2)))) (h "0qiv28h8cs3hqzv7p1wj2f3jp3xb7ckkzlv94dc1vy5fwhzz9dd9") (f (quote (("f64") ("f32") ("default"))))))

(define-public crate-augmented-audio-volume-0.5.0 (c (n "augmented-audio-volume") (v "0.5.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.3.0") (d #t) (k 2)))) (h "0fbq1nf1xy9az6nhmbdvf2pj37sszw8xy8k1gmg3nf0gaagqmb3y") (f (quote (("f64") ("f32") ("default"))))))

(define-public crate-augmented-audio-volume-0.6.0 (c (n "augmented-audio-volume") (v "0.6.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.4.0") (d #t) (k 2)))) (h "1rlbcc6b7y2yngvdhv7xbkgm1y250502i8qgb3jw9l9038nv99kx") (f (quote (("f64") ("f32") ("default"))))))

(define-public crate-augmented-audio-volume-0.7.0 (c (n "augmented-audio-volume") (v "0.7.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.5.0") (d #t) (k 2)))) (h "0wpy5d5vgc374gzar9qis4wlzrq4gxzvns3iz0x88wy5yvv2015g") (f (quote (("f64") ("f32") ("default"))))))

(define-public crate-augmented-audio-volume-0.8.0 (c (n "augmented-audio-volume") (v "0.8.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.6.0") (d #t) (k 2)))) (h "0x2znxiqks7bfhmnwa2s50qmz34cq84v93dld5669v8qc1c3qf6j") (f (quote (("f64") ("f32") ("default"))))))

