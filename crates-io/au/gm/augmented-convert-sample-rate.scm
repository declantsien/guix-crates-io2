(define-module (crates-io au gm augmented-convert-sample-rate) #:use-module (crates-io))

(define-public crate-augmented-convert-sample-rate-1.0.0-alpha.1 (c (n "augmented-convert-sample-rate") (v "1.0.0-alpha.1") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.1") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0bl8pbp622wd5dksa1db4nbs10sg277m4nrn7fz3zjd180axr9vb")))

(define-public crate-augmented-convert-sample-rate-1.0.0-alpha.2 (c (n "augmented-convert-sample-rate") (v "1.0.0-alpha.2") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.2") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1hirc7xam01lriwwinfcw21pggng8iiby94f1qdpv2icwy196c2k")))

(define-public crate-augmented-convert-sample-rate-1.0.0-alpha.3 (c (n "augmented-convert-sample-rate") (v "1.0.0-alpha.3") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.3") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0iq8pd0a2gvc28qily9rjmz20aq1d3kfcffn1w9bnfp382p9x8d2")))

(define-public crate-augmented-convert-sample-rate-1.0.0-alpha.4 (c (n "augmented-convert-sample-rate") (v "1.0.0-alpha.4") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.4") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "04wjdhalr66ajh3rzadqcymjak4yb2wsrfwhjr8hb11ajx41ljnd")))

(define-public crate-augmented-convert-sample-rate-1.0.0-alpha.5 (c (n "augmented-convert-sample-rate") (v "1.0.0-alpha.5") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.5") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "08wkrdxkdccsh7r8rf1ib00v0kpvvffy33rf5yfzpdkmq7rivcxn")))

(define-public crate-augmented-convert-sample-rate-1.0.0-alpha.6 (c (n "augmented-convert-sample-rate") (v "1.0.0-alpha.6") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "0bmgr1djfinr95kw3gz448j63di1amzbp6wzszvyd93313dzmxzx")))

(define-public crate-augmented-convert-sample-rate-1.0.0-alpha.7 (c (n "augmented-convert-sample-rate") (v "1.0.0-alpha.7") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.7") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "0ihhi3mpbl99722w798jgphcrrpfr35ldw0wyc50smpai2hzgczp")))

(define-public crate-augmented-convert-sample-rate-1.0.0-alpha.8 (c (n "augmented-convert-sample-rate") (v "1.0.0-alpha.8") (d (list (d (n "audio-processor-testing-helpers") (r "^1.0.0-alpha.8") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "1yal9yykgwihvp4scjm6hgas72r664xsfh4w44z7i1g58nckv8mz")))

(define-public crate-augmented-convert-sample-rate-1.1.0 (c (n "augmented-convert-sample-rate") (v "1.1.0") (d (list (d (n "audio-processor-testing-helpers") (r "^1.1.0") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "0y3zpy6r7r2kxpavhwrbll5hvcijknac8mv32vywc4vy6973hgph")))

(define-public crate-augmented-convert-sample-rate-1.2.0 (c (n "augmented-convert-sample-rate") (v "1.2.0") (d (list (d (n "audio-processor-testing-helpers") (r "^1.2.0") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "0qjn5aqf3s020ic2s66j9krag5ykmyr50jqn2xjw4k5zh7g1j3g8")))

(define-public crate-augmented-convert-sample-rate-1.3.0 (c (n "augmented-convert-sample-rate") (v "1.3.0") (d (list (d (n "audio-processor-testing-helpers") (r "^1.3.0") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "0sxnkrm19ci746z30ah587cg5cxky78mv7m6g9bjamjg9hpdljr9")))

(define-public crate-augmented-convert-sample-rate-1.4.0 (c (n "augmented-convert-sample-rate") (v "1.4.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.3.0") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "1y2asfks2jdlw521kh97wbfqk5khn1cd3akry8j6l1z123si7dq4")))

(define-public crate-augmented-convert-sample-rate-1.5.0 (c (n "augmented-convert-sample-rate") (v "1.5.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.4.0") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "1k5j9bix894w4q1b2k5kab14zibys801j94bkipd4wixvs65vqqi")))

(define-public crate-augmented-convert-sample-rate-1.6.0 (c (n "augmented-convert-sample-rate") (v "1.6.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.5.0") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "0f1wahjpw65l3j4aimr29rbizpkml59jflmrfv4rgn8rg9rmdx2r")))

(define-public crate-augmented-convert-sample-rate-1.7.0 (c (n "augmented-convert-sample-rate") (v "1.7.0") (d (list (d (n "audio-processor-testing-helpers") (r "^2.6.0") (d #t) (k 2)) (d (n "augmented_oscillator") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "samplerate") (r "^0.2.4") (d #t) (k 0)))) (h "0a91vd0crlcl58il5cy8jn5zzpq3fnyxpv95a6qd9jff79i0fhxx")))

