(define-module (crates-io au gm augmented-adsr-envelope) #:use-module (crates-io))

(define-public crate-augmented-adsr-envelope-0.1.0-alpha.2 (c (n "augmented-adsr-envelope") (v "0.1.0-alpha.2") (d (list (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "0b5cq66iszc66fwmjj66wb9jlr5a6q9bnmd6n1nyw7wviwwhrq3a")))

(define-public crate-augmented-adsr-envelope-0.1.0-alpha.3 (c (n "augmented-adsr-envelope") (v "0.1.0-alpha.3") (d (list (d (n "augmented-atomics") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "0f3ndby190shbalyglymj0vaxwbl8q8jzhzpn8f344fv4dlnmrw5")))

(define-public crate-augmented-adsr-envelope-0.1.0-alpha.4 (c (n "augmented-adsr-envelope") (v "0.1.0-alpha.4") (d (list (d (n "augmented-atomics") (r "^0.1.0-alpha.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "1868jskdxhaq2sk0czf5b9nniqxp74qk5iw1b2ihxjxpjw4mza6f")))

(define-public crate-augmented-adsr-envelope-0.1.0-alpha.5 (c (n "augmented-adsr-envelope") (v "0.1.0-alpha.5") (d (list (d (n "augmented-atomics") (r "^0.1.0-alpha.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "0hkwkjg87k4hlhdj4zdnavxfwbasdwsp6cyigw3mb6f5qxc4s312")))

(define-public crate-augmented-adsr-envelope-0.1.0-alpha.6 (c (n "augmented-adsr-envelope") (v "0.1.0-alpha.6") (d (list (d (n "augmented-atomics") (r "^0.1.0-alpha.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "19ml9nmjf403c5vps55l7xvrbpff16jaqg37qfida4rwvvqjijis")))

(define-public crate-augmented-adsr-envelope-0.1.0-alpha.7 (c (n "augmented-adsr-envelope") (v "0.1.0-alpha.7") (d (list (d (n "augmented-atomics") (r "^0.1.0-alpha.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "08w92bbs2s06zg4ga3660swpkdfm5dwz18xm094b1wg097krhzqi")))

(define-public crate-augmented-adsr-envelope-0.2.0 (c (n "augmented-adsr-envelope") (v "0.2.0") (d (list (d (n "augmented-atomics") (r "^0.1.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "0rfpb26cnkli5643j7cd6kf48bgbdc743hjm1mf0cvqxl51q79np")))

(define-public crate-augmented-adsr-envelope-0.3.0 (c (n "augmented-adsr-envelope") (v "0.3.0") (d (list (d (n "augmented-atomics") (r "^0.1.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "11b2qm00k2by9x8yv208nm2q9qy3c3jdivsdf0wanxv4nxkdr9wv")))

(define-public crate-augmented-adsr-envelope-0.4.0 (c (n "augmented-adsr-envelope") (v "0.4.0") (d (list (d (n "augmented-atomics") (r "^0.1.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "1jvv5kflip61vclq6pm3d7f4d697v4cz1j6zvpn1vrdac3zryf9a")))

(define-public crate-augmented-adsr-envelope-0.5.0 (c (n "augmented-adsr-envelope") (v "0.5.0") (d (list (d (n "augmented-atomics") (r "^0.2.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "1plsb2g0zbrmcv81yykc6pzif6d2ddnrpvjq9wwpj237k82c5a05")))

