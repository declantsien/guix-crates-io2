(define-module (crates-io au gm augmented_oscillator) #:use-module (crates-io))

(define-public crate-augmented_oscillator-1.0.0-alpha.1 (c (n "augmented_oscillator") (v "1.0.0-alpha.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)))) (h "1igf4rihqxg60hzy2mcbr78ql2aww1bqn0s1ka8f48p5qdxhrbbw")))

(define-public crate-augmented_oscillator-1.0.0-alpha.2 (c (n "augmented_oscillator") (v "1.0.0-alpha.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)))) (h "1bqqqh9v89w3gzr8yld7xzawrbyccm9w46piwjl7jhdrsc8639qa")))

(define-public crate-augmented_oscillator-1.0.0-alpha.3 (c (n "augmented_oscillator") (v "1.0.0-alpha.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)))) (h "15g4irgdspgn5jm2kh0wb8w810amzw8nrjwna6gv19v21zz2j0yh")))

(define-public crate-augmented_oscillator-1.0.0-alpha.4 (c (n "augmented_oscillator") (v "1.0.0-alpha.4") (d (list (d (n "augmented-atomics") (r "^0.1.0-alpha.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)))) (h "0mqk9fb7y4n0pidcz26m7h4j7jswvyczflh853l0dsj8nf2ywm65")))

(define-public crate-augmented_oscillator-1.0.0-alpha.5 (c (n "augmented_oscillator") (v "1.0.0-alpha.5") (d (list (d (n "augmented-atomics") (r "^0.1.0-alpha.7") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)))) (h "15wm61jg38n99rg8b7c2k3266v7lb3mzqychajjl3qg0m9n8v5kk")))

(define-public crate-augmented_oscillator-1.0.0-alpha.6 (c (n "augmented_oscillator") (v "1.0.0-alpha.6") (d (list (d (n "augmented-atomics") (r "^0.1.0-alpha.8") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)))) (h "13dxasdz09z7md7fp2ybr9qghicv2d3fx2bnkskafl0drsiyx6dk")))

(define-public crate-augmented_oscillator-1.1.0 (c (n "augmented_oscillator") (v "1.1.0") (d (list (d (n "augmented-atomics") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)))) (h "06wxg8p47ffw9awadsrlms4vafmh5j9y52gy0lvhag3wz268mzmg")))

(define-public crate-augmented_oscillator-1.2.0 (c (n "augmented_oscillator") (v "1.2.0") (d (list (d (n "augmented-atomics") (r "^0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)))) (h "1yzcggl28gpd0d489hfmpp2fd7gligm65k5d2kzxxy3d2r7viy28")))

(define-public crate-augmented_oscillator-1.2.1 (c (n "augmented_oscillator") (v "1.2.1") (d (list (d (n "augmented-atomics") (r "^0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)))) (h "0z5c2i1s0a4pcpqizagwgbzw2mnws972hy5vfqhjvc1v3fd6xdvh")))

(define-public crate-augmented_oscillator-1.3.0 (c (n "augmented_oscillator") (v "1.3.0") (d (list (d (n "augmented-atomics") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)))) (h "0iw557nz0qal8njsnb68k5yzcrpq0b82kcdkhahj2f4ygwwy5cs2")))

(define-public crate-augmented_oscillator-1.4.0 (c (n "augmented_oscillator") (v "1.4.0") (d (list (d (n "augmented-atomics") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)))) (h "1jhic72i5jg6zllwmaqywh03kqvnaj77zsiw114wv70ahkrksndi")))

