(define-module (crates-io au gm augmented-atomics) #:use-module (crates-io))

(define-public crate-augmented-atomics-0.1.0-alpha.3 (c (n "augmented-atomics") (v "0.1.0-alpha.3") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0f8mh441xxq8zx4pl1wqkc1fix7zvdqya3w8nz1l1cz6qad6ja75")))

(define-public crate-augmented-atomics-0.1.0-alpha.4 (c (n "augmented-atomics") (v "0.1.0-alpha.4") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0daf8na2gfp69mhkbvsfya4i1ab83yf9c47f5imwi77y0x0dnbki")))

(define-public crate-augmented-atomics-0.1.0-alpha.5 (c (n "augmented-atomics") (v "0.1.0-alpha.5") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "18zdyhdac3r4a2i2jn27x9p8b9vm9fxai52r7h3mc53y04yvn72i")))

(define-public crate-augmented-atomics-0.1.0-alpha.6 (c (n "augmented-atomics") (v "0.1.0-alpha.6") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1mxjihhfrbs760gw44g99g3i9gxr360p2z57j43l43kzv8syzmhv")))

(define-public crate-augmented-atomics-0.1.0-alpha.7 (c (n "augmented-atomics") (v "0.1.0-alpha.7") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0rgsd81fbgp6dllqj07a9n5ic1bsl4rbavsh5s60x79vxajgl55i")))

(define-public crate-augmented-atomics-0.1.0-alpha.8 (c (n "augmented-atomics") (v "0.1.0-alpha.8") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "12rxjn9k4kvip5gg1xri0b2sqz1wc70jdrsayjc40s4kypph8zjd")))

(define-public crate-augmented-atomics-0.1.1 (c (n "augmented-atomics") (v "0.1.1") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0yddg8ij6wvaw3nflckd6k35g3m2rwnb83pg70b9k1v4xnn5pj3y")))

(define-public crate-augmented-atomics-0.1.2 (c (n "augmented-atomics") (v "0.1.2") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "11m8zapjna8p0fmlpgallc230nim3x3s2686lqfik54rzcda3l99")))

(define-public crate-augmented-atomics-0.2.0 (c (n "augmented-atomics") (v "0.2.0") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1vil6vxf9x5nrj5j10365ix6cdhy34f1ymdq748298a95vjir564")))

