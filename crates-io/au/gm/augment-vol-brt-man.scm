(define-module (crates-io au gm augment-vol-brt-man) #:use-module (crates-io))

(define-public crate-augment-vol-brt-man-0.1.0-alpha (c (n "augment-vol-brt-man") (v "0.1.0-alpha") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18.1") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "1m046pqg2klpqqaw6l0qhjhlxcyzxlqz6ikn62ns442y3qgm8122")))

(define-public crate-augment-vol-brt-man-0.1.1-alpha (c (n "augment-vol-brt-man") (v "0.1.1-alpha") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18.1") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "0zrldc4rycjic9z9kpdkvr4zfly6vyary2ix6w6bc6b0m9miq24x")))

