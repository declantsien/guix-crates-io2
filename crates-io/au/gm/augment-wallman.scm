(define-module (crates-io au gm augment-wallman) #:use-module (crates-io))

(define-public crate-augment-wallman-0.1.0-alpha (c (n "augment-wallman") (v "0.1.0-alpha") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18.1") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "1fx9q7jvima70iv159y8lclradhqlxsdi8fxnp937nsbj2wq3knw")))

