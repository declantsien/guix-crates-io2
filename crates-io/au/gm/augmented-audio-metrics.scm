(define-module (crates-io au gm augmented-audio-metrics) #:use-module (crates-io))

(define-public crate-augmented-audio-metrics-1.0.0-alpha.1 (c (n "augmented-audio-metrics") (v "1.0.0-alpha.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1yf7p1wcxsymnjylsjqfq3rrwsmwag1zk1ax3h1xxmw3f5l0y3g1")))

(define-public crate-augmented-audio-metrics-1.0.0-alpha.2 (c (n "augmented-audio-metrics") (v "1.0.0-alpha.2") (d (list (d (n "audio-garbage-collector") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^1.0.0-alpha.5") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.0-alpha.6") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0rwnqcsmhj1f2mpmmkb7gi3qp9jb7xmcqdk3q7nykd3vzjmqzw77")))

(define-public crate-augmented-audio-metrics-1.0.0-alpha.3 (c (n "augmented-audio-metrics") (v "1.0.0-alpha.3") (d (list (d (n "audio-garbage-collector") (r "^1.0.0-alpha.4") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.0-alpha.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1kjcq0cmz9am5xsyylc85lzwgav9rl09z7lmc06fr7bbwkfygrwv")))

(define-public crate-augmented-audio-metrics-1.0.0-alpha.4 (c (n "augmented-audio-metrics") (v "1.0.0-alpha.4") (d (list (d (n "audio-garbage-collector") (r "^1.0.0-alpha.4") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.0-alpha.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0vhk513lakzssrl0cg5cja16rdsscilzp1awi36phbnl1fmbf7sy")))

(define-public crate-augmented-audio-metrics-1.0.0-alpha.5 (c (n "augmented-audio-metrics") (v "1.0.0-alpha.5") (d (list (d (n "audio-garbage-collector") (r "^1.0.0-alpha.5") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^1.0.0-alpha.7") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.0-alpha.8") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1gpkmw6hrykc4vbmv8kpnd3i42mrix0xynyv58w5y1r9ncxndrr6")))

(define-public crate-augmented-audio-metrics-1.0.0-alpha.6 (c (n "augmented-audio-metrics") (v "1.0.0-alpha.6") (d (list (d (n "audio-garbage-collector") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^1.0.0-alpha.8") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.0-alpha.8") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "13p4iglygq522hay06rgh61w49k69idsqd65bh28sxdz7l9bjcmb")))

(define-public crate-augmented-audio-metrics-1.1.0 (c (n "augmented-audio-metrics") (v "1.1.0") (d (list (d (n "audio-garbage-collector") (r "^1.1.0") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^1.1.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "06smh362lfrikdh5rglqmdgfxpr6mc40kc3zm3cww90bz2lv1qpr")))

(define-public crate-augmented-audio-metrics-1.2.0 (c (n "augmented-audio-metrics") (v "1.2.0") (d (list (d (n "audio-garbage-collector") (r "^1.1.0") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^2.1.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0rq9d81z5qkc21sabax0wchjkznp6da56faqj04ns3rfhw63hr2g")))

(define-public crate-augmented-audio-metrics-1.3.0 (c (n "augmented-audio-metrics") (v "1.3.0") (d (list (d (n "audio-garbage-collector") (r "^1.1.0") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^2.2.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1jldqimqhmqb8j5093l5z3fv1bkr5mv0c8526agbf8qf0zys0nvk")))

(define-public crate-augmented-audio-metrics-1.5.0 (c (n "augmented-audio-metrics") (v "1.5.0") (d (list (d (n "audio-garbage-collector") (r "^1.1.1") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^3.2.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0jp7sna6wyy0jqdm41p3qais5agwdhhp2ckqxr8icjwmaqyzc2hd")))

(define-public crate-augmented-audio-metrics-1.6.0 (c (n "augmented-audio-metrics") (v "1.6.0") (d (list (d (n "audio-garbage-collector") (r "^1.2.0") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^4.0.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0wg8sxix27b2v1hwhn6njdcwgj31qf08asdfln9hxcs0b6gf7h8r")))

(define-public crate-augmented-audio-metrics-1.7.0 (c (n "augmented-audio-metrics") (v "1.7.0") (d (list (d (n "audio-garbage-collector") (r "^1.2.0") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^4.1.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1pjdh76lsmvhhixp4q9q4wrmw5v27kxkkbxn0mkv3d148sywrsqh")))

(define-public crate-augmented-audio-metrics-1.8.0 (c (n "augmented-audio-metrics") (v "1.8.0") (d (list (d (n "audio-garbage-collector") (r "^1.2.0") (d #t) (k 0)) (d (n "audio-processor-traits") (r "^4.2.0") (d #t) (k 0)) (d (n "augmented-atomics") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "19d6ic1i299yw949cf3xm19s9vg61brq0zpnhs5jw67916fxwi7w")))

