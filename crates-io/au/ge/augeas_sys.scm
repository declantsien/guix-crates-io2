(define-module (crates-io au ge augeas_sys) #:use-module (crates-io))

(define-public crate-augeas_sys-0.0.1 (c (n "augeas_sys") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1whsvnlrljgg04wqm9n0hvwfxwn64j5avnkiphxpcr1pxsxsm14k")))

(define-public crate-augeas_sys-0.0.2 (c (n "augeas_sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "0czhyk19s7jbl1jw8fpls62ppz5dqlqfblipfqhlxksvb9bqkx6p")))

