(define-module (crates-io au ge augeas) #:use-module (crates-io))

(define-public crate-augeas-0.0.1 (c (n "augeas") (v "0.0.1") (d (list (d (n "augeas_sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "06cb504pn9v24qkskxjly559vb7njki7whl2hfdylzqcv9yfdjz4")))

