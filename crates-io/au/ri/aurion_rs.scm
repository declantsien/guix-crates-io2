(define-module (crates-io au ri aurion_rs) #:use-module (crates-io))

(define-public crate-aurion_rs-0.1.0 (c (n "aurion_rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dyer") (r "^3.3.2") (f (quote ("xpath"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "02kd20y2gna3s5jq46pcg5dsyc8z0w87bbqdmsj8xp2wjwazxbrm")))

(define-public crate-aurion_rs-0.2.0 (c (n "aurion_rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dyer") (r "^3.3.2") (f (quote ("xpath"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0ig2k4qzx6swz4411dvpiz651qmcq1xlg4plqcn2q2fzmpwpbgyg")))

(define-public crate-aurion_rs-0.2.1 (c (n "aurion_rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dyer") (r "^3.3.2") (f (quote ("xpath"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "12m7yml04xj22f9jp19pxj6dsarhbwrsi60was890520jkh0caf0")))

