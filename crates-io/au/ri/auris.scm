(define-module (crates-io au ri auris) #:use-module (crates-io))

(define-public crate-auris-0.1.0 (c (n "auris") (v "0.1.0") (d (list (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "0khdgc8mrpxkpqcxqlbmg74ddj9dsvkm05466bj3g1n7m8qc67xk")))

(define-public crate-auris-0.1.1 (c (n "auris") (v "0.1.1") (d (list (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "11i91sp6vq2y1zjjilna5y5zklp5pbb870x7q2k8j5pbpmhqm5na")))

(define-public crate-auris-0.1.2 (c (n "auris") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "119g0pajnqnf6aqvmd8b8bf3jmzimwj70x9xid4bi5an9wh50nz5")))

(define-public crate-auris-0.1.3 (c (n "auris") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "1l3hbwydh6hcqdigy4ppal8gj38m27gy5fnirdbz415n6q2r9dy9")))

