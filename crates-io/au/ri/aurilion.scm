(define-module (crates-io au ri aurilion) #:use-module (crates-io))

(define-public crate-aurilion-0.1.0 (c (n "aurilion") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01bx631j20pfypw5yvahlczyknnqw2f0gh3054hdwxrlz0vg046d")))

(define-public crate-aurilion-0.1.1 (c (n "aurilion") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mxli339m6kaxnfsfinywkcv70v284fkkazprllbznasnmxkzskh")))

