(define-module (crates-io au xt auxtrate) #:use-module (crates-io))

(define-public crate-auxtrate-0.1.0 (c (n "auxtrate") (v "0.1.0") (d (list (d (n "frame-support") (r "^21.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sp-weights") (r "^19.0.0") (d #t) (k 0)))) (h "1qg54sy7ksfzb97wf8981lwi2n9g29fff77rjld9cc280bx2630i")))

(define-public crate-auxtrate-0.1.1 (c (n "auxtrate") (v "0.1.1") (d (list (d (n "frame-support") (r "^21.0.0") (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "sp-weights") (r "^19.0.0") (k 0)))) (h "06zg8wxyry3sgxpazyswsk0nxh7yqblbk3s46jam26mzzc66m7ms") (f (quote (("try-runtime" "frame-support/try-runtime") ("std" "frame-support/std" "log/std" "sp-weights/std") ("runtime-benchmarks" "frame-support/runtime-benchmarks") ("default" "std"))))))

