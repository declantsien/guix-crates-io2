(define-module (crates-io au xt auxtools-impl) #:use-module (crates-io))

(define-public crate-auxtools-impl-0.1.0 (c (n "auxtools-impl") (v "0.1.0") (d (list (d (n "hex") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l4dqi3p4x7aj0bmhvpkhyg2whd796pllhw4hihbddvws09ia8w4") (y #t)))

