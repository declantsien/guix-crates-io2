(define-module (crates-io au th authy-rs) #:use-module (crates-io))

(define-public crate-authy-rs-0.1.0 (c (n "authy-rs") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "phonenumber") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pbhcf1clgydrja5a93pq0pqpvmi97343xrpkdclr0bc8vmx322y")))

