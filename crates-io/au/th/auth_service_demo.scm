(define-module (crates-io au th auth_service_demo) #:use-module (crates-io))

(define-public crate-auth_service_demo-0.1.0 (c (n "auth_service_demo") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ws1bkyr758sb4bs7xq6kj7dwhp00bvvl4kaw363a51s1csshxvm")))

(define-public crate-auth_service_demo-0.1.1 (c (n "auth_service_demo") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g44vp4xc0m06nr9kdkmr7m05pj27qrs05lzh7i6zzwqlbmx0v0z")))

(define-public crate-auth_service_demo-0.1.2 (c (n "auth_service_demo") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01n9m2jwakgzdh7pimcpgnvrfiib24lixvwln55hywcmvhsa326c")))

