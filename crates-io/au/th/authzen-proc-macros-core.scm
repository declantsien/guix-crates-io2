(define-module (crates-io au th authzen-proc-macros-core) #:use-module (crates-io))

(define-public crate-authzen-proc-macros-core-0.1.0-alpha.0 (c (n "authzen-proc-macros-core") (v "0.1.0-alpha.0") (d (list (d (n "authzen-proc-macro-util") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1krgbx78b5v95vfzbhaglyhs1qqiydhmxl2vfs9s6ckk1ijr2igx")))

