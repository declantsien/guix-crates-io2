(define-module (crates-io au th authzen-diesel-proc-macros) #:use-module (crates-io))

(define-public crate-authzen-diesel-proc-macros-0.1.0-alpha.0 (c (n "authzen-diesel-proc-macros") (v "0.1.0-alpha.0") (d (list (d (n "authzen-diesel-proc-macros-core") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "10qiw9crs6nkh6gswa9mf6f1v3ppyxp7f699b1v2p7pdzrlblr2h")))

