(define-module (crates-io au th auth-encrypt) #:use-module (crates-io))

(define-public crate-auth-encrypt-0.1.0 (c (n "auth-encrypt") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1s0mvh0z71pcx3nfipyjfm86jd15al6rm07wdycz2dzm2407w280")))

