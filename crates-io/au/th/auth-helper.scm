(define-module (crates-io au th auth-helper) #:use-module (crates-io))

(define-public crate-auth-helper-0.1.0 (c (n "auth-helper") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^7.2.0") (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("std"))) (k 0)) (d (n "rust-argon2") (r "^0.8.3") (k 0)) (d (n "serde") (r "^1.0.30") (f (quote ("std" "derive"))) (k 0)))) (h "187wxiblmp4wnk07ynjbrxx50grpsp1rfz2wzf55d8a154g276bj")))

(define-public crate-auth-helper-0.2.0 (c (n "auth-helper") (v "0.2.0") (d (list (d (n "jsonwebtoken") (r "^7.2.0") (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("std"))) (k 0)) (d (n "rust-argon2") (r "^0.8.3") (k 0)) (d (n "serde") (r "^1.0.30") (f (quote ("std" "derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)))) (h "1yxnxdn9im28ay95avjwg9n7xgfmq4p3dzlq2b6z2icr5z3mck2p")))

(define-public crate-auth-helper-0.3.0 (c (n "auth-helper") (v "0.3.0") (d (list (d (n "jsonwebtoken") (r "^8.1.0") (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("std"))) (k 0)) (d (n "rust-argon2") (r "^1.0.0") (k 0)) (d (n "serde") (r "^1.0.30") (f (quote ("std" "derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)))) (h "1sk65prnmyf06mxk0dl9msrxfyym4jcswib4ds1280zjfchfhs8m")))

