(define-module (crates-io au th authorized_keys) #:use-module (crates-io))

(define-public crate-authorized_keys-0.9.0 (c (n "authorized_keys") (v "0.9.0") (d (list (d (n "data-encoding") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (o #t) (d #t) (k 0)))) (h "07prirfij5j3zvss7xn5xrv377l6wal7yqbqq8bfz8fzrbp71ma3") (f (quote (("parse" "pest" "pest_derive") ("key_encoding" "data-encoding") ("default" "parse"))))))

(define-public crate-authorized_keys-0.10.0 (c (n "authorized_keys") (v "0.10.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "data-encoding") (r "^2.1") (o #t) (d #t) (k 0)))) (h "0g2rsasi0bqjkksnr40qg3bcx1cp8j1by34flv64d319drs1bjxh") (f (quote (("key_encoding" "data-encoding") ("default"))))))

(define-public crate-authorized_keys-0.11.0 (c (n "authorized_keys") (v "0.11.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "data-encoding") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.0.0-beta2") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0b1235d82h83clbsszpqnbjx4rhaxs2iw0dcy3y2f99ibn8rlj6d") (f (quote (("key_encoding" "data-encoding") ("default"))))))

(define-public crate-authorized_keys-1.0.0 (c (n "authorized_keys") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "data-encoding") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6") (d #t) (k 2)))) (h "01lpk7pbqsp4py1vc3nxj6sgzb845j32g1nnsz4c5lx9g95m7c4d") (f (quote (("key_encoding" "data-encoding") ("default"))))))

