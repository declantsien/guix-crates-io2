(define-module (crates-io au th authorized) #:use-module (crates-io))

(define-public crate-authorized-0.1.0 (c (n "authorized") (v "0.1.0") (h "1g46gm7dakgzdhr26ikx3dlnhc061swmy95bnzs08bqbif0dng97")))

(define-public crate-authorized-0.1.1 (c (n "authorized") (v "0.1.1") (d (list (d (n "authorized_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0imh9y608x803xx3qp74473q62nj3isfqs0wzq98xhrm8ihf0bw3") (f (quote (("with_serde" "serde") ("default"))))))

