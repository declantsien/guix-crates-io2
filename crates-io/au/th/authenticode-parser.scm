(define-module (crates-io au th authenticode-parser) #:use-module (crates-io))

(define-public crate-authenticode-parser-0.1.0 (c (n "authenticode-parser") (v "0.1.0") (d (list (d (n "authenticode-parser-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0w7d4938n8h23vvcrnjh14cn71l66c3b432diw1bkprvkmc9hxzk") (f (quote (("default") ("bindgen" "authenticode-parser-sys/bindgen"))))))

(define-public crate-authenticode-parser-0.2.0 (c (n "authenticode-parser") (v "0.2.0") (d (list (d (n "authenticode-parser-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1d7839fxycn1cpnmlc70mmnm4kjqm1d9vl01vx6sihpcd30ar1m8") (f (quote (("default") ("bindgen" "authenticode-parser-sys/bindgen"))))))

(define-public crate-authenticode-parser-0.2.1 (c (n "authenticode-parser") (v "0.2.1") (d (list (d (n "authenticode-parser-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0k7mdqphwp3sfw0hdpvw20qck6vazd4zvw7m25n369f4qgzp3hhh") (f (quote (("default") ("bindgen" "authenticode-parser-sys/bindgen"))))))

(define-public crate-authenticode-parser-0.2.2 (c (n "authenticode-parser") (v "0.2.2") (d (list (d (n "authenticode-parser-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0y6qsyvh1scgw984nz15ybmrff8wjjhg5rmwgp4pirnm6ns0wwsg") (f (quote (("default") ("bindgen" "authenticode-parser-sys/bindgen"))))))

(define-public crate-authenticode-parser-0.2.3 (c (n "authenticode-parser") (v "0.2.3") (d (list (d (n "authenticode-parser-sys") (r "^0.2.1") (d #t) (k 0)))) (h "040lcj96kj09mj4mlsb1rs2zpcygi6dpl8hs6wqqkps2ph4nv9ld") (f (quote (("default") ("bindgen" "authenticode-parser-sys/bindgen"))))))

(define-public crate-authenticode-parser-0.3.0 (c (n "authenticode-parser") (v "0.3.0") (d (list (d (n "authenticode-parser-sys") (r "^0.3.0") (d #t) (k 0)))) (h "03wgbvcnik48h61ylg0gra81a0c2swy0id6svsrbqg80nchdgn96") (f (quote (("default") ("bindgen" "authenticode-parser-sys/bindgen")))) (r "1.60")))

(define-public crate-authenticode-parser-0.3.1 (c (n "authenticode-parser") (v "0.3.1") (d (list (d (n "authenticode-parser-sys") (r "^0.3.1") (d #t) (k 0)))) (h "0qhklq0vjh06kll35knwa5gqb17gpahmkpmg7lhm9h5wzy1ri8h5") (f (quote (("default") ("bindgen" "authenticode-parser-sys/bindgen")))) (r "1.60")))

(define-public crate-authenticode-parser-0.3.2 (c (n "authenticode-parser") (v "0.3.2") (d (list (d (n "authenticode-parser-sys") (r "^0.3.1") (d #t) (k 0)))) (h "180qaldgp0a6w1gl5qfm9sm2dpfzva4sb2933b8xbgpd9dnk6mx1") (f (quote (("default") ("bindgen" "authenticode-parser-sys/bindgen")))) (r "1.60")))

(define-public crate-authenticode-parser-0.4.0 (c (n "authenticode-parser") (v "0.4.0") (d (list (d (n "authenticode-parser-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1yfrmms5xa5dc1vch7s22hm02cz4bsvj07vbm77byav74gb06hb0") (f (quote (("openssl-static" "authenticode-parser-sys/openssl-static") ("default") ("bindgen" "authenticode-parser-sys/bindgen")))) (r "1.65")))

(define-public crate-authenticode-parser-0.5.0 (c (n "authenticode-parser") (v "0.5.0") (d (list (d (n "authenticode-parser-sys") (r "^0.5.0") (d #t) (k 0)))) (h "11v057kpxpnjhaq29bc2cfqqf18wmpdh8iibclrpfggsd1hqlk5h") (f (quote (("openssl-static" "authenticode-parser-sys/openssl-static") ("default") ("bindgen" "authenticode-parser-sys/bindgen")))) (r "1.65")))

