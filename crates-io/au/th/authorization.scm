(define-module (crates-io au th authorization) #:use-module (crates-io))

(define-public crate-authorization-0.1.0 (c (n "authorization") (v "0.1.0") (d (list (d (n "handlebars") (r "^3.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xs3b0gh29xa2kdx8f2n597z6xiy57nlfalx4rn9bdxawzb866yf")))

(define-public crate-authorization-0.1.1 (c (n "authorization") (v "0.1.1") (d (list (d (n "handlebars") (r "^3.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nxi5wm9p9qwjn7fmz7h0h1wfclncdn4h7yh16z4r3fshcz6fp1i")))

(define-public crate-authorization-0.1.2 (c (n "authorization") (v "0.1.2") (d (list (d (n "handlebars") (r "^3.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ab354w1ha25d9ssqwk0dq0dlw7vcn8h8xmjf8kma9xmsvfy6803")))

