(define-module (crates-io au th authorized-keys-merger) #:use-module (crates-io))

(define-public crate-authorized-keys-merger-0.1.0 (c (n "authorized-keys-merger") (v "0.1.0") (h "1jvdx5jslcvmvdajld80b92kn3ikda112qx9m2h99m74c5idxq2b")))

(define-public crate-authorized-keys-merger-0.1.1 (c (n "authorized-keys-merger") (v "0.1.1") (h "0l9cyd9pmqm8ad77qc44k5ajwarbgq28w7575j3417xpywpmmxjf")))

(define-public crate-authorized-keys-merger-0.1.2 (c (n "authorized-keys-merger") (v "0.1.2") (h "1plgfinh65j3c7c9mw5qdllwpalngj1z23qic7msxd5afdvd7v2l")))

(define-public crate-authorized-keys-merger-0.1.3 (c (n "authorized-keys-merger") (v "0.1.3") (h "1sas7nds22pp49vxcmn7mzkmvf6khwq4rm4mni2vjyx32lcv5wfg")))

