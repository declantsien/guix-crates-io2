(define-module (crates-io au th authorizer-cli-test) #:use-module (crates-io))

(define-public crate-authorizer-cli-test-0.1.0 (c (n "authorizer-cli-test") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "rpassword") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0235s1h1nfw26x0hc7365653y0bp7fpndymdxpr3j2vrkwbcjpmq")))

