(define-module (crates-io au th auth_client_axum) #:use-module (crates-io))

(define-public crate-auth_client_axum-0.1.0 (c (n "auth_client_axum") (v "0.1.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w5kzs1gn95za4y4pacx0177dkb2y2gvmjsg2x6qcnvpmga82ll9")))

(define-public crate-auth_client_axum-0.1.1 (c (n "auth_client_axum") (v "0.1.1") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s4kp5zcq7vk60fnwwpi0mk7hw3a2ncvxinsjzplr9dbz5vrxq74")))

(define-public crate-auth_client_axum-0.1.2 (c (n "auth_client_axum") (v "0.1.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "090r9xbkif9l6jn6prl21a00l8pk6mj59bhvr0ix0cvr61blaivy")))

(define-public crate-auth_client_axum-0.1.3 (c (n "auth_client_axum") (v "0.1.3") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14i6s3dg6b6ffcfqv52qdd9x943w2fyzs6ksb1g3xgkk45fjn4iw")))

(define-public crate-auth_client_axum-0.1.4 (c (n "auth_client_axum") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vpdbjhhd2q5jw11sqh0magsm5h6fpdgzklx6fscng9cpxmrck4s")))

(define-public crate-auth_client_axum-0.1.5 (c (n "auth_client_axum") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kadmnx4ipd7pjrz2js7j9vm0ly4drzgblny59kpy6axmd0s0cll")))

(define-public crate-auth_client_axum-0.1.6 (c (n "auth_client_axum") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cnyyzs8gyrxkg4nd2942qyi5yj94xw3q5mhm7iig9gibf7v4vfj")))

