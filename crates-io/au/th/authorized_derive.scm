(define-module (crates-io au th authorized_derive) #:use-module (crates-io))

(define-public crate-authorized_derive-0.1.0 (c (n "authorized_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "1szq5s6mq39kminm62ahgkfzqqj5hv0ns9z8n8d6b5lyfbmlyph3")))

