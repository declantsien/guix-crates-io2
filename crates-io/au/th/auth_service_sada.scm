(define-module (crates-io au th auth_service_sada) #:use-module (crates-io))

(define-public crate-auth_service_sada-0.1.0 (c (n "auth_service_sada") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0376fm87hk9w7x7hy448bkxh0sa64wr05s7895kbx4y4y2g617vn")))

(define-public crate-auth_service_sada-0.1.1 (c (n "auth_service_sada") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0f9mjxc5vbk62knjyyib5i55n8z80pjp3pcm550axvd025qb9g1g")))

