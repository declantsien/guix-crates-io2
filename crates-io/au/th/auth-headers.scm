(define-module (crates-io au th auth-headers) #:use-module (crates-io))

(define-public crate-auth-headers-0.1.0 (c (n "auth-headers") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 2)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ynbxqw27c1kgczfz7l55n5hz166zbl6zmxvysmiw43i3jlyay81")))

