(define-module (crates-io au th authy) #:use-module (crates-io))

(define-public crate-authy-0.9.0 (c (n "authy") (v "0.9.0") (d (list (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0mqz7c4f8qrw4fvnz2lfn4kk1p7wdrx29lzw8bwlc0gnr7lg4xaq")))

(define-public crate-authy-0.9.1 (c (n "authy") (v "0.9.1") (d (list (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0yd98lgh1fa1ryhafa7xc6xyirnkdbi9av5ysahspvm15dj6brgc")))

(define-public crate-authy-0.9.2 (c (n "authy") (v "0.9.2") (d (list (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0jc67qa0ih91aq6lp5fkdwbhx856l2rjm4wra7lfgnvmi32x25x7")))

(define-public crate-authy-0.9.3 (c (n "authy") (v "0.9.3") (d (list (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0fpv3vzl946cyvw58ijn1ig68ydlnyi40fy8mwnmzj30xwxzsg0s")))

(define-public crate-authy-0.9.5 (c (n "authy") (v "0.9.5") (d (list (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kbzwczs3wi8kdi3v3q1fdzydszm9pm21c25m2dy2bv00rbxgq1c")))

(define-public crate-authy-0.9.6 (c (n "authy") (v "0.9.6") (d (list (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04jj42ss66a7lmsdqn53wg9f76vxnxxpxfkbgbakvmh7nnsml4ml")))

(define-public crate-authy-0.9.7 (c (n "authy") (v "0.9.7") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x649y8crw5ip1d10lp252wcqbg87318r7d5f6c68z32b3j6b6ja")))

(define-public crate-authy-0.9.8 (c (n "authy") (v "0.9.8") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "1cnbqwzv0drha5436w5djwa1in1pg3a1bfzwz1ywr6z9d260cfdi")))

