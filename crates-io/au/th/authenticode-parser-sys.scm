(define-module (crates-io au th authenticode-parser-sys) #:use-module (crates-io))

(define-public crate-authenticode-parser-sys-0.1.0 (c (n "authenticode-parser-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "13ra7y0p3hbwd63hvf9vn7bf7jmiilz9ml4x76j44d3nlbl1xrwk") (f (quote (("default")))) (s 2) (e (quote (("bindgen" "dep:bindgen"))))))

(define-public crate-authenticode-parser-sys-0.2.1 (c (n "authenticode-parser-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0sljix75njb5lxqxfayda1sk4xy37gjgpjrmgzyprf73acxxr9lv") (f (quote (("default")))) (s 2) (e (quote (("bindgen" "dep:bindgen"))))))

(define-public crate-authenticode-parser-sys-0.2.3 (c (n "authenticode-parser-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1z0yprvhzl9cks9h23gp6c8gxxvyz77836dvvaqbgv6smzq8aw27") (f (quote (("default")))) (s 2) (e (quote (("bindgen" "dep:bindgen"))))))

(define-public crate-authenticode-parser-sys-0.3.0 (c (n "authenticode-parser-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1hlrd0v7sdw05y8f9yyvcvmhj54zvl4k4i3srn1z0zdrjd23lxx1") (f (quote (("default")))) (l "authenticode-parser") (s 2) (e (quote (("bindgen" "dep:bindgen")))) (r "1.60")))

(define-public crate-authenticode-parser-sys-0.3.1 (c (n "authenticode-parser-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1rs340n92d4nkgd6zdljqsmd8aaxj0nm6d3ahdqvkxksga1hd303") (f (quote (("default")))) (l "authenticode-parser") (s 2) (e (quote (("bindgen" "dep:bindgen")))) (r "1.60")))

(define-public crate-authenticode-parser-sys-0.4.0 (c (n "authenticode-parser-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0r13drvy4kd6yh8czpv24d3d9v611gjkzj306vvnri4h8zd44ji1") (f (quote (("openssl-static") ("default")))) (l "authenticode-parser") (s 2) (e (quote (("bindgen" "dep:bindgen")))) (r "1.60")))

(define-public crate-authenticode-parser-sys-0.5.0 (c (n "authenticode-parser-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0drp9xb6n8k3w62wdp5bkmrd227b3d80z8vvv903f3z9g24w9znn") (f (quote (("openssl-static") ("default")))) (l "authenticode-parser") (s 2) (e (quote (("bindgen" "dep:bindgen")))) (r "1.60")))

