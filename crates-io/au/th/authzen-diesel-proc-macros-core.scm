(define-module (crates-io au th authzen-diesel-proc-macros-core) #:use-module (crates-io))

(define-public crate-authzen-diesel-proc-macros-core-0.1.0-alpha.0 (c (n "authzen-diesel-proc-macros-core") (v "0.1.0-alpha.0") (d (list (d (n "authzen-proc-macro-util") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (k 0)) (d (n "uuid") (r "^1") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1gdbm3ggm7vl4vibn80924l5nv1bzw3pqivx95fx6s1jzix3y26a")))

