(define-module (crates-io au th authenticator-cli) #:use-module (crates-io))

(define-public crate-authenticator-cli-0.1.0 (c (n "authenticator-cli") (v "0.1.0") (d (list (d (n "authenticator") (r "^0.3.2-dev.1") (f (quote ("crypto_openssl"))) (k 0) (p "authenticator-ctap2-2021")) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (f (quote ("env-filter" "fmt"))) (d #t) (k 0)))) (h "1wsiqw6qzi7ymhkysxxlyz0bikmcvp96pmdlsa2na6p2669kxc6s")))

