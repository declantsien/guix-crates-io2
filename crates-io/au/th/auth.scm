(define-module (crates-io au th auth) #:use-module (crates-io))

(define-public crate-auth-0.1.0 (c (n "auth") (v "0.1.0") (d (list (d (n "grpc") (r "^0.3.0") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.0") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.14") (d #t) (k 0)))) (h "0cijhlziqjhs6j2rxz1r5p4asysav9a9gn9q9szkm9l1bsq1gqic")))

