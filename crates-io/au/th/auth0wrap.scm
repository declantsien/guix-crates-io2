(define-module (crates-io au th auth0wrap) #:use-module (crates-io))

(define-public crate-auth0wrap-0.1.0 (c (n "auth0wrap") (v "0.1.0") (d (list (d (n "figment") (r "^0.10.12") (f (quote ("toml" "json" "env"))) (k 0)) (d (n "mockito") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rubedo") (r "^0.4.0") (d #t) (k 0)) (d (n "rubedo-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0c1sxhrfkm6lq56ng1kg3gsivg0nkl4wi3gsscg8cc65rq9g4vgp")))

(define-public crate-auth0wrap-0.1.1 (c (n "auth0wrap") (v "0.1.1") (d (list (d (n "figment") (r "^0.10.12") (f (quote ("toml" "json" "env"))) (k 0)) (d (n "mockito") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rubedo") (r "^0.4.0") (d #t) (k 0)) (d (n "rubedo-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "11d8zxr3rc3p0194h9c3i1q1nb6wbhg3wfs9s5bgz7cqask7xkfr")))

