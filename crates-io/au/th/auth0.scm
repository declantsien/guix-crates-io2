(define-module (crates-io au th auth0) #:use-module (crates-io))

(define-public crate-auth0-0.1.0 (c (n "auth0") (v "0.1.0") (h "0xw8qxa3ysfbj4gbp0lfi4wv3nq06048knwzfqcpp820fi4pdmnp")))

(define-public crate-auth0-0.3.0 (c (n "auth0") (v "0.3.0") (d (list (d (n "mockito") (r "^0.27.0") (k 2)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "159g0kxl8g1igizi9zb5f3m9a6p50nccsn3jg4y7ix2sr6hvsicc")))

