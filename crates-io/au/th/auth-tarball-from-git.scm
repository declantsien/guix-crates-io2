(define-module (crates-io au th auth-tarball-from-git) #:use-module (crates-io))

(define-public crate-auth-tarball-from-git-0.1.0 (c (n "auth-tarball-from-git") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("macros" "rt-multi-thread" "process" "fs"))) (d #t) (k 0)))) (h "0bg0b1p5cr0w5pfqp32bp82y34042aydjfi76w4amj0k02vikh0l")))

(define-public crate-auth-tarball-from-git-0.2.0 (c (n "auth-tarball-from-git") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("macros" "rt-multi-thread" "process" "fs"))) (d #t) (k 0)))) (h "0b5asy2n0x4rdfrsfjzs6r5j8gbm01sw6nlr01qwzmv1f4q9ldx3")))

