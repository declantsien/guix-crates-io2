(define-module (crates-io au th authorize) #:use-module (crates-io))

(define-public crate-authorize-0.1.0 (c (n "authorize") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2.36") (o #t) (d #t) (k 0)))) (h "1rzla4fc2xnwx1da9xwc3sa43jppkjh3d33q4iaxkmc5b99wxfqm") (f (quote (("hashing" "rust-crypto") ("default" "hashing"))))))

(define-public crate-authorize-0.1.1 (c (n "authorize") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2.36") (o #t) (d #t) (k 0)))) (h "0gb8874kcaaw8v11y67m2nxsi69lxr7div4lnij933wx1jgknfak") (f (quote (("hashing" "rust-crypto") ("default" "hashing"))))))

(define-public crate-authorize-0.1.2 (c (n "authorize") (v "0.1.2") (d (list (d (n "rust-crypto") (r "^0.2.36") (o #t) (d #t) (k 0)))) (h "0xh81gpzhak3g74i7dzx456b3kndk8xryv3bfzhl3df4kp3bjprh") (f (quote (("hashing" "rust-crypto") ("default" "hashing"))))))

(define-public crate-authorize-0.1.3 (c (n "authorize") (v "0.1.3") (h "1y9hlcl00c2mw3s97sjy12m0zwwniix0jg8lf6q62rfff2f9sl7b")))

(define-public crate-authorize-0.1.4 (c (n "authorize") (v "0.1.4") (h "09ldy83mnb8y040bl48phhaidzf0w9d5mvq5bm97isxwi89pfcaw")))

