(define-module (crates-io au th auth_service15285) #:use-module (crates-io))

(define-public crate-auth_service15285-0.1.0 (c (n "auth_service15285") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0k7bp102lbkg4b6d3psnq854y341baa7a1kdmnsbf8qwxz6syhbg") (y #t)))

(define-public crate-auth_service15285-0.1.1 (c (n "auth_service15285") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1avh1c9iziil1l5w7vjpsy3967f0mynzj39plzm4lkcmbwc5ggb0")))

