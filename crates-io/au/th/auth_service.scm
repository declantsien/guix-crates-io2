(define-module (crates-io au th auth_service) #:use-module (crates-io))

(define-public crate-auth_service-0.1.0 (c (n "auth_service") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "03jgfmspip3k63y13hj0ns20fzphzxi09v6n0cwcgasp89rx2a30")))

(define-public crate-auth_service-0.1.1 (c (n "auth_service") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "08lfknd41k7rh42zllvq87w1kfp4ywbrkn7ahyxsnnnl12frp72k")))

