(define-module (crates-io au th authzen-proc-macro-util) #:use-module (crates-io))

(define-public crate-authzen-proc-macro-util-0.1.0-alpha.0 (c (n "authzen-proc-macro-util") (v "0.1.0-alpha.0") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "prettyplease") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "13d5q340i4z7m853jrpnqss9plmzm897wkrv10jpc320yqwkl4lx")))

