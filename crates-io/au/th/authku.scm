(define-module (crates-io au th authku) #:use-module (crates-io))

(define-public crate-authku-0.1.0 (c (n "authku") (v "0.1.0") (d (list (d (n "asession") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "09ahj9kskra6hf3hmb16slsq05zi5iy4zpp8yrxa22drgvr7430w")))

(define-public crate-authku-0.1.1 (c (n "authku") (v "0.1.1") (d (list (d (n "asession") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "115dly80jl8rwmzmmnsf2g0gfwf4qlvz9ng9v4z1p1bwhb267zni")))

(define-public crate-authku-0.1.2 (c (n "authku") (v "0.1.2") (d (list (d (n "asession") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1dx9ya3limsik9jfv0g9921fkmyfj7gl1bfnxbvarkd6ysmdwr4v") (y #t)))

(define-public crate-authku-0.1.3 (c (n "authku") (v "0.1.3") (d (list (d (n "asession") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0na735zsvjp4sj5lsaxh2f80v3wb23g0cwm3cbjpcxz63bfl9i2p") (y #t)))

(define-public crate-authku-0.1.4 (c (n "authku") (v "0.1.4") (d (list (d (n "asession") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0b43gibcy12z66avr0q2dnz1khnh4rsv8siqizdhs4kvjal3mlxy")))

