(define-module (crates-io au th authentik-rust) #:use-module (crates-io))

(define-public crate-authentik-rust-0.0.1 (c (n "authentik-rust") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "12dg24xp8iqnvpbp3c72j6ffav5h3mda2ncqn1acb0ycv6c79df8")))

