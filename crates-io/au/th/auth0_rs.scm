(define-module (crates-io au th auth0_rs) #:use-module (crates-io))

(define-public crate-auth0_rs-0.1.0 (c (n "auth0_rs") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04wgp913s09jfa343brm4czibcjlaqhyh2wm91646is6517bz981")))

(define-public crate-auth0_rs-0.2.0 (c (n "auth0_rs") (v "0.2.0") (d (list (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1nmhgrkqjw7wf71hz40a7z8al36j3ls1gbpz0y4gsgj2sps4vvhp")))

