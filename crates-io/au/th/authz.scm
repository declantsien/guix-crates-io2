(define-module (crates-io au th authz) #:use-module (crates-io))

(define-public crate-authz-0.1.0 (c (n "authz") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1bh08d1p5zk3kgin6i966lnpmmvvmfblvd85j55ca1j3gfgdavbp") (f (quote (("nightly" "serde_derive") ("default" "serde_codegen")))) (y #t)))

(define-public crate-authz-0.1.1 (c (n "authz") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.9") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "^0.9.5") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "0pglayy2fb6kjkwp4smnr5m2234kfrjmw3494an8x7lm90wxasa9") (f (quote (("default" "serde_codegen"))))))

(define-public crate-authz-0.1.2 (c (n "authz") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "1894lhj0a9a5vf5xfmsx0ssa0012ss1wq9zvpmm4la75rc4iaqf5")))

(define-public crate-authz-0.1.3 (c (n "authz") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "0pag4j8f9z4bzy7xr22q5k2qnsmbzzi6rpbgsrfs8pi1m706r3v8")))

(define-public crate-authz-0.1.4 (c (n "authz") (v "0.1.4") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "0fg42c7vs6gknb50a32g7ijd244xadfyb1x7ypbs53qf401l69bs")))

(define-public crate-authz-0.1.5 (c (n "authz") (v "0.1.5") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "0vihn4d3qycm4ifmlimnbh9a19qy6fy15xfd9pdwmxd0ina5r9g1")))

