(define-module (crates-io au th authenticator-rs) #:use-module (crates-io))

(define-public crate-authenticator-rs-0.1.0 (c (n "authenticator-rs") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "ureq") (r "^1.5.5") (d #t) (k 2)))) (h "09i6z6a2nfl39d6cdqswd59hm6hp2x5a11rhfygrmgxawk14y3l8") (f (quote (("with-qrcode" "qrcode") ("default"))))))

(define-public crate-authenticator-rs-0.2.0 (c (n "authenticator-rs") (v "0.2.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "ureq") (r "^1.5.5") (d #t) (k 2)))) (h "1pw73dc6vxq5hh2sl18zcqvfpz3jzbngxq3dql4k1b5wq4nq2ypz") (f (quote (("with-qrcode" "qrcode") ("default"))))))

