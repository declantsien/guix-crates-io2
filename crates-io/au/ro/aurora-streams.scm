(define-module (crates-io au ro aurora-streams) #:use-module (crates-io))

(define-public crate-aurora-streams-0.2.0 (c (n "aurora-streams") (v "0.2.0") (d (list (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "redis") (r "^0.24.0") (f (quote ("tokio-comp"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s31wn2bjc2s5v0q55yrvccnrkx64fwrmcyi5hkdcsq81rgn2bnp") (f (quote (("event-routing" "redis") ("default"))))))

(define-public crate-aurora-streams-1.0.0 (c (n "aurora-streams") (v "1.0.0") (d (list (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "redis") (r "^0.24.0") (f (quote ("tokio-comp"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j0vlf1vw6yivf597sz873b54qi3r77bqhrddp3jcx513l1k8p26") (f (quote (("event-routing" "redis") ("default"))))))

(define-public crate-aurora-streams-1.0.1 (c (n "aurora-streams") (v "1.0.1") (d (list (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "redis") (r "^0.24.0") (f (quote ("tokio-comp"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y8ag8lgn25kx3i69hhf5077hsfnrgl6ab08kn6rxcp9y63r2ni0") (f (quote (("event-routing" "redis") ("default"))))))

