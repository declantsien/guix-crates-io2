(define-module (crates-io au ro aurora-engine-modexp) #:use-module (crates-io))

(define-public crate-aurora-engine-modexp-1.0.0 (c (n "aurora-engine-modexp") (v "1.0.0") (d (list (d (n "hex") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "ibig") (r "^0.3") (f (quote ("num-traits"))) (o #t) (k 0)) (d (n "num") (r "^0.4") (f (quote ("alloc"))) (k 0)))) (h "0w3g78dcsqq8zrz8in6ybfjp7pzzsj7fnj89cyhgqf71x63avb5z") (f (quote (("std" "num/std" "hex/std") ("default" "std") ("bench" "ibig"))))))

(define-public crate-aurora-engine-modexp-1.1.0 (c (n "aurora-engine-modexp") (v "1.1.0") (d (list (d (n "hex") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "ibig") (r "^0.3") (f (quote ("num-traits"))) (o #t) (k 0)) (d (n "num") (r "^0.4") (f (quote ("alloc"))) (k 0)))) (h "1rg7f2v0280drbwkc73a0n98dkf5k5jzlx5vzcslylhyhl97gvqa") (f (quote (("std" "num/std" "hex/std") ("default" "std") ("bench" "ibig"))))))

