(define-module (crates-io au ro aurochs) #:use-module (crates-io))

(define-public crate-aurochs-0.1.0 (c (n "aurochs") (v "0.1.0") (h "1l0ahplrzy6dvfdlrvsv2341jwqcs0myh7bmfia0wff7dx7l0zvf")))

(define-public crate-aurochs-0.1.1 (c (n "aurochs") (v "0.1.1") (h "1nksksknsmcg16xd8r0j61kf479d8qzaznp23fqxf6b84fpsz40d")))

(define-public crate-aurochs-0.1.2 (c (n "aurochs") (v "0.1.2") (h "08k0ykg72cgq3s23rb0pcqlfi3wd5yb4rf80616mmlms162n5v1p")))

