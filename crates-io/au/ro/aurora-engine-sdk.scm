(define-module (crates-io au ro aurora-engine-sdk) #:use-module (crates-io))

(define-public crate-aurora-engine-sdk-1.0.0 (c (n "aurora-engine-sdk") (v "1.0.0") (d (list (d (n "aurora-engine-types") (r "^1.0.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)) (d (n "borsh") (r "^0.10") (k 0)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "sha3") (r "^0.10") (k 0)))) (h "1yk1ik55ql8fn43jds5w7x3syqnnw3jbl8ksm41dryxiyjdnm9b8") (f (quote (("testnet") ("std" "aurora-engine-types/std" "borsh/std" "sha3/std" "sha2/std" "base64/std") ("mainnet") ("log") ("contract") ("all-promise-actions"))))))

(define-public crate-aurora-engine-sdk-1.1.0 (c (n "aurora-engine-sdk") (v "1.1.0") (d (list (d (n "aurora-engine-types") (r "^1.1.0") (k 0)) (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "sha3") (r "^0.10") (k 0)))) (h "1a3avl3l9y25y0z9v6pixczgslcw3l5ym7lpmdphfprvykljnvjv") (f (quote (("testnet") ("std" "aurora-engine-types/std" "sha3/std" "sha2/std" "base64/std") ("mainnet") ("log") ("contract") ("all-promise-actions"))))))

