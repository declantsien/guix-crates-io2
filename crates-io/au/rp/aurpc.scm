(define-module (crates-io au rp aurpc) #:use-module (crates-io))

(define-public crate-aurpc-0.1.0 (c (n "aurpc") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.1") (d #t) (k 0)) (d (n "async-std") (r "^1.6.1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)))) (h "14q3dfj57i3zkxcp3b3q34cy6ja0hv1nwgz09rnr161jpqnlcncw")))

(define-public crate-aurpc-0.1.1 (c (n "aurpc") (v "0.1.1") (d (list (d (n "async-std") (r "^1.6.1") (d #t) (k 0)) (d (n "async-std") (r "^1.6.1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)))) (h "1azan475s0r5kaafif787j8p869vpb9ni7ns1d07y5790xb13lm9")))

(define-public crate-aurpc-0.2.0 (c (n "aurpc") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.1") (d #t) (k 0)) (d (n "async-std") (r "^1.6.1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)))) (h "0vj0j19n4v06y95w80js6jspadwg0jz2k3ys1banhjc7bawdgsda")))

(define-public crate-aurpc-0.3.0 (c (n "aurpc") (v "0.3.0") (d (list (d (n "async-std") (r "^1.6.1") (d #t) (k 0)) (d (n "async-std") (r "^1.6.1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)))) (h "1asbg4xkas4xzpkasw8658qqjp3rmpz2451qw6xnka4wzgkj1yaw")))

(define-public crate-aurpc-0.3.1 (c (n "aurpc") (v "0.3.1") (d (list (d (n "async-std") (r "^1.6.1") (d #t) (k 0)) (d (n "async-std") (r "^1.6.1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)))) (h "1q2pwc4cs24zsq58pfbj9rm82dk3msdhpq2kkfcg4s8469jzhrf0")))

