(define-module (crates-io au gh aught_common) #:use-module (crates-io))

(define-public crate-aught_common-0.1.0 (c (n "aught_common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0v2p3kap5qzym0cahxybyx6wx1fbhzidfxj2y8s9hpjjaf1s43yk")))

