(define-module (crates-io au gh augh) #:use-module (crates-io))

(define-public crate-augh-0.1.0 (c (n "augh") (v "0.1.0") (h "1vgvhcz91gzl5h4vcg8v8dypxrmma9fzfryp0l13hg80v44axf99")))

(define-public crate-augh-0.1.1 (c (n "augh") (v "0.1.1") (h "1sm648n12d4jny3dqxcnmbvc6zfrzlqai01z4qjwqjs7qg3brn31")))

