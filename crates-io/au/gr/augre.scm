(define-module (crates-io au gr augre) #:use-module (crates-io))

(define-public crate-augre-0.1.0 (c (n "augre") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chatgpt_rs") (r "^1.2.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "termimad") (r "^0.25.2") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0zv9shnlaq368lx5s4by5bmdpz9ghi32hwrljgzk6rfqmq3pyp1m")))

