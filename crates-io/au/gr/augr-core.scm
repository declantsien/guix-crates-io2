(define-module (crates-io au gr augr-core) #:use-module (crates-io))

(define-public crate-augr-core-0.2.0 (c (n "augr-core") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1jwnh2hfn1rna6f3gwxby10v0v71sh54ilj664bkq7hzrmv4hwgc")))

(define-public crate-augr-core-0.2.1 (c (n "augr-core") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "flame") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "16lrjaj8s1qx3gps2kjsgw6q87cs6rp1n3bqwwls3q3i63yw1in0") (f (quote (("flame_it" "flame" "flamer") ("default"))))))

