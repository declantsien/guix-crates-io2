(define-module (crates-io au bu auburn) #:use-module (crates-io))

(define-public crate-auburn-0.1.0 (c (n "auburn") (v "0.1.0") (d (list (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "lk_math") (r "^0.4.0") (d #t) (k 0)) (d (n "round-to") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)))) (h "0haxjrwxpqdgmb9s1iipncxlgcwp13kw3za3m8m24spqi2dmwd4p") (f (quote (("penetrates_dir"))))))

(define-public crate-auburn-0.1.1 (c (n "auburn") (v "0.1.1") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "lk_math") (r "^0.4.0") (d #t) (k 0)) (d (n "round-to") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)))) (h "06pc9wls015bln9s5a366gwfmgpflyc4ll9b1bfyn7763mcy040i") (f (quote (("penetrates_dir"))))))

(define-public crate-auburn-0.1.2 (c (n "auburn") (v "0.1.2") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "lk_math") (r "^0.4.0") (d #t) (k 0)) (d (n "round-to") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)))) (h "0san30py5pmdxhdzvgrj0ipc4rxccbn6wiic6igcjd07qcwdmlp7") (f (quote (("penetrates_dir"))))))

(define-public crate-auburn-0.1.3 (c (n "auburn") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "lk_math") (r "^0.4.0") (d #t) (k 0)) (d (n "round-to") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)))) (h "14ijfp34q1bhvniwzqfrdals5liw66fk0yphphr0af21nb6qy50g") (s 2) (e (quote (("bevy" "dep:bevy"))))))

