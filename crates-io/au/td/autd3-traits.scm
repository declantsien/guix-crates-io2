(define-module (crates-io au td autd3-traits) #:use-module (crates-io))

(define-public crate-autd3-traits-1.0.0 (c (n "autd3-traits") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "162m7k352r9sjl4aapd2j53k5rky4jwp4ywlyqardll789szid1l")))

(define-public crate-autd3-traits-1.3.0 (c (n "autd3-traits") (v "1.3.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1wwbriahfag6qr4lpy8b51n13dsyqrp2164562z1s874y0ib15al")))

(define-public crate-autd3-traits-1.6.0 (c (n "autd3-traits") (v "1.6.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1bg5g2i7brk17mc3wsr7ccm2hg9x61blga36yyi05a70928llxg4")))

(define-public crate-autd3-traits-1.8.0 (c (n "autd3-traits") (v "1.8.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1lcnlnvr9vyxc62m78l8z3ska2m7hr3x7359w80k668462kdsig8")))

(define-public crate-autd3-traits-1.9.0 (c (n "autd3-traits") (v "1.9.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "1ymwqk8vl8xk53p9czf21jm703acqfpjz27l04pvxk7v4q0y2rbv")))

(define-public crate-autd3-traits-1.9.3 (c (n "autd3-traits") (v "1.9.3") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "09l5awn002pz1fc299njc56ji89hl9ng747m2bwh836a41kz0fd0")))

(define-public crate-autd3-traits-1.10.0 (c (n "autd3-traits") (v "1.10.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "041y6i82f462w61ca5mc5wglr1fm9n9nls5dhb7vq39jw8ajkicp")))

(define-public crate-autd3-traits-2.0.0 (c (n "autd3-traits") (v "2.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1jgfm5qwapahn2pk811w3i5y4pqkr6f2zj7h1bq0yhp3qz5z4n29")))

(define-public crate-autd3-traits-2.0.1 (c (n "autd3-traits") (v "2.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1y9671vd3vvn5nj4yh7aal21jivy7dskmjs6h2iig9rl85w4bvww")))

(define-public crate-autd3-traits-2.0.2 (c (n "autd3-traits") (v "2.0.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "18cl1l3nx5mq3gcdgkkj832174m53jkqdhx0sfn9vn9q0sc90plc")))

(define-public crate-autd3-traits-2.0.3 (c (n "autd3-traits") (v "2.0.3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0q8wlnqczrq5v0l5ixyirp4dppmipp11q49rs8ixcylzq4fjr4kz")))

(define-public crate-autd3-traits-2.1.0 (c (n "autd3-traits") (v "2.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1sgvidhd9knl7xvy7hac2cxg9r13nw88klc4wv5cdp6xwj9hgk7j")))

(define-public crate-autd3-traits-2.2.0 (c (n "autd3-traits") (v "2.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0wx33m02q4sdpn8marvn1xygmqljjcac5hp86p2qcl6psqv6l1hw")))

(define-public crate-autd3-traits-2.2.1 (c (n "autd3-traits") (v "2.2.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "00zgn9z5fjsvpmixm3dn7mhyr9kza0nkz99w37s3dzisj6srqb34")))

(define-public crate-autd3-traits-2.2.2 (c (n "autd3-traits") (v "2.2.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0z6bilch45b0f5pkwpqgj20j5fqh8iv7zqghdlpvmhl9jww6b59h")))

(define-public crate-autd3-traits-2.3.0 (c (n "autd3-traits") (v "2.3.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1pzbbsrxhqbydc3zhyx3siiq8lbr09kzsj5ldiccnpd6r7c81xsd")))

(define-public crate-autd3-traits-2.3.1 (c (n "autd3-traits") (v "2.3.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "05w44yn8021x5lhc6ya0d3yj6v0hl0n2gbas7v714zfbmdk599ci")))

(define-public crate-autd3-traits-2.4.1 (c (n "autd3-traits") (v "2.4.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0nsm2pfgsap9zp0mdx61lsqa3slx8kfixjbppxmfw76w9jrkvpzv")))

(define-public crate-autd3-traits-2.4.2 (c (n "autd3-traits") (v "2.4.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1k4hzsbd1z24snl5l1wrrlnkdsh1q461spxlkf3ppwal8cdg4ci4")))

(define-public crate-autd3-traits-2.4.3 (c (n "autd3-traits") (v "2.4.3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0a9nml2rprggbcp5rikmd9ywx1smy5d8xm10irzvcmwsz4a76q2n")))

(define-public crate-autd3-traits-2.4.4 (c (n "autd3-traits") (v "2.4.4") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0jn18nnczq6k1njbrrfnwl558jp6zpri4r5wzdnbv9jr4ggq5j3q")))

(define-public crate-autd3-traits-2.4.5 (c (n "autd3-traits") (v "2.4.5") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "13465w5jk98nyjxxgfd77jclr5c0dq6kiqk9ca8m8gb4zi0nb0ij")))

(define-public crate-autd3-traits-2.5.0 (c (n "autd3-traits") (v "2.5.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "06s5ihjhc1hzi4va5i589isffhw6zzqlgz057w417m3f0kja6cyw")))

(define-public crate-autd3-traits-2.5.1 (c (n "autd3-traits") (v "2.5.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0y8b9x75nprm1ssr20c7fs7g9kxdxxb71zdc905a1ackfm57wcl3")))

(define-public crate-autd3-traits-2.5.2 (c (n "autd3-traits") (v "2.5.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1m54miwhf8zkw6n22wgvvj4fs1mmrafym4mc06ldvpayyp7y5djb")))

(define-public crate-autd3-traits-2.6.0 (c (n "autd3-traits") (v "2.6.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0amb1rbc7pldmynbpl4d363w0vbhakhz2x6kjbgzmlj06j9jc839")))

(define-public crate-autd3-traits-2.6.1 (c (n "autd3-traits") (v "2.6.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0jd8fn153vzpkidavavhq19r2lzbr32ygnca1k90iz7ip9ml3vcc")))

(define-public crate-autd3-traits-2.6.2 (c (n "autd3-traits") (v "2.6.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "05lbb3n2z5x7n18xwr060mrfdfxqmy8ycrvsxqm1vmaksqfg1s90")))

(define-public crate-autd3-traits-2.6.3 (c (n "autd3-traits") (v "2.6.3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0r1105vnrv0y32ylk1zm4n9nwic02lcb13nfv9mqmprpdzvy7d08")))

(define-public crate-autd3-traits-2.6.4 (c (n "autd3-traits") (v "2.6.4") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "03xzidm43c47l3swij08n4jkm6a0h3y9a9dlc92vjjh73i28xj4d")))

(define-public crate-autd3-traits-2.6.5 (c (n "autd3-traits") (v "2.6.5") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "014dlzafidwj6dsgaify0n72hb59yrrz58kd3vg4g6ddq08cgndf")))

(define-public crate-autd3-traits-2.6.6 (c (n "autd3-traits") (v "2.6.6") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0slq4208jhj89jrdsb9fiin23z65a6g6s1kwr7srxmqqddw8ya8k")))

(define-public crate-autd3-traits-2.6.7 (c (n "autd3-traits") (v "2.6.7") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "166mcl4r3xni0xqw203nwhw9fi1l65hv7v0i0ss57xl19dara4sd")))

(define-public crate-autd3-traits-2.6.8 (c (n "autd3-traits") (v "2.6.8") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0cmazvh4gk4ri19khvy20rghmlyv1jzkqscv51ifgls1pp8c631c")))

(define-public crate-autd3-traits-2.7.0 (c (n "autd3-traits") (v "2.7.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "036c7jyijmc11qz8dbka8mhjp4ypb35pjfna4lsm6msmshpp61bw")))

(define-public crate-autd3-traits-2.7.1 (c (n "autd3-traits") (v "2.7.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0i6a8yb61ci35jzfhg8qrww2ib9fpgg9lnssxvqqqgbijbdxra50")))

(define-public crate-autd3-traits-2.7.2 (c (n "autd3-traits") (v "2.7.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1jrb250m2d5lnq4a6jb4712sbw4klbv6lpkbyjxxqfmgvpnq77c7")))

(define-public crate-autd3-traits-2.7.3 (c (n "autd3-traits") (v "2.7.3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0frvzymh02nzwyz8w1fjyi0ijsmfd6f06i6lf9i7h9ync1l4vsv7")))

(define-public crate-autd3-traits-2.7.4 (c (n "autd3-traits") (v "2.7.4") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0vi8wsxvzd6bph5gqiclnp4hxgcbbvq0cl2bjkzmp28ds8fwi76n")))

(define-public crate-autd3-traits-2.7.5 (c (n "autd3-traits") (v "2.7.5") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0kqfgzp8ihs8gpw3k8r41f3bpxmwmfkjx0jwij71jdxja01bk84z")))

(define-public crate-autd3-traits-2.7.6 (c (n "autd3-traits") (v "2.7.6") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0cml0dgvci04s2dyblca6zamwbwhklkwmhr5wy73b9ns2qs25vh7")))

(define-public crate-autd3-traits-2.8.0 (c (n "autd3-traits") (v "2.8.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0j4rbh4d1i6jpiicrr8hq6ngl83zqwyymm5x2grp4wfrijnkdrhc")))

(define-public crate-autd3-traits-8.1.0 (c (n "autd3-traits") (v "8.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1lvrnxm80s8gjm3zm791yyy0dvdabrik1yfmgn3ad243csld0i3h")))

(define-public crate-autd3-traits-8.1.1 (c (n "autd3-traits") (v "8.1.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0jifmih7xqh22m7g4fw1b9xmghmdqjmsm3khbbyn9pq9z5vmhg1k")))

(define-public crate-autd3-traits-8.1.2 (c (n "autd3-traits") (v "8.1.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1rl2shk00l42ylsqw3vavw173srfikw4g6zrg1znwjsq4liwlvsa")))

(define-public crate-autd3-traits-8.2.0 (c (n "autd3-traits") (v "8.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "07npmaigqr3vba6bls6w9xj6lyy1pxcd4bw6jikbbb8d43pa39pm")))

(define-public crate-autd3-traits-8.3.0 (c (n "autd3-traits") (v "8.3.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "01ch5rhzwd9s48k8613zss6lad583n7gz5gg638rj7xvjmr2rg9q")))

(define-public crate-autd3-traits-8.4.0 (c (n "autd3-traits") (v "8.4.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1psz4hcvq542s4nfb4b42irgpbc5gmlhvidb9vkh91vrp4yminkl")))

(define-public crate-autd3-traits-8.4.1 (c (n "autd3-traits") (v "8.4.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "183n3fymmxdp3wkvhjrdz28wkz4miamgf0g20c3f3n0d33r2pqrp")))

(define-public crate-autd3-traits-8.5.0 (c (n "autd3-traits") (v "8.5.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0rj3s6gq1w0r7vvaifn1xhhwwm7sqknc06fpa1ygp97j7xkkb220")))

(define-public crate-autd3-traits-9.0.0 (c (n "autd3-traits") (v "9.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0j32hxk8dgjp2pc2y91j8c1nw8gppw5cayfi0jg2fkixwn4wlki0")))

(define-public crate-autd3-traits-9.0.1 (c (n "autd3-traits") (v "9.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0spyj8nlj2h0bs2pghcc4d1milsjb2f62l1nk7szpgnzj0c0q32s")))

(define-public crate-autd3-traits-10.0.0 (c (n "autd3-traits") (v "10.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1ac2yk7n056mllab16y8y19jqkrz5qhzn31dkhvq5r7gl7iaddvw")))

(define-public crate-autd3-traits-11.0.0 (c (n "autd3-traits") (v "11.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1ir3nbjgywmfvp97pzn5l3vmc7hdz5n86136w9wy8dpqcs7xwq34")))

(define-public crate-autd3-traits-11.0.1 (c (n "autd3-traits") (v "11.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0y0m93dhm008vsaj5jf2m4ahgbm1s0848x4iax8iccyah1v9hn55")))

(define-public crate-autd3-traits-11.0.2 (c (n "autd3-traits") (v "11.0.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1nlqgbglba5w2m07wiiy46yphqjsgc593rkmjiqfm7gk7pkwrjpr")))

(define-public crate-autd3-traits-11.1.0 (c (n "autd3-traits") (v "11.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1xxcwl7khfip2fnwvnz7r651iwbm8bkf8mksg26zd0ym1cpd4i31")))

(define-public crate-autd3-traits-11.2.0 (c (n "autd3-traits") (v "11.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "08al9qwp395richf34gjrfv3z8cf43812pv83k2yvrx8i24vvzqg")))

(define-public crate-autd3-traits-12.0.0 (c (n "autd3-traits") (v "12.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0ffbzxx1ranzn6pyvn6zdikdw0al7a2a3f258m84j9qic749nlq3")))

(define-public crate-autd3-traits-12.1.0 (c (n "autd3-traits") (v "12.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "00vpd36193bnwj5p96f9r8igz2vpj5j9p1nkra4nfw2l5hdix3wd")))

(define-public crate-autd3-traits-12.1.1 (c (n "autd3-traits") (v "12.1.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0aidmh4bjfaxkkip4607fqpvkn3gcvs73m1vqgd5lgfznrxfsz5n")))

(define-public crate-autd3-traits-12.2.0 (c (n "autd3-traits") (v "12.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1dr26bj6sr9d42mjvr2lgj2xm6xancx29f23j5xz0l321d4h17wn")))

(define-public crate-autd3-traits-12.3.0 (c (n "autd3-traits") (v "12.3.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0p95042hmpd7p0xw4yxsrqbqlfv55zkqz1630zcb4nfjdgh6622w")))

(define-public crate-autd3-traits-12.3.1 (c (n "autd3-traits") (v "12.3.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1vns65fld0hi8dvqf972vmnb29921szb6vns4ljqibbbnb6k58j5")))

(define-public crate-autd3-traits-13.0.0 (c (n "autd3-traits") (v "13.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1rqlc4msr7cpw73q4r8mg8s38gw875xs1v4y7a3s0j3aizqx8qpd")))

(define-public crate-autd3-traits-14.0.0 (c (n "autd3-traits") (v "14.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "09qh546kyrkzxmcvz12f08c51w81qrw990j7qgy764rbz6x5107p")))

(define-public crate-autd3-traits-14.0.1 (c (n "autd3-traits") (v "14.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "168c4nardwksxb6ihr5yac95i32000mw2aqf9xqcgf7dncpc6mvs")))

(define-public crate-autd3-traits-14.1.0 (c (n "autd3-traits") (v "14.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "076j16vhdj7aq7k1jgq3zsggnrh78sv70sxp7g04j5jagjkmjcx0")))

(define-public crate-autd3-traits-14.2.0 (c (n "autd3-traits") (v "14.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1pkv2fpijh6nxs611b1zkbvi20i50qz34m3snbsvam7xy3ids9si")))

(define-public crate-autd3-traits-14.2.1 (c (n "autd3-traits") (v "14.2.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "11pg49siz8arccy42vh3dy57n92ry3k1yy8bzrjrw63n089g46wd")))

(define-public crate-autd3-traits-14.2.2 (c (n "autd3-traits") (v "14.2.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "06asbfmsr9fy4jkwc1dc37lri4ljv0wmx7sc7q03ap6ldczjb4ga")))

