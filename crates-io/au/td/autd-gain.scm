(define-module (crates-io au td autd-gain) #:use-module (crates-io))

(define-public crate-autd-gain-1.0.0 (c (n "autd-gain") (v "1.0.0") (d (list (d (n "autd-geometry") (r "^1.0") (d #t) (k 0)))) (h "1xfixdsdb9h4spx0hk69d2iwvyl1izimxvsd8ik8bq3hy7lmgzc6") (y #t)))

(define-public crate-autd-gain-6.0.0 (c (n "autd-gain") (v "6.0.0") (d (list (d (n "autd-geometry") (r "^1.0") (d #t) (k 0)))) (h "05mniwz56m7ryw8k04kn7y3wzh0krcm9mz1lmd32w6pbnzgn53b6") (y #t)))

