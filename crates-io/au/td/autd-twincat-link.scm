(define-module (crates-io au td autd-twincat-link) #:use-module (crates-io))

(define-public crate-autd-twincat-link-4.0.0 (c (n "autd-twincat-link") (v "4.0.0") (d (list (d (n "autd-link") (r "^1.0.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5.2") (d #t) (k 0)))) (h "12h8ppial98xb0nr97ifvi7q61q11nbd6lp4cg7l6gbd36sjw2d2")))

(define-public crate-autd-twincat-link-4.0.1 (c (n "autd-twincat-link") (v "4.0.1") (d (list (d (n "autd-link") (r "^1.0.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5.2") (d #t) (k 0)))) (h "0g073gynazz2zqcb1ckjgzwbhj1z03j9h46r7w1f3vi8h531fl5d")))

(define-public crate-autd-twincat-link-5.0.0-rc1 (c (n "autd-twincat-link") (v "5.0.0-rc1") (d (list (d (n "autd-link") (r "^1.0.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5.2") (d #t) (k 0)))) (h "0y3x60w2pmfvk3b55ql5q15dq9cc6ipyzdfmby0rry2avg26rmgb")))

(define-public crate-autd-twincat-link-5.0.0 (c (n "autd-twincat-link") (v "5.0.0") (d (list (d (n "autd-link") (r "^1.0.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5.2") (d #t) (k 0)))) (h "12m5js2v6zqc4pwph9lvdhf6wd66q8lxf54yrs27s00032y3p6km")))

(define-public crate-autd-twincat-link-6.1.1 (c (n "autd-twincat-link") (v "6.1.1") (d (list (d (n "autd") (r "^6") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "12zinffnf0xxgg5b49hqgq6ch66ywrcr2bmxgcvyr8jhnxbp0h6r")))

(define-public crate-autd-twincat-link-8.0.0 (c (n "autd-twincat-link") (v "8.0.0") (d (list (d (n "autd") (r "^8") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6.6") (d #t) (k 0)))) (h "02m27hhqhx1iya3dcfgpm6lp3hl86sw2whs6xack2ykq3p2j7cz2")))

(define-public crate-autd-twincat-link-8.0.1 (c (n "autd-twincat-link") (v "8.0.1") (d (list (d (n "autd") (r "^8.0.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6.6") (d #t) (k 0)))) (h "0hnqd56s1xdfdk3cwc7khc03ly6wp6qb47kighkkqm2mzjbi2vkx")))

(define-public crate-autd-twincat-link-9.0.0 (c (n "autd-twincat-link") (v "9.0.0") (d (list (d (n "autd") (r "^9.0.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "02yjahrqwyacs7zhjjpyc6gqxa82a0crsw7z70nnci2kxc04qpfk")))

(define-public crate-autd-twincat-link-9.0.1 (c (n "autd-twincat-link") (v "9.0.1") (d (list (d (n "autd") (r "^9.0.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "1rydy49hb55ayc79b2wbbhcjyl2c9f25bfycj3m2600p1lr1vgp2")))

