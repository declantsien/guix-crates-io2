(define-module (crates-io au td autd3-derive) #:use-module (crates-io))

(define-public crate-autd3-derive-15.0.0 (c (n "autd3-derive") (v "15.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1hzxf70djh7srbvw8s196y8wzji4kwsw32fmikiz4vkw2yn5pdnq") (f (quote (("default"))))))

(define-public crate-autd3-derive-15.0.2 (c (n "autd3-derive") (v "15.0.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1z8jrvh7nkmmlqrz3p8fhwwsnpxz1bzdnyfvihsnr0y5dxs4aavs") (f (quote (("default"))))))

(define-public crate-autd3-derive-15.1.0 (c (n "autd3-derive") (v "15.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0vq57vb36b1cs4c8ngd8swc96v1ggpb31yzlsxhbrnpav7a2a938") (f (quote (("default"))))))

(define-public crate-autd3-derive-15.1.1 (c (n "autd3-derive") (v "15.1.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0x8779icmkab8i0szi8lf280k3d53zamc7l4srx8za949c8365pk") (f (quote (("default"))))))

(define-public crate-autd3-derive-15.1.2 (c (n "autd3-derive") (v "15.1.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "01aq28wxsnxi3w7sgnhsqzqz0x35s284306kr5agj2w8fqhnapsy") (f (quote (("default"))))))

(define-public crate-autd3-derive-15.2.0 (c (n "autd3-derive") (v "15.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "15vaq6fmfym9sdsn11h815zdgkcw19470ij12yzv2xylwnfncjzc") (f (quote (("default"))))))

(define-public crate-autd3-derive-15.2.1 (c (n "autd3-derive") (v "15.2.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "04hngjnfnb0c5h0xisywazl5jgrx1bzizlz6vhhgdkc5hgqq063g") (f (quote (("default"))))))

(define-public crate-autd3-derive-15.3.0 (c (n "autd3-derive") (v "15.3.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0jdif21dz0l6n87vpl4fzqrvivaqn8xd7ck32qf69jcfr2klmq5d") (f (quote (("default"))))))

(define-public crate-autd3-derive-15.3.1 (c (n "autd3-derive") (v "15.3.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1hadv8hcbyxw9ish7lphwcz0wc30658dw55sxkv87zj3hn09rd5r") (f (quote (("default"))))))

(define-public crate-autd3-derive-16.0.0 (c (n "autd3-derive") (v "16.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1jsxsk0xcmz7lpfzq237j796z0nr8shxnm9bx7j7hmmdi6hdbbd2") (f (quote (("default"))))))

(define-public crate-autd3-derive-16.0.1 (c (n "autd3-derive") (v "16.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0x1c6343p3a40bqdsbz9igccxmvm6zabfjwp1ix0cgqyz88rqc1s") (f (quote (("default"))))))

(define-public crate-autd3-derive-16.0.2 (c (n "autd3-derive") (v "16.0.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "038nl63rcdz36pwic27krcb7gj3n00akvy9v89798mqkn792ci65") (f (quote (("default"))))))

(define-public crate-autd3-derive-16.0.3 (c (n "autd3-derive") (v "16.0.3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0jv53bvvg6lwjf8lwcy8wn8ja08y1bj9brx3ckbzppbl81sc8r64") (f (quote (("default"))))))

(define-public crate-autd3-derive-17.0.0 (c (n "autd3-derive") (v "17.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0k4rjj5dq5xf0a1lcxaxv7ajvm7v6g5dqizrdsps4dc6hqmyh2mc") (f (quote (("default"))))))

(define-public crate-autd3-derive-17.0.1 (c (n "autd3-derive") (v "17.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0nkwd79gv65q0q2b7lxbjgb9ich3ifbiddl7mscry1d5aqps3chd") (f (quote (("default"))))))

(define-public crate-autd3-derive-17.0.2 (c (n "autd3-derive") (v "17.0.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0qqaa9cpqmlngjbfw9b412zqj9713yw374yykny1dyv73s49707c") (f (quote (("default"))))))

(define-public crate-autd3-derive-17.0.3 (c (n "autd3-derive") (v "17.0.3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1br34swlf93ffbv4cbpg7q0jamwa6fig581zz8lnlhbaaa7lg2sd") (f (quote (("default"))))))

(define-public crate-autd3-derive-18.0.0 (c (n "autd3-derive") (v "18.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "18wpwwxmq5hrijbr0rbgwra41a1gcdjwm3l79zhbxdchn051plvj") (f (quote (("default"))))))

(define-public crate-autd3-derive-18.0.1 (c (n "autd3-derive") (v "18.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1bp3ad9j5amry297a6l9q9rnap6qgmvfyjrjg7cwazmv811ni9v9") (f (quote (("default"))))))

(define-public crate-autd3-derive-19.0.0 (c (n "autd3-derive") (v "19.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0hvdmhp2za79b38ii28203wlipc7axqdih3gjp9psjn2aaacih0q") (f (quote (("default"))))))

(define-public crate-autd3-derive-19.0.1 (c (n "autd3-derive") (v "19.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0wjjycs3ps1fma62hq175p4sls41wvplyamqlfk42763khp7w6dn") (f (quote (("default"))))))

(define-public crate-autd3-derive-19.1.0 (c (n "autd3-derive") (v "19.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0wyirjzrz9m8dx7jr74k5sjqb1fzah3dlrnw6nffc5166f69klqa") (f (quote (("default"))))))

(define-public crate-autd3-derive-20.0.0 (c (n "autd3-derive") (v "20.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "080hsqyify37za1jy9763dskcjgxdjl6bpwcdak7b9hmyidak8fn") (f (quote (("default"))))))

(define-public crate-autd3-derive-20.0.1 (c (n "autd3-derive") (v "20.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0v4gscgy7rp0b5gdy1ip02dc81yrqw588c39fvazgjik0wkvhxxp") (f (quote (("default"))))))

(define-public crate-autd3-derive-20.0.2 (c (n "autd3-derive") (v "20.0.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "15ind1hybvr0bvji1a946z0lq1ny3jx4gjzd1y9z93iqy48wdk39") (f (quote (("default"))))))

(define-public crate-autd3-derive-20.0.3 (c (n "autd3-derive") (v "20.0.3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1wbk720yzp1mck3xy3ah86hnawrklkz4ipllrhm9vs3lqnw4kdk2") (f (quote (("default"))))))

(define-public crate-autd3-derive-21.0.0-alpha.0 (c (n "autd3-derive") (v "21.0.0-alpha.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0awa9csgynzfzrn01x94rx86p04sdvw8zfmi56fqamxdglalgbhs") (f (quote (("default"))))))

(define-public crate-autd3-derive-21.0.0-alpha.1 (c (n "autd3-derive") (v "21.0.0-alpha.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1v5jrl4y028pm75lmqh78dd7kw5sxp541la1mv1ahvzv16x3zjlm") (f (quote (("default"))))))

(define-public crate-autd3-derive-21.0.0-alpha.2 (c (n "autd3-derive") (v "21.0.0-alpha.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "179asiz0qmch0cvkii99g56y76j5crrhqbxjsa18bvwqf500wbg6") (f (quote (("default"))))))

(define-public crate-autd3-derive-21.0.0-alpha.3 (c (n "autd3-derive") (v "21.0.0-alpha.3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1rxym2vnp1rbsmwi4lf4k4i7gxadxs1710shi7xk956npp6c74vi") (f (quote (("default"))))))

(define-public crate-autd3-derive-21.0.0-alpha.4 (c (n "autd3-derive") (v "21.0.0-alpha.4") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1yim95q3z4c12d7azdb50k57gfkpny9blq57ih0xsqbbivx04cpj") (f (quote (("default"))))))

(define-public crate-autd3-derive-21.0.0-beta.0 (c (n "autd3-derive") (v "21.0.0-beta.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "11gj6jqbr32cxjfxj76ya5a0ib8j4irnhvwyycrdb0gr9xll4ji0") (f (quote (("default"))))))

(define-public crate-autd3-derive-21.0.0 (c (n "autd3-derive") (v "21.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0av2h44mqv6h5w4l3fazd27ihk746yzan5dkbpkg5zkmnzv7kicg") (f (quote (("default"))))))

(define-public crate-autd3-derive-21.0.1 (c (n "autd3-derive") (v "21.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1j6syby9gy02gdhc5438vrmnkw03w8frfqfcb1gwgnjr06pqzc5n") (f (quote (("default"))))))

(define-public crate-autd3-derive-21.1.0 (c (n "autd3-derive") (v "21.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0zny0jrnr8h1fzf7bcrbpjhvj11pv273f3sl5wm47qjb84hp2pc7") (f (quote (("default"))))))

(define-public crate-autd3-derive-22.0.0-rc.0 (c (n "autd3-derive") (v "22.0.0-rc.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1iw0kc3jrrzjgl1fgfl27znml41qnrsmcy3fvfxhq8f12q2klqi7") (f (quote (("default"))))))

(define-public crate-autd3-derive-22.0.0-rc.1 (c (n "autd3-derive") (v "22.0.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1x1jkr4q1l758azfhg29l3r53pq55vd7fxyqfwysfbyaa4swrcd4") (f (quote (("default"))))))

(define-public crate-autd3-derive-22.0.0-rc.2 (c (n "autd3-derive") (v "22.0.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1r38lqrrczhwx5r4sa2vjvwd6hy1qcv2rf689mmn536xs1zx9886") (f (quote (("default"))))))

(define-public crate-autd3-derive-22.0.0-rc.3 (c (n "autd3-derive") (v "22.0.0-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "19drlaxs0y7b7f529xg626jzy1kjzysad984hsphflj57vqv8gqy") (f (quote (("default"))))))

(define-public crate-autd3-derive-22.0.0 (c (n "autd3-derive") (v "22.0.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "13vplbpmaw8agj68pad8z5dcf2id92m1wl0r21pn4zf1i2hf8wc8") (f (quote (("default"))))))

(define-public crate-autd3-derive-22.0.1 (c (n "autd3-derive") (v "22.0.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "16qn3lfbiji5fi2rkb7viwjig2h6hpdwl4m1ng2b7v2kmhfh00r3") (f (quote (("default"))))))

(define-public crate-autd3-derive-22.0.2 (c (n "autd3-derive") (v "22.0.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0q6nchn726zks7k05af5k29pg442fzjgnf7al37y5l1d5bjd4i11") (f (quote (("default"))))))

(define-public crate-autd3-derive-22.0.3 (c (n "autd3-derive") (v "22.0.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "03ic3cq50qnnxq396jl2d6y2gz0zwc4sbiayskbhzc35y9pkmb9i") (f (quote (("default"))))))

(define-public crate-autd3-derive-22.0.4 (c (n "autd3-derive") (v "22.0.4") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0in0i8b865fiq8yydmw0z27lkizvdw2njhqqnnb6mzv69laqbx0s") (f (quote (("default"))))))

(define-public crate-autd3-derive-22.1.0 (c (n "autd3-derive") (v "22.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "05lmh5gx0s2wslqhmfll3k721h15p75liq7n86diswjhds63v9c4") (f (quote (("default"))))))

(define-public crate-autd3-derive-23.0.0-rc.0 (c (n "autd3-derive") (v "23.0.0-rc.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0wy9pc31i2kic8w9h0qbifxwabd8kcr7s5pmk88m4dmrbqfipvhy") (f (quote (("default"))))))

(define-public crate-autd3-derive-23.0.0-rc.1 (c (n "autd3-derive") (v "23.0.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0pg5nbb68kfdzqr20cmn1ai5x7iilfhwxns8nh297nhf9r03ky93") (f (quote (("default"))))))

(define-public crate-autd3-derive-23.0.0 (c (n "autd3-derive") (v "23.0.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0k6m8lgi5vwaxrzp8698413qhp20hw5a00in4b0w8md3p6p7ymff") (f (quote (("default"))))))

(define-public crate-autd3-derive-23.0.1 (c (n "autd3-derive") (v "23.0.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0shfjy0bp1r8hxvjz3bm7fi5gqy5ndl2hqgbacb8ffj027bcmyf5") (f (quote (("default"))))))

(define-public crate-autd3-derive-23.1.0 (c (n "autd3-derive") (v "23.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1la2flds6rn2dlg350hkgd95zxr79s21zgvdk97ks87giqi769i0") (f (quote (("default"))))))

(define-public crate-autd3-derive-24.0.0-rc.0 (c (n "autd3-derive") (v "24.0.0-rc.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "10wf30m3j7bmfhbxihn1z41md0l1l500hyb9vfjxlhy4yzsqrd0v") (f (quote (("default"))))))

(define-public crate-autd3-derive-24.0.0-rc.1 (c (n "autd3-derive") (v "24.0.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0pqrb6s22h7z4jmrvifr540yyqx62vy5jjdc1nvg2sj3rrhz33h8") (f (quote (("default"))))))

(define-public crate-autd3-derive-24.0.0-rc.2 (c (n "autd3-derive") (v "24.0.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "05f1ddczipfw0xvrabzl67kgz22lk1m6dv75f43s27gnpcnv2p3x") (f (quote (("default"))))))

(define-public crate-autd3-derive-24.0.0 (c (n "autd3-derive") (v "24.0.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "06id2my1mpficjrna8jlhmb2x7lkafnw64alkfq2ah3m2b13vbsb") (f (quote (("default"))))))

(define-public crate-autd3-derive-24.1.0 (c (n "autd3-derive") (v "24.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0dbdjs1lwhpwckc7dl6xakalzlask03x5qc65p9hyrzsnpqkm64f") (f (quote (("default"))))))

