(define-module (crates-io au td autd-geometry) #:use-module (crates-io))

(define-public crate-autd-geometry-1.0.0 (c (n "autd-geometry") (v "1.0.0") (d (list (d (n "na") (r "^0.20.0") (d #t) (k 0)))) (h "1j17pzl5mpxk6f03zgi9xs9608rgrgd45czfpf7axv5idw0r7i7f") (y #t)))

(define-public crate-autd-geometry-1.1.0 (c (n "autd-geometry") (v "1.1.0") (d (list (d (n "na") (r "^0.20.0") (d #t) (k 0)))) (h "15zxcc07zgi8zkj5cpnjm2pxwff536c1v6vsrj1zmis2a4hmszqc") (y #t)))

