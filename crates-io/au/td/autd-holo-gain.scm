(define-module (crates-io au td autd-holo-gain) #:use-module (crates-io))

(define-public crate-autd-holo-gain-1.0.0 (c (n "autd-holo-gain") (v "1.0.0") (d (list (d (n "autd-gain") (r "^1.0.0") (d #t) (k 0)) (d (n "autd-geometry") (r "^1.0.0") (d #t) (k 0)) (d (n "autd-utils") (r "^0.1") (d #t) (k 0)) (d (n "na") (r "^0.20.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1v8b63imfcwalf1m6hriy4z5rdwdfhss53fb2b3zfabn1k0849jd")))

(define-public crate-autd-holo-gain-6.0.0 (c (n "autd-holo-gain") (v "6.0.0") (d (list (d (n "autd-gain") (r "^6.0.0") (d #t) (k 0)) (d (n "autd-geometry") (r "^1.0.0") (d #t) (k 0)) (d (n "autd-utils") (r "^0.1") (d #t) (k 0)) (d (n "na") (r "^0.20.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1n2kzzprn2la4ndkr20rga2b7hd3kwis7k1rypb8fsc58fjiamr9")))

(define-public crate-autd-holo-gain-6.1.0 (c (n "autd-holo-gain") (v "6.1.0") (d (list (d (n "autd") (r "^6") (d #t) (k 0)) (d (n "na") (r "^0.21") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "05ys5xcw4scbkf0lxgb4pj2k75s8b8v4s4hn7yk3098vdqqbgj9q")))

(define-public crate-autd-holo-gain-6.1.1 (c (n "autd-holo-gain") (v "6.1.1") (d (list (d (n "autd") (r "^6") (d #t) (k 0)) (d (n "na") (r "^0.21") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1bj1x0wrmay1fsmp4mj06rj2d29y8q379yv3sa41hxp6zmdj9is4")))

(define-public crate-autd-holo-gain-8.0.0 (c (n "autd-holo-gain") (v "8.0.0") (d (list (d (n "autd") (r "^8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "na") (r "^0.23.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0innbd839syqmg113949gjsr1ch2v622fm3nv524xnqqzxq45psz")))

(define-public crate-autd-holo-gain-8.0.1 (c (n "autd-holo-gain") (v "8.0.1") (d (list (d (n "autd") (r "^8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "na") (r "^0.23.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1z3pmp9cfiz87746aqr7aj621r6hyxr2h2vlfslmrp3azwgp60qr")))

(define-public crate-autd-holo-gain-9.0.0 (c (n "autd-holo-gain") (v "9.0.0") (d (list (d (n "autd") (r "^9.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "na") (r "^0.24.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "11i3558wmqnh6mbnx43amkhfplisdc574nz5nh4myz7gwprwwpvc")))

(define-public crate-autd-holo-gain-9.0.1 (c (n "autd-holo-gain") (v "9.0.1") (d (list (d (n "autd") (r "^9.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "na") (r "^0.24.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0zbp1kp2y1ixbihxa0prpbq7k0bi5fs0a7m2anw27l3czp9c0ai6")))

