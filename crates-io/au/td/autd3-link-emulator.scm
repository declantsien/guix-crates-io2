(define-module (crates-io au td autd3-link-emulator) #:use-module (crates-io))

(define-public crate-autd3-link-emulator-2.0.0 (c (n "autd3-link-emulator") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "095sahb7lfmdkf5xap9506pgb5h3hzhd3z8b8znffi8m0frzhi2i")))

(define-public crate-autd3-link-emulator-2.0.2 (c (n "autd3-link-emulator") (v "2.0.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1xbvqi4508ff9cx6hyfpxhk1wik0sb32ibvz8qvh2ll10mw901ms")))

(define-public crate-autd3-link-emulator-2.0.3 (c (n "autd3-link-emulator") (v "2.0.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0nb6cyj7r1i2vf64ambcshl2y8niyjj99hkl2g9h6j917n64yb9x")))

(define-public crate-autd3-link-emulator-2.1.0 (c (n "autd3-link-emulator") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1qzp974mw7ip8flinbmrxxl8qn0dfalaa96vz4cjs4jj9z71cc80")))

(define-public crate-autd3-link-emulator-2.2.0 (c (n "autd3-link-emulator") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0zv5zr8iccg6jwx2f0jyc4icxlclpyd6hxpr265661l0i2w62x11")))

(define-public crate-autd3-link-emulator-2.2.1 (c (n "autd3-link-emulator") (v "2.2.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "150w9cz3zsggl9xjy3iws8d4sizjgvip0mqd7a9zwlpwx5wkczsp")))

(define-public crate-autd3-link-emulator-2.2.2 (c (n "autd3-link-emulator") (v "2.2.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "11cs4c7icl3wviwc315mjzrn693aa8fkjzqkdl4hccwg06k05g6r")))

(define-public crate-autd3-link-emulator-2.3.0 (c (n "autd3-link-emulator") (v "2.3.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0hq0mn9rgmn731yh8z2a7gb311s0c72k57fgnf452rz4qnx52nd0")))

(define-public crate-autd3-link-emulator-2.3.1 (c (n "autd3-link-emulator") (v "2.3.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "16bcvqky18cggbal6a7czj6f1n19vq3pqsrjlk00n91wczyn67wj")))

