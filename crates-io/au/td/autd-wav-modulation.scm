(define-module (crates-io au td autd-wav-modulation) #:use-module (crates-io))

(define-public crate-autd-wav-modulation-1.0.0 (c (n "autd-wav-modulation") (v "1.0.0") (d (list (d (n "autd-modulation") (r "^1.0.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "09iki09b8bn0ragwcip116cs6b30crdz2cavbcaq62jcrva6bk3v")))

(define-public crate-autd-wav-modulation-6.1.1 (c (n "autd-wav-modulation") (v "6.1.1") (d (list (d (n "autd") (r "^6") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)))) (h "15zf94x6ygw4ncvy3yim492xpbl1vd489slk8yx7ka2njq0p81cb")))

(define-public crate-autd-wav-modulation-8.0.0 (c (n "autd-wav-modulation") (v "8.0.0") (d (list (d (n "autd") (r "^8") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)))) (h "12snb78b61bpjm9kxnr6qr72ww3bk7wzz4v29yqxg9k3lz8cdrlf")))

(define-public crate-autd-wav-modulation-9.0.0 (c (n "autd-wav-modulation") (v "9.0.0") (d (list (d (n "autd") (r "^9.0.0") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)))) (h "161ajjqffz3f9h2h5a91vi6ly8ahxl9a7mvwiz5q8vrahmxkrvsh")))

(define-public crate-autd-wav-modulation-9.0.1 (c (n "autd-wav-modulation") (v "9.0.1") (d (list (d (n "autd") (r "^9.0.1") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)))) (h "0914i7ynqfyrsl7jcsv1m7mg3nnvzn8wvf3dgzkdf3pvidsrdzzi")))

