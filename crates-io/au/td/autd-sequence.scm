(define-module (crates-io au td autd-sequence) #:use-module (crates-io))

(define-public crate-autd-sequence-1.0.0-rc1 (c (n "autd-sequence") (v "1.0.0-rc1") (d (list (d (n "autd-geometry") (r "^1.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0dsxy41nrxfmwpinafkjyniqs62g6c6161v3i3fcnj0xv8yi34bp") (y #t)))

(define-public crate-autd-sequence-1.0.0 (c (n "autd-sequence") (v "1.0.0") (d (list (d (n "autd-geometry") (r "^1.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1452y6fvw4vi667imppp8dpgd5h6xpmqvx44x53bb4vyvx5wzxkr") (y #t)))

(define-public crate-autd-sequence-6.0.0 (c (n "autd-sequence") (v "6.0.0") (d (list (d (n "autd-geometry") (r "^1.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1bqcx9d3ib966nkh15x0and18jwx3j88r51as1r4bg2pchxsrcxi") (y #t)))

