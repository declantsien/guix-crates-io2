(define-module (crates-io au td autd-csv-gain) #:use-module (crates-io))

(define-public crate-autd-csv-gain-1.0.0 (c (n "autd-csv-gain") (v "1.0.0") (d (list (d (n "autd-gain") (r "^1") (d #t) (k 0)) (d (n "autd-geometry") (r "^1.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)))) (h "0xdwn3b6z4j08j5fqdp0h2klrjwncpzdrbd8arjcmmj06n3x3m9s")))

(define-public crate-autd-csv-gain-6.0.0 (c (n "autd-csv-gain") (v "6.0.0") (d (list (d (n "autd-gain") (r "^6") (d #t) (k 0)) (d (n "autd-geometry") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)))) (h "1c727nm1a9rzwp2rh8kj08mxsy6h40xna5i7dvm0dw0q1simsqbl")))

(define-public crate-autd-csv-gain-6.1.0 (c (n "autd-csv-gain") (v "6.1.0") (d (list (d (n "autd") (r "^6") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1kfscq2qb2ld28si3pkyfy9szdih97vhdk1nhq7xjq7w4pi4b1dq")))

(define-public crate-autd-csv-gain-6.1.1 (c (n "autd-csv-gain") (v "6.1.1") (d (list (d (n "autd") (r "^6") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "0dqq28in2ni09dr5abinbzvw7kr5kxj7i7lyyfdsbq2afywrsi7f")))

(define-public crate-autd-csv-gain-8.0.0 (c (n "autd-csv-gain") (v "8.0.0") (d (list (d (n "autd") (r "^8") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1c8lxjg9kwy94xj2awn02mq1grby43i66d848rb787vhyidh8w13")))

(define-public crate-autd-csv-gain-9.0.0 (c (n "autd-csv-gain") (v "9.0.0") (d (list (d (n "autd") (r "^9.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1vrbkm4fhwr1s8hrl2qbkbn5psi5lsq0g84pdmdk87jbj67hd34g")))

(define-public crate-autd-csv-gain-9.0.1 (c (n "autd-csv-gain") (v "9.0.1") (d (list (d (n "autd") (r "^9.0.1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "0q2w3r6xxdpphabvzfda1ysw0l32l4s1smwl504anyx7j4hrwby7")))

