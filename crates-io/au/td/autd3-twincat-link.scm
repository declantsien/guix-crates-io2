(define-module (crates-io au td autd3-twincat-link) #:use-module (crates-io))

(define-public crate-autd3-twincat-link-1.0.0 (c (n "autd3-twincat-link") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0y2h9kybyq507g6gh47z2aqi2nj7hg7a1xw1clfs5k5m3f08rvpl")))

(define-public crate-autd3-twincat-link-1.7.1 (c (n "autd3-twincat-link") (v "1.7.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1plswqskr2aaakxm84bbfda8wvsid211v7l3iwzg9ql4jrra39b4")))

(define-public crate-autd3-twincat-link-1.8.0 (c (n "autd3-twincat-link") (v "1.8.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0xpb005x2pnmq312q19cnxk01lm027z6fq6qmfi50msp4nlh10nz")))

(define-public crate-autd3-twincat-link-1.9.0 (c (n "autd3-twincat-link") (v "1.9.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "08dypbsqm3nf0szpb41wyw6544wlqdibc3kv1kmcfmkzh3hkqpgs")))

(define-public crate-autd3-twincat-link-1.9.2 (c (n "autd3-twincat-link") (v "1.9.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.9.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1irisixqjs15i7kibrf5cx5fqss4xms3nai7rpc32vi7bav5ifp4")))

(define-public crate-autd3-twincat-link-1.9.3 (c (n "autd3-twincat-link") (v "1.9.3") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.9.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "13s177g6c99vszpai02nfhz4p8gfw93f1icc8i3pakd3jbvgjh25")))

(define-public crate-autd3-twincat-link-1.10.0 (c (n "autd3-twincat-link") (v "1.10.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.10.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "040ycmahn7xd6lgykyb2n5srncnhyzpiksrxz2b4y61khx0sw811")))

