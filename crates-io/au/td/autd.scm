(define-module (crates-io au td autd) #:use-module (crates-io))

(define-public crate-autd-4.1.0 (c (n "autd") (v "4.1.0") (d (list (d (n "autd-core") (r "^4") (d #t) (k 0)) (d (n "autd-gain") (r "^1") (d #t) (k 0)) (d (n "autd-geometry") (r "^1") (d #t) (k 0)) (d (n "autd-link") (r "^1") (d #t) (k 0)) (d (n "autd-modulation") (r "^1") (d #t) (k 0)) (d (n "autd-soem-link") (r "^4") (d #t) (k 2)) (d (n "autd-timer") (r "^1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1gw4hm7fiflavvswn71nlch90c9wlk7py39am2jics107fkzziqf") (y #t)))

(define-public crate-autd-5.0.0-rc1 (c (n "autd") (v "5.0.0-rc1") (d (list (d (n "autd-core") (r "^5.0.0-rc1") (d #t) (k 0)) (d (n "autd-gain") (r "^1") (d #t) (k 0)) (d (n "autd-geometry") (r "^1.1") (d #t) (k 0)) (d (n "autd-link") (r "^1") (d #t) (k 0)) (d (n "autd-modulation") (r "^1") (d #t) (k 0)) (d (n "autd-sequence") (r "^1.0.0-rc1") (d #t) (k 0)) (d (n "autd-soem-link") (r "^5.0.0-rc1") (d #t) (k 2)) (d (n "autd-timer") (r "^1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0gd81iy3111xfny80h8ynyqqyvcknhx8nvc6hcixq802sgay5173") (y #t)))

(define-public crate-autd-5.0.0 (c (n "autd") (v "5.0.0") (d (list (d (n "autd-core") (r "^5.0.0") (d #t) (k 0)) (d (n "autd-gain") (r "^1") (d #t) (k 0)) (d (n "autd-geometry") (r "^1.1") (d #t) (k 0)) (d (n "autd-link") (r "^1") (d #t) (k 0)) (d (n "autd-modulation") (r "^1") (d #t) (k 0)) (d (n "autd-sequence") (r "^1.0.0") (d #t) (k 0)) (d (n "autd-soem-link") (r "^5.0.0") (d #t) (k 2)) (d (n "autd-timer") (r "^1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "04xfbbzrmcaawc98pj6pjp2kr9izhnj3h5jxffqc1c77pn8jk7gb") (y #t)))

(define-public crate-autd-6.0.0 (c (n "autd") (v "6.0.0") (d (list (d (n "autd-core") (r "^6") (d #t) (k 0)) (d (n "autd-gain") (r "^6") (d #t) (k 0)) (d (n "autd-geometry") (r "^1.1") (d #t) (k 0)) (d (n "autd-link") (r "^1") (d #t) (k 0)) (d (n "autd-modulation") (r "^1") (d #t) (k 0)) (d (n "autd-sequence") (r "^6") (d #t) (k 0)) (d (n "autd-soem-link") (r "^5.0.0") (d #t) (k 2)) (d (n "autd-timer") (r "^1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1imfms41nlwcxzf4zwhff7v768m46lxfp9fhjm508sx11zklpgzw") (y #t)))

(define-public crate-autd-6.1.0 (c (n "autd") (v "6.1.0") (d (list (d (n "autd-timer") (r "^1.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "na") (r "^0.21") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "000x3jwwwka73p35flfgv6hrd31xfnxpjs87a93klfldwy66iwvw")))

(define-public crate-autd-6.1.1 (c (n "autd") (v "6.1.1") (d (list (d (n "autd-timer") (r "^1.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "na") (r "^0.21") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0my0h087dvgj0wps1dizdxgw6xasw2wk9iryccf4n32x3n4sj6gw")))

(define-public crate-autd-8.0.0 (c (n "autd") (v "8.0.0") (d (list (d (n "autd-timer") (r "^2.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "na") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)))) (h "17g6daniccbaikj1n0vjjxrfqynh6pglzib1wdw2jxn6fs6jhgm0") (f (quote (("double") ("default"))))))

(define-public crate-autd-8.0.1 (c (n "autd") (v "8.0.1") (d (list (d (n "autd-timer") (r "^2.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "na") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)))) (h "0229jq6zisvpmrv5pqmjvcs7yflll4xz33fcalk72svvdiw36m8a") (f (quote (("double") ("default"))))))

(define-public crate-autd-9.0.0 (c (n "autd") (v "9.0.0") (d (list (d (n "autd-timer") (r "^2.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "na") (r "^0.24.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1qblrx9zzj55ck6wb6w4w22qp2srm84507fl0ihq3l32x2cjgklb") (f (quote (("double") ("default"))))))

(define-public crate-autd-9.0.1 (c (n "autd") (v "9.0.1") (d (list (d (n "autd-timer") (r "^2.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "na") (r "^0.24.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0i4rbf8mxb11b6h3gpgwkwvfz44961znbrghismy14hhw23a133l") (f (quote (("double") ("default"))))))

