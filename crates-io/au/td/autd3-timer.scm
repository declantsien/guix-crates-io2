(define-module (crates-io au td autd3-timer) #:use-module (crates-io))

(define-public crate-autd3-timer-1.0.0 (c (n "autd3-timer") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 1)))) (h "0i8i2z87idwr09hxfaib9l3v2r2643p8axp1bdzkkbjiqsi9j3zs")))

(define-public crate-autd3-timer-1.8.0 (c (n "autd3-timer") (v "1.8.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (t "cfg(windows)") (k 1)))) (h "0qh2bmsgj3hh11fcnh5w9ixyv31qj5y3c1w373ln41r156f1ra9s")))

(define-public crate-autd3-timer-1.9.0 (c (n "autd3-timer") (v "1.9.0") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (t "cfg(windows)") (k 1)))) (h "03lnxw18wh66jisxynmxl286mci1xrngxs44fg4z2d8ych2hkgki")))

(define-public crate-autd3-timer-1.9.2 (c (n "autd3-timer") (v "1.9.2") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.28.0") (f (quote ("Win32_Media_Multimedia" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yhkl80psgacpgny91a03s3lfzzw8s4k7zfysb9v8afsgr84y69g")))

(define-public crate-autd3-timer-1.9.3 (c (n "autd3-timer") (v "1.9.3") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.28.0") (f (quote ("Win32_Media_Multimedia" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wcjy0ascnc17gnbyrgpqb2d315bbjspzjw0sz2v7vd6nz06vzvv")))

(define-public crate-autd3-timer-1.10.0 (c (n "autd3-timer") (v "1.10.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.28.0") (f (quote ("Win32_Media_Multimedia" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fj4mc6sznva1jv358i6awqdqjb4gq4irls5jmlqzs2gpzwyyyki")))

(define-public crate-autd3-timer-8.3.0 (c (n "autd3-timer") (v "8.3.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.46.0") (f (quote ("Win32_Media_Multimedia" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0z7fsyjry2lczyl0xp4k9fbhdi93nibgxbfs0zfax6rzfaj8navm")))

(define-public crate-autd3-timer-8.4.0 (c (n "autd3-timer") (v "8.4.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.46.0") (f (quote ("Win32_Media_Multimedia" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0nnwida1194jmpqdpdkxibbprb7mikql2pl6j28m9r9x22m6cyds")))

(define-public crate-autd3-timer-8.4.1 (c (n "autd3-timer") (v "8.4.1") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.46.0") (f (quote ("Win32_Media_Multimedia" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "014w7pkjnncf2ndpfrbsh9sv17sinnla6mgpx3rdg49xyv5xc24l")))

(define-public crate-autd3-timer-8.5.0 (c (n "autd3-timer") (v "8.5.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Media_Multimedia" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12rphwcv3hxcnlv9cjrwfgkibdf8q893q4g6rl3aym7ps8jf90m4")))

(define-public crate-autd3-timer-9.0.0 (c (n "autd3-timer") (v "9.0.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Media_Multimedia" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04jdhfjdmq2y1fbj4jpfp8zx1aadvzjbvjnhfpgcc8kd1ff1pwd8")))

(define-public crate-autd3-timer-9.0.1 (c (n "autd3-timer") (v "9.0.1") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Media_Multimedia" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "179ll8szpnv5sbmc5x49lmpz8l1q58gvglzg8n9llf931gvb8p2w")))

