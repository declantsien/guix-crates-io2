(define-module (crates-io au td autd-grouped-gain) #:use-module (crates-io))

(define-public crate-autd-grouped-gain-1.0.0 (c (n "autd-grouped-gain") (v "1.0.0") (d (list (d (n "autd-gain") (r "^1") (d #t) (k 0)) (d (n "autd-geometry") (r "^1") (d #t) (k 0)))) (h "1caqqgw4s2v716spxlrsxvfiqxrccmp2ac9c063amfacrnpq5ifz") (y #t)))

(define-public crate-autd-grouped-gain-6.0.0 (c (n "autd-grouped-gain") (v "6.0.0") (d (list (d (n "autd-gain") (r "^6") (d #t) (k 0)) (d (n "autd-geometry") (r "^1") (d #t) (k 0)))) (h "1znkjzxaw247wrdls8dq1kmvdagmqks29wya48m5a6qpdw83msji") (y #t)))

