(define-module (crates-io au td autd3-link-debug) #:use-module (crates-io))

(define-public crate-autd3-link-debug-2.0.0 (c (n "autd3-link-debug") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.0.0") (d #t) (k 0)) (d (n "autd3-firmware-emulator") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ijc8fhs4fvpm9g5i3jmqqd28vnmvl5mhkvx8lk8n029hqfaghxj")))

(define-public crate-autd3-link-debug-2.0.2 (c (n "autd3-link-debug") (v "2.0.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.0.2") (d #t) (k 0)) (d (n "autd3-firmware-emulator") (r "^2.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0q44hi5kz7f433n1ijrggxrsr7ix3rrjzgniap2ld1kfz6h56m3x")))

(define-public crate-autd3-link-debug-2.0.3 (c (n "autd3-link-debug") (v "2.0.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.0.3") (d #t) (k 0)) (d (n "autd3-firmware-emulator") (r "^2.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04lw6q5ncax0ck81ajgilz7b4irmnfwlxh07qacc2ikama11q0ly")))

(define-public crate-autd3-link-debug-2.1.0 (c (n "autd3-link-debug") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.1.0") (d #t) (k 0)) (d (n "autd3-firmware-emulator") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0xbx7jvl0v7hrr9322y9bv735jhykbq8anms4qzw046nq9s96rf4")))

(define-public crate-autd3-link-debug-2.2.0 (c (n "autd3-link-debug") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.2.0") (d #t) (k 0)) (d (n "autd3-firmware-emulator") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05c0b5ysv8f4dv0gqvp1n4cwzv09capzgksf4bs3hwwhr462wcwg")))

(define-public crate-autd3-link-debug-2.2.1 (c (n "autd3-link-debug") (v "2.2.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.2.1") (d #t) (k 0)) (d (n "autd3-firmware-emulator") (r "^2.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "01j9n5d9pvi7bgfjsjvcz5b46mjf4zf0zv66my90g3q9mfnfsi3x")))

(define-public crate-autd3-link-debug-2.2.2 (c (n "autd3-link-debug") (v "2.2.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.2.2") (d #t) (k 0)) (d (n "autd3-firmware-emulator") (r "^2.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1k51han9v8a1028w93xbw43j29pm6n47aixcf301v3fzdaf6474j")))

(define-public crate-autd3-link-debug-2.3.0 (c (n "autd3-link-debug") (v "2.3.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.3.0") (d #t) (k 0)) (d (n "autd3-firmware-emulator") (r "^2.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "19faiv6sd24pymivwchz46akjv0zlkrspvdr0lbs6wicqr1d3pq7")))

(define-public crate-autd3-link-debug-2.3.1 (c (n "autd3-link-debug") (v "2.3.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "autd3-core") (r "^2.3.1") (d #t) (k 0)) (d (n "autd3-firmware-emulator") (r "^2.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "15alnphgyj1wx87jc06w1s1fn25ws638nf3zx3hk5k9m5h5fc2qf")))

