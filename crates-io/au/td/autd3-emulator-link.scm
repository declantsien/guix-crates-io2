(define-module (crates-io au td autd3-emulator-link) #:use-module (crates-io))

(define-public crate-autd3-emulator-link-1.5.0 (c (n "autd3-emulator-link") (v "1.5.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1xpwp56l8nra8alnpy37pw0cnzrgax0gdkhzqgcxygl231pmj7pf")))

(define-public crate-autd3-emulator-link-1.6.0 (c (n "autd3-emulator-link") (v "1.6.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0jr0zk0q5pnzdi3y055rd5l918brypzadfvkywl3ryiz2lz0nxx0")))

(define-public crate-autd3-emulator-link-1.8.0 (c (n "autd3-emulator-link") (v "1.8.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0kr3bl46awyi2jwkdvgjpiy0pv7qxvazcd9q486l46xb0qhbi615")))

(define-public crate-autd3-emulator-link-1.9.0 (c (n "autd3-emulator-link") (v "1.9.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.9.0") (d #t) (k 0)))) (h "06hwqii3x7k89qd468np3mgiyrjn61l3pd4z8vz9bz4gv4xzr8ps")))

(define-public crate-autd3-emulator-link-1.9.2 (c (n "autd3-emulator-link") (v "1.9.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.9.2") (d #t) (k 0)))) (h "04xya0dnfqmz75zl65vdh0sj56rhawjf37vil5l7hjbi5ga6ssmc")))

(define-public crate-autd3-emulator-link-1.9.3 (c (n "autd3-emulator-link") (v "1.9.3") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.9.3") (d #t) (k 0)))) (h "0qqsxj0wn2x7hpn8ld76grycx2al6kznr1r75b90cwdq4f9b8zri")))

(define-public crate-autd3-emulator-link-1.10.0 (c (n "autd3-emulator-link") (v "1.10.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "autd3-core") (r "^1.10.0") (d #t) (k 0)))) (h "14hjik1hdv4nm5nkshy4bdyaxwmhs40rnxisah3lf08q626vdsmn")))

