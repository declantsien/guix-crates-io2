(define-module (crates-io au td autd-timer) #:use-module (crates-io))

(define-public crate-autd-timer-1.0.0 (c (n "autd-timer") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("threadpoollegacyapiset" "errhandlingapi" "handleapi" "winerror" "processthreadsapi" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wx3hpp23g14p3a7d6icxj97fc9r3wb836dwbkn1h2xa5wsav5a8") (y #t)))

(define-public crate-autd-timer-1.1.0 (c (n "autd-timer") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("threadpoollegacyapiset" "errhandlingapi" "handleapi" "winerror" "processthreadsapi" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0prx12ap9rvlnqbk74a5pn85lgq99x4w881i2chhlrjhbb67aqdk") (y #t)))

(define-public crate-autd-timer-1.2.0 (c (n "autd-timer") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("threadpoollegacyapiset" "errhandlingapi" "handleapi" "winerror" "processthreadsapi" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0gbzdlm2z3zild5i33x1c6gi35l6g25wk0mpy8i6x0aaf5si650z")))

(define-public crate-autd-timer-1.2.1 (c (n "autd-timer") (v "1.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("threadpoollegacyapiset" "errhandlingapi" "handleapi" "winerror" "processthreadsapi" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rf76ll885zx3smir84kybgp1nh1kgfjv5cz5pn2iwsmnwp3dcb2")))

(define-public crate-autd-timer-1.2.2 (c (n "autd-timer") (v "1.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("threadpoollegacyapiset" "errhandlingapi" "handleapi" "winerror" "processthreadsapi" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1hd3yvy8drqwsf6a86yijlr3409221bss9081afhhdpqspfn8rdy")))

(define-public crate-autd-timer-2.0.0 (c (n "autd-timer") (v "2.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("threadpoollegacyapiset" "timeapi" "mmsystem" "basetsd"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ky873sk64m70nynd081j2mp3fxwqnw78kw7g931z3xgwy82b3dp")))

