(define-module (crates-io au td autd-core) #:use-module (crates-io))

(define-public crate-autd-core-4.0.0 (c (n "autd-core") (v "4.0.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0gjp1vy7bwb1s4znh9asa4v8rqrd8wgnhsm61kbk26n4awvzbbb0") (y #t)))

(define-public crate-autd-core-5.0.0-rc1 (c (n "autd-core") (v "5.0.0-rc1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "171h52kwpc7xn4mxn949p9s1wgn7q2g1lxipcwy3fh138945qrrr") (y #t)))

(define-public crate-autd-core-5.0.0 (c (n "autd-core") (v "5.0.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1996z869nhm78xf3zs55bh5840x7hc6w1b76zyizfv165h03zhpb") (y #t)))

(define-public crate-autd-core-6.0.0 (c (n "autd-core") (v "6.0.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1f1ipzchzwwpfs2nzrsifv9qy2q8a7zwl0czhixkj6wgyy1ai624") (y #t)))

