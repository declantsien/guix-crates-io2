(define-module (crates-io au r- aur-rs) #:use-module (crates-io))

(define-public crate-aur-rs-0.1.0 (c (n "aur-rs") (v "0.1.0") (d (list (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1v4ny4bsfc0r8dm7frnq29alwidrly9ix93vqif6qdxbx8hsnv13")))

(define-public crate-aur-rs-0.1.1 (c (n "aur-rs") (v "0.1.1") (d (list (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0iznxl0ismgc94dzpi7qpbc4sjck8zpc81b0qr1qdv2ikkhq3cpc")))

