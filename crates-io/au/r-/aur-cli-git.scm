(define-module (crates-io au r- aur-cli-git) #:use-module (crates-io))

(define-public crate-aur-cli-git-0.1.3 (c (n "aur-cli-git") (v "0.1.3") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "doe") (r "^0.1.40") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "tsu") (r "^1.0.1") (d #t) (k 0)))) (h "1pjlx7vrzvlsbi2vihg8b34s1v58wgangq093sxbjydp9zwfpagv")))

