(define-module (crates-io au r- aur-fetch) #:use-module (crates-io))

(define-public crate-aur-fetch-0.1.0 (c (n "aur-fetch") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2.3") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "09h7b9a2hxijd3pi2rij807zyxld01z3zvqpisfcaqxq07ndss5l")))

(define-public crate-aur-fetch-0.2.0 (c (n "aur-fetch") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2.3") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0nxgd88zipdbncmimcq8xl4rnlnan32dsg7djvvxm5gaxmjg1f7j")))

(define-public crate-aur-fetch-0.3.0 (c (n "aur-fetch") (v "0.3.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2.3") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0qkn2cihj7aq81hz8wpv800dfxmsxv2gz6pqba74293j6206klij")))

(define-public crate-aur-fetch-0.4.0 (c (n "aur-fetch") (v "0.4.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2.3") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0vhzml4k9cnbm7xzmvbxvh7yadhfac04pqm70sbnnm4l43b3j6n5")))

(define-public crate-aur-fetch-0.4.1 (c (n "aur-fetch") (v "0.4.1") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2.3") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1wlp2ip1vz1r56sjlhhw9cwlg3hs6gkh1cr3kjmb891kb3yzwk4n")))

(define-public crate-aur-fetch-0.4.2 (c (n "aur-fetch") (v "0.4.2") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2.3") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0699c69k4f0881lxc3cggwwv3kqqip56wc53mb3pnpknqnlm49hg")))

(define-public crate-aur-fetch-0.5.0 (c (n "aur-fetch") (v "0.5.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2.3") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0a0xv0v5m14nzjqsg0cx3phkp6abmf9hphlq4289bs37dxh9yf4j")))

(define-public crate-aur-fetch-0.5.1 (c (n "aur-fetch") (v "0.5.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("process"))) (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0dyb6bz3z82fwqzgvid8ja6pmm36v9f0pah0zl2dv3kgk40wpjgz")))

(define-public crate-aur-fetch-0.6.0 (c (n "aur-fetch") (v "0.6.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("process"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1k5inn1qc5vmdzzqywfwi3pd256cb34ndiis2i0zkh8jnf4915xb")))

(define-public crate-aur-fetch-0.7.0 (c (n "aur-fetch") (v "0.7.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("process"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0g3j8qy42p2xnfxmqfzjx5b74alr2iffavpfq6qjf23dfwi377rk") (f (quote (("view" "tempdir") ("default" "view"))))))

(define-public crate-aur-fetch-0.7.1 (c (n "aur-fetch") (v "0.7.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("process"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1qql8f7fq0cqgchx8iwrpp0zrcg37b6q531pxyp53wwr6vdaaxna") (f (quote (("view" "tempdir") ("default" "view"))))))

(define-public crate-aur-fetch-0.7.2 (c (n "aur-fetch") (v "0.7.2") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("process"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0j4sdf55pyjgnw6lqcy76fmqajr31lxjjcad03m47wh5yybqrsg3") (f (quote (("view" "tempdir") ("default" "view"))))))

(define-public crate-aur-fetch-0.7.3 (c (n "aur-fetch") (v "0.7.3") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("process"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "152dgigiyf9lxyj7fgra08flvrli2nr6hplllk3b9jawl9d260kd") (f (quote (("view" "tempfile") ("default" "view"))))))

(define-public crate-aur-fetch-0.8.0 (c (n "aur-fetch") (v "0.8.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("process"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "04psl3ijrmg93g94mycrfg4mr5548arwz2zravrvxi8jjpmqbk1p") (f (quote (("view" "tempfile") ("default" "view"))))))

(define-public crate-aur-fetch-0.8.1 (c (n "aur-fetch") (v "0.8.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("process"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0d0slxw2pi004081981wdjjcyh037rkih9g7vjchbxsa6i7izvpq") (f (quote (("view" "tempfile") ("default" "view"))))))

(define-public crate-aur-fetch-0.9.0 (c (n "aur-fetch") (v "0.9.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("process"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1f2a1is17zdxyk8qfp03sxp2l8zyfax48gbsw2pcimj7qwvj1ldf") (f (quote (("view" "tempfile") ("default" "view"))))))

(define-public crate-aur-fetch-0.9.1 (c (n "aur-fetch") (v "0.9.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("process"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1p2izy8bv4z406vq81yv6xk4z34gjx51z2w6rcyzg96c8fw49ld8") (f (quote (("view" "tempfile") ("default" "view"))))))

(define-public crate-aur-fetch-0.10.0 (c (n "aur-fetch") (v "0.10.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("process"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0adr9a418h6wfvfj8imvv1azcxrjhiycn51s33fs7y59jgskk5cc") (f (quote (("view" "tempfile") ("default" "view"))))))

(define-public crate-aur-fetch-0.11.0 (c (n "aur-fetch") (v "0.11.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1x33n7wnbjsvzn1jkd0fnb6zc1ag91brzpr1c9i4wb6jrn2pil08")))

(define-public crate-aur-fetch-0.11.1 (c (n "aur-fetch") (v "0.11.1") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0fwsfngwp6xkpfa2p4b70a859lyjqi3m6sjphjm1rhg2wk26hg3q")))

(define-public crate-aur-fetch-0.11.2 (c (n "aur-fetch") (v "0.11.2") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0kh7c7g2gvqyc6bf4ngwkw17a8hm70rhq86011wckyiw0134z8xk")))

