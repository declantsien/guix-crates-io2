(define-module (crates-io au r- aur-client) #:use-module (crates-io))

(define-public crate-aur-client-0.1.0 (c (n "aur-client") (v "0.1.0") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mzz7d0j74ichpk7fxdfw916x1s0lbl9d4gq2h9skcv1vsswsh0i")))

(define-public crate-aur-client-0.1.1 (c (n "aur-client") (v "0.1.1") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ir4r36hlcbl81ci4amididwi0l4x3zs73rmd7idaqp75crs8p2d")))

(define-public crate-aur-client-0.1.2 (c (n "aur-client") (v "0.1.2") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (f (quote ("derive"))) (d #t) (k 0)))) (h "0blw0k1nijkff31j8nnjibzypvbm1kbginr752i6bm1ch4qxjxp4")))

(define-public crate-aur-client-0.1.3 (c (n "aur-client") (v "0.1.3") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nncclxrazf3jrx4hjfh16fmv6q4a7j3jsnb7xmnfdgsz6ix8yx1")))

