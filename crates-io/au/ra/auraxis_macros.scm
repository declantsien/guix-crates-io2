(define-module (crates-io au ra auraxis_macros) #:use-module (crates-io))

(define-public crate-auraxis_macros-0.1.0 (c (n "auraxis_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "08x2dh7swyj33548lxb6akhijj4q4k09zm6jvwzf75psbqg9iswn")))

