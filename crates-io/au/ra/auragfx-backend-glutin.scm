(define-module (crates-io au ra auragfx-backend-glutin) #:use-module (crates-io))

(define-public crate-auragfx-backend-glutin-0.1.0 (c (n "auragfx-backend-glutin") (v "0.1.0") (d (list (d (n "auragfx-backend-api") (r "^0.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.13.0") (d #t) (k 0)) (d (n "glutin") (r "^0.21.0") (d #t) (k 0)))) (h "1ccglg4cz7hafvna8bsf4vxjjyjydllr9virjfw573lyahfdx1z2")))

