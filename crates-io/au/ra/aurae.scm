(define-module (crates-io au ra aurae) #:use-module (crates-io))

(define-public crate-aurae-0.1.0 (c (n "aurae") (v "0.1.0") (h "0b1rcznn8xkwg3xmiwk3c9kx29ggfkcsqay3s8sxn82jmfm5fvhq") (y #t)))

(define-public crate-aurae-0.0.0 (c (n "aurae") (v "0.0.0") (h "0f5zk0k0w60m714hxqbpf24s7x112xs1rpszadz4kalnd9kjr4xk")))

