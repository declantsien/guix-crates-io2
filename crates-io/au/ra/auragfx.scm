(define-module (crates-io au ra auragfx) #:use-module (crates-io))

(define-public crate-auragfx-0.1.0 (c (n "auragfx") (v "0.1.0") (d (list (d (n "auragfx-backend-api") (r "^0.1.0") (d #t) (k 0)) (d (n "auragfx-backend-glutin") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "shaderc") (r "^0.6.0") (d #t) (k 0)) (d (n "spirv_cross") (r "^0.15.0") (d #t) (k 0)))) (h "1d6dancdi78kdhinr5dmckxhql0ay6grfr83wpz4r4i89f61k97d") (f (quote (("default" "backend-glutin") ("backend-glutin" "auragfx-backend-glutin"))))))

