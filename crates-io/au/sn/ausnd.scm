(define-module (crates-io au sn ausnd) #:use-module (crates-io))

(define-public crate-ausnd-0.5.0 (c (n "ausnd") (v "0.5.0") (d (list (d (n "audio-codec-algorithms") (r "^0.5.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1jj5iyf7xnnn8rz7z3gpq47mqk5lbz0bwyqvvjdxxhs7a5rdqfmm")))

(define-public crate-ausnd-0.5.1 (c (n "ausnd") (v "0.5.1") (d (list (d (n "audio-codec-algorithms") (r "^0.5.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "08mcqlzkl4k1xxzgwfm73v7pk0s3378ml4gkzxcz9x5n9rzgmb9i")))

