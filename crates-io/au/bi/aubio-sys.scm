(define-module (crates-io au bi aubio-sys) #:use-module (crates-io))

(define-public crate-aubio-sys-0.1.0 (c (n "aubio-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "git2") (r "^0.11") (o #t) (d #t) (k 1)))) (h "1j49wrcn38jzi6fq6a2jsh60nqvvxg5qfhlr7brypzkwwcrzjg3i") (f (quote (("with-wav") ("with-sndfile") ("with-samplerate") ("with-jack") ("with-fftw3f") ("with-fftw3") ("with-avcodec") ("rustdoc") ("generate-bindings" "bindgen") ("fetch-prebuilt" "fetch_unroll") ("dynamic-link") ("compile-library" "git2"))))))

(define-public crate-aubio-sys-0.1.2 (c (n "aubio-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (o #t) (d #t) (k 1)))) (h "0minbv875bfabcgm1i88851vn6xdpckvanamz4zndvn36xvgv5c0") (f (quote (("rustdoc") ("generate-bindings" "bindgen" "fetch_unroll"))))))

(define-public crate-aubio-sys-0.1.3 (c (n "aubio-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08x23y5hbwvv93p68njv4pji9y8jnf2xajnd732z7fyhb6phk2y7") (f (quote (("rustdoc") ("generate-bindings" "bindgen" "fetch_unroll") ("build"))))))

(define-public crate-aubio-sys-0.2.0 (c (n "aubio-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "fftw-sys") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "1v0mgr7sfvp7gs254awixaq73r0dnh56jzddm3fj1n0rn4rv54lz") (f (quote (("update-bindings" "bindgen") ("static") ("shared") ("rustdoc") ("intelipp") ("fftw3" "fftw-sys") ("double") ("builtin") ("blas") ("atlas") ("accelerate")))) (l "aubio")))

(define-public crate-aubio-sys-0.2.1 (c (n "aubio-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.58") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "fftw-sys") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "1a10iygrqm4rc1swb041pkpdj9y9mh4k883jdn50pkgcmkz2vvwr") (f (quote (("update-bindings" "bindgen") ("static") ("shared") ("rustdoc") ("intelipp") ("fftw3" "fftw-sys") ("double") ("builtin") ("blas") ("atlas") ("accelerate")))) (l "aubio")))

