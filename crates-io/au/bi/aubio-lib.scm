(define-module (crates-io au bi aubio-lib) #:use-module (crates-io))

(define-public crate-aubio-lib-0.1.2 (c (n "aubio-lib") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (d #t) (k 1)))) (h "0558zppgyg695mmipkqci5y96n8fyyp1fm7dnvrbi81wca5pjvnn") (f (quote (("with-fftw3" "cmake") ("with-double") ("shared-fftw3") ("shared") ("rustdoc") ("nolink-fftw3"))))))

(define-public crate-aubio-lib-0.1.3 (c (n "aubio-lib") (v "0.1.3") (d (list (d (n "aubio-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (d #t) (k 1)))) (h "0jcq99zmibkrgpyxw0faj4j2g3p8rcg2xgqz6x8wqb8g0fq4aard") (f (quote (("with-fftw3" "cmake") ("with-double") ("shared-fftw3") ("shared") ("rustdoc") ("nolink-fftw3") ("build" "aubio-sys/build"))))))

