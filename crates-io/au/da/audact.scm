(define-module (crates-io au da audact) #:use-module (crates-io))

(define-public crate-audact-0.1.0 (c (n "audact") (v "0.1.0") (d (list (d (n "cpal") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)))) (h "0ga3vf5jakispbvpm46namjj8xpccf7r4r4linl62blwq2inkdw9")))

(define-public crate-audact-0.2.0 (c (n "audact") (v "0.2.0") (d (list (d (n "cpal") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "110zz22y0m2r09yl9daahlldy6wjxgylb51hv42iwp265iyrx3xq")))

(define-public crate-audact-0.3.0 (c (n "audact") (v "0.3.0") (d (list (d (n "cpal") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0jffahlbg4sys9lsa57qcpjky1g8b2d9wa1kw3fdpv2pq9nbj4zq")))

(define-public crate-audact-0.4.0 (c (n "audact") (v "0.4.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "rodio") (r "^0.5") (d #t) (k 0)))) (h "0kgszd70m0aw53f8yw3qdgh1fzmai7rhbk7j7zvqj8hk1bjyhkb4")))

(define-public crate-audact-0.5.0 (c (n "audact") (v "0.5.0") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.6") (d #t) (k 0)))) (h "1djg9gfmjd2r11nyww8k0hmym93604bs9yz3h704s615iba3sji4")))

(define-public crate-audact-0.5.1 (c (n "audact") (v "0.5.1") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rodio") (r "^0.12") (d #t) (k 0)))) (h "0dxdwprvgcxr1n54gpzikchfdix7g1dkfa1rw5z8k85g51fz1awq")))

