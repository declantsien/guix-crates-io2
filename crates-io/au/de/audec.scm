(define-module (crates-io au de audec) #:use-module (crates-io))

(define-public crate-audec-0.1.0 (c (n "audec") (v "0.1.0") (d (list (d (n "bzip2") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lz4") (r "^1.24") (o #t) (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0ybwwcjx2hfra5a46ciby5ldffi4jwlf2dxrpwldffgc4b1lfnqa") (f (quote (("default" "flate2" "zstd"))))))

