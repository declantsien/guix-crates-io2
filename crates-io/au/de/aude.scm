(define-module (crates-io au de aude) #:use-module (crates-io))

(define-public crate-aude-0.1.0 (c (n "aude") (v "0.1.0") (d (list (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)))) (h "1ypzqf9kinjpm2p86i873q8kfw1s4xiargc71mbrzbb3856kh4jr")))

(define-public crate-aude-0.2.0 (c (n "aude") (v "0.2.0") (d (list (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)))) (h "029n92mw2fw5yvk24y89bqr67l61sqja5k2wi0ij4x6gmnf1azvn")))

