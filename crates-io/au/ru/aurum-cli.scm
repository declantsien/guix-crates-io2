(define-module (crates-io au ru aurum-cli) #:use-module (crates-io))

(define-public crate-aurum-cli-0.1.0 (c (n "aurum-cli") (v "0.1.0") (d (list (d (n "aurum") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10imnyi8lbgv6b5c2lla2qqr7daag9ynwhw4bw1451vj1i5ghsnd")))

