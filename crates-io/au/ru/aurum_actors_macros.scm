(define-module (crates-io au ru aurum_actors_macros) #:use-module (crates-io))

(define-public crate-aurum_actors_macros-0.0.0 (c (n "aurum_actors_macros") (v "0.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1qkmfgrhc5s1sfr1vkny89axddjhl1qlkyhxfc7jjk55bmcm6imk")))

(define-public crate-aurum_actors_macros-0.0.1 (c (n "aurum_actors_macros") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1cs3gdannxjgdrj9c4fgs8wrkid4izkfg35aahwcamrbxjrcgzh1")))

