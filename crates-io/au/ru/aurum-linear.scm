(define-module (crates-io au ru aurum-linear) #:use-module (crates-io))

(define-public crate-aurum-linear-0.2.0 (c (n "aurum-linear") (v "0.2.0") (d (list (d (n "aurum-numeric") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.8.21") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.21") (o #t) (d #t) (k 0)))) (h "10m3bgp0zjhm7fj4qn5i7r7nvspxplr9qz8vgj3xfnqlsczkr4iq") (y #t)))

