(define-module (crates-io au ru aurum-numeric) #:use-module (crates-io))

(define-public crate-aurum-numeric-0.1.0 (c (n "aurum-numeric") (v "0.1.0") (h "1yyx77w6xizd8rgpkc7dfcpv7zmn8z2v6pmqaxbph37987lnan6g") (y #t)))

(define-public crate-aurum-numeric-0.1.1 (c (n "aurum-numeric") (v "0.1.1") (h "0p2ixhnv33lmfmp51ihvpcpv271ynlk5g819209905mbpq63v3rk") (y #t)))

(define-public crate-aurum-numeric-0.1.2 (c (n "aurum-numeric") (v "0.1.2") (h "051i2w78057lrcjc0wzv173wyzxvc1r6jrkns8rzmbcm3mv0dr0q") (y #t)))

(define-public crate-aurum-numeric-0.1.3 (c (n "aurum-numeric") (v "0.1.3") (h "11qw3rnbx1hsdqpssf6nyk8z29wjmnvyzdmkak6s1l2w3mb4n8ih") (y #t)))

(define-public crate-aurum-numeric-0.2.0 (c (n "aurum-numeric") (v "0.2.0") (h "1si263mxajhl95jp2wypswic9i4vcsb7fgz28r8vdq719g1pg0l2") (y #t)))

