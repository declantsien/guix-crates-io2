(define-module (crates-io au ru aurum-winbase) #:use-module (crates-io))

(define-public crate-aurum-winbase-0.2.0 (c (n "aurum-winbase") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "07hswv8p8hqkyd3g3rn3vhfl1bkvndb7r1nm117zzin6c7pjxxis") (y #t)))

