(define-module (crates-io au ru aurum-image) #:use-module (crates-io))

(define-public crate-aurum-image-0.2.0 (c (n "aurum-image") (v "0.2.0") (d (list (d (n "aurum-color") (r "^0.2.0") (d #t) (k 0)) (d (n "aurum-linear") (r "^0.2.0") (d #t) (k 0)) (d (n "aurum-numeric") (r "^0.2.0") (d #t) (k 0)))) (h "0wxpqq6rlgbb5rjszxnm8bw8ndydcskrlqzm3yw32wivy7l0ycc6") (y #t)))

