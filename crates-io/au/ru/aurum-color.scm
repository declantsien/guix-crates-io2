(define-module (crates-io au ru aurum-color) #:use-module (crates-io))

(define-public crate-aurum-color-0.2.0 (c (n "aurum-color") (v "0.2.0") (d (list (d (n "aurum-numeric") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.8.21") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.21") (o #t) (d #t) (k 0)))) (h "0nj5p0n9dryiq4v08b07jdd491sppp8zi7p21nhympbysfzxczdg") (y #t)))

