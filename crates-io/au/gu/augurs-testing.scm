(define-module (crates-io au gu augurs-testing) #:use-module (crates-io))

(define-public crate-augurs-testing-0.1.0-alpha.0 (c (n "augurs-testing") (v "0.1.0-alpha.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1gs95ffxlyi028yzyz9czis6xnmlai8nkswvw8gnc8jy16pn3nsj")))

(define-public crate-augurs-testing-0.1.0 (c (n "augurs-testing") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1fgr6zdi6fdvdl4za6jma73lzgmasvwrbcp0d6x5ficilfafs0qd")))

(define-public crate-augurs-testing-0.1.1 (c (n "augurs-testing") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "12bk25s85pzgydppsbba4zp0i4mi844sh7b49v2bfgja1gpq0l98")))

(define-public crate-augurs-testing-0.1.2 (c (n "augurs-testing") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "043zpj89hkck7hjc9nmc2lcs84bh1d54a2f8w8hvwp2cka4s4bf2")))

