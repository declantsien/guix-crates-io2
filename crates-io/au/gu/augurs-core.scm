(define-module (crates-io au gu augurs-core) #:use-module (crates-io))

(define-public crate-augurs-core-0.1.0-alpha.0 (c (n "augurs-core") (v "0.1.0-alpha.0") (d (list (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rayl82vrq4y344lk1g8r020h04mwik79sxl6ln48z8yfk71yfb1")))

(define-public crate-augurs-core-0.1.0 (c (n "augurs-core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17ws6h949jn8xv0f0f48k9k1jy2s402amp60q9y16l7nlqqcv1nh")))

(define-public crate-augurs-core-0.1.1 (c (n "augurs-core") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0w5b8v7mgncj5vwinybn3s8knrcxdm34wpcb48hq9j882b5m2a7k")))

(define-public crate-augurs-core-0.1.2 (c (n "augurs-core") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wzxdh94qmvvmyjb42lbgh64xnrs78gr5ps2vcb6441dfvr4ya08")))

