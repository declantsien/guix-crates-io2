(define-module (crates-io au xv auxv) #:use-module (crates-io))

(define-public crate-auxv-0.1.0 (c (n "auxv") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.41") (d #t) (k 1)) (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "libc") (r "^0.2.19") (d #t) (k 1)))) (h "0xmlz8nq7a5zy8lhnzi2r3ws6p7r16gsb759ss2p4n1a3na7pm78") (f (quote (("auxv-64bit-ulong") ("auxv-32bit-ulong"))))))

(define-public crate-auxv-0.2.0 (c (n "auxv") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.41") (d #t) (k 1)) (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "libc") (r "^0.2.19") (d #t) (k 1)))) (h "0wf7i5p9nz511kplihd2zfjlibz3i6g7j2ql2l2v8ynsa76g8axi") (f (quote (("auxv-64bit-ulong") ("auxv-32bit-ulong"))))))

(define-public crate-auxv-0.3.1 (c (n "auxv") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.41") (d #t) (k 1)) (d (n "libc") (r "^0.2.19") (d #t) (k 2)))) (h "0giphs6vn3fg40g942x5jdsph04jh1hm3dadg9p1mgjhnn2gyfk0")))

(define-public crate-auxv-0.3.2 (c (n "auxv") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.41") (d #t) (k 1)) (d (n "libc") (r "^0.2.19") (d #t) (k 2)))) (h "19lp5qh8lfkxm27qz81qj55fi9s2i72h7ggkrw9kdbbbsirkwpvq")))

(define-public crate-auxv-0.3.3 (c (n "auxv") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.41") (d #t) (k 1)) (d (n "libc") (r "^0.2.19") (d #t) (k 2)))) (h "0gasmz0pkl6qra2r5c039xgb09maxrv1ra4z741gpvxqpvwk0175")))

