(define-module (crates-io au tu autumn) #:use-module (crates-io))

(define-public crate-autumn-0.2.0 (c (n "autumn") (v "0.2.0") (h "15yczq7x8m658p4vwgbbszdnqwp436dbm8ayg98aknifs1nng9d7")))

(define-public crate-autumn-0.4.0 (c (n "autumn") (v "0.4.0") (h "1vk5mgvwzra52fjpiwf5442bdmp0vx31d1k8gkxq3b9qyr8dsvfs")))

(define-public crate-autumn-0.4.1 (c (n "autumn") (v "0.4.1") (h "13631wbbh7ysxriz904s05giwi5jggxrxbq3k2gy11zhx17yh87l")))

(define-public crate-autumn-0.4.2 (c (n "autumn") (v "0.4.2") (h "0s5lrq00a93gjfqrxzv3lgm4h39i3wdhy04k5pdq30n1pwyv485i")))

(define-public crate-autumn-0.4.3 (c (n "autumn") (v "0.4.3") (h "1jcwgb7hbpg8lsis70i5y0cx7s7b24wmr5amzg6l7hah6blzd9kx")))

