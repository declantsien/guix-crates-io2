(define-module (crates-io au di audio-processor-iced-storybook) #:use-module (crates-io))

(define-public crate-audio-processor-iced-storybook-0.2.0 (c (n "audio-processor-iced-storybook") (v "0.2.0") (d (list (d (n "audio-processor-iced-design-system") (r "^0.3.1") (d #t) (k 0)) (d (n "iced") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wisual-logger") (r "^0.1.3") (d #t) (k 0)))) (h "0a6w44qw1mhgq6wmmilik3ygs7sbyf4is1aq6nnmm761jh04dzcc") (f (quote (("wgpu" "iced/wgpu" "audio-processor-iced-design-system/wgpu") ("glow" "iced/glow" "audio-processor-iced-design-system/glow") ("default"))))))

(define-public crate-audio-processor-iced-storybook-1.0.0 (c (n "audio-processor-iced-storybook") (v "1.0.0") (d (list (d (n "audio-processor-iced-design-system") (r "^1.0.0") (d #t) (k 0)) (d (n "iced") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wisual-logger") (r "^0.1.3") (d #t) (k 0)))) (h "1r08mzzd8sfwf2k6zzdmdrbaimbbgg7n00q7196b4jf5f7h309f6") (f (quote (("wgpu" "iced/wgpu" "audio-processor-iced-design-system/wgpu") ("glow" "iced/glow" "audio-processor-iced-design-system/glow") ("default"))))))

