(define-module (crates-io au di audi) #:use-module (crates-io))

(define-public crate-audi-0.1.0 (c (n "audi") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1h50v13pcj86ga8s45nbjn7biafwzfj8hm5hs8wg5zccjbfmc08w")))

(define-public crate-audi-0.2.0 (c (n "audi") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0p1gy3c0z5zf4ixsrrk7c7ik7sl6dgvkmh4r51g0sk40mbvjam60")))

(define-public crate-audi-0.2.1 (c (n "audi") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1fgkxyg5ffj8y2mnk3dzfdpc7q8bwhhry4vlsw97yh443g9jlqh2")))

(define-public crate-audi-0.2.2 (c (n "audi") (v "0.2.2") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0cbsdk5nf8c7r1418xaif2apyz4j9ki7ikhr4r5qy183yi1xpv02")))

(define-public crate-audi-0.2.3 (c (n "audi") (v "0.2.3") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "13v1g73y411805p19qn5zxrjrnsmdmdp8cbwhs4k6csg3ix9zrqf")))

(define-public crate-audi-0.2.4 (c (n "audi") (v "0.2.4") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.5") (d #t) (k 0)))) (h "0wp2sh071px99fvs1sqmw8lpgf2c88l48hx8kp1z27c4lr3ffngc")))

(define-public crate-audi-0.3.0 (c (n "audi") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.5") (d #t) (k 0)))) (h "1g82d7hmynwms0a8a5jn3fjdvvafrqpq0b5vv0zf5a62irhzlf3m")))

