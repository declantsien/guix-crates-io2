(define-module (crates-io au di audiodb) #:use-module (crates-io))

(define-public crate-audiodb-1.0.0 (c (n "audiodb") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "1ibbkw9rvq2hff2dsian03n84q904770q1k4062ba4c5l1jqgbhn")))

