(define-module (crates-io au di auditable-extract) #:use-module (crates-io))

(define-public crate-auditable-extract-0.1.0-rc0 (c (n "auditable-extract") (v "0.1.0-rc0") (d (list (d (n "binfarce") (r "^0.1.0-rc0") (d #t) (k 0)))) (h "176cd394ysb9pkgjii8s581ckqcyvhcn3fsfgnkh8380p4sxd7dr")))

(define-public crate-auditable-extract-0.1.0 (c (n "auditable-extract") (v "0.1.0") (d (list (d (n "binfarce") (r "^0.1.0") (d #t) (k 0)))) (h "0xwyxwm5pfikdn277qkrvnk1nrzz6zzd78xqxhkpv3i5jcqzyqhm")))

(define-public crate-auditable-extract-0.2.0-alpha.1 (c (n "auditable-extract") (v "0.2.0-alpha.1") (d (list (d (n "binfarce") (r "^0.2") (d #t) (k 0)))) (h "0pidvfbi33wjknpi2ini57i5j0cz8z7pvvc0n7v9mwhk27vd4h8x")))

(define-public crate-auditable-extract-0.2.0 (c (n "auditable-extract") (v "0.2.0") (d (list (d (n "binfarce") (r "^0.2") (d #t) (k 0)))) (h "164vlmvbwpcbqfbsb95mni0rqnm7fbphb2j21rgp0mcv92vs28qf")))

(define-public crate-auditable-extract-0.3.0 (c (n "auditable-extract") (v "0.3.0") (d (list (d (n "binfarce") (r "^0.2") (d #t) (k 0)))) (h "19874064s8iiy6v15z1q5a012rfcpb27mvhlqg9la22cf8hw5r6q")))

(define-public crate-auditable-extract-0.3.1 (c (n "auditable-extract") (v "0.3.1") (d (list (d (n "binfarce") (r "^0.2") (d #t) (k 0)))) (h "1k3fyqbg08ifp3dbpkxbgjhyh1dypw1bg8n7kbv81g90hxzziigp")))

(define-public crate-auditable-extract-0.3.2 (c (n "auditable-extract") (v "0.3.2") (d (list (d (n "binfarce") (r "^0.2") (d #t) (k 0)))) (h "0asy1fvsjzdgmq382r5cwhbqs8l7jndh9jjz1frjlajj99pjm9pq")))

(define-public crate-auditable-extract-0.3.3 (c (n "auditable-extract") (v "0.3.3") (d (list (d (n "binfarce") (r "^0.2") (d #t) (k 0)) (d (n "wasmparser") (r "^0.206.0") (o #t) (d #t) (k 0)))) (h "0in0695cfhsahd03xdgaigmwy8frd4649k8vlcs9480qngkblybz") (f (quote (("wasm" "wasmparser"))))))

(define-public crate-auditable-extract-0.3.4 (c (n "auditable-extract") (v "0.3.4") (d (list (d (n "binfarce") (r "^0.2") (d #t) (k 0)) (d (n "wasmparser") (r "^0.207.0") (o #t) (k 0)))) (h "0vpr6an8qbh8sk1f7yvrrylvqm05rhpvrhw4daqrhfdsnryaca37") (f (quote (("wasm" "wasmparser") ("default" "wasm"))))))

