(define-module (crates-io au di audit-info) #:use-module (crates-io))

(define-public crate-audit-info-0.5.0-alpha.1 (c (n "audit-info") (v "0.5.0-alpha.1") (d (list (d (n "auditable-extract") (r "^0.3.0") (d #t) (k 0)) (d (n "auditable-serde") (r "^0.5.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (o #t) (d #t) (k 0)))) (h "0pkgcxaqdb381alxx6sshzvwgikvghbjklqhwc44ndf2gk8azknf") (f (quote (("serde" "serde_json") ("default" "serde"))))))

(define-public crate-audit-info-0.5.0-alpha.2 (c (n "audit-info") (v "0.5.0-alpha.2") (d (list (d (n "auditable-extract") (r "^0.3.0") (d #t) (k 0)) (d (n "auditable-serde") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (o #t) (d #t) (k 0)))) (h "0rhbxsrm3bd85fr2ggayz8nbnx2qlz6d88iwvm040831zgdwkvnk") (f (quote (("serde" "serde_json" "auditable-serde") ("default" "serde"))))))

(define-public crate-audit-info-0.5.0-alpha.3 (c (n "audit-info") (v "0.5.0-alpha.3") (h "00qswcg0qb5j1mbgv839b60gnrwkjb6iljjdgir0wf756gldv4ll")))

