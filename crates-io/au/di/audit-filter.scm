(define-module (crates-io au di audit-filter) #:use-module (crates-io))

(define-public crate-audit-filter-0.1.0 (c (n "audit-filter") (v "0.1.0") (d (list (d (n "docopt") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "1hk7a9mr1lnz7pijd57akqawfsjdasz9hi8npbhh9ghnqnq3q73h")))

(define-public crate-audit-filter-0.1.1 (c (n "audit-filter") (v "0.1.1") (d (list (d (n "docopt") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "0zgmvf014fhj471aryf0dzim9a09m81imsql065gnaf3h61nxqf8")))

(define-public crate-audit-filter-0.2.0 (c (n "audit-filter") (v "0.2.0") (d (list (d (n "docopt") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "14vz4cmbhdv3wm5b6q7qmk7lavmqkqb90nlqizq92jvm8zxw13gr")))

(define-public crate-audit-filter-0.2.1 (c (n "audit-filter") (v "0.2.1") (d (list (d (n "docopt") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "02k4a9ghldi7k2hqrsmswmlgyac6bn239j54jbj6mbjy9lkg8hi5")))

(define-public crate-audit-filter-0.2.2 (c (n "audit-filter") (v "0.2.2") (d (list (d (n "docopt") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "0l38adh2kvfgf3svj3jyfmgv701m498fbaznh2lz0ryynbwqzdc3")))

(define-public crate-audit-filter-0.2.3 (c (n "audit-filter") (v "0.2.3") (d (list (d (n "docopt") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "167qk8lsn9wx2hcfpmkz72sdm06jlb6b8c8mza6qbl3p09b1dsrg")))

(define-public crate-audit-filter-0.2.4 (c (n "audit-filter") (v "0.2.4") (d (list (d (n "docopt") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "1xsi71v13vgwwvdmh440k0rrc2xl4jnll7zc7sva37ymv3sjinjs")))

(define-public crate-audit-filter-0.2.5 (c (n "audit-filter") (v "0.2.5") (d (list (d (n "docopt") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "06z5y2ylvsjq47vdkw9g197qbs9lhvzz2mma71rl2ffvxjy7rlj6")))

(define-public crate-audit-filter-0.3.0 (c (n "audit-filter") (v "0.3.0") (d (list (d (n "docopt") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0spnk6451gnrv1cadwyky1rwfj3hc902hhkrj5msn7qg0r391as4")))

(define-public crate-audit-filter-0.4.0 (c (n "audit-filter") (v "0.4.0") (d (list (d (n "docopt") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1m0wps23jbwn1iv41iy5npz7qanzajxj2zv1wrijh19r56ccwga3")))

(define-public crate-audit-filter-0.4.1 (c (n "audit-filter") (v "0.4.1") (d (list (d (n "docopt") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0vrfckax0fj1ip827ig9knp7xprg0vq8iacn05a62grfra2xl2s6")))

(define-public crate-audit-filter-0.4.2 (c (n "audit-filter") (v "0.4.2") (d (list (d (n "docopt") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "04rj77f589qsckkrph4qg6mh8b67j2i9y601wdr7cvyf86qm3rz2")))

(define-public crate-audit-filter-0.4.3 (c (n "audit-filter") (v "0.4.3") (d (list (d (n "docopt") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0wijc7alk5dwb87jz8dkl23chmx524mnpinfwpnrq66pi8li8gs6")))

(define-public crate-audit-filter-0.5.0 (c (n "audit-filter") (v "0.5.0") (d (list (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "05xf8xyihawd1xzp4qjsnlgw8mr4wxax3xg8ivyv42mvf900iy0b")))

