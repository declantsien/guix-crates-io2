(define-module (crates-io au di audio-processor-testing-helpers) #:use-module (crates-io))

(define-public crate-audio-processor-testing-helpers-1.0.0-alpha.1 (c (n "audio-processor-testing-helpers") (v "1.0.0-alpha.1") (d (list (d (n "audio-processor-traits") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0whw1ggia5lkqyjm84rd2khp6y4wpz86rdbjvvdq34gwk1a86db6")))

(define-public crate-audio-processor-testing-helpers-1.0.0-alpha.2 (c (n "audio-processor-testing-helpers") (v "1.0.0-alpha.2") (d (list (d (n "audio-processor-traits") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "05ji8gz7pbb83gc7lhh7lwwq99hcrxg9ryg6ldbw8q6gz6qjmb8b")))

(define-public crate-audio-processor-testing-helpers-1.0.0-alpha.3 (c (n "audio-processor-testing-helpers") (v "1.0.0-alpha.3") (d (list (d (n "audio-processor-traits") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0xb79y74dayp8kq8qynw9frkmlmlmkl0m774qacsscwmbfcxnc2b")))

(define-public crate-audio-processor-testing-helpers-1.0.0-alpha.4 (c (n "audio-processor-testing-helpers") (v "1.0.0-alpha.4") (d (list (d (n "audio-processor-traits") (r "^1.0.0-alpha.4") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "00dwd2p7y2i5664240slyxw4drspcjqbcm0y6300r26406b4csjj")))

(define-public crate-audio-processor-testing-helpers-1.0.0-alpha.5 (c (n "audio-processor-testing-helpers") (v "1.0.0-alpha.5") (d (list (d (n "audio-processor-traits") (r "^1.0.0-alpha.5") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "02q0y1azx7c2pdj6hmajcxspndxy10q87ix948mylbzdmdvaj71p")))

(define-public crate-audio-processor-testing-helpers-1.0.0-alpha.6 (c (n "audio-processor-testing-helpers") (v "1.0.0-alpha.6") (d (list (d (n "audio-processor-traits") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.5") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1a6d5vfphbbigmpw4kxcyywv3drr6v7adyrfpbxa745rrf0rc569")))

(define-public crate-audio-processor-testing-helpers-1.0.0-alpha.7 (c (n "audio-processor-testing-helpers") (v "1.0.0-alpha.7") (d (list (d (n "audio-processor-traits") (r "^1.0.0-alpha.7") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0wnz1hj3zr1hsplq02xlzy1ym448xq7sgyyz5s6m3gyyx0s0rq4w")))

(define-public crate-audio-processor-testing-helpers-1.0.0-alpha.8 (c (n "audio-processor-testing-helpers") (v "1.0.0-alpha.8") (d (list (d (n "audio-processor-traits") (r "^1.0.0-alpha.8") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1k5dyahnb6ql2w7riclj8336flhc7jdmqb5bmm5k1q3y6wz940ik")))

(define-public crate-audio-processor-testing-helpers-1.1.0 (c (n "audio-processor-testing-helpers") (v "1.1.0") (d (list (d (n "audio-processor-traits") (r "^1.1.0") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.1.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0xkyvf18kpgpf4g04nmyp856nrmk5gadl6la6m4cqzkslvnd1iaf")))

(define-public crate-audio-processor-testing-helpers-1.2.0 (c (n "audio-processor-testing-helpers") (v "1.2.0") (d (list (d (n "audio-processor-traits") (r "^2.1.0") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.1.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1b6a2mhk2vnapbs9abxvq0bzhqvc350wmslnzhra10ai5bxbnbl9")))

(define-public crate-audio-processor-testing-helpers-1.3.0 (c (n "audio-processor-testing-helpers") (v "1.3.0") (d (list (d (n "audio-processor-traits") (r "^2.2.0") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.2.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "13a5gfiphn3vll7xwhirq7001wqifdk89jkj83x0cy53780jc6fx")))

(define-public crate-audio-processor-testing-helpers-2.3.0 (c (n "audio-processor-testing-helpers") (v "2.3.0") (d (list (d (n "audio-processor-traits") (r "^3.2.0") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.2.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "176lwzxlbyjfmmqyk6bjs9s9ndp47z8llla9zj1ip6sfzby39rw1")))

(define-public crate-audio-processor-testing-helpers-2.4.0 (c (n "audio-processor-testing-helpers") (v "2.4.0") (d (list (d (n "audio-processor-traits") (r "^4.0.0") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.2.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0gx8bi4xcr26l9kxvpzzca92ddwa8xiysizhhpjg7396y9kxc7vs")))

(define-public crate-audio-processor-testing-helpers-2.5.0 (c (n "audio-processor-testing-helpers") (v "2.5.0") (d (list (d (n "audio-processor-traits") (r "^4.1.0") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.3.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1y9016x4spqazfglsfm8faydsmgnsksbv94kdaa1gfy99pmrxdi9")))

(define-public crate-audio-processor-testing-helpers-2.6.0 (c (n "audio-processor-testing-helpers") (v "2.6.0") (d (list (d (n "audio-processor-traits") (r "^4.2.0") (d #t) (k 0)) (d (n "augmented_oscillator") (r "^1.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1l7hs0m80v7grkk8g8070agvqfmm5ffyzvvrrwdzshjj5bb50rsx")))

