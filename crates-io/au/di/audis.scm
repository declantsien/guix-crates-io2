(define-module (crates-io au di audis) #:use-module (crates-io))

(define-public crate-audis-0.1.0 (c (n "audis") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "redis") (r "^0.13") (d #t) (k 0)))) (h "16162nbr7qp68cy54f6lqrwq42imh2x5nc41vkdzc9943drld1yq") (y #t)))

(define-public crate-audis-0.2.0 (c (n "audis") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "redis") (r "^0.13") (d #t) (k 0)))) (h "0nbdcpwm440wab6vssn6pfmg2iim8qmq5kjsvjqy7frcmf9hw360")))

(define-public crate-audis-0.2.1 (c (n "audis") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "redis") (r "^0.13") (d #t) (k 0)))) (h "18l8b8clz5j02gdpw3b3h0yj1w8h8fk82yyp70kkbyxpfp9hjgxw")))

