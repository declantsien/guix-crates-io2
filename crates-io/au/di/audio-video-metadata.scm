(define-module (crates-io au di audio-video-metadata) #:use-module (crates-io))

(define-public crate-audio-video-metadata-0.1.0 (c (n "audio-video-metadata") (v "0.1.0") (d (list (d (n "mp3-metadata") (r "^0.1") (d #t) (k 0)) (d (n "mp4parse") (r "^0.4") (d #t) (k 0)) (d (n "ogg_metadata") (r "^0.3") (d #t) (k 0)))) (h "1wvswysyy3hy9yz2zwkr81c3lkzab92zylzc8b61grb0nmqy7j3b")))

(define-public crate-audio-video-metadata-0.1.1 (c (n "audio-video-metadata") (v "0.1.1") (d (list (d (n "mp3-metadata") (r "^0.1") (d #t) (k 0)) (d (n "mp4parse") (r "^0.4") (d #t) (k 0)) (d (n "ogg_metadata") (r "^0.3") (d #t) (k 0)))) (h "1bdsynic1cc2lavg6ncl31m1gcpfq86pvh2763qsbyg596qhpfmm")))

(define-public crate-audio-video-metadata-0.1.2 (c (n "audio-video-metadata") (v "0.1.2") (d (list (d (n "mp3-metadata") (r "^0.2") (d #t) (k 0)) (d (n "mp4parse") (r "^0.5") (d #t) (k 0)) (d (n "ogg_metadata") (r "^0.3") (d #t) (k 0)))) (h "0014mqrbp7nhylapqw1l88f8kxvcq9hr45wc46pkzzl9rd82bnh3")))

(define-public crate-audio-video-metadata-0.1.3 (c (n "audio-video-metadata") (v "0.1.3") (d (list (d (n "mp3-metadata") (r "^0.2") (d #t) (k 0)) (d (n "mp4parse") (r "^0.7") (d #t) (k 0)) (d (n "ogg_metadata") (r "^0.4") (d #t) (k 0)))) (h "1l999jb2xz250k5dc3hijhi7f907nizzsis5ycvmmncax6gg4viv")))

(define-public crate-audio-video-metadata-0.1.4 (c (n "audio-video-metadata") (v "0.1.4") (d (list (d (n "mp3-metadata") (r "^0.3") (d #t) (k 0)) (d (n "mp4parse") (r "^0.7") (d #t) (k 0)) (d (n "ogg_metadata") (r "^0.4") (d #t) (k 0)))) (h "05j4wzvj5cgmca5jqqzrw8xnga7r2cfvw9pqqahgg72kjrbgv9gr")))

(define-public crate-audio-video-metadata-0.1.5 (c (n "audio-video-metadata") (v "0.1.5") (d (list (d (n "mp3-metadata") (r "^0.3") (d #t) (k 0)) (d (n "mp4parse") (r "^0.8") (d #t) (k 0)) (d (n "ogg_metadata") (r "^0.4") (d #t) (k 0)))) (h "0glf4wqyrzvdal13y14ivf70kiawld289v2d9izmimplkk5xvipg")))

(define-public crate-audio-video-metadata-0.1.6 (c (n "audio-video-metadata") (v "0.1.6") (d (list (d (n "mp3-metadata") (r "^0.3") (d #t) (k 0)) (d (n "mp4parse") (r "^0.9") (d #t) (k 0)) (d (n "ogg_metadata") (r "^0.4") (d #t) (k 0)))) (h "063dj5cb5fxcx67pyvff2df9sbsfrlnprfjg4wnajnwz0y160lvi")))

(define-public crate-audio-video-metadata-0.1.7 (c (n "audio-video-metadata") (v "0.1.7") (d (list (d (n "mp3-metadata") (r "^0.3") (d #t) (k 0)) (d (n "mp4parse") (r "^0.10") (d #t) (k 0)) (d (n "ogg_metadata") (r "^0.4") (d #t) (k 0)))) (h "0p5d5irsd16yczwdsbhrszkpazaxdxiwmkzmh79rqnxkj41608rw")))

