(define-module (crates-io au di audio-widgets) #:use-module (crates-io))

(define-public crate-audio-widgets-0.1.0 (c (n "audio-widgets") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "scales") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("CanvasRenderingContext2d" "Document" "Element" "HtmlCanvasElement" "Window" "CssStyleDeclaration" "DomRect" "TouchEvent" "TouchList" "Touch"))) (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0h8r34wrg6xbrn5i78vlmba4ywrfv3k1jmjl5mmj0wkb6l9gr6qa") (f (quote (("Yew" "yew" "JS") ("JS" "wasm-bindgen" "js-sys" "web-sys"))))))

