(define-module (crates-io au di audio-codec-algorithms) #:use-module (crates-io))

(define-public crate-audio-codec-algorithms-0.5.2 (c (n "audio-codec-algorithms") (v "0.5.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "00iz0iwxbb2pnabpv6k38hyklw1gh5lvfi6v8k8mql314sgs8i6h")))

(define-public crate-audio-codec-algorithms-0.5.5 (c (n "audio-codec-algorithms") (v "0.5.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0nz09c8rbivd7yg0wr55kl7adbczg3b7k3a9pz8y00jz31h90a32") (s 2) (e (quote (("internal-no-panic" "dep:no-panic"))))))

(define-public crate-audio-codec-algorithms-0.5.6 (c (n "audio-codec-algorithms") (v "0.5.6") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "08qk235fr1wnxxxrd1hm19xlfv6b1zancsf30n59461a1r9mf832") (s 2) (e (quote (("internal-no-panic" "dep:no-panic"))))))

