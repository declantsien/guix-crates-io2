(define-module (crates-io au di audio-time) #:use-module (crates-io))

(define-public crate-audio-time-0.0.1-alpha.1 (c (n "audio-time") (v "0.0.1-alpha.1") (d (list (d (n "audio-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05lfmia9amh628c4lgqccrig6b1v4zkcqfbgy4wwm95d2sdfxgkv") (y #t)))

(define-public crate-audio-time-0.0.1-alpha.2 (c (n "audio-time") (v "0.0.1-alpha.2") (d (list (d (n "audio-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bvl3bvbfvxf5p85dvzaw0bn5852rakx6c3frk0l66lk3ybisyzn")))

