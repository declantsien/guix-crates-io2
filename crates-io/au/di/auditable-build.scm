(define-module (crates-io au di auditable-build) #:use-module (crates-io))

(define-public crate-auditable-build-0.1.0-rc0 (c (n "auditable-build") (v "0.1.0-rc0") (d (list (d (n "auditable-serde") (r "^0.1.0-rc0") (f (quote ("from_metadata"))) (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0pcxn9lxf7w89iwr7n959zzmfz3nd069q8p4fn1nhh55d7fb4g2k")))

(define-public crate-auditable-build-0.1.0 (c (n "auditable-build") (v "0.1.0") (d (list (d (n "auditable-serde") (r "^0.1.0") (f (quote ("from_metadata"))) (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1lhv7a3hk9jqv8yiyral4m2qmsdc5r9cv51ks3lv2nmnr3ma8xbv")))

(define-public crate-auditable-build-0.2.0-alpha.1 (c (n "auditable-build") (v "0.2.0-alpha.1") (d (list (d (n "auditable-serde") (r "^0.2.0-alpha.1") (f (quote ("from_metadata"))) (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1njz52hrmylswn10kzmfr45m64qr9qzsnf26288a99pqkmj24q9b")))

(define-public crate-auditable-build-0.2.0 (c (n "auditable-build") (v "0.2.0") (d (list (d (n "auditable-serde") (r "^0.2.0") (f (quote ("from_metadata"))) (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1jqixrp2b1v0f4zwcic88m3l7z6nbcaqcvd3l75la4kmijq3mky4")))

