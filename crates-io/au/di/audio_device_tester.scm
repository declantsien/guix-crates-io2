(define-module (crates-io au di audio_device_tester) #:use-module (crates-io))

(define-public crate-audio_device_tester-0.1.4 (c (n "audio_device_tester") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "cpal") (r "^0.14.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1wi4jw2ndhqdnd8x0qrwmjjdl2q9ffzhax4994ggnccdkgb0qfjd")))

(define-public crate-audio_device_tester-0.1.5 (c (n "audio_device_tester") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "cpal") (r "^0.14.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0ivk1cq7xzi8s2cbqxr5xijsizgacs7waasbj8727abaakx7fl4q")))

