(define-module (crates-io au di audioviz) #:use-module (crates-io))

(define-public crate-audioviz-0.1.0 (c (n "audioviz") (v "0.1.0") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "1ygdffi0cj5xz2mjdq18ah0m6bi4vk95xab97m990yvnqvlkpayp")))

(define-public crate-audioviz-0.1.1 (c (n "audioviz") (v "0.1.1") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "13ld2gjpy0ww90jwlv8gbp0zbs8nrzk0bi6gjdd7i6s3a5a0x3d8")))

(define-public crate-audioviz-0.1.2 (c (n "audioviz") (v "0.1.2") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "1v0rbkf2yki8w3c58bfx6n26pnjd483qgssz0pwg8d3w81q7wjxq")))

(define-public crate-audioviz-0.1.3 (c (n "audioviz") (v "0.1.3") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "0p6a0bz0rnq7x1aq4cm6809i4gwf9pkqs665jhm2fpba7hqbd4bp")))

(define-public crate-audioviz-0.1.4 (c (n "audioviz") (v "0.1.4") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "1aw4vzfhvlsks6l55glkr3vzb0qrrx5p6qvdln83rsqrrlng2hh5")))

(define-public crate-audioviz-0.2.0 (c (n "audioviz") (v "0.2.0") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "splines") (r "^4.0.3") (d #t) (k 0)))) (h "1p1llzvkk6ym54f45pqfzbka53d4b3v58avf9zjdqcj6da6bd163")))

(define-public crate-audioviz-0.3.0 (c (n "audioviz") (v "0.3.0") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "splines") (r "^4.0.3") (d #t) (k 0)))) (h "0va5fmcmxjmxg38xkaaqn7irnvbz7csib4ngsvwzic2ljm78w6a7")))

(define-public crate-audioviz-0.3.1 (c (n "audioviz") (v "0.3.1") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "splines") (r "^4.0.3") (d #t) (k 0)))) (h "1f0634b316km7zxig94hinp20gjszbh9gqsy0zfl5rvm7yhrskis")))

(define-public crate-audioviz-0.3.2 (c (n "audioviz") (v "0.3.2") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "splines") (r "^4.0.3") (d #t) (k 0)))) (h "155r2rb00ix0nlmda2vz2ir7bmk7szj7ray46wgzj6axh9m2vimz")))

(define-public crate-audioviz-0.4.0 (c (n "audioviz") (v "0.4.0") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "cpal") (r "^0.13.4") (o #t) (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "splines") (r "^4.0.3") (d #t) (k 0)))) (h "0wjjdi1q7lpplniy6z09cp8v0xjrnwkvdf0qn5s21ay4g8jd0md7") (f (quote (("default" "cpal"))))))

(define-public crate-audioviz-0.4.1 (c (n "audioviz") (v "0.4.1") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "cpal") (r "^0.13.4") (o #t) (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "splines") (r "^4.0.3") (d #t) (k 0)))) (h "0b7z4nb1yk5xzgx0vsj3x99r2n9qf0v0qw4wdilh26ymgmg8pszf") (f (quote (("default" "cpal"))))))

(define-public crate-audioviz-0.4.2 (c (n "audioviz") (v "0.4.2") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "cpal") (r "^0.13.4") (o #t) (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "splines") (r "^4.0.3") (d #t) (k 0)))) (h "0nsajy3c5kc6xz894wb89kqxi1a9csi9dm9mlgivkxbr5zvzv67i") (f (quote (("default" "cpal"))))))

(define-public crate-audioviz-0.4.3 (c (n "audioviz") (v "0.4.3") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "cpal") (r "^0.13.4") (o #t) (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "splines") (r "^4.0.3") (d #t) (k 0)))) (h "0bnl2f3h65j75z1za6xlfxmd4z9xv3bwgxrqskmf2qr7y5sy3nnd") (f (quote (("default" "cpal")))) (y #t)))

(define-public crate-audioviz-0.4.4 (c (n "audioviz") (v "0.4.4") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "cpal") (r "^0.13.4") (o #t) (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "splines") (r "^4.0.3") (d #t) (k 0)))) (h "1fgwxqzf7hb68cvklcfh0drq6lmh4j0jqg6skadczy1r00z1kn1z") (f (quote (("default" "cpal"))))))

(define-public crate-audioviz-0.5.0 (c (n "audioviz") (v "0.5.0") (d (list (d (n "apodize") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "cpal") (r "^0.13.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "splines") (r "^4.0.3") (o #t) (d #t) (k 0)))) (h "1wxqarnkay7d6wbkqk515ycpfjvbfa1xcg58yg0sdlqxdd64j747") (f (quote (("std") ("spectrum" "fft" "apodize" "splines") ("processor" "fft") ("io" "std" "cpal") ("fft" "rustfft") ("default" "std" "io" "spectrum" "processor"))))))

(define-public crate-audioviz-0.6.0 (c (n "audioviz") (v "0.6.0") (d (list (d (n "apodize") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "cpal") (r "^0.13.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "splines") (r "^4.0.3") (o #t) (d #t) (k 0)))) (h "0r2cr7a910n7n5dyl8vfgvx6421gps80d712nq7yq34w349dc964") (f (quote (("std") ("spectrum" "fft" "apodize" "splines") ("processor" "fft" "apodize") ("lissajous") ("io" "std" "cpal") ("fft" "rustfft") ("default" "std" "io" "spectrum" "processor" "lissajous"))))))

