(define-module (crates-io au di audio-mixer) #:use-module (crates-io))

(define-public crate-audio-mixer-0.1.0 (c (n "audio-mixer") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1iyy2flh6haf2knry0rbhp9yx334dskkz3kbm9waf9f35rkmsmg6")))

(define-public crate-audio-mixer-0.1.1 (c (n "audio-mixer") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0asizwnnab3bfm81gkhrpdh7018mdasp9mdxq2k5aixjjzy75wr7")))

(define-public crate-audio-mixer-0.1.2 (c (n "audio-mixer") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "float-cmp") (r "^0.6") (d #t) (k 2)))) (h "16gzirh7mwvvlbfnvwp3829pv7sxwfcai7rz2b8pf90cif5j14nd")))

(define-public crate-audio-mixer-0.1.3 (c (n "audio-mixer") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "float-cmp") (r "^0.6") (d #t) (k 2)))) (h "1qdk5m5yvw2z3c3mpnbkinvvga8l73sjvifgyf7yag4pq44c07f4")))

(define-public crate-audio-mixer-0.2.0 (c (n "audio-mixer") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "float-cmp") (r "^0.6") (d #t) (k 2)))) (h "1cji1jjfjhmf9sffpxjy55k4ar1ramb067xcq6x89hl55ymla4jg")))

