(define-module (crates-io au di audiotags2) #:use-module (crates-io))

(define-public crate-audiotags2-0.3.0 (c (n "audiotags2") (v "0.3.0") (d (list (d (n "audiotags-dev-macro") (r "^0.1.4") (d #t) (k 0)) (d (n "id3") (r "^1.0.3") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.11.0") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1.0") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1aqn3x85k0a6bz954qcmsmn09v4a9sc16d5kyl0ra19mp70vmk42") (f (quote (("from") ("default" "from"))))))

(define-public crate-audiotags2-0.3.1 (c (n "audiotags2") (v "0.3.1") (d (list (d (n "audiotags-dev-macro") (r "^0.1.4") (d #t) (k 0)) (d (n "id3") (r "^1.1.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.11.0") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1.0") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1b3hlwm4i8r9h4iai3mf6ks1bg74k1a3k7n8nnjrfzi9vcqhiy86") (f (quote (("from") ("default" "from"))))))

