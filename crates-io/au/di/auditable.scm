(define-module (crates-io au di auditable) #:use-module (crates-io))

(define-public crate-auditable-0.0.1 (c (n "auditable") (v "0.0.1") (h "13n6pc494w0xljh3145bwrx7smd9x22si5yja7xf4y4swvx89fgb")))

(define-public crate-auditable-0.0.2 (c (n "auditable") (v "0.0.2") (h "1hhnxvm1h2ckjq8iy2jcqd7h5y4yii5gnb9wp0q5q0v6pb64r6v1")))

(define-public crate-auditable-0.0.3 (c (n "auditable") (v "0.0.3") (h "1m31sqqknbqch5pl97fz5pj47r0n2dzcfgzc1jniqnnalbbss6wh")))

(define-public crate-auditable-0.1.0-rc0 (c (n "auditable") (v "0.1.0-rc0") (h "0cw3s7xqn0hsc1m78a5z91fciiyydvv60v879p33vm9q291484hr")))

(define-public crate-auditable-0.1.0 (c (n "auditable") (v "0.1.0") (h "14g8ca1b902ynj9q1zlynrcz64l16n8b85025bccbl1k6kcqhmja")))

(define-public crate-auditable-0.2.0-alpha.1 (c (n "auditable") (v "0.2.0-alpha.1") (h "14kv8zwaaxqd0i35lq8vlqk4sljrlk03v89fsmbngzsf59aq46lg")))

(define-public crate-auditable-0.2.0 (c (n "auditable") (v "0.2.0") (h "0wss6jf1g325db7hjdv4qq95a2y38nzsl5r3imb9dn4vrbs7iy6g")))

