(define-module (crates-io au di audiotags-dev-macro) #:use-module (crates-io))

(define-public crate-audiotags-dev-macro-0.1.0 (c (n "audiotags-dev-macro") (v "0.1.0") (h "1rpmxswprmj4yrs6wkyz53mm0n5jaw3nwnkmd5qvz3wkj8jid0yf")))

(define-public crate-audiotags-dev-macro-0.1.1 (c (n "audiotags-dev-macro") (v "0.1.1") (h "00rvi74q68xc8d3hk7qnynhdwr7c7h1z7g0gn5wb0g89kh93zh4p")))

(define-public crate-audiotags-dev-macro-0.1.2 (c (n "audiotags-dev-macro") (v "0.1.2") (h "0grzy3bi75kwjly6y0zp6snvq76x5mni5j75439p6pyx73hqxw64")))

(define-public crate-audiotags-dev-macro-0.1.3 (c (n "audiotags-dev-macro") (v "0.1.3") (h "0rjh2kp0q6vpwwbkpv6i1l5rwa9bc0wd440fd3hplhbiqqq5za5m")))

(define-public crate-audiotags-dev-macro-0.1.4 (c (n "audiotags-dev-macro") (v "0.1.4") (h "0vmk7hlyqi2a131xq0z3dd30gvk362bgfz9j0qpk27qnj62jjy9v")))

