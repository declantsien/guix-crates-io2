(define-module (crates-io au di audio-codec-primitives) #:use-module (crates-io))

(define-public crate-audio-codec-primitives-0.5.2 (c (n "audio-codec-primitives") (v "0.5.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1gz2bfq4yw21gnc8x2n2cpj9cdwgq3z63kw2larpwnj27rlk4zm5") (y #t)))

(define-public crate-audio-codec-primitives-0.5.3 (c (n "audio-codec-primitives") (v "0.5.3") (h "0bvl63rgcika55l7329vdd1aq91n8zrwk1c5dfw4zkrz9xf1h2jy") (y #t)))

(define-public crate-audio-codec-primitives-0.5.4 (c (n "audio-codec-primitives") (v "0.5.4") (h "073j76bxk2a2lzjs8wm2srx1y3j148xw2hbgsr84xjn2f3b6lqfd") (y #t)))

