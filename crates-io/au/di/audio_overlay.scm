(define-module (crates-io au di audio_overlay) #:use-module (crates-io))

(define-public crate-audio_overlay-0.1.0 (c (n "audio_overlay") (v "0.1.0") (h "13y4fic4c5h1nzsadv262jhfd1h5kv34478cvjs675i8fara4wpm")))

(define-public crate-audio_overlay-0.1.1 (c (n "audio_overlay") (v "0.1.1") (h "1n1411clla5qi41rvdc77b70ikaw2qkfl3hi8plh91i7bv3af8kj")))

(define-public crate-audio_overlay-0.1.2 (c (n "audio_overlay") (v "0.1.2") (h "13jjikhhggsxajcimndg38wkyn3naz64d1h31h34mpy21ppf7icv")))

(define-public crate-audio_overlay-0.1.3 (c (n "audio_overlay") (v "0.1.3") (h "0idvqs4qzic6d93c4wbnpzl5rfq9l14s8l7z97z2f9c9i192akki")))

(define-public crate-audio_overlay-0.1.4 (c (n "audio_overlay") (v "0.1.4") (h "0x4xaa0266qp9li0q1x1gvyqd2wl4pci31ymx5i86n0dp2n0m38y")))

(define-public crate-audio_overlay-0.1.5 (c (n "audio_overlay") (v "0.1.5") (h "086xyrgn87vrh72n29i2n2rgf1wzhfl39xdmrfs6n0zrsdyzswf2")))

