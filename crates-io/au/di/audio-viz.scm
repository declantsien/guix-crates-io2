(define-module (crates-io au di audio-viz) #:use-module (crates-io))

(define-public crate-audio-viz-0.0.0 (c (n "audio-viz") (v "0.0.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "1022kyfvrfqpa7zk195p6rbxwdxn1fjsr3v2c7hyq9jzflgh99fa") (y #t) (r "1.75")))

(define-public crate-audio-viz-0.1.0 (c (n "audio-viz") (v "0.1.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "1vcn6ym2r1vjqlkpkw3h0c4qnjsny16ypdha11bjf1axklagvw90") (y #t) (r "1.75")))

(define-public crate-audio-viz-0.1.1 (c (n "audio-viz") (v "0.1.1") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "145i42bn7hgn0amj83hxy897d7d9h6jh81gvlz803agafvc26y6s") (r "1.75")))

(define-public crate-audio-viz-0.2.0 (c (n "audio-viz") (v "0.2.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "0dnlw8a68j9c6hnvg0f65dqp39mm8nch5hgv7xrfwlj6215kv70q") (r "1.75")))

(define-public crate-audio-viz-0.2.1 (c (n "audio-viz") (v "0.2.1") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "1vq2478prqq1xkkqr52vadsi5d25nar5lg5q9s2laxll1xdrbjym") (r "1.75")))

(define-public crate-audio-viz-0.2.2 (c (n "audio-viz") (v "0.2.2") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "0cgrjj14dv2hah1l76plipcllvkhjcyfqgjlx9yqp9shy5pcaxlf") (r "1.75")))

(define-public crate-audio-viz-0.3.0 (c (n "audio-viz") (v "0.3.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "0f4qin3gv4yid29cjl8n5rqjmyww4rf5waxrshkffwk7nv9m9lyy") (r "1.75")))

(define-public crate-audio-viz-0.4.0 (c (n "audio-viz") (v "0.4.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "1wznqbyy0a1g55m4i7cki7v55mdggyx3d3fa22wcf932yqcimw7m") (r "1.75")))

(define-public crate-audio-viz-0.5.0 (c (n "audio-viz") (v "0.5.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "0pb41s6n4nyw9d9k6qnfpd9k4s7nhh50wynaqvhgvsd34wi80zws") (r "1.75")))

(define-public crate-audio-viz-0.5.1 (c (n "audio-viz") (v "0.5.1") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "0v3a47xya580vf2r6yh0ck507lakxfvxbshfcsrxcr83v56dhwpi") (r "1.75")))

(define-public crate-audio-viz-0.6.0 (c (n "audio-viz") (v "0.6.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "0j7ygcijqdaqv6m6h88ll0sishdka8skiwqzs7xj3nn9lhv87542") (r "1.75")))

(define-public crate-audio-viz-0.6.1 (c (n "audio-viz") (v "0.6.1") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "1vsyfbljixvj1nsqqp1c8w3m302x27pg0kwcc8xsqw1ayhsrbb5p") (r "1.75")))

(define-public crate-audio-viz-0.6.2 (c (n "audio-viz") (v "0.6.2") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (k 0)))) (h "0bqq87rmsf1jgh1as56vhdl9jwaknggy10yj7ahkglgcbvyhx4bb") (r "1.75")))

(define-public crate-audio-viz-0.7.0 (c (n "audio-viz") (v "0.7.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)))) (h "1rd1fk3vav3zzdb4iwzcsrfk1nf068jzfzwh1471830z2rrdhjm8") (r "1.75")))

(define-public crate-audio-viz-0.8.0 (c (n "audio-viz") (v "0.8.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)))) (h "01i7fps9phymj23fm102j720yggbv3va8y1rhr3cjmsc1h214xfw") (r "1.75")))

(define-public crate-audio-viz-0.8.1 (c (n "audio-viz") (v "0.8.1") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)))) (h "033vdgzk27bmw4mvkb8q2572jzdjfs9h0vnk7jmms9lz2s4md7y4") (r "1.75")))

(define-public crate-audio-viz-0.8.2 (c (n "audio-viz") (v "0.8.2") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)))) (h "1zvn108q7q9knhl8xz35mpd5alsxw9nlycslfs9pilzf8pqrnpll") (y #t) (r "1.75")))

(define-public crate-audio-viz-0.8.3 (c (n "audio-viz") (v "0.8.3") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)))) (h "1bqxr2jgxbxhvfk4v146lc9m81lbva232h28agc2wic96hr0waal") (r "1.75")))

(define-public crate-audio-viz-0.8.4 (c (n "audio-viz") (v "0.8.4") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)))) (h "1rs74g9lzqw0dkwk681s6gyc7f78klyx7ha5n2h6khv59ghws2ai") (r "1.75")))

(define-public crate-audio-viz-0.8.5 (c (n "audio-viz") (v "0.8.5") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)))) (h "1n6wlalbjpvs1kgr2ybn9r9n6lnzbwmvcg39rm4w91yfadk0wvix") (r "1.75")))

