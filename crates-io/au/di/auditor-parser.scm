(define-module (crates-io au di auditor-parser) #:use-module (crates-io))

(define-public crate-auditor-parser-0.1.0 (c (n "auditor-parser") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "08x16mvld2ngfn9fi3s1vwyw4i8b9bfblz2v4d8c3677lysklzvz")))

