(define-module (crates-io au di audios) #:use-module (crates-io))

(define-public crate-audios-0.1.0 (c (n "audios") (v "0.1.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "19xf3zp59123l4nqr4casg9zd8ry971mfj9r4hxn3rpiizv3fwhz")))

