(define-module (crates-io au di audiowiz) #:use-module (crates-io))

(define-public crate-audiowiz-0.1.0 (c (n "audiowiz") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1sm9x9lf3394qhz7kwbxr44f4dcfqlqsy3dsgfdc2wczzn2z1c56")))

(define-public crate-audiowiz-0.1.1 (c (n "audiowiz") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1gp0hdkppw4w1c5ij6i3lni7wc1q8qrr38h6sz5w4vkpp7xqp3hv")))

(define-public crate-audiowiz-0.1.2 (c (n "audiowiz") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1y3bwcy49bmdj6yji5n5ld95m4k7ypb5hmmc8h3s7lfwkdjbmfkj")))

(define-public crate-audiowiz-0.1.3 (c (n "audiowiz") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "189w655zf6d08i1g02sdqs4mba0qifhd4y9s5d8hfnc3164fd3rh")))

(define-public crate-audiowiz-0.1.4 (c (n "audiowiz") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "188hh435phlk7hz8xaj1fmkb3qxf804qr7b0p2221jrarkxxcmc5")))

(define-public crate-audiowiz-0.1.5 (c (n "audiowiz") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "id3") (r "^0.6.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1v1jhbllx7zzq87zl5n6sfj7v28m251zb68g3x8nnwhddbk9q84r")))

