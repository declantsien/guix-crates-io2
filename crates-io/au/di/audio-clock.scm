(define-module (crates-io au di audio-clock) #:use-module (crates-io))

(define-public crate-audio-clock-0.1.0 (c (n "audio-clock") (v "0.1.0") (h "0lab432f2rkicwzka6dyc2w2nl1f9szda6zmkpv2i84s20r4y55z")))

(define-public crate-audio-clock-0.2.0 (c (n "audio-clock") (v "0.2.0") (h "029b4kmv41l9y4zzkcfs2xxg0iv2p4yqn2cfx7i85xw3l4a9scxd")))

