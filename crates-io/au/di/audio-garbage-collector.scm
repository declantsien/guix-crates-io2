(define-module (crates-io au di audio-garbage-collector) #:use-module (crates-io))

(define-public crate-audio-garbage-collector-1.0.0-alpha.1 (c (n "audio-garbage-collector") (v "1.0.0-alpha.1") (d (list (d (n "basedrop") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "1ahbcwhijb7hljkx4zr8a13z89nypjsnnvhzai157lnrj15ghaxx")))

(define-public crate-audio-garbage-collector-1.0.0-alpha.2 (c (n "audio-garbage-collector") (v "1.0.0-alpha.2") (d (list (d (n "basedrop") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "0y5ypvh1swrxl5g3g25gsyimspm24mhh702vaxyllvaglzj1a5f2")))

(define-public crate-audio-garbage-collector-1.0.0-alpha.3 (c (n "audio-garbage-collector") (v "1.0.0-alpha.3") (d (list (d (n "basedrop") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "0v2h38phl0k2jqcvajnx6cgy2xxbmf0lfjngdvy1yp9chjwi0p7a")))

(define-public crate-audio-garbage-collector-1.0.0-alpha.4 (c (n "audio-garbage-collector") (v "1.0.0-alpha.4") (d (list (d (n "basedrop") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "0k80f3phqlal50yzgblv0myg03m3ifz19kr78hk7jf33cwj2m1na")))

(define-public crate-audio-garbage-collector-1.0.0-alpha.5 (c (n "audio-garbage-collector") (v "1.0.0-alpha.5") (d (list (d (n "basedrop") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "0l6b2iwjwbqf59k3zqmb0zhv2gxd8lrfk6dl5a8d8z6fwlfj92pf")))

(define-public crate-audio-garbage-collector-1.0.0-alpha.6 (c (n "audio-garbage-collector") (v "1.0.0-alpha.6") (d (list (d (n "basedrop") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "1jivf4hardmza2my8w3nwkxdnbri23y3dzjvrhzag76kqikyismh")))

(define-public crate-audio-garbage-collector-1.1.0 (c (n "audio-garbage-collector") (v "1.1.0") (d (list (d (n "basedrop") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "1laldazgiwcqxz9q5545znq9p0i4mvlcix46zkq6xqrqpljqq1w4")))

(define-public crate-audio-garbage-collector-1.1.1 (c (n "audio-garbage-collector") (v "1.1.1") (d (list (d (n "basedrop") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "0arxzs3x2lkbypcnlbpb5yqvjhrppc1w5ilpi4100l59hqglpl6i")))

(define-public crate-audio-garbage-collector-1.2.0 (c (n "audio-garbage-collector") (v "1.2.0") (d (list (d (n "basedrop") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "wasm_thread") (r "^0.2.0") (f (quote ("es_modules"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "09r0qksv7mr0nrqaf99cck4ym4r0imp350askr3vcwia16l9db14")))

