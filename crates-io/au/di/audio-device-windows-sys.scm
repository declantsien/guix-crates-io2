(define-module (crates-io au di audio-device-windows-sys) #:use-module (crates-io))

(define-public crate-audio-device-windows-sys-0.1.0-alpha.1 (c (n "audio-device-windows-sys") (v "0.1.0-alpha.1") (d (list (d (n "windows") (r "^0.8.0") (k 0)))) (h "145ly82hcp2gn1ig8bfrjk277851im5m4lqcm06n8f6wr620vbix") (f (quote (("xaudio2") ("wasapi"))))))

