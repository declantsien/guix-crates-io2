(define-module (crates-io au di audible-dl) #:use-module (crates-io))

(define-public crate-audible-dl-0.1.0 (c (n "audible-dl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt" "macros" "fs"))) (d #t) (k 0)))) (h "0xkwnarnxsw7xbqlvxbz3kfxza0gmh2nhjbwk08m3c6yyg4i7674")))

