(define-module (crates-io au di audiobook_tts) #:use-module (crates-io))

(define-public crate-audiobook_tts-0.0.1 (c (n "audiobook_tts") (v "0.0.1") (h "19hnib3kivvn4ppfk4izyds6jiycvg2h1qcjxwb56jlwi73v00z6") (y #t)))

(define-public crate-audiobook_tts-1.0.0 (c (n "audiobook_tts") (v "1.0.0") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "15ajk13xd5qdw5wx9qb5r8yx2qcjis5ymg933g620abkpgkqg0z3") (y #t)))

(define-public crate-audiobook_tts-1.0.1 (c (n "audiobook_tts") (v "1.0.1") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "1qhjz509qb341i0s23hris2nbqcj582h7wpgvffmxlc8ddnvg9ad") (y #t)))

(define-public crate-audiobook_tts-1.0.2 (c (n "audiobook_tts") (v "1.0.2") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "16w00whnp1nai3sfdr1v5hs7qfc1bd3a07dl16b1iwpiqkid9g1p") (y #t)))

(define-public crate-audiobook_tts-1.0.3 (c (n "audiobook_tts") (v "1.0.3") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "1w3zzyadlhwbh4rgickjmiq00cg51l1bnxv8s10ms3zzpi6vcx5p") (y #t)))

(define-public crate-audiobook_tts-1.1.0 (c (n "audiobook_tts") (v "1.1.0") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0nifsj0xkhb93nnkpsax9h3539523w63ij2zipna76r7b78g1v7a") (y #t)))

(define-public crate-audiobook_tts-1.1.1 (c (n "audiobook_tts") (v "1.1.1") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "1nfcp4l7sklr3a192j61hczyry85gn282jhrfzlkn6hw3vgq3din") (y #t)))

(define-public crate-audiobook_tts-1.1.2 (c (n "audiobook_tts") (v "1.1.2") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "17izf70p58lx9agqmpnin7369896xmdsy6i6w949ai8gf9rrpyyd") (y #t)))

(define-public crate-audiobook_tts-1.1.3 (c (n "audiobook_tts") (v "1.1.3") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "093ywy57jf4iw00nc9k4dp0hml8smb88m790s7iwi1bypm6wjr0d") (y #t)))

(define-public crate-audiobook_tts-1.1.4 (c (n "audiobook_tts") (v "1.1.4") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0si9q6hmgdh14mv7y89vfxfm0ynmb3pq5fa0kqhrwl7khdadx1ky") (y #t)))

(define-public crate-audiobook_tts-1.1.5 (c (n "audiobook_tts") (v "1.1.5") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "1365in77d51i093kdxj2yr9ag3k8dziggjra5a27nf6rg8bmkp0k") (y #t)))

(define-public crate-audiobook_tts-1.1.6 (c (n "audiobook_tts") (v "1.1.6") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "15g8qjqm3a3266qvm2l35wgky1ff2ghv2vj17x4rgfc0k7yzwlzy") (y #t)))

(define-public crate-audiobook_tts-1.2.0 (c (n "audiobook_tts") (v "1.2.0") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0bmj0in4srkxhab4c88zygd4kfjxl2xpi6dpvdmgv4g14iixvbpn") (y #t)))

(define-public crate-audiobook_tts-1.2.1 (c (n "audiobook_tts") (v "1.2.1") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0x3000j2nvvq8mnggp0y6brxq94paa5djsv1nvyqrij6sv1k9adc") (y #t)))

