(define-module (crates-io au di audiokit) #:use-module (crates-io))

(define-public crate-audiokit-0.1.1 (c (n "audiokit") (v "0.1.1") (h "08cb70kdkcfdcvan3cyp30i2dj674mn2fprjfmgyfvcxmjkkzisk")))

(define-public crate-audiokit-0.1.2 (c (n "audiokit") (v "0.1.2") (h "08gqpf6mymwdxx9f3wjii8jn1y1r70f2zk8filjc5d3rkfhrcx6s")))

