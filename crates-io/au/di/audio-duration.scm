(define-module (crates-io au di audio-duration) #:use-module (crates-io))

(define-public crate-audio-duration-0.1.0 (c (n "audio-duration") (v "0.1.0") (h "129bbz9qp5bcngr9m9niv8yqahim94q84i23xdqiadd3igsh8vn2")))

(define-public crate-audio-duration-0.2.0 (c (n "audio-duration") (v "0.2.0") (h "1xnn5ayyz4nwl8fn643ybf7cm9pbj8jvbb2m4yvb3w3hnh6byckr")))

(define-public crate-audio-duration-0.2.1 (c (n "audio-duration") (v "0.2.1") (h "1fibxfvp20ics0zg98i8i517in2cwxxllgmx8pcgbccx326ixbv4")))

(define-public crate-audio-duration-0.2.2 (c (n "audio-duration") (v "0.2.2") (h "09jh1scpllz52x98agamnaayqc0jvxsskps9mlhfizcp32kqh96r")))

