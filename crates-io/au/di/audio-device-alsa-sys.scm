(define-module (crates-io au di audio-device-alsa-sys) #:use-module (crates-io))

(define-public crate-audio-device-alsa-sys-0.1.0-alpha.1 (c (n "audio-device-alsa-sys") (v "0.1.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0yma255kbrsql77ndwjb137v9dsak3r2ij3r01bahsjdghw09c7h")))

