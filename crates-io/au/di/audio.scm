(define-module (crates-io au di audio) #:use-module (crates-io))

(define-public crate-audio-0.0.2 (c (n "audio") (v "0.0.2") (h "0q7mgkjzr488a588wrl9w4ilqqra0pk635hfb6cgs87r214gnmx8") (y #t)))

(define-public crate-audio-0.0.9 (c (n "audio") (v "0.0.9") (h "1qz6r24zfxv8hmr8h9q8b0jfsyr1y6v8bqn44v1pg0hizkcwwrci") (y #t)))

(define-public crate-audio-0.0.10 (c (n "audio") (v "0.0.10") (h "1yndyngy2wl2sg1rwnlx18xfpnab2bi08xl78db9irm3xn8d1hw2") (y #t)))

(define-public crate-audio-0.0.11 (c (n "audio") (v "0.0.11") (h "1cfb5x6jq97kc9v94lq6vkqybgm65scg9gmq86z3h5i5w7jvc0ci") (y #t)))

(define-public crate-audio-0.0.12 (c (n "audio") (v "0.0.12") (h "0csp9cnada1ckqfl9m43xnb7sxbrzwcjsm361vzswjqwj9fq1g4l") (y #t)))

(define-public crate-audio-0.0.13 (c (n "audio") (v "0.0.13") (h "1zabk1351r3mr6bshx9ha6klc3iy34cfrh1idl72ln9kn8bgbarl") (y #t)))

(define-public crate-audio-0.0.14 (c (n "audio") (v "0.0.14") (h "0ddcivbfsv38nxzw22iah4zls84zalrxffkbjggndgvda1s6yvsh") (y #t)))

(define-public crate-audio-0.1.0 (c (n "audio") (v "0.1.0") (h "0dkvlpnrchqzpvfvl0jb1pknrdpv7n5f0c1pmhnx5jb9fbs9bmkr") (y #t)))

(define-public crate-audio-0.2.0-alpha.2 (c (n "audio") (v "0.2.0-alpha.2") (d (list (d (n "audio-core") (r "^0.2.0-alpha.2") (d #t) (k 0)) (d (n "bittle") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "078afbvp1dlw92w297m8rp71jk7zwj0ciglig22inxgl1jvdsc96") (y #t)))

(define-public crate-audio-0.2.0-alpha.3 (c (n "audio") (v "0.2.0-alpha.3") (d (list (d (n "audio-core") (r "^0.2.0-alpha.3") (d #t) (k 0)) (d (n "bittle") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0aff20z9ikg0b2nnm9lzx1kllx0k835nizkm9ls0bgdvgr8vl3jm") (y #t)))

(define-public crate-audio-0.2.0-alpha.4 (c (n "audio") (v "0.2.0-alpha.4") (d (list (d (n "audio-core") (r "^0.2.0-alpha.4") (d #t) (k 0)) (d (n "bittle") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vyglyqbsk7bjj881x6kgr94zpqs47r7s1ls9l6r749ji5fvvz47") (f (quote (("std" "audio-core/std") ("default" "std"))))))

(define-public crate-audio-0.2.0 (c (n "audio") (v "0.2.0") (d (list (d (n "audio-core") (r "^0.2.0") (d #t) (k 0)) (d (n "bittle") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kx167akqpg274g18nzsvllvvlynvvbpdx6bxxz9ddpnpaygvn62") (f (quote (("std" "audio-core/std") ("default" "std")))) (r "1.65")))

