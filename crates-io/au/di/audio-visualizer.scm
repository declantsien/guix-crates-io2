(define-module (crates-io au di audio-visualizer) #:use-module (crates-io))

(define-public crate-audio-visualizer-0.1.0 (c (n "audio-visualizer") (v "0.1.0") (d (list (d (n "minimp3") (r "^0.5.1") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)))) (h "0rnr63p0vnlrhzwl3ax2az2i8d4mxyz2lkvbw5xyzs1xabz8xf2g")))

(define-public crate-audio-visualizer-0.1.1 (c (n "audio-visualizer") (v "0.1.1") (d (list (d (n "minimp3") (r "^0.5.1") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)))) (h "1ijnp42djvghwwkhqilnvw7k0hgd3w0dpy8xavkfs9ad3wlnh7di")))

(define-public crate-audio-visualizer-0.1.2 (c (n "audio-visualizer") (v "0.1.2") (d (list (d (n "minimp3") (r "^0.5.1") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)))) (h "1h1pqyllvzxqxdc6zwckwicbgpah69wynfjcmyi8v0xf8lq06lrb")))

(define-public crate-audio-visualizer-0.1.3 (c (n "audio-visualizer") (v "0.1.3") (d (list (d (n "minimp3") (r "^0.5.1") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)))) (h "05zpsw142crwvcx3qn7fgdzzj0pay1jpa3mq5vby3mp99wcp7m5a")))

(define-public crate-audio-visualizer-0.1.4 (c (n "audio-visualizer") (v "0.1.4") (d (list (d (n "minimp3") (r "^0.5.1") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)))) (h "147cgyj95619472piapxvkws30f90x8crqpycjb0arbbihm359vz")))

(define-public crate-audio-visualizer-0.2.0 (c (n "audio-visualizer") (v "0.2.0") (d (list (d (n "minimp3") (r "^0.5.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)))) (h "0vdwaznhxl26lrw5x8mbmapzdd2zv5kcbv2biviw0irv4py2j9d8")))

(define-public crate-audio-visualizer-0.2.1 (c (n "audio-visualizer") (v "0.2.1") (d (list (d (n "minimp3") (r "^0.5.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)))) (h "1dr8vc9x51px8amfx1wqjsfikw3xg9ybi38a0b0qhd353xy2ii31")))

(define-public crate-audio-visualizer-0.2.2 (c (n "audio-visualizer") (v "0.2.2") (d (list (d (n "minimp3") (r "^0.5.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)))) (h "1k5n5spmw7cf7nxb5fjxnism8mql70bf3lplwkq19gdw9v1zjwk8")))

(define-public crate-audio-visualizer-0.2.3 (c (n "audio-visualizer") (v "0.2.3") (d (list (d (n "minimp3") (r "^0.5.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "png") (r "^0.17.1") (d #t) (k 0)))) (h "19fkgfbvd550b0m7vx5w84nn9zyl2yy0gx86bgqi8l3d27j0qg3h")))

(define-public crate-audio-visualizer-0.2.4 (c (n "audio-visualizer") (v "0.2.4") (d (list (d (n "minimp3") (r "^0.5.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "png") (r "^0.17.1") (d #t) (k 0)))) (h "1rx6g7zzhcsihgaadhc90kyq0234y9nk8gq0hbm6grdmrbwfinic")))

(define-public crate-audio-visualizer-0.3.0 (c (n "audio-visualizer") (v "0.3.0") (d (list (d (n "biquad") (r "^0.4") (d #t) (k 2)) (d (n "cpal") (r "^0.13") (d #t) (k 0)) (d (n "lowpass-filter") (r "^0.2") (d #t) (k 2)) (d (n "minifb") (r "^0.19") (d #t) (k 0)) (d (n "minimp3") (r "^0.5") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (f (quote ("bitmap_backend" "line_series"))) (k 0)) (d (n "plotters-bitmap") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)) (d (n "ringbuffer") (r "^0.8") (d #t) (k 0)) (d (n "spectrum-analyzer") (r "^1.1") (d #t) (k 2)))) (h "1761vxiyq6r1c9yxsp358hzk4ja7w5wzpnxfzdw1hvhdp68vkvv9")))

(define-public crate-audio-visualizer-0.3.1 (c (n "audio-visualizer") (v "0.3.1") (d (list (d (n "biquad") (r "^0.4") (d #t) (k 2)) (d (n "cpal") (r "^0.13") (d #t) (k 0)) (d (n "lowpass-filter") (r "^0.3") (d #t) (k 2)) (d (n "minifb") (r "^0.19") (d #t) (k 0)) (d (n "minimp3") (r "^0.5") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (f (quote ("bitmap_backend" "line_series"))) (k 0)) (d (n "plotters-bitmap") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)) (d (n "ringbuffer") (r "^0.8") (d #t) (k 0)) (d (n "spectrum-analyzer") (r "^1.1") (d #t) (k 2)))) (h "1xcv89jdr4szc388abmhfdq3q2jjac8wiwjjj43ac5z6fhsqwz1h")))

(define-public crate-audio-visualizer-0.4.0 (c (n "audio-visualizer") (v "0.4.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 2)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "lowpass-filter") (r "^0.3.2") (d #t) (k 2)) (d (n "minifb") (r "^0.25.0") (d #t) (k 0)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)) (d (n "plotters") (r "^0.3.5") (f (quote ("bitmap_backend" "line_series"))) (d #t) (k 0)) (d (n "plotters-bitmap") (r "^0.3.3") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)) (d (n "ringbuffer") (r "^0.15.0") (d #t) (k 0)) (d (n "spectrum-analyzer") (r "^1.4.0") (d #t) (k 2)))) (h "0bi8g2add0p6hn6m4dyrdkikd5k96gzrp589ykaq58f28wfhk9f9")))

