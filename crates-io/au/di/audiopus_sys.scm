(define-module (crates-io au di audiopus_sys) #:use-module (crates-io))

(define-public crate-audiopus_sys-0.1.0 (c (n "audiopus_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.48.1") (d #t) (k 1)) (d (n "log") (r "^0.4.6") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1y9n5sw5jf9mcwaj86ldwdhgnih97p0xjy2dhhrqq2ayjyyx7snw") (f (quote (("static") ("dynamic") ("default"))))))

(define-public crate-audiopus_sys-0.1.1 (c (n "audiopus_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.48.1") (d #t) (k 1)) (d (n "log") (r "^0.4.6") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1jw64gggls5im0q2ixmpwcmhqjfj5j5mmmlv71di9z0avqla7729") (f (quote (("static") ("dynamic") ("default"))))))

(define-public crate-audiopus_sys-0.1.2 (c (n "audiopus_sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.48.1") (d #t) (k 1)) (d (n "log") (r "^0.4.6") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1pdrqs2yixvdcqyy4x4h45jgzxv34zlx2qw27kik4p656yxmrnpb") (f (quote (("static") ("dynamic") ("default"))))))

(define-public crate-audiopus_sys-0.1.3 (c (n "audiopus_sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.48.1") (d #t) (k 1)) (d (n "log") (r "^0.4.6") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0klmr446qgh66yhvbhd76rwqjrxgaccc88817q6j1lbrv61r7crr") (f (quote (("static") ("dynamic") ("default"))))))

(define-public crate-audiopus_sys-0.1.4 (c (n "audiopus_sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.48.1") (d #t) (k 1)) (d (n "log") (r "^0.4.6") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1bns9jpnpjdgvvsm18zsmwiv4bavqvpm4c5s6p506229kwlhdvnq") (f (quote (("static") ("dynamic") ("default"))))))

(define-public crate-audiopus_sys-0.1.5 (c (n "audiopus_sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.48.1") (d #t) (k 1)) (d (n "log") (r "^0.4.6") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1yipxvg7ws5rv8p08n1wvdjdjqg0nd1cpak9li8vsr4vcgwqjvgy") (f (quote (("static") ("dynamic") ("default"))))))

(define-public crate-audiopus_sys-0.1.6 (c (n "audiopus_sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.48") (o #t) (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lmyj32h96hmfirdxx7mqj3c475brm9xk0p70n3ljbp1gdvy25bl") (f (quote (("static") ("generate_binding" "bindgen") ("dynamic") ("default"))))))

(define-public crate-audiopus_sys-0.1.7 (c (n "audiopus_sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.48") (o #t) (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08qjzins6wmz7a07pih3ff5kfc3dq8yncsibn7xzcywssnfa5gya") (f (quote (("static") ("generate_binding" "bindgen") ("dynamic") ("default"))))))

(define-public crate-audiopus_sys-0.1.8 (c (n "audiopus_sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.48") (o #t) (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ygzb1mdx18scfz06d24cihcwhcsf4cszgrdk3maq3zp8vg92xwj") (f (quote (("static") ("generate_binding" "bindgen") ("dynamic") ("default"))))))

(define-public crate-audiopus_sys-0.2.0 (c (n "audiopus_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ip1sb1fjhz2lalrhc47a96cwd8pk9dng7s6n81pkjnfpq5dhqh5") (f (quote (("static") ("generate_binding" "bindgen") ("dynamic") ("default"))))))

(define-public crate-audiopus_sys-0.2.1 (c (n "audiopus_sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.58") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0v6yva3k5ydn4896nch24ccp37kmxqmpd04v31xs824s5xf4lsdf") (f (quote (("static") ("generate_binding" "bindgen") ("dynamic") ("default"))))))

(define-public crate-audiopus_sys-0.2.2 (c (n "audiopus_sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.58") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0lc6kzdw65kbgqaghig99f8642k2ikl5imk56q1lw1m28qallcb2") (f (quote (("static") ("generate_binding" "bindgen") ("dynamic") ("default"))))))

