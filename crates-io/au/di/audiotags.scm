(define-module (crates-io au di audiotags) #:use-module (crates-io))

(define-public crate-audiotags-0.0.1 (c (n "audiotags") (v "0.0.1") (d (list (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.5.1") (d #t) (k 0)) (d (n "strum") (r "^0.19.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "02qnkc9dljkab3jn3p20dm731zd5my7x6w344jx71fsdjyizmi77")))

(define-public crate-audiotags-0.0.2 (c (n "audiotags") (v "0.0.2") (d (list (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.5.1") (d #t) (k 0)) (d (n "strum") (r "^0.19.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zb4nvck3ik7ncsflmdir8bxj0zq2inbsd5cr77kivr4frjz5ira")))

(define-public crate-audiotags-0.1.0 (c (n "audiotags") (v "0.1.0") (d (list (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.5.1") (d #t) (k 0)) (d (n "strum") (r "^0.19.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nr16xw44d1b8bmfxy98a4yf7qmdzvwhxq54gilcj7klgx5q5848")))

(define-public crate-audiotags-0.1.1 (c (n "audiotags") (v "0.1.1") (d (list (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.5.1") (d #t) (k 0)) (d (n "strum") (r "^0.19.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yadhhaas3qi3rj4n0z2d7ay95jdwyq81iclwawcn4k4g46mcms8")))

(define-public crate-audiotags-0.1.2 (c (n "audiotags") (v "0.1.2") (d (list (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.5.1") (d #t) (k 0)) (d (n "strum") (r "^0.19.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p3dwfwhn7ixmrk8kwzk1p30b9j2lqxs2cg8fhmlq5vrk849dwkg")))

(define-public crate-audiotags-0.1.3 (c (n "audiotags") (v "0.1.3") (d (list (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.5.1") (d #t) (k 0)) (d (n "strum") (r "^0.19.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k1bfld63pys8jibwsrhnlyvg8f0qjdnb0hpa47scffhncxprw53")))

(define-public crate-audiotags-0.1.5 (c (n "audiotags") (v "0.1.5") (d (list (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.5.1") (d #t) (k 0)) (d (n "strum") (r "^0.19.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "000wypim5g7yvyaw5rv78nbg9gpzqalgvl1d8ijy5j8h3hw4y7ym")))

(define-public crate-audiotags-0.2.0 (c (n "audiotags") (v "0.2.0") (d (list (d (n "beef") (r "^0.4.4") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.6") (d #t) (k 0)) (d (n "strum") (r "^0.19.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "0in4jwhqnvh8myqxn1c79l08gxwva7449vr5k6dpvsq2qv3szij0")))

(define-public crate-audiotags-0.2.1 (c (n "audiotags") (v "0.2.1") (d (list (d (n "beef") (r "^0.4.4") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.6") (d #t) (k 0)) (d (n "strum") (r "^0.19.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0ivbkk0fxap3fddz8d7pqy8rl876abzrcax3x69a8z5mivc92jwn")))

(define-public crate-audiotags-0.2.2 (c (n "audiotags") (v "0.2.2") (d (list (d (n "beef") (r "^0.4.4") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "07s872aygc6a7j6w1ppsg9mvksf0nqlqa6aj6dy1fxgf2jxfl0rg")))

(define-public crate-audiotags-0.2.3 (c (n "audiotags") (v "0.2.3") (d (list (d (n "beef") (r "^0.4.4") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1hk8zn1yp4i20pcf43vfzb7xi6y8xk6y6pmzsxx5xvqsa52m373f")))

(define-public crate-audiotags-0.2.5 (c (n "audiotags") (v "0.2.5") (d (list (d (n "audiotags-dev-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "beef") (r "^0.4.4") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0hhsn7mxjlhcn77dpwadnaqxmn73bvdvzm5xq2iz3haazdiz2z16")))

(define-public crate-audiotags-0.2.7 (c (n "audiotags") (v "0.2.7") (d (list (d (n "audiotags-dev-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "beef") (r "^0.4.4") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1pj0fvllhj3rlq0ik9i9p6fz9flb96rp7xm2i5qp3mik6zrqfisf")))

(define-public crate-audiotags-0.2.71 (c (n "audiotags") (v "0.2.71") (d (list (d (n "audiotags-dev-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0vxk0s54dqcjwbnpswfdsj23g46x3p7zgv8g1pdp8z1f0dhcj1q2")))

(define-public crate-audiotags-0.2.718 (c (n "audiotags") (v "0.2.718") (d (list (d (n "audiotags-dev-macro") (r "^0.1") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1ivn42y0xa99r2gr0zm80bg2s9zqvwrr4hj4pa7d2r8b9jjw8klm")))

(define-public crate-audiotags-0.2.7182 (c (n "audiotags") (v "0.2.7182") (d (list (d (n "audiotags-dev-macro") (r "^0.1.4") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "metaflac") (r "^0.2") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1n76kkx7hkqxf6z3b9028yb2b6c75lpdvi8kbyxic97l308i2zdq") (f (quote (("from") ("defualt" "from"))))))

(define-public crate-audiotags-0.4.0 (c (n "audiotags") (v "0.4.0") (d (list (d (n "audiotags-dev-macro") (r "^0.1.4") (d #t) (k 0)) (d (n "id3") (r "^1.1.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.11.0") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1.0") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ncnxyx0gn42c64qch7pp74ls6h300f64m4q7pp365n3h4x64szk") (f (quote (("from") ("default" "from"))))))

(define-public crate-audiotags-0.4.1 (c (n "audiotags") (v "0.4.1") (d (list (d (n "audiotags-dev-macro") (r "^0.1.4") (d #t) (k 0)) (d (n "id3") (r "^1.1.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.11.0") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1.0") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "16y70bhir9ivf9xrj38xqr1i99k2zqxvnwmaz09l3yn4mkqvsiqz") (f (quote (("from") ("default" "from"))))))

(define-public crate-audiotags-0.5.0 (c (n "audiotags") (v "0.5.0") (d (list (d (n "audiotags-macro") (r "^0.2") (d #t) (k 0)) (d (n "id3") (r "^1.10.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.11.0") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1.1") (d #t) (k 1)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0fgda2q8cml7irc48d83vjirc79hasdq9hzjf6f5kkv40779grs4") (f (quote (("from") ("default" "from"))))))

