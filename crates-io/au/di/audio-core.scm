(define-module (crates-io au di audio-core) #:use-module (crates-io))

(define-public crate-audio-core-0.2.0-alpha.2 (c (n "audio-core") (v "0.2.0-alpha.2") (d (list (d (n "audio") (r "^0.2.0-alpha.1") (d #t) (k 2)))) (h "0nz7s1q4y7jmwynv21xhchd1aa9sk59qmslkhczg3xv2fjgsnhga") (y #t)))

(define-public crate-audio-core-0.2.0-alpha.3 (c (n "audio-core") (v "0.2.0-alpha.3") (d (list (d (n "audio") (r "^0.2.0-alpha.3") (d #t) (k 2)))) (h "1vn83l79ahqrd055ls5l8bxpimcx0yciws313r5n36m5szkjyf71") (y #t)))

(define-public crate-audio-core-0.2.0-alpha.4 (c (n "audio-core") (v "0.2.0-alpha.4") (d (list (d (n "audio") (r "^0.2.0-alpha.4") (d #t) (k 2)))) (h "0p6127z85873blz4m656xianknlis2xyqiswaghkm3gqifvj95r3") (f (quote (("std") ("default" "std"))))))

(define-public crate-audio-core-0.2.0 (c (n "audio-core") (v "0.2.0") (d (list (d (n "audio") (r "^0.2.0") (d #t) (k 2)))) (h "1q2qx4v8ymvnyilwmdm6ia0528h0gzcp7vv0yibh3kj9ww868i6v") (f (quote (("std") ("default" "std")))) (r "1.65")))

