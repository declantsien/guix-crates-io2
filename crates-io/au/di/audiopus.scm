(define-module (crates-io au di audiopus) #:use-module (crates-io))

(define-public crate-audiopus-0.1.0 (c (n "audiopus") (v "0.1.0") (d (list (d (n "audiopus_sys") (r "^0.1") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "082z8r184cx0j9djkk6kgb35bhva5imsanl7mnsnblqh279fa0fs") (f (quote (("repacketizer" "packet") ("packet") ("multistream") ("encoder" "packet") ("default_features" "coder") ("decoder" "packet") ("coder" "encoder" "decoder"))))))

(define-public crate-audiopus-0.1.1 (c (n "audiopus") (v "0.1.1") (d (list (d (n "audiopus_sys") (r "^0.1") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "1arxhc6si5r5qy7kwfpxawrclk6y38189qvybxnfv9bbmlnfys50") (f (quote (("repacketizer" "packet") ("packet") ("multistream") ("encoder" "packet") ("default_features" "coder") ("decoder" "packet") ("coder" "encoder" "decoder"))))))

(define-public crate-audiopus-0.1.2 (c (n "audiopus") (v "0.1.2") (d (list (d (n "audiopus_sys") (r "^0.1") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "1libv1ixl8mgnzbds817hgp23cgkzabk5ifz4bbx4c56r4m325vw") (f (quote (("repacketizer" "packet") ("packet") ("multistream") ("encoder" "packet") ("default_features" "coder") ("decoder" "packet") ("coder" "encoder" "decoder"))))))

(define-public crate-audiopus-0.1.3 (c (n "audiopus") (v "0.1.3") (d (list (d (n "audiopus_sys") (r "^0.1") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "0008aj7y87bxlffhsxjk444j0frz8mis4m5lsabs451mh1ykncf5") (f (quote (("repacketizer" "packet") ("packet") ("multistream") ("encoder" "packet") ("default_features" "coder") ("decoder" "packet") ("coder" "encoder" "decoder"))))))

(define-public crate-audiopus-0.2.0 (c (n "audiopus") (v "0.2.0") (d (list (d (n "audiopus_sys") (r "^0.1") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "0n64pqiikywacs3b5zk6kp5y9j5h3j2hk9giz7v5q4z9cyam2hrp") (f (quote (("repacketizer" "packet") ("packet") ("multistream") ("encoder" "packet") ("default_features" "coder") ("decoder" "packet") ("coder" "encoder" "decoder"))))))

(define-public crate-audiopus-0.3.0-rc.0 (c (n "audiopus") (v "0.3.0-rc.0") (d (list (d (n "audiopus_sys") (r "^0.2.2") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "0m9r8zk3n7r4x1p2fsmy6gn2axrd2bdyai7mb4yxxinpaq7fnmdb") (f (quote (("repacketizer" "packet") ("packet") ("multistream") ("encoder" "packet") ("default_features" "coder") ("decoder" "packet") ("coder" "encoder" "decoder"))))))

