(define-module (crates-io au di audiovis) #:use-module (crates-io))

(define-public crate-audiovis-0.1.0 (c (n "audiovis") (v "0.1.0") (d (list (d (n "cpal") (r "^0.15.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.128.0") (o #t) (d #t) (k 0)) (d (n "ringbuffer") (r "^0.12.0") (d #t) (k 0)) (d (n "spectrum-analyzer") (r "^1.2.6") (d #t) (k 0)))) (h "1ga8mr6grpsksfqhv1615l4h8a5mwp09hr4q3jznfc3a92szp3ji") (s 2) (e (quote (("piston" "dep:piston_window"))))))

(define-public crate-audiovis-0.1.1 (c (n "audiovis") (v "0.1.1") (d (list (d (n "cpal") (r "^0.15.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.131.0") (o #t) (d #t) (k 0)) (d (n "ringbuffer") (r "^0.15.0") (d #t) (k 0)) (d (n "spectrum-analyzer") (r "^1.2.6") (d #t) (k 0)))) (h "0hc9vp3xm9gk8895qh2a0snx2wcxzvymxg89g8bwbvy208a23fbd") (s 2) (e (quote (("piston" "dep:piston_window"))))))

