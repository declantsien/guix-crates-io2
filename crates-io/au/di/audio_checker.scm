(define-module (crates-io au di audio_checker) #:use-module (crates-io))

(define-public crate-audio_checker-0.1.0 (c (n "audio_checker") (v "0.1.0") (d (list (d (n "symphonia") (r "^0.5") (f (quote ("mp3" "aac" "alac" "flac" "isomp4" "mkv" "ogg" "pcm" "vorbis" "wav"))) (d #t) (k 0)))) (h "0cc8dn0j2qn4nd2xlws8d4y38fwr41vjfn4jbgpgcbj94l14y8r0")))

