(define-module (crates-io au di audio-gate) #:use-module (crates-io))

(define-public crate-audio-gate-0.1.0 (c (n "audio-gate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "ringbuf") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "181b7v2knps036f38d654dwg05xn15cplgbrcfamhnlsy8gwrc7a") (y #t)))

(define-public crate-audio-gate-0.1.1 (c (n "audio-gate") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "ringbuf") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "08j3czzx1gmf39bizd3hj91hp114cdk9lgxxavmb6c7vxnw8c6vs")))

