(define-module (crates-io au di auditfile) #:use-module (crates-io))

(define-public crate-auditfile-0.1.0 (c (n "auditfile") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1nf1svg457vagfvj6f3fg53m82lj0i7pgdg1f5a06x4lycy6pqma")))

(define-public crate-auditfile-0.1.2 (c (n "auditfile") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "132fgix1wxi6bk8qzhqfzdm6ma97fapkif4a0ndvq9r1d4p2as2f")))

