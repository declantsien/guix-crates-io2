(define-module (crates-io au di audiokeyboard) #:use-module (crates-io))

(define-public crate-audiokeyboard-1.0.0 (c (n "audiokeyboard") (v "1.0.0") (d (list (d (n "fltk") (r "^1.3.15") (f (quote ("fltk-bundled" "use-ninja"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "0vkl5hjqfhiai8v3cgnxn05x2jppx08cjskva1gxqjlmkjgglsxf") (r "1.61.0")))

(define-public crate-audiokeyboard-1.1.0 (c (n "audiokeyboard") (v "1.1.0") (d (list (d (n "fltk") (r "^1.3.15") (f (quote ("fltk-bundled" "use-ninja"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "1fkdw7pg91z40wxkgk706182mv3lf3mvnv3zhg8k3sijic4p8g26") (r "1.61.0")))

(define-public crate-audiokeyboard-1.1.1 (c (n "audiokeyboard") (v "1.1.1") (d (list (d (n "fltk") (r "^1.3.15") (f (quote ("fltk-bundled" "use-ninja"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "07h8s125sfrl0ridy16c28pi4vj8wflrbs4zby725025wnv9n409") (r "1.61.0")))

(define-public crate-audiokeyboard-1.1.2 (c (n "audiokeyboard") (v "1.1.2") (d (list (d (n "fltk") (r "^1.3.15") (f (quote ("fltk-bundled" "use-ninja"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "032ibaw4dm2hsdrzf0y27xslxv03xvncln5ybawxisdggcf0fdsg") (r "1.61.0")))

(define-public crate-audiokeyboard-1.2.0 (c (n "audiokeyboard") (v "1.2.0") (d (list (d (n "fltk") (r "^1.3.15") (f (quote ("fltk-bundled" "use-ninja"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "1347cpmz2s262ri0gsy7lniv85zl2wm32adax3axmxfwsw7q7sr7") (r "1.61.0")))

(define-public crate-audiokeyboard-2.0.0 (c (n "audiokeyboard") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "11g53564jk26hk9dzdzhc19y8lpqmjmanr2q0zjgp6pz97nk1qqz") (r "1.61.0")))

