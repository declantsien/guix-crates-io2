(define-module (crates-io au di audiomoth-hid-rs) #:use-module (crates-io))

(define-public crate-audiomoth-hid-rs-0.1.0 (c (n "audiomoth-hid-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0k9rm6p9073bxygwyrpnmnd743s53nddzczzmjzdk0p23c4h12y6")))

(define-public crate-audiomoth-hid-rs-0.1.1 (c (n "audiomoth-hid-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0idhbmx5kd0d0qyw36xkwr4mcnxp4m1qy2i1xpw3k8mnzx3m0raw")))

(define-public crate-audiomoth-hid-rs-0.1.2 (c (n "audiomoth-hid-rs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1xrix1zaxnhx213lah1q1rm93jckxnwb1vc84bq86imws1lxk8ns")))

