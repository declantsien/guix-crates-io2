(define-module (crates-io au st austin-db) #:use-module (crates-io))

(define-public crate-austin-db-0.1.0 (c (n "austin-db") (v "0.1.0") (h "1bccxx6w4nw3c9g3yjbigv5hsc79vskkdxgzg6kc0sm677q0mi8d") (y #t)))

(define-public crate-austin-db-0.1.1 (c (n "austin-db") (v "0.1.1") (h "01nva0wjvkjlmjk1mv8lqm0jv0z0yn57fwcgblsnv8dnbap5rafd")))

