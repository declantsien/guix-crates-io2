(define-module (crates-io au st austenite) #:use-module (crates-io))

(define-public crate-austenite-0.0.1 (c (n "austenite") (v "0.0.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "059qvrdy8z9d8b5a39fcyfspsrqfpq4icny9m0ih8yiaw88ka9jk")))

