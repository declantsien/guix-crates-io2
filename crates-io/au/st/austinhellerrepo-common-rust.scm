(define-module (crates-io au st austinhellerrepo-common-rust) #:use-module (crates-io))

(define-public crate-austinhellerrepo-common-rust-0.1.0 (c (n "austinhellerrepo-common-rust") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "01pjbl0kapc29091s1bh715v0aih45gxl453xig0a0ind8d5ra4c") (y #t)))

(define-public crate-austinhellerrepo-common-rust-0.1.1 (c (n "austinhellerrepo-common-rust") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0p8dli07fqamncv2b4ca23rhpcg13aq80js0hjnc2k5rdy9mkik0") (y #t)))

(define-public crate-austinhellerrepo-common-rust-0.2.0 (c (n "austinhellerrepo-common-rust") (v "0.2.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "11q6f72bmq72319xl8x4553ia0ndcj82zz5kph2i58zpmg591mqq") (y #t)))

(define-public crate-austinhellerrepo-common-rust-0.2.1 (c (n "austinhellerrepo-common-rust") (v "0.2.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1n63rd6yqf1sl18xc813gjs9vb5v3iww5fj4finfkkb1c5y37iyw") (y #t)))

(define-public crate-austinhellerrepo-common-rust-0.2.2 (c (n "austinhellerrepo-common-rust") (v "0.2.2") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lkikrscjhnrayhzl8rdyzqcp7nir7x9yyjn5rvv0nr8j4vq3216") (y #t)))

