(define-module (crates-io ow fs owfs) #:use-module (crates-io))

(define-public crate-owfs-0.0.1 (c (n "owfs") (v "0.0.1") (h "1wxlzqpv1l6d3xiniv7i1bjslnx941188n4c6vhrrx9xjhahf2y3")))

(define-public crate-owfs-0.0.2 (c (n "owfs") (v "0.0.2") (h "0p2yrfxiqdci08m5ksfpsaim7sy5mnais8rmgzv6s9mj9clhfhng")))

(define-public crate-owfs-0.0.3 (c (n "owfs") (v "0.0.3") (h "05rlmzfx6an8k20i6sljg83as6q029i4cszpf03pq1a5c616f2ac")))

(define-public crate-owfs-0.0.4 (c (n "owfs") (v "0.0.4") (h "02m1ab9gwqc0zkkf8gmwh5q38q40rf821jwkgyn1h0hahm52mhcz")))

(define-public crate-owfs-0.0.5 (c (n "owfs") (v "0.0.5") (h "149sc0225k08fn2y1674wy6x8mbm3hy2gf9dk4xim3qzcps3cm8x")))

(define-public crate-owfs-0.0.6 (c (n "owfs") (v "0.0.6") (h "18p20w6z2bp584n8pc64vvj8pk70yw0nqi69aw966h4ayf7pvv6g")))

(define-public crate-owfs-0.0.7 (c (n "owfs") (v "0.0.7") (h "0nkazs6wiq5w0b0hjsg55r8c902jrg3nv81wkn6n45rnq9qhkd6g")))

(define-public crate-owfs-0.0.8 (c (n "owfs") (v "0.0.8") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "0q9vhy4750aqcj2155897r728h8zxhkdphzswiwnkz5jdhfqkil4")))

(define-public crate-owfs-0.0.9 (c (n "owfs") (v "0.0.9") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "14dpw4kf954jq749fcm7bqm8ggy9pg753xdm833b9q9lh80lnxvr")))

(define-public crate-owfs-0.1.0 (c (n "owfs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (o #t) (d #t) (k 1)) (d (n "ureq") (r "^2.9.7") (o #t) (d #t) (k 1)))) (h "03qmy38xai8b3z8q0vcxpibwrm272wxiqxrin7223ip1mcv0kwal") (f (quote (("vendored" "num_cpus" "ureq"))))))

