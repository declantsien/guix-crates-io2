(define-module (crates-io ow m- owm-rs) #:use-module (crates-io))

(define-public crate-owm-rs-1.0.6 (c (n "owm-rs") (v "1.0.6") (d (list (d (n "dotenvy") (r "^0.15.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pxk2rqmf7fdwg5ymhlzxs1pr7ppzjwiak9qhn1npwgzxg9j8aqk") (f (quote (("utils"))))))

(define-public crate-owm-rs-1.0.7 (c (n "owm-rs") (v "1.0.7") (d (list (d (n "dotenvy") (r "^0.15.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g4jq5xx9hfc7y4sfnvfaavzxpmpg7z4pymxrckg1m0x967cv3cy") (f (quote (("utils"))))))

(define-public crate-owm-rs-1.0.8 (c (n "owm-rs") (v "1.0.8") (d (list (d (n "dotenvy") (r "^0.15.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1mpfsly385x8f2pj6qbbc316s4n7rb06h2gqxri31qykn5h7h6jf") (f (quote (("utils"))))))

(define-public crate-owm-rs-1.0.9 (c (n "owm-rs") (v "1.0.9") (d (list (d (n "dotenvy") (r "^0.15.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qi53kknsxhc303wq0i5jnwhilgz894cm96m5h3kakvwn3bqgwwb") (f (quote (("utils"))))))

(define-public crate-owm-rs-1.0.11 (c (n "owm-rs") (v "1.0.11") (d (list (d (n "dotenvy") (r "^0.15.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z3ps6wmplyvh6d6ac67m5wwk6als64pl47w25wdrjhgbf6n1hfy") (f (quote (("utils"))))))

(define-public crate-owm-rs-1.0.12 (c (n "owm-rs") (v "1.0.12") (d (list (d (n "dotenvy") (r "^0.15.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jshpkwg84h903y5kxbyirdzx8wfdid3g9sgx3rm0sakh81zchg1") (f (quote (("utils"))))))

(define-public crate-owm-rs-1.0.13 (c (n "owm-rs") (v "1.0.13") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p7bsfqi3hwzcfkcn96292rjdi19hbxq2m7djbsci5jf2whcf31s") (f (quote (("utils"))))))

(define-public crate-owm-rs-1.0.14 (c (n "owm-rs") (v "1.0.14") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.17") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "065zxkzl6khmzyv66lbbq5skcyyhld4ffqnb8p8vkf9rbyp7sy5z") (f (quote (("utils"))))))

(define-public crate-owm-rs-1.0.16 (c (n "owm-rs") (v "1.0.16") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f6s46362g2c797d1g2afh20l70329nlfyck1i750b3b0xirga9m") (f (quote (("utils"))))))

