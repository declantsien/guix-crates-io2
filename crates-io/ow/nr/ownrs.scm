(define-module (crates-io ow nr ownrs) #:use-module (crates-io))

(define-public crate-ownrs-0.1.0 (c (n "ownrs") (v "0.1.0") (h "18a2l72145bg7i9dfxhhly9rgbl620ynblvkwl8wmikl66vf8n4h") (y #t)))

(define-public crate-ownrs-0.1.1 (c (n "ownrs") (v "0.1.1") (h "00n85z219lvdbafslcfi1f71afb3d56n2g4vagwrbpkvnab47yrl")))

(define-public crate-ownrs-0.1.2 (c (n "ownrs") (v "0.1.2") (d (list (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "1ha63sw2xk052nyqf9jalynl14747vnxxks0x6hmk328q2n527bx")))

(define-public crate-ownrs-0.2.0 (c (n "ownrs") (v "0.2.0") (d (list (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "0p13fjcwaj0hprrxaa7l0plqfz9vpi44qpdxssflac8pdcn93x6b")))

(define-public crate-ownrs-0.3.0 (c (n "ownrs") (v "0.3.0") (d (list (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "1g7wki1iw4jcybwy74jx7plcmnahb3a3100p0hxf7p5nwd0sny8p")))

(define-public crate-ownrs-0.3.1 (c (n "ownrs") (v "0.3.1") (d (list (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "1iazq2brz65hwhlp4hj3ik9ml9c9anhd949pzvkvcif2zb2bj76m")))

(define-public crate-ownrs-0.3.2 (c (n "ownrs") (v "0.3.2") (d (list (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "1z62s0imksvlmz5620b7irrqzy2vi4m4245w5102am5qb9ym7wym")))

(define-public crate-ownrs-0.3.3 (c (n "ownrs") (v "0.3.3") (d (list (d (n "globset") (r "^0.4.5") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "0cpbd9va6j65prdslhz7jb9i0kbradw8f5ijy5awz703l97g73fy")))

(define-public crate-ownrs-0.3.4 (c (n "ownrs") (v "0.3.4") (d (list (d (n "globset") (r "^0.4.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)))) (h "01kgyhgjxiddnn5d7222n89c8snhzmf6z318hcl9p0vassqdhpaf")))

(define-public crate-ownrs-0.3.5 (c (n "ownrs") (v "0.3.5") (d (list (d (n "globset") (r "^0.4.6") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)))) (h "0sllw5jpc61dsc8ihnxippaqxg47j2ifhi8llwiq92b337vapppg")))

