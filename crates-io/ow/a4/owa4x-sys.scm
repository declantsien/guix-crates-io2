(define-module (crates-io ow a4 owa4x-sys) #:use-module (crates-io))

(define-public crate-owa4x-sys-0.1.0 (c (n "owa4x-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)))) (h "187kmmv0n25sck62cn76m3b50632vxb56q3ha7zvhv5pbhmw83ds")))

(define-public crate-owa4x-sys-0.1.1 (c (n "owa4x-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)))) (h "0plyx66ww0g4gh8mp9rf1z1q3vfrg04x7ydzq3af276kqdam3g9x")))

(define-public crate-owa4x-sys-0.1.2 (c (n "owa4x-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)))) (h "0rn9g92m33hcmadprhk52rf1223fkmaf0vr385vnrq2pxfp1y3dy")))

(define-public crate-owa4x-sys-0.1.3 (c (n "owa4x-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)))) (h "0ilsiaf8msv8bc778yjz8gwafww0zb7yiayc667hf2jr8fxd2z19")))

(define-public crate-owa4x-sys-0.1.4 (c (n "owa4x-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "09war8kc111xrid7jlr4m53gz7x0590098c672fnbkzcxhdb6c5x")))

(define-public crate-owa4x-sys-0.1.5 (c (n "owa4x-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "1ymmcksnf15zkcad2qz0cwcqm8ibbdk25wqhvjpqcnhnx0v1j97a")))

(define-public crate-owa4x-sys-0.1.6 (c (n "owa4x-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "17znxv2wykl15c6nyq4h30skq4bq6q4cwqkwaw7vn1xl1f9gyr0v")))

(define-public crate-owa4x-sys-0.1.7 (c (n "owa4x-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "01a5xb53ddigvpdl0sk7721gz15gncm8qcwss6m37fl7am0lk0f8")))

