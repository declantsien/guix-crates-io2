(define-module (crates-io ow ld owldict) #:use-module (crates-io))

(define-public crate-owldict-0.1.0 (c (n "owldict") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^1") (f (quote ("json"))) (d #t) (k 0)))) (h "18frbw556rihsf2lhby2qlb14ia3gimwlqhffcq7bkhpixvm3sxw")))

(define-public crate-owldict-1.0.0 (c (n "owldict") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^1") (f (quote ("json"))) (d #t) (k 0)))) (h "0ng4qnwkgsbx15x37d7hx8abd0c40vfx5av1fz6yix0p9d9389m1")))

(define-public crate-owldict-1.0.1 (c (n "owldict") (v "1.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^1") (f (quote ("json"))) (d #t) (k 0)))) (h "1glgnp24rkl9md4g8wfd4gqnjjd7srhvy1r2nwr5djlpl2k5v2d1")))

