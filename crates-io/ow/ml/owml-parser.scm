(define-module (crates-io ow ml owml-parser) #:use-module (crates-io))

(define-public crate-owml-parser-0.1.0 (c (n "owml-parser") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1xq8g5snjnbg64znywcz23g3c5p67dcqf6dmp5a5rcygigprpwk3")))

(define-public crate-owml-parser-0.1.1 (c (n "owml-parser") (v "0.1.1") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "143csbrp8vj36l7wdqvmp7vr27z0sagh0iyxly6qsiri5dgz67pi")))

(define-public crate-owml-parser-0.1.2 (c (n "owml-parser") (v "0.1.2") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1p75ahl22n9x7lrf205jvhg68jqmqydqyz8imnrs6lv5b7x3g9v9")))

