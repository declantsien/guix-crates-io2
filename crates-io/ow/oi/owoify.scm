(define-module (crates-io ow oi owoify) #:use-module (crates-io))

(define-public crate-owoify-0.1.0 (c (n "owoify") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "0477i8hkqnd0d0sy00gsb1ymq2w6ydzlljfz5dkg93m5pm069mrj")))

(define-public crate-owoify-0.1.1 (c (n "owoify") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "1yriaw9y429f6yx387047xiqrv5chfwc0hfva4z9y2wry8ihazq7")))

(define-public crate-owoify-0.1.2 (c (n "owoify") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "1jlvpwc5fn0y6f5g6r7fv6cviacb3idvasa87f1j0wk9h1kdz1nd")))

(define-public crate-owoify-0.1.3 (c (n "owoify") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "042a7xp0b1l7lqnnf992c200dlm9c2xvksxlfbd5wli7a65r31yi")))

(define-public crate-owoify-0.1.4 (c (n "owoify") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "09zpkam83p2mciws9fc0gyldfi06d416w3zm7538zikj121bzm74")))

(define-public crate-owoify-0.1.5 (c (n "owoify") (v "0.1.5") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)))) (h "0fcklkzji47ajciypq99y5hp38qw9sp605r3h1h2ngyihlv3gmgm")))

