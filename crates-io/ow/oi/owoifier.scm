(define-module (crates-io ow oi owoifier) #:use-module (crates-io))

(define-public crate-owoifier-0.1.0 (c (n "owoifier") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02m0i1rlvf2basxkmjr1xnsmkhqvyimcm8hwk6fmbb10fgiha5z9") (y #t)))

(define-public crate-owoifier-1.0.0 (c (n "owoifier") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17xna7c51afx32f8k34mgl65zwk3nw87rmhd6wx11gfdygzarnfy")))

(define-public crate-owoifier-1.1.0 (c (n "owoifier") (v "1.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0i8qfahkrznp7ll9yi75j1y798571qvmflvcikyjmm31cmy05kw6")))

(define-public crate-owoifier-1.2.0-beta (c (n "owoifier") (v "1.2.0-beta") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mizjan692bi72nsajy0hjwcddglhxz6sqlgwwys6dwk4ixlmlzv")))

(define-public crate-owoifier-1.2.0 (c (n "owoifier") (v "1.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "000l6xvrsv28svas7gaa9nv4nb1zffvrpxh59v13qzg54zympsh4")))

