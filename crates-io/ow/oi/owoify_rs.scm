(define-module (crates-io ow oi owoify_rs) #:use-module (crates-io))

(define-public crate-owoify_rs-0.1.0 (c (n "owoify_rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ydqffyrshyb3dqd9fsd2g8z64xh1siqr5cl5dwwcikw3iwgskkh")))

(define-public crate-owoify_rs-0.1.1 (c (n "owoify_rs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1syd6iim43dl8gg564p9m2dymmnf8wj9amdsb1slvq4yicd4hfdp")))

(define-public crate-owoify_rs-0.1.2 (c (n "owoify_rs") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pc2iyj4hmf3ymy0mafy6kv9d2byhqnjqa8d310g1954af50q2cl")))

(define-public crate-owoify_rs-0.1.3 (c (n "owoify_rs") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0y078wsi2l7gaxqpdmwj0yhhw6war5j5r37s2w1vqkz00kvzwmic")))

(define-public crate-owoify_rs-0.1.4 (c (n "owoify_rs") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jw2bs4q7n8zi5j787ykvs9igl9n15f6h3yaij5iasdyy9bfrz5p")))

(define-public crate-owoify_rs-0.1.5 (c (n "owoify_rs") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gcfkb2m782zqpwzdqqrh41mn3zmmq3mcah5pp49krqrqv9nzhg5")))

(define-public crate-owoify_rs-0.2.0 (c (n "owoify_rs") (v "0.2.0") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mgixj7scdwd8mkw2d2nvbip5nc6b3iy163gnc8ldn7bzi748i96")))

(define-public crate-owoify_rs-1.0.0 (c (n "owoify_rs") (v "1.0.0") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0yv7iv03jrp0l63yb5snm6qmrcxv6y5m9g2m0vpcrabm10sz5kv0")))

(define-public crate-owoify_rs-1.0.1 (c (n "owoify_rs") (v "1.0.1") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gh613yix5bwb826k6zz1kdpwsh39mg0njfic4pdl5r3j41j9b6w")))

(define-public crate-owoify_rs-1.1.0 (c (n "owoify_rs") (v "1.1.0") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xrz2sdm1vcg7pign9axafnchadz1vbhv32pknsalqq2whls009x")))

