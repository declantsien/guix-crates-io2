(define-module (crates-io ow ap owapi) #:use-module (crates-io))

(define-public crate-owapi-0.1.0 (c (n "owapi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "05fwis7b65fg8kwbm5haihldip4zqip0mj27jfb7fvsh39ns7rc7")))

(define-public crate-owapi-1.0.0 (c (n "owapi") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1adbvfv96q4hil4ip85vbs1z6dyanpqnyj046zvbqph5m6kj4pjg")))

(define-public crate-owapi-2.0.0 (c (n "owapi") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1g90j8ydzm8haqn6rmw9y41j42nyyvdgzjx62dybhgx3cg97g692")))

