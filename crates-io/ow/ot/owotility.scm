(define-module (crates-io ow ot owotility) #:use-module (crates-io))

(define-public crate-owotility-0.1.0 (c (n "owotility") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "serial2") (r "^0.2.20") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)) (d (n "usbd-human-interface-device") (r "^0.4.5") (d #t) (k 0)))) (h "08b6h2rhd35xdb78q9zvbym500lqcqn5wgnsllibpdybyki47i41") (y #t)))

(define-public crate-owotility-0.1.1 (c (n "owotility") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "serial2") (r "^0.2.20") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)) (d (n "usbd-human-interface-device") (r "^0.4.5") (d #t) (k 0)))) (h "0fr96d61ks430qx9hiq6ah762ypyh4nz3x30wxq3sj4yvb7fyy13")))

