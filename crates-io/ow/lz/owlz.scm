(define-module (crates-io ow lz owlz) #:use-module (crates-io))

(define-public crate-owlz-0.1.0 (c (n "owlz") (v "0.1.0") (d (list (d (n "enum-assoc") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_derive2") (r "^0.1.17") (d #t) (k 0)))) (h "1m7vs8xcyg2v4004m8v49lrakdf5l5svigiv26gnh1vp57gf9n5i")))

(define-public crate-owlz-0.1.1 (c (n "owlz") (v "0.1.1") (d (list (d (n "enum-assoc") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_derive2") (r "^0.1.17") (d #t) (k 0)))) (h "1zrm6vqnmjgx33sbmfrzwaw9zawhvv3biwhs9szy4scycckxjzya")))

