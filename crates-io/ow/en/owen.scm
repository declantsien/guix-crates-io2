(define-module (crates-io ow en owen) #:use-module (crates-io))

(define-public crate-owen-0.1.0 (c (n "owen") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)))) (h "18d1418r45p9bdz9fl6hqkpp8q0fgi3fisw5mjx7qdh6njzsr31g") (y #t)))

(define-public crate-owen-0.0.1 (c (n "owen") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)))) (h "0sm4mc2hj88q057hz1cp317ym1vx8rfnmdqcbqc3wm9k971zd8rl") (y #t)))

(define-public crate-owen-0.0.0 (c (n "owen") (v "0.0.0") (h "1vhd42ykic118sp728jq6dmyz6bx67klrsb5m6233qi3xcdxl562")))

