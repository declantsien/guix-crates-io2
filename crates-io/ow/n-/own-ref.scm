(define-module (crates-io ow n- own-ref) #:use-module (crates-io))

(define-public crate-own-ref-0.0.0 (c (n "own-ref") (v "0.0.0") (h "0dchv7b0h9cd025wbd90pgqj6v49hrr2yhdjy6bm81mh82qpkd2a")))

(define-public crate-own-ref-0.1.0-alpha (c (n "own-ref") (v "0.1.0-alpha") (d (list (d (n "extension-traits") (r "^1.0.1") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.1.3") (d #t) (k 2)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)))) (h "1hnkwx1v6fnhnl95q9j9cxmg1j1fxdw2brrkzlqyv3yj3l7amd6q") (f (quote (("offset_of") ("default" "offset_of")))) (r "1.68.0")))

