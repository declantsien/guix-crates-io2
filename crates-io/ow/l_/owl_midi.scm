(define-module (crates-io ow l_ owl_midi) #:use-module (crates-io))

(define-public crate-owl_midi-0.1.0 (c (n "owl_midi") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0ivfd0ihykliicays2amsv77r0jj5rhdp6f0iqjla72lmmb2k442")))

(define-public crate-owl_midi-0.2.0 (c (n "owl_midi") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1psa2ji42s87mppgx48vms0ia9w2173kb9llq3lsviqad7xvvklz")))

(define-public crate-owl_midi-0.2.1 (c (n "owl_midi") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0xw7cklkqjb9i9v5f3iyrkybm64jxdwci7fnwm41g641k3dh9azr")))

(define-public crate-owl_midi-0.2.2 (c (n "owl_midi") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "0dhnd8m86inqfl1wf582dc0gfsmnwgnz0ngda38c380xs2bj28zl")))

(define-public crate-owl_midi-0.3.0 (c (n "owl_midi") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "0fabvypk9bp5nx3ld5f8zjigpm708g8sd7rpssqamaphkgsmahb3")))

(define-public crate-owl_midi-0.3.1 (c (n "owl_midi") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "03q68zlvql8p42ilx5j46xi1gw63cdgc74bz3rw93k8ryk5xfa8l")))

(define-public crate-owl_midi-0.3.2 (c (n "owl_midi") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (f (quote ("full-syntax"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0khl3m9522l6sd67rw7n0ibi2sawavsriqhpzs6lm2fsnw5bli3z")))

(define-public crate-owl_midi-0.3.3 (c (n "owl_midi") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (f (quote ("full-syntax"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1fzcsb9v197wizxrwrkmxvc43ybamzv8lzghhiz8yvvwxh112hvp")))

(define-public crate-owl_midi-0.4.0 (c (n "owl_midi") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (f (quote ("full-syntax"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0hhp1c79pg64v7snrpx01ih24cpq0w76rwy7jq05v1ss4273lwav")))

(define-public crate-owl_midi-0.4.1 (c (n "owl_midi") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (f (quote ("full-syntax"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0z91gk8dd2if8sfwwcphy7a9hvs9mpancdk01d9g7kqgyf3zb6rk")))

(define-public crate-owl_midi-0.4.2 (c (n "owl_midi") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (f (quote ("full-syntax"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1v0xc60wlxv7hwlz1kylazl72v8f00ym697fi2lw7lnbdlg222f9")))

(define-public crate-owl_midi-0.4.3 (c (n "owl_midi") (v "0.4.3") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (f (quote ("full-syntax"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19nci79smsr9kszvi4lq3yrx6izvqpbvjz2849gckn68q61xqjm5")))

(define-public crate-owl_midi-0.4.4 (c (n "owl_midi") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (f (quote ("full-syntax"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0csjzjiak7swdc1crlrpmc0bi5g7b35pi75s6qyn9kq4b75b78si")))

(define-public crate-owl_midi-0.4.5 (c (n "owl_midi") (v "0.4.5") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (f (quote ("full-syntax"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12xd9z2nfkncas5rsjwax3dppqibc8b5g5vk7g15w7abz76w39kg")))

