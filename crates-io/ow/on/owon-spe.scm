(define-module (crates-io ow on owon-spe) #:use-module (crates-io))

(define-public crate-owon-spe-0.1.0 (c (n "owon-spe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "async-compat") (r "^0.2.1") (d #t) (k 2)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-serial") (r "^5.4.4") (d #t) (k 2)))) (h "0glzlzpcxl623lmr1d7phwq4ndflq9vaynmdd089ag2blmpyxrjw") (s 2) (e (quote (("serialport" "dep:serialport") ("async" "dep:futures"))))))

