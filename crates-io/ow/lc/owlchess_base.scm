(define-module (crates-io ow lc owlchess_base) #:use-module (crates-io))

(define-public crate-owlchess_base-0.1.0 (c (n "owlchess_base") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "14dgl9c7bqk7h202l3q0w53h4jlvw80lls0qj9vqww0pp17dp1a6")))

(define-public crate-owlchess_base-0.2.0 (c (n "owlchess_base") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1cl9glvw45w3dq3d2lkim2kd29xn41hk7blj6z2hqlwycxydwhqg")))

(define-public crate-owlchess_base-0.2.1 (c (n "owlchess_base") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "16q8a1hszbgshf4p3hgylmw41bwhiqw3rvr3j840zjbzzxzf7jky")))

(define-public crate-owlchess_base-0.2.2 (c (n "owlchess_base") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0xlc6n4nclhz2jjj7hhp6svw2j5g2ha9803smypypab30j8khrp1")))

(define-public crate-owlchess_base-0.2.3 (c (n "owlchess_base") (v "0.2.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "005lm73kmyccrsixssg5lk44nm0vfpkdjm1p7cpfzm2wa7v1j681")))

(define-public crate-owlchess_base-0.3.0 (c (n "owlchess_base") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "18m7gzs3bp68wkpamvmcxs40sl4iwpm0yi30cgk6dnyg5q3fj32g")))

(define-public crate-owlchess_base-0.3.1 (c (n "owlchess_base") (v "0.3.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1gpvx89w6wwfvgbmnx97fbsasgllf741798y45y2ynfb8z6k6pxr")))

(define-public crate-owlchess_base-0.3.2 (c (n "owlchess_base") (v "0.3.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1nwbzkp618sydgv1ysqq8xz9zwprnq2dhgx7m7hsnm354syfcngj")))

(define-public crate-owlchess_base-0.3.3 (c (n "owlchess_base") (v "0.3.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1hkw95niqpqqglifsf1a3zb3iapscyj97i5jkdvh6nga7cywzaqj")))

(define-public crate-owlchess_base-0.4.0 (c (n "owlchess_base") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1x5ngxvg10pjw7g7b65vq44ajcnqayk1iqhk2r2rnprb5r1q113k")))

