(define-module (crates-io ow ne owned-pin-macros) #:use-module (crates-io))

(define-public crate-owned-pin-macros-0.1.0 (c (n "owned-pin-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1nfxvif6l53ij9ks3579il89b84j5gk2scq6wwc35pr2xnkakfh4")))

(define-public crate-owned-pin-macros-0.2.0 (c (n "owned-pin-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0yrc2wjz9r2fhk0iwqj0i8hnd8xppd5zhgan3i58k9f3zdk07sc3")))

