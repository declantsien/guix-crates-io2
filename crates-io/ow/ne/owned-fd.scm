(define-module (crates-io ow ne owned-fd) #:use-module (crates-io))

(define-public crate-owned-fd-0.1.0 (c (n "owned-fd") (v "0.1.0") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "tempfile") (r "2.*") (d #t) (k 2)))) (h "1lisqlv5cr2gmz7l7j9c0l12lvzkfdfws5hcsnv38dpsk4ja4369")))

