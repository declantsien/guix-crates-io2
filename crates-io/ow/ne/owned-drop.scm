(define-module (crates-io ow ne owned-drop) #:use-module (crates-io))

(define-public crate-owned-drop-0.1.0 (c (n "owned-drop") (v "0.1.0") (h "1d0x511f5ymg4bkkai5z074m6g415cwx2p0g94r6jwdji2azdw7p")))

(define-public crate-owned-drop-0.1.1 (c (n "owned-drop") (v "0.1.1") (h "0fc8i56vva410qc42zmza167f3y40ab3wcp3zp214z0xmcbj5sjh")))

