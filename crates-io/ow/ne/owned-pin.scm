(define-module (crates-io ow ne owned-pin) #:use-module (crates-io))

(define-public crate-owned-pin-0.1.0 (c (n "owned-pin") (v "0.1.0") (h "0cx0j517ac49d9fkqk3i9ds6vpcz02jnfzj6s01pklvcx9n5xh43") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-0.1.1 (c (n "owned-pin") (v "0.1.1") (h "151ab9pc5wqjpqx9h0a19i1rn6202003ni1hmr438r67f0i80lq5") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-0.2.0 (c (n "owned-pin") (v "0.2.0") (h "05472jpa4igv86nvp8yjb2v9ymygpjpml3gzi80knf7ihln9s389") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-1.0.0 (c (n "owned-pin") (v "1.0.0") (h "1i08r6xfjff6p1ydgly0nvbfvqlclpqhw2x9wgrnmd25afv41qpq") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-1.0.1 (c (n "owned-pin") (v "1.0.1") (h "1vwj0s7wnqdjxhjdhshhdiaf2xwig5l90zri940lq4cbwhcs6jyy") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-1.1.0 (c (n "owned-pin") (v "1.1.0") (h "0bnbrcslaabg71slqznhc4nggni9n4j8vi7cnrjvi613239pxr99") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-1.2.0 (c (n "owned-pin") (v "1.2.0") (d (list (d (n "pinned-init") (r "^0.0") (o #t) (k 0)))) (h "07kj08qccxrhdfdf948ay06jca033ml7ia0sf7m98jh11sl42lyq") (f (quote (("default" "alloc" "pinned-init") ("alloc" "pinned-init/alloc")))) (s 2) (e (quote (("pinned-init" "dep:pinned-init"))))))

(define-public crate-owned-pin-1.3.0 (c (n "owned-pin") (v "1.3.0") (d (list (d (n "owned-pin-macros") (r "^0.1") (d #t) (k 0)) (d (n "pinned-init") (r "^0.0") (o #t) (k 0)))) (h "0v7sw1ahcjzkkxmc1yvb1rk4nh2w5fw7qpvlwhdldvq6579cwqvm") (f (quote (("default" "alloc" "pinned-init") ("alloc" "pinned-init/alloc")))) (y #t) (s 2) (e (quote (("pinned-init" "dep:pinned-init"))))))

(define-public crate-owned-pin-1.3.1 (c (n "owned-pin") (v "1.3.1") (d (list (d (n "owned-pin-macros") (r "^0.1") (d #t) (k 0)) (d (n "pinned-init") (r "^0.0") (o #t) (k 0)))) (h "0rb7r9rrrg4l0mafnvmqlbrq8j3abdyj759y79p8x6jzxq4j43dc") (f (quote (("default" "alloc" "pinned-init") ("alloc" "pinned-init/alloc")))) (s 2) (e (quote (("pinned-init" "dep:pinned-init"))))))

(define-public crate-owned-pin-1.3.2 (c (n "owned-pin") (v "1.3.2") (d (list (d (n "owned-pin-macros") (r "^0.2") (d #t) (k 0)) (d (n "pinned-init") (r "^0.0") (o #t) (k 0)))) (h "0a7yzws3xa52lbnpnpj78yzzc3iq71ka5fb9dyninr6aw0vf9wkf") (f (quote (("default" "alloc" "pinned-init") ("alloc" "pinned-init/alloc")))) (s 2) (e (quote (("pinned-init" "dep:pinned-init"))))))

(define-public crate-owned-pin-1.3.3 (c (n "owned-pin") (v "1.3.3") (d (list (d (n "owned-pin-macros") (r "^0.2") (d #t) (k 0)) (d (n "pinned-init") (r "^0.0") (o #t) (k 0)))) (h "06hqfpxvmj05mk9a9qd71c8c64a5mrbv8kcf983ms098l291wfi0") (f (quote (("default" "alloc" "pinned-init") ("alloc" "pinned-init/alloc")))) (s 2) (e (quote (("pinned-init" "dep:pinned-init"))))))

