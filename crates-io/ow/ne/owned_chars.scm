(define-module (crates-io ow ne owned_chars) #:use-module (crates-io))

(define-public crate-owned_chars-0.1.0 (c (n "owned_chars") (v "0.1.0") (h "15w861h5hlanq27za62wclihgppxacqm06xsgnvmw94f00vgrck9")))

(define-public crate-owned_chars-0.1.1 (c (n "owned_chars") (v "0.1.1") (h "00b7lmhd9gqhig2wfsyzx9i28yv8wv0a3khgjxsxcvl3pd4jlfcq")))

(define-public crate-owned_chars-0.2.0 (c (n "owned_chars") (v "0.2.0") (h "0l6372bly1r010fmhc7a002rfm5qfr8a6ipl2xdhhhba6dnp2wsj")))

(define-public crate-owned_chars-0.2.1 (c (n "owned_chars") (v "0.2.1") (h "1jcym7rwq268ilaqpnln16x50w3f9r8j6b0bd3lnpagn34r70lwz")))

(define-public crate-owned_chars-0.3.0 (c (n "owned_chars") (v "0.3.0") (d (list (d (n "delegate") (r "^0.1.2") (d #t) (k 0)))) (h "1nvylv0hy8vykz09ri5cwdfb0bz47wdb5nhvx3q6wzl4vmmsxl3v")))

(define-public crate-owned_chars-0.3.1 (c (n "owned_chars") (v "0.3.1") (d (list (d (n "delegate") (r "^0.4") (d #t) (k 0)))) (h "12q4dnrzipak2ai01ya8ylaiqx46qxx0gisdwkbn83fng76zm6id")))

(define-public crate-owned_chars-0.3.2 (c (n "owned_chars") (v "0.3.2") (d (list (d (n "delegate-attr") (r "^0.2.6") (d #t) (k 0)))) (h "0fyw0v1amxgv64xl9b4hardzj5jpv0fbp1g8skb5fw5c00qsznq9")))

