(define-module (crates-io ow ne ownedbytes) #:use-module (crates-io))

(define-public crate-ownedbytes-0.1.0 (c (n "ownedbytes") (v "0.1.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "1v8ra9bm3qix2an0b4w59pf9bxfikajaxmni7fhwp5bbdxwb0qyc")))

(define-public crate-ownedbytes-0.2.0 (c (n "ownedbytes") (v "0.2.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "17n9lm25yxkfhg9fzg5c102w86sc5rwjfm5qg0fl2fbs465j1yhb")))

(define-public crate-ownedbytes-0.3.0 (c (n "ownedbytes") (v "0.3.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "0h60bjqw2vhj7g2m3waqfky6k8km29b60g08a1n0x9xjrzbip672")))

(define-public crate-ownedbytes-0.4.0 (c (n "ownedbytes") (v "0.4.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "1nzq0wwwsb5df4vlbl4i7bbfj1f550qmnvj1anbz76d2cjm7x5cf")))

(define-public crate-ownedbytes-0.5.0 (c (n "ownedbytes") (v "0.5.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "173kc1ldfgxr2zhhh5sc4lcirxi2990pzl2ibpxxa107nacf8667")))

(define-public crate-ownedbytes-0.6.0 (c (n "ownedbytes") (v "0.6.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "1dxli131kz85jdq7v6kb5d21sghji011k351nfmri0df32wp52kf")))

(define-public crate-ownedbytes-0.7.0 (c (n "ownedbytes") (v "0.7.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "0n65zhnkxci7sw7664z9c3zfv1dxp7k45q28p4jz9f33n3pmk863")))

