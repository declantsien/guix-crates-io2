(define-module (crates-io ow ne owned-singleton) #:use-module (crates-io))

(define-public crate-owned-singleton-0.1.0 (c (n "owned-singleton") (v "0.1.0") (d (list (d (n "owned-singleton-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "10zd9vys3az5gsljbbg1w33qs9g81k7pdid40svpsz69d643gai1")))

