(define-module (crates-io ow ne owned_chunks) #:use-module (crates-io))

(define-public crate-owned_chunks-0.1.0 (c (n "owned_chunks") (v "0.1.0") (h "1i9al7n18y2cwszgnwdcb49bnvl13f70z0klrsbww42vhpdsz3rs") (y #t)))

(define-public crate-owned_chunks-0.1.1 (c (n "owned_chunks") (v "0.1.1") (h "0kjxan1vxbmflmk7kh6qmzag6phx8fmmkfix46g9z15smmpwswgd")))

