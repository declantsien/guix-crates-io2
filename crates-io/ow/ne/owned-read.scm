(define-module (crates-io ow ne owned-read) #:use-module (crates-io))

(define-public crate-owned-read-0.1.0 (c (n "owned-read") (v "0.1.0") (d (list (d (n "rental") (r "^0.5") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1") (d #t) (k 0)))) (h "07s2yksd6wfzyp6sr3a6bfnxr1017dzr2qpawnfgc2vajg28c6l9")))

(define-public crate-owned-read-0.2.0 (c (n "owned-read") (v "0.2.0") (d (list (d (n "rental") (r "^0.5") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1") (d #t) (k 0)))) (h "1wilxlkid7m9jmrxcj7wafx8ljp481h7nnbgf30nvfbffgr8nf5j")))

(define-public crate-owned-read-0.3.0 (c (n "owned-read") (v "0.3.0") (d (list (d (n "rental") (r "^0.5") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1") (d #t) (k 0)))) (h "0zas7jxj22arf7daf1lfv926b1h4sm7bvz9cj9mpj9v60vnq4zh8")))

(define-public crate-owned-read-0.4.0 (c (n "owned-read") (v "0.4.0") (d (list (d (n "rental") (r "^0.5") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1") (d #t) (k 0)))) (h "0frd3gplrvhv00gj6957zl8m4nsvbaid8y5xzx6zq9yn32mpzm85")))

(define-public crate-owned-read-0.4.1 (c (n "owned-read") (v "0.4.1") (d (list (d (n "stable_deref_trait") (r "^1") (d #t) (k 0)))) (h "188vyywh8hfy7r1zpyln2h2qqijgny4hnmckrx2wisxwb8iiwvdn")))

