(define-module (crates-io ow ne owned-buf) #:use-module (crates-io))

(define-public crate-owned-buf-0.0.0 (c (n "owned-buf") (v "0.0.0") (h "01gzdq60yx6siw5487018ijr8fmi3p32dq5f49mva5fd9qj6zfhn")))

(define-public crate-owned-buf-0.1.0 (c (n "owned-buf") (v "0.1.0") (h "0dgvp2rrpg2ylaw8yjfw7rik2i3r1y79mgm9zmmqnyd18gsch752")))

(define-public crate-owned-buf-0.3.0 (c (n "owned-buf") (v "0.3.0") (h "03d07vyv1hdnh227jgl63sdj15wmx3mfag87rjcbvdi2nvh13cr9")))

