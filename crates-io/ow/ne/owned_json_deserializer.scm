(define-module (crates-io ow ne owned_json_deserializer) #:use-module (crates-io))

(define-public crate-owned_json_deserializer-1.0.0 (c (n "owned_json_deserializer") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02fkb29yz259g7sm92r111as650272lwsaxw9sf0ff6ckr4f7r4m")))

