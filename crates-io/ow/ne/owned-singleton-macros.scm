(define-module (crates-io ow ne owned-singleton-macros) #:use-module (crates-io))

(define-public crate-owned-singleton-macros-0.1.0 (c (n "owned-singleton-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1qvffpwnmwzvr35gdzb3lszlmvxhgw1hgdm5f2zifl5rf702lp4d")))

