(define-module (crates-io ow ne owned-alloc) #:use-module (crates-io))

(define-public crate-owned-alloc-0.1.0 (c (n "owned-alloc") (v "0.1.0") (h "1rbpcyrgc13dra1r7mgbcjsfqy3qwzhf1c8xliys0lv5lbvsrv2f")))

(define-public crate-owned-alloc-0.2.0 (c (n "owned-alloc") (v "0.2.0") (h "1mh54983yz8chn6lc4rgvhazys73dc129y654a9gy4ls3x0ypz1h")))

