(define-module (crates-io ow na ownable-macro) #:use-module (crates-io))

(define-public crate-ownable-macro-0.5.0 (c (n "ownable-macro") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0qz0acnkyv73dkb5cb0gnx8wgg6a08mmvm9c1xjnyyl9qdsc5kkj")))

(define-public crate-ownable-macro-0.6.0 (c (n "ownable-macro") (v "0.6.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "proc-macro" "clone-impls"))) (k 0)))) (h "0yqi857vih9shy1fwwmd8rjz6f1nwq0il6jq6h9fqkb2h4kivjdj")))

