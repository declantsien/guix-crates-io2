(define-module (crates-io ow na ownable) #:use-module (crates-io))

(define-public crate-ownable-0.5.0 (c (n "ownable") (v "0.5.0") (d (list (d (n "ownable-macro") (r "^0.5") (d #t) (k 0)))) (h "04n6pldfj7p3g1jydpj065wpbs0gagydnrp3nyd14y2jn0yqf7av") (f (quote (("std") ("default" "std"))))))

(define-public crate-ownable-0.6.0 (c (n "ownable") (v "0.6.0") (d (list (d (n "ownable-macro") (r "^0.6") (d #t) (k 0)))) (h "1368nr9qqxk7038c35hlp89jmvkv6mzm8ml96skbmvmdmj6sdav1") (f (quote (("std") ("default" "std"))))))

(define-public crate-ownable-0.6.1 (c (n "ownable") (v "0.6.1") (d (list (d (n "ownable-macro") (r "^0.6") (d #t) (k 0)))) (h "0426nrw8hawk10fh4h8bqkf902g59axnqlsxiishhspp77i11yga") (f (quote (("std") ("default" "std"))))))

(define-public crate-ownable-0.6.2 (c (n "ownable") (v "0.6.2") (d (list (d (n "ownable-macro") (r "^0.6") (d #t) (k 0)))) (h "0l4kbprj35mhhm1agfgx2n1dlf3c6lkgv5kx51qc9z1n2m6skjsd") (f (quote (("std") ("default" "std"))))))

