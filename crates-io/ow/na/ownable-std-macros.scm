(define-module (crates-io ow na ownable-std-macros) #:use-module (crates-io))

(define-public crate-ownable-std-macros-0.1.0 (c (n "ownable-std-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0zchfvaihkwapng45qy2mzwbxlnx68680bhxw6hxqcz9s2r3wbg9")))

(define-public crate-ownable-std-macros-0.1.1 (c (n "ownable-std-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02xbpgag36fsbwjhqfrrqijvf9rva6325dd390gwjg1qkgrhr64r")))

(define-public crate-ownable-std-macros-0.1.2 (c (n "ownable-std-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0dl5rv12fdw3gwvgjq4lqfygf5vpfvjd04lnsdabf9igwzwxj4cd")))

