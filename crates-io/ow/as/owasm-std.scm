(define-module (crates-io ow as owasm-std) #:use-module (crates-io))

(define-public crate-owasm-std-0.12.0 (c (n "owasm-std") (v "0.12.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "fixed-hash") (r "^0.3.0-beta") (k 0)) (d (n "pwasm-alloc") (r "^0.4") (d #t) (k 0)) (d (n "pwasm-libc") (r "^0.2") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (d #t) (k 0)) (d (n "uint") (r "^0.5.0-beta") (k 0)))) (h "1q63yikqm5s0s93gk285pkcsc8iyah4fmby52h805cs9fyqd1d1b") (f (quote (("test" "std" "strict") ("strict" "pwasm-libc/strict" "pwasm-alloc/strict") ("std" "fixed-hash/std" "uint/std" "byteorder/std") ("panic_with_msg") ("default" "uint/common"))))))

(define-public crate-owasm-std-0.10.0 (c (n "owasm-std") (v "0.10.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "owasm-alloc") (r "^0.4") (d #t) (k 0)) (d (n "owasm-libc") (r "^0.2") (d #t) (k 0)) (d (n "parity-hash") (r "^1") (k 0)) (d (n "tiny-keccak") (r "^1") (d #t) (k 0)))) (h "1mbsmz91hk7mh6plhdwappbkw1q9g3pdl952r3sm94lvgkcnj3qa") (f (quote (("strict" "owasm-libc/strict" "owasm-alloc/strict") ("std" "parity-hash/std") ("panic_with_msg") ("default"))))))

(define-public crate-owasm-std-0.12.1 (c (n "owasm-std") (v "0.12.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "fixed-hash") (r "^0.3.0-beta") (k 0)) (d (n "tiny-keccak") (r "^1") (d #t) (k 0)) (d (n "uint") (r "^0.5.0-beta") (k 0)))) (h "134kzfqhcsq7i4n8lwvd6zksnsyw7li1xxmcz2m7ylnv0y5izfj3") (f (quote (("test" "std") ("std" "fixed-hash/std" "uint/std" "byteorder/std") ("panic_with_msg") ("default" "uint/common" "std"))))))

(define-public crate-owasm-std-0.12.2 (c (n "owasm-std") (v "0.12.2") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "fixed-hash") (r "^0.3.0-beta") (k 0)) (d (n "tiny-keccak") (r "^1") (d #t) (k 0)) (d (n "uint") (r "^0.5.0-beta") (k 0)))) (h "0v4jkj6gscn0h84haxl12s0vnk2ih812xjfa77nf3j4p4fm62850") (f (quote (("test" "std") ("std" "fixed-hash/std" "uint/std" "byteorder/std") ("panic_with_msg") ("default" "uint/common" "std"))))))

(define-public crate-owasm-std-0.10.1 (c (n "owasm-std") (v "0.10.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "parity-hash") (r "^1") (k 0)) (d (n "tiny-keccak") (r "^1") (d #t) (k 0)))) (h "0i4zcrpmrxp6wy18rlb4ym6hf19fxfxrhj7hv8fq7n3g7qk22gdd") (f (quote (("test" "std") ("std" "parity-hash/std") ("panic_with_msg") ("default"))))))

(define-public crate-owasm-std-0.13.0 (c (n "owasm-std") (v "0.13.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "parity-hash") (r "^1") (k 0)) (d (n "tiny-keccak") (r "^1") (d #t) (k 0)))) (h "0glrnck5cd2lnai3f9yc1mljshv0dijan218bl9likdyidzgasdm") (f (quote (("test" "std") ("std" "parity-hash/std") ("panic_with_msg") ("default"))))))

(define-public crate-owasm-std-0.20.0 (c (n "owasm-std") (v "0.20.0") (d (list (d (n "fixed-hash") (r "^0.3.0") (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "uint") (r "^0.6.1") (k 0)))) (h "0bmd8wz2pm82nfmxcgrbk3khcb9jkffdix0dkw6an31wqkq3655k") (f (quote (("platform-alloc"))))))

(define-public crate-owasm-std-0.20.1 (c (n "owasm-std") (v "0.20.1") (d (list (d (n "fixed-hash") (r "^0.3.0") (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "uint") (r "^0.6.1") (k 0)))) (h "1403vgdwpgajzkjc34j7ijvljr89ikr12hnpcnmr55nq2ml60qjr") (f (quote (("platform-alloc"))))))

