(define-module (crates-io ow as owasm-utils-cli) #:use-module (crates-io))

(define-public crate-owasm-utils-cli-0.1.0 (c (n "owasm-utils-cli") (v "0.1.0") (h "1klva6vxv46snxqj6m6x38hfrdksdgjflip59qrg6avrq5230dn8")))

(define-public crate-owasm-utils-cli-0.6.0 (c (n "owasm-utils-cli") (v "0.6.0") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "owasm-utils") (r "^0.6") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.31.3") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1gb3m6nngqdj3dcrci7p35gk2mgs1jmc36a225wpkcfmq2gb2lbi")))

(define-public crate-owasm-utils-cli-0.6.1 (c (n "owasm-utils-cli") (v "0.6.1") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "owasm-utils") (r "^0.6") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.31.3") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "039i8shmn5d1m50adjhzbm4r0xh5h7kk9137ig1hpwbbvdxpf6b4")))

