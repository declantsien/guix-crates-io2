(define-module (crates-io ow as owasm-abi) #:use-module (crates-io))

(define-public crate-owasm-abi-0.1.0 (c (n "owasm-abi") (v "0.1.0") (h "162i3b5x1sk8s8y21r4l1ngmkwy9azsrn0bmhhg1570zq6g5bw5i")))

(define-public crate-owasm-abi-0.1.12 (c (n "owasm-abi") (v "0.1.12") (d (list (d (n "byteorder") (r "^1.2.3") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "pwasm-std") (r "^0.10") (d #t) (k 0)) (d (n "uint") (r "^0.4") (k 0)))) (h "1czsgngzkiqxz9wqlch9r92xdj6crbm6l0932f48v9zk43qbj36g") (f (quote (("strict") ("std" "uint/std" "pwasm-std/std") ("default" "std"))))))

(define-public crate-owasm-abi-0.1.13 (c (n "owasm-abi") (v "0.1.13") (d (list (d (n "byteorder") (r "^1.2.3") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "owasm-std") (r "^0.10") (d #t) (k 0)) (d (n "uint") (r "^0.4") (k 0)))) (h "0m9d2ifxsz5xlxhlfzws4hidly4xfgj18wkp6c1xli1j1sz578l0") (f (quote (("strict") ("std" "uint/std" "owasm-std/std") ("default" "std"))))))

(define-public crate-owasm-abi-0.1.14 (c (n "owasm-abi") (v "0.1.14") (d (list (d (n "byteorder") (r "^1.2.3") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "owasm-std") (r "^0.13") (d #t) (k 0)) (d (n "uint") (r "^0.4") (k 0)))) (h "1bp45s99231x7nv8mmx6z9xkz7kfpl5px9d32ammc51gqpi5azks") (f (quote (("strict") ("std" "uint/std" "owasm-std/std") ("default" "std"))))))

(define-public crate-owasm-abi-0.3.0 (c (n "owasm-abi") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "owasm-std") (r "^0.20.1") (d #t) (k 0)))) (h "0dnlhsh9zsgsddazqa254qj2li1ji1syh0wsp07raqacl0rbb37a") (f (quote (("strict") ("default"))))))

