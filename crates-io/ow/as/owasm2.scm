(define-module (crates-io ow as owasm2) #:use-module (crates-io))

(define-public crate-owasm2-0.1.7 (c (n "owasm2") (v "0.1.7") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "1va0ma6nx6spqxrcxj43xfpffyvggbb75ffj29fiicyk276w4nhs")))

(define-public crate-owasm2-0.1.8 (c (n "owasm2") (v "0.1.8") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "036mrx26n1x01zk897nbbgnblvfk4832xmg8pq2pr9p0dqw29mmz")))

