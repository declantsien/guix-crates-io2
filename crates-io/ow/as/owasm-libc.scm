(define-module (crates-io ow as owasm-libc) #:use-module (crates-io))

(define-public crate-owasm-libc-0.2.0 (c (n "owasm-libc") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "rlibc") (r "^1.0") (d #t) (k 0)))) (h "14c7gg0s3q5ml9xpnv3vn910v2dgi0gz83vw8sky9q4ngl03kih8") (f (quote (("strict") ("ext_memops"))))))

