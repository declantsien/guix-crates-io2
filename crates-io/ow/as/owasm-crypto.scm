(define-module (crates-io ow as owasm-crypto) #:use-module (crates-io))

(define-public crate-owasm-crypto-0.2.0 (c (n "owasm-crypto") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kk15w276wgjlyl45cwh8hkw6jqizv346bw76ph81f02bx4dsrzi") (f (quote (("default") ("backtraces"))))))

(define-public crate-owasm-crypto-0.2.1 (c (n "owasm-crypto") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nkqsdr8sz9ai67rg2f3nx0j86c33km6haacfhyk6bwpvvv8ja00") (f (quote (("default") ("backtraces"))))))

(define-public crate-owasm-crypto-0.2.2 (c (n "owasm-crypto") (v "0.2.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wv42bpjy1xc8s66pw48hrw9ndzslwzd8q4v3xdvfzx4fhm70gyh") (f (quote (("default") ("backtraces"))))))

(define-public crate-owasm-crypto-0.3.1 (c (n "owasm-crypto") (v "0.3.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l7shp4hp8c36zh1ykv2llnhpn59ccnvgvn1s25h5srhiqgrixpa") (f (quote (("default") ("backtraces"))))))

