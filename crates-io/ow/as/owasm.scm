(define-module (crates-io ow as owasm) #:use-module (crates-io))

(define-public crate-owasm-0.1.0 (c (n "owasm") (v "0.1.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18msv5v36iv5mq678i3mdlbn9wxinj9hrj3yrnr8bhyw2dmqx2m0")))

(define-public crate-owasm-0.1.1 (c (n "owasm") (v "0.1.1") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ci8719l1by63gg158i3p02bvnav44s4blfa7p921saw3agvpf5c")))

(define-public crate-owasm-0.1.2 (c (n "owasm") (v "0.1.2") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q3g2j4lqarb9ynvk1hckpiz977z5ddgzxif3m5hr6l2sa72l5a7")))

(define-public crate-owasm-0.1.3 (c (n "owasm") (v "0.1.3") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03ml22gljwrhyyy1np03g8bfn2svf8lrh6rsi4s0a8bkm0hkd62h")))

(define-public crate-owasm-0.1.4 (c (n "owasm") (v "0.1.4") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fz05kj1hyawcvlvcw920zzg3akpg4lvfnhkbiil5azj3q3747r2")))

(define-public crate-owasm-0.1.5 (c (n "owasm") (v "0.1.5") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jmpifl079nhs6gqfrk6na8z651f3r081kjarybi73dll23i6s6r")))

(define-public crate-owasm-0.1.6 (c (n "owasm") (v "0.1.6") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00x08dcz7nj010xn7dz47nmjckl6mvqbkb4zl1kflp8hv5x05mrx")))

(define-public crate-owasm-0.1.7 (c (n "owasm") (v "0.1.7") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gk08nkbgp8afwcl78zc7b59xcvccdbb9babiiqd77rgnbjydsjz")))

(define-public crate-owasm-0.1.9 (c (n "owasm") (v "0.1.9") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.41") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.17") (d #t) (k 0)) (d (n "wasmer-runtime-core") (r "^0.17") (d #t) (k 0)) (d (n "wasmer-singlepass-backend") (r "^0.17") (d #t) (k 0)))) (h "0bp4nyrxybfddab8q4x28vq448s5phzs72wi0w7kyf3b2prn2x3k")))

(define-public crate-owasm-0.1.10 (c (n "owasm") (v "0.1.10") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.41") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "pwasm-utils") (r "^0.12") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "wasmer-runtime") (r "^0.17") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "wasmer-runtime-core") (r "^0.17") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "wasmer-singlepass-backend") (r "^0.17") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "08g3swlf10da1vmkn931zgfhan7kxqba8mn4kkn6fhyqhl1cyq8x")))

(define-public crate-owasm-0.1.11 (c (n "owasm") (v "0.1.11") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 0)) (d (n "clru") (r "^0.2.0") (d #t) (k 0)) (d (n "cosmwasm-vm") (r "^0.13.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.41") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "wasmer") (r "^1.0.2") (f (quote ("jit" "singlepass" "compiler"))) (k 0)))) (h "0llxdjahvvnh25amxdzprjs31664yic45ng85hpyn4hq74jabd0p") (y #t)))

