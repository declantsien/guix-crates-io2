(define-module (crates-io ow as owasm-abi-utils) #:use-module (crates-io))

(define-public crate-owasm-abi-utils-0.1.0 (c (n "owasm-abi-utils") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0wv5imahbiy3cwlhcphsn5n9jq0ar4kp01xmvdgilxw0i5wkfm25") (f (quote (("default" "syn/full" "syn/parsing"))))))

