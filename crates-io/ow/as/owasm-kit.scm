(define-module (crates-io ow as owasm-kit) #:use-module (crates-io))

(define-public crate-owasm-kit-0.1.12 (c (n "owasm-kit") (v "0.1.12") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1pqmgy9w8s5f6qhyl9shb2bx58rg8drvdbd56gr6jlrgrxhaq36j")))

(define-public crate-owasm-kit-0.1.13 (c (n "owasm-kit") (v "0.1.13") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1vd5k8mxsk46h0gkhm3z2bn6nq7aymp7rvf6srcc9dz411d1mi51")))

(define-public crate-owasm-kit-0.2.0 (c (n "owasm-kit") (v "0.2.0") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "07dnlpwy4sl2ipyw43gxgs55ybsv1z8zqrkw7flnaz5z8pdziicb")))

(define-public crate-owasm-kit-0.2.2 (c (n "owasm-kit") (v "0.2.2") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0dwdlxg3ay806wljkw1jyir00yh325a40f23gpysmz8ffv8ax812")))

(define-public crate-owasm-kit-0.3.0 (c (n "owasm-kit") (v "0.3.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0vc4r91q4x17rxl708ikaqh9p16fh2jvjl193jwhnq5l21b5mydb")))

(define-public crate-owasm-kit-0.3.1 (c (n "owasm-kit") (v "0.3.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0w7ybzhzkvzjakwdzjp2wcj4khx647kbjgrz1nvljgfg23iz7abk")))

