(define-module (crates-io ow as owasm-debug) #:use-module (crates-io))

(define-public crate-owasm-debug-0.1.0 (c (n "owasm-debug") (v "0.1.0") (d (list (d (n "owasm-std") (r "^0.20.1") (d #t) (k 0)))) (h "1n9kh3rrqxrkkchnmlsvida128a83jj47hq63nw1njw4307i9njf")))

(define-public crate-owasm-debug-0.1.1 (c (n "owasm-debug") (v "0.1.1") (d (list (d (n "owasm-std") (r "^0.20.1") (d #t) (k 0)))) (h "1z52780hqc7vq90m6kwp8dsyxzb072hnkcfavc4zm05rsz4xz7gf")))

