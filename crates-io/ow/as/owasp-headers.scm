(define-module (crates-io ow as owasp-headers) #:use-module (crates-io))

(define-public crate-owasp-headers-0.1.0 (c (n "owasp-headers") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "19bhllyqhxard5k9fksjyhmbpxwc2311sr4lgqxb2iqdvdfv2g0q")))

(define-public crate-owasp-headers-0.1.1 (c (n "owasp-headers") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0v8l02lm0hfasgpdkghpj2h19k4ahgzj3c9nd6rhbzf3a85g8fdq")))

(define-public crate-owasp-headers-0.1.2 (c (n "owasp-headers") (v "0.1.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "1v4vsrsy5akipdd3rz5n0qdk10rzv329jsskbbczvrwgjqfvh5bg")))

