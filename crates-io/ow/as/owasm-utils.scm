(define-module (crates-io ow as owasm-utils) #:use-module (crates-io))

(define-public crate-owasm-utils-0.1.0 (c (n "owasm-utils") (v "0.1.0") (h "0ls9sahm0mvxkcgclicg0m73kpc59k32pgb0m1iq8v332k99kmj7")))

(define-public crate-owasm-utils-0.6.0 (c (n "owasm-utils") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "parity-wasm") (r "^0.31.3") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "wabt") (r "^0.2") (d #t) (k 2)))) (h "1pxjmx6nggvfnxnqzxv3wpxb8573k7fzl067j4a015qivzwby45a") (f (quote (("std" "parity-wasm/std" "log/std" "byteorder/std") ("default" "std"))))))

