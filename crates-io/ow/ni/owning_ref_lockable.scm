(define-module (crates-io ow ni owning_ref_lockable) #:use-module (crates-io))

(define-public crate-owning_ref_lockable-0.4.2 (c (n "owning_ref_lockable") (v "0.4.2") (d (list (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0yydz8zkvb8vk9nymdqcqnrmvzsas5f2l5j3k2wqhg7vba043rdn")))

