(define-module (crates-io ow ni owning_ref_async) #:use-module (crates-io))

(define-public crate-owning_ref_async-0.4.3 (c (n "owning_ref_async") (v "0.4.3") (d (list (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "0if4rxi9if37cjhcnysh5j04r4j9lq3gwrz6w546vwgkv36wa08k") (f (quote (("async")))) (y #t)))

(define-public crate-owning_ref_async-0.4.4 (c (n "owning_ref_async") (v "0.4.4") (d (list (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "1n978h825iljpm18mc21r0v5w2kgfdccbljwqjqai3cqv5j5g1ri") (f (quote (("async"))))))

(define-public crate-owning_ref_async-0.4.5 (c (n "owning_ref_async") (v "0.4.5") (d (list (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "0821289lqyp04lf2gb0rvm5n5kzh2fccgjxklba92qcgkv3n9vgc") (f (quote (("async"))))))

