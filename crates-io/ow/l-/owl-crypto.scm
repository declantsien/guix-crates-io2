(define-module (crates-io ow l- owl-crypto) #:use-module (crates-io))

(define-public crate-owl-crypto-0.1.0 (c (n "owl-crypto") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.24.0") (f (quote ("std" "recovery"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)) (d (n "un-prim") (r "^0.1") (d #t) (k 0)))) (h "1hldza1d94c2p63qi24m0dmm13sd2d2a4s9w3a625w4ps6axgcvw")))

(define-public crate-owl-crypto-0.1.2 (c (n "owl-crypto") (v "0.1.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.24.0") (f (quote ("std" "recovery"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)) (d (n "un-prim") (r "^0.1") (d #t) (k 0)))) (h "1rvr3ns32i4zjrv7kbaxgy2y5vwshdgc80wx650pawknir4nq0bn")))

(define-public crate-owl-crypto-0.1.3 (c (n "owl-crypto") (v "0.1.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.24.0") (f (quote ("std" "recovery"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)) (d (n "un-prim") (r "^0.1") (d #t) (k 0)))) (h "0wnmzg31dzl87zbimq89ibrk0r6wxh4ar8lkhidgkhv038nk55z6")))

(define-public crate-owl-crypto-0.1.4 (c (n "owl-crypto") (v "0.1.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.24.0") (f (quote ("std" "recovery"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)) (d (n "un-prim") (r "^0.1") (d #t) (k 0)))) (h "0w1alh6hcy4yivh4920vpbypsasyc6wwbwcjfrbqnlmagmi5cyhc")))

