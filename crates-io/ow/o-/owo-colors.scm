(define-module (crates-io ow o- owo-colors) #:use-module (crates-io))

(define-public crate-owo-colors-0.0.0 (c (n "owo-colors") (v "0.0.0") (h "0pq0h9xyzzgiwcnddvp0yjd9l67326jib5mnd9k3m88lik952ib9")))

(define-public crate-owo-colors-1.0.0 (c (n "owo-colors") (v "1.0.0") (h "1l05d21v6ql44sn1304hqyq4z3aq34k0x5w765f4irv84af299bs")))

(define-public crate-owo-colors-1.0.1 (c (n "owo-colors") (v "1.0.1") (h "0308bxwipy6h8zs80gjsnirigl22g4kxg5rn1xjszyf0r3iiv5zi")))

(define-public crate-owo-colors-1.0.2 (c (n "owo-colors") (v "1.0.2") (h "0k9n091pkbdqifikly2fdj7il6d2m433b7di4k1ahf026vyh2h2d")))

(define-public crate-owo-colors-1.0.3 (c (n "owo-colors") (v "1.0.3") (h "0hxk065pkp1ik1cgandxg0khdxr7kdkp5l368n45fjbp36081dl4")))

(define-public crate-owo-colors-1.1.0 (c (n "owo-colors") (v "1.1.0") (h "19188218dsm102pn0diri2z7n5hpvrcqcgnjk4zgma257sbrjqnx") (f (quote (("custom"))))))

(define-public crate-owo-colors-1.1.1 (c (n "owo-colors") (v "1.1.1") (h "1x4qy4ajyy0r3y7h06yd9gqi13qln8adkkqci38wa1xxx5rb9zzi") (f (quote (("custom")))) (y #t)))

(define-public crate-owo-colors-1.1.2 (c (n "owo-colors") (v "1.1.2") (h "1j8jr1zify92lbjqqlxnbh005ky4j6nci4xyl58f46qr5b53nxwl") (f (quote (("custom")))) (y #t)))

(define-public crate-owo-colors-1.1.3 (c (n "owo-colors") (v "1.1.3") (h "1wqh8icl30ikfw1zc9qslh0gqcgri4lyhnibajyzdvh3s76m04ks") (f (quote (("custom"))))))

(define-public crate-owo-colors-1.2.0 (c (n "owo-colors") (v "1.2.0") (h "1jzrarp0palx4rglb2ysh415r5qs5mvhj6dnq5n5z40mfhd8kb15") (f (quote (("custom"))))))

(define-public crate-owo-colors-1.2.1 (c (n "owo-colors") (v "1.2.1") (h "0yxaqcfigw9msr82syimn16419nwyjs916xn3dq2jhj78jp0sdqk") (f (quote (("custom"))))))

(define-public crate-owo-colors-1.3.0 (c (n "owo-colors") (v "1.3.0") (d (list (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0mbs3z0c6p48wh96paa230xf6c6h8nhyyk1d118pybqwx7mv91i3") (f (quote (("tty" "atty") ("custom"))))))

(define-public crate-owo-colors-1.4.0 (c (n "owo-colors") (v "1.4.0") (d (list (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0jq7ppinx7hlf01nlwxj8448b2y5d80hgpj6kawz23g7kb03gsx9") (f (quote (("tty" "atty") ("custom")))) (y #t)))

(define-public crate-owo-colors-1.4.1 (c (n "owo-colors") (v "1.4.1") (d (list (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0yl4ps6nis7f9c9zg5189fs9yn31qw20rw1sa3cym15lkxwrj3nn") (f (quote (("tty" "atty") ("custom")))) (y #t)))

(define-public crate-owo-colors-1.4.2 (c (n "owo-colors") (v "1.4.2") (d (list (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0rybl2lvhaycpkpaq45099idp5ny7nv4sqsafz0cvfqw1wjfy9vz") (f (quote (("tty" "atty") ("custom")))) (y #t)))

(define-public crate-owo-colors-2.0.0 (c (n "owo-colors") (v "2.0.0") (d (list (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0zsdyvaw0k971p7y9fv806q9misrvcalf575rsfcq21b6yzl7zpj") (f (quote (("tty" "atty") ("custom"))))))

(define-public crate-owo-colors-2.1.0-beta.0 (c (n "owo-colors") (v "2.1.0-beta.0") (d (list (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "supports-color") (r "^1.1") (o #t) (d #t) (k 0)))) (h "163q9w2mqkjkl2xh46ky19rhcw57r9ww08zpqkalifdgnadj65yd") (f (quote (("tty" "atty") ("supports-colors" "supports-color") ("custom"))))))

(define-public crate-owo-colors-2.1.0 (c (n "owo-colors") (v "2.1.0") (d (list (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "supports-color") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0z2j9vlajrg65j5pc8nsp7zwdbqzl2hs64iqnayhmi5f4mcpcq9s") (f (quote (("tty" "atty") ("supports-colors" "supports-color") ("custom"))))))

(define-public crate-owo-colors-3.0.0-beta.0 (c (n "owo-colors") (v "3.0.0-beta.0") (d (list (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "supports-color") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1wqnf6i7c7wimf3n03dcnsgxczrs0rvm9ch60krsx2bfbcyi0q9f") (f (quote (("tty" "atty") ("supports-colors" "supports-color") ("custom"))))))

(define-public crate-owo-colors-3.0.0-beta.1 (c (n "owo-colors") (v "3.0.0-beta.1") (d (list (d (n "supports-color") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0hy4in0s0mg09hpq4q3srn3njqnq23s98xg4lc7vr60l4q51fig6") (f (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3.0.0-beta.2 (c (n "owo-colors") (v "3.0.0-beta.2") (d (list (d (n "supports-color") (r "^1.3") (o #t) (d #t) (k 0)))) (h "02ykfh2zbaipn2jrsbpazjvcnl4f1rh4i1036pq1x7qnkjr9ax7v") (f (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3.0.0 (c (n "owo-colors") (v "3.0.0") (d (list (d (n "supports-color") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0629qds6qvl1vs4k558l9rcsdn2858n6lprfbz4pp2by9by11sng") (f (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3.0.1 (c (n "owo-colors") (v "3.0.1") (d (list (d (n "supports-color") (r "^1.3") (o #t) (d #t) (k 0)))) (h "04wyq17g0rb895ml3gxqilmwa5c2z4fpafn9mnwfyv0q6ik1lfjh") (f (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3.1.0 (c (n "owo-colors") (v "3.1.0") (d (list (d (n "supports-color") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1zvg7wkjm7f81r0hvwf56cdn1kdfpxl4zbxpmg6528yw5hi6vbgr") (f (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3.1.1 (c (n "owo-colors") (v "3.1.1") (d (list (d (n "supports-color") (r "^1.3") (o #t) (d #t) (k 0)))) (h "121b75wgs500pbldq9frma728krl19wh7asxg9r72hbybsh1qfwd") (f (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3.2.0 (c (n "owo-colors") (v "3.2.0") (d (list (d (n "supports-color") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1x7b8kf3854zlix6cpai065fjrdgfil4gq5v2pmfc17cg3b8yi10") (f (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3.3.0 (c (n "owo-colors") (v "3.3.0") (d (list (d (n "supports-color") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1sq3i8g5haj6xy56ln1ggm8waazzkkfj6272i8y9kl70g02y6wjy") (f (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3.4.0 (c (n "owo-colors") (v "3.4.0") (d (list (d (n "supports-color") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0yx8glp9vipa6gybbqmpg8lyrcsrv9z6dia94p5lvshzja0p7kyy") (f (quote (("supports-colors" "supports-color") ("alloc"))))))

(define-public crate-owo-colors-3.5.0 (c (n "owo-colors") (v "3.5.0") (d (list (d (n "supports-color") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0vyvry6ba1xmpd45hpi6savd8mbx09jpmvnnwkf6z62pk6s4zc61") (f (quote (("supports-colors" "supports-color") ("alloc")))) (r "1.51")))

(define-public crate-owo-colors-3.6.0 (c (n "owo-colors") (v "3.6.0") (d (list (d (n "supports-color") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0943lynkwz1glq3w7m9anv73lnrhd8yabs09krbh49g1wz4lxp39") (f (quote (("supports-colors" "supports-color") ("alloc")))) (y #t) (r "1.51")))

(define-public crate-owo-colors-4.0.0-rc.0 (c (n "owo-colors") (v "4.0.0-rc.0") (d (list (d (n "supports-color") (r "^2.0") (o #t) (d #t) (k 0)))) (h "18jz0vwk60dzpg92j8862bd580wm56fdj89xs8zabyjkxrxszbkh") (f (quote (("supports-colors" "supports-color") ("alloc")))) (r "1.56")))

(define-public crate-owo-colors-4.0.0-rc.1 (c (n "owo-colors") (v "4.0.0-rc.1") (d (list (d (n "supports-color") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0jhvab83rxp2m4i0s9p0b95wfwdy1gjvsaq5064s5b5qhaj2xvkh") (f (quote (("supports-colors" "supports-color") ("alloc")))) (r "1.56")))

(define-public crate-owo-colors-4.0.0 (c (n "owo-colors") (v "4.0.0") (d (list (d (n "supports-color") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0grsk47cllj0s4nc4qxvy4gdhj2lyiglbqx4lmw2m7grdmq59zya") (f (quote (("supports-colors" "supports-color") ("alloc")))) (r "1.56")))

