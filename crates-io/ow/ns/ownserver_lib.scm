(define-module (crates-io ow ns ownserver_lib) #:use-module (crates-io))

(define-public crate-ownserver_lib-0.5.0 (c (n "ownserver_lib") (v "0.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "104z005wfqc1wm2qrnc5hf06yhah15l4himfkrcygi3dmqr0w31y")))

(define-public crate-ownserver_lib-0.5.1 (c (n "ownserver_lib") (v "0.5.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "092gjnx1awiq5ld9whl0gpmgs0rs8d4i5rmljxg4jqsmm0nm21b8")))

(define-public crate-ownserver_lib-0.6.0 (c (n "ownserver_lib") (v "0.6.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1yds3jvx7325j4wrhnpnz1qvi3pkdi0sp1n9s8vidpbm9qpxdvr8")))

