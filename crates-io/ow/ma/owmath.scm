(define-module (crates-io ow ma owmath) #:use-module (crates-io))

(define-public crate-owmath-0.0.1 (c (n "owmath") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "066w03s7jrsjcn9caps4lms95gc70d0ncg10pbajqj9cin31qz2c")))

(define-public crate-owmath-0.0.2 (c (n "owmath") (v "0.0.2") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0d9pyqi60amq07yq8mgac3qgm4rvhqnij2zgdkbf7mh8s2wsr3yq")))

(define-public crate-owmath-0.0.3 (c (n "owmath") (v "0.0.3") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "11xrrzyn6s0453lpki961i4ldgz0j5ka1pqi4z47lpq7dcd24fc0")))

(define-public crate-owmath-0.0.4 (c (n "owmath") (v "0.0.4") (d (list (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "1dqm17l9d0igj3acgr94f1hvlhiphc88aqi408d0vf0vfccfil8d")))

(define-public crate-owmath-0.0.5 (c (n "owmath") (v "0.0.5") (d (list (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0idqmpyry5qzgi7h5dl3mga47c2r52bgwbfbqm4xwy2b9ayxxjzb")))

(define-public crate-owmath-0.0.7 (c (n "owmath") (v "0.0.7") (d (list (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0qyg77zqpzj4k22xy98kav8vjvf5jlams3wf5vg5wfbh27ikl6n8")))

(define-public crate-owmath-0.0.8 (c (n "owmath") (v "0.0.8") (d (list (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)))) (h "0k5x6xjp7826ihhgyv74j2xp8v1hs8gxnr5bg0kcyfa4hhf8kymy")))

