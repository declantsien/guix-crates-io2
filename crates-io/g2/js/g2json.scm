(define-module (crates-io g2 js g2json) #:use-module (crates-io))

(define-public crate-g2json-0.1.0 (c (n "g2json") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "deno_ast") (r "^0.27") (f (quote ("transpiling"))) (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "run_script") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 0)))) (h "0099q8ybzsvy3yyh0awvxa9rbhvspm1rj1n2vlygv845xlqiy5jl")))

