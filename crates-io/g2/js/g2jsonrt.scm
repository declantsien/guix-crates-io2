(define-module (crates-io g2 js g2jsonrt) #:use-module (crates-io))

(define-public crate-g2jsonrt-0.1.0 (c (n "g2jsonrt") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (f (quote ("macros" "json" "ws" "headers"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.20") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tungstenite") (r "^0.20") (d #t) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "05jg1gfql683sc92jgq5sazgzv0y6cq82r931dbjywpk61nc4b8n")))

