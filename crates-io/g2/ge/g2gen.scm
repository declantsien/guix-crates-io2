(define-module (crates-io g2 ge g2gen) #:use-module (crates-io))

(define-public crate-g2gen-0.1.0 (c (n "g2gen") (v "0.1.0") (d (list (d (n "g2poly") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1mvybkl6il20b7zznd89w4jb3r57q06c0n9bxvhk39z88ly6b0p3")))

(define-public crate-g2gen-0.2.0 (c (n "g2gen") (v "0.2.0") (d (list (d (n "g2poly") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0i8n5b2sqmazyjhljd63m4r2359bc8q9j6hfy6zj93cs0r23vp9s")))

(define-public crate-g2gen-0.2.1 (c (n "g2gen") (v "0.2.1") (d (list (d (n "g2poly") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0nxib3wyvsg9bnmdw6qf9j0l3n0rxajq8hywl7y8iv354vry5g0s")))

(define-public crate-g2gen-0.4.0 (c (n "g2gen") (v "0.4.0") (d (list (d (n "g2poly") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0m8590wqm57p4dik77s5x8lcydas9kwkzllc71f8r033djqh1h9g")))

(define-public crate-g2gen-1.0.0 (c (n "g2gen") (v "1.0.0") (d (list (d (n "g2poly") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "057zd470k24rp9k7s9sbrmk0zy8m63jgnvz8q6z51pw4hii3jdrb")))

(define-public crate-g2gen-1.0.1 (c (n "g2gen") (v "1.0.1") (d (list (d (n "g2poly") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "03l243pnj9pabd0pi0jcf4fyrxxhddx8ixv31gchs9gwn8jpcb7w")))

