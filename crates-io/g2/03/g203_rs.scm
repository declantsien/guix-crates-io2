(define-module (crates-io g2 #{03}# g203_rs) #:use-module (crates-io))

(define-public crate-g203_rs-0.1.0 (c (n "g203_rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "1xgmcpw3rjl3lyyscagr34cd26y6w7gjazgqc39qvkqwhbgwg3hs")))

(define-public crate-g203_rs-0.1.1 (c (n "g203_rs") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "1jwm4jjp6z6n571068xjnd04dbmpblaxha8i7i3gl0amxj9ma97j")))

