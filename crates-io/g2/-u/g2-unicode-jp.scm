(define-module (crates-io g2 -u g2-unicode-jp) #:use-module (crates-io))

(define-public crate-g2-unicode-jp-0.4.0 (c (n "g2-unicode-jp") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)))) (h "0qjfmmpsgf11jm61wqa1wrqk4z1hl8ci28kza0fvzbqh3n3f51g6")))

(define-public crate-g2-unicode-jp-0.4.1 (c (n "g2-unicode-jp") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)))) (h "1fsh282515368n7zph9lr0vwlw4ss1crynhl5cd4kw9zmhdazjd2")))

