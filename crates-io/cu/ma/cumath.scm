(define-module (crates-io cu ma cumath) #:use-module (crates-io))

(define-public crate-cumath-0.1.0 (c (n "cumath") (v "0.1.0") (h "1fjm5n3aqfmim08h5w40m0f64rb99m6cnwp7rh0aw3xb9762js01") (y #t)))

(define-public crate-cumath-0.1.1 (c (n "cumath") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "0ak5nqg3yap86pirpmd2wfcy47zj0wa7hh2kgfya6l7zi2xxgl00") (y #t)))

(define-public crate-cumath-0.1.11 (c (n "cumath") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "1x47ihl39z90aj3d6a4zrhdp55mmavckvfcjs1mpb834lzynvcjk") (y #t)))

(define-public crate-cumath-0.0.1 (c (n "cumath") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "1bq0jhpqrkdgpg1rn2wlbjqvdc9jsrm14xwq535z2qzr4i5730r0") (y #t)))

(define-public crate-cumath-0.0.2 (c (n "cumath") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "1xk58rapsfi2vm5spbx4892smkb5jg6cj2xhq26afxv11lby1zrx") (y #t)))

(define-public crate-cumath-0.2.0 (c (n "cumath") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "0lbfijg7vrr37dxk0g3frd8c92hgh5593r8l4lksqrznp0z16jd9")))

(define-public crate-cumath-0.2.1 (c (n "cumath") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "13nrpqd4ii7rb0z85kwj6fc82x6w5xiscd1hznm9z2p5lx58zq37") (f (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2.2 (c (n "cumath") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "04qvc7pim635b4x4rngcyzzmygq4br1ar3vjjjnp41wagci5652z") (f (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2.3 (c (n "cumath") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ihynnfnkrba6m3mrpfc86nqqvymfpdaxx39nya6890jmjkp9aav") (f (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2.4 (c (n "cumath") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1djgf2jbb8a6zzzh6b4b38bfn5rn5n0q6wcilzs5hm88lbb4r4gw") (f (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2.5 (c (n "cumath") (v "0.2.5") (d (list (d (n "cc") (r "^1.0.15") (d #t) (k 1)))) (h "0wrkr91h2gafs64xp41pbjjizhz6c1245qv66zvw04ddmh9ilkxb") (f (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2.6 (c (n "cumath") (v "0.2.6") (d (list (d (n "cc") (r "^1.0.15") (d #t) (k 1)))) (h "024iigs8njg1r53xvdc68qf7ad4rzqhwwrbmrhm21j1gjnbx7400") (f (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2.7 (c (n "cumath") (v "0.2.7") (d (list (d (n "cc") (r "^1.0.15") (d #t) (k 1)))) (h "1qni0nxpawrvdnsff4nd8l02pls0vl4vm12f9zq2irlsbv55n9vh") (f (quote (("disable_checks") ("default"))))))

