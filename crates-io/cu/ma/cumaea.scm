(define-module (crates-io cu ma cumaea) #:use-module (crates-io))

(define-public crate-cumaea-0.1.0 (c (n "cumaea") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0sd0gi184flx0r3iijl3y56671wxr97w1q75whc43d574dajz9dp")))

(define-public crate-cumaea-0.1.1 (c (n "cumaea") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0fl2d35n7xkmg88p3b1p4my4ih3k6ja72linh79rq4991vdb3w7h")))

