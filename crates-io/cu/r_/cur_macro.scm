(define-module (crates-io cu r_ cur_macro) #:use-module (crates-io))

(define-public crate-cur_macro-0.1.0 (c (n "cur_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xv0k53bic9pnld15ldka9xj7xsk44zijswv1z6bbi1p2agwssxk")))

(define-public crate-cur_macro-0.2.0 (c (n "cur_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fix9xvwjcxl6mqcl2j1mrvfzzb12svnjnb94dki3ldpg5v5nd5f")))

(define-public crate-cur_macro-0.3.0 (c (n "cur_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04awwgz8znsdw0z61a8k4hw6pilp9zgaascbvxyj7qvv5gmc7bzy")))

(define-public crate-cur_macro-0.4.0 (c (n "cur_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1v9mcw71zihrp6rl1g4xjf4yl0hhjv5dhn931sls433sfv2vm90j")))

(define-public crate-cur_macro-0.5.0 (c (n "cur_macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pzwdrd1hpav2p963zv8lybvw09mdzh1smkzrzfhmpgsjyp1q7v5")))

