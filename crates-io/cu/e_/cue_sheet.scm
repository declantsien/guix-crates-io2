(define-module (crates-io cu e_ cue_sheet) #:use-module (crates-io))

(define-public crate-cue_sheet-0.1.0 (c (n "cue_sheet") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "15241n19cjrlmjn1g7xkxac4cz1r5wz7z3v8s53gy1rdd9bln2hf")))

(define-public crate-cue_sheet-0.2.0 (c (n "cue_sheet") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "02dggq8yc9gv72mgrbf83a4v076v6yh1mggs35hwda935d984vfh")))

(define-public crate-cue_sheet-0.3.0 (c (n "cue_sheet") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)))) (h "096vd0hdlkgq4mvizgbj8zmncx1isvknp01qm1405mcxk3gnr5ha")))

