(define-module (crates-io cu ad cuadra) #:use-module (crates-io))

(define-public crate-cuadra-0.0.1 (c (n "cuadra") (v "0.0.1") (h "098gd3drvys9qxkv2b8lnchv1f1irq46xic8di4bx8drni9fd580") (r "1.61.0")))

(define-public crate-cuadra-0.0.2 (c (n "cuadra") (v "0.0.2") (h "1inbcs7k2byfb8jxijka52sg82ijqzbn47zcwagf0rrsfby91cvq") (r "1.61.0")))

(define-public crate-cuadra-0.1.0 (c (n "cuadra") (v "0.1.0") (h "1wadhw3msrcss7dj9sfhsp0s1mz4hzf5kzdw0b2jnazy42xqjgb2") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-cuadra-0.2.0 (c (n "cuadra") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1ihy5hzs8idcrwapl3hzkqx7nl6439fmbc690w6916p7zg91277i") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-cuadra-0.3.0 (c (n "cuadra") (v "0.3.0") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1ly8bmhar5bg469rxlz5pqz5q6p4n3blvzkkj6p66y2flmza2777") (f (quote (("std") ("nightly") ("default" "std")))) (r "1.64.0")))

(define-public crate-cuadra-0.3.1 (c (n "cuadra") (v "0.3.1") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "00mnc5db88nydpdnx5xz8md9d7m3v984y8mhjr9agvxqak4n2q0l") (f (quote (("std") ("nightly") ("default" "std")))) (r "1.64.0")))

