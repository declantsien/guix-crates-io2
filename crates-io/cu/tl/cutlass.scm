(define-module (crates-io cu tl cutlass) #:use-module (crates-io))

(define-public crate-cutlass-0.1.0 (c (n "cutlass") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a32032x7vz5qic9ky401blbv42jy0nbjqdn6ffrp99x6w07w7xs")))

