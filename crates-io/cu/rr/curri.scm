(define-module (crates-io cu rr curri) #:use-module (crates-io))

(define-public crate-curri-0.1.0 (c (n "curri") (v "0.1.0") (d (list (d (n "bevy_utils") (r "^0.12.1") (d #t) (k 0)))) (h "0m5wb110767j5gnmjw9fky771s2r6wk6sl3das6zasbjr3cl6khl")))

(define-public crate-curri-0.1.1 (c (n "curri") (v "0.1.1") (d (list (d (n "bevy_utils") (r "^0.12.1") (d #t) (k 0)))) (h "1faxrsn8pslm6a4i78v9wlz290p3kwjcnky6vvw77vy5ijyw5xnq")))

(define-public crate-curri-0.1.2 (c (n "curri") (v "0.1.2") (h "1smfjl6pyhr0h043px3l2rw15mjwxp1qiayl5q8hkc23vqmq519n")))

