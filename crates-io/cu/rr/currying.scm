(define-module (crates-io cu rr currying) #:use-module (crates-io))

(define-public crate-currying-0.1.0 (c (n "currying") (v "0.1.0") (d (list (d (n "tupleops") (r "^0.1.1") (f (quote ("concat"))) (d #t) (k 0)))) (h "1wzn24xax19adv46sq34qikbc9mkppaa0fdvlh94baqgb9nk80xx")))

(define-public crate-currying-0.2.0 (c (n "currying") (v "0.2.0") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)) (d (n "tupleops") (r "^0.1.1") (f (quote ("concat"))) (d #t) (k 0)))) (h "09j6v3vx675m0g0ap57mfsiacvavi3g9ddba24nkkmdhdarf5n65")))

(define-public crate-currying-0.2.1 (c (n "currying") (v "0.2.1") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)) (d (n "tupleops") (r "^0.1.1") (f (quote ("concat"))) (d #t) (k 0)))) (h "0hzv4imrzrwda0iixfz9m29b0dj82mf95mrwh3fybvzkb8zz6m7h")))

(define-public crate-currying-0.2.2 (c (n "currying") (v "0.2.2") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "tupleops") (r "^0.1.1") (f (quote ("concat"))) (d #t) (k 0)))) (h "1hb3bq072x3vr1h6wck68fwbi9bwz6glvg4gxv4qqs0v7a6hlbf6")))

