(define-module (crates-io cu rr currycompose) #:use-module (crates-io))

(define-public crate-currycompose-0.1.0 (c (n "currycompose") (v "0.1.0") (d (list (d (n "tuple_split") (r "^0.1.1") (d #t) (k 0)) (d (n "tupleops") (r "^0.1.1") (f (quote ("concat"))) (d #t) (k 0)))) (h "06240j04ff5savnnn7si7cn4v1104i3n9pggc2jf8mhjplni5kda")))

