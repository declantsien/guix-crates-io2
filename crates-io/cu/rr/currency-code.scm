(define-module (crates-io cu rr currency-code) #:use-module (crates-io))

(define-public crate-currency-code-0.0.0 (c (n "currency-code") (v "0.0.0") (h "13dxxzqm1nafg2rhm2xjwbcgv4nzmyhlgfnilykzj6grqskyb4wn")))

(define-public crate-currency-code-0.1.0 (c (n "currency-code") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "15lq8r9734dqiymf7317yab0x4gm0ljx5z3fpcwxnz4i7db231k1") (f (quote (("std") ("default" "std"))))))

(define-public crate-currency-code-0.2.0 (c (n "currency-code") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "impl-macros") (r "^0.1.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1g17fwp41868szwc9agmd2bhsd14jpd38x354a0hbzfnyg1kx2xk") (f (quote (("std") ("default" "std"))))))

(define-public crate-currency-code-0.3.0 (c (n "currency-code") (v "0.3.0") (d (list (d (n "impl-macros") (r "^0.1.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0fa3741ms9hbbgjy33nm39w071q3ipgrq8dfffap83xwhcgfj84d") (f (quote (("std") ("default" "std"))))))

