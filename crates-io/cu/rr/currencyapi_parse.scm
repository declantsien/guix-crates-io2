(define-module (crates-io cu rr currencyapi_parse) #:use-module (crates-io))

(define-public crate-currencyapi_parse-0.1.0 (c (n "currencyapi_parse") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.32") (o #t) (d #t) (k 0)))) (h "1sbs3c27mv6sn9kw7ifl2782kk78srcx2a76bj0jjbrjpg3jbgr4") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:thiserror"))))))

(define-public crate-currencyapi_parse-0.1.1 (c (n "currencyapi_parse") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.32") (o #t) (d #t) (k 0)))) (h "1y9r0gfjicl5id516jvlvglb99yxy51wiy82m0c0g3wcnmr8hljb") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:thiserror"))))))

