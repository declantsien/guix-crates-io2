(define-module (crates-io cu rr currencies) #:use-module (crates-io))

(define-public crate-currencies-0.0.1 (c (n "currencies") (v "0.0.1") (h "17bqv4yfr14kch696dmsbqzif6zlx40ak1ng68ydnvxylhdp2c0a")))

(define-public crate-currencies-0.1.0 (c (n "currencies") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12.2") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "uint") (r "^0.9.5") (d #t) (k 0)))) (h "07wwvrnr03wfnf6955yikizdm6c2g9mjfbfg48rq44xnanhdvwlq")))

(define-public crate-currencies-0.1.1 (c (n "currencies") (v "0.1.1") (d (list (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12.2") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "uint") (r "^0.9.5") (d #t) (k 0)))) (h "0809qvavb9anq46k9pj6zlfq9bqi2wwyjyfnjbcm0w9k5mj965sv")))

(define-public crate-currencies-0.1.2 (c (n "currencies") (v "0.1.2") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "1pwg37f8n1bzsvc5lfm5bhc0kp5vc4fdvrwg274irzrbiyxwdkki")))

(define-public crate-currencies-0.1.3 (c (n "currencies") (v "0.1.3") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "1i8ynwmsachxs0s99qjfzg25yfc0c9k3i3sd546gwmk1vk4ify16")))

(define-public crate-currencies-0.2.0 (c (n "currencies") (v "0.2.0") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "0fghrfxl49wy6m7rwpyrbz8jckbw92z83pi9la6rliygvsc8yfhc") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-currencies-0.3.0 (c (n "currencies") (v "0.3.0") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "quoth") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "0hlaq00zy9g6f51blnd2d52m9310dgrrrqsqyxpdk59wsnn1l9by") (f (quote (("std") ("default" "parsing")))) (s 2) (e (quote (("serde" "dep:serde") ("parsing" "serde" "std" "dep:quoth"))))))

(define-public crate-currencies-0.3.1 (c (n "currencies") (v "0.3.1") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "quoth") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "1pdk8wxhl7ym7lcb6dr1sm8y9i21bgrprz5l3mqnjan96ihc1gih") (f (quote (("std") ("default" "parsing")))) (s 2) (e (quote (("serde" "dep:serde") ("parsing" "serde" "std" "dep:quoth"))))))

(define-public crate-currencies-0.3.2 (c (n "currencies") (v "0.3.2") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "quoth") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "18y0knrcrgyb8gaxrz1gvzqjvf29bz8b85g6bijzdwhx1kcl076r") (f (quote (("std") ("default" "parsing")))) (s 2) (e (quote (("serde" "dep:serde") ("parsing" "serde" "std" "dep:quoth"))))))

