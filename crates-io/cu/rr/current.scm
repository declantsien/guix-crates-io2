(define-module (crates-io cu rr current) #:use-module (crates-io))

(define-public crate-current-0.0.4 (c (n "current") (v "0.0.4") (h "0a0ig45ny6dv0b69m1g79aagbplmhg8aicrz51lncapm58dds8c0")))

(define-public crate-current-0.0.5 (c (n "current") (v "0.0.5") (h "0pqhmsvpqld4xpxkjm2j2xydfsah75dcfx3a5mbb3008fbivrqjs")))

(define-public crate-current-0.0.6 (c (n "current") (v "0.0.6") (h "0yjh281m8q7w7j1xzc82r59k87r7mibcz651dnww4imv355a3ps5")))

(define-public crate-current-0.0.7 (c (n "current") (v "0.0.7") (h "0s2vpq1qc2n0mvrpzbfidkyiibi1ryi6bfc5ap51g5c59jxj2f5l")))

(define-public crate-current-0.0.10 (c (n "current") (v "0.0.10") (h "0q4v1s5kyka1bc4n6kcb0m14vpy944nsyy793dvm3wjjgmn2wbvd")))

(define-public crate-current-0.1.0 (c (n "current") (v "0.1.0") (h "0i2r1fqky0pya3fs12c8x53ygrmjswkmfnwmv6wq9m3gqw6mf9wc")))

(define-public crate-current-0.1.1 (c (n "current") (v "0.1.1") (h "1k9bh7bz3p2ylrkgmw42s01wmxm5j7xj9r1yyp1w7nw40mfsargc")))

(define-public crate-current-0.1.2 (c (n "current") (v "0.1.2") (h "1w0nk7lc19qjdl3gkwixsh3yszsynw2m98v1skcdhc143flf1aij") (f (quote (("unstable") ("default"))))))

