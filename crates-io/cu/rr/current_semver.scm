(define-module (crates-io cu rr current_semver) #:use-module (crates-io))

(define-public crate-current_semver-0.1.0 (c (n "current_semver") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p2d0q5l4mi546cd8l2wzh00xv9mlc24w72l0ybcd2j8gm9k4fqa") (y #t)))

(define-public crate-current_semver-0.1.1 (c (n "current_semver") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zj9n0x3r1kg5l4p6kc21xizjzphpvsf8ggc519nd9rmmqk0bik1")))

