(define-module (crates-io cu rr currency-token-client) #:use-module (crates-io))

(define-public crate-currency-token-client-0.1.3 (c (n "currency-token-client") (v "0.1.3") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-cron") (r "^0.2.8") (d #t) (k 0)) (d (n "ic-event-hub") (r "^0.1.10") (d #t) (k 0)) (d (n "ic-event-hub-macros") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0mxi8la4jwnrx618kqcnk9b00spgj5ha2xp8qvcy4099sa6vwf17")))

