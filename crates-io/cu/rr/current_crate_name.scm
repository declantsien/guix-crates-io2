(define-module (crates-io cu rr current_crate_name) #:use-module (crates-io))

(define-public crate-current_crate_name-0.1.0 (c (n "current_crate_name") (v "0.1.0") (h "0v5arg5lxmc1vbnx0l0ikdhsxxxd4jfkrynf3l1gw0ddzrdyi0cn") (y #t)))

(define-public crate-current_crate_name-0.1.1 (c (n "current_crate_name") (v "0.1.1") (h "1k3a0i8w0ip4hx33b2axhvjjhajj7v08fl4a4ljbgwpahc9lv0rx") (y #t)))

