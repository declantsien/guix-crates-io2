(define-module (crates-io cu rr currawong_core) #:use-module (crates-io))

(define-public crate-currawong_core-0.1.0 (c (n "currawong_core") (v "0.1.0") (d (list (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1jzwml2fdyzhhfa202si8m0flbkpfbkfc7bwf6kivh6v439y77in") (f (quote (("midi" "midly") ("default" "midi"))))))

(define-public crate-currawong_core-0.2.0 (c (n "currawong_core") (v "0.2.0") (d (list (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1pwff6kkfp8bfl5ir16s29bai8cqdqq18lmsz85q36bdhrgmmz6m") (f (quote (("midi" "midly") ("default" "midi"))))))

(define-public crate-currawong_core-0.3.0 (c (n "currawong_core") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0bg21fjc2yj56pdq0cji57249rai43frnb3l6cg9sf9731r3rdc2") (f (quote (("web" "getrandom/js") ("midi" "midly") ("default" "midi"))))))

(define-public crate-currawong_core-0.4.0 (c (n "currawong_core") (v "0.4.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0asnfbnwjw0fi1a4h64wi83fpd91z4p6gr9xja6ksmdmn1wq1naz") (f (quote (("web" "getrandom/js") ("midi" "midly") ("default" "midi"))))))

(define-public crate-currawong_core-0.4.1 (c (n "currawong_core") (v "0.4.1") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1ak07z0gmnxcy4j3g7sas05gnzpz0qrblgvl1ng56pjvimyciqxz") (f (quote (("web" "getrandom/js") ("midi" "midly") ("default" "midi"))))))

(define-public crate-currawong_core-0.4.2 (c (n "currawong_core") (v "0.4.2") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "06skb2riq62dm58a41vjphq6v46qafq35c03saqr29q4bip8wxw3") (f (quote (("web" "getrandom/js") ("midi" "midly") ("default" "midi"))))))

(define-public crate-currawong_core-0.5.0 (c (n "currawong_core") (v "0.5.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1irm2v9n488zp298m3bay32nqcgvfn4g1zsi087lilqy57n7b9ws") (f (quote (("web" "getrandom/js") ("midi" "midly") ("default" "midi"))))))

