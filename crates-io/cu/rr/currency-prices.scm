(define-module (crates-io cu rr currency-prices) #:use-module (crates-io))

(define-public crate-currency-prices-0.1.0 (c (n "currency-prices") (v "0.1.0") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gj0xfcfcbmn6x23ms343qpwabmy5hvwyccni2d6y8l9c48almh8")))

(define-public crate-currency-prices-0.1.1 (c (n "currency-prices") (v "0.1.1") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nr052qzji7hifmkp6xq3my8sy0nxzvc9ikk1i2hayginm27qy9q")))

(define-public crate-currency-prices-0.1.2 (c (n "currency-prices") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.32") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.33") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1hnw7gnjq4zd5z79csbw9f2dmbg6gmjbqk2fa7id30rj1bl8vh7z")))

