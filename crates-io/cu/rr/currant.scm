(define-module (crates-io cu rr currant) #:use-module (crates-io))

(define-public crate-currant-0.1.0 (c (n "currant") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "1clps2x9r6ma3dvjazb8xp51n72xqkhy12axl5krch4k3xx5i1j1")))

(define-public crate-currant-0.1.1 (c (n "currant") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "11nzbgm1rxqhy3p07msibj6srmp46svwgf95v5n2r9w78hcqiqv9")))

(define-public crate-currant-0.1.2 (c (n "currant") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "054mj4szj4ld0kx1w3s3kl03c6c9rivjs60gf30x0bwp1jkwm0pl")))

(define-public crate-currant-0.1.3 (c (n "currant") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "0rvzm1zi8kp4694l0mrk9ldxb7rn2i3jp64wz5gkylxjmzxnq6js")))

(define-public crate-currant-0.1.4 (c (n "currant") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "0h8vg1jjwy6zf2i4zhz72bhrcmxhjwnzdphxykpa7m3n5935hlsi")))

(define-public crate-currant-0.1.5 (c (n "currant") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "1hbdzjnaphcswsbjmfqdhbzvjrrlrwccq5py3dhh5g9vd0c99sd1")))

(define-public crate-currant-0.1.6 (c (n "currant") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "052rdgx40ynacphp9xnwgzn7impljkjm9ljkkjqzfiyzqmbbr8zn")))

(define-public crate-currant-0.1.7 (c (n "currant") (v "0.1.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "1hlwi4jl7qvq5zdaibg5bjraqvv19wc0g1vz2dinbjp7lgy2cxqi")))

(define-public crate-currant-0.2.1 (c (n "currant") (v "0.2.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "0ivyy0n9qwdq5sv18lbjqpmrcdvr82lbgdd2lrclhbghn1rm5plm")))

(define-public crate-currant-0.2.2 (c (n "currant") (v "0.2.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "1hzlp9xdyjclgbcmybnqgyryjsag90gdiw1y48cylcl8hxphydvm")))

(define-public crate-currant-0.2.3 (c (n "currant") (v "0.2.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "125rcvg75pgdzilrl9djrvbxx97qxj6k38pk0q4z7br94qxkaan6")))

(define-public crate-currant-0.2.4 (c (n "currant") (v "0.2.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "0wqd8a8lz2v70vq1ags5qibicc2sc3r7lxxpdnwhhw1p8c33hn3z")))

(define-public crate-currant-0.2.5 (c (n "currant") (v "0.2.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)))) (h "0z3xhgr2cmbj6lgl3z0cnhjlx3nhif6ndsh7pmfp907lgs5nv7q2")))

(define-public crate-currant-0.2.6 (c (n "currant") (v "0.2.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1h4cnx6xj7f0wqn7dggnvjnxqal68i77gpfiaklggqnqv7vimmgm")))

(define-public crate-currant-0.2.7 (c (n "currant") (v "0.2.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "00npn4iz9xrsfqx95ygvlxg7wj1li2ph0ksklphwm9iy176n7bgj")))

(define-public crate-currant-0.2.8 (c (n "currant") (v "0.2.8") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1r5wj70kn9z08kzka3xwyzkmy7fqhni56bf9rrv309wjkw731d1m")))

