(define-module (crates-io cu rr currency-iso4217) #:use-module (crates-io))

(define-public crate-currency-iso4217-0.1.0 (c (n "currency-iso4217") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (d #t) (k 0)))) (h "0rmnpmh9qvcix5axjwsdsx36cnjgsyrzljbplsj79v1dbj6gykic") (f (quote (("std") ("serde-std" "serde/std" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-currency-iso4217-0.1.1 (c (n "currency-iso4217") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (d #t) (k 0)))) (h "1l01329srhs0gksx490cmjp8j3xjkasv5mgx2wj7jiclnakb8827") (f (quote (("std") ("slow-tests") ("serde-std" "serde/std" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

