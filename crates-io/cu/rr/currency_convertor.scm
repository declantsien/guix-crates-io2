(define-module (crates-io cu rr currency_convertor) #:use-module (crates-io))

(define-public crate-currency_convertor-0.1.0 (c (n "currency_convertor") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "06swmy50bvcwp1gskf5bc17h42z3z460hk8cxrrdis4pb9cc1x5g")))

