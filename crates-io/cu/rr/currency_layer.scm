(define-module (crates-io cu rr currency_layer) #:use-module (crates-io))

(define-public crate-currency_layer-0.1.0 (c (n "currency_layer") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)))) (h "160cx270ixb3adnl8pfindwzsng4aplbpllb0rsxiqplhlh737ys")))

(define-public crate-currency_layer-0.1.1 (c (n "currency_layer") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)))) (h "0sjbn16324cvnf9xl0nfmwr7x6010lpi6znyj86prcw49vg8ji65")))

(define-public crate-currency_layer-0.2.0 (c (n "currency_layer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)) (d (n "rusty-money") (r "^0.4.1") (f (quote ("iso"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1h5sqdv2a6b6j88hp3vb0hcg4p8yp8mlppnmj7n7l19jn2af5ms9")))

