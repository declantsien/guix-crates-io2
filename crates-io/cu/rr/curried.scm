(define-module (crates-io cu rr curried) #:use-module (crates-io))

(define-public crate-curried-0.1.0 (c (n "curried") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)))) (h "1xxnahck5kdr47zha3v99c12c1j4rf5if3n79la47hyxwpddbclh")))

(define-public crate-curried-0.1.1 (c (n "curried") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)))) (h "1f3rjx2i7lb7y36ghvamhn2fvjl532cdp76yr462ssxrgdb8h2ag")))

(define-public crate-curried-0.1.2 (c (n "curried") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6wnd2vzqgcw99g6mrm27w3lvwargrxlr8sjmk5l06gli2dw9m9")))

