(define-module (crates-io cu rr curryrs) #:use-module (crates-io))

(define-public crate-curryrs-0.1.0 (c (n "curryrs") (v "0.1.0") (h "1fzxsa7vgjs19aizbrbkbycc51dim5l7w77p08qd3nv9k7m72adh") (y #t)))

(define-public crate-curryrs-0.1.1 (c (n "curryrs") (v "0.1.1") (h "0vz5iq12p9pzqrk2404ygmn090nck2djmm1rmwhrjrlprsm7xgrc") (y #t)))

(define-public crate-curryrs-0.2.0 (c (n "curryrs") (v "0.2.0") (h "0byipkslqh0509nvc29fmmc83q5mm1q6l8ma6iw76i1i7prd9kzf") (f (quote (("threaded_l") ("threaded_debug") ("threaded")))) (y #t)))

