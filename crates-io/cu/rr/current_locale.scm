(define-module (crates-io cu rr current_locale) #:use-module (crates-io))

(define-public crate-current_locale-0.1.0 (c (n "current_locale") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.82") (d #t) (t "cfg(windows)") (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winnls" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jg226s9p2k7alkj12cynmpgk877gnyqbrn8lngjanifxv17fhlx")))

(define-public crate-current_locale-0.1.1 (c (n "current_locale") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.82") (d #t) (t "cfg(windows)") (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winnls" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "046d0bsf4373991m586s0is8aybwxb72391iis6yd2ysrkf8vgp0")))

