(define-module (crates-io cu rr current_platform) #:use-module (crates-io))

(define-public crate-current_platform-0.1.0 (c (n "current_platform") (v "0.1.0") (h "1kh53f18hdmz511h6jl8zqhq7q0ciap8cc5dsclmq4qkv2h2yvky")))

(define-public crate-current_platform-0.2.0-rc.1 (c (n "current_platform") (v "0.2.0-rc.1") (h "1qhwpyznbwd3hw7raqk6nm2njivmwfa2zfr597ap0kx06p3cqs3w") (r "1.19")))

(define-public crate-current_platform-0.2.0 (c (n "current_platform") (v "0.2.0") (h "1g504i1l733bn1hyzzfvwmc8qq84dxxpscs9rcb21cj4zsy5hj57") (r "1.19")))

