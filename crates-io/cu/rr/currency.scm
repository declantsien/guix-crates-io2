(define-module (crates-io cu rr currency) #:use-module (crates-io))

(define-public crate-currency-0.1.1 (c (n "currency") (v "0.1.1") (h "0jhai930vyhwll8zcnnp8aribz5zb73xp3l8n8hxf74zh7cz62dj")))

(define-public crate-currency-0.1.2 (c (n "currency") (v "0.1.2") (h "199yzs9yjilx0pq34h30r7xvq2agnagxsi4d8v0fxx9hyzm7bf4k")))

(define-public crate-currency-0.1.3 (c (n "currency") (v "0.1.3") (h "1pv0051qmwnlzy4kb5s2i1hp7fhkf6wphiigwwc59mhzl48q5znx")))

(define-public crate-currency-0.2.0 (c (n "currency") (v "0.2.0") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "11hglxw41apfwssp07f1b2yrc1ckzdxz2kykc1p9cjcpr896ahvf")))

(define-public crate-currency-0.3.0 (c (n "currency") (v "0.3.0") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "0wgbsvlb7pqhilmcpq8vasdgycmbnpliq99q1gdlnnjwpv40q1nk")))

(define-public crate-currency-0.4.0 (c (n "currency") (v "0.4.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1swkzgdacs43zl5xl9ap8r9kz7qpdzp9gx58k38k1s8p69hs23h1")))

