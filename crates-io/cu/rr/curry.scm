(define-module (crates-io cu rr curry) #:use-module (crates-io))

(define-public crate-curry-0.0.0 (c (n "curry") (v "0.0.0") (h "0pb5nzxfihzmrjb54zg46wmj0wfz2nqqmx99y5iq9d5d8qmzpxgp")))

(define-public crate-curry-0.1.0 (c (n "curry") (v "0.1.0") (h "0c0nnn8d7wg87y091x7jf1sm5n5jp7488fbbj9a1l4cp42kk5q1b")))

