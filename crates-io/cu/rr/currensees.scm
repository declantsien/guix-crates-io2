(define-module (crates-io cu rr currensees) #:use-module (crates-io))

(define-public crate-currensees-0.1.0 (c (n "currensees") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1kwccr5s7z3mxq03h9my12f694q3dsffgm0zqhgqbkmk3icm08gj") (y #t)))

(define-public crate-currensees-0.1.1 (c (n "currensees") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cqxqbnr4fi76fc1amwq4w5gnyaaq7n7ln4d25a18ymvls9vsfn3") (y #t)))

(define-public crate-currensees-0.1.2 (c (n "currensees") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1q7gwqh3s3zkqilzrnrh0dk7d348dw3qg8dsrqab32v7fv2nz68j") (y #t)))

(define-public crate-currensees-0.1.3 (c (n "currensees") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mlml6899lnjf6xygmr2xqb9yjbv25njg2rnvkpn1vjaqjdq19d0") (y #t)))

(define-public crate-currensees-0.1.4 (c (n "currensees") (v "0.1.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l3cvkrvvjbdbxswifcp6fz6xi9jka81kqnjcr62mwszvm034m75") (y #t)))

