(define-module (crates-io cu rr currency_rs) #:use-module (crates-io))

(define-public crate-currency_rs-1.0.0 (c (n "currency_rs") (v "1.0.0") (d (list (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)))) (h "1jc0ph4fx0cfdxdgxscrm0vd92hj5yxfwvcf54jw2makn8hqihr8")))

(define-public crate-currency_rs-1.1.0 (c (n "currency_rs") (v "1.1.0") (d (list (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)))) (h "0znr3mn6nqy0290ajrj5kcbdcrcr3km1map647hvmrxn4nl2nq27")))

(define-public crate-currency_rs-1.1.1 (c (n "currency_rs") (v "1.1.1") (d (list (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)))) (h "1ny2jwds1z8nij5zz7l71b1j131zgkkddq7b1lj77y4vg0jhwx0p")))

(define-public crate-currency_rs-1.1.2 (c (n "currency_rs") (v "1.1.2") (d (list (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)))) (h "15l65v6lgr041kc734p95nlz7xj16vbrhc7qcq4cd01rjvizg83w")))

(define-public crate-currency_rs-1.1.3 (c (n "currency_rs") (v "1.1.3") (d (list (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)))) (h "0qbd4x7jhci4h60g625nkacy5jajkc8fw7wa5v3rfldfbqdhnwpw")))

(define-public crate-currency_rs-1.1.4 (c (n "currency_rs") (v "1.1.4") (d (list (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)))) (h "1k91l9k36hpn6h4x8vk7zm872ww989mkaj898gm1hwj2vxxi7d6h")))

(define-public crate-currency_rs-1.2.0 (c (n "currency_rs") (v "1.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("serde"))) (d #t) (k 2)))) (h "1qbpjjrcmk6k7s37hws7x0hj7pn5pw13asyav73sl2kmzln1bql2")))

(define-public crate-currency_rs-1.3.0 (c (n "currency_rs") (v "1.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("serde"))) (d #t) (k 2)))) (h "0apkns5kpkyzn9hv7jbwb1fbmb14qi5gv542nfi6482nfh1yavn7")))

