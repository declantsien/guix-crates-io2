(define-module (crates-io cu rr curri-fsm) #:use-module (crates-io))

(define-public crate-curri-fsm-0.1.0 (c (n "curri-fsm") (v "0.1.0") (d (list (d (n "curri") (r "^0.1.0") (d #t) (k 0)))) (h "11pac9sl8ssmx4y6dbkapwrf8n5hm6xv383sb94kzsx7qpzavqyb")))

(define-public crate-curri-fsm-0.1.1 (c (n "curri-fsm") (v "0.1.1") (d (list (d (n "curri") (r "^0.1.0") (d #t) (k 0)))) (h "0jlsrbd0mp4flpb9dcwm4x75z00x670ckkyd65zzv0rd4y3l2cmr")))

(define-public crate-curri-fsm-0.1.2 (c (n "curri-fsm") (v "0.1.2") (d (list (d (n "curri") (r "^0.1.0") (d #t) (k 0)))) (h "0f25n0zqfbmddd29587m25wqq0pscaprmgfm8mjc5w1v5l3ffjak")))

(define-public crate-curri-fsm-0.1.3 (c (n "curri-fsm") (v "0.1.3") (d (list (d (n "curri") (r "^0.1.0") (d #t) (k 0)))) (h "1x6vl4gr9578r0909pqmaizm2mw1i8qp0frp01yv043dd7gzxc7q")))

(define-public crate-curri-fsm-0.1.3-alpha.1 (c (n "curri-fsm") (v "0.1.3-alpha.1") (d (list (d (n "curri") (r "^0.1.0") (d #t) (k 0)))) (h "0cwb12hnva6918zr949byql4xkyijkpqgpz4g0hkv2z4wvyiqwhr")))

(define-public crate-curri-fsm-0.1.4-alpha.1 (c (n "curri-fsm") (v "0.1.4-alpha.1") (d (list (d (n "curri") (r "^0.1.0") (d #t) (k 0)))) (h "043gf5ki2fgnj5zcsnym8ls0d32wa7cinij5xzjpjh3y918zx2fh")))

(define-public crate-curri-fsm-0.1.4-alpha.2 (c (n "curri-fsm") (v "0.1.4-alpha.2") (d (list (d (n "curri") (r "^0.1.2") (d #t) (k 0)))) (h "0xwca6w2zr49zqi8lprl5v682p6a9v8b45qhcndcwgyachfrb6vp")))

