(define-module (crates-io cu tr cutr) #:use-module (crates-io))

(define-public crate-cutr-0.1.0 (c (n "cutr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "cargo-release") (r "^0.25.6") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "02vyggf5jgs3fhh6sd9x7hv8nqvfa9vn1ni2isnbarfs1h0rhyn7")))

