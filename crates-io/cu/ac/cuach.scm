(define-module (crates-io cu ac cuach) #:use-module (crates-io))

(define-public crate-cuach-0.1.0 (c (n "cuach") (v "0.1.0") (d (list (d (n "cuach-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.4") (d #t) (k 0)))) (h "0521j337xsj2dl9s4qj5h6d44cn72jni0dgpl65zdzi5pc0kj1s4")))

(define-public crate-cuach-0.1.1 (c (n "cuach") (v "0.1.1") (d (list (d (n "cuach-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.4") (d #t) (k 0)))) (h "0jnbxj44bpsxdiy7769j5gzb1i2sn06ybvld5pvh9fmbya5nhs1v")))

(define-public crate-cuach-0.2.0 (c (n "cuach") (v "0.2.0") (d (list (d (n "cuach-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.4") (d #t) (k 0)))) (h "07q2znp0z08dwisww5wskwwhizkykvpv8rgp4z8ajyx2n2i1kqhm")))

(define-public crate-cuach-0.2.1 (c (n "cuach") (v "0.2.1") (d (list (d (n "cuach-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.4") (d #t) (k 0)))) (h "13vvrnjac3i2hlwj2yk0aq449s1gqa40dfmw7fkpkm1z48caw303")))

(define-public crate-cuach-0.2.2 (c (n "cuach") (v "0.2.2") (d (list (d (n "cuach-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.4") (d #t) (k 0)))) (h "0lrbf5hpac63gfj2v6awl5ysggqjpd1hxdy9gb22x7a4089k9m0g")))

(define-public crate-cuach-0.3.0 (c (n "cuach") (v "0.3.0") (d (list (d (n "cuach-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.4") (d #t) (k 0)))) (h "0ccwirnkb9qgy9l30vh9dqad9ks3frpwq5d6wiz5dnnwpzwx3s6n")))

(define-public crate-cuach-0.3.1 (c (n "cuach") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cuach-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.4") (d #t) (k 0)))) (h "1v9n1l63yd9zi8d9y3g3s4jwsxqy2sfv8ljiw0r487pviasfhp5s")))

(define-public crate-cuach-0.4.0 (c (n "cuach") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cuach-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.12") (d #t) (k 0)))) (h "1iz9cb06fvlsvdav976fxa87zyyfb5v9j631mljjhhz4mgf3wq5m")))

(define-public crate-cuach-0.4.1 (c (n "cuach") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cuach-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.13") (d #t) (k 0)))) (h "0xm0jnia3vzg9jj6yjl2jj85fvhrzsy8immq8rr677abpmv8kr6r")))

(define-public crate-cuach-0.4.2 (c (n "cuach") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cuach-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.12") (d #t) (k 0)))) (h "0l5y4y9vg9cj3qg8p9v9r4n2qrzkwd018nxm1znwcw78c2nzfzq6")))

(define-public crate-cuach-0.5.0 (c (n "cuach") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cuach-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.12") (d #t) (k 0)))) (h "1w6lc19wj31qrilm7xxbgx1jkb79iycqips0wf38qzk7yy6z4gsr")))

(define-public crate-cuach-0.6.0 (c (n "cuach") (v "0.6.0") (d (list (d (n "cuach-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.12") (d #t) (k 0)))) (h "12isy94hnr31iydsdm7g6vzyphbsmqb7ik9s4dz32gi3yrcy8jcm")))

(define-public crate-cuach-0.6.1 (c (n "cuach") (v "0.6.1") (d (list (d (n "cuach-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.12") (d #t) (k 0)))) (h "1m52zhwhwv83pj01wy7dr5mmdsxmbid0yn0gyhcylb7z0f960h8q")))

(define-public crate-cuach-0.7.0 (c (n "cuach") (v "0.7.0") (d (list (d (n "cuach-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.15") (d #t) (k 0)))) (h "1sfxm9zw73zhlcvz0rnnb95spsgpq2f2jy5n8cvmh1vzn65s6w6w")))

(define-public crate-cuach-0.8.0 (c (n "cuach") (v "0.8.0") (d (list (d (n "cuach-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.15") (d #t) (k 0)))) (h "1rkryyybsnn7krpdbmqldh8pjzf7kpml19fb3xsyy1fz7206hfav")))

(define-public crate-cuach-0.9.0 (c (n "cuach") (v "0.9.0") (d (list (d (n "cuach-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.15") (d #t) (k 0)))) (h "093mfs5mjwi1af643krq22xcjwj53iywrzvh97aianpb71lrsnvd")))

