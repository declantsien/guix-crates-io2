(define-module (crates-io cu pc cupcake) #:use-module (crates-io))

(define-public crate-Cupcake-0.1.0 (c (n "Cupcake") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "modinverse") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1x3sl1ih58gmadqqp60psrxiwih6gbih199n1i80i1kflq0xa7k6") (y #t)))

(define-public crate-Cupcake-0.1.1 (c (n "Cupcake") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "modinverse") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0d8wfrlbj81f2inpi0qmqwl5a1bk6szx0rsn46clr6db3jq4f2sl")))

(define-public crate-Cupcake-0.2.0 (c (n "Cupcake") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "modinverse") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0jyjpmnfr9fgz2i4v9sxxd2g2c35c05dzqfzvdxdzzfvld3zxnkk")))

(define-public crate-Cupcake-0.2.1 (c (n "Cupcake") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "modinverse") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0s6xypghxmwr5nkc46kc37dp40yijy6r431j9njxy135i3dhszry") (f (quote (("bench"))))))

