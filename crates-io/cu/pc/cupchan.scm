(define-module (crates-io cu pc cupchan) #:use-module (crates-io))

(define-public crate-cupchan-0.1.0 (c (n "cupchan") (v "0.1.0") (d (list (d (n "loom") (r "^0.5") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)))) (h "1k4jli1379x3554nlw6j0i8knz3iiykqxm7rxa8wyj056vz8icf0")))

(define-public crate-cupchan-0.1.1 (c (n "cupchan") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 2)) (d (n "flume") (r "^0.10.12") (d #t) (k 2)) (d (n "loom") (r "^0.5.4") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)))) (h "0d6jf8nqjw5ylmjpx8cxyxwxmxphbzjfbhzw9kip4kxlyhx4in5h")))

(define-public crate-cupchan-0.1.2 (c (n "cupchan") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 2)) (d (n "flume") (r "^0.10.12") (d #t) (k 2)) (d (n "loom") (r "^0.5.4") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)))) (h "1d2f8dm4hq23h66dpnwjil9fbwgixyfzmhkzgfhnlmyx9hczvzh1")))

