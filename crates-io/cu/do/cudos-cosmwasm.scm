(define-module (crates-io cu do cudos-cosmwasm) #:use-module (crates-io))

(define-public crate-cudos-cosmwasm-0.0.1 (c (n "cudos-cosmwasm") (v "0.0.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0m317lcx4cck49v1kg2d89vn6ak3a952cfcchb7525abpzbzp2mv") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-cudos-cosmwasm-0.0.2 (c (n "cudos-cosmwasm") (v "0.0.2") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "06swfjsmi1rll8qi67q3r383pp9mpgcgx4zbmhdicf0mpnyq0bv7") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-cudos-cosmwasm-0.0.3 (c (n "cudos-cosmwasm") (v "0.0.3") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1c37fb5rw0jlaapny884hj3zzrmn0w751v703vgsqisf2381gl2v") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-cudos-cosmwasm-0.0.4 (c (n "cudos-cosmwasm") (v "0.0.4") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0kqwmfpwppy8iw8xg8f8zq45nfwnvgsh19lz8g107k2dw6j6hv2k") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-cudos-cosmwasm-0.0.5 (c (n "cudos-cosmwasm") (v "0.0.5") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0hmkx3s923ajrg7fm6j7vwpikjrmqf40l760wjvrd68ksv5gb093") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-cudos-cosmwasm-0.0.6 (c (n "cudos-cosmwasm") (v "0.0.6") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1079r4ggkqdixxfx6anxj7kmyzcnh9s5gpyf89v5jxxv3cz6n8x7") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-cudos-cosmwasm-0.0.7 (c (n "cudos-cosmwasm") (v "0.0.7") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "16wnjl50f26mjghrd5qhmhawcxkl60p79s5g4c8lragmhg4cawn4") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

