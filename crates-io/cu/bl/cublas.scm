(define-module (crates-io cu bl cublas) #:use-module (crates-io))

(define-public crate-cublas-0.1.0 (c (n "cublas") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.35") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.7") (d #t) (k 2)) (d (n "cublas-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hccqsyjbyd35mhhy2v11i5ilqwdhlygxhfc2jkr0vfsaz2h9cmq") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cublas-0.2.0 (c (n "cublas") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.35") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.7") (d #t) (k 2)) (d (n "cublas-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15qhky0cswd58rrzd6xax7c7fl6iwb1civxrvyv9m5c571mf9amc") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

