(define-module (crates-io cu sh cushy-macros) #:use-module (crates-io))

(define-public crate-cushy-macros-0.2.0 (c (n "cushy-macros") (v "0.2.0") (d (list (d (n "attribute-derive") (r "^0.8.1") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "manyhow") (r "^0.10.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.15") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "quote-use") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0891lvv6yfjdky1bjybl090qd6frjx8qgi5yx45hc981a6zy5z26")))

(define-public crate-cushy-macros-0.3.0 (c (n "cushy-macros") (v "0.3.0") (d (list (d (n "attribute-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "manyhow") (r "^0.11.1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.15") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "quote-use") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1xahcpi76k7rwvw2d7pzqk1wr5zzd9y1drariww563mm5h2xa5mq")))

