(define-module (crates-io cu be cube-query) #:use-module (crates-io))

(define-public crate-cube-query-0.1.0 (c (n "cube-query") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0nqvfwn4cjlp11pjpgj4vhs7lff82ip2lylakxz128qyi9rcq361")))

(define-public crate-cube-query-0.1.1 (c (n "cube-query") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1cjmibqi6k5aqxgwiym0d3rlaws9jajayjkwadm0fij0h24a2faf")))

(define-public crate-cube-query-0.1.2 (c (n "cube-query") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0i11fjr0687r8dgayxj5sh8vkf2z74rdigsairm8ks90x20ib2km")))

(define-public crate-cube-query-0.1.3 (c (n "cube-query") (v "0.1.3") (d (list (d (n "cube-query-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0yzvqr3qwjd7abc89fqgz1iczj7sxjqmzapnnq9nc3fwy26vhfc1")))

(define-public crate-cube-query-0.1.4 (c (n "cube-query") (v "0.1.4") (d (list (d (n "cube-query-lib") (r "^0.1.1") (d #t) (k 0)))) (h "1p3hl9ykrb163xbwq9q203vdzbxq52r9bi95gncdn7ribzrrkqsn")))

