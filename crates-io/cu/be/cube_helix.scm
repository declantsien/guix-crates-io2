(define-module (crates-io cu be cube_helix) #:use-module (crates-io))

(define-public crate-cube_helix-0.1.0 (c (n "cube_helix") (v "0.1.0") (h "0875vzj04kf1k2c9qva47wpx8h96shvm5jacrlj568g3vg47chz3")))

(define-public crate-cube_helix-0.1.1 (c (n "cube_helix") (v "0.1.1") (h "1hpyylf0q2mfxfmxymx7j0psi2m8g3jwhyln38ska0wvpkg4qx9c")))

