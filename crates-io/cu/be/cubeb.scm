(define-module (crates-io cu be cubeb) #:use-module (crates-io))

(define-public crate-cubeb-0.4.0 (c (n "cubeb") (v "0.4.0") (d (list (d (n "cubeb-core") (r "^0.4") (d #t) (k 0)))) (h "1ifjamzzb697w32hvhsfmamv4mkdl4bkmbba6adxplvla4isb8cl")))

(define-public crate-cubeb-0.4.1 (c (n "cubeb") (v "0.4.1") (d (list (d (n "cubeb-core") (r "^0.4.1") (d #t) (k 0)))) (h "1y02m5akvwggby3626lqg5hihq3i9czsnv4y10lir7brp427h8zd") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.5.0 (c (n "cubeb") (v "0.5.0") (d (list (d (n "cubeb-core") (r "^0.5.0") (d #t) (k 0)))) (h "11dvapkh5238dbqmjbqzim4bl2056vfbmdp8qfh4p9m1sp9bw8vr") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.5.1 (c (n "cubeb") (v "0.5.1") (d (list (d (n "cubeb-core") (r "^0.5.0") (d #t) (k 0)))) (h "1bral7xjx4ahqhp17g55c9y8721fqcab5m2m0davbg5d23avkskk") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.5.2 (c (n "cubeb") (v "0.5.2") (d (list (d (n "cubeb-core") (r "^0.5.1") (d #t) (k 0)))) (h "15i8kavdi8vd5jjkwm59m33005vlv13bmlk59x95ry8vzam04dca") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.5.3 (c (n "cubeb") (v "0.5.3") (d (list (d (n "cubeb-core") (r "^0.5.3") (d #t) (k 0)))) (h "1sjwacr60fxvynpa872hmlspg3paq7l6znz7bhmq8cpx1nzb4r6w") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.5.4 (c (n "cubeb") (v "0.5.4") (d (list (d (n "cubeb-core") (r "^0.5.4") (d #t) (k 0)))) (h "0amw1m1k3nvc69zj9ncb9q1w39kb8qhpk9v4cc957d2wmpr0sgyv") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.5.5 (c (n "cubeb") (v "0.5.5") (d (list (d (n "cubeb-core") (r "^0.5.5") (d #t) (k 0)))) (h "1g214j3dgfpili65rj0jq1lq3668pz9igrh3cb4vv01sri3agxjh") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.5.6 (c (n "cubeb") (v "0.5.6") (d (list (d (n "cubeb-core") (r "^0.5.6") (d #t) (k 0)))) (h "0fwk9pb6qzmqb5pm0j9313l17nmy4x6ssp0mwp6p339rf4ax19jx") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.6.0 (c (n "cubeb") (v "0.6.0") (d (list (d (n "cubeb-core") (r "^0.6.0") (d #t) (k 0)))) (h "1p60dc77wihqib66xd3cs254rmf16y83ixfdjfardmb6fm3jjl2q") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree")))) (y #t)))

(define-public crate-cubeb-0.6.1 (c (n "cubeb") (v "0.6.1") (d (list (d (n "cubeb-core") (r "^0.6.1") (d #t) (k 0)))) (h "1z7a77kfl6vh1v30n3p6848k8vj6863frpd13ndf9v8dh8h0bc4k") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree")))) (y #t)))

(define-public crate-cubeb-0.6.2 (c (n "cubeb") (v "0.6.2") (d (list (d (n "cubeb-core") (r "^0.6.2") (d #t) (k 0)))) (h "095fmjncy5262fy0wp0v5r9x7alnmbzniw7gyq51c6d3kvgdzg1w") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.7.0 (c (n "cubeb") (v "0.7.0") (d (list (d (n "cubeb-core") (r "^0.7.0") (d #t) (k 0)))) (h "0pqkfg7hsdx6z80rfcv1k5pvm0rkyqy2y20ydagikja5c1nn05hi") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.8.0 (c (n "cubeb") (v "0.8.0") (d (list (d (n "cubeb-core") (r "^0.8.0") (d #t) (k 0)))) (h "0ywi3i3kzamqb8aky4cwqdgpr81l587xcq91hlykyd2p5dl55698") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.9.0 (c (n "cubeb") (v "0.9.0") (d (list (d (n "cubeb-core") (r "^0.9.0") (d #t) (k 0)))) (h "1h1n3hqx89sm5hxa37h5rrc767qa1z5b78bcvwcd4yn0d89lrj1k") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.10.0 (c (n "cubeb") (v "0.10.0") (d (list (d (n "cubeb-core") (r "^0.10.0") (d #t) (k 0)))) (h "1gc4hjrj0540k4nd48fms3hw216rl1b2j9nxsybfjd401g08qz6l") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.10.1 (c (n "cubeb") (v "0.10.1") (d (list (d (n "cubeb-core") (r "^0.10.1") (d #t) (k 0)))) (h "15xv2b3a312463d8k460bkqbsckg4r3h3s042xvsdxccxn31y1ak") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.10.2 (c (n "cubeb") (v "0.10.2") (d (list (d (n "cubeb-core") (r "^0.10.2") (d #t) (k 0)))) (h "1p0bkqxvccvb6gxq2gg8lnnk663sjl34g4m9rbz72arxycny25y3") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.10.3 (c (n "cubeb") (v "0.10.3") (d (list (d (n "cubeb-core") (r "^0.10.3") (d #t) (k 0)))) (h "11a2i96wjl4janxnrf31j56l3dkbhnh6a0awiaykxn2b4c7w09bp") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.10.5 (c (n "cubeb") (v "0.10.5") (d (list (d (n "cubeb-core") (r "^0.10.5") (d #t) (k 0)))) (h "14vy2m5m8ml454wsyqdgx3cihv4zshq70kl97q9yqah49rrcbbh0") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.10.6 (c (n "cubeb") (v "0.10.6") (d (list (d (n "cubeb-core") (r "^0.10.6") (d #t) (k 0)))) (h "0rfvb8h5rrayr0h7y283c3vjcqbbyqhflp08k9nmpb6iyvq8b1zm") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.10.7 (c (n "cubeb") (v "0.10.7") (d (list (d (n "cubeb-core") (r "^0.10.7") (d #t) (k 0)))) (h "1p5x8lp583v96hx2ly1cvx2qmr0w3a7f5f5xj18pi1bf3zq1jpx6") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.12.0 (c (n "cubeb") (v "0.12.0") (d (list (d (n "cubeb-core") (r "^0.12.0") (d #t) (k 0)))) (h "165s1yir0pdcc4k4fb3cjgfdj5jq6b9fh88pq4a04zv1y9q7bdbd") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-0.13.0 (c (n "cubeb") (v "0.13.0") (d (list (d (n "cubeb-core") (r "^0.13.0") (d #t) (k 0)))) (h "0hm0c74d6bzs0925fi3a21k4gbrjd38f0v3r1srwsdl0rx3ma41x") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

