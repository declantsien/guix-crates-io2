(define-module (crates-io cu be cubeb-core) #:use-module (crates-io))

(define-public crate-cubeb-core-0.4.0 (c (n "cubeb-core") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.4") (d #t) (k 0)))) (h "0cy31hvfxvj46bs0vw347kfc4c5c0raf6hild9ywqgyqlf8n7frx")))

(define-public crate-cubeb-core-0.4.1 (c (n "cubeb-core") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1qygxzqcfk5i1lvlfzmy477dsdfb2i2qaa4h9aarkzgwfmflgl1v") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.4.2 (c (n "cubeb-core") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1aknlfv80axllz3ffg7317c1b0qm5j2p1jc410nawiw7kngbpi8s") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.4.3 (c (n "cubeb-core") (v "0.4.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1yzaqyvp755afxcgvwkdypvb033va8k9n8ffwysg27aik5mqwmd6") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.4.4 (c (n "cubeb-core") (v "0.4.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1q49pb90p3nazhpd4588lzkazsbsgmg33sxxvzqq216wza7xrkkx") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.5.0 (c (n "cubeb-core") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0rfmr8y8b4hl8aix9p9sd5icymphmqr5344p8nf0z9nsylv9m78n") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.5.1 (c (n "cubeb-core") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0ial5d4ibxjsl7mr5cbdvj17dxxza5jj6qw8l9m4nkksfl7v5xrp") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.5.3 (c (n "cubeb-core") (v "0.5.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.5.3") (d #t) (k 0)))) (h "0x7rrgj9wqkshbw8rzbbapw1ghyk69ycdl4x7i2d3rqs5anjdrg5") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.5.4 (c (n "cubeb-core") (v "0.5.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.5.4") (d #t) (k 0)))) (h "08jh0ia9jqkm5ig43jiy8cw4j2f9dgc0pjbjl6kjp4k4g0rgjq81") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.5.5 (c (n "cubeb-core") (v "0.5.5") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.5.5") (d #t) (k 0)))) (h "01qrfb3mzdnwk0dsrn1svzyx20qbjy5kgq2j4i7xq19m5pkwvii0") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.5.6 (c (n "cubeb-core") (v "0.5.6") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.5.6") (d #t) (k 0)))) (h "0x9viamnswq595lm1pbm7iqz549i1jvnkvac1x2xqcz0rh36di8y") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.6.0 (c (n "cubeb-core") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0wbi3aip08pgbjghf4vk3sf9pcrxk5h60pnff6f4q9jcgmpfv1z3") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree")))) (y #t)))

(define-public crate-cubeb-core-0.6.1 (c (n "cubeb-core") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.6.1") (d #t) (k 0)))) (h "1b00az7qbqp6yp1xs6pcqyyszgy21myd6wy398q0hp91b4qzg00l") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree")))) (y #t)))

(define-public crate-cubeb-core-0.6.2 (c (n "cubeb-core") (v "0.6.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.6.2") (d #t) (k 0)))) (h "1j5lf39qr7v2jdvva3ihnb655x9x1728zldh36afvbxn3kmb5ndz") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.7.0 (c (n "cubeb-core") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1rfg6zi3zqs40qjmqz4n8x45m2mh64fw07rf4kj2cyglp0lmbigp") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.8.0 (c (n "cubeb-core") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.8.0") (d #t) (k 0)))) (h "1wvxj5c1nh21lsg9y9bnxvqjs2phi3v5jc7sqdri85wlbf07y6f6") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.9.0 (c (n "cubeb-core") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.9.0") (d #t) (k 0)))) (h "1sbh2bw314vfvd8ndfqay7847383acklsk3526pplqmxv7z7gaj0") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.10.0 (c (n "cubeb-core") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.10.0") (d #t) (k 0)))) (h "1nap7l86bfw2lvd5nkhi5qix101d3k6r4bsvyask10q8zbbvrah4") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.10.1 (c (n "cubeb-core") (v "0.10.1") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.10.1") (d #t) (k 0)))) (h "155hvnbi09gg8zp47018bgws3dxv2xp9v0jb5v0r7qy27mbsrh28") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.10.2 (c (n "cubeb-core") (v "0.10.2") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.10.2") (d #t) (k 0)))) (h "0b8j89cscym0jflfn8f4fajr843cb9wf1i27lzpcyxxjvqqs85bs") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.10.3 (c (n "cubeb-core") (v "0.10.3") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.10.3") (d #t) (k 0)))) (h "115wk9a78albl47r3wvpry9jqihphmrn08fcfwh3ir1ag9bcs0v2") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.10.4 (c (n "cubeb-core") (v "0.10.4") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.10.3") (d #t) (k 0)))) (h "114my28cfqwgjh56w2nai6ax143jcncny5l0zr47kdwsrhgk69s7") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.10.5 (c (n "cubeb-core") (v "0.10.5") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.10.5") (d #t) (k 0)))) (h "15cqchf0nrxzil2gvygijg38pbqymyikkxq01z6yrfxr7c4pm631") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.10.6 (c (n "cubeb-core") (v "0.10.6") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.10.6") (d #t) (k 0)))) (h "0zlh6k3hc8jni01rplhqypvrdc5w9rpxncbcr04xj1xc2h1mjc5g") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.10.7 (c (n "cubeb-core") (v "0.10.7") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.10.7") (d #t) (k 0)))) (h "1d8m4hc9ixfsrhaqpyvjrzk7b4yyvnbwgcj881xzxcw72hdlqcns") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.12.0 (c (n "cubeb-core") (v "0.12.0") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.12.0") (d #t) (k 0)))) (h "1ivawh4fidc4va5dcbxz7kjkafa21wsdc452yr23gsphglxc1013") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

(define-public crate-cubeb-core-0.13.0 (c (n "cubeb-core") (v "0.13.0") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "cubeb-sys") (r "^0.13") (d #t) (k 0)))) (h "0wxwh6a0k5k0vl070dj5k2wd73n9k44cgvndkhfx9ihyvlad625c") (f (quote (("gecko-in-tree" "cubeb-sys/gecko-in-tree"))))))

