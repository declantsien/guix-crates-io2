(define-module (crates-io cu be cube-engine) #:use-module (crates-io))

(define-public crate-cube-engine-0.1.0 (c (n "cube-engine") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "speculate") (r "^0.1") (d #t) (k 2)))) (h "0qw9wgh8dd3vvf2khpwcwm2g8c6xds543vn0r87gbw6cp43hnjzb")))

