(define-module (crates-io cu be cubeb-backend) #:use-module (crates-io))

(define-public crate-cubeb-backend-0.4.0 (c (n "cubeb-backend") (v "0.4.0") (d (list (d (n "cubeb-core") (r "^0.4") (d #t) (k 0)))) (h "0h83zkbmdl7k917s59ggjw429qxf6s28hv7hjbvrk23zsi29g1ia")))

(define-public crate-cubeb-backend-0.4.1 (c (n "cubeb-backend") (v "0.4.1") (d (list (d (n "cubeb-core") (r "^0.4.1") (d #t) (k 0)))) (h "10adxq863dy2dg6s32cbg16m28fjn6pb0pn4mvlwry8wk8c8yvag") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.5.0 (c (n "cubeb-backend") (v "0.5.0") (d (list (d (n "cubeb-core") (r "^0.5.0") (d #t) (k 0)))) (h "0f5ig9cg11wbyj9ks0h6p4jxmrwkc87c8bfv2k49wva135awkjpx") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.5.1 (c (n "cubeb-backend") (v "0.5.1") (d (list (d (n "cubeb-core") (r "^0.5.0") (d #t) (k 0)))) (h "1w8izp2azgc60spqpdlycgl1gsy2gv418qhb5f57g6izik8a0ch4") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.5.3 (c (n "cubeb-backend") (v "0.5.3") (d (list (d (n "cubeb-core") (r "^0.5.3") (d #t) (k 0)))) (h "1i73ghm7zk3jy4r35yimzczl9pym9igwx3ik271kjdi3va9g719x") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.5.4 (c (n "cubeb-backend") (v "0.5.4") (d (list (d (n "cubeb-core") (r "^0.5.4") (d #t) (k 0)))) (h "1v1g4rj2d0vn66nmfn3jbdizzcnr43iqq9r50mrn5zw0p77xab24") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.5.5 (c (n "cubeb-backend") (v "0.5.5") (d (list (d (n "cubeb-core") (r "^0.5.5") (d #t) (k 0)))) (h "1sm2l37vxadkl0pfvy23l6q63czxn7zb42fq9m2pyp46illlmhal") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.5.6 (c (n "cubeb-backend") (v "0.5.6") (d (list (d (n "cubeb-core") (r "^0.5.6") (d #t) (k 0)))) (h "11lqd2y0zx6s5kvxnii4pkmjym3grqm7prlbf77zdhslc5s6vy4l") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.6.0 (c (n "cubeb-backend") (v "0.6.0") (d (list (d (n "cubeb-core") (r "^0.6.0") (d #t) (k 0)))) (h "1b0anxf0jyj0m6a63yhxdaf0sqr9d28h00vm24hscypca2rbqv37") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree")))) (y #t)))

(define-public crate-cubeb-backend-0.6.1 (c (n "cubeb-backend") (v "0.6.1") (d (list (d (n "cubeb-core") (r "^0.6.1") (d #t) (k 0)))) (h "1b7gmyd9s70px4y3l0hfl6rznhbw44kqnv1czw73w4kq2hs0j3d5") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree")))) (y #t)))

(define-public crate-cubeb-backend-0.6.2 (c (n "cubeb-backend") (v "0.6.2") (d (list (d (n "cubeb-core") (r "^0.6.2") (d #t) (k 0)))) (h "177dqkabna7p76rjr8c94gavx0l464i2j5r4pfpahhkn9vfpl7js") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.6.3 (c (n "cubeb-backend") (v "0.6.3") (d (list (d (n "cubeb-core") (r "^0.6.2") (d #t) (k 0)))) (h "0bnig9srjazhmr5q2fah4pfra9z7ww7bqc4krb6a51dx1aq7i1r2") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.7.0 (c (n "cubeb-backend") (v "0.7.0") (d (list (d (n "cubeb-core") (r "^0.7.0") (d #t) (k 0)))) (h "0x2pnx07h317mf65ny5zgczhxh39hfam2hyzlna5km1krcjl8r6c") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.8.0 (c (n "cubeb-backend") (v "0.8.0") (d (list (d (n "cubeb-core") (r "^0.8.0") (d #t) (k 0)))) (h "0p60gvrhrrgr77k9kws0x2dysl03zds7h5ma8d548yz0lsghjli0") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.9.0 (c (n "cubeb-backend") (v "0.9.0") (d (list (d (n "cubeb-core") (r "^0.9.0") (d #t) (k 0)))) (h "0j7csvhi3zzkjy582ps4zczqrl11k39gvrizgrd9ylc1mnxp9f73") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.10.0 (c (n "cubeb-backend") (v "0.10.0") (d (list (d (n "cubeb-core") (r "^0.10.0") (d #t) (k 0)))) (h "0z92cjq8dsps9qwsvf344xrixpmwyacbqjqhf5jnh4yhkmvwvlg3") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.10.1 (c (n "cubeb-backend") (v "0.10.1") (d (list (d (n "cubeb-core") (r "^0.10.1") (d #t) (k 0)))) (h "0937hhl7mwsdg5amrahlrd9q42zmylfxsf9lp2bzycwsa1dxdqhc") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.10.2 (c (n "cubeb-backend") (v "0.10.2") (d (list (d (n "cubeb-core") (r "^0.10.2") (d #t) (k 0)))) (h "1ndalbqhxb2640n23zd0yswnay8vcg2h04n87sn07f499wxjhpx4") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.10.3 (c (n "cubeb-backend") (v "0.10.3") (d (list (d (n "cubeb-core") (r "^0.10.3") (d #t) (k 0)))) (h "102ajh8wk2wn03qlgfkn9jnlisb5wil8lyrc2q8gilpdq4f6704g") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.10.5 (c (n "cubeb-backend") (v "0.10.5") (d (list (d (n "cubeb-core") (r "^0.10.5") (d #t) (k 0)))) (h "12bg6ancflv72n6shy5jv4q7g8wpj26fcvrdxiqmy8v1z9msyhza") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.10.6 (c (n "cubeb-backend") (v "0.10.6") (d (list (d (n "cubeb-core") (r "^0.10.6") (d #t) (k 0)))) (h "141jqlfbjb7na73y20gv2yxgqr75jysp6cg2iz5qhr9piyj93agm") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.10.7 (c (n "cubeb-backend") (v "0.10.7") (d (list (d (n "cubeb-core") (r "^0.10.7") (d #t) (k 0)))) (h "04cnsa77ylqc2vnsz9vdp6ncdqhk7j88m4fyzbvriw4mp68alm1i") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.12.0 (c (n "cubeb-backend") (v "0.12.0") (d (list (d (n "cubeb-core") (r "^0.12.0") (d #t) (k 0)))) (h "0444vhd00rfabgqg44x4cxi0ccq632a8gqf4v0dmf5g3hhxhy2xh") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

(define-public crate-cubeb-backend-0.13.0 (c (n "cubeb-backend") (v "0.13.0") (d (list (d (n "cubeb-core") (r "^0.13.0") (d #t) (k 0)))) (h "10fazgw1drzdvirci5s8y4fxkpdn54jk5kihlbi9jicvnkliydk7") (f (quote (("gecko-in-tree" "cubeb-core/gecko-in-tree"))))))

