(define-module (crates-io cu be cubeb-sys) #:use-module (crates-io))

(define-public crate-cubeb-sys-0.4.0 (c (n "cubeb-sys") (v "0.4.0") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0p24hd4var0n5jkic9nrch4fl5xrlkcydwrfk1qsqwl8mn2yxk02")))

(define-public crate-cubeb-sys-0.4.1 (c (n "cubeb-sys") (v "0.4.1") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1aj15xbfqllkhw2hdmasgnamhx1rfk1p45gcrd5zfc132l78rcps") (f (quote (("gecko-in-tree"))))))

(define-public crate-cubeb-sys-0.5.0 (c (n "cubeb-sys") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1apjl2z90drybb9wmkzazq0fkbyfx0jmk3lrlszpq6iczx6ss1g4") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.5.1 (c (n "cubeb-sys") (v "0.5.1") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "00kmsskdzbxlfd52vc6ydmjzyxgfbhkqcibwvaif5nrmblj9wfv5") (f (quote (("gecko-in-tree"))))))

(define-public crate-cubeb-sys-0.5.2 (c (n "cubeb-sys") (v "0.5.2") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1w2j19x11qrw67xk9gxjywkhm2y5f4lfcqcz9z8ri2x8l6ygh0gh") (f (quote (("gecko-in-tree"))))))

(define-public crate-cubeb-sys-0.5.3 (c (n "cubeb-sys") (v "0.5.3") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1a61wdamn0hqyv39xhcn87pd4axc0lsv1wqam2zfp24p9bh37xh1") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.5.4 (c (n "cubeb-sys") (v "0.5.4") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "15fz1vx8ln5vj292q3mrv5h0bzwq14vxlm3qgp7mj0dr2w6599rv") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.5.5 (c (n "cubeb-sys") (v "0.5.5") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0fqahv141917zw79a50lx73hmp6yb90baal82pw55pnmsrh9p39i") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.5.6 (c (n "cubeb-sys") (v "0.5.6") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1s4p2q5dy7478v04yjha5la2pl9ffjbbpx5sh4xpb50dlzv850sq") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.6.0 (c (n "cubeb-sys") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1zgq10wql34akda070x16vch562k1qygjyfp6m0nzwfgglypbh73") (f (quote (("gecko-in-tree")))) (y #t) (l "cubeb")))

(define-public crate-cubeb-sys-0.6.1 (c (n "cubeb-sys") (v "0.6.1") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07355wyjxpqrd6kjscbfga2y7416nzk119qcgfc7kf40mfmiwlgn") (f (quote (("gecko-in-tree")))) (y #t) (l "cubeb")))

(define-public crate-cubeb-sys-0.6.2 (c (n "cubeb-sys") (v "0.6.2") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1mm3h1dxacvaakbl4hwqm4jlnrg4rdk8ac5xcf1w00zsqlwmi71h") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.7.0 (c (n "cubeb-sys") (v "0.7.0") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "145fvschwz9dgjykwcfa62r3cp1fk77fnglyryx2mxncnqp5dg6w") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.8.0 (c (n "cubeb-sys") (v "0.8.0") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1naayx0gb330ayblrlqj2sm27ygvkjwi5n6gkdc1y0zf58gpa3sv") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.9.0 (c (n "cubeb-sys") (v "0.9.0") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "15l7h4xd19fisjnj0mcirpiizqa27va5yfzwbv254jhrqridivga") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.9.1 (c (n "cubeb-sys") (v "0.9.1") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "151prr8q3zcmba87zdkdn8bha8dv2x7q2aljd62h5dxc7jhsy9zy") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.10.0 (c (n "cubeb-sys") (v "0.10.0") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "061qy15f12mvh1pnyvrczjkkyj43kpq2x47li2a6wll5jwrn33ys") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.10.1 (c (n "cubeb-sys") (v "0.10.1") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1vnm2b61l6kpg09plig9fk7n0gbgdahrwr4hlwdjnmgzmhl99d1k") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.10.2 (c (n "cubeb-sys") (v "0.10.2") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "18mkr979zlwclcxgmqsnb1y77251zqhz9gjx1w3dafz6h63xw4gq") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.10.3 (c (n "cubeb-sys") (v "0.10.3") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dj9x6r59qksm3gcanv2nrlkbwkrwx52k6y5yp5wawhc4qrpz7yk") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.10.5 (c (n "cubeb-sys") (v "0.10.5") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0zlsi4z57lx5whgk317kdhvdbfl7m20khqvidd6ns4sl18hipz03") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.10.6 (c (n "cubeb-sys") (v "0.10.6") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lv4dg6grgf9fki38dcf2rpr09fkcn03awb4h83llnc64j1yjl1b") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.10.7 (c (n "cubeb-sys") (v "0.10.7") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "13ycl6sps94aa5liz7gj9d4k6x0yjhsxr6ncdnpv9hsndcznaa6h") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.12.0 (c (n "cubeb-sys") (v "0.12.0") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07byl2cmg0w1ifcp598pk6g0nhmljsf7b70s1ipaskdksxbw881w") (f (quote (("gecko-in-tree")))) (l "cubeb")))

(define-public crate-cubeb-sys-0.13.0 (c (n "cubeb-sys") (v "0.13.0") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0fdb9kwkv45v5j495q371nd60mqmj9b9z0q448ks8svv1kakq1r6") (f (quote (("gecko-in-tree")))) (l "cubeb")))

