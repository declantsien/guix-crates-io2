(define-module (crates-io cu be cubers) #:use-module (crates-io))

(define-public crate-cubers-0.1.0 (c (n "cubers") (v "0.1.0") (h "02f23n4cjb3mscj7l92f0mnxr6grc6nyj1wiv540rxxnmxckm24x")))

(define-public crate-cubers-0.1.1 (c (n "cubers") (v "0.1.1") (h "0h9jglp1pq25sqjsipw44k5zmw6gnw9h0hljny50v6h1wp1wzkk1")))

