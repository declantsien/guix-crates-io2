(define-module (crates-io cu be cubeio) #:use-module (crates-io))

(define-public crate-cubeio-0.1.0 (c (n "cubeio") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 2)))) (h "1a3zf6lvbdmn9vanh2nb5k9ccxiq5qj3dw7rjwpq3pp3azz7l0yy")))

(define-public crate-cubeio-0.2.0 (c (n "cubeio") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 2)))) (h "0hm1jb4lp915ziqaky57jcyj0x4m9007q73gm8gca7r3rf4fpwl0")))

(define-public crate-cubeio-0.2.1 (c (n "cubeio") (v "0.2.1") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 2)))) (h "0da77q5v44ig3r7d1j7543l7s2w4lrz8ysqgpd0hckj68h28k4yv")))

(define-public crate-cubeio-0.2.2 (c (n "cubeio") (v "0.2.2") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 2)))) (h "1509r98a76a3m7mslywm2ha9aa4kq9kvkw5x5hfxj87w2ywaph39")))

(define-public crate-cubeio-0.2.3 (c (n "cubeio") (v "0.2.3") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 2)))) (h "15pvf5xb3bgbj4fzr4rg4rdnpi91mayi84kp4k64rc622f14cxa4")))

(define-public crate-cubeio-0.2.4 (c (n "cubeio") (v "0.2.4") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 2)))) (h "1vnwrqs8z7fs2sx714fflfzgsm353mqd8qpr204miciwk22jmwdg")))

