(define-module (crates-io cu be cube_scrambler) #:use-module (crates-io))

(define-public crate-cube_scrambler-0.1.0 (c (n "cube_scrambler") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y635v5jdjsyhp5h3fkn1ibnrgm5pjz7ymim1r0s4h4z54xyh62m")))

(define-public crate-cube_scrambler-0.1.1 (c (n "cube_scrambler") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jdb0sxwvvp9b6j857f7m1rq82sw6320c5n0d547dmsp188zlq6l")))

(define-public crate-cube_scrambler-0.1.2 (c (n "cube_scrambler") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12875fcg69f0s80lxixma155r0fv8q0mikzax9glxfg1wcqfsymb")))

(define-public crate-cube_scrambler-0.1.3 (c (n "cube_scrambler") (v "0.1.3") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jl6mqjhqj7iv4zdlmxmy76mvijd7xf53vw23zg7kfjw6brl60p2")))

(define-public crate-cube_scrambler-0.2.0 (c (n "cube_scrambler") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1imacl1q7ngrq5i0lfimsq1k4w9dxxjjmizxbww0kclszi31zqz7")))

(define-public crate-cube_scrambler-0.2.1 (c (n "cube_scrambler") (v "0.2.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wd1bfg4wakwzwpwqpb6vy2ja966grmcahnkdzxnhhgq1s0kacpk")))

