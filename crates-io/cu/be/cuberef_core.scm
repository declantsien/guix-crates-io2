(define-module (crates-io cu be cuberef_core) #:use-module (crates-io))

(define-public crate-cuberef_core-0.0.1 (c (n "cuberef_core") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tonic") (r "^0.9.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.1") (d #t) (k 1)) (d (n "vec3D") (r "^0.3.0") (d #t) (k 0)))) (h "1i652syvjmw9pk1y2jgsi1zq7qd3dk1r3ripvaagjhq5l2ccf3s9")))

