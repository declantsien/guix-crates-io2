(define-module (crates-io cu ba cuba) #:use-module (crates-io))

(define-public crate-cuba-0.1.0 (c (n "cuba") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1kgiac66ps03sbidgbk880vqjig1kpzg1dvhb866nrvzxwv99awn")))

(define-public crate-cuba-0.1.1 (c (n "cuba") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0q92i8a1qb8zaj0h3vn81cw55jp6c8ml7iws8lakwkx403ixhhr9")))

(define-public crate-cuba-0.1.2 (c (n "cuba") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "16m95fmqc4nwkq1zpgsfkj1m43kr10adrqsnh4sr8gllx45plyxy")))

(define-public crate-cuba-0.1.3 (c (n "cuba") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0j826an83k7vhfk7srfr145749amqn7r4dcy8pb7ahy23g71469l")))

(define-public crate-cuba-0.1.4 (c (n "cuba") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "10nm59cn115w0y9f11frdzrmp4vbz1s0xkvks1px4y6ngs7lzzsz")))

(define-public crate-cuba-0.1.5 (c (n "cuba") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "18f3lyvfllhfyy1j4yqdl9rl1755f42wcrsyp5gfj3lqxz63qdwi")))

(define-public crate-cuba-0.1.6 (c (n "cuba") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0himl692a7md5dnwr12rwpfh8h6rr5b1fk8pyhlp9s1dng7d6asl")))

(define-public crate-cuba-0.1.7 (c (n "cuba") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0nzarj4cgx9di9gsqvw54am8zwhjblddnn50410a8dyri06smk77")))

(define-public crate-cuba-0.1.8 (c (n "cuba") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0vshacnwvmpsx02z1hv8fc4fwrjik2nz1jdlsk711i0z0wvnhq4j")))

(define-public crate-cuba-0.1.9 (c (n "cuba") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0ycj3dxmdxf7mr0plr0w9nmx07wva6bbr5z6g8c946lhgxrajm3d")))

(define-public crate-cuba-0.1.10 (c (n "cuba") (v "0.1.10") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "09ynmz2sc7p933swc2h8cdfqpkwdsj7r60zfgn18xzcycvqlayxk")))

(define-public crate-cuba-0.1.11 (c (n "cuba") (v "0.1.11") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "00drglh65fg4lwh55bcsrqbz25dak3xjwa51kg8929hbsb7cnmsd")))

(define-public crate-cuba-0.1.12 (c (n "cuba") (v "0.1.12") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0k2i3hgk3gamhi1ir400z66dnk824y55l9qy3ndb8gn36gh96qph")))

(define-public crate-cuba-0.1.13 (c (n "cuba") (v "0.1.13") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0fc4086kvrgw37hrabzcyg1xpgsl9vi7akm0zav4f94k1pfgvsck")))

(define-public crate-cuba-0.1.14 (c (n "cuba") (v "0.1.14") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1i2n4y3qls2a7a8r1fjh4z334m00r8cw8cwwar9jl8kd8vj7brpm")))

(define-public crate-cuba-0.2.1 (c (n "cuba") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0dw6hmkxcvyrdxrdwin9dwpdclmc7f33acyp8d7wpg4j36r8idf3")))

(define-public crate-cuba-0.2.2 (c (n "cuba") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "13jhlx2lr01bm0dkqxm9v33pnb211j422nl81laarpgsw6vqxqi9")))

(define-public crate-cuba-0.2.3 (c (n "cuba") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0l00ll52sr8s0pv57mx62yqh0zgi2i07absszd1qizz273dhjp1x")))

(define-public crate-cuba-0.2.4 (c (n "cuba") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1fy321kmi475vv3n7gniqrbmmkbm1k0f3hl1i8dbrdx73lq1ijq9")))

