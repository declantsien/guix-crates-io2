(define-module (crates-io cu -s cu-sys) #:use-module (crates-io))

(define-public crate-cu-sys-0.1.0 (c (n "cu-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qcs73a6r087vp94g9b252r922ad3fpb5yv0qfsdd7h77hqqydr1")))

