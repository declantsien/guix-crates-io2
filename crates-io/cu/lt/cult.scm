(define-module (crates-io cu lt cult) #:use-module (crates-io))

(define-public crate-cult-0.0.1 (c (n "cult") (v "0.0.1") (h "1l0dzgj3nxs73mh4fw1cs7gclmbfdjc8l6yakw3fhwia9hggfrdf")))

(define-public crate-cult-0.0.2 (c (n "cult") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0yghgzwljix7yp8wm05yqzfbjymwsv78hbxlbald16hbsvkzavyz")))

(define-public crate-cult-0.0.3 (c (n "cult") (v "0.0.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1pphj9gk4rcli2zh5vj71kn7d0z1wp9b6i459869r9ihl5g2va6y")))

