(define-module (crates-io cu te cute_custom_default) #:use-module (crates-io))

(define-public crate-cute_custom_default-1.0.0 (c (n "cute_custom_default") (v "1.0.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "18kwy6593s9ahi4na1wg48d0a3izb592jwbfgf0vsh6afjn20y4j") (y #t)))

(define-public crate-cute_custom_default-2.0.0 (c (n "cute_custom_default") (v "2.0.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1x6syn1chix8y62zg20snvi8py6jzdabz62rkpr0qf8svhw4szfn")))

(define-public crate-cute_custom_default-2.1.0 (c (n "cute_custom_default") (v "2.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1if4vnnvi3d6z3vxrw03r1ar35ixhfijgibwmmigscr88jzilhzd")))

