(define-module (crates-io cu te cutefetch) #:use-module (crates-io))

(define-public crate-cutefetch-0.1.0 (c (n "cutefetch") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.4") (d #t) (k 0)) (d (n "whoami") (r "^1.4.0") (d #t) (k 0)))) (h "1xhj1dnbmk10k6wsmm1f4paakcm49di8j93kqnzxna3i6vnmaary")))

(define-public crate-cutefetch-0.1.1 (c (n "cutefetch") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.4") (d #t) (k 0)) (d (n "whoami") (r "^1.4.0") (d #t) (k 0)))) (h "03b5rwqf6kkm5phzh613zwr20qcxrdrqjfwafvvvhxnqppc12ha3")))

(define-public crate-cutefetch-0.1.2 (c (n "cutefetch") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.4") (d #t) (k 0)) (d (n "whoami") (r "^1.4.0") (k 0)))) (h "162xsad28n262w1g779m61wjw1hsv9695k0aybk59j3f223bp0ga")))

(define-public crate-cutefetch-0.2.0 (c (n "cutefetch") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "machine-info") (r "^1.0.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "uptime_lib") (r "^0.2.2") (d #t) (k 0)) (d (n "whoami") (r "^1.4.0") (k 0)))) (h "0lx38pikl1ah1hgmy5h8xxh11cr1z1xscryvbib6q37hhmr5prwn")))

