(define-module (crates-io cu te cute_print) #:use-module (crates-io))

(define-public crate-cute_print-0.1.0 (c (n "cute_print") (v "0.1.0") (h "1d3v50k3zf2vsf7g5aa1dxn8mp8bp8nfl7053na7ma0z04wllj71")))

(define-public crate-cute_print-0.1.4 (c (n "cute_print") (v "0.1.4") (h "1cqgxmqki7vyfbri8z04a5f504xc9zi9y7ibhfkdm8ghvrg9yr9p")))

(define-public crate-cute_print-0.2.0 (c (n "cute_print") (v "0.2.0") (h "0mpdcyh5cjz9r40scsw0za72vxllwrvvy5n41lwgbmhd4x3b2l08")))

(define-public crate-cute_print-0.2.1 (c (n "cute_print") (v "0.2.1") (h "0r6pqrzdmnz7m0p3fmgrxna2akfd5yj5d2gdvipbxnpl8hvbspkp") (r "1.56")))

(define-public crate-cute_print-0.3.0 (c (n "cute_print") (v "0.3.0") (h "0spiridaj04g2jrc9w6pwl7lpfp4pm6hfgsah9ah2if75sp0d9gm") (r "1.56")))

(define-public crate-cute_print-0.4.0 (c (n "cute_print") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "processenv" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1ygk8mkx0pwyx2n5fpfdswjla86r2zm68h86g6jnqnz6zkwnrp8v") (r "1.56")))

(define-public crate-cute_print-0.5.0 (c (n "cute_print") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "processenv" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0rsvv51xfcsvv30b6sx3g3lriw2crsjj7mm3qzk06fm6lyhdpm2m") (r "1.56")))

(define-public crate-cute_print-0.6.1 (c (n "cute_print") (v "0.6.1") (d (list (d (n "libc") (r "^0.2.139") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "processenv" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1ff55bmrhwq482rzb441pmal7k7gg07vfj1nax4nkqvswjqxyjjl") (r "1.56")))

(define-public crate-cute_print-0.7.0 (c (n "cute_print") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "processenv" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1mgn4rigwpsnmzdfk1jw2dn1bhn8grfc7jyrpjpa3z68jikd1d2c") (r "1.56")))

