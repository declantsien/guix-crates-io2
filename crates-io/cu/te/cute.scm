(define-module (crates-io cu te cute) #:use-module (crates-io))

(define-public crate-cute-0.1.0 (c (n "cute") (v "0.1.0") (h "0wbdwc093lp1l9gnf5kzswrlgv7x8ayvs3cgncbcg5lf7jml9031")))

(define-public crate-cute-0.1.1 (c (n "cute") (v "0.1.1") (h "0n0pg84jwla445kr130yfqpcaxsydga8kpp2cgczkb57i2x09apk")))

(define-public crate-cute-0.1.2 (c (n "cute") (v "0.1.2") (h "1k7gc93ib68m111njf7gllhay04yrzzjil0sqk1q41rsvyqcq12k")))

(define-public crate-cute-0.1.3 (c (n "cute") (v "0.1.3") (h "164dmxjdvdrr4fl6f8532cyx8zfibh3dmicrbkamrmh0mhykn72q")))

(define-public crate-cute-0.2.0 (c (n "cute") (v "0.2.0") (h "1ja3k26flqykg8arwy7p1mw4vic8vbbkflygb9ixi6zm723pmx1g")))

(define-public crate-cute-0.2.1 (c (n "cute") (v "0.2.1") (h "15fwr42dlqshd046wjz7ybcylambn5ywii1xr7b46znkg00ilm46")))

(define-public crate-cute-0.3.0 (c (n "cute") (v "0.3.0") (h "1za9nkkb2w9zwbx5diaqd9al0c5rxvgv4yayd6dymzn3s7101rs5")))

