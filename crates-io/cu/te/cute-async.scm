(define-module (crates-io cu te cute-async) #:use-module (crates-io))

(define-public crate-cute-async-0.1.0 (c (n "cute-async") (v "0.1.0") (h "1gs3p10qz8pp0ph8jcwdwcqalp8b5ybw070gjvrhgbvcg016x60i")))

(define-public crate-cute-async-0.2.0 (c (n "cute-async") (v "0.2.0") (h "101z54fygmnif5mg2dhb2i2pyk2ciaarra4xvb2v9jxfib7ayq20")))

(define-public crate-cute-async-0.3.0 (c (n "cute-async") (v "0.3.0") (d (list (d (n "tokio-net") (r "^0.2.0-alpha") (o #t) (k 0)) (d (n "tokio-net") (r "^0.2.0-alpha") (f (quote ("tcp"))) (d #t) (k 2)))) (h "1060v3zipi6sr9blhj2lw33w2hqzm7zqf5nmncvz3y921d5kqp08") (f (quote (("tokio" "tokio-net"))))))

(define-public crate-cute-async-0.3.1 (c (n "cute-async") (v "0.3.1") (d (list (d (n "tokio-net") (r "^0.2.0-alpha") (o #t) (k 0)) (d (n "tokio-net") (r "^0.2.0-alpha") (f (quote ("tcp"))) (d #t) (k 2)))) (h "1cyzgaqcmxck4iv63z8a4gfv2yvpxhlnp9hjpqhgahvya3c9jvrx") (f (quote (("tokio" "tokio-net"))))))

(define-public crate-cute-async-0.3.2 (c (n "cute-async") (v "0.3.2") (d (list (d (n "tokio-net") (r "^0.2.0-alpha") (o #t) (k 0)) (d (n "tokio-net") (r "^0.2.0-alpha") (f (quote ("tcp"))) (d #t) (k 2)))) (h "06lr65v6lfry0rs8mkj2blkx3kb363hwddbk9jw5f1765l3k06ik") (f (quote (("tokio" "tokio-net"))))))

(define-public crate-cute-async-0.3.3 (c (n "cute-async") (v "0.3.3") (d (list (d (n "tokio-net") (r "^0.2.0-alpha") (o #t) (k 0)) (d (n "tokio-net") (r "^0.2.0-alpha") (f (quote ("tcp"))) (d #t) (k 2)))) (h "0pkx9bbh0njrilqc8ximazrz0blmb47bg8wvlkwvaqbdkj3ml67s") (f (quote (("tokio" "tokio-net"))))))

(define-public crate-cute-async-0.4.0 (c (n "cute-async") (v "0.4.0") (h "1k76lad3h8nn5zfdcrznzm4ka5bcjjvlq1iz97qibxlnn5igwdws") (f (quote (("default"))))))

