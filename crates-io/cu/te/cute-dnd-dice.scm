(define-module (crates-io cu te cute-dnd-dice) #:use-module (crates-io))

(define-public crate-cute-dnd-dice-1.0.0 (c (n "cute-dnd-dice") (v "1.0.0") (d (list (d (n "rand") (r "^0.5") (k 0)))) (h "1nwdxgcgbjqwxry82sa70irfa1734zy16w9jvgzg1p24f7jwham1") (f (quote (("std" "rand/std") ("default" "std"))))))

(define-public crate-cute-dnd-dice-1.0.1 (c (n "cute-dnd-dice") (v "1.0.1") (d (list (d (n "rand") (r "^0.5") (k 0)))) (h "1i3bkdr6smqj12kki70cby9bmpbgzg22m08miydb6klhr182jz68") (f (quote (("std" "rand/std") ("default" "std"))))))

(define-public crate-cute-dnd-dice-2.0.0 (c (n "cute-dnd-dice") (v "2.0.0") (d (list (d (n "rand") (r "^0.5") (k 0)))) (h "1dzwpc9dr0na0zfhxrrvyzral1ssyvh44arfnraqxhg6gggx6187") (f (quote (("std" "rand/std") ("default" "std"))))))

(define-public crate-cute-dnd-dice-2.1.0 (c (n "cute-dnd-dice") (v "2.1.0") (d (list (d (n "rand") (r "^0.7") (k 0)))) (h "0pjnzlp0wi8wkygh2f2m964mrynd2wrdvlzcrv73lj9aq1b0hc4n") (f (quote (("std" "rand/std") ("default" "std"))))))

(define-public crate-cute-dnd-dice-3.0.0 (c (n "cute-dnd-dice") (v "3.0.0") (h "0wif156s5psraxll4ml1ig1f3mrdvyb729ghl47wcs6kh86sg3h2")))

(define-public crate-cute-dnd-dice-3.1.0 (c (n "cute-dnd-dice") (v "3.1.0") (h "1rf8hzzyayw50g73dk9clz63ihr4m6mfg0x124czl6y8bslq9kzy")))

(define-public crate-cute-dnd-dice-4.0.0 (c (n "cute-dnd-dice") (v "4.0.0") (d (list (d (n "getrandom") (r "^0.2") (k 0)))) (h "1pvizf58apl09694662xwdi573hfgz8qn2yhv5fw8qpy980yqz5h") (f (quote (("std"))))))

