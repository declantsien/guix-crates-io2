(define-module (crates-io cu eb cueball-static-resolver) #:use-module (crates-io))

(define-public crate-cueball-static-resolver-0.3.0 (c (n "cueball-static-resolver") (v "0.3.0") (d (list (d (n "cueball") (r "^0.3.0") (d #t) (k 0)) (d (n "cueball-tcp-stream-connection") (r "^0.3.0") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (d #t) (k 2)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 2)))) (h "1k9pm92s6kv7rx7cbmdq6r8dlgv8w9rg1kld390ck3bv3rx4cjcv")))

(define-public crate-cueball-static-resolver-0.3.1 (c (n "cueball-static-resolver") (v "0.3.1") (d (list (d (n "cueball") (r "^0.3.5") (d #t) (k 0)) (d (n "cueball-tcp-stream-connection") (r "^0.3.0") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (d #t) (k 2)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 2)))) (h "0xhinrrrgg2yqpvszn1pyajaa6g83j6008d870c8y4n0fi8dkm27")))

