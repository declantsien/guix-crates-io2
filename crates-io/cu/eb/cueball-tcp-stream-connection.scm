(define-module (crates-io cu eb cueball-tcp-stream-connection) #:use-module (crates-io))

(define-public crate-cueball-tcp-stream-connection-0.3.0 (c (n "cueball-tcp-stream-connection") (v "0.3.0") (d (list (d (n "cueball") (r "^0.3.0") (d #t) (k 0)))) (h "0qrls6ypyld0n50wzb3562iinglqdbsjr1a5aah1hms7i2ks72dd")))

(define-public crate-cueball-tcp-stream-connection-0.3.1 (c (n "cueball-tcp-stream-connection") (v "0.3.1") (d (list (d (n "cueball") (r "^0.3.5") (d #t) (k 0)))) (h "1l9ljj2wm8f6k4ncigywllnpb2lhnjck6fs8v0mmcvyyvhx04i1z")))

