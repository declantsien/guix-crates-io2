(define-module (crates-io cu dd cudd) #:use-module (crates-io))

(define-public crate-cudd-0.1.0 (c (n "cudd") (v "0.1.0") (d (list (d (n "cudd-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0iyx7q476bdycd83g2slbi09ylw1zhmnyfqpra410g6k3iyf8dr8")))

(define-public crate-cudd-0.1.1 (c (n "cudd") (v "0.1.1") (d (list (d (n "cudd-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0kj7jqrir6a559vncmn1hqh5n6mnq51a7ncxv8f2kz1xzs5xskhg")))

(define-public crate-cudd-0.1.2 (c (n "cudd") (v "0.1.2") (d (list (d (n "cudd-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0bj3292w00c2aw0yvqx07spvz2izxsdfppz2d7r254nyybd0hqqp")))

(define-public crate-cudd-0.1.3 (c (n "cudd") (v "0.1.3") (d (list (d (n "cudd-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0z7i2bav0kv2syykhrycg7ysk1mciarhbzr0y8d931nm5w8rkjqd")))

(define-public crate-cudd-0.1.4 (c (n "cudd") (v "0.1.4") (d (list (d (n "cudd-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "libc-stdhandle") (r "^0.1.0") (d #t) (k 0)))) (h "081sxq9rbdrijdcqg2lw4i19amh908nzhyn7i4b6m2nabm5yqpis")))

