(define-module (crates-io cu dd cudd-sys) #:use-module (crates-io))

(define-public crate-cudd-sys-0.1.0 (c (n "cudd-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "00992jakq5apghgxk6m4c4k155xlkgscajmc6f2g4x3p51zlxlq5")))

(define-public crate-cudd-sys-0.1.1 (c (n "cudd-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0763aslb1f220isd0imj97qihm100zhkzs0iialqybrwr3h70fin")))

(define-public crate-cudd-sys-1.0.0 (c (n "cudd-sys") (v "1.0.0") (d (list (d (n "autotools") (r "^0.2.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sh9yacjjazlky2nxh92jplzihcf8a0xv21445vv7gjs4j8vj16s") (f (quote (("default" "build_cudd") ("build_cudd"))))))

