(define-module (crates-io cu mi cumin) #:use-module (crates-io))

(define-public crate-cumin-0.9.9 (c (n "cumin") (v "0.9.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "0dmpznqlrpn19lp546ihfp0m4vclwwz4h2mkf8wyz0cmr6h31xih")))

(define-public crate-cumin-0.9.10 (c (n "cumin") (v "0.9.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1q02zw45maair40j6n2zmjm838bs4340n4c0zh928z0dnxivgavh")))

(define-public crate-cumin-0.9.13 (c (n "cumin") (v "0.9.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1b5djxzkrmjh2k8bmcbhx07jpln83n3133pgbg49fcdxxsyj6vf6")))

