(define-module (crates-io cu ps cups-sys) #:use-module (crates-io))

(define-public crate-cups-sys-0.1.0 (c (n "cups-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.28.0") (d #t) (k 1)))) (h "1ab3iwpmq91w5wpjhwcmykyynpd012hlr9s461fznxn3v9rk9hhb")))

(define-public crate-cups-sys-0.1.1 (c (n "cups-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.28.0") (d #t) (k 1)))) (h "173fgxdsc2mrnawijr1drjhmnvkrbk1j1x34gni2ns63kxy91bxb")))

(define-public crate-cups-sys-0.1.2 (c (n "cups-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)))) (h "0xmb2jf7h14qq6w0crq7zk8858vymc5hb2dsfpskx9hz6sm2bx0s") (y #t)))

(define-public crate-cups-sys-0.1.3 (c (n "cups-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)))) (h "01mysb4xkibqbcjakik7ify9z2ds2x1k4rwqxy26n4rpcs5diji1")))

(define-public crate-cups-sys-0.1.4 (c (n "cups-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)))) (h "0lkbsb01g14ddyqa3xaq3yk4gspzswg3p5ma7y2mdwfwhxr5nx9i")))

