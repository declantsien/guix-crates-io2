(define-module (crates-io cu st custom-slice-macros) #:use-module (crates-io))

(define-public crate-custom-slice-macros-0.1.0 (c (n "custom-slice-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "18ijy2n3flapxnmcygwxfal20kjs0ciihb9kvparfcnkaa10vxl5")))

(define-public crate-custom-slice-macros-0.1.1 (c (n "custom-slice-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0mkch79bbaavkmzxfchn3imcxxwvd63radny9znfx0c49wln7dzc")))

