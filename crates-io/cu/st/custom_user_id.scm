(define-module (crates-io cu st custom_user_id) #:use-module (crates-io))

(define-public crate-custom_user_id-0.0.1 (c (n "custom_user_id") (v "0.0.1") (d (list (d (n "uuid") (r "^1.2.1") (f (quote ("v4" "fast-rng" "macro-diagnostics" "js"))) (d #t) (k 0)))) (h "0yhixy0zlw7dhpmsybbdrjri1mj5szhx98qa9nl6wjdhy9v8yxl0") (y #t)))

(define-public crate-custom_user_id-0.0.2 (c (n "custom_user_id") (v "0.0.2") (d (list (d (n "uuid") (r "^1.2.1") (f (quote ("v4" "fast-rng" "macro-diagnostics" "js"))) (d #t) (k 0)))) (h "1cchy9017fg37m4zsfj62z94yr2xwh4l5rhv2pqvv5c7bh63nvfw") (y #t)))

(define-public crate-custom_user_id-0.0.3 (c (n "custom_user_id") (v "0.0.3") (d (list (d (n "uuid") (r "^1.2.1") (f (quote ("v4" "fast-rng" "macro-diagnostics" "js"))) (d #t) (k 0)))) (h "1bkn10ps1cwy6slfhn8hrvxbr0cms0q5r4gbp5kk3h9nvpmbxr4d")))

