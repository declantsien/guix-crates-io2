(define-module (crates-io cu st custom-format) #:use-module (crates-io))

(define-public crate-custom-format-0.0.1 (c (n "custom-format") (v "0.0.1") (h "1sj48j19fafnzsx0a0lh9hvj4ivl8d3v3jlpwpjhqq8jjknlhbil")))

(define-public crate-custom-format-0.0.2 (c (n "custom-format") (v "0.0.2") (d (list (d (n "custom-format-macros") (r "^0.0.2") (d #t) (k 0)))) (h "0pnwrv37gjrwir71h6dpb6mw4mxx3s0w13m31vxywyf41mr5yb39")))

(define-public crate-custom-format-0.0.3 (c (n "custom-format") (v "0.0.3") (d (list (d (n "custom-format-macros") (r "^0.0.3") (d #t) (k 0)))) (h "139yl6fbyglr29h8rk7rb962i3w2p78zlyrn953v3dxs3bkh7ipj")))

(define-public crate-custom-format-0.0.4 (c (n "custom-format") (v "0.0.4") (d (list (d (n "custom-format-macros") (r "^0.0.4") (d #t) (k 0)))) (h "00yz78kr9gsfnyp2vlcj7j8l46i2ssfz7q1lazrkynbjbrgic7z7") (f (quote (("runtime" "custom-format-macros/runtime") ("compile-time" "custom-format-macros/compile-time"))))))

(define-public crate-custom-format-0.0.5 (c (n "custom-format") (v "0.0.5") (d (list (d (n "custom-format-macros") (r "^0.0.5") (d #t) (k 0)))) (h "0fch1aq092fjzks6nf7p1f4hjavxcwhixldhw3kka25z4rhmz2d3") (f (quote (("runtime" "custom-format-macros/runtime") ("compile-time" "custom-format-macros/compile-time"))))))

(define-public crate-custom-format-0.0.6 (c (n "custom-format") (v "0.0.6") (d (list (d (n "custom-format-macros") (r "^0.0.6") (d #t) (k 0)))) (h "1djg7h742kx1bj7kwwv09xbf4i9cnjm5aq4da0d5q48cv086i1z3") (f (quote (("runtime" "custom-format-macros/runtime") ("compile-time" "custom-format-macros/compile-time"))))))

(define-public crate-custom-format-0.1.0 (c (n "custom-format") (v "0.1.0") (d (list (d (n "custom-format-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0idybs03yiimxz86fvpv6qp860mxkpfj11g9l42cy0r15alxd6p1") (f (quote (("runtime" "custom-format-macros/runtime") ("compile-time" "custom-format-macros/compile-time"))))))

(define-public crate-custom-format-0.1.1 (c (n "custom-format") (v "0.1.1") (d (list (d (n "custom-format-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1llvgfrw9c72nf0syhfzdsi4ivz54cawm0yd4rr08i99m6js419f") (f (quote (("runtime" "custom-format-macros/runtime") ("compile-time" "custom-format-macros/compile-time"))))))

(define-public crate-custom-format-0.1.2 (c (n "custom-format") (v "0.1.2") (d (list (d (n "custom-format-macros") (r "^0.1.2") (k 0)))) (h "13rqhhsalllqfnd66cpkqg8pppa7f86xp5sc9m6sn2r8ba1k1lzh") (f (quote (("runtime" "custom-format-macros/runtime") ("default" "better-parsing") ("compile-time" "custom-format-macros/compile-time") ("better-parsing" "custom-format-macros/better-parsing"))))))

(define-public crate-custom-format-0.2.0 (c (n "custom-format") (v "0.2.0") (d (list (d (n "custom-format-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0lnnyjasx9xnf7nda5kgcfvhyzirnn496srh4iy1x1rf4n1jm3h4") (f (quote (("runtime") ("default" "compile-time" "runtime") ("compile-time"))))))

(define-public crate-custom-format-0.3.0 (c (n "custom-format") (v "0.3.0") (d (list (d (n "custom-format-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0li5z9qxbd6rhf6bjqxrky2vsbzr9bzmfm7cl27svvqqrrp89hyz") (f (quote (("runtime") ("default" "compile-time" "runtime") ("compile-time"))))))

(define-public crate-custom-format-0.3.1 (c (n "custom-format") (v "0.3.1") (d (list (d (n "custom-format-macros") (r "^0.3.1") (d #t) (k 0)))) (h "0yvmaykmv885gf0gikw5fa6npa3s1irgblyyshl1nr2mzj79gsk9") (f (quote (("runtime") ("default" "compile-time" "runtime") ("compile-time"))))))

