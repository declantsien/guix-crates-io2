(define-module (crates-io cu st custom-try-proc_macros) #:use-module (crates-io))

(define-public crate-custom-try-proc_macros-0.1.0-rc1 (c (n "custom-try-proc_macros") (v "0.1.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1xfzy32cj2s6201gviigk92x66yvax5z5mj28c3qg044glaa98sy")))

