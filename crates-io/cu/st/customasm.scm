(define-module (crates-io cu st customasm) #:use-module (crates-io))

(define-public crate-customasm-0.10.6 (c (n "customasm") (v "0.10.6") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)))) (h "07z73s07d485v4qzxvkr1psi5qz67gracgcf9bv6jj1w9g826rnz")))

(define-public crate-customasm-0.11.0 (c (n "customasm") (v "0.11.0") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "0wpiq153wfa8kdy3plrxm8qd1m7xwy6ayc7x8gf0pkj8s84v5c05")))

(define-public crate-customasm-0.11.1 (c (n "customasm") (v "0.11.1") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "1kbwrrwmjafzfksl730666sdka9gc064snqicj4ldkdyhiqiwf4q")))

(define-public crate-customasm-0.11.2 (c (n "customasm") (v "0.11.2") (d (list (d (n "getopts") (r ">=0.2.17, <0.3.0") (d #t) (k 0)) (d (n "num-bigint") (r ">=0.1.0, <0.2.0") (k 0)) (d (n "num-integer") (r ">=0.1.0, <0.2.0") (k 0)) (d (n "num-traits") (r ">=0.1.0, <0.2.0") (k 0)) (d (n "sha2") (r ">=0.9.1, <0.10.0") (d #t) (k 2)) (d (n "vergen") (r ">=3.1.0, <4.0.0") (d #t) (k 1)))) (h "073ci5yrd1kr4n1lb5g8rpzj3vzkp57pmkyrrpkm9zh3zz0jqyv0")))

(define-public crate-customasm-0.11.3 (c (n "customasm") (v "0.11.3") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "13xlndakgclxpy6w73dx3151xs2dia827zail6bfcfsw488pgadb")))

(define-public crate-customasm-0.11.4 (c (n "customasm") (v "0.11.4") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "059cdd7adfxbh7rpimy2g9za2rqjs9affc2wc2j0pnl9sj2c2p4c")))

(define-public crate-customasm-0.11.5 (c (n "customasm") (v "0.11.5") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "04hcag524y0yydag4aqvj9cdsk7gjpfxm17vbzpc8yih8531dy64")))

(define-public crate-customasm-0.11.6 (c (n "customasm") (v "0.11.6") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "0ydh8fwzkbl0zafqibw9cdz2iqal986mk819dlb90icni6035fij")))

(define-public crate-customasm-0.11.7 (c (n "customasm") (v "0.11.7") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "17yz41s5043lml10xv3049yy4dbgihv83rl4pnd5ndmdkr7pa3is")))

(define-public crate-customasm-0.11.8 (c (n "customasm") (v "0.11.8") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "1j0hvv4dbrwk1kpanbf8xza1qw33frg6n1djyadn1dq58wqdayg6")))

(define-public crate-customasm-0.11.9 (c (n "customasm") (v "0.11.9") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "14c54c6lkf5w9k12w3h3qdnzf9ix09dhcbjzcgfp0syhqac70bhl")))

(define-public crate-customasm-0.11.10 (c (n "customasm") (v "0.11.10") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "1f3664dshgj1zwk0yhfxfc5i374ilvbrbwrv0439qb8k44m2gx1q")))

(define-public crate-customasm-0.11.11 (c (n "customasm") (v "0.11.11") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "1yr5h13cihx6gmyjlk7h7ban0fm21gzbaviss3jpb4bhybil0d7a")))

(define-public crate-customasm-0.11.12 (c (n "customasm") (v "0.11.12") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "054p28l6crbb4shlh988xi6mf639m83kyhb2ksy0571wkc1i0cw7")))

(define-public crate-customasm-0.11.13 (c (n "customasm") (v "0.11.13") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "0wc5g4q1x4y2jirdcdn3f9mynmlfscfwsabcnr75dq7izaibawin")))

(define-public crate-customasm-0.11.14 (c (n "customasm") (v "0.11.14") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "1hjdyn83rs3sny0pc8ig9kxab7krwv30xxwajajdwrkbsxwybv2w")))

(define-public crate-customasm-0.11.15 (c (n "customasm") (v "0.11.15") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "1dam2bbyy1ad841qxhcd4axa2y5pyz77hlyvwiwyrblmkav862qp")))

(define-public crate-customasm-0.12.0 (c (n "customasm") (v "0.12.0") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "14gj1jkfp8anhy8j2a0s5bn2272r4lpwmjhhngj3ki8i49d4hwka")))

(define-public crate-customasm-0.13.0 (c (n "customasm") (v "0.13.0") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "0bar4kw20hx51n943ybi3bnf5rz1rawc7vgvik0iiwavqg61s9dw")))

(define-public crate-customasm-0.13.1 (c (n "customasm") (v "0.13.1") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "0w2f9gydd0mdfv7f5j3afdgv8xcz0zy5hr8j56fb6l9c745ivpys")))

(define-public crate-customasm-0.13.2 (c (n "customasm") (v "0.13.2") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "1ff8vz60dyl8nnw4sandzckdydsdy5w5rhlvw621bccby4x3y7qf")))

(define-public crate-customasm-0.13.3 (c (n "customasm") (v "0.13.3") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "01rj0kvygfhl1yv4gp21g2drpbcdqv9s4lja91dmg4rmsksg1rvv")))

(define-public crate-customasm-0.13.4 (c (n "customasm") (v "0.13.4") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "0xi6rh8m2qd0k9bmrxskijhfx992qb1q1v8xk0d1vix645cpfi3z")))

(define-public crate-customasm-0.13.5 (c (n "customasm") (v "0.13.5") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "15n2m0a39wb69vi3ygihd8f4h2qkvr2mkjaav9v6y8lv6yljzw92")))

