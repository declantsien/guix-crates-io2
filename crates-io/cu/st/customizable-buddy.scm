(define-module (crates-io cu st customizable-buddy) #:use-module (crates-io))

(define-public crate-customizable-buddy-0.0.1 (c (n "customizable-buddy") (v "0.0.1") (h "0pr9q6rnndz9gm9qg0jv918mkdy0bpd946rnwhhns114grppiq9x")))

(define-public crate-customizable-buddy-0.0.2 (c (n "customizable-buddy") (v "0.0.2") (h "0bm609s7hwr5m875xvv8yj9arg07sg3jwz7jshhwl5w8fljn7kpa")))

(define-public crate-customizable-buddy-0.0.3 (c (n "customizable-buddy") (v "0.0.3") (h "1glnrgfjd68iwh5vhk8vn1g371fjgcysdyikih7jwr5v2d25hv3g")))

