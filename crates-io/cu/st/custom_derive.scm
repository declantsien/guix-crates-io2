(define-module (crates-io cu st custom_derive) #:use-module (crates-io))

(define-public crate-custom_derive-0.1.0 (c (n "custom_derive") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "0jrym38fghal9f1qhlhk4nik211gaygzswl2597kc81s5nkfymdm")))

(define-public crate-custom_derive-0.1.1 (c (n "custom_derive") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "05497nl54z4zb1y1wmjx40xn8qqsg5ahqhvw6pkss0vpi9vhh1gs")))

(define-public crate-custom_derive-0.1.2 (c (n "custom_derive") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "0rl4msqly04cyff87rk9fx217942h3193n2n0wlknk117xb781jb")))

(define-public crate-custom_derive-0.1.4 (c (n "custom_derive") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "0b26xfn75xi6l1mnz22mqjblwfb5fy6ljv18klriyhkypzn62fqc")))

(define-public crate-custom_derive-0.1.5 (c (n "custom_derive") (v "0.1.5") (d (list (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "09lcbwyygbjf6q01j0cl1g3xxyimd8apm240zz443s7nk6z644pa") (f (quote (("std") ("default" "std"))))))

(define-public crate-custom_derive-0.1.7 (c (n "custom_derive") (v "0.1.7") (d (list (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "1f81bavw1wnykwh21hh4yyzigs6zl6f6pkk9p3car8kq95yfb2pg") (f (quote (("std") ("default" "std"))))))

