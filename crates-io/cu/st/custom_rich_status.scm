(define-module (crates-io cu st custom_rich_status) #:use-module (crates-io))

(define-public crate-custom_rich_status-1.1.0 (c (n "custom_rich_status") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "discord-rpc-client") (r "^0.2") (d #t) (k 0)) (d (n "quoted_strings") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0yy201d0i3cxvgidm4168240x05vv10x1il7a1bzxs9lxkrkvsbx")))

