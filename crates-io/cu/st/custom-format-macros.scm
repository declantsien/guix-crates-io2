(define-module (crates-io cu st custom-format-macros) #:use-module (crates-io))

(define-public crate-custom-format-macros-0.0.2 (c (n "custom-format-macros") (v "0.0.2") (d (list (d (n "litrs") (r "^0.2.3") (k 0)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "11p8blj70ldnkc6l3xw2y7khnsl57pk3jzd2c25vbhp7wk6bi3al")))

(define-public crate-custom-format-macros-0.0.3 (c (n "custom-format-macros") (v "0.0.3") (d (list (d (n "litrs") (r "^0.2.3") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 2)))) (h "03nkvgb3irlz71nivx5r5qzjb8p6x0v9sddfd2vsrahk852x1chx")))

(define-public crate-custom-format-macros-0.0.4 (c (n "custom-format-macros") (v "0.0.4") (d (list (d (n "litrs") (r "^0.2.3") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 2)))) (h "0d0npran6z3f0wrlg6vasdsldmd9zl3dl9dabr0aps63qhzsc3k7") (f (quote (("runtime") ("compile-time"))))))

(define-public crate-custom-format-macros-0.0.5 (c (n "custom-format-macros") (v "0.0.5") (d (list (d (n "litrs") (r "^0.2.3") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 2)))) (h "1536ghfwr8644hys8h81al45rhdmzx8hnzwx4xavybr2m4x71rda") (f (quote (("runtime") ("compile-time"))))))

(define-public crate-custom-format-macros-0.0.6 (c (n "custom-format-macros") (v "0.0.6") (d (list (d (n "litrs") (r "^0.2.3") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 2)))) (h "1fh35470b2r771knw83kssa35mxkpz97cgy1sijn55whyj5d4ya2") (f (quote (("runtime") ("compile-time"))))))

(define-public crate-custom-format-macros-0.1.0 (c (n "custom-format-macros") (v "0.1.0") (d (list (d (n "litrs") (r "^0.2.3") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 2)))) (h "057mf49fi1dvis1jnm9dn01mq9ipa6lkw6iwchllv8lg1k7yfa9x") (f (quote (("runtime") ("compile-time"))))))

(define-public crate-custom-format-macros-0.1.1 (c (n "custom-format-macros") (v "0.1.1") (d (list (d (n "litrs") (r "^0.2.3") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 2)))) (h "1hqr6lvsy8xzmkanl2hn6smaj2fqa574z39660jxn8g075d8bq0b") (f (quote (("runtime") ("compile-time"))))))

(define-public crate-custom-format-macros-0.1.2 (c (n "custom-format-macros") (v "0.1.2") (d (list (d (n "litrs") (r "^0.2.3") (k 0)) (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (o #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing"))) (o #t) (k 0)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (k 2)))) (h "0ly49g7hninlfyqsaxs1qzs6g0lwh58z0zahh2l57a861nfgn9bx") (f (quote (("runtime") ("default" "better-parsing") ("compile-time") ("better-parsing" "syn" "quote" "proc-macro2"))))))

(define-public crate-custom-format-macros-0.2.0 (c (n "custom-format-macros") (v "0.2.0") (d (list (d (n "litrs") (r "^0.2.3") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (k 2)))) (h "0002hjmp42lw4nhcsf5kmphi8dpdzjii6hqmycp7m6wz2qrnmhh1")))

(define-public crate-custom-format-macros-0.3.0 (c (n "custom-format-macros") (v "0.3.0") (d (list (d (n "litrs") (r "^0.3.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (k 2)))) (h "0xwjb6mkn7h4qry125hf3i130lmn8dmggmr55jb6q87vy8rf3198")))

(define-public crate-custom-format-macros-0.3.1 (c (n "custom-format-macros") (v "0.3.1") (d (list (d (n "litrs") (r "^0.4.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (k 2)))) (h "1ccpj7a9sbpvjdqd69xcxy0l61g6w01kk006jcfzdlw29by9g8jy")))

