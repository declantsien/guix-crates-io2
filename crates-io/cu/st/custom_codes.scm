(define-module (crates-io cu st custom_codes) #:use-module (crates-io))

(define-public crate-custom_codes-1.0.0 (c (n "custom_codes") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)))) (h "1xg7cv4gf5ka03bkxzqrv0j1cp2pvfw28ij5w0901711xxk6sdn9")))

(define-public crate-custom_codes-1.0.1 (c (n "custom_codes") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)))) (h "11kbp0l72v94r7869lcl38vvnrqhikcq5bq1kpnjr0zj5bvv075n")))

(define-public crate-custom_codes-1.0.2 (c (n "custom_codes") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)))) (h "0j8kwmkj3w0wr3pv8za1g223i5l33kwqj70cql3rgmjfaq9prz0a")))

(define-public crate-custom_codes-1.0.3 (c (n "custom_codes") (v "1.0.3") (d (list (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)))) (h "1klxs5llbdmrcilild71sm3i4fbms684n4absr90460afbpx3p1f")))

(define-public crate-custom_codes-1.0.4 (c (n "custom_codes") (v "1.0.4") (d (list (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)))) (h "1qfwvf7151yaalqmk7m82ff7ax4q4fbjmkinzrk9isix8vb162yb")))

(define-public crate-custom_codes-1.0.5 (c (n "custom_codes") (v "1.0.5") (d (list (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)))) (h "189p8mxlhy0s0rhkzb2gixqqb8iqff9aca4g1h2zyzmcgq4gnqa8")))

(define-public crate-custom_codes-1.1.0 (c (n "custom_codes") (v "1.1.0") (d (list (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)))) (h "1k2bnpcdpp7vi3x02l8gk8y0ms0vl1bwk24pxdij3kyqc3byhknp")))

(define-public crate-custom_codes-1.2.0 (c (n "custom_codes") (v "1.2.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tai64") (r "^3.1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1dphvm9z0z3gs7iywqadhqxwbqaxm747dnymlg2f1c8s2mjjyfvl")))

(define-public crate-custom_codes-1.2.1 (c (n "custom_codes") (v "1.2.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "14ak7l7927qzbi5nr5lhpr4h8l5ld46bjyk9zn0ry1ic98jxrraj")))

(define-public crate-custom_codes-1.2.2 (c (n "custom_codes") (v "1.2.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gj6pnyzmz9g0q4mq6dq88bsqv5bhr3gzyawknjhcc46px8plzmj")))

(define-public crate-custom_codes-1.2.3 (c (n "custom_codes") (v "1.2.3") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "1izy94jfhkix7261wpk5sb8qj113wzzrq1rg7qc41jd8mlrws4jj")))

(define-public crate-custom_codes-1.2.4 (c (n "custom_codes") (v "1.2.4") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "05z9z49mjqjj7y9daf78j1f76ycyfy43jw0i9ksg5nq1w0g9c89x")))

(define-public crate-custom_codes-1.2.5 (c (n "custom_codes") (v "1.2.5") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n2g3h1kqdp01q0pjga2ry4h1km2ik71ya1viwkb98ircvf0aylp")))

(define-public crate-custom_codes-1.2.6 (c (n "custom_codes") (v "1.2.6") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "0apkwdyzd05zsmknky61gqkiaxgyn24ga5jfhasw63x49h8n8wmn")))

(define-public crate-custom_codes-1.2.7 (c (n "custom_codes") (v "1.2.7") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hwiynzba2civ4zjwxcf0bnw30j6nd6rgd18l1vlw8y0qfsdzyfb")))

(define-public crate-custom_codes-1.2.8 (c (n "custom_codes") (v "1.2.8") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "041h162ajwi23ynd2y06jj2sk85hvh00bric9v0r8sgcigib5plf")))

(define-public crate-custom_codes-1.2.9 (c (n "custom_codes") (v "1.2.9") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q0hn1aagqhizxvnpj3fmf7rfr206z1p1sm6b4mvj1340jqhh4ra")))

(define-public crate-custom_codes-1.2.10 (c (n "custom_codes") (v "1.2.10") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w630x6qxdpqmwg8vc2dcikin1am6v7ic3yqqjdfmid2jq3zmgsm")))

(define-public crate-custom_codes-2.0.0 (c (n "custom_codes") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y5s3124gyhi1lm3m7kxgasc8smcqkvzpgwhq6097zkskdvsb8vi")))

(define-public crate-custom_codes-2.0.1 (c (n "custom_codes") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kv76kry4xlnwgv6jf7yhnlahqka82l8khgr3i72nqvaf769i47b")))

(define-public crate-custom_codes-2.0.2 (c (n "custom_codes") (v "2.0.2") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z9z3bg5hf4y5j0whqwjhzsbjwn3vmcp5s4dhgmgs2m7b91bnidi")))

(define-public crate-custom_codes-2.0.3 (c (n "custom_codes") (v "2.0.3") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "1za311shmqv86rrjk2w49rkybq17m3claw9jhbwim3yjwmwvxrk7")))

(define-public crate-custom_codes-2.0.4 (c (n "custom_codes") (v "2.0.4") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "135pm8wq16ghi29963kma3lh745347khzg6mhna8ayvw1ya7bcsi")))

