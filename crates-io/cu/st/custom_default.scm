(define-module (crates-io cu st custom_default) #:use-module (crates-io))

(define-public crate-custom_default-0.0.0 (c (n "custom_default") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "139xx6asbnszvqszrwd2gjfli122kffz555lmpmvf5kppjdkhc2r") (y #t)))

(define-public crate-custom_default-0.0.1 (c (n "custom_default") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "16sxjsmpia9qch2vkmpif57aykcxk86cpa57zb6aswiywx4aw2cz") (y #t)))

(define-public crate-custom_default-0.0.2 (c (n "custom_default") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "16nsk0cb3wvwkfi4qkay785sypapnfg6np4g7yh0nlh29a5glgbc") (y #t)))

