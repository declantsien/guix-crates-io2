(define-module (crates-io cu st cust_derive) #:use-module (crates-io))

(define-public crate-cust_derive-0.1.0 (c (n "cust_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19h4k6kyil7biavabnyvggwpxb8dgsxj4iwxj0bv6if04jn85jjp")))

(define-public crate-cust_derive-0.1.1 (c (n "cust_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ckxjfdlwhmdyf5s2v10cslpb6wri9xl8nk3qirz8rsn5x1hn61v")))

(define-public crate-cust_derive-0.2.0 (c (n "cust_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rigqx5d1941cbpbd76i455ifh4yzz6fcga2na9fv6k2zsavr8z8")))

