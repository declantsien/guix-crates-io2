(define-module (crates-io cu st custos-macro) #:use-module (crates-io))

(define-public crate-custos-macro-0.1.0 (c (n "custos-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rjn3phzn69hkbb2as88wyqs01x7wizxvri3vm1w03a53zfvjnni")))

(define-public crate-custos-macro-0.1.1 (c (n "custos-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lmk182s0q99vyb50vk94i2858ck63gs3ly32ydfd57aynnc3qyz")))

