(define-module (crates-io cu st custom_error) #:use-module (crates-io))

(define-public crate-custom_error-1.0.0 (c (n "custom_error") (v "1.0.0") (h "1sb2nizinwi74q06jpm6gfr4hlb535h1i4blbvsm7wcbc069y932")))

(define-public crate-custom_error-1.0.1 (c (n "custom_error") (v "1.0.1") (h "169vka4valg2dkhxgacrrv0iyh1wpgqw02m7y64rbd6fgn3rm5pr")))

(define-public crate-custom_error-1.1.0 (c (n "custom_error") (v "1.1.0") (h "0cqhwd3drhiwcqlsakhv6sdbimmjzh49w7jfmsw8z4pilljfa0z4")))

(define-public crate-custom_error-1.1.1 (c (n "custom_error") (v "1.1.1") (h "10x670r5lg54z95kzaq139yx731dm10yxjq6ygviczqr6nvhn4ry")))

(define-public crate-custom_error-1.1.2 (c (n "custom_error") (v "1.1.2") (h "1mq251aw57icyfprz2g34j8j38iw80k88prf0s4haiwylsljnbrs")))

(define-public crate-custom_error-1.2.0 (c (n "custom_error") (v "1.2.0") (h "1mc0z9a7ajdyi6wyd76idx5fvypqc28ai88imfbabbams27q4lq1")))

(define-public crate-custom_error-1.3.0 (c (n "custom_error") (v "1.3.0") (h "1jnxdyp5s3wgvr8krgshd3y9j4bnziw49fzfrjrs071rv23a0kcr")))

(define-public crate-custom_error-1.4.0 (c (n "custom_error") (v "1.4.0") (h "02wblcdcqym8ccvd5y6qmj142pwd2d1ch6byfs35dgzmdn6r5ywy")))

(define-public crate-custom_error-1.5.0 (c (n "custom_error") (v "1.5.0") (h "1r79ryijaj52c8ymi36jk6njpzk2wn75kpdlsvcc8rpyn5bvi8xh")))

(define-public crate-custom_error-1.5.1 (c (n "custom_error") (v "1.5.1") (h "0dj226b8v882wqndbjhlq92mzsgnba1mxqg6dpijp4yxip5z3dbb")))

(define-public crate-custom_error-1.6.0 (c (n "custom_error") (v "1.6.0") (h "1bd2p1wrrs10hvl22ifm6zlh3hg4lvv8ggvydkv3ff2nc0fpb2f1")))

(define-public crate-custom_error-1.7.0 (c (n "custom_error") (v "1.7.0") (h "09prd4j8imqpb24dwxrvh1r0zyrfrz6dxidbz8b6i3a43ss1v35r")))

(define-public crate-custom_error-1.7.1 (c (n "custom_error") (v "1.7.1") (h "0bvrwgxdxwhy2wyp1851zz21pz9fmjyn93nnr2priscsfdjzr84k")))

(define-public crate-custom_error-1.8.0 (c (n "custom_error") (v "1.8.0") (h "04kchrplqkd5j88wd3rm72xkzz9fgsj22izs0f5fx8zylycmxb2i")))

(define-public crate-custom_error-1.9.0 (c (n "custom_error") (v "1.9.0") (h "0hibvrqw5z0s7vbh68zkfcqmkjrgvqljvh76cgxnnqb9mi9z3mai") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-custom_error-1.9.1 (c (n "custom_error") (v "1.9.1") (h "0accs6p1vg33w9d4di0cww5jswbl18vz7cxbmcgkibciryi9q08l") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-custom_error-1.9.2 (c (n "custom_error") (v "1.9.2") (h "19mwa90z1hgwn3mqj07b4cy6j4yc8c59k2n99mdvm9kz37fm32jg") (f (quote (("unstable") ("std") ("default" "std"))))))

