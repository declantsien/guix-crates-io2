(define-module (crates-io cu st cust_raw) #:use-module (crates-io))

(define-public crate-cust_raw-0.11.2 (c (n "cust_raw") (v "0.11.2") (d (list (d (n "find_cuda_helper") (r "^0.1") (d #t) (k 1)))) (h "1mhxzgzqrj0mnls40pnb1m2vq3djnsnaaml9yk0vw8c73p5264qc")))

(define-public crate-cust_raw-0.11.3 (c (n "cust_raw") (v "0.11.3") (d (list (d (n "find_cuda_helper") (r "^0.2") (d #t) (k 1)))) (h "1y1b82gf0fmaqxhvzjd3cxgd54vvbj3vji68pcl9ijqjvrm0vx7v")))

