(define-module (crates-io cu st cust_core) #:use-module (crates-io))

(define-public crate-cust_core-0.1.0 (c (n "cust_core") (v "0.1.0") (d (list (d (n "cust_derive") (r "^0.1") (d #t) (k 0)) (d (n "glam") (r "^0.20") (f (quote ("cuda" "libm"))) (o #t) (k 0)) (d (n "half") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "vek") (r "^0.15.1") (f (quote ("libm"))) (o #t) (k 0)))) (h "1dck3nw85sgiwyr5qw3r46sfysxdqnnwq7bchbpq9zsm7bnjpbsz") (f (quote (("default" "vek" "glam" "mint")))) (y #t)))

(define-public crate-cust_core-0.1.1 (c (n "cust_core") (v "0.1.1") (d (list (d (n "cust_derive") (r "^0.2") (d #t) (k 0)) (d (n "glam") (r "^0.20") (f (quote ("cuda" "libm"))) (o #t) (k 0)) (d (n "half") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "vek") (r "^0.15.1") (f (quote ("libm"))) (o #t) (k 0)))) (h "01jzjwywsngqm8d1vxk3zr9klvidab6iis1myg5r1y5q5ik7k7q3") (f (quote (("default" "vek" "glam" "mint"))))))

