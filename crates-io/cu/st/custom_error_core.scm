(define-module (crates-io cu st custom_error_core) #:use-module (crates-io))

(define-public crate-custom_error_core-1.8.0 (c (n "custom_error_core") (v "1.8.0") (h "1g3ghxc8bz42q5pvh07vi9kyllk7c3pxqf6slixn2m8v1w2688fr") (y #t)))

(define-public crate-custom_error_core-1.8.1 (c (n "custom_error_core") (v "1.8.1") (h "1q2cz31r3zwmaknj0ac31i2aw59sw1dc1p3knsc5vhgg9lsbgzgg")))

(define-public crate-custom_error_core-1.9.0 (c (n "custom_error_core") (v "1.9.0") (h "0gp7mzbq75bnz45m3rrzalgmb6cbh5hdkl7sn7z09ihi5kkhpv9p") (f (quote (("unstable") ("std") ("default" "std"))))))

