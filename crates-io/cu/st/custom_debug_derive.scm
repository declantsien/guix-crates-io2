(define-module (crates-io cu st custom_debug_derive) #:use-module (crates-io))

(define-public crate-custom_debug_derive-0.1.0 (c (n "custom_debug_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "syn") (r "^0.15.7") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.0") (d #t) (k 0)))) (h "1p1dkahwfracrja2qadxarqxyjw5g5vwha23bfihpk90c7dcdjiq")))

(define-public crate-custom_debug_derive-0.1.1 (c (n "custom_debug_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "syn") (r "^0.15.7") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.0") (d #t) (k 0)))) (h "10ssmh0lciw6zr44a9ik1whggp72b87cijbcvhflrp58vk2v0fiy")))

(define-public crate-custom_debug_derive-0.1.2 (c (n "custom_debug_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "syn") (r "^0.15.7") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.0") (d #t) (k 0)))) (h "0nvqi69gxvvnamn697c99s7l7blc4j26dd2x1pd4dwkibgz29fnd")))

(define-public crate-custom_debug_derive-0.1.3 (c (n "custom_debug_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "syn") (r "^0.15.7") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.0") (d #t) (k 0)))) (h "1srlqwiv48p01kzx43qhml4gpkww846j58qryvzpy0xkx40hwb5q")))

(define-public crate-custom_debug_derive-0.1.4 (c (n "custom_debug_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "syn") (r "^0.15.7") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.0") (d #t) (k 0)))) (h "1qxlslb3zvck51shi932mynj7wics01vr925i8mxglkpisc3zhjs")))

(define-public crate-custom_debug_derive-0.1.5 (c (n "custom_debug_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "syn") (r "^0.15.7") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.0") (d #t) (k 0)))) (h "1k17kgnnb4kv2hln4vw10vhv4mkvs96l36ig9771vss0sjsq7h72")))

(define-public crate-custom_debug_derive-0.1.6 (c (n "custom_debug_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0ybysvcvfb92wrhicc97rsvjyqlyha6342jzb82phf9w7lp5pyy2")))

(define-public crate-custom_debug_derive-0.1.7 (c (n "custom_debug_derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "06c14wdb5ifw85zb33wy3gxvzanrrpc2r6y9bk11l7g6gqylsxji")))

(define-public crate-custom_debug_derive-0.2.0 (c (n "custom_debug_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)))) (h "05m040yx92q0yfci7r0sgy2xizyd56kn1fygycnz39w6amiig5n5")))

(define-public crate-custom_debug_derive-0.3.0 (c (n "custom_debug_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)))) (h "0549p13jmwlhspgdzrq12zrhp66lwjzwq286cpnx5fgig7djig30")))

(define-public crate-custom_debug_derive-0.4.0 (c (n "custom_debug_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)))) (h "16k4dw6dp64c8jq4k5da0fs5fyx6y3c36fv8vhf3ly42q85gsfca")))

(define-public crate-custom_debug_derive-0.5.0 (c (n "custom_debug_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)))) (h "0a9psacjv37xd8xqsa248rjyhabqwxgcbwckq0rjvgq4n17d6dcv")))

(define-public crate-custom_debug_derive-0.5.1 (c (n "custom_debug_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)))) (h "1xxsw5la1397b8kqfr4a1adxwjbph9s2jy52x8ngdj9l2aag7a88")))

(define-public crate-custom_debug_derive-0.6.0 (c (n "custom_debug_derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1p7frj26zz2xqkzpbjbyjqsp2afrzr74j0hpgc5i35dxdbx70wyj")))

(define-public crate-custom_debug_derive-0.6.1 (c (n "custom_debug_derive") (v "0.6.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0sagiyiw5h0yi0wmc8dgqyrk4xy9zb0mwinbagi10ff7745l8cgp")))

