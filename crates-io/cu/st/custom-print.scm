(define-module (crates-io cu st custom-print) #:use-module (crates-io))

(define-public crate-custom-print-0.1.0 (c (n "custom-print") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 2)) (d (n "readme-sync") (r "^0.2.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "1ljv6kiyvd8v2gb6kvbqj3v9klnr6800g1krmk6lw33bgw3ajl6b") (f (quote (("std" "alloc") ("default" "alloc" "std") ("alloc"))))))

(define-public crate-custom-print-1.0.0 (c (n "custom-print") (v "1.0.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 2)) (d (n "readme-sync") (r "^0.2.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1r6cp7zzab0rpy6v4cyaahswdqrl81i3cj7bkx8kcdwr94k6asgh") (f (quote (("std" "alloc") ("default" "alloc" "std") ("alloc")))) (r "1.60")))

