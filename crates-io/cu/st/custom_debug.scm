(define-module (crates-io cu st custom_debug) #:use-module (crates-io))

(define-public crate-custom_debug-0.3.0 (c (n "custom_debug") (v "0.3.0") (d (list (d (n "custom_debug_derive") (r "^0.3.0") (d #t) (k 0)))) (h "0lm4lkg4hff4f5d0s87gcjgp2894pc17vhb317b3jj52vwlizwxj")))

(define-public crate-custom_debug-0.4.0 (c (n "custom_debug") (v "0.4.0") (d (list (d (n "custom_debug_derive") (r "^0.4.0") (d #t) (k 0)))) (h "0b43ffn72w8z7zkh7qc6g2rlwrkf7g6c6px6pigxg7kpbs1qr4bp")))

(define-public crate-custom_debug-0.5.0 (c (n "custom_debug") (v "0.5.0") (d (list (d (n "custom_debug_derive") (r "^0.5.0") (d #t) (k 0)))) (h "07ld94ffdwr7k1av36aac10fkfxxc3hd6sqy6bsdqradcx3m08p8")))

(define-public crate-custom_debug-0.5.1 (c (n "custom_debug") (v "0.5.1") (d (list (d (n "custom_debug_derive") (r "^0.5.1") (d #t) (k 0)))) (h "1qn81isz144p531j3q4pi9lw15k0rvim0p6hjnay4ax4qbi0m7p8")))

(define-public crate-custom_debug-0.6.0 (c (n "custom_debug") (v "0.6.0") (d (list (d (n "custom_debug_derive") (r "^0.6.0") (d #t) (k 0)))) (h "0a4i7pa12g994wfzgfq5j1waz7gyqz9kgcd5swnfklm6ksdcv20g")))

(define-public crate-custom_debug-0.6.1 (c (n "custom_debug") (v "0.6.1") (d (list (d (n "custom_debug_derive") (r "^0.6.1") (d #t) (k 0)))) (h "0mnj2k1k1mjg34f2zcyyiqp205fjkpim5h3nf2f90gjh1szibrql")))

