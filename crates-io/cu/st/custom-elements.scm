(define-module (crates-io cu st custom-elements) #:use-module (crates-io))

(define-public crate-custom-elements-0.1.0 (c (n "custom-elements") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlElement" "Node" "ShadowRoot" "ShadowRootInit" "ShadowRootMode"))) (d #t) (k 0)))) (h "0bl4a4wh9j071plckfbrahkv5bv9rf0a6dwqb3lii32xx00h7yc9")))

(define-public crate-custom-elements-0.2.0 (c (n "custom-elements") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "Element" "HtmlElement" "Node" "ShadowRoot" "ShadowRootInit" "ShadowRootMode" "Window"))) (d #t) (k 0)))) (h "1d4ymy3g3kzvlnjba8qv13ii2sdnqqg5qhkp2bbc4ln4vp8yg2pj")))

(define-public crate-custom-elements-0.2.1 (c (n "custom-elements") (v "0.2.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "Element" "HtmlElement" "Node" "ShadowRoot" "ShadowRootInit" "ShadowRootMode" "Window"))) (d #t) (k 0)))) (h "0a2hqj37z51l63cs162d9blxznndm85bf2s4pq7v3ljb4cjssx32")))

