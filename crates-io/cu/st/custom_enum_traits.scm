(define-module (crates-io cu st custom_enum_traits) #:use-module (crates-io))

(define-public crate-custom_enum_traits-0.1.0 (c (n "custom_enum_traits") (v "0.1.0") (h "0zqdzzdgca2d7vz33hkvk6ajf6nm3i7jschvzjg16nppk3hj8202")))

(define-public crate-custom_enum_traits-0.2.0 (c (n "custom_enum_traits") (v "0.2.0") (h "18c3d7858blqqprk0v5mg6ydcda5qnanliarq5gmr1dd898kf569")))

