(define-module (crates-io cu st custos-math) #:use-module (crates-io))

(define-public crate-custos-math-0.1.0 (c (n "custos-math") (v "0.1.0") (d (list (d (n "custos") (r "^0.1.4") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08d595c6j1irmyhfa2l97dk02zf43xaj8glzpfs4vkjxp4hwl97q") (f (quote (("safe" "custos/safe") ("opencl" "custos/opencl") ("default" "opencl" "cuda") ("cuda" "custos/cuda"))))))

(define-public crate-custos-math-0.1.1 (c (n "custos-math") (v "0.1.1") (d (list (d (n "custos") (r "^0.1.4") (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "1wmjsqxxg3k3mk3jh77n807k5z4rpq72r2xs5rbqxy8jlcifzhcr") (f (quote (("safe" "custos/safe") ("opencl" "custos/opencl") ("default" "opencl") ("cuda" "custos/cuda")))) (y #t)))

(define-public crate-custos-math-0.1.2 (c (n "custos-math") (v "0.1.2") (d (list (d (n "custos") (r "^0.1.5") (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "1y5jmmrrr2zcaq4lz4zwc5r9q955h8qqam7d709lhjfy1rhnapdi") (f (quote (("safe" "custos/safe") ("opencl" "custos/opencl") ("default" "opencl" "cuda") ("cuda" "custos/cuda")))) (y #t)))

(define-public crate-custos-math-0.1.3 (c (n "custos-math") (v "0.1.3") (d (list (d (n "custos") (r "^0.1.5") (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "026lbmi317a5fsb5gd5izffg8hlf7xplwdiq2c8pcvhcx04083mk") (f (quote (("safe" "custos/safe") ("opencl" "custos/opencl") ("default" "opencl" "cuda") ("cuda" "custos/cuda"))))))

(define-public crate-custos-math-0.1.4 (c (n "custos-math") (v "0.1.4") (d (list (d (n "custos") (r "^0.2.1") (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "1pjj5b987v7a34r6qa5if4l1hzfhg2gd1ssd68ib47myz123x1s6") (f (quote (("opencl" "custos/opencl") ("default" "opencl" "cuda") ("cuda" "custos/cuda"))))))

(define-public crate-custos-math-0.2.0 (c (n "custos-math") (v "0.2.0") (d (list (d (n "custos") (r "^0.4.0") (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "13s4jchjwr9aqfk2dbvzcc9kxck5ca62gkn5f4vjmhf0bhrixxvp") (f (quote (("realloc" "custos/realloc") ("opencl" "custos/opencl") ("default" "opencl" "cuda") ("cuda" "custos/cuda"))))))

(define-public crate-custos-math-0.2.1 (c (n "custos-math") (v "0.2.1") (d (list (d (n "custos") (r "^0.4.1") (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "01ci0ysnkmc9p7aqnmq8xxzrmbi46jnqvq32ls1jdpz6540zaf8q") (f (quote (("realloc" "custos/realloc") ("opencl" "custos/opencl") ("default" "opencl" "cuda") ("cuda" "custos/cuda"))))))

(define-public crate-custos-math-0.4.3 (c (n "custos-math") (v "0.4.3") (d (list (d (n "custos") (r "^0.4.3") (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "0bpyp50sv2in7ga8vwapfcjg982slk22lijw83ahnwfb1wnbr1aq") (f (quote (("realloc" "custos/realloc") ("opencl" "custos/opencl") ("default" "opencl" "cuda") ("cuda" "custos/cuda"))))))

(define-public crate-custos-math-0.4.4 (c (n "custos-math") (v "0.4.4") (d (list (d (n "custos") (r "^0.4.4") (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "1f770xw3r3caqssh0gyfk61zf5rx7bqs723s2a8vksxc4avggmwd") (f (quote (("realloc" "custos/realloc") ("opencl" "custos/opencl") ("default" "opencl" "cuda") ("cuda" "custos/cuda"))))))

(define-public crate-custos-math-0.4.6 (c (n "custos-math") (v "0.4.6") (d (list (d (n "custos") (r "^0.4.6") (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "0kdfnk2jxphfgnm73124irw95ifv195pjcv9lvs7lc0y50k4j35h") (f (quote (("realloc" "custos/realloc") ("opencl" "custos/opencl") ("default" "opencl" "cuda") ("cuda" "custos/cuda"))))))

(define-public crate-custos-math-0.5.0 (c (n "custos-math") (v "0.5.0") (d (list (d (n "custos") (r "^0.5.0") (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "01x1f0qvd5889h7im92aa0vv86szfa11lyasy4sgpg944mngcy07") (f (quote (("realloc" "custos/realloc") ("opencl" "custos/opencl") ("default" "realloc") ("cuda" "custos/cuda"))))))

(define-public crate-custos-math-0.6.0 (c (n "custos-math") (v "0.6.0") (d (list (d (n "custos") (r "^0.6.1") (f (quote ("macro"))) (k 0)) (d (n "fastrand") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "matrixmultiply") (r "^0.3.2") (o #t) (k 0)))) (h "1cq4fwwd3f9yd9pk48cdjlc0md149z28rfvakz4flps8whd1w5b2") (f (quote (("static-api" "custos/static-api") ("stack" "custos/stack") ("realloc" "custos/realloc") ("opencl" "custos/opencl" "cpu") ("no-std" "custos/no-std" "stack") ("default" "blas" "fastrand" "cpu" "opencl" "static-api" "cuda") ("cuda" "custos/cuda") ("cpu" "custos/cpu") ("blas" "custos/blas")))) (y #t) (r "1.65")))

(define-public crate-custos-math-0.6.2 (c (n "custos-math") (v "0.6.2") (d (list (d (n "custos") (r "^0.6.2") (f (quote ("macro"))) (k 0)) (d (n "fastrand") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "matrixmultiply") (r "^0.3.2") (o #t) (k 0)))) (h "03sgfx7f4ccybd3x84gwlml9gp6wi2fgxmrw8rrlrxwwwigmqkkz") (f (quote (("static-api" "custos/static-api") ("stack" "custos/stack") ("realloc" "custos/realloc") ("opencl" "custos/opencl" "cpu") ("no-std" "custos/no-std" "stack") ("default" "blas" "fastrand" "cpu" "opencl" "static-api" "cuda") ("cuda" "custos/cuda") ("cpu" "custos/cpu") ("blas" "custos/blas")))) (y #t) (r "1.65")))

(define-public crate-custos-math-0.6.3 (c (n "custos-math") (v "0.6.3") (d (list (d (n "custos") (r "^0.6.3") (f (quote ("macro"))) (k 0)) (d (n "fastrand") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "matrixmultiply") (r "^0.3.2") (o #t) (k 0)))) (h "0zn8j07fwn6xml4blwpc82d05lx4pvjq58bmxzkxzw485xr9phin") (f (quote (("static-api" "custos/static-api") ("stack" "custos/stack") ("realloc" "custos/realloc") ("opencl" "custos/opencl" "cpu") ("no-std" "custos/no-std" "stack") ("default" "opencl") ("cuda" "custos/cuda" "cpu") ("cpu" "custos/cpu") ("blas" "custos/blas")))) (r "1.65")))

