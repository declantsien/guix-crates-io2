(define-module (crates-io cu st custom_egui_frame) #:use-module (crates-io))

(define-public crate-custom_egui_frame-0.1.0 (c (n "custom_egui_frame") (v "0.1.0") (d (list (d (n "eframe") (r "^0.26.2") (d #t) (k 0)) (d (n "egui") (r "^0.26.2") (d #t) (k 0)) (d (n "egui_extras") (r "^0.26.2") (f (quote ("all_loaders"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("jpeg" "png"))) (d #t) (k 0)))) (h "16shnbxdgxbd6bpiliahbagf2r8i47kczkavx77xnnpqncnnhqna")))

(define-public crate-custom_egui_frame-0.1.1 (c (n "custom_egui_frame") (v "0.1.1") (d (list (d (n "eframe") (r "^0.26.2") (d #t) (k 0)) (d (n "egui") (r "^0.26.2") (d #t) (k 0)) (d (n "egui_extras") (r "^0.26.2") (f (quote ("all_loaders"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("jpeg" "png"))) (d #t) (k 0)))) (h "0bajflc36plw8s7rgwm6k217r7rxrchbk7499f96wymhfj8s9sl2")))

