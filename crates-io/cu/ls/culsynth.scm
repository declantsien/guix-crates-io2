(define-module (crates-io cu ls culsynth) #:use-module (crates-io))

(define-public crate-culsynth-0.1.0 (c (n "culsynth") (v "0.1.0") (d (list (d (n "fixed") (r "^1.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_40"))) (d #t) (k 0)))) (h "0xnwm9255yfkymc7ya9qk9jp077s1pl1jc8s6p85p6p6rq6apsms") (f (quote (("libm" "num-traits/libm"))))))

(define-public crate-culsynth-0.2.0 (c (n "culsynth") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "fixed") (r "^1.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (k 0)))) (h "1k5apdbrc8hpjy9lwn3ikjnwr3wmcbb10ypi23mzmqwdfndwgvcg") (f (quote (("rand_defaults" "rand/default") ("libm" "num-traits/libm"))))))

