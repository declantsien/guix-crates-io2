(define-module (crates-io cu da cuda-driver-sys) #:use-module (crates-io))

(define-public crate-cuda-driver-sys-0.3.0-alpha.1 (c (n "cuda-driver-sys") (v "0.3.0-alpha.1") (d (list (d (n "cuda-config") (r "^0.1.0") (d #t) (k 1)))) (h "1gldnwwwa04avaasq42m0x7scflq2xx3106v2y05qh44gzh5lplr") (l "cuda")))

(define-public crate-cuda-driver-sys-0.3.0-alpha.2 (c (n "cuda-driver-sys") (v "0.3.0-alpha.2") (d (list (d (n "cuda-config") (r "^0.1.0") (d #t) (k 1)))) (h "0hl6ikmhm489kz0w5jm0v08m668wp9ndsib1iiqlj6m0d97cyr03") (l "cuda")))

(define-public crate-cuda-driver-sys-0.3.0 (c (n "cuda-driver-sys") (v "0.3.0") (d (list (d (n "cuda-config") (r "^0.1.0") (d #t) (k 1)))) (h "1ggziylcr3dqk5kjmmv257ijpm3mvc8izk8bv1vli1fyq0n5ak0x") (l "cuda")))

