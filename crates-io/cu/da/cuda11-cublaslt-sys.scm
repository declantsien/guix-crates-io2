(define-module (crates-io cu da cuda11-cublaslt-sys) #:use-module (crates-io))

(define-public crate-cuda11-cublasLt-sys-0.1.0 (c (n "cuda11-cublasLt-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "cuda11-cuda-sys") (r "^0.1") (d #t) (k 2)) (d (n "cuda11-cudart-sys") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a8wr9xjpxb6vkgwclw7z1ix3jm6p75fw1swll25kvjfd00rzi6q") (f (quote (("generate" "bindgen") ("default" "generate"))))))

(define-public crate-cuda11-cublasLt-sys-0.2.0 (c (n "cuda11-cublasLt-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "cuda11-cuda-sys") (r "^0.2") (d #t) (k 2)) (d (n "cuda11-cudart-sys") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cb1j70kndqq91d22slavc99mhsaf8k0krliacaiy5jp56c6rkj4") (f (quote (("generate" "bindgen") ("default" "generate"))))))

(define-public crate-cuda11-cublasLt-sys-0.3.0 (c (n "cuda11-cublasLt-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cbmzsdjk7p6yhpzhsp89dxs712hb7p4hncxzgsa5qbh9yhnwp72") (f (quote (("generate" "bindgen") ("default" "generate"))))))

