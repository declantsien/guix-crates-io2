(define-module (crates-io cu da cuda_std) #:use-module (crates-io))

(define-public crate-cuda_std-0.1.0 (c (n "cuda_std") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cuda_std_macros") (r "^0.1") (d #t) (k 0)) (d (n "half") (r "^1.7.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "vek") (r "^0.15.1") (f (quote ("libm"))) (k 0)))) (h "1s4d2xfxlbgd7ck0ypq3q5dah80icpj6wmgf3m2b3yv7p7rg8iqa")))

(define-public crate-cuda_std-0.2.0 (c (n "cuda_std") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cuda_std_macros") (r "^0.2") (d #t) (k 0)) (d (n "half") (r "^1.7.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "vek") (r "^0.15.1") (f (quote ("libm"))) (k 0)))) (h "05dkjbf3vx4b5bb9sw0yiivcf1z9ywq7pjavgq4fq1m60npkb51q")))

(define-public crate-cuda_std-0.2.1 (c (n "cuda_std") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cuda_std_macros") (r "^0.2") (d #t) (k 0)) (d (n "half") (r "^1.7.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "vek") (r "^0.15.1") (f (quote ("libm"))) (k 0)))) (h "1f4gl9wmkg0nidxmbwcf0sa1chygslcykdal98bd3az57mgvnfsi")))

(define-public crate-cuda_std-0.2.2 (c (n "cuda_std") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cuda_std_macros") (r "^0.2") (d #t) (k 0)) (d (n "half") (r "^1.7.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "vek") (r "^0.15.1") (f (quote ("libm"))) (k 0)))) (h "174237dj152dvndvykcn17nz2d0kdzsyyxnb6fsdz3i7xa8lfcgn")))

