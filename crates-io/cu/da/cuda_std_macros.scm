(define-module (crates-io cu da cuda_std_macros) #:use-module (crates-io))

(define-public crate-cuda_std_macros-0.1.0 (c (n "cuda_std_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "1fww5zgkdd999s9qkmm86s460x786qw13b013i7rjzj87hg2ycay")))

(define-public crate-cuda_std_macros-0.2.0 (c (n "cuda_std_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "0hlakxn9pz8233pwsh44j8gzqzf078a3lnnq3v2cadmb4c4l1mlz")))

