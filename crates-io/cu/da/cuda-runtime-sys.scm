(define-module (crates-io cu da cuda-runtime-sys) #:use-module (crates-io))

(define-public crate-cuda-runtime-sys-0.3.0-alpha.1 (c (n "cuda-runtime-sys") (v "0.3.0-alpha.1") (d (list (d (n "cuda-config") (r "^0.1.0") (d #t) (k 1)))) (h "0fcc9sv9rjwmxyyrswh9bk3vfir2y5f44np7273f7zl724q0n1sx") (l "cudart")))

