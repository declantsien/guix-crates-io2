(define-module (crates-io cu da cuda_parsers) #:use-module (crates-io))

(define-public crate-cuda_parsers-0.1.0 (c (n "cuda_parsers") (v "0.1.0") (d (list (d (n "async-compression") (r "^0.4.5") (f (quote ("tokio" "zstd"))) (d #t) (k 2)) (d (n "deku") (r "^0.15") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "goblin") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "lz4") (r "^1.24.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "038l3c6iraq3giy08qhnab28hy4w0hh5j29vwakdvfbcqyd11vvj")))

