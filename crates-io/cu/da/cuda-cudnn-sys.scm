(define-module (crates-io cu da cuda-cudnn-sys) #:use-module (crates-io))

(define-public crate-cuda-cudnn-sys-0.0.1 (c (n "cuda-cudnn-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0v7j22qkk7mjcp1jrczajbmz98ykd7yavb2fqnw9wk0ksi4ajqla")))

