(define-module (crates-io cu da cuda11-cutensor-sys) #:use-module (crates-io))

(define-public crate-cuda11-cutensor-sys-0.1.0 (c (n "cuda11-cutensor-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "cuda11-cuda-sys") (r "^0.1") (d #t) (k 2)) (d (n "cuda11-cudart-sys") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06m3b09pbg15g0rv08w2m3yn3sk2id3mwd9kxq7qr2wm7s2p3ihm") (f (quote (("generate" "bindgen") ("default" "generate"))))))

(define-public crate-cuda11-cutensor-sys-0.2.0 (c (n "cuda11-cutensor-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "cuda11-cuda-sys") (r "^0.2") (d #t) (k 2)) (d (n "cuda11-cudart-sys") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cf1j00czrs1k6033ns6061kkn2zf4sh9xpcg4nmfyxd8g2cqd0j") (f (quote (("generate" "bindgen") ("default" "generate"))))))

(define-public crate-cuda11-cutensor-sys-0.3.0 (c (n "cuda11-cutensor-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mv1wnb9rfz9hinjhmjmbj8xb7viz1zcfl4zzs9an26y47zpnaif") (f (quote (("generate" "bindgen") ("default" "generate"))))))

