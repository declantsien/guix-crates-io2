(define-module (crates-io cu da cuda-rs) #:use-module (crates-io))

(define-public crate-cuda-rs-0.1.0 (c (n "cuda-rs") (v "0.1.0") (d (list (d (n "cuda-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "100vi6c5jkla80n3rjykvqan398bacd9dk847i2vka6m1m2hgwxy")))

(define-public crate-cuda-rs-0.1.1 (c (n "cuda-rs") (v "0.1.1") (d (list (d (n "cuda-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vvw6ach2zdq3g6h9nx1ciyjis1ixar45zrgfm07fxlzx7i5g8vs")))

(define-public crate-cuda-rs-0.1.2 (c (n "cuda-rs") (v "0.1.2") (d (list (d (n "cuda-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0m9k2b4pnbb9s9ycl9rc5sambqb0rgjvickihgy6nmfqrhn5d3sa")))

(define-public crate-cuda-rs-0.1.3 (c (n "cuda-rs") (v "0.1.3") (d (list (d (n "cuda-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kkv8qak5ad57mvvsipq7vbxc476zwbm4qx6v0cprn7dxk70as1h")))

(define-public crate-cuda-rs-0.1.4 (c (n "cuda-rs") (v "0.1.4") (d (list (d (n "cuda-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cg0hmv046sh0fp248g2i3xjs23p02jkzag15va7bdy8c4lprrgs")))

(define-public crate-cuda-rs-0.1.5 (c (n "cuda-rs") (v "0.1.5") (d (list (d (n "cuda-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k0zdzlr65cfd3dr958vpd75zgyik4xza5ys8d07b5w92v643h5b")))

(define-public crate-cuda-rs-0.1.6 (c (n "cuda-rs") (v "0.1.6") (d (list (d (n "cuda-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bdcns1n7z1rvqgpk38xkaa04zy5mf2r51gvx10m9iiyxk5ylgx6")))

(define-public crate-cuda-rs-0.1.7 (c (n "cuda-rs") (v "0.1.7") (d (list (d (n "cuda-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1b131lbxdv63f7fxqs8gb9xjya3sv0kgzm1gk74w9wcgxh1wg2kg")))

(define-public crate-cuda-rs-0.1.8 (c (n "cuda-rs") (v "0.1.8") (d (list (d (n "cuda-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1m7c7cgd20rblc1kkjr0vfmandmzf2k61kfdsvav3dfgyf6xnaal")))

(define-public crate-cuda-rs-0.1.9 (c (n "cuda-rs") (v "0.1.9") (d (list (d (n "cuda-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1r6mczv27r175aby000anca3qkp5qn5pp9lbby3yg5p3r42nyz96")))

