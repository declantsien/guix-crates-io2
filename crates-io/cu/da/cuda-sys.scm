(define-module (crates-io cu da cuda-sys) #:use-module (crates-io))

(define-public crate-cuda-sys-0.1.0 (c (n "cuda-sys") (v "0.1.0") (h "1ibz054cp541nkf153c60fgb8avrsgw4r69f3mjyw30yylzb8w5r")))

(define-public crate-cuda-sys-0.2.0 (c (n "cuda-sys") (v "0.2.0") (h "1hz6r80ickhx2ww4xss5576vzkgx6321p1dvrmxchvqj2dsy2jxk") (l "cuda")))

