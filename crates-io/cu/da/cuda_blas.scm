(define-module (crates-io cu da cuda_blas) #:use-module (crates-io))

(define-public crate-cuda_blas-0.1.0 (c (n "cuda_blas") (v "0.1.0") (d (list (d (n "cuda") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09f46mr7amz870xr0dlx2qhxrbxiwmsx2rb3myzs1iipdhbxd7wc")))

