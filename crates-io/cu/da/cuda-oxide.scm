(define-module (crates-io cu da cuda-oxide) #:use-module (crates-io))

(define-public crate-cuda-oxide-0.1.0 (c (n "cuda-oxide") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0q41vjh42sp6px1dyh2b77pdfbakisw2rikyzfj5nccrfwll0zqd")))

(define-public crate-cuda-oxide-0.1.1 (c (n "cuda-oxide") (v "0.1.1") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1h2w6vbyabwillg50hwmyv005vk08mj0bi18sihxx1n3g5v8rffv")))

(define-public crate-cuda-oxide-0.2.0 (c (n "cuda-oxide") (v "0.2.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qm8nri0pvr1lw7b39ssbzn4x0pw6fjsmi39n3d6i18h8jx9p4vi")))

(define-public crate-cuda-oxide-0.2.1 (c (n "cuda-oxide") (v "0.2.1") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0cpw1b5wnw4ms6rbqz7nak5naa4jv9jnc2prgp2j83bpz3iwyz6k")))

(define-public crate-cuda-oxide-0.3.0 (c (n "cuda-oxide") (v "0.3.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1k1gzkal8x8rawy0y0w7gx36ppy374qiz0b2cy6srf5rkn6xv5hq")))

(define-public crate-cuda-oxide-0.3.1 (c (n "cuda-oxide") (v "0.3.1") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0gs03avac7j2wcfddgiq16mvbw8fmgszhrqk5dvxdng0a60b349g")))

(define-public crate-cuda-oxide-0.4.0 (c (n "cuda-oxide") (v "0.4.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0rnb0fgbi5s9aphji7w8hzwnw6jyvzy11ilw3gzjhj6qhb1g47ri")))

