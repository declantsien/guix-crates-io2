(define-module (crates-io cu da cuda_builder) #:use-module (crates-io))

(define-public crate-cuda_builder-0.1.0 (c (n "cuda_builder") (v "0.1.0") (d (list (d (n "find_cuda_helper") (r "^0.1") (d #t) (k 0)) (d (n "nvvm") (r "^0.1") (d #t) (k 0)) (d (n "rustc_codegen_nvvm") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "102n25ycl84bfvax4gnx7174jrqq9qm20dfgl4in70njbkyiq000")))

(define-public crate-cuda_builder-0.2.0 (c (n "cuda_builder") (v "0.2.0") (d (list (d (n "find_cuda_helper") (r "^0.2") (d #t) (k 0)) (d (n "nvvm") (r "^0.1") (d #t) (k 0)) (d (n "rustc_codegen_nvvm") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0grx42kc2r7szn2dz3kkj2kk46550j1iwpzcl8k5v208vssn1qhh")))

(define-public crate-cuda_builder-0.3.0 (c (n "cuda_builder") (v "0.3.0") (d (list (d (n "find_cuda_helper") (r "^0.2") (d #t) (k 0)) (d (n "nvvm") (r "^0.1") (d #t) (k 0)) (d (n "rustc_codegen_nvvm") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1fm74mcjbvkzayb3ah2k1g988ab7lp89nzzk89yw9i7vmxhlq3iz")))

