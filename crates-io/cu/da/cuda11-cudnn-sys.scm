(define-module (crates-io cu da cuda11-cudnn-sys) #:use-module (crates-io))

(define-public crate-cuda11-cudnn-sys-0.1.0 (c (n "cuda11-cudnn-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "cuda11-cuda-sys") (r "^0.1") (d #t) (k 2)) (d (n "cuda11-cudart-sys") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vm41gd8734kmxli965gd0xjrwhm3789v85vl3kxndxmx39cr4rk") (f (quote (("generate" "bindgen") ("default" "generate"))))))

(define-public crate-cuda11-cudnn-sys-0.2.0 (c (n "cuda11-cudnn-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "cuda11-cuda-sys") (r "^0.2") (d #t) (k 2)) (d (n "cuda11-cudart-sys") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1j79jg1dz879xm7i3yhwid6zam1pvwq2w23wa70jdxkbh9pcqhha") (f (quote (("generate" "bindgen") ("default" "generate"))))))

(define-public crate-cuda11-cudnn-sys-0.3.0 (c (n "cuda11-cudnn-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00bsp0879jl7nqvs23xly80qhcbpjvs4lgjxjqpmq7dngbq5wmj1") (f (quote (("generate" "bindgen") ("default" "generate"))))))

