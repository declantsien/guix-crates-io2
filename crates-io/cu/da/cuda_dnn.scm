(define-module (crates-io cu da cuda_dnn) #:use-module (crates-io))

(define-public crate-cuda_dnn-0.1.0 (c (n "cuda_dnn") (v "0.1.0") (d (list (d (n "cuda") (r "^0.1") (d #t) (k 0)) (d (n "float") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cyxzj01pnk7jl3505kwl7fq2sfvcb1cl7j54a4gd2mjgq2zg7jb")))

(define-public crate-cuda_dnn-0.1.1 (c (n "cuda_dnn") (v "0.1.1") (d (list (d (n "cuda") (r "^0.1") (d #t) (k 0)) (d (n "float") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17ha0dgm3s42xy113sgxdn387h8g1zbcg8y379h2pvnpjbahqz8f")))

