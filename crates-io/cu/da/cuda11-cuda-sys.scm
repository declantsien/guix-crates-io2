(define-module (crates-io cu da cuda11-cuda-sys) #:use-module (crates-io))

(define-public crate-cuda11-cuda-sys-0.1.0 (c (n "cuda11-cuda-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18vqz9b7qdnz413jnn5hji3cb9x7bysdqazshzbyl693bbr6j40f") (f (quote (("generate" "bindgen") ("default" "generate"))))))

(define-public crate-cuda11-cuda-sys-0.2.0 (c (n "cuda11-cuda-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "075n3rj0pflzdm6zlkd8jqdc567cxqvp2j9wkh8aqagqxacd3810") (f (quote (("generate" "bindgen") ("default" "generate"))))))

