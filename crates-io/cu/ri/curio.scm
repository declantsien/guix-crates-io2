(define-module (crates-io cu ri curio) #:use-module (crates-io))

(define-public crate-curio-0.0.1 (c (n "curio") (v "0.0.1") (d (list (d (n "rustls") (r "^0.18.1") (d #t) (k 0)) (d (n "webpki") (r "^0.21.3") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.20.0") (d #t) (k 0)))) (h "196aipr3bzdv96l6llybrhirwrb8znbkg8n2n1dzqsmb4g22gxz8")))

(define-public crate-curio-0.0.2 (c (n "curio") (v "0.0.2") (d (list (d (n "chunked_transfer") (r "^1.2.0") (d #t) (k 0)) (d (n "rustls") (r "^0.18.1") (d #t) (k 0)) (d (n "webpki") (r "^0.21.3") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.20.0") (d #t) (k 0)))) (h "16alf61nk8p1lyknlik0zpgr6pfw4rz927vda0xxa338fsfwfhci")))

(define-public crate-curio-0.0.3 (c (n "curio") (v "0.0.3") (d (list (d (n "chunked_transfer") (r "^1.2.0") (d #t) (k 0)) (d (n "rustls") (r "^0.18.1") (d #t) (k 0)) (d (n "webpki") (r "^0.21.3") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.20.0") (d #t) (k 0)))) (h "0r9rwwxix6p1gc3rqajrjk2y7ww0lyn46xx0q40w004a6y4r4wgd")))

