(define-module (crates-io cu ri curie) #:use-module (crates-io))

(define-public crate-curie-0.0.1 (c (n "curie") (v "0.0.1") (h "0phm3ygz7p0yi8qhmx6va76fv2d05cdfsgsfwxx88y0bs1rl910b")))

(define-public crate-curie-0.0.2 (c (n "curie") (v "0.0.2") (h "03vh9dm8ikfh76ndf2y2pcvgd0rd5f3sfq66wwmrzraizqi41bc6")))

(define-public crate-curie-0.0.3 (c (n "curie") (v "0.0.3") (h "1jkpb3yi9xxj96f3mr4r1v13y4l31012ajapvk0nfjrghj2v7176")))

(define-public crate-curie-0.0.4 (c (n "curie") (v "0.0.4") (h "1g8f2m41wgbmd55arhp809q54j722lmvd4yqg5dh14494zhlm7xr")))

(define-public crate-curie-0.0.5 (c (n "curie") (v "0.0.5") (h "00nij5sw7md31fvmvpl5chd54ww363v4gwdcs140vf2h9s64c1bq")))

(define-public crate-curie-0.0.6 (c (n "curie") (v "0.0.6") (h "1bh9d6spdx1mli80wm34fgplg59mnkw6yi4hi293xgb2kslmim51")))

(define-public crate-curie-0.0.7 (c (n "curie") (v "0.0.7") (h "18zblw4i90rqmfb6hszgy10p134yc2q5zg36pcv4vkxncdfayzim")))

(define-public crate-curie-0.0.8 (c (n "curie") (v "0.0.8") (h "03nvisjiv3vgkffih1n70nfq6863j5kklfnw5srd7gigd0rm9ga7")))

(define-public crate-curie-0.1.0 (c (n "curie") (v "0.1.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "03g6xvyapv3bns8qk1ki3qm21z9hydfvdrpmpdvfm2bf9yf0c41j")))

(define-public crate-curie-0.1.1 (c (n "curie") (v "0.1.1") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "0g107a34rh8bigx3ijyi3g3n3as4sksgv8q5xikrc33s3c9cw5a2")))

(define-public crate-curie-0.1.2 (c (n "curie") (v "0.1.2") (d (list (d (n "indexmap") (r "^2") (d #t) (k 0)))) (h "1hw22ai1s3rz12rdnpv55xlcyansgqj7lfj6s2q48zzpqbs50v0d")))

