(define-module (crates-io cu ri curiosity) #:use-module (crates-io))

(define-public crate-curiosity-0.0.1 (c (n "curiosity") (v "0.0.1") (d (list (d (n "cosmos") (r "^0.0.12") (d #t) (k 0)) (d (n "docker") (r "^0.0.20") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "1qjsypswjx8shqfph5k6khjnivl58lf98pbd138yjdvqwcpxhjly")))

