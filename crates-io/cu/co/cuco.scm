(define-module (crates-io cu co cuco) #:use-module (crates-io))

(define-public crate-cuco-0.1.0 (c (n "cuco") (v "0.1.0") (h "0n9kfm869pkzf7zvkzg0ngq194hrykac6a6bhwy7ra3i0bmpvmpj")))

(define-public crate-cuco-0.1.1 (c (n "cuco") (v "0.1.1") (h "06p2617qwadws0p1l6ch3ski96y7ci943m0gl2dan3lgphffmbkr")))

(define-public crate-cuco-0.1.2 (c (n "cuco") (v "0.1.2") (h "1lc921m17cwsz53197lgfn6wl22r3ids5i2w5bsw35y3sy8szdfy")))

(define-public crate-cuco-0.1.3 (c (n "cuco") (v "0.1.3") (h "052mriy7bk7xbpaxfw8gfc25hr73d7jy7akvi3500rshdzjxlr40")))

