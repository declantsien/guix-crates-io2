(define-module (crates-io cu co cucoqu) #:use-module (crates-io))

(define-public crate-cucoqu-0.1.0 (c (n "cucoqu") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "glifparser") (r "^1.2") (f (quote ("more-iof"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14sgznhk4lzwk39mkdgfz22v891d0wrg5s3ag8jiplj42ma5viy1")))

