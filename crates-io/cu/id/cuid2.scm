(define-module (crates-io cu id cuid2) #:use-module (crates-io))

(define-public crate-cuid2-0.1.0 (c (n "cuid2") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (f (quote ("num-bigint"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "16nak1dzina34i3wqzwyx34cx3lglxjg1iln29l4xdghq3qbzsqx")))

(define-public crate-cuid2-0.1.1 (c (n "cuid2") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "cuid-util") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("num-bigint"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "0vl6dlbagshvmfm61lrbwx5lsp4vlfbds13n37kndrc2qhzhyiky")))

(define-public crate-cuid2-0.1.2 (c (n "cuid2") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "cuid-util") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("num-bigint"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "1s6hr4sca25fpfm6jk8p41avfgwpq7453l0a96vpvmigsnn9rna7")))

