(define-module (crates-io cu id cuid) #:use-module (crates-io))

(define-public crate-cuid-0.1.0 (c (n "cuid") (v "0.1.0") (d (list (d (n "criterion") (r "~0.2") (d #t) (k 2)) (d (n "hostname") (r "~0.1") (d #t) (k 0)) (d (n "lazy_static") (r "~1.2") (d #t) (k 0)) (d (n "rand") (r "~0.6") (d #t) (k 0)))) (h "0bbiw8svchqryq168q63yhjvk97ijz3pcnxdklyr2rd79vv30195")))

(define-public crate-cuid-1.0.0 (c (n "cuid") (v "1.0.0") (d (list (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "hostname") (r "~0.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)) (d (n "rand") (r "~0.7") (d #t) (k 0)))) (h "1zz8555y8vzli0hxxla23fhf1048zknxdc2xy2plp8ig4r1j5i4a")))

(define-public crate-cuid-1.0.1 (c (n "cuid") (v "1.0.1") (d (list (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "hostname") (r "~0.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)) (d (n "rand") (r "~0.7") (d #t) (k 0)))) (h "1c4ilb9ij4hqmkgr1klwxvd04qwdb67bj5zrghbx5sfaf9dg5gnw")))

(define-public crate-cuid-1.0.2 (c (n "cuid") (v "1.0.2") (d (list (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "hostname") (r "~0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)) (d (n "rand") (r "~0.8.0") (d #t) (k 0)))) (h "09ic07yd9vrhgyx0n69y8xn3bzqfl0w03nzg0g79qc7f0l1f2s41")))

(define-public crate-cuid-1.1.0 (c (n "cuid") (v "1.1.0") (d (list (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "hostname") (r "~0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)) (d (n "rand") (r "~0.8.0") (d #t) (k 0)))) (h "1zdlm4vrkyraahw1lvwvg3gf6mwd1jhi90hfhhv7zq6cm4sm3g16")))

(define-public crate-cuid-1.2.0 (c (n "cuid") (v "1.2.0") (d (list (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "hostname") (r "~0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)) (d (n "rand") (r "~0.8.0") (d #t) (k 0)))) (h "01r87hfrimf85920yymibx3nfk8n1295gwh62r3gi1330lf8k8nj")))

(define-public crate-cuid-1.3.0 (c (n "cuid") (v "1.3.0") (d (list (d (n "base36") (r "^0.0.1") (d #t) (k 0)) (d (n "bigint") (r "^4.4.3") (d #t) (k 0)) (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "cuid2") (r "^0.1.0") (d #t) (k 0)) (d (n "hostname") (r "~0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("num-bigint"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.0") (d #t) (k 0)))) (h "0lczqihwvl1vxbab6y1h8mp29cm5n6kj5vd4d1xbqzjpf7x5iyb7")))

(define-public crate-cuid-1.3.1 (c (n "cuid") (v "1.3.1") (d (list (d (n "base36") (r "^0.0.1") (d #t) (k 0)) (d (n "bigint") (r "^4.4.3") (d #t) (k 0)) (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "cuid2") (r "^0.1.0") (d #t) (k 0)) (d (n "hostname") (r "~0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("num-bigint"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.0") (d #t) (k 0)))) (h "1yz6zs1v6h0spwp08wljw6bz3n9sdzsgyhmih339qpsgb6n06pgy")))

(define-public crate-cuid-1.3.2 (c (n "cuid") (v "1.3.2") (d (list (d (n "base36") (r "^0.0.1") (d #t) (k 0)) (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "cuid-util") (r "^0.1.0") (d #t) (k 0)) (d (n "cuid2") (r "^0.1.0") (d #t) (k 0)) (d (n "hostname") (r "~0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("num-bigint"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.0") (d #t) (k 0)))) (h "0fg7afgayd41fhaxs8ffgpi9002x8a45qv4kj8y7dsrq3nqlsaai")))

