(define-module (crates-io cu id cuid-util) #:use-module (crates-io))

(define-public crate-cuid-util-0.1.0 (c (n "cuid-util") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (f (quote ("num-bigint"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 2)))) (h "04rpihfihp5aidlb3xmgb2izx6mfvzw2v6w1fk5bgwbg6ghbz8jy")))

