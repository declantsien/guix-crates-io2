(define-module (crates-io cu bo cuboid) #:use-module (crates-io))

(define-public crate-cuboid-0.1.0 (c (n "cuboid") (v "0.1.0") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.43.0") (d #t) (k 0)))) (h "1xgcj7r26r9jzs990hqhy35b1c3z4dqkzr155yzz8xg7fgkg1v3q")))

