(define-module (crates-io cu bo cubo) #:use-module (crates-io))

(define-public crate-cubo-0.0.1 (c (n "cubo") (v "0.0.1") (d (list (d (n "ferris-says") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0d12n7s2pdbkrj9i0is6283sj8633c1rblxlhyx6w2myc58sbm4c")))

