(define-module (crates-io cu ck cuckoo) #:use-module (crates-io))

(define-public crate-cuckoo-0.2.0 (c (n "cuckoo") (v "0.2.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1g36cq250pglspqfbw6dfa8l5qdwrwmy45zvnv9ghancj6s8yg14") (f (quote (("default") ("VERBOSE_FLAG"))))))

(define-public crate-cuckoo-0.3.0 (c (n "cuckoo") (v "0.3.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "01bcc1zaxzng8r5ff48cxa6hx27rcsgqbwsazs14fa27wmr5jlnl") (f (quote (("default") ("VERBOSE_FLAG"))))))

(define-public crate-cuckoo-0.4.0 (c (n "cuckoo") (v "0.4.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "14kz0sh4si6iycd114kr359k2d1wgwrr4smw9456fismsliv64rf") (f (quote (("default") ("VERBOSE_FLAG"))))))

(define-public crate-cuckoo-0.5.0 (c (n "cuckoo") (v "0.5.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "19bxs8x6ibaxcpv3fajwavyb0cprdf8pi93p0bgxpvxlkbcazbnq") (f (quote (("default") ("VERBOSE_FLAG"))))))

(define-public crate-cuckoo-0.6.0 (c (n "cuckoo") (v "0.6.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1vgajfmhsarasz5b81mdqlcf323cfcjky6ry3jvmxqrp5sahz8ni") (f (quote (("default") ("VERBOSE_FLAG"))))))

(define-public crate-cuckoo-0.7.0 (c (n "cuckoo") (v "0.7.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1xna2dpb7rl9jd7dmxa3j0b60lylrq8p76gc82ad9qw1ibfrhpp2") (f (quote (("default") ("VERBOSE_FLAG"))))))

(define-public crate-cuckoo-0.8.0 (c (n "cuckoo") (v "0.8.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1zzh7wi4zynqgbpyjxinmssxvbm4f05hmci32kv9m7s96s46rygw") (f (quote (("default") ("VERBOSE_FLAG_SUMMARY") ("VERBOSE_FLAG_ALL"))))))

