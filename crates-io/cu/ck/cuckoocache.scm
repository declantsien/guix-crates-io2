(define-module (crates-io cu ck cuckoocache) #:use-module (crates-io))

(define-public crate-cuckoocache-0.1.0 (c (n "cuckoocache") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)))) (h "15lg471lzmgmiiy1ngf7r81z4cdy1yb22qhl3a4vdw31gx26gd62")))

(define-public crate-cuckoocache-0.1.1 (c (n "cuckoocache") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)))) (h "0gb1vmsvzmbbbrc5yafazknhy7ddpg4imq7759mah4s8xjxbcw44")))

(define-public crate-cuckoocache-0.1.2 (c (n "cuckoocache") (v "0.1.2") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)))) (h "16j3k5ks2z63rqlk30i5pncxdn0xbbnk77mvllm5mg0ifsnk51yl")))

