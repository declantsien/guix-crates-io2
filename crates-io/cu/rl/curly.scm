(define-module (crates-io cu rl curly) #:use-module (crates-io))

(define-public crate-curly-0.0.0 (c (n "curly") (v "0.0.0") (h "1agx43kmg8v42w64nrx48pnrwq5gzbb6nv9anjqdman0as1sna66")))

(define-public crate-curly-0.0.1 (c (n "curly") (v "0.0.1") (h "0jsgqd2v0hdrs0pycrr8lkr95r5rx1p6dlrlvhciw2hh9i1b7pcv")))

