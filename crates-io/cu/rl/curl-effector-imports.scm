(define-module (crates-io cu rl curl-effector-imports) #:use-module (crates-io))

(define-public crate-curl-effector-imports-0.1.1 (c (n "curl-effector-imports") (v "0.1.1") (d (list (d (n "curl-effector-types") (r "^0.1.1") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.14.0") (f (quote ("logger"))) (d #t) (k 0)))) (h "1a4zgg4s04cgvcnl467l3c5dwa1583vr61k8agjk53gk033632h3")))

