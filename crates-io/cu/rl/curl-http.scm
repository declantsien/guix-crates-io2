(define-module (crates-io cu rl curl-http) #:use-module (crates-io))

(define-public crate-curl-http-0.1.0 (c (n "curl-http") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.14") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.77") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "0df4kbgkmvnic9pb3z5xglwap46mmh94wa4d7x1gifr45aqyzz6c")))

