(define-module (crates-io cu rl curl-easybuilder) #:use-module (crates-io))

(define-public crate-curl-easybuilder-0.1.0 (c (n "curl-easybuilder") (v "0.1.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)))) (h "0h094s1wdsb2apmiaiajzg8b09cvxncv6qz1drf6ky7hjm97ch4i")))

