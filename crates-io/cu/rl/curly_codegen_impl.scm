(define-module (crates-io cu rl curly_codegen_impl) #:use-module (crates-io))

(define-public crate-curly_codegen_impl-0.0.0 (c (n "curly_codegen_impl") (v "0.0.0") (h "17xngykdhrlr2p5zywyry2jfw3shpl861l4840q310325v4g0a1q")))

(define-public crate-curly_codegen_impl-0.0.1 (c (n "curly_codegen_impl") (v "0.0.1") (h "0q8l3jjlqavl2m0s4xqzl3ijwiy269hjcwnfczpi7n4p1xb2baz2")))

