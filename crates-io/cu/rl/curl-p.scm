(define-module (crates-io cu rl curl-p) #:use-module (crates-io))

(define-public crate-curl-p-0.1.0 (c (n "curl-p") (v "0.1.0") (d (list (d (n "sponge-preview") (r "^0.2.0") (d #t) (k 0)))) (h "1w9vpwfcsalh0r3wiar0mgyn73fd8flbigjbi1llgzdpm0z78m28")))

(define-public crate-curl-p-0.2.0 (c (n "curl-p") (v "0.2.0") (d (list (d (n "sponge-preview") (r "^0.2.0") (d #t) (k 0)))) (h "10s9wrgs5ynkza24qynjlwrxmy63llwwm9n4spjxn62pbvq7j554")))

(define-public crate-curl-p-0.3.0 (c (n "curl-p") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0ipym551vcdwm1jv86viqkg8ppv6g8d69vvp4f5nc4q5ihap238k")))

