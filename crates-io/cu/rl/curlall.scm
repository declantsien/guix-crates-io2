(define-module (crates-io cu rl curlall) #:use-module (crates-io))

(define-public crate-curlall-0.1.0 (c (n "curlall") (v "0.1.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1bwa8ms5n687vay4f4x7ydzj1wa760rg6jxmwib6rsgr961x2nyk")))

(define-public crate-curlall-0.2.0 (c (n "curlall") (v "0.2.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bxi7gin6ji1g30f94kv0wflq486kva1cwqxfkahmgd85nqpi44y")))

