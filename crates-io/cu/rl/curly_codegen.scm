(define-module (crates-io cu rl curly_codegen) #:use-module (crates-io))

(define-public crate-curly_codegen-0.0.0 (c (n "curly_codegen") (v "0.0.0") (h "0zgf44xp58z0m006h13j5hkh56h1svlpkxd7fy8ssi5a5abcmpiw")))

(define-public crate-curly_codegen-0.0.1 (c (n "curly_codegen") (v "0.0.1") (h "1mi67ny8xrxpfd8nx63vzbdjb0q6j01vw9a3qgvi88jjz3f5b605")))

