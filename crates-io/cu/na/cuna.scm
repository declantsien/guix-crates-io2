(define-module (crates-io cu na cuna) #:use-module (crates-io))

(define-public crate-cuna-0.1.0 (c (n "cuna") (v "0.1.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fgq1hpjkd7cz4dfnr67m5hb5jlcq91h82y0h205qqpbgfj2bls0")))

(define-public crate-cuna-0.2.0 (c (n "cuna") (v "0.2.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1knp3fjicj9gplciih9l5s0x4nns8k5v6znsb0w85sdgids29w99")))

(define-public crate-cuna-0.2.1 (c (n "cuna") (v "0.2.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i7q4m60pv41zci0lals5fid56yil3mv269v9n1r7pdg943zksdq")))

(define-public crate-cuna-0.3.0 (c (n "cuna") (v "0.3.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jzq7kqv54zfd4lnfgl7k20fyz9fanbk38in71p2czdnp4lbzivw")))

(define-public crate-cuna-0.3.1 (c (n "cuna") (v "0.3.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wqmpsgbvf85aab8sy9layk140pnx0pb6hd4wd9m7qxx28hn621k")))

(define-public crate-cuna-0.4.0 (c (n "cuna") (v "0.4.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jfxv0h8ngx77s1micnb70l6q98djmk99vnsjqgb6a92r2qiz1lf")))

(define-public crate-cuna-0.4.1 (c (n "cuna") (v "0.4.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qsaiyixbg7zwgsgfv4si9c9c0igpn9qbiyfyig8n6h60vflzh1h")))

(define-public crate-cuna-0.5.0 (c (n "cuna") (v "0.5.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1as88spsrc1cr070zzsmxarh8w7ihwskh9k2i4g6k6djhx425mi0")))

(define-public crate-cuna-0.5.1 (c (n "cuna") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0z2ygzr18z4mfh5q3d66sishha0607r14hbiwq7vynjgfb02p02j")))

(define-public crate-cuna-0.5.2 (c (n "cuna") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12dzm6kldrir5l4v252vjdl5zyp157v8bqlnbvqs5bbbbai6396c")))

(define-public crate-cuna-0.6.0 (c (n "cuna") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00an4jysxa7czqa2rhqalxx1gzc6r5ghnyxqgw6m4gbfmhgnx0v3") (y #t)))

(define-public crate-cuna-0.6.1 (c (n "cuna") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11ffcy18jy9b13yq0i33nqc0r83ckilkcxb5xgcagr1l0m1kl289") (y #t)))

(define-public crate-cuna-0.6.2 (c (n "cuna") (v "0.6.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zr2gacabv7fbamwzmjzi2dw5mc0jml4afbis2g779iblpc1yp28") (y #t)))

(define-public crate-cuna-0.6.3 (c (n "cuna") (v "0.6.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rknpfkyfb9s5gjvy961z5kphpf5wpcc5d8bqcmrp5n5r7z2zvd6")))

(define-public crate-cuna-0.7.0 (c (n "cuna") (v "0.7.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1qbf7jhp6d5sxaiw1p3j1dsf0ry9z7ydk2ks7shqcna54p2aqpws")))

