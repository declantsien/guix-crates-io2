(define-module (crates-io cu iv cuivre) #:use-module (crates-io))

(define-public crate-cuivre-0.1.0 (c (n "cuivre") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "ron") (r "^0.4.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)))) (h "1rgqk5z1mcdpv1d3j1b7izny1mxxv4jma2kfsjy30vyhxi9wqirp")))

(define-public crate-cuivre-0.1.1 (c (n "cuivre") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "ron") (r "^0.4.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)))) (h "1r3nvyagfc0gc8d2f524klxqp25wxbl860swb6kfkxrs1bhqlq61")))

(define-public crate-cuivre-0.1.2 (c (n "cuivre") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "ron") (r "^0.4.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)))) (h "1y9igkv7df7zdgy4r19p5hw2alq2abf9ixa8csvkxhawrrgdz0s7")))

(define-public crate-cuivre-0.1.3 (c (n "cuivre") (v "0.1.3") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "ron") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.6.3") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.7") (d #t) (k 0)))) (h "0y54yw5w68x8ajfdha90ikr4nlbfp7ww7cp0sbvrgnx4h346jmx9")))

(define-public crate-cuivre-0.1.4 (c (n "cuivre") (v "0.1.4") (d (list (d (n "cgmath") (r "^0.16.1") (f (quote ("swizzle"))) (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "ron") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.6.3") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.7") (d #t) (k 0)))) (h "0d8p4gapdrdjm5vnkzsvwyry5h44hl6ks0n9c22ldi2ydq5vw0bc")))

