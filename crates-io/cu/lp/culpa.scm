(define-module (crates-io cu lp culpa) #:use-module (crates-io))

(define-public crate-culpa-1.0.0 (c (n "culpa") (v "1.0.0") (d (list (d (n "culpa-macros") (r "=1.0.0") (d #t) (k 0)))) (h "0gkpnizirms5yq26wlw5bkba6yxkhhyhwf647bspybzczpadzbp1")))

(define-public crate-culpa-1.0.1 (c (n "culpa") (v "1.0.1") (d (list (d (n "culpa-macros") (r "=1.0.1") (d #t) (k 0)))) (h "1pkhyhmy9i7cg5lhr0j2y7dwf8mn0fzsv6flxd3jb27x25jr8brl")))

(define-public crate-culpa-1.0.2 (c (n "culpa") (v "1.0.2") (d (list (d (n "culpa-macros") (r "=1.0.2") (d #t) (k 0)))) (h "1lqzzl88vs3h6ljpj6gi8wgkwyqm9gp6dmsnbbzv873v67lvzq2s")))

