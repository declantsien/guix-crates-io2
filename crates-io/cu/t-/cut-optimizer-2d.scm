(define-module (crates-io cu t- cut-optimizer-2d) #:use-module (crates-io))

(define-public crate-cut-optimizer-2d-0.1.0 (c (n "cut-optimizer-2d") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (o #t) (d #t) (k 0)))) (h "0k46hy53ss2lzflv5wyj8v2vlvkywikgnirxq948rb8579aql1fk") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-cut-optimizer-2d-0.1.1 (c (n "cut-optimizer-2d") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (o #t) (d #t) (k 0)))) (h "1xjy4qk7faj5nv9anf7sbc20wjxl9l7hmgzzqnkpyjhyi9vkdx7i") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-cut-optimizer-2d-0.1.2 (c (n "cut-optimizer-2d") (v "0.1.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04dy30yv9vwnspksh202waav99wyxgr47ggh2j9a71sch9lrryvr") (f (quote (("serialize" "serde")))) (y #t)))

(define-public crate-cut-optimizer-2d-0.1.3 (c (n "cut-optimizer-2d") (v "0.1.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ysx9n9kf345anjwlhsp6c4pswf4ihamqd5c775d8w51hza93jjz") (f (quote (("serialize" "serde"))))))

(define-public crate-cut-optimizer-2d-0.1.4 (c (n "cut-optimizer-2d") (v "0.1.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19psxl0kbn6vs6niyr8szia23bw7w0wl4c9k8nqrxv74fmhdk2p5") (f (quote (("serialize" "serde"))))))

(define-public crate-cut-optimizer-2d-0.1.5 (c (n "cut-optimizer-2d") (v "0.1.5") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1k2acsrzmh39wndkr5qpnb7752gmid4dkmvmfapg31iaxdcvkmcy") (f (quote (("serialize" "serde"))))))

(define-public crate-cut-optimizer-2d-0.2.0 (c (n "cut-optimizer-2d") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lhd4m7i7m2svdyvx77ja1w3pkv5a8wlxzzf97ayl280jz457hjp") (f (quote (("serialize" "serde"))))))

(define-public crate-cut-optimizer-2d-0.3.0 (c (n "cut-optimizer-2d") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dmbg8b2vmvn8gswnqpxkh3cppw2jv7ggq0cm6xx6acdz7f42l6h") (f (quote (("serialize" "serde"))))))

(define-public crate-cut-optimizer-2d-0.4.0 (c (n "cut-optimizer-2d") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hsvb3sf1ch6fp4as2xq0rypdbhb0j4ilhpbrbrddfm69qfki9qh") (f (quote (("serialize" "serde"))))))

(define-public crate-cut-optimizer-2d-0.4.1 (c (n "cut-optimizer-2d") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "150k2k5xiz7lvs7rjn9bpd4204xk8jmrkhv46l00zvijxznzh0i3") (f (quote (("serialize" "serde"))))))

(define-public crate-cut-optimizer-2d-0.4.2 (c (n "cut-optimizer-2d") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "0xcp1ngya2364d71by386z3z9llrbkfmz9s2awjj2a8rz2mr2z2b") (f (quote (("serialize" "serde"))))))

