(define-module (crates-io cu t- cut-optimizer-1d) #:use-module (crates-io))

(define-public crate-cut-optimizer-1d-0.1.0 (c (n "cut-optimizer-1d") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cyixihvn9hm1ggdzl82byi4p946x4y9wmnlkpx7cdf13a6d3xxq") (f (quote (("serialize" "serde"))))))

(define-public crate-cut-optimizer-1d-0.2.0 (c (n "cut-optimizer-1d") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m2lrrrxgbsvdcvm538p2g73bbfc4fazrgl3f8iarslfwzpym09z") (f (quote (("serialize" "serde"))))))

(define-public crate-cut-optimizer-1d-0.2.1 (c (n "cut-optimizer-1d") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04lia89vrgff87dfs7w05qdhpyg57m1fd5namdy7ryl3wdqvg3n1") (f (quote (("serialize" "serde"))))))

(define-public crate-cut-optimizer-1d-0.2.2 (c (n "cut-optimizer-1d") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "1in3wvk4fbahlyb2bqiz5dvq7xfyqlvbpfyf60ln8688gjmsn8kz") (f (quote (("serialize" "serde"))))))

