(define-module (crates-io cu pi cupi) #:use-module (crates-io))

(define-public crate-cupi-0.1.0 (c (n "cupi") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mmap") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "spidev") (r "*") (d #t) (k 0)))) (h "11bysazpb934wkw1i0f1lamh6wck1wzll60780qn0hnlqw9k9mdg")))

