(define-module (crates-io cu pi cupid) #:use-module (crates-io))

(define-public crate-cupid-0.1.0 (c (n "cupid") (v "0.1.0") (h "1lb755c5fx1wjfqfbabn11xnbc73diwdkq3gzgr2xspz7fvqy8ly")))

(define-public crate-cupid-0.2.0 (c (n "cupid") (v "0.2.0") (h "1g9ipysakp6rn6vjg5vn2vvpkc0lwc47vnxscpzmn47gkfwykfdy")))

(define-public crate-cupid-0.2.1 (c (n "cupid") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "0n1s42qb3p6yan61qrsz2vl0n0qn0nrzppjkcq8d9vxz0irn3gia")))

(define-public crate-cupid-0.3.0 (c (n "cupid") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "069vhhag4wyvd8ywrmd9h1lpcwz07hfnm9lj74zp5qlimkcbaly1") (f (quote (("unstable"))))))

(define-public crate-cupid-0.4.0 (c (n "cupid") (v "0.4.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0plrzx3rsadaxcyyfhxl8hxazns5ws2j07rl9aqscwbg7iplrmp7") (f (quote (("unstable"))))))

(define-public crate-cupid-0.5.0 (c (n "cupid") (v "0.5.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "019gw605c4a3jkblpy0knbqa5q99q6mspccc9rv8hj58y9lxbhhv") (f (quote (("unstable"))))))

(define-public crate-cupid-0.6.0 (c (n "cupid") (v "0.6.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "rustc_version") (r "^0.2.2") (d #t) (k 1)))) (h "05idn80l6ky4w965fibl7ckd09sbsnyg1xc0cpns4wqkxd17mxyw")))

(define-public crate-cupid-0.6.1 (c (n "cupid") (v "0.6.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "rustc_version") (r "^0.2.2") (d #t) (k 1)))) (h "18ci3zyzzfirnycvgl15k98vhg4hxsl3lkl5llwcqrxmhhm3bbcb")))

