(define-module (crates-io cu -c cu-core) #:use-module (crates-io))

(define-public crate-cu-core-0.1.0 (c (n "cu-core") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "cu-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "185wvivvhpxdydynkn65fkfz6f2d7mnha2c120im10jfrbk36ynh")))

