(define-module (crates-io cu rv curved_gear) #:use-module (crates-io))

(define-public crate-curved_gear-0.1.0 (c (n "curved_gear") (v "0.1.0") (d (list (d (n "ascii") (r "^0.7") (d #t) (k 0)) (d (n "bincode") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nettraits") (r "0.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)))) (h "1v8rlkmlw68nvv4zw4rw31c3dmhimrzdrli3qgqq1sigjq468wcp") (y #t)))

