(define-module (crates-io cu rv curve) #:use-module (crates-io))

(define-public crate-curve-0.1.0 (c (n "curve") (v "0.1.0") (h "0z9va8wn9hhkryf0rybkrvkydr4w74g1xn2l3sair576r4m9a42h")))

(define-public crate-curve-0.2.0 (c (n "curve") (v "0.2.0") (d (list (d (n "num") (r "*") (k 0)))) (h "1wn0q46398jx2pv8s7w6b3cqsyij0b4pjrcn5m35dzc038mfsvnn")))

(define-public crate-curve-0.3.0 (c (n "curve") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0a6dymhhci6xqcvrlmxqvzchk644xpp6fkwdsdg3wfc1pkh9bs9i")))

(define-public crate-curve-0.4.0 (c (n "curve") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0rbfpr51hdcj8mfr3y8ns0kgqsrywmkid1cv4nh5q80gczn60aww")))

(define-public crate-curve-0.5.0 (c (n "curve") (v "0.5.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1pdmygcfvlddfkkq5ijpqb345r5kkchiq94fw9ffw56xnw7g6p0l")))

(define-public crate-curve-0.6.0 (c (n "curve") (v "0.6.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1m0wcaacvcqws1yxm3xf0qxvmy25mlqgklzj6wg81rklm820w8f4")))

(define-public crate-curve-0.6.1 (c (n "curve") (v "0.6.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1w0v4ia1wyn93x2h383wwypaf6vg35haf4hyzggxq6hbcs04ij65")))

