(define-module (crates-io cu rv curve25519-dalek-derive) #:use-module (crates-io))

(define-public crate-curve25519-dalek-derive-0.1.0 (c (n "curve25519-dalek-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0nsvhsa04565gac2mv0b2j11w4p4kjy3j9l6lnzwwkc0yjbszzc3")))

(define-public crate-curve25519-dalek-derive-0.1.1 (c (n "curve25519-dalek-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1cry71xxrr0mcy5my3fb502cwfxy6822k4pm19cwrilrg7hq4s7l")))

