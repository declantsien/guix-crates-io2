(define-module (crates-io cu rv curve25519-compact) #:use-module (crates-io))

(define-public crate-curve25519-compact-2.0.0 (c (n "curve25519-compact") (v "2.0.0") (d (list (d (n "ed25519-compact") (r "^2") (f (quote ("random" "std" "x25519" "disable-signatures"))) (k 0)))) (h "0xwg1v8ln932mihfw5lxj6a2i8dhyblih8qjjzwmkrajkh51zy17")))

