(define-module (crates-io cu rv curve-sampling) #:use-module (crates-io))

(define-public crate-curve-sampling-0.1.0 (c (n "curve-sampling") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rgb") (r "^0.8.32") (d #t) (k 0)))) (h "04rzdmdq8qn65khavyjiipb8lq3hb9pf4bsx8xdwkm4h0im4nq4h")))

(define-public crate-curve-sampling-0.2.0 (c (n "curve-sampling") (v "0.2.0") (d (list (d (n "rgb") (r "^0.8.32") (d #t) (k 0)))) (h "1dfy0xlkwbx1ryff3cylgpafrls926sbkd73lc8hzd98s57sqpss")))

(define-public crate-curve-sampling-0.3.0 (c (n "curve-sampling") (v "0.3.0") (d (list (d (n "rgb") (r "^0.8.32") (d #t) (k 0)))) (h "0d69dal3nilcj0xmknz1nrknmncq738vv86w10f92df6jrpip3qh")))

(define-public crate-curve-sampling-0.4.0 (c (n "curve-sampling") (v "0.4.0") (d (list (d (n "rgb") (r "^0.8.32") (d #t) (k 0)))) (h "0grh9s7gwnxvs6n7sqa2i5z7xd53lz5a194xi0dk39k43sld7jhi")))

