(define-module (crates-io cu rv curve25519-parser) #:use-module (crates-io))

(define-public crate-curve25519-parser-0.1.0 (c (n "curve25519-parser") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^2") (d #t) (k 0)) (d (n "der-parser") (r "^3") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "pem") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)) (d (n "x25519-dalek") (r "^0") (d #t) (k 0)))) (h "1gbr0g949yilb8l831djqdcz8mxf4v0ylf9b2j0mgywrx730dcfz")))

(define-public crate-curve25519-parser-0.2.0 (c (n "curve25519-parser") (v "0.2.0") (d (list (d (n "curve25519-dalek") (r "^3") (d #t) (k 0)) (d (n "der-parser") (r "^4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "pem") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)) (d (n "x25519-dalek") (r "^1") (d #t) (k 0)))) (h "1fkdl2dscg1a8rwgcg5p2nfbdwdvfl70srdzcc1pzsdxwgbjv5ki")))

(define-public crate-curve25519-parser-0.3.0 (c (n "curve25519-parser") (v "0.3.0") (d (list (d (n "curve25519-dalek") (r "^3") (f (quote ("u64_backend"))) (k 0)) (d (n "der-parser") (r "^8") (k 0)) (d (n "pem") (r "^1") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "sha2") (r "^0") (k 0)) (d (n "x25519-dalek") (r "^1") (k 0)))) (h "09l7z6igqj4kmc4y0bh1k078hdmy5zvkpksjpc4pkqlx3njnxsxg")))

(define-public crate-curve25519-parser-0.4.0 (c (n "curve25519-parser") (v "0.4.0") (d (list (d (n "curve25519-dalek") (r "^4") (k 0)) (d (n "der-parser") (r "^8") (k 0)) (d (n "pem") (r "^3") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "sha2") (r "^0") (k 0)) (d (n "x25519-dalek") (r "^2") (f (quote ("static_secrets"))) (k 0)))) (h "165001qkv1m216bnmrx43m1wcrkq6pszq80g1k6y6w266kbnrs1j")))

