(define-module (crates-io cu rv curve-trees) #:use-module (crates-io))

(define-public crate-curve-trees-0.1.0 (c (n "curve-trees") (v "0.1.0") (d (list (d (n "bulletproofs-plus") (r "^0.1") (k 0)) (d (n "ciphersuite") (r "^0.3") (k 0)) (d (n "ecip") (r "^0.1") (f (quote ("pasta"))) (k 0)) (d (n "multiexp") (r "^0.3") (k 0)) (d (n "pasta_curves") (r "^0.5") (f (quote ("bits" "alloc"))) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "subtle") (r "^2.4") (k 0)) (d (n "transcript") (r "^0.3") (d #t) (k 0) (p "flexible-transcript")) (d (n "zeroize") (r "^1.5") (f (quote ("zeroize_derive"))) (k 0)))) (h "1mbfgq9kaxd09sk0hr64i80bgp858blqp95w6yfdq0jmhgkrxqr1")))

