(define-module (crates-io cu rv curves-rs) #:use-module (crates-io))

(define-public crate-curves-rs-0.1.0 (c (n "curves-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "19dnyx0zhg3r612xb8kw2s4mn4i3fmxfjn49k2isrp73ydq4x2qz")))

