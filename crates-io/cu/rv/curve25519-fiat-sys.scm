(define-module (crates-io cu rv curve25519-fiat-sys) #:use-module (crates-io))

(define-public crate-curve25519-fiat-sys-0.1.4 (c (n "curve25519-fiat-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1x1m87kh3cj3nv51a4gjrqsnd7q8bz0y7rkz6688nqbfvj9d7xlk") (y #t)))

(define-public crate-curve25519-fiat-sys-0.1.5 (c (n "curve25519-fiat-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "149hz55ss9sm9qkyx59f268ddw01p77ssqpvs480y6wajq59jpl8") (y #t)))

(define-public crate-curve25519-fiat-sys-0.1.6 (c (n "curve25519-fiat-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "11h0s699viih35wahk8ckrvzxk969av7k1d5h5k7cdf7xcb8nna3") (y #t)))

(define-public crate-curve25519-fiat-sys-0.1.7 (c (n "curve25519-fiat-sys") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "00xavsrq9k2n6r1fhcj8z3zcaz72cqswbs6hbk80i7lcb3d8xmpp") (y #t)))

(define-public crate-curve25519-fiat-sys-0.1.8 (c (n "curve25519-fiat-sys") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ls6283i852rbdnd1z0vn65zg01pwjwqjf0zi2cfvaxnx3ndhhh5") (y #t)))

(define-public crate-curve25519-fiat-sys-0.1.9 (c (n "curve25519-fiat-sys") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0f3r02wim74p3w9jbqi1rwakcnwp0g97pi36iyasl9b8gaiw800i") (y #t) (l "curve25519_fiat_sys")))

(define-public crate-curve25519-fiat-sys-0.1.10 (c (n "curve25519-fiat-sys") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0w6fs9qn5yx9hngjkk54cbyjkp5qx2bwg9dp02kh9cws5ynmz4af") (y #t) (l "curve25519-fiat-sys")))

(define-public crate-curve25519-fiat-sys-0.1.11 (c (n "curve25519-fiat-sys") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0y0m64k2z5qfahjb1ka2gh0jgapn78g8kxl6g2hy7k3m3ivzyf0d") (y #t) (l "curve25519-fiat-sys")))

(define-public crate-curve25519-fiat-sys-0.1.12 (c (n "curve25519-fiat-sys") (v "0.1.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "13xh5ln26gccq576yislb8d27ipr0lxfphlz4rc2dlir0pyr4sz1") (y #t) (l "curve25519-fiat-sys")))

