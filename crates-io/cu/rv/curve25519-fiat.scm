(define-module (crates-io cu rv curve25519-fiat) #:use-module (crates-io))

(define-public crate-curve25519-fiat-0.1.0 (c (n "curve25519-fiat") (v "0.1.0") (d (list (d (n "sha2") (r "^0.8.0") (d #t) (k 1)))) (h "0pwg142q9z9jadwxxkirh5wry8kgfmryyfg1746gf4b1ac4xilny") (y #t)))

(define-public crate-curve25519-fiat-0.1.1 (c (n "curve25519-fiat") (v "0.1.1") (d (list (d (n "sha2") (r "^0.8.0") (d #t) (k 1)))) (h "1l1fgrls6wdygn3ig2z1c45mm1hr3k6drj6mccqd06cd4f7vrsqi") (y #t)))

(define-public crate-curve25519-fiat-0.1.2 (c (n "curve25519-fiat") (v "0.1.2") (d (list (d (n "sha2") (r "^0.8.0") (d #t) (k 1)))) (h "1k3b4jdiwwgycphb290a644hkg7r3kjfklymbq6w5df9d6gcncga") (y #t)))

(define-public crate-curve25519-fiat-0.1.4 (c (n "curve25519-fiat") (v "0.1.4") (d (list (d (n "sha2") (r "^0.8.0") (d #t) (k 1)))) (h "0j3d9l0vm69qw1ly3g5919mfwr6rjwpl1h7m0m8x0nrajnih382d")))

