(define-module (crates-io cu rb curb) #:use-module (crates-io))

(define-public crate-curb-0.1.0 (c (n "curb") (v "0.1.0") (d (list (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0nablg8j7km2h18lk3cs2s1n1z9c9wyp9ji3kgnq4yxvc0vgx54w")))

(define-public crate-curb-0.1.1 (c (n "curb") (v "0.1.1") (d (list (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0i6i67fvqlkdb7nwd0z9kps74adb05nagfb7g2r4xn8628dy1b4d")))

(define-public crate-curb-0.1.2 (c (n "curb") (v "0.1.2") (d (list (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0pizvd64crz0s0pfsfbza21fc7lil5bxcwik2f2fbfhbzr463gxq")))

(define-public crate-curb-0.1.3 (c (n "curb") (v "0.1.3") (d (list (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1cspryi58k9nq4hpaijmd32pay1a22wyr25is8ms8xbqvp6bqk1p")))

(define-public crate-curb-0.1.4 (c (n "curb") (v "0.1.4") (d (list (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "034rf8h53rimilzzbhdqsll9981s9w88kpic46py33b85yayk8wf")))

(define-public crate-curb-0.1.5 (c (n "curb") (v "0.1.5") (d (list (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "01m7m2kjnd1v79wgbl3qr7mcqrr7dj6300dh6y8dqw0j7hc18vb4")))

(define-public crate-curb-0.1.6 (c (n "curb") (v "0.1.6") (d (list (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "19p1jzk99c3rzsnip4b9370q9ih4i1ss65schmh3qw7f7hv7wh0v")))

