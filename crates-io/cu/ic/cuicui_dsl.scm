(define-module (crates-io cu ic cuicui_dsl) #:use-module (crates-io))

(define-public crate-cuicui_dsl-0.3.0 (c (n "cuicui_dsl") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10") (k 0)))) (h "1v8l84qvwwvi7l7vw7xy8m82gvd6y7y4s7fj4x53a1bd0wr3k6xl")))

(define-public crate-cuicui_dsl-0.4.0 (c (n "cuicui_dsl") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "13p8ir31ja4ivmb4wfr84x6v5zdmiiklwski69ygd4jck8zg6d52") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.5.0 (c (n "cuicui_dsl") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "1b0n7vp5qsv1jjyfniqrkp5jvsmsgy5qw9yhrnidwzscqxra1rj2") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.6.0 (c (n "cuicui_dsl") (v "0.6.0") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "1q9carwqz1sfhy27fcmd7zdaf3qb2j7kz50a4shqnlirs1ckcgsk") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.7.0 (c (n "cuicui_dsl") (v "0.7.0") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "1c97pfi8n7idk77k91p4sx4vkj76aymqqmdpjxz818maal2vs6k9") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.8.0 (c (n "cuicui_dsl") (v "0.8.0") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "0l49hkh64ds5pd18lq7lagfgxgr19226b1742cyp78arpvdbbynz") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.8.1 (c (n "cuicui_dsl") (v "0.8.1") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "1qf1fd6rrh5nd2z9a248wbia9w8jsjvwh61shmsfirpy83q6fa51") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.8.2 (c (n "cuicui_dsl") (v "0.8.2") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "1pwwn24y2byybs4jq6as4si93r8k560fhciqxi6zq3fav2gkqskw") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.9.0 (c (n "cuicui_dsl") (v "0.9.0") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "12b5czcc9p158mk2hr7blmgrc7j00lmmzlihz3bc31hhpr7vypa3") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.10.0 (c (n "cuicui_dsl") (v "0.10.0") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "07qy3797qk6s2sywhzxj7ik26f1scsdx1b172ymsj4wr7gr688fw") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.10.1 (c (n "cuicui_dsl") (v "0.10.1") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "0hh4qcqqmzdzagkja0sypbn4cpq9kv012hfz52w3kv1bq6g7kinq") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.10.2 (c (n "cuicui_dsl") (v "0.10.2") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "02p1mnia0wz04hr3i0fd05rrh8isr5l75c6k4f1j6kh020am52b3") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.11.0 (c (n "cuicui_dsl") (v "0.11.0") (d (list (d (n "bevy") (r "^0.12") (k 0)))) (h "1lp32zprkmadc0qsj571771asg2xk31bcd97w1a2whfn6z58lg2g") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

(define-public crate-cuicui_dsl-0.12.0 (c (n "cuicui_dsl") (v "0.12.0") (d (list (d (n "bevy") (r "^0.12.0") (k 0)))) (h "12q5dn5hmd4lv71rn2v0rqsxz9iyv8anwppqn9d297na1fbicsxp") (f (quote (("test_and_doc" "bevy/bevy_render" "bevy/bevy_asset") ("default"))))))

