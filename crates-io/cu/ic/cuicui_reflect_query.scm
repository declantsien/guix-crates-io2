(define-module (crates-io cu ic cuicui_reflect_query) #:use-module (crates-io))

(define-public crate-cuicui_reflect_query-0.1.0 (c (n "cuicui_reflect_query") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (k 0)))) (h "05gai61r4dgfahfacn9j01r08ybz9rcavjji83884jg2sywp4yhm") (f (quote (("register_ui" "bevy/bevy_ui") ("register_text" "bevy/bevy_text") ("register_sprite" "bevy/bevy_sprite") ("register_render" "bevy/bevy_render") ("register_pbr" "bevy/bevy_pbr") ("register_core_pipeline" "bevy/bevy_core_pipeline") ("default"))))))

(define-public crate-cuicui_reflect_query-0.1.1 (c (n "cuicui_reflect_query") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10") (k 0)))) (h "1h5y6zna7yxdlr6lm322pamymlvllwqnf5i6nwd86mssy3yah45d") (f (quote (("register_ui" "bevy/bevy_ui") ("register_text" "bevy/bevy_text") ("register_sprite" "bevy/bevy_sprite") ("register_render" "bevy/bevy_render") ("register_pbr" "bevy/bevy_pbr") ("register_core_pipeline" "bevy/bevy_core_pipeline") ("default"))))))

