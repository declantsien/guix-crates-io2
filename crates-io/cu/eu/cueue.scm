(define-module (crates-io cu eu cueue) #:use-module (crates-io))

(define-public crate-cueue-0.1.0 (c (n "cueue") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "05yssr51zvwy504kiq9cn42ap7bmf2jdlnzl2162ck0rz7g7v5px")))

(define-public crate-cueue-0.1.1 (c (n "cueue") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "1lzxv2fk651dbqvjnxh5f94swlxszi3lbrzz7c6gcsyw6byq2n3n")))

(define-public crate-cueue-0.2.0 (c (n "cueue") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "15v3ingi2rij8hbp3289j6x6csj5acps9ijgc1acih2fm6g5q3i6")))

(define-public crate-cueue-0.3.0 (c (n "cueue") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "159ab7i67sals4m3dm5va8b2l6kkcx5pqygp2d49w98jhcp82xp1")))

(define-public crate-cueue-0.3.1 (c (n "cueue") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "1mhjgiq9x74cmkfw2laxn78zc29xbk8kzkq1ps2cc8bsdk7mg3cm")))

