(define-module (crates-io cu bi cubism) #:use-module (crates-io))

(define-public crate-cubism-0.1.0 (c (n "cubism") (v "0.1.0") (d (list (d (n "ab_glyph") (r "^0.2") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0hazaxq6g4g2g49klbjs5gxvzlqn6swyn0fc63yn5pfrrk895y27")))

(define-public crate-cubism-0.2.0 (c (n "cubism") (v "0.2.0") (d (list (d (n "ab_glyph") (r "^0.2") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0siqd0pzzf8kj3jscb8mnj42mp6y3p8vinprllfi8x2hv66f7c8b") (f (quote (("default" "bevy_color") ("bevy_color" "bevy"))))))

