(define-module (crates-io cu bi cubik) #:use-module (crates-io))

(define-public crate-cubik-0.1.0 (c (n "cubik") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "glium") (r "^0.30") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "19awwviq4rcd7yjpppc1rmmpp4jkxxgji2paad1pl24w3y6yfal4")))

