(define-module (crates-io cu bi cubic_spline) #:use-module (crates-io))

(define-public crate-cubic_spline-0.5.0 (c (n "cubic_spline") (v "0.5.0") (d (list (d (n "wasm-bindgen") (r "^0.2.42") (d #t) (k 0)))) (h "0lid61vrippgzbp88galipkfii891vxwr2j6dphpr2jpfisg3xkd")))

(define-public crate-cubic_spline-0.6.0 (c (n "cubic_spline") (v "0.6.0") (d (list (d (n "wasm-bindgen") (r "^0.2.42") (d #t) (k 0)))) (h "1aa9b2aldmv5rf1rlvkrv0w9i5327z30bns61dvjmhfzb61lbwxc")))

(define-public crate-cubic_spline-0.6.3 (c (n "cubic_spline") (v "0.6.3") (d (list (d (n "wasm-bindgen") (r "^0.2.42") (d #t) (k 0)))) (h "0lpwq5idci2zcz47j83s4qjncj4brqqdpn1wd3b21vdzw0w6amks")))

(define-public crate-cubic_spline-0.7.0 (c (n "cubic_spline") (v "0.7.0") (d (list (d (n "wasm-bindgen") (r "^0.2.43") (d #t) (k 0)))) (h "0k7jhnsa23v71lsgvr0jb3drqc6pwxl1s4ss7pi7mibqs8gy407b")))

(define-public crate-cubic_spline-0.7.1 (c (n "cubic_spline") (v "0.7.1") (d (list (d (n "wasm-bindgen") (r "^0.2.43") (d #t) (k 0)))) (h "1mr04hdrahpa0044li1gk5s3nim06nrc4fkwzxmn9qy24fzngpy8")))

(define-public crate-cubic_spline-0.7.2 (c (n "cubic_spline") (v "0.7.2") (d (list (d (n "wasm-bindgen") (r "^0.2.43") (d #t) (k 0)))) (h "0dc3g0z8jizizx2zi8h4a3s1dfcc336airmhq27q27l8vga0f0wc")))

(define-public crate-cubic_spline-0.7.5 (c (n "cubic_spline") (v "0.7.5") (d (list (d (n "wasm-bindgen") (r "^0.2.43") (d #t) (k 0)))) (h "115bjw8j1z6g215rlhlg5xjyf98bmm39ln7zi140ndxgs7121239")))

(define-public crate-cubic_spline-0.7.6 (c (n "cubic_spline") (v "0.7.6") (d (list (d (n "wasm-bindgen") (r "^0.2.43") (d #t) (k 0)))) (h "136n6iw0nj7iahva6r4dxydrx175i0bq8flf7g0sarzwllxrg1k8")))

(define-public crate-cubic_spline-0.7.7 (c (n "cubic_spline") (v "0.7.7") (d (list (d (n "wasm-bindgen") (r "^0.2.47") (d #t) (k 0)))) (h "08lgazr0ic7lrsyr9hyp48pw50svh7cv55rfmn6172nll62dyfn8")))

(define-public crate-cubic_spline-0.7.8 (c (n "cubic_spline") (v "0.7.8") (d (list (d (n "wasm-bindgen") (r "^0.2.47") (d #t) (k 0)))) (h "1sglnqibmnklp2hywxs2zrrj3grs40sr8fgk41pi03l8fxb9lcj4")))

(define-public crate-cubic_spline-0.8.0 (c (n "cubic_spline") (v "0.8.0") (d (list (d (n "wasm-bindgen") (r "^0.2.50") (d #t) (k 0)))) (h "0njy2rm6jyadkxlys9iw8pzcn3nh3711qnvpn1rj0gyw42pa8ja7")))

(define-public crate-cubic_spline-0.8.1 (c (n "cubic_spline") (v "0.8.1") (d (list (d (n "wasm-bindgen") (r "^0.2.50") (d #t) (k 0)))) (h "1rna5d5an61jxv9dz808k1bj7nqdl7sl0c8kmkmshk57qqa9fzdz")))

(define-public crate-cubic_spline-0.9.0 (c (n "cubic_spline") (v "0.9.0") (d (list (d (n "wasm-bindgen") (r "^0.2.50") (d #t) (k 0)))) (h "0hx32az8d1wfn21rqkylh4vlhnawkkcb71xr549qb8v7q07sg5pr")))

(define-public crate-cubic_spline-0.9.1 (c (n "cubic_spline") (v "0.9.1") (d (list (d (n "wasm-bindgen") (r "^0.2.58") (d #t) (k 0)))) (h "1s3skybgcgdg52ybk45sdb39vk1s1v7w4k2ywg58kcrvgh0dcvi6")))

(define-public crate-cubic_spline-0.9.2 (c (n "cubic_spline") (v "0.9.2") (d (list (d (n "wasm-bindgen") (r "^0.2.60") (d #t) (k 0)))) (h "0ipghqyfj20rvq0cpajai8sjhcy310b0zx50k871b21n0zv5wgg3")))

(define-public crate-cubic_spline-0.9.3 (c (n "cubic_spline") (v "0.9.3") (d (list (d (n "wasm-bindgen") (r "^0.2.65") (d #t) (k 0)))) (h "06hw0j21jgj4350jxqgpms50g5brdw4lh7bi845xl5a8lmnv3i90")))

(define-public crate-cubic_spline-0.9.8 (c (n "cubic_spline") (v "0.9.8") (d (list (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (k 0)))) (h "0079b4l8fw0y4sn2f5sqkm5xj4vjiaf77awgf3a79vm9jj0y4gqg")))

(define-public crate-cubic_spline-0.9.9 (c (n "cubic_spline") (v "0.9.9") (d (list (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (k 0)))) (h "10apd6mmgphwi2qqm8z2fasidjwgqi91lzvmyh9gabsb59jk5kyk")))

(define-public crate-cubic_spline-1.0.0 (c (n "cubic_spline") (v "1.0.0") (d (list (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1wxb2m0iwk2ljdckaqw1adhh7spmfh4c932ifz4k64sd96i3viiq")))

