(define-module (crates-io cu bi cubic-splinterpol) #:use-module (crates-io))

(define-public crate-cubic-splinterpol-0.1.0 (c (n "cubic-splinterpol") (v "0.1.0") (d (list (d (n "plotters") (r "^0.3.0") (d #t) (k 2)))) (h "0r1mnbz9qlc7167l32wm1x4k6rr3lfqqxhmx2ldaq5q2a3qck1y5")))

