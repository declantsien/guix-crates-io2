(define-module (crates-io cu bi cubic-protocol-derive) #:use-module (crates-io))

(define-public crate-cubic-protocol-derive-0.1.6 (c (n "cubic-protocol-derive") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1f8gsaq1lavv7j3lnxjffa2q8agz62k6wafxcan93v6igavs116q")))

(define-public crate-cubic-protocol-derive-0.1.7 (c (n "cubic-protocol-derive") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1cwl68g16vlknxi42g0yj4f37aglzf745vd2h2gymxqa34crwh81")))

