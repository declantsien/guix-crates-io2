(define-module (crates-io cu bi cubic-splines) #:use-module (crates-io))

(define-public crate-cubic-splines-0.2.0 (c (n "cubic-splines") (v "0.2.0") (d (list (d (n "roots") (r "^0.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1n65diag7a7lva9xfjac5m8gc71z6fgmyw25jmsmxhj4h04xrbhc") (f (quote (("serialization" "serde" "serde_derive"))))))

