(define-module (crates-io cu bi cubic-bezier) #:use-module (crates-io))

(define-public crate-cubic-bezier-1.0.0 (c (n "cubic-bezier") (v "1.0.0") (d (list (d (n "hypermelon") (r "^0.5.5") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 2)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "0q1iqh9dnqy438nhydbwp1kikpqrkcv29v28xsr4rrkdj4dhnvg4")))

