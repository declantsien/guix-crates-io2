(define-module (crates-io cu bi cubing_macros) #:use-module (crates-io))

(define-public crate-cubing_macros-0.12.0 (c (n "cubing_macros") (v "0.12.0") (d (list (d (n "cubing_core") (r "=0.12.0") (d #t) (k 0)) (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "049cgv0jxw3hkr2wkm5638rs5xm1nd5cdzblaxl7755yc1479l64")))

(define-public crate-cubing_macros-0.12.1 (c (n "cubing_macros") (v "0.12.1") (d (list (d (n "cubing_core") (r "=0.12.1") (d #t) (k 0)) (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "10n1bvfnvqnk7rwl4cxf25r3wqxawssk7p1ij4687bwv5nzxxi2h")))

(define-public crate-cubing_macros-0.12.2 (c (n "cubing_macros") (v "0.12.2") (d (list (d (n "cubing_core") (r "=0.12.2") (d #t) (k 0)) (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "0f5vwkzvqwrry1ghgfvgnhk8r2m40qhnmvkvxgbm43k5ci42cb04")))

(define-public crate-cubing_macros-0.12.3 (c (n "cubing_macros") (v "0.12.3") (d (list (d (n "cubing_core") (r "=0.12.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1z9zd7nk7f96syz0nnd7lf1b636pjdx9nvvran1irs60g58f6rm6")))

(define-public crate-cubing_macros-0.12.4 (c (n "cubing_macros") (v "0.12.4") (d (list (d (n "cubing_core") (r "=0.12.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1qcfd2rsi3fh5vxffpml028gd6a0a81znvz4s7giyqa0js802ph5")))

(define-public crate-cubing_macros-0.12.5 (c (n "cubing_macros") (v "0.12.5") (d (list (d (n "cubing_core") (r "=0.12.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1d7gbndaclkcd06r2j95s4xdfy6796di5y4fj40k3jz9pgrh6vqs")))

(define-public crate-cubing_macros-0.12.6 (c (n "cubing_macros") (v "0.12.6") (d (list (d (n "cubing_core") (r "=0.12.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "159zsni6i1m6hkb7qipb2sbhrj9hzpwk9q49frhap5qh4q51qrwn")))

(define-public crate-cubing_macros-0.13.0 (c (n "cubing_macros") (v "0.13.0") (d (list (d (n "cubing_core") (r "=0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0qjy4b0w94jd1w5ga16xcsjd5ypvk2dzld62h4jaiwcixl5530gw")))

