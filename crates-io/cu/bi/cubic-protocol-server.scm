(define-module (crates-io cu bi cubic-protocol-server) #:use-module (crates-io))

(define-public crate-cubic-protocol-server-0.1.7 (c (n "cubic-protocol-server") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "cubic-protocol") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1yqa89g1hasnqi2cmq88y91ah2x8p5srkr21y42q92q2fwxfc582")))

(define-public crate-cubic-protocol-server-0.1.8 (c (n "cubic-protocol-server") (v "0.1.8") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "cubic-protocol") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "19wp39y9ljm83k1a02hynac93ypw3dib0470qfi7n7b8wa8fml8f")))

(define-public crate-cubic-protocol-server-0.1.9 (c (n "cubic-protocol-server") (v "0.1.9") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "cubic-protocol") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0x3ai9nn1fzbv3bm6sj8b54ryf4d1dncmyvb3byjkz090ksh71w3")))

