(define-module (crates-io cu bi cubic-chat) #:use-module (crates-io))

(define-public crate-cubic-chat-0.1.0 (c (n "cubic-chat") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.80") (d #t) (k 0)))) (h "09qjww35rbra1bwq4c43vi0qycf4kadagb8vnxnzh54hjjqyds9r")))

(define-public crate-cubic-chat-0.1.1 (c (n "cubic-chat") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.80") (d #t) (k 0)))) (h "10vx0qrxbny7xqscxq9bcxdrzdlwj3vrkbjlrh22lh68y57wwby8")))

