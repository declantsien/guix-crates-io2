(define-module (crates-io cu rs cursh) #:use-module (crates-io))

(define-public crate-cursh-0.1.0 (c (n "cursh") (v "0.1.0") (d (list (d (n "cli-clipboard") (r "^0.2.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0yciim9y5ank739y4qp4l34idvk7pjgwx9ygbvidrqd1b045r4fk")))

(define-public crate-cursh-0.1.1 (c (n "cursh") (v "0.1.1") (d (list (d (n "cli-clipboard") (r "^0.2.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1qgga8vzrr589pvnsp4a47fbwvbla92f8hh4g29a26ai8jvpvp9j")))

