(define-module (crates-io cu rs cursed-collections) #:use-module (crates-io))

(define-public crate-cursed-collections-0.2.0 (c (n "cursed-collections") (v "0.2.0") (h "1srfxn11dg345nyq258l3v9zjbcc1my65fhff8zp5sj5swn8ajfq")))

(define-public crate-cursed-collections-0.3.0 (c (n "cursed-collections") (v "0.3.0") (h "00xdxbq1kxhvwlyrgx4n9pn4pkj2hlxmh379w110dg3xkngamf6h")))

(define-public crate-cursed-collections-0.4.0 (c (n "cursed-collections") (v "0.4.0") (h "1nv6jnk2fc1mmslf39b8525zfmbsvxcnqsqfr65p179j5jgs7gig")))

(define-public crate-cursed-collections-0.5.0 (c (n "cursed-collections") (v "0.5.0") (h "1c6ibjjd1nvmvajyfi3ik59fms8dp6y9xjx4xa6qv7hy7y3gm17y")))

(define-public crate-cursed-collections-0.6.0 (c (n "cursed-collections") (v "0.6.0") (h "18bq22lc8kqz8q603xnqc95vc2x52sdmjii242ifigxaq26i31ww")))

(define-public crate-cursed-collections-0.6.1 (c (n "cursed-collections") (v "0.6.1") (h "17hyzxk9b4zadadinyj6gmrjqzh83gxx1za7j3lh1amswri4p6ym")))

(define-public crate-cursed-collections-0.6.2 (c (n "cursed-collections") (v "0.6.2") (h "0mc422w8v2a3nasiqk32dv1j9ial1bxrg1m3wnw32h0as9nddag1")))

(define-public crate-cursed-collections-0.7.0 (c (n "cursed-collections") (v "0.7.0") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0rz9ph6mj9qgxrrsy7ysrm3ffvmigry5slqpr9xcf3l4bsrrrmvf")))

(define-public crate-cursed-collections-0.8.0 (c (n "cursed-collections") (v "0.8.0") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0kh6wzg5r5ryl6gaj025vi699q99bq31d86sx80mhz5ssfr7ihvb")))

(define-public crate-cursed-collections-0.8.1 (c (n "cursed-collections") (v "0.8.1") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1p0db7gazlzyipfixn3cqgnj5wbabxlmc0hgwvygpid991iyg56x")))

(define-public crate-cursed-collections-0.8.2 (c (n "cursed-collections") (v "0.8.2") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1cvh84qdn6ajba7vzi7p9avbkg7914z0qyiz27xf8xvzd3wxl07a")))

(define-public crate-cursed-collections-0.8.3 (c (n "cursed-collections") (v "0.8.3") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1m3xh5yzkbgclg4ixq3w3ppqw27dq1sv7wjf2a7hn678cwjr9mz3")))

