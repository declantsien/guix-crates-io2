(define-module (crates-io cu rs cursive_tree_view) #:use-module (crates-io))

(define-public crate-cursive_tree_view-0.1.0 (c (n "cursive_tree_view") (v "0.1.0") (d (list (d (n "cursive") (r "^0.5") (d #t) (k 0)))) (h "0yz75fi2fc343pqlcazbj7d4nip68rczwc29kixqz9ninjv32p2m") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_tree_view-0.2.0 (c (n "cursive_tree_view") (v "0.2.0") (d (list (d (n "cursive") (r "^0.6") (d #t) (k 0)) (d (n "debug_stub_derive") (r "^0.3.0") (d #t) (k 0)))) (h "1q7kylzxgj0b08l31wfmvhyjb937v02w0k1jjq7mvdris2sf1hq5") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_tree_view-0.2.1 (c (n "cursive_tree_view") (v "0.2.1") (d (list (d (n "cursive") (r "^0.7") (d #t) (k 0)) (d (n "debug_stub_derive") (r "^0.3.0") (d #t) (k 0)))) (h "11m849ir5bnprc5qmpmdmzkqr72vliwxclgiiwim79p99i09ry5l") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_tree_view-0.3.0 (c (n "cursive_tree_view") (v "0.3.0") (d (list (d (n "cursive") (r "^0.10") (k 0)) (d (n "debug_stub_derive") (r "^0.3.0") (d #t) (k 0)))) (h "048mfpxxh4l3iy6hf2l2rkq33fc6j7c40yqx5j6icq0jys8na0s6") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_tree_view-0.4.0 (c (n "cursive_tree_view") (v "0.4.0") (d (list (d (n "cursive") (r "^0.12") (k 0)) (d (n "cursive") (r "^0.12") (d #t) (k 2)) (d (n "debug_stub_derive") (r "^0.3.0") (d #t) (k 0)))) (h "1x93lwxx1s1rpdk058aghlj05gc6s60naf9wcp06qzfp471z98b2")))

(define-public crate-cursive_tree_view-0.5.0 (c (n "cursive_tree_view") (v "0.5.0") (d (list (d (n "cursive") (r "^0.13") (k 0)) (d (n "cursive") (r "^0.13") (d #t) (k 2)) (d (n "debug_stub_derive") (r "^0.3.0") (d #t) (k 0)))) (h "03k53xyji75iyzl3b8x672lyv2fmjxsb9507rjlpxpvhkln1pas2")))

(define-public crate-cursive_tree_view-0.6.0 (c (n "cursive_tree_view") (v "0.6.0") (d (list (d (n "cursive") (r "^0.14") (k 0)) (d (n "cursive") (r "^0.14") (d #t) (k 2)) (d (n "debug_stub_derive") (r "^0.3.0") (d #t) (k 0)))) (h "196g320a98cwf7zqvwrry79q6g8i89cfmh4c0a6sz4h85s75aard")))

(define-public crate-cursive_tree_view-0.7.0 (c (n "cursive_tree_view") (v "0.7.0") (d (list (d (n "cursive") (r "^0.16") (d #t) (k 2)) (d (n "cursive_core") (r "^0.2") (d #t) (k 0)) (d (n "debug_stub_derive") (r "^0.3.0") (d #t) (k 0)))) (h "180p0zccf8j06birj0kin2p479rqgzzbabwp4zgnj2k0dpyaynfd")))

(define-public crate-cursive_tree_view-0.8.0 (c (n "cursive_tree_view") (v "0.8.0") (d (list (d (n "cursive") (r "^0.17") (d #t) (k 2)) (d (n "cursive_core") (r "^0.3") (d #t) (k 0)) (d (n "debug_stub_derive") (r "^0.3.0") (d #t) (k 0)))) (h "0z5gpbq9qlk1ml4w912f9ahb5dnppm3q5zm5jfvkl2v3jmwcy4yp")))

