(define-module (crates-io cu rs cursorvec) #:use-module (crates-io))

(define-public crate-cursorvec-0.1.0 (c (n "cursorvec") (v "0.1.0") (h "0w5h0kd7457x60872z7a0sz9b8nf9jd0qklyyr0xshz6z5h15ca0")))

(define-public crate-cursorvec-0.2.0 (c (n "cursorvec") (v "0.2.0") (h "0gqwh5g78flad6fv8k5g0lis3sjw4mss39j9dw3q6y5zydinpk64") (f (quote (("strict"))))))

