(define-module (crates-io cu rs cursion) #:use-module (crates-io))

(define-public crate-cursion-0.1.0 (c (n "cursion") (v "0.1.0") (d (list (d (n "enumset") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0dkjml5brzi8mnxppg3ivdwsrwvfhai260y6zix7w0fkp2p9dc8h")))

