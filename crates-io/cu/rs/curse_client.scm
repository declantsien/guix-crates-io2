(define-module (crates-io cu rs curse_client) #:use-module (crates-io))

(define-public crate-curse_client-0.1.0 (c (n "curse_client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1q6f3f74l88z06nr6bm4m2pcdrfggrnply62824xx9wjs5g2j5sb")))

