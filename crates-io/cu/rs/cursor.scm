(define-module (crates-io cu rs cursor) #:use-module (crates-io))

(define-public crate-cursor-1.0.0 (c (n "cursor") (v "1.0.0") (h "08ifw839734zni54gpxd6w193bc6yhv1gg97fvdyyvxl695dh85f")))

(define-public crate-cursor-1.1.0 (c (n "cursor") (v "1.1.0") (h "0m7m8dkw665kf79in5iaqy31m27nh6a4wfj9qb505fis52iiwfbr")))

(define-public crate-cursor-2.0.0 (c (n "cursor") (v "2.0.0") (h "0r94d24jm9471xq422ckardqqq01y3hxzvmghm74aflhxwnsd0l1") (f (quote (("std") ("default" "std"))))))

(define-public crate-cursor-2.1.0 (c (n "cursor") (v "2.1.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 2)))) (h "0d1g7dsflxagnik6v49w27dr71d6mvl3925iv1f4fcvawfpkkiy5") (f (quote (("std") ("default" "std"))))))

(define-public crate-cursor-2.2.0 (c (n "cursor") (v "2.2.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 2)))) (h "07f8gic8n7h68m2443ks5189fmbm8a14d4f9ijmi9q8d23p10j1w") (f (quote (("std") ("default" "std"))))))

(define-public crate-cursor-2.3.0 (c (n "cursor") (v "2.3.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 2)))) (h "0b7xhvxm98whr6c2d1f049nnb7dxz7x1l69a9dnlvmcbgkk4k1x4") (f (quote (("std") ("default" "std"))))))

