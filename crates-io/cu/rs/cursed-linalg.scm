(define-module (crates-io cu rs cursed-linalg) #:use-module (crates-io))

(define-public crate-cursed-linalg-0.0.1-alpha.1 (c (n "cursed-linalg") (v "0.0.1-alpha.1") (d (list (d (n "approx") (r "^0.5.1") (f (quote ("num-complex"))) (d #t) (k 2)) (d (n "cauchy") (r "^0.4.0") (d #t) (k 0)) (d (n "miette") (r "^3.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (f (quote ("approx"))) (d #t) (k 2)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "06kxr5ms0a0k92zdljgrrsc28zps6x8khqdkvg70kq264kgx5ss5")))

