(define-module (crates-io cu rs cursed) #:use-module (crates-io))

(define-public crate-cursed-0.0.0-preview.0 (c (n "cursed") (v "0.0.0-preview.0") (h "06agah9626wd22n7d07fdyvkxhxsac0n7zwrxk02nl0kcpy2b720")))

(define-public crate-cursed-0.0.1 (c (n "cursed") (v "0.0.1") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "1wvmcwn3fbx1y8m60iwnfpdb091598w62x2mvvxpzf7xs8310qsf") (f (quote (("no-std" "memchr") ("futures" "futures-preview"))))))

