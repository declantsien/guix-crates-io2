(define-module (crates-io cu rs cursive-syntect) #:use-module (crates-io))

(define-public crate-cursive-syntect-0.1.0 (c (n "cursive-syntect") (v "0.1.0") (d (list (d (n "cursive") (r "^0.19.0") (d #t) (k 2)) (d (n "cursive_core") (r "^0.3.5") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (f (quote ("parsing"))) (o #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1b9jw3fb0qfn3nrxmavankw85bky676kzyb10vpncvpyaiv3v897") (f (quote (("regex-onig" "syntect/regex-onig") ("regex-fancy" "syntect/regex-fancy") ("default" "regex-onig"))))))

