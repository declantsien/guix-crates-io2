(define-module (crates-io cu rs cursieve_derive) #:use-module (crates-io))

(define-public crate-cursieve_derive-0.1.0 (c (n "cursieve_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1p4bpizyzqzja6cdi74dj4lr15ndhsp9zcgmcsvgw5ypazblp20w") (r "1.60")))

