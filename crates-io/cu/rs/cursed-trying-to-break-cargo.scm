(define-module (crates-io cu rs cursed-trying-to-break-cargo) #:use-module (crates-io))

(define-public crate-cursed-trying-to-break-cargo-0.0.0 (c (n "cursed-trying-to-break-cargo") (v "0.0.0") (h "1fwywyzm6nddmh9nfzdp8qf6p42fnzcn86ikmim5vkbwmsczz4i4")))

(define-public crate-cursed-trying-to-break-cargo-0.0.0-Pre-Release2.7+wO30-w.8.0 (c (n "cursed-trying-to-break-cargo") (v "0.0.0-Pre-Release2.7+wO30-w.8.0") (h "0bqkzvl8cjrf0qkixc45dnhrvy0a6yzyqwxgn6hn0css3nmarmg9")))

(define-public crate-cursed-trying-to-break-cargo-18446744073709551615.18446744073709551615.18446744073709551615 (c (n "cursed-trying-to-break-cargo") (v "18446744073709551615.18446744073709551615.18446744073709551615") (h "0sykgcpybqg1ws5srjrifjnm79iwfwdaa5r1slxf46mjpp73wbji")))

(define-public crate-cursed-trying-to-break-cargo-18446744073709551615.18446744073709551615.18446744073709551615---gREEAATT-.-V3R510N+--W0W.1.3-.nice (c (n "cursed-trying-to-break-cargo") (v "18446744073709551615.18446744073709551615.18446744073709551615---gREEAATT-.-V3R510N+--W0W.1.3-.nice") (h "15jy6iyg8k4xv9a8qnz0f384f6lk449r3qk1xfz998qrr53cl28y")))

(define-public crate-cursed-trying-to-break-cargo-1.0.0-2.0.0 (c (n "cursed-trying-to-break-cargo") (v "1.0.0-2.0.0") (h "1p5jwc29jhk4lykn6jkwl4rzm1lnpf0q5hhhdia6ljj244pa5bwl")))

(define-public crate-cursed-trying-to-break-cargo-1.0.0-2.0.0+3.0.0 (c (n "cursed-trying-to-break-cargo") (v "1.0.0-2.0.0+3.0.0") (h "06mps21wps7akpg13qdff66ds2rgav2d098h3mklj16xjgiajnv0")))

(define-public crate-cursed-trying-to-break-cargo-1.0.0-2.0.0-3.0.0+4.0.0-5.0.0 (c (n "cursed-trying-to-break-cargo") (v "1.0.0-2.0.0-3.0.0+4.0.0-5.0.0") (h "0h3yzlv9yy83azsh6lj5ir6m8xgyp5d6l0rydjx1a5hpalfl8kpz")))

(define-public crate-cursed-trying-to-break-cargo-0.0.0-0 (c (n "cursed-trying-to-break-cargo") (v "0.0.0-0") (h "0yzmamvarhinl3vhy7knpm277b4dvkimviqqpmc931i1q2g798b4")))

(define-public crate-cursed-trying-to-break-cargo-0.0.0+0 (c (n "cursed-trying-to-break-cargo") (v "0.0.0+0") (h "1cnbcyw723l33p1anrp99dhfgj7xyjkfsny62g2gpp546an1ya70")))

(define-public crate-cursed-trying-to-break-cargo-0.0.5 (c (n "cursed-trying-to-break-cargo") (v "0.0.5") (h "1006wy6j1r23pyza5zq3v2xwsvfg674jbl292csp0jxckgslvx3y")))

(define-public crate-cursed-trying-to-break-cargo-0.0.6 (c (n "cursed-trying-to-break-cargo") (v "0.0.6") (h "1ln5wpgf1y71yzgj4zcczcjnmzab4fj563483awkqhikx2a8xki1")))

(define-public crate-cursed-trying-to-break-cargo-0.0.5+1 (c (n "cursed-trying-to-break-cargo") (v "0.0.5+1") (h "0z6ywahdvpccf3b1kjkv0aap6wdgnshyk97rkx37p204vx1pnw6y")))

(define-public crate-cursed-trying-to-break-cargo-0.0.5+2 (c (n "cursed-trying-to-break-cargo") (v "0.0.5+2") (h "0j4g6lbsmxaa78hxvrf1f6wdy52721kxcgszqs1d27khhln2048a")))

(define-public crate-cursed-trying-to-break-cargo-0.0.5+3 (c (n "cursed-trying-to-break-cargo") (v "0.0.5+3") (h "01hhdxfpq6pk81yvb8j856ipcph5zsxdmhifrdrdgb8mrvfxnh14")))

(define-public crate-cursed-trying-to-break-cargo-1.0.0-0.HDTV-BluRay.1020p.YTSUB.L33TRip.mkv (c (n "cursed-trying-to-break-cargo") (v "1.0.0-0.HDTV-BluRay.1020p.YTSUB.L33TRip.mkv") (h "1x84lqzh2ga3vd64xv4x07n6hz287c5fygn3la5bjyg0f30p69v7")))

