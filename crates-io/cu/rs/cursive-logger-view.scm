(define-module (crates-io cu rs cursive-logger-view) #:use-module (crates-io))

(define-public crate-cursive-logger-view-0.6.0 (c (n "cursive-logger-view") (v "0.6.0") (d (list (d (n "arraydeque") (r "^0.4.5") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 2)) (d (n "cursive_core") (r "^0.3") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.27.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0ihs55q578p1c22avk6f2n9a35ibw7clj46ai8kv563vq0v6ayw2")))

