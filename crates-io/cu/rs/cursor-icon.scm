(define-module (crates-io cu rs cursor-icon) #:use-module (crates-io))

(define-public crate-cursor-icon-0.1.0 (c (n "cursor-icon") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (o #t) (k 0)))) (h "0mdqisl7gfy45v09xpf93wj90kz7l8nh6m8c5lr23jczq466swbp") (r "1.64.0")))

(define-public crate-cursor-icon-1.0.0 (c (n "cursor-icon") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (o #t) (k 0)) (d (n "wayland-client") (r "^0.30.1") (d #t) (k 2)) (d (n "wayland-cursor") (r "^0.30.0") (d #t) (k 2)))) (h "1gr60niab2xpxq8njlwv2hpzdrwy83s58sci340kblg2m29b22vl") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.64.0")))

(define-public crate-cursor-icon-1.1.0 (c (n "cursor-icon") (v "1.1.0") (d (list (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (o #t) (k 0)) (d (n "wayland-client") (r "^0.31.1") (d #t) (k 2)) (d (n "wayland-cursor") (r "^0.31.0") (d #t) (k 2)))) (h "14brf4vd6az9hnszwzqj7xyfaymqx9806d4i7xmwlaja3wjsr9ln") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.65.0")))

