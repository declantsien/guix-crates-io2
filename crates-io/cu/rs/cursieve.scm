(define-module (crates-io cu rs cursieve) #:use-module (crates-io))

(define-public crate-cursieve-0.1.0 (c (n "cursieve") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "cursieve_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "058yamkhln6hnq3gdmj3am0saadqvabrg5g2khrh7npmvmwfzv3f") (r "1.60")))

