(define-module (crates-io cu rs cursive-cached-text-view) #:use-module (crates-io))

(define-public crate-cursive-cached-text-view-0.1.0 (c (n "cursive-cached-text-view") (v "0.1.0") (d (list (d (n "cursive") (r "^0.20") (d #t) (k 0)) (d (n "lru") (r "^0.10.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "13v5r5jkcd12hd369r4ypcb1fwlfi7yp2fcqrqklnah8lmb380mh") (y #t)))

(define-public crate-cursive-cached-text-view-0.1.1 (c (n "cursive-cached-text-view") (v "0.1.1") (d (list (d (n "cursive") (r "^0.20") (d #t) (k 0)) (d (n "lru") (r "^0.10.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1g9h6q4f9g86lp84n2b8a14k6asmnbjbsf12bvhm9r64d0rw7881")))

