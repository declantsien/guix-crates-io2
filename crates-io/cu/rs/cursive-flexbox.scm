(define-module (crates-io cu rs cursive-flexbox) #:use-module (crates-io))

(define-public crate-cursive-flexbox-0.1.0 (c (n "cursive-flexbox") (v "0.1.0") (d (list (d (n "cursive_core") (r "^0.3.7") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 2)))) (h "1p5gmq853qmkvny12k2c2331nzqij0y3cdch4awbzmyn8ikpspah")))

(define-public crate-cursive-flexbox-0.2.0 (c (n "cursive-flexbox") (v "0.2.0") (d (list (d (n "cursive_core") (r "^0.3.7") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 2)))) (h "02gxm0b0azb8y56nzi35zp2nl7nsk9bfhjfpdqx890y90v8w2asr")))

(define-public crate-cursive-flexbox-0.3.0 (c (n "cursive-flexbox") (v "0.3.0") (d (list (d (n "cursive_core") (r "^0.3.7") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)))) (h "0g7raqamj971aga2r8bgq2fw4nzbff05n1ly1vi4nj9vfjkq076c")))

