(define-module (crates-io cu rs curses-sys) #:use-module (crates-io))

(define-public crate-curses-sys-6.0.0-1 (c (n "curses-sys") (v "6.0.0-1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06zzn8szrwvhyak8lw20gcn67j7dcyvlfs70d5ky2lm0my9vb2x5") (f (quote (("wide") ("panel") ("mouse") ("menu") ("form") ("extensions") ("default" "wide" "mouse" "extensions"))))))

(define-public crate-curses-sys-6.0.0-2 (c (n "curses-sys") (v "6.0.0-2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l167a3cd5nip2cx43cvpd14d1kr9fgakbmccflblkqwsy5advqx") (f (quote (("wide") ("panel") ("mouse") ("menu") ("form") ("extensions") ("default" "wide" "mouse" "extensions"))))))

