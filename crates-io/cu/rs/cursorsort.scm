(define-module (crates-io cu rs cursorsort) #:use-module (crates-io))

(define-public crate-cursorsort-0.1.0 (c (n "cursorsort") (v "0.1.0") (h "00ag6fc82sgyyj44fhszcf3bk20ghy72x6c8ljwih6vdjsibyzgm")))

(define-public crate-cursorsort-0.2.0 (c (n "cursorsort") (v "0.2.0") (h "0zf80mxrx967s614hx0jjp19yc3m959hvzf2pkwl1b3g8khhklkb")))

(define-public crate-cursorsort-0.3.0 (c (n "cursorsort") (v "0.3.0") (h "0j5fdd6rylq08d0vgslywgwqid5di734rrjhaka054jxajw79w6i")))

(define-public crate-cursorsort-0.3.1 (c (n "cursorsort") (v "0.3.1") (h "0rcakhxmngav0hb2hq64x9amrmz4bhp6lbphmapk15l0x7nwvzvc")))

(define-public crate-cursorsort-0.3.2 (c (n "cursorsort") (v "0.3.2") (h "0d1xk0k4a50z4sk6xwchpingz749fskfvfjxkbjfknrncqj99nzl")))

(define-public crate-cursorsort-0.3.3 (c (n "cursorsort") (v "0.3.3") (h "0ya2xy551azcsswv9bgdx4922376cxlnfb0ng3k85v63ygic51d3")))

(define-public crate-cursorsort-0.4.0 (c (n "cursorsort") (v "0.4.0") (h "0rnvl885k3j3v3hp39qqq9snib3zarcm6i21rhabz5d4m55rfb6k")))

