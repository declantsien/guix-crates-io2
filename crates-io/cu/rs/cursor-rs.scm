(define-module (crates-io cu rs cursor-rs) #:use-module (crates-io))

(define-public crate-cursor-rs-0.1.0 (c (n "cursor-rs") (v "0.1.0") (d (list (d (n "inout_port-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "vgainfo-rs") (r "^0.1.1") (d #t) (k 0)))) (h "03x42mlxi6zh7vfdfwfzzgrp4ih5hlj1bymn50rd2ywqz86gw6nz") (y #t)))

(define-public crate-cursor-rs-0.1.1 (c (n "cursor-rs") (v "0.1.1") (d (list (d (n "inout_port-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "vgainfo-rs") (r "^0.1.1") (d #t) (k 0)))) (h "0nlx47ngqbbfp6w93mabrrlvbgz1lmql40ca8sgzsk11w9ggm9s7") (y #t)))

(define-public crate-cursor-rs-0.1.2 (c (n "cursor-rs") (v "0.1.2") (d (list (d (n "inout_port-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "vgainfo-rs") (r "^0.1.1") (d #t) (k 0)))) (h "0gqsvbc1v8824h2pb27hifhj99h5xardsnfy8achx8vs3ml4cpr4")))

(define-public crate-cursor-rs-0.1.3 (c (n "cursor-rs") (v "0.1.3") (d (list (d (n "inout_port-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "vgainfo-rs") (r "^0.1.1") (d #t) (k 0)))) (h "1ww0vlswrjr12gl9d71vk2yg8mkmmcsryhg4dnz57370z87njahv")))

(define-public crate-cursor-rs-0.1.4 (c (n "cursor-rs") (v "0.1.4") (d (list (d (n "inout_port-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "vgainfo-rs") (r "^0.1.1") (d #t) (k 0)))) (h "0z7z00wdav1kyd7i2vvm66w2qn67pnl351g29nqsakng5pfkzmqq")))

