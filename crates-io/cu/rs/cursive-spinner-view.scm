(define-module (crates-io cu rs cursive-spinner-view) #:use-module (crates-io))

(define-public crate-cursive-spinner-view-0.1.1 (c (n "cursive-spinner-view") (v "0.1.1") (d (list (d (n "cursive") (r "^0.16") (d #t) (k 2)) (d (n "cursive_core") (r "^0.2") (d #t) (k 0)) (d (n "ntest") (r "^0.7") (d #t) (k 2)))) (h "11vphby6fmx4dmglrcmsksh0rgpsgkag12dgh8kc7x77pcmffkzy")))

(define-public crate-cursive-spinner-view-0.1.2 (c (n "cursive-spinner-view") (v "0.1.2") (d (list (d (n "cursive") (r "^0.17") (d #t) (k 2)) (d (n "cursive_core") (r "^0.3") (d #t) (k 0)) (d (n "ntest") (r "^0.7") (d #t) (k 2)))) (h "1g69r77a7ipm3gvi8ygz9qgrcf3g5n3r0q642mp3vznq66skj8zz")))

(define-public crate-cursive-spinner-view-0.1.3 (c (n "cursive-spinner-view") (v "0.1.3") (d (list (d (n "cursive") (r "^0.17") (d #t) (k 2)) (d (n "cursive_core") (r "^0.3") (d #t) (k 0)) (d (n "ntest") (r "^0.7") (d #t) (k 2)))) (h "12bqbprbpxn66swkipb1sgmmmg5y0lhrp6znx7w01hbsgwhravvy")))

(define-public crate-cursive-spinner-view-0.1.4 (c (n "cursive-spinner-view") (v "0.1.4") (d (list (d (n "cursive") (r "^0.17") (d #t) (k 2)) (d (n "cursive_core") (r "^0.3") (d #t) (k 0)) (d (n "ntest") (r "^0.7") (d #t) (k 2)))) (h "0kc5yiphqvf5ljy0y5p3inzvisyxfrlwh1zrddfjcdhqp0acw754")))

