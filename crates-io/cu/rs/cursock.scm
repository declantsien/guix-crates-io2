(define-module (crates-io cu rs cursock) #:use-module (crates-io))

(define-public crate-cursock-1.0.0 (c (n "cursock") (v "1.0.0") (d (list (d (n "curerr") (r "^1.0.1") (d #t) (k 0)))) (h "1aq039cksjry3v073nm4f0zixk5m5pqyw2bjqkbzjkyl8161fap8") (y #t)))

(define-public crate-cursock-1.1.0 (c (n "cursock") (v "1.1.0") (d (list (d (n "curerr") (r "^1.0.1") (d #t) (k 0)))) (h "0zkn0abfk965k12madwnl4a183a7hwn2gpavl0fnx0fp0cx94si3") (y #t)))

(define-public crate-cursock-1.1.1 (c (n "cursock") (v "1.1.1") (d (list (d (n "curerr") (r "^1.0.1") (d #t) (k 0)))) (h "13h3livysk4y0cslri0327m2ypid81q6p1k99ll9003kgp7mlpng")))

(define-public crate-cursock-1.2.0 (c (n "cursock") (v "1.2.0") (d (list (d (n "curerr") (r "^1.0.1") (d #t) (k 0)))) (h "1vy4bafdw6wwdcbj9r8yf1s3m017aip2pj4s3jvs87cgzraqsxc7") (y #t)))

(define-public crate-cursock-1.2.3 (c (n "cursock") (v "1.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1cfh330cvxq3srsxb3al0dnw3s5prrn0jagygkyr5vda7cwhma0s")))

(define-public crate-cursock-1.2.7 (c (n "cursock") (v "1.2.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07ssz0ysqv8rf1il04kqdyzhafs16adamwwncsvsr14vwqxwscy5")))

