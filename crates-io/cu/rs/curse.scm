(define-module (crates-io cu rs curse) #:use-module (crates-io))

(define-public crate-curse-0.1.0 (c (n "curse") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "cursebox") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r42wpyzn9gmhpai7bygwghzq46gslv87nl0d4f59xmkpimdi1q6")))

(define-public crate-curse-0.1.1 (c (n "curse") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "cursebox") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "0rzd1xy2xfb44dv4nsf2vfbkmkjc6ks1mkyc26s6kq9y1pdgxvrz")))

(define-public crate-curse-0.1.2 (c (n "curse") (v "0.1.2") (d (list (d (n "cursebox") (r "^0.1") (d #t) (k 0)) (d (n "flags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "0bn4y8079sfnb33zwf45422gyrabiw8jq7rrwxziy7bihh6fvzjz")))

(define-public crate-curse-0.1.3 (c (n "curse") (v "0.1.3") (d (list (d (n "cursebox") (r "^0.1") (d #t) (k 0)) (d (n "flags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "1dybzgrdkp3hidsc2w4nq7qbvd93sm3va9690qf0b7a2f0mms92r")))

