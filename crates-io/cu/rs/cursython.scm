(define-module (crates-io cu rs cursython) #:use-module (crates-io))

(define-public crate-cursython-0.1.0 (c (n "cursython") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.12") (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.2.10") (d #t) (k 0)))) (h "16chqiq02hj72w048pwp25m0d2fbb87mxd544jbw2d8aqw876wj5")))

