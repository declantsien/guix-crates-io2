(define-module (crates-io cu rs curses-line-ui) #:use-module (crates-io))

(define-public crate-curses-line-ui-0.1.0 (c (n "curses-line-ui") (v "0.1.0") (d (list (d (n "pancurses") (r "^0.8.0") (d #t) (k 0)))) (h "127rvdsq5q23r0yqspmj11hdh0ysbm49zdi608047dj38jkn4s45")))

(define-public crate-curses-line-ui-0.1.1 (c (n "curses-line-ui") (v "0.1.1") (d (list (d (n "pancurses") (r "^0.8.0") (d #t) (k 0)))) (h "07aqk9gjdgq9qlrw5avhwq1ijx1wpww5z5dp40lq4b88rk273m65")))

