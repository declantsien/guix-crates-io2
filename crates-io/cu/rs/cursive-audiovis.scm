(define-module (crates-io cu rs cursive-audiovis) #:use-module (crates-io))

(define-public crate-cursive-audiovis-0.1.0 (c (n "cursive-audiovis") (v "0.1.0") (d (list (d (n "audiovis") (r "^0") (d #t) (k 0)) (d (n "cursive_core") (r "^0") (d #t) (k 0)) (d (n "music-lounge-core") (r "^0") (f (quote ("cursive"))) (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "10fda4l858m9xkmjj9cq07q45wq0f6l23l0xg1xbcll4cmhnn7fk")))

(define-public crate-cursive-audiovis-0.1.1 (c (n "cursive-audiovis") (v "0.1.1") (d (list (d (n "audiovis") (r "^0") (d #t) (k 0)) (d (n "cursive_core") (r "^0") (d #t) (k 0)) (d (n "music-lounge-core") (r "^0") (f (quote ("cursive"))) (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "1jy14r4ai0mm7shmypqlc99gm99i4a9kwd5dlr056psg9x3483c4")))

