(define-module (crates-io cu rs cursive_hexview) #:use-module (crates-io))

(define-public crate-cursive_hexview-0.1.1 (c (n "cursive_hexview") (v "0.1.1") (d (list (d (n "cursive") (r "^0.7") (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "1civlzmmmjsa4r6hy04lxj1y4rh3320dm3vx2kyhj6035ybyhbbi") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_hexview-0.2.0 (c (n "cursive_hexview") (v "0.2.0") (d (list (d (n "cursive") (r "^0.9") (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "18jnp6z685wg1mik8zny9847lh833vrjhbchinqikdq53xbwkkwz") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_hexview-0.3.0 (c (n "cursive_hexview") (v "0.3.0") (d (list (d (n "cursive") (r "^0.14") (k 0)) (d (n "cursive") (r "^0.14") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1k2ijn6p3fx1qviscypcbzi72sfmjpjgbmnhx4x7lf2qbskshz7j")))

(define-public crate-cursive_hexview-0.6.0 (c (n "cursive_hexview") (v "0.6.0") (d (list (d (n "cursive") (r "^0.16") (k 0)) (d (n "cursive") (r "^0.16") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0c3y5nhkrfrdldjrk9p1ggfylc1xmbi2pldgarfr47n7fxw6z3qx")))

(define-public crate-cursive_hexview-0.7.0 (c (n "cursive_hexview") (v "0.7.0") (d (list (d (n "cursive") (r "^0.17") (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0qgwsxrqnlms32q6fhdgrbn1kq5nicaykx42kmc83p2czfgsyc25")))

