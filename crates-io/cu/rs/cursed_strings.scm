(define-module (crates-io cu rs cursed_strings) #:use-module (crates-io))

(define-public crate-cursed_strings-0.1.0 (c (n "cursed_strings") (v "0.1.0") (h "005q6qa2djyi4cvk340h16b61byyfxsj9hzj7iaskblrv6h4g433")))

(define-public crate-cursed_strings-0.1.1 (c (n "cursed_strings") (v "0.1.1") (h "1acq2d82zfm70sj4nhiva63y4c9mziydpc1022v187f7f2bbliz4")))

