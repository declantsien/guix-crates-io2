(define-module (crates-io cu rs curseforge_webview) #:use-module (crates-io))

(define-public crate-curseforge_webview-0.1.0 (c (n "curseforge_webview") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "closure") (r "^0.3.0") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "open") (r "^3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "wry") (r "^0.27.0") (f (quote ("objc-exception"))) (k 0)))) (h "0z5hxp3prwhmf0i8nvj0smz59y0s35rl1ylk2g2kx55j84nq94wp")))

