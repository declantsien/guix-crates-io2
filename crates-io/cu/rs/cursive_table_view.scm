(define-module (crates-io cu rs cursive_table_view) #:use-module (crates-io))

(define-public crate-cursive_table_view-0.1.0 (c (n "cursive_table_view") (v "0.1.0") (d (list (d (n "cursive") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0ppcksiixk2zs4s96yfhy8x08iayi70zgz9rlxwa8lb35fhmzpdv") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_table_view-0.2.0 (c (n "cursive_table_view") (v "0.2.0") (d (list (d (n "cursive") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1s0007ash7aayxyaq5vizxn1f73fa3g9vxfn0spxlgpjazlbgz5w") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_table_view-0.3.0 (c (n "cursive_table_view") (v "0.3.0") (d (list (d (n "cursive") (r "^0.5") (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1v5q5m4pq41xmyg70raq44lzlfjybjdphmbc2x61ii1zvpsav960") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_table_view-0.4.0 (c (n "cursive_table_view") (v "0.4.0") (d (list (d (n "cursive") (r "^0.5") (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "11qccca58jm953lpnjgmjyh7g9wfaqhqvprdn5jqgiy1028y4s37") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_table_view-0.4.1 (c (n "cursive_table_view") (v "0.4.1") (d (list (d (n "cursive") (r "^0.6") (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0i1dm592qp4gb9mm5wrjf5ibjl85nrxd3azlh0sia6l5qr3b7c7z") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_table_view-0.4.2 (c (n "cursive_table_view") (v "0.4.2") (d (list (d (n "cursive") (r "^0.7") (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0xgpdlz62jh81k7p9dfc6wk0r39n1kcjdgps26667z08w6p8b59w") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_table_view-0.5.0 (c (n "cursive_table_view") (v "0.5.0") (d (list (d (n "cursive") (r "^0.8") (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "1zl1czs89l7lqg68db4nq6q87npv294ib75yzm5x095bnkn7bz8w") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_table_view-0.7.0 (c (n "cursive_table_view") (v "0.7.0") (d (list (d (n "cursive") (r "^0.10") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "02cs9a56qz1b93nq0j6c07w3bb2lqq8s4467ha0df38s5107p09c") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_table_view-0.7.1 (c (n "cursive_table_view") (v "0.7.1") (d (list (d (n "cursive") (r "^0.10") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "034s2b9nbjiwyjz982zdciq4hr2h8k9kr5swscx6aaxzrhsxnq3b") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_table_view-0.8.0 (c (n "cursive_table_view") (v "0.8.0") (d (list (d (n "cursive") (r "^0.11") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "19wws8lnw9jalq9bcn415pz2fxr84nvf02c7rq2pxs6n00lhpmnc") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("blt-backend" "cursive/blt-backend"))))))

(define-public crate-cursive_table_view-0.9.0 (c (n "cursive_table_view") (v "0.9.0") (d (list (d (n "cursive") (r "^0.11") (k 0)) (d (n "cursive") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0j9wxllz8q3529w78pmggwv1xchdaa7sazqx2vi54j78djcaz0dg")))

(define-public crate-cursive_table_view-0.10.0 (c (n "cursive_table_view") (v "0.10.0") (d (list (d (n "cursive") (r "^0.12") (k 0)) (d (n "cursive") (r "^0.12") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "00ss00qc6z605b1bfd0hl3y4nf9y8nkwafavwsxrz6pggcngdmcx")))

(define-public crate-cursive_table_view-0.12.0 (c (n "cursive_table_view") (v "0.12.0") (d (list (d (n "cursive") (r "^0.14") (k 0)) (d (n "cursive") (r "^0.14") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1ys1r6gzix4wnqyk7bpnaasqryynv7b3jc5xfhj71r8ppxkh07i3")))

(define-public crate-cursive_table_view-0.13.0 (c (n "cursive_table_view") (v "0.13.0") (d (list (d (n "cursive") (r "^0.16") (d #t) (k 2)) (d (n "cursive_core") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "11773hxy41vfv87n627lgqrxm2plnfsjvcllr3s37i80hgfslpz5")))

(define-public crate-cursive_table_view-0.13.1 (c (n "cursive_table_view") (v "0.13.1") (d (list (d (n "cursive") (r "^0.16") (d #t) (k 2)) (d (n "cursive_core") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "05spipbs06z6rhar47mac6vc3icli1nl5zwxc7a0pcjxgfd5jiml")))

(define-public crate-cursive_table_view-0.13.2 (c (n "cursive_table_view") (v "0.13.2") (d (list (d (n "cursive") (r "^0.16") (d #t) (k 2)) (d (n "cursive_core") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0275kaxf3x1ixrp3h62z16h8dsmyq32a1m1rp62mwd3pqdfsm51d")))

(define-public crate-cursive_table_view-0.13.3 (c (n "cursive_table_view") (v "0.13.3") (d (list (d (n "cursive") (r "^0.16") (d #t) (k 2)) (d (n "cursive_core") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1k7h44im12n4nwg1r28qccjzd4ar2i796h10l064ha39caipilvl")))

(define-public crate-cursive_table_view-0.14.0 (c (n "cursive_table_view") (v "0.14.0") (d (list (d (n "cursive") (r "^0.17.0") (d #t) (k 2)) (d (n "cursive_core") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0q40wj4336xdq1j0s7cf0nqnakkviacbqidj0rslpi8rgpc5v4zq")))

