(define-module (crates-io cu rs cursedcontainer) #:use-module (crates-io))

(define-public crate-cursedcontainer-0.1.0 (c (n "cursedcontainer") (v "0.1.0") (d (list (d (n "spin") (r "^0.9") (f (quote ("mutex" "spin_mutex"))) (k 0)))) (h "151fswvjxspdlb0x8zil9ng7yvy50ih71zc524vy6p266imqi0nx") (f (quote (("std" "spin/std") ("default" "std"))))))

(define-public crate-cursedcontainer-0.1.1 (c (n "cursedcontainer") (v "0.1.1") (d (list (d (n "spin") (r "^0.9") (f (quote ("mutex" "spin_mutex"))) (k 0)))) (h "11q2w3d5k9ahlm8s4k2b2700v3rywz5qyxwn7f9m1j00p9yphbjr") (f (quote (("std" "spin/std") ("default" "std"))))))

(define-public crate-cursedcontainer-0.1.2 (c (n "cursedcontainer") (v "0.1.2") (d (list (d (n "spin") (r "^0.9") (f (quote ("mutex" "spin_mutex"))) (k 0)))) (h "05lf6677gxfk9h6s6mlhvdbf5lwzaxwjynsqlg5wr21fk7r6i8b1") (f (quote (("std" "spin/std") ("default" "std"))))))

