(define-module (crates-io cu rs cursive-tabs) #:use-module (crates-io))

(define-public crate-cursive-tabs-0.1.0 (c (n "cursive-tabs") (v "0.1.0") (d (list (d (n "cursive") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1r1j71gkvjrr3lkcgzkp0pif3kds837zibd42r7sr0jly58c79gk")))

(define-public crate-cursive-tabs-0.1.1 (c (n "cursive-tabs") (v "0.1.1") (d (list (d (n "cursive") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1lwrcd40jb3ddm652bzbyzca6k00s7qkd5zs81cg2j6q5ra6pfxr")))

(define-public crate-cursive-tabs-0.2.0 (c (n "cursive-tabs") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "cursive") (r "^0.13.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "02icfbnn90rj86qnvxf6crf9ylvnd89f8ghzpj2d8v2j963av7y5")))

(define-public crate-cursive-tabs-0.2.1 (c (n "cursive-tabs") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "cursive") (r "^0.13.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1xy3mhfi006k3jndwqhqxvp6a6g4z6gkjrmw8f7rr2jy7846mv7r")))

(define-public crate-cursive-tabs-0.2.2 (c (n "cursive-tabs") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "cursive") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0jba4cmia28dvg9y9my97yghyhac6gl1bf1a1d3phi80ww7l2sbq")))

(define-public crate-cursive-tabs-0.2.3 (c (n "cursive-tabs") (v "0.2.3") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "cursive") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0z3p4r3rya634gy2ds215pdh5c3hgrc8k3v10jr6hcqialhjkyrb")))

(define-public crate-cursive-tabs-0.2.4 (c (n "cursive-tabs") (v "0.2.4") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "cursive") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0cy425dvxkdy05w55a6lvgkl48md4147jk5h1x81jyrr2k9mrnvj")))

(define-public crate-cursive-tabs-0.2.5 (c (n "cursive-tabs") (v "0.2.5") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "cursive") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1zqhzjv3c3895kv6qx68q0lzkwj2fdhmjgd1k57f3k2n5fykbccn")))

(define-public crate-cursive-tabs-0.2.6 (c (n "cursive-tabs") (v "0.2.6") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "cursive") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0lrb8vviagx3mqri6590fl9ka66f0ss3p8i8j0an5cq0yvvl13v5")))

(define-public crate-cursive-tabs-0.2.7 (c (n "cursive-tabs") (v "0.2.7") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "cursive") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "12aw6pr44a29v1x3pnkr4xnlmqd9nk83h7ai8izs2fgp5p8mdd1g")))

(define-public crate-cursive-tabs-0.3.0 (c (n "cursive-tabs") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "cursive") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1q04driac8r54bf7mkfxa9ywih91wj3gfz7p9fj983r120zsah6j")))

(define-public crate-cursive-tabs-0.3.1 (c (n "cursive-tabs") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "cursive") (r "~0.14") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0gls23ybj2ks2bwiva9n2x87glyd8avzz1cdz125avdqqa4i4ycr") (y #t)))

(define-public crate-cursive-tabs-0.4.0 (c (n "cursive-tabs") (v "0.4.0") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "cursive") (r "^0.14.0") (k 0)) (d (n "cursive") (r "^0.14") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0qq43shidfcw9xgss0wb3ldzgpf9v2vz3vxzkpbjc94v65sy6p7v")))

(define-public crate-cursive-tabs-0.4.1 (c (n "cursive-tabs") (v "0.4.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "cursive") (r "^0.14.0") (k 0)) (d (n "cursive") (r "^0.14.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "0vabyqlwagknzl4jiky2j97s109f7nwfy218yzbnmapdxg6dc60m")))

(define-public crate-cursive-tabs-0.4.2 (c (n "cursive-tabs") (v "0.4.2") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "cursive") (r "^0.14.0") (k 0)) (d (n "cursive") (r "^0.14.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "00836qpnfn8ar70a9bvnk71w571573zn604p7x7cijaxxj4l0wq4")))

(define-public crate-cursive-tabs-0.5.0 (c (n "cursive-tabs") (v "0.5.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "cursive") (r "^0.15.0") (d #t) (k 2)) (d (n "cursive_core") (r "^0.1") (d #t) (k 0)) (d (n "insta") (r "^0.13.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "1r1pd98svdifb78zhk1s49c5wqw65qypdyxmdpbb4ijdx2cmyfal")))

(define-public crate-cursive-tabs-0.6.0 (c (n "cursive-tabs") (v "0.6.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "cursive") (r "^0.16") (d #t) (k 2)) (d (n "cursive_core") (r "^0.2") (d #t) (k 0)) (d (n "insta") (r "^1.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "0zg8whphz6vqdfzcv2pp5pvy40xjf5vigyq01d3hkgqfm1qmrj1g")))

(define-public crate-cursive-tabs-0.7.0 (c (n "cursive-tabs") (v "0.7.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "cursive") (r "^0.17.0") (d #t) (k 2)) (d (n "cursive_core") (r "^0.3") (d #t) (k 0)) (d (n "insta") (r "^1.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 2)))) (h "0ayph6dnpphlv935l6spdr3rzw8d36q5kw35394nf3ifacg1pc00")))

