(define-module (crates-io cu ne cuneiform) #:use-module (crates-io))

(define-public crate-cuneiform-0.0.0 (c (n "cuneiform") (v "0.0.0") (h "0l76vy1bv8qqzn5nrlhlbf6bbvssvfiymjvg4q5sc9lkpdw5hdjs")))

(define-public crate-cuneiform-0.1.0 (c (n "cuneiform") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "136rnjqcnzp7zj4ypf2f8wpnc8pbnvjiyi88bbbsvmbp7ws47vdg")))

(define-public crate-cuneiform-0.1.1 (c (n "cuneiform") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1bp3viprrd39k9p4g0055431jvrspvprpxjqh3ckyfmrhhc8fldw")))

