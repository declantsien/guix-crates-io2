(define-module (crates-io cu ne cuneiform-fields) #:use-module (crates-io))

(define-public crate-cuneiform-fields-0.0.0 (c (n "cuneiform-fields") (v "0.0.0") (h "1g05iwqsk3lmywrz3r77pbzf9vbgfizcdj9qdzvwcylzh6qnvqs6")))

(define-public crate-cuneiform-fields-0.1.0 (c (n "cuneiform-fields") (v "0.1.0") (d (list (d (n "cuneiform") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0c6fw4rm28y5k8vmcbpbd2f0hlx7m4s5r1s279zghcz5lnp2k9yh")))

(define-public crate-cuneiform-fields-0.1.1 (c (n "cuneiform-fields") (v "0.1.1") (d (list (d (n "cuneiform") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "14mxzgx7ir23215fsj6vn10wjdf28njj340sjwglkp8m4mkmy2n6")))

