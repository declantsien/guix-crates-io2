(define-module (crates-io cu pe cupertino-sys) #:use-module (crates-io))

(define-public crate-cupertino-sys-0.1.0 (c (n "cupertino-sys") (v "0.1.0") (h "0yf59m22bpbiy5z9fs5jl5hcgdz8ybzk870hgsflc4wc330fm01y") (y #t)))

(define-public crate-cupertino-sys-0.0.0 (c (n "cupertino-sys") (v "0.0.0") (h "1dj8i30rxixhd9aqfaqi6i0g2wwd4g0x5aqcwfd3ilqys3y551r5")))

