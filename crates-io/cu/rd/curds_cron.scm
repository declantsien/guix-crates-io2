(define-module (crates-io cu rd curds_cron) #:use-module (crates-io))

(define-public crate-curds_cron-0.1.0 (c (n "curds_cron") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mockall") (r "^0.9.1") (d #t) (k 2)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1v491ip8q8axancv0a341b660llh3h0sgab9rh49iyj9ff101v0a")))

(define-public crate-curds_cron-0.2.0 (c (n "curds_cron") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mockall") (r "^0.9.1") (d #t) (k 2)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "149x8df5318z5nfdkgv3jzj2xanmkd6l0953143vak4bbs5hndqa")))

