(define-module (crates-io cu dn cudnn) #:use-module (crates-io))

(define-public crate-cudnn-0.1.0 (c (n "cudnn") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.3") (d #t) (k 2)) (d (n "cudnn-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1z85749blp36vg69mj33qskipsbc1iw6gc9dnd4lghz9pm20k1a8") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cudnn-0.1.1 (c (n "cudnn") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.5") (d #t) (k 2)) (d (n "cudnn-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08fbp6h9mgr51yx3svn62xj5lar4yajaksd68icdjrdfq580ahsw") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cudnn-0.1.2 (c (n "cudnn") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.5") (d #t) (k 2)) (d (n "cudnn-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yaf7q8ns32xqhvaz70afzax88m0k3nq8ch2sx966ypzav6idwlv") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cudnn-0.1.3 (c (n "cudnn") (v "0.1.3") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.5") (d #t) (k 2)) (d (n "cudnn-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d58ixpxwzlssvab1qzx5b4kd3hzxwzd78msgg9lzvjhybpyhj37") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cudnn-1.0.0 (c (n "cudnn") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.5") (d #t) (k 2)) (d (n "cudnn-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k78abicc0fpc4x0494jldmpgiqqvncarqrl64am7agvn7k76ld6") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cudnn-1.0.1 (c (n "cudnn") (v "1.0.1") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.7") (d #t) (k 0)) (d (n "cudnn-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rrmy4l10s8qx23pyds71nakkaf0l52kzgna6lncfgna6qpb6wvd") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cudnn-1.1.0 (c (n "cudnn") (v "1.1.0") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.7") (d #t) (k 0)) (d (n "cudnn-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cjzrxiqfmcak0bgfhgpvhr2q2pzhy2x30x0fw8a7xfk4273hzz0") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cudnn-1.2.0 (c (n "cudnn") (v "1.2.0") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.7") (d #t) (k 0)) (d (n "cudnn-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10zrb3kdc1rzfbcavb1iywzjjh563z2j53bll7pzb390hggq61b9") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cudnn-1.2.1 (c (n "cudnn") (v "1.2.1") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.8") (d #t) (k 0)) (d (n "cudnn-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03p658frv0925874bnx3dhlsv3s3h5cnb1q5w6m4fp61si9gdi0z") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cudnn-1.3.0 (c (n "cudnn") (v "1.3.0") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "cudnn-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xglyk39iq5xffginn3r50j0d6m46iv18gkccpbxmkcizvdmkr8g") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cudnn-1.3.1 (c (n "cudnn") (v "1.3.1") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "cudnn-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00csm5fvy3f6k1n49cz3bm7vm1yfg84jl9jr7sipgwr4584irp7z") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

