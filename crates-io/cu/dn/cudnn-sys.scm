(define-module (crates-io cu dn cudnn-sys) #:use-module (crates-io))

(define-public crate-cudnn-sys-0.0.1 (c (n "cudnn-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "160wf1m4bw4i0y5v8iksrmrniziq3cg69kph7ac098hq9azq9rmr")))

(define-public crate-cudnn-sys-0.0.2 (c (n "cudnn-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dnfhig2rrxbxj5m9vjh98jd9995vwp71a2rxl64llxkg8rzsdkb")))

(define-public crate-cudnn-sys-0.0.3 (c (n "cudnn-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rsckv2dnf6flg1117fbkg7039vaq4m3zi5sq6l2iz1cm6s9x0ji")))

