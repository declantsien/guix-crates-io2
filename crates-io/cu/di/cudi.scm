(define-module (crates-io cu di cudi) #:use-module (crates-io))

(define-public crate-cudi-0.1.0 (c (n "cudi") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "cust") (r "^0.3.2") (d #t) (k 0)))) (h "0nc55is9csb2r0i55c3irlfyzbhzky5b6hpjp5bb9ysd54c4j4zj")))

