(define-module (crates-io cu et cuet) #:use-module (crates-io))

(define-public crate-cuet-0.0.1 (c (n "cuet") (v "0.0.1") (h "0421rf9h3vdnb2aagihvxa6dn0gxyld3smmz1zrxyfmcz5mnb1d1")))

(define-public crate-cuet-0.0.2 (c (n "cuet") (v "0.0.2") (h "0mcfz9p1p5026mn83cgwd30qfpxcrvd869ndpi63dfi57qkxrc9r")))

(define-public crate-cuet-0.0.3 (c (n "cuet") (v "0.0.3") (h "0rqlq63rawbr7llmyp9vgj2hq4ywgicrl8np47pdn9p8zpdwha85")))

