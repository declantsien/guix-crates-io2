(define-module (crates-io cu pp cuppa) #:use-module (crates-io))

(define-public crate-cuppa-0.0.1 (c (n "cuppa") (v "0.0.1") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0k3ga73y8fwqmlkb728jabjv2pzs7fv9a4dqm74fn4f7sl6ykqll")))

(define-public crate-cuppa-0.0.2 (c (n "cuppa") (v "0.0.2") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0ffp3cva6s8s0396pimi4gpp53kvn5l8kv9dgsvc52d6s0jcw0dc")))

(define-public crate-cuppa-0.0.3 (c (n "cuppa") (v "0.0.3") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1h173gb6c3ffd0kq2xayn35ncsjg53z36zqr3xwmqxihnv303l2n")))

(define-public crate-cuppa-0.0.4 (c (n "cuppa") (v "0.0.4") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0dv32pv874dqwf34lgmybi0z5pycx9d1a8z7049pwf91ayv3iw3y")))

(define-public crate-cuppa-0.0.5 (c (n "cuppa") (v "0.0.5") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0165kclwrqyx0wvxz8lrg5l6xr94avslmd8sj1vw9rpmgs2c5l55")))

(define-public crate-cuppa-0.0.6 (c (n "cuppa") (v "0.0.6") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "00c57csb96zni855175qr9pg06hgxvmkyfmikmmhhisf0chnvhns")))

(define-public crate-cuppa-0.0.7 (c (n "cuppa") (v "0.0.7") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0x25p7qd589a3s7k9dhsdy04kvij8q8is747wxk05ywak4zanfki")))

(define-public crate-cuppa-0.0.8 (c (n "cuppa") (v "0.0.8") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1rn1fvj83lisaij6c4lkzqbsdnd4aqbv9xx8a0754fvzjizzggvd")))

(define-public crate-cuppa-0.0.9 (c (n "cuppa") (v "0.0.9") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1d79r8whnwz1ivg6s5a1d1h4dd3v0jj933w62lv7i3zrkwfh6lfj")))

(define-public crate-cuppa-0.0.10 (c (n "cuppa") (v "0.0.10") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0l21imb76dbz80kh558gn5n3crd5k0kqgxvn09k4lghpmd0qayzf")))

(define-public crate-cuppa-0.0.11 (c (n "cuppa") (v "0.0.11") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1hqsyk4vnflfqg58gpcqnbf8w187zzpg7syk1aqb75ia149wz203")))

(define-public crate-cuppa-0.0.13 (c (n "cuppa") (v "0.0.13") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0q0ydyyrl3d44ipz7jzv9g8n6r4s5kpw10v4mwjhcnrj0r52jn0z")))

(define-public crate-cuppa-0.0.14 (c (n "cuppa") (v "0.0.14") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "09a4y6fg2j5bvjmbfn8a4w05pnmx0zs8jpwlzprsgvi32b36hh6g")))

(define-public crate-cuppa-0.0.15 (c (n "cuppa") (v "0.0.15") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0gkli3aiy52177i1jxg3vfzy45dvw5qprzxfb18g10lc1yhf1cx6")))

(define-public crate-cuppa-0.0.16 (c (n "cuppa") (v "0.0.16") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0ivgdx1jdm2f9bv64fwxwzjs60ya3bklcq4z3hnny42wywrn3dhx")))

(define-public crate-cuppa-0.0.17 (c (n "cuppa") (v "0.0.17") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1pisjb32s7fc8id6v5cnd0p1vzisiz7mfh5fac3m72glj6jz9r2c")))

(define-public crate-cuppa-0.0.18 (c (n "cuppa") (v "0.0.18") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0f8pmqp7vs19npz9lq9jc47yi2wvh2dwwvkf17qj8bpwb6c00y0b")))

