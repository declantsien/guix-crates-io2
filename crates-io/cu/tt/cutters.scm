(define-module (crates-io cu tt cutters) #:use-module (crates-io))

(define-public crate-cutters-0.1.0 (c (n "cutters") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "14116yc636z41yiqpyrpd200dnzj6x3gs4dpx3zl1vp0lnydiai1")))

(define-public crate-cutters-0.1.1 (c (n "cutters") (v "0.1.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "13jfhnm2b61d6gihrimx5rv1vkfww9hd1l7p8rzwsl8ap40pnslr") (y #t)))

(define-public crate-cutters-0.1.2 (c (n "cutters") (v "0.1.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "08ns2hrz3qnq1aks4shjppkfidafgdq787vfflszcvfqn91nl9kc")))

(define-public crate-cutters-0.1.3 (c (n "cutters") (v "0.1.3") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "140b6czmw8vn417hpb4jr162qrj3h6b5cvqdzfy5r5n1i6wxk4x2")))

(define-public crate-cutters-0.1.4 (c (n "cutters") (v "0.1.4") (d (list (d (n "pest") (r "=2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "=2.5.7") (d #t) (k 0)))) (h "1l7rrjl811sxha926mniwbmspzqwqr6xk9x2c6ddbk4sdx245m3g")))

