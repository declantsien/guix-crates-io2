(define-module (crates-io cu tt cutter) #:use-module (crates-io))

(define-public crate-cutter-0.1.0 (c (n "cutter") (v "0.1.0") (h "0rdbjr3ad5aw5si0391l44yfxm12s7gisbnz7v3d86laxvmva6ag")))

(define-public crate-cutter-0.1.1 (c (n "cutter") (v "0.1.1") (h "1s0jcmagw3fwvr8q2niv5ivz5fiqlg2ycj93lgzfhfw8s4p4l3ja")))

(define-public crate-cutter-0.2.0 (c (n "cutter") (v "0.2.0") (h "1vf30k35jpwnqqxb7dp9s6dksjw8vnmhxzgkya7djzsfibp5azzd")))

