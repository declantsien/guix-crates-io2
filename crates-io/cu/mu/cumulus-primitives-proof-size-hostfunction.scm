(define-module (crates-io cu mu cumulus-primitives-proof-size-hostfunction) #:use-module (crates-io))

(define-public crate-cumulus-primitives-proof-size-hostfunction-0.0.0 (c (n "cumulus-primitives-proof-size-hostfunction") (v "0.0.0") (h "04bcsh9wgdsgf17l6q3j0fxbl127zs4fy37fx2alms2i0ijqxkvb")))

(define-public crate-cumulus-primitives-proof-size-hostfunction-0.1.0 (c (n "cumulus-primitives-proof-size-hostfunction") (v "0.1.0") (d (list (d (n "sp-externalities") (r "^0.24.0") (k 0)) (d (n "sp-runtime-interface") (r "^23.0.0") (k 0)) (d (n "sp-trie") (r "^28.0.0") (k 0)))) (h "0c7gzrvnyyhch7pgy3i0ydix8fvn7ldq29hjbbyvackb4nxni1jd") (f (quote (("std" "sp-externalities/std" "sp-runtime-interface/std" "sp-trie/std") ("default" "std"))))))

(define-public crate-cumulus-primitives-proof-size-hostfunction-0.2.0 (c (n "cumulus-primitives-proof-size-hostfunction") (v "0.2.0") (d (list (d (n "sp-externalities") (r "^0.25.0") (k 0)) (d (n "sp-runtime-interface") (r "^24.0.0") (k 0)) (d (n "sp-trie") (r "^29.0.0") (k 0)))) (h "1955zky1mg59z1glxw9lcjb5p9jwndndbc04a2y3d567q9xf2cm1") (f (quote (("std" "sp-externalities/std" "sp-runtime-interface/std" "sp-trie/std") ("default" "std"))))))

(define-public crate-cumulus-primitives-proof-size-hostfunction-0.3.0 (c (n "cumulus-primitives-proof-size-hostfunction") (v "0.3.0") (d (list (d (n "sp-externalities") (r "^0.26.0") (k 0)) (d (n "sp-runtime-interface") (r "^25.0.0") (k 0)) (d (n "sp-trie") (r "^30.0.0") (k 0)))) (h "06g1a117508wc66zmszifxxhjr6s8vg7m53az55ry2qr868lzdy1") (f (quote (("std" "sp-externalities/std" "sp-runtime-interface/std" "sp-trie/std") ("default" "std"))))))

(define-public crate-cumulus-primitives-proof-size-hostfunction-0.4.0 (c (n "cumulus-primitives-proof-size-hostfunction") (v "0.4.0") (d (list (d (n "sp-externalities") (r "^0.27.0") (k 0)) (d (n "sp-runtime-interface") (r "^26.0.0") (k 0)) (d (n "sp-trie") (r "^31.0.0") (k 0)))) (h "1jv2rfzp0c7s6dg67440r74mrvh64hjpsg0605b9ygl8y2c4w0yp") (f (quote (("std" "sp-externalities/std" "sp-runtime-interface/std" "sp-trie/std") ("default" "std"))))))

(define-public crate-cumulus-primitives-proof-size-hostfunction-0.5.0 (c (n "cumulus-primitives-proof-size-hostfunction") (v "0.5.0") (d (list (d (n "sp-externalities") (r "^0.27.0") (k 0)) (d (n "sp-runtime-interface") (r "^26.0.0") (k 0)) (d (n "sp-trie") (r "^32.0.0") (k 0)))) (h "0x3bf59zxkd4l95am1f0my85dz1pq20vmydsbm2mqzffinb2fkkn") (f (quote (("std" "sp-externalities/std" "sp-runtime-interface/std" "sp-trie/std") ("default" "std"))))))

(define-public crate-cumulus-primitives-proof-size-hostfunction-0.6.0 (c (n "cumulus-primitives-proof-size-hostfunction") (v "0.6.0") (d (list (d (n "sp-externalities") (r "^0.28.0") (k 0)) (d (n "sp-runtime-interface") (r "^27.0.0") (k 0)) (d (n "sp-trie") (r "^33.0.0") (k 0)))) (h "1p6pfw0ckjqx1x48ik51g399h3wmlsl3pmd2x5gag1ngdqrfb71s") (f (quote (("std" "sp-externalities/std" "sp-runtime-interface/std" "sp-trie/std") ("default" "std"))))))

(define-public crate-cumulus-primitives-proof-size-hostfunction-0.7.0 (c (n "cumulus-primitives-proof-size-hostfunction") (v "0.7.0") (d (list (d (n "sp-externalities") (r "^0.28.0") (k 0)) (d (n "sp-runtime-interface") (r "^27.0.0") (k 0)) (d (n "sp-trie") (r "^34.0.0") (k 0)))) (h "1k1271xwj9bzarq4zqavm6cyr99g8z1k0v3m67chh0sbdd576dp7") (f (quote (("std" "sp-externalities/std" "sp-runtime-interface/std" "sp-trie/std") ("default" "std"))))))

(define-public crate-cumulus-primitives-proof-size-hostfunction-0.8.0 (c (n "cumulus-primitives-proof-size-hostfunction") (v "0.8.0") (d (list (d (n "sp-externalities") (r "^0.28.0") (k 0)) (d (n "sp-runtime-interface") (r "^27.0.0") (k 0)) (d (n "sp-trie") (r "^35.0.0") (k 0)))) (h "1w9ijs49m476n8hrmbiiqpa694x96w0pg0c8mkd48nwadlzcf5gq") (f (quote (("std" "sp-externalities/std" "sp-runtime-interface/std" "sp-trie/std") ("default" "std"))))))

