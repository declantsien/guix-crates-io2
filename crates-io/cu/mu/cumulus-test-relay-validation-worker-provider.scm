(define-module (crates-io cu mu cumulus-test-relay-validation-worker-provider) #:use-module (crates-io))

(define-public crate-cumulus-test-relay-validation-worker-provider-0.0.0 (c (n "cumulus-test-relay-validation-worker-provider") (v "0.0.0") (h "13g8z8c7rk7kb42rfzryckgvzbmiy22kszq77wzr8ac3xw29vzdf") (y #t)))

(define-public crate-cumulus-test-relay-validation-worker-provider-0.1.0-dev.1 (c (n "cumulus-test-relay-validation-worker-provider") (v "0.1.0-dev.1") (d (list (d (n "polkadot-node-core-pvf") (r "=0.1.0-dev.6") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 1)))) (h "1nzm3kz5jf98gjff97d7rif2cp59h0py5pgsk3fd4yc2hvqjq3dx")))

(define-public crate-cumulus-test-relay-validation-worker-provider-0.1.0-dev.2 (c (n "cumulus-test-relay-validation-worker-provider") (v "0.1.0-dev.2") (d (list (d (n "polkadot-node-core-pvf") (r "=0.1.0-dev.6") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 1)))) (h "0qmhwk7a7yl0fbhy34hkdsgxq3hs4vfjfjsy1xs38ldqwaq52ng5")))

(define-public crate-cumulus-test-relay-validation-worker-provider-0.1.0-dev.3 (c (n "cumulus-test-relay-validation-worker-provider") (v "0.1.0-dev.3") (d (list (d (n "polkadot-node-core-pvf") (r "=0.1.0-dev.6") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 1)))) (h "02ha8blh03c08dfxadvvbsawi4yyg1q3aqxsp7qbvq4404cb125s")))

(define-public crate-cumulus-test-relay-validation-worker-provider-0.1.0 (c (n "cumulus-test-relay-validation-worker-provider") (v "0.1.0") (d (list (d (n "polkadot-node-core-pvf") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 1)))) (h "0yzbj9lbhkxk5zvbb6a2n17mmg04zg24nmdm3id1f3zv0aaxzfdc")))

