(define-module (crates-io cu mu cumulfreqtable) #:use-module (crates-io))

(define-public crate-cumulfreqtable-0.1.0 (c (n "cumulfreqtable") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03fi76bjmazr2329r9ijlihpdfqks5j139f9ar2iln5942rbh03c")))

(define-public crate-cumulfreqtable-0.1.1 (c (n "cumulfreqtable") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nfrcwmw1q5bbkxh0r6yq7rzh3py4hq7d5p10paphb2pmbbx1rvh")))

(define-public crate-cumulfreqtable-0.1.2 (c (n "cumulfreqtable") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1jdpi424n8px5gwj47wjwsbb7f0vfcqpks1yi2ad77mni1k87lai")))

(define-public crate-cumulfreqtable-0.1.3 (c (n "cumulfreqtable") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "12ypgvmvz4hr4r5p5vl10g3y2ikqf1b9vwnkz5xkrq6d66pzhv07")))

