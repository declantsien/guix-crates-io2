(define-module (crates-io cu mu cumulus) #:use-module (crates-io))

(define-public crate-cumulus-0.0.0 (c (n "cumulus") (v "0.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0if26sip68zipcp9gbrgw7wvn8xijfzlk8fiyp9biapb3912r56a")))

(define-public crate-cumulus-0.0.1 (c (n "cumulus") (v "0.0.1") (d (list (d (n "inspector") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (o #t) (d #t) (k 0)))) (h "0prmn8ginz8v30yrddcfmikhwijglsdbmxcyyilcvisn59rld0lk") (f (quote (("yaml" "serde_yaml") ("pedantic"))))))

(define-public crate-cumulus-0.0.2 (c (n "cumulus") (v "0.0.2") (d (list (d (n "inspector") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (o #t) (d #t) (k 0)))) (h "0ldv8q7k5gsnh106i9mshjkmaz1q6a4pqfiddmvagg1d695gbxks") (f (quote (("yaml" "serde_yaml") ("pedantic"))))))

(define-public crate-cumulus-0.0.3 (c (n "cumulus") (v "0.0.3") (d (list (d (n "inspector") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (o #t) (d #t) (k 0)))) (h "0z7mfj0h9rdm3syfxxmiawkr99spc87y7sl7391bnaybwbrb9hbd") (f (quote (("yaml" "serde_yaml") ("pedantic"))))))

(define-public crate-cumulus-0.0.4 (c (n "cumulus") (v "0.0.4") (d (list (d (n "inspector") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (o #t) (d #t) (k 0)))) (h "1bj2jzrd227w0gy9sv3zgjap47nm02bhsa345jczdgdv3swkz17y") (f (quote (("yaml" "serde_yaml") ("pedantic"))))))

(define-public crate-cumulus-0.0.6 (c (n "cumulus") (v "0.0.6") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 1)) (d (n "inspector") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11zldipdhsjc7cy5m5yks2anl235745f0ymw651qcb19nca3psa6") (f (quote (("yaml" "serde_yaml") ("pedantic") ("default" "yaml"))))))

