(define-module (crates-io cu ra cura) #:use-module (crates-io))

(define-public crate-cura-0.1.0 (c (n "cura") (v "0.1.0") (h "0l9d1avgfs39xippxg27gvb544n5adm6wag6pgjdb9j2pgkldbjh")))

(define-public crate-cura-0.2.0 (c (n "cura") (v "0.2.0") (h "18g7w08sl4bbvczswx142k96pfmr8bn6h67p3wbxks3khknfy8cq")))

(define-public crate-cura-0.3.0 (c (n "cura") (v "0.3.0") (h "1dqc9lf0dmgkf0qzvf4l54bbv5zl0scdinc4ifnzpzrn04yjrna4")))

(define-public crate-cura-0.4.0 (c (n "cura") (v "0.4.0") (h "1mlcbx229ij7p3b6dg5gp7ss6xmj1ilhvpwal408izqf07zjw2cl")))

(define-public crate-cura-0.4.1 (c (n "cura") (v "0.4.1") (h "0w0il1hvqwyhw8318qv61s4hv6i8kc9zzcjfzxkx9lgk03x0cxhy")))

(define-public crate-cura-0.4.2 (c (n "cura") (v "0.4.2") (h "0sn9dhm0pvfc7106s85716gw59ygzifcbm7wxyl8z4xclfmbhv6i")))

(define-public crate-cura-0.5.0 (c (n "cura") (v "0.5.0") (h "0i9fd0gd0mi87wszcnm9qj4w9srq6r2ygh37b47canay98payxjs")))

(define-public crate-cura-0.6.0 (c (n "cura") (v "0.6.0") (h "0q4bkzs2hzdfa30zj78vhvyxji4v3jzpyd6h9b0bayiyya3bvpdv")))

(define-public crate-cura-0.6.1 (c (n "cura") (v "0.6.1") (h "0pxsv6x6k7i3a7wc944sip3l88qlmy6kik6dlysbjf2zp68ysr9w")))

(define-public crate-cura-0.7.0 (c (n "cura") (v "0.7.0") (h "0pzp1863qnar8sy7f2cjc07w5xa5rwlvnwbw8aqd2b0ww0bbhn53")))

(define-public crate-cura-0.7.1 (c (n "cura") (v "0.7.1") (h "091x4w30kq0ya3n60kgijm6smsfknsyx8kw4qlnwl83n63qgmssv")))

(define-public crate-cura-0.7.2 (c (n "cura") (v "0.7.2") (h "1h6jmjnbiszhf6zmv6aisp5qmavn53fmn2lzh25x1i8gwv2svm49")))

(define-public crate-cura-0.8.0 (c (n "cura") (v "0.8.0") (h "1ls7d3jisn8pb8vhlzxvvsagk699kjs2kyi3yv743wahvnfcakc4")))

