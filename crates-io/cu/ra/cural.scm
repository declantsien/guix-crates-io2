(define-module (crates-io cu ra cural) #:use-module (crates-io))

(define-public crate-cural-0.1.0 (c (n "cural") (v "0.1.0") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_System_Diagnostics_ToolHelp" "Win32_System_Diagnostics_Debug" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (k 0)))) (h "0hyhmf48ilfyc9sclpdhmj3x4rinv2bsj4nbcz62vk6lylcbmmh7")))

(define-public crate-cural-0.1.1 (c (n "cural") (v "0.1.1") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_System_Diagnostics_ToolHelp" "Win32_System_Diagnostics_Debug" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (k 0)))) (h "18zycsv4bb1igj0zabcz3mafawwix3yckajq1xriw8aj7kjwxdlc")))

(define-public crate-cural-0.1.3 (c (n "cural") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("tlhelp32" "handleapi" "memoryapi" "wow64apiset" "processthreadsapi"))) (d #t) (k 0)))) (h "0k2vxcf5l543rda7c01mbymw9jmnj6w0vh190xycrilm3zcw93mg")))

