(define-module (crates-io cu e- cue-sdk-sys) #:use-module (crates-io))

(define-public crate-cue-sdk-sys-0.0.1 (c (n "cue-sdk-sys") (v "0.0.1") (h "1hz1r37lpgqvg4ap82hn7ad5l5m5hcc33n3v96735mfz18b4fnp7") (l "CUESDK")))

(define-public crate-cue-sdk-sys-0.0.2 (c (n "cue-sdk-sys") (v "0.0.2") (h "05mwyi2s46xlza5a5ghdzdxb673n3xnzhjy8fb03fmmmzlygk1w8") (l "CUESDK")))

(define-public crate-cue-sdk-sys-0.0.3 (c (n "cue-sdk-sys") (v "0.0.3") (h "088p2mxypj18wdzl3pav2m32zw444szd9pg5myjp6iaq7ilah5vj") (l "CUESDK")))

(define-public crate-cue-sdk-sys-0.0.4 (c (n "cue-sdk-sys") (v "0.0.4") (h "1a0c598y8m5rb1v91dm43g631afvam87ydh7ryzvk5kha92kmavk") (l "CUESDK")))

(define-public crate-cue-sdk-sys-0.0.5 (c (n "cue-sdk-sys") (v "0.0.5") (h "0vxksh199x0mzsiry0sipv9jgirqsrrl379l4zbfawg5j8zlxs26") (l "CUESDK")))

