(define-module (crates-io cu e- cue-sys) #:use-module (crates-io))

(define-public crate-cue-sys-1.0.0 (c (n "cue-sys") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "1s77lkl5xm82sw32shy2nrs2nlyj6k9qgy68yj8yjp6f623nif5v")))

(define-public crate-cue-sys-1.1.0 (c (n "cue-sys") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "0h0ly48ks4584s4qmmpzn761ynq9jdicgng52nr9fggpsfgbzdky")))

