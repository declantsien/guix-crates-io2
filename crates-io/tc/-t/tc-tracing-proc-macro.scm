(define-module (crates-io tc -t tc-tracing-proc-macro) #:use-module (crates-io))

(define-public crate-tc-tracing-proc-macro-0.0.0 (c (n "tc-tracing-proc-macro") (v "0.0.0") (h "075z6w5sbqmnwg2idhnqglcmn65dn9siy8mj4x0a7w140i29jchz") (y #t)))

(define-public crate-tc-tracing-proc-macro-2.0.0 (c (n "tc-tracing-proc-macro") (v "2.0.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("proc-macro" "full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0q6rfzbnlwzkrx9sicmxy39yhhlyim2hxr068pr7qa9pda7vnijp")))

