(define-module (crates-io tc -t tc-telemetry) #:use-module (crates-io))

(define-public crate-tc-telemetry-0.0.0 (c (n "tc-telemetry") (v "0.0.0") (h "1vksyixw6p56mkbhi405694rmrgag8d1fxjlryjxgm57vwri5vjc") (y #t)))

(define-public crate-tc-telemetry-2.0.0 (c (n "tc-telemetry") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)) (d (n "tetcore-utils") (r "^2.0.2") (d #t) (k 0)) (d (n "tetsy-libp2p") (r "^0.34.2") (f (quote ("dns" "tcp-async-io" "wasm-ext" "websocket"))) (k 0)) (d (n "tracing") (r "^0.1.10") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.13") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2.5") (d #t) (k 0)))) (h "15l5f4nad8aijavxw4a85f3s3l5kjysxnfyw2xhma01dw3dialyh")))

