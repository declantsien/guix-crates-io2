(define-module (crates-io tc ps tcps) #:use-module (crates-io))

(define-public crate-tcps-0.1.0 (c (n "tcps") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (f (quote ("logging"))) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)))) (h "0cpszd7kwbq22pxss1yi52a1kkayv94h9fzwl3s0p201101l71yw") (y #t)))

