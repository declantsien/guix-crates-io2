(define-module (crates-io tc -p tc-proposer-metrics) #:use-module (crates-io))

(define-public crate-tc-proposer-metrics-0.0.0 (c (n "tc-proposer-metrics") (v "0.0.0") (h "0qcqvfblzzhxwbnm1sw6hm2z7b0a89713njzbmnykj0drjdcbbv8") (y #t)))

(define-public crate-tc-proposer-metrics-0.8.0 (c (n "tc-proposer-metrics") (v "0.8.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.8.1") (d #t) (k 0) (p "prometheus-endpoint")))) (h "1n7b9k422sq97q71nlxhjw4kf8w6mc43s6rpkich134f1arqc0vz")))

