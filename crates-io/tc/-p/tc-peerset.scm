(define-module (crates-io tc -p tc-peerset) #:use-module (crates-io))

(define-public crate-tc-peerset-0.0.0 (c (n "tc-peerset") (v "0.0.0") (h "0kj3w0h9mj8l0xzy09x1inqh7fbdqnqpf9b1iaczl3q9ig5mi4ba") (y #t)))

(define-public crate-tc-peerset-2.0.0 (c (n "tc-peerset") (v "2.0.0") (d (list (d (n "futures") (r "^0.3.9") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tetcore-utils") (r "^2.0.2") (d #t) (k 0)) (d (n "tetsy-libp2p") (r "^0.34.2") (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "0cmrrr5s4cvkjhjyw036mr0z1dmyvv9l96md7xcfrjkrsmc9pkwq")))

