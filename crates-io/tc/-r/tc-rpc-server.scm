(define-module (crates-io tc -r tc-rpc-server) #:use-module (crates-io))

(define-public crate-tc-rpc-server-0.0.0 (c (n "tc-rpc-server") (v "0.0.0") (h "0wccl0dp80h8i29wjfyrifz8ww6glzgbham6p1vfnlqhw2i0nlyr") (y #t)))

(define-public crate-tc-rpc-server-2.0.0 (c (n "tc-rpc-server") (v "2.0.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "http") (r "^15.1.0") (d #t) (t "cfg(not(target_os = \"unknown\"))") (k 0) (p "tetsy-jsonrpc-http-server")) (d (n "ipc") (r "^15.1.0") (d #t) (t "cfg(not(target_os = \"unknown\"))") (k 0) (p "tetsy-jsonrpc-ipc-server")) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.8.1") (d #t) (k 0) (p "prometheus-endpoint")) (d (n "pubsub") (r "^15.1.0") (d #t) (k 0) (p "tetsy-jsonrpc-pubsub")) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tetsy-jsonrpc-core") (r "^15.1.0") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)) (d (n "ws") (r "^15.1.0") (d #t) (t "cfg(not(target_os = \"unknown\"))") (k 0) (p "tetsy-jsonrpc-ws-server")))) (h "1xpwrv9n8l6qzvc9ifdrffn5cxhi0m1w1zwa3h840gw0bsl42nn5")))

