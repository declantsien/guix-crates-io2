(define-module (crates-io tc h- tch-ext) #:use-module (crates-io))

(define-public crate-tch-ext-0.1.0 (c (n "tch-ext") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.18.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "tch") (r "^0.13.0") (f (quote ("python-extension"))) (d #t) (k 0)) (d (n "torch-sys") (r "^0.13.0") (f (quote ("python-extension"))) (d #t) (k 0)))) (h "0kg2bhmh590q5fqvnq3c9474y2qxrm1wjaw5bcikhprkwwf86lf8")))

(define-public crate-tch-ext-0.1.1 (c (n "tch-ext") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.18.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pyo3-tch") (r "^0.13.0") (d #t) (k 0)) (d (n "torch-sys") (r "^0.13.0") (d #t) (k 0)))) (h "0il9lcayh162rc9m3h3c6yg8xkrh3afy4g3x1v7das9pd1k2sbbr")))

(define-public crate-tch-ext-0.1.2 (c (n "tch-ext") (v "0.1.2") (d (list (d (n "pyo3") (r "^0.18.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pyo3-tch") (r "^0.15.0") (d #t) (k 0)) (d (n "torch-sys") (r "^0.15.0") (d #t) (k 0)))) (h "05lx1cmaypyk6d26nl4fdq7yvnaj60yk758l8wr5lhnhbvqxydq9")))

