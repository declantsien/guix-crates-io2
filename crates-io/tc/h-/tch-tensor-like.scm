(define-module (crates-io tc h- tch-tensor-like) #:use-module (crates-io))

(define-public crate-tch-tensor-like-0.2.0 (c (n "tch-tensor-like") (v "0.2.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "tch") (r "^0.3") (d #t) (k 0)) (d (n "tch-tensor-like-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "09i3d6sbd0h0jrkk2g0mrdrvpb0bfg3hklm9x0qb6h9hifa6c2c4") (f (quote (("derive" "tch-tensor-like-derive"))))))

(define-public crate-tch-tensor-like-0.3.0 (c (n "tch-tensor-like") (v "0.3.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "tch") (r "^0.4.0") (d #t) (k 0)) (d (n "tch-tensor-like-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "07bar310ic66dj348xpgcsvr19pg3wv58vsahapisk8mnzfwkvk4") (f (quote (("doc-only" "tch/doc-only") ("derive" "tch-tensor-like-derive"))))))

(define-public crate-tch-tensor-like-0.4.0 (c (n "tch-tensor-like") (v "0.4.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "tch") (r "^0.5.0") (d #t) (k 0)) (d (n "tch-tensor-like-derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1a1p4v0pkm7qm9klsrhyk7z5ja1zmgd0kk1n2d2x73pd6rfy3j37") (f (quote (("doc-only" "tch/doc-only") ("derive" "tch-tensor-like-derive"))))))

(define-public crate-tch-tensor-like-0.5.0 (c (n "tch-tensor-like") (v "0.5.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "tch") (r "^0.6.1") (d #t) (k 0)) (d (n "tch-tensor-like-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1q26h01vphihws5sjdqz5fxy5a7qs249wilcls2z25f89hn1x03d") (f (quote (("doc-only" "tch/doc-only") ("derive" "tch-tensor-like-derive"))))))

(define-public crate-tch-tensor-like-0.6.0 (c (n "tch-tensor-like") (v "0.6.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "tch") (r "^0.7.0") (d #t) (k 0)) (d (n "tch-tensor-like-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1x1z3244yfa2lcw1nib9v50mp327zn56r6hg8nynxdpahy7fsy1d") (f (quote (("doc-only" "tch/doc-only") ("derive" "tch-tensor-like-derive"))))))

