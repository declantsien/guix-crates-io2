(define-module (crates-io tc _g tc_generic) #:use-module (crates-io))

(define-public crate-tc_generic-0.1.0 (c (n "tc_generic") (v "0.1.0") (d (list (d (n "tc_core") (r "^0.2") (d #t) (k 0)))) (h "0fpfyjz05rhvdg1ghzv6bd1x1vqc7gp0l2hqavkmk2flffx4pdn3")))

(define-public crate-tc_generic-0.2.0 (c (n "tc_generic") (v "0.2.0") (d (list (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "039pnhimvniwryz27isazgc7s4jf4hr9g7jss95q1nfyy7j8rh3j")))

(define-public crate-tc_generic-0.2.1 (c (n "tc_generic") (v "0.2.1") (d (list (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "0vcz9kk7ymc4wwbaa7fj832f1204asns47k7bcc7m0f93svww6p1")))

