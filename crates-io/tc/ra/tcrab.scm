(define-module (crates-io tc ra tcrab) #:use-module (crates-io))

(define-public crate-tcrab-0.1.0 (c (n "tcrab") (v "0.1.0") (d (list (d (n "image") (r "^0.21.2") (f (quote ("png_codec"))) (k 2)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "tcrab_console") (r "^0.1.0") (d #t) (k 0)) (d (n "tcrab_console_gl") (r "^0.1.0") (d #t) (k 2)))) (h "15i1crd4ggmr6cqb376nj8s0bpbajkd1r11jjbim725xmjmhw2l2")))

(define-public crate-tcrab-0.2.0 (c (n "tcrab") (v "0.2.0") (d (list (d (n "image") (r "^0.21.2") (f (quote ("png_codec"))) (k 2)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "tcrab_console") (r "^0.2.0") (d #t) (k 0)) (d (n "tcrab_console_gl") (r "^0.2.0") (d #t) (k 2)))) (h "07wk1xgdivad1safm8ab2sgah0swa5a88lb5fgfnqr0v759i05lf")))

