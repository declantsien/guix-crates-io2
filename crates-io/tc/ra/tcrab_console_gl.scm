(define-module (crates-io tc ra tcrab_console_gl) #:use-module (crates-io))

(define-public crate-tcrab_console_gl-0.1.0 (c (n "tcrab_console_gl") (v "0.1.0") (d (list (d (n "gl") (r "^0.12.0") (d #t) (k 0)) (d (n "glutin") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tcrab_console") (r "^0.1.0") (d #t) (k 0)))) (h "05ibng61hsi2kkm5rbqc8nlmql9yzwkjdgaszw62v5wil5ykk6xg")))

(define-public crate-tcrab_console_gl-0.1.1 (c (n "tcrab_console_gl") (v "0.1.1") (d (list (d (n "gl") (r "^0.12.0") (d #t) (k 0)) (d (n "glutin") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tcrab_console") (r "^0.1.0") (d #t) (k 0)))) (h "14acv7xw1xyf1rfrw6xs4p42jdqqpj52jqcjn65x701m0zpxrqcl")))

(define-public crate-tcrab_console_gl-0.2.0 (c (n "tcrab_console_gl") (v "0.2.0") (d (list (d (n "gl") (r "^0.12.0") (d #t) (k 0)) (d (n "glutin") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tcrab_console") (r "^0.2.0") (d #t) (k 0)))) (h "12zgm4zbqlhwiwlz5qamihqz2z03gxniphwx6il7g5s5l25g6lg5")))

