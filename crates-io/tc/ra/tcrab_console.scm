(define-module (crates-io tc ra tcrab_console) #:use-module (crates-io))

(define-public crate-tcrab_console-0.1.0 (c (n "tcrab_console") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ra8570l035l5ddd8fagq341kcz5j78m1am21frs7v666cc4mndd") (f (quote (("serialize" "serde"))))))

(define-public crate-tcrab_console-0.2.0 (c (n "tcrab_console") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09y8mb8n3ybcd98m4y8w4y07kcshcp6ljybsriclqy2v2yg2zlzg") (f (quote (("serialize" "serde"))))))

