(define-module (crates-io tc a9 tca9555) #:use-module (crates-io))

(define-public crate-tca9555-0.1.0 (c (n "tca9555") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1ajg9grklrfsl0970jq399kdg4adzqkkzzj5nz7smfn8kknv7sb1") (f (quote (("unproven" "embedded-hal/unproven") ("default" "unproven")))) (s 2) (e (quote (("use_defmt" "dep:defmt"))))))

