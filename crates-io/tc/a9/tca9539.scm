(define-module (crates-io tc a9 tca9539) #:use-module (crates-io))

(define-public crate-tca9539-0.1.0 (c (n "tca9539") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (k 0)))) (h "0xiva3i32m5hyis8shkn8gjnxadyj7qja67biplpg07vb3hvl7cc")))

(define-public crate-tca9539-0.2.0 (c (n "tca9539") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (k 0)))) (h "0wd9r5px54jhb2766k5mmsqixmgnh98jm3yyq1j5ng57lc08ni85")))

(define-public crate-tca9539-0.2.1 (c (n "tca9539") (v "0.2.1") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (k 0)))) (h "0pdzvxp3097cv5lsqci8v7n4jrphwsla8h3a0mz7n0815m8ns7vv")))

