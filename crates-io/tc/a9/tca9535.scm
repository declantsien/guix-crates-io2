(define-module (crates-io tc a9 tca9535) #:use-module (crates-io))

(define-public crate-tca9535-0.1.0 (c (n "tca9535") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "15g28yk76c9vgygprisshwh9mrb5gn8slkg8m6n04xnjk3yfdv1j")))

