(define-module (crates-io tc a9 tca9548) #:use-module (crates-io))

(define-public crate-tca9548-0.1.0 (c (n "tca9548") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "02s7adl5147wghc66m8x74kmjvidrigr814flyfj16zylgpndbvg")))

