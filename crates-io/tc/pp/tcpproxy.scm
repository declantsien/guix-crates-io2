(define-module (crates-io tc pp tcpproxy) #:use-module (crates-io))

(define-public crate-tcpproxy-0.1.0 (c (n "tcpproxy") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1jwan8kw6p67n21cbql1kv2jhm7121np2da4y7sr06zaamppnjb9")))

(define-public crate-tcpproxy-0.3.0 (c (n "tcpproxy") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "1nsqzw2jxf0j5q41cln9jpnnfy149y51krj7378x9m082ghc66g7")))

(define-public crate-tcpproxy-0.3.1 (c (n "tcpproxy") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "0ad4ap3rv8yjryp9w4pigdi9zdvi6sms33nxxqb4girlxjpm52cl")))

(define-public crate-tcpproxy-0.4.0 (c (n "tcpproxy") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "134jx8r9s4zn0zpppwpg3f5af8mdck146gmqyfwswlrih1ng09i2")))

