(define-module (crates-io tc pp tcpp) #:use-module (crates-io))

(define-public crate-tcpp-0.1.0 (c (n "tcpp") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "0j2hm8gqpqn3ib6xcq4pgazvii6p204nhjrhrz3h9rwm6v4pjjmq")))

