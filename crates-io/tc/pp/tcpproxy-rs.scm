(define-module (crates-io tc pp tcpproxy-rs) #:use-module (crates-io))

(define-public crate-tcpproxy-rs-0.5.0 (c (n "tcpproxy-rs") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "06lvh2jm1dn2s5nsc1kfhw04svggg7prwy9dfrsmhp7230iw1qvf")))

(define-public crate-tcpproxy-rs-0.5.1 (c (n "tcpproxy-rs") (v "0.5.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "10qmdl89h7ia8ibv1s67j1ybn472myvfrhhiwdhb8b9rphlwhrsn")))

(define-public crate-tcpproxy-rs-0.5.2 (c (n "tcpproxy-rs") (v "0.5.2") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "1hw0jz9807an736dskb8alpjys5dzpi0qjk1jgz52gw7y0jm73cy")))

(define-public crate-tcpproxy-rs-0.6.0 (c (n "tcpproxy-rs") (v "0.6.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "1cx1771wpba5b2kz28q9ydvqnjdlphh1xrxg07nj373dp1lqvj2c")))

(define-public crate-tcpproxy-rs-0.7.0 (c (n "tcpproxy-rs") (v "0.7.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "0h4f7yndjs7a9syaijcbv49agxfhs8nic1wh6hpq421y8am9fc18")))

(define-public crate-tcpproxy-rs-0.7.1 (c (n "tcpproxy-rs") (v "0.7.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "0j7j1pdh8v1rv05bnmxl6hchid9kvfg1i49l4yz64vj2z9pih7lw")))

(define-public crate-tcpproxy-rs-0.7.2 (c (n "tcpproxy-rs") (v "0.7.2") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "1y0vqldz5llm64p081i9pr7lnraf5xq5606slxj9449wir0nr167")))

(define-public crate-tcpproxy-rs-0.7.3 (c (n "tcpproxy-rs") (v "0.7.3") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "03l86wqzbkiw6gdday5kbrx3g4jys07pkgwkffbizj2fwy0gkmlw")))

(define-public crate-tcpproxy-rs-0.7.4 (c (n "tcpproxy-rs") (v "0.7.4") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "1qw1i3s80f49z0xrfbzrmvxay6k2dprsfw8zfn4ddk073h60xh14")))

(define-public crate-tcpproxy-rs-0.7.5 (c (n "tcpproxy-rs") (v "0.7.5") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "1xzmyksgzy9f7pzfwysr6j676yc8qyhm0yl88j4c7vmsavaibdzj")))

