(define-module (crates-io tc li tcli) #:use-module (crates-io))

(define-public crate-tcli-0.1.0 (c (n "tcli") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "194535933mwvk7v4wyw7mw2jccjxccy8zq4rr9splrjgwhj8jbms")))

(define-public crate-tcli-0.1.1 (c (n "tcli") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1jdn70r5b7mj544i52k86n4z726hwqajqfl26m2v26wabwb5djl7")))

(define-public crate-tcli-0.1.2 (c (n "tcli") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "05f9jyph8a67j6v99ikvvw8nddvgzs79w419f8668dfa68lhkqqs")))

(define-public crate-tcli-0.1.3 (c (n "tcli") (v "0.1.3") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "18q92cqiqxidln9fnxmfixi220bk1z44q8n1pansj1pn8k1n7gam")))

(define-public crate-tcli-0.1.4 (c (n "tcli") (v "0.1.4") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0g1bl0qfz4kwd6xqaczjmkn22plp9r22gbqzr6cv6id0vk72sgfw")))

