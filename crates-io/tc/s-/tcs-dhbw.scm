(define-module (crates-io tc s- tcs-dhbw) #:use-module (crates-io))

(define-public crate-tcs-dhbw-0.1.0 (c (n "tcs-dhbw") (v "0.1.0") (h "04rj2zh14140hh2f0dawpyz3fx257yaaghrbr8sa21hr0c0wvsb0")))

(define-public crate-tcs-dhbw-0.1.1 (c (n "tcs-dhbw") (v "0.1.1") (h "0ibxw8am3i6qh2zhvjx4bl9l346kf0i81chhmzhih25m5nk65s7d")))

(define-public crate-tcs-dhbw-0.1.2 (c (n "tcs-dhbw") (v "0.1.2") (h "11af0fz508fnkjyd02ckpi6aidn5cx2w78bvdilf8k0y7wwcx7qm")))

(define-public crate-tcs-dhbw-0.1.3 (c (n "tcs-dhbw") (v "0.1.3") (h "1pvpsh9qv84187i75isvb7s6w4wdmqmyndzggs4zxqw9hz93hzd3")))

(define-public crate-tcs-dhbw-0.1.4 (c (n "tcs-dhbw") (v "0.1.4") (h "09yx0nyihljjd5hilkw66bziwnxkxrpqj417n94gyq282mwjs2yn")))

(define-public crate-tcs-dhbw-0.1.5 (c (n "tcs-dhbw") (v "0.1.5") (h "1iw5kpxkjy1bi6zsjdlkqamysb0gxfrhsrkcqvzc1nf61p4wmb7m")))

(define-public crate-tcs-dhbw-1.0.0 (c (n "tcs-dhbw") (v "1.0.0") (h "18p1vmzax4qk74rk53dy36j0cdzngzmh854360656mjy618jk3ff")))

