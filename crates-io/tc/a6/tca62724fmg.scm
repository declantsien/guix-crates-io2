(define-module (crates-io tc a6 tca62724fmg) #:use-module (crates-io))

(define-public crate-tca62724fmg-0.1.0 (c (n "tca62724fmg") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "10x07x6kw8rddr6xip9nz4w4nj3j93yv8vffysprixc0fj067im8")))

(define-public crate-tca62724fmg-0.1.1 (c (n "tca62724fmg") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.3") (d #t) (k 2)) (d (n "stm32h7") (r "^0.10.0") (f (quote ("stm32h743"))) (d #t) (k 2)) (d (n "stm32h7xx-hal") (r "^0.4.0") (f (quote ("stm32h743" "rt"))) (d #t) (k 2)))) (h "1h19gqb403yblalz6ssmhnvxdrlq6lyvpwa8a0vsfjg0gfxf2z79")))

(define-public crate-tca62724fmg-0.1.2 (c (n "tca62724fmg") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.3") (d #t) (k 2)) (d (n "stm32h7") (r "^0.10.0") (f (quote ("stm32h743"))) (d #t) (k 2)) (d (n "stm32h7xx-hal") (r "^0.4.0") (f (quote ("stm32h743" "rt"))) (d #t) (k 2)))) (h "06wf140zxryg47m74lh96jyr329d98kxw7bsw77nqmrdja71grxl")))

