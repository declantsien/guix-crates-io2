(define-module (crates-io tc -e tc-executor-wasmtime) #:use-module (crates-io))

(define-public crate-tc-executor-wasmtime-0.0.0 (c (n "tc-executor-wasmtime") (v "0.0.0") (h "1siddy2mv8gax9awfb2zb49gp0s83dlxjdbr1irlz16q5adk0s11") (y #t)))

(define-public crate-tc-executor-wasmtime-0.8.0 (c (n "tc-executor-wasmtime") (v "0.8.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "codec") (r "^2.0.1") (d #t) (k 0) (p "tetsy-scale-codec")) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0") (d #t) (k 0)) (d (n "tc-executor-common") (r "^0.8.0") (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 0)) (d (n "tetcore-wasm-interface") (r "^2.0.2") (d #t) (k 0)) (d (n "tetsy-wasm") (r "^0.41.0") (d #t) (k 0)) (d (n "tp-allocator") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime-interface") (r "^2.0.2") (d #t) (k 0)) (d (n "twasm-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "wasmtime") (r "^0.19") (d #t) (k 0)))) (h "1dyxk0pdh1s08g6xqclk5n3d6c6jd125q4rkv87j7g3qbw4br3nf")))

