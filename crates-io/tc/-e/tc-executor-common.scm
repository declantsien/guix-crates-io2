(define-module (crates-io tc -e tc-executor-common) #:use-module (crates-io))

(define-public crate-tc-executor-common-0.0.0 (c (n "tc-executor-common") (v "0.0.0") (h "0c1jswk72qy7n5rbcmf6msqx5w481nmir21m3w9aq19k5g2fvr8x") (y #t)))

(define-public crate-tc-executor-common-0.8.0 (c (n "tc-executor-common") (v "0.8.0") (d (list (d (n "codec") (r "^2.0.1") (d #t) (k 0) (p "tetsy-scale-codec")) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "serializer") (r "^2.0.2") (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 0)) (d (n "tetcore-wasm-interface") (r "^2.0.2") (d #t) (k 0)) (d (n "tetsy-wasm") (r "^0.41.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)) (d (n "tp-allocator") (r "^2.0.2") (d #t) (k 0)) (d (n "twasmi") (r "^0.6.2") (d #t) (k 0)))) (h "0m94abppwqmh7jpbnhrf038zg7679j7i68z4la0cwxm3jxdykjv9") (f (quote (("default"))))))

