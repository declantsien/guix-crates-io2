(define-module (crates-io tc l- tcl-sys) #:use-module (crates-io))

(define-public crate-tcl-sys-0.1.0 (c (n "tcl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0pnrqday9w1md8pgbnw2ccvscjdhvgq4l2n80m60i1im32204blf")))

(define-public crate-tcl-sys-0.2.0-alpha (c (n "tcl-sys") (v "0.2.0-alpha") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "bindgen") (r "^0.64.0") (f (quote ("runtime"))) (k 1)) (d (n "jobserver") (r "^0.1.26") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "15h6inwbln37zrbdb1s10mxsqxm4q7cnqhf5glr9mikwakd5x7w3") (l "tcl")))

(define-public crate-tcl-sys-0.2.0-alpha.1 (c (n "tcl-sys") (v "0.2.0-alpha.1") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "bindgen") (r "^0.64.0") (f (quote ("runtime"))) (k 1)) (d (n "jobserver") (r "^0.1.26") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "17cnwi16l1376j9p2qswywxk2gmgk5v2s5agw2dwbk1cg8cfifli") (l "tcl")))

(define-public crate-tcl-sys-0.2.0 (c (n "tcl-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "bindgen") (r "^0.64.0") (f (quote ("runtime"))) (k 1)) (d (n "jobserver") (r "^0.1.26") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1h3cfr6dcxxinwkffhqpzqz2y852ywwk55almfw8mizrw1pd8708") (l "tcl")))

