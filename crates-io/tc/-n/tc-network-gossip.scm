(define-module (crates-io tc -n tc-network-gossip) #:use-module (crates-io))

(define-public crate-tc-network-gossip-0.0.0 (c (n "tc-network-gossip") (v "0.0.0") (h "0s495vi8sggh98azcjzan3l212lpawisfw6hplb0bncvb0v71a0z") (y #t)))

(define-public crate-tc-network-gossip-0.8.0 (c (n "tc-network-gossip") (v "0.8.0") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 2)) (d (n "futures") (r "^0.3.9") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "lru") (r "^0.6.1") (d #t) (k 0)) (d (n "prometheus-endpoint") (r "^0.8.1") (d #t) (k 0) (p "prometheus-endpoint")) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "tc-network") (r "^0.8.0") (d #t) (k 0)) (d (n "tetsy-libp2p") (r "^0.34.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "0vc8gzi6z97bgm0b8nwx2mkj44c1l65ykf3bsygdx3wbvmbngig5")))

