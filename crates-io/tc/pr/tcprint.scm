(define-module (crates-io tc pr tcprint) #:use-module (crates-io))

(define-public crate-tcprint-0.0.0-dev.0 (c (n "tcprint") (v "0.0.0-dev.0") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0b2sg46pfc55r169b1d3ns7a5wmm4nijx8fmhn4cy696ycisprj4")))

(define-public crate-tcprint-0.0.0-dev.1 (c (n "tcprint") (v "0.0.0-dev.1") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "05h54dgbg4qkhri4nqp0l99k3ch37c28cmd7kz2mv7g9pxpv03p3")))

(define-public crate-tcprint-0.0.0-dev.2 (c (n "tcprint") (v "0.0.0-dev.2") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1bfi2kz3v0lf0msajjmwl4hg0pkkb05jrfazvbplc51mza64xj6i")))

(define-public crate-tcprint-0.0.0-dev.3 (c (n "tcprint") (v "0.0.0-dev.3") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1gi62w6ypfxv5wssxwdzrh8zvqaas8na1qc6ypy73yjdxkf8mjy2")))

(define-public crate-tcprint-0.1.5 (c (n "tcprint") (v "0.1.5") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0b2f33fcav6n1mcvph5bzm167zaihnblhhmac68fmkkfxk8zb47r")))

