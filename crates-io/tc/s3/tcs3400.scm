(define-module (crates-io tc s3 tcs3400) #:use-module (crates-io))

(define-public crate-tcs3400-0.1.0 (c (n "tcs3400") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1dlw6n3nx5cmri99y4ynwqc5dms4dcdd26cxvf1f1l8yg7jdv8xv")))

