(define-module (crates-io tc s3 tcs3472) #:use-module (crates-io))

(define-public crate-tcs3472-0.1.0 (c (n "tcs3472") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0w4i24461xw1qjzkblpf17cgsn74dnc73yba6h79qrjdmpzdvdgi")))

(define-public crate-tcs3472-0.1.1 (c (n "tcs3472") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "15wh3wn47r22yqayl7m3xg7kvnrijsjkr95s2dvyyh003dkhbizl")))

(define-public crate-tcs3472-0.2.0 (c (n "tcs3472") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0kgc9n1l9qb1f2xm1i7z9bdv857c0pk4wx5vfp9mmjkz8sq69dbz")))

