(define-module (crates-io tc _c tc_cli_client) #:use-module (crates-io))

(define-public crate-tc_cli_client-0.1.0 (c (n "tc_cli_client") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tc_core") (r "^0.1") (d #t) (k 0)))) (h "0y6fqrljp0kvvzjzq06y8nwa1drd7n5gsmjy7cs1x3xdb4iblw1w")))

(define-public crate-tc_cli_client-0.1.1 (c (n "tc_cli_client") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6") (d #t) (k 2)) (d (n "tc_core") (r "^0.2") (d #t) (k 0)))) (h "05gmi0232afj2rzqmkw6avlqsfwqb9s6d33pgyzskmdb8424ac74")))

(define-public crate-tc_cli_client-0.2.0 (c (n "tc_cli_client") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6") (d #t) (k 2)) (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "11rby3076zw1lm670lbv0nciywhwlpiqh6kxbprq7bfwvijsq67l")))

(define-public crate-tc_cli_client-0.2.1 (c (n "tc_cli_client") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "spectral") (r "^0.6") (d #t) (k 2)) (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "070r500rkrz10lhppaf98s7a8zd6igqvv0iin2zgc904qs2c1rj0")))

