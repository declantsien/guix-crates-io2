(define-module (crates-io tc _c tc_coblox_bitcoincore) #:use-module (crates-io))

(define-public crate-tc_coblox_bitcoincore-0.1.0 (c (n "tc_coblox_bitcoincore") (v "0.1.0") (d (list (d (n "bitcoin_rpc_client") (r "^0.1.0") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)) (d (n "testcontainers") (r "^0.1.0") (d #t) (k 0)))) (h "01k0n6svjnp1wgawgpakdpl00vh2bbg3b4fvwmnfdxkyqykc58nw")))

(define-public crate-tc_coblox_bitcoincore-0.1.1 (c (n "tc_coblox_bitcoincore") (v "0.1.1") (d (list (d (n "bitcoin_rpc_client") (r "^0.1.0") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)) (d (n "testcontainers") (r "^0.2.0") (d #t) (k 0)))) (h "0qhbdprpkyi974vcm54ljvaa79iicryb2agrjzb2rkfd44g8dlkk") (y #t)))

(define-public crate-tc_coblox_bitcoincore-0.2.0 (c (n "tc_coblox_bitcoincore") (v "0.2.0") (d (list (d (n "bitcoin_rpc_client") (r "^0.1.0") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)) (d (n "testcontainers") (r "^0.2.0") (d #t) (k 0)))) (h "0nhz7vp5m9kwvvmchxpj90j4hsvb5bc66jp1nvnkz1h6imnkkk0p")))

(define-public crate-tc_coblox_bitcoincore-0.3.0 (c (n "tc_coblox_bitcoincore") (v "0.3.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "hmac") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)) (d (n "tc_core") (r "^0.1") (d #t) (k 0)))) (h "033lri839apkx3p9w0vy3kjlp1d54awgx53zfl2va66pym01ksjz")))

(define-public crate-tc_coblox_bitcoincore-0.3.1 (c (n "tc_coblox_bitcoincore") (v "0.3.1") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "hmac") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)) (d (n "tc_core") (r "^0.2") (d #t) (k 0)))) (h "1fwbsfkb1vznn5rn2qdzjqzl36rflklzfa9b9z2gpl46mkmjbsx1")))

(define-public crate-tc_coblox_bitcoincore-0.3.2 (c (n "tc_coblox_bitcoincore") (v "0.3.2") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "tc_core") (r "^0.2") (d #t) (k 0)))) (h "0sf154dbngl4h4zsn5irmajafnjh7h22dilwg39i1vcblyrxgycj") (y #t)))

(define-public crate-tc_coblox_bitcoincore-0.4.0 (c (n "tc_coblox_bitcoincore") (v "0.4.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "tc_core") (r "^0.2") (d #t) (k 0)))) (h "0bnw02a37iz3pn2md08wh8lxi7xyn9lzb9ccpx4d36cs9ykrybxk")))

(define-public crate-tc_coblox_bitcoincore-0.5.0 (c (n "tc_coblox_bitcoincore") (v "0.5.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "01nd5sc24chjjng2dy9ls4x52hm6wp5r1wyfc3w7y9951p54hyqg")))

(define-public crate-tc_coblox_bitcoincore-0.5.1 (c (n "tc_coblox_bitcoincore") (v "0.5.1") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "1xrrh3c5vic77qmcib4b6f8mgrzxag4pk3m97cv962zz9q113l7w")))

