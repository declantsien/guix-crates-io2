(define-module (crates-io tc ma tcmalloc) #:use-module (crates-io))

(define-public crate-tcmalloc-0.1.0 (c (n "tcmalloc") (v "0.1.0") (h "0iid4bay05zg5ijsjv9k9w7pk0bcfwp65fijx4n8lc6dbcn91hdh")))

(define-public crate-tcmalloc-0.1.1 (c (n "tcmalloc") (v "0.1.1") (h "0hnl9djjkggfxzh78h065040ywqkwpsh0bm20p9dp94cs1p8cc8l")))

(define-public crate-tcmalloc-0.1.2 (c (n "tcmalloc") (v "0.1.2") (h "1bfsfjg83qh6xmb4bim8rr4bz60csvhbizc3w5c063rx4wna3pqd")))

(define-public crate-tcmalloc-0.1.3 (c (n "tcmalloc") (v "0.1.3") (h "097sy977a75dx9h79lrvx1x4dr4sz8ablqxmw6cb6cr681vw6znk")))

(define-public crate-tcmalloc-0.1.4 (c (n "tcmalloc") (v "0.1.4") (h "0k7vqm6lcfxjmd0dy481ybxvdm9fdi1908b29db927jxqqgbv1c3")))

(define-public crate-tcmalloc-0.1.5 (c (n "tcmalloc") (v "0.1.5") (h "1i66hj2rw7lq5z85px5chvy159y0p0v82x3wwjkph6lg7ypv8spz")))

(define-public crate-tcmalloc-0.2.0 (c (n "tcmalloc") (v "0.2.0") (h "0ch2awcg725r22bb8nb5g5x1pxch1hc3nk7svzpnv0dw2ibyp487")))

(define-public crate-tcmalloc-0.3.0 (c (n "tcmalloc") (v "0.3.0") (d (list (d (n "tcmalloc-sys") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "16w1kpfzv093mljk9k2skldbwhcfw06amgk7xppcb8c47l8halip") (f (quote (("default") ("bundled" "tcmalloc-sys"))))))

