(define-module (crates-io tc ma tcmalloc2) #:use-module (crates-io))

(define-public crate-tcmalloc2-0.1.0+2.10 (c (n "tcmalloc2") (v "0.1.0+2.10") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "num_cpus") (r "^1.15") (d #t) (k 1)))) (h "19y7pyrxy1j0vxx5bmn7vsbnx0m6axsg3spi05lwa8hnzx4lm48g")))

(define-public crate-tcmalloc2-0.1.1+2.10 (c (n "tcmalloc2") (v "0.1.1+2.10") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "num_cpus") (r "^1.15") (d #t) (k 1)))) (h "0l1q1zwnn17vcjamcpay13ppf2vgcgbnbnnikf5jz5fasdkh7mwp")))

(define-public crate-tcmalloc2-0.1.2+2.13 (c (n "tcmalloc2") (v "0.1.2+2.13") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "num_cpus") (r "^1") (d #t) (k 1)))) (h "1984kzd70ydmy12h57jnnqdzqwfnj6qjka2vbvh3m74n6vza9yas")))

(define-public crate-tcmalloc2-0.2.0+2.15 (c (n "tcmalloc2") (v "0.2.0+2.15") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "num_cpus") (r "^1") (d #t) (k 1)))) (h "10qsf5nnd10gdvqh4n9nlmsafdcv0z85sbxy07kfzgz9bhldhlgy")))

