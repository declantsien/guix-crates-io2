(define-module (crates-io tc _p tc_parity_parity) #:use-module (crates-io))

(define-public crate-tc_parity_parity-0.1.0 (c (n "tc_parity_parity") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "testcontainers") (r "^0.2.0") (d #t) (k 0)) (d (n "web3") (r "^0.4") (d #t) (k 2)))) (h "1ld7i9b710mwhj90mh04wdfrla6v4wrb37x4rj6bw5w9y5rj6zkd")))

(define-public crate-tc_parity_parity-0.2.0 (c (n "tc_parity_parity") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tc_core") (r "^0.1") (d #t) (k 0)) (d (n "web3") (r "^0.4") (d #t) (k 2)))) (h "1y931s10s9gd6cgfbxzldir7fic09fh9wly1739pijl0ffws9dw2")))

(define-public crate-tc_parity_parity-0.2.1 (c (n "tc_parity_parity") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tc_core") (r "^0.2") (d #t) (k 0)) (d (n "web3") (r "^0.4") (d #t) (k 2)))) (h "0gxb96a43rkq962f3wfzrwpd5gss1fawnbvpnn0zrhl8cvyrfyfn")))

(define-public crate-tc_parity_parity-0.3.0 (c (n "tc_parity_parity") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tc_core") (r "^0.2") (d #t) (k 0)) (d (n "web3") (r "^0.4") (d #t) (k 2)))) (h "1qqkfk13j07hd0x9fgzzwlazvzpm64xbgs85g2dqm16m8p87q2r2")))

(define-public crate-tc_parity_parity-0.4.0 (c (n "tc_parity_parity") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tc_core") (r "^0.3") (d #t) (k 0)) (d (n "web3") (r "^0.5") (d #t) (k 2)))) (h "1j0zlxlp2g3qi07kry2r0ywpb6avjq9fnq360wj4fpjijrjj4dcq")))

(define-public crate-tc_parity_parity-0.5.0 (c (n "tc_parity_parity") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tc_core") (r "^0.3") (d #t) (k 0)) (d (n "web3") (r "^0.6") (d #t) (k 2)))) (h "0kp0ga19z453p55xcavfffxpf62dxpgf6bz4is33rc3whyallmw5")))

(define-public crate-tc_parity_parity-0.5.1 (c (n "tc_parity_parity") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tc_core") (r "^0.3") (d #t) (k 0)) (d (n "web3") (r "^0.8") (d #t) (k 2)))) (h "09w4dqhlg5fgj6c2q5m31ma0lnybr1cg2gvis75z17malg62d68n")))

