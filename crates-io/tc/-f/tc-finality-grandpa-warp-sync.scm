(define-module (crates-io tc -f tc-finality-grandpa-warp-sync) #:use-module (crates-io))

(define-public crate-tc-finality-grandpa-warp-sync-0.0.0 (c (n "tc-finality-grandpa-warp-sync") (v "0.0.0") (h "03zldkfd4l4kl09m0107hcgsrcvsdsfsbv2yf6p0xkx7lllb0c1x") (y #t)))

(define-public crate-tc-finality-grandpa-warp-sync-0.8.0 (c (n "tc-finality-grandpa-warp-sync") (v "0.8.0") (d (list (d (n "codec") (r "^2.0.1") (d #t) (k 0) (p "tetsy-scale-codec")) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tc-client-api") (r "^2.0.0") (d #t) (k 0)) (d (n "tc-finality-grandpa") (r "^0.8.0") (d #t) (k 0)) (d (n "tc-network") (r "^0.8.0") (d #t) (k 0)) (d (n "tc-service") (r "^0.8.0") (d #t) (k 0)) (d (n "tp-blockchain") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)))) (h "0vvbwmh8qfwllb6bidshmf5psgf1iibkz3a3q29fcfm2mly25lil")))

