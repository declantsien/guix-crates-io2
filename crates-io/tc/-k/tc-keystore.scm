(define-module (crates-io tc -k tc-keystore) #:use-module (crates-io))

(define-public crate-tc-keystore-0.0.0 (c (n "tc-keystore") (v "0.0.0") (h "0lazbq4d8ckcq8ldxjnlpv5byf2dyg8y6aklns6zr9hx4h0w0p79") (y #t)))

(define-public crate-tc-keystore-2.0.0 (c (n "tc-keystore") (v "2.0.0") (d (list (d (n "async-trait") (r "^0.1.30") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "merlin") (r "^2.0") (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "subtle") (r "^2.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tet-application-crypto") (r "^2.0.2") (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-keystore") (r "^0.8.1") (d #t) (k 0)))) (h "025hqzsz5i1dcazsg1ixxilrvqbzr8xbibzrsyq49niln8rw8b5n")))

