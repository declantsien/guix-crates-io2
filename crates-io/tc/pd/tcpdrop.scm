(define-module (crates-io tc pd tcpdrop) #:use-module (crates-io))

(define-public crate-tcpdrop-0.1.0 (c (n "tcpdrop") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.0") (f (quote ("extern_crate_std"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_NetworkManagement_IpHelper"))) (d #t) (k 0)))) (h "1zsp0y9hpj89kdanlprwmx5q71wwpjh303nzhiv059yg168j42c7")))

