(define-module (crates-io tc -s tc-sync-state-rpc) #:use-module (crates-io))

(define-public crate-tc-sync-state-rpc-0.0.0 (c (n "tc-sync-state-rpc") (v "0.0.0") (h "0a7w5sw55ic21xh4bsjvl9sacp6r64ica83idapb6h0bvb2d9a6m") (y #t)))

(define-public crate-tc-sync-state-rpc-0.8.0 (c (n "tc-sync-state-rpc") (v "0.8.0") (d (list (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "tc-chain-spec") (r "^2.0.0") (d #t) (k 0)) (d (n "tc-client-api") (r "^2.0.0") (d #t) (k 0)) (d (n "tc-consensus-babe") (r "^0.8.0") (d #t) (k 0)) (d (n "tc-consensus-epochs") (r "^0.8.0") (d #t) (k 0)) (d (n "tc-finality-grandpa") (r "^0.8.0") (d #t) (k 0)) (d (n "tc-rpc-api") (r "^0.8.0") (d #t) (k 0)) (d (n "tetsy-jsonrpc-core") (r "^15.0") (d #t) (k 0)) (d (n "tetsy-jsonrpc-core-client") (r "^15.0") (d #t) (k 0)) (d (n "tetsy-jsonrpc-derive") (r "^15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)) (d (n "tp-blockchain") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)))) (h "1gp9mim120jl5cyca7nxr3dg1ak0p4iwqpvxyflkygby9l5w9b60")))

