(define-module (crates-io tc _r tc_redis) #:use-module (crates-io))

(define-public crate-tc_redis-0.1.0 (c (n "tc_redis") (v "0.1.0") (d (list (d (n "tc_core") (r "^0.2") (d #t) (k 0)))) (h "1iiipz4hvi47y525w1mw5n1hxp62zvx72yk1zx852ysknsdrwxl3")))

(define-public crate-tc_redis-0.2.0 (c (n "tc_redis") (v "0.2.0") (d (list (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "148ccxln94z6nb0y2l79fnjk903l0xdwgqcsj3gdpp6dai4vbblh")))

(define-public crate-tc_redis-0.2.1 (c (n "tc_redis") (v "0.2.1") (d (list (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "1lgndnsi8iql9valrsf3382v82y06ksr761r68hbblw9536qndil")))

