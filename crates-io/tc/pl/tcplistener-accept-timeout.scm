(define-module (crates-io tc pl tcplistener-accept-timeout) #:use-module (crates-io))

(define-public crate-tcplistener-accept-timeout-0.0.0 (c (n "tcplistener-accept-timeout") (v "0.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0zwlbvdhkr2sxfmhv3bywn2qsrnpbmg0a7y1az5f76q9mkw75p5b")))

