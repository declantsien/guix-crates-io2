(define-module (crates-io tc pl tcplinecombine) #:use-module (crates-io))

(define-public crate-tcplinecombine-0.1.0 (c (n "tcplinecombine") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "async-compression") (r "^0.3.15") (f (quote ("tokio" "zstd"))) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "net" "rt" "fs" "io-util" "time" "sync"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.4") (f (quote ("codec"))) (d #t) (k 0)) (d (n "xflags") (r "^0.3.1") (d #t) (k 0)))) (h "1l674ixvyfkac6j8h4svxzgq8sbcvbn5k6zk90l1i1qifgx2a5wa")))

