(define-module (crates-io tc pl tcplscan) #:use-module (crates-io))

(define-public crate-tcplscan-0.1.0 (c (n "tcplscan") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1sdw4rpdp60mx17gng9x4361k2a08gs3y0rvsd70h4vcdk13q92j")))

(define-public crate-tcplscan-0.1.1 (c (n "tcplscan") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "08ny1wnyg2d7nw3v8mkdij2mqk51l820ij62jf5b5n4fcrndfw4n")))

(define-public crate-tcplscan-0.1.2 (c (n "tcplscan") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "09d3s7yg7lnybmv4mph1visqr64ay1p8n9q177sfihd4dr52bsh6")))

(define-public crate-tcplscan-0.1.3 (c (n "tcplscan") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zv9rd1yv0pn57awb1bj9cyk0c4awsmvyi7fyb3mcrz47vjxm565")))

(define-public crate-tcplscan-0.1.4 (c (n "tcplscan") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1nsb5j9hh5bbqsj53bpn911rj39riqj7j4528x0gy0fmmcgr0p02")))

(define-public crate-tcplscan-0.1.5 (c (n "tcplscan") (v "0.1.5") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0r442ija1mnagmrh8fb16jg6a8yw583af3imlfsmsc4n7a4llj03")))

