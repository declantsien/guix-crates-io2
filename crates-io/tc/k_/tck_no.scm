(define-module (crates-io tc k_ tck_no) #:use-module (crates-io))

(define-public crate-tck_no-1.0.0 (c (n "tck_no") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0xbl4s50ifpfcx0mijg4hqcfnf3006jn7nchq3m9fnjl2fngbz09")))

(define-public crate-tck_no-1.0.1 (c (n "tck_no") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1k71nywik8w067zjlgrg56v1qibws0yd8pwxf8m0flknk9p3pjwm")))

(define-public crate-tck_no-1.0.2 (c (n "tck_no") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "14dz2b627vik9fw49p4dk1q9ls9j19pqvjkqqdj55567n15s1i2a")))

