(define-module (crates-io tc kn tckn_utils) #:use-module (crates-io))

(define-public crate-tckn_utils-0.1.0 (c (n "tckn_utils") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "079j6csiadg0lv55rw75gil4126r4bsbvybkf3p3sk0nmqg6skdx")))

