(define-module (crates-io tc #{-i}# tc-informant) #:use-module (crates-io))

(define-public crate-tc-informant-0.0.0 (c (n "tc-informant") (v "0.0.0") (h "1jfci7gl77y6ppiynx8z8pfvm8px8nxx1v11a7dv0k6y02h1mncf") (y #t)))

(define-public crate-tc-informant-0.8.0 (c (n "tc-informant") (v "0.8.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tc-client-api") (r "^2.0.0") (d #t) (k 0)) (d (n "tc-network") (r "^0.8.0") (d #t) (k 0)) (d (n "tetcore-utils") (r "^2.0.2") (d #t) (k 0)) (d (n "tetsy-util-mem") (r "^0.9.0") (f (quote ("tetsy-primitive-types"))) (k 0)) (d (n "tp-blockchain") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-transaction-pool") (r "^2.0.2") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "1vqhqb9z7vf81mqmldwmm5x8q14dic0s5yizyzlqlyls6v9d46h5")))

