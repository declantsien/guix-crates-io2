(define-module (crates-io tc _k tc_kimlik) #:use-module (crates-io))

(define-public crate-tc_kimlik-0.1.0 (c (n "tc_kimlik") (v "0.1.0") (h "0m4mcf0mwxz108f7ppdbw2rxhm9w5jqpfkqmmf25wqxqbsb3j9jk")))

(define-public crate-tc_kimlik-0.2.0 (c (n "tc_kimlik") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1v2rdc93b3jfayh90mvgxirxm19cw23yrdgrikc9w9mqri3hp4lm")))

