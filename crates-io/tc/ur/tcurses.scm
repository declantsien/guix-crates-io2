(define-module (crates-io tc ur tcurses) #:use-module (crates-io))

(define-public crate-tcurses-0.1.0 (c (n "tcurses") (v "0.1.0") (h "1y7j276ll96460dkpa2hmk661v71mx10x0gkgzid12ba2pkx4ll8")))

(define-public crate-tcurses-0.1.1 (c (n "tcurses") (v "0.1.1") (h "1yxxgavvgnnkri37c4pr95vhyh6d4i2cjmjffgb5xrzhfxk4zcjq")))

(define-public crate-tcurses-0.1.2 (c (n "tcurses") (v "0.1.2") (h "0i5fzbarbqc4nq0iv8dkk5fa3jmc9iq268vl771f53lfzgcqg7xb")))

