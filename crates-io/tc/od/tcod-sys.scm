(define-module (crates-io tc od tcod-sys) #:use-module (crates-io))

(define-public crate-tcod-sys-1.5.2 (c (n "tcod-sys") (v "1.5.2") (h "18913nbjbmn59y1pdppacz9jjnina7av6pphcrs0cs487jp5hl6x")))

(define-public crate-tcod-sys-2.0.0 (c (n "tcod-sys") (v "2.0.0") (h "1q58nm8qi6s777pi3x544gx7p63mr6qbxinzv5y3sc08xw7vxppm")))

(define-public crate-tcod-sys-2.0.1 (c (n "tcod-sys") (v "2.0.1") (h "1ffm1vcsaf7c735777jc55xj0irbm2rvn4zy3z8hvy8d4zr02q0w")))

(define-public crate-tcod-sys-2.0.2 (c (n "tcod-sys") (v "2.0.2") (h "076dccrazp2b30qd7f2gfd4jfg3f1bnr8nxklzbrcw5wiil0y1vy")))

(define-public crate-tcod-sys-2.0.3 (c (n "tcod-sys") (v "2.0.3") (h "0l9y0gdxf4pxxcyck0w5pw07a7p1pkazjas6h3v0a7453m572mbn")))

(define-public crate-tcod-sys-2.0.4 (c (n "tcod-sys") (v "2.0.4") (h "0d91vq52naq11sjx3xfv5in7mnb0l894x1ajrmizyzpb1zqd3hk9")))

(define-public crate-tcod-sys-2.0.5 (c (n "tcod-sys") (v "2.0.5") (h "008czbnrl8bs3ivghwnd7dkkghw4q3z1kaw72n7ldr35lpbs3xic")))

(define-public crate-tcod-sys-2.0.6 (c (n "tcod-sys") (v "2.0.6") (d (list (d (n "libc") (r "^0.1.2") (d #t) (k 0)))) (h "0vmasdscpv39xnv2xwkaa415rhxmgpvz25daljm3j2cb4i03358i")))

(define-public crate-tcod-sys-2.0.7 (c (n "tcod-sys") (v "2.0.7") (d (list (d (n "libc") (r "^0.1.2") (d #t) (k 0)))) (h "0cvw2bdlbfhxz5sqz3xlps7xjyrzh31zhf49l28api2br1ymlygd")))

(define-public crate-tcod-sys-2.0.8 (c (n "tcod-sys") (v "2.0.8") (d (list (d (n "libc") (r "^0.1.2") (d #t) (k 0)))) (h "0kh85v6g8pqgyg79n6519i42awz1nyhcyfr3sl10clcp06d27s1w")))

(define-public crate-tcod-sys-2.0.9 (c (n "tcod-sys") (v "2.0.9") (d (list (d (n "libc") (r "^0.1.2") (d #t) (k 0)))) (h "103pi8wa8vkk0ni4w36d6nhfrrivjxmqgx431k66axh1spxlkhj2")))

(define-public crate-tcod-sys-2.0.10 (c (n "tcod-sys") (v "2.0.10") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)))) (h "055zprx06x74ky08m2p6ibg6379iww16pyh4s1i9zfc7dcq7nryz")))

(define-public crate-tcod-sys-2.0.11 (c (n "tcod-sys") (v "2.0.11") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)))) (h "0hl5ayqm6w0k631f21s5agc68x2ykx1vp5p3jji64gz6n9y9fz5r")))

(define-public crate-tcod-sys-2.0.12 (c (n "tcod-sys") (v "2.0.12") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)))) (h "0yjpzil6098sxvdv91yhmg89s7cwhanmr052s2i5wrxmal0axqjl")))

(define-public crate-tcod-sys-3.0.0 (c (n "tcod-sys") (v "3.0.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "01fb7a6kiy06sksisr49mzmb64r3bb965rsmc2lggpyvy7vkzcds")))

(define-public crate-tcod-sys-3.0.1 (c (n "tcod-sys") (v "3.0.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1qaf62pn4bbs541b7dm581mc7nk0fl38nprpfycjm379b4591wxm")))

(define-public crate-tcod-sys-3.0.2 (c (n "tcod-sys") (v "3.0.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1ajjlfs96zbikypsmrhx36v60snnglxggw3bxb82xpywy1kcv9rn")))

(define-public crate-tcod-sys-4.0.0 (c (n "tcod-sys") (v "4.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "15y32ym7cs8d6x751mhpnyy69ilffh1ldrywy52d5s5frdxcm4jl")))

(define-public crate-tcod-sys-4.0.1 (c (n "tcod-sys") (v "4.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11iigzjsakmyfmdy8zzh56hsclsq28675ncmwym86df3nakr2jsp")))

(define-public crate-tcod-sys-4.1.0 (c (n "tcod-sys") (v "4.1.0") (d (list (d (n "gcc") (r "^0.3.53") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0mi4vc4091dl959n0bj8ca8sfl5n41ksghng0k19s5426z3fdjcm")))

(define-public crate-tcod-sys-5.0.0 (c (n "tcod-sys") (v "5.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "00a5wz5bsdy1kipfhpifsacziq1dgd4wsw77f92ppwk9fnmfqmjg") (f (quote (("dynlib")))) (l "tcod")))

(define-public crate-tcod-sys-5.0.1 (c (n "tcod-sys") (v "5.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1apllm7vnfhrjjswrviw2wv0hzr7f2pc6qbsmrhcixv149yv43vk") (f (quote (("dynlib")))) (l "tcod")))

