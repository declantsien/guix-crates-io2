(define-module (crates-io tc od tcod_window) #:use-module (crates-io))

(define-public crate-tcod_window-0.0.1 (c (n "tcod_window") (v "0.0.1") (d (list (d (n "pistoncore-input") (r ">= 0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r ">= 0.0.5") (d #t) (k 0)) (d (n "quack") (r ">= 0.0.8") (d #t) (k 0)) (d (n "tcod") (r ">= 0.4.6") (d #t) (k 0)))) (h "13dk2b2qa6g6h20dby8r0yp4v8cpfzsf9l2m3bdda7llvc657ks1")))

(define-public crate-tcod_window-0.0.3 (c (n "tcod_window") (v "0.0.3") (d (list (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.12") (d #t) (k 0)) (d (n "quack") (r "^0.0.13") (d #t) (k 0)) (d (n "tcod") (r "^0.5.0") (d #t) (k 0)))) (h "0qgs50l0hck21ca19a35zh2k9csmy1lshzmi28fx6zwpd3jci9pr")))

(define-public crate-tcod_window-0.1.0 (c (n "tcod_window") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.63") (o #t) (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.10.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.17.0") (d #t) (k 0)) (d (n "tcod") (r "^0.9.0") (d #t) (k 0)))) (h "1z3231an0ms8zn9dgwgpxm9d6100rf8gf7xlsnlm2q012xrwk1bc") (f (quote (("nightly-testing" "clippy"))))))

(define-public crate-tcod_window-0.2.0 (c (n "tcod_window") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.63") (o #t) (d #t) (k 0)) (d (n "piston") (r "^0.20.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.10.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.17.0") (d #t) (k 0)) (d (n "tcod") (r "^0.9.0") (d #t) (k 0)))) (h "1mwrs1yf6b7kq1hszl786abbk1sk9bilf4k379isj5nscpwn3z59") (f (quote (("nightly-testing" "clippy"))))))

