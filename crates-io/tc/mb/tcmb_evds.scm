(define-module (crates-io tc mb tcmb_evds) #:use-module (crates-io))

(define-public crate-tcmb_evds-0.1.0 (c (n "tcmb_evds") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.38") (d #t) (k 0)))) (h "1xs871xbwd1q1wwrc07a9mddkaa8g6dxzwagqrqs0qfy8i4c10av") (f (quote (("sync_mode") ("default" "async_mode") ("async_mode"))))))

