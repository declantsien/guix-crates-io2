(define-module (crates-io tc _s tc_save_monger) #:use-module (crates-io))

(define-public crate-tc_save_monger-0.1.0 (c (n "tc_save_monger") (v "0.1.0") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "1kmxx3rmmgzzl43k0izs908y9yg0n454nr6vhbrlxjh2hzdxxzlg") (y #t)))

(define-public crate-tc_save_monger-0.1.1 (c (n "tc_save_monger") (v "0.1.1") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "0y5jzz53kcvblp9m77nxyxl8kgnhkn29a3fdixzkgfdv2yqjrxpv") (y #t)))

(define-public crate-tc_save_monger-0.2.0 (c (n "tc_save_monger") (v "0.2.0") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "0f76dizkj9lr53nc224pyjvjvy6wx6ms9qis63ncbgn7ip0a51bp") (y #t)))

(define-public crate-tc_save_monger-0.2.1 (c (n "tc_save_monger") (v "0.2.1") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "0y2l5dzcayknl5wnwyhyiws5szhc5vy4g1j38w0dq46raf43i6jp") (y #t)))

(define-public crate-tc_save_monger-0.2.2 (c (n "tc_save_monger") (v "0.2.2") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "169xh2cjlmkw27zwifgm8cp8v28glzh7cwwq9hfixsi8vfvzyv50") (y #t)))

(define-public crate-tc_save_monger-0.3.0 (c (n "tc_save_monger") (v "0.3.0") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "172x8vnw27cgj1vcgkjrnq3bqqyqz3marxnh2s4xx8ihjkx5lha8")))

(define-public crate-tc_save_monger-0.4.0 (c (n "tc_save_monger") (v "0.4.0") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "1bn2k07rrgai4m9iqb9lac1x3d6g6hnz5vhsny8pbli8d8cbg16i") (y #t)))

(define-public crate-tc_save_monger-0.4.1 (c (n "tc_save_monger") (v "0.4.1") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "00ljyarb415jqfjcjs85c6q2qqgw9cpn7hrc9a9jmj7k30c5zzrw") (y #t)))

(define-public crate-tc_save_monger-0.4.2 (c (n "tc_save_monger") (v "0.4.2") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "1kqcbjx1i3dv1kfvjf09isx94c0i626aqc4zl23bxv0y9z7bzrk4") (y #t)))

(define-public crate-tc_save_monger-0.4.3 (c (n "tc_save_monger") (v "0.4.3") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "14rqk2vs77w5ydi4723a110jd5jjbmzy3cyvkf7lp37x724g7fvq")))

(define-public crate-tc_save_monger-0.4.4 (c (n "tc_save_monger") (v "0.4.4") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "0fac4hsslfgwmhv86i00hbhjn0k0v8b77bn66cx9gcwxnykwybrg")))

(define-public crate-tc_save_monger-0.4.5 (c (n "tc_save_monger") (v "0.4.5") (d (list (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "0swl0xr7cmdj02qxgp162r5vspr693rrn5hxgfz8k63gf3hzfdb1")))

