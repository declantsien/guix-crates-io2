(define-module (crates-io tc fs tcfsmftttcs) #:use-module (crates-io))

(define-public crate-TCFSMFTTTCS-0.1.0 (c (n "TCFSMFTTTCS") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)))) (h "1n6gfy9zqqxf8fwwha5rc3gq6v1b3695nvcgzlm3gql8flxff1g9")))

(define-public crate-TCFSMFTTTCS-0.1.1 (c (n "TCFSMFTTTCS") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)))) (h "0bvhnsyf2bskl6qd1pcv2bjmcsc6kh0cgwy9lrcg57s10vd2646k")))

(define-public crate-TCFSMFTTTCS-0.1.2 (c (n "TCFSMFTTTCS") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)))) (h "1agyp7yijv30a2868m49967m70f6cj932p1gjn5qhddcnq9ad252")))

(define-public crate-TCFSMFTTTCS-0.1.3 (c (n "TCFSMFTTTCS") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)))) (h "1zwdpbgsgkc5i7b90kg0vgk1i5ngyi14m5wx6lpz4qxi7dg2x98d")))

