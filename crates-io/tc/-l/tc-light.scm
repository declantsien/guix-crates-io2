(define-module (crates-io tc -l tc-light) #:use-module (crates-io))

(define-public crate-tc-light-0.0.0 (c (n "tc-light") (v "0.0.0") (h "1g6i9h09zsg8jh4cl89b365wl7r3bk08h6k5brrg3xkaxd8qzdhv") (y #t)))

(define-public crate-tc-light-2.0.0 (c (n "tc-light") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (d #t) (k 0) (p "tetsy-scale-codec")) (d (n "externalities") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tc-client-api") (r "^2.0.0") (d #t) (k 0)) (d (n "tc-executor") (r "^0.8.0") (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 0)) (d (n "tetsy-hash-db") (r "^0.15.2") (d #t) (k 0)) (d (n "tp-api") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-blockchain") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-state-machine") (r "^0.8.2") (d #t) (k 0)))) (h "1f99bhvl3ygn92fifb98p9fqki3qhnmazg4gw373hdsi7f671jfq") (f (quote (("default"))))))

