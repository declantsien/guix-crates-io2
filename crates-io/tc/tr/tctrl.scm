(define-module (crates-io tc tr tctrl) #:use-module (crates-io))

(define-public crate-tctrl-0.1.0 (c (n "tctrl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "13yiygf3ca6s8m14xxd47z9f7ib485r59jzyg38bnw4i1y3spv8a")))

(define-public crate-tctrl-0.2.0 (c (n "tctrl") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.3.1") (f (quote ("tmux_3_3a"))) (d #t) (k 0)))) (h "1l30aw70cx5gx8xaqbi0gcf4f3v86bpjxfz41qgyiknkcddm86cm")))

(define-public crate-tctrl-0.2.1 (c (n "tctrl") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.3.1") (f (quote ("tmux_3_3a"))) (d #t) (k 0)))) (h "166kks9ijhq1qp633xq4zinr9d9b1fh5y2qd49bisib8hfmzps7c")))

(define-public crate-tctrl-0.2.2 (c (n "tctrl") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.3.1") (f (quote ("tmux_3_3a"))) (d #t) (k 0)))) (h "0i4y4nzvlm4yz68726rbcp3lw6hx26fm89nbs2idyip6myfhpqp8")))

(define-public crate-tctrl-0.3.0 (c (n "tctrl") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.3.1") (f (quote ("tmux_3_3a"))) (d #t) (k 0)))) (h "1gcdi0dqrv1f8aisi2d6phxzgiqrcfa2lnybkawc8b0is1z04jph")))

(define-public crate-tctrl-0.3.1 (c (n "tctrl") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.3.1") (f (quote ("tmux_3_3a"))) (d #t) (k 0)))) (h "0ma5jzmnm5ynfpy7n64glnfd711hrwcgydppfiad7qd164gb2ryf")))

(define-public crate-tctrl-0.4.0 (c (n "tctrl") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.3.1") (f (quote ("tmux_3_3a"))) (d #t) (k 0)))) (h "08jm6cwbwmry361hc2icp1gfx3h60ijwzpk0nvaf1h72xkkclc4g")))

(define-public crate-tctrl-0.4.1 (c (n "tctrl") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.3.1") (f (quote ("tmux_3_3a"))) (d #t) (k 0)))) (h "0fvxy947bcn4mgxw7xapcrj0hgzvrgvkxv5p2isvy1qybdn5gj39")))

(define-public crate-tctrl-0.5.0 (c (n "tctrl") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "rlua") (r "^0.19.7") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.3.1") (f (quote ("tmux_3_3a"))) (d #t) (k 0)))) (h "0jaah1l0xnidpz4y37pv9xvxf8cs8wn5bk68s37x2dx0f2ccs9b8")))

(define-public crate-tctrl-0.6.0 (c (n "tctrl") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "rlua") (r "^0.19.7") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.3.1") (f (quote ("tmux_3_3a"))) (d #t) (k 0)))) (h "0bszsrsl8ky3h5066chcwc7v8hlqyz5jl5yfsm3a02wma65jad3j")))

