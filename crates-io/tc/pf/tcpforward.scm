(define-module (crates-io tc pf tcpforward) #:use-module (crates-io))

(define-public crate-tcpforward-0.1.0 (c (n "tcpforward") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14jyc6jgr4pq9l1pd0dhyvk1l84pkrm7jc565dr0qa7h1f07fgnl")))

(define-public crate-tcpforward-0.1.1 (c (n "tcpforward") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0plfvm46ny2gqjyps691gnkrwrbd1m1zg702nl74kaq0isdb5qvg")))

(define-public crate-tcpforward-0.1.2 (c (n "tcpforward") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nl3g5vg46vnjbfp4ivn41jx1zfllgpjnvzwbqzjkz7rb55q7xjm")))

(define-public crate-tcpforward-0.1.3 (c (n "tcpforward") (v "0.1.3") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12vk6n32kn3gcanxpji0lpwjj229vwzj554dbfvhcivrc5r796gx")))

