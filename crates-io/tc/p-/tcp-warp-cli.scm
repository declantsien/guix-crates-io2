(define-module (crates-io tc p- tcp-warp-cli) #:use-module (crates-io))

(define-public crate-tcp-warp-cli-0.1.0 (c (n "tcp-warp-cli") (v "0.1.0") (d (list (d (n "clap-verbosity-flag") (r "^0.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tcp-warp") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0jdfwy0qcm83m2kdybrxap2xhhxz5x5nqfsdinlsbynsfnh1rblr")))

(define-public crate-tcp-warp-cli-0.2.0 (c (n "tcp-warp-cli") (v "0.2.0") (d (list (d (n "clap-verbosity-flag") (r "^0.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tcp-warp") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0445kfkgapafv2f6higbhqz5hmk171y2x1zw2l5jdp19lr5n9q5d")))

