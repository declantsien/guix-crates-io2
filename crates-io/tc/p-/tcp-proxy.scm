(define-module (crates-io tc p- tcp-proxy) #:use-module (crates-io))

(define-public crate-tcp-proxy-0.1.0 (c (n "tcp-proxy") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1gg4j5qmhc08gmq2kdl1dzv47g0fqvpcyw9b594v0kjr35rbrxrq") (y #t)))

(define-public crate-tcp-proxy-0.2.0 (c (n "tcp-proxy") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "10ffb34psdx6qfmrf99r28m2yq1b78xrh32k3jsrqcjbar97jw2i") (y #t)))

(define-public crate-tcp-proxy-0.3.0 (c (n "tcp-proxy") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1rsp8mai8rdzwm17967dnsc7cx054lvxf4xx4g1k5606lghxljs6") (y #t)))

