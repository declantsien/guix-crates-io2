(define-module (crates-io tc p- tcp-wrapper-net) #:use-module (crates-io))

(define-public crate-tcp-wrapper-net-0.1.0 (c (n "tcp-wrapper-net") (v "0.1.0") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0786z6blkwa3pbg82f66z02bpc7xxns3yi25crqqm8nlily59h51") (y #t)))

(define-public crate-tcp-wrapper-net-0.1.1 (c (n "tcp-wrapper-net") (v "0.1.1") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mfxyk00vfkhx8nrnvdznw64g97ww68x8xpsmqv6y1kb47hcr2rk")))

(define-public crate-tcp-wrapper-net-0.1.11 (c (n "tcp-wrapper-net") (v "0.1.11") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1higchhlsi8wg6ch159phlcpicac3m2hccrrlw97x16p430qyn0q")))

(define-public crate-tcp-wrapper-net-0.1.111 (c (n "tcp-wrapper-net") (v "0.1.111") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jwm2qn0bbgs2igd51mnafn5z5dswyfgqc8p7vg8l6snhsb5l7ri")))

