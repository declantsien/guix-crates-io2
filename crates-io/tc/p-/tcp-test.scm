(define-module (crates-io tc p- tcp-test) #:use-module (crates-io))

(define-public crate-tcp-test-0.0.1 (c (n "tcp-test") (v "0.0.1") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0xnsw0yk3a5gzxwjpl14x7gr81gbyn8a94i3w6kmjh8hz1g3f5ky") (f (quote (("only_panic"))))))

(define-public crate-tcp-test-0.1.0 (c (n "tcp-test") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "106xsd8ayipz0xq32xsngw8jl13sacpq5qbqz8nxzgrgni5d9sp2")))

