(define-module (crates-io tc p- tcp-clone) #:use-module (crates-io))

(define-public crate-tcp-clone-0.99.2 (c (n "tcp-clone") (v "0.99.2") (d (list (d (n "async-std") (r "^1.1.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "03n5ns8w1nblbpad26g1r33gvarlkx3a3jzvrnx73xyzwfyydcyd") (y #t)))

(define-public crate-tcp-clone-0.99.3 (c (n "tcp-clone") (v "0.99.3") (d (list (d (n "async-std") (r "^1.1.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "04sr4hfyifk5pn6mwg92r306i804a7ry9h8crva45hhxyxjzw3ak") (y #t)))

(define-public crate-tcp-clone-0.99.4 (c (n "tcp-clone") (v "0.99.4") (d (list (d (n "async-std") (r "^1.1.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "01yg8irvc24navcn8jhhz6x1k3xx5g7nn506mb7kjxm479zzsqnk")))

