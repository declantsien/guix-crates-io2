(define-module (crates-io tc p- tcp-channel) #:use-module (crates-io))

(define-public crate-tcp-channel-0.1.0 (c (n "tcp-channel") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)))) (h "02bqw77phwikkqa778bdm522i5bqjmvv1qhm2j3lrrl9jgi9hxcc")))

(define-public crate-tcp-channel-0.2.0 (c (n "tcp-channel") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 2)))) (h "18wacqx40x3vng164l1xipgc3lfmjxfqpv8756sia4qn1arwkgfq")))

(define-public crate-tcp-channel-0.2.1 (c (n "tcp-channel") (v "0.2.1") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 2)))) (h "05c1ffds8v755ldx9s5szrw1pa9pc47l1j4yb1wcw7wz1jqm3jd3")))

(define-public crate-tcp-channel-0.3.0 (c (n "tcp-channel") (v "0.3.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 2)))) (h "1il25aj3saa6zx8ckl33wkzs20mr1qyymj3hhvjr9cz264jgj5ps")))

(define-public crate-tcp-channel-0.3.1 (c (n "tcp-channel") (v "0.3.1") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 2)))) (h "0k7gd6hd737ab5lbsafbisc9wgl1hi20qb3fzk8hw51sqdff0i7c")))

(define-public crate-tcp-channel-0.3.2 (c (n "tcp-channel") (v "0.3.2") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 2)))) (h "0inpxfg6hj8ijr0ssdlclsz28ylwh0s32ra6bb8200bw9h5zs34a")))

