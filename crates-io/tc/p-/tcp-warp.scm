(define-module (crates-io tc p- tcp-warp) #:use-module (crates-io))

(define-public crate-tcp-warp-0.1.0 (c (n "tcp-warp") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "tcp" "rt-core" "sync" "stream"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0m7p82g7ibr9755g3i0k2zwl8y4wqqzq4fjn08a6g1xi1f4k1d6h")))

(define-public crate-tcp-warp-0.2.0 (c (n "tcp-warp") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("dns" "io-util" "tcp" "rt-core" "sync" "stream" "time"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bngcpwbanr3wr835243birv6lsv95i63cc6iq893p6nnkxm1rf5")))

