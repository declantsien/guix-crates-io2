(define-module (crates-io tc p- tcp-relay-rust) #:use-module (crates-io))

(define-public crate-tcp-relay-rust-0.1.0 (c (n "tcp-relay-rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "net" "macros" "io-util" "io-std"))) (d #t) (k 0)))) (h "1hnim7zslfd7wx0cjxci75vgmzmhgdfb3cdfx7n5h9abk57lj784")))

