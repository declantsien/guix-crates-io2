(define-module (crates-io tc p- tcp-channel-client) #:use-module (crates-io))

(define-public crate-tcp-channel-client-0.1.0 (c (n "tcp-channel-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-channel") (r "^1.9.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "159vadll9gg5ipnivid3pxm4aa2wpj76l7i4bmgb7clgn7ih2w6w")))

(define-public crate-tcp-channel-client-0.2.0 (c (n "tcp-channel-client") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-channel") (r "^2.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "0gig7fk1dy081lp2c97219pjdq8fl2ykf0jmcypq8wfmdaynzaq3")))

