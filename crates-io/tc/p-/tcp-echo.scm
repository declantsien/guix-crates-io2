(define-module (crates-io tc p- tcp-echo) #:use-module (crates-io))

(define-public crate-tcp-echo-0.1.0 (c (n "tcp-echo") (v "0.1.0") (d (list (d (n "acto-rs") (r "^0.3.7") (d #t) (k 0)))) (h "0c992z9czd19ylmv160qmvs0gppjrd5k32aygf061j17d6bnzg9l")))

(define-public crate-tcp-echo-0.1.2 (c (n "tcp-echo") (v "0.1.2") (d (list (d (n "acto-rs") (r "^0.3.7") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "13rpnrgbr9fri27xkfz7pg0jja1djjgnkrm3856xh3hygkl17ijk")))

