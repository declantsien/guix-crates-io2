(define-module (crates-io tc p- tcp-scan) #:use-module (crates-io))

(define-public crate-tcp-scan-0.0.0 (c (n "tcp-scan") (v "0.0.0") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11kqhhd0qw4bbqlyiy4cafc393yi7lhprpibk774fkr7br1q9jsh")))

(define-public crate-tcp-scan-0.0.1 (c (n "tcp-scan") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05wij7q7xnzmy525y3vfwqdm9k978cycfxdhk8il7gqnsmw5jrig")))

