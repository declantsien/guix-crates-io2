(define-module (crates-io tc _e tc_elasticmq) #:use-module (crates-io))

(define-public crate-tc_elasticmq-0.1.0 (c (n "tc_elasticmq") (v "0.1.0") (d (list (d (n "tc_core") (r "^0.2") (d #t) (k 0)))) (h "0n3makw72cn6rqdq6g0jx1m4cqasfgd18vkbqaj8kfq1vpfqyhc9")))

(define-public crate-tc_elasticmq-0.2.0 (c (n "tc_elasticmq") (v "0.2.0") (d (list (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "1c98j28a35pixgzd6m2xwss8gbay7mc6jqhq9052h4q9qxr7qyhd")))

(define-public crate-tc_elasticmq-0.2.1 (c (n "tc_elasticmq") (v "0.2.1") (d (list (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "1l0s6gvxq0a0xwkpgyry28r2cmrz8624wqynscq1g04wj9izgh86")))

