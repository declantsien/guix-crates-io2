(define-module (crates-io tc p_ tcp_typed) #:use-module (crates-io))

(define-public crate-tcp_typed-0.1.0 (c (n "tcp_typed") (v "0.1.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (t "cfg(unix)") (k 0)) (d (n "palaver") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "149qz3xi2g16aamqpcw84j6w8jw3zsz2yx3qkzsmq4w2458gy463")))

(define-public crate-tcp_typed-0.1.1 (c (n "tcp_typed") (v "0.1.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (t "cfg(unix)") (k 0)) (d (n "palaver") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "1021aldjpcva9cpwns3r609di9sbk4v6w9lfx25rzki2wchrx7hh")))

(define-public crate-tcp_typed-0.1.2 (c (n "tcp_typed") (v "0.1.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "palaver") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0y75ygxw33fa3gl3q2w5jrm57wgc6g1n66glam8m2v97rq5pgkv9")))

(define-public crate-tcp_typed-0.1.3 (c (n "tcp_typed") (v "0.1.3") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (t "cfg(unix)") (k 0)) (d (n "palaver") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "1ii4lijli7v9pl81m4yn138yb9ri4s5kdwmxn9y3c3fh3m4grbbs")))

(define-public crate-tcp_typed-0.1.4 (c (n "tcp_typed") (v "0.1.4") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (t "cfg(unix)") (k 0)) (d (n "palaver") (r "^0.2") (d #t) (k 0)) (d (n "socketstat") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "05z84bmvshwc9j72r2nhx6m5qap0f4nl3w6kk0axrgnw27w4dnw3")))

