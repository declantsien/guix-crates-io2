(define-module (crates-io tc p_ tcp_wrapper) #:use-module (crates-io))

(define-public crate-tcp_wrapper-0.1.0 (c (n "tcp_wrapper") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r787yl1h2m7wz1z86nd5pwvr0915y3i0v515iv2bi5dzb1xibl7")))

(define-public crate-tcp_wrapper-0.1.2 (c (n "tcp_wrapper") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "04z9xz8qx22cp1di4ld4fmc1qgcmpnsj2d0d4zc8f4q674xm863x") (r "1.56")))

(define-public crate-tcp_wrapper-0.2.0 (c (n "tcp_wrapper") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hq08i3n7qhh0zp3rq4641h2r4mhh6fldqq9kk6wk6j7as689nlf") (r "1.56")))

(define-public crate-tcp_wrapper-0.2.1 (c (n "tcp_wrapper") (v "0.2.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09ld0qgfm4wc2f123ksl8y03gg482pf7hicrc0qfj5yzyd3h66ly") (r "1.56")))

(define-public crate-tcp_wrapper-0.2.2 (c (n "tcp_wrapper") (v "0.2.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14dsqphs25ay5djypg5qqkyl26f4xdapp9z60irf04q34jk9r0qn") (r "1.56")))

