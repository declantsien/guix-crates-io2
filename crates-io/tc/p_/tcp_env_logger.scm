(define-module (crates-io tc p_ tcp_env_logger) #:use-module (crates-io))

(define-public crate-tcp_env_logger-1.0.0 (c (n "tcp_env_logger") (v "1.0.0") (d (list (d (n "env_logger") (r "0.10.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0989sd6yzmlm0v8xlbqvricmia69gkzbb61gqab3jiaqsvc7vaqs")))

