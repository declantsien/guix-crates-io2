(define-module (crates-io tc -c tc-consensus-epochs) #:use-module (crates-io))

(define-public crate-tc-consensus-epochs-0.0.0 (c (n "tc-consensus-epochs") (v "0.0.0") (h "1xnww7wma2jzxlsdgncz04z1fff26gn5k86kmz23aq1dcccnsmqw") (y #t)))

(define-public crate-tc-consensus-epochs-0.8.0 (c (n "tc-consensus-epochs") (v "0.8.0") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (d #t) (k 0) (p "tetsy-scale-codec")) (d (n "forktree") (r "^2.0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tc-client-api") (r "^2.0.0") (d #t) (k 0)) (d (n "tp-blockchain") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.0") (d #t) (k 0)))) (h "0j9351aa3p78wwy15ya2csldkdbq7vlbbf8mcxc3s0qn295a5r5q")))

