(define-module (crates-io tc -c tc-consensus-uncles) #:use-module (crates-io))

(define-public crate-tc-consensus-uncles-0.0.0 (c (n "tc-consensus-uncles") (v "0.0.0") (h "10b4j0sax5clmi342saa220vn7g0cxs0z8ib0avpfj9gr7403m8i") (y #t)))

(define-public crate-tc-consensus-uncles-0.8.0 (c (n "tc-consensus-uncles") (v "0.8.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tc-client-api") (r "^2.0.0") (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-authorship") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-consensus") (r "^0.8.2") (d #t) (k 0)) (d (n "tp-inherents") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)))) (h "0vwpnz6zs02larldb1vijn1qxgd0hcqadzfr3hhvc24alpv71hvy")))

