(define-module (crates-io tc -c tc-chain-spec-derive) #:use-module (crates-io))

(define-public crate-tc-chain-spec-derive-0.0.0 (c (n "tc-chain-spec-derive") (v "0.0.0") (h "0awmcd8azccknq6wxnlch0s98xnnzgpzvy5qm14rwkmyvgm4fjw0") (y #t)))

(define-public crate-tc-chain-spec-derive-2.0.0 (c (n "tc-chain-spec-derive") (v "2.0.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "123panxmhrqk88qgjjlz2mbgy4a892wifjajx5k6i40z1rzcg9c0")))

