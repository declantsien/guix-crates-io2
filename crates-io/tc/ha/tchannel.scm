(define-module (crates-io tc ha tchannel) #:use-module (crates-io))

(define-public crate-tchannel-0.0.1 (c (n "tchannel") (v "0.0.1") (d (list (d (n "futures") (r "^0.1.17") (o #t) (d #t) (k 0)))) (h "1xqyfp0baz9gw18yy113y12dh46ffas7c138fcr7w7rknq9i9i7x") (f (quote (("with-futures" "futures") ("default"))))))

(define-public crate-tchannel-0.0.2 (c (n "tchannel") (v "0.0.2") (d (list (d (n "futures") (r "^0.1.17") (o #t) (d #t) (k 0)))) (h "1pz88kpaplwq246p1jixf8fjshsgrnj555q5zb6cn2la8blzkkbz") (f (quote (("with-futures" "futures") ("default"))))))

