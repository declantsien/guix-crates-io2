(define-module (crates-io tc ha tchain) #:use-module (crates-io))

(define-public crate-tchain-0.1.0 (c (n "tchain") (v "0.1.0") (d (list (d (n "lmdb-zero") (r "^0.4.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "05dfmfg45wkdiml59bsakbpdx923mvc3ck0p0q8glq2brl3af65b") (y #t)))

(define-public crate-tchain-0.1.1 (c (n "tchain") (v "0.1.1") (d (list (d (n "lmdb-zero") (r "^0.4.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "0m9wjaw84bn577bkc8rc2ahq7mb501y9pgqhy8g06b8irixjp81k") (y #t)))

(define-public crate-tchain-0.1.2 (c (n "tchain") (v "0.1.2") (d (list (d (n "lmdb-zero") (r "^0.4.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "0k0s6krz1ij61grg9sr5cwilbjsxf52ya95i8fav6283zhy9r8ig")))

(define-public crate-tchain-0.1.3 (c (n "tchain") (v "0.1.3") (d (list (d (n "lmdb-zero") (r "^0.4.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "1vp73d07gim7nhv8jhxyq9jk577ax06vnf8fwimmpfqs6n19ikgf")))

