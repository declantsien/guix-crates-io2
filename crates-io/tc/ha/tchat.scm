(define-module (crates-io tc ha tchat) #:use-module (crates-io))

(define-public crate-tchat-0.1.0 (c (n "tchat") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "07knzfk1ypcdawmk9gqb72jy7jbjr4gpbs00fdyirszs22x4iwvc")))

(define-public crate-tchat-0.1.1 (c (n "tchat") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "05ap8nzzf3dphbnzj2z3jj9qqnhzi65y2qhm7lnz9rmrk4yaivg4")))

(define-public crate-tchat-0.1.2 (c (n "tchat") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0fhpsnamm7mxd9bfdsflaizazxwb0aj66r7qi8lkiv7ww2pz69lp")))

