(define-module (crates-io tc _t tc_tea) #:use-module (crates-io))

(define-public crate-tc_tea-0.1.0 (c (n "tc_tea") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (f (quote ("rand_chacha"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0ylsvm5y20ysh06jvjg02x1bf309240xzbmnffjpw1gmhigp9gb8") (f (quote (("secure_random" "rand/getrandom") ("default" "secure_random"))))))

(define-public crate-tc_tea-0.1.1 (c (n "tc_tea") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.0") (f (quote ("rand_chacha"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1h4jfwaw9dscn1abqp3npkniqh1gz3qyi4q4lrhjw96i3n8ijyf3") (f (quote (("secure_random" "rand/getrandom") ("default" "secure_random"))))))

(define-public crate-tc_tea-0.1.3 (c (n "tc_tea") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.0") (f (quote ("rand_chacha"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0q8wg1b1j8dc36lj1kwjdwc8i0ry2d4r24461sk06fb5cd3s950r") (f (quote (("secure_random" "rand/getrandom") ("default" "secure_random"))))))

(define-public crate-tc_tea-0.1.4 (c (n "tc_tea") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.0") (f (quote ("rand_chacha"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1lk6gl9gv9mcki5x3nj39db5prnz4j0npiw2hpncpr8fwy2n5cna") (f (quote (("secure_random" "rand/getrandom") ("default" "secure_random"))))))

