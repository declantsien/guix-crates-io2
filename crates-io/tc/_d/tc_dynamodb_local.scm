(define-module (crates-io tc _d tc_dynamodb_local) #:use-module (crates-io))

(define-public crate-tc_dynamodb_local-0.1.0 (c (n "tc_dynamodb_local") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tc_core") (r "^0.2") (d #t) (k 0)))) (h "1hb5pk5ryqbvq8ij57qqzdf0mi984nsdnx2zwnd71aj30cfsy5f8")))

(define-public crate-tc_dynamodb_local-0.2.0 (c (n "tc_dynamodb_local") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "0vl6qn9pa58y0pnjgnbfb2njizsziakcwjzcb1fkgngrfq9x5yql")))

(define-public crate-tc_dynamodb_local-0.2.1 (c (n "tc_dynamodb_local") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tc_core") (r "^0.3") (d #t) (k 0)))) (h "1zx9f83c5q6zbnr24bsly9w7d766538c9g7l3007hjmwqykqqxvg")))

