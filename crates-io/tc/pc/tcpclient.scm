(define-module (crates-io tc pc tcpclient) #:use-module (crates-io))

(define-public crate-tcpclient-0.1.0 (c (n "tcpclient") (v "0.1.0") (d (list (d (n "aqueue") (r ">=0.1.18, <0.2.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.2.0, <0.3.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1asrg0zy69py6aa99mmfddnhmccpldw6wmf8hfvysc7ri7p8sa9x")))

(define-public crate-tcpclient-0.1.1 (c (n "tcpclient") (v "0.1.1") (d (list (d (n "aqueue") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ablm0g2iwiicarvqsms1x0rznmy7jdz1rig6d230f0pzhga7dpj")))

(define-public crate-tcpclient-0.2.0 (c (n "tcpclient") (v "0.2.0") (d (list (d (n "aqueue") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0lnk64ckxb0cbinpk2a0wckzv1qk28jgqk8w9x6g6kfjmgqhs5ng")))

(define-public crate-tcpclient-0.2.1 (c (n "tcpclient") (v "0.2.1") (d (list (d (n "aqueue") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0v4lnpn2bjj75993vwr2jyxcwa0lrk2v2rkb60pjidy966zj8ryh")))

(define-public crate-tcpclient-0.3.0 (c (n "tcpclient") (v "0.3.0") (d (list (d (n "aqueue") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03vj3vx3027ay3ng1ab62i50zwbqxd8j1il73rjhgwi4yq7rvlsy")))

(define-public crate-tcpclient-0.3.1 (c (n "tcpclient") (v "0.3.1") (d (list (d (n "aqueue") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03nph108ghkmbg4k19y8x2si6z8s71fv3vvz7qavxg0qwylpms6g")))

(define-public crate-tcpclient-0.3.2 (c (n "tcpclient") (v "0.3.2") (d (list (d (n "aqueue") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0350xynxa86k3hj3baznnfskx6qijm9w02hf33m8z71jirrr7g4i")))

(define-public crate-tcpclient-0.3.3 (c (n "tcpclient") (v "0.3.3") (d (list (d (n "aqueue") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yry8n6ipp1qp2a0xvq8mxg3si1wq8ww0zn8i13mvwgqrx5ackx3")))

(define-public crate-tcpclient-1.0.0 (c (n "tcpclient") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pii5vbxwd8d6hbhn48vldpidbjmlsp2bbhn2hc54dqgcbfnxj10")))

(define-public crate-tcpclient-1.0.1 (c (n "tcpclient") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hqrkc2ms8m6mrw4a4rmjigv8wkhjijaw5zp2nvbg7w7w30k5mbz")))

(define-public crate-tcpclient-1.1.0 (c (n "tcpclient") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pqcx8912iaajf5kbwf8gj6cn0zqv44sxqhzsv2l9wi915hp8a9i")))

(define-public crate-tcpclient-1.2.0 (c (n "tcpclient") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mk6z54b1xxa8k5nri245gdjjlqji2gczaa6ixrbgqnsmcgd9bin")))

(define-public crate-tcpclient-1.2.1 (c (n "tcpclient") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "0638hcbwa16j4mhhhwkhxxmir6ndzclm0vv8b6h1j71a08wkvrp3")))

(define-public crate-tcpclient-1.2.2 (c (n "tcpclient") (v "1.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "1vbxi1p4k08fhf4gk6d9yvjvzdx7dq4md5wc89cffzgai1v762pk")))

(define-public crate-tcpclient-1.2.3 (c (n "tcpclient") (v "1.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "0jiz27wc59namswailfxya14jhqsrhf9qi9bbqy4b4h182fpnv0g")))

(define-public crate-tcpclient-1.2.4 (c (n "tcpclient") (v "1.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "0xvn79qcvixdf57rn7fa4rm8wbw6jjz5jcprsrwzyfbxsvac27d6")))

(define-public crate-tcpclient-1.2.5 (c (n "tcpclient") (v "1.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "0242x4zyy24wdxigjyj10xi5nfv12whn83yqgyrkcc059m51vzp3")))

(define-public crate-tcpclient-1.3.0 (c (n "tcpclient") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "0a19aqqia8mg8yzii9hi84mgg5lsann6qkmik7lgdq6bk9mhm3pb") (y #t)))

(define-public crate-tcpclient-1.3.1 (c (n "tcpclient") (v "1.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "13dgm5650zjk6dy80nhc1gajmb83lj6zlbs7kk0pjbaydkdkwbrw")))

(define-public crate-tcpclient-1.3.2 (c (n "tcpclient") (v "1.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "1d64qdxm7k1z5nwzjk9wjhqkrscd3ij0iz7rz38h2xzrmhggbly5")))

(define-public crate-tcpclient-1.3.3 (c (n "tcpclient") (v "1.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "0ffgn1rdkbhmar4rc9ajhl4pqx1v8fdf9nf0vgnzv1jflpnblprp")))

(define-public crate-tcpclient-1.3.4 (c (n "tcpclient") (v "1.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "14mnvz67g6wgfzc3pkky1vpx70qys2das931zmgp9aw3invcgh5y")))

(define-public crate-tcpclient-1.4.0 (c (n "tcpclient") (v "1.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "1sg7fc3hdgc3p999p4ffh6wz1168l3d2dynfnxxizjg51n0nf0m7")))

(define-public crate-tcpclient-1.4.1 (c (n "tcpclient") (v "1.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "178c9dg56h0xkc5mr8hicyg41wza1whr7rp7ksmn4p6w3p8m7wzj")))

(define-public crate-tcpclient-2.0.0 (c (n "tcpclient") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aqueue") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)))) (h "1v0z4jl2dd1d9hdcs2jmalx14wrm63ky79h1vs57in5y4w8wbpjr")))

