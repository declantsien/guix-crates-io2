(define-module (crates-io tc pn tcpnet) #:use-module (crates-io))

(define-public crate-tcpnet-0.1.0 (c (n "tcpnet") (v "0.1.0") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02p0df9prhkzi0pv2lw3dzjn7g3s2vswfd0qxlinspggbrrd2vhk") (y #t)))

(define-public crate-tcpnet-0.1.1 (c (n "tcpnet") (v "0.1.1") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06aa990fjh9rhfh9q8zhlm94yimc8zzjai5jgb3w4qch74mdb49k")))

