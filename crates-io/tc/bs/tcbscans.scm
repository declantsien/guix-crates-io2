(define-module (crates-io tc bs tcbscans) #:use-module (crates-io))

(define-public crate-tcbscans-0.1.0 (c (n "tcbscans") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "imagequant") (r "^4") (d #t) (k 0)) (d (n "lodepng") (r "^3.6") (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nyvsyr2igi8k1csir8c24rg8v8pgwwayd0vfkf068m72l7ip83f")))

