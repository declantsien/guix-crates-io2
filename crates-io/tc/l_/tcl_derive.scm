(define-module (crates-io tc l_ tcl_derive) #:use-module (crates-io))

(define-public crate-tcl_derive-0.1.0 (c (n "tcl_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0c8kqbrsrc22j0biyii26bvw0lvf39ibvnz69gd02k3rin7khmq6")))

(define-public crate-tcl_derive-0.1.1 (c (n "tcl_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0vkvsb2v34j7c9zdih84v7wi1242i0ccff48rf4sy2b1nng4z2sp")))

(define-public crate-tcl_derive-0.1.2 (c (n "tcl_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1b3qn9f5z1fkisxbfg12ff9y7ipzcp6i6v4ld2hr7q8z5vjz5zsr")))

(define-public crate-tcl_derive-0.1.3 (c (n "tcl_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1q3p1apacbnbwh1jhr8vimv1w7nc7g96ynr766ajxqb81qkrqb3s")))

(define-public crate-tcl_derive-0.1.4 (c (n "tcl_derive") (v "0.1.4") (d (list (d (n "bind_syn") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "01srh9dgz7j5gfwk1zqg928jb0ai9ilw8gy5hbhl9d61zgs33mnb")))

(define-public crate-tcl_derive-0.1.5 (c (n "tcl_derive") (v "0.1.5") (d (list (d (n "bind_syn") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "14vvd34irk949466pcli50dbgg3jmnq62s5dvqqvy6r3fbk9apb2")))

