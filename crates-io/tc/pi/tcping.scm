(define-module (crates-io tc pi tcping) #:use-module (crates-io))

(define-public crate-tcping-0.0.1 (c (n "tcping") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)))) (h "0mnxwf9xymks79k3k82bx52gmyvjcd15xn2nv54jdwgxxhki945j")))

(define-public crate-tcping-1.0.0 (c (n "tcping") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)))) (h "1hq36l6l2akyzbwr2jybp8rwpj2p1y1zpclaan6r2qyxhgvl5mrl")))

(define-public crate-tcping-1.0.1 (c (n "tcping") (v "1.0.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)))) (h "1wxmkrccacarw8gn5x9llvm1ziryvlr3jbrspyyf116bnr98caq1")))

