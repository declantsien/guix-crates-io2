(define-module (crates-io tc -b tc-block-builder) #:use-module (crates-io))

(define-public crate-tc-block-builder-0.0.0 (c (n "tc-block-builder") (v "0.0.0") (h "0gbnfzdfnkzcpp92g4pr28g836x50xr91g19r88hh5mg1r59z0s8") (y #t)))

(define-public crate-tc-block-builder-0.8.0 (c (n "tc-block-builder") (v "0.8.0") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (d #t) (k 0) (p "tetsy-scale-codec")) (d (n "tc-client-api") (r "^2.0.0") (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-api") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-block-builder") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-blockchain") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-consensus") (r "^0.8.2") (d #t) (k 0)) (d (n "tp-inherents") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-state-machine") (r "^0.8.2") (d #t) (k 0)) (d (n "tp-trie") (r "^2.0.2") (d #t) (k 2)))) (h "043q04jphh3kfbpciilv9zy6800s566yvzpw8jcvs9rv1mwxm9s7")))

