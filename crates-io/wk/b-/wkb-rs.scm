(define-module (crates-io wk b- wkb-rs) #:use-module (crates-io))

(define-public crate-wkb-rs-0.1.0 (c (n "wkb-rs") (v "0.1.0") (d (list (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1qz568pv8g8n529hqqg8066dachmkr4q3hbqw409vgq9lhcyzcif")))

(define-public crate-wkb-rs-0.1.1 (c (n "wkb-rs") (v "0.1.1") (d (list (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12khc76dgwf86qnj0415s0hap43g6kzp069vv3iccgkdwishpnxz")))

