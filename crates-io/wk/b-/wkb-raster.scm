(define-module (crates-io wk b- wkb-raster) #:use-module (crates-io))

(define-public crate-wkb-raster-0.1.0 (c (n "wkb-raster") (v "0.1.0") (h "1l6da5xx3vss74zq0famsjjbg1j7vyhmn7l60biafnc7iqv646sd")))

(define-public crate-wkb-raster-0.1.1 (c (n "wkb-raster") (v "0.1.1") (h "0jlmikcnrgnmnhm17g2ncazhs9x4kv93kj5w64g7k6536ixr9zci")))

(define-public crate-wkb-raster-0.2.0 (c (n "wkb-raster") (v "0.2.0") (h "1qk0f9qvmch5ich95vqzcvzr4qzj9kr726c99lv6bw9vldyhcq13")))

(define-public crate-wkb-raster-0.2.1 (c (n "wkb-raster") (v "0.2.1") (h "1qwg2n709lmxada7qa9q24pdj6wq0ihgk9a1339gdg205wvjaf4p")))

