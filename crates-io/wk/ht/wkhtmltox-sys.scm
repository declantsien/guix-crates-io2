(define-module (crates-io wk ht wkhtmltox-sys) #:use-module (crates-io))

(define-public crate-wkhtmltox-sys-0.1.0 (c (n "wkhtmltox-sys") (v "0.1.0") (h "1x315pvrw53lc0kb5zzx8fll4xqbs6zdl3k01sizywlxxvanmh5p")))

(define-public crate-wkhtmltox-sys-0.1.1 (c (n "wkhtmltox-sys") (v "0.1.1") (h "0h2kgba68qazjq4l3f3sf8q54v6pz3l3sxv7g952w9wfqvc0a9x5")))

(define-public crate-wkhtmltox-sys-0.1.2 (c (n "wkhtmltox-sys") (v "0.1.2") (h "078vmdsihik2y4ssbg90sks5y0w2mdj2kx8vdfbd1gkjmbr0a15b")))

