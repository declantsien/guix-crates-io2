(define-module (crates-io wk ht wkhtmltopdf) #:use-module (crates-io))

(define-public crate-wkhtmltopdf-0.2.0 (c (n "wkhtmltopdf") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "thread-id") (r "^2.0.0") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)) (d (n "wkhtmltox-sys") (r "^0.1.0") (d #t) (k 0)))) (h "00qmvymi07bz21ali5m797p5anlvbmwxz18hmpz227qfabfr8xlz")))

(define-public crate-wkhtmltopdf-0.3.0 (c (n "wkhtmltopdf") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "thread-id") (r "^2.0.0") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)) (d (n "wkhtmltox-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0k94az5iczk648m27sh1ih9a23xf1yinqpj342jn2ig99d87jc2c")))

(define-public crate-wkhtmltopdf-0.4.0 (c (n "wkhtmltopdf") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)) (d (n "wkhtmltox-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1sv5ccgwrbjwafm3k1qp4j2ycmvxviyxkxnby5kggqz5bb3dyf52")))

