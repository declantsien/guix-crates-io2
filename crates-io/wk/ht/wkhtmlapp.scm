(define-module (crates-io wk ht wkhtmlapp) #:use-module (crates-io))

(define-public crate-wkhtmlapp-0.1.0 (c (n "wkhtmlapp") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1glwkx3zhsrjvidkigf90jb7wbwn3vj2p3qnhlnvy5vnq7v6ay59") (y #t)))

(define-public crate-wkhtmlapp-0.1.1 (c (n "wkhtmlapp") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1amlpyxhdp1311gbjc32fvylwavcvs1z8sm384c256mz1ry0v60p") (y #t)))

(define-public crate-wkhtmlapp-0.1.2 (c (n "wkhtmlapp") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0a6v5dbnkj5wcmmjd90idb705blvd3xcwk0xnpm9khfw7fqdr896") (y #t)))

(define-public crate-wkhtmlapp-0.1.3 (c (n "wkhtmlapp") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "136j29hq1lcn1q6105xz0izq4il354byl2m99vgb4ss7misy9lyg")))

(define-public crate-wkhtmlapp-0.1.4 (c (n "wkhtmlapp") (v "0.1.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0py6d6zbzjaprd06yyvqzpss4cr45bp4cqqnjp6g54bfbk4kiq9c")))

(define-public crate-wkhtmlapp-0.1.5 (c (n "wkhtmlapp") (v "0.1.5") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1jsv5lchi80hypnm5vvb0h0yhiq5hx5q36nh9547fp2avaqq4i6d")))

(define-public crate-wkhtmlapp-0.2.0 (c (n "wkhtmlapp") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1v57inm98m2qsfzjf73xh39g5shm62kgpdfqyiy2c0hkvngji9j2")))

(define-public crate-wkhtmlapp-1.0.0 (c (n "wkhtmlapp") (v "1.0.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1wzrkylfpvdsnrin8y4vqcarr5axgbc60cici0smga1gjlwfjhly")))

(define-public crate-wkhtmlapp-1.0.1 (c (n "wkhtmlapp") (v "1.0.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)))) (h "1zg08g50ymlwhz5vnyh1bc1fwwajqmz4zm2x8sh6110hskgf6h29")))

(define-public crate-wkhtmlapp-1.0.2 (c (n "wkhtmlapp") (v "1.0.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)))) (h "01inhz3dsj8g9kc1l2cvlprr26jgshkpmp6rpdkqhdaclnkxapkn")))

