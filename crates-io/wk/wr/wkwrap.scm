(define-module (crates-io wk wr wkwrap) #:use-module (crates-io))

(define-public crate-wkwrap-1.4.0 (c (n "wkwrap") (v "1.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18gp8x25d4nn97rg0hd570fxpkqf4axa9sqp59vjc3rnhnjfp2m6")))

(define-public crate-wkwrap-1.5.0 (c (n "wkwrap") (v "1.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cwzrh2p7djja79ns2ggr023awfnwqx5ysm9wfpmpgvi91m03shy")))

(define-public crate-wkwrap-1.6.0 (c (n "wkwrap") (v "1.6.0") (d (list (d (n "lz4") (r "^1.23") (d #t) (k 0)))) (h "1xn61hhabrgc5qbs0p6pwzzfn6nr824scx3xkvards11w18i13m7")))

