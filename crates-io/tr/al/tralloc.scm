(define-module (crates-io tr al tralloc) #:use-module (crates-io))

(define-public crate-tralloc-0.1.0 (c (n "tralloc") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "06q75i390ygxy1liv24lj93fash0s36vz5ia9cbricj1hig22bfh")))

(define-public crate-tralloc-0.1.1 (c (n "tralloc") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1xjzlmi3ijghsgs6gk5j46gz6p8cwqsc093mns7halrmn6xbhxmq")))

