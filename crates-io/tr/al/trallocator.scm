(define-module (crates-io tr al trallocator) #:use-module (crates-io))

(define-public crate-trallocator-0.1.0 (c (n "trallocator") (v "0.1.0") (h "15zg3807s1fn77lghi2z68rsm1bjdb1m2jx1kjwd093lk84pa1yd") (f (quote (("max-usage") ("allocator-api"))))))

(define-public crate-trallocator-0.2.0 (c (n "trallocator") (v "0.2.0") (h "1pmxfbhwx51zfmhh2i6z4ljn827c58afaaf5773rxkb3r14mi5jq") (f (quote (("max-usage") ("allocator-api"))))))

(define-public crate-trallocator-0.2.1 (c (n "trallocator") (v "0.2.1") (h "0rlqs3acpp1w4lwccrc1r9pmvhi0xlpai8npiykm009gn47wbvyi") (f (quote (("max-usage") ("allocator-api"))))))

