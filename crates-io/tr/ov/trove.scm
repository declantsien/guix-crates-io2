(define-module (crates-io tr ov trove) #:use-module (crates-io))

(define-public crate-trove-0.1.0 (c (n "trove") (v "0.1.0") (h "1fq11sn6gf7fwdjwgai08c2rsi66pfhsclxpq5hc3vmrdx9rg1yk")))

(define-public crate-trove-0.2.0 (c (n "trove") (v "0.2.0") (h "14g02sp7qrl5q8r7yykvrn3gqv6hfg3p5y1gsnnlfpvl9sb3iwpz")))

(define-public crate-trove-0.3.0 (c (n "trove") (v "0.3.0") (h "0yx0lrg3vf1wqphj5xcc08rbpbgarrnvbk9yilnralw1d4x0i7np")))

(define-public crate-trove-0.4.0 (c (n "trove") (v "0.4.0") (d (list (d (n "vec_map") (r "^0.8.1") (d #t) (k 0)))) (h "0hw7szrw959j2hx1y5cnq3pzpz13dp9503pql6a0dqi29q2k7ym8")))

(define-public crate-trove-0.4.1 (c (n "trove") (v "0.4.1") (d (list (d (n "vec_map") (r "^0.8.1") (d #t) (k 0)))) (h "0qqzld16bbdr1rnfl34rz8d3v18f9wqibcqnc7v9inbhfpbxqgkm")))

(define-public crate-trove-0.4.2 (c (n "trove") (v "0.4.2") (d (list (d (n "vec_map") (r "^0.8.1") (d #t) (k 0)))) (h "0nqd0hpbq835r4blq7787cq3lrknk4awmmpmna1ym7c2zs5xc0lk")))

(define-public crate-trove-1.0.0 (c (n "trove") (v "1.0.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.8.1") (d #t) (k 0)))) (h "1v5sp42d554j48d08fg0dqxf760w25himx70bn25n7b3ip8h4wz4")))

(define-public crate-trove-1.0.1 (c (n "trove") (v "1.0.1") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.8.1") (d #t) (k 0)))) (h "0a1yd9flvbc0y3qqr18z0glcswhyrj3b9879fmz7s2772w3xmn52")))

