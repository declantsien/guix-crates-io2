(define-module (crates-io tr ov trove-classifiers) #:use-module (crates-io))

(define-public crate-trove-classifiers-0.1.0 (c (n "trove-classifiers") (v "0.1.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1ad2brifbq9psbfxyhhi2v6ykm0yfdmrziakpl7cw7fxj0i0hf69")))

(define-public crate-trove-classifiers-0.2.0 (c (n "trove-classifiers") (v "0.2.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1zs9r17d02kjadaybyahxyhjkc53xh0fjvqrr03hid88jw7711v5")))

(define-public crate-trove-classifiers-0.3.0 (c (n "trove-classifiers") (v "0.3.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "19xis60kqzv5z7jmz5lkqlvqcwl9g77f27fc5g7205dyvbv9dr9k")))

(define-public crate-trove-classifiers-0.4.0 (c (n "trove-classifiers") (v "0.4.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1wsyw7dq5zav7972bb85z2b8nl1liis2b19sgmrc7j992y950z8w")))

(define-public crate-trove-classifiers-0.5.0 (c (n "trove-classifiers") (v "0.5.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1xzg7a6nxvmpg8j7xpvp6fmxkik3ws7dm6pr6iklx9qgksfjln25")))

(define-public crate-trove-classifiers-0.6.0 (c (n "trove-classifiers") (v "0.6.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "14klcc4g63g4d9xzlhzv3nwbfy9il0yshrkfz75djp37yn0i6qqp")))

(define-public crate-trove-classifiers-0.7.0 (c (n "trove-classifiers") (v "0.7.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "16jxl7pjgz2g1r2awqcpgl6rvxdqygqk7kx0xp9dcq9486acf2mz")))

(define-public crate-trove-classifiers-0.8.0 (c (n "trove-classifiers") (v "0.8.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0fzpfq6n1993c7giwv4is7d1p8x24myff3a1brlggsvqrmica9b4")))

(define-public crate-trove-classifiers-0.9.0 (c (n "trove-classifiers") (v "0.9.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0nn1nsxym5r9bxh27i3z3cwm7dxv0lw2zvf7i2463f6407z5j1dc")))

(define-public crate-trove-classifiers-0.10.0 (c (n "trove-classifiers") (v "0.10.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0bjd755l0idpckr9id9hqrr2kpqnff7h7nhjhfccfhal0kpgnbaa")))

(define-public crate-trove-classifiers-0.11.0 (c (n "trove-classifiers") (v "0.11.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0rrwbgbqpqmnx680v6h19dflmgvgpkwgxkm850wqsq9mqcbga3vs")))

(define-public crate-trove-classifiers-0.12.0 (c (n "trove-classifiers") (v "0.12.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "02bj5w3znhjicjs2vs7mjxylsxx497pn6vgviljdd2jh93nc2vn2")))

(define-public crate-trove-classifiers-0.13.0 (c (n "trove-classifiers") (v "0.13.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0mmnfd3wh9vkq982hq54c26irav38c265wfbw68swridvnzcp9fq")))

(define-public crate-trove-classifiers-0.14.0 (c (n "trove-classifiers") (v "0.14.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "18albcz4s7m3px4s1252cy3xqad08gamfx45ilvzbqm5ychzjg84")))

(define-public crate-trove-classifiers-0.15.0 (c (n "trove-classifiers") (v "0.15.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1wrajsbq75vg2kh8rk7aih79qka5k01bspk77hs0dkgrj3qrjbdm")))

(define-public crate-trove-classifiers-0.16.0 (c (n "trove-classifiers") (v "0.16.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0rhahpmarb0vfivq7ka55w9gi4y05pzlip1cs588vvki8aklamif")))

(define-public crate-trove-classifiers-0.17.0 (c (n "trove-classifiers") (v "0.17.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1vcs0vxnvf8ayqyh6xqs9cxp1vhn7c1px1zfrz1xbis1lzcn6gag")))

(define-public crate-trove-classifiers-0.18.0 (c (n "trove-classifiers") (v "0.18.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0rvx2jfj808r7bishmzdssawv2xjbvjwmgiarwggv2q3k8z7vb1f")))

(define-public crate-trove-classifiers-0.19.0 (c (n "trove-classifiers") (v "0.19.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0i917mlyi2pi8kq3cclxgyyjm6ww6r28srmsh9gkkmyqv98jyj3k")))

(define-public crate-trove-classifiers-0.20.0 (c (n "trove-classifiers") (v "0.20.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "18na3fya3zpxghb9314lhlbwqi4q1irlfacc18996jc9dcr5w61k")))

(define-public crate-trove-classifiers-0.21.0 (c (n "trove-classifiers") (v "0.21.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0473k4zr2gglyr1g6yjdgfjh73c297wh8abv41fc1315kjmgwglx")))

(define-public crate-trove-classifiers-0.22.0 (c (n "trove-classifiers") (v "0.22.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0k7syw512pxsk54v0cdbmb562984s2v7lqjjxzah9yy42z9q9yfx")))

(define-public crate-trove-classifiers-0.23.0 (c (n "trove-classifiers") (v "0.23.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1mcmh92rz5j8q7za17i04lci8n2nz5kqj0gzif3k52hljqj506fb")))

(define-public crate-trove-classifiers-0.24.0 (c (n "trove-classifiers") (v "0.24.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1nl4v1fgbn5ss5knc8d2lbs0xks3l2mj8lwl6a1kp42jq2jz7g72")))

(define-public crate-trove-classifiers-0.25.0 (c (n "trove-classifiers") (v "0.25.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0ksh1f7vzpfz3wqyj8z1a62g1y2nb2yrmi2fg6ljv84g4g22nxgq")))

(define-public crate-trove-classifiers-0.26.0 (c (n "trove-classifiers") (v "0.26.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0iricsnibv6hz9shhqvj3gvxd6h1zagjailwb75b9jzmhg2sgsaf")))

(define-public crate-trove-classifiers-0.27.0 (c (n "trove-classifiers") (v "0.27.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1fp8mzsli5q5nrk90qwd0463daa01yh97mr30zpj8q6cqjvdg576")))

(define-public crate-trove-classifiers-0.28.0 (c (n "trove-classifiers") (v "0.28.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1c83pz50mrzqqyr4bkb5bi4g48b4gw9hnzk7shxhpbcm0lh91w7k")))

(define-public crate-trove-classifiers-0.29.0 (c (n "trove-classifiers") (v "0.29.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "02mnnhrb0pkv75jv4vmpv62fyshkhihwm670hvdsxnmz0kz3wxhl")))

(define-public crate-trove-classifiers-0.30.0 (c (n "trove-classifiers") (v "0.30.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1nhgxfb61mggp4ygnf6c4qarcmbdf82lg5j6q1knmdal3i297np8")))

(define-public crate-trove-classifiers-0.31.0 (c (n "trove-classifiers") (v "0.31.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "17f273zpj9b5iyp1wsj2ypsbn8j2i2k007dfq7y8pn1wbjdm319i")))

(define-public crate-trove-classifiers-0.32.0 (c (n "trove-classifiers") (v "0.32.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "03c6ar2his1q8bn0j5r04fc40p9bm6fxndrc2jic7a4yfx7z2jk9")))

(define-public crate-trove-classifiers-0.33.0 (c (n "trove-classifiers") (v "0.33.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "03lb7hqnnhydp4gdii0v4mw2ff3g0g9gp1kqicgxl17icgbng6h1")))

(define-public crate-trove-classifiers-0.34.0 (c (n "trove-classifiers") (v "0.34.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "00qqhibycncqna1nhp601x76ppcg62dl6m6gxhqdq5a6dsfm5771")))

(define-public crate-trove-classifiers-0.35.0 (c (n "trove-classifiers") (v "0.35.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0zppz9c15p7igg7brg4qwkb6h6hga1rq0mvcg8983r35ni1an20b")))

(define-public crate-trove-classifiers-0.36.0 (c (n "trove-classifiers") (v "0.36.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0ljidh17a9bi400flk8bdc14zf74017j0qadf4dbyy5188wm1k0v")))

(define-public crate-trove-classifiers-0.37.0 (c (n "trove-classifiers") (v "0.37.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0qxwnhk99cwm8p282hpjhbmkcglwyfmfv8vl07dy00kha3x8r7k7")))

(define-public crate-trove-classifiers-0.38.0 (c (n "trove-classifiers") (v "0.38.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "16kqdpssgv1bn7q5cdqcks9rah4nvzki0zxlafjqk9cm1lm91ggh")))

(define-public crate-trove-classifiers-0.39.0 (c (n "trove-classifiers") (v "0.39.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0jannzxldw6a6mnjdald2xyihck6i2c3ga6j8dv8p000zfpzh3r3")))

(define-public crate-trove-classifiers-0.40.0 (c (n "trove-classifiers") (v "0.40.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1p8jsfr3yfi7pfy48whqk4n9fzdxcyqkhq3n7ik13zc2q2sf5frp")))

(define-public crate-trove-classifiers-0.41.0 (c (n "trove-classifiers") (v "0.41.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1f9zl7mvynr8g2z7k4xqjifrp19n9ifd3dw8dgy4h1nxl06a39hd")))

(define-public crate-trove-classifiers-0.42.0 (c (n "trove-classifiers") (v "0.42.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "03yyi6zikczdfsar42q21alhzj410j30h8j9nmaa596dd91g8mwi")))

(define-public crate-trove-classifiers-0.43.0 (c (n "trove-classifiers") (v "0.43.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "06zm3ag9rmq90pkll2qmqfi4pv2hlpraj1dc6i8fjpphll9xlapm")))

(define-public crate-trove-classifiers-0.44.0 (c (n "trove-classifiers") (v "0.44.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1dxfggyipyizcjqlxfzzzk9dkm8ka9ylplkvrdsxg4xwrinhzwwy")))

(define-public crate-trove-classifiers-0.45.0 (c (n "trove-classifiers") (v "0.45.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1bkk03fhhpviy68mbf52y0yymg911pmj5jxqzgzwbcx7415m8if4")))

(define-public crate-trove-classifiers-0.46.0 (c (n "trove-classifiers") (v "0.46.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "06kxbgn3inrrw5xrii9v7va7dzr2g964q1n9klf6i0jfv5n5il93")))

(define-public crate-trove-classifiers-0.47.0 (c (n "trove-classifiers") (v "0.47.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "10mn0xdwhafiirkcclfga0mf39v4g17pgwlg0bkjbysj59w2hbhz")))

(define-public crate-trove-classifiers-0.48.0 (c (n "trove-classifiers") (v "0.48.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1b8idpng6mmxbpbgh9817d5mci35kzysz9igb30b8paw4kqm1qfk")))

(define-public crate-trove-classifiers-0.49.0 (c (n "trove-classifiers") (v "0.49.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0051qr92jvd9zvpqhjmhw1r4iykrrc8llglblg725m4p0xdw42pd")))

(define-public crate-trove-classifiers-0.50.0 (c (n "trove-classifiers") (v "0.50.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "11hhbn6mqpvx10slsg6i8nkrfcmkc48cyx2gdyy6yibg1z04bask")))

(define-public crate-trove-classifiers-0.51.0 (c (n "trove-classifiers") (v "0.51.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1gsdlkx38z2md8vs3m6qq2b6fd4nvigasyv8scd30i444mldhb7k")))

(define-public crate-trove-classifiers-0.52.0 (c (n "trove-classifiers") (v "0.52.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "19d4slxggv8ay8968pmwfcdiz08hfpyzbawwx0ni0s69ygiavsjx")))

(define-public crate-trove-classifiers-0.53.0 (c (n "trove-classifiers") (v "0.53.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0s2kbq21zm4kfzvjdzfrpn55yij5vz203vfvwpcqf4vqvd53s27g")))

(define-public crate-trove-classifiers-0.54.0 (c (n "trove-classifiers") (v "0.54.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0820f6axpjfgx40pjxpdmyacj0kdarv5d2zx2drm4b6n6mxnq5lb")))

(define-public crate-trove-classifiers-0.55.0 (c (n "trove-classifiers") (v "0.55.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1zm3zj1gykdkq25zvcxk26ssks74fss51g6scrfx5i69a2y7bzk0")))

(define-public crate-trove-classifiers-0.56.0 (c (n "trove-classifiers") (v "0.56.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1dy3gplvsffq6x370qddq5r36aw3dpddhw4w769kh4dpl7y9a5p1")))

(define-public crate-trove-classifiers-0.57.0 (c (n "trove-classifiers") (v "0.57.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "07am7l6a6fb1rhpn5qnd7ifkilmjaz8xkvdaimxfbia4610i86yg")))

(define-public crate-trove-classifiers-0.58.0 (c (n "trove-classifiers") (v "0.58.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "01wh6ayd5xf9q6bnzj22vjd3zss6yn7ryjc1lsc283san57pqnb4")))

(define-public crate-trove-classifiers-0.59.0 (c (n "trove-classifiers") (v "0.59.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0gm8mz67vpgrwgh4xw6c56viwrsqix2vfs01q76sl61fspyvqrsm")))

(define-public crate-trove-classifiers-0.60.0 (c (n "trove-classifiers") (v "0.60.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "026madmcbf5y5arb9svkr26lq0m5lqav1i1s3c5fjcjbxf2y63kg")))

(define-public crate-trove-classifiers-0.61.0 (c (n "trove-classifiers") (v "0.61.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0rndcazy2i43j20bzapyjn65g730rp7r3vbpy4qq4ikr25wqcbly")))

(define-public crate-trove-classifiers-0.62.0 (c (n "trove-classifiers") (v "0.62.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1npn9jfclx9aa5byrwy1hngqiq8rbnxxskwr2b5rbqp4yzpz3asn")))

(define-public crate-trove-classifiers-0.63.0 (c (n "trove-classifiers") (v "0.63.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "11hrh91vl73ynh2h5i2w35zb9lf23gxg1k1sy5y99c0bfsp5fp5p")))

(define-public crate-trove-classifiers-0.64.0 (c (n "trove-classifiers") (v "0.64.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1lljzmav9bds15g751nkm4mf41g3dkdjc0z0ivcp5459jg60pny5")))

