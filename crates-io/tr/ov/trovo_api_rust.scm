(define-module (crates-io tr ov trovo_api_rust) #:use-module (crates-io))

(define-public crate-trovo_api_rust-0.1.0 (c (n "trovo_api_rust") (v "0.1.0") (d (list (d (n "sentry") (r "^0.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01l301dqw7sslpxd8z5ann72kfrhncc4ynag1a49nb6pgxdmbk33")))

