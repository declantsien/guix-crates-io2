(define-module (crates-io tr n- trn-pact) #:use-module (crates-io))

(define-public crate-trn-pact-0.1.0 (c (n "trn-pact") (v "0.1.0") (d (list (d (n "bit_reverse") (r "^0.1.8") (k 0)))) (h "0l8f8r4x5pl17cdm4lyzdww61bk0hqirkc4yfqdplmzfrcwqp7iw") (f (quote (("std") ("default" "std"))))))

(define-public crate-trn-pact-0.2.0 (c (n "trn-pact") (v "0.2.0") (d (list (d (n "bit_reverse") (r "^0.1.8") (k 0)))) (h "0wgarf3n9mah2b9fxwrsvs3880p5jbkkknv3jb33awn6qg749566") (f (quote (("std") ("default" "std"))))))

(define-public crate-trn-pact-0.2.1 (c (n "trn-pact") (v "0.2.1") (d (list (d (n "bit_reverse") (r "^0.1.8") (k 0)))) (h "15p0fzcyb5x1r7v1ivdpjwi24fxif2lhlah0i1n6y6xa5sw0hydz") (f (quote (("std") ("default" "std"))))))

