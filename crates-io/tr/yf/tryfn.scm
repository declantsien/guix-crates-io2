(define-module (crates-io tr yf tryfn) #:use-module (crates-io))

(define-public crate-tryfn-0.1.0 (c (n "tryfn") (v "0.1.0") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.5.11") (k 0)))) (h "1abv403d2m72qasjs8y1sz6rxy41m0ssxm65kh8hz152c2y5riry") (f (quote (("diff" "snapbox/diff") ("default" "color-auto" "diff") ("color-auto" "snapbox/color-auto") ("color" "snapbox/color")))) (r "1.65")))

(define-public crate-tryfn-0.2.0 (c (n "tryfn") (v "0.2.0") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.5.12") (k 0)))) (h "19l1cari99vbhnc5ycjr3v8a42phla04rjr0qw7mjy7llgfyi4hf") (f (quote (("diff" "snapbox/diff") ("default" "color-auto" "diff") ("color-auto" "snapbox/color-auto") ("color" "snapbox/color")))) (r "1.65")))

(define-public crate-tryfn-0.2.1 (c (n "tryfn") (v "0.2.1") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.6.0") (k 0)))) (h "0fa8qvnv3ypzlyjjbbfz6crirl0v5bipns32yxil7f9b66816gj9") (f (quote (("diff" "snapbox/diff") ("default" "color-auto" "diff") ("color-auto" "snapbox/color-auto") ("color" "snapbox/color")))) (r "1.65")))

