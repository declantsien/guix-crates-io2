(define-module (crates-io tr yf tryfrom-via-fromstr) #:use-module (crates-io))

(define-public crate-tryfrom-via-fromstr-0.1.0 (c (n "tryfrom-via-fromstr") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "1sv2n6wrgw81l6v3b3jpafcsivq0fncm9a4p4kw7nw9r9i34kfbd")))

(define-public crate-tryfrom-via-fromstr-0.1.1 (c (n "tryfrom-via-fromstr") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "0c5z166hh3w9b6kf0wfr5mnpd2y2q6shhlla26d78q5s8w6vxiqw")))

