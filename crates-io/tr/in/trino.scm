(define-module (crates-io tr in trino) #:use-module (crates-io))

(define-public crate-trino-0.1.0 (c (n "trino") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12f43a66ay7jkmrfcpk7fpf51gbw3iyr942bra8cs042nl630dxa")))

