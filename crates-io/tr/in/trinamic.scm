(define-module (crates-io tr in trinamic) #:use-module (crates-io))

(define-public crate-trinamic-0.0.0-beta0 (c (n "trinamic") (v "0.0.0-beta0") (d (list (d (n "bxcan") (r "^0.4.0") (d #t) (k 0)) (d (n "embassy-traits") (r "^0.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1v7i40ja2qr52my1b6678yhdm7h363d6yx3sn79v1jvvirzp9r6h")))

(define-public crate-trinamic-0.0.0-beta1 (c (n "trinamic") (v "0.0.0-beta1") (d (list (d (n "bxcan") (r "^0.4.0") (d #t) (k 0)) (d (n "embassy-traits") (r "^0.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "067jydjnlzi4bga0y631fyj873w2frq3h0p09nmki16jxanya3qi")))

(define-public crate-trinamic-0.0.0-beta2 (c (n "trinamic") (v "0.0.0-beta2") (d (list (d (n "bxcan") (r "^0.5.0") (d #t) (k 0)) (d (n "embassy-traits") (r "^0.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1dl72c417j46ws5wl96kl5s0qw79m666658d9pi39q8bhbnr6h6i")))

