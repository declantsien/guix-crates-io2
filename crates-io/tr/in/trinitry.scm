(define-module (crates-io tr in trinitry) #:use-module (crates-io))

(define-public crate-trinitry-0.0.0 (c (n "trinitry") (v "0.0.0") (h "0d8v76n7ly5a4gb039iqk1ygfm9ig4nlhw5vw2xrqyays4gjjqz4")))

(define-public crate-trinitry-0.1.0 (c (n "trinitry") (v "0.1.0") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (f (quote ("grammar-extras"))) (d #t) (k 0)))) (h "1m41sy2h2cjbxiwm61db1w82wnkjxj30x937nl5g7c64j961nqhf")))

(define-public crate-trinitry-0.2.0 (c (n "trinitry") (v "0.2.0") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (f (quote ("grammar-extras"))) (d #t) (k 0)))) (h "1xgg8a1f7wm504v8440bpk2f4zm1p2hx8wbi4afg3a89vidszr7y")))

(define-public crate-trinitry-0.2.1 (c (n "trinitry") (v "0.2.1") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (f (quote ("grammar-extras"))) (d #t) (k 0)))) (h "1l2ngw9kyrfyi26fkaynkmj2g8f0vc0fllwjzvbmxf146phkyc47")))

