(define-module (crates-io tr in trin-utils) #:use-module (crates-io))

(define-public crate-trin-utils-0.1.1-alpha.1 (c (n "trin-utils") (v "0.1.1-alpha.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (t "cfg(windows)") (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "1qg8hzzk0f2wig9pgs3viqmc3j1rrm35nv3hpdlrrg57a7hxbi39") (y #t)))

