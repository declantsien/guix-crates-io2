(define-module (crates-io tr in trinket) #:use-module (crates-io))

(define-public crate-trinket-0.1.0 (c (n "trinket") (v "0.1.0") (d (list (d (n "gettext-rs") (r "^0.6") (f (quote ("gettext-system"))) (d #t) (k 0)) (d (n "gtk") (r "^0.2") (d #t) (k 0) (p "gtk4")) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "1x0db92glriwzffblmhiq0w7np56z1945azfnzpzvxy7j16ck82l") (f (quote (("bindings" "libc"))))))

(define-public crate-trinket-0.1.1 (c (n "trinket") (v "0.1.1") (d (list (d (n "gtk") (r "^0.2") (d #t) (k 0) (p "gtk4")) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "qrcode") (r "^0.12") (d #t) (k 0)))) (h "08wpv5cpa1r3gpvrqfb7a6czyfpdv1l238pg0ff9blfzax1nha3s") (f (quote (("bindings"))))))

