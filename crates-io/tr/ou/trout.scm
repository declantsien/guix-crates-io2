(define-module (crates-io tr ou trout) #:use-module (crates-io))

(define-public crate-trout-0.1.0 (c (n "trout") (v "0.1.0") (d (list (d (n "hyper") (r "^0.13.5") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 2)))) (h "0gr8jsv94mr390hzyr9487r7asm19psgdjfm93ch9s12hjcsccrs") (f (quote (("default" "hyper"))))))

(define-public crate-trout-0.1.1 (c (n "trout") (v "0.1.1") (d (list (d (n "hyper") (r "^0.13.5") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 2)))) (h "0ya6gdb1p1x517sfxfdpr94i1d53b314jan3yynadww8d7661vcp") (f (quote (("default" "hyper"))))))

(define-public crate-trout-0.2.0 (c (n "trout") (v "0.2.0") (d (list (d (n "hyper") (r "^0.13.5") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 2)))) (h "1qp4qj0d7yfif42an96pwzwammqlm4zvxn0s365bpbkg63ffn9f5") (f (quote (("default" "hyper"))))))

(define-public crate-trout-0.3.0 (c (n "trout") (v "0.3.0") (d (list (d (n "hyper") (r "^0.13.5") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 2)))) (h "14zils1x6hlyfw8zbzw82v43ahpf50khms3yf5591ygj8dq93yma") (f (quote (("default" "hyper"))))))

(define-public crate-trout-0.4.0 (c (n "trout") (v "0.4.0") (d (list (d (n "http02") (r "^0.2.6") (o #t) (d #t) (k 0) (p "http")) (d (n "hyper") (r "^0.14.16") (f (quote ("server" "tcp" "http1"))) (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0sms095kfv45i3qx1x7577v9hlv08yj8ax3hqwbhfg9k28rldvln") (f (quote (("default" "http02"))))))

