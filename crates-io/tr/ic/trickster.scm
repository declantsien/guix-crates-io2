(define-module (crates-io tr ic trickster) #:use-module (crates-io))

(define-public crate-trickster-0.0.1 (c (n "trickster") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "0s7l1s8xn26nzb74xw8k3x5ipclngvp5q1p791sp80yc611jm2b7")))

(define-public crate-trickster-0.0.2 (c (n "trickster") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1r3w84v0yxp4dcvijlvnr39a45abv7i8b7mxis7izybxg0d4cjyb")))

(define-public crate-trickster-0.0.3 (c (n "trickster") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "0n369cf5cldh407hsjajka0j0vis74fq6qh3q9vyfnpfkygiz6m0")))

(define-public crate-trickster-0.0.4 (c (n "trickster") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.5") (d #t) (k 0)))) (h "0vfz34zn8yp8xxvvv2s0qb68q2b8083ld4i1i9v9llf35pxzsd7c")))

(define-public crate-trickster-0.0.5 (c (n "trickster") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.5") (d #t) (k 0)))) (h "1rqy4jp3w0cx0j1zj7p19ipifi3xlkyq43kda77rsnqcsx83gxq2")))

(define-public crate-trickster-0.0.6 (c (n "trickster") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.5") (d #t) (k 0)))) (h "0cs75alpdy13b0b8vrw2ckvgad56na7q1nws0bbd5zbnsx0zxpcm") (f (quote (("default" "byteorder-utils") ("byteorder-utils" "byteorder"))))))

