(define-module (crates-io tr ic trice) #:use-module (crates-io))

(define-public crate-trice-0.1.0 (c (n "trice") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.56") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.56") (f (quote ("Window" "Performance" "PerformanceTiming"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1vq9jjf9j2wl2d1wk15lnlyimkkd037dpvn3vb2gzl53mc8a38r7")))

(define-public crate-trice-0.2.0 (c (n "trice") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3.56") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.56") (f (quote ("Window" "Performance" "PerformanceTiming"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0cl1arhs9iz3fiy15yfdgljd985bvmngcbbzqy3wbg6r0k6srmkg")))

(define-public crate-trice-0.3.0 (c (n "trice") (v "0.3.0") (d (list (d (n "js-sys") (r "^0.3.56") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.56") (f (quote ("Window" "Performance" "PerformanceTiming"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0hh0i8ajbw9lwfziam2jr8lji50lb24rvv9jf4y28blavv1frv2b")))

(define-public crate-trice-0.3.1 (c (n "trice") (v "0.3.1") (d (list (d (n "js-sys") (r "^0.3.61") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("Window" "Performance" "PerformanceTiming"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0hsx0qddmcvy6d2k19nqlqns5i9sbcyybf9cp5ap59fwq78lrak1")))

(define-public crate-trice-0.4.0 (c (n "trice") (v "0.4.0") (d (list (d (n "js-sys") (r "^0.3.66") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("Window" "Performance" "PerformanceTiming"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0njibc87nlz8674zwdwhyd2hilhgdzsjnxcjyc80pb4zmq8apank")))

