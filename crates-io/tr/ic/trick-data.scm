(define-module (crates-io tr ic trick-data) #:use-module (crates-io))

(define-public crate-trick-data-0.1.0 (c (n "trick-data") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "071fanllr9rrsff9chwa3x1w2817rihnzdl7p5ab8dikkc84cqp5")))

