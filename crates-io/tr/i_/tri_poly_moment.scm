(define-module (crates-io tr i_ tri_poly_moment) #:use-module (crates-io))

(define-public crate-tri_poly_moment-0.1.0 (c (n "tri_poly_moment") (v "0.1.0") (h "1h9218in6x40c1hmjsz0ixi3bfzgmybdvm6b0vb25ail6vvk4ncm")))

(define-public crate-tri_poly_moment-0.1.1 (c (n "tri_poly_moment") (v "0.1.1") (h "1jq8h98km550b7161fbiadq9f7wqrl1cd8nx0149wzg1fb91xsf0")))

