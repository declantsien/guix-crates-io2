(define-module (crates-io tr i_ tri_avltree) #:use-module (crates-io))

(define-public crate-tri_avltree-0.1.0 (c (n "tri_avltree") (v "0.1.0") (d (list (d (n "rustc-hash") (r "^1.0") (k 0)))) (h "0n97jr1ahl3jw36j0bh5rhz5svvpj6rn8brciwq0gqvlhxbmg3l1") (y #t)))

(define-public crate-tri_avltree-0.2.0 (c (n "tri_avltree") (v "0.2.0") (d (list (d (n "rustc-hash") (r "^1.0") (k 0)))) (h "04jw9yyhqv4gknc0m5iivzmiclnsiwbv9m9kf0h9ppfmglpw6c37") (y #t)))

(define-public crate-tri_avltree-0.3.0 (c (n "tri_avltree") (v "0.3.0") (d (list (d (n "rustc-hash") (r "^1.0") (k 0)))) (h "1f3n357bsxqa14hy0xs06g6jwswq0k9iwi2pgr5qcgs6d3803ikx") (y #t)))

(define-public crate-tri_avltree-0.4.0 (c (n "tri_avltree") (v "0.4.0") (d (list (d (n "rustc-hash") (r "^1.0") (k 0)))) (h "001im8a16zdjlbg6ndyipl0vi5ag5abkz88ifsjshx26djajc5x5") (y #t)))

(define-public crate-tri_avltree-0.4.1 (c (n "tri_avltree") (v "0.4.1") (d (list (d (n "rustc-hash") (r "^1.0") (k 0)))) (h "0xsq5qff2yqqfcriiz4yv34n9s11pgy3k4bqr85gds7chpd8w84b") (y #t)))

(define-public crate-tri_avltree-0.4.2 (c (n "tri_avltree") (v "0.4.2") (d (list (d (n "rustc-hash") (r "^1.0") (k 0)))) (h "1r5vpxjm1mmf79g39wiz89m1a336pxz7dr015ls38gjyp0ywlx9w") (y #t)))

(define-public crate-tri_avltree-0.4.3 (c (n "tri_avltree") (v "0.4.3") (d (list (d (n "rustc-hash") (r "^1.0") (k 0)))) (h "1mmivmmi0z17y2ykzr05p3hh2n6rdxj5njswklskc1b2m7lc0k1b") (y #t)))

(define-public crate-tri_avltree-0.4.4 (c (n "tri_avltree") (v "0.4.4") (d (list (d (n "rustc-hash") (r "^1.0") (k 0)))) (h "10cranvfyansbixa75k4fc7z3vk3623rqnskvgnqniajfs6bdx3w") (y #t)))

(define-public crate-tri_avltree-0.4.5 (c (n "tri_avltree") (v "0.4.5") (d (list (d (n "rustc-hash") (r "^1.0") (k 0)))) (h "01cdydmy3rf3pzvdizp4v9py0nxcm0sbbg8zbx18sjwyif1b7wg7") (y #t)))

(define-public crate-tri_avltree-0.4.6 (c (n "tri_avltree") (v "0.4.6") (d (list (d (n "rustc-hash") (r "^1.0") (k 0)))) (h "138sqjqgq6c39vgda6i8zjbmp7vr1lv2swailg41cpfwkrs6lfkc") (y #t)))

(define-public crate-tri_avltree-0.4.7 (c (n "tri_avltree") (v "0.4.7") (d (list (d (n "rustc-hash") (r "^1.0") (k 0)))) (h "16ll53i4igh62pjajbxlmziblz2pmdv84riprw2ifxpc7x0a52ay") (y #t)))

