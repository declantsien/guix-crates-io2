(define-module (crates-io tr ek trek) #:use-module (crates-io))

(define-public crate-trek-0.2.0 (c (n "trek") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "docopt") (r "^0.6.72") (d #t) (k 2)) (d (n "postgres") (r "^0.10.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 2)))) (h "0g821n6k7ln098g4hdv1q5g67nw57gd0m13jv1c3p5rl6bkjq151")))

(define-public crate-trek-0.2.1 (c (n "trek") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "docopt") (r "^0.6.72") (d #t) (k 2)) (d (n "postgres") (r "^0.10.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 2)))) (h "1mcxmfr9k0jzgidbyjwz1lqwg1az8n1hpp5dbb4vgrr3srycrlra")))

(define-public crate-trek-0.2.2 (c (n "trek") (v "0.2.2") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "docopt") (r "^0.6.72") (d #t) (k 2)) (d (n "postgres") (r "^0.11.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 2)))) (h "1rw956dmxwa1648fwds6829g71x0fi6hn1svdglv2z6isb5xm2aq")))

(define-public crate-trek-0.3.0 (c (n "trek") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "docopt") (r "^0.6.72") (d #t) (k 2)) (d (n "postgres") (r "^0.13.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 2)))) (h "0kwsgprn8ay7dmw8s204qgjg7d576x6avyi8zn8zrb0pcnkb4kg8")))

(define-public crate-trek-0.3.1 (c (n "trek") (v "0.3.1") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "docopt") (r "^0.6.72") (d #t) (k 2)) (d (n "postgres") (r "^0.15.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 2)))) (h "1h4fw9n06dn12yqmpf4hwdqslsr0zcxwf6acy5cqwk3h71rl9wc3")))

