(define-module (crates-io tr ek trek-router) #:use-module (crates-io))

(define-public crate-trek-router-0.0.1 (c (n "trek-router") (v "0.0.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 2)) (d (n "path-tree") (r "^0.1") (d #t) (k 0)))) (h "18iqc7zm0bmkzm906fga4nss8mmvs055hsp0hd43n04c7jwzmmsb")))

(define-public crate-trek-router-0.0.2 (c (n "trek-router") (v "0.0.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "path-tree") (r "^0.1") (d #t) (k 0)))) (h "1fibmff49maq7q3nf4q0vwmff8l00hqbfazvcn67pq53698j5pqx")))

