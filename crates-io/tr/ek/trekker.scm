(define-module (crates-io tr ek trekker) #:use-module (crates-io))

(define-public crate-trekker-0.2.2 (c (n "trekker") (v "0.2.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)) (d (n "seahorse") (r "^2.1.0") (d #t) (k 0)))) (h "0zrjq520nf9443rbxrr61h4ld1nd7zs398np3rz3kqzwy7x59387")))

