(define-module (crates-io tr uf trufflehunter) #:use-module (crates-io))

(define-public crate-trufflehunter-0.1.0 (c (n "trufflehunter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "larry") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "two_timer") (r "^1") (d #t) (k 0)))) (h "0yaylf7cdhiwj5dgfallh3plr6yc4ldh4gn0fs69qa0kplhyr8wh")))

(define-public crate-trufflehunter-0.1.1 (c (n "trufflehunter") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "larry") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "two_timer") (r "^1") (d #t) (k 0)))) (h "0gk4qr26bdzw8cp6q6gf33vf0iwsgx6akagb45m7fa8y9f0w7llx")))

