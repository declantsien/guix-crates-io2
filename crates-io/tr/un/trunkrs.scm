(define-module (crates-io tr un trunkrs) #:use-module (crates-io))

(define-public crate-trunkrs-0.1.0 (c (n "trunkrs") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0lbhi148la70b30zzc13yrhy293rd5szpfq6d8c1nfvxbz7ccrm6")))

(define-public crate-trunkrs-0.2.0 (c (n "trunkrs") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0q2csajjkmfrh671b3zmma1mjnrl828xax3zl6zv9lvyqb2kj6fy")))

(define-public crate-trunkrs-0.2.5 (c (n "trunkrs") (v "0.2.5") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0l0s9f87nkz1bc67v6lragd7rnk5hfik5rgmql31f6cm6y4nqmcw")))

(define-public crate-trunkrs-0.2.6 (c (n "trunkrs") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0h3bc2rvl9s1j4y5k4rgxzy8dvdz4m0q1313rvd3qw4fsl3jfxa9")))

(define-public crate-trunkrs-0.2.7 (c (n "trunkrs") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "14nyiskzjhc9839d2b07xwa4g3rxh36lj8ygp3fy5ylskwwnazk2")))

