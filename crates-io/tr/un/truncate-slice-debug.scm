(define-module (crates-io tr un truncate-slice-debug) #:use-module (crates-io))

(define-public crate-truncate-slice-debug-0.1.0 (c (n "truncate-slice-debug") (v "0.1.0") (h "1fpc0i2n1wyvzl9z0qd6c2z9rayvflyrp8spykbkgwfxxpbykkh8")))

(define-public crate-truncate-slice-debug-0.1.1 (c (n "truncate-slice-debug") (v "0.1.1") (h "1y4az6jsn5wiv8s57qcyiw70vw4lrffavk0v0gp8s02bnymic6px")))

