(define-module (crates-io tr un trunc8) #:use-module (crates-io))

(define-public crate-trunc8-0.1.0 (c (n "trunc8") (v "0.1.0") (h "0xl59z2h7jlwhw7giffilw3f33sgway7szgyw9w03mrx2rlv77zr")))

(define-public crate-trunc8-0.2.0 (c (n "trunc8") (v "0.2.0") (h "1zgbgp68b3rk93cvfn088nh2c2pbfq9043a6mr203vkyymhafjh4")))

