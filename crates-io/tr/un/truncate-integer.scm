(define-module (crates-io tr un truncate-integer) #:use-module (crates-io))

(define-public crate-truncate-integer-0.5.0 (c (n "truncate-integer") (v "0.5.0") (h "0agx2rb71kcwrjqdwa2xqs1mxbx9rba78f30vqvvwb8ym1a4bfpq") (r "1.47")))

(define-public crate-truncate-integer-0.5.1 (c (n "truncate-integer") (v "0.5.1") (h "0i0qf6ffsrxhvx45iazhc47z7i7aj1v4g75j729ywwf4iz8f8ky1") (r "1.47")))

