(define-module (crates-io tr un truncate_string_at_whitespace) #:use-module (crates-io))

(define-public crate-truncate_string_at_whitespace-1.0.0 (c (n "truncate_string_at_whitespace") (v "1.0.0") (h "128k7i70frzan2yir3589262z1p7i6smmnchv88fm7ld655c79l8")))

(define-public crate-truncate_string_at_whitespace-1.0.1 (c (n "truncate_string_at_whitespace") (v "1.0.1") (h "1zcyr9hsw78m3panmaihs3n9gbxq9lzln1rsvgq1m26ikcb0gsf1")))

(define-public crate-truncate_string_at_whitespace-1.0.2 (c (n "truncate_string_at_whitespace") (v "1.0.2") (h "172d083cq5hm06zzjjas0h0ia92ksdivydv4lj3hriz0cga6y0wg")))

(define-public crate-truncate_string_at_whitespace-1.0.3 (c (n "truncate_string_at_whitespace") (v "1.0.3") (h "03ann98q5m170ni193a4ckn93cvravhb8xhsm21mr0h8dhazyz4x")))

