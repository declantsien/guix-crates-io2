(define-module (crates-io tr un trundler) #:use-module (crates-io))

(define-public crate-trundler-0.1.0 (c (n "trundler") (v "0.1.0") (h "170fbq7b1j8vh0c6a6nl6ygxlidb3z5y7vcc3qx6gljjq6vy4lsd")))

(define-public crate-trundler-0.2.0 (c (n "trundler") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8") (d #t) (k 0)) (d (n "graphql_client") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.17") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1hdws0kz8dw3n07rpw2xpyjimww9p6yyv6s91pkdv9n830abkxz2")))

(define-public crate-trundler-0.3.0 (c (n "trundler") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "graphql_client") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.17") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1xl2k8af9aavyn39zpdnkr97xk20b19sz9hjzpjwldm3lmblww9c")))

(define-public crate-trundler-0.4.0 (c (n "trundler") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "graphql_client") (r "^0.13") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.17") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0b4l77gm4174p4x1q92g83fw0x0ywir5q4i6rhbdpdypaw2rq6l6")))

