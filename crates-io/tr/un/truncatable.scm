(define-module (crates-io tr un truncatable) #:use-module (crates-io))

(define-public crate-truncatable-0.1.0 (c (n "truncatable") (v "0.1.0") (h "1aa8dxghsx0zg6nn27f1hxxvj6qv26w5177zmccvl9q9cmbag7br")))

(define-public crate-truncatable-0.1.1 (c (n "truncatable") (v "0.1.1") (h "1cpm7qmd02sg5cfanipsy97qb8np65hmszran8aizgcgaf14s5c9")))

(define-public crate-truncatable-0.1.2 (c (n "truncatable") (v "0.1.2") (h "1ghqhldqf0xzza8lsq3pnw4yjjdzry21kxapwpbxy1q7ziaxricj")))

(define-public crate-truncatable-0.1.3 (c (n "truncatable") (v "0.1.3") (h "1kkkgqkc4pvyn6arm78z87ix3lanjrlj3pfsx9mxqj3xqywb7c0v")))

