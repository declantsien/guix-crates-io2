(define-module (crates-io tr un truncrate) #:use-module (crates-io))

(define-public crate-truncrate-0.1.0 (c (n "truncrate") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 0)))) (h "0mk3c522hz9wj2fd0pcd2s05lkbzgnkk12wjzb5kzif2lv0q9b7v")))

(define-public crate-truncrate-0.1.1 (c (n "truncrate") (v "0.1.1") (d (list (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 0)))) (h "1cni0n2blg2riz5rfp2vbal9nl5hzimzv9a6aba7ba2v2nmz25r1")))

(define-public crate-truncrate-0.1.2 (c (n "truncrate") (v "0.1.2") (d (list (d (n "unicode-segmentation") (r "^0.1.2") (d #t) (k 0)))) (h "1g1qnqs33zvzd1pgl9s4zj0sizmjhrrm574abzqk3nzjz7qbg9pw") (y #t)))

(define-public crate-truncrate-0.1.3 (c (n "truncrate") (v "0.1.3") (d (list (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 0)))) (h "0ps4fsr6q6zfxl5437azl647d4f9yv1yhsvfan7civkzll1svvkk")))

