(define-module (crates-io tr id trident-derive-fuzz-test-executor) #:use-module (crates-io))

(define-public crate-trident-derive-fuzz-test-executor-0.0.1 (c (n "trident-derive-fuzz-test-executor") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05dsjygdqchx4nalizk5j5anq5pm735as2p93wnlfc4znzzddd2n") (r "1.60")))

