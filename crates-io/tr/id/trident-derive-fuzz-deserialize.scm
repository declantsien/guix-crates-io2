(define-module (crates-io tr id trident-derive-fuzz-deserialize) #:use-module (crates-io))

(define-public crate-trident-derive-fuzz-deserialize-0.0.1 (c (n "trident-derive-fuzz-deserialize") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "16yljqghys1bjp452vkz7764jqpdvlnmbv28zrd44bklhw78fm9g") (r "1.60")))

