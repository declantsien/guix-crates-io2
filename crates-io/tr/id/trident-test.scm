(define-module (crates-io tr id trident-test) #:use-module (crates-io))

(define-public crate-trident-test-0.3.2 (c (n "trident-test") (v "0.3.2") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (k 0)))) (h "1pja526b2q8lf8kfn207i51fpwl6ky7346wryfrmi59c3bj9k1gf")))

