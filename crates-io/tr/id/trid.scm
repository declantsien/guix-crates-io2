(define-module (crates-io tr id trid) #:use-module (crates-io))

(define-public crate-trid-0.1.0-alpha (c (n "trid") (v "0.1.0-alpha") (h "1299nkrwzkrc1vxpv0j13qy2pddnwh4fyw9pf5vn88ccfnpk2lfl")))

(define-public crate-trid-0.1.1-alpha (c (n "trid") (v "0.1.1-alpha") (h "0n8rzzhy8hgl4m6ylv7dkl69v2w301asa3w0hw64aq8p2cy5aks8")))

(define-public crate-trid-0.2.0-alpha (c (n "trid") (v "0.2.0-alpha") (h "0ax9ramh9as856mvdq4l9k31y4m1dnl2sys7kwqhbgqy251l0955")))

(define-public crate-trid-0.2.1-alpha (c (n "trid") (v "0.2.1-alpha") (h "0y4g6rahl2xyl9rwbfsxbhr9qwwcdsbb64lsqshxin794hqik59s")))

(define-public crate-trid-0.2.2-alpha (c (n "trid") (v "0.2.2-alpha") (h "1fbvm7n3c0w6pdhkk8bgx5wp6mcxznhid7z2jrp56b0qxdind9sp")))

(define-public crate-trid-0.3.0-alpha (c (n "trid") (v "0.3.0-alpha") (h "0g0axlncnbh4syl6vcbb7jybwxlyx4ir2vgcrn89s6mhl9v6ys8j")))

(define-public crate-trid-0.3.1-alpha (c (n "trid") (v "0.3.1-alpha") (h "1phj11galr4cpdly67vidyxraaa8lwdl3fwqb02ln9mwi1dw8hd4")))

(define-public crate-trid-0.3.2-alpha (c (n "trid") (v "0.3.2-alpha") (h "1gs3n27s16pp5hv363padlqkbk00kqrn4a6phi68n595sr8xbxz7")))

(define-public crate-trid-0.4.0-alpha (c (n "trid") (v "0.4.0-alpha") (h "0aj3fxc12mi5jzhzcjq40nm1wadmq0r44vqys45y07lza026zfb4")))

(define-public crate-trid-1.0.0 (c (n "trid") (v "1.0.0") (h "1f4zs3pxyh8p5x20niz7maw7mmkpszh78cvmnyn9i7j4i95zva89")))

(define-public crate-trid-2.0.0 (c (n "trid") (v "2.0.0") (h "1hzl3m3bg4xjf3h9w79fi3k4m3x6mkx33c3arc9dl3kplw9gv85m")))

(define-public crate-trid-3.0.0 (c (n "trid") (v "3.0.0") (h "0b8m258q0ij4ls3jb29l3si88w7q5f9c1ijnnqrxyqmxckxqdqa1") (y #t)))

(define-public crate-trid-3.1.0 (c (n "trid") (v "3.1.0") (h "1sw4702w7kjg52w3l57iz7nabxx5g2vb2macigrlnslyz7lgvvh4") (y #t)))

(define-public crate-trid-3.1.1 (c (n "trid") (v "3.1.1") (h "1apady3w12pfnfshijdlr66pvj24gzp7qdpliaxfg6f4vca3irv8")))

(define-public crate-trid-4.0.0 (c (n "trid") (v "4.0.0") (h "1m9sj7mvdkk9vyxb49v27fpa5bafvnmk2byv72fmw04ira6zrk14")))

(define-public crate-trid-5.0.0 (c (n "trid") (v "5.0.0") (h "040ylxrxrxwzdz65qkpifk9wx3v6dcz63qmm6nz1fkq0x13n6859")))

