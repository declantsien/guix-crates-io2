(define-module (crates-io tr id trident-derive-displayix) #:use-module (crates-io))

(define-public crate-trident-derive-displayix-0.0.1 (c (n "trident-derive-displayix") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vqa9qw4h1w6ksxbg0q6dslp9h78rjph7r9mgan2wdmnb8iakfa4") (r "1.60")))

