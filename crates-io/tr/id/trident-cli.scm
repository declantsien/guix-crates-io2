(define-module (crates-io tr id trident-cli) #:use-module (crates-io))

(define-public crate-trident-cli-0.6.0 (c (n "trident-cli") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.45") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (k 0)) (d (n "solana-sdk") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (k 0)) (d (n "trident-client") (r "^0.6.0") (d #t) (k 0)) (d (n "trident-explorer") (r "^0.3.2") (d #t) (k 0)))) (h "0ngip5jcf4gzfkif8ix6yfxhmmqilkb025ybhxp2h6zgc8fq2406")))

