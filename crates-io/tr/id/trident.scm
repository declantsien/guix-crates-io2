(define-module (crates-io tr id trident) #:use-module (crates-io))

(define-public crate-trident-0.1.0 (c (n "trident") (v "0.1.0") (h "1n5nwl5qgmk0qvv79nk1rjgvf3ra6k24v4vs6j9rgm2hfggh1m3p")))

(define-public crate-trident-0.2.0 (c (n "trident") (v "0.2.0") (h "1k1g3gkcmdpyfc0imz8cs5v0i59j9fal8yzcf9wzsr94vhazqqvp")))

(define-public crate-trident-0.2.1 (c (n "trident") (v "0.2.1") (h "0rffwyqxaq2s9bd8l5fipjb7vljzgnfyg8jfa3fcgpnbzylhx5jz")))

