(define-module (crates-io tr ii trii) #:use-module (crates-io))

(define-public crate-trii-0.1.0 (c (n "trii") (v "0.1.0") (h "17i57j5l4mzlj158i7mnk32k29c9is480lnz7n4z4w19rvm9ym3r")))

(define-public crate-trii-0.1.1 (c (n "trii") (v "0.1.1") (h "020n7crn4nn90cypxyl1ym90qqzlmkr46sx092ysqjsljqqw7awk")))

