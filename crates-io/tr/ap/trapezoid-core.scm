(define-module (crates-io tr ap trapezoid-core) #:use-module (crates-io))

(define-public crate-trapezoid-core-0.1.0 (c (n "trapezoid-core") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (k 0)) (d (n "vulkano") (r "^0.33") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.33") (d #t) (k 0)))) (h "1icp17rfnky8b2idp9m9alz9mj0q7qzl1bwygjp0m30zc2l34014") (f (quote (("debugger"))))))

(define-public crate-trapezoid-core-0.1.1 (c (n "trapezoid-core") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (k 0)) (d (n "vulkano") (r "^0.33") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.33") (d #t) (k 0)))) (h "00s7a72lcrnlqwqqszmh43s5bvjgxdl933bz2qf0l4j7yic2j02g") (f (quote (("debugger"))))))

(define-public crate-trapezoid-core-0.1.2 (c (n "trapezoid-core") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (k 0)) (d (n "vulkano") (r "^0.33") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.33") (d #t) (k 0)))) (h "15b5j1q6nwvvqya7iyj8kar378gvs4mx7xm9dyd36n8jqr53np1r") (f (quote (("debugger"))))))

