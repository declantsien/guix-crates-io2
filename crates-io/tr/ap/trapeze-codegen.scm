(define-module (crates-io tr ap trapeze-codegen) #:use-module (crates-io))

(define-public crate-trapeze-codegen-0.1.0 (c (n "trapeze-codegen") (v "0.1.0") (d (list (d (n "prost-build") (r "^0.12") (d #t) (k 0)))) (h "00az2a7md2gvlpvki9i7m1in4bqfizzgl39pb7zayajddsasrn04")))

(define-public crate-trapeze-codegen-0.2.0 (c (n "trapeze-codegen") (v "0.2.0") (d (list (d (n "prost-build") (r "^0.12") (d #t) (k 0)))) (h "1a1ys9xzmmhk1dskqrisk3jnlqp0skip686x6wbnl7b535ayg8k2")))

(define-public crate-trapeze-codegen-0.3.0 (c (n "trapeze-codegen") (v "0.3.0") (d (list (d (n "prost-build") (r "^0.12") (d #t) (k 0)))) (h "0f5656la005ziky6y9v9gnp361gjjvmmsrcdpn9njajykxddw71k")))

(define-public crate-trapeze-codegen-0.4.0 (c (n "trapeze-codegen") (v "0.4.0") (d (list (d (n "prost-build") (r "^0.12") (d #t) (k 0)))) (h "0lk7xi1vz9k03vxvi1j2ks2mnjlcqi9hlsac3hsjjr29ciji3m3r")))

(define-public crate-trapeze-codegen-0.4.1 (c (n "trapeze-codegen") (v "0.4.1") (d (list (d (n "prost-build") (r "^0.12") (d #t) (k 0)))) (h "0lvd5khi685g0g9as89pwmrakw4xykypq61mii8z0lr6mzniiq6x")))

(define-public crate-trapeze-codegen-0.5.0 (c (n "trapeze-codegen") (v "0.5.0") (d (list (d (n "prost-build") (r "^0.12") (d #t) (k 0)))) (h "0aapb1xf210rw98nnd6x8h27vjsa79i1kpkk50fkjmff1hbaldkn")))

(define-public crate-trapeze-codegen-0.5.1 (c (n "trapeze-codegen") (v "0.5.1") (d (list (d (n "prost-build") (r "^0.12") (d #t) (k 0)))) (h "1d3ycdrdyidfldr059y3vx108y5x8f4ljim2rgrxqn4my5ch4zqm")))

(define-public crate-trapeze-codegen-0.5.2 (c (n "trapeze-codegen") (v "0.5.2") (d (list (d (n "prost-build") (r "^0.12") (d #t) (k 0)))) (h "1ggvqnfmb2cx049w68p3hvhymrcps7q7mv391z9ql2d3i292zj60")))

(define-public crate-trapeze-codegen-0.6.0 (c (n "trapeze-codegen") (v "0.6.0") (d (list (d (n "prost-build") (r "^0.12") (d #t) (k 0)))) (h "1wbphy58zmb6zivz93nxxwbymxz2k6jij7ciyminwjdi7cf7amb6")))

