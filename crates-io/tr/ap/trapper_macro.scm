(define-module (crates-io tr ap trapper_macro) #:use-module (crates-io))

(define-public crate-trapper_macro-1.0.0 (c (n "trapper_macro") (v "1.0.0") (d (list (d (n "quote") (r "0.6.*") (d #t) (k 0)) (d (n "syn") (r "0.15.*") (d #t) (k 0)) (d (n "trapper") (r ">= 1.0.0, <= 2.0.0") (d #t) (k 2)))) (h "1xc1zwdh7pn8339fjbbg4ii0yy3zib8p18wbxpni3k3kn7a4x5wh")))

