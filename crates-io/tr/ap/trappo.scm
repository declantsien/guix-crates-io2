(define-module (crates-io tr ap trappo) #:use-module (crates-io))

(define-public crate-trappo-0.1.0 (c (n "trappo") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "docopt") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.2") (d #t) (k 0)))) (h "1kdqgrc9fwmyblllz27yhbq0cdppk164vrwmxlim6p5rzhskbf01")))

