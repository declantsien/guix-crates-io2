(define-module (crates-io tr af trafo) #:use-module (crates-io))

(define-public crate-trafo-0.1.0 (c (n "trafo") (v "0.1.0") (d (list (d (n "getopts") (r "*") (d #t) (k 0)))) (h "1fa341jfw93az15jgy5h67ah03f8q2mzfcm0id37a3v845q8daj8") (y #t)))

(define-public crate-trafo-0.1.1 (c (n "trafo") (v "0.1.1") (d (list (d (n "getopts") (r "*") (d #t) (k 0)))) (h "0xajnlw4nmwysxiyf941yy9rhjmdjii26166zv0blaiyfarsl9fn")))

