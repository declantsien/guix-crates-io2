(define-module (crates-io tr af traffic_router_async) #:use-module (crates-io))

(define-public crate-traffic_router_async-0.1.0 (c (n "traffic_router_async") (v "0.1.0") (d (list (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0v6g62421ddlf42qzn1nk6lvmyx9708i34mky3wcqv1pmz5wrz54")))

(define-public crate-traffic_router_async-0.1.1 (c (n "traffic_router_async") (v "0.1.1") (d (list (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1w9s6s1cyzy0qm2p8kq08igpqwdjmdn9x2kl38s82mg4hl0rbb64")))

