(define-module (crates-io tr af traffic_generator) #:use-module (crates-io))

(define-public crate-traffic_generator-0.1.0 (c (n "traffic_generator") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.0") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "ws") (r "^0.6.0") (d #t) (k 0)))) (h "1s34360k323i1l2qxliav9i09hr7gfb7xsbqdcxygdz14icmv653")))

