(define-module (crates-io tr ee tree-sitter-hexdump) #:use-module (crates-io))

(define-public crate-tree-sitter-hexdump-0.1.0 (c (n "tree-sitter-hexdump") (v "0.1.0") (d (list (d (n "tree-sitter-c2rust") (r "^0.20.9") (o #t) (d #t) (k 0)) (d (n "tree-sitter-standard") (r "^0.20.9") (o #t) (d #t) (k 0) (p "tree-sitter")) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter-wasm-build-tool") (r "^0.1.0") (o #t) (d #t) (k 1)))) (h "18c1552rp69gyvbzgmvicc21j5dyr2hf9b6vx95fg9c9ddvidz1i") (f (quote (("default" "tree-sitter-standard")))) (s 2) (e (quote (("tree-sitter-standard" "dep:tree-sitter-standard") ("tree-sitter-c2rust" "dep:tree-sitter-c2rust" "dep:tree-sitter-wasm-build-tool"))))))

