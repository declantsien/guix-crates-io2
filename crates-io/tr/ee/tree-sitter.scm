(define-module (crates-io tr ee tree-sitter) #:use-module (crates-io))

(define-public crate-tree-sitter-0.1.0 (c (n "tree-sitter") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1a6dvlv8cds81miz1p4h1rxw0axwlp36hhxmpv2djinjs8qxx5vg")))

(define-public crate-tree-sitter-0.1.1 (c (n "tree-sitter") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0c6lfvpavxyvs4mpmdgyxn91hxck1b26nshz5dbq0vi23rwh1mzw")))

(define-public crate-tree-sitter-0.2.0 (c (n "tree-sitter") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1s27jgk6m1dg1g7lm9qxdy9mrjff5nxkbiw77f665nr9g2s39sfw")))

(define-public crate-tree-sitter-0.3.0 (c (n "tree-sitter") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "19vs1mkn4p8dphydw034fkx4pd4mnhf7mdi8ika4ciq43mg3ijdg")))

(define-public crate-tree-sitter-0.3.1 (c (n "tree-sitter") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1q7pzryq1pdrfypdyyq3m7x0h85kgqvdlvl1rdwjp0qa61999wrl")))

(define-public crate-tree-sitter-0.3.2 (c (n "tree-sitter") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15w5yj9gjf7ywkvw8zcd0x5j75ldlzxqx1sy3583kkhvzkr82vhh")))

(define-public crate-tree-sitter-0.3.3 (c (n "tree-sitter") (v "0.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06ir8l2xgl2yzp4zydyw79y8ar33lczck5hib8l1dj2a00gdy6ii")))

(define-public crate-tree-sitter-0.3.4 (c (n "tree-sitter") (v "0.3.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n230ysjfw5bqiqr3izw4fc2bqpx2mpybpxh0bngdkzmg7ijylr2")))

(define-public crate-tree-sitter-0.3.5 (c (n "tree-sitter") (v "0.3.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qdw1qflj1cbhrcp0nyjn0y41lxdbi3zpwi9cl3balmgd3ax97s6")))

(define-public crate-tree-sitter-0.3.6 (c (n "tree-sitter") (v "0.3.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07dl84s3k5mblhr1r08jac6s05z27rvlyzp4dc4gbx3n131jqq7n")))

(define-public crate-tree-sitter-0.3.7 (c (n "tree-sitter") (v "0.3.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ijcvcdxrqa05l4m6m38b8zj65vi6dgkxrwiqjbc8milivjinns8")))

(define-public crate-tree-sitter-0.3.8 (c (n "tree-sitter") (v "0.3.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01yxri4kbw9r09j6bwgf5dldcyl8szbm7jhw5g00lsrc2jr570fv")))

(define-public crate-tree-sitter-0.3.9 (c (n "tree-sitter") (v "0.3.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hc28z8wkpcfcr64cqvb2vs4vzcsylybm2hg0ia7bvh080i34ch2")))

(define-public crate-tree-sitter-0.3.10 (c (n "tree-sitter") (v "0.3.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18pb0jlhp0lr6aczgwmraqmw9bgw3zx9xpiq0mv25r6jan2q98px")))

(define-public crate-tree-sitter-0.4.0 (c (n "tree-sitter") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mijfl145f2c7hwy9ap6x45b8417gdzsk0z16vpckrjyl2gayky3")))

(define-public crate-tree-sitter-0.5.0 (c (n "tree-sitter") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08wbf2al21s1fkwz5q6jyz8p0g08s111lny7slzg4h2cvcr1nqik")))

(define-public crate-tree-sitter-0.6.0 (c (n "tree-sitter") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qd0gpp5hy0c9xhrvcsf9hixf1qns86j68chqq4p80nnlgqq64jy")))

(define-public crate-tree-sitter-0.6.1 (c (n "tree-sitter") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sbd2q9hqwxssq543mbmn8837lkjf5lgrl3rqvh545kl5w6bizsc")))

(define-public crate-tree-sitter-0.6.2 (c (n "tree-sitter") (v "0.6.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jlagadm78r8nagqscb6y547a6hn3p0w1m49l09vds1dzyzix836")))

(define-public crate-tree-sitter-0.6.3 (c (n "tree-sitter") (v "0.6.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1c65218wdgybwmhnlliw29lgsmh6a3r6r0395nnpp9lxnqf1hvip")))

(define-public crate-tree-sitter-0.16.0 (c (n "tree-sitter") (v "0.16.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0bxxdknzainp5anm69jpdv3i8i2nz0phyqd0vw7wa3zdasjs5v02")))

(define-public crate-tree-sitter-0.16.1 (c (n "tree-sitter") (v "0.16.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bv7l6yxcy56mpqgr2l7zc4ngfa9p33xkgfwdsvlyzmhxn03dwqx")))

(define-public crate-tree-sitter-0.17.0 (c (n "tree-sitter") (v "0.17.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0p2bwnfqpilgbs0z3bld56kp13q4f72n8z9ahviwvbn3zrq77vkh")))

(define-public crate-tree-sitter-0.17.1 (c (n "tree-sitter") (v "0.17.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1b7s436zxsbcz655hbhpndllmls6b5j1vl84vfkapzrsdmvwp3fi")))

(define-public crate-tree-sitter-0.19.0 (c (n "tree-sitter") (v "0.19.0") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spin") (r "^0.7") (o #t) (d #t) (k 0)))) (h "144ng8vxnl0g7v16n8h8c7278xl0ahjpsj6sq7xx6f2yj3v74fgw") (f (quote (("allocation-tracking" "lazy_static" "spin"))))))

(define-public crate-tree-sitter-0.19.1 (c (n "tree-sitter") (v "0.19.1") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spin") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0xyd7fnprjx9dkk8ivcnh864w2fvaxk8dcs8jfjbyi2y6lgpclq5") (f (quote (("allocation-tracking" "lazy_static" "spin"))))))

(define-public crate-tree-sitter-0.19.2 (c (n "tree-sitter") (v "0.19.2") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spin") (r "^0.7") (o #t) (d #t) (k 0)))) (h "085s72s8b2vkahsblpyd46grfqdb54dr36yhivrp5r75f89431c0") (f (quote (("allocation-tracking" "lazy_static" "spin"))))))

(define-public crate-tree-sitter-0.19.3 (c (n "tree-sitter") (v "0.19.3") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spin") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1aw1r4rb4asbq5hrhgplqga518kk2z30372s80hbbcrxxlgj0h8z") (f (quote (("allocation-tracking" "lazy_static" "spin"))))))

(define-public crate-tree-sitter-0.19.5 (c (n "tree-sitter") (v "0.19.5") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spin") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1h6adq5kqf4izzsklch5lfxx2aisxga463zz7w44rgwnck16wwmd") (f (quote (("allocation-tracking" "lazy_static" "spin"))))))

(define-public crate-tree-sitter-0.20.0 (c (n "tree-sitter") (v "0.20.0") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spin") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1yg4p54hsfsxxknjq492i8b4rvibzpl2zdvr2bwvwakqgah05v33") (f (quote (("allocation-tracking" "lazy_static" "spin"))))))

(define-public crate-tree-sitter-0.20.1 (c (n "tree-sitter") (v "0.20.1") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spin") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1wcb2gg7xmmqyd7n739n1i9ppyxmg0p317mpd8ymyywnzvdyk54k") (f (quote (("allocation-tracking" "lazy_static" "spin"))))))

(define-public crate-tree-sitter-0.20.2 (c (n "tree-sitter") (v "0.20.2") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "10pxbxbbg89db1j0pw0l4h17d053h310rbli4h8mmn0j4lif6sy3")))

(define-public crate-tree-sitter-0.20.3 (c (n "tree-sitter") (v "0.20.3") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1gyhrcdiwgf5z1da282zbqwlzc7zbn2mai8lrzj6z0vxbvmmwdd2")))

(define-public crate-tree-sitter-0.20.4 (c (n "tree-sitter") (v "0.20.4") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "16p3kysfzfgd8nyagfs2l8jpfdhr5cdlg0kk0czmwm5cirzk4d2f")))

(define-public crate-tree-sitter-0.20.5 (c (n "tree-sitter") (v "0.20.5") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06ksxp0y3l70z385cajdsz3wy4xcc11x2gxvl3y99fgbiy7z5m4v")))

(define-public crate-tree-sitter-0.20.6 (c (n "tree-sitter") (v "0.20.6") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0wcvxgnvj7ga1y7xa7wm0pmabkfj8936ifg8jacd4201cj0vgcq9")))

(define-public crate-tree-sitter-0.20.7 (c (n "tree-sitter") (v "0.20.7") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "183niy9malzr4dm81swcgl05xkwqamim6ck0gw5xb6k78nprz6jl")))

(define-public crate-tree-sitter-0.20.8 (c (n "tree-sitter") (v "0.20.8") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "183vnmn97l52jsk504k8l9zcyvfb2a767hjr3g9fa28crbiz72r6")))

(define-public crate-tree-sitter-0.20.9 (c (n "tree-sitter") (v "0.20.9") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1g0q7vz3lcxmz9f77j2gj8d7pc2n2g3ws1g5j759h4z19xw3qhnl")))

(define-public crate-tree-sitter-0.20.10 (c (n "tree-sitter") (v "0.20.10") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0g9xd1nadhb2ikhxkj0z7kg9f50h97hzmha8llwyscdrnzwv2iz7") (r "1.65")))

(define-public crate-tree-sitter-0.21.0 (c (n "tree-sitter") (v "0.21.0") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "wasmtime") (r "^18.0.1") (f (quote ("cranelift"))) (o #t) (k 0)) (d (n "wasmtime-c-api") (r "^18.0.1") (o #t) (k 0) (p "wasmtime-c-api-impl")))) (h "16imkbnnnflxj59dpj0xbznqf7gjykr44r9sgpfp20cdjp0gfnvh") (f (quote (("wasm" "wasmtime" "wasmtime-c-api")))) (l "tree-sitter") (r "1.70")))

(define-public crate-tree-sitter-0.22.0 (c (n "tree-sitter") (v "0.22.0") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "wasmtime") (r "^18.0.1") (f (quote ("cranelift"))) (o #t) (k 0)) (d (n "wasmtime-c-api") (r "^18.0.1") (o #t) (k 0) (p "wasmtime-c-api-impl")))) (h "1nb3hwx06xaw91ihm1g08zizbbb3r8yqrjbqb9ck99y42b921kf6") (f (quote (("wasm" "wasmtime" "wasmtime-c-api")))) (y #t) (l "tree-sitter") (r "1.74.1")))

(define-public crate-tree-sitter-0.22.1 (c (n "tree-sitter") (v "0.22.1") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "wasmtime") (r "^18.0.1") (f (quote ("cranelift"))) (o #t) (k 0)) (d (n "wasmtime-c-api") (r "^18.0.1") (o #t) (k 0) (p "wasmtime-c-api-impl")))) (h "10cqpr3gj0ck3lbi4b1ygsb7wdzzd55n66rz58mhvcpi2kkvj1jn") (f (quote (("wasm" "wasmtime" "wasmtime-c-api")))) (l "tree-sitter") (r "1.74.1")))

(define-public crate-tree-sitter-0.22.2 (c (n "tree-sitter") (v "0.22.2") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "wasmtime") (r "^18.0.1") (f (quote ("cranelift"))) (o #t) (k 0)) (d (n "wasmtime-c-api") (r "^18.0.1") (o #t) (k 0) (p "wasmtime-c-api-impl")))) (h "0sq43b1355ps6hmnc6wrhqvjdrhn3ll6mn701v8dr4dfbvqwkfdx") (f (quote (("wasm" "wasmtime" "wasmtime-c-api")))) (l "tree-sitter") (r "1.65")))

(define-public crate-tree-sitter-0.22.3 (c (n "tree-sitter") (v "0.22.3") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.92") (d #t) (k 1)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "wasmtime-c-api") (r "^19") (o #t) (k 0) (p "wasmtime-c-api-impl")))) (h "1ibrffi21n9qikilizpffjfqs7mwqybn3ixhih8ybasxlhxb2x72") (f (quote (("wasm" "wasmtime-c-api")))) (l "tree-sitter") (r "1.65")))

(define-public crate-tree-sitter-0.22.4 (c (n "tree-sitter") (v "0.22.4") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.92") (d #t) (k 1)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "wasmtime-c-api") (r "^19") (o #t) (k 0) (p "wasmtime-c-api-impl")))) (h "0d2mncax9sn0vj1ghjgls4kvyaywy1zh1ic47dkda03143fmr7dw") (f (quote (("wasm" "wasmtime-c-api")))) (l "tree-sitter") (r "1.65")))

(define-public crate-tree-sitter-0.22.5 (c (n "tree-sitter") (v "0.22.5") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.92") (d #t) (k 1)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "wasmtime-c-api") (r "^19") (o #t) (k 0) (p "wasmtime-c-api-impl")))) (h "06y7mdcs1n6maq3i077vcskzay9z92qfq6bpka7xhvf78bc010k8") (f (quote (("wasm" "wasmtime-c-api")))) (l "tree-sitter") (r "1.65")))

(define-public crate-tree-sitter-0.22.6 (c (n "tree-sitter") (v "0.22.6") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.95") (d #t) (k 1)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "wasmtime-c-api") (r "^19") (o #t) (k 0) (p "wasmtime-c-api-impl")))) (h "1jkda5n43m7cxmx2h7l20zxc74nf9v1wpm66gvgxrm5drscw8z6z") (f (quote (("wasm" "wasmtime-c-api")))) (l "tree-sitter") (r "1.65")))

