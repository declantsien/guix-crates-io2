(define-module (crates-io tr ee tree-sitter-edit) #:use-module (crates-io))

(define-public crate-tree-sitter-edit-0.1.0 (c (n "tree-sitter-edit") (v "0.1.0") (d (list (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 2)))) (h "0pgz6xkz21g665favqm4j52x0z91g2brffvqid912bls18zczwbm")))

(define-public crate-tree-sitter-edit-0.1.1 (c (n "tree-sitter-edit") (v "0.1.1") (d (list (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 2)))) (h "1hhxwjvpli3sivgmjy37v0kwr1q840zzdxf7kfib9zh6c7pgs5s4")))

(define-public crate-tree-sitter-edit-0.1.2 (c (n "tree-sitter-edit") (v "0.1.2") (d (list (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 2)))) (h "0wnsia574wj4f74sjhmpqnck12z7p4g69cvsld845gxzcaf0y01b")))

(define-public crate-tree-sitter-edit-0.2.0 (c (n "tree-sitter-edit") (v "0.2.0") (d (list (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)) (d (n "tree-sitter-traversal") (r "^0.1") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 2)))) (h "1ngr2wm0j2a925nf46j7b795srhx13agaaw39lyjqdq1xx0gv1wj")))

(define-public crate-tree-sitter-edit-0.3.0 (c (n "tree-sitter-edit") (v "0.3.0") (d (list (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)) (d (n "tree-sitter-traversal") (r "^0.1") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 2)))) (h "0cc0bjai6l4kw0a1g6ldc593vpwhbhxr2fd5xi49gsanwqz23lwy")))

