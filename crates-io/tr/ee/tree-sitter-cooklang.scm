(define-module (crates-io tr ee tree-sitter-cooklang) #:use-module (crates-io))

(define-public crate-tree-sitter-cooklang-0.0.1 (c (n "tree-sitter-cooklang") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1lcaahqwk5fz1a8z00vcxzkl1294nah8y1963a4xpr6jv4in4rl2")))

(define-public crate-tree-sitter-cooklang-0.0.2 (c (n "tree-sitter-cooklang") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "18k7iq9x1v1hsmzn0583xagz880qp15f2bcxg9q25hjs4pwir0p8")))

