(define-module (crates-io tr ee tree-sitter-smali) #:use-module (crates-io))

(define-public crate-tree-sitter-smali-0.0.1 (c (n "tree-sitter-smali") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "06kbqfqy9gvvvw5pbykiq267sd154cgarwjyb0q60h2lfvpw4al8")))

(define-public crate-tree-sitter-smali-0.0.3 (c (n "tree-sitter-smali") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0pc7z9kqiqflivsdxwryc91x7y2r4lz9lz6m2praa95kvn8wk90w")))

(define-public crate-tree-sitter-smali-0.0.4 (c (n "tree-sitter-smali") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1qg3jv4c192b3j1hqxybwa8aajl7rm90sqvjn7f5p871mq7878b3")))

(define-public crate-tree-sitter-smali-1.0.0 (c (n "tree-sitter-smali") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0ajy8fpzkfji6iarhiws7gwgjg3vdr3l82dgzj57p0dcmykp38vi")))

