(define-module (crates-io tr ee tree-sitter-angular) #:use-module (crates-io))

(define-public crate-tree-sitter-angular-0.3.0 (c (n "tree-sitter-angular") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.5") (d #t) (k 0)) (d (n "tree-sitter-html") (r "~0.20.3") (d #t) (k 0)))) (h "182a4pglxigvzhkw0w3j1jjz6sfxvhfi2klr2g2phdj5z83f0fd6")))

