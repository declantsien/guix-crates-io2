(define-module (crates-io tr ee tree-sitter-c-sharp) #:use-module (crates-io))

(define-public crate-tree-sitter-c-sharp-0.16.1 (c (n "tree-sitter-c-sharp") (v "0.16.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.17") (d #t) (k 0)))) (h "03qhr5l97pw418pbn9hg2077wya9rjlxfz3jgphwdi0s207bz04y")))

(define-public crate-tree-sitter-c-sharp-0.19.1 (c (n "tree-sitter-c-sharp") (v "0.19.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "0sdg7xsn8cp1hcxml26796v8n4gc8qrfp41yr6fwyj9vwk9ja900")))

(define-public crate-tree-sitter-c-sharp-0.20.0 (c (n "tree-sitter-c-sharp") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0cdf37z6dfiff83p3p8dvsv19g1dysakllqhkvx28jgk1333vaxr")))

(define-public crate-tree-sitter-c-sharp-0.21.0 (c (n "tree-sitter-c-sharp") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1s4xjacpsx7x0bfjydxvf3flvdlrf88k9kbpr0r79wslinmx8w6n")))

(define-public crate-tree-sitter-c-sharp-0.21.1 (c (n "tree-sitter-c-sharp") (v "0.21.1") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "0pznsdlyplj73n8wk3cqka0kk6mbrz6871h0knm9kr4w5i03172v")))

(define-public crate-tree-sitter-c-sharp-0.21.2 (c (n "tree-sitter-c-sharp") (v "0.21.2") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1nhqrpjhpqy9p22lp4rx5kqxs00lvfa7w6w9j6wgn7wa0qvr12gz")))

