(define-module (crates-io tr ee tree-sitter-stack-graphs-java) #:use-module (crates-io))

(define-public crate-tree-sitter-stack-graphs-java-0.0.1 (c (n "tree-sitter-stack-graphs-java") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "tree-sitter-java") (r "^0.20.0") (d #t) (k 0)) (d (n "tree-sitter-stack-graphs") (r "~0.6.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "13jar3v92n5f7w4p66jlpvyidwc4k229lr8djr2h0b8sa43g0h81")))

(define-public crate-tree-sitter-stack-graphs-java-0.1.0 (c (n "tree-sitter-stack-graphs-java") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "tree-sitter-java") (r "~0.20.0") (d #t) (k 0)) (d (n "tree-sitter-stack-graphs") (r "~0.6.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "0nj3zflhd6d5qa9ad1bbpbhgnp2m213r4n0xr3g3mz01yj888b5x")))

(define-public crate-tree-sitter-stack-graphs-java-0.2.0 (c (n "tree-sitter-stack-graphs-java") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "tree-sitter-java") (r "~0.20.0") (d #t) (k 0)) (d (n "tree-sitter-stack-graphs") (r "~0.6.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "15212cv7546kcpnx3g0cwqg613iqn5bfkfs09mm6kwygagrcl1hl")))

(define-public crate-tree-sitter-stack-graphs-java-0.3.0 (c (n "tree-sitter-stack-graphs-java") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tree-sitter-java") (r "=0.20.0") (d #t) (k 0)) (d (n "tree-sitter-stack-graphs") (r "^0.8") (d #t) (k 0)) (d (n "tree-sitter-stack-graphs") (r "^0.8") (f (quote ("cli"))) (d #t) (k 2)))) (h "0yqcnjdiqzdfvcraygz30kkbwh1xz3xz2k4fppnfv49bql1w53z6") (f (quote (("cli" "anyhow" "clap" "tree-sitter-stack-graphs/cli"))))))

