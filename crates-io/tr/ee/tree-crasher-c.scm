(define-module (crates-io tr ee tree-crasher-c) #:use-module (crates-io))

(define-public crate-tree-crasher-c-0.2.0 (c (n "tree-crasher-c") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.0") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)))) (h "1cpl987w8pvb54cxipszb67iwypw0c6ijpi3s2aq0wjb94ln4pxl")))

(define-public crate-tree-crasher-c-0.2.1 (c (n "tree-crasher-c") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.1") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)))) (h "1av63db63wjwrkyfns8g8fyf1rx2nzzmn0s4nrxsmx5zhi29bn7g")))

(define-public crate-tree-crasher-c-0.2.2 (c (n "tree-crasher-c") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.2") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)))) (h "1n400c8zjzzsdgi8rjn0agsmhx0rl92s8snaq8bx1gxjr2q4pbkp")))

(define-public crate-tree-crasher-c-0.2.3 (c (n "tree-crasher-c") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.3") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)))) (h "1r235j72qfmkx6h4p3ibx9qksyvh3in31f289686df5gyjgcgisp")))

(define-public crate-tree-crasher-c-0.3.0 (c (n "tree-crasher-c") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.3.0") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)))) (h "0k08md2rlsjjmp43yr24kawg9b7hsmkg002q6drajyahw4s46fpq")))

(define-public crate-tree-crasher-c-0.4.0 (c (n "tree-crasher-c") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.4.0") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)))) (h "0irlcagckh172qs3kzsi36x3qxwd28k8pih9m3f7rqw0zbj77x46")))

