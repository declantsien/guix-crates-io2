(define-module (crates-io tr ee treeedb-rust) #:use-module (crates-io))

(define-public crate-treeedb-rust-0.1.0-rc.5 (c (n "treeedb-rust") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "treeedb") (r "^0.1.0-rc.5") (f (quote ("cli"))) (d #t) (k 0)))) (h "02kgdj3rzn6yfvr7lk7ybnfmxbngf3a4bs499q7pd1v2h5sn3k9a")))

