(define-module (crates-io tr ee tree-sitter-query) #:use-module (crates-io))

(define-public crate-tree-sitter-query-0.1.0 (c (n "tree-sitter-query") (v "0.1.0") (d (list (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1d2cgw9yc27zwf3i63icj525a0xmhf9fg402lp4v50k32jx78hwx")))

