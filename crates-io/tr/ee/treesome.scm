(define-module (crates-io tr ee treesome) #:use-module (crates-io))

(define-public crate-treesome-0.1.0 (c (n "treesome") (v "0.1.0") (h "0vxf74l6gj8lsmq6svv2bpfw590g917b7vxrm2g9yf3ixyq5jxrl")))

(define-public crate-treesome-0.2.0 (c (n "treesome") (v "0.2.0") (h "16pcx33rx1m6jhk3jpmqp67s9dnqrp33h2yl1hk2wbfblrk3gycb")))

(define-public crate-treesome-0.3.0 (c (n "treesome") (v "0.3.0") (h "17dm8ihqaa6lzbwyg5wqwwijpvdpv3sclmy7gzxb99awx8hhi2cr")))

(define-public crate-treesome-0.4.0 (c (n "treesome") (v "0.4.0") (h "05f92isv1dm83wzd2qy3aipggd9v81nwvvpmxyn8x09ciisbp125")))

(define-public crate-treesome-0.5.0 (c (n "treesome") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0sfni3l6jzggak6pjvpx072garsq7l2pnp7hf4bpnvvj9azz18i5") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-treesome-0.6.0 (c (n "treesome") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "03lrl2nn4psizjssc3w502fxxdg4r9kmblnvzlaz8x1wipdqjfdb") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-treesome-0.7.0 (c (n "treesome") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "19ivl8ycjipk4p0y4z0918ks899j2si604b6j92hkx4x8kq7fwm2") (s 2) (e (quote (("serde" "dep:serde"))))))

