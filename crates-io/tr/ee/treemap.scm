(define-module (crates-io tr ee treemap) #:use-module (crates-io))

(define-public crate-treemap-0.1.0 (c (n "treemap") (v "0.1.0") (d (list (d (n "ord_subset") (r "^3") (d #t) (k 0)))) (h "10yngwvdn78ymicbs3256xi48ndhcdr6f3p4l079slnjkwjj8jl8")))

(define-public crate-treemap-0.2.0 (c (n "treemap") (v "0.2.0") (d (list (d (n "ord_subset") (r "^3") (d #t) (k 0)))) (h "0yz8szv1jkgjhi74zn9x3h5h3b98j44hz2rv6yppiym989w2rb6w")))

(define-public crate-treemap-0.2.1 (c (n "treemap") (v "0.2.1") (d (list (d (n "ord_subset") (r "^3") (d #t) (k 0)))) (h "1a38czv21kmqlxsshl3bbjnmnja9hai9v3aj0mcnzdb3a0ivisf9")))

(define-public crate-treemap-0.3.0 (c (n "treemap") (v "0.3.0") (h "0iv6pqm9gvf0fh13b8j9pka8y1mrr9rwd57915cb4bbq275vyx02")))

(define-public crate-treemap-0.3.1 (c (n "treemap") (v "0.3.1") (h "1cyvrazllhgrnjzjy2ayrgjgarinkm47k54ll5v0n6ibcbvg3nhz")))

(define-public crate-treemap-0.3.2 (c (n "treemap") (v "0.3.2") (h "0dkgrgpakjd6m8x91j5l69j8rshrjnmy2kihhfmf3997va4iymx1")))

