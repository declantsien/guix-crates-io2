(define-module (crates-io tr ee tree-sitter-slint) #:use-module (crates-io))

(define-public crate-tree-sitter-slint-0.0.1 (c (n "tree-sitter-slint") (v "0.0.1") (d (list (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1kn7xnpmq0f00lmcmpx8w2n2mxf2d4110rga1n51qqzwdp4c7rrf")))

(define-public crate-tree-sitter-slint-0.1.0 (c (n "tree-sitter-slint") (v "0.1.0") (d (list (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0yln1inj7pdr9llp11x3ni0850cfwcfzsj799rc2fyy8h3x494bw")))

