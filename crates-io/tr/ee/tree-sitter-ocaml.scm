(define-module (crates-io tr ee tree-sitter-ocaml) #:use-module (crates-io))

(define-public crate-tree-sitter-ocaml-0.16.0 (c (n "tree-sitter-ocaml") (v "0.16.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "1j1paipr5g3fkyfwj2iijkanw92p8fgpxj9x7ww1cdswi1n11f71")))

(define-public crate-tree-sitter-ocaml-0.19.0 (c (n "tree-sitter-ocaml") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "01xa98hykf21qdjbxdp6378393zms09335y8fzm7wghr6aj1v20d")))

(define-public crate-tree-sitter-ocaml-0.20.1 (c (n "tree-sitter-ocaml") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "16ygpxzvfhc14sbkyz4vykhzcnwkn0dwlibv34j6qrrm247zxrk2")))

(define-public crate-tree-sitter-ocaml-0.20.2 (c (n "tree-sitter-ocaml") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1r6fnpxyvmkbrqa0kbwfppphrli1m773yl7cwnsp8ngk827mj5px")))

(define-public crate-tree-sitter-ocaml-0.20.3 (c (n "tree-sitter-ocaml") (v "0.20.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0swhaxb3gyhdls10zy37fb1mvji6axwsxbkmk3j5yzqfv3c5sqgh")))

(define-public crate-tree-sitter-ocaml-0.20.4 (c (n "tree-sitter-ocaml") (v "0.20.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "12i0mkrj83lgn3rp9pi9hyws407fsd5qzggzxkh8mksqqsmn64gx")))

(define-public crate-tree-sitter-ocaml-0.21.0 (c (n "tree-sitter-ocaml") (v "0.21.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.21") (d #t) (k 0)))) (h "0cnlxx5j2dik3mmm4qag1yda0s84h8ddn3i2mr9ys8yhz2cxfgqq")))

(define-public crate-tree-sitter-ocaml-0.21.1 (c (n "tree-sitter-ocaml") (v "0.21.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.21") (d #t) (k 0)))) (h "1l85dkcvd6fav230mky3am1bcwjliqncqlzmamp2f95kfhvg86v8")))

(define-public crate-tree-sitter-ocaml-0.21.2 (c (n "tree-sitter-ocaml") (v "0.21.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.21") (d #t) (k 0)))) (h "03lkyk7svgz24ff6f6gqgp196ky6hhjpmk0wz6bvy90mvhmiwqj7")))

(define-public crate-tree-sitter-ocaml-0.22.0 (c (n "tree-sitter-ocaml") (v "0.22.0") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22") (d #t) (k 0)))) (h "1lhdybzp51071j5zv10cmp0ryw5mcq71vlvqcz514bch93lfiwp2")))

