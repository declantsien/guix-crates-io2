(define-module (crates-io tr ee treeflection) #:use-module (crates-io))

(define-public crate-treeflection-0.1.0 (c (n "treeflection") (v "0.1.0") (h "0x9smndg895jrxl3sdh5ji0d51pmx0l8yhjj0mmfhwdrqlyfl72k")))

(define-public crate-treeflection-0.1.1 (c (n "treeflection") (v "0.1.1") (h "04sy1ix3fiwag3y313qybam4zn73x7r5bgfxldhnnxl7l533fl9c")))

(define-public crate-treeflection-0.1.2 (c (n "treeflection") (v "0.1.2") (h "1zg7ihf46hs29vssxyijs0frnx51r0vmga9mca54nwq348vpcdcn")))

(define-public crate-treeflection-0.1.3 (c (n "treeflection") (v "0.1.3") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0vq6kp89q12kz8kq2xy21i2kkk2n91npq82ms3mm06vhrfwamvg2")))

(define-public crate-treeflection-0.1.4 (c (n "treeflection") (v "0.1.4") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "13z0wx9f6a75mqkfw7jxfnaz5x459xhqa1m6lfnraqgngrq2wslg")))

(define-public crate-treeflection-0.1.5 (c (n "treeflection") (v "0.1.5") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1lms17lws3yp3r7dagisacqyz8j4wb4aql4dj1dwzqqyvkcy8mpq")))

(define-public crate-treeflection-0.1.6 (c (n "treeflection") (v "0.1.6") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0q1z5ifh9m8s8yp847y6c6n4rqh347l6kkd9172xp546m6rj4x4m")))

(define-public crate-treeflection-0.1.7 (c (n "treeflection") (v "0.1.7") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "09z0wpls58jz1asmlwsk2gdgjzdph5mrlx43q2jbddpwd36kr7d5")))

(define-public crate-treeflection-0.1.8 (c (n "treeflection") (v "0.1.8") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1jbv729b0k9sbpw49pz1wdj4hg16wn6v7ck33snq9d9qsfm7f65s")))

(define-public crate-treeflection-0.1.9 (c (n "treeflection") (v "0.1.9") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "05ysaaqxf2f2ii2cbn3s4q0kmiqymhmf097ip3p15ksf0992gn84")))

(define-public crate-treeflection-0.1.10 (c (n "treeflection") (v "0.1.10") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1y7kaa7lfc5jjhl3jqszf9mm3cnzz3k06lfl65gmxyqis8m3jzig")))

(define-public crate-treeflection-0.1.11 (c (n "treeflection") (v "0.1.11") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "08jajcmsw51vxqyjn4v1z5ssnav01n54kzfz8m2vpj8k7q3kgmpg")))

(define-public crate-treeflection-0.1.12 (c (n "treeflection") (v "0.1.12") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0rmd86cjax71lphymyszvi1qx1h0yhrxdr6jqis5dfb3zvw76m1c")))

(define-public crate-treeflection-0.1.13 (c (n "treeflection") (v "0.1.13") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0fs9g57jdmm0xnnz1cjns1rpvgyslc2n61gg0rxpiihpg7diy61b")))

(define-public crate-treeflection-0.1.14 (c (n "treeflection") (v "0.1.14") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "09gkqa2wr97b9jd8v4skkwd364xln01cy2icvj699in29brpl1gd")))

(define-public crate-treeflection-0.1.15 (c (n "treeflection") (v "0.1.15") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "02haswii0x0v77zv0bpssvwmvxpvf0ma3r9zkd9kjf2g72y3py75")))

(define-public crate-treeflection-0.1.16 (c (n "treeflection") (v "0.1.16") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0pa9mrcdsaqq5s0zkxkay2s0ilm9j3fsk3qav7kagq6smva5cyvn")))

(define-public crate-treeflection-0.1.17 (c (n "treeflection") (v "0.1.17") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1ill8n65wgz9i4qnrxyz98zj4hb34n29m25kgcljmkmaixl2qchh")))

(define-public crate-treeflection-0.1.18 (c (n "treeflection") (v "0.1.18") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ss2jx8ckahjm5mip6j4jiad0sdv7xp4pql9h343zi9l1i7hair7")))

(define-public crate-treeflection-0.1.19 (c (n "treeflection") (v "0.1.19") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mfdfyf3ws4va6ki1j79264gxwnd02bk0w40d137n6iiss5k02md")))

(define-public crate-treeflection-0.1.20 (c (n "treeflection") (v "0.1.20") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hliwrkga3syg5773a013sdswfp7kbvp7fw7l95s12y3l3anws5r")))

(define-public crate-treeflection-0.1.21 (c (n "treeflection") (v "0.1.21") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q4q41f6ha27gm0gsibs7h5z2zi9c2hly3j1mv2wjaknn3bz4xlm")))

(define-public crate-treeflection-0.1.22 (c (n "treeflection") (v "0.1.22") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pp360fgabwvsb13vzwadc2sfvlsw0nlgmsnxv9f4zkz67k1h4lf")))

(define-public crate-treeflection-0.1.23 (c (n "treeflection") (v "0.1.23") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x447gg7984aih7l761iqid9f7b4aafx9z7jskjwdbl5fyg2rbas")))

(define-public crate-treeflection-0.1.24 (c (n "treeflection") (v "0.1.24") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aagbrv78dhljd858sfyc89w94g8y1j24bi0mq3g0fk7kprr9bv1")))

(define-public crate-treeflection-0.1.25 (c (n "treeflection") (v "0.1.25") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bkc6gpbq5phl09zfjdq6fp8i4rxwscbg4qnxi7cavphf290n5yh")))

(define-public crate-treeflection-0.1.26 (c (n "treeflection") (v "0.1.26") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mxbfdgl9ap021arq07xd04fb7bvnkk0058q9c3n2d75bmx4na9x")))

(define-public crate-treeflection-0.1.27 (c (n "treeflection") (v "0.1.27") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dh5vwbmgrrnq3jhz1g6h0m570wr0qx4zgjnzp7rkwwlxbqscqb4")))

(define-public crate-treeflection-0.1.28 (c (n "treeflection") (v "0.1.28") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yc3a47hkzkimgbgvi1alhbfndzkx6f6krhl7gdyi4aay16v7bfh")))

(define-public crate-treeflection-0.1.29 (c (n "treeflection") (v "0.1.29") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nj129rb2lhgpiwcq6zav0m2ylxw0h0fhh03ld6vj7cy029g60is")))

(define-public crate-treeflection-0.1.30 (c (n "treeflection") (v "0.1.30") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fb19gb7jmb47wfh7jy1dcgq8d2lgkp4pnzdfz9s6f0sh9bxzl1k")))

(define-public crate-treeflection-0.1.31 (c (n "treeflection") (v "0.1.31") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qw35rbrngh80l0b00gnzy9ri6qq0x4rgpqpvfgbhh3yxfilrphf")))

(define-public crate-treeflection-0.1.32 (c (n "treeflection") (v "0.1.32") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jrla0lp54q8503g1n4amjjizr34vn2b8dzs51ndn774n33c8ppx")))

(define-public crate-treeflection-0.1.33 (c (n "treeflection") (v "0.1.33") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1401yq3y6zrjpvjxq322yb6cpib1309bgjnpm7f612g25f9v0yp0")))

(define-public crate-treeflection-0.1.34 (c (n "treeflection") (v "0.1.34") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0qgy1jlwsi4ccvzjm39prjxzl862pkz1lrmhkqvk5hyq988sac84")))

(define-public crate-treeflection-0.1.35 (c (n "treeflection") (v "0.1.35") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wg7ym3wpr68jb688ckylbn1ybv26qd9ch4ksk4hzld96jbsmpx7")))

