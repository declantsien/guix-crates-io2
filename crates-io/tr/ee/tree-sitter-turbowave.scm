(define-module (crates-io tr ee tree-sitter-turbowave) #:use-module (crates-io))

(define-public crate-tree-sitter-turbowave-1.7.1 (c (n "tree-sitter-turbowave") (v "1.7.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "1i7ym0nvfmjbzfh66jljspp4sk35frzzz8svj0qgwihiyf96g5wq")))

