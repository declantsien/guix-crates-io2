(define-module (crates-io tr ee tree-sitter-vue3) #:use-module (crates-io))

(define-public crate-tree-sitter-vue3-0.0.1 (c (n "tree-sitter-vue3") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1fkbq8ksdyqi94fv7lybsqh9wra8d7b2ax28p1jl34da8x0dsk8b")))

(define-public crate-tree-sitter-vue3-0.0.2 (c (n "tree-sitter-vue3") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1rwn8i0xidk0yg157djpr00aa9rizcwj1bjyh978h5j22yk29yvs")))

(define-public crate-tree-sitter-vue3-0.0.3 (c (n "tree-sitter-vue3") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1z9zna9py202v354qwwy4kann8plfd6422653b83jvzvdqsy948n")))

(define-public crate-tree-sitter-vue3-0.0.4 (c (n "tree-sitter-vue3") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "14zfqkp3d36wy8jlp8h8zfkpyaifwq8q69ca97v498jk5k4qc3v7")))

