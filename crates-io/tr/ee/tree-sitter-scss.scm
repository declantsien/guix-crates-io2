(define-module (crates-io tr ee tree-sitter-scss) #:use-module (crates-io))

(define-public crate-tree-sitter-scss-1.0.0 (c (n "tree-sitter-scss") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.92") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "095ssi0ncq97dlq8dd14s396f9xy9ff99sb16jzyp433m2f9m41k")))

