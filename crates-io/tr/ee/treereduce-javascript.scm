(define-module (crates-io tr ee treereduce-javascript) #:use-module (crates-io))

(define-public crate-treereduce-javascript-0.2.0 (c (n "treereduce-javascript") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "1k5caj51zvr0c7n4m58vvg5a88nd5pypp09w8vqncrxh8rip1h30")))

(define-public crate-treereduce-javascript-0.2.1 (c (n "treereduce-javascript") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.1") (f (quote ("cli"))) (d #t) (k 0)))) (h "0ij3hn8j5nawgy6mzcq281ydkjf1nfj3cyqzk3lzlj0ma9wwsqqq")))

(define-public crate-treereduce-javascript-0.2.2 (c (n "treereduce-javascript") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.2") (f (quote ("cli"))) (d #t) (k 0)))) (h "05i49zkvr2xkfi9z90x4xqwkvjllygnr6g9v5v5caxpic33vka80")))

(define-public crate-treereduce-javascript-0.3.0 (c (n "treereduce-javascript") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.3.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "0bv7himdnhcbbvkswmnc45njg7f9p0cm52mzg81ykajiddsddbfz")))

