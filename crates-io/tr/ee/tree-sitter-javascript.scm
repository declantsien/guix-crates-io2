(define-module (crates-io tr ee tree-sitter-javascript) #:use-module (crates-io))

(define-public crate-tree-sitter-javascript-0.16.0 (c (n "tree-sitter-javascript") (v "0.16.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.17") (d #t) (k 0)))) (h "1pyiklqqc7wpkq9bf0w6wvqh63bfl469x22kqbhb5wq8kchgbwrq")))

(define-public crate-tree-sitter-javascript-0.19.0 (c (n "tree-sitter-javascript") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "1yd5m9f3jgpmci2am2vh3bkbc92sgwlhgzvnp5vcp163ygav82w4")))

(define-public crate-tree-sitter-javascript-0.19.1 (c (n "tree-sitter-javascript") (v "0.19.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1yd2qr2z7a6lp15cmqbj041fp1lx623lyc9kwhbc1qsc6pk4gvgx")))

(define-public crate-tree-sitter-javascript-0.20.0 (c (n "tree-sitter-javascript") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1v86b4vv9fdya72sx0wd3i8nbgrw8xipn3rj7jacicihhsqgm414")))

(define-public crate-tree-sitter-javascript-0.20.1 (c (n "tree-sitter-javascript") (v "0.20.1") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1xrc20a56bdsv1x7s8bjvddsmfgdka2syv8a3yyr9lmxfqrndg7d")))

(define-public crate-tree-sitter-javascript-0.20.2 (c (n "tree-sitter-javascript") (v "0.20.2") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0j9rps227x2171ap57zqqhrrdz9pgmy3rkjk56j5ag3wc5wf6hn0")))

(define-public crate-tree-sitter-javascript-0.20.3 (c (n "tree-sitter-javascript") (v "0.20.3") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1h0fswg9p827mdr3jlxfb7ciwqn78pz7rnv124bm4w5yylx4dl9q")))

(define-public crate-tree-sitter-javascript-0.20.4 (c (n "tree-sitter-javascript") (v "0.20.4") (d (list (d (n "cc") (r "~1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0b1wywi2kr7nhk20rnm7lz1xz1ij70fgg7rjyw3chqlbm4pc05fh")))

(define-public crate-tree-sitter-javascript-0.21.0 (c (n "tree-sitter-javascript") (v "0.21.0") (d (list (d (n "cc") (r "~1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "0ngcf5497vzfjkpl222bzkyb7awm6czjw4pa76a8ylfrby9a3v16")))

(define-public crate-tree-sitter-javascript-0.21.1 (c (n "tree-sitter-javascript") (v "0.21.1") (d (list (d (n "cc") (r "~1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "13hrmqbabn3df3wrdwaanx5s6cspwkjf8bvi2ki9lj1y1qwhgpmf")))

(define-public crate-tree-sitter-javascript-0.21.2 (c (n "tree-sitter-javascript") (v "0.21.2") (d (list (d (n "cc") (r "~1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "06wrznp2mcmv14jn8x1f2972yfixkbnfrbnfp4yd1s5nij880208")))

(define-public crate-tree-sitter-javascript-0.21.3 (c (n "tree-sitter-javascript") (v "0.21.3") (d (list (d (n "cc") (r "~1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1vzy6zg4ahgp84mm438vsyqs7ky99aczvb8riv6jfriysh8dbkjg")))

