(define-module (crates-io tr ee tree-sitter-kotlin) #:use-module (crates-io))

(define-public crate-tree-sitter-kotlin-0.2.8 (c (n "tree-sitter-kotlin") (v "0.2.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "067zw8n2a440k6jdfc5zai1rga0cdm68lqd23vvrx177zy2gysif")))

(define-public crate-tree-sitter-kotlin-0.2.9 (c (n "tree-sitter-kotlin") (v "0.2.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0bliljpqjjhw1wmb9p800l1kh857dsh5sgqcv48blxc0w13fr6j2")))

(define-public crate-tree-sitter-kotlin-0.2.10 (c (n "tree-sitter-kotlin") (v "0.2.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0dsh2adwman0fsacv63aw9q68c46s38z58x8ibbqc3j19n0j38f3")))

(define-public crate-tree-sitter-kotlin-0.2.11 (c (n "tree-sitter-kotlin") (v "0.2.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1z67spirjjlhjg7fpzc9glidxjyz7sg9ihx5v4hghwl9y4zb37m6")))

(define-public crate-tree-sitter-kotlin-0.3.0 (c (n "tree-sitter-kotlin") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1v50mc0d0swdz14nj2xbrrhy7yqh10wp7m58y1wvp9cpsz7gp2v2")))

(define-public crate-tree-sitter-kotlin-0.3.1 (c (n "tree-sitter-kotlin") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "11lf6am18xx4vgrcsysy609sl59dq4gq7n52jjhj00i1crs3cpqv")))

(define-public crate-tree-sitter-kotlin-0.3.3 (c (n "tree-sitter-kotlin") (v "0.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "156b909sa3v43kx5nc5arqq41pc9ccdppxm5qiwcphgq2hgqdb31")))

(define-public crate-tree-sitter-kotlin-0.3.4 (c (n "tree-sitter-kotlin") (v "0.3.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1hfshcpfcfxq0jda3gqva3d0gzwvy20lqsrag8fvb4rh2kx36cl5")))

(define-public crate-tree-sitter-kotlin-0.3.5 (c (n "tree-sitter-kotlin") (v "0.3.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "09p7avpp2fc7s26h6rg2lizd7wrr4j9xwmriw7rlkipyw6h1gwld")))

(define-public crate-tree-sitter-kotlin-0.3.6 (c (n "tree-sitter-kotlin") (v "0.3.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21, <0.23") (d #t) (k 0)))) (h "16x7ypwn82i6d87dssca7kxac5fijfgv242wbifql49k4frgp3f8")))

