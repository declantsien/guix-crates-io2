(define-module (crates-io tr ee tree-sitter-highlight) #:use-module (crates-io))

(define-public crate-tree-sitter-highlight-0.1.0 (c (n "tree-sitter-highlight") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r ">= 0.3.7") (d #t) (k 0)))) (h "0pmx86i2b3f9igvz7l16a4lxvqpqbk3d9d6x7wmxkyg4c1kr4ja1")))

(define-public crate-tree-sitter-highlight-0.1.1 (c (n "tree-sitter-highlight") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r ">= 0.3.7") (d #t) (k 0)))) (h "0barwh1s5qznmgbszn8kyk724nkp3pby83x1a7dnlg4jln5z9l8i")))

(define-public crate-tree-sitter-highlight-0.1.2 (c (n "tree-sitter-highlight") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r ">= 0.3.7") (d #t) (k 0)))) (h "0idbrncg65k6hv8i8bmyswcy96m8ixy37jz55y3g3k7pfpy2z3r7")))

(define-public crate-tree-sitter-highlight-0.1.3 (c (n "tree-sitter-highlight") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r ">= 0.3.7") (d #t) (k 0)))) (h "1ranzvag62ppxph9jzwqbhfxrm8qwf6aavh46ncijlpx9zgq6ccv")))

(define-public crate-tree-sitter-highlight-0.1.4 (c (n "tree-sitter-highlight") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r ">= 0.3.7") (d #t) (k 0)))) (h "107vp0icxjmhajaip8z6r12bb6vf8h90sryv2bls1mybwwzm7afq")))

(define-public crate-tree-sitter-highlight-0.1.5 (c (n "tree-sitter-highlight") (v "0.1.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r ">= 0.3.7") (d #t) (k 0)))) (h "18hn6sg57ixicxj7kw7kczv4jgl94naz8l0s6i5kd9vpx76kacl1")))

(define-public crate-tree-sitter-highlight-0.1.6 (c (n "tree-sitter-highlight") (v "0.1.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r ">= 0.3.7") (d #t) (k 0)))) (h "0vp1qlwq61kdf3j5z2kkzi1zb6bkckhd7vs7gj0khy1xb3ar2x20")))

(define-public crate-tree-sitter-highlight-0.2.0 (c (n "tree-sitter-highlight") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r ">= 0.3.7") (d #t) (k 0)))) (h "1lccb3fj8pcy5xiybji66jwlwj3wpp5r6j2s0hm5dc8dmh0ciqy6")))

(define-public crate-tree-sitter-highlight-0.3.0 (c (n "tree-sitter-highlight") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.3.7") (d #t) (k 0)))) (h "1l9myymgx9cwc98hzjkyj0dsf48rag95lz8wgwnjmvnhrliiqvl5")))

(define-public crate-tree-sitter-highlight-0.19.0 (c (n "tree-sitter-highlight") (v "0.19.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.3.7") (d #t) (k 0)))) (h "1nbpinp7l0ybr5qmx2rlhj00map260lk40higc3dw1ax2lrhjb3d")))

(define-public crate-tree-sitter-highlight-0.19.1 (c (n "tree-sitter-highlight") (v "0.19.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.3.7") (d #t) (k 0)))) (h "16i1hlhyl0q6d60cbdj2cynj6ajza180z5riayjhlvkijmcgrz88")))

(define-public crate-tree-sitter-highlight-0.19.2 (c (n "tree-sitter-highlight") (v "0.19.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.3.7") (d #t) (k 0)))) (h "0w2rnhyq671b6kyxq89r5w7l14zswc1g784zwkqjmr2rn952hdzv")))

(define-public crate-tree-sitter-highlight-0.20.0 (c (n "tree-sitter-highlight") (v "0.20.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "01hvq3dvfc08lb4ry1qjwnfvs82hi2li5pqk9mznv7jji42p7rz2")))

(define-public crate-tree-sitter-highlight-0.20.1 (c (n "tree-sitter-highlight") (v "0.20.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1g3811rccl8lw2hkkcy4qvfni61zmfyy5i4z7n1hnyjs9ic448q4")))

(define-public crate-tree-sitter-highlight-0.21.0 (c (n "tree-sitter-highlight") (v "0.21.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.21.0") (d #t) (k 0)))) (h "183a7g2m647981k64xs1gq0s8ngwjb4pndshn42dn9vqw7s0v31g") (r "1.70")))

(define-public crate-tree-sitter-highlight-0.22.0 (c (n "tree-sitter-highlight") (v "0.22.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.0") (d #t) (k 0)))) (h "0qydq32db829k4nfghfa3gi2lbmhcwkm68211r79k2b2zpsraq1f") (y #t) (r "1.74.1")))

(define-public crate-tree-sitter-highlight-0.22.1 (c (n "tree-sitter-highlight") (v "0.22.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.0") (d #t) (k 0)))) (h "0dl3xw721cg7ihf7mn1bs2ybi15gp4jvi20p7wa38bc50gab7fmc") (r "1.74.1")))

(define-public crate-tree-sitter-highlight-0.22.2 (c (n "tree-sitter-highlight") (v "0.22.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "1mxanm3g5daqx6mz56207q2105sf3n5dhqnmjp3ybkpbk4rd38cb") (r "1.74.1")))

(define-public crate-tree-sitter-highlight-0.22.3 (c (n "tree-sitter-highlight") (v "0.22.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "1fng1jxgjy80gda7pgv84wxkk52jymg6h2svajk97a4rnd6ib9cf") (r "1.74.1")))

(define-public crate-tree-sitter-highlight-0.22.4 (c (n "tree-sitter-highlight") (v "0.22.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "1lzdibzrshcmp5mnn0qhq0nnzihh2jdm9k333vl3wn86wyllcfc9") (r "1.74.1")))

(define-public crate-tree-sitter-highlight-0.22.5 (c (n "tree-sitter-highlight") (v "0.22.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "0y1d1px81j9dgzal9r806smrcdv6m6dirqd533b4w4bbi7pxlhll") (r "1.74.1")))

(define-public crate-tree-sitter-highlight-0.22.6 (c (n "tree-sitter-highlight") (v "0.22.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.6") (d #t) (k 0)))) (h "01qrn9cqk4wkgwl7qj0pcddfdbqvps6k0qwfm9mfqvm99zihzjpa") (r "1.74.1")))

