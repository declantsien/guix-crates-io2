(define-module (crates-io tr ee treeedbgen-souffle-c) #:use-module (crates-io))

(define-public crate-treeedbgen-souffle-c-0.1.0-rc.5 (c (n "treeedbgen-souffle-c") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)) (d (n "treeedbgen-souffle") (r "^0.1.0-rc.5") (f (quote ("cli"))) (d #t) (k 0)))) (h "0bc9117i067ib5blqcs0k39fpdjrfxw4b0rc9703imhmayn82q6c")))

