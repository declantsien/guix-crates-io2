(define-module (crates-io tr ee tree-sitter-c) #:use-module (crates-io))

(define-public crate-tree-sitter-c-0.20.0 (c (n "tree-sitter-c") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "00alsh6zx6vcz5p5803smq044qfw87mx2yg3whs744q6vbawslgc")))

(define-public crate-tree-sitter-c-0.20.1 (c (n "tree-sitter-c") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "097xcl1w51k0fycv7sfl61nvvi6kshfypjj6j909rhybqrs5bp3v")))

(define-public crate-tree-sitter-c-0.20.2 (c (n "tree-sitter-c") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0ysclcdkg7zr4kpn28b4za68mg53dxxzd2rqkz3lsjvxhbs138nc")))

(define-public crate-tree-sitter-c-0.20.3 (c (n "tree-sitter-c") (v "0.20.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "01yb4fhdzxighl9rk4v018gj2xnax5b29zli4nkday2drqgg3i2d")))

(define-public crate-tree-sitter-c-0.20.4 (c (n "tree-sitter-c") (v "0.20.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1afwjywyz7mrbka5njj86hh1jpp27rad1z7ywisqgj0184xbf6zs")))

(define-public crate-tree-sitter-c-0.20.5 (c (n "tree-sitter-c") (v "0.20.5") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0dvsbxls69ip2sv036377bmqj1nccnsbpb1in22ay09xwmngb6ys")))

(define-public crate-tree-sitter-c-0.20.6 (c (n "tree-sitter-c") (v "0.20.6") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0jwzxl6l6mh5lz8nqv1x6acc5y5zfhd5hcg8prx0a84047gkpc1h")))

(define-public crate-tree-sitter-c-0.20.7 (c (n "tree-sitter-c") (v "0.20.7") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "15zlzhdislxnddfkfcmjyn8j7j1jn772pnr275d5q5qq9b1qwmws")))

(define-public crate-tree-sitter-c-0.20.8 (c (n "tree-sitter-c") (v "0.20.8") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0wkbrjwi68pmqpv6n9791yh9ybhw75narbgjz20qbh2qhqymzgab")))

(define-public crate-tree-sitter-c-0.21.0 (c (n "tree-sitter-c") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.89") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1slh05b3snq81b20cp4faggby4jj9yl9ng0n7l3y6hmn6l7mhqyl")))

(define-public crate-tree-sitter-c-0.21.1 (c (n "tree-sitter-c") (v "0.21.1") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1wmqs4p36062hk65vbcbg073qjlxvikq71xq5rwklqzxhg1ybw5l")))

(define-public crate-tree-sitter-c-0.21.2 (c (n "tree-sitter-c") (v "0.21.2") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1k09wyp2bxq0w9wzjd9vpy0wq0sxabvksmca9m079qz6dwlrpm5g")))

(define-public crate-tree-sitter-c-0.21.3 (c (n "tree-sitter-c") (v "0.21.3") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1wxr5c37qzki7h8542mqzqr945a170sxp58wsjca5p94kdw2v50f")))

