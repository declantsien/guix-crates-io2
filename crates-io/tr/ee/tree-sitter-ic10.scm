(define-module (crates-io tr ee tree-sitter-ic10) #:use-module (crates-io))

(define-public crate-tree-sitter-ic10-0.0.1 (c (n "tree-sitter-ic10") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1lspm16h1adjp71ydf933yypm0gbd0xr3wvrznxnwf6msh7faj96")))

(define-public crate-tree-sitter-ic10-0.0.2 (c (n "tree-sitter-ic10") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1d6avvhrbbhwysxbcqfx6gavknn7g3ln94lrh4fpp4hf1blqh5s1")))

(define-public crate-tree-sitter-ic10-0.1.0 (c (n "tree-sitter-ic10") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1hgsxj7s5mfn9v9i7fglsvnxij6z5yzh5w71hg08pf1y00n1qm8q") (y #t)))

(define-public crate-tree-sitter-ic10-0.1.1 (c (n "tree-sitter-ic10") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1r0yx3wdpk4dbjgphkjb8l1fy39xrdbxbm11fsl00vg0q2zkq8wh")))

(define-public crate-tree-sitter-ic10-0.2.0 (c (n "tree-sitter-ic10") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0zy88j68x3snjjzdqd5rdjfg6dhm6zsj5zx943g5zkcy4zc7ybzs")))

(define-public crate-tree-sitter-ic10-0.3.0 (c (n "tree-sitter-ic10") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "07dbn3pqqczg3vmrds6xm3g065pzrlc9flh1nkhzfddh38db4yy8")))

(define-public crate-tree-sitter-ic10-0.4.0 (c (n "tree-sitter-ic10") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1bh5w88rlrdhypjkj2xwlm22ygbfnf4j1afckx1s9fhd8g5i5995")))

(define-public crate-tree-sitter-ic10-0.5.0 (c (n "tree-sitter-ic10") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0qcyjh1bymgapww6mwjkvmf4ahnizalrd8z6kwlnm42vbfcps0am")))

(define-public crate-tree-sitter-ic10-0.5.1 (c (n "tree-sitter-ic10") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "08f01pdd4qfwxndfmzrc90vxmi1pr0srhy81jff7212ky8zzbll9")))

(define-public crate-tree-sitter-ic10-0.5.2 (c (n "tree-sitter-ic10") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1rir9pcfsw20q5zhrz7vvd2fjc35ffmmr4wgmj0c4ngk8f001289")))

