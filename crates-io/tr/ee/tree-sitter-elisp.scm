(define-module (crates-io tr ee tree-sitter-elisp) #:use-module (crates-io))

(define-public crate-tree-sitter-elisp-1.2.0 (c (n "tree-sitter-elisp") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0z4qb5v98rdv79bk9wjn9r65w5j6ij05j9qsm3svr8w6mgakr3s1")))

(define-public crate-tree-sitter-elisp-1.3.0 (c (n "tree-sitter-elisp") (v "1.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0fsgs1snfdyzdcmnfmvqlykfzm8302vn9yvxfz0xm0pm2v976i0a")))

