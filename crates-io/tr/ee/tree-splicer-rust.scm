(define-module (crates-io tr ee tree-splicer-rust) #:use-module (crates-io))

(define-public crate-tree-splicer-rust-0.1.0 (c (n "tree-splicer-rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "tree-splicer") (r "^0.1.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "15zg5l145anlhfh7zc2jk5jkdzwn8l7aapnphc0pygmiv01myplb")))

(define-public crate-tree-splicer-rust-0.2.0 (c (n "tree-splicer-rust") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "tree-splicer") (r "^0.2.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "0r8786pfl40yp40mv2mdikmcr3dm6md63h628kqkqfzjxqj75524")))

(define-public crate-tree-splicer-rust-0.3.0 (c (n "tree-splicer-rust") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "tree-splicer") (r "^0.3.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "14lz7ibz1qylj7y0m19ggwbxybfmzah06b6rffq3s6ygf8vb73rn")))

(define-public crate-tree-splicer-rust-0.3.1 (c (n "tree-splicer-rust") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "tree-splicer") (r "^0.3.1") (f (quote ("cli"))) (d #t) (k 0)))) (h "0p9ihmivrv4qpm56sh81max38zgc647pai7b46lr9sg5lrhfmb97")))

(define-public crate-tree-splicer-rust-0.4.0 (c (n "tree-splicer-rust") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "tree-splicer") (r "^0.4.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "0gfxclkwlv4m1a9jsvnlgdb2cjyra7r5arz385ai796q0902np0w")))

(define-public crate-tree-splicer-rust-0.5.0 (c (n "tree-splicer-rust") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "tree-splicer") (r "^0.5.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "0x628nb7sxh3sbhmcj4ln05qhl2fbc6bsiz74s47sg10jjy97j6c")))

