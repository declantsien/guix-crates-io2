(define-module (crates-io tr ee tree-sitter-pony) #:use-module (crates-io))

(define-public crate-tree-sitter-pony-0.0.1 (c (n "tree-sitter-pony") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "188gphs1bjdg633smwd64nrsmh15q704hwf998xpfdph4bljlx4v")))

(define-public crate-tree-sitter-pony-0.0.2 (c (n "tree-sitter-pony") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "13q4wgkiy0q6pmv1f71idrb5wz0klzfqm2hyx8vly4qxvl3j0ikm")))

(define-public crate-tree-sitter-pony-0.0.3 (c (n "tree-sitter-pony") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0dacgf530873f468z5zs5vjbwjrywcaqrad3dpwk78x6zz1kdj45")))

(define-public crate-tree-sitter-pony-1.0.0 (c (n "tree-sitter-pony") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0dvrc6mg4yn73dwxmff497wz37991jqgxhnvkxr2xv2nbv3fi3bx")))

