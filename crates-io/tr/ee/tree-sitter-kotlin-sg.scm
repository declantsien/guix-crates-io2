(define-module (crates-io tr ee tree-sitter-kotlin-sg) #:use-module (crates-io))

(define-public crate-tree-sitter-kotlin-sg-0.3.6 (c (n "tree-sitter-kotlin-sg") (v "0.3.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.21") (d #t) (k 0)))) (h "1xlramx9szia8i8vnps5xkqq6mwbs5lr2sax3imb4j3mznbsgl8d")))

(define-public crate-tree-sitter-kotlin-sg-0.3.7 (c (n "tree-sitter-kotlin-sg") (v "0.3.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21") (d #t) (k 0)))) (h "1nx4xbgrj1z4xc5365vh6qg8rayp0zzmlmgjjr5dr7c9b8drgafs")))

