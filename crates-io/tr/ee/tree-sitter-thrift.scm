(define-module (crates-io tr ee tree-sitter-thrift) #:use-module (crates-io))

(define-public crate-tree-sitter-thrift-0.1.0 (c (n "tree-sitter-thrift") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "05k78wc7y29fjqy667d4ig8bwc8qx82nn7hqblbqx75km5jspi86")))

(define-public crate-tree-sitter-thrift-0.2.0 (c (n "tree-sitter-thrift") (v "0.2.0") (d (list (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)) (d (n "cc") (r "~1.0") (d #t) (k 1)))) (h "1m0fhiyh650rvddmyhqihkj6clp35qijj14096pqg6dhm85pyqjg")))

(define-public crate-tree-sitter-thrift-0.3.0 (c (n "tree-sitter-thrift") (v "0.3.0") (d (list (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)) (d (n "cc") (r "~1.0") (d #t) (k 1)))) (h "13hp7n80m06kn3m4di9ixybg6sjkd40sa1hkc8cpr957j2f2cbmk")))

(define-public crate-tree-sitter-thrift-0.4.0 (c (n "tree-sitter-thrift") (v "0.4.0") (d (list (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)) (d (n "cc") (r "~1.0") (d #t) (k 1)))) (h "1nz5i7ipxqwqwx0yrz5yz2mp4zml867yh2qh1dcrx64ijb32qvxx")))

(define-public crate-tree-sitter-thrift-0.5.0 (c (n "tree-sitter-thrift") (v "0.5.0") (d (list (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)) (d (n "cc") (r "~1.0") (d #t) (k 1)))) (h "19dyv1y1kry40f96kndabqnqqaqxhpszln3xg3cahpq49l9nnm0h")))

