(define-module (crates-io tr ee tree-sitter-hare) #:use-module (crates-io))

(define-public crate-tree-sitter-hare-0.20.6 (c (n "tree-sitter-hare") (v "0.20.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "1ycaffgackz8d2h6af51i2x101s3w8vm6wn5dizbjfrkiyzxmhng") (y #t)))

(define-public crate-tree-sitter-hare-0.20.7 (c (n "tree-sitter-hare") (v "0.20.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "12qrzzaayf4sin0ryn5i8j2hybrhsnvxgyj9akgff6vj2ph5kgbc")))

(define-public crate-tree-sitter-hare-0.20.8 (c (n "tree-sitter-hare") (v "0.20.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.8") (d #t) (k 0)))) (h "10y9247s48nz5178zcr424zwl9ivxrc8xla4x7lf2k0gk1iyrax2") (y #t)))

