(define-module (crates-io tr ee tree-sitter-merlin6502) #:use-module (crates-io))

(define-public crate-tree-sitter-merlin6502-1.0.0 (c (n "tree-sitter-merlin6502") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "0hjg2rsrv6ik2c2s04g50kvnpy6y49ivq33963wz2c5317j1czxx")))

(define-public crate-tree-sitter-merlin6502-2.0.0 (c (n "tree-sitter-merlin6502") (v "2.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "1rvy7yhn92d2nn53svd07lncq94q5hc8vx3dyj5gsbx9bgz66djm")))

(define-public crate-tree-sitter-merlin6502-2.1.0 (c (n "tree-sitter-merlin6502") (v "2.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "0jqzaa7yg8z62x4qf4px8a5pzxkhch2l850z13h43jgj4lw18bfd")))

(define-public crate-tree-sitter-merlin6502-2.2.0 (c (n "tree-sitter-merlin6502") (v "2.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.4") (d #t) (k 0)))) (h "062lw35rjbz4kfg70i6cj8mfjfzjaddjm7in7xh83qy0ncz6s9rh")))

