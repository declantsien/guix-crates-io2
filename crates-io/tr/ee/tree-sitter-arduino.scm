(define-module (crates-io tr ee tree-sitter-arduino) #:use-module (crates-io))

(define-public crate-tree-sitter-arduino-0.20.0 (c (n "tree-sitter-arduino") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0p81fsbhp936kkb8352isjrs1hw4bgaxgryadjdfmv30l55cz82s")))

(define-public crate-tree-sitter-arduino-0.20.1 (c (n "tree-sitter-arduino") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1r3k4fh5xn5imzmjlx9xbczh7fhw1mqqxd28ysqgh94g5yzmbf5z")))

(define-public crate-tree-sitter-arduino-0.20.2 (c (n "tree-sitter-arduino") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1jyaazhivi3jhgjz3qvynr499py9z97dh1m918kijfp2fcby6cny")))

(define-public crate-tree-sitter-arduino-0.20.3 (c (n "tree-sitter-arduino") (v "0.20.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1g0x6if7c7kbg20mclfyl8g8ha3hnx6lgyryns6ddf1cdzng3gn1")))

(define-public crate-tree-sitter-arduino-0.21.0 (c (n "tree-sitter-arduino") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "0a5064mc49f2rvg60qrlrh51iy8qc41i6hqql0mzmd8wg3h2v4w5")))

