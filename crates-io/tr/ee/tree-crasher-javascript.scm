(define-module (crates-io tr ee tree-crasher-javascript) #:use-module (crates-io))

(define-public crate-tree-crasher-javascript-0.1.0 (c (n "tree-crasher-javascript") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.1.0") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "0gfhfxakjd2cbsrkxlj3vgp2bqigghi4cxs1w7bc881n71s19cia")))

(define-public crate-tree-crasher-javascript-0.1.1 (c (n "tree-crasher-javascript") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.1.1") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "0qlmyw6znwpd0pnnlgfg6cljgh6ibjziss301ifzl2jcgp2p97sg")))

(define-public crate-tree-crasher-javascript-0.1.2 (c (n "tree-crasher-javascript") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.1.2") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "1fqrya9b0y3zhs5rzgmwxyn03m7g6b850ap4a41pw6ym0a821biz")))

(define-public crate-tree-crasher-javascript-0.2.0 (c (n "tree-crasher-javascript") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.0") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "0kqdhch3m71icl6dlkmvyi3zxqjvzrk5x3wnc76r667gkff4qblh")))

(define-public crate-tree-crasher-javascript-0.2.1 (c (n "tree-crasher-javascript") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.1") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "1xc09my023bln3jsvml7c0isc5vvxnbjgzmcn4kmv76wask4jnnd")))

(define-public crate-tree-crasher-javascript-0.2.2 (c (n "tree-crasher-javascript") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.2") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "0648r66mw6hrv5r86xkifmjk595ndsggb1w7gpx7x63swmlwjwq8")))

(define-public crate-tree-crasher-javascript-0.2.3 (c (n "tree-crasher-javascript") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.3") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "1wcmyrc2kx1lc736b8vbpk0wrbz458crs65ixs44adgqsigpyd45")))

(define-public crate-tree-crasher-javascript-0.3.0 (c (n "tree-crasher-javascript") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.3.0") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "1nhm7cmipi7qcdsnwl2gypf542x05qzw5d45nkb2jbkjapsqgarm")))

(define-public crate-tree-crasher-javascript-0.4.0 (c (n "tree-crasher-javascript") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.4.0") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "0808dj945gvpcl4bg2pprifbsagln56s455zbafbfp62jfnpsk8d")))

