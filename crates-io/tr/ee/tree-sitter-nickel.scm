(define-module (crates-io tr ee tree-sitter-nickel) #:use-module (crates-io))

(define-public crate-tree-sitter-nickel-0.1.0 (c (n "tree-sitter-nickel") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0jpn96qj8s99nsldz5v48hb5gg4n3g1y98sdirv8qr7hcivjd5cf")))

