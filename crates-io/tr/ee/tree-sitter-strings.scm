(define-module (crates-io tr ee tree-sitter-strings) #:use-module (crates-io))

(define-public crate-tree-sitter-strings-0.1.0 (c (n "tree-sitter-strings") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0m2rlzq9nw2qxvcg9l9iddfy5cpc25yy03g7bjr5r6qz1km537k4")))

