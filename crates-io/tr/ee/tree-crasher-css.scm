(define-module (crates-io tr ee tree-crasher-css) #:use-module (crates-io))

(define-public crate-tree-crasher-css-0.2.0 (c (n "tree-crasher-css") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.0") (d #t) (k 0)) (d (n "tree-sitter-css") (r "^0.19") (d #t) (k 0)))) (h "1254zpn3sm3mnzdla2abpa9r6jzxznjp4sx2hqqa5h9mf7dhsizh")))

(define-public crate-tree-crasher-css-0.2.1 (c (n "tree-crasher-css") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.1") (d #t) (k 0)) (d (n "tree-sitter-css") (r "^0.19") (d #t) (k 0)))) (h "16aapwdv2fi6n2ajqv5yaij15z8lmvxcv72kdlj6k4bq67d0z241")))

(define-public crate-tree-crasher-css-0.2.2 (c (n "tree-crasher-css") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.2") (d #t) (k 0)) (d (n "tree-sitter-css") (r "^0.19") (d #t) (k 0)))) (h "0qvyf6mbly9iv13wbr3cx4pdmsfs3kvq35jr51ppm0mq7b7g9amr")))

(define-public crate-tree-crasher-css-0.2.3 (c (n "tree-crasher-css") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.3") (d #t) (k 0)) (d (n "tree-sitter-css") (r "^0.19") (d #t) (k 0)))) (h "1n7kma0rdrdbnf4hl744njczwvd7hg2gys6j0ys3zr7vlx59x88s")))

(define-public crate-tree-crasher-css-0.3.0 (c (n "tree-crasher-css") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.3.0") (d #t) (k 0)) (d (n "tree-sitter-css") (r "^0.19") (d #t) (k 0)))) (h "13xkjzlxipvbww449xy6r4n86gimin755z7xsy9h22mgjl85rh5l")))

(define-public crate-tree-crasher-css-0.4.0 (c (n "tree-crasher-css") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.4.0") (d #t) (k 0)) (d (n "tree-sitter-css") (r "^0.19") (d #t) (k 0)))) (h "0n4q6nbivzbmyg89l9qzxngqy6388cw10wvrv47yb8ixqkh0zwql")))

