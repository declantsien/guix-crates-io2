(define-module (crates-io tr ee tree-sitter-dot) #:use-module (crates-io))

(define-public crate-tree-sitter-dot-0.1.1 (c (n "tree-sitter-dot") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "0yfwpfnr1894w0skvx1gr4aghb9nyvcg99wzply7ma8i3fqii106")))

(define-public crate-tree-sitter-dot-0.1.2 (c (n "tree-sitter-dot") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "14f86nkhhr7jrsvrvvhy97v7v37qphqvv26h34732bqy5ia7x51g") (y #t)))

(define-public crate-tree-sitter-dot-0.1.3 (c (n "tree-sitter-dot") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "1prg4bjvhhyix89rpprn3hjaf4ih239sw00a608y71isjq8dadhw") (y #t)))

(define-public crate-tree-sitter-dot-0.1.4 (c (n "tree-sitter-dot") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "0mhqdwk06c33a75162hm2irmj9iv6w801zmz0c0q4mim3lai0llw")))

(define-public crate-tree-sitter-dot-0.1.5 (c (n "tree-sitter-dot") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "0vi98833v13lhjbj4vs39jyn1yr9hzmv578m9bpswimlnbk0zkvc")))

(define-public crate-tree-sitter-dot-0.1.6 (c (n "tree-sitter-dot") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0blr7lcvxsq343030mqij13f22as3kir4y9hlzmh6z50a6bivnbs")))

