(define-module (crates-io tr ee tree-sitter-muttrc) #:use-module (crates-io))

(define-public crate-tree-sitter-muttrc-0.0.1 (c (n "tree-sitter-muttrc") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0nnnw4sk03l5fajhyp2s6n2nfdbx0f424n29iwi7dg5fz6bqb426")))

(define-public crate-tree-sitter-muttrc-0.0.2 (c (n "tree-sitter-muttrc") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1irhgqc81ba185h1cyw0v6syq34hbg26swb7y44gg9v5f4690m08")))

(define-public crate-tree-sitter-muttrc-0.0.3 (c (n "tree-sitter-muttrc") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0jya9mhr7w07rq7hl5xcwbgm2xwss90g8fjbvwr92g1zg61l383w")))

(define-public crate-tree-sitter-muttrc-0.0.4 (c (n "tree-sitter-muttrc") (v "0.0.4") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.2") (d #t) (k 0)))) (h "14gm93m3jz81rzncp21h6fl1g8s2g1aasd5n3zbbkizqb1h7cy9n")))

(define-public crate-tree-sitter-muttrc-0.0.5 (c (n "tree-sitter-muttrc") (v "0.0.5") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.2") (d #t) (k 0)))) (h "0qi0a72y3zqj8b7bvmz81wk6nca1l9f66mm9ly4mpvrj9mf6y4zy")))

