(define-module (crates-io tr ee tree-sitter-apex) #:use-module (crates-io))

(define-public crate-tree-sitter-apex-0.0.1 (c (n "tree-sitter-apex") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0y01iir02s37g0szl8zkkl46l9vpdaa5yvqq6z0gxgpapxrlv5y6")))

(define-public crate-tree-sitter-apex-1.0.0 (c (n "tree-sitter-apex") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "08kjpds9wrsgyqzv5icb7zcdnwsync21dy8mshcw829wv8wicsiz")))

