(define-module (crates-io tr ee tree-sitter-svelte) #:use-module (crates-io))

(define-public crate-tree-sitter-svelte-0.8.1 (c (n "tree-sitter-svelte") (v "0.8.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)))) (h "0rl2lgn5cxy61ia812lgp2mvwr860f18gm1pxpl3fxylyqk5pykd")))

(define-public crate-tree-sitter-svelte-0.10.1 (c (n "tree-sitter-svelte") (v "0.10.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "05lnqpapchx7kyql1v46d3930nf7f9fxdwpf4vkkgr6i8jdvwc4n")))

(define-public crate-tree-sitter-svelte-0.10.2 (c (n "tree-sitter-svelte") (v "0.10.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "10p70g1pahp6vmhw52p51knyp6vlnvpklrab8ksy3zmxypb8w3w0")))

