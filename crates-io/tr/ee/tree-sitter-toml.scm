(define-module (crates-io tr ee tree-sitter-toml) #:use-module (crates-io))

(define-public crate-tree-sitter-toml-0.20.0 (c (n "tree-sitter-toml") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1s2kkcrw6kinb9x64nxdzwgahzs0i0kcqiq2g0h3vclqi9bpylfa")))

