(define-module (crates-io tr ee tree-struc) #:use-module (crates-io))

(define-public crate-tree-struc-0.1.0 (c (n "tree-struc") (v "0.1.0") (h "1va3z76i4lsd2736n0rw06bgpnbq0k0a6vf31m5gvjxwp10bn3y1")))

(define-public crate-tree-struc-0.1.1 (c (n "tree-struc") (v "0.1.1") (h "0jmvyfyv8yn3gnb6pxmxms7dwgrl1jxnqzsk93iax4n7qig5gnkk")))

(define-public crate-tree-struc-0.1.2 (c (n "tree-struc") (v "0.1.2") (h "15zmw8q7s3r5ndk1wf1976gb6pk44jwzvhp7ysva3cs38223wiza")))

