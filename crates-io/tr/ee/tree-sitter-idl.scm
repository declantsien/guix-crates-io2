(define-module (crates-io tr ee tree-sitter-idl) #:use-module (crates-io))

(define-public crate-tree-sitter-idl-0.0.1 (c (n "tree-sitter-idl") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.95") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "1k5m44f9k3v5h1n1ac5xwil5xq22cikjvm023dp19ibhy4i1rkix")))

(define-public crate-tree-sitter-idl-0.0.2 (c (n "tree-sitter-idl") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.95") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "0pgg7jyijw3ccyfhvnkgqhsx71ax3cj4y8lwnng697sx230m7mp9")))

(define-public crate-tree-sitter-idl-0.0.3 (c (n "tree-sitter-idl") (v "0.0.3") (d (list (d (n "cc") (r "^1.0.95") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "1956wysv34r7m5kbf9w2i2pdy6xzvw5cgv1xrahzgmnjw5r49gph")))

(define-public crate-tree-sitter-idl-0.0.4 (c (n "tree-sitter-idl") (v "0.0.4") (d (list (d (n "cc") (r "^1.0.95") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "0yll1a6kzjlbpkh6iv4krw60l66mi8h7q71lswrsq8ywkc2ky8nj")))

(define-public crate-tree-sitter-idl-0.0.5 (c (n "tree-sitter-idl") (v "0.0.5") (d (list (d (n "cc") (r "^1.0.95") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "07g7pka6z5krskgg2yfsyf4pj035l4rb4bcaiqr31nhmlm2w2wcr")))

