(define-module (crates-io tr ee treeedbgen-souffle) #:use-module (crates-io))

(define-public crate-treeedbgen-souffle-0.1.0-rc.5 (c (n "treeedbgen-souffle") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "treeedbgen") (r "^0.1.0-rc.5") (d #t) (k 0)))) (h "1wv0p16h2wpxxzwpcc78ddrda61xpg3j83y123rp7msl7ijg9zhb") (f (quote (("default")))) (s 2) (e (quote (("cli" "dep:anyhow" "dep:clap"))))))

