(define-module (crates-io tr ee tree-sitter-lura) #:use-module (crates-io))

(define-public crate-tree-sitter-lura-0.0.2 (c (n "tree-sitter-lura") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0v9dpc2kjr9dfd13as7y81vak44sidcij83dg86mn8x5895j83g1")))

(define-public crate-tree-sitter-lura-0.0.3 (c (n "tree-sitter-lura") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1f86pk7w5l3srhws48rp4cw5nb0l08cb13kl2whc7fc9hp7922vr")))

(define-public crate-tree-sitter-lura-0.0.4 (c (n "tree-sitter-lura") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1xc3axjs2bibd4fm5swzv60xh3l9a1m1cqn0vkd0l2mhbvwmfi6q")))

(define-public crate-tree-sitter-lura-0.0.5 (c (n "tree-sitter-lura") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0y3lfk6b0w4sjcnfic374qdcw9d4hqxfr3hnigan1s5xr0q165qd")))

(define-public crate-tree-sitter-lura-0.0.6 (c (n "tree-sitter-lura") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1jhzh608p3679imk47dlqiflsmypy795ln853rsr2n5xnp3v0y8z")))

(define-public crate-tree-sitter-lura-0.0.7 (c (n "tree-sitter-lura") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "037r7f98qszs2xvnidn8n899acjy28vqi4q81cvrw1hllh2md1wy")))

(define-public crate-tree-sitter-lura-0.0.8 (c (n "tree-sitter-lura") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "18jzy6plfsv9d3rz8k39f7x4p1q33yxxmyi6sz03h2hn0xfbrvpi")))

(define-public crate-tree-sitter-lura-0.0.9 (c (n "tree-sitter-lura") (v "0.0.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0ajm01acbzzrfwxjx4bvm7z59m8766v1p1vrl2fpnkn0sbpw8b2h")))

(define-public crate-tree-sitter-lura-0.1.0 (c (n "tree-sitter-lura") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "04mbh9bi7ip9sb87j80jw2hdjv34cix8j4bjjfy9xix3w7yas9br")))

(define-public crate-tree-sitter-lura-0.1.5 (c (n "tree-sitter-lura") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1z2gw05k00csnsrdfjl1zfn78x57s77g710f29qsm5jnkcxlm5l4")))

(define-public crate-tree-sitter-lura-0.1.6 (c (n "tree-sitter-lura") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "18lk6dgm0xpgq5sd95ylapnva517ms3mli9nlrk5ndasgkjgilyj")))

(define-public crate-tree-sitter-lura-0.1.7 (c (n "tree-sitter-lura") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "17f8y002dws682c174lxh5p867byj20khqaa4fy1my5pz23rij8x")))

(define-public crate-tree-sitter-lura-0.1.8 (c (n "tree-sitter-lura") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "14kmf9mi8hcgpfyvysgwzjvi4v2r07wmn1a566ixdsyw32i8zfna")))

(define-public crate-tree-sitter-lura-0.1.9 (c (n "tree-sitter-lura") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0qmfzz2lj66300lrncy7f6gizv1r4ik1qisbk4sd6s9sipy6p2aj")))

(define-public crate-tree-sitter-lura-0.1.10 (c (n "tree-sitter-lura") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "15w36dszlmsl9f0s1q41059rgypjly2ba8wf23f0n0znwvwqi18i")))

(define-public crate-tree-sitter-lura-0.1.11 (c (n "tree-sitter-lura") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "07dab86dgpfmwprjcmk1q38a1wv9hlzvv39wvkk4xzanv2m22hga")))

(define-public crate-tree-sitter-lura-0.1.12 (c (n "tree-sitter-lura") (v "0.1.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0kl3bx560xpqcir44dk06rn4x1gh3c9hmqnc1vb2a29xfb039f8f")))

(define-public crate-tree-sitter-lura-0.1.13 (c (n "tree-sitter-lura") (v "0.1.13") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "11iby5xnsvw4gp0933rx3mvabvfjk0mnqclqhgrxfw8mfznqhfz7")))

(define-public crate-tree-sitter-lura-0.1.14 (c (n "tree-sitter-lura") (v "0.1.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0my5wb2yirksph906b9vh8sydpwf010izh43av5ynvxqi7i7g2ls")))

(define-public crate-tree-sitter-lura-0.1.15 (c (n "tree-sitter-lura") (v "0.1.15") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0ww671wkzfyw776l4rs5s0zzzzpfgikrz3ny2fnfqq7k2340j95z")))

(define-public crate-tree-sitter-lura-0.1.16 (c (n "tree-sitter-lura") (v "0.1.16") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0aca3pf6c9h0w1zg1ma9nrwf6ap5ab1kf07qh3s33d602gr17acr")))

(define-public crate-tree-sitter-lura-0.1.17 (c (n "tree-sitter-lura") (v "0.1.17") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "06cqmmylr3mh0991v1nmdv5wpfxk7x65civzrwh0nf22gn8n5x9p")))

(define-public crate-tree-sitter-lura-0.1.18 (c (n "tree-sitter-lura") (v "0.1.18") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "12x2lzhrgy6v8pbx58njpmrx5b5yqcsgr36kh3pa62lf2fppnj8g")))

(define-public crate-tree-sitter-lura-0.1.19 (c (n "tree-sitter-lura") (v "0.1.19") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1ckyfigvg128dh7q0zk255lhfdylapjbgj6zyl6wqk5z5lhdify7")))

(define-public crate-tree-sitter-lura-0.1.20 (c (n "tree-sitter-lura") (v "0.1.20") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1j3nxilizfkwrnkq00lsfqw69yxf2dm7k2ad4y5cyj76csddf0gj")))

