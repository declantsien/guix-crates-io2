(define-module (crates-io tr ee tree-sitter-rigz) #:use-module (crates-io))

(define-public crate-tree-sitter-rigz-0.0.1 (c (n "tree-sitter-rigz") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.2") (d #t) (k 0)))) (h "009h1bxi2hglvc1a41qd50g19wciszlh2ly7ch0gv09j6qy40zm2")))

