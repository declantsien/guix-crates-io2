(define-module (crates-io tr ee tree-sitter-yuck) #:use-module (crates-io))

(define-public crate-tree-sitter-yuck-0.0.1 (c (n "tree-sitter-yuck") (v "0.0.1") (d (list (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0zfissxcvjva7hrfxgh0p8s3isggq66h328q7az9m7p3gil4nbmz")))

(define-public crate-tree-sitter-yuck-0.0.2 (c (n "tree-sitter-yuck") (v "0.0.2") (d (list (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ryca47yla9al1a4fgbyk8d6n4dv2x28wgsjys9c3pb4q2s0n72z")))

