(define-module (crates-io tr ee tree-sitter-go-sum) #:use-module (crates-io))

(define-public crate-tree-sitter-go-sum-0.0.1 (c (n "tree-sitter-go-sum") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "047rkz2w1bx6fqz0q1bka9fm2dhz92k7lcs9zqmkjbd642dfvcrv")))

(define-public crate-tree-sitter-go-sum-0.0.2 (c (n "tree-sitter-go-sum") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "17cpa2vnsi9zp1l19f8q23jlvs8a101ky59s0jj3n8x1lqavy21m")))

(define-public crate-tree-sitter-go-sum-1.0.0 (c (n "tree-sitter-go-sum") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0wkwf5xm41zy7yj9pdxnhjzrfh9cwvjr4kbag5wnvaqvshmdnns3")))

