(define-module (crates-io tr ee tree-sitter-jack) #:use-module (crates-io))

(define-public crate-tree-sitter-jack-0.1.0 (c (n "tree-sitter-jack") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0bckzvahm1llgvgakh3g45pfn8rqki0zzz4rgizb5ylfwb7pfgp1")))

(define-public crate-tree-sitter-jack-0.1.1 (c (n "tree-sitter-jack") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0r87r3pmap153vzzckkma501i2fdhrdwlp578knvrkpvcnz9n90x")))

