(define-module (crates-io tr ee tree-sitter-config) #:use-module (crates-io))

(define-public crate-tree-sitter-config-0.19.0 (c (n "tree-sitter-config") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "00syxl6lp775vsx15q2clpwnmjx0vcrb6cb64k9fllph4z5w9zpm")))

(define-public crate-tree-sitter-config-0.21.0 (c (n "tree-sitter-config") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0grr6dw0gzjszwy694fqd49rknirjg8nk9l24cbjlvg19z28rnng") (r "1.70")))

(define-public crate-tree-sitter-config-0.22.0 (c (n "tree-sitter-config") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "07pib0vfxdm0pg5c5kvfpwa17fdvzwda3ycrnlxc2sagd09b4y7g") (r "1.74.1")))

(define-public crate-tree-sitter-config-0.22.1 (c (n "tree-sitter-config") (v "0.22.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0zyq7sysr9422074x4wmg3mja98k3r3g5jnznib72r7d5y7pk6cj") (r "1.74.1")))

(define-public crate-tree-sitter-config-0.22.2 (c (n "tree-sitter-config") (v "0.22.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "04w3fcd5gbm2gfns89h4zywblmz46c7kp3fmci5p9jixwly40602") (r "1.74.1")))

(define-public crate-tree-sitter-config-0.22.3 (c (n "tree-sitter-config") (v "0.22.3") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "09k9y16k9rzz1m06yd9i45pdic7ndpbdgp02yy96g9nqxh0b523k") (r "1.74.1")))

(define-public crate-tree-sitter-config-0.22.4 (c (n "tree-sitter-config") (v "0.22.4") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0056ilp1hb621x6sm64bff88f62avp865lca5fpn4igfsq9z6r1w") (r "1.74.1")))

(define-public crate-tree-sitter-config-0.22.5 (c (n "tree-sitter-config") (v "0.22.5") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "194jfz6qq8n613sfhiyxyq1n0212p07wvqkzbnxf5b92vq707a57") (r "1.74.1")))

(define-public crate-tree-sitter-config-0.22.6 (c (n "tree-sitter-config") (v "0.22.6") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1gbyc16cscmfs6x4mi1bm677cnliljzwmkmgwdb2z0hxi9hb8r2x") (r "1.74.1")))

