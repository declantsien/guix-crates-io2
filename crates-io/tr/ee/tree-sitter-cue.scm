(define-module (crates-io tr ee tree-sitter-cue) #:use-module (crates-io))

(define-public crate-tree-sitter-cue-0.0.1 (c (n "tree-sitter-cue") (v "0.0.1") (d (list (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1v6b35kk8siirlb6jbkshvn552d0k3kx91szk9wfs0k50509i6hm")))

