(define-module (crates-io tr ee treeedbgen-souffle-souffle) #:use-module (crates-io))

(define-public crate-treeedbgen-souffle-souffle-0.1.0-rc.5 (c (n "treeedbgen-souffle-souffle") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tree-sitter-souffle") (r "^0.4.0") (d #t) (k 0)) (d (n "treeedbgen-souffle") (r "^0.1.0-rc.5") (f (quote ("cli"))) (d #t) (k 0)))) (h "1jhdiqmfvvhqr3f7ynr019bxy9anblp34ajcbx2s5cq2ajzw8asp")))

