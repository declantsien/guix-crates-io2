(define-module (crates-io tr ee tree-sitter-frugurt) #:use-module (crates-io))

(define-public crate-tree-sitter-frugurt-0.0.1 (c (n "tree-sitter-frugurt") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.4") (d #t) (k 0)))) (h "1m1mfx5sinm50ppd80pv88kndv0szmm8k8k39gdk5m8krmv08vlw")))

(define-public crate-tree-sitter-frugurt-0.0.2 (c (n "tree-sitter-frugurt") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.4") (d #t) (k 0)))) (h "1g3wn4s91b3c40mfsmq1zca2x3zjmx9wdkl1rn3ndcdbhy0cjmji")))

(define-public crate-tree-sitter-frugurt-0.0.3 (c (n "tree-sitter-frugurt") (v "0.0.3") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.4") (d #t) (k 0)))) (h "04xhjfzamr1x3ksl48ikjas5q5xy2zdaalssyzr1zn7fvlw3w8xj")))

(define-public crate-tree-sitter-frugurt-0.0.4 (c (n "tree-sitter-frugurt") (v "0.0.4") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.4") (d #t) (k 0)))) (h "0ckhqrqvwf4gwrv5w0pbiifba7yj67vh2fxl0bnrsgcgqk4zkc90")))

(define-public crate-tree-sitter-frugurt-0.0.5 (c (n "tree-sitter-frugurt") (v "0.0.5") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.4") (d #t) (k 0)))) (h "1rih5chhb413d6yd4amsj8y4cncc4c2pfbqx6id0nh9q028p11zn")))

(define-public crate-tree-sitter-frugurt-0.0.6 (c (n "tree-sitter-frugurt") (v "0.0.6") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.4") (d #t) (k 0)))) (h "050jsdz4qb7mgkxy8frrsyy4ac94gqzvblqhqgwajk2wnnlrvvgc")))

(define-public crate-tree-sitter-frugurt-0.0.7 (c (n "tree-sitter-frugurt") (v "0.0.7") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.4") (d #t) (k 0)))) (h "12kwscziwrr7hfh8dh8fa9zd5h1wv8x1k5wmgv85pvh4dv5lsmmb")))

(define-public crate-tree-sitter-frugurt-0.0.8 (c (n "tree-sitter-frugurt") (v "0.0.8") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.4") (d #t) (k 0)))) (h "0z48ysmclrqfi14mxbhd34zb0665hc7wnxxwi6s96f55z22wprl1")))

(define-public crate-tree-sitter-frugurt-0.0.9 (c (n "tree-sitter-frugurt") (v "0.0.9") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.4") (d #t) (k 0)))) (h "0889x32ka3wxi7sms6ygzlb57isi24283080x3w1cpbvdil1114p")))

(define-public crate-tree-sitter-frugurt-0.0.10 (c (n "tree-sitter-frugurt") (v "0.0.10") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.4") (d #t) (k 0)))) (h "0dg7hhd8l70c5k1hn1h4h889h99kl7kifqxai9rgr6mqvygq3gmn")))

