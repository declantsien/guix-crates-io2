(define-module (crates-io tr ee treeedb) #:use-module (crates-io))

(define-public crate-treeedb-0.1.0-rc.3 (c (n "treeedb") (v "0.1.0-rc.3") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0r096m0rap7vkzbbpf1fc9w0lsdzkkm8flj6bklga9b831vgfc7d") (f (quote (("default")))) (y #t) (s 2) (e (quote (("cli" "dep:anyhow" "dep:clap"))))))

(define-public crate-treeedb-0.1.0-rc.4 (c (n "treeedb") (v "0.1.0-rc.4") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0h85cj1nbv3nzbxck4ng481zz8qrx3df1jlndyyfjs2rh4vxq8n1") (f (quote (("default")))) (y #t) (s 2) (e (quote (("cli" "dep:anyhow" "dep:clap"))))))

(define-public crate-treeedb-0.1.0-rc.5 (c (n "treeedb") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1riyw18jk1sm14mdvhy7c3djzhsrw8h2c9g3qhbqjlffk8mgb89d") (f (quote (("default")))) (s 2) (e (quote (("cli" "dep:anyhow" "dep:clap"))))))

