(define-module (crates-io tr ee tree-sitter-qmljs) #:use-module (crates-io))

(define-public crate-tree-sitter-qmljs-0.1.0 (c (n "tree-sitter-qmljs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "07hkhpmb7gf71zac224inx9b3naq8c551irllf8msdvjv8pd4vvf")))

(define-public crate-tree-sitter-qmljs-0.1.1 (c (n "tree-sitter-qmljs") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1xc5xckys71wm1p64f43j4gxbfilnim64kc7l14735a897q1ab5z")))

(define-public crate-tree-sitter-qmljs-0.1.2 (c (n "tree-sitter-qmljs") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1gfqdgy02xl4qcnwkd42ajsza0hmvr8fdil4m19safs1glhh7q63")))

