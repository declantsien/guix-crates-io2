(define-module (crates-io tr ee tree2tui) #:use-module (crates-io))

(define-public crate-tree2tui-0.1.2 (c (n "tree2tui") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "cursive") (r "^0.16.3") (f (quote ("crossterm-backend"))) (k 0)) (d (n "cursive_tree_view") (r "^0.7.0") (d #t) (k 0)) (d (n "indextree") (r "^4.3.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "12q530z51yr2zl32aha75hnwggkpypz7jh4bnzm33labp5gymnng")))

