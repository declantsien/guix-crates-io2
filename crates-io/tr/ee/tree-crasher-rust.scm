(define-module (crates-io tr ee tree-crasher-rust) #:use-module (crates-io))

(define-public crate-tree-crasher-rust-0.1.1 (c (n "tree-crasher-rust") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.1.1") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "068hia9xvwr100z117k6zg7594v6675w7na3bhrvhsk2rl60b7hi")))

(define-public crate-tree-crasher-rust-0.1.2 (c (n "tree-crasher-rust") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.1.2") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "0rvrdvr92nhd2741wv2ixhyf9aghjiqknr3zl5qz3ji7vfw7ff9x")))

(define-public crate-tree-crasher-rust-0.2.0 (c (n "tree-crasher-rust") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.0") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "0rjvgrfw8dz4xdhskzna5glkfs1m0fqyyh0ha8i3ykhma0qny3a0")))

(define-public crate-tree-crasher-rust-0.2.1 (c (n "tree-crasher-rust") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.1") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "1ia8zval510sipfcs36q4bi2bbs90lak6wz3i2cwka0mfsdikr8j")))

(define-public crate-tree-crasher-rust-0.2.2 (c (n "tree-crasher-rust") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.2") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "02baix74l6sl4fhxxvcicim3hfwlcikca926jsqii5c0y5lgvjlh")))

(define-public crate-tree-crasher-rust-0.2.3 (c (n "tree-crasher-rust") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.3") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "04x53aj7wb375gk08ljbkhqbwhwrpw0naw4y0md137wmbwbfyxk9")))

(define-public crate-tree-crasher-rust-0.3.0 (c (n "tree-crasher-rust") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.3.0") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "1npbxq24g49nm05zhakh6d0wsg2s1n00afqy63ahlzvxmiwbhd83")))

(define-public crate-tree-crasher-rust-0.4.0 (c (n "tree-crasher-rust") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.4.0") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "1z54qgmq9w4141lh6x5vfrdahqm40rswl3j09djqrwni9pmjdk4d")))

