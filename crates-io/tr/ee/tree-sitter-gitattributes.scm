(define-module (crates-io tr ee tree-sitter-gitattributes) #:use-module (crates-io))

(define-public crate-tree-sitter-gitattributes-0.1.4 (c (n "tree-sitter-gitattributes") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0km0y3qki786q9k22diqyxpqr6m8nqparb0d08gw422hr1q2a9j0")))

(define-public crate-tree-sitter-gitattributes-0.1.5 (c (n "tree-sitter-gitattributes") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0xq4y5hfz31yax2z8cfq28fzwlia0arlixl89yakzpq6lri0m33z")))

(define-public crate-tree-sitter-gitattributes-0.1.6 (c (n "tree-sitter-gitattributes") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1l2c2r2dis1ixvc393wh4g9z35lzwwcv215rbz82r2fnp0xg2rz8")))

