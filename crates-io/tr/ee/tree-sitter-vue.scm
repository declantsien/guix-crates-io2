(define-module (crates-io tr ee tree-sitter-vue) #:use-module (crates-io))

(define-public crate-tree-sitter-vue-0.0.1 (c (n "tree-sitter-vue") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "04jakjv4fs6yn4prf304ml0f8r66i45df8ych4w88rk0ayxgf3nv")))

(define-public crate-tree-sitter-vue-0.0.2 (c (n "tree-sitter-vue") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0n239cbbik1l0823mvzrh33l85n7l8hab7q37nqla1hx5m3j0lfp")))

(define-public crate-tree-sitter-vue-0.0.3 (c (n "tree-sitter-vue") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0c47py62li3vaimrqiwpib9vgx3gk9qm2mzlk6kmsjkdmwm8ri8z")))

