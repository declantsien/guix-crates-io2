(define-module (crates-io tr ee tree-sitter-kind) #:use-module (crates-io))

(define-public crate-tree-sitter-kind-0.0.1 (c (n "tree-sitter-kind") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1sc0h62gqsj07lhx48gnav73kf3kgr3ki427kkh015hg41n9grd8")))

