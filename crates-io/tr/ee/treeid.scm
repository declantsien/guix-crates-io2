(define-module (crates-io tr ee treeid) #:use-module (crates-io))

(define-public crate-treeid-0.1.0 (c (n "treeid") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "02zqp9m6gyyzs5kj5g2j1i6g6scczap07jprj1lqaqfl1y3nnlqf")))

(define-public crate-treeid-0.1.1 (c (n "treeid") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "1a8digysrazfh52ads8kjb4yjh5d0nkxb9raf27qmsqx55mmbq5x")))

(define-public crate-treeid-0.1.2 (c (n "treeid") (v "0.1.2") (d (list (d (n "bitvec") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "03idm1x3pm464q3jxgrb7gqc881lcjf4z2is8bkspask0pyxk8ms")))

(define-public crate-treeid-0.2.0 (c (n "treeid") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "1m6zvxdlj987xyy9mh46cagx0ah34qpf5v4ywgv66mqr1r6k8jg1")))

(define-public crate-treeid-0.2.1 (c (n "treeid") (v "0.2.1") (d (list (d (n "bitvec") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "12d60hngfxc70cg37ck08ln5l11y8c40q5csam8gpxrvfpm3r7cs")))

(define-public crate-treeid-0.2.2 (c (n "treeid") (v "0.2.2") (d (list (d (n "bitvec") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "0p4xnh1bg3i2xjrx4ld9mza6akvs635qszx0rpv3lw5w2g2xrp24")))

(define-public crate-treeid-0.2.3 (c (n "treeid") (v "0.2.3") (d (list (d (n "bitvec") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "0l585ln33prxl3h5x87gy39d7lk2j7ryxblj6xzd6s5b0qkc2amw")))

(define-public crate-treeid-0.3.0 (c (n "treeid") (v "0.3.0") (d (list (d (n "bitvec") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "16k2239nxzw1l7c7m1qbfzg1j0pl0mmwv2d1rl2hrvjr1dzr0d4y")))

(define-public crate-treeid-0.3.1 (c (n "treeid") (v "0.3.1") (d (list (d (n "bitvec") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "0qsdkv7iqdp7mi8jagpbv3j89qcsl7f4skw0b32sz7mhb50dkid4")))

(define-public crate-treeid-0.3.2 (c (n "treeid") (v "0.3.2") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "1bn78pgxswfnbifnxvzyq3p1fwz7ffnbkdmi570q19pq5yl3bi5c")))

(define-public crate-treeid-0.3.3 (c (n "treeid") (v "0.3.3") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "00yh75isqq4axd8fyx67338zylkyb34lbmsxvlzlch00l97lg2p7")))

(define-public crate-treeid-0.3.4 (c (n "treeid") (v "0.3.4") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "1i1cs3w89853wjvqvyhmahrxwxaspd7la521zmpj148n5078d5qp")))

(define-public crate-treeid-0.3.5 (c (n "treeid") (v "0.3.5") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "1ryi6br6szy86l5117yqyximy0rb5lhk9xcpi2mk2x2anhw93jq8")))

(define-public crate-treeid-0.3.6 (c (n "treeid") (v "0.3.6") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "0z0k0ga0i0401ham491fbzg2nvl5ljmzm56nxb3m508ar0iqadk5")))

(define-public crate-treeid-0.3.7 (c (n "treeid") (v "0.3.7") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "1bdf22yh0v0bwhmzw7536z8jm051c35c0kjcgny4az8mga99967m")))

(define-public crate-treeid-0.3.8 (c (n "treeid") (v "0.3.8") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "1sq33bw8npd69wq6130rgqjyxs0ay6gwgfaw38gyyfsgjksa0mrp")))

(define-public crate-treeid-0.3.9 (c (n "treeid") (v "0.3.9") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "1nyxmr5av46ah9v8d8ahf2p8jh22g9vw21kzzq98gvrqgqp3c3my")))

(define-public crate-treeid-0.3.10 (c (n "treeid") (v "0.3.10") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "1j8cj6ys8kiz2lg5j4ns4mrpnmx3zdcafpyv99rn46dgsys7yvpv")))

(define-public crate-treeid-0.3.11 (c (n "treeid") (v "0.3.11") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 2)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "1303dm7hydr41m3v7hgm9d0h1j3p0a6psqxsdrsx88gka3f4waj0")))

