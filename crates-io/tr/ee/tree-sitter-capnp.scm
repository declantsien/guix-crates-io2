(define-module (crates-io tr ee tree-sitter-capnp) #:use-module (crates-io))

(define-public crate-tree-sitter-capnp-0.0.1 (c (n "tree-sitter-capnp") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "185k3i7saiz9xiv8jjf511q9qcsh9pxaq4f0ckv233lav848pv6g")))

(define-public crate-tree-sitter-capnp-0.0.2 (c (n "tree-sitter-capnp") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0x0j77sr5ixhfkmj9x60knyrcl4qfcmxirgv0hh6axfdm6d1vrmz")))

(define-public crate-tree-sitter-capnp-1.0.0 (c (n "tree-sitter-capnp") (v "1.0.0") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0q4ppjmn21xdcc9lhj2i7km29dwk8qmfdq76yk20g6fpgc518ai8")))

(define-public crate-tree-sitter-capnp-1.1.0 (c (n "tree-sitter-capnp") (v "1.1.0") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0y6zcxiwwrizgm24aykbcg9d1mvs5jflx2r01my0p8gfcz4s9rdh")))

(define-public crate-tree-sitter-capnp-1.2.0 (c (n "tree-sitter-capnp") (v "1.2.0") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "10fnnm68f9mzp4l5afggwm75jsmcsgpw5p7p2rkiyaxmzzswdbxq")))

(define-public crate-tree-sitter-capnp-1.3.0 (c (n "tree-sitter-capnp") (v "1.3.0") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0anl862n7pq1i5lab8yh0ck41qnrzgp2z8vxrsadn2jqn5s8czvk")))

(define-public crate-tree-sitter-capnp-1.4.0 (c (n "tree-sitter-capnp") (v "1.4.0") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "004fp62645p09zx6ic0g7bv0958nbrsd775l3ahkxbhy8fr4k4ss")))

(define-public crate-tree-sitter-capnp-1.5.0 (c (n "tree-sitter-capnp") (v "1.5.0") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0av9l919jrmdnx0w4yb5bhjvccsbsai931m42ca158kkbng752a5")))

