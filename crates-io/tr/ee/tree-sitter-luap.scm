(define-module (crates-io tr ee tree-sitter-luap) #:use-module (crates-io))

(define-public crate-tree-sitter-luap-0.0.1 (c (n "tree-sitter-luap") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "199q0680way3xx0g6f3rvaqiwjvj89slq4mwllcjizjgj4vad9hj")))

(define-public crate-tree-sitter-luap-1.0.0 (c (n "tree-sitter-luap") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0xzm89xi0xms6y1ym0xnw884qjz7p922whfwjw3ji2m454jvzcdx")))

