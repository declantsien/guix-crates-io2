(define-module (crates-io tr ee treexml) #:use-module (crates-io))

(define-public crate-treexml-0.1.0 (c (n "treexml") (v "0.1.0") (d (list (d (n "xml-rs") (r "^0.2") (d #t) (k 0)))) (h "19nblk882zd6x0yyfiy09zf090767jixq5n5iasslmnixjy80v1l")))

(define-public crate-treexml-0.1.1 (c (n "treexml") (v "0.1.1") (d (list (d (n "xml-rs") (r "^0.2") (d #t) (k 0)))) (h "16a7hjb20nlyrm3za3rrfnimls9n4w17vx7r4dnfxn4nx6244az7")))

(define-public crate-treexml-0.2.0 (c (n "treexml") (v "0.2.0") (d (list (d (n "xml-rs") (r "^0.2") (d #t) (k 0)))) (h "03jbbj7m03z9w5r2vkqi59jjkilfvhisbpkg1jm1nhfsizw1rsf3")))

(define-public crate-treexml-0.3.0 (c (n "treexml") (v "0.3.0") (d (list (d (n "xml-rs") (r "^0.2") (d #t) (k 0)))) (h "1n5xy00lgf1v8sll50z5pn9cw462g9q090wpyqawdql1pqxd1acm")))

(define-public crate-treexml-0.4.0 (c (n "treexml") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "0x1rzrvzf087n30rk8sp4lwan8lzvqnrfg8wimayz2hbbpfhp51r")))

(define-public crate-treexml-0.5.0 (c (n "treexml") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "1xg4s512jzvhsmb1q3qmm6xax9fq3bvfh1cfm0jw9xprzpw0hbr4")))

(define-public crate-treexml-0.6.0 (c (n "treexml") (v "0.6.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "17d4z0hbzxr9pgz0403xczivk5ay9n70q00b4fsdc3ganagp43i0")))

(define-public crate-treexml-0.6.1 (c (n "treexml") (v "0.6.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "0i2n1ivfmjjmnyvhv5jw9ggckazwl29b1aab01lcqgw7a2r12bzl")))

(define-public crate-treexml-0.6.2 (c (n "treexml") (v "0.6.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "1669lz1sw919irwxbqpb0q9ycj9j73rzz1vn6zxqq6dp3nykr94l")))

(define-public crate-treexml-0.7.0 (c (n "treexml") (v "0.7.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "02bnb9ckdxdbbcwzqjrwbygfanqy45k5jfncz4kx8l8zx1vqmdb1")))

