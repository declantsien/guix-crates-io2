(define-module (crates-io tr ee tree-sitter-prisma-io) #:use-module (crates-io))

(define-public crate-tree-sitter-prisma-io-1.2.0 (c (n "tree-sitter-prisma-io") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0jy8s19x5djdy1b4ib3h8k6h4ai2gihncnzzl9lh53wsid6kbw7p")))

(define-public crate-tree-sitter-prisma-io-1.2.1 (c (n "tree-sitter-prisma-io") (v "1.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0rh92yc2rfhc1s73dsn39i69blxd6ir8rql784h7215mv8fx3a28")))

(define-public crate-tree-sitter-prisma-io-1.2.2 (c (n "tree-sitter-prisma-io") (v "1.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0apd3n1lbvkvjhxxrjydxxzyk7l3h28kwg2p4cjyy6c1r647sp6y")))

(define-public crate-tree-sitter-prisma-io-1.3.0 (c (n "tree-sitter-prisma-io") (v "1.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0x4i13b8spwqd9f1fpm78cyplpb9qrcrdp94zcgjil3xpr4k710m")))

(define-public crate-tree-sitter-prisma-io-1.4.0 (c (n "tree-sitter-prisma-io") (v "1.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "19j4f70pp4qpiqam48d97kfkdpz59b5x03knx3b67cb2n69fag40")))

