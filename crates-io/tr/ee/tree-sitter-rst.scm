(define-module (crates-io tr ee tree-sitter-rst) #:use-module (crates-io))

(define-public crate-tree-sitter-rst-0.1.0 (c (n "tree-sitter-rst") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1fvigmk4vfm2mhv9175dlrfvz2fbdlah0r60p64c75riwnba3hk0")))

