(define-module (crates-io tr ee tree-sitter-navi-stream) #:use-module (crates-io))

(define-public crate-tree-sitter-navi-stream-0.0.1 (c (n "tree-sitter-navi-stream") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "138rz1pzcvmkpmggmvj5yxz1l8g6vnn4gafg1gjdplxf98nl68dp")))

(define-public crate-tree-sitter-navi-stream-0.0.2 (c (n "tree-sitter-navi-stream") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0k9qhspb224s3g666fpqbns70w1p7f5k47gwskgq19mxgw9ngr8m")))

(define-public crate-tree-sitter-navi-stream-0.0.3 (c (n "tree-sitter-navi-stream") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0lwvssj84x8l4dmkqjq8680hh5bsbcjvmkj4lawn190sjdcv2zls")))

(define-public crate-tree-sitter-navi-stream-0.0.4 (c (n "tree-sitter-navi-stream") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "09ybm3f1rsvbkyl3ydl0ycax4g1s4x30xjjxsz1vpkdv5q7zrnqf")))

