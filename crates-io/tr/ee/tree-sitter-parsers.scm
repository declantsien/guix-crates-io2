(define-module (crates-io tr ee tree-sitter-parsers) #:use-module (crates-io))

(define-public crate-tree-sitter-parsers-0.0.1 (c (n "tree-sitter-parsers") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "1kgvn6spy48s9v41vc4vks4fqyi88liqk1prxnkc27p0fprry3gp") (y #t)))

(define-public crate-tree-sitter-parsers-0.0.2 (c (n "tree-sitter-parsers") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "0sfdi59rgi5yi3ni6lpr0hfplvkvjkl1lgk5bclzghv6mw8x07nz")))

(define-public crate-tree-sitter-parsers-0.0.3 (c (n "tree-sitter-parsers") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "1ivziby869kcnkv9hg9wxw4kvrhmfyn7wn53xm89qmdzmkra9gi9")))

(define-public crate-tree-sitter-parsers-0.0.4 (c (n "tree-sitter-parsers") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "02v1imyjy14gsamkall4g505jnx8l483mx1qr5gs76k29rh8nncy")))

(define-public crate-tree-sitter-parsers-0.0.5 (c (n "tree-sitter-parsers") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "1i05lxb8d8h3kdr3bgjy6a4pqp0j3ryvbr8hj9bsgr6mv7sx458c")))

