(define-module (crates-io tr ee tree-sitter-md) #:use-module (crates-io))

(define-public crate-tree-sitter-md-0.0.1 (c (n "tree-sitter-md") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "03i8q9h6yjqmlb6zpll9axxf8nxw1r8pwqzhqvgf9v6j3pdkhfrs")))

(define-public crate-tree-sitter-md-0.1.0 (c (n "tree-sitter-md") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0shwphrvsyv5dcznnb6ym7v8iz38v8pkb3x6jaa1ncyhc8x5fchv")))

(define-public crate-tree-sitter-md-0.1.1 (c (n "tree-sitter-md") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "138p25n5gr07k4zx24xqgqg6hwgyihgskdmf166l79al5bsyrrc7")))

(define-public crate-tree-sitter-md-0.1.2 (c (n "tree-sitter-md") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0qhmg1kfvcxqy0mvwa20ni9saspsain8ar6jfrsq36wmbn0qv8xm")))

(define-public crate-tree-sitter-md-0.1.3 (c (n "tree-sitter-md") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1i1lzhwdsivhv47knspg3mccmqryj69wkrl4ncdfd5j9rig34qsy")))

(define-public crate-tree-sitter-md-0.1.4 (c (n "tree-sitter-md") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0m5jjkx6qblkm649plci58hzsbg020q75i1pxl4cdl9cvjyrcixy")))

(define-public crate-tree-sitter-md-0.1.5 (c (n "tree-sitter-md") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1qap04ilx2qzvxnvn7gg8zjq298prh49piw3qxv6nikb1yhpy8ss")))

(define-public crate-tree-sitter-md-0.1.7 (c (n "tree-sitter-md") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0306jnjhb24gcgj9ddx1gzc8xgw4kllyc1x38jv30910ippx681w")))

(define-public crate-tree-sitter-md-0.2.0 (c (n "tree-sitter-md") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.89") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "0ahc6xi6a2hy6didlamsng45h0627pk1fq90kgcz5v7mpm7753zd") (y #t)))

(define-public crate-tree-sitter-md-0.2.1 (c (n "tree-sitter-md") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.89") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "066ws78v0pcj5lf5hazxigj273amgx6csw25bmswjkh36h1cmkwm")))

(define-public crate-tree-sitter-md-0.2.2 (c (n "tree-sitter-md") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.89") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "122dgx1i8cdm9d5qhf28k35q3z97p48vvf82k3bvasc2f76mk5s5")))

(define-public crate-tree-sitter-md-0.2.3 (c (n "tree-sitter-md") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.89") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "0nlnjqzxs4g3fvxymrv3g2qi4khn65s41zyqpd874lpjd38czhyr")))

