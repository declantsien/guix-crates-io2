(define-module (crates-io tr ee tree_sitter_lint_proc_macros) #:use-module (crates-io))

(define-public crate-tree_sitter_lint_proc_macros-0.0.1-dev.0 (c (n "tree_sitter_lint_proc_macros") (v "0.0.1-dev.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "01x3nyr66hrmwf4dvvnxa8ws4w0h30z3rsfd0drdd5ppj541fz5p") (r "1.70")))

