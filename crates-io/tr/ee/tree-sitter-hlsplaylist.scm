(define-module (crates-io tr ee tree-sitter-hlsplaylist) #:use-module (crates-io))

(define-public crate-tree-sitter-hlsplaylist-0.0.2 (c (n "tree-sitter-hlsplaylist") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1s4dy9j2g1h9yb0cd40w386gccix6jx6gksfv5ipb87l6wwxsjsx")))

(define-public crate-tree-sitter-hlsplaylist-0.0.3 (c (n "tree-sitter-hlsplaylist") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0g430fj3riay5lp675ni1ay0l12p5lpsy04jcphrx50wdrv4y49a")))

(define-public crate-tree-sitter-hlsplaylist-0.0.4 (c (n "tree-sitter-hlsplaylist") (v "0.0.4") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.2") (d #t) (k 0)))) (h "034gvr8khl4kjkxj7xb2kwb8147rh8zmg6ryqwmkqj8pmy482vf2")))

