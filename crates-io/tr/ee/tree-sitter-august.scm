(define-module (crates-io tr ee tree-sitter-august) #:use-module (crates-io))

(define-public crate-tree-sitter-august-0.0.1 (c (n "tree-sitter-august") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1h3v56nak9hlwizwx9jq6lnp0g23a8md0bvryhjd4kc12q9mm5py")))

(define-public crate-tree-sitter-august-0.0.2 (c (n "tree-sitter-august") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0k51dq6chyvq3pcmwhmsqx5xiv1cix6dvvdwmc3i3xrfmdhag8py")))

(define-public crate-tree-sitter-august-0.0.3 (c (n "tree-sitter-august") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0k43mfrfhyqagbp9lw6yls47k46xrbqigvpibm48g3il18s3lfvb")))

(define-public crate-tree-sitter-august-0.0.4 (c (n "tree-sitter-august") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "17i8qfrb3b7q7804q62s44k3d58drl2sd25xjj8lkrijcgqpyvc8")))

(define-public crate-tree-sitter-august-0.0.5 (c (n "tree-sitter-august") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1qn8saxnyd3ysdljsvbnrvv1aszvb5gafkwdhnf88ida7xvf432x")))

