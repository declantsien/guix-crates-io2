(define-module (crates-io tr ee tree-sitter-erlang) #:use-module (crates-io))

(define-public crate-tree-sitter-erlang-0.0.1 (c (n "tree-sitter-erlang") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)))) (h "164ir8k220bskakd6126yk7pv8nwc9rx7v7dmzfq860jnsqjp8ig")))

(define-public crate-tree-sitter-erlang-0.1.0 (c (n "tree-sitter-erlang") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0zb0ha6l3hjfsdvmw8ar9p91ni70kma98znyaspwsymsnxa0c7kr")))

(define-public crate-tree-sitter-erlang-0.2.0 (c (n "tree-sitter-erlang") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0h8ihqzjs75kxcq6wilc4szpq718x7k4vg6gv22vjddflxi0s4cd")))

(define-public crate-tree-sitter-erlang-0.3.0 (c (n "tree-sitter-erlang") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "01vvh7v1n5f6sky5ci08cwa44bn3vqhc8cs4vm4p1q1yryvz04p8")))

(define-public crate-tree-sitter-erlang-0.4.0 (c (n "tree-sitter-erlang") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1x47n9ckzw3d2ssalbz1570prnh8w5s5n1dz8crgh5xvbqadbklk")))

(define-public crate-tree-sitter-erlang-0.5.0 (c (n "tree-sitter-erlang") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.1") (d #t) (k 0)))) (h "1rvzm2hzc4qr7n69m2q7vs952rmgvwvjcy0r24757ixn7jfysprf")))

(define-public crate-tree-sitter-erlang-0.6.0 (c (n "tree-sitter-erlang") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "0d3vh7jkap07d0iklqxlhj7h4pgq91cbijsyi6sv79fqwr913dld")))

