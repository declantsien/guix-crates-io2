(define-module (crates-io tr ee tree-sitter-gomod) #:use-module (crates-io))

(define-public crate-tree-sitter-gomod-1.0.0 (c (n "tree-sitter-gomod") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.17") (d #t) (k 0)))) (h "0la2m369nsgc1xbg8cxjb65a045mp6c4fpkvml5243rv11gpvfxa")))

(define-public crate-tree-sitter-gomod-1.0.1 (c (n "tree-sitter-gomod") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "14j9hy5w8vrcmm091ifdcxjrjfrcjwfj94ggjzxq78x4ld4ail1i")))

