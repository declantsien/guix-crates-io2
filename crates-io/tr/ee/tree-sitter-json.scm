(define-module (crates-io tr ee tree-sitter-json) #:use-module (crates-io))

(define-public crate-tree-sitter-json-0.19.0 (c (n "tree-sitter-json") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "14igm5zrag2zwcx5s00qnsbcqrlnzb44xjlzxcsra4wj3974rc4h")))

(define-public crate-tree-sitter-json-0.20.1 (c (n "tree-sitter-json") (v "0.20.1") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "19nyypd1x9jymsfhwh41dp9rdkzrz3jarql92b3msrzf6cp2vn2h")))

(define-public crate-tree-sitter-json-0.20.2 (c (n "tree-sitter-json") (v "0.20.2") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0kgdhpj1ci9lgwcwygx52wdglscdq0g3hl3ks745rdbrqslki6js")))

(define-public crate-tree-sitter-json-0.21.0 (c (n "tree-sitter-json") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.91") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1nq57g9508fzpzicxwrb05mch4w12pg3spsassvp8pf3fg5pswsv")))

