(define-module (crates-io tr ee tree-sitter-earthfile) #:use-module (crates-io))

(define-public crate-tree-sitter-earthfile-0.1.0 (c (n "tree-sitter-earthfile") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "1cdr4yhhd6lcn923nzfdpsxjmby8cqp8b1zyarzdpr0jnx9yx64i")))

(define-public crate-tree-sitter-earthfile-0.1.1 (c (n "tree-sitter-earthfile") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "0vmp85yfgc5s9v64ppj4knfbf6aa89hcjwnlyyxjnf9l29gxhs3j")))

(define-public crate-tree-sitter-earthfile-0.1.2 (c (n "tree-sitter-earthfile") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "0wbc44vxhrv0m8mp8l9lp86k999nsv1l9zjdy734fj01acvf34xw")))

(define-public crate-tree-sitter-earthfile-0.2.0 (c (n "tree-sitter-earthfile") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "1xv7jcq2422ii4hngng9qdwbl5v04hhkf1vc8zv04b6l7lnk9il7")))

(define-public crate-tree-sitter-earthfile-0.2.1 (c (n "tree-sitter-earthfile") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "1y1nlgxaqindp0xyv099r6ayaazvrkr9i10jjp6b6w8k5p6r8ska")))

(define-public crate-tree-sitter-earthfile-0.2.2 (c (n "tree-sitter-earthfile") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "1h9lm0k3nj9bpx810cbin93mg80jhdvvp4z8gksx878bskrkvq6q")))

(define-public crate-tree-sitter-earthfile-0.2.3 (c (n "tree-sitter-earthfile") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "09zanyrgwpkwl8973lqcnrcp9rbshwy1b57gbb2bdv2ajvnnr083")))

(define-public crate-tree-sitter-earthfile-0.2.4 (c (n "tree-sitter-earthfile") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "1b3j7pfdiq379hj3ib623ksf268rz7x9girk7qgbl5i8l444vvl6")))

(define-public crate-tree-sitter-earthfile-0.3.0 (c (n "tree-sitter-earthfile") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "0dp5rj81ivz910nfq4s9awaxyanpzyhysz34ar2nr1w7178nvxl0")))

(define-public crate-tree-sitter-earthfile-0.4.0 (c (n "tree-sitter-earthfile") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "0snvpw4iigywlv9rrard7dqsv21fqvyrknma1j4i5wgkw6wyzjkf")))

(define-public crate-tree-sitter-earthfile-0.4.1 (c (n "tree-sitter-earthfile") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "1gp70vvic8brgbq1wd5sk0cjxwv387sa6zvcygg64sdfdw54dm5s")))

(define-public crate-tree-sitter-earthfile-0.4.2 (c (n "tree-sitter-earthfile") (v "0.4.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "1dz4hlfkcprgzyhg96s08bmkkazdsf1jc2zy573vak993v6nr1qx")))

(define-public crate-tree-sitter-earthfile-0.4.3 (c (n "tree-sitter-earthfile") (v "0.4.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "1ybih7h4mhb5g2y1khl3p75alk7k78k3rkjmg2v94pm3f9si2sgi")))

(define-public crate-tree-sitter-earthfile-0.5.0 (c (n "tree-sitter-earthfile") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "03z6zwyf8vpfb7vrflh8mfb3ncn39pfjg2b8qlq5v90rgc838c6w")))

(define-public crate-tree-sitter-earthfile-0.5.1 (c (n "tree-sitter-earthfile") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.1") (d #t) (k 0)))) (h "1qc1wqpr0gzfwf6wxwd73xfz1ymx153bf5m7j4nq5d3hrn0yqpw3")))

