(define-module (crates-io tr ee tree-sitter-objc) #:use-module (crates-io))

(define-public crate-tree-sitter-objc-1.0.0 (c (n "tree-sitter-objc") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "19ll948hliym6bwb5apj6wk46w6akwwpblh5zi4g7cpnybavd8l4")))

(define-public crate-tree-sitter-objc-1.1.0 (c (n "tree-sitter-objc") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0i0h78paiks3yigjsy30r7dgvbh35gyq8phkz0c3298j20b4fqz1")))

(define-public crate-tree-sitter-objc-1.2.0 (c (n "tree-sitter-objc") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1m9qp0i2gg3vyja0vy3yhk1inl7r4km0alxm7mf30cakwwhx2b1r")))

(define-public crate-tree-sitter-objc-3.0.0 (c (n "tree-sitter-objc") (v "3.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "09qbzq25x55kiy0gcc10yxgs1pjl14syz5vwhgjb207l4hd5qilg")))

(define-public crate-tree-sitter-objc-2.1.0 (c (n "tree-sitter-objc") (v "2.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0bdyqd7zqw1idnrwn925hmz8ycd0rcjj0886lw0hair77x19gi52")))

