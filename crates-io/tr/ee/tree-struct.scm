(define-module (crates-io tr ee tree-struct) #:use-module (crates-io))

(define-public crate-tree-struct-0.1.0 (c (n "tree-struct") (v "0.1.0") (d (list (d (n "ptrplus") (r "^2.1.0") (k 0)))) (h "0ap5jq205zyy02cfs59aivd3yla3zgs3ar67av96faszb1m7g57b") (y #t)))

(define-public crate-tree-struct-0.1.1 (c (n "tree-struct") (v "0.1.1") (d (list (d (n "ptrplus") (r "^2.1.0") (k 0)))) (h "1fy9sdbbfjsrxlc8v9pbj1xv1d6ny3n0gnk6jp7p3iiq5mk2bxdp")))

