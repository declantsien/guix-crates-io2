(define-module (crates-io tr ee tree-sitter-toml-ng) #:use-module (crates-io))

(define-public crate-tree-sitter-toml-ng-0.6.0 (c (n "tree-sitter-toml-ng") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.92") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.5") (d #t) (k 0)))) (h "0mblvbhba1lg59jy6b2f7n41jfxlsxv3xw3kqw16rwdchg6j0pb9")))

