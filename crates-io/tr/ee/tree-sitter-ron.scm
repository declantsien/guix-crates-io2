(define-module (crates-io tr ee tree-sitter-ron) #:use-module (crates-io))

(define-public crate-tree-sitter-ron-0.2.0 (c (n "tree-sitter-ron") (v "0.2.0") (d (list (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1mxj82aqqk167gdbi8c3l27jsl9ajclmdppl5cpjkcqrpalwwr4y")))

(define-public crate-tree-sitter-ron-0.1.0 (c (n "tree-sitter-ron") (v "0.1.0") (d (list (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1y4cblylx0ykrg564dq8lnblcgypfpwinfw143mg1y8a6xkqmab9")))

