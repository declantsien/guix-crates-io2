(define-module (crates-io tr ee tree-sitter-cwscript) #:use-module (crates-io))

(define-public crate-tree-sitter-cwscript-0.0.1 (c (n "tree-sitter-cwscript") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0gm2zihjbajqq283pfm6zlfvdvvcvwid34qyrsqjd56c8nxr3wx0")))

(define-public crate-tree-sitter-cwscript-0.0.2 (c (n "tree-sitter-cwscript") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1b9gwi2bxb80rdzyz1507mdf57mi9c7ypw8x0wh9vfhbip26f53c")))

(define-public crate-tree-sitter-cwscript-0.0.3 (c (n "tree-sitter-cwscript") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0kdrpi38qfzkxvgl1ixnqhhl9rfqpryrh40dws1zw8mrr36cfj4w")))

(define-public crate-tree-sitter-cwscript-0.0.4 (c (n "tree-sitter-cwscript") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0ifr0ba9ia41b4hm7w64prk4xgdihd5llzv1fdnh41n5km4dbffj")))

(define-public crate-tree-sitter-cwscript-0.0.5 (c (n "tree-sitter-cwscript") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0gmrg88af351fflf57myx9q4v62znabjkf30cq5ssmv1j67sb2ff")))

