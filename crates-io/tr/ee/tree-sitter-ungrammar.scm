(define-module (crates-io tr ee tree-sitter-ungrammar) #:use-module (crates-io))

(define-public crate-tree-sitter-ungrammar-0.0.1 (c (n "tree-sitter-ungrammar") (v "0.0.1") (d (list (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "06s2bxaqybjivzp2nzz36zlajiq2q1y1s3bgiyq184pgn9850l2d")))

