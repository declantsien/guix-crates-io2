(define-module (crates-io tr ee tree-sitter-bicep) #:use-module (crates-io))

(define-public crate-tree-sitter-bicep-0.0.1 (c (n "tree-sitter-bicep") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "17zjnqwanvwz2mbg6dgv37aycmjaq1rdcd2jkdafjsyagj6nvxy6")))

(define-public crate-tree-sitter-bicep-1.0.0 (c (n "tree-sitter-bicep") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1gngw4nb7bmjv44p4p8x3af7z99cf1b3rnncy4lahdj59mblll5c")))

(define-public crate-tree-sitter-bicep-1.0.1 (c (n "tree-sitter-bicep") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "04vhi8r05wdxl54qfgyjpg472r9nc5wkyzw0ikyi42936nskczi1")))

