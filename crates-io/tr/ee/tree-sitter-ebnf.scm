(define-module (crates-io tr ee tree-sitter-ebnf) #:use-module (crates-io))

(define-public crate-tree-sitter-ebnf-0.1.0 (c (n "tree-sitter-ebnf") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1fscw3d1kx5kz0a6n3pncmgxdyn6wik4rfsvql6v0fpz6319vfd0")))

