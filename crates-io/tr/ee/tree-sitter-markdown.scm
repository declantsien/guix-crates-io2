(define-module (crates-io tr ee tree-sitter-markdown) #:use-module (crates-io))

(define-public crate-tree-sitter-markdown-0.7.0 (c (n "tree-sitter-markdown") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "1mh15569qjj5qyjwb2mjjg7zp48dn5mkrd058al2245qq44x7841")))

(define-public crate-tree-sitter-markdown-0.7.1 (c (n "tree-sitter-markdown") (v "0.7.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "187r0p8s7hzhjh0nkqkpl04fn847sk9bqrwhr46503kb77hczhf7")))

