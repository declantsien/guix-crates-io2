(define-module (crates-io tr ee tree-sitter-nix) #:use-module (crates-io))

(define-public crate-tree-sitter-nix-0.0.1 (c (n "tree-sitter-nix") (v "0.0.1") (d (list (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1c9nagn66sjms3bz801rks72dvp9bc9liq5agmkdkz1avnvr735f")))

