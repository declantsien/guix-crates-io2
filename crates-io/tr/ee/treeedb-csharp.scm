(define-module (crates-io tr ee treeedb-csharp) #:use-module (crates-io))

(define-public crate-treeedb-csharp-0.1.0-rc.5 (c (n "treeedb-csharp") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-c-sharp") (r "^0.20") (d #t) (k 0)) (d (n "treeedb") (r "^0.1.0-rc.5") (f (quote ("cli"))) (d #t) (k 0)))) (h "1qvhr6174dfadddbivpvzfd31wx0zrsqxjf83lnpgzx25m0vn68h")))

