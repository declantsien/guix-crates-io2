(define-module (crates-io tr ee tree_sitter_grep_proc_macros) #:use-module (crates-io))

(define-public crate-tree_sitter_grep_proc_macros-0.1.0-dev.0 (c (n "tree_sitter_grep_proc_macros") (v "0.1.0-dev.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0gc72dhlady7j0w4qqvs9rjqcxbqmkqlnwidxdf9q7mzrfxd89sq") (r "1.70")))

(define-public crate-tree_sitter_grep_proc_macros-0.1.0 (c (n "tree_sitter_grep_proc_macros") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0i5mrarc3sy0z6alik9nmmw7m0sf922y5kpqlj0lbsjkmnmcg8k3") (r "1.70")))

