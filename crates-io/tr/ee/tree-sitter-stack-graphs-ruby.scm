(define-module (crates-io tr ee tree-sitter-stack-graphs-ruby) #:use-module (crates-io))

(define-public crate-tree-sitter-stack-graphs-ruby-0.0.1 (c (n "tree-sitter-stack-graphs-ruby") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4") (o #t) (d #t) (k 0)) (d (n "tree-sitter-ruby") (r "^0.20.0") (d #t) (k 0)) (d (n "tree-sitter-stack-graphs") (r "^0.6.0") (d #t) (k 0)) (d (n "tree-sitter-stack-graphs") (r "^0.6.0") (f (quote ("cli"))) (d #t) (k 2)))) (h "0il9cnxcrp7hb7gfxbn99rn2yzcrqz0cjc0x5rz30zhn6d1v0wnl") (f (quote (("cli" "anyhow" "clap" "tree-sitter-stack-graphs/cli"))))))

