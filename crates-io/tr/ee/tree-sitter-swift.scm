(define-module (crates-io tr ee tree-sitter-swift) #:use-module (crates-io))

(define-public crate-tree-sitter-swift-0.1.0 (c (n "tree-sitter-swift") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0iz68i7lkxj7vpwxpy0va0sg9sdr9awcvhkn0wh7x1bz9w6vqp85")))

(define-public crate-tree-sitter-swift-0.1.1 (c (n "tree-sitter-swift") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1n35k84zmpss6pkhhwpyrz39iiin6ic21n01nya6hn2bppig3013")))

(define-public crate-tree-sitter-swift-0.1.2 (c (n "tree-sitter-swift") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0pxnb6pzpdga5iqxgcilbc1b4avqc1kk5hmrm60j7g5zf36fnhyp")))

(define-public crate-tree-sitter-swift-0.1.3 (c (n "tree-sitter-swift") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1p5h53dnvisbykbfjiz8znvc0lpn5f8awjrzhkqy0lm1xgpfv9x7")))

(define-public crate-tree-sitter-swift-0.1.4 (c (n "tree-sitter-swift") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1vqqyp4lrr8c5jgxplrc49cqm1n55gacqjljyg1ya43dxvyjmvnq")))

(define-public crate-tree-sitter-swift-0.2.0 (c (n "tree-sitter-swift") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0aw1gy84x7apss9ccs9v9sdb8npsm9hfd2n9zzz5j1s46npfzxf2")))

(define-public crate-tree-sitter-swift-0.3.0 (c (n "tree-sitter-swift") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0l903mn36fjjy9g18r3a0dxf8g7mmkbnldw8rv79c2ks7k9100q9")))

(define-public crate-tree-sitter-swift-0.3.1 (c (n "tree-sitter-swift") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1lj6k822as3rq0k7g51dc4gfqyn1mffdfk2p3haag642xz9m6kcs")))

(define-public crate-tree-sitter-swift-0.3.2 (c (n "tree-sitter-swift") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1jb3mdq921lhh8w2446a5qxxrj9x1y985crnmdmmp5hhlvpv728k")))

(define-public crate-tree-sitter-swift-0.3.3 (c (n "tree-sitter-swift") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1i4vgj5q8f016zs50y6lzdyzvzz33hym5ykd6qfdyia8xhi2khmc")))

(define-public crate-tree-sitter-swift-0.3.4 (c (n "tree-sitter-swift") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "045m4ngsvsg3kmm6hn7zpfg6q1i2ldjmn69rc83d6k1cg5mdzq3z")))

(define-public crate-tree-sitter-swift-0.3.5 (c (n "tree-sitter-swift") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.4") (d #t) (k 0)))) (h "02q78zm31wnbrfiq5n93mryasvy1ahb9i5x3qh4dxg0xn3qs2v9q")))

(define-public crate-tree-sitter-swift-0.3.6 (c (n "tree-sitter-swift") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.4") (d #t) (k 0)))) (h "0601wxkjx1n39ygga73adxkwz6f7lsykygl8wkcs320s23mxpqpf")))

(define-public crate-tree-sitter-swift-0.4.0 (c (n "tree-sitter-swift") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.4") (d #t) (k 0)))) (h "1rvbxk6fb1j5z2gai4aifyyzv11pzv9vh01lv7fa10jbl7h6wbj5")))

(define-public crate-tree-sitter-swift-0.4.1 (c (n "tree-sitter-swift") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.4") (d #t) (k 0)))) (h "0zx70qp7nkrv3s2d632mwscjanh8cgqbiq4s1r4s59067k009prm")))

(define-public crate-tree-sitter-swift-0.4.2 (c (n "tree-sitter-swift") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.20.4, <0.23.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.4") (d #t) (k 2)))) (h "12chhxijhq6kah1j9n4p4cvqbkh4l3ax4kgmqan6g9mnq1zhkcrn")))

(define-public crate-tree-sitter-swift-0.4.3 (c (n "tree-sitter-swift") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.4") (d #t) (k 0)))) (h "03rnjn9y3060sqbdpn4128q3wspp3i6wb00giz5mfc28f81wnpp5")))

(define-public crate-tree-sitter-swift-0.5.0 (c (n "tree-sitter-swift") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "0hwcl8nnzdc68b3b5vgkxncc6ky0czl2ndr0gbfml70j83w4mzbm")))

