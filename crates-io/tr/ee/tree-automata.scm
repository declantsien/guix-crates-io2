(define-module (crates-io tr ee tree-automata) #:use-module (crates-io))

(define-public crate-tree-automata-0.1.0 (c (n "tree-automata") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "terms") (r "^0.1.1") (d #t) (k 0)))) (h "0swwxwj6q4mkr64jfs8ns7wsjn8pnmgb6l5wbb665667xjipf47w")))

(define-public crate-tree-automata-0.1.1 (c (n "tree-automata") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "terms") (r "^0.1.2") (d #t) (k 0)))) (h "1wwpw10hfylc9w6gaql9yymxnbfbvf89rfy58w83f0xd96i116d6")))

(define-public crate-tree-automata-0.1.2 (c (n "tree-automata") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "terms") (r "^0.1.2") (d #t) (k 0)))) (h "0ha78xwy3dpbpfci8ddndw20vsdq6yg04bnhy4nmnpl39wmc2994")))

