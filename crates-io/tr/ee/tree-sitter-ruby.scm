(define-module (crates-io tr ee tree-sitter-ruby) #:use-module (crates-io))

(define-public crate-tree-sitter-ruby-0.16.2 (c (n "tree-sitter-ruby") (v "0.16.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.17") (d #t) (k 0)))) (h "05yn6l7xl0xfk0ihicrd22qk3f08547iszh0qrxjhaa56vmvbpsh")))

(define-public crate-tree-sitter-ruby-0.17.0 (c (n "tree-sitter-ruby") (v "0.17.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.17") (d #t) (k 0)))) (h "1kk3j6m425ad0d2pa81swzy4jd4wx08jnwzmyk8sy3ywnqdq93j2")))

(define-public crate-tree-sitter-ruby-0.19.0 (c (n "tree-sitter-ruby") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "13960lbw8wj449832nv0vw2y0ylcpgh6n12jf1c5yjl65g8xkf1q")))

(define-public crate-tree-sitter-ruby-0.20.0 (c (n "tree-sitter-ruby") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "06kipf34n07czixc93123r18g46rsr1ybk8wdvkkldk02nxhrhqa")))

(define-public crate-tree-sitter-ruby-0.20.1 (c (n "tree-sitter-ruby") (v "0.20.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1zi74hxv8196l1xjl6b80fwqrvdc3yq5yk02bx4gi7a6hgrhxma4")))

(define-public crate-tree-sitter-ruby-0.21.0 (c (n "tree-sitter-ruby") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1cgh847mc8fs98j82k40l6ci0q986k208wdpszdg4wh7gil1y0y0")))

