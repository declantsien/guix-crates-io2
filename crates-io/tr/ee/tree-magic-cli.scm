(define-module (crates-io tr ee tree-magic-cli) #:use-module (crates-io))

(define-public crate-tree-magic-cli-0.1.0 (c (n "tree-magic-cli") (v "0.1.0") (d (list (d (n "built") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)) (d (n "tree_magic_mini") (r "^1.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1gxvgyz4r85s8zd27mkpgskn4jwq3pxcihgykyxqlmaaddxbl0pl")))

