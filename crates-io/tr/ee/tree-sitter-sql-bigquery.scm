(define-module (crates-io tr ee tree-sitter-sql-bigquery) #:use-module (crates-io))

(define-public crate-tree-sitter-sql-bigquery-0.0.5 (c (n "tree-sitter-sql-bigquery") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)))) (h "1zaflw23fb8gz1b0gdrj0s8sf1q3dxxbza24ly05pc43742hhads")))

(define-public crate-tree-sitter-sql-bigquery-0.0.6 (c (n "tree-sitter-sql-bigquery") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)))) (h "1bqk4h1278hns18zl69dafl5y4i7mqqrdjbl0ijw7jsddmr4427j")))

(define-public crate-tree-sitter-sql-bigquery-0.0.7 (c (n "tree-sitter-sql-bigquery") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)))) (h "0id93msnhg4wl3mmkz1f1jyifm7d6zq7wskg2wjcpndm77967zqa")))

(define-public crate-tree-sitter-sql-bigquery-0.0.8 (c (n "tree-sitter-sql-bigquery") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)))) (h "1fll5sca2m0467dk5l3i1hqk4d9dp0s89p0sgjdy2hd62626imzr")))

(define-public crate-tree-sitter-sql-bigquery-0.0.15 (c (n "tree-sitter-sql-bigquery") (v "0.0.15") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0x385s1ds7gfycxhphyk1cb2x3nrmmdb96kq17z4glfi3syf2s89")))

(define-public crate-tree-sitter-sql-bigquery-0.0.16 (c (n "tree-sitter-sql-bigquery") (v "0.0.16") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0kwz3rib50aygjib4d228x41pha3mkkc1ncaqmjakj2sl1l2b5zh")))

(define-public crate-tree-sitter-sql-bigquery-0.0.17 (c (n "tree-sitter-sql-bigquery") (v "0.0.17") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0s5b7gqrvwhfc82j3lnlnpc7q71jw4dyxp4ws690in5bh634vqad")))

(define-public crate-tree-sitter-sql-bigquery-0.0.22 (c (n "tree-sitter-sql-bigquery") (v "0.0.22") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1sdnk2svhs17aarv2h350nj5p6lmvp34gc4skf1mn7jfgkgc6ngp")))

(define-public crate-tree-sitter-sql-bigquery-0.1.1 (c (n "tree-sitter-sql-bigquery") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0xchshp058xd8rkw6fcmfl94x9g3zzrx68wk242lmy5wymnlp8v0")))

(define-public crate-tree-sitter-sql-bigquery-0.1.2 (c (n "tree-sitter-sql-bigquery") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0d837hwn4dmi0k2jf0hhr89l4fgzkx3mmk7m8nrrnli43z42c7sn")))

(define-public crate-tree-sitter-sql-bigquery-0.1.4 (c (n "tree-sitter-sql-bigquery") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1gxfiyrxwhai92j1hp0xm4d8fw3vc09n2p16f1n8gaa7k2bqvnp5")))

(define-public crate-tree-sitter-sql-bigquery-0.1.5 (c (n "tree-sitter-sql-bigquery") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0svjg1j277n6ylnpgmlinscw9lx4dxprgw521kbv3pfi0s59acl4")))

(define-public crate-tree-sitter-sql-bigquery-0.1.6 (c (n "tree-sitter-sql-bigquery") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1zhl6ncazh2j0dwxdiq8s5q7fi9c957nsvpwn6n076ffw0gyq7bv")))

(define-public crate-tree-sitter-sql-bigquery-0.1.7 (c (n "tree-sitter-sql-bigquery") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "04zxpwq7mz7pg3sdni7jhbiyhbxwphkj6q7mmim70rjggr7pn6zk")))

(define-public crate-tree-sitter-sql-bigquery-0.1.8 (c (n "tree-sitter-sql-bigquery") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1fy4pc9n6kdnk764gzpnps6jcffq8hrdyypjzzrb7s8ma09jmj56")))

(define-public crate-tree-sitter-sql-bigquery-0.1.9 (c (n "tree-sitter-sql-bigquery") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1n5xn9i3b3h1fxc4cqf9xjlh00qwmpm4h4y37z48fnaav61a1hnf")))

(define-public crate-tree-sitter-sql-bigquery-0.1.10 (c (n "tree-sitter-sql-bigquery") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1lylb4knc64nibb689z6n6qbqqgcw8wf2sdl0v521yw8m73jq9q9")))

(define-public crate-tree-sitter-sql-bigquery-0.1.11 (c (n "tree-sitter-sql-bigquery") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "04zbgy8pyjkcga1xadai81qa96fv5s93c66pdb3rkpqzdwk51sg9")))

(define-public crate-tree-sitter-sql-bigquery-0.1.12 (c (n "tree-sitter-sql-bigquery") (v "0.1.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1z9m14rb67b4cskr2xc01mphaq6r942yn8p2gjihnszpb0759f7b")))

(define-public crate-tree-sitter-sql-bigquery-0.2.0 (c (n "tree-sitter-sql-bigquery") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0wq4w4qkpidbrjsvccykk38267j0wd8krppd9za69183xgirrd3s")))

(define-public crate-tree-sitter-sql-bigquery-0.3.0 (c (n "tree-sitter-sql-bigquery") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1wh2az5yf0zjm6ym8qqfi1g19vbah1cn7v7yycahvs5154mcbkqq")))

(define-public crate-tree-sitter-sql-bigquery-0.3.1 (c (n "tree-sitter-sql-bigquery") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1cpdsc24d4023zlac771fn58zf0x4x7ang9akdjwsli5kia687ss")))

(define-public crate-tree-sitter-sql-bigquery-0.4.0 (c (n "tree-sitter-sql-bigquery") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0rlwlnpc36c30gsvg20f5x6fwj300sda5arvgr2l44048dqlfmhm")))

(define-public crate-tree-sitter-sql-bigquery-0.4.1 (c (n "tree-sitter-sql-bigquery") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0qv6ic71q7b58rlwng67ppzslv1iakw6ys52w2zggia0v0dfcdi9")))

(define-public crate-tree-sitter-sql-bigquery-0.4.2 (c (n "tree-sitter-sql-bigquery") (v "0.4.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "14dw2cf7zdhmqqwypnfpmywj3sx1lp5a0v39k8yvj7r61h2bv07r")))

(define-public crate-tree-sitter-sql-bigquery-0.5.0 (c (n "tree-sitter-sql-bigquery") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1fc4195vp613r2jhnw6zznxi0fgrf8xnmc83qxypgjin704yw6h4")))

(define-public crate-tree-sitter-sql-bigquery-0.6.0 (c (n "tree-sitter-sql-bigquery") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1k48dr4a3lacmjmjimks1qrkcmya701byl26jsn5daww1vdjh9if")))

(define-public crate-tree-sitter-sql-bigquery-0.7.0 (c (n "tree-sitter-sql-bigquery") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.23") (d #t) (k 0)))) (h "16ix40q2vrd4nqzpv3s93xc9jl6y3zrfzly13w1a3163y0fg4cl1")))

(define-public crate-tree-sitter-sql-bigquery-0.7.1 (c (n "tree-sitter-sql-bigquery") (v "0.7.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.23") (d #t) (k 0)))) (h "0h1vcib5xikbiq3zvdr89djcwf04rk50z1x5n3skv69maidwph91")))

(define-public crate-tree-sitter-sql-bigquery-0.8.0 (c (n "tree-sitter-sql-bigquery") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.23") (d #t) (k 0)))) (h "0qz4fmlf5q3c7rsbgx4lg21xbjinpfdmmy8ca7njbkfx21dqm583")))

