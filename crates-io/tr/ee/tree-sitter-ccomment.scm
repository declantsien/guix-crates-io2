(define-module (crates-io tr ee tree-sitter-ccomment) #:use-module (crates-io))

(define-public crate-tree-sitter-ccomment-0.19.0 (c (n "tree-sitter-ccomment") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)))) (h "16nnxm76b22pax1w1kszgjww7qk0fan9mdasgr2vn9wrafy05d6k")))

(define-public crate-tree-sitter-ccomment-0.20.1 (c (n "tree-sitter-ccomment") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)))) (h "1fcbkhiycszf84zk35vwblhjf28h427l7j9flk20gbjhsf2nwd4y")))

