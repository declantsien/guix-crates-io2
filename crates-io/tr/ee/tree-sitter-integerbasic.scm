(define-module (crates-io tr ee tree-sitter-integerbasic) #:use-module (crates-io))

(define-public crate-tree-sitter-integerbasic-1.0.1 (c (n "tree-sitter-integerbasic") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.6") (d #t) (k 0)))) (h "06ndiqjxfndnyqzw1l9zfawc7zzrb5qybs5vrb654grnv581q1kr")))

(define-public crate-tree-sitter-integerbasic-1.0.2 (c (n "tree-sitter-integerbasic") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "0slxchzj0zkhg3mahkhy9bsp8hk18gcw9gy2857hlvgzyv6ihhnw")))

(define-public crate-tree-sitter-integerbasic-1.0.3 (c (n "tree-sitter-integerbasic") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1zc69prkg0102d38sgf03hq352hdyhgwbq91r6k3g2h4g4vqljgd")))

(define-public crate-tree-sitter-integerbasic-1.1.1 (c (n "tree-sitter-integerbasic") (v "1.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.4") (d #t) (k 0)))) (h "0sbq7y1yzkrg5pc983nr72nzzdmvkv1wrysm3p53zqiizr4ihc9w")))

