(define-module (crates-io tr ee treesize) #:use-module (crates-io))

(define-public crate-treesize-0.0.1 (c (n "treesize") (v "0.0.1") (d (list (d (n "tabwriter") (r "^0.1") (d #t) (k 0)))) (h "17rmk1rmsyrfkvbamgivi48izwp5bs3fcidisajyqbnfpx7hf470")))

(define-public crate-treesize-0.1.0 (c (n "treesize") (v "0.1.0") (d (list (d (n "tabwriter") (r "^0.1") (d #t) (k 0)))) (h "0a5ab4rgsp5r4w3hhhh7519gwsqkm0wqw2mw9lg0wdppsk33a0kr")))

(define-public crate-treesize-0.1.1 (c (n "treesize") (v "0.1.1") (d (list (d (n "tabwriter") (r "^0.1") (d #t) (k 0)))) (h "1n2ryx3mzf840j0g5r25q130nikrfli2zgx2msm04977yjr61jrp")))

(define-public crate-treesize-0.1.2 (c (n "treesize") (v "0.1.2") (d (list (d (n "tabwriter") (r "^0.1") (d #t) (k 0)))) (h "0s7r681r2n2q15s53rpw0z57yqgi6fl0rhb01ascmdxxrk1pxshx")))

(define-public crate-treesize-0.2.0 (c (n "treesize") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "tabwriter") (r "^0.1") (d #t) (k 0)))) (h "1haly6366h1zd4viiq90mzh3zaajcvsmn1jlp7cg3nmkk8f3fl7b")))

(define-public crate-treesize-0.2.1 (c (n "treesize") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "tabwriter") (r "^0.1") (d #t) (k 0)))) (h "1x13dyfdsvrd4135a48w6krc8wg2sifb9ah35g6l3zkw9vcxwvr1")))

(define-public crate-treesize-0.3.0 (c (n "treesize") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "tabwriter") (r "^0.1") (d #t) (k 0)))) (h "1px33wmsvzqhjnk00ic4sbkdjbhkr0l3mazjg6afp9167g6kmfnw")))

(define-public crate-treesize-0.4.0 (c (n "treesize") (v "0.4.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "tabwriter") (r "^1.0") (d #t) (k 0)))) (h "1a4gannzkndmvv92s8n3jdir3bf26y5hhsz27mdnlnmzv2r536fh")))

(define-public crate-treesize-0.5.0 (c (n "treesize") (v "0.5.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "tabwriter") (r "^1.0") (d #t) (k 0)))) (h "14l52yj2b32ybi6ni2gi4vdg3k5ipn0j00dsicrckz7imra1919c")))

