(define-module (crates-io tr ee treeedbgen) #:use-module (crates-io))

(define-public crate-treeedbgen-0.1.0-rc.3 (c (n "treeedbgen") (v "0.1.0-rc.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1i552m34crcmax3n14r0v47rg6r5iyd5wndwrddz1nfn3kw0qdrl") (f (quote (("default")))) (y #t) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-treeedbgen-0.1.0-rc.4 (c (n "treeedbgen") (v "0.1.0-rc.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06n8f9x2csa8dkihq35ivcs3cg8vyr7p0ja2iy5m9ip55xflypxy") (f (quote (("default")))) (y #t) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-treeedbgen-0.1.0-rc.5 (c (n "treeedbgen") (v "0.1.0-rc.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0pp5xvnkcp586jqc9nmsiw7mwxrq6qxbsiz695nhy7mzm0f8lm3x") (f (quote (("default")))) (s 2) (e (quote (("cli" "dep:clap"))))))

