(define-module (crates-io tr ee treelike) #:use-module (crates-io))

(define-public crate-treelike-0.1.0 (c (n "treelike") (v "0.1.0") (h "17gi984yy6gr6gpcv0iyp31bkz6a011mys0102hrgsg343k7s6qb") (f (quote (("std") ("default" "std"))))))

(define-public crate-treelike-0.2.0 (c (n "treelike") (v "0.2.0") (h "165dffv8q0s9kl1lds3w1gflf8rkpym6805csymkn1cq9cq59r4m") (f (quote (("default" "alloc") ("alloc"))))))

