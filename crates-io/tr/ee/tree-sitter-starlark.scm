(define-module (crates-io tr ee tree-sitter-starlark) #:use-module (crates-io))

(define-public crate-tree-sitter-starlark-0.0.1 (c (n "tree-sitter-starlark") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0kly84zwwlpgv13b19yxf8fk1h20228zxd7rg88n4xrs93ly2mrd")))

(define-public crate-tree-sitter-starlark-0.0.2 (c (n "tree-sitter-starlark") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1sj03b5ssd0zw9xc45x0g3gs2cxgfnb16z8qwxx8i32lxq4yrwvi")))

(define-public crate-tree-sitter-starlark-0.0.3 (c (n "tree-sitter-starlark") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)))) (h "06kgixvycf5pbf7kbbibrmls7z5g3syy301d4zrjvyswy9s6a8lg")))

(define-public crate-tree-sitter-starlark-1.0.0 (c (n "tree-sitter-starlark") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)))) (h "0h5j8mx62y45w6hpgb3cyybgqa0nijp0ngyacxjh21gxza1pjacr")))

(define-public crate-tree-sitter-starlark-1.1.0 (c (n "tree-sitter-starlark") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)))) (h "08ynsazm38069jp72xd0x1yssz28prbvb7b88yf72lpp9g5qc5kk")))

(define-public crate-tree-sitter-starlark-1.2.0 (c (n "tree-sitter-starlark") (v "1.2.0") (d (list (d (n "cc") (r "^1.0.98") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1k4dz23bgx68bsl8kh2wm177c0x1s4k5mkgg6h3j4mw276101kvj")))

