(define-module (crates-io tr ee tree-sitter-bucket) #:use-module (crates-io))

(define-public crate-tree-sitter-bucket-0.0.1 (c (n "tree-sitter-bucket") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1aqy1zidbdchqsnzh2v6lxvkf7h0ff5bdpraxkqd4ina68slpwhr")))

(define-public crate-tree-sitter-bucket-0.0.2 (c (n "tree-sitter-bucket") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0q022lbh648w066dmirpsy8x2v3h2693w6k65sbkp81v2jl7d4m6")))

(define-public crate-tree-sitter-bucket-0.0.3 (c (n "tree-sitter-bucket") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1lgb9xkzypxhmpfwkz3sqs9lah138m78crdfcqqwhz2pdvbdqngi")))

