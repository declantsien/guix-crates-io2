(define-module (crates-io tr ee tree-cli) #:use-module (crates-io))

(define-public crate-tree-cli-0.1.0 (c (n "tree-cli") (v "0.1.0") (h "01zirm0pckp629g8yaywmvvv1r6gvcvz3f5iwpgrvjr8nm1zccbc") (y #t)))

(define-public crate-tree-cli-0.1.1 (c (n "tree-cli") (v "0.1.1") (h "1p0fmdp3js0c20q2fbnhmicgwn56xl9nksag5hr1wr4m1n3gv8f5")))

