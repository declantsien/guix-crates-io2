(define-module (crates-io tr ee tree-sitter-solidity-unofficial) #:use-module (crates-io))

(define-public crate-tree-sitter-solidity-unofficial-0.0.1 (c (n "tree-sitter-solidity-unofficial") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.4") (d #t) (k 0)))) (h "0qsa9w9kimzg6w4w8hgj82icgflymv1bvms7hryy3r5rbvi9wldp")))

(define-public crate-tree-sitter-solidity-unofficial-0.0.2 (c (n "tree-sitter-solidity-unofficial") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter-unofficial") (r "^0.0.1") (d #t) (k 0)))) (h "19s6bj49mr7msm0a32mkhqc5fam9zwbrz571m12p019kdkrp6rjx")))

