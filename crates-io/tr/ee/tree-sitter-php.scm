(define-module (crates-io tr ee tree-sitter-php) #:use-module (crates-io))

(define-public crate-tree-sitter-php-0.20.0 (c (n "tree-sitter-php") (v "0.20.0") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1plp0wrj2c6j8pksfi46fqb7nwvz9q02h1swgrqg1lbvlnm8kdhq")))

(define-public crate-tree-sitter-php-0.21.1 (c (n "tree-sitter-php") (v "0.21.1") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0sh3h4ag2n73m2wzr8p5z6h42kh8d9l4nfk8hfsxynlsf277icqd")))

(define-public crate-tree-sitter-php-0.22.0 (c (n "tree-sitter-php") (v "0.22.0") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "12rb93f0j8bmjin2rcr9c41nynfcjg12bgjvk6i2s6bpq0mqhzaa")))

(define-public crate-tree-sitter-php-0.22.1 (c (n "tree-sitter-php") (v "0.22.1") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0a5cjps5fnp9d7n8pnwn8zkv7x6g7l2jd93ym5904s332nijvr7v")))

(define-public crate-tree-sitter-php-0.22.2 (c (n "tree-sitter-php") (v "0.22.2") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "19gxnnixvjiagvmzy6hw565lgkl8qnz7hfzxsi8iw3hz08mnzd6p")))

(define-public crate-tree-sitter-php-0.22.3 (c (n "tree-sitter-php") (v "0.22.3") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1wbicv60zznxdgipqbfycgzrv87r0621l1ljdh1z4ibc9hv67s63")))

(define-public crate-tree-sitter-php-0.22.4 (c (n "tree-sitter-php") (v "0.22.4") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "0pmg9bmrpd7ky257zqckr24njdpdg2wzcvnr5hdy2bz3q30a3lmw")))

(define-public crate-tree-sitter-php-0.22.5 (c (n "tree-sitter-php") (v "0.22.5") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "14qm6ibbld5v2l0qf3f1l6kfpxrs4cgpis48fsp5q40phbgrk4jp")))

