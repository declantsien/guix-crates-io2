(define-module (crates-io tr ee tree-sitter-cuda) #:use-module (crates-io))

(define-public crate-tree-sitter-cuda-0.0.1 (c (n "tree-sitter-cuda") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "0mxii0qs8vpnkl4lbsz9l5fmarhmv7alil10h5xxs28s8d5il143")))

(define-public crate-tree-sitter-cuda-0.0.2 (c (n "tree-sitter-cuda") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "0jyny5y12hny94db4frf72rk00rbk823gxkwjbcs5jlkmhgvsja4")))

(define-public crate-tree-sitter-cuda-0.0.3 (c (n "tree-sitter-cuda") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "1m46srd56fadgy67ydzdk8ps6k1h6bs9b7bj7400wnijgqf3vgn9")))

(define-public crate-tree-sitter-cuda-0.0.4 (c (n "tree-sitter-cuda") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "0sb5csdnj9ac8r2hb1y7nydyya126g97mmfqbjbvma5x8p22gmv0")))

(define-public crate-tree-sitter-cuda-0.1.0 (c (n "tree-sitter-cuda") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.1") (d #t) (k 0)))) (h "0gsaa6n3p4r2cn9drdrq5la56gi339y0xk20i4qxnqd25by5jl6v")))

(define-public crate-tree-sitter-cuda-0.20.2 (c (n "tree-sitter-cuda") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.1") (d #t) (k 0)))) (h "1ybfbv51kg95yhqjpx9n3drir7x6w08f38rpiyqfim969rb2l5y5")))

(define-public crate-tree-sitter-cuda-0.20.3 (c (n "tree-sitter-cuda") (v "0.20.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "11xcawg808v497v35078fr7p70rw0qgziqm7i79a452g27ma8ny4")))

(define-public crate-tree-sitter-cuda-0.20.4 (c (n "tree-sitter-cuda") (v "0.20.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0lp5h9qj49llg3b384r2m8jj775a6ralmkndhlv65cnff4afl768")))

