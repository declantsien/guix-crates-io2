(define-module (crates-io tr ee tree-sitter-tmux) #:use-module (crates-io))

(define-public crate-tree-sitter-tmux-0.0.1 (c (n "tree-sitter-tmux") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0xnkv2n1d1msqg18h9iq1qzlvb614b1hdx7xh9iila6wxwdyg75a")))

(define-public crate-tree-sitter-tmux-0.0.2 (c (n "tree-sitter-tmux") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0ji1w70wcn644m0w33h5a55w31d10ccia6s4vcvkw2kfyjf7n2rb")))

(define-public crate-tree-sitter-tmux-0.0.3 (c (n "tree-sitter-tmux") (v "0.0.3") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.2") (d #t) (k 0)))) (h "0h0gx47shs0c0dcy0cy3bap6cgbspqjp2g09sa2sfl6v2351a89c")))

