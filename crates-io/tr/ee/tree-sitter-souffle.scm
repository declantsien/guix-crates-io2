(define-module (crates-io tr ee tree-sitter-souffle) #:use-module (crates-io))

(define-public crate-tree-sitter-souffle-0.1.0 (c (n "tree-sitter-souffle") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "156q41si61nq2q7jgcch7ckxw0ki6m5cfs7cavyinndk0xlv0gbx") (y #t)))

(define-public crate-tree-sitter-souffle-0.3.0 (c (n "tree-sitter-souffle") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1qnrh9l9qhwf7ylw3qcaraz1q3fr6f04hgjvx9wz4azb4y8gr4xm")))

(define-public crate-tree-sitter-souffle-0.4.0 (c (n "tree-sitter-souffle") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "12zz3pzbkwyfq8199wqj2k0bpf46vrrgijgacr4542krl9dg5x0h")))

