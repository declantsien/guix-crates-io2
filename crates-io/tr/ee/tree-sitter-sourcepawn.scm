(define-module (crates-io tr ee tree-sitter-sourcepawn) #:use-module (crates-io))

(define-public crate-tree-sitter-sourcepawn-0.1.0 (c (n "tree-sitter-sourcepawn") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "0506lvmn1ijl65hx480hchbylaz7lwvv2cicwx8r93dg0wm4l4s9")))

(define-public crate-tree-sitter-sourcepawn-0.3.0 (c (n "tree-sitter-sourcepawn") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "1z0hzxc8j479r4vl4ig25gq1c045fi6chasxqwx4g74j136mjr2z")))

(define-public crate-tree-sitter-sourcepawn-0.3.1 (c (n "tree-sitter-sourcepawn") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "0nyv8ryiajxh9qqac04wjrcz4li5igbxv3hb8v4qw1i9zi27bcxs")))

(define-public crate-tree-sitter-sourcepawn-0.4.0 (c (n "tree-sitter-sourcepawn") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "0llrif7cs6skyy991zpzj9la6kv7s6aas2hch8fpld5yy1zqdswb")))

(define-public crate-tree-sitter-sourcepawn-0.4.1 (c (n "tree-sitter-sourcepawn") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "1dw7sa0bi7q0xja30bfn146f7hq6xs2zyfg7f79bhmdrfg08m9is")))

(define-public crate-tree-sitter-sourcepawn-0.5.0 (c (n "tree-sitter-sourcepawn") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "0rdldrjmb5x83yi7ss8kk01y49jiwzf2jqwyxg6am76s23vnrvz4")))

(define-public crate-tree-sitter-sourcepawn-0.5.1 (c (n "tree-sitter-sourcepawn") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "1lja5b2z7r23rbl7q1dxh3cbva8nyjglpclisnaza1gzv0qcy2ww")))

(define-public crate-tree-sitter-sourcepawn-0.6.0 (c (n "tree-sitter-sourcepawn") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "08wz03rka6bs35vk7r5k0pxxr4358anqbpk698mm74z7pr66hc2m")))

(define-public crate-tree-sitter-sourcepawn-0.7.0 (c (n "tree-sitter-sourcepawn") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "08vw8ffnrlmncpkk4qr17jb6hp49473xvsf6yw6m91rzljzl3gz8")))

(define-public crate-tree-sitter-sourcepawn-0.7.1 (c (n "tree-sitter-sourcepawn") (v "0.7.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "1b0dhgmdh373w1hckzk9q2kj134n9ywl03c2l6lm094bf675l2ir")))

(define-public crate-tree-sitter-sourcepawn-0.7.2 (c (n "tree-sitter-sourcepawn") (v "0.7.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.21.0") (d #t) (k 0)))) (h "1b47lsn6wp7lbvh375ljvz4b6rx1gl935mcaj6p2irb0pzhki8j9")))

(define-public crate-tree-sitter-sourcepawn-0.7.3 (c (n "tree-sitter-sourcepawn") (v "0.7.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.21.0") (d #t) (k 0)))) (h "1q4a8c7mr65k61jqp3304fgz3ysq53av6khn0qs6m3hh0p19a5q4")))

(define-public crate-tree-sitter-sourcepawn-0.7.4 (c (n "tree-sitter-sourcepawn") (v "0.7.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.21.0") (d #t) (k 0)))) (h "13slbhg90igdrymq3la3vpg0gbvpq1g482psklybn2zrpy0j3xc3")))

(define-public crate-tree-sitter-sourcepawn-0.7.5 (c (n "tree-sitter-sourcepawn") (v "0.7.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.21.0") (d #t) (k 0)))) (h "1nkwvm9mmiwaksdpbgkzd8dkl21x5qfzdqfc1imnglx7mcs2dy4y")))

