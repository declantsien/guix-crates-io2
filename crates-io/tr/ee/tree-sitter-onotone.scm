(define-module (crates-io tr ee tree-sitter-onotone) #:use-module (crates-io))

(define-public crate-tree-sitter-onotone-0.1.0 (c (n "tree-sitter-onotone") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1jwllrm9vxaj0zc832fcjmyqbcss4ja5c86kxvxfv6295pnc8z70")))

(define-public crate-tree-sitter-onotone-0.1.1 (c (n "tree-sitter-onotone") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "13mrjcd74zy8j0ar878zqdg0lbrv54nph91vnrw093dvqnsriqg0")))

