(define-module (crates-io tr ee tree-sitter-wdl-1) #:use-module (crates-io))

(define-public crate-tree-sitter-wdl-1-0.1.0 (c (n "tree-sitter-wdl-1") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0nlq5gnw7gr4k2sg6xhz637cf7jk807hb2505l0xq98xnlmc8r70")))

(define-public crate-tree-sitter-wdl-1-0.1.1 (c (n "tree-sitter-wdl-1") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1lx84iq3nlf4n1dzad91nldlnkpjks3k4pm9bsi9yrrppw7ljpjk")))

(define-public crate-tree-sitter-wdl-1-0.1.2 (c (n "tree-sitter-wdl-1") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "02fixg2r2a6gwrmh7n62xmviqp5q8ck5f08vjws3ig2938ssak13")))

(define-public crate-tree-sitter-wdl-1-0.1.3 (c (n "tree-sitter-wdl-1") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "11lw6kn3pyp2p07wjykp0w9v29q55mwvw2dh2l1xr0z643mz9gdx")))

(define-public crate-tree-sitter-wdl-1-0.1.4 (c (n "tree-sitter-wdl-1") (v "0.1.4") (d (list (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0pks2qc78704gnhzj5a8lhcb1h68rwp40z4sbimjzrh9mrkqc52y")))

(define-public crate-tree-sitter-wdl-1-0.1.5 (c (n "tree-sitter-wdl-1") (v "0.1.5") (d (list (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1sbqrzfpg6flpqprll4b62mcrpjmqm4qmfbfrp4bwrrb6j6ihg9m")))

(define-public crate-tree-sitter-wdl-1-0.1.6 (c (n "tree-sitter-wdl-1") (v "0.1.6") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0mijls18kbzpg59r6g9vl4qr3bc3m01pbs47sypic4msrkfjjsn2")))

(define-public crate-tree-sitter-wdl-1-0.1.7 (c (n "tree-sitter-wdl-1") (v "0.1.7") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0kgx0p9b8jqpbvm7p9i0vdzplvidfk9sk5a9lwn5jlpqnljd3fw1")))

(define-public crate-tree-sitter-wdl-1-0.1.8 (c (n "tree-sitter-wdl-1") (v "0.1.8") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "12b5z3kn9ylqkm07yca4nn81ii924g0dly9j7k8954jq4lfwyfii")))

(define-public crate-tree-sitter-wdl-1-0.1.10 (c (n "tree-sitter-wdl-1") (v "0.1.10") (d (list (d (n "serde_json") (r "^1.0.93") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1bj3d46zrcfhaxd2mvca763waail6phcmyhkaxgbsqk2mcy4jcd3") (s 2) (e (quote (("json" "dep:serde_json"))))))

