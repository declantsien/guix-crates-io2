(define-module (crates-io tr ee tree-sitter-typescript) #:use-module (crates-io))

(define-public crate-tree-sitter-typescript-0.19.0 (c (n "tree-sitter-typescript") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "0fxzcmnzacvrhjl02psay50gykd1iqbmxvhjqj8z4sz5qr4jvxnk")))

(define-public crate-tree-sitter-typescript-0.20.0 (c (n "tree-sitter-typescript") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0wxbzp0zk5x116zagww0pab0jn87frjcnykd5fdpq1ljgkcyydc9")))

(define-public crate-tree-sitter-typescript-0.20.1 (c (n "tree-sitter-typescript") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0vg8wq2mbq34b0wv1zyrg4knw58msv64agx1qq9zzk9ip7nd13jf")))

(define-public crate-tree-sitter-typescript-0.20.2 (c (n "tree-sitter-typescript") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "02i95phz1lkkyn8nlj53zdkyjc6ax8x3jrhw224x16nk69f6k707")))

(define-public crate-tree-sitter-typescript-0.20.3 (c (n "tree-sitter-typescript") (v "0.20.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "06h7m3pk4bssya2nfcpv1k6kvdb2l56v4ysx42n2mfzsmbq4jl57")))

(define-public crate-tree-sitter-typescript-0.20.4 (c (n "tree-sitter-typescript") (v "0.20.4") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1zpfnxc1av5mnlbrqh2s3906zxg544qiqhwzf668d8jgc9b7xy8g")))

(define-public crate-tree-sitter-typescript-0.20.5 (c (n "tree-sitter-typescript") (v "0.20.5") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0w4nc7yjg2k64jrx1y782wvvd7dci1lbawbs17plhsi74hn1vg68")))

(define-public crate-tree-sitter-typescript-0.21.0 (c (n "tree-sitter-typescript") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "0p8p0kc9zwl55vvvia8850j882d4jpiq1daj7d8h8g3mdlyqjz2a")))

(define-public crate-tree-sitter-typescript-0.21.1 (c (n "tree-sitter-typescript") (v "0.21.1") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1d6bvm68pkpf2hai8h0aniyqgvm7rk03i458c29m521v3vjj6xgh")))

