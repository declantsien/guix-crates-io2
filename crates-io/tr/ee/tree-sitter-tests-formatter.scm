(define-module (crates-io tr ee tree-sitter-tests-formatter) #:use-module (crates-io))

(define-public crate-tree-sitter-tests-formatter-0.1.0 (c (n "tree-sitter-tests-formatter") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (f (quote ("glob" "redactions"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "145gypsjw1j1ixhd18qz6d7p6prr02crlas7fcwkrxgia5glk45j")))

