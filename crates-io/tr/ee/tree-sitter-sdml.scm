(define-module (crates-io tr ee tree-sitter-sdml) #:use-module (crates-io))

(define-public crate-tree-sitter-sdml-0.0.1 (c (n "tree-sitter-sdml") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "00c7fxhhbz1c331dnpsgdypq9qssnx4hgh041yxxdi3sqif3xa04")))

(define-public crate-tree-sitter-sdml-0.1.2 (c (n "tree-sitter-sdml") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1yh89bkxhi5fnn66cnyhxysvcqd3x126cd6khli6y17sdv3yn350")))

(define-public crate-tree-sitter-sdml-0.1.3 (c (n "tree-sitter-sdml") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "068ssi6ivzf7xaz1dhxgg9ma7ilc3qcjlim03w569337nkvw6lqk")))

(define-public crate-tree-sitter-sdml-0.1.4 (c (n "tree-sitter-sdml") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1ii6cd1b241vzk6xnibz9dzxjppcbnb6rhfbwybnaq9524wvxxkj")))

(define-public crate-tree-sitter-sdml-0.1.5 (c (n "tree-sitter-sdml") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "06zi73v2i9qpp72a0f3gddzhccx5151pvksb3029awx29p7zp5s3")))

(define-public crate-tree-sitter-sdml-0.1.6 (c (n "tree-sitter-sdml") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0d5bgj5fv354c3gjcz3if1izay8qnwiazgc6ry7wn3zfrwg78yax")))

(define-public crate-tree-sitter-sdml-0.1.7 (c (n "tree-sitter-sdml") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1alxqaxkgj299cjyqpri9nm6vsykv3alh41zk9m9rzsalvwn60np")))

(define-public crate-tree-sitter-sdml-0.1.8 (c (n "tree-sitter-sdml") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "16zc13jcq383711h4l43r7q6xfr4nsd4v558bd2gff4nkk0py78z")))

(define-public crate-tree-sitter-sdml-0.1.9 (c (n "tree-sitter-sdml") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "05k1zhwlaqh6lay6v7nnr2vdpmv1a5yfh2w9s6116paivf1hqw7y")))

(define-public crate-tree-sitter-sdml-0.1.11 (c (n "tree-sitter-sdml") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0pq4r36nhnlhbghinq90l6s4isgm8bvi0zzypc5scjms6n4yicpl")))

(define-public crate-tree-sitter-sdml-0.1.12 (c (n "tree-sitter-sdml") (v "0.1.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1zwa5dd6fb1aiylri19xhl6zz9a8hx5y95n9mh1ln1qcv5ah8nvf")))

(define-public crate-tree-sitter-sdml-0.1.13 (c (n "tree-sitter-sdml") (v "0.1.13") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0476jwr0wrr0bczdwpx1h3vcvwyfqkc21ycgp0wbya17mk79faxp")))

(define-public crate-tree-sitter-sdml-0.1.14 (c (n "tree-sitter-sdml") (v "0.1.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "03xhbcz42gq72npdppbsw2056q29iaamdg85a1qasfcf93lf3smi")))

(define-public crate-tree-sitter-sdml-0.1.15 (c (n "tree-sitter-sdml") (v "0.1.15") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1sz1ab9p6swmfks5xv5fr7h3bwc3ljx10i4mcy3f8cm7lyf2qwkq")))

(define-public crate-tree-sitter-sdml-0.1.16 (c (n "tree-sitter-sdml") (v "0.1.16") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0dsy8lazbxrknh6a20phydpjmw72k7kv0fvgyyhxr8f82jrkwcci")))

(define-public crate-tree-sitter-sdml-0.1.17 (c (n "tree-sitter-sdml") (v "0.1.17") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0gz35zrw97j7i0ml29hzzlqksdilf2y0dd1hs3wjxzzrn70mw50h")))

(define-public crate-tree-sitter-sdml-0.1.18 (c (n "tree-sitter-sdml") (v "0.1.18") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0s3slvnhgsamr54zxipp67s17nwjf6l4j9fapx9a9dyn63w5h53x")))

(define-public crate-tree-sitter-sdml-0.1.19 (c (n "tree-sitter-sdml") (v "0.1.19") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "06gvmkzq2264ifji3p79ll0wz146w1p7vj4d10w0smvw3l0m3x5l")))

(define-public crate-tree-sitter-sdml-0.1.20 (c (n "tree-sitter-sdml") (v "0.1.20") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "07grjpffsaa3qhsyk25zd8zccllsf9b8b6f9sxjdgpxxk5ch1phd")))

(define-public crate-tree-sitter-sdml-0.1.21 (c (n "tree-sitter-sdml") (v "0.1.21") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0vnmqxph6gxqk9irpwssf704yydglddbc4rckw5lnfmqnr14hdz6")))

(define-public crate-tree-sitter-sdml-0.1.22 (c (n "tree-sitter-sdml") (v "0.1.22") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1v192mn2233jw099q380ij9jn5b6kcnl0zxric9lj1baa2sjg050")))

(define-public crate-tree-sitter-sdml-0.1.23 (c (n "tree-sitter-sdml") (v "0.1.23") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1c4rd1wdh0gzwxrfw3dngs2f2fma5zizrlpwf11b8wydgv3yavpf")))

(define-public crate-tree-sitter-sdml-0.1.25 (c (n "tree-sitter-sdml") (v "0.1.25") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0nq05qr81a2mjajyx0irr8v6n6wghxc0wwpgbqhksx2xw80vvqwg")))

(define-public crate-tree-sitter-sdml-0.1.26 (c (n "tree-sitter-sdml") (v "0.1.26") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0fycqn6ng1mmiirc7plqfylx7fqlc5jr99dlicac28ls0ddjpqcd")))

(define-public crate-tree-sitter-sdml-0.1.27 (c (n "tree-sitter-sdml") (v "0.1.27") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1jwzfsz6rj0f44ahksdwhqdbsmhxrzs7g081znphqmbfvqf5fdp7")))

(define-public crate-tree-sitter-sdml-0.1.28 (c (n "tree-sitter-sdml") (v "0.1.28") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1yvmqnglw4q5aw1al2d46jx3islp4jwvbm4730mcbi4vjg1xv8la")))

(define-public crate-tree-sitter-sdml-0.1.29 (c (n "tree-sitter-sdml") (v "0.1.29") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1xp9yv8n4x9fv61xfb4xjd0rxicjhdrkvdpfcsvfw67bxm7xkz15")))

(define-public crate-tree-sitter-sdml-0.1.30 (c (n "tree-sitter-sdml") (v "0.1.30") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1wpac0hak3dpbimkgqqq5cm3ajsgcpnq97jzjiqz6a9mhvcwsyxg")))

(define-public crate-tree-sitter-sdml-0.1.31 (c (n "tree-sitter-sdml") (v "0.1.31") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1g8ppkyjbw72vfm2q9wk2pm0a4ji3l4jrrqsg4rli8ajv37pn3a8")))

(define-public crate-tree-sitter-sdml-0.1.33 (c (n "tree-sitter-sdml") (v "0.1.33") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "14dx03c6n2k9nrx9gdm1xf24x3c6g4m7dzvqrms85kzzq0ylzf5c")))

(define-public crate-tree-sitter-sdml-0.1.34 (c (n "tree-sitter-sdml") (v "0.1.34") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1wgmnjzriqkr77wmw0abc8iimyx95m0712cajsi5wn250a76clj1")))

(define-public crate-tree-sitter-sdml-0.1.35 (c (n "tree-sitter-sdml") (v "0.1.35") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0canzai6r97fs091byyayzgdxdlkd4mmhjfgyz8skxp4a7b06ph7")))

(define-public crate-tree-sitter-sdml-0.1.37 (c (n "tree-sitter-sdml") (v "0.1.37") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1haass860hn7z28dhdzpq9zazzvjri53z6kix20r5f52wgblmgfi")))

(define-public crate-tree-sitter-sdml-0.1.38 (c (n "tree-sitter-sdml") (v "0.1.38") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "09rncyk4pvymx57pvpq2ddi66hfpdmbwmn6iy6kvv8gw4rgq6c85")))

(define-public crate-tree-sitter-sdml-0.1.39 (c (n "tree-sitter-sdml") (v "0.1.39") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0n36znqff9lxm5qqlhqdrhp3y7g3m15bs71snyq211sx7vxs4mz0")))

(define-public crate-tree-sitter-sdml-0.1.40 (c (n "tree-sitter-sdml") (v "0.1.40") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0wg68bm20rnmdfsp1hc9fhw2dqj8hlfj18jwc4mpbiwkknf8091j")))

(define-public crate-tree-sitter-sdml-0.1.41 (c (n "tree-sitter-sdml") (v "0.1.41") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0vij0fdw21jivrnls2zrp2nhyc4ilm1qnzsvn8xxph8g32x090zc")))

(define-public crate-tree-sitter-sdml-0.2.0 (c (n "tree-sitter-sdml") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0q1x15kacx0b1qj2gk1rbpk9x41gp8rhzb91bi24ahg9sg9m0ffm")))

(define-public crate-tree-sitter-sdml-0.2.1 (c (n "tree-sitter-sdml") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0m3pq8x4s6k46f40dlgcaqbm1b8vqf48lbk6g4i9nngi6fl1nn0z")))

(define-public crate-tree-sitter-sdml-0.2.2 (c (n "tree-sitter-sdml") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1q2mvagxx54pniks2lscl8vbdcq6xsl1hl70ksibyzwjcadvsgxg")))

(define-public crate-tree-sitter-sdml-0.2.3 (c (n "tree-sitter-sdml") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0r6106lkibr43cbshyjhbvxfg9dlassa5qvwmyvrfaq62v00z12x")))

(define-public crate-tree-sitter-sdml-0.2.4 (c (n "tree-sitter-sdml") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0z3ifyqwqs8d49x6cqfba5p799j3axakz3lr54m9hjakybz9cgl7")))

(define-public crate-tree-sitter-sdml-0.2.5 (c (n "tree-sitter-sdml") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1w2fpyd2qkv7pxcwss21cxmw4i78fipcgi7dcap8yh8dwryxgw7g")))

(define-public crate-tree-sitter-sdml-0.2.6 (c (n "tree-sitter-sdml") (v "0.2.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1vjlw5na7brr75g8dlckjy7lqx4ybm9wqnn4vdzd56hp5p2badxb")))

(define-public crate-tree-sitter-sdml-0.2.7 (c (n "tree-sitter-sdml") (v "0.2.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "04ly0s6b5w3y9f7ja22z6xr1mwfja13k2v49vci2mjfv59inwgcp")))

(define-public crate-tree-sitter-sdml-0.2.8 (c (n "tree-sitter-sdml") (v "0.2.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0klv466z23l8r7kajpr8wizfmhigzmny6dvwbj8c3hhz2p7a8qav")))

(define-public crate-tree-sitter-sdml-0.2.9 (c (n "tree-sitter-sdml") (v "0.2.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1g2qn1b7m43jrac74y511gyrv8p058qkf9f22zm393ax7h09sg1f")))

(define-public crate-tree-sitter-sdml-0.2.10 (c (n "tree-sitter-sdml") (v "0.2.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0bsar8zhydxrn10qvqvh8zvmhvip35mgyf6ca03pqp47gg21gsgq")))

(define-public crate-tree-sitter-sdml-0.2.11 (c (n "tree-sitter-sdml") (v "0.2.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0l114yj0gi8kwaa52ppaany6jm0aph1j1d3z37qgaifxk5gbba9m")))

(define-public crate-tree-sitter-sdml-0.2.12 (c (n "tree-sitter-sdml") (v "0.2.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1h4yr7ig8pws0x6lz3ils9jngn9732bxj94ha1qz1v5abv74gjw2")))

(define-public crate-tree-sitter-sdml-0.2.14 (c (n "tree-sitter-sdml") (v "0.2.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0fj6g6qn6gvzq38rnlnh70w05977psyrdwg3nalr4q5hi790ybci")))

(define-public crate-tree-sitter-sdml-0.2.15 (c (n "tree-sitter-sdml") (v "0.2.15") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1dngys2nzwi6phhddgxjmkpk82sg94s26b78s26kw2cdcl64qi03")))

(define-public crate-tree-sitter-sdml-0.2.16 (c (n "tree-sitter-sdml") (v "0.2.16") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "15i1pf5q63871glqaz2s600rbbw6sayhflca5rzh55m6bvh8x4nq")))

(define-public crate-tree-sitter-sdml-0.3.1 (c (n "tree-sitter-sdml") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1rmiijf9kqc57pz1pbyyz6qch7q2b1dn9rfxd0njbdi9g91cd6dp")))

