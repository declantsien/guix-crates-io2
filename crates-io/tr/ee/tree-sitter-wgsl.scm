(define-module (crates-io tr ee tree-sitter-wgsl) #:use-module (crates-io))

(define-public crate-tree-sitter-wgsl-0.0.1 (c (n "tree-sitter-wgsl") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1qifvf71sj5xhik94mfd2xbkrlany666kn0brpfys9k4ckjck655")))

(define-public crate-tree-sitter-wgsl-0.0.2 (c (n "tree-sitter-wgsl") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0d1d8jn2n0sss2jwrwyp0wdxci475fzya0i5qlr9z0rnjybm95dd")))

(define-public crate-tree-sitter-wgsl-0.0.3 (c (n "tree-sitter-wgsl") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1qhvrwp1mpn35bahjww5plr2y3mdaryivahm85qgcxp47gmwwa3c")))

(define-public crate-tree-sitter-wgsl-0.0.4 (c (n "tree-sitter-wgsl") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1rg48a4j2ggjsk455gbcldvsvza0kmsaw9wmcafhdy43wsbs0v85")))

(define-public crate-tree-sitter-wgsl-0.0.5 (c (n "tree-sitter-wgsl") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.6") (d #t) (k 0)))) (h "1mdx1111wapszzp47fxslz9n5a5xz7q2j5paxl8d2rz8dr1mg8bh")))

(define-public crate-tree-sitter-wgsl-0.0.6 (c (n "tree-sitter-wgsl") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.6") (d #t) (k 0)))) (h "1qrqr6m6mzf3g8c045jvvfc8g96s07anmcz2s2cg25sqbk7bcsiy")))

