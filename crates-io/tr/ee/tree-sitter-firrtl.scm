(define-module (crates-io tr ee tree-sitter-firrtl) #:use-module (crates-io))

(define-public crate-tree-sitter-firrtl-0.8.0 (c (n "tree-sitter-firrtl") (v "0.8.0") (d (list (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1dcx2jsccb2abnp0sclz7mk6gazx4vsyvv6m18kcrlrpnvnh5dp7")))

