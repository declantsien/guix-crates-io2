(define-module (crates-io tr ee tree-sitter-r) #:use-module (crates-io))

(define-public crate-tree-sitter-r-0.19.3 (c (n "tree-sitter-r") (v "0.19.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "0ql2jmlj88n1wfdajxpnamfyvq96shjv77xjkg61v49rg5szc3qi")))

(define-public crate-tree-sitter-r-0.19.4 (c (n "tree-sitter-r") (v "0.19.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "0hzv1df8smmdk5bysfg6037pi5z7gzplgb2w1klx9377x0fixgw5")))

(define-public crate-tree-sitter-r-0.19.5 (c (n "tree-sitter-r") (v "0.19.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "036skgjbskw4b7kpvswhzm8vvylgk05d8jlxn54328a6rks16b2j")))

