(define-module (crates-io tr ee tree-sitter-beancount) #:use-module (crates-io))

(define-public crate-tree-sitter-beancount-0.0.1 (c (n "tree-sitter-beancount") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0p7i5d0aadv948kn98dvd933ad01waxivcb5ws5gmpwp224c15g9") (y #t)))

(define-public crate-tree-sitter-beancount-2.0.0 (c (n "tree-sitter-beancount") (v "2.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0hq0qcngimc07k77y5rlm9qx7v21yxydhgqvazc10qqgv3qvz6rs")))

(define-public crate-tree-sitter-beancount-2.1.0 (c (n "tree-sitter-beancount") (v "2.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1igjckrjxhrqdmcz1kmp4y955vrgnx5ba8rw6271h9nzrmbmgp90")))

(define-public crate-tree-sitter-beancount-2.1.1 (c (n "tree-sitter-beancount") (v "2.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "12wy8h0y7s8fzd91yy539rc3div1jc2gjn7flkhfk7pi49qqlb6p")))

(define-public crate-tree-sitter-beancount-2.1.2 (c (n "tree-sitter-beancount") (v "2.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0zffzwn95ihzj66npdb169c5ql9hs0h639lbwypsk3xkw7ilsr0f")))

(define-public crate-tree-sitter-beancount-2.1.3 (c (n "tree-sitter-beancount") (v "2.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "02abdv249i5srs8qg92fxjfdjzn9a5676lvdbmx08qa1zsh2l7bc")))

(define-public crate-tree-sitter-beancount-2.2.0 (c (n "tree-sitter-beancount") (v "2.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1d1ix6idam02bs0ffa7rkwarcg6nbvmkkbh48xscgbdgvfk4jpc0")))

(define-public crate-tree-sitter-beancount-2.3.1 (c (n "tree-sitter-beancount") (v "2.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0wcf6ghi22rscb1i91vmhq497z8hzr964cfna4vrkid63ymdg60a")))

(define-public crate-tree-sitter-beancount-2.3.2 (c (n "tree-sitter-beancount") (v "2.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1nkl74qi3z2cr66vmhca5si35gih70wzrmq677kiys6rv8h4wla6")))

