(define-module (crates-io tr ee tree-sitter-eds) #:use-module (crates-io))

(define-public crate-tree-sitter-eds-0.0.1 (c (n "tree-sitter-eds") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "00rqyjrc5drrjhzvx7g58lal2zqk5ck21fw44z0xyymmj4xsfbkr")))

