(define-module (crates-io tr ee tree-sitter-properties) #:use-module (crates-io))

(define-public crate-tree-sitter-properties-0.1.0 (c (n "tree-sitter-properties") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1d48602drw6f83yjy4l5z8g8912l73qf7lqjz7ac1kxd69qh4klp")))

(define-public crate-tree-sitter-properties-0.1.1 (c (n "tree-sitter-properties") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1l0sz9zbfhn6j8sv5ln086lyzsn0kbx733d5r1yj3p0q8aw2nyp0")))

(define-public crate-tree-sitter-properties-0.2.0 (c (n "tree-sitter-properties") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0dsscdxxrwhcp9p0ag42h8m24yanak1nxfj5j25bwj3f771vshq4")))

