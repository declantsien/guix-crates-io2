(define-module (crates-io tr ee tree-sitter-slang) #:use-module (crates-io))

(define-public crate-tree-sitter-slang-0.1.0 (c (n "tree-sitter-slang") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0byzq6wc2749ls10yp4wbwcjj72xnpq35p1gg8b64x1mchzlhw8q")))

(define-public crate-tree-sitter-slang-0.2.0 (c (n "tree-sitter-slang") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0wvs3mlc0njvywvi0zh9c1hmkhlqh3r7askam3qn3g90dhs9cnnd")))

(define-public crate-tree-sitter-slang-0.2.1 (c (n "tree-sitter-slang") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0qdpa6vjq9lxwh9w8c628fm5n3fk7ikmrjnhc2k0xcwmcsgfxvfj")))

(define-public crate-tree-sitter-slang-0.2.2 (c (n "tree-sitter-slang") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1m8dn2vsv3ja43gq7dvciim1asgrr4n52kn3zppmapkhkz28ccr4")))

