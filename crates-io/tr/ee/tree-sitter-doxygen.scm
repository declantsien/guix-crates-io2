(define-module (crates-io tr ee tree-sitter-doxygen) #:use-module (crates-io))

(define-public crate-tree-sitter-doxygen-1.0.0 (c (n "tree-sitter-doxygen") (v "1.0.0") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1br7a7zg0489dwb6i8x7qvkgfb4hjl09p0ka8s2dggm5gb2pjpj0")))

(define-public crate-tree-sitter-doxygen-1.1.0 (c (n "tree-sitter-doxygen") (v "1.1.0") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1qzy95q1f5rzc2na527n1545mhbgv4dhiw29y5ncnvilj53hhnxs")))

