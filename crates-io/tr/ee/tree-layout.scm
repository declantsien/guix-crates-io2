(define-module (crates-io tr ee tree-layout) #:use-module (crates-io))

(define-public crate-tree-layout-0.0.0 (c (n "tree-layout") (v "0.0.0") (d (list (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 2)) (d (n "shape-core") (r "^0.1.16") (d #t) (k 0)))) (h "1g5ndcm8v3n1lva7fllvvpqqzb8zmiii46f79xkh9z4hlpx17p54")))

(define-public crate-tree-layout-0.0.1 (c (n "tree-layout") (v "0.0.1") (d (list (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 2)) (d (n "shape-core") (r "^0.1.16") (d #t) (k 0)))) (h "0zbgdm6dszvzmv9x3f9d8lxcz89m0jhdyrmf3dw9b8zzvjygsziw")))

(define-public crate-tree-layout-0.0.2 (c (n "tree-layout") (v "0.0.2") (d (list (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 2)) (d (n "shape-core") (r "^0.1.16") (d #t) (k 0)))) (h "1fg3hraqj4j0gagsx13mvwvlylr09q0rzr8snjw08clij6gy3g27")))

