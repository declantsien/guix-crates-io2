(define-module (crates-io tr ee tree-sitter-icelang) #:use-module (crates-io))

(define-public crate-tree-sitter-icelang-0.1.0 (c (n "tree-sitter-icelang") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1883qg3vh4q15pmxqvljx0fmzycfhwzix2g2g5cv662pgw0n59ml")))

(define-public crate-tree-sitter-icelang-0.1.1 (c (n "tree-sitter-icelang") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0xvy1d5gzmaqn71j9x68a9yp9akcdvf1z3f7apiqpv11rnhha4mw")))

(define-public crate-tree-sitter-icelang-0.1.2 (c (n "tree-sitter-icelang") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "14zx2py4rgirahbnhq9hldz2xqjj0jqz1mn0r0jzlpb7yfkkb7hd")))

(define-public crate-tree-sitter-icelang-0.1.3 (c (n "tree-sitter-icelang") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1pycrrvik5xaxj3r393pvhspj0ldz5f0jrjrafc41rdjzhyds3rg")))

(define-public crate-tree-sitter-icelang-0.1.4 (c (n "tree-sitter-icelang") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1df9akpnbxf8za4b3k2qpibzrgnpq7l77r8h9j8v3fb2x7skfpcb")))

(define-public crate-tree-sitter-icelang-0.1.5 (c (n "tree-sitter-icelang") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "15y15xszqip9xkag11g6rdajhfinf7vlsfrjmh7nifz9xg10zkx7")))

(define-public crate-tree-sitter-icelang-0.1.6 (c (n "tree-sitter-icelang") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0ci4v85zc94m0i5w495qzjiirfdhr6qvb8rz3minsvksvfybpd33")))

