(define-module (crates-io tr ee tree-sitter-nasl) #:use-module (crates-io))

(define-public crate-tree-sitter-nasl-0.0.1 (c (n "tree-sitter-nasl") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "129821lxf3fiqa8jcqqsmrw3ib4vzs27lcpir9ww16wchivyg2a1")))

(define-public crate-tree-sitter-nasl-0.0.2 (c (n "tree-sitter-nasl") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0yh602k807m93nl2fbbh33s7g9jzbhck0fcmyy73yi6vslyvilp6")))

(define-public crate-tree-sitter-nasl-0.0.3 (c (n "tree-sitter-nasl") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1n4fg24pndapgx4m6ivfl2a17h7k0f0s6qjpdxx2r41gaak69877")))

(define-public crate-tree-sitter-nasl-0.0.4 (c (n "tree-sitter-nasl") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0syhy9bnqskdhwqm17h71gd46vx4ykj4bcqvq7wd1avif45d52f7")))

(define-public crate-tree-sitter-nasl-0.0.5 (c (n "tree-sitter-nasl") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "01v3g66rm3nbcqsxdcgvk1n92diwnh7pnr680c4lhi7mac1q4csb")))

(define-public crate-tree-sitter-nasl-0.0.6 (c (n "tree-sitter-nasl") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1rr6ad67ijqhk2fsmxwwj2niml953xkfin4p9hxshv95jmd0xvfb")))

(define-public crate-tree-sitter-nasl-0.0.7 (c (n "tree-sitter-nasl") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0iy7yxc56w9qg3nfkww1rfj8kwa46skmpfs7kmh2fms8fj9cifd8")))

(define-public crate-tree-sitter-nasl-0.0.8 (c (n "tree-sitter-nasl") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0k2rz87crc878rq4kq3dpa8rvjm46bpqmc5hm3vmzpdl6r1sxpgs")))

(define-public crate-tree-sitter-nasl-0.1.0 (c (n "tree-sitter-nasl") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1br5x2jfiyvakxng66rn8b181xdhpqy9bb1a7x0r7vyc104c3ab9")))

