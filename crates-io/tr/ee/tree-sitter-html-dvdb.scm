(define-module (crates-io tr ee tree-sitter-html-dvdb) #:use-module (crates-io))

(define-public crate-tree-sitter-html-dvdb-0.20.0 (c (n "tree-sitter-html-dvdb") (v "0.20.0") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0aa8lj7jq0p417i8lggzhy1bc9ki2ywfg6fkqhnaig12hi40as0w")))

