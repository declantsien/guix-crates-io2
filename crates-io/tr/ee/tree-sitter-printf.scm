(define-module (crates-io tr ee tree-sitter-printf) #:use-module (crates-io))

(define-public crate-tree-sitter-printf-0.3.0 (c (n "tree-sitter-printf") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0br6yabscx6iisa0dkxn1pp7vj6ngfzirx09zhr6594wc80fsjb2")))

(define-public crate-tree-sitter-printf-0.3.1 (c (n "tree-sitter-printf") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1g28vvpic1a99iiv3gv3qc1dlqrzcvjfd76ygpwkfrg3v8mxqkxv")))

