(define-module (crates-io tr ee treeedbgen-souffle-csharp) #:use-module (crates-io))

(define-public crate-treeedbgen-souffle-csharp-0.1.0-rc.5 (c (n "treeedbgen-souffle-csharp") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tree-sitter-c-sharp") (r "^0.20") (d #t) (k 0)) (d (n "treeedbgen-souffle") (r "^0.1.0-rc.5") (f (quote ("cli"))) (d #t) (k 0)))) (h "09302fiqrzlg89jdkz3gz12zqyzarkd53jfx3b1z8ddx8arc1xs8")))

