(define-module (crates-io tr ee tree-sitter-clingo) #:use-module (crates-io))

(define-public crate-tree-sitter-clingo-0.0.1 (c (n "tree-sitter-clingo") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0myriw9gkxla4z1dzqlim80n4l25lqvr0w19146vdfs8lp67y01p")))

(define-public crate-tree-sitter-clingo-0.0.2 (c (n "tree-sitter-clingo") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0w2waf777hs1rvsqgxl8ckdn4gx3wfzf993wbyzrfpa00zkbzprj")))

(define-public crate-tree-sitter-clingo-0.0.3 (c (n "tree-sitter-clingo") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1x6x2b6vv317bwsvf9dd7fqyw2yxar9mhkycqp77sxc8cbpf96s9")))

(define-public crate-tree-sitter-clingo-0.0.4 (c (n "tree-sitter-clingo") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0x7mca6m7pay3xrfiiycf637xm59171r0fk2gkv7b0bb4c8w3bz1")))

(define-public crate-tree-sitter-clingo-0.0.5 (c (n "tree-sitter-clingo") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0mazv6h2x3p9b05bswbm4c9nczfi43wkj5njawdbp7zw8zanvfr2")))

(define-public crate-tree-sitter-clingo-0.0.6 (c (n "tree-sitter-clingo") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1bm4m4bwdq3z5lgr1mkgv1a9bgwah3sqqcws733hn6n4jjhp5kvw")))

(define-public crate-tree-sitter-clingo-0.0.7 (c (n "tree-sitter-clingo") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0i9kwi709bg4c4dgbak4nzhqqbyak0bmfinqh69bf5pr0kb3nqca")))

(define-public crate-tree-sitter-clingo-0.0.8 (c (n "tree-sitter-clingo") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "12kqspcd052pi1sifpfynvc7ksc1y2qmm1xc6ypygvii9316hk7k")))

(define-public crate-tree-sitter-clingo-0.0.9 (c (n "tree-sitter-clingo") (v "0.0.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "12aih96s74dylpbpa5xbq94wfa2vf6fndgvl9dlg2h65scc1w6l4")))

(define-public crate-tree-sitter-clingo-0.0.10 (c (n "tree-sitter-clingo") (v "0.0.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1q35jnpckap75vrqm8c8wcjf6qmz486jzybd7fijqgiv7vcngdqi")))

(define-public crate-tree-sitter-clingo-0.0.11 (c (n "tree-sitter-clingo") (v "0.0.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0kkv9isr7xpz3i3zyalb88lc8j0l7i80sq1q8dv9l6zz09z0ji57")))

(define-public crate-tree-sitter-clingo-0.0.12 (c (n "tree-sitter-clingo") (v "0.0.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "06406cqb0j66ph8p679mhy9iszvz44dqkxs3hv56ng641pbwfyr5")))

(define-public crate-tree-sitter-clingo-0.0.13 (c (n "tree-sitter-clingo") (v "0.0.13") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.5") (d #t) (k 0)))) (h "1fnxxanbkx241hsiqsm9xwja11yhs48c9qbv7cfb1m2mxiqq7ska")))

