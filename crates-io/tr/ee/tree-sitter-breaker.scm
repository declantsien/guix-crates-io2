(define-module (crates-io tr ee tree-sitter-breaker) #:use-module (crates-io))

(define-public crate-tree-sitter-breaker-0.1.0 (c (n "tree-sitter-breaker") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1546ag52v79xvr05xiipsqn3vm7xdwfz437p4yp0chi6af6vaflz")))

(define-public crate-tree-sitter-breaker-0.1.1 (c (n "tree-sitter-breaker") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0v6ia37axf0pp4zk9cp07b3my6f8a9pailbp2gvxfz9lvf2g3205")))

(define-public crate-tree-sitter-breaker-0.2.0 (c (n "tree-sitter-breaker") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "03bz8lad8vdyxxm2zp3mk1qh9g0p00di20ylv7666c40q2lzn0jn")))

