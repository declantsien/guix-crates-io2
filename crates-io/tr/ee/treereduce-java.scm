(define-module (crates-io tr ee treereduce-java) #:use-module (crates-io))

(define-public crate-treereduce-java-0.1.0 (c (n "treereduce-java") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-java") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.1.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "03qlnn6kfnzz35gw6iq84jlhq23vmzslrqnl59a7qyfh6lzahfwc")))

(define-public crate-treereduce-java-0.2.0 (c (n "treereduce-java") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-java") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "1r8ggxn7s9fj3z5wq51dlnmh26b352fsrgrgy21cy4xysyzwbzns")))

(define-public crate-treereduce-java-0.2.1 (c (n "treereduce-java") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-java") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.1") (f (quote ("cli"))) (d #t) (k 0)))) (h "1ddrlwssyy2c1wpzcjp56zpcn2qn8j2bn5wvpmiacqp86asxyvlz")))

(define-public crate-treereduce-java-0.2.2 (c (n "treereduce-java") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-java") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.2") (f (quote ("cli"))) (d #t) (k 0)))) (h "03w0k1fvz9kjfrrn9idnjh05b1yzwhnvzvcmhbwswxhmk2gx083a")))

(define-public crate-treereduce-java-0.3.0 (c (n "treereduce-java") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-java") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.3.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "1fqycfq9dfwlv01w7bhnrjg2pn1qpaszfa0clksks1ysf1fbs4lz")))

