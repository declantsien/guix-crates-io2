(define-module (crates-io tr ee tree-builder-macro) #:use-module (crates-io))

(define-public crate-tree-builder-macro-0.0.1 (c (n "tree-builder-macro") (v "0.0.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (d #t) (k 0)))) (h "1ja3p04l29dyav6cnzcsfhwdh1336hx5jj1cw86wg5zfhyzzy19c")))

