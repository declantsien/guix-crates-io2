(define-module (crates-io tr ee treeedbgen-souffle-rust) #:use-module (crates-io))

(define-public crate-treeedbgen-souffle-rust-0.1.0-rc.5 (c (n "treeedbgen-souffle-rust") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "treeedbgen-souffle") (r "^0.1.0-rc.5") (f (quote ("cli"))) (d #t) (k 0)))) (h "1cdfim15kib6wb5bl6mcahpf1qh5b9il145h5kv5zvhm3wg4s5bi")))

