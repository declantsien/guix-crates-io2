(define-module (crates-io tr ee treebitmap) #:use-module (crates-io))

(define-public crate-treebitmap-0.1.0 (c (n "treebitmap") (v "0.1.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 2)) (d (n "rand") (r "0.3.*") (d #t) (k 2)))) (h "1aqc8qnqxiyvkj1rdggi41ip3zhdpfx0ickbmxspz3bidm8spqrc") (f (quote (("full-bgp-tests")))) (y #t)))

(define-public crate-treebitmap-0.1.1 (c (n "treebitmap") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 2)) (d (n "rand") (r "0.3.*") (d #t) (k 2)))) (h "18mv8pgd182h8klpncn5gr9pqd5ydbddxpzzj4a73cd94p2bc3r9") (f (quote (("full-bgp-tests") ("dev" "clippy")))) (y #t)))

(define-public crate-treebitmap-0.2.0 (c (n "treebitmap") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.41") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 2)) (d (n "rand") (r "0.3.*") (d #t) (k 2)))) (h "0mkkc7m7xp5gs3n0rd3j857lg38bhacx01xa2fqmxp1zaynxdpjz") (f (quote (("full-bgp-tests") ("dev" "clippy")))) (y #t)))

(define-public crate-treebitmap-0.2.1 (c (n "treebitmap") (v "0.2.1") (h "0cnv5plk4fp8lzffy9rgmhpdd1c13w8rsphv0prbpm0csf9pzm5v") (y #t)))

(define-public crate-treebitmap-0.2.2 (c (n "treebitmap") (v "0.2.2") (h "1ibriiwsn3y8wjwhsdi4iwkh0w4v6g9zcj9jy7psamf1c2kw1z0y") (y #t)))

(define-public crate-treebitmap-0.2.3 (c (n "treebitmap") (v "0.2.3") (h "0jg69ygrwkc25cf55505n7bcs7msmr1i47l5bhpn7zkqrp9faaq7") (y #t)))

(define-public crate-treebitmap-0.3.0 (c (n "treebitmap") (v "0.3.0") (h "0iw7065km45p4cn46ilgfm7brqrmw337r55ys8vcq7dm1d1c42js") (y #t)))

(define-public crate-treebitmap-0.3.1 (c (n "treebitmap") (v "0.3.1") (h "0szm8qam0y99fxy18dg766hk9681vgaa94cdncjqlvi6sppy1a4g") (y #t)))

(define-public crate-treebitmap-0.4.0 (c (n "treebitmap") (v "0.4.0") (h "1dcjsrmbd27xn47qpkzrhrjifjbi7a49p1w86w4g9k69ka9j7x3b") (f (quote (("alloc")))) (y #t)))

