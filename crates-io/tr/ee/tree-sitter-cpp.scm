(define-module (crates-io tr ee tree-sitter-cpp) #:use-module (crates-io))

(define-public crate-tree-sitter-cpp-0.19.0 (c (n "tree-sitter-cpp") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "059ylrxc16l3gqrhwr25s672p6wwim2l1g0gs2g3cnfvnz3r1gf7")))

(define-public crate-tree-sitter-cpp-0.20.0 (c (n "tree-sitter-cpp") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "17bp2znqlv9g1p3cs56s1bk10c6phkfah5mbx6s5skpgbhy9x1la")))

(define-public crate-tree-sitter-cpp-0.20.1 (c (n "tree-sitter-cpp") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1h2ffcmm04rjq79j31prsakijx2h60as5qprncjvgykb0vsdpghd")))

(define-public crate-tree-sitter-cpp-0.20.2 (c (n "tree-sitter-cpp") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1saryhka6ipkxv7vrywzasg02lwwsxdiyljfqqxfccq3bn9gv20w")))

(define-public crate-tree-sitter-cpp-0.20.3 (c (n "tree-sitter-cpp") (v "0.20.3") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1qbrky5kbnr5k2q2xh5z96p14ry269am9w4w9da70wvayhjvdd13")))

(define-public crate-tree-sitter-cpp-0.20.4 (c (n "tree-sitter-cpp") (v "0.20.4") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "037r4z1kn001h7qhvm83l23s3k0hkh2pvhbvmz8fdzcqrrkxhs95")))

(define-public crate-tree-sitter-cpp-0.20.5 (c (n "tree-sitter-cpp") (v "0.20.5") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0x5vzrykwmr6vc3115x15s6wqjh9q5n6m5lmk3xrl1biv9d4mc26")))

(define-public crate-tree-sitter-cpp-0.21.0 (c (n "tree-sitter-cpp") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "15w8b8qqvbas7d3j75bj1fvijs9hrp9in6mk4brshm7vrswx8n70")))

(define-public crate-tree-sitter-cpp-0.22.0 (c (n "tree-sitter-cpp") (v "0.22.0") (d (list (d (n "cc") (r "^1.0.94") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "0svkx6cmkqp5wa974ij55bc51aibnh65p8b6m4fyhcqqwpklbk98")))

(define-public crate-tree-sitter-cpp-0.22.1 (c (n "tree-sitter-cpp") (v "0.22.1") (d (list (d (n "cc") (r "^1.0.94") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "16b1b75vpkx84bghdywi90ldmajprmvz1660c5b3cvr359d4k38j")))

