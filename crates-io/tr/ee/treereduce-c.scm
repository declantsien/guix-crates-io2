(define-module (crates-io tr ee treereduce-c) #:use-module (crates-io))

(define-public crate-treereduce-c-0.1.0 (c (n "treereduce-c") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.1.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "16lcka3399wasdbimj0v51cs4cs9s48alfjc81fxwl3wd5ksdx9m")))

(define-public crate-treereduce-c-0.2.0 (c (n "treereduce-c") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "04y6hpzvsxx1n88cdzi74psjja980d0zr8g953g1lrx4p403ch86")))

(define-public crate-treereduce-c-0.2.1 (c (n "treereduce-c") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.1") (f (quote ("cli"))) (d #t) (k 0)))) (h "09dkbjy0hjhfnn1jdqp1dr8kmsd6c24wlh9iqiib0dvahdgiw91f")))

(define-public crate-treereduce-c-0.2.2 (c (n "treereduce-c") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.2") (f (quote ("cli"))) (d #t) (k 0)))) (h "00ia37lbahx7dwhfg941ln884f9ywl2fl3ljd6kwji76zq7lqafy")))

(define-public crate-treereduce-c-0.3.0 (c (n "treereduce-c") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.3.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "0kww9x63knfy4255da4r3qrybqiavzybjb8mp9xxjz1givzsimha")))

