(define-module (crates-io tr ee treeedb-javascript) #:use-module (crates-io))

(define-public crate-treeedb-javascript-0.1.0-rc.5 (c (n "treeedb-javascript") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)) (d (n "treeedb") (r "^0.1.0-rc.5") (f (quote ("cli"))) (d #t) (k 0)))) (h "1j5cicm36nz9mvcp1pkfy30dsz6qxrcfd6qi5kha6llv2z6clm76")))

