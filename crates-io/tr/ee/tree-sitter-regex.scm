(define-module (crates-io tr ee tree-sitter-regex) #:use-module (crates-io))

(define-public crate-tree-sitter-regex-0.20.0 (c (n "tree-sitter-regex") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0p4czzkc8y92125qzl3ks7j83zbhn6b6f8bnaj4mf1wrkhzabdgg")))

(define-public crate-tree-sitter-regex-0.21.0 (c (n "tree-sitter-regex") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1mf0c3ymsxiigrnq7dncshc8cv57gaizyfa892bjf6v5x5pjiwaz")))

