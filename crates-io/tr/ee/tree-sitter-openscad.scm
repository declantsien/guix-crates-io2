(define-module (crates-io tr ee tree-sitter-openscad) #:use-module (crates-io))

(define-public crate-tree-sitter-openscad-0.1.0 (c (n "tree-sitter-openscad") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "0hj9ig88ydv9ill0vac1zg9f0h0xm5b170477wbgmsa7zcfbqw20")))

(define-public crate-tree-sitter-openscad-0.2.0 (c (n "tree-sitter-openscad") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0ic9nbw10l7kqs83yds77ca7y1b0zllz7cv4bpc08cz98l8hmiv1")))

(define-public crate-tree-sitter-openscad-0.2.1 (c (n "tree-sitter-openscad") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0hc22a2x2gxx8k0nxv3q7m0y1n3c85v54qbvk9xml8iinl9kf1n5")))

(define-public crate-tree-sitter-openscad-0.3.0 (c (n "tree-sitter-openscad") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0csjsdf2adcm1607awi6w5f1xpgbnnab5lsmf649bilx0xkhzkx0")))

(define-public crate-tree-sitter-openscad-0.4.0 (c (n "tree-sitter-openscad") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0z12miijr3ivsv8b77ncabx49wahxl3sihvxlbv6rsl065wajwdx")))

(define-public crate-tree-sitter-openscad-0.4.1 (c (n "tree-sitter-openscad") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0xjkgpglnkf45qm749xnjl52naw56hpwmfjh9j270ywb79awh6cc")))

(define-public crate-tree-sitter-openscad-0.4.2 (c (n "tree-sitter-openscad") (v "0.4.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1b7244d75msn10aifh0jgywki5vk7xgcq0j2lgsy5c4cjfnz47q6")))

(define-public crate-tree-sitter-openscad-0.5.0 (c (n "tree-sitter-openscad") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1zl3hwirqh35h7n5qk86pbdhkhqn664l781mz6ymzpnr1w5mcl9j")))

(define-public crate-tree-sitter-openscad-0.5.1 (c (n "tree-sitter-openscad") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0slchhfapwcivq3cmjw4hmgk2i6myy6pcnhkj8ravv6n0bckfv7i")))

