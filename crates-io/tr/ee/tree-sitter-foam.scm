(define-module (crates-io tr ee tree-sitter-foam) #:use-module (crates-io))

(define-public crate-tree-sitter-foam-0.1.0 (c (n "tree-sitter-foam") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0gc0rm6agxcnj45116fnwnl3dhnhrq98sakp356vlkg0hiy5jm3l")))

(define-public crate-tree-sitter-foam-0.1.1 (c (n "tree-sitter-foam") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1vzdayxpgddpzhskz0fw2q87qsmqwn7by6m29daqxd96qlv4dkik")))

(define-public crate-tree-sitter-foam-0.1.2 (c (n "tree-sitter-foam") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "06qk5c26m9gnxk5bw1p5y8za0gl584cysgnwqzn9y1fsmv39m5ns")))

(define-public crate-tree-sitter-foam-0.1.3 (c (n "tree-sitter-foam") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "08jmgafnjkgfzwp6lwjvr8kgvkzyabr380n8jb12rl0f1p5wjz34")))

(define-public crate-tree-sitter-foam-0.3.0 (c (n "tree-sitter-foam") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "19x6fnaxy1cyq5d68x1xcfqws5g5wba6slxr0q112dgsqiamdb1w")))

(define-public crate-tree-sitter-foam-0.4.0 (c (n "tree-sitter-foam") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1wf04zki72vigijnq4f6bwmmc1fz8647l58wcjcq4j1p404hbaql")))

