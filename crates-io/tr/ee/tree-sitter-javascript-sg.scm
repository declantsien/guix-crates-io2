(define-module (crates-io tr ee tree-sitter-javascript-sg) #:use-module (crates-io))

(define-public crate-tree-sitter-javascript-sg-0.21.2 (c (n "tree-sitter-javascript-sg") (v "0.21.2") (d (list (d (n "cc") (r "~1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1wl80jba3mxsj915lmxgxsasx05sdail2qqzqqygl4kf7js0vk3b")))

