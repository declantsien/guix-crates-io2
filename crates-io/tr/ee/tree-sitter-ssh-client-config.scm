(define-module (crates-io tr ee tree-sitter-ssh-client-config) #:use-module (crates-io))

(define-public crate-tree-sitter-ssh-client-config-1.0.1 (c (n "tree-sitter-ssh-client-config") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0sslngk5mp5h8klyya82p1zc15cdra6g3myqh5pdnmdgwab772ch")))

(define-public crate-tree-sitter-ssh-client-config-1.0.3 (c (n "tree-sitter-ssh-client-config") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0nmqjnfiha3r884f0hdjd0m9avl1d2jj95lnfn3kkaq8arih3y9v")))

(define-public crate-tree-sitter-ssh-client-config-1.0.4 (c (n "tree-sitter-ssh-client-config") (v "1.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1dx9y07mwmzkbq7j3v7gv88d5m627j8ypb7cyhmq6p8bsbq0iwbd")))

(define-public crate-tree-sitter-ssh-client-config-2023.2.2 (c (n "tree-sitter-ssh-client-config") (v "2023.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0dpxpz5dy15saf6za11535yszwkjg6ld8lzd5v3g2y195ad622lx")))

(define-public crate-tree-sitter-ssh-client-config-2023.2.16 (c (n "tree-sitter-ssh-client-config") (v "2023.2.16") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1rjv7fs8zpyczv2mfds4m0byxnf4lnkyx21853v1jyqfnbf8i2xw")))

(define-public crate-tree-sitter-ssh-client-config-2023.2.23 (c (n "tree-sitter-ssh-client-config") (v "2023.2.23") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0hclj72f3042f60p1sjwb9mcwf7d1jaww5zn92g21a0xj30z5fbh")))

(define-public crate-tree-sitter-ssh-client-config-2023.3.21 (c (n "tree-sitter-ssh-client-config") (v "2023.3.21") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "14b14fcrxfikgh3jv6f0b6ajkbfnvjyqyfhhbnnsf7nbqiydr70z")))

(define-public crate-tree-sitter-ssh-client-config-2023.3.23 (c (n "tree-sitter-ssh-client-config") (v "2023.3.23") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0yrk3cabgkjf3hajg44laby103nb1fp960p8lljyn4hx4sfhhvdx")))

(define-public crate-tree-sitter-ssh-client-config-2023.4.13 (c (n "tree-sitter-ssh-client-config") (v "2023.4.13") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0db2890jgasmwj0yd8nys06sm9d7p028pvdfga8sa4saj9gjqgyy")))

(define-public crate-tree-sitter-ssh-client-config-2023.5.4 (c (n "tree-sitter-ssh-client-config") (v "2023.5.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0hcqjqs0ynpdaswxjcq1rpyfb6aqbviy3h26yxh2cncksqr9al2x")))

(define-public crate-tree-sitter-ssh-client-config-2023.5.18 (c (n "tree-sitter-ssh-client-config") (v "2023.5.18") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "18i3qa5dyyz0zrz321hgp3c8kzbxpi18xfv4p6c5gdzq5wgi7686")))

(define-public crate-tree-sitter-ssh-client-config-2023.6.22 (c (n "tree-sitter-ssh-client-config") (v "2023.6.22") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0x633n1iq5dnkvcxngmn733g47xmr041gh4fagj1vqkmf497jhhy")))

(define-public crate-tree-sitter-ssh-client-config-2023.6.29 (c (n "tree-sitter-ssh-client-config") (v "2023.6.29") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1vwz5ifbf9jklfigfs8hz9pznfpbwkyw6hzi5k3zw1dfzpr9qy8m")))

(define-public crate-tree-sitter-ssh-client-config-2023.7.6 (c (n "tree-sitter-ssh-client-config") (v "2023.7.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1b5qp7faq43azq7234fbcbdy16jdwhpgdsyyz43irccwmnslhxbn")))

(define-public crate-tree-sitter-ssh-client-config-2023.8.10 (c (n "tree-sitter-ssh-client-config") (v "2023.8.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "05blj3rrph8gyg9b9p4sb3ww0b5rzws24vgfkfz4bb7w66bg1c5m")))

(define-public crate-tree-sitter-ssh-client-config-2023.8.17 (c (n "tree-sitter-ssh-client-config") (v "2023.8.17") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1snsbmwz56702pnb3n0aa1bqyi8dzcryf5j9jwv9rkfq0dw24w02")))

(define-public crate-tree-sitter-ssh-client-config-2023.8.24 (c (n "tree-sitter-ssh-client-config") (v "2023.8.24") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1d6mc1qhx4llabx9vk1m1a0y9izb55inxq112cwgpgnlpzksm2nn")))

(define-public crate-tree-sitter-ssh-client-config-2023.8.31 (c (n "tree-sitter-ssh-client-config") (v "2023.8.31") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1bvg8zi7nic1nrysxfb7n522qnhjib41w8ih8wcdgdwriijviy94")))

(define-public crate-tree-sitter-ssh-client-config-2023.9.14 (c (n "tree-sitter-ssh-client-config") (v "2023.9.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0h6jkkys0z7hcwpziwp44kpf6ki42jnd76kyga68f34ma0zn8cv6")))

(define-public crate-tree-sitter-ssh-client-config-2023.9.21 (c (n "tree-sitter-ssh-client-config") (v "2023.9.21") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "09dm12q0dkyj8jx0mhhl1an87wjfd0m4zyizp8114yyi15ifha0f")))

(define-public crate-tree-sitter-ssh-client-config-2023.10.26 (c (n "tree-sitter-ssh-client-config") (v "2023.10.26") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "071z0almb76hdiwgc8bjbd4mw2sv4qcrasi2ql7bb26xybicxa7v")))

(define-public crate-tree-sitter-ssh-client-config-2023.11.16 (c (n "tree-sitter-ssh-client-config") (v "2023.11.16") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0zpi6gpwg4qm3f718iwm615dkw3xfa3w7k8p61xppyfm6d3s5425")))

(define-public crate-tree-sitter-ssh-client-config-2023.12.14 (c (n "tree-sitter-ssh-client-config") (v "2023.12.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "08kba2s93wbimfhsig3jhmd05lpnb67sl08lw25kf0yymrrpdc0y")))

(define-public crate-tree-sitter-ssh-client-config-2023.12.21 (c (n "tree-sitter-ssh-client-config") (v "2023.12.21") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0ra4p5ssvf93ahabskkba9f8vq5isfz3i4zw02iaqfbyvj7hbvab")))

(define-public crate-tree-sitter-ssh-client-config-2024.1.18 (c (n "tree-sitter-ssh-client-config") (v "2024.1.18") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0q5qxaayx6h2699jkyyab9k98jzvl8l6kj8n8m0cby5v6ympc43m")))

(define-public crate-tree-sitter-ssh-client-config-2024.2.8 (c (n "tree-sitter-ssh-client-config") (v "2024.2.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0jb5fck6dfppwavn3bf0r1nhjssqsclv6adkfw2xpc0zadk7y89c")))

(define-public crate-tree-sitter-ssh-client-config-2024.2.22 (c (n "tree-sitter-ssh-client-config") (v "2024.2.22") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "16bidzlpmmsq80s7zg58ad8xsj3s7mq9cn5bvhd7r9s1xxrd8lwz")))

(define-public crate-tree-sitter-ssh-client-config-2024.2.29 (c (n "tree-sitter-ssh-client-config") (v "2024.2.29") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.21") (d #t) (k 0)))) (h "1cr06vcidbz442f08nhn3jsvw5q38cxxf2jxsc3vmgc3c7rq2wgv")))

(define-public crate-tree-sitter-ssh-client-config-2024.3.7 (c (n "tree-sitter-ssh-client-config") (v "2024.3.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.21") (d #t) (k 0)))) (h "1n2pkv5096rcr02ygw1jzpqhlfa6lqd4zmynwwl92xzq1v7dvanp")))

(define-public crate-tree-sitter-ssh-client-config-2024.3.14 (c (n "tree-sitter-ssh-client-config") (v "2024.3.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22") (d #t) (k 0)))) (h "08i3xc53xwijjf9hfpr527j8nzzshlci1yiv9z16blk99mfs9m7y")))

(define-public crate-tree-sitter-ssh-client-config-2024.3.21 (c (n "tree-sitter-ssh-client-config") (v "2024.3.21") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22") (d #t) (k 0)))) (h "1j6gyfgx53n2pacazm2q3z514c33crskhb8rjnw3xnlb8yx2pn57")))

(define-public crate-tree-sitter-ssh-client-config-2024.3.28 (c (n "tree-sitter-ssh-client-config") (v "2024.3.28") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22") (d #t) (k 0)))) (h "08njn1q58xjpqf0z6qffl78rv106qgi888y4fs7gcibwjgcng54s")))

(define-public crate-tree-sitter-ssh-client-config-2024.4.4 (c (n "tree-sitter-ssh-client-config") (v "2024.4.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22") (d #t) (k 0)))) (h "1qa6pb4k65zclf5ac4laigfn1dny8mp4bicmf117nkzg7jgk4kiy")))

(define-public crate-tree-sitter-ssh-client-config-2024.4.11 (c (n "tree-sitter-ssh-client-config") (v "2024.4.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22") (d #t) (k 0)))) (h "1vvg8f8634q00x5vhv2nykw9lmbj7mfgr8djzw0wghl9xxi893kp")))

(define-public crate-tree-sitter-ssh-client-config-2024.4.18 (c (n "tree-sitter-ssh-client-config") (v "2024.4.18") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22") (d #t) (k 0)))) (h "0xpnk4m7khin4cz7mdxmpdxqf0sdzygk93m5gxar7vfjmc0cp6g2")))

(define-public crate-tree-sitter-ssh-client-config-2024.4.25 (c (n "tree-sitter-ssh-client-config") (v "2024.4.25") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22") (d #t) (k 0)))) (h "0q3cpyk4s298cz177pjss1m1650f9qbhw1b279kibdlpsgkv700n")))

(define-public crate-tree-sitter-ssh-client-config-2024.5.2 (c (n "tree-sitter-ssh-client-config") (v "2024.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22") (d #t) (k 0)))) (h "1kkcy7qk3crrhwpac57wvahyy8lpsmyvlzcn12z20pwzwca3rh7b")))

(define-public crate-tree-sitter-ssh-client-config-2024.5.9 (c (n "tree-sitter-ssh-client-config") (v "2024.5.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22") (d #t) (k 0)))) (h "03f6jj9xvncpmyhjr6b9vf27775k0nfpghbkhzvvaagv51bvy245")))

(define-public crate-tree-sitter-ssh-client-config-2024.5.23 (c (n "tree-sitter-ssh-client-config") (v "2024.5.23") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22") (d #t) (k 0)))) (h "1jr9dcpcxhyq1p9x8f2hpmjwqaz82fd9s3fn8qyp4894q08d9fiy")))

