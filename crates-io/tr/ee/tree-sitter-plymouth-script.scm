(define-module (crates-io tr ee tree-sitter-plymouth-script) #:use-module (crates-io))

(define-public crate-tree-sitter-plymouth-script-0.1.0 (c (n "tree-sitter-plymouth-script") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "09668l5j4gabafib4s11nl70g42i0n6aalznm4pwj5b052hcakb9")))

