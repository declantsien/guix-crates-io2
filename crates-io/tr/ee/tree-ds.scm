(define-module (crates-io tr ee tree-ds) #:use-module (crates-io))

(define-public crate-tree-ds-0.1.0 (c (n "tree-ds") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1bmh18lyn6yfa3285y9pswgp7b32695xmmrpc2gz90s1i7qahdps") (f (quote (("serde") ("default") ("async")))) (r "1.71.0")))

(define-public crate-tree-ds-0.1.1 (c (n "tree-ds") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0xdmf7bagx1mqhnb0l5i7i787nbhaqmnlsflwi7vmq1g2qabd6vd") (f (quote (("default") ("async")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.71.0")))

