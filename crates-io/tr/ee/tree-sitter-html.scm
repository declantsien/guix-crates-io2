(define-module (crates-io tr ee tree-sitter-html) #:use-module (crates-io))

(define-public crate-tree-sitter-html-0.19.0 (c (n "tree-sitter-html") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0hrl8jvw239a71xnwq4jrrlkbj2qcpimzgw7vh1l6d9sjmvnnkhq")))

(define-public crate-tree-sitter-html-0.20.0 (c (n "tree-sitter-html") (v "0.20.0") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "07gsnzb0ydbi73cjsa36x1a35qhwyqsbiavzsr5kr122pnv24y01")))

(define-public crate-tree-sitter-html-0.20.3 (c (n "tree-sitter-html") (v "0.20.3") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "0lwb12dhn3l6szghskinfi8ms49vzzr0xswzqxfbz1m710mlkcwm")))

