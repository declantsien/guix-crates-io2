(define-module (crates-io tr ee tree-experiments) #:use-module (crates-io))

(define-public crate-tree-experiments-0.1.0 (c (n "tree-experiments") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "swap3") (r "^0.2.0") (d #t) (k 0)))) (h "1d1ksi4ryb00j5ckh11sgmkmk8c2wzm13b9glpj0g34w54rll1yv")))

