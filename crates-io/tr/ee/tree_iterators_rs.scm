(define-module (crates-io tr ee tree_iterators_rs) #:use-module (crates-io))

(define-public crate-tree_iterators_rs-1.0.0 (c (n "tree_iterators_rs") (v "1.0.0") (h "1r0m8b6j2kh9r99yxl99xfs1ky9asp0sdfvq397avji7q1mqqyj6")))

(define-public crate-tree_iterators_rs-1.0.1 (c (n "tree_iterators_rs") (v "1.0.1") (d (list (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)))) (h "1742sss4l4rbarf0g46b273hs6yqwd5pr064zp6abk73w9jigkar")))

(define-public crate-tree_iterators_rs-1.0.2 (c (n "tree_iterators_rs") (v "1.0.2") (d (list (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)))) (h "1a1q3f2avjjy2lppm0vs9l636cgpr2s6q8y6l608n667c4b0rhh8")))

(define-public crate-tree_iterators_rs-1.1.0 (c (n "tree_iterators_rs") (v "1.1.0") (d (list (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)))) (h "1s9dm7lrjc3a5wcv4i9dsyn69wppsrj49svkydvh5mndskqcrz28")))

(define-public crate-tree_iterators_rs-1.1.1 (c (n "tree_iterators_rs") (v "1.1.1") (d (list (d (n "serde") (r "^1.0.189") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.189") (o #t) (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)))) (h "0m65npp4gl4mzlymnympz40mnim0qbya9m0rcl3ynb3yc6ka0rnm") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-tree_iterators_rs-1.1.2 (c (n "tree_iterators_rs") (v "1.1.2") (d (list (d (n "serde") (r "^1.0.189") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.189") (o #t) (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)))) (h "1iygmv961fz69paj5369n03n6y337dvyc71iz45dzy9rbv00p264") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-tree_iterators_rs-1.1.3 (c (n "tree_iterators_rs") (v "1.1.3") (d (list (d (n "serde") (r "^1.0.189") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.189") (o #t) (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.9") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1qzbgh4dc61yk9kqdkj77qkyfixrj4xq85rskg3sfkl721zc9hvv") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-tree_iterators_rs-1.2.0 (c (n "tree_iterators_rs") (v "1.2.0") (d (list (d (n "serde") (r "^1.0.189") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.189") (o #t) (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.9") (f (quote ("alloc"))) (d #t) (k 0)))) (h "00b1mdczi35y7pwv736w6sfpxfbii4qxybxqwb07f9wpsa753pd1") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-tree_iterators_rs-1.1.4 (c (n "tree_iterators_rs") (v "1.1.4") (d (list (d (n "serde") (r "^1.0.189") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.189") (o #t) (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.9") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1jkfxpn6flfl8w3bx0q7344wbwv997yxn9i6f9bg1vlkanzdzs36") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-tree_iterators_rs-1.2.1 (c (n "tree_iterators_rs") (v "1.2.1") (d (list (d (n "serde") (r "^1.0.193") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (o #t) (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.9") (f (quote ("alloc"))) (d #t) (k 0)))) (h "032zxf2f0z83vg2pkaidfhjk79078pnd9v3j0vi5i367g39j5g9m") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

