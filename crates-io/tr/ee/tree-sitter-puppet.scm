(define-module (crates-io tr ee tree-sitter-puppet) #:use-module (crates-io))

(define-public crate-tree-sitter-puppet-0.0.1 (c (n "tree-sitter-puppet") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1x1qm9fgrva339wwcpx1042jn332x98amqa3y26n4n9xp42yikd3")))

(define-public crate-tree-sitter-puppet-1.0.0 (c (n "tree-sitter-puppet") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1kw3vjnsinf8frwyhvc1ycs1ynip4c1rmfsc71ryllhpvqw8fl4j")))

(define-public crate-tree-sitter-puppet-1.1.0 (c (n "tree-sitter-puppet") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1lmd74ycngqqapjwl14iiyza9n1f3a0jpvwchh9jc6zf435vnmbn")))

(define-public crate-tree-sitter-puppet-1.2.0 (c (n "tree-sitter-puppet") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0gnf0i5r7wc3jkkyg0k05ppbpmaz3swd1pasjwp82y81jyfbvj1f")))

