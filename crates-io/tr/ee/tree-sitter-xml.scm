(define-module (crates-io tr ee tree-sitter-xml) #:use-module (crates-io))

(define-public crate-tree-sitter-xml-0.3.0 (c (n "tree-sitter-xml") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0fk27idgs4027c3pgkkpxix8lc06814xx72jmr266zhiy43m8cwi")))

(define-public crate-tree-sitter-xml-0.4.0 (c (n "tree-sitter-xml") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "18d7sq19bfykqdvhlp8xia4yvfxl1dga3zq590jbkkzrpdyn7ic8")))

(define-public crate-tree-sitter-xml-0.5.0 (c (n "tree-sitter-xml") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0x300mnkdvj69c0d83wnv5bf3nxxp23qvv84hldqwa8nzq2ihja8")))

(define-public crate-tree-sitter-xml-0.5.1 (c (n "tree-sitter-xml") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0iz6bhyl0fa6hbqwfqygrak7yypwaffifqgn4kgzdk80z571790j")))

(define-public crate-tree-sitter-xml-0.5.2 (c (n "tree-sitter-xml") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1c6cf727gbxf3p8sx53zzm5hs98mys5z0b1xdkd9f0fffgcqhbma")))

(define-public crate-tree-sitter-xml-0.5.3 (c (n "tree-sitter-xml") (v "0.5.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0igwv5ip1l06c50m7bszhhmqa5cc21g1c890al8n22c1wwg6m86m")))

(define-public crate-tree-sitter-xml-0.6.0 (c (n "tree-sitter-xml") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.88") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.21.0") (d #t) (k 0)))) (h "12mc1vc799wvm3i0ry2kcd2z57ld1f5a3sjy7cykr1vh1kwmqkcr")))

(define-public crate-tree-sitter-xml-0.6.1 (c (n "tree-sitter-xml") (v "0.6.1") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "02syrwgmbns91c291fjakpvri73kc3phk40vm606b70sb7fz5skv")))

(define-public crate-tree-sitter-xml-0.6.2 (c (n "tree-sitter-xml") (v "0.6.2") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "1ibqpdck2ivg5dmk6x8b36amrz81x3pb4h1wwjg26h37djb3pvd7")))

(define-public crate-tree-sitter-xml-0.6.3 (c (n "tree-sitter-xml") (v "0.6.3") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "1pzdqnqkgvxrf0nplz9fnnhwj7m2acszffn65z46cmasfdh9hwkp")))

