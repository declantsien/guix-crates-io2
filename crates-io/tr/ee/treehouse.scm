(define-module (crates-io tr ee treehouse) #:use-module (crates-io))

(define-public crate-treehouse-0.1.0 (c (n "treehouse") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "sled") (r "^0.32.0-rc1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0p5bhy1ylgiyla36glj5hdh31p3hnia9casvp63njmf59f0pz153") (f (quote (("default" "bincode"))))))

