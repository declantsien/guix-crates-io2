(define-module (crates-io tr ee tree-sitter-apachesynapse) #:use-module (crates-io))

(define-public crate-tree-sitter-apachesynapse-0.0.1 (c (n "tree-sitter-apachesynapse") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1kg5wc57li95nrql92n56gsx0cnrpia2p67pl638kxxwpr3alqam")))

(define-public crate-tree-sitter-apachesynapse-0.0.2 (c (n "tree-sitter-apachesynapse") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0ywh9495lwhkx2wh9wya4gy10j31vjfxpb28xx62i7v5734s78ii")))

(define-public crate-tree-sitter-apachesynapse-0.0.3 (c (n "tree-sitter-apachesynapse") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0gbjgygyxs3m2p116vdy8z6fsa6q5bp98cziqw2prixivpbdlms4")))

