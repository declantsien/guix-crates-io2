(define-module (crates-io tr ee tree-sitter-commonlisp) #:use-module (crates-io))

(define-public crate-tree-sitter-commonlisp-0.2.0 (c (n "tree-sitter-commonlisp") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "0q2s3psqqdiiyqxdin5hs4yw2fa9kzi7hx2acz26rnspyrn1qs68")))

(define-public crate-tree-sitter-commonlisp-0.3.0 (c (n "tree-sitter-commonlisp") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.1") (d #t) (k 0)))) (h "1irzy38jak0a40wyhzbwzqa09v5961yh90v5c7qzza3fgw1sa01j")))

(define-public crate-tree-sitter-commonlisp-0.3.1 (c (n "tree-sitter-commonlisp") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.1") (d #t) (k 0)))) (h "06jmj1b1jf70lfwc8qj5lz56pqajjpv8fl51gc6k93cvwvd1a17z")))

(define-public crate-tree-sitter-commonlisp-0.3.2 (c (n "tree-sitter-commonlisp") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.1") (d #t) (k 0)))) (h "0kb1kcgwz5sk7shwqxsir6wwvlgrfr8sjwyxqczbwiv4kz0clnmm")))

(define-public crate-tree-sitter-commonlisp-0.3.3 (c (n "tree-sitter-commonlisp") (v "0.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.1") (d #t) (k 0)))) (h "14k6rzvw541amarx484wflrbgf3i7gwqrhb9v9zk001gdbhrkdqf")))

