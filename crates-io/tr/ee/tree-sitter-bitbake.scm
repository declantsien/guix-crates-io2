(define-module (crates-io tr ee tree-sitter-bitbake) #:use-module (crates-io))

(define-public crate-tree-sitter-bitbake-1.0.0 (c (n "tree-sitter-bitbake") (v "1.0.0") (d (list (d (n "cc") (r "~1.0.82") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1s9m9kpwjmxvgm3djrvi4rvzfvsn9kxkm9kdvlkh96nds09jyg9a")))

(define-public crate-tree-sitter-bitbake-1.0.1 (c (n "tree-sitter-bitbake") (v "1.0.1") (d (list (d (n "cc") (r "~1.0.82") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1gah4n9k2cc3r112pdzi7b67524amihzjxiaphcv4c3s3q6k04f1")))

(define-public crate-tree-sitter-bitbake-1.0.2 (c (n "tree-sitter-bitbake") (v "1.0.2") (d (list (d (n "cc") (r "~1.0.82") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "05jc5lnscf1xgddl3zdhfdyfm024a9z7607050793x9i8v7gdhkv")))

(define-public crate-tree-sitter-bitbake-1.1.0 (c (n "tree-sitter-bitbake") (v "1.1.0") (d (list (d (n "cc") (r "~1.0.82") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1p2sn2nsqs27is91l3s3h9420wz5lw2lm8rsrmizp8fkk8zr8nmv")))

