(define-module (crates-io tr ee tree-sitter-navi) #:use-module (crates-io))

(define-public crate-tree-sitter-navi-0.0.1 (c (n "tree-sitter-navi") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1fqjli059vsp3bnwkc8x6acb6460k64hypzwzp4pslyyqb39sbnk")))

(define-public crate-tree-sitter-navi-0.0.2 (c (n "tree-sitter-navi") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "077i781k27ykcmnszavgfpq5bw3g0smjk9a1rkrr6lpn1wg9rbvs")))

(define-public crate-tree-sitter-navi-0.0.3 (c (n "tree-sitter-navi") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0izwja8chi4xbyblxq6p574pjnmmkqlb64mkd12q9kkzqs2mg600")))

(define-public crate-tree-sitter-navi-0.0.4 (c (n "tree-sitter-navi") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "12dfja8rw3zkc2sbrkvj2xr8998if0xr1hhv6i6rm5qcvxrw3xsg")))

(define-public crate-tree-sitter-navi-0.0.5 (c (n "tree-sitter-navi") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0laxyk21h3bv0gifx8c496pw25qjqxnagz967fx4v28wz6kly929")))

(define-public crate-tree-sitter-navi-0.0.8 (c (n "tree-sitter-navi") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0745blmlabz66p4pax2laid7y3999ddbqd2zi5qcwvsqyxk526cd")))

(define-public crate-tree-sitter-navi-0.0.9 (c (n "tree-sitter-navi") (v "0.0.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0jr8vpdrfz98wkvvj9f8i0w9ykyjismrr1y2067vqdl3737ay4y7")))

(define-public crate-tree-sitter-navi-0.0.12 (c (n "tree-sitter-navi") (v "0.0.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "162z58w76lap0zs9f11b5hqrb5ca765i6hfbd05ad40mvqny4hnq")))

(define-public crate-tree-sitter-navi-0.0.13 (c (n "tree-sitter-navi") (v "0.0.13") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1aph02gs6qdfkcgkp6rrhiyxxmpzh6xiv86105qs9hb9zm71iywy")))

(define-public crate-tree-sitter-navi-0.0.14 (c (n "tree-sitter-navi") (v "0.0.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0mwjvwbip9qh2l4pj6bndi08pcai4ba1prx0s80f05l30s2mqa8r")))

(define-public crate-tree-sitter-navi-0.1.0 (c (n "tree-sitter-navi") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "07wc9hvm6vf8sc92mbnmfxqvpxjr2py1d6m50anxpa0rpi0w7xdi")))

