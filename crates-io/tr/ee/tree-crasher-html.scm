(define-module (crates-io tr ee tree-crasher-html) #:use-module (crates-io))

(define-public crate-tree-crasher-html-0.2.0 (c (n "tree-crasher-html") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.0") (d #t) (k 0)) (d (n "tree-sitter-html") (r "^0.19") (d #t) (k 0)))) (h "0yzrgh08233wn1s1v6k49j3mf32bxlxzr3m1avzp143b3izxx3b3")))

(define-public crate-tree-crasher-html-0.2.1 (c (n "tree-crasher-html") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.1") (d #t) (k 0)) (d (n "tree-sitter-html") (r "^0.19") (d #t) (k 0)))) (h "0z4r5zi66a6glinln08knc2qs8pgngvd2s01kqk6vr1mi5mx036d")))

(define-public crate-tree-crasher-html-0.2.2 (c (n "tree-crasher-html") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.2") (d #t) (k 0)) (d (n "tree-sitter-html") (r "^0.19") (d #t) (k 0)))) (h "1q90lhzq28c3nnbqwpqvz5hrvgq2l77y8a01n5yzwv1xkpwgmamv")))

(define-public crate-tree-crasher-html-0.2.3 (c (n "tree-crasher-html") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.3") (d #t) (k 0)) (d (n "tree-sitter-html") (r "^0.19") (d #t) (k 0)))) (h "0a081azp96ksbmpiiqq9gfs4xhwb5pc4j0qf1xnq13x3fn1jlxgs")))

(define-public crate-tree-crasher-html-0.3.0 (c (n "tree-crasher-html") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.3.0") (d #t) (k 0)) (d (n "tree-sitter-html") (r "^0.19") (d #t) (k 0)))) (h "1gj7hr5w97mdplw9mh94gpkyavyhdb4yxd773dcw01gaym4p0bm3")))

(define-public crate-tree-crasher-html-0.4.0 (c (n "tree-crasher-html") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.4.0") (d #t) (k 0)) (d (n "tree-sitter-html") (r "^0.19") (d #t) (k 0)))) (h "00971mcp9hl05xpbp3g36pc9s3hmlz7hncbabkyrjxlyg11pcqyi")))

