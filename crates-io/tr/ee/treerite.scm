(define-module (crates-io tr ee treerite) #:use-module (crates-io))

(define-public crate-treerite-0.1.0 (c (n "treerite") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1q96xf0bxvmif33hsrisvg7j5zkq5c6zdm7n1zszkxb9s2sbhcn2") (f (quote (("static") ("free_panic") ("dynamic") ("default" "static" "free_panic"))))))

