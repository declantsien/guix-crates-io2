(define-module (crates-io tr ee tree-sitter-gitcommit) #:use-module (crates-io))

(define-public crate-tree-sitter-gitcommit-0.0.1 (c (n "tree-sitter-gitcommit") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1lm46vb6is7b4yii45mapli63m0i5bzms5gqm4xwxhzghbcwyh0m") (y #t)))

(define-public crate-tree-sitter-gitcommit-0.3.0 (c (n "tree-sitter-gitcommit") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19") (d #t) (k 0)))) (h "1rcb4nh85d7zg9bb2xzyb601ixq0zq5x03963mhrvd448224s3an")))

(define-public crate-tree-sitter-gitcommit-0.3.1 (c (n "tree-sitter-gitcommit") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19") (d #t) (k 0)))) (h "1v5kj4apk3217j2ahxcsxp1dfjwsfq82h3fvhqflcsc7vmnk1y20")))

(define-public crate-tree-sitter-gitcommit-0.3.2 (c (n "tree-sitter-gitcommit") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19") (d #t) (k 0)))) (h "07yb7nm4l11xm3lib4w7q6i71wiy2x9ddznkhgab5zx32xvvhz2c")))

(define-public crate-tree-sitter-gitcommit-0.3.3 (c (n "tree-sitter-gitcommit") (v "0.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19") (d #t) (k 0)))) (h "0g2s59lgqnl758x287prl65fkgdlc0r2jlr5lmhqcaq3blcdy24y")))

