(define-module (crates-io tr ee tree-sitter-pmf) #:use-module (crates-io))

(define-public crate-tree-sitter-pmf-0.0.1 (c (n "tree-sitter-pmf") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "083cpjq7yqsmqfq479pmra5sw1l7zyf36pw6ijya6m4blkg1fbq8")))

(define-public crate-tree-sitter-pmf-0.0.2 (c (n "tree-sitter-pmf") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0kbjnv18mv8812sc74w8z2iax9gablvv0hz88w8hksj95qy45qfs")))

(define-public crate-tree-sitter-pmf-0.0.3 (c (n "tree-sitter-pmf") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0d7bn06jj3bwn4y5rfs5wn1ddy8gaicjixg1xgazpvkindb7y5wr")))

