(define-module (crates-io tr ee tree-splicer-typescript) #:use-module (crates-io))

(define-public crate-tree-splicer-typescript-0.2.0 (c (n "tree-splicer-typescript") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)) (d (n "tree-splicer") (r "^0.2.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "093884ghgkjbsdlnsknaf5yzkgklgpvwfr67hc7d2s4vzfbfch06")))

(define-public crate-tree-splicer-typescript-0.3.0 (c (n "tree-splicer-typescript") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)) (d (n "tree-splicer") (r "^0.3.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "1d2hrvl7yyazdg5y89hrmxxixpw1p78vc632d24p9w268d26x9ni")))

(define-public crate-tree-splicer-typescript-0.3.1 (c (n "tree-splicer-typescript") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)) (d (n "tree-splicer") (r "^0.3.1") (f (quote ("cli"))) (d #t) (k 0)))) (h "0jrb60qzyzk96v8sh8w9m98n2ha9318y45zznj9qkqj56rilwr1z")))

(define-public crate-tree-splicer-typescript-0.4.0 (c (n "tree-splicer-typescript") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)) (d (n "tree-splicer") (r "^0.4.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "0dvns0xwhgvpxsbdmy2n784l0cn4yygks6b3r4w5j3b62hwlljbf")))

(define-public crate-tree-splicer-typescript-0.5.0 (c (n "tree-splicer-typescript") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)) (d (n "tree-splicer") (r "^0.5.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "0mcdsxllgs1yl8kc5z01nchr39srgdppa3rp5lbzcqyjfbw93fiq")))

