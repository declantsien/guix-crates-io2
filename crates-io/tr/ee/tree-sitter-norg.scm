(define-module (crates-io tr ee tree-sitter-norg) #:use-module (crates-io))

(define-public crate-tree-sitter-norg-0.0.1 (c (n "tree-sitter-norg") (v "0.0.1") (d (list (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0xvlizv47w0zzk82b15l6ngnwbhm8q2i02gi8p24xccriv53qwxh")))

(define-public crate-tree-sitter-norg-0.1.0 (c (n "tree-sitter-norg") (v "0.1.0") (d (list (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0cq993szvpzbpls9lja61x92pl1rn31fyy40crsgg5yfmpbwywv4")))

(define-public crate-tree-sitter-norg-0.1.1 (c (n "tree-sitter-norg") (v "0.1.1") (d (list (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0948smdybzqv2glf72zp94h5510n5p34qw5k7b7j1090nflqz3lf")))

