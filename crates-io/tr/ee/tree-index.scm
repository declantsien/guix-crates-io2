(define-module (crates-io tr ee tree-index) #:use-module (crates-io))

(define-public crate-tree-index-0.1.0 (c (n "tree-index") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "flat-tree") (r "^3.0.0") (d #t) (k 0)) (d (n "sparse-bitfield") (r "^0.2.0") (d #t) (k 0)))) (h "0ir4wpq6xdmvr3k7ldw02jyknmc2gl3b42ppzm0d7aqg2213bwz3")))

(define-public crate-tree-index-0.2.0 (c (n "tree-index") (v "0.2.0") (d (list (d (n "flat-tree") (r "^3.3.0") (d #t) (k 0)) (d (n "sparse-bitfield") (r "^0.7.0") (d #t) (k 0)))) (h "075cm9yvdvxr4jwva9g5aghldbbdlww93hq8ydlh83sajddgnxcg")))

(define-public crate-tree-index-0.3.0 (c (n "tree-index") (v "0.3.0") (d (list (d (n "flat-tree") (r "^3.3.0") (d #t) (k 0)) (d (n "sparse-bitfield") (r "^0.7.0") (d #t) (k 0)))) (h "1ki283ff9sy1advqlmyyfy4ixkim2nyyi3gd9cgcfn0sx9jkarwp")))

(define-public crate-tree-index-0.4.0 (c (n "tree-index") (v "0.4.0") (d (list (d (n "flat-tree") (r "^3.3.0") (d #t) (k 0)) (d (n "sparse-bitfield") (r "^0.7.0") (d #t) (k 0)))) (h "06f8fxslg8c12nab8dkbwvrrmgkyg0hg4r3kz1qkq2gbm6mjvbfg")))

(define-public crate-tree-index-0.4.1 (c (n "tree-index") (v "0.4.1") (d (list (d (n "flat-tree") (r "^3.3.0") (d #t) (k 0)) (d (n "sparse-bitfield") (r "^0.7.0") (d #t) (k 0)))) (h "0a8n17pcfy5yv8zvwb9z64qvrr8ynplmvprc5v7clb2lclmnmg7i")))

(define-public crate-tree-index-0.5.0 (c (n "tree-index") (v "0.5.0") (d (list (d (n "flat-tree") (r "^3.4.0") (d #t) (k 0)) (d (n "sparse-bitfield") (r "^0.8.1") (d #t) (k 0)))) (h "083rbjmgv7gr47hin45si0m333hicz3k8k1z85xjk3kvbwag3cnl")))

(define-public crate-tree-index-0.6.0 (c (n "tree-index") (v "0.6.0") (d (list (d (n "flat-tree") (r "^5.0.0") (d #t) (k 0)) (d (n "sparse-bitfield") (r "^0.11.0") (d #t) (k 0)))) (h "02fkwhmqq9nycq42f1zsd93a07abyw129xnsk1yamg552k3c7j1g")))

(define-public crate-tree-index-0.6.1 (c (n "tree-index") (v "0.6.1") (d (list (d (n "flat-tree") (r "^5.0.0") (d #t) (k 0)) (d (n "sparse-bitfield") (r "^0.11.0") (d #t) (k 0)))) (h "18q1gpg4zywwlgdhlryvrqzr5rj899gp446qfr39sw80ajgcvk68")))

