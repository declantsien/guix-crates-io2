(define-module (crates-io tr ee tree-sitter-deb822) #:use-module (crates-io))

(define-public crate-tree-sitter-deb822-0.1.0 (c (n "tree-sitter-deb822") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "11f7jwlkw3ah9mi4anj33js9kpvq7prr11qlw16gaqw8xhbik9dz")))

(define-public crate-tree-sitter-deb822-0.2.0 (c (n "tree-sitter-deb822") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1l6npav0b8s9kmq3f6zkl5bmhd16s55zzqdqzwbq623div21xnk3") (y #t)))

(define-public crate-tree-sitter-deb822-0.2.1 (c (n "tree-sitter-deb822") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0rc840sbs7g7g8ikpwdxx9blfv1j0ih9dxxjmg1md6x430jffy7y")))

(define-public crate-tree-sitter-deb822-0.3.0 (c (n "tree-sitter-deb822") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.22.6") (d #t) (k 0)))) (h "0sqyzpnky5psskys1i5shj94hdl6nrvznaqs604f0xlfnbya4axq")))

