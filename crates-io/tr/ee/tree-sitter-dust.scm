(define-module (crates-io tr ee tree-sitter-dust) #:use-module (crates-io))

(define-public crate-tree-sitter-dust-0.0.1 (c (n "tree-sitter-dust") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0m00dapmk4rk822rmg1qm6lj0riqxwgjqz27c2wjrqda13qqbbb5")))

