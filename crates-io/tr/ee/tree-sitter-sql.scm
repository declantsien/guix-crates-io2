(define-module (crates-io tr ee tree-sitter-sql) #:use-module (crates-io))

(define-public crate-tree-sitter-sql-0.0.1 (c (n "tree-sitter-sql") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)))) (h "1wrkr17fa1aym49yw4yj08x51c5p1k8kh4490b97l47n7chfzdam") (y #t)))

(define-public crate-tree-sitter-sql-0.0.2 (c (n "tree-sitter-sql") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)))) (h "0bn8djk16myx5p29067casvkbdfjl8ygyisgl19q2lkn4lvzpr04")))

