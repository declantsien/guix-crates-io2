(define-module (crates-io tr ee tree-sitter-qmldir) #:use-module (crates-io))

(define-public crate-tree-sitter-qmldir-0.0.1 (c (n "tree-sitter-qmldir") (v "0.0.1") (d (list (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1p5fbg2qhrkdir9kqg5276xpv8s77d1h2izki469pbcpsca0qr1d")))

