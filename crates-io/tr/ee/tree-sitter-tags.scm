(define-module (crates-io tr ee tree-sitter-tags) #:use-module (crates-io))

(define-public crate-tree-sitter-tags-0.2.0 (c (n "tree-sitter-tags") (v "0.2.0") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r ">= 0.3.7") (d #t) (k 0)))) (h "0yq7hjw2g5niknqifl439nfld29v8qbvql4rmxai1dqyyil2zaik")))

(define-public crate-tree-sitter-tags-0.3.0 (c (n "tree-sitter-tags") (v "0.3.0") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.17.0") (d #t) (k 0)))) (h "0ps6wp1kl5w65vrmlz80nk40azb5lqz8bsd0izial8jl77vwn50v")))

(define-public crate-tree-sitter-tags-0.19.0 (c (n "tree-sitter-tags") (v "0.19.0") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.17.0") (d #t) (k 0)))) (h "1ql8hs16h2ay9dqgyihpd2ilx40crl8liv43pjbif7b5diqgnf83")))

(define-public crate-tree-sitter-tags-0.19.1 (c (n "tree-sitter-tags") (v "0.19.1") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.17.0") (d #t) (k 0)))) (h "19h01vczj65m1aix9qg9qhk39fv20h7yc4p1mv8lhvmk2vy9fik4")))

(define-public crate-tree-sitter-tags-0.19.2 (c (n "tree-sitter-tags") (v "0.19.2") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.17.0") (d #t) (k 0)))) (h "12j1i091jgm9hlvyiqhpdmbyw1z51rxbqs5c91bvz6idmyb8mjhx")))

(define-public crate-tree-sitter-tags-0.20.0 (c (n "tree-sitter-tags") (v "0.20.0") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "11l64y8a7q1zq5dfa3drmw05zjqvhm4mq7vvg3av2ibz6ghra67z")))

(define-public crate-tree-sitter-tags-0.20.1 (c (n "tree-sitter-tags") (v "0.20.1") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0d77f6wm00pxl03v8mhcql5nixjcch2qm9ambznwjy6qxrbb9vdq")))

(define-public crate-tree-sitter-tags-0.20.2 (c (n "tree-sitter-tags") (v "0.20.2") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "019cvbpakxpmzbabcjhwkj5zsddacp7iwx89m0vhllqrc8vz3cyc")))

(define-public crate-tree-sitter-tags-0.21.0 (c (n "tree-sitter-tags") (v "0.21.0") (d (list (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.21.0") (d #t) (k 0)))) (h "0b2zl8yx77bzkas931wzjrd7wqsac0f27xp7qf424rpwlg29cxlh") (r "1.70")))

(define-public crate-tree-sitter-tags-0.22.0 (c (n "tree-sitter-tags") (v "0.22.0") (d (list (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.0") (d #t) (k 0)))) (h "1via6v8ggxzy3pn2140zzfx2z236yxyd07b82grf6ibpcq2kbnx4") (y #t) (r "1.74.1")))

(define-public crate-tree-sitter-tags-0.22.1 (c (n "tree-sitter-tags") (v "0.22.1") (d (list (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.0") (d #t) (k 0)))) (h "15qjskq7gqnnayrz99jd5vak987xw23ds6p7inp4hkv21blbkwvl") (r "1.74.1")))

(define-public crate-tree-sitter-tags-0.22.2 (c (n "tree-sitter-tags") (v "0.22.2") (d (list (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "089mn0rknfrkbln8mh8i4gvamzz3h7v61fyg96yghann7cassrws") (r "1.74.1")))

(define-public crate-tree-sitter-tags-0.22.3 (c (n "tree-sitter-tags") (v "0.22.3") (d (list (d (n "memchr") (r "^2.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "1f4dcy6mk4dw5crlpbiknsbj7zf0scw48mza5170bz2454ns79qr") (r "1.74.1")))

(define-public crate-tree-sitter-tags-0.22.4 (c (n "tree-sitter-tags") (v "0.22.4") (d (list (d (n "memchr") (r "^2.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "0p6d3fhk7i0kwjsrrdvn8jr4j6ghvfxln3230ppi3nbg7d9srz2i") (r "1.74.1")))

(define-public crate-tree-sitter-tags-0.22.5 (c (n "tree-sitter-tags") (v "0.22.5") (d (list (d (n "memchr") (r "^2.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)))) (h "040f22356fafsf0w8ab68rhlk11v2abpcx2y3y8d7d6qm1y9mbay") (r "1.74.1")))

(define-public crate-tree-sitter-tags-0.22.6 (c (n "tree-sitter-tags") (v "0.22.6") (d (list (d (n "memchr") (r "^2.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.6") (d #t) (k 0)))) (h "0sfcka1jcgg5rxg9mvmnmvza83hmkryqhgyq9hdnvcvs14b08f1l") (r "1.74.1")))

