(define-module (crates-io tr ee tree-sitter-solidity) #:use-module (crates-io))

(define-public crate-tree-sitter-solidity-0.0.1 (c (n "tree-sitter-solidity") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)))) (h "0pyicglm8zmm56blgv697svns22smyd7w6zlz3wvsdq9ymln6aix")))

(define-public crate-tree-sitter-solidity-0.0.2 (c (n "tree-sitter-solidity") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "07d0c0sr6q2scn6h94s53c7x4qb6x2wb7zvvav5i9cxi2jn6p27d")))

(define-public crate-tree-sitter-solidity-0.0.3 (c (n "tree-sitter-solidity") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "1r1dywk8qamkdiak76dsqmkkfkcdv6kc0b4w1brd6igll9pgn5db")))

(define-public crate-tree-sitter-solidity-1.2.2 (c (n "tree-sitter-solidity") (v "1.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0sa7z0y9pw8nhbx4xgizdm58l6xm0s1siszb7wkgia18x0rncnzl")))

(define-public crate-tree-sitter-solidity-1.2.3 (c (n "tree-sitter-solidity") (v "1.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1i8zf6mh2mqjaqsaflyxlqnjc55wavg190m6nqrk05hqlbb4mlzh")))

(define-public crate-tree-sitter-solidity-1.2.4 (c (n "tree-sitter-solidity") (v "1.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "01pibg0nlsnz836545b2m4b03fq2ihhy2ydg89cvwa4jvqm87rwd")))

(define-public crate-tree-sitter-solidity-1.2.5 (c (n "tree-sitter-solidity") (v "1.2.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "1zirn1a7q0qz44lh8xqq8a2p0drg0p8g1rs77nvzc0acbfbz02am")))

(define-public crate-tree-sitter-solidity-1.2.6 (c (n "tree-sitter-solidity") (v "1.2.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "1f16ppv2m90dgrjwzla31jd7l915pyggpdlxjz2v2cp8ch34x5sz")))

