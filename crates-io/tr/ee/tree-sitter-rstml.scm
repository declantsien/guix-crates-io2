(define-module (crates-io tr ee tree-sitter-rstml) #:use-module (crates-io))

(define-public crate-tree-sitter-rstml-0.0.1 (c (n "tree-sitter-rstml") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1lcldm9wnylmd75xwb2cjpsnxrj6igmfijgshwr4kx0p0xvm9b9b")))

(define-public crate-tree-sitter-rstml-0.1.0 (c (n "tree-sitter-rstml") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1xawlh93v7ppbc8bz7076zizvwh9z013m367q543h2ypnij76rbj")))

(define-public crate-tree-sitter-rstml-0.2.0 (c (n "tree-sitter-rstml") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0v1qc7xr4x0di3xb30qps8wvygpnylz9a42rb6rxcp779h114pal")))

(define-public crate-tree-sitter-rstml-0.3.0 (c (n "tree-sitter-rstml") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1jqz8agq7xfwclnb6v44az9sc94yx84qwhga16ip5ciyjdda7n56")))

(define-public crate-tree-sitter-rstml-1.0.0 (c (n "tree-sitter-rstml") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "033wz0s4lmgl8rdb9r804swipbm1ab89j8hwz51rz57wafzghkv1")))

(define-public crate-tree-sitter-rstml-1.1.0 (c (n "tree-sitter-rstml") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1c0q9z913w45ypaayn2idi5nrl3m1mnn8nmflligz4qjywcpj1vy")))

(define-public crate-tree-sitter-rstml-1.2.0 (c (n "tree-sitter-rstml") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0dd4riyv1f3f2rn7f0sh56lcf5h58iiykj11w6242ih1lfc3cz7j")))

