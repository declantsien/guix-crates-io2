(define-module (crates-io tr ee treelite) #:use-module (crates-io))

(define-public crate-treelite-0.1.0 (c (n "treelite") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.2") (d #t) (k 0)))) (h "1cw4z1wpymw574mszfc43xlya63r2z1sqg2k8wzkavm486nhz7id")))

(define-public crate-treelite-0.1.1 (c (n "treelite") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.2") (d #t) (k 0)))) (h "1r37p3lgcjrq6cy1da96c78hvfsf2yn2xmb1namkjv0g7n0j80xl")))

(define-public crate-treelite-0.1.2 (c (n "treelite") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.2") (d #t) (k 0)))) (h "17vv0hyf492g0vwrk15k7bzaj2a9ya1y465h8d6iwm22nl26sgqx")))

(define-public crate-treelite-0.1.3 (c (n "treelite") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.2") (d #t) (k 0)))) (h "1vxjm9ascr868qz9n451df63wsl6v4jfyyr7xncqxg8cgy20h45n")))

