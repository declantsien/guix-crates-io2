(define-module (crates-io tr ee tree-sitter-d) #:use-module (crates-io))

(define-public crate-tree-sitter-d-0.3.8 (c (n "tree-sitter-d") (v "0.3.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1wppxgwdr2lwhbvjmadd88fwycvnawrk9cq0xyqhahvb8xfyix72")))

