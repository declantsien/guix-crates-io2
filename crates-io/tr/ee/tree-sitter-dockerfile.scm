(define-module (crates-io tr ee tree-sitter-dockerfile) #:use-module (crates-io))

(define-public crate-tree-sitter-dockerfile-0.0.1 (c (n "tree-sitter-dockerfile") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "07r3wy5jcdn5s63gvip59lmvwzxvyfn006nd7g75y7jz3fvl2vi9")))

(define-public crate-tree-sitter-dockerfile-0.1.0 (c (n "tree-sitter-dockerfile") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "199pfrphq8x1r46gj36hrvd8q4ar34ra5wvrhpgr50wa4zni9qq1")))

(define-public crate-tree-sitter-dockerfile-0.2.0 (c (n "tree-sitter-dockerfile") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0aj3ykvp4w0hpahx9g1yj64f60l24874pakrr926fqdczh732ims")))

