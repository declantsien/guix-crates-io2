(define-module (crates-io tr ee tree-sitter-ocamllex) #:use-module (crates-io))

(define-public crate-tree-sitter-ocamllex-0.20.0 (c (n "tree-sitter-ocamllex") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0i0lfn5bl09ilpqpbn1rq088a6pkq700a68f0jms2z6fpy11ylaj")))

(define-public crate-tree-sitter-ocamllex-0.20.1 (c (n "tree-sitter-ocamllex") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0pn6fxb3lnb0i80b6f7gdy3ab8sfm414fwyj4zkz9140zvljfz75")))

(define-public crate-tree-sitter-ocamllex-0.20.2 (c (n "tree-sitter-ocamllex") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0qls885z0gshaanxp9rag2zqdhxz3wizn1b6kfcnbl3g10i44xwy")))

