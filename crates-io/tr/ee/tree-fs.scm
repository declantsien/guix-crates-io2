(define-module (crates-io tr ee tree-fs) #:use-module (crates-io))

(define-public crate-tree-fs-0.1.0 (c (n "tree-fs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (o #t) (d #t) (k 0)))) (h "0qi3nk5rrx84a90s5vlgddmv9bk9kv06wg1lvzfa6qx7ya5zxg7p") (f (quote (("default" "yaml")))) (s 2) (e (quote (("yaml" "dep:serde_yaml" "dep:thiserror"))))))

