(define-module (crates-io tr ee treeflection_derive) #:use-module (crates-io))

(define-public crate-treeflection_derive-0.1.0 (c (n "treeflection_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.5") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)) (d (n "treeflection") (r "^0.1.1") (d #t) (k 2)))) (h "14z91i1g29vd5ix4j58grz0v6vwk6rc1r0ww11yacy48am922gf5")))

(define-public crate-treeflection_derive-0.1.1 (c (n "treeflection_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.5") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0jrsa5b6kyph4vyy1347czqpb0gb1zr9j3gdq0rhxgl4nk9lbzj0")))

(define-public crate-treeflection_derive-0.1.2 (c (n "treeflection_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.3.5") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0sw8rf2xpfshrk4l343nhv62ccnlb2p6slvl08yc73yvlsqj2aqg")))

(define-public crate-treeflection_derive-0.1.3 (c (n "treeflection_derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0i6jw5vv8pfxhif1vm52jvdgdz0sdqnx9rzd7nm43iazcfbmnflh")))

(define-public crate-treeflection_derive-0.1.4 (c (n "treeflection_derive") (v "0.1.4") (d (list (d (n "quote") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1ilfk3wd0qqm5v4dbrbf99kxl8n413pdkbn934ii2w0577m9g4j2")))

(define-public crate-treeflection_derive-0.1.5 (c (n "treeflection_derive") (v "0.1.5") (d (list (d (n "quote") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1ahxg7mvn19yc78cjdk4s593dl8rm9fkxi454d9k9cl0x01w09m3")))

(define-public crate-treeflection_derive-0.1.6 (c (n "treeflection_derive") (v "0.1.6") (d (list (d (n "quote") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1hdcn1jd4paq0hvkqi5p21ahsrgx9j2ky3c7wq2zp7qz5mmmmj44")))

(define-public crate-treeflection_derive-0.1.7 (c (n "treeflection_derive") (v "0.1.7") (d (list (d (n "quote") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1kj89d9fppd73sw7p5dxhflc72xqbidh7srvghv3mjwkxv33hyg2")))

(define-public crate-treeflection_derive-0.1.8 (c (n "treeflection_derive") (v "0.1.8") (d (list (d (n "quote") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0x3l5b7ndwrkc7w4rmxnvx1dypnnb5prkbirqv9wp7643rj2lr1a")))

(define-public crate-treeflection_derive-0.1.9 (c (n "treeflection_derive") (v "0.1.9") (d (list (d (n "quote") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^0.10.3") (d #t) (k 0)))) (h "0v6way5r6hwhykk5i8mh69rmc1w52slbig5qwd7sccl6wjcw206m")))

(define-public crate-treeflection_derive-0.1.10 (c (n "treeflection_derive") (v "0.1.10") (d (list (d (n "quote") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^0.10.3") (d #t) (k 0)))) (h "05fg3mww2lp3h5sd0x831z8zbc4a7w0ip346ml8614ppkri5d4rh")))

(define-public crate-treeflection_derive-0.1.11 (c (n "treeflection_derive") (v "0.1.11") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "150kfmdhiwmijhd7da7ab1b0s96y7vj6njfxkanp6sl1k1w38kq7")))

(define-public crate-treeflection_derive-0.1.12 (c (n "treeflection_derive") (v "0.1.12") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0py5lzld6llyfppz1brs01sz059izksh2bjq2fyglhfcnk4z8q0f")))

(define-public crate-treeflection_derive-0.1.13 (c (n "treeflection_derive") (v "0.1.13") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0dwb0bzvcr6xlbzqghyyl53jyiy1x3642zw8vh1rvrzl14kgjcch")))

(define-public crate-treeflection_derive-0.1.14 (c (n "treeflection_derive") (v "0.1.14") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1k2bppwr48kxvc9jzsha3vfvq4wc96kl1fdsm98ia1gm74kcd74b")))

(define-public crate-treeflection_derive-0.1.15 (c (n "treeflection_derive") (v "0.1.15") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "treeflection") (r "^0.1") (d #t) (k 2)))) (h "0mks8dh471zln3dp299sm1n5hc60y6ddw61hg2cd4q9cgf6zz3ii")))

(define-public crate-treeflection_derive-0.1.16 (c (n "treeflection_derive") (v "0.1.16") (d (list (d (n "matches") (r "^0.1.4") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "treeflection") (r "^0.1") (d #t) (k 2)))) (h "0l7pp3jagrwlqsj0mj2xx2n8i74fjrmfkaangj1g5dlys9sz0r7s")))

(define-public crate-treeflection_derive-0.2.0 (c (n "treeflection_derive") (v "0.2.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)) (d (n "treeflection") (r "^0.1") (d #t) (k 2)))) (h "0qvan69v4n9bxlddhdh9ybsv0bmqlffg8mdbj8rilyhswy2fdm9k")))

(define-public crate-treeflection_derive-0.2.1 (c (n "treeflection_derive") (v "0.2.1") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)) (d (n "treeflection") (r "^0.1") (d #t) (k 2)))) (h "0garffq54r491vff1ac2n9m220xj6pxwcmx3p9b1gwv3hlv9dvx0")))

(define-public crate-treeflection_derive-0.3.0 (c (n "treeflection_derive") (v "0.3.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "treeflection") (r "^0.1") (d #t) (k 2)))) (h "0bgcggfyl1555vwgaz18a8r3gvwfcccy2gdgam2kbhlzd8dilqq3")))

(define-public crate-treeflection_derive-0.3.1 (c (n "treeflection_derive") (v "0.3.1") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "treeflection") (r "^0.1") (d #t) (k 2)))) (h "0qqsrqim186z5fwivfqapp81xvy6c475dcd7d0j9ikfmkiz9f29m")))

(define-public crate-treeflection_derive-0.3.2 (c (n "treeflection_derive") (v "0.3.2") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "treeflection") (r "^0.1") (d #t) (k 2)))) (h "0mgvfka5i0n6h5prizmf06ibgy4pkw96zs93g97qr8rzd16gbc34")))

(define-public crate-treeflection_derive-0.4.0 (c (n "treeflection_derive") (v "0.4.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "treeflection") (r "^0.1") (d #t) (k 2)))) (h "05zyzyxqyhpz5h8c8g5vz6kqik3dcyd7sbdx6n6p2y75jrf604y0")))

