(define-module (crates-io tr ee treeify) #:use-module (crates-io))

(define-public crate-treeify-0.1.2 (c (n "treeify") (v "0.1.2") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1b91y059fkf2imlsw59dcg9ry0vqi1b3nkz8020zpwg2z3w2k8xa")))

(define-public crate-treeify-0.1.3 (c (n "treeify") (v "0.1.3") (d (list (d (n "clap") (r "^2.9") (d #t) (k 0)) (d (n "clap") (r "^2.9") (d #t) (k 1)))) (h "1kaln1c7pwaqmfdxcxxh6hkmva5iwbjqpvs4s73cnhdvy8qpg18k")))

(define-public crate-treeify-0.1.4 (c (n "treeify") (v "0.1.4") (d (list (d (n "clap") (r "^2.9") (d #t) (k 0)) (d (n "clap") (r "^2.9") (d #t) (k 1)))) (h "1sf98fv2b66x8mk8n11m4xzdmnq1nb5p00rl5dqvvm58ykwpxrcc")))

