(define-module (crates-io tr ee tree_sitter_grep_tree-sitter-rust) #:use-module (crates-io))

(define-public crate-tree_sitter_grep_tree-sitter-rust-0.20.3-dev.0 (c (n "tree_sitter_grep_tree-sitter-rust") (v "0.20.3-dev.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.20") (d #t) (k 0)))) (h "0izrny5l5w0zamljkvxmp3msq5mlyzxz6ph9q766nzkig9fl47yh")))

