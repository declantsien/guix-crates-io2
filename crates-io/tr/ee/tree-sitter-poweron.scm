(define-module (crates-io tr ee tree-sitter-poweron) #:use-module (crates-io))

(define-public crate-tree-sitter-poweron-0.5.1 (c (n "tree-sitter-poweron") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0f9p9gig899xv0akx6w6n2dkxw09innnhnr7ij321bc5w39yc3dg")))

(define-public crate-tree-sitter-poweron-0.5.2 (c (n "tree-sitter-poweron") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "18ppyx27zm89gvxxf0di2gyxvznq2j1nmn3lrkkgwddv3s9v305r")))

(define-public crate-tree-sitter-poweron-1.0.0 (c (n "tree-sitter-poweron") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "02gg1s4irg4pw934f6r7xcxq3p6pppz01sqp6azkd8nw2ky7fspx")))

(define-public crate-tree-sitter-poweron-1.0.1 (c (n "tree-sitter-poweron") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0hj3n12935pp1mp7s0l00g08xn6mw3mwg69ia681n4iqs425lz0b")))

(define-public crate-tree-sitter-poweron-1.0.2 (c (n "tree-sitter-poweron") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1fbii6f8mpcvnz1n7q8r9722y83jahy4bjm4m5da0qdxpmrd755y")))

(define-public crate-tree-sitter-poweron-1.0.3 (c (n "tree-sitter-poweron") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "01qz6qnnhdrnfh9miy7bcsyxpgslaj04590xywvbj299nz555qzz")))

(define-public crate-tree-sitter-poweron-1.0.4 (c (n "tree-sitter-poweron") (v "1.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0rnc5zb8420wbg43rmpxj4fsdq9n9d98q0cycpnq33xprjn5mcsx")))

(define-public crate-tree-sitter-poweron-1.0.5 (c (n "tree-sitter-poweron") (v "1.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "18rljygr8ysx1z5jjb7mlg15fm75fnak18rm6rw00q1d8wr1143l")))

(define-public crate-tree-sitter-poweron-1.0.7 (c (n "tree-sitter-poweron") (v "1.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0svmjnaljfp2n8my9vcsl0lza73avci3h4bllrd1fckqr3znhpv3")))

(define-public crate-tree-sitter-poweron-1.0.8 (c (n "tree-sitter-poweron") (v "1.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1wmf5prxy0wnwaim5d5d5r6b7y4h66q9bkzi8m6jnxfhd293v52w")))

(define-public crate-tree-sitter-poweron-1.0.9 (c (n "tree-sitter-poweron") (v "1.0.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0v0nxrf0j50kdr7j7s4jww7wyxgkny46mvzqf1vj6bif6hva66rl")))

(define-public crate-tree-sitter-poweron-1.0.10 (c (n "tree-sitter-poweron") (v "1.0.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1a34dxz6cmbfrfw9hvwfrawc5hqd2ip5j9n83q820d4gfhv3alqm")))

(define-public crate-tree-sitter-poweron-1.0.11 (c (n "tree-sitter-poweron") (v "1.0.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0cvscljnbjxi7ww87rl2c1zm7sarzagsfqd4absrfmgczfhqzmsm")))

(define-public crate-tree-sitter-poweron-1.0.12 (c (n "tree-sitter-poweron") (v "1.0.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1ns2w699ak687p24kaaxwf76rbz4fgz50zjxr5dark5jzz9hi6wx")))

