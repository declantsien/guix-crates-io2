(define-module (crates-io tr ee treedir) #:use-module (crates-io))

(define-public crate-treedir-0.1.0 (c (n "treedir") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1yvkhi2z6v7hk0p77rp3qwgxjsyva33c07xdi8dmld0hw94ky1qn")))

(define-public crate-treedir-0.1.1 (c (n "treedir") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1lpj2js1936xgm9s0aszz6xrpzdqhqi1p78f2mqd5idfpl5dzi6f")))

