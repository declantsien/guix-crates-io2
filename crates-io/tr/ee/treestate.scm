(define-module (crates-io tr ee treestate) #:use-module (crates-io))

(define-public crate-treestate-0.1.0 (c (n "treestate") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.1") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "utime") (r "^0.3.1") (d #t) (k 2)))) (h "05gbrcvcnghh4rmm79i523akjix4h3ii9avj3d5qqqs8p1aag233")))

(define-public crate-treestate-0.1.1 (c (n "treestate") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "utime") (r "^0.3.1") (d #t) (k 2)))) (h "09b9rb7ifzb9sfqnp3g2f3g0x0zdfx6d7syfpqpv8hl23vqgafw7")))

