(define-module (crates-io tr ee tree-sitter-python) #:use-module (crates-io))

(define-public crate-tree-sitter-python-0.16.1 (c (n "tree-sitter-python") (v "0.16.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.17") (d #t) (k 0)))) (h "0rpnfd5gi5qinf74krrdj6rzwp3m7b94f8881i7fnxb70v8mkkgg")))

(define-public crate-tree-sitter-python-0.17.0 (c (n "tree-sitter-python") (v "0.17.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.17") (d #t) (k 0)))) (h "1jk1phh0f1c5q24h6j993hg1kcb1pv9aqagjyjwqlh5r08ys6s6w")))

(define-public crate-tree-sitter-python-0.19.0 (c (n "tree-sitter-python") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "0v7xx60n5nl5ks2q4m89cyvyl4f36j1w7q2cf4dw5caf3kkvyijn")))

(define-public crate-tree-sitter-python-0.19.1 (c (n "tree-sitter-python") (v "0.19.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "171pik6pqiyig2v8x0lgbi8lq41bipdd4g3n4ddjmg1yalb6ki43")))

(define-public crate-tree-sitter-python-0.20.0 (c (n "tree-sitter-python") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0vfxhfybfz5yhw9dr6gkds59356ylagxplcxads64hb5bi3iblz0")))

(define-public crate-tree-sitter-python-0.20.1 (c (n "tree-sitter-python") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1ix7rpirc2g490cfs1m8jrgy2q9ld4ixvimhhjvpchx99dl70cbi")))

(define-public crate-tree-sitter-python-0.20.2 (c (n "tree-sitter-python") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "11sjjw8qgmiz4sa0vdfjwq9y3f7z3mlzz2hmryfhbxa8h3si98fx")))

(define-public crate-tree-sitter-python-0.20.3 (c (n "tree-sitter-python") (v "0.20.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "051sq8n0dlp3caskw5nx65a8kvzjgd8hi6rqyjr689v3mjfbszpl")))

(define-public crate-tree-sitter-python-0.20.4 (c (n "tree-sitter-python") (v "0.20.4") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "19cajx86brj6vgdjvxng9l5kvr2q63ym2ps4nffkj3dx3wdkpjg6")))

(define-public crate-tree-sitter-python-0.21.0 (c (n "tree-sitter-python") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.89") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1achqgyj89irf4g9hgg7rhs4ij25b4h1ymn4qbw65ybqyrn6q1ml")))

