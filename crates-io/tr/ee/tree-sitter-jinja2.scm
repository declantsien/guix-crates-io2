(define-module (crates-io tr ee tree-sitter-jinja2) #:use-module (crates-io))

(define-public crate-tree-sitter-jinja2-0.0.1 (c (n "tree-sitter-jinja2") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0hc3lx9bfpcv368g65mmxgd20mig5jlg4yva0dakjli9wb9vq6l5")))

(define-public crate-tree-sitter-jinja2-0.0.2 (c (n "tree-sitter-jinja2") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "18gipg5bqb05cq0xd9k6zp44vh33zvxx46jf2hqc4p0zzzszjds4") (y #t)))

(define-public crate-tree-sitter-jinja2-0.0.3 (c (n "tree-sitter-jinja2") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "090191gl1hxsq3s0wsmral05hssxf51bkykn4dglp0bb7p1irdlf")))

(define-public crate-tree-sitter-jinja2-0.0.4 (c (n "tree-sitter-jinja2") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "149yffws2l0vb4gzsgkb22s2fryr4d3p7pbnh5qpysz8i6sxxyy6")))

(define-public crate-tree-sitter-jinja2-0.0.5 (c (n "tree-sitter-jinja2") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0bw21vdvf428d3ws5fwhym9m9i67xj4cgvch3ka5xsphggkaf0cw")))

(define-public crate-tree-sitter-jinja2-0.0.6 (c (n "tree-sitter-jinja2") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "12r5y397qd7shrnj1c2v3c5q41ihwsp88vrjlk89a78nkkmkxq86")))

