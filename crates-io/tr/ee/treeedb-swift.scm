(define-module (crates-io tr ee treeedb-swift) #:use-module (crates-io))

(define-public crate-treeedb-swift-0.1.0-rc.5 (c (n "treeedb-swift") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-swift") (r "^0.3.4") (d #t) (k 0)) (d (n "treeedb") (r "^0.1.0-rc.5") (f (quote ("cli"))) (d #t) (k 0)))) (h "02q5gwg2pn07xcjp6mj441xc0xhawy94zs64hajjs4j5ddsjsqii")))

