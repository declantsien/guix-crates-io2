(define-module (crates-io tr ee tree-sitter-org) #:use-module (crates-io))

(define-public crate-tree-sitter-org-1.3.0 (c (n "tree-sitter-org") (v "1.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "1a59fijn6wf4smm44aafhbq43w8c4pmsyqa3x61gw9ch0ls2faly")))

(define-public crate-tree-sitter-org-1.3.1 (c (n "tree-sitter-org") (v "1.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0bc9ix7x7k560acdxv2nf6b29z5wnywphgci01igxv1z1140l39z")))

(define-public crate-tree-sitter-org-1.3.2 (c (n "tree-sitter-org") (v "1.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0xrfg4pzwninmbs2qj69pb86dhv3dgkcbfc38xwhgy15hdfirwwl")))

(define-public crate-tree-sitter-org-1.3.3 (c (n "tree-sitter-org") (v "1.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "037bkl87i94w5pfri7k8m2azi43zd6710izb8267zc6z43nxvqj2")))

