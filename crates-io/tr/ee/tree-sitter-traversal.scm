(define-module (crates-io tr ee tree-sitter-traversal) #:use-module (crates-io))

(define-public crate-tree-sitter-traversal-0.1.0 (c (n "tree-sitter-traversal") (v "0.1.0") (d (list (d (n "tree-sitter") (r ">=0.20") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20.0") (d #t) (k 2)))) (h "1pj4g234a965ksznl8429gzmgpb4rqxxs2zq3x9h1fbra7l1jmf7")))

(define-public crate-tree-sitter-traversal-0.1.1 (c (n "tree-sitter-traversal") (v "0.1.1") (d (list (d (n "tree-sitter") (r ">=0.19, <0.21") (o #t) (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20.0") (d #t) (k 2)))) (h "00wsf0az7avbzjr16pk439p31b153y2jjw2sdl3g4kvc675ldqhs") (f (quote (("default" "tree-sitter"))))))

(define-public crate-tree-sitter-traversal-0.1.2 (c (n "tree-sitter-traversal") (v "0.1.2") (d (list (d (n "tree-sitter") (r ">=0.19, <0.21") (o #t) (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20.0") (d #t) (k 2)))) (h "01zkd2c7jz6bvi00bdq17yr0klgdm6xl04q7bx8di9744n11b2nz") (f (quote (("default" "tree-sitter"))))))

