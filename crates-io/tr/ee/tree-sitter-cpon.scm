(define-module (crates-io tr ee tree-sitter-cpon) #:use-module (crates-io))

(define-public crate-tree-sitter-cpon-0.0.1 (c (n "tree-sitter-cpon") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0p06cnpi5vqa5l2nswr8b330n10751dmadc5iy22aqxd6bn2mzy9")))

(define-public crate-tree-sitter-cpon-0.0.2 (c (n "tree-sitter-cpon") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1pr490cvkbfi76d6a510g8m9iy32lklqg1i30kmdr0236l231yqg")))

(define-public crate-tree-sitter-cpon-1.0.0 (c (n "tree-sitter-cpon") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0x5cwr7h5ccadz026944b8739b34jyb4qk5yvfawjr4v218hizxq")))

