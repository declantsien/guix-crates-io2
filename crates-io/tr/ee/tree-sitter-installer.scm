(define-module (crates-io tr ee tree-sitter-installer) #:use-module (crates-io))

(define-public crate-tree-sitter-installer-0.1.0 (c (n "tree-sitter-installer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "1s7sksgyljxhin4wg5npn27agpags8rc9yinwbpgd3kvhvyk5p55")))

(define-public crate-tree-sitter-installer-0.1.1 (c (n "tree-sitter-installer") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "1l7sh2phc5xaf058x4df147q7snnabz537250q3z4cnhdfa5bjfc")))

(define-public crate-tree-sitter-installer-0.2.0 (c (n "tree-sitter-installer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "0jv32lk3py7kv80lg3d0xr46wv9s5wdw5qx5fax5lb4h3flgab5s")))

