(define-module (crates-io tr ee tree-sitter-mozjs) #:use-module (crates-io))

(define-public crate-tree-sitter-mozjs-0.19.0 (c (n "tree-sitter-mozjs") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.19.0") (d #t) (k 1)))) (h "1j0rfgmsnc4zpri1li97m1fqh24hajj1y68jp5wksz8m20fb5xny")))

(define-public crate-tree-sitter-mozjs-0.20.1 (c (n "tree-sitter-mozjs") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20.0") (d #t) (k 1)))) (h "0h2bld6mi1r4c6wzy3vir42zr8s6db7xanp1cvzbg3cndsak4vc3")))

