(define-module (crates-io tr ee tree-buf-macros) #:use-module (crates-io))

(define-public crate-tree-buf-macros-0.2.0 (c (n "tree-buf-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1a7xfmlrwy2wgm2n2m4iivgbi3slfbm8a8znx7vyg07z0h978rwj")))

(define-public crate-tree-buf-macros-0.3.0 (c (n "tree-buf-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "11mg51rppry3fa08gyhk9ljs24781hc4lnwkmh8pysp861dn7s65")))

(define-public crate-tree-buf-macros-0.4.0 (c (n "tree-buf-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0g066qg6c8pyhh1ldvlk3zjsv4xp4cq198hzcc8s4cx2msq5iqs1")))

(define-public crate-tree-buf-macros-0.5.0 (c (n "tree-buf-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0sv55sg6xj56rxk5wc5510rrjxbgw47v91pjfxi4yakm9xism1ci")))

(define-public crate-tree-buf-macros-0.6.0 (c (n "tree-buf-macros") (v "0.6.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0ag10vs5dkd7n6lw5pl8hz6jhycq2j823wsj6584sjv38vlg61cx")))

(define-public crate-tree-buf-macros-0.7.0 (c (n "tree-buf-macros") (v "0.7.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0sqppf09kab7n5fx0k9g05mbv8dxvjb70f6riih1prkc27w9dshc")))

(define-public crate-tree-buf-macros-0.8.0 (c (n "tree-buf-macros") (v "0.8.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0w4f89xma2dnkrc419ndjpphfy6pzxlmzsh9m019kxm6l9r30zcc")))

(define-public crate-tree-buf-macros-0.9.0 (c (n "tree-buf-macros") (v "0.9.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1lkvkmyxqqf55hjrcnjcgwn8zyr8csl0xigzqbljg08581j4xqvy")))

(define-public crate-tree-buf-macros-0.10.0 (c (n "tree-buf-macros") (v "0.10.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0a1fssyaqz922dviawnvm9mpdysry5yaki56zh3faxzpafz1lxyd")))

