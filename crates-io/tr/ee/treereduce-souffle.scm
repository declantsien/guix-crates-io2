(define-module (crates-io tr ee treereduce-souffle) #:use-module (crates-io))

(define-public crate-treereduce-souffle-0.1.0 (c (n "treereduce-souffle") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-souffle") (r "^0.4.0") (d #t) (k 0)) (d (n "treereduce") (r "^0.1.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "0r4ljchp411kwsi2k3jwvyni3mx1nd8jr1vq58s4x9xznm5p7b7i")))

(define-public crate-treereduce-souffle-0.2.0 (c (n "treereduce-souffle") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-souffle") (r "^0.4.0") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "15v8rygan56cwkbhdmwl9zcmkcchmyv7vamrldhz83xm42mif3ln")))

(define-public crate-treereduce-souffle-0.2.1 (c (n "treereduce-souffle") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-souffle") (r "^0.4.0") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.1") (f (quote ("cli"))) (d #t) (k 0)))) (h "0gygriv85cn1rw0r7088glncc2m8x3r9sx3w3i48xq4cx09arpz0")))

(define-public crate-treereduce-souffle-0.2.2 (c (n "treereduce-souffle") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-souffle") (r "^0.4.0") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.2") (f (quote ("cli"))) (d #t) (k 0)))) (h "1k1gp9xf89j5bzydj6sf59x224l0r2ipgmjk262hgqjbjxpvpz33")))

(define-public crate-treereduce-souffle-0.3.0 (c (n "treereduce-souffle") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-souffle") (r "^0.4.0") (d #t) (k 0)) (d (n "treereduce") (r "^0.3.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "18bbrb1clf1zqd6v94c7jx2li7i4mm9ji8c39092w1nhq2j4jiaa")))

