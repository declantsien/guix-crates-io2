(define-module (crates-io tr ee tree-sitter-luau) #:use-module (crates-io))

(define-public crate-tree-sitter-luau-1.0.0 (c (n "tree-sitter-luau") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "17f7p1ddlz0xk3va3s63k7q15i69dg8xnq046j9w8s1pwg98izh3")))

(define-public crate-tree-sitter-luau-1.1.0 (c (n "tree-sitter-luau") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1h0l2sy0bs5vgga6alp16l1awz9gmgiingzx6pfax25rxbfijwkp")))

