(define-module (crates-io tr ee tree-sitter-dart) #:use-module (crates-io))

(define-public crate-tree-sitter-dart-0.0.1 (c (n "tree-sitter-dart") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.8") (d #t) (k 0)))) (h "0azjvgm0820rsd22wx129j7aj7a6dpvf17ilxyylqfvnr391pxdl")))

(define-public crate-tree-sitter-dart-0.0.2 (c (n "tree-sitter-dart") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.8") (d #t) (k 0)))) (h "0yr983190lwd12n981gjjalzmghm0ygj22x250qpg5xk1lyb6z4b")))

(define-public crate-tree-sitter-dart-0.0.3 (c (n "tree-sitter-dart") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.8") (d #t) (k 0)))) (h "1fijs5s2fi8vzlhiaw0x20fw8ip4jqfviksqqma0bpc94qcs5dig")))

(define-public crate-tree-sitter-dart-0.0.4 (c (n "tree-sitter-dart") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.20.8") (d #t) (k 0)))) (h "12p3yhv81fxsmd55nlw7jpl9sbmsnmkyzz5a2hz38hffh05zgw8r")))

