(define-module (crates-io tr ee tree-sitter-tlaplus) #:use-module (crates-io))

(define-public crate-tree-sitter-tlaplus-0.1.0 (c (n "tree-sitter-tlaplus") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "0k0wvwp917z8g1dmr2w3qxpmnw157s21f09midly7dmrag304g62")))

(define-public crate-tree-sitter-tlaplus-0.1.1 (c (n "tree-sitter-tlaplus") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "0kjv48akkv5dx7j0ygz6i1w43x9rijq1flyqz1k2p0lrla4xp2v2")))

(define-public crate-tree-sitter-tlaplus-0.1.2 (c (n "tree-sitter-tlaplus") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "1g4j21l2cwpwqv54gsm55przymmmp68yzh78z4xcyd8isrnimn4g")))

(define-public crate-tree-sitter-tlaplus-0.2.0 (c (n "tree-sitter-tlaplus") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "0n1hjfqx00lvcvdv4rlqxk4g283mfs4yb2dz4xypxpq3gil7snbl")))

(define-public crate-tree-sitter-tlaplus-0.2.1 (c (n "tree-sitter-tlaplus") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "146c0d8kzkimy544qcxqiflqxd2zw63rynlpdy8039cbpxggw50k")))

(define-public crate-tree-sitter-tlaplus-0.3.0 (c (n "tree-sitter-tlaplus") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "0f2j5a0ffl3mi0fq6cbnlkl3by3js9snwwf6pa6qvkpybp8f32p9")))

(define-public crate-tree-sitter-tlaplus-0.4.0 (c (n "tree-sitter-tlaplus") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.3") (d #t) (k 0)))) (h "1x4k26ppzl2yf73i28ra57qirmb18787swlxk8a0v6ih95g9q0jv")))

(define-public crate-tree-sitter-tlaplus-0.4.1 (c (n "tree-sitter-tlaplus") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.3") (d #t) (k 0)))) (h "1rxacwr971qgqc6zzdp3l8kc96609bqp0rmsapmxwxdsimmam2c4")))

(define-public crate-tree-sitter-tlaplus-0.5.0 (c (n "tree-sitter-tlaplus") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.3") (d #t) (k 0)))) (h "06z89l5qzc8mamh428nixy8ixgmfis71qi7jkvz2fdyayrlhbmmn")))

(define-public crate-tree-sitter-tlaplus-0.6.0 (c (n "tree-sitter-tlaplus") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.3") (d #t) (k 0)))) (h "0m9gbcn3j6ldi0lhj9mwi2r9yhldvj1fi7vp2hc7ybc43l8lmv5q")))

(define-public crate-tree-sitter-tlaplus-0.6.1 (c (n "tree-sitter-tlaplus") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.3") (d #t) (k 0)))) (h "1q181sbpkzavbyanvldw5r100idavk4p8735l3vcvsy4jrd9r10q")))

(define-public crate-tree-sitter-tlaplus-0.6.2 (c (n "tree-sitter-tlaplus") (v "0.6.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.3") (d #t) (k 0)))) (h "0y6vnx0avzdwjdy2qqi1zzk3m66kp6rplwb0jv70qkgv8g3kzsdz")))

(define-public crate-tree-sitter-tlaplus-1.0.1 (c (n "tree-sitter-tlaplus") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.3") (d #t) (k 0)))) (h "1f32mq6kd2a98ml6gj3v8hbsn70rqi4ccpvb2603iml3bzhnrcg4")))

(define-public crate-tree-sitter-tlaplus-1.0.2 (c (n "tree-sitter-tlaplus") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.3") (d #t) (k 0)))) (h "1p7b5sq6vdmmpmpkibq7b90llca1vm1amsmz3cdf8a73fw85qzag")))

(define-public crate-tree-sitter-tlaplus-1.0.3 (c (n "tree-sitter-tlaplus") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.3") (d #t) (k 0)))) (h "15qljmmjhlw5v4rahw2kshn55pkrfbg264n4qby02z3zx7invkv5")))

(define-public crate-tree-sitter-tlaplus-1.0.4 (c (n "tree-sitter-tlaplus") (v "1.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.3") (d #t) (k 0)))) (h "07qfi5xr3fdprdnab8kzpkkjj07z0xxiy26ary01m6ymjrds0r5h")))

(define-public crate-tree-sitter-tlaplus-1.0.6 (c (n "tree-sitter-tlaplus") (v "1.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1iraq40z4sj76agv5pzsnma7m7y8qgb4davssbq33pmxl1dy70vz")))

(define-public crate-tree-sitter-tlaplus-1.0.7 (c (n "tree-sitter-tlaplus") (v "1.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0lshraps6arxx3k6wc4n20j30f6xig5vx1vkq1ipg0hgpmi99lkb")))

(define-public crate-tree-sitter-tlaplus-1.0.8 (c (n "tree-sitter-tlaplus") (v "1.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0p59zld05b8hg25dkvma7izcbar424zzzbll078p3n43x9l7wapd")))

(define-public crate-tree-sitter-tlaplus-1.1.0 (c (n "tree-sitter-tlaplus") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "06x583palhl3n5r3asai25zk4qa1jc9s3k43r5x0jdah1lnbiyql")))

(define-public crate-tree-sitter-tlaplus-1.2.0 (c (n "tree-sitter-tlaplus") (v "1.2.0") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.1") (d #t) (k 0)))) (h "1l43gynx5lc0dbbmra08lpqkcr5al5bn7sqhzzapizg11kjw3vsb")))

(define-public crate-tree-sitter-tlaplus-1.2.1 (c (n "tree-sitter-tlaplus") (v "1.2.1") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.1") (d #t) (k 0)))) (h "165zpm8j1y36yp07ym13wlzp2m38c0f65qfikrz8yyqy02d985by")))

(define-public crate-tree-sitter-tlaplus-1.2.3 (c (n "tree-sitter-tlaplus") (v "1.2.3") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.1") (d #t) (k 0)))) (h "0n9wpc1h8bkpf1zsgjib1g6lqjq8awhpjyganak7c6g05n71w92w")))

(define-public crate-tree-sitter-tlaplus-1.2.4 (c (n "tree-sitter-tlaplus") (v "1.2.4") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.1") (d #t) (k 0)))) (h "1g1slyfljxksm36l2q1lwb6ipad0jrcmv23p8m5cdqh6xf6yqq9n")))

(define-public crate-tree-sitter-tlaplus-1.2.7 (c (n "tree-sitter-tlaplus") (v "1.2.7") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.1") (d #t) (k 0)))) (h "10qx9c9ib53ilcmqr3p2w3cs441n0izqdiab6prxv8wglcjrbgxh")))

(define-public crate-tree-sitter-tlaplus-1.2.8 (c (n "tree-sitter-tlaplus") (v "1.2.8") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "0ysdrm66hgjdsy6k3s1gmavrsf953ly15wiqp5m13xs71xnnw0z3")))

(define-public crate-tree-sitter-tlaplus-1.3.0 (c (n "tree-sitter-tlaplus") (v "1.3.0") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "1nlgr75dqd2wx44r00fhx6ji51fdm7zb81mc8qdia6bpg2h44c17")))

(define-public crate-tree-sitter-tlaplus-1.3.3 (c (n "tree-sitter-tlaplus") (v "1.3.3") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "1lksyb0h1syvc53cqds39z1zag2bfi12dhrfd6rdr46hqi6fzf7q")))

(define-public crate-tree-sitter-tlaplus-1.3.4 (c (n "tree-sitter-tlaplus") (v "1.3.4") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "0xa7va2j7d21zih8kfl89qfndma9dlq1qfk4hi3404cbm5ws3n83")))

(define-public crate-tree-sitter-tlaplus-1.3.5 (c (n "tree-sitter-tlaplus") (v "1.3.5") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "1mcdvkx0bim5m45v6ms5frc11fwd7w78q95vq6g39r8xg0fb1sqy")))

(define-public crate-tree-sitter-tlaplus-1.3.6 (c (n "tree-sitter-tlaplus") (v "1.3.6") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)))) (h "0yxgsmhmrgxvgcq146abi4iq6q5fch70xqf5n2h0rq1a7n6hah82")))

