(define-module (crates-io tr ee tree-crasher-python) #:use-module (crates-io))

(define-public crate-tree-crasher-python-0.4.0 (c (n "tree-crasher-python") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.4.0") (d #t) (k 0)) (d (n "tree-sitter-python") (r "^0.20") (d #t) (k 0)))) (h "0jpzsvn87y0qp0lk3hpxmbfawpy6sn1ggq6456x59gbb2iclbvl3")))

