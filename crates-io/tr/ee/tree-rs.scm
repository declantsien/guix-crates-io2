(define-module (crates-io tr ee tree-rs) #:use-module (crates-io))

(define-public crate-tree-rs-0.1.0 (c (n "tree-rs") (v "0.1.0") (d (list (d (n "clap") (r "~2.20") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "1axykj29fk7ibp0q7jzni3gibl67bj03rpqhbx72jwx8khfnjkib")))

(define-public crate-tree-rs-0.2.0 (c (n "tree-rs") (v "0.2.0") (d (list (d (n "clap") (r "~2.20") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "070lj6vazqm5gwjqd1m4fvzannki01jbgagy52fm80f5ivgf36gm")))

(define-public crate-tree-rs-0.3.0 (c (n "tree-rs") (v "0.3.0") (d (list (d (n "clap") (r "~2.20") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "1vpc3xadzpdcb75cg0lf7hfsrx503ywdds102siqz30x01hzlywh")))

(define-public crate-tree-rs-0.3.1 (c (n "tree-rs") (v "0.3.1") (d (list (d (n "clap") (r "~2.20") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "0q3c9br2dlkzaf05kjxqr5x5plyk21kvq7snw5amlm8x3vpg5cjl")))

(define-public crate-tree-rs-0.3.2 (c (n "tree-rs") (v "0.3.2") (d (list (d (n "clap") (r "~2.20") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "0bv9py3g166lr187i2xijv275kachwg39k6zhycrhvzb78si5zm9")))

(define-public crate-tree-rs-0.4.0 (c (n "tree-rs") (v "0.4.0") (d (list (d (n "clap") (r "~2.20") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "1z305zkw7am4bpfgy2imy8wkk7fjx5wi0145dlykb468557dyj90")))

(define-public crate-tree-rs-0.5.0 (c (n "tree-rs") (v "0.5.0") (d (list (d (n "clap") (r "~2.20") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "1n6bsi469m5iz216nzpzjwgp6vldk71shpjcnk3r1hfcdw3v8wn5")))

(define-public crate-tree-rs-0.6.0 (c (n "tree-rs") (v "0.6.0") (d (list (d (n "clap") (r "~2.20") (d #t) (k 0)) (d (n "globset") (r "^0.1.3") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "1sdm6x761nd84m3v3nnkrjyxknd4lrg1lrwdb1z02znmr0makwgk")))

(define-public crate-tree-rs-0.6.1 (c (n "tree-rs") (v "0.6.1") (d (list (d (n "clap") (r "~2.20") (d #t) (k 0)) (d (n "globset") (r "^0.1.3") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "0ag9r27w7snrq0b94dy7ym8hw5n9vd6anvw154xbggvzrqg5084b")))

(define-public crate-tree-rs-0.6.2 (c (n "tree-rs") (v "0.6.2") (d (list (d (n "clap") (r "~2.20") (d #t) (k 0)) (d (n "globset") (r "^0.1.3") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "0vjbf015rnzg0s04y3x5bmfjlg9xs13k7yv10i8ffrkc67mdrvjf")))

(define-public crate-tree-rs-0.6.3 (c (n "tree-rs") (v "0.6.3") (d (list (d (n "clap") (r "~2.20") (d #t) (k 0)) (d (n "globset") (r "^0.1.3") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "06d6xv7lvyw62b7a6b7jvyvlb825b32bx0ca2d2al4xv9ck85677")))

(define-public crate-tree-rs-0.6.4 (c (n "tree-rs") (v "0.6.4") (d (list (d (n "clap") (r "~2.25") (d #t) (k 0)) (d (n "globset") (r "0.2.*") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)))) (h "1656ljmvs0v546wpcalickp6zfg7b26qv47npc12zpmj3rwf6aml")))

(define-public crate-tree-rs-0.6.5 (c (n "tree-rs") (v "0.6.5") (d (list (d (n "clap") (r "~2.31") (d #t) (k 0)) (d (n "globset") (r "~0.4.0") (d #t) (k 0)) (d (n "term") (r "~0.5.1") (d #t) (k 0)))) (h "1dxrb89x84dnfbz7y845r2pyhna50wp79y7bfp23xyfsfl41gbxm")))

