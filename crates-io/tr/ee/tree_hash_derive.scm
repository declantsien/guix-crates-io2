(define-module (crates-io tr ee tree_hash_derive) #:use-module (crates-io))

(define-public crate-tree_hash_derive-0.1.0 (c (n "tree_hash_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1912dwdmh85fvf40prnbxn3iy8zbpxc1q2978by3qh21n7gabjl1")))

(define-public crate-tree_hash_derive-0.3.0 (c (n "tree_hash_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "0n1xcxcf3qw4bsacwck5d6jdfrk4mmk7raa42phzrl5v23v66dba")))

(define-public crate-tree_hash_derive-0.3.1 (c (n "tree_hash_derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "00vnmsrn2gwprpla405jaj03nbam272m3z42yf5y5ryvg4mzzbfb")))

(define-public crate-tree_hash_derive-0.4.0 (c (n "tree_hash_derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "1b88wshmmwlcd3705i78pwbky40ixyd12ldv6i27m0sph492vliw")))

(define-public crate-tree_hash_derive-1.0.0-beta.0 (c (n "tree_hash_derive") (v "1.0.0-beta.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "0vnj6gangydgaq78f0w7fair2ciz1idsmfj3qi6ia3ivqf8d56xv")))

(define-public crate-tree_hash_derive-0.5.0 (c (n "tree_hash_derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "0xl9bs2qwlaz6jv1hfdw5affs0cf7px2rcya61l291wx890b4n0z")))

(define-public crate-tree_hash_derive-0.5.1 (c (n "tree_hash_derive") (v "0.5.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "12gc9j0a19mrk7dgcp0s61q61qa11zxqsqzpzvjqjs6rjijs5fl3")))

(define-public crate-tree_hash_derive-0.5.2 (c (n "tree_hash_derive") (v "0.5.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "11ms6wsnabi15yq7kmmima44vq4m3qfj9kfr7rd0hpysgjf3lc44")))

(define-public crate-tree_hash_derive-0.6.0 (c (n "tree_hash_derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "1wbzhfbf1sfbacly500xlb1g3nsbh2avrxva8c9s4nc3ag6brrww")))

