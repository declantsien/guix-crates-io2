(define-module (crates-io tr ee tree-sitter-elm) #:use-module (crates-io))

(define-public crate-tree-sitter-elm-4.3.2 (c (n "tree-sitter-elm") (v "4.3.2") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "1mzsp2281z1hs4g3q3bp454bqbznpjrxzyr52m6m1s4qcsxyymv7")))

(define-public crate-tree-sitter-elm-4.3.3 (c (n "tree-sitter-elm") (v "4.3.3") (d (list (d (n "cc") (r ">=1.0.61, <2.0.0") (d #t) (k 1)) (d (n "serde") (r ">=1.0.117, <2.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 2)) (d (n "toml") (r ">=0.5.7, <0.6.0") (d #t) (k 2)) (d (n "tree-sitter") (r ">=0.17.1, <0.18.0") (d #t) (k 0)))) (h "13z8iylfv9m0jy298gmfjw7bcyscx3diwnj4rc6vp3wjyl4ak1dc")))

(define-public crate-tree-sitter-elm-4.3.4 (c (n "tree-sitter-elm") (v "4.3.4") (d (list (d (n "cc") (r ">=1.0.61, <2.0.0") (d #t) (k 1)) (d (n "serde") (r ">=1.0.117, <2.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 2)) (d (n "toml") (r ">=0.5.7, <0.6.0") (d #t) (k 2)) (d (n "tree-sitter") (r ">=0.17.1, <0.18.0") (d #t) (k 0)))) (h "1rd25bvsj03k6kqwvg5ms4rldh6fm4d3majpmfyl2rncbpsf57zx")))

(define-public crate-tree-sitter-elm-4.4.0 (c (n "tree-sitter-elm") (v "4.4.0") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "0w3f62i71czcbrvqkr9fvw1n8q6fyh04fjnds65br4aq4cgvpfp5")))

(define-public crate-tree-sitter-elm-4.4.1 (c (n "tree-sitter-elm") (v "4.4.1") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "0g0hcz7llid31wcpbd9kmm1x354igqsm9qnd1c5y7h290q12mmw6")))

(define-public crate-tree-sitter-elm-4.5.0 (c (n "tree-sitter-elm") (v "4.5.0") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "0v5mahlgc447idk809blvr2k3c9c9x965w5qsfap7rr24aklg00l")))

(define-public crate-tree-sitter-elm-5.0.0 (c (n "tree-sitter-elm") (v "5.0.0") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "16jgynlp0a0n77db0rfdiqd7j3z1bkgmb0zpbc3brv7gkw9md4hh")))

(define-public crate-tree-sitter-elm-5.0.1 (c (n "tree-sitter-elm") (v "5.0.1") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "1128s3shcmrb8pyjn4h1sr0smrn48mvdw74lpyhws5r5yhqdnwck")))

(define-public crate-tree-sitter-elm-5.1.0 (c (n "tree-sitter-elm") (v "5.1.0") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "0dy7bb44l5l5hjg0x8gcrqm3qlv6h3z1ysvmpszjp9mfkh5krg9y")))

(define-public crate-tree-sitter-elm-5.1.1 (c (n "tree-sitter-elm") (v "5.1.1") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "08p3scj85rgsssd5p7sg0xrvr0ylzi8nvdm0va801q8gb0z2yfjz")))

(define-public crate-tree-sitter-elm-5.2.0 (c (n "tree-sitter-elm") (v "5.2.0") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "0n6lxmn7s1aywb3jksnavrlsawrgdngza0nip55xa9jjb2b75hav")))

(define-public crate-tree-sitter-elm-5.2.1 (c (n "tree-sitter-elm") (v "5.2.1") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "0ibm4783325y45gh9kfbw7vjqmqgqrd3nzybbaicmgklsb7k4xd8")))

(define-public crate-tree-sitter-elm-5.2.2 (c (n "tree-sitter-elm") (v "5.2.2") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "1n7nzsprsxa10ib4in1086snqv3dkpjlnrzmn0rjyb4s8p8mjfm2")))

(define-public crate-tree-sitter-elm-5.2.3 (c (n "tree-sitter-elm") (v "5.2.3") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.17.1") (d #t) (k 0)))) (h "17hs5602lm2412ydpqk12s5sc76kg8khiifvqhdlcs56sx5g67hi")))

(define-public crate-tree-sitter-elm-5.3.0 (c (n "tree-sitter-elm") (v "5.3.0") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.19.0") (d #t) (k 0)))) (h "0dxnikrap3va7l2ifdlbkaarxbb8rb5c8zf2wlzikm81smsfi1fg")))

(define-public crate-tree-sitter-elm-5.3.1 (c (n "tree-sitter-elm") (v "5.3.1") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.19.1") (d #t) (k 0)))) (h "1j4h4wmf9b79pvjvas7p067hfccd23pybd03hnrs4rb6jrqlxb15")))

(define-public crate-tree-sitter-elm-5.3.2 (c (n "tree-sitter-elm") (v "5.3.2") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.19.1") (d #t) (k 0)))) (h "08xnhnsc6yqjypwdpdibb7wk54g835yswcvbxvakd6c4h230b1z4")))

(define-public crate-tree-sitter-elm-5.3.3 (c (n "tree-sitter-elm") (v "5.3.3") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "toml") (r "^0.5.7") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.19.1") (d #t) (k 0)))) (h "1sxg7gy7vrzbfv7580apaf574ly04pbxihhf22qbnv2va2zyxgrn")))

(define-public crate-tree-sitter-elm-5.3.4 (c (n "tree-sitter-elm") (v "5.3.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "0dfvr5a06gp0mwczj6xylii59lywprvd0v9gribq58cqysv7ibzk")))

(define-public crate-tree-sitter-elm-5.3.5 (c (n "tree-sitter-elm") (v "5.3.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "1yv310m8rlg6fp3hk98k5kqcwvc9r1b4m0vb6wm9i91zwpkzd7hi")))

(define-public crate-tree-sitter-elm-5.3.6 (c (n "tree-sitter-elm") (v "5.3.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "1hly7m5z398a3j4b8268073pbag7fz8lwv357fcrkz309bqwkq8h")))

(define-public crate-tree-sitter-elm-5.3.7 (c (n "tree-sitter-elm") (v "5.3.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.5") (d #t) (k 0)))) (h "166g0c8bik47sadj1qgz8pjw49ycrqknpc3rc1qa9gz9jjcklq82")))

(define-public crate-tree-sitter-elm-5.4.0 (c (n "tree-sitter-elm") (v "5.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1cgqm4c54mxji6nrjqqi6p793y0m9lqzh950w8h08q6gn0nkznsn")))

(define-public crate-tree-sitter-elm-5.4.1 (c (n "tree-sitter-elm") (v "5.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "13ml371jiyqyxgi3n777ayifmlxx5adwqxqcyr14f0302adfysdk")))

(define-public crate-tree-sitter-elm-5.5.0 (c (n "tree-sitter-elm") (v "5.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0778mg7hn023cl3h6zn93hh1fz7dqy0vwiwciyr0jcc61133yskg")))

(define-public crate-tree-sitter-elm-5.5.1 (c (n "tree-sitter-elm") (v "5.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "0pfdff7w8c7swdv550qjfqjdc09fhjkwsnxvjaygyv35vapwfpsv")))

(define-public crate-tree-sitter-elm-5.6.0 (c (n "tree-sitter-elm") (v "5.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1wyx1a8awx4r7lg2y66365k4wrlzs82iw67blmr0v2nvpfn2hk0x")))

(define-public crate-tree-sitter-elm-5.6.2 (c (n "tree-sitter-elm") (v "5.6.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.8") (d #t) (k 0)))) (h "118vp23w9qafqwxf62n6nivqwspgm8r3m5q43kwwrpbzvj2xpgw4")))

(define-public crate-tree-sitter-elm-5.6.3 (c (n "tree-sitter-elm") (v "5.6.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.8") (d #t) (k 0)))) (h "0fq9jmwa4039iib4baj1d38slzqh9w6g5fij85vjgajhsa541f92")))

(define-public crate-tree-sitter-elm-5.6.4 (c (n "tree-sitter-elm") (v "5.6.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0kmrkw1pf6i544dk93ak762p5innc9bfjdinwz4077f6rzhs50zc")))

(define-public crate-tree-sitter-elm-5.6.5 (c (n "tree-sitter-elm") (v "5.6.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0yzbr7b3p8ljsypkjpabnmb6g33fgfmfdrr347wzrm8wz9an28wm")))

(define-public crate-tree-sitter-elm-5.6.6 (c (n "tree-sitter-elm") (v "5.6.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1aqn6dd2gx70ng4r0wch9jbkavw5az6sjfz57yx3cji5p5vcagyh")))

(define-public crate-tree-sitter-elm-5.7.0 (c (n "tree-sitter-elm") (v "5.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0p83qr3v3wi9ik1vmiyqmvnh7pr7bi8azw9bgzmdk310qbp3drqy")))

