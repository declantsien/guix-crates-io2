(define-module (crates-io tr ee tree-sitter-rush) #:use-module (crates-io))

(define-public crate-tree-sitter-rush-0.1.0 (c (n "tree-sitter-rush") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "1phyp043rrxmb9lzhrbi0wknp3g202rsa0x06w56hqqrp6gxj6v9")))

