(define-module (crates-io tr ee tree-sitter-haskell) #:use-module (crates-io))

(define-public crate-tree-sitter-haskell-0.15.0 (c (n "tree-sitter-haskell") (v "0.15.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1wml8ma57rd33f1j5kgqqa5fs0sm7ywg8c88ph37f4ncss35nqxc")))

(define-public crate-tree-sitter-haskell-0.21.0 (c (n "tree-sitter-haskell") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "16mzz60hgbjjv3ccaqdxf2rqad3vdr0dbnxpq3hcph9wqzkaf9gg")))

