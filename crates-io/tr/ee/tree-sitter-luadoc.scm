(define-module (crates-io tr ee tree-sitter-luadoc) #:use-module (crates-io))

(define-public crate-tree-sitter-luadoc-0.0.1 (c (n "tree-sitter-luadoc") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "1wfjhgsvywicwx8xrl508n70nd0nwmpvw6ifig5n7nfh2yrl4zcy")))

(define-public crate-tree-sitter-luadoc-0.0.2 (c (n "tree-sitter-luadoc") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "15pn322q983xrwdwmn39xhkwfh7g1r51mg60dfvzkdnnb926bc6d")))

(define-public crate-tree-sitter-luadoc-0.0.3 (c (n "tree-sitter-luadoc") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "1hmiyc9z5ykjs0r2rb0jg479b0skmijkyir4kjkmxnrl8q4zj2pg")))

(define-public crate-tree-sitter-luadoc-0.0.4 (c (n "tree-sitter-luadoc") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "1liwba1qnyp2r1wppkz018wlair6nsrajwas3gjifak1969ixw7b")))

(define-public crate-tree-sitter-luadoc-1.0.0 (c (n "tree-sitter-luadoc") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "00inax581kqvw12w8q85c2gbzcz11rarfib4fypqdyzlzxfxpfdf")))

(define-public crate-tree-sitter-luadoc-1.0.1 (c (n "tree-sitter-luadoc") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0ccdj42ill231wgd5rv3hnfhc5k2ynafw1if1anyjns6va2v7d1h")))

(define-public crate-tree-sitter-luadoc-1.1.0 (c (n "tree-sitter-luadoc") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1kgw4a5wcvhk5wi5v3szdh9gzr0wbg96wfj6cdnkjh1rq6d5bg6g")))

