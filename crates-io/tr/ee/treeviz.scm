(define-module (crates-io tr ee treeviz) #:use-module (crates-io))

(define-public crate-treeviz-0.1.0 (c (n "treeviz") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1xnyc6qkjcv2fw0yg0l01q7lksgbg6a999p87d4s8dj46vc1laff")))

