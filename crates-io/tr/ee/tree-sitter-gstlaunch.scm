(define-module (crates-io tr ee tree-sitter-gstlaunch) #:use-module (crates-io))

(define-public crate-tree-sitter-gstlaunch-0.1.0 (c (n "tree-sitter-gstlaunch") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1gswsqrphh7pz3k4447mgr4jvd0y2i7xlp1vbh0cnpf9j7p33s9m")))

