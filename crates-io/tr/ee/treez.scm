(define-module (crates-io tr ee treez) #:use-module (crates-io))

(define-public crate-treez-0.1.0 (c (n "treez") (v "0.1.0") (h "0fzzqknms5krk0kp3m2dfaq0f34vgrx4v46rajsiryrxzz7lf42f")))

(define-public crate-treez-0.1.1 (c (n "treez") (v "0.1.1") (h "0hdxyq7var1ip7xhrbm6pna0zgnh81bw6wrmjlkpgrz323a8w7hd")))

(define-public crate-treez-0.1.2 (c (n "treez") (v "0.1.2") (h "15zcq45r1g3zr6g7b3hah0fjiiq50k26k4bkw03xhsa8s694n0zd")))

(define-public crate-treez-0.1.4 (c (n "treez") (v "0.1.4") (h "0ksf3li0s9wr2wdap4ys8xq4pg0m6zg1wnhvp76abyiv5q6n4z0j")))

(define-public crate-treez-0.1.5 (c (n "treez") (v "0.1.5") (h "0vgin5xdsacji63bvsvw7ccz6f0m8l7073kh9lcryksi98q8kln8")))

(define-public crate-treez-0.2.1 (c (n "treez") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "19ns70v5da6zb9lzq21vky0d47k959md2kil06cq968zfvfavmgz")))

(define-public crate-treez-0.4.0 (c (n "treez") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1n3vqzgiv582pj1xdfdjhn0y05wpai9ml46yq0yhyaayvq981bkf")))

(define-public crate-treez-0.4.1 (c (n "treez") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "010yxpyqfnc0nm61p865lkyfhgf9v52vw5x4p86amdnimgk0230z")))

(define-public crate-treez-0.4.2 (c (n "treez") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1bz1yrdn4argrxmbi70giyd1awbik3ycp2am4ak1wh3p2l3dzpia")))

(define-public crate-treez-0.5.0 (c (n "treez") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "119xyhw3agc0r9h4qj8kzd65prxa7lcqlda1yxmyimkraz361w23")))

(define-public crate-treez-0.6.0 (c (n "treez") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1jjfivaxycnnqw17bb4js743qr3wpl9s55flf9bq0mqp456762s7")))

(define-public crate-treez-0.7.0 (c (n "treez") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "16zlc945khazi9dpnq33xn1q5xb1jd6pfqfda1b55vy63izji52y")))

(define-public crate-treez-0.7.1 (c (n "treez") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1hgr1c9h20vz3zj30zlq4p5x475rhm69jb8hmwrgpxkls738fx8r")))

(define-public crate-treez-0.7.2 (c (n "treez") (v "0.7.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0a5d64a5l1nb1fr24skf2y20wlr976pmpldb5fqv0mdbasmrz7q2")))

(define-public crate-treez-0.8.0 (c (n "treez") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ssy77w3bxf647242ns3wck2pvizs9n5w0mpwx6qddkraadqfdqm")))

(define-public crate-treez-0.9.0 (c (n "treez") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1i7b63q491vivswp26y5inag89cwpbab670dp0rlgggv8vijycqf")))

(define-public crate-treez-0.9.1 (c (n "treez") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1am2n1bdxa5w4n84r9fgl58mbbwlym2dz07v23861ar2l0hcr1kx")))

(define-public crate-treez-0.10.0 (c (n "treez") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0jd2s699f2357sfl2wgv06pkdm2qpzyba3cldggr9awxjrnlp3zq")))

(define-public crate-treez-0.11.0 (c (n "treez") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "104plysml0kcjmsfydyhi34h72abpqis84yaqf6n2swjaplx0wjr")))

(define-public crate-treez-0.12.0 (c (n "treez") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "mazth") (r "^0.0.0") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1g9bn2cmjn7akb0z23z0p0vk8si0d69i2jvcxl9s59bairaf0b5z")))

(define-public crate-treez-1.0.0 (c (n "treez") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "mazth") (r "^0.0.0") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1461cp3495p42ix7l78y96qdcmbcb7hnxr3is2di1q7f0xcfblnj")))

(define-public crate-treez-1.0.1 (c (n "treez") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "mazth") (r "^0.0.0") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "17m7plwggnhz52v8r01rygvlwgdk4hrs4c9k05vyg7d52nxhwas7")))

(define-public crate-treez-1.0.2 (c (n "treez") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "mazth") (r "^0.0.0") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1alxj859gn38ihszlrfa9ilcshhzszg9qy15c10r75iwldphq3nm")))

(define-public crate-treez-1.1.0 (c (n "treez") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "mazth") (r "^0.0.0") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "05inrpgz70pg57kdajl14q9j87rpl4d88hh7s7spl0lxl4lwgzsn")))

(define-public crate-treez-1.4.0 (c (n "treez") (v "1.4.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "mazth") (r "^0.0.0") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "030qarjahbwnfhf5w6zb5jhcc91q20m494f3ah4gvzzrzli0g3md")))

(define-public crate-treez-1.4.1 (c (n "treez") (v "1.4.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "mazth") (r "^0.0.0") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1iqm8hbix9szhmyk6x1dd4c1b0bljixdwkaivk6j60kzwa4hrwk7")))

(define-public crate-treez-1.5.0 (c (n "treez") (v "1.5.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "mazth") (r "^0.0.0") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "04dwgvljj20s6aqi6y3y27l12jxgh2izxpag0fqpczjffiqbra9s")))

(define-public crate-treez-1.6.0 (c (n "treez") (v "1.6.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "mazth") (r "^0.0.0") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "181hjls2gvmjca5cab3946wb70rcb8z35clfrbffm7r2rwqsl32v")))

