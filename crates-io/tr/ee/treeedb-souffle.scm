(define-module (crates-io tr ee treeedb-souffle) #:use-module (crates-io))

(define-public crate-treeedb-souffle-0.1.0-rc.5 (c (n "treeedb-souffle") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-souffle") (r "^0.4.0") (d #t) (k 0)) (d (n "treeedb") (r "^0.1.0-rc.5") (f (quote ("cli"))) (d #t) (k 0)))) (h "12k2hxdrdkqb6nkmvwky9qm2ah8ffz47p70jbb7xr5j5rbwd43ln")))

