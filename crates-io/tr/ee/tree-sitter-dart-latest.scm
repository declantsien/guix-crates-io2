(define-module (crates-io tr ee tree-sitter-dart-latest) #:use-module (crates-io))

(define-public crate-tree-sitter-dart-latest-0.0.1 (c (n "tree-sitter-dart-latest") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.17") (d #t) (k 0)))) (h "1rbjw36dm4w3lxj0by28w0xqs492wch7ihvpzzrr1ifi8c2gv2mr")))

