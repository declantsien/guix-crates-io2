(define-module (crates-io tr ee treestats) #:use-module (crates-io))

(define-public crate-treestats-0.1.0 (c (n "treestats") (v "0.1.0") (d (list (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "15c4qnwmh5slqlzhwi1m4c9xik4vwiavsqb6hm0khnvlx5cl61dg")))

