(define-module (crates-io tr ee tree-sitter-whitespace) #:use-module (crates-io))

(define-public crate-tree-sitter-whitespace-0.0.1 (c (n "tree-sitter-whitespace") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1mik83kssv056gr5ks03n5l0465w944rwmi2grl9nmi921n7b9x4")))

