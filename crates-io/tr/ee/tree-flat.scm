(define-module (crates-io tr ee tree-flat) #:use-module (crates-io))

(define-public crate-tree-flat-0.1.0 (c (n "tree-flat") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("cargo_bench_support" "html_reports"))) (k 2)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "128pviqikvhy0szf19nfivsyvk5csfy142w122pwvxw6x7ps3xn7")))

(define-public crate-tree-flat-0.1.1 (c (n "tree-flat") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("cargo_bench_support" "html_reports"))) (k 2)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "13xrhn70jl6dvla85ij8674ymc0pwgp941pmb7snmkigd4lckj9h")))

(define-public crate-tree-flat-0.1.2 (c (n "tree-flat") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("cargo_bench_support" "plotters" "html_reports"))) (k 2)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1hkkain06ycimb3z2w36glqkfqmw1dpi6a6b2xc6fhg0g881wbj0")))

(define-public crate-tree-flat-0.1.3 (c (n "tree-flat") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("cargo_bench_support" "plotters" "html_reports"))) (k 2)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1w6j681xsvrfh59i0nkg94plnyixd2vcc5hl51j1rwm8ar3x3bpx")))

