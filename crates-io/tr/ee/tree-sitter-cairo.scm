(define-module (crates-io tr ee tree-sitter-cairo) #:use-module (crates-io))

(define-public crate-tree-sitter-cairo-0.0.1 (c (n "tree-sitter-cairo") (v "0.0.1") (d (list (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1wa6nqz20i9q579lbq12flkr5sxm1qriiwwvja8g37plf9lrfyh3")))

