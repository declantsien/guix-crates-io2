(define-module (crates-io tr ee tree-crasher-typescript) #:use-module (crates-io))

(define-public crate-tree-crasher-typescript-0.2.0 (c (n "tree-crasher-typescript") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.0") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)))) (h "196irp3j4i9flip0shrc4xqhijviavnc6fqihsnjvakjqw3r1v9w")))

(define-public crate-tree-crasher-typescript-0.2.1 (c (n "tree-crasher-typescript") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.1") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)))) (h "07cv3gbx9ggjm3izpg804ndn655h9fh1lrq3n8i9szlmf7nf6275")))

(define-public crate-tree-crasher-typescript-0.2.2 (c (n "tree-crasher-typescript") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.2") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)))) (h "0c4x032wszgyfxdl565wmybmjqxcrjjwfqqw2dwddwawqzd76084")))

(define-public crate-tree-crasher-typescript-0.2.3 (c (n "tree-crasher-typescript") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.2.3") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)))) (h "1h4m9hajnxjwrdkf1hvbchn813md3240hgccx9gr2ax4njvx3bir")))

(define-public crate-tree-crasher-typescript-0.3.0 (c (n "tree-crasher-typescript") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.3.0") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)))) (h "1wpm4hz0qbfm61avcjy0hvzcg3s7lnkqsnjiylh5bpcg11yzmqz9")))

(define-public crate-tree-crasher-typescript-0.4.0 (c (n "tree-crasher-typescript") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-crasher") (r "^0.4.0") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)))) (h "0fmfyj1v2p8d1ypjh94vy8xnm2806m40np63prrkr9qxq34w2h8j")))

