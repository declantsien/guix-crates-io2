(define-module (crates-io tr ee tree-sitter-bass) #:use-module (crates-io))

(define-public crate-tree-sitter-bass-0.0.1 (c (n "tree-sitter-bass") (v "0.0.1") (d (list (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0krvrn19px7sy9lfym36azbn4i5jjndyzi3cvr20aw98llj6dinj")))

(define-public crate-tree-sitter-bass-0.0.2 (c (n "tree-sitter-bass") (v "0.0.2") (d (list (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0gpxwinb406df7bpzs51b97cpg3f0zv3qkgvfa6zw9v4s9ard5xk")))

