(define-module (crates-io tr ee tree-sitter-elixir) #:use-module (crates-io))

(define-public crate-tree-sitter-elixir-0.1.0 (c (n "tree-sitter-elixir") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "17j6kl7zi6grghs9nj4kp2whqwp8jx7610l5mf53q2y8w7rid6cs")))

(define-public crate-tree-sitter-elixir-0.1.1 (c (n "tree-sitter-elixir") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0ns1664w51gpbv86bxfz57m3lkhmzlr1y5z55ai2rwfrwvrv3h0v")))

(define-public crate-tree-sitter-elixir-0.2.0 (c (n "tree-sitter-elixir") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "0w7gniypkapf8d02a2adw3wk7bnl5s0i87zfnb5b2s3p0mzvz56z")))

