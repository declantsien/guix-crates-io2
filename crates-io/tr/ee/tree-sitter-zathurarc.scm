(define-module (crates-io tr ee tree-sitter-zathurarc) #:use-module (crates-io))

(define-public crate-tree-sitter-zathurarc-0.0.1 (c (n "tree-sitter-zathurarc") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "138p8szm74jw1fp7ji1bf4qflqghsyqwrfm2643jrm47q0qywhhp")))

(define-public crate-tree-sitter-zathurarc-0.0.2 (c (n "tree-sitter-zathurarc") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0apfalxyzlzmjbspq54vmr37hh7k4jv25ka1dl1izk8w7a023v5b")))

(define-public crate-tree-sitter-zathurarc-0.0.3 (c (n "tree-sitter-zathurarc") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1kisxjcg75spqy16bkf7v6v65zcfm06jzaap6dqigbvm7pymvwi6")))

(define-public crate-tree-sitter-zathurarc-0.0.4 (c (n "tree-sitter-zathurarc") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1yf8p0nv7kn7fhwj0ah19q7h94jw5bbrkjzd058pknfkcbl42xiy")))

(define-public crate-tree-sitter-zathurarc-0.0.5 (c (n "tree-sitter-zathurarc") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1jbc6b221rpncd3pxy6wq2jsg2n0aazqij5zv506j2a63mdmhw9i")))

(define-public crate-tree-sitter-zathurarc-0.0.6 (c (n "tree-sitter-zathurarc") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1hh06qbvl5nms0ccijvz8p5q2m4mpk3b0q5x3cc8k3iyzq9bf4zp")))

(define-public crate-tree-sitter-zathurarc-0.0.7 (c (n "tree-sitter-zathurarc") (v "0.0.7") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.2") (d #t) (k 0)))) (h "08zw0c39vifxc09pf9pzg5z34jx2878zvfqb346z042lmaqlci1m")))

(define-public crate-tree-sitter-zathurarc-0.0.8 (c (n "tree-sitter-zathurarc") (v "0.0.8") (d (list (d (n "cc") (r "^1.0.87") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22.2") (d #t) (k 0)))) (h "10bijrmnkdc2hp49m9f85752n6il6jzwhdall025a78nxb21d5n4")))

