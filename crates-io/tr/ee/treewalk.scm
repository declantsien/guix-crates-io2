(define-module (crates-io tr ee treewalk) #:use-module (crates-io))

(define-public crate-treewalk-0.1.0 (c (n "treewalk") (v "0.1.0") (h "1zhm4vg3ik9phasa91v9yrl6s1bq3l9c56qb76ilf8n15qzfk20g") (y #t)))

(define-public crate-treewalk-0.1.1 (c (n "treewalk") (v "0.1.1") (h "1fgxb0n3jz41b89igjpsq1inavks568h1ibmk2w485dd4d0rcfzb") (y #t)))

(define-public crate-treewalk-0.1.2 (c (n "treewalk") (v "0.1.2") (h "0binsgfibpslszvrzflbrp5kfxag7cvzazq1p2l8a833dax7rr7l") (y #t)))

(define-public crate-treewalk-0.1.3 (c (n "treewalk") (v "0.1.3") (h "0wa4qbk5p34gcx5im25k649m1slqg44yqbibckgr3rc32arwac6p") (y #t)))

(define-public crate-treewalk-0.1.4 (c (n "treewalk") (v "0.1.4") (h "141k9j39rx26i5s2lwd17517z35c7g08wlqyh8z9y59yp0z8bp8g")))

