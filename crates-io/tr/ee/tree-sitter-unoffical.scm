(define-module (crates-io tr ee tree-sitter-unoffical) #:use-module (crates-io))

(define-public crate-tree-sitter-unoffical-0.0.1 (c (n "tree-sitter-unoffical") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12ra6r81v0m90ng6rsn55i3kg5bzalhbp6z4rmbfkna68l001dsm") (y #t) (r "1.65")))

