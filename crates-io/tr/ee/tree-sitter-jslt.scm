(define-module (crates-io tr ee tree-sitter-jslt) #:use-module (crates-io))

(define-public crate-tree-sitter-jslt-0.1.0 (c (n "tree-sitter-jslt") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0f4zdr48vmvjqdcqnnlpdmmlgnf0acmjzj1xccq49aljnhrkv42x")))

(define-public crate-tree-sitter-jslt-0.1.1 (c (n "tree-sitter-jslt") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1cq94ss2f92nizzx2zr0fzvqakwcjas99s07cwvhq32131mwd37i")))

