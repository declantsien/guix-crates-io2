(define-module (crates-io tr ee tree-sitter-embedded-template) #:use-module (crates-io))

(define-public crate-tree-sitter-embedded-template-0.19.0 (c (n "tree-sitter-embedded-template") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "14vda7yh8n21hhw3ddjxgxwkvgnrxmxsrr4h31fphwczngiy2gqr")))

(define-public crate-tree-sitter-embedded-template-0.20.0 (c (n "tree-sitter-embedded-template") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.20") (d #t) (k 0)))) (h "0f6c1nrqlgsb7dpi07dp4kyfk7g044in0jlh9wns6wwcjbg7m09k")))

(define-public crate-tree-sitter-embedded-template-0.21.0 (c (n "tree-sitter-embedded-template") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1yxhmn3mrcv4mx1rbf66pivchbb6qzlrwcg05nl5klry4wmvw85r")))

