(define-module (crates-io tr ee tree-sitter-devicetree) #:use-module (crates-io))

(define-public crate-tree-sitter-devicetree-0.10.0 (c (n "tree-sitter-devicetree") (v "0.10.0") (d (list (d (n "cc") (r "^1.0.89") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1k4ckjvl314jsf01lm8vhxn2rf4rgcazjxvyhr0c0zf12hjd30hb")))

