(define-module (crates-io tr ee tree-sitter-java) #:use-module (crates-io))

(define-public crate-tree-sitter-java-0.16.0 (c (n "tree-sitter-java") (v "0.16.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.17") (d #t) (k 0)))) (h "0ikdhjzlspaipyi9f1djxrd0k1cpcslssd52l4sr4n36ichif60a")))

(define-public crate-tree-sitter-java-0.19.0 (c (n "tree-sitter-java") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "0c2lim4324dkw4qq91x8prdn8014ci3vjvf0bn9vzq8kg3pf46ih")))

(define-public crate-tree-sitter-java-0.20.0 (c (n "tree-sitter-java") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0814dp25cmarkjnq88bcidx2j90d3h838n40w6zzrgwca0zmvgzh")))

(define-public crate-tree-sitter-java-0.20.2 (c (n "tree-sitter-java") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19, <0.21") (d #t) (k 0)))) (h "0lr68vpj1ryp7zv18hjgjql3n3idp0mpsifph487dgsspyb5dp1a")))

(define-public crate-tree-sitter-java-0.21.0 (c (n "tree-sitter-java") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.91") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "06s89gdz7ilyzm3pnq57drm5x5j3mc3h3l6rfp0779riz2nj3g1k")))

