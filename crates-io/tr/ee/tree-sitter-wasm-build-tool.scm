(define-module (crates-io tr ee tree-sitter-wasm-build-tool) #:use-module (crates-io))

(define-public crate-tree-sitter-wasm-build-tool-0.1.0 (c (n "tree-sitter-wasm-build-tool") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "14z9b8sz43vix8p1hvag1lp4cr4vm0c89a0hb2v866yrcacr6i6w")))

(define-public crate-tree-sitter-wasm-build-tool-0.2.0 (c (n "tree-sitter-wasm-build-tool") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "0wni2nvnm5ws83ssz98s69ymr5xizx78apb68qlcqc9qv2dvv4la")))

(define-public crate-tree-sitter-wasm-build-tool-0.2.1 (c (n "tree-sitter-wasm-build-tool") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "0vg19qk06mz39dhp5vi7l5m0aa9spakgd7hxxw6p4mchrdr57gdm")))

(define-public crate-tree-sitter-wasm-build-tool-0.2.2 (c (n "tree-sitter-wasm-build-tool") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "048si5c6sskp6icfan4s9p61p26w6kv9j1w6m4a9mg2y02qqr5d6")))

