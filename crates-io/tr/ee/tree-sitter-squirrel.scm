(define-module (crates-io tr ee tree-sitter-squirrel) #:use-module (crates-io))

(define-public crate-tree-sitter-squirrel-0.0.1 (c (n "tree-sitter-squirrel") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "11lrz645kr1h6rc6vqk5w13flpq9cr5b7dja7f5shlzaidpjblma")))

(define-public crate-tree-sitter-squirrel-0.0.2 (c (n "tree-sitter-squirrel") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "0a6b4rrj9lkw86yxa20i6w35j0wjybpcn40b9m8as5i7df9chxcc")))

(define-public crate-tree-sitter-squirrel-1.0.0 (c (n "tree-sitter-squirrel") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "19fmrnxc8l90kfvj7qnrnw30vyk6sdnzq8c8xkwxm0rwx9dnpwwa")))

