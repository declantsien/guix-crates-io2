(define-module (crates-io tr ee treesum) #:use-module (crates-io))

(define-public crate-treesum-0.1.0 (c (n "treesum") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "parallel-iterator") (r "^0.1.2") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "197m2xbq6fcwicm0qnnrpzvvaj88gyr0a4rg33xndkpi227951g8")))

(define-public crate-treesum-0.1.1 (c (n "treesum") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "parallel-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "0iybi5v5ld7j79ls14dz2bc9ln7vzwppz96dps17myrjhp4drbkm")))

(define-public crate-treesum-0.1.2 (c (n "treesum") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "parallel-iterator") (r "^0.1.5") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1iba7k0namk8qr5rbvjk0g10jrz3kb1fa59klf6g3v3k0ysmqq3k")))

