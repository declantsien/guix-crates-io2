(define-module (crates-io tr ee tree-sitter-glsl) #:use-module (crates-io))

(define-public crate-tree-sitter-glsl-0.1.0 (c (n "tree-sitter-glsl") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "0l52zmi49ybs1gynh6w2y1m63v1fik42hvvfgbj8c7qh7m8f28x5")))

(define-public crate-tree-sitter-glsl-0.1.1 (c (n "tree-sitter-glsl") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "1bhp9ng6bgib9fdjwk54pai702vp5llvrcyi3bk8p97qsn3pcxm8")))

(define-public crate-tree-sitter-glsl-0.1.2 (c (n "tree-sitter-glsl") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "1yzhy5473jd62dw7gcc638iwc2l7d5kb1zqqcvvc90n768scj81x")))

(define-public crate-tree-sitter-glsl-0.1.3 (c (n "tree-sitter-glsl") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "14dls8kricryl6z1infbpz31bidcayx89m648lcdshxdrk896vph")))

(define-public crate-tree-sitter-glsl-0.1.4 (c (n "tree-sitter-glsl") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "1fz8c1ff4zq0md9p9jm1c2xzlv8ppy4v9jvxsbvza4k6506hjha7")))

(define-public crate-tree-sitter-glsl-0.1.5 (c (n "tree-sitter-glsl") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1q8vsdb1yfan4mncd99hjxmkznz4xr5aq13swkzxw8dj7g04p8bn")))

(define-public crate-tree-sitter-glsl-0.1.6 (c (n "tree-sitter-glsl") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "06c1z120f02h9hna9dcd9yn4dsl2pjrfm53m3xqlg5zhbws3cl7v")))

(define-public crate-tree-sitter-glsl-0.1.7 (c (n "tree-sitter-glsl") (v "0.1.7") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "06adnzlfh8pylwqrrxnlm8vz2pd90d1zbwdhbgwz73fdmxk3w9b5")))

(define-public crate-tree-sitter-glsl-0.1.8 (c (n "tree-sitter-glsl") (v "0.1.8") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1bfdj09p9jrl84hrmc22z3pc05yilgz2cxvqdyfkizdwh71847xn")))

