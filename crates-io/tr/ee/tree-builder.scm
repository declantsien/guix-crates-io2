(define-module (crates-io tr ee tree-builder) #:use-module (crates-io))

(define-public crate-tree-builder-0.0.1 (c (n "tree-builder") (v "0.0.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "tree-builder-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ksrfhr3z431rx04n6vlc94w4rfzrwpkll411kypq6am36ya5zl6")))

(define-public crate-tree-builder-0.0.2 (c (n "tree-builder") (v "0.0.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "tree-builder-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (f (quote ("diff"))) (d #t) (k 2)))) (h "05aligq20xbwcmbpbyay6bnxkrkxfn131yn8wfciixxc5ch1jk41") (y #t)))

(define-public crate-tree-builder-0.0.3 (c (n "tree-builder") (v "0.0.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "tree-builder-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (f (quote ("diff"))) (d #t) (k 2)))) (h "0s7i5nhbfqg7jzd7xdpxy6vclz6rk3mr8qvidlpdg06imw11d9zs")))

