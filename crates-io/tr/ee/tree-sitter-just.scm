(define-module (crates-io tr ee tree-sitter-just) #:use-module (crates-io))

(define-public crate-tree-sitter-just-0.0.1 (c (n "tree-sitter-just") (v "0.0.1") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1qxsghm4s7izak79z661nfsbqmmx2ryn28rn691ngl3ni95ghzxk")))

