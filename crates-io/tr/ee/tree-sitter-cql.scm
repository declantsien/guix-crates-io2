(define-module (crates-io tr ee tree-sitter-cql) #:use-module (crates-io))

(define-public crate-tree-sitter-cql-0.0.1 (c (n "tree-sitter-cql") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (f (quote ("async_tokio" "html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1d14s7szg4lyaimvg86p50bqsp8r4mz0n1lj0nvq57iqy0xxgp1g")))

(define-public crate-tree-sitter-cql-0.1.0 (c (n "tree-sitter-cql") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (f (quote ("async_tokio" "html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "17xr2w6xldcxypsp936j2pin62jk48ki8rdjxqbmgjxzabdqip33")))

(define-public crate-tree-sitter-cql-0.1.1 (c (n "tree-sitter-cql") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (f (quote ("async_tokio" "html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1fyj7xg4b0dp751vsy9s2kaayi8vrw1sf6j2h348lwz9zkajay24")))

(define-public crate-tree-sitter-cql-0.1.2 (c (n "tree-sitter-cql") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (f (quote ("async_tokio" "html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.6") (d #t) (k 0)))) (h "0r958zgnv760007j9jkfhqz6vswmbhfpdg5fw9xpbpjqjwm1ck56") (y #t)))

(define-public crate-tree-sitter-cql-0.2.0 (c (n "tree-sitter-cql") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (f (quote ("async_tokio" "html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.6") (d #t) (k 0)))) (h "1ldnm9djs6nq8kq3511sbk4sk19xiv3vl0xrapz614hrbwkz7z9r")))

