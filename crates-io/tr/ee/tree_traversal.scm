(define-module (crates-io tr ee tree_traversal) #:use-module (crates-io))

(define-public crate-tree_traversal-0.1.0 (c (n "tree_traversal") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1hdmp1qhm5v7w38an138vrpn9fgy5z1c8baxqkjrnc5d2q3qvv38")))

(define-public crate-tree_traversal-0.1.1 (c (n "tree_traversal") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1y5l2289xq8jnjbz4yyyip28f6h9d6d5dsbfcsrdcm20zhakqmzs")))

(define-public crate-tree_traversal-0.2.0 (c (n "tree_traversal") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0020xk5j27gii9bp7zmb2h8ww85nqwyhvd3wycnw4jprdi0g25sc")))

