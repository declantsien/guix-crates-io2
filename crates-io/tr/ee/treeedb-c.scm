(define-module (crates-io tr ee treeedb-c) #:use-module (crates-io))

(define-public crate-treeedb-c-0.1.0-rc.5 (c (n "treeedb-c") (v "0.1.0-rc.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)) (d (n "treeedb") (r "^0.1.0-rc.5") (f (quote ("cli"))) (d #t) (k 0)))) (h "103y0lgcnbcp4dbdxdbbxy766c83x3s6r13zsizy6rsqqqsqphyj")))

