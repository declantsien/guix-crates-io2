(define-module (crates-io tr ee tree-sitter-applesoft) #:use-module (crates-io))

(define-public crate-tree-sitter-applesoft-0.0.1 (c (n "tree-sitter-applesoft") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1w7hh3nzicawd635mnqrynznhbhmkcv42zvw4hvzbka72lzfzvhd")))

(define-public crate-tree-sitter-applesoft-0.0.2 (c (n "tree-sitter-applesoft") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0igmsf798laplywvpwhxsqhhsc44xaxgnz3w7fiadm59yi3azx14")))

(define-public crate-tree-sitter-applesoft-0.1.0 (c (n "tree-sitter-applesoft") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0z5as46v8zmh9vz77169xg54ppswgyp8razxwh0a8nm8qamz4ixa")))

(define-public crate-tree-sitter-applesoft-1.2.0 (c (n "tree-sitter-applesoft") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "1w982yc4l2w3hipwfz2y4vkj7nwghdsrnhfp0a67dpqx11s5cwf4")))

(define-public crate-tree-sitter-applesoft-1.2.1 (c (n "tree-sitter-applesoft") (v "1.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.0") (d #t) (k 0)))) (h "0kp6q6v89apbhk4il9x2j92db45s6vy7smqdgj51pmpirjf2z713")))

(define-public crate-tree-sitter-applesoft-1.2.2 (c (n "tree-sitter-applesoft") (v "1.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.6") (d #t) (k 0)))) (h "1wrzfz2zj96494z00jq9ml0hx3x3jl7i16fbksidds7dx9a7n9kz")))

(define-public crate-tree-sitter-applesoft-2.0.0 (c (n "tree-sitter-applesoft") (v "2.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.6") (d #t) (k 0)))) (h "0mi5wr5z1qmrljfw0gfqm11gpvjbm7q11gmq6x1ddarbbmdw008y")))

(define-public crate-tree-sitter-applesoft-2.0.1 (c (n "tree-sitter-applesoft") (v "2.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.6") (d #t) (k 0)))) (h "1fcdpv6344wwgr5n7xkd9nlwpdzbv0mvkn0jxzqn4g08zyg079x0")))

(define-public crate-tree-sitter-applesoft-2.0.2 (c (n "tree-sitter-applesoft") (v "2.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.6") (d #t) (k 0)))) (h "1130fin21d83lacz20w0i04jmljpn6x95ih1848s1hab49qbnwm5")))

(define-public crate-tree-sitter-applesoft-2.0.3 (c (n "tree-sitter-applesoft") (v "2.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.6") (d #t) (k 0)))) (h "0ycjpd9612fw2njmr4k6sjb1ija73spb55p02is2rbbc8ldapvi1")))

(define-public crate-tree-sitter-applesoft-2.0.4 (c (n "tree-sitter-applesoft") (v "2.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.7") (d #t) (k 0)))) (h "0yaf5h5ba2jfmsjgg5dgm9fj1b9jnhch2pzp8in21qdmann34f8s")))

(define-public crate-tree-sitter-applesoft-3.0.0 (c (n "tree-sitter-applesoft") (v "3.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.7") (d #t) (k 0)))) (h "19hn0yf02r8cz3i0893my13ys8ckvwcshkcngbiarmmn99rkn30z")))

(define-public crate-tree-sitter-applesoft-3.1.0 (c (n "tree-sitter-applesoft") (v "3.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "0izm56dnak9572kfmcqn0p2zwsz6zgkjl3ib1iblkjbi896zynzx")))

(define-public crate-tree-sitter-applesoft-3.1.1 (c (n "tree-sitter-applesoft") (v "3.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.8") (d #t) (k 0)))) (h "1n9gn68v8sz0cg39pgxrsw9b1bylr2fwgin0wm3wng927q990ndf")))

(define-public crate-tree-sitter-applesoft-3.2.0 (c (n "tree-sitter-applesoft") (v "3.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.4") (d #t) (k 0)))) (h "0lkb7cdrmfkg9bpl2g089825miv8hg40z5h6ka48ch2368pxpf1p")))

(define-public crate-tree-sitter-applesoft-3.2.1 (c (n "tree-sitter-applesoft") (v "3.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.22.4") (d #t) (k 0)))) (h "08r9g2j0xz9m638ccd3xzhhprkb9srkhn0mmav2jg880v9snvsk2")))

