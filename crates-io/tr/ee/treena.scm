(define-module (crates-io tr ee treena) #:use-module (crates-io))

(define-public crate-treena-0.0.1 (c (n "treena") (v "0.0.1") (h "0c1cbiw9dmx1s2h1nf0bnrwszbppnyqpv89a5428d0aqpx2hl0cg") (f (quote (("std") ("default" "std") ("debug-print"))))))

(define-public crate-treena-0.0.2 (c (n "treena") (v "0.0.2") (h "1g16rzzw2lyk9x3pf40mf9f89k10n0jcyxzqhl4psjs7gybnyh5z") (f (quote (("std") ("default" "std") ("debug-print"))))))

(define-public crate-treena-0.0.3 (c (n "treena") (v "0.0.3") (h "0jgi7nghrhvchz60a312zln3cvpib8fk4vkf67y4a8b520k17n4q") (f (quote (("std") ("default" "std") ("debug-print")))) (r "1.56")))

(define-public crate-treena-0.0.4 (c (n "treena") (v "0.0.4") (h "1m17sjvhxqf778676rlf75imwzjj7fjc1sps24n1hw9ag404my7x") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-treena-0.0.5 (c (n "treena") (v "0.0.5") (h "1kwxwsk23329xdfqk66gy1ll81r5yq1nw86zfdrw8ai9sh7q8wna") (f (quote (("std") ("default" "std")))) (r "1.56")))

