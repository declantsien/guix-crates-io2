(define-module (crates-io tr ee tree-sitter-lox) #:use-module (crates-io))

(define-public crate-tree-sitter-lox-0.0.0 (c (n "tree-sitter-lox") (v "0.0.0") (h "0rbq9f0dzhy6hqfbs7nbkk51ajhpid5da7vjr1yl7wd4n0y7dqmq") (y #t)))

(define-public crate-tree-sitter-lox-0.1.0 (c (n "tree-sitter-lox") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.4") (d #t) (k 0)))) (h "0drz2lc3ridijrclqx08187kc7c5rjyh1yzkjlz487sw9mzd58kk")))

