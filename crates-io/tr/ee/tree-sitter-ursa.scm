(define-module (crates-io tr ee tree-sitter-ursa) #:use-module (crates-io))

(define-public crate-tree-sitter-ursa-0.0.1 (c (n "tree-sitter-ursa") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "08mzlfwhyqcf6q6aczf4ijzg7cglsjkq4bwyx1ddanv1ia443511")))

(define-public crate-tree-sitter-ursa-0.0.2 (c (n "tree-sitter-ursa") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "11k3f6m9amy6ii6yba6ik6abqazw130lrcdjq5nvnr3gs9lkf29i")))

(define-public crate-tree-sitter-ursa-0.0.3 (c (n "tree-sitter-ursa") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0xzqnlm3hn00zfix124cndlakjnywd0zg9rfbzph4x7b30nj172d")))

(define-public crate-tree-sitter-ursa-1.0.1 (c (n "tree-sitter-ursa") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0x4c4n6j96kzjkllliggkg13f29fprifd869qb0i9axgllrh4ccr")))

(define-public crate-tree-sitter-ursa-1.0.2 (c (n "tree-sitter-ursa") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1dscfyqvrxyh2nzyfd6s2rc3gbr083r2blvymymhzw7xn2fmwb8g")))

(define-public crate-tree-sitter-ursa-1.0.3 (c (n "tree-sitter-ursa") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "01r5rz1a7pa14vxbknzz1c7969sa66b9rcax3yjfzd6abzha1imp")))

(define-public crate-tree-sitter-ursa-1.0.4 (c (n "tree-sitter-ursa") (v "1.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0gl1m78hrjv12rmmxg1z1c7d1n37i0i219r07ls4gasd0qrl07ym")))

(define-public crate-tree-sitter-ursa-1.0.5 (c (n "tree-sitter-ursa") (v "1.0.5") (d (list (d (n "cargo-edit") (r "^0.12.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1gqgfk1xaizzpig39s14jad88qnv878b41ljw7r4hyzard49qqq9")))

(define-public crate-tree-sitter-ursa-1.0.6 (c (n "tree-sitter-ursa") (v "1.0.6") (d (list (d (n "cargo-edit") (r "^0.12.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0d6wnqk60k0s5k5h6walj2drrkg9mmjhxzmz4gpw3p4qzr5a80gj")))

(define-public crate-tree-sitter-ursa-1.0.7 (c (n "tree-sitter-ursa") (v "1.0.7") (d (list (d (n "cargo-edit") (r "^0.12.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1vhici8k34iqrsk7vkfd62nn0yyxpgbjxp6a9jp2yhs0iyp9mg3m")))

(define-public crate-tree-sitter-ursa-1.0.8 (c (n "tree-sitter-ursa") (v "1.0.8") (d (list (d (n "cargo-edit") (r "^0.12.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0si1zy9zbq6sxygxhz6amflis3v98mzbnscs4ln2l04gq52350i8")))

