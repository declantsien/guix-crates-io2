(define-module (crates-io tr ee treereduce-lua) #:use-module (crates-io))

(define-public crate-treereduce-lua-0.3.0 (c (n "treereduce-lua") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tree-sitter-lua") (r "^0.0.18") (d #t) (k 0)) (d (n "treereduce") (r "^0.3.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "1s040j182p6dgs2kl1gczqm4v830kj6rap7n0p0z829i92z2a4sd")))

