(define-module (crates-io tr ee treers) #:use-module (crates-io))

(define-public crate-treers-0.1.0 (c (n "treers") (v "0.1.0") (h "0cmbrgxh2avz6riw9app4byy8gv3mll5mj8ahflpgzp23prlbmz2")))

(define-public crate-treers-0.1.1 (c (n "treers") (v "0.1.1") (h "02c2yhasrqrmzf14acqh8f92wdwx1cv93ff823dkrjh5cgypkmhc")))

(define-public crate-treers-0.1.2 (c (n "treers") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0q88mhpah2511pkjfrgsc9zb0d99vfxpxls6z7dm3vb8163rylka")))

