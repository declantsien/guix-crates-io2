(define-module (crates-io tr ee tree-sitter-css) #:use-module (crates-io))

(define-public crate-tree-sitter-css-0.19.0 (c (n "tree-sitter-css") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.19") (d #t) (k 0)))) (h "16r7kczq188p4zpa86hznyhvqj2lxiqjfkiqwr8yygv0kf7wwky4")))

(define-public crate-tree-sitter-css-0.20.0 (c (n "tree-sitter-css") (v "0.20.0") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1z84794z6qn8lrfj68sy47mzpazzfhcx24r6vanq29hxzbg6sc63")))

(define-public crate-tree-sitter-css-0.21.0 (c (n "tree-sitter-css") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1d0fwaamicqigznj6bvssd18zk9x58bxgzgm440jnxinc7whdy72")))

