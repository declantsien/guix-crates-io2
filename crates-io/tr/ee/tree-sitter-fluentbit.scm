(define-module (crates-io tr ee tree-sitter-fluentbit) #:use-module (crates-io))

(define-public crate-tree-sitter-fluentbit-0.1.0-alpha.1 (c (n "tree-sitter-fluentbit") (v "0.1.0-alpha.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0sv7p67vf4bciihqgfq7d4v7hg37by8a9pgb8xfhvmi4djbwilpp")))

(define-public crate-tree-sitter-fluentbit-0.1.0-alpha.2 (c (n "tree-sitter-fluentbit") (v "0.1.0-alpha.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "062wcng22skp645nma51dpx1w6xc3aqkc0r8yfid8706i9d3gx0a")))

