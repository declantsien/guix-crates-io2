(define-module (crates-io tr ee tree-sitter-smithy) #:use-module (crates-io))

(define-public crate-tree-sitter-smithy-0.0.1 (c (n "tree-sitter-smithy") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1dhm8v6vj9gq7vjc177qpn6aq8v211xsiv3pxpjg93krvcj5fqh9")))

