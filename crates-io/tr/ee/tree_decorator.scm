(define-module (crates-io tr ee tree_decorator) #:use-module (crates-io))

(define-public crate-tree_decorator-0.1.0 (c (n "tree_decorator") (v "0.1.0") (h "02yijfql1i9wyzxdw04685x82w3g56dz3lkl3iv7z7kwgkbdl0w7")))

(define-public crate-tree_decorator-0.1.1 (c (n "tree_decorator") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)))) (h "09gavmr7d26jl7ap3pyym9hrd4vq5jxch5pj2aj9gypgrvibxpi3") (f (quote (("no_log") ("default" "log"))))))

(define-public crate-tree_decorator-0.1.2 (c (n "tree_decorator") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)))) (h "0mng8x26h9658b80ik62y1774gfdf4lmh5i6f36ly4gfk258qzqr") (f (quote (("no_log") ("default" "log"))))))

