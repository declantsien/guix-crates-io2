(define-module (crates-io tr ee tree-sitter-lua) #:use-module (crates-io))

(define-public crate-tree-sitter-lua-0.0.1 (c (n "tree-sitter-lua") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "12a0fg60yy29qbwnh7mlkdwcm34frc8rjr3qy9hbbc0mzhw3r6id")))

(define-public crate-tree-sitter-lua-0.0.2 (c (n "tree-sitter-lua") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "1g4pdcws8g6nkrlx2w0gl765yjscjhrzv9yvzf8677mnyhd5qgk4")))

(define-public crate-tree-sitter-lua-0.0.3 (c (n "tree-sitter-lua") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "10d313f5k0m7cw8x4wqymiw3wb4bvfvf7yl5vsdji8dybkxxhh99")))

(define-public crate-tree-sitter-lua-0.0.4 (c (n "tree-sitter-lua") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "1j1wsglhhfrx0qsh08a87m4maqfwialmqkyn9c8f3zdwvsdvd5cr")))

(define-public crate-tree-sitter-lua-0.0.5 (c (n "tree-sitter-lua") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "0vhf2y17wvs8rvahqf4gjk0j65gas4l00k13l3gldy7hjjw538b8")))

(define-public crate-tree-sitter-lua-0.0.6 (c (n "tree-sitter-lua") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "1c9dny04kr2d37qa9jbr8aqsiiw67fnafx2z6pryvr644dmdidx2") (y #t)))

(define-public crate-tree-sitter-lua-0.0.7 (c (n "tree-sitter-lua") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "19w4sks8zzj69as0gr8wh8k3rlgjcj0xvxhpz2hv24d3sk7y106a")))

(define-public crate-tree-sitter-lua-0.0.8 (c (n "tree-sitter-lua") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "0dagc9f9k1lizdkqm948bzd5b0nnhs3hqj6nikndaldj916calap")))

(define-public crate-tree-sitter-lua-0.0.9 (c (n "tree-sitter-lua") (v "0.0.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "11h9r2rglg3542nglh85qibbs6h7rqmvn0qbwgzjq04apapz0am2")))

(define-public crate-tree-sitter-lua-0.0.10 (c (n "tree-sitter-lua") (v "0.0.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1gjdipnyw1sv8rzyq3xmg48w620g5h92wmm82n5hsiq2n8p8xb62")))

(define-public crate-tree-sitter-lua-0.0.11 (c (n "tree-sitter-lua") (v "0.0.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "19dllv6bjipczpn6as0xb8gdbzbicr6jwcnlwi4crj5xaa7xs8yk")))

(define-public crate-tree-sitter-lua-0.0.12 (c (n "tree-sitter-lua") (v "0.0.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0jqvrbmrcd816ipf4mp3za2vkzy5qcmqg5wwbn8ri5hq4wbg3v6y")))

(define-public crate-tree-sitter-lua-0.0.13 (c (n "tree-sitter-lua") (v "0.0.13") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0bv81ysyhp2w2fj90nml9cl78jw6q0n6xx6hblb0rvz4608658ls")))

(define-public crate-tree-sitter-lua-0.0.14 (c (n "tree-sitter-lua") (v "0.0.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "10mjd4n88nwba4272hy1730g15hw11fgq2wk0ignvym2s4zqg2fl")))

(define-public crate-tree-sitter-lua-0.0.15 (c (n "tree-sitter-lua") (v "0.0.15") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1p6r8pljr4wc17z6qhjz1dkvkjja02h078s66cvazg7mrawcifkm")))

(define-public crate-tree-sitter-lua-0.0.16 (c (n "tree-sitter-lua") (v "0.0.16") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "199xp1hd6y32yrzisz0g0xdvawixijf5l13f6dnx1hjwjvmzhcz1")))

(define-public crate-tree-sitter-lua-0.0.17 (c (n "tree-sitter-lua") (v "0.0.17") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0q1gj7b3gx6apvw5yz2ai2lplcksvsnww23mb20v6rsh4j0hldvq")))

(define-public crate-tree-sitter-lua-0.0.18 (c (n "tree-sitter-lua") (v "0.0.18") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0ni21z0rx1h8fby60326iicnbrk36par1x4703qrng9sga4bihc1")))

(define-public crate-tree-sitter-lua-0.0.19 (c (n "tree-sitter-lua") (v "0.0.19") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "08clm8nvl16g436mkygjpixpwh4pzphxs8c9l9nx5lgac94wys09")))

(define-public crate-tree-sitter-lua-0.1.0 (c (n "tree-sitter-lua") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.89") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "0mk80i21b03xlvdf132f1q2xmciz8l1cn7zw8cchwj5xhzyfd7rv")))

