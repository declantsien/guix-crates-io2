(define-module (crates-io tr ee tree-sitter-unofficial) #:use-module (crates-io))

(define-public crate-tree-sitter-unofficial-0.0.1 (c (n "tree-sitter-unofficial") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "164fz5szmi3p014s32ca880ds3gq0fryy153fsnrnxdvsjffi9vw") (r "1.65")))

