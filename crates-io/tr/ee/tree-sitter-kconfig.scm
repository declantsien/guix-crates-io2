(define-module (crates-io tr ee tree-sitter-kconfig) #:use-module (crates-io))

(define-public crate-tree-sitter-kconfig-1.0.0 (c (n "tree-sitter-kconfig") (v "1.0.0") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1c3l6q5knwxrh6dzfhyk00yf0x3q0al754kwyv9hjaln8x8ca6xm")))

(define-public crate-tree-sitter-kconfig-1.0.1 (c (n "tree-sitter-kconfig") (v "1.0.1") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0s1ycb87l6kmfg1xa1rpqvvj0sny3knlkjlvyargsk839x9pz9si")))

(define-public crate-tree-sitter-kconfig-1.1.0 (c (n "tree-sitter-kconfig") (v "1.1.0") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1ww2sd0dhwjs9llwlsrjv5jzwvnyxh6nc0b7z2q2bwzixb42cmmn")))

(define-public crate-tree-sitter-kconfig-1.1.1 (c (n "tree-sitter-kconfig") (v "1.1.1") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "142p7mdpxnsbq5wy3yifrvnzax5cg4fh1fjkyd9a180vlg87rbc6")))

