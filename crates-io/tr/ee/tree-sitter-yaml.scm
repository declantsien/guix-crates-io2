(define-module (crates-io tr ee tree-sitter-yaml) #:use-module (crates-io))

(define-public crate-tree-sitter-yaml-0.0.1 (c (n "tree-sitter-yaml") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1s8grydqjnygiy7mdbhyrr7advfnbipqzfd4g938iibbmp86firj")))

(define-public crate-tree-sitter-yaml-0.6.0 (c (n "tree-sitter-yaml") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.92") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1xqdh91m1q704h4ll878p9ch6kd0y3q2rqs4jizz2sp652nva5y6")))

(define-public crate-tree-sitter-yaml-0.6.1 (c (n "tree-sitter-yaml") (v "0.6.1") (d (list (d (n "cc") (r "^1.0.92") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1dn6i5aza3nr6p4vcqg5xj9cay5lzzyx6kgn2jsxhhykdb27xlma")))

