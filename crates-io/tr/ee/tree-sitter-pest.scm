(define-module (crates-io tr ee tree-sitter-pest) #:use-module (crates-io))

(define-public crate-tree-sitter-pest-0.0.1 (c (n "tree-sitter-pest") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1gzm45lmzaz63bnk58l5zvk203j64p6m6sc27k00fv9krj6676wd")))

(define-public crate-tree-sitter-pest-0.0.2 (c (n "tree-sitter-pest") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0jly4akgz4k777ngv1ijx60mkrkn6g2sg18dmaq2z6rbf3z4j4xl")))

