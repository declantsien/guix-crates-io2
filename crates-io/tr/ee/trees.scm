(define-module (crates-io tr ee trees) #:use-module (crates-io))

(define-public crate-trees-0.1.0 (c (n "trees") (v "0.1.0") (h "071vhfj1sk62g5p1f760js0k918f6bybc9v7l8m7q6skamy4vp6j")))

(define-public crate-trees-0.1.1 (c (n "trees") (v "0.1.1") (h "0via1ggmz0p475dqpqqawngskajcrhjnak4dn7c9prdy2dnl3rz8")))

(define-public crate-trees-0.1.2 (c (n "trees") (v "0.1.2") (h "1vy27cbm8x1r3n72abi418271fgw3r8qyzcn1k69q0cklmqnmssi")))

(define-public crate-trees-0.1.3 (c (n "trees") (v "0.1.3") (h "10glncbl46mzrd0zsxchfnvm15wirrzkdfh004h4a81mq08ifwmx")))

(define-public crate-trees-0.1.4 (c (n "trees") (v "0.1.4") (h "0iamz75a7d12ham1ppb0nbpmhbav1254g67nsvrlg5l2aj0r9ai8") (f (quote (("no_std"))))))

(define-public crate-trees-0.1.5 (c (n "trees") (v "0.1.5") (h "1409vzyh6z8c0bv14jj8lw75fvj7q86dab96gawrl6jj23hk9afq") (f (quote (("no_std"))))))

(define-public crate-trees-0.1.6 (c (n "trees") (v "0.1.6") (h "0iakr3jz5lnwazi3j0g84v6zlga1wvchnrpjqxn6665i46qi8y85") (f (quote (("no_std"))))))

(define-public crate-trees-0.2.0 (c (n "trees") (v "0.2.0") (h "1xfmx7y1gy2vllj5y92vvi5kajfzc8l743gdliw2hv68946mxbw9") (f (quote (("no_std"))))))

(define-public crate-trees-0.2.1 (c (n "trees") (v "0.2.1") (d (list (d (n "indexed") (r "^0.1") (d #t) (k 0)))) (h "10618zvwgxljg5irrv8br42x29hf5k1xp2yhbg65ckxyhlg858dg") (f (quote (("no_std"))))))

(define-public crate-trees-0.3.0 (c (n "trees") (v "0.3.0") (d (list (d (n "indexed") (r "^0.2") (d #t) (k 0)))) (h "1yqnl57lppss5mq6fcmkah2kgs1aqrwdddy24h3hn1ckyv3i5k8z") (f (quote (("no_std"))))))

(define-public crate-trees-0.4.0 (c (n "trees") (v "0.4.0") (h "1bb72wq6kw0fapn732kvnhv19ajcp8cxd3nnswajrqjj6rxr095x") (f (quote (("no_std"))))))

(define-public crate-trees-0.4.1 (c (n "trees") (v "0.4.1") (h "18w6c5jxc0x6ad1y0r0cnim6b3kbfgdv40jbv33f44vcaygggn1a") (f (quote (("no_std"))))))

(define-public crate-trees-0.4.2 (c (n "trees") (v "0.4.2") (h "0y1q5zqmv10hy4z3cq6wzsazmbgf7wyc7pcl394y525brqwggr8d") (f (quote (("no_std"))))))

