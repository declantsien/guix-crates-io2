(define-module (crates-io tr ee tree-sitter-gn) #:use-module (crates-io))

(define-public crate-tree-sitter-gn-1.0.0 (c (n "tree-sitter-gn") (v "1.0.0") (d (list (d (n "cc") (r "~1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "07v4qa6baimm37bszsryfq893vh42sd9fj3n4725hvnq23gfc5cn")))

