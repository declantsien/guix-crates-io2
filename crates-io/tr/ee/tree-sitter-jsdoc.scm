(define-module (crates-io tr ee tree-sitter-jsdoc) #:use-module (crates-io))

(define-public crate-tree-sitter-jsdoc-0.21.0 (c (n "tree-sitter-jsdoc") (v "0.21.0") (d (list (d (n "cc") (r "^1.0.96") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "060g92xss95a6kilpanr0sf8vqd7gyiih0w9fjasa4pz040r41wd")))

