(define-module (crates-io tr ee tree-sitter-uxntal) #:use-module (crates-io))

(define-public crate-tree-sitter-uxntal-0.0.1 (c (n "tree-sitter-uxntal") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "19dq9kha8v3i6498gx5xghn88srv5hkg3f0f5ndgcs48721g13vn")))

(define-public crate-tree-sitter-uxntal-1.0.0 (c (n "tree-sitter-uxntal") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.9") (d #t) (k 0)))) (h "17hbh4kc1dga8si3bkgiln78jrvcvmvp7hckg8xhgzq8ydyvlzfx")))

