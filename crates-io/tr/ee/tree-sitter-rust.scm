(define-module (crates-io tr ee tree-sitter-rust) #:use-module (crates-io))

(define-public crate-tree-sitter-rust-0.19.0 (c (n "tree-sitter-rust") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "05fzgn9d30yvh4v81v1fc153d9dljmg7ifylq9fqjk5xrpwpwkvq")))

(define-public crate-tree-sitter-rust-0.20.0 (c (n "tree-sitter-rust") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1jf3hmgw2glqvdi60bx22cvvw148ylw7rxga49fh2m6pjfj41x9x")))

(define-public crate-tree-sitter-rust-0.20.1 (c (n "tree-sitter-rust") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "05sp369nngzmbadwa09g32d8k6jknl9i1g7mjv7klyijnyphyiqk")))

(define-public crate-tree-sitter-rust-0.20.2 (c (n "tree-sitter-rust") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.20") (d #t) (k 0)))) (h "0zv0v31xjq7wf1i0ym87l9bn5ahsh9cqvmsgfrdsdbq7jhs1zl4w")))

(define-public crate-tree-sitter-rust-0.20.3 (c (n "tree-sitter-rust") (v "0.20.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.20") (d #t) (k 0)))) (h "19mx436dkx3gmbgmvbn25y17ncxzc20a20ylwldc2b957rrl4y3r")))

(define-public crate-tree-sitter-rust-0.20.4 (c (n "tree-sitter-rust") (v "0.20.4") (d (list (d (n "cc") (r "~1.0.82") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "153mz8k8jh1l87z6nlb8nir1szmlij0hwp6fc0vx7dmjn04j70xh")))

(define-public crate-tree-sitter-rust-0.21.0 (c (n "tree-sitter-rust") (v "0.21.0") (d (list (d (n "cc") (r "~1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "1n20xbwp1x2glqj7m89p2ld4y67msb1p8w9czd3ndn78122v7ij7")))

(define-public crate-tree-sitter-rust-0.21.1 (c (n "tree-sitter-rust") (v "0.21.1") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "0ir7il4rw7yp9jgd04m6ykirssjqa2p0sxp6zy137y4m949kcbr2")))

(define-public crate-tree-sitter-rust-0.21.2 (c (n "tree-sitter-rust") (v "0.21.2") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "14qwvrn248k8lrbvlz04wr7nrr5c72h3skwqx8fp945z43s90xi7")))

