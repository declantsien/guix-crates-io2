(define-module (crates-io tr ee tree-sitter-c2rust-core) #:use-module (crates-io))

(define-public crate-tree-sitter-c2rust-core-0.20.9 (c (n "tree-sitter-c2rust-core") (v "0.20.9") (d (list (d (n "c2rust-bitfields") (r "^0.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.5") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)))) (h "1g5z9binh0glkb9bpy5jlwxa4zzw7skjz3l9bsaid97m0a9yib81")))

