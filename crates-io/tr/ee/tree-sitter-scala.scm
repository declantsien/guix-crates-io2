(define-module (crates-io tr ee tree-sitter-scala) #:use-module (crates-io))

(define-public crate-tree-sitter-scala-0.20.0 (c (n "tree-sitter-scala") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.7") (d #t) (k 0)))) (h "14y15v6bj4hll5r2hpi2hdlpx90js88xsdjvnz60lyj91xbn2744")))

(define-public crate-tree-sitter-scala-0.20.1 (c (n "tree-sitter-scala") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.7") (d #t) (k 0)))) (h "1vs5yza85fd7k7y7ik7z0wqz5dcpxv55viqbqhvmkjbllbkis7sf")))

(define-public crate-tree-sitter-scala-0.20.2 (c (n "tree-sitter-scala") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.7") (d #t) (k 0)))) (h "0as1517ansgsy7mqvsib80xlbjmv8swvjgp7jzz9jcib9yml7pwk")))

(define-public crate-tree-sitter-scala-0.20.3 (c (n "tree-sitter-scala") (v "0.20.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.7") (d #t) (k 0)))) (h "0l53vld3qs6gp5d29ynxmd2ybwr4p5c2dx9zmk5vb8w8i9ig9z24")))

(define-public crate-tree-sitter-scala-0.21.0 (c (n "tree-sitter-scala") (v "0.21.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.21.0") (d #t) (k 0)))) (h "1nsybbbn96fg8a791qpgzv97q4dsmdindks1cnsr3c047bik7jz4")))

