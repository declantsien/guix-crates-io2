(define-module (crates-io tr ee treereduce-rust) #:use-module (crates-io))

(define-public crate-treereduce-rust-0.1.0 (c (n "treereduce-rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.1.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "1gw1i4zh55n33bcnlgn5ymj259iplyv86nsapdb8llsbk7qwbfdh")))

(define-public crate-treereduce-rust-0.2.0 (c (n "treereduce-rust") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "1vnhzyd6yiyxbnx1vpr0q35w4g4xcbpb51gisl0sawld9pijj54r")))

(define-public crate-treereduce-rust-0.2.1 (c (n "treereduce-rust") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.1") (f (quote ("cli"))) (d #t) (k 0)))) (h "0hkg79ac9id5hkvs8k3ijk40prc0a5dlgik1b43kzd08myhvkdmy")))

(define-public crate-treereduce-rust-0.2.2 (c (n "treereduce-rust") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.2.2") (f (quote ("cli"))) (d #t) (k 0)))) (h "0n2ifqvhjdrh213s9aq7vf3qxfadl8qa7n3cvkhgaw6sdpwjlr26")))

(define-public crate-treereduce-rust-0.3.0 (c (n "treereduce-rust") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)) (d (n "treereduce") (r "^0.3.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "0i6fqxyyhc2b9304mypknakf0af5i9l9fw0zqmfnnqghipnr17hl")))

