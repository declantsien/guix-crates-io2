(define-module (crates-io tr ee tree-sitter-wgsl-bevy) #:use-module (crates-io))

(define-public crate-tree-sitter-wgsl-bevy-0.1.1 (c (n "tree-sitter-wgsl-bevy") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)))) (h "191kk9z201vbi55xrz2577bmkb73b6j22z4rfdfkbg8x9mbmx0qw")))

(define-public crate-tree-sitter-wgsl-bevy-0.1.2 (c (n "tree-sitter-wgsl-bevy") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)))) (h "1akf41qkhdvirxb9v6dk9wrmbpq7dp8bygdzyqlqq2pic43z8616")))

(define-public crate-tree-sitter-wgsl-bevy-0.1.3 (c (n "tree-sitter-wgsl-bevy") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)))) (h "1fvl1fz0z04bqp260qaz1x0bih6bk0wdg35vp6n4k34sa77k82y5")))

