(define-module (crates-io tr ee tree-sitter-odin) #:use-module (crates-io))

(define-public crate-tree-sitter-odin-1.0.0 (c (n "tree-sitter-odin") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "14pd2zscy3fxqmdzzlk4npjry6z5g4bpfykjhyqdjz9f5828kf3p")))

(define-public crate-tree-sitter-odin-1.0.1 (c (n "tree-sitter-odin") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1cffy6ckmp8asd6dly2nbjgphdf1l08bcsd23y5qsb7mvhapbivy")))

(define-public crate-tree-sitter-odin-1.1.0 (c (n "tree-sitter-odin") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1ynpr82a8yrzh8y3fs1z8jx0h0592n1ba18vn567y01a2gvzcj65")))

(define-public crate-tree-sitter-odin-1.2.0 (c (n "tree-sitter-odin") (v "1.2.0") (d (list (d (n "cc") (r "^1.0.92") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.21.0") (d #t) (k 0)))) (h "09dm98lsnhywlgrxz9sh3v1zzjqhsj7ppdl1k3lij9b9cbj9xa34")))

