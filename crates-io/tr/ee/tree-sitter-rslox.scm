(define-module (crates-io tr ee tree-sitter-rslox) #:use-module (crates-io))

(define-public crate-tree-sitter-rslox-0.1.0 (c (n "tree-sitter-rslox") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1vxnc28mgszw8337zg3zrprpzgl4ss2q3rga3k7ng8jjfs3c7pzh")))

(define-public crate-tree-sitter-rslox-0.1.1 (c (n "tree-sitter-rslox") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "06hha28w4kwkz4270xzy3c23s2bllvc92dml995wcgykh2hy9g7p")))

