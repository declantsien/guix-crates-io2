(define-module (crates-io tr ee tree-sitter-csv) #:use-module (crates-io))

(define-public crate-tree-sitter-csv-1.0.0 (c (n "tree-sitter-csv") (v "1.0.0") (d (list (d (n "cc") (r "~1.0.82") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1h5dzxsr2z5i8lj92fk5c7scnb9c5n0qpr3a2s9c0rbjh3j2zb7a")))

(define-public crate-tree-sitter-csv-1.1.0 (c (n "tree-sitter-csv") (v "1.1.0") (d (list (d (n "cc") (r "~1.0.82") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1wxk9w5g9smnaq5cly06m3931j9n9pfcm30wycjk6vk8lql7s064")))

(define-public crate-tree-sitter-csv-1.1.1 (c (n "tree-sitter-csv") (v "1.1.1") (d (list (d (n "cc") (r "~1.0.82") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1w5l5wfv92bz1hrf7m288i0yg6s6lj44dji75mvj3pkxcjb7qyym")))

(define-public crate-tree-sitter-csv-1.2.0 (c (n "tree-sitter-csv") (v "1.2.0") (d (list (d (n "cc") (r "~1.0.82") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0396d5xdhcfrybvi4mp0b20015y0bvmq71izj6ry5k0665w48a92")))

