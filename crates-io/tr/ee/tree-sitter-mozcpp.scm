(define-module (crates-io tr ee tree-sitter-mozcpp) #:use-module (crates-io))

(define-public crate-tree-sitter-mozcpp-0.19.0 (c (n "tree-sitter-mozcpp") (v "0.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.19.0") (d #t) (k 1)))) (h "1mq20bjsx9bdg01z6gxnj9l46h07vjq19hqjnixkwwm2hhfmxnjn")))

(define-public crate-tree-sitter-mozcpp-0.19.3 (c (n "tree-sitter-mozcpp") (v "0.19.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.19.0") (d #t) (k 1)))) (h "0iwgligy1py2b4jrv4q8qiz5cssr6y4lsnxx6x3pbn1hhms4cil1")))

(define-public crate-tree-sitter-mozcpp-0.19.5 (c (n "tree-sitter-mozcpp") (v "0.19.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.19.0") (d #t) (k 1)))) (h "1ijcmjb9r467r5cx4bljrkhn20mqv8l5s5nhzjgg2nk8nwr9yhym")))

(define-public crate-tree-sitter-mozcpp-0.20.2 (c (n "tree-sitter-mozcpp") (v "0.20.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "=0.20.9") (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.20.0") (d #t) (k 1)))) (h "0gni01s6d4mfv91ilpmycywpzbl84xqa5zvz091mqmqavsxlwlg9")))

