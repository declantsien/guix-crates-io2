(define-module (crates-io tr ee tree-sitter-hlsl) #:use-module (crates-io))

(define-public crate-tree-sitter-hlsl-0.1.0 (c (n "tree-sitter-hlsl") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.8") (d #t) (k 0)))) (h "05w972qb1rqrmj7j26m24xw5f4zbdz2bcags1bvz7vgbndm0jfhv")))

(define-public crate-tree-sitter-hlsl-0.1.1 (c (n "tree-sitter-hlsl") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.8") (d #t) (k 0)))) (h "02b7q1x3ry91l36bvakn6fs0bk2akx1sakqicmkw3bba2b2byidw")))

(define-public crate-tree-sitter-hlsl-0.1.2 (c (n "tree-sitter-hlsl") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.8") (d #t) (k 0)))) (h "0fnr33j3dbd74wmrsgfffw0sb8y6m303khyw9vqsaxkp99kv0as8")))

(define-public crate-tree-sitter-hlsl-0.1.3 (c (n "tree-sitter-hlsl") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "0wr40hc0ygz5gm69n2xbqz3zdy633y1l8x3h40h0pa7lin70wxn0")))

(define-public crate-tree-sitter-hlsl-0.1.4 (c (n "tree-sitter-hlsl") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.10") (d #t) (k 0)))) (h "1wqgyrwl9y66ry1h9x2qih60rm6ils7slr3rsacprhvhhdcrb8dw")))

