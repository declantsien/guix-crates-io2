(define-module (crates-io tr ee tree-sitter-comment) #:use-module (crates-io))

(define-public crate-tree-sitter-comment-0.1.0 (c (n "tree-sitter-comment") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0w9g1km8iqlswqncj08x3fmblnmmpnyl5rc3a0rhsjhx0qjyn59q")))

