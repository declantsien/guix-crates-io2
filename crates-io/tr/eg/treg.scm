(define-module (crates-io tr eg treg) #:use-module (crates-io))

(define-public crate-treg-0.1.0 (c (n "treg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02j1np9b8ihbwwmqpwg8a3q9ppycndiyimy05jnbwy9sbny7d6rq")))

(define-public crate-treg-0.1.1 (c (n "treg") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1766cnx7aa4axpjjrdmd805pvmkp7xy3mqmfl4xzz5jiq10nr7c3")))

