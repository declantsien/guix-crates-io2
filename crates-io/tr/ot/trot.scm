(define-module (crates-io tr ot trot) #:use-module (crates-io))

(define-public crate-trot-0.0.0 (c (n "trot") (v "0.0.0") (h "0jgk0kpd2bli5yrgpabrnd4lsk0319l8358x4swxs3yzlyrkmn1m") (y #t)))

(define-public crate-trot-0.0.1 (c (n "trot") (v "0.0.1") (h "0mciwpnj6g3y61dlz0k5b76ahbl4clqy0d7xyk3kjn5vag7pnr66") (y #t)))

(define-public crate-trot-0.0.2 (c (n "trot") (v "0.0.2") (h "0xpri0qfg3zkkdjlrkn0xw4ciq3pah4vrmm8f17rvrq12wrf3vrw") (y #t)))

(define-public crate-trot-0.1.0 (c (n "trot") (v "0.1.0") (h "0zj7j1mmxpycx1h6css6f0xp9l2xlmr5z16l9148f0krk6ygny5c")))

(define-public crate-trot-0.1.1 (c (n "trot") (v "0.1.1") (h "1644mixgj32bpcjgr13hc2y6hwxkvr3537gyqkmwzkpwanc8m2z3")))

(define-public crate-trot-0.1.2 (c (n "trot") (v "0.1.2") (h "138ly8skbkjy9m7kw4wbr94f59i7l4gbiscz6d903wi6hqrg76bj")))

