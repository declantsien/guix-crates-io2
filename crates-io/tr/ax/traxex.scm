(define-module (crates-io tr ax traxex) #:use-module (crates-io))

(define-public crate-traxex-0.1.0 (c (n "traxex") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1c9sy5cl8hg4nf1my6q1y5wllqj9arcgdbvdsfv83qwdc18gl432")))

(define-public crate-traxex-0.1.1 (c (n "traxex") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "0r2dbw73xf27nkcf843cz49idgs06rqy4q01fs3bnhfb8jac5q1z")))

