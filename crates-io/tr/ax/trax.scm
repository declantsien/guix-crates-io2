(define-module (crates-io tr ax trax) #:use-module (crates-io))

(define-public crate-trax-0.0.1 (c (n "trax") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1930q192casrrskvcx6sc5066bj41xb7c5xndakh6ydy70xb7vdr")))

