(define-module (crates-io tr is tristate) #:use-module (crates-io))

(define-public crate-tristate-0.0.1 (c (n "tristate") (v "0.0.1") (h "1flzj0dmz7wfnb0f94ybgfiqccsbkiynkpcd5dlzn3njbn57f201")))

(define-public crate-tristate-0.0.2 (c (n "tristate") (v "0.0.2") (h "0p7yb5a0jmpww4iq5q9rqwnr8gg0hbki8qx970lwmbprh5y5knsn")))

(define-public crate-tristate-0.1.0 (c (n "tristate") (v "0.1.0") (h "14qkrm4bli1b3qcbmr596ml921kn9lkz028acqaxm0wccry2c3l5")))

(define-public crate-tristate-0.1.1 (c (n "tristate") (v "0.1.1") (h "0di2alk1g76b0xglsqdjscbz3rl7jyjz9097nczc7jmbh1awbww0")))

