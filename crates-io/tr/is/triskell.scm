(define-module (crates-io tr is triskell) #:use-module (crates-io))

(define-public crate-triskell-0.1.0 (c (n "triskell") (v "0.1.0") (h "1bfv3d6rkpz5nvrwq875vi0q5qf9czqgj6j465pi8kb1lndsr4y5")))

(define-public crate-triskell-0.1.1 (c (n "triskell") (v "0.1.1") (h "061j72pmdndfbd8c8zb1vr80cs9bp317avd7xbqjm8w8w2bsdl06")))

(define-public crate-triskell-0.1.2 (c (n "triskell") (v "0.1.2") (h "03cxl32v9sy8rps3jz8ii3nixfvcxllrnmshmbgpjvcn6di66ffc")))

