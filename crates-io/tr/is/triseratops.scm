(define-module (crates-io tr is triseratops) #:use-module (crates-io))

(define-public crate-triseratops-0.0.1 (c (n "triseratops") (v "0.0.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "id3") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "textwrap") (r "^0.13") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g0aryb2b56v8a928a82cvfh32iqn9ckbija3qfxmwdyd0f0mjnq")))

(define-public crate-triseratops-0.0.2 (c (n "triseratops") (v "0.0.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "id3") (r "^1") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kiyi2b2i8d9i66bpab0dmx22x9k4ak9sawx68pkbvh2jvrxfm1x")))

(define-public crate-triseratops-0.0.3 (c (n "triseratops") (v "0.0.3") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "id3") (r "^1") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01ka0k7wi1gkqkz6mk0pjq78r2pg37f6lylwcb0mmya3rv9z57dg")))

