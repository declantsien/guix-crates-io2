(define-module (crates-io tr oi troika) #:use-module (crates-io))

(define-public crate-troika-0.1.2 (c (n "troika") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1zhkzx2mdzy016np8jhpp9ccl0pw5yhq1pl4nj3c26vkqvdx8kgx")))

