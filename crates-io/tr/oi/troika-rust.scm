(define-module (crates-io tr oi troika-rust) #:use-module (crates-io))

(define-public crate-troika-rust-0.1.0 (c (n "troika-rust") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ict") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0p7w3y4bc3zvkvarcswn3yn5ms74z0jmz2h6fvnaq6ah2m65xh9f")))

(define-public crate-troika-rust-0.1.1 (c (n "troika-rust") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ict") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0g9lgp0856wcwqm1jdl4lfx3v80qhn18ph095zn7qicy8a1iaznp")))

