(define-module (crates-io tr es tresorit-dropbox-discovery) #:use-module (crates-io))

(define-public crate-tresorit-dropbox-discovery-1.0.0 (c (n "tresorit-dropbox-discovery") (v "1.0.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0ln0537qm5sqa8gd61b2fymrd6byvab82apcsjjh94a9nwg7c8ha")))

