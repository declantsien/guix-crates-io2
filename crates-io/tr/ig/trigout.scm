(define-module (crates-io tr ig trigout) #:use-module (crates-io))

(define-public crate-trigout-0.1.0 (c (n "trigout") (v "0.1.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "1mjkzwydsjj7rx4p93q3kjgdsrm3abiflpmwfzj5q7fdmm9ifg2c")))

