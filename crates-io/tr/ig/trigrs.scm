(define-module (crates-io tr ig trigrs) #:use-module (crates-io))

(define-public crate-trigrs-0.1.0 (c (n "trigrs") (v "0.1.0") (d (list (d (n "nannou") (r "^0.18") (d #t) (k 0)))) (h "1369fn4p4saydnhdn3l5xjy3m40jiz00i467y8w9hxjlvhv2ppm6")))

(define-public crate-trigrs-1.0.0 (c (n "trigrs") (v "1.0.0") (d (list (d (n "nannou") (r "^0.18") (d #t) (k 0)))) (h "1mm874slzpf1gsdnpskvam1b7swpw24zxvwnzqsswjmq2sx0cqww")))

