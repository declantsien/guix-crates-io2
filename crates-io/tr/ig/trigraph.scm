(define-module (crates-io tr ig trigraph) #:use-module (crates-io))

(define-public crate-trigraph-0.1.0 (c (n "trigraph") (v "0.1.0") (h "05vsbpr9avvplr0zj0hg12q9myijzdlx4614jy15l4byxld9d8ak")))

(define-public crate-trigraph-0.1.1 (c (n "trigraph") (v "0.1.1") (h "0aps0xwapdw41rcx04vrkbn75lkz2hkig2n985zb7psgq3lldpqm")))

(define-public crate-trigraph-0.1.3 (c (n "trigraph") (v "0.1.3") (h "1gyy731q2yr680xm0amn9l1z35v5pcs0fhwl8dn2li9bvpk2n3js")))

