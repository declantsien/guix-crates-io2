(define-module (crates-io tr ig triggered) #:use-module (crates-io))

(define-public crate-triggered-0.1.0 (c (n "triggered") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2.11") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "06jgs6hclf570hz2nsbbshi6bb7gd5caqbszkx17mfhfy429h2qg")))

(define-public crate-triggered-0.1.1 (c (n "triggered") (v "0.1.1") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2.11") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1wasfb1m5r400lyba3kkczkhw1n4z5y6wp0m070zf5zx7xgmm89s")))

(define-public crate-triggered-0.1.2 (c (n "triggered") (v "0.1.2") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2.11") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1ld20djkfri072vn5fc88wlvi70x4v1iyrdfjhdnqdqs1np8w56f")))

