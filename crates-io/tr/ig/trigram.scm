(define-module (crates-io tr ig trigram) #:use-module (crates-io))

(define-public crate-trigram-0.1.0 (c (n "trigram") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "16dxd0qqvaa4fkv1zx865vjjl4w234669kjpdgg7pxkcz9iwjg09")))

(define-public crate-trigram-0.2.0 (c (n "trigram") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1w1d7jlya5f7050q11jgshdf9xsj9zazv807jm0387hbcn92y893")))

(define-public crate-trigram-0.2.1 (c (n "trigram") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0v4wg9gzybs1jnwr4jmdg67s8n0i7ylk73zqi0r0a839alaq5cv6")))

(define-public crate-trigram-0.2.2 (c (n "trigram") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0ghccbhrc14986vqpghk5sp02vswfx6mb2hidyf6j35a81rhb1k0")))

(define-public crate-trigram-0.2.3 (c (n "trigram") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0sa9gc383n21r711k571in32gha92cgi6qrs8qf9lzw2lskrz57g")))

(define-public crate-trigram-0.2.4 (c (n "trigram") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1y3d4a8kq0aqbywyxxy76g9wifk4s0ny7ccs7va2cj4apgqxwklr")))

(define-public crate-trigram-0.3.0 (c (n "trigram") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)))) (h "0x3c9r9hfn4cfs7y1q1jnyndy6qhpnfsn1srpk259mg8a9pz03cm")))

(define-public crate-trigram-0.4.0 (c (n "trigram") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)))) (h "08wz4i4lmb1nwv4rympm9zjfmy8dgmfdzni48q9gndq0pmrs2q4l")))

(define-public crate-trigram-0.4.1 (c (n "trigram") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)))) (h "1yxdn2z9rxs5jyd4lkh9r89p1y1ifq7w6ax3clw31n7lnvkfl5g1")))

(define-public crate-trigram-0.4.3 (c (n "trigram") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)))) (h "0b00756pv51lhg874cxn0wpxb2fr3qgi6la45nxbcnbls68iskhv")))

(define-public crate-trigram-0.4.4 (c (n "trigram") (v "0.4.4") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)))) (h "0isk3j2hgpba40isy5mblx4sr57k3709k4rcky3cdpw16l850hrj")))

