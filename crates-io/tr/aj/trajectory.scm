(define-module (crates-io tr aj trajectory) #:use-module (crates-io))

(define-public crate-trajectory-0.0.1 (c (n "trajectory") (v "0.0.1") (d (list (d (n "gnuplot") (r "^0.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "00idi984nhh2d38cg35mw87ngyv4bglij2jcn4bl4awzvpgfrrlx")))

(define-public crate-trajectory-0.0.2 (c (n "trajectory") (v "0.0.2") (d (list (d (n "gnuplot") (r "^0.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1x8zp5gwwq8jxqpz3xfr15xvgy5gs4y8m53ackwsggmfvvnsz9jm")))

(define-public crate-trajectory-0.1.0 (c (n "trajectory") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "gnuplot") (r "^0.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0jisqxlj0444jipz8s7zpqs5y55is6z17fl6fqwzhwa039x1ka7k")))

