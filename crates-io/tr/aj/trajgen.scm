(define-module (crates-io tr aj trajgen) #:use-module (crates-io))

(define-public crate-trajgen-0.1.0 (c (n "trajgen") (v "0.1.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)))) (h "0qazz2qq54bv7jy3a1xykaib456azil95wa3y868ab9j50i2wwa6")))

(define-public crate-trajgen-0.1.1 (c (n "trajgen") (v "0.1.1") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)))) (h "0kwab84qln5av6nk47ifmwkpvj21jhfys59adx2i784rs3p7c79x")))

