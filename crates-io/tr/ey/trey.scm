(define-module (crates-io tr ey trey) #:use-module (crates-io))

(define-public crate-trey-0.1.0 (c (n "trey") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0wascpy2iz1cxasn0kwn1ypr3d0l9b9xd1alb6lqgmqpfhfyhavc")))

(define-public crate-trey-0.2.0 (c (n "trey") (v "0.2.0") (d (list (d (n "lyn") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "1p2y6w2ablzi6hirnxvbsja3lwai7jny1h8zn947rfxdlh5yywqz")))

