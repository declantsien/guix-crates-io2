(define-module (crates-io tr aw trawldb) #:use-module (crates-io))

(define-public crate-trawldb-0.2.2 (c (n "trawldb") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 2)) (d (n "zbus") (r "^2") (f (quote ("tokio"))) (k 0)))) (h "1cqs694n617jjs1km16a2ff2q90n1p8k10hgqisz6jy0jgwmjna9")))

(define-public crate-trawldb-0.2.3 (c (n "trawldb") (v "0.2.3") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trawld") (r "^0.2.2") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 2)) (d (n "zbus") (r "^2") (f (quote ("tokio"))) (k 0)))) (h "11g3z9k2xl4j044l6flamqzflhcj0z8lnl9vx3rm2zx4w0jfxamc")))

(define-public crate-trawldb-0.2.4 (c (n "trawldb") (v "0.2.4") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trawld") (r "^0.2.2") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 2)) (d (n "zbus") (r "^2") (f (quote ("tokio"))) (k 0)) (d (n "zvariant") (r "^3.4.1") (d #t) (k 0)))) (h "1r5zf5pnszkwzdaprw9larrwrkgrnpg2wjjg7czda1rfk21pniar")))

(define-public crate-trawldb-0.2.5 (c (n "trawldb") (v "0.2.5") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trawld") (r "^0.2") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 2)) (d (n "zbus") (r "^2") (f (quote ("tokio"))) (k 0)) (d (n "zvariant") (r "^3.4.1") (d #t) (k 0)))) (h "14f8c7dv806ylc84smmagdi0z3l2b4dwjcpqwkpfrd2w42kpapzd")))

