(define-module (crates-io tr aw trawld) #:use-module (crates-io))

(define-public crate-trawld-0.2.2 (c (n "trawld") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 2)) (d (n "zbus") (r "^2") (f (quote ("tokio"))) (k 0)))) (h "1hj13b12n32i5kjcj82sql4g0dggwcz8c1gz20n33p4kab78f1j3")))

(define-public crate-trawld-0.2.3 (c (n "trawld") (v "0.2.3") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 2)) (d (n "zbus") (r "^2") (f (quote ("tokio"))) (k 0)))) (h "1gqxhs1p8ipk0l0bz4fsk6gbqj46wb6927jp976as6bd3kj27iaw")))

