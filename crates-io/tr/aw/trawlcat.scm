(define-module (crates-io tr aw trawlcat) #:use-module (crates-io))

(define-public crate-trawlcat-0.2.2 (c (n "trawlcat") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trawldb") (r "^0.2.2") (d #t) (k 0)))) (h "08ysg1ld85pz7s96r47jsl2h68vmqiwhsqsksmcq7kg9p5id7gac")))

(define-public crate-trawlcat-0.2.3 (c (n "trawlcat") (v "0.2.3") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string-error") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trawldb") (r "^0.2.2") (d #t) (k 0)))) (h "1fsi1fwp9wqd0ha44hssf5v66h57xxdnkkjxxzjfd8g0f5vaj51j")))

(define-public crate-trawlcat-0.2.4 (c (n "trawlcat") (v "0.2.4") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string-error") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trawldb") (r "^0.2.2") (d #t) (k 0)))) (h "080vp3kac3hrsd7ddanfplhbdcvngapb71m8wd1f27vcy1ncff32")))

(define-public crate-trawlcat-0.2.5 (c (n "trawlcat") (v "0.2.5") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string-error") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trawldb") (r "^0.2.2") (d #t) (k 0)))) (h "01bc3s0zcyj4b84yss8mpqdwqxd7l5b9cbg2ii2405pqqaf0fyqn")))

(define-public crate-trawlcat-0.2.6 (c (n "trawlcat") (v "0.2.6") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string-error") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trawldb") (r "^0.2.4") (d #t) (k 0)))) (h "02gaixyl1m54bbm9kj2lvi3ggj2jwx24kmn1kkaz4badsga02wpb")))

(define-public crate-trawlcat-0.2.7 (c (n "trawlcat") (v "0.2.7") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string-error") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trawldb") (r "^0.2") (d #t) (k 0)))) (h "1gyaskc89b2w8vsfbd6yvb8ja80rl4d04vyakyyyy2h3n9npsqad")))

