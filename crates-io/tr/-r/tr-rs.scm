(define-module (crates-io tr -r tr-rs) #:use-module (crates-io))

(define-public crate-tr-rs-0.1.0 (c (n "tr-rs") (v "0.1.0") (d (list (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "ta-common") (r "^0.1.0") (d #t) (k 0)) (d (n "wilders-rs") (r "^0.1.0") (d #t) (k 0)))) (h "1yfz7zrnm3f20s0r723riw24c4yy6xf596r6alrhckbjg3kln51w")))

