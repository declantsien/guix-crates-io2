(define-module (crates-io tr -l tr-lang) #:use-module (crates-io))

(define-public crate-tr-lang-0.1.3 (c (n "tr-lang") (v "0.1.3") (h "074wxsdw3bkri3l6mn74w46p6pch2v716vsggcfwr5g24xj1aa0w")))

(define-public crate-tr-lang-0.1.4 (c (n "tr-lang") (v "0.1.4") (h "10n9dd5y02znxriq1ijhjhyw9vbld8d73r0s0in92c43b3fipxds")))

(define-public crate-tr-lang-0.1.5 (c (n "tr-lang") (v "0.1.5") (h "06bphh32cxncykz37xsi35dyrgwkhxk465a2shaa0b45ly599hsq")))

(define-public crate-tr-lang-0.2.0 (c (n "tr-lang") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z246ir0jyii06sipcw48zin1rn09bfc6fsb8f528n4y2jnbbmz2")))

(define-public crate-tr-lang-0.2.1 (c (n "tr-lang") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s1npg8jr6mbdhw4ms0hf2ckb8jmba9px9yfn342xvm7yd5j6c3h")))

(define-public crate-tr-lang-0.2.2 (c (n "tr-lang") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s4w2wn75lqsjsshajlfdmlrld99qgxkc2w3sm07midi0v7mypl8")))

(define-public crate-tr-lang-0.2.3 (c (n "tr-lang") (v "0.2.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0718qp4q00jim1cf3qzckja61p0q5qnlkrkjqqw5yaw999vs3zsf")))

(define-public crate-tr-lang-0.3.0 (c (n "tr-lang") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18yy0414c80xdms0clrm6smpzhkxp8wgk1llvbgwqmivxlvzq47m")))

(define-public crate-tr-lang-0.3.1 (c (n "tr-lang") (v "0.3.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11bryhsqp7icppmdkfpmsl5g1bc8b1b2z2bdsa77466mg6zl74b7")))

(define-public crate-tr-lang-0.4.0 (c (n "tr-lang") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "locale_config") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1imx1xxhci3f02a0x0pmdw8n767q1pnq3v359zc8p3jiz22lr5jh") (f (quote (("interactive" "rustyline" "regex" "lazy_static") ("default" "interactive"))))))

