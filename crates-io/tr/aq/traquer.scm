(define-module (crates-io tr aq traquer) #:use-module (crates-io))

(define-public crate-traquer-0.0.1 (c (n "traquer") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0v89yihz6g9nzckvafs8sfra64yl7r8pixrsdyf2ampmvvqpgsy1")))

