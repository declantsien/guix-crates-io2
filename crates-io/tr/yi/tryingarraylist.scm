(define-module (crates-io tr yi tryingarraylist) #:use-module (crates-io))

(define-public crate-tryingarraylist-0.1.0 (c (n "tryingarraylist") (v "0.1.0") (h "1qclw04psgk14kb9iv6yg9ylzl3n6czzn9xpa046xl4mw8bbipa0")))

(define-public crate-tryingarraylist-0.1.1 (c (n "tryingarraylist") (v "0.1.1") (h "0gbjwxmw3sg3vhadxqnlqjys2sxciy4kqhcdcng5wfn8yr8a7rm4")))

(define-public crate-tryingarraylist-0.1.2 (c (n "tryingarraylist") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16wadn0cm31ywv7dxkimy66lhpa3bjqzmaxcqqf114iyh1y5y319")))

