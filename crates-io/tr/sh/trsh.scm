(define-module (crates-io tr sh trsh) #:use-module (crates-io))

(define-public crate-trsh-0.1.1 (c (n "trsh") (v "0.1.1") (d (list (d (n "clap") (r ">=3.0.0-beta.2, <4.0.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "openssl") (r ">=0.10.30, <0.11.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.3.4, <0.4.0") (f (quote ("macros" "io-util" "net" "rt"))) (d #t) (k 0)) (d (n "tokio-fd") (r ">=0.2.1, <0.3.0") (d #t) (k 0)) (d (n "tokio-openssl") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "18h8sgz7822gv0wb4chvdvy3bvcgnylm0x3c81bmys6qiw17k9q1")))

(define-public crate-trsh-0.1.2 (c (n "trsh") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "tokio") (r "^0.3.4") (f (quote ("macros" "io-util" "net" "rt"))) (d #t) (k 0)) (d (n "tokio-fd") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio-openssl") (r "^0.5.0") (d #t) (k 0)))) (h "0zsigfkkahspv7wl1ds08y0kp89kpnkqc525wjzdnqrikkd0cpfq")))

