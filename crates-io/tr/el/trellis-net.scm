(define-module (crates-io tr el trellis-net) #:use-module (crates-io))

(define-public crate-trellis-net-0.1.0 (c (n "trellis-net") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "141ia0shjs2il9crf0c5xszigmwrhcdmgsn9wq6jiah9ichmblch")))

