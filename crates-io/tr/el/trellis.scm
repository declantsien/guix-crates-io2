(define-module (crates-io tr el trellis) #:use-module (crates-io))

(define-public crate-trellis-0.1.0 (c (n "trellis") (v "0.1.0") (d (list (d (n "i2cdev") (r "^0.3.0") (d #t) (k 0)))) (h "1w6mw71had37kpnxn6z3cpn2shificyjf52jv9gd18msz9fynvnq")))

(define-public crate-trellis-0.2.0 (c (n "trellis") (v "0.2.0") (d (list (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)))) (h "19krv57gckxbpiwfxqppxq0xki73zyjai6kdijb325dqw7bgv0hk")))

(define-public crate-trellis-0.2.1 (c (n "trellis") (v "0.2.1") (d (list (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)))) (h "12ar2dd9xr9khw16dmvrj0295hqw7ag9ixlbl40q1n618kf0hpzh")))

