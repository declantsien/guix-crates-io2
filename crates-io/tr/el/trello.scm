(define-module (crates-io tr el trello) #:use-module (crates-io))

(define-public crate-trello-0.1.2 (c (n "trello") (v "0.1.2") (d (list (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0qa92mcx8af9nmwl0k11l7lylxk9hv9nsgbjfljhj8yaqa3y70kw")))

(define-public crate-trello-0.1.4 (c (n "trello") (v "0.1.4") (d (list (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1l6946k3jwwmy6pvalqskkillcqd9f4j1lm9m3kd7s1zrpyi8zkh")))

(define-public crate-trello-0.1.5 (c (n "trello") (v "0.1.5") (d (list (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1ypy7yfwsc4bd2jlw45smhird35pqm75d9aa19xrws8vifr236zf")))

(define-public crate-trello-0.1.6 (c (n "trello") (v "0.1.6") (d (list (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0vj1f6a5a39r28zwmcq0c52iypy5zffasshf2l304ypbh037sr69")))

(define-public crate-trello-0.1.7 (c (n "trello") (v "0.1.7") (d (list (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "mockito") (r "^0.1.4") (f (quote ("use_hyper"))) (d #t) (k 0)) (d (n "mockito") (r "^0.1.4") (f (quote ("mock_hyper"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0phc9wiqlpa7h9vh3vxy0zfb3ysrjilmws99l7jd7b8ra2d8wbrg")))

