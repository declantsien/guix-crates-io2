(define-module (crates-io tr am tram) #:use-module (crates-io))

(define-public crate-tram-0.1.0 (c (n "tram") (v "0.1.0") (h "00zikzyx34gi3bb1z8ipf5ijzpykd1phxchr6q1nx8vhx7ih59s2")))

(define-public crate-tram-0.2.0 (c (n "tram") (v "0.2.0") (h "11y5n4srx15njpmawssx3k25c8glbl53kcfpp654hx0cybzsvcam")))

(define-public crate-tram-0.3.0 (c (n "tram") (v "0.3.0") (h "0yk65cpvjmdi8yr657ps2qmj24f0nsgdpin6007qbn2hrhjk4h5r") (y #t)))

(define-public crate-tram-0.3.1 (c (n "tram") (v "0.3.1") (h "1p437yibnk53npzs5j0mhs7gxskaimmxw2h9g7q11318v9y3f7rc")))

(define-public crate-tram-0.4.0 (c (n "tram") (v "0.4.0") (h "08139pjvby9wi5laf1s9prjacc89725m87z4ykahs4gyvrs0vrcl") (y #t)))

(define-public crate-tram-0.4.1 (c (n "tram") (v "0.4.1") (h "150ys3ql7n545v7nk8zv23a5w2fn7f2scgsblcc6dg0w0ynb08c6")))

(define-public crate-tram-0.4.2 (c (n "tram") (v "0.4.2") (h "0cg4hwrajxiccwr404lj6jjyz37j87zyrd401fnadk7nk2saslvy")))

(define-public crate-tram-0.5.0 (c (n "tram") (v "0.5.0") (h "19a1ayyhl214vwfshkh6v8prqy9rkh5p8m83dfn024xrpgwpaib4")))

