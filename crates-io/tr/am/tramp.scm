(define-module (crates-io tr am tramp) #:use-module (crates-io))

(define-public crate-tramp-0.1.0 (c (n "tramp") (v "0.1.0") (h "1g0a3adw6nc3y77wa62h4mb4nm613gsyxqv6aav8bgh4rnydvy7h")))

(define-public crate-tramp-0.2.0 (c (n "tramp") (v "0.2.0") (h "12zf4l5zs0m7x2v1y48rx92a88z13zvf8s0r5xifcj8x26klx5xq")))

(define-public crate-tramp-0.2.1 (c (n "tramp") (v "0.2.1") (h "09b5zh683z2r1r94d0va4bkahvgx8dj0pah4k7906pri6dimrghd")))

(define-public crate-tramp-0.3.0 (c (n "tramp") (v "0.3.0") (h "1x1c7c0xrijfckivjh81rmw9niafxarbplpyjfdq2blzv8p07vmi")))

