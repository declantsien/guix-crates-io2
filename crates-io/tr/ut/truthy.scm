(define-module (crates-io tr ut truthy) #:use-module (crates-io))

(define-public crate-truthy-0.0.1 (c (n "truthy") (v "0.0.1") (h "19ylyalnxf4lnwvl9vga7c1wwiyq1lb8gp0291cj35naq90vpdx7") (y #t)))

(define-public crate-truthy-1.0.0 (c (n "truthy") (v "1.0.0") (h "0zysxkyz39ki4awfwfx8cj6mycxhanvz54b2c0wv6lfww56icd74")))

(define-public crate-truthy-1.1.0 (c (n "truthy") (v "1.1.0") (d (list (d (n "either") (r "^1") (o #t) (d #t) (k 0)))) (h "08llbz30qpqdfq5qmpj34g28g980zsvwq1wdhw3zv3r9vzqx1sy6") (f (quote (("default") ("and-or" "either"))))))

