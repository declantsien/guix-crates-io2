(define-module (crates-io tr ut truth-table) #:use-module (crates-io))

(define-public crate-truth-table-0.1.0 (c (n "truth-table") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)))) (h "0655m71k59jkjxm8hwm5mqxvlcxqkpnf2v5rf1qizkvg8sbsngg1")))

(define-public crate-truth-table-0.2.0 (c (n "truth-table") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0gzn74kvlc5b110s1p2jpmdj9lr5c5jap9cbkc0biw3qnqbbr0m1")))

