(define-module (crates-io tr ut truthful) #:use-module (crates-io))

(define-public crate-truthful-0.1.0 (c (n "truthful") (v "0.1.0") (d (list (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "permutator") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml-base-config") (r "^0.1") (d #t) (k 0)))) (h "110xvzbxn9066fiad5wrbyl2qzj9aw2gjl17rhhvwj7akd9ph4pa")))

(define-public crate-truthful-0.1.1 (c (n "truthful") (v "0.1.1") (d (list (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "permutator") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml-base-config") (r "^0.1") (d #t) (k 0)))) (h "0vmqp6yn70w1pvg8az4k8qd94gy25wp6z1c5wz6sj5375qkr7h60")))

