(define-module (crates-io tr ez trezorlib) #:use-module (crates-io))

(define-public crate-trezorlib-0.0.2 (c (n "trezorlib") (v "0.0.2") (d (list (d (n "bitcoin") (r "^0.18.0") (d #t) (k 0)) (d (n "bitcoin-bech32") (r "^0.9.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "fern") (r "^0.5.6") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hid") (r "^0.3") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)) (d (n "secp256k1") (r "^0.12.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.7") (d #t) (k 0)))) (h "07f5hqix2mjcm6yx8bb6rvfb69v76gammkbq110v4gjzv98i5q4x")))

