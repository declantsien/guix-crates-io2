(define-module (crates-io tr ez trezor-crypto) #:use-module (crates-io))

(define-public crate-trezor-crypto-0.1.0 (c (n "trezor-crypto") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "trezor-crypto-sys") (r "^0.1.0") (d #t) (k 0)))) (h "159h130n48snz2fr1dgjapr38y4ij3hwvixax0p9bgff824brqzy")))

(define-public crate-trezor-crypto-0.2.0 (c (n "trezor-crypto") (v "0.2.0") (d (list (d (n "derivation-path") (r "^0.1.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "trezor-crypto-sys") (r "^0.1.0") (d #t) (k 0)))) (h "131d1chciv31qnxjafkr2dzh0bcn8ln8s0wv7kbwz1zci8syyrwr")))

(define-public crate-trezor-crypto-0.2.1 (c (n "trezor-crypto") (v "0.2.1") (d (list (d (n "derivation-path") (r "^0.1.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "trezor-crypto-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1gvyalw65syr9n6dqzv613h229vmpkcgqcirxsf18ca1v6f74fhd")))

(define-public crate-trezor-crypto-0.2.2 (c (n "trezor-crypto") (v "0.2.2") (d (list (d (n "derivation-path") (r "^0.1.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "trezor-crypto-sys") (r "^0.1.0") (d #t) (k 0)))) (h "08p0wz2ssw3wahqf0y8a3mrpaqsns076slb07qxqm06dpiawcbni")))

(define-public crate-trezor-crypto-0.2.3 (c (n "trezor-crypto") (v "0.2.3") (d (list (d (n "derivation-path") (r "^0.1.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "trezor-crypto-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1d8wlg46m0cpfcjy9sxcwaihdi9z83maq292j4a60rqqqm60gssc")))

(define-public crate-trezor-crypto-0.2.4 (c (n "trezor-crypto") (v "0.2.4") (d (list (d (n "derivation-path") (r "^0.1.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "trezor-crypto-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0nza3lfv0lvprldzch3qf2vb8qqkyri6k2r9xbwy3lshf6m5x5rq")))

(define-public crate-trezor-crypto-0.2.5 (c (n "trezor-crypto") (v "0.2.5") (d (list (d (n "derivation-path") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "trezor-crypto-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0rh2cfw0anfj1hn55m77m8rds04nswm861v0iy24i4mpqp9l79lw")))

