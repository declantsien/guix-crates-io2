(define-module (crates-io tr ex trex) #:use-module (crates-io))

(define-public crate-trex-0.1.5 (c (n "trex") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.7.2") (d #t) (k 2)) (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "0lxhb3khswa9b1cag9jgyr36ig82afnni1ha9xhpkjb32m6l28w9")))

(define-public crate-trex-0.1.6 (c (n "trex") (v "0.1.6") (d (list (d (n "ansi_term") (r "^0.7.2") (d #t) (k 2)) (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "1scw3hxzn68c912m4zk6b0wgad6a6s5k0gccch4qkdp55c574m57")))

(define-public crate-trex-0.2.0 (c (n "trex") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.7.2") (d #t) (k 2)) (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "08kc0qynkp5zmdg4bviib64s0w34w3lxf8z2qly959wyaqv2sjia")))

