(define-module (crates-io tr ex trexio) #:use-module (crates-io))

(define-public crate-trexio-2.4.0 (c (n "trexio") (v "2.4.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "16sglihniv2lspvz3hv7sz82xf5m55lc3zd0ql35kcanzxjgpcc1") (y #t)))

(define-public crate-trexio-2.4.1 (c (n "trexio") (v "2.4.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0b77ln4nl04af0x14xwi7khg47111c1b727rqvjaska10hahrgk0")))

(define-public crate-trexio-2.4.2 (c (n "trexio") (v "2.4.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "hdf5") (r "^0.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "0vsrqvlhqfdsqd432kq9zcscyldp0mzli32w2k2sw6vmbwdqbb5x")))

