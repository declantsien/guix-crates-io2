(define-module (crates-io tr ex trexter) #:use-module (crates-io))

(define-public crate-trexter-0.1.0 (c (n "trexter") (v "0.1.0") (h "1bcj5ajpncvmx55rljg5dxsh1ciwmld4g121sslk6k2f5mkiv2rw")))

(define-public crate-trexter-0.1.1 (c (n "trexter") (v "0.1.1") (h "1j8yc7advsarbzf1zb1naz0489sbx49zsr86n21bz8jh14c66lwr")))

