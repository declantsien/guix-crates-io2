(define-module (crates-io tr de trdelnik-test) #:use-module (crates-io))

(define-public crate-trdelnik-test-0.1.0 (c (n "trdelnik-test") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "12gh88x3p7xrf5wbjwjkf5xzd2szq0sk63hq5vg01019yamgh826")))

(define-public crate-trdelnik-test-0.1.1 (c (n "trdelnik-test") (v "0.1.1") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "1ad3a4d0zf6vf1l692sfdg7yzmlkysa1bb681md3j4b5824kbnwa")))

(define-public crate-trdelnik-test-0.1.2 (c (n "trdelnik-test") (v "0.1.2") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "15s2ga37p62kyz8v5nj1dxlmzv8m264f0n4j2yvcq8z2qwc2xcms")))

(define-public crate-trdelnik-test-0.2.0 (c (n "trdelnik-test") (v "0.2.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "19z1gkvim9jbrqw4y7wglfvdwzrprxsq5xby8f39jmns9byk12a8")))

(define-public crate-trdelnik-test-0.3.0 (c (n "trdelnik-test") (v "0.3.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "1zc8nh3i4bryk8pbj0gf6j38r547xjyqp2fni0fgafd5xcvbqhl9") (r "1.59")))

(define-public crate-trdelnik-test-0.3.1 (c (n "trdelnik-test") (v "0.3.1") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "091ai0p174pzxzm0rdsvrgrc9v52d298l4skdiwqcj96n3na7584")))

