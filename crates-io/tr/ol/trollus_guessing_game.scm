(define-module (crates-io tr ol trollus_guessing_game) #:use-module (crates-io))

(define-public crate-trollus_guessing_game-0.1.2 (c (n "trollus_guessing_game") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1z6ghgmj641d1h8v49ncvy04gzm553301dpffp2a3vy1yn69cpkg") (y #t)))

(define-public crate-trollus_guessing_game-0.1.3 (c (n "trollus_guessing_game") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0j09yhg0c8dwhh66w8gdyzlply4lpkyla9i2w2kihgfpvm395b2r")))

