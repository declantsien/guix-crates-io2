(define-module (crates-io tr ol trolling) #:use-module (crates-io))

(define-public crate-trolling-0.1.0 (c (n "trolling") (v "0.1.0") (d (list (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "winapi") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0") (d #t) (k 0)))) (h "0q9cwz40wm4slv5g6zl5f5yp964vsfm6h04jh8vvphmb47vdv7bq")))

(define-public crate-trolling-0.1.111111111111 (c (n "trolling") (v "0.1.111111111111") (d (list (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "winapi") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0") (d #t) (k 0)))) (h "145sny26h34pr6fwy4kzlgzy4yfrwvya9pvifyjkdqgkzck580jy")))

(define-public crate-trolling-0.1.11111112223621 (c (n "trolling") (v "0.1.11111112223621") (d (list (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "winapi") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0") (d #t) (k 0)))) (h "193h7wjydw7apf4xc4025cwp6afhbfyrn6yd7ixncr29q7hm7a86")))

