(define-module (crates-io tr yh tryhard) #:use-module (crates-io))

(define-public crate-tryhard-0.1.0 (c (n "tryhard") (v "0.1.0") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "pin-project") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.2.0, <0.3.0") (f (quote ("time" "macros" "test-util" "rt-core" "sync"))) (k 0)))) (h "1q5flhyzdc1n9njg4a58830fvfg58bsn27n9bavscgq9wys66pc8")))

(define-public crate-tryhard-0.2.0 (c (n "tryhard") (v "0.2.0") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "pin-project") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "tokio-02") (r ">=0.2.0, <0.3.0") (f (quote ("time" "macros" "test-util" "rt-core" "sync"))) (o #t) (k 0) (p "tokio")) (d (n "tokio-03") (r ">=0.3.0, <0.4.0") (f (quote ("rt" "time" "macros" "test-util" "sync"))) (o #t) (k 0) (p "tokio")))) (h "0njp6mfz714mdmhlkai8caw4c0fdc59bji9yp3vjgzsanzgrl81r") (f (quote (("default" "tokio-03"))))))

(define-public crate-tryhard-0.3.0 (c (n "tryhard") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio-02") (r "^0.2") (f (quote ("time" "macros" "test-util" "rt-core" "sync"))) (o #t) (k 0) (p "tokio")) (d (n "tokio-1") (r "^1") (f (quote ("rt" "time" "macros" "test-util" "sync"))) (o #t) (k 0) (p "tokio")))) (h "17bapnrs8115120vpmf0bx02wmf9mkrlwx9gjkpn42ds5v6qyzy7") (f (quote (("default" "tokio-1"))))))

(define-public crate-tryhard-0.4.0 (c (n "tryhard") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time" "macros" "test-util" "sync"))) (k 0) (p "tokio")))) (h "1ylj7gy6ak0bj3wyzfr8acv1qrcvliwm2pria1pm64249nw2gk9h")))

(define-public crate-tryhard-0.5.0 (c (n "tryhard") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time" "macros" "test-util" "sync"))) (k 0) (p "tokio")))) (h "14c6d1l82r9wkfbvmw9yzips09jl0g6b1zz09gp3lynnnm5cac3s")))

(define-public crate-tryhard-0.5.1 (c (n "tryhard") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time" "macros" "test-util" "sync"))) (k 0) (p "tokio")))) (h "17ffrwvmglc7fp2knsqi5qnwsm5s5n3hvzvcb0inks44jxq0m7ww")))

