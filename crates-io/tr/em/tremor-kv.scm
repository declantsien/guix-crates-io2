(define-module (crates-io tr em tremor-kv) #:use-module (crates-io))

(define-public crate-tremor-kv-0.1.0 (c (n "tremor-kv") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.2") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1wn0ivzbh2nmxs0n0977wxk7ms71mb6g2gpyngssqvjnxv9izr8w")))

(define-public crate-tremor-kv-0.1.1 (c (n "tremor-kv") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.3") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1cfm060d4l7kk23b0y5iaw82xxj5wxrcdfi2bdfshr5qq0x920hc")))

(define-public crate-tremor-kv-0.1.2 (c (n "tremor-kv") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.3") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1kw76hv2vp3v2ni5wxqglffd0jr4zs3bi0433pkq0n5hjwm70fjb")))

(define-public crate-tremor-kv-0.1.3 (c (n "tremor-kv") (v "0.1.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.3") (f (quote ("known-key"))) (d #t) (k 0)))) (h "0l504q11y2lv2nn7nqc78m9n7hqnqpngkr6vwi48wxcz1qqd2la8")))

(define-public crate-tremor-kv-0.1.4 (c (n "tremor-kv") (v "0.1.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.3") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1ha08k1yixcwjik791zgf6b3il0ikab2ksjs4b5w117ha4qphnsf")))

(define-public crate-tremor-kv-0.2.0 (c (n "tremor-kv") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.4") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1c3zklvbgl3zkq81k84w2yb0psn353anzlv4wppplfgn1zg6af1y")))

(define-public crate-tremor-kv-0.2.1 (c (n "tremor-kv") (v "0.2.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.5") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1vz2n8nvq08n5fksv18bc8y35wi1pqmn6sdbpgqjvw8s81xarx5g") (y #t)))

(define-public crate-tremor-kv-0.3.0 (c (n "tremor-kv") (v "0.3.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.5") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1slq3wxj611svclijvpp0z4zmjbxihr2x7m4xmm06433nzxwyb58")))

(define-public crate-tremor-kv-0.4.0 (c (n "tremor-kv") (v "0.4.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.6") (f (quote ("known-key"))) (d #t) (k 0)))) (h "0z2xfagdvdj8x6ll7rfg4ss7bllx77lbb72l967ds50jqhc2sp2k")))

(define-public crate-tremor-kv-0.5.0 (c (n "tremor-kv") (v "0.5.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.7") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1vd1nghn9dz3sr5f7rpln2h25lmkm87sw5m5ygjm56p6nac0mpxb")))

(define-public crate-tremor-kv-0.6.0 (c (n "tremor-kv") (v "0.6.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.10.3") (d #t) (k 0)))) (h "1ck9a7p21mfskm0r17a6xrjrvzc0n6zcjkrm2cdgi1l46x0058jc") (f (quote (("known-key" "simd-json/known-key") ("default" "known-key") ("arraybackend" "simd-json/arraybackend"))))))

(define-public crate-tremor-kv-0.6.1 (c (n "tremor-kv") (v "0.6.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.11.1") (d #t) (k 0)))) (h "0hl5zignhiq8innnbcxf3879w7jv00dcz0xi8q52r2jmj5q1dx2q") (f (quote (("known-key" "simd-json/known-key") ("default" "known-key") ("arraybackend" "simd-json/arraybackend"))))))

(define-public crate-tremor-kv-0.6.2 (c (n "tremor-kv") (v "0.6.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.13") (d #t) (k 0)))) (h "14labxapng4vknjb4d19ig0awb6ng4d32hhhjh5rjs64s20vf0bj") (f (quote (("known-key" "simd-json/known-key") ("default" "known-key") ("arraybackend" "simd-json/arraybackend"))))))

