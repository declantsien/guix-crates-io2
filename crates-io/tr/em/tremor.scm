(define-module (crates-io tr em tremor) #:use-module (crates-io))

(define-public crate-tremor-0.1.0 (c (n "tremor") (v "0.1.0") (d (list (d (n "serde") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.4.0") (d #t) (k 0)))) (h "0a9w3jddikq6mxhkq57sxpqiqi3gic4fmdfjzsxgnj3vp3533k4q")))

(define-public crate-tremor-0.1.1 (c (n "tremor") (v "0.1.1") (d (list (d (n "serde") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.4.0") (d #t) (k 0)))) (h "0nvw9a6lmvdbhv3rca28jwr4452dp64sk6pmpcp93gmdrgm7m70w")))

(define-public crate-tremor-0.1.2 (c (n "tremor") (v "0.1.2") (d (list (d (n "serde") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.4.0") (d #t) (k 0)))) (h "058qx7b5z1dj4wahvv0pxik39fjbs27qpz938ivmpb2c472sw64c")))

