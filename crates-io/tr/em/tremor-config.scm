(define-module (crates-io tr em tremor-config) #:use-module (crates-io))

(define-public crate-tremor-config-0.13.0-rc.18 (c (n "tremor-config") (v "0.13.0-rc.18") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.13") (d #t) (k 0)) (d (n "tremor-value") (r "^0.13.0-rc.18") (d #t) (k 0)))) (h "0i51hd8082cmgs8vv7g36xsfxwb5jr0zzql8v9a9d7d6kgz44ngr")))

(define-public crate-tremor-config-0.13.0-rc.20 (c (n "tremor-config") (v "0.13.0-rc.20") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.13") (d #t) (k 0)) (d (n "tremor-value") (r "^0.13.0-rc.20") (d #t) (k 0)))) (h "0zhdkibfvi9wgns3mb9za20jlg62c5c99z9681z4rrcfgsc63287")))

