(define-module (crates-io tr im trimmeroni) #:use-module (crates-io))

(define-public crate-trimmeroni-0.1.0 (c (n "trimmeroni") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0bmshiwjbrb9zzps0x8l6v9fcvgqn61110ann9h17wr0yvdzf8dr")))

(define-public crate-trimmeroni-0.2.1 (c (n "trimmeroni") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "11czn3y370p5qjsmg9d5gqawhfydmg0zaccgalk1ycgb7fgaljby")))

(define-public crate-trimmeroni-0.2.2 (c (n "trimmeroni") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1fan3p5hr4qpv3qvgh34wbgxri0bxljfv0yzsp6q737z8lp8nxnc")))

