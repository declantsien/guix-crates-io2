(define-module (crates-io tr im trim) #:use-module (crates-io))

(define-public crate-trim-0.1.0 (c (n "trim") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "08r40yd87688zvnb0hn5q2vmcpnqzdr5wj2impshsbbqfcbi7pba")))

(define-public crate-trim-0.1.1 (c (n "trim") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "12r7dvfc60hqfa8x52frabi3zqj5wp44hz5k2q1a4bmg2f1hx4kx")))

(define-public crate-trim-0.1.2 (c (n "trim") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0g7498bnnnj2ii5a6v0yi36fz2cf4cb1zkkrh7nkipg6lq2vb5cy")))

(define-public crate-trim-0.1.3 (c (n "trim") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1634169maxy01bsjjf6qa786jnd4s1kbik7zm6wxybpk0lnnw1ii")))

(define-public crate-trim-0.1.4 (c (n "trim") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1dva77s5vs4q0wf62mkfc0zmmggjqpl5llzj4aidhidf38bwfrx4")))

(define-public crate-trim-0.1.5 (c (n "trim") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1j82k2fi905k185lyrp5lz0k2n79sps26mwd4z13abhn5slq258p")))

(define-public crate-trim-1.0.0 (c (n "trim") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0kj802m0nd9q1nvg97n8l1v3gaiilvm6i0zxx3y8fr31i77j886h")))

(define-public crate-trim-1.0.1 (c (n "trim") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "19s7c6hvb7j2y2zvzakaqv0v9aq6r1h4qmdx4z928i0qk8sdbrx8")))

(define-public crate-trim-1.0.2 (c (n "trim") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1q88rrb3h43hc1bz6xvli36bf1s95cvazwm7mcnrkiwnihshg1kh")))

(define-public crate-trim-1.0.3 (c (n "trim") (v "1.0.3") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0qqsqp2a3h01qy8s0fn7b24yv26pz9aacixlvc9x2jpms5ypk824")))

(define-public crate-trim-1.1.0 (c (n "trim") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1hdglr0lzpi85k751syq4ndxdhh18qh7i5984lbbhv4cqf72948i")))

(define-public crate-trim-1.2.0 (c (n "trim") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "colmac") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0s5x5m5m2hxdxsdrimy24vw8wlyfmbfdsf7b6y800gqmhdqk3nqk")))

(define-public crate-trim-1.2.1 (c (n "trim") (v "1.2.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "colmac") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1nz8gd4yiyx8ifdvqcgy5xm71h6ghy1806mj146xd4djw71d3vwv")))

(define-public crate-trim-2.0.0 (c (n "trim") (v "2.0.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "colmac") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0ppw30vnd5zz479v7nym5l9227jzj8pbjf8ncc22r334xmmlp4dl")))

(define-public crate-trim-2.0.1 (c (n "trim") (v "2.0.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "colmac") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1hqp954arkdyki0gbm1nv96vhhy2dx14vxphz937ax476m2rxqgk")))

(define-public crate-trim-2.0.2 (c (n "trim") (v "2.0.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "colmac") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1531wnkkn0l02jsvwlwl4p1fkfiy3p5msa2sli9h5fwb2m7afi71")))

