(define-module (crates-io tr im trim_lines) #:use-module (crates-io))

(define-public crate-trim_lines-0.1.0 (c (n "trim_lines") (v "0.1.0") (h "0ihhiiynl57lazyx2qrr89ni5gvhnpbs8kxh95sq4bd21p0s2ps4")))

(define-public crate-trim_lines-0.1.1 (c (n "trim_lines") (v "0.1.1") (h "0irhin8qh6dj8gd0js6826vprb46m39sd8m07hid2gla1xsm66kd")))

(define-public crate-trim_lines-0.2.0 (c (n "trim_lines") (v "0.2.0") (h "1gpnaw2mmi0gkrmc59wizfik46l190yf1c42lchk7g7lk615zpl2")))

