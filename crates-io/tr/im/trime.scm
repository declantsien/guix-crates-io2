(define-module (crates-io tr im trime) #:use-module (crates-io))

(define-public crate-trime-0.1.0 (c (n "trime") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1mvm6kqjxhpklalq0ik3h939455jlq7qj94pjsfg88mm36cjlvm5") (y #t)))

(define-public crate-trime-0.1.1 (c (n "trime") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1pmkvs9jjj4pwlgl8m021p69wn7dzaglskjnm4cr89p476iqjk4d")))

