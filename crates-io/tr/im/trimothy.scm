(define-module (crates-io tr im trimothy) #:use-module (crates-io))

(define-public crate-trimothy-0.1.0 (c (n "trimothy") (v "0.1.0") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "1rz7rpnajc0qazwn4hpbmii461x8mabm55mxr55nsrp1mdzrc0z4") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-trimothy-0.1.1 (c (n "trimothy") (v "0.1.1") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "1ab4krhnirs96akhf3sl7s10z4x6x7irm2gqrj7201hcbhda59kf") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-trimothy-0.1.2 (c (n "trimothy") (v "0.1.2") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "17za93y96lrzaq5x0ikfcw5nsrdjzpgwpdnbpr28hn8ls706mdzf") (r "1.60")))

(define-public crate-trimothy-0.1.3 (c (n "trimothy") (v "0.1.3") (d (list (d (n "brunch") (r "0.2.*, >=0.2.5") (d #t) (k 2)))) (h "10ysdfa5azibliph4ix09g9zjrps6cw57h2qncd92d9wdpz0w3lf") (r "1.60")))

(define-public crate-trimothy-0.1.4 (c (n "trimothy") (v "0.1.4") (d (list (d (n "brunch") (r "0.3.*") (d #t) (k 2)))) (h "16n1p01i9ycjplr239j76g5ynlh2wy1zpscw0mq7g1vyyfxrjyrf") (r "1.56")))

(define-public crate-trimothy-0.1.5 (c (n "trimothy") (v "0.1.5") (d (list (d (n "brunch") (r "0.3.*") (d #t) (k 2)))) (h "14hf5ig6jahf96diph1mia09x33lpil0x3bipanw014pvbjma4gw") (r "1.56")))

(define-public crate-trimothy-0.1.6 (c (n "trimothy") (v "0.1.6") (d (list (d (n "brunch") (r "0.4.*") (d #t) (k 2)))) (h "1nxskjbsxlzj0hd7nvfqmarb0d417rwyzjd9k5szshayizjl8qnq") (r "1.56")))

(define-public crate-trimothy-0.1.7 (c (n "trimothy") (v "0.1.7") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)))) (h "1c35ldrvvhw713ysbxgbkkj9ik7d2v9az91gid1npjjj0xrdmh7x") (y #t) (r "1.56")))

(define-public crate-trimothy-0.1.8 (c (n "trimothy") (v "0.1.8") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)))) (h "0zhsq4lcpbyi8mzsk4a7a87ihhhnadf5d9i847xw003j9nxyxd34") (r "1.56")))

(define-public crate-trimothy-0.2.0 (c (n "trimothy") (v "0.2.0") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)))) (h "0cz5vwq5mrp6lxya6qxf56glbrc6db2sl365xj838j81pav4pins") (r "1.56")))

(define-public crate-trimothy-0.2.1 (c (n "trimothy") (v "0.2.1") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)))) (h "06ac928rg7jj4ymjpryhyix39cnk29w4xi9qnjvvmlf655iyfa7i") (r "1.56")))

(define-public crate-trimothy-0.2.2 (c (n "trimothy") (v "0.2.2") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)))) (h "175v0w3ziw5hvjlagmf25aq6s64qz193q3lisl7788nlfi4r2q1a") (r "1.56")))

