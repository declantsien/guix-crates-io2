(define-module (crates-io tr im trim-go-asm) #:use-module (crates-io))

(define-public crate-trim-go-asm-0.1.0 (c (n "trim-go-asm") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)))) (h "0g7h0b0s7w22z01w1vylkgmzfm952i5ifvbqgkcjynanxlxmfrnr") (r "1.56")))

(define-public crate-trim-go-asm-0.1.1 (c (n "trim-go-asm") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)))) (h "0kx8rjl4gz6r5pzv45inmbbq635krqqqk21r4i5l5fpr3s744bnc") (r "1.56")))

(define-public crate-trim-go-asm-0.1.3 (c (n "trim-go-asm") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.2") (d #t) (k 0)))) (h "0p5dkcrnc08673cqqph7i6aq1sfgqx3bq0aj9cq80rfpqx9cbzma") (r "1.56")))

