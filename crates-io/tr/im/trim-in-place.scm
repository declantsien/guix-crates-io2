(define-module (crates-io tr im trim-in-place) #:use-module (crates-io))

(define-public crate-trim-in-place-0.1.0 (c (n "trim-in-place") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0s0v5pdrn06l4icg7w881nf0ijmhnva9pjnx2ch9499br5jnh9sl")))

(define-public crate-trim-in-place-0.1.1 (c (n "trim-in-place") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1fnl8lmm0dc40fjd7v4v4hfi32wf4ym5xwy9nfx63avs4pak94c4")))

(define-public crate-trim-in-place-0.1.2 (c (n "trim-in-place") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1xcqdmyw82gzxvl8i276ss6clp8lr7nd1krcprsjk08a4b23mijb")))

(define-public crate-trim-in-place-0.1.3 (c (n "trim-in-place") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1aphqv50lg2bviyy7hy1wam78fqy2wz08k00bqg3ka8h1nwqsw39")))

(define-public crate-trim-in-place-0.1.4 (c (n "trim-in-place") (v "0.1.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "04mp26qzfb2anp9ji3y24nqs5hgi1kk61giyw97d6fzv3pa3han4")))

(define-public crate-trim-in-place-0.1.5 (c (n "trim-in-place") (v "0.1.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1czxinpf0qjhghyyjxd673ii4pyn1ad8fnkz1az1wc50vyw7nl51")))

(define-public crate-trim-in-place-0.1.6 (c (n "trim-in-place") (v "0.1.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "122a3pzv0ibxq5v1lnfcj5i4y4pzh6p31di3vqwpqs9ca859gbjh")))

(define-public crate-trim-in-place-0.1.7 (c (n "trim-in-place") (v "0.1.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1z04g79xkrpf3h4g3cc8wax72dn6h6v9l4m39zg8rg39qrpr4gil")))

