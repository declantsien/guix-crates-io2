(define-module (crates-io tr ef tref) #:use-module (crates-io))

(define-public crate-tref-0.1.0 (c (n "tref") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1qkhsisqll3x0ipfz9k2q53k7ms6valv74vbj4148a48gig9wfqg")))

(define-public crate-tref-0.1.1 (c (n "tref") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "10svzfgvgxgh0nc5206zfm42sfhjzwbphyhg45dww0sx9gbj5242")))

(define-public crate-tref-0.2.0 (c (n "tref") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "100w4h3kxbnwz5qd13q742lnmda296rs7ydvihqsz7ka9xrlnmw0")))

(define-public crate-tref-0.3.0 (c (n "tref") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "socarel") (r "^0.4.0") (d #t) (k 0)))) (h "1pc85gzq50b5226i2nq5vqi8nbc2l3b7x7ljyinnm1qdg70bca0f")))

(define-public crate-tref-0.4.0 (c (n "tref") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "socarel") (r "^0.4.0") (d #t) (k 0)))) (h "0dcqjqam8s84chkgdx7qna78asmy2f1c05y6q507ndpq5j86117w")))

