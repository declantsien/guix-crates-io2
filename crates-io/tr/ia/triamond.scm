(define-module (crates-io tr ia triamond) #:use-module (crates-io))

(define-public crate-triamond-0.0.1 (c (n "triamond") (v "0.0.1") (d (list (d (n "amethyst") (r "^0.15.1") (f (quote ("vulkan"))) (d #t) (k 0)))) (h "14c7i4qcysajdj8bgpmfmv5s9psb40kj6z808x2ms6x0fdj7kb0h") (y #t)))

(define-public crate-triamond-0.0.2-discontinued (c (n "triamond") (v "0.0.2-discontinued") (h "03ixq8mazg4vqr80rhal2kibd4is4v65y7an5iaxrbc7mi4nvy3i")))

