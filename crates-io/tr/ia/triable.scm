(define-module (crates-io tr ia triable) #:use-module (crates-io))

(define-public crate-triable-0.1.0 (c (n "triable") (v "0.1.0") (h "0cb4cxqx30is9ndzpvv67qy8dmyaaxyh5xbyj9s1c2wxqks9ibq8")))

(define-public crate-triable-0.1.1 (c (n "triable") (v "0.1.1") (h "1l2qsrx98m7apq23998vrbkgvackfzhyb29sjdb972wvsy8pvma2")))

(define-public crate-triable-0.1.2 (c (n "triable") (v "0.1.2") (h "121cldpbvsxpk7lcdjlyp812d9rqwc5l7ysz6kbp5x16vx3xv7cc")))

