(define-module (crates-io tr ia triangular-earth-calender-lib) #:use-module (crates-io))

(define-public crate-triangular-earth-calender-lib-0.1.0 (c (n "triangular-earth-calender-lib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dateparser") (r "^0.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "152yaxizy4gb339ij6xgh5c24h0kxx68vl774g9hh466m4sqri8x")))

