(define-module (crates-io tr ia triangle_matrix) #:use-module (crates-io))

(define-public crate-triangle_matrix-0.1.0 (c (n "triangle_matrix") (v "0.1.0") (h "08nl90lk7r6h77da2k4bd2ds7vjy715vll4bhvym513gj04zz74n")))

(define-public crate-triangle_matrix-0.1.1 (c (n "triangle_matrix") (v "0.1.1") (h "0x76xjkxhjaxpzyyhc7in1rlmk2l6rvb2drrm80jgam2mgfddycz")))

(define-public crate-triangle_matrix-0.2.0 (c (n "triangle_matrix") (v "0.2.0") (h "0z15h8rvax66110693lzvz5fp884nih6wx45a7yxmc4rqwwsnzwh")))

(define-public crate-triangle_matrix-0.2.1 (c (n "triangle_matrix") (v "0.2.1") (h "1jy00q46w9fj5ipwhca1zj3v269rrwqnxvia6yznzifp534hmi43")))

(define-public crate-triangle_matrix-0.2.2 (c (n "triangle_matrix") (v "0.2.2") (h "11za5v46mk32nw04kwnwzd3pn3f96g15n0k1qhjch7ik874ia2ki")))

(define-public crate-triangle_matrix-0.2.3 (c (n "triangle_matrix") (v "0.2.3") (h "1nqk5ny09ygw5z6n4ffiys6mhfbwsdhj214lsbrgqp1ln5irpqg7")))

(define-public crate-triangle_matrix-0.2.4 (c (n "triangle_matrix") (v "0.2.4") (h "0vkzsdnfxzri2zhgv4dqrfkyx86i14gg6183i9am8ba8g2gjshfh")))

(define-public crate-triangle_matrix-0.3.0 (c (n "triangle_matrix") (v "0.3.0") (h "0y3sgznxkhixmj2bdrmzp1smkl0i1kycd8rgnlm7xdkzrc8y2bl3")))

