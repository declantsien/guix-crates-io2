(define-module (crates-io tr ia triangle) #:use-module (crates-io))

(define-public crate-triangle-0.1.0 (c (n "triangle") (v "0.1.0") (h "08f0abykv53z1yw5zw04n0b7f42f6jj97rkah1smlhkg8sv1g2q8") (y #t)))

(define-public crate-triangle-0.1.1 (c (n "triangle") (v "0.1.1") (h "0b8j73d45k18wbm9y30sz24d5r6z471i7lxd7wvim25wr9lhxb2w") (y #t)))

(define-public crate-triangle-0.1.2 (c (n "triangle") (v "0.1.2") (h "1j94mfhz9ji2chmggkw0fwcz1acdkk38dfy551hcpszh2mvvsfhj") (y #t)))

(define-public crate-triangle-0.1.3 (c (n "triangle") (v "0.1.3") (h "0xhy9il1slnbwkdy96ppcgbb2bxwdyli959wvw8lz7q4b8hgb5jg") (y #t)))

(define-public crate-triangle-0.1.31 (c (n "triangle") (v "0.1.31") (h "0acy27d4nnic4nflciaqfznzijvgk28zigvi70zdjvhwxipj7ywf") (y #t)))

(define-public crate-triangle-0.1.311 (c (n "triangle") (v "0.1.311") (h "1xi7m8s1aqvll2km3rdyhwrh26zh0r86v0bfqm9xx9zlf7i12chc") (y #t)))

(define-public crate-triangle-0.1.32 (c (n "triangle") (v "0.1.32") (h "1h6ax04ypjvqqg0d4zwxnzihqj5hh4bcjdiya9jf9dcl7lxxcsql") (y #t)))

(define-public crate-triangle-0.1.34 (c (n "triangle") (v "0.1.34") (h "1nvy50cr8w4461ab3h7ail2cxf12wpz4wc5fl5nrpv8cvs9i717k") (y #t)))

(define-public crate-triangle-0.1.340 (c (n "triangle") (v "0.1.340") (h "06lv5y41b9jlyw19l3fgrypzzh1lpsxicz791z4c76qmxqmyyghp") (y #t)))

(define-public crate-triangle-0.1.341 (c (n "triangle") (v "0.1.341") (h "19qg5n1b8dfcyl9qwbfb9372f44ysa9ca8dqvv6phxy9jmbzszpk") (y #t)))

(define-public crate-triangle-0.1.342 (c (n "triangle") (v "0.1.342") (h "14mdpgbqxz3wn35frgpzf4ckd0klfxlyr1vr6f4j42zqa280wgzf")))

(define-public crate-triangle-0.1.344 (c (n "triangle") (v "0.1.344") (h "16rrv7rqr3x1r8dn0vjs89ms9w50kaqh183wfi5b40vk77b10sa7")))

(define-public crate-triangle-0.1.345 (c (n "triangle") (v "0.1.345") (h "1f05hwi2a0knv8qm3bnqm5gf0shixvnpzfxymgl0zvgh2vgalvq8")))

(define-public crate-triangle-0.1.346 (c (n "triangle") (v "0.1.346") (h "1q0dv0wv88pj6qwl920mypg5j40hh2i1dn46v81czppc2gyv3vk8")))

(define-public crate-triangle-0.1.347 (c (n "triangle") (v "0.1.347") (h "1cqjri5gn5cbkncrgz6g58n794bviarxnnippkihiz3nprgzc0w2")))

(define-public crate-triangle-0.1.348 (c (n "triangle") (v "0.1.348") (h "1a3ar6bmy6n277vz911s3jfvv40jx6qi8czsgk97a567z95kq4z5")))

(define-public crate-triangle-0.1.349 (c (n "triangle") (v "0.1.349") (h "0x72a0faz43jdm8ij98grazzc4nqqp4rgzgc2y5i83kck6y0akav")))

(define-public crate-triangle-0.1.350 (c (n "triangle") (v "0.1.350") (h "1gf4nc6rf800w50hwi44ncp9z829qb4w34fdcrlr48458b1rzy79")))

(define-public crate-triangle-0.1.351 (c (n "triangle") (v "0.1.351") (h "0jpk01nj1876a2jnn3aimlj0n2id7iaz6abyji5sj61k4ldqz3vl")))

(define-public crate-triangle-0.1.352 (c (n "triangle") (v "0.1.352") (h "17ys2nwdviz9akaw5mz2y63fka08yjkshcp4fy3fwgnwmnimzp48") (y #t)))

(define-public crate-triangle-0.1.353 (c (n "triangle") (v "0.1.353") (h "11gxr4z4cgnji0wcqq1jc53s86x4d6bwn3wj9bb46d410nnr2f31")))

(define-public crate-triangle-0.2.0 (c (n "triangle") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0b84b13fzhh9wymnhx9mf4gi5k9r8ms9c32q5hhpcmpllkrrk38b")))

