(define-module (crates-io tr ia trianglo) #:use-module (crates-io))

(define-public crate-trianglo-0.1.0 (c (n "trianglo") (v "0.1.0") (h "04788flrwyzjixlq6xsczm6jqs3haqynmqnk1r3y8bbx7krzb39p")))

(define-public crate-trianglo-0.1.1 (c (n "trianglo") (v "0.1.1") (h "0iylgnpfcw22w1ml8r5s9cxdlcvwxk2bc0xa29fznnxdia04awvg")))

(define-public crate-trianglo-0.1.2 (c (n "trianglo") (v "0.1.2") (h "1divyqqf5rgmdbwd83blrrhj7wmmzllx8b15smw2gbjyl4x2gl8c")))

