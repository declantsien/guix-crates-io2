(define-module (crates-io tr ia triaka-rpc-proto) #:use-module (crates-io))

(define-public crate-triaka-rpc-proto-0.1.0 (c (n "triaka-rpc-proto") (v "0.1.0") (d (list (d (n "bincode-json") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)))) (h "05k3ldyvbjbyn1i0631hdjiplh2k0wzizzfll4fw9nnyprpxyg7h") (y #t)))

(define-public crate-triaka-rpc-proto-0.2.0 (c (n "triaka-rpc-proto") (v "0.2.0") (d (list (d (n "bincode-json") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (o #t) (d #t) (k 0)))) (h "1v7xrrb833hd1kr1gzqs3r2wfv8i2swbi4qc8x2iajyk4wwn2yyj") (f (quote (("default" "nonblocking")))) (y #t) (s 2) (e (quote (("nonblocking" "dep:tokio"))))))

(define-public crate-triaka-rpc-proto-0.2.1 (c (n "triaka-rpc-proto") (v "0.2.1") (d (list (d (n "bincode-json") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (o #t) (d #t) (k 0)))) (h "1mmxwssa3fnixjarl9qkc38w123j5fmazjhkp8cykv4s0z4iic7l") (f (quote (("default" "nonblocking")))) (y #t) (s 2) (e (quote (("nonblocking" "dep:tokio"))))))

