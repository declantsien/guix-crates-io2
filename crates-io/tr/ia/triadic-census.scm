(define-module (crates-io tr ia triadic-census) #:use-module (crates-io))

(define-public crate-triadic-census-0.0.1 (c (n "triadic-census") (v "0.0.1") (d (list (d (n "petgraph") (r "*") (d #t) (k 0)))) (h "12b8b58mkv2k1xf95z742vd6sqkh7jlmx1x97j9gfz8y3jj0isqz")))

(define-public crate-triadic-census-0.1.0 (c (n "triadic-census") (v "0.1.0") (d (list (d (n "fixedbitset") (r "^0.1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.0") (d #t) (k 0)))) (h "1ys84pygy9wymyxyrrmn4sgxyvvadzhbr7q80brspq6ysqivndav")))

(define-public crate-triadic-census-0.1.1 (c (n "triadic-census") (v "0.1.1") (d (list (d (n "fixedbitset") (r "^0.1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.0") (d #t) (k 0)))) (h "0pb5vjnfdwgzym7dxygi0wnwx5dk0614mxx4fxag8lsrxsn7fb3x")))

(define-public crate-triadic-census-0.2.0 (c (n "triadic-census") (v "0.2.0") (d (list (d (n "fixedbitset") (r "^0.1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.0") (d #t) (k 0)))) (h "08nr3v11m0kqs86z62qra0b2iz175ac641za830g3waic1sv48bj")))

