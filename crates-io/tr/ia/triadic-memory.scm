(define-module (crates-io tr ia triadic-memory) #:use-module (crates-io))

(define-public crate-triadic-memory-0.1.0 (c (n "triadic-memory") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive" "env" "regex" "unicode"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "00c4nyy6s5rgj8xv9fky8nb4akbxl02n87yhb7bakykpal55fyw4")))

(define-public crate-triadic-memory-0.1.1 (c (n "triadic-memory") (v "0.1.1") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive" "env" "regex" "unicode"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0nf3w7qngz774bjyz02yji8h1g5y7irkk5z7a8rarzxcbdgj8qp9")))

