(define-module (crates-io tr ia triangular) #:use-module (crates-io))

(define-public crate-triangular-0.1.0 (c (n "triangular") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17.0") (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (k 2)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "19bz1y7akvbm2m84knpxhz2jn2j2fkq26jsk63yl6pzfp2mq4pz6")))

(define-public crate-triangular-0.1.1 (c (n "triangular") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.17.0") (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (k 2)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "068bdnhzp3qgpx13pg8j1yvnhcsyw6pqqh1r3x9xkm7arjl9lfa6")))

