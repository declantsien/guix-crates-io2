(define-module (crates-io tr ia triangulation) #:use-module (crates-io))

(define-public crate-triangulation-0.1.0 (c (n "triangulation") (v "0.1.0") (d (list (d (n "image") (r "^0.20.1") (d #t) (k 2)) (d (n "imageproc") (r "^0.17.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 2)))) (h "176pq711cn766f5c9fvvvs6hqbj0za8zws7vh54ri4xc2zsbyk3l") (f (quote (("parallel" "rayon") ("default"))))))

(define-public crate-triangulation-0.1.1 (c (n "triangulation") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "image") (r "^0.20.1") (d #t) (k 2)) (d (n "imageproc") (r "^0.17.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 2)))) (h "0ymkk6nr7r63hml27g6crlls78qp93994gafphsnby3rrlicshhd") (f (quote (("parallel" "rayon") ("default"))))))

