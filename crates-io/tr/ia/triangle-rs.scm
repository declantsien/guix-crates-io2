(define-module (crates-io tr ia triangle-rs) #:use-module (crates-io))

(define-public crate-triangle-rs-0.1.0 (c (n "triangle-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-pickle") (r "^0.6.2") (d #t) (k 0)) (d (n "spade") (r "^1.8.2") (d #t) (k 2)))) (h "0s4cnsqi7qmlbbz9cjm48hkj1dahhmmjfvvqm57p7nsd7jvmsf3b")))

(define-public crate-triangle-rs-0.1.1 (c (n "triangle-rs") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-pickle") (r "^0.6.2") (d #t) (k 0)) (d (n "spade") (r "^1.8.2") (d #t) (k 2)))) (h "0s8hg577pqgq1dvkigp1mf93cpvzq8lz024bpjghld598fgssz5n")))

(define-public crate-triangle-rs-0.1.2 (c (n "triangle-rs") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-pickle") (r "^0.6.2") (d #t) (k 0)) (d (n "spade") (r "^1.8.2") (d #t) (k 2)))) (h "0k6fdakpns3ikmp0as0nyv87prvkgchpf140m8l9vq0f5yy49b3r")))

