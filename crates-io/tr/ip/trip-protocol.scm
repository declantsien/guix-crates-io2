(define-module (crates-io tr ip trip-protocol) #:use-module (crates-io))

(define-public crate-trip-protocol-0.1.0 (c (n "trip-protocol") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0bv2c6jx326ag4m29fz5i9wrvg5mjip4w3dsrd4llng628p78b8r") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

