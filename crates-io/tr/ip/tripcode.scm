(define-module (crates-io tr ip tripcode) #:use-module (crates-io))

(define-public crate-tripcode-0.1.0 (c (n "tripcode") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "10lvax7w5m4rvs12ddsd4fnb7g1pa7gp54qamjbxhzcxwy0khnky")))

(define-public crate-tripcode-0.1.1 (c (n "tripcode") (v "0.1.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1wgpfr1qdiwg2kglx6hfwf9ibd0g22p75sj8vyaw2agx72wrcr7f")))

(define-public crate-tripcode-0.2.0 (c (n "tripcode") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0lf281lbijx38knkwl75ggs0aavvbaphia9giizp2fhzmnxvxc0v")))

(define-public crate-tripcode-0.2.1 (c (n "tripcode") (v "0.2.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1q19fic0hggxjblcwd858b0hnah8s2d3mzgq3l9krrihnq3a22f9")))

