(define-module (crates-io tr ip trip) #:use-module (crates-io))

(define-public crate-trip-0.1.0 (c (n "trip") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0gmbgmxk9sii3wspzsg1l2ngjjpv83xm422xkw705xbfg9c92rg0")))

(define-public crate-trip-0.1.1 (c (n "trip") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1j404hdnjlv9rk1x6brhdqam343wvrw1r5g13zv988jdkxxcs9lq")))

(define-public crate-trip-0.1.2 (c (n "trip") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1zdfyq5xr6b09hspm38gnbb59qfawb0lf33fwqxzwcfsqfp0crda")))

(define-public crate-trip-0.1.3 (c (n "trip") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "09995p2cap5wsh2mrcq32wbp644ixjviyp3r551h23599fjkh0vq")))

(define-public crate-trip-0.1.4 (c (n "trip") (v "0.1.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0iyr6sqa09if1lff9byf3dbk3625kczyzfaqzsbjmbfyrz7pvqsv")))

(define-public crate-trip-0.1.5 (c (n "trip") (v "0.1.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1yq3agkq80a530bs2188r26ddi2caby5hmqmmx7lc6mblsfjj1ah")))

(define-public crate-trip-0.1.6 (c (n "trip") (v "0.1.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1cd8r0kjkfh8d3kiibcfks6wq4bkg2d526p8nscylwq6444fcz1y")))

(define-public crate-trip-0.1.9 (c (n "trip") (v "0.1.9") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1iyw1fbhpp0zhbvwb9cgagj3mcfhby6gxayahb2d75zwrk2c9nvh")))

(define-public crate-trip-0.1.11 (c (n "trip") (v "0.1.11") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1zn52jz21mnhz9k8s5rfmlay1432cbiir14vi2nb4mm68f42d92j")))

(define-public crate-trip-0.1.12 (c (n "trip") (v "0.1.12") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1rh9n0l1rnwfxz3xal83n1z7bdbg1bh80m848x6l5mifdpra2bj1")))

