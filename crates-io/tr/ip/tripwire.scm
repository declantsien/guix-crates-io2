(define-module (crates-io tr ip tripwire) #:use-module (crates-io))

(define-public crate-tripwire-0.1.0-alpha.0 (c (n "tripwire") (v "0.1.0-alpha.0") (d (list (d (n "assert2") (r "^0.3.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.12") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0z37a7ni3kss2p5zsc16x77fxfbl6ybgl72dpbd6jl83ywbpsyz8")))

