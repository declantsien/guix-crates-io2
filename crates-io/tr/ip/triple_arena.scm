(define-module (crates-io tr ip triple_arena) #:use-module (crates-io))

(define-public crate-triple_arena-0.1.0 (c (n "triple_arena") (v "0.1.0") (d (list (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 2)))) (h "0sc451i2li6nmd3c1v7pwdb0jiq7blf0zi9qp3x263l0wzba31jv")))

(define-public crate-triple_arena-0.2.0 (c (n "triple_arena") (v "0.2.0") (d (list (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 2)))) (h "1sv1vqgp7bknxqvs63ihrmgqmhq1qyndrdqmp2ix8m18zpij0y7r")))

(define-public crate-triple_arena-0.3.0 (c (n "triple_arena") (v "0.3.0") (h "1zykjdhy4rl3hm0qf78mibwjhbj3d25p28daylknxinw5d6jmhb6")))

(define-public crate-triple_arena-0.3.1 (c (n "triple_arena") (v "0.3.1") (h "0icg3hbjbc51647fr98kivr04qrm1y3wfasqjdkr9fz2awz94n2n")))

(define-public crate-triple_arena-0.4.0 (c (n "triple_arena") (v "0.4.0") (h "1bx9lyilk6jwign6gsh7pjp298hs4pllwxncwic1jb4q02vjdnjv")))

(define-public crate-triple_arena-0.4.1 (c (n "triple_arena") (v "0.4.1") (h "0k9qf5hbsm1zh0zxlzi3g172n6kdvqqk4cnqs7rfhdyyr35lpcnk")))

(define-public crate-triple_arena-0.5.0 (c (n "triple_arena") (v "0.5.0") (h "035g2pnlfc80iw3zrprc21h1c4jl2xfq0pakh8l2g8nzb761x1g3")))

(define-public crate-triple_arena-0.6.0 (c (n "triple_arena") (v "0.6.0") (h "1qrsx3ga6v4pgp666w0fdj5ajxn6ri5jmkvyfmrmkdnm7j04n01w")))

(define-public crate-triple_arena-0.7.0 (c (n "triple_arena") (v "0.7.0") (h "0714z591mlqljsnv4nbm939xn2ysp5cr3c2ih35by1gmmgyh526n")))

(define-public crate-triple_arena-0.8.0 (c (n "triple_arena") (v "0.8.0") (h "0ljd495wf1agp5fid4k1bpw5ziggbdcm4yi2rjx8f80kzvsa9qy9")))

(define-public crate-triple_arena-0.9.0 (c (n "triple_arena") (v "0.9.0") (h "1jvy3dhbkzhha5dqm31riqqh9f9gm21b9r71qzsj4by3dla8yy2z")))

(define-public crate-triple_arena-0.10.0 (c (n "triple_arena") (v "0.10.0") (h "1f3qwj99ms86c4j6dz9s8i93wbhnz2yqlwp66y3wvcdmsf2g8fyf") (f (quote (("expose_internal_utils"))))))

(define-public crate-triple_arena-0.11.0 (c (n "triple_arena") (v "0.11.0") (d (list (d (n "recasting") (r "^0.1") (d #t) (k 0)))) (h "12ijl11szsclgv8avw2bvsq9m690ziqh1949k9gcvwq4mpav0csk") (f (quote (("std" "recasting/std") ("expose_internal_utils") ("default"))))))

(define-public crate-triple_arena-0.12.0 (c (n "triple_arena") (v "0.12.0") (d (list (d (n "recasting") (r "^0.2") (f (quote ("alloc"))) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1mrnzq1mb1b0dlmngmppgdlsfbvr1s57s8j2spz8drhrh3m50nkx") (f (quote (("std" "recasting/std") ("serde_support" "serde") ("expose_internal_utils") ("default"))))))

(define-public crate-triple_arena-0.12.1 (c (n "triple_arena") (v "0.12.1") (d (list (d (n "recasting") (r "^0.2") (f (quote ("alloc"))) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "06m0c9cmfx09bckcivsm1cpi2nzjvd7ics37k03vzxq48car7x7k") (f (quote (("std" "recasting/std") ("serde_support" "serde") ("expose_internal_utils") ("default"))))))

(define-public crate-triple_arena-0.13.0 (c (n "triple_arena") (v "0.13.0") (d (list (d (n "recasting") (r "^0.2") (f (quote ("alloc"))) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0vx2l71gmlly0rysa45lsqlf4sn120afxgwhil2ahbyc2ksh7xyr") (f (quote (("std" "recasting/std") ("serde_support" "serde") ("expose_internal_utils") ("default")))) (r "1.70")))

