(define-module (crates-io tr ip triple_accel) #:use-module (crates-io))

(define-public crate-triple_accel-0.1.0 (c (n "triple_accel") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "14bk6y55wy8yr63drcx5qlzxgnisjjjvm02jmsq41kb0091jqmf6") (f (quote (("jewel-sse") ("jewel-avx") ("jewel-8bit") ("jewel-32bit") ("jewel-16bit") ("default" "jewel-avx" "jewel-sse" "jewel-8bit" "jewel-16bit" "jewel-32bit"))))))

(define-public crate-triple_accel-0.2.0 (c (n "triple_accel") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0ixhf3wvk3y1nsyakhcrkv046cdszj92n0k6hc1gcsv86x3l6kwm") (f (quote (("jewel-sse") ("jewel-avx") ("jewel-8bit") ("jewel-32bit") ("jewel-16bit") ("default" "jewel-avx" "jewel-sse" "jewel-8bit" "jewel-16bit" "jewel-32bit"))))))

(define-public crate-triple_accel-0.2.1 (c (n "triple_accel") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1465vwid2rfl2y5xpgc74jma1vw02inn5vni2hzmnfvih9xj28qq") (f (quote (("jewel-sse") ("jewel-avx") ("jewel-8bit") ("jewel-32bit") ("jewel-16bit") ("default" "jewel-avx" "jewel-sse" "jewel-8bit" "jewel-16bit" "jewel-32bit"))))))

(define-public crate-triple_accel-0.2.2 (c (n "triple_accel") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0kzg1iv2sp48vgqpzq4ziyxd4zjl2gg7s6xvz7kxpj80nfg45vww") (f (quote (("jewel-sse") ("jewel-avx") ("jewel-8bit") ("jewel-32bit") ("jewel-16bit") ("default" "jewel-avx" "jewel-sse" "jewel-8bit" "jewel-16bit" "jewel-32bit"))))))

(define-public crate-triple_accel-0.3.0 (c (n "triple_accel") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0dy3cc641s4f57hb5wbdvbgqac6hplqi1qcwk4lvvvrw5khp525x") (f (quote (("jewel-sse") ("jewel-avx") ("jewel-8bit") ("jewel-32bit") ("jewel-16bit") ("default" "jewel-avx" "jewel-sse" "jewel-8bit" "jewel-16bit" "jewel-32bit"))))))

(define-public crate-triple_accel-0.3.1 (c (n "triple_accel") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "13rxm36q5wginid5i9zwzjjybbkna5a30l5zrpwzalixzlxskxqg") (f (quote (("jewel-sse") ("jewel-avx") ("jewel-8bit") ("jewel-32bit") ("jewel-16bit") ("default" "jewel-avx" "jewel-sse" "jewel-8bit" "jewel-16bit" "jewel-32bit"))))))

(define-public crate-triple_accel-0.3.2 (c (n "triple_accel") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1db6f00wvzlijm529s0m2z387dhv384czlwhq81j4sw0ir6z9zhk") (f (quote (("jewel-sse") ("jewel-avx") ("jewel-8bit") ("jewel-32bit") ("jewel-16bit") ("default" "jewel-avx" "jewel-sse" "jewel-8bit" "jewel-16bit" "jewel-32bit") ("debug"))))))

(define-public crate-triple_accel-0.3.3 (c (n "triple_accel") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "07r8jn210dbpsqby50894xn21y9z8aqw8h9i8kp5rg81r2nvhi31") (f (quote (("jewel-sse") ("jewel-avx") ("jewel-8bit") ("jewel-32bit") ("jewel-16bit") ("default" "jewel-avx" "jewel-sse" "jewel-8bit" "jewel-16bit" "jewel-32bit") ("debug"))))))

(define-public crate-triple_accel-0.3.4 (c (n "triple_accel") (v "0.3.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0v795l496crk3h6yff9zh1cjyrh5s9v23fbgccc4dpz25z70jav2") (f (quote (("jewel-sse") ("jewel-avx") ("jewel-8bit") ("jewel-32bit") ("jewel-16bit") ("default" "jewel-avx" "jewel-sse" "jewel-8bit" "jewel-16bit" "jewel-32bit") ("debug"))))))

(define-public crate-triple_accel-0.4.0 (c (n "triple_accel") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0qqyhl1pdvmfbx9fgw5jc15j42d0j1i7b6pzn42zsbzvbp4qn112") (f (quote (("jewel-sse") ("jewel-avx") ("jewel-8bit") ("jewel-32bit") ("jewel-16bit") ("default" "jewel-avx" "jewel-sse" "jewel-8bit" "jewel-16bit" "jewel-32bit") ("debug"))))))

