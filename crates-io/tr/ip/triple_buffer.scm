(define-module (crates-io tr ip triple_buffer) #:use-module (crates-io))

(define-public crate-triple_buffer-0.1.0 (c (n "triple_buffer") (v "0.1.0") (h "07kjfb1cnx7bvjy9ln0g17qqd75y6jzrnh3jrj5jcqyl324yc0nh")))

(define-public crate-triple_buffer-0.1.1 (c (n "triple_buffer") (v "0.1.1") (h "0ly1sivvq4rm3vkprdzlbxqscpkiv0ri40dbbdn1a21cw65315cv")))

(define-public crate-triple_buffer-0.2.0 (c (n "triple_buffer") (v "0.2.0") (h "0skknwz71d4w3s8g8lqnynib1bv189b2mh3q5lvfa8bbnrmqbyai") (y #t)))

(define-public crate-triple_buffer-0.2.1 (c (n "triple_buffer") (v "0.2.1") (h "0c0qmgnml0dizxmm2spvbfnks2amshikzkaqizcx6qdrkli2ywxn")))

(define-public crate-triple_buffer-0.2.2 (c (n "triple_buffer") (v "0.2.2") (h "0r7ri8avcz1zp67j5znz626rc534ma5qvlhff69zrw3xls7rmcpk")))

(define-public crate-triple_buffer-0.2.3 (c (n "triple_buffer") (v "0.2.3") (h "08xgnhkk7gp3jy04h2ymafxb6isadl05hlf919nrg7f9vvlrgfav")))

(define-public crate-triple_buffer-0.2.4 (c (n "triple_buffer") (v "0.2.4") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "0lx3lqp9ghldrnxpphhjkf0dlk9lyb61npm117mz8z1q6gy6yvx0")))

(define-public crate-triple_buffer-0.3.0 (c (n "triple_buffer") (v "0.3.0") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "11dvzq3846yrlq23dm5xb3czjv3na4idq5km2wpqqldhxpnqnxj9")))

(define-public crate-triple_buffer-0.3.1 (c (n "triple_buffer") (v "0.3.1") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "0z70nhl2q62pwg6dg9hs76kxsdmz0bir878z519ipk8wzwbjzbfq")))

(define-public crate-triple_buffer-0.3.2 (c (n "triple_buffer") (v "0.3.2") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "0mq2273675q5x8wv45r4640a4162as21rggr63dlg9dgzj8bp3sj")))

(define-public crate-triple_buffer-0.3.3 (c (n "triple_buffer") (v "0.3.3") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "0gbl8j4861xn2rkhcrnw98cd20b47nca2xxsxgar0nr4ji0sdvk7")))

(define-public crate-triple_buffer-0.3.4 (c (n "triple_buffer") (v "0.3.4") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "1hcxnss7c0w4iasxr7zf5ph51rysnpwcg8jilzq17dzwb7rwv1v4")))

(define-public crate-triple_buffer-1.0.0 (c (n "triple_buffer") (v "1.0.0") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "0f7glrbn0xvn911l9ig4gzsdvj0jiwf6g6hz8bppydvfp5l9lh8h")))

(define-public crate-triple_buffer-1.1.0 (c (n "triple_buffer") (v "1.1.0") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "0675d4y77l6ydb31lnz2bln38dh3m522b2xlcy800wqvn8aac2xk") (f (quote (("raw"))))))

(define-public crate-triple_buffer-1.1.1 (c (n "triple_buffer") (v "1.1.1") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "1bfi013s4a4mdyik0482v29sp499qw9acnvm9mn1bxydgiv1h6ym") (f (quote (("raw"))))))

(define-public crate-triple_buffer-2.0.0 (c (n "triple_buffer") (v "2.0.0") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "08lfxjlj6a4777aa8ylk595hqqlxy0wk71dr59lrc5smpffpqqjr") (f (quote (("raw"))))))

(define-public crate-triple_buffer-3.0.0 (c (n "triple_buffer") (v "3.0.0") (d (list (d (n "crossbeam-utils") (r "^0.5") (d #t) (k 0)) (d (n "testbench") (r "^0.5") (d #t) (k 0)))) (h "1glx0akzl2yg9knnx7k1axy6biwsjcsdmwg4s6w5lhjnfbi1y17f") (f (quote (("raw"))))))

(define-public crate-triple_buffer-3.0.1 (c (n "triple_buffer") (v "3.0.1") (d (list (d (n "crossbeam-utils") (r "^0.5") (d #t) (k 0)) (d (n "testbench") (r "^0.5") (d #t) (k 2)))) (h "1a7q0z0zi6sy42n9dqhcgzsm3jdkzd61ll0jk6av9qlpn85qvmfc") (f (quote (("raw"))))))

(define-public crate-triple_buffer-4.0.0 (c (n "triple_buffer") (v "4.0.0") (d (list (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "testbench") (r "^0.6") (d #t) (k 2)))) (h "1winnhcwhqfhvs4r7lwx12ivvzbrcpja21vzng0dgwmxkdinqqg3") (f (quote (("raw"))))))

(define-public crate-triple_buffer-4.0.1 (c (n "triple_buffer") (v "4.0.1") (d (list (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "testbench") (r "^0.6") (d #t) (k 2)))) (h "1k9qbj7zxnilcbzbyhvx96994famrzfhcnv1nxm9bs2jhrmjm8jn") (f (quote (("raw"))))))

(define-public crate-triple_buffer-5.0.0 (c (n "triple_buffer") (v "5.0.0") (d (list (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "testbench") (r "^0.6") (d #t) (k 2)))) (h "1ii958wxcl7bgd1g1h70q53wmb386z7h88d3iy9fg3idj3qpx103") (f (quote (("raw"))))))

(define-public crate-triple_buffer-5.0.1 (c (n "triple_buffer") (v "5.0.1") (d (list (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "testbench") (r "^0.7") (d #t) (k 2)))) (h "1lxvs438b9lk3zwbp0cfzjvmnr11fzk4bxjhz16sqn7fcg2p1dil") (f (quote (("raw"))))))

(define-public crate-triple_buffer-5.0.2 (c (n "triple_buffer") (v "5.0.2") (d (list (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "testbench") (r "^0.7") (d #t) (k 2)))) (h "0asrys5bdqy7699ddhym635vfx0sddmpilv8z551mvg85l0kp9i6") (f (quote (("raw"))))))

(define-public crate-triple_buffer-5.0.3 (c (n "triple_buffer") (v "5.0.3") (d (list (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "testbench") (r "^0.7") (d #t) (k 2)))) (h "0h8lawhp2ws7sd26n6b0azjaf8rg2bxw7s864w28hgw2ib0ifj33") (f (quote (("raw"))))))

(define-public crate-triple_buffer-5.0.4 (c (n "triple_buffer") (v "5.0.4") (d (list (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "testbench") (r "^0.7") (d #t) (k 2)))) (h "0y0ldysbvkpah3n6bvh8iri8rqyjca7qj0zfp45cvni4avn25qgz") (f (quote (("raw"))))))

(define-public crate-triple_buffer-5.0.5 (c (n "triple_buffer") (v "5.0.5") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "testbench") (r "^0.7") (d #t) (k 2)))) (h "04k503hd4yvx0jssp1brgbw353s54y5f18kby1lzyvlz4ai7ymq6") (f (quote (("raw"))))))

(define-public crate-triple_buffer-5.0.6 (c (n "triple_buffer") (v "5.0.6") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "testbench") (r "^0.8") (d #t) (k 2)))) (h "18x1f79p352kkw775r3xdls830xsjwazl6hiv39p0yirm3jncfc0") (f (quote (("raw"))))))

(define-public crate-triple_buffer-6.0.0 (c (n "triple_buffer") (v "6.0.0") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "testbench") (r "^0.8") (d #t) (k 2)))) (h "169dky0327skdvsc2c46gl9ddh81ng1fyvwrwjxr7scq2bhgc51p") (r "1.46")))

(define-public crate-triple_buffer-6.1.0 (c (n "triple_buffer") (v "6.1.0") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "testbench") (r "^0.8") (d #t) (k 2)))) (h "0kxap73633sp76vq94hljcpqxm15siz23v6y0d59ivw69lpdff6j") (r "1.46")))

(define-public crate-triple_buffer-6.2.0 (c (n "triple_buffer") (v "6.2.0") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "testbench") (r "^0.8") (d #t) (k 2)))) (h "1fdc3pb2sf24z69255n6g8i6kz36rx0s7k003n1l3x37q089xpl8") (r "1.46")))

(define-public crate-triple_buffer-7.0.0 (c (n "triple_buffer") (v "7.0.0") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "crossbeam-utils") (r "^0.8.11") (k 0)) (d (n "testbench") (r "^1.0") (d #t) (k 2)))) (h "1vfandjfg25bzxb4qfa1sch7mx0qk07va3m7p205kkjj0aq1lvii") (f (quote (("miri")))) (r "1.70")))

