(define-module (crates-io tr ip triple) #:use-module (crates-io))

(define-public crate-triple-0.0.1 (c (n "triple") (v "0.0.1") (d (list (d (n "clippy") (r "^0.0.96") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "1r2ginlzm8inkgjvfgdc9prrilcsi9gr6ik4bggfkqqr72vawnqf")))

(define-public crate-triple-0.0.2 (c (n "triple") (v "0.0.2") (d (list (d (n "clippy") (r "^0.0.96") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "01v2bg4fm09pvhf7vmccanhslwz4d73l44k1y5djkjal3mc7cj9y")))

