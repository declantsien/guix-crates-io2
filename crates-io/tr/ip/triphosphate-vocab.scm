(define-module (crates-io tr ip triphosphate-vocab) #:use-module (crates-io))

(define-public crate-triphosphate-vocab-0.0.0 (c (n "triphosphate-vocab") (v "0.0.0") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "cid") (r "^0.10.1") (d #t) (k 0)) (d (n "libipld") (r "^0.16.0") (f (quote ("dag-cbor" "derive"))) (k 0)) (d (n "oxilangtag") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "winnow") (r "^0.5.10") (d #t) (k 0)))) (h "1bzdgx76r1w1pky434yfsxi2ljr1vpj7h44qxzfahwkl7m58k3fz")))

