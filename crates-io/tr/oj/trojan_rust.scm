(define-module (crates-io tr oj trojan_rust) #:use-module (crates-io))

(define-public crate-trojan_rust-0.1.0 (c (n "trojan_rust") (v "0.1.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rustls") (r "^0.22") (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)))) (h "1gz12paa5pm1iymw4sdhi2p9zxykbw77ydy3j7bgr4cm7pr40mph")))

