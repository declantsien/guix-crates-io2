(define-module (crates-io tr mv trmv) #:use-module (crates-io))

(define-public crate-trmv-1.0.0 (c (n "trmv") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1bcca1nagqa1x1nynzbx3hsxdmxzq22kyx54iygq7cm1mar7a9h0")))

(define-public crate-trmv-1.0.1 (c (n "trmv") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "09rqndirynmdl3m68klwjiz7ilkc4yglhzf1p7vjy6dlzg9h1v5w")))

(define-public crate-trmv-1.0.2 (c (n "trmv") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0xl7rz87an2ndih5qld6wj1vxrm5ps68q53mgar7yhlmhw947k0q")))

