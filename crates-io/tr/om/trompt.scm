(define-module (crates-io tr om trompt) #:use-module (crates-io))

(define-public crate-trompt-0.0.1 (c (n "trompt") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (d #t) (k 0)))) (h "1gka2kspaxnn1lifgrwh0pqngy4cfmq40jzd29wy393r9knn6jhc")))

(define-public crate-trompt-0.0.2 (c (n "trompt") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (d #t) (k 0)))) (h "1b6flj1m7yck3wx50arj9v4w2wafb85wx12iip0qqnpk1wmdcbca")))

(define-public crate-trompt-0.0.3 (c (n "trompt") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (d #t) (k 0)))) (h "0hy5hviz4slg2787jfww7lka2rywjga1mdzsmcrw108kw80zw92d")))

(define-public crate-trompt-0.0.4 (c (n "trompt") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (d #t) (k 0)))) (h "1m96win54x223pimz9njq0f3qmvy1b457rl11nca8ghm6bvxcan4")))

