(define-module (crates-io tr iv trivial-verifier) #:use-module (crates-io))

(define-public crate-trivial-verifier-0.1.0 (c (n "trivial-verifier") (v "0.1.0") (d (list (d (n "mmb-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "trivial-kernel") (r "^0.2.0") (d #t) (k 0)))) (h "1vww656kdnba4pqf50qlhazxlb3i18c5hnb2zkzlsbsd82n1iy9h")))

(define-public crate-trivial-verifier-0.2.0 (c (n "trivial-verifier") (v "0.2.0") (d (list (d (n "mmb-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "trivial-compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "trivial-kernel") (r "^0.3.0") (d #t) (k 0)))) (h "0s4f1l9hl7j2h4prhglcacpgjbi5hw6sxb5wz7jb0f5zjp7vvyxg")))

(define-public crate-trivial-verifier-0.3.0 (c (n "trivial-verifier") (v "0.3.0") (d (list (d (n "mmb-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "trivial-compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "trivial-kernel") (r "^0.4.0") (d #t) (k 0)))) (h "0mfhwih4prxbfb9n6348szrisyn46dba13cl4fxqx6s1ddqk9fpd")))

(define-public crate-trivial-verifier-0.4.0 (c (n "trivial-verifier") (v "0.4.0") (d (list (d (n "mmb-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "trivial-compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "trivial-kernel") (r "^0.5.0") (d #t) (k 0)))) (h "04c2ammaqsnbqv2f11zdjqaa5p5dq7vj2dhg5xls4sgy3w0j7imz")))

(define-public crate-trivial-verifier-0.5.0 (c (n "trivial-verifier") (v "0.5.0") (d (list (d (n "mmb-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "trivial-compiler") (r "^0.2.0") (d #t) (k 0)) (d (n "trivial-kernel") (r "^0.5.0") (d #t) (k 0)))) (h "0dvrmwpl0hdkz979syk2cw0h6ydp96wjysninlvxxhs7rg2zpbzb")))

(define-public crate-trivial-verifier-0.6.0 (c (n "trivial-verifier") (v "0.6.0") (d (list (d (n "mmb-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "trivial-compiler") (r "^0.2.0") (d #t) (k 0)) (d (n "trivial-kernel") (r "^0.5.0") (d #t) (k 0)))) (h "06hv9n0r6lpkalylnqyipg1smaf9jib8v7clq87ghdk9j45a5in6")))

(define-public crate-trivial-verifier-0.7.0 (c (n "trivial-verifier") (v "0.7.0") (d (list (d (n "mmb-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "trivial-compiler") (r "^0.2.0") (d #t) (k 0)) (d (n "trivial-kernel") (r "^0.7.0") (d #t) (k 0)))) (h "1r1wawd1xa7s293gs5x4ng63gzb5rzc9n8q44i604gjamgm0kmph")))

(define-public crate-trivial-verifier-0.8.0 (c (n "trivial-verifier") (v "0.8.0") (d (list (d (n "mmb-parser") (r "^0.5.0") (d #t) (k 0)) (d (n "trivial-compiler") (r "^0.3.0") (d #t) (k 0)) (d (n "trivial-kernel") (r "^0.8.0") (d #t) (k 0)))) (h "10j5c96n1ppdwmjfqdzls33jzx774zy0yj8m53z2rzg33ksj490y")))

