(define-module (crates-io tr iv trivial-compiler) #:use-module (crates-io))

(define-public crate-trivial-compiler-0.1.0 (c (n "trivial-compiler") (v "0.1.0") (d (list (d (n "mmb-types") (r "^0.2.0") (d #t) (k 0)))) (h "1qdzadzjdksr9vy76hskwamjv8wp40gckicsgwfp7vg3mhmf3d5r")))

(define-public crate-trivial-compiler-0.2.0 (c (n "trivial-compiler") (v "0.2.0") (d (list (d (n "mmb-types") (r "^0.2.0") (d #t) (k 0)))) (h "1lwhck9ws97nshdjq8xlvfc14g1vy0vyqxapa21zmx3r5r8495gx")))

(define-public crate-trivial-compiler-0.3.0 (c (n "trivial-compiler") (v "0.3.0") (d (list (d (n "mmb-types") (r "^0.3.0") (d #t) (k 0)))) (h "0lvvmik5j517nqvvfyqgq6axzm0lrlcc544svm8pxmx3z26i4s8v")))

