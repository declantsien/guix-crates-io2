(define-module (crates-io tr iv trivet) #:use-module (crates-io))

(define-public crate-trivet-1.0.0 (c (n "trivet") (v "1.0.0") (h "1nwqdwwkdgabsfkj28kk5g3d0k0difdhm7ssw3a0fl8pqiarff31") (f (quote (("strict"))))))

(define-public crate-trivet-1.0.1 (c (n "trivet") (v "1.0.1") (h "09lzikimrjbz5x601aw6m8zqxyh950ar02i598ar63bs71g5dvys") (f (quote (("strict"))))))

(define-public crate-trivet-1.0.2 (c (n "trivet") (v "1.0.2") (h "0q1n3srjsflj8z9vkvlvn8im8lls3598d77c219qra8rzy0p12lw") (f (quote (("strict"))))))

(define-public crate-trivet-1.0.3 (c (n "trivet") (v "1.0.3") (h "0r2vk5hmblsdbp3g6fvakgjwxr7r49kqz8a5mqc6drlai6mpd4qj") (f (quote (("strict"))))))

(define-public crate-trivet-1.1.0 (c (n "trivet") (v "1.1.0") (h "0i4fmwsv3phmy34mafbxqqcj54pglf36anr0p0psbzg3h0gf3mv6") (f (quote (("strict"))))))

(define-public crate-trivet-1.1.1 (c (n "trivet") (v "1.1.1") (h "1ch2lh8s09sgnwdfy38dl30ny8n7lrmbalhjqv0lxag9biaipb5i") (f (quote (("strict"))))))

(define-public crate-trivet-2.0.0 (c (n "trivet") (v "2.0.0") (h "02dkjb5cj1081b4mady1fmvif3n0722jvnsccw21crpd48cxx2ss") (f (quote (("uppercase_hex") ("strict") ("no_ucd") ("no_tracking") ("no_stall_detection") ("legacy") ("default" "legacy"))))))

(define-public crate-trivet-2.0.1 (c (n "trivet") (v "2.0.1") (h "0brvda51148lvmgv99cxinh670j9nlj91vwdafn823j7334fm454") (f (quote (("uppercase_hex") ("strict") ("no_ucd") ("no_tracking") ("no_stall_detection") ("legacy") ("default" "legacy"))))))

(define-public crate-trivet-2.0.2 (c (n "trivet") (v "2.0.2") (h "19nza3nr04b93vaw7qyl1b7swpybgkysf0mvf0njfhb2ssvlax3f") (f (quote (("uppercase_hex") ("strict") ("no_ucd") ("no_tracking") ("no_stall_detection") ("legacy") ("default" "legacy"))))))

