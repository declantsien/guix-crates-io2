(define-module (crates-io tr iv trivial-kernel) #:use-module (crates-io))

(define-public crate-trivial-kernel-0.1.0 (c (n "trivial-kernel") (v "0.1.0") (h "1npa184r4qmslnbkfmxi0749ls5qp5smf8g63qr9n27hz7qp22yw")))

(define-public crate-trivial-kernel-0.2.0 (c (n "trivial-kernel") (v "0.2.0") (d (list (d (n "mmb-types") (r "^0.2.0") (d #t) (k 0)))) (h "1fidi1z5sxap8vym5604cq65714dns8fdimhfck2ibalp65zz655")))

(define-public crate-trivial-kernel-0.3.0 (c (n "trivial-kernel") (v "0.3.0") (d (list (d (n "mmb-types") (r "^0.2.0") (d #t) (k 0)))) (h "0f46cdy1kpzd9pa4wc2vq21y5dirsfmg4nqap834syff5813iqbd")))

(define-public crate-trivial-kernel-0.4.0 (c (n "trivial-kernel") (v "0.4.0") (d (list (d (n "mmb-types") (r "^0.2.0") (d #t) (k 0)))) (h "1nz5jylkkj655f015a1fnlcd4smsmjvyxa82fbv18ry7x0fyxd0v")))

(define-public crate-trivial-kernel-0.5.0 (c (n "trivial-kernel") (v "0.5.0") (d (list (d (n "mmb-types") (r "^0.2.0") (d #t) (k 0)))) (h "0ydk72wm9icfp0zi8pp4cnxba9krrglnmxxcpc4wa6qdv0xplq30")))

(define-public crate-trivial-kernel-0.6.0 (c (n "trivial-kernel") (v "0.6.0") (d (list (d (n "mmb-types") (r "^0.2.0") (d #t) (k 0)))) (h "1pf3rkvj8gghj6h4qwzjmlya82v4d9lqb70bvkhpja30d76flaka")))

(define-public crate-trivial-kernel-0.7.0 (c (n "trivial-kernel") (v "0.7.0") (d (list (d (n "mmb-types") (r "^0.2.0") (d #t) (k 0)))) (h "0fx0j6mvxrf978xaz5r6mcnaj6bm13bzs1xrrldrnxrh8f0q23fv")))

(define-public crate-trivial-kernel-0.8.0 (c (n "trivial-kernel") (v "0.8.0") (d (list (d (n "mmb-types") (r "^0.3.0") (d #t) (k 0)))) (h "14ipqz3y1lx6xk5m3r3vqis44zx91rna0y493bnbxjlgvlbhdr4v")))

(define-public crate-trivial-kernel-0.8.1 (c (n "trivial-kernel") (v "0.8.1") (d (list (d (n "mmb-types") (r "^0.3.0") (d #t) (k 0)))) (h "1jgfj2qr7wmhdvp4nbr29ir418blk5l56y1xv35jk5ix69l576hi")))

(define-public crate-trivial-kernel-0.9.0 (c (n "trivial-kernel") (v "0.9.0") (d (list (d (n "mmb-types") (r "^0.3.0") (d #t) (k 0)))) (h "0n63j9dx6yacsc4i0pca9lzzykddq89l24567ik13ndvnld2rw73")))

