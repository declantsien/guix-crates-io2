(define-module (crates-io tr iv trivialdb) #:use-module (crates-io))

(define-public crate-trivialdb-0.1.0 (c (n "trivialdb") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0v3vjijg1nlg0h38pb7cclnshpf71nnmhdzp7r7mk8nc4v2d431y")))

(define-public crate-trivialdb-0.1.1 (c (n "trivialdb") (v "0.1.1") (d (list (d (n "bindgen") (r ">=0.65") (d #t) (k 1)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0nndfag9rgswrjp77cnsqmhxwdfapv7j0qpz13vq0wgw9q092hss")))

(define-public crate-trivialdb-0.1.2 (c (n "trivialdb") (v "0.1.2") (d (list (d (n "bindgen") (r ">=0.65") (d #t) (k 1)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0v5ax94lkk84b2idjrbs8npb9yy62w33dp46pbsp500l90npcydr")))

(define-public crate-trivialdb-0.1.3 (c (n "trivialdb") (v "0.1.3") (d (list (d (n "bindgen") (r ">=0.65") (d #t) (k 1)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.1.1") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "12nxwg91ph6ii54ns1j5p5vm673vkbpknjyyx2bd8q86zhxwmw3y")))

(define-public crate-trivialdb-0.1.4 (c (n "trivialdb") (v "0.1.4") (d (list (d (n "bindgen") (r ">=0.65") (d #t) (k 1)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.1.1") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "18cc1dglphxq37s6ds8brxwjhzd3mb8ly9ppakjawn11p0vmyiy8")))

