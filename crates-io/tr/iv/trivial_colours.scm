(define-module (crates-io tr iv trivial_colours) #:use-module (crates-io))

(define-public crate-trivial_colours-0.1.0 (c (n "trivial_colours") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0csyc1hf0mg4alr82q87gznrh74hpcvg71c9rkz0yyr48zw1kgcn") (y #t)))

(define-public crate-trivial_colours-0.1.1 (c (n "trivial_colours") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1dc1myjg73zawdmisadn0yns79cpmkg6hnwad6ixa76i496nwc18")))

(define-public crate-trivial_colours-0.2.0 (c (n "trivial_colours") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2") (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.2") (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1a4519f7crp7i3y2bkysnk72pvq3h0z232y80zam5ss6i2m2dpq3") (f (quote (("use_winapi" "kernel32-sys" "lazy_static" "winapi") ("default" "use_winapi"))))))

(define-public crate-trivial_colours-0.2.1 (c (n "trivial_colours") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2") (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.2") (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0sg8b1j6pygfjv8ijx6xfd5hxkqvlz0pjsbfz5al1ki9wjqrfd45") (f (quote (("use_winapi" "kernel32-sys" "lazy_static" "winapi") ("default" "use_winapi"))))))

(define-public crate-trivial_colours-0.3.0 (c (n "trivial_colours") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "^0.2") (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.2") (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0n5lddjkj1hkh876g7y4j0fv59ir6frs2kdwxvi0qnkcl5g3clvi") (f (quote (("use_winapi" "kernel32-sys" "lazy_static" "winapi") ("default" "use_winapi"))))))

