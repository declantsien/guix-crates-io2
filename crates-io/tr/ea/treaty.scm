(define-module (crates-io tr ea treaty) #:use-module (crates-io))

(define-public crate-treaty-0.0.1 (c (n "treaty") (v "0.0.1") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0vm86ynsmnqmqq7x0s03nwp9660sm47m7f1wvwb3w1cpk6vqrmwi") (f (quote (("default" "std" "serde")))) (s 2) (e (quote (("std" "alloc" "serde?/std") ("serde" "dep:serde") ("alloc" "serde?/alloc"))))))

