(define-module (crates-io tr ea treap) #:use-module (crates-io))

(define-public crate-treap-0.0.1 (c (n "treap") (v "0.0.1") (h "0v2bk79rv6iyrz9g52kpgm9r6xdf50nmy7mzjycd7ph6q8y8dqn8")))

(define-public crate-treap-0.0.2 (c (n "treap") (v "0.0.2") (d (list (d (n "rand") (r "0.1.*") (d #t) (k 0)))) (h "1cz9wrlxxiynsb86ywccbbh7xkvsih244rzawlri3cnsz31v0ja6")))

(define-public crate-treap-0.0.3 (c (n "treap") (v "0.0.3") (d (list (d (n "rand") (r "0.1.*") (d #t) (k 0)))) (h "12hrrnkm6977i8m00s021fn6c6gq2vjgfcd6fip0n23x4kz1if56")))

