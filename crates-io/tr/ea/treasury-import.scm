(define-module (crates-io tr ea treasury-import) #:use-module (crates-io))

(define-public crate-treasury-import-0.1.0 (c (n "treasury-import") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "treasury-id") (r "=0.1.0") (d #t) (k 0)))) (h "1akqykjs8wpbnbs8k8kcwiq27glsp3xk2vy45sydbw42jrrycyl0")))

(define-public crate-treasury-import-0.2.0 (c (n "treasury-import") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "treasury-id") (r "=0.1.0") (d #t) (k 0)))) (h "15haigzm0afp0wlmd7hnhm8ib6c18iabzaxwp53bv1z5y9na9nab")))

