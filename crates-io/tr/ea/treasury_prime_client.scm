(define-module (crates-io tr ea treasury_prime_client) #:use-module (crates-io))

(define-public crate-treasury_prime_client-5.0.0-beta.90 (c (n "treasury_prime_client") (v "5.0.0-beta.90") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1167zdk1k70gj54bpk3b16560kk5kv9qca5vwjzxfbcdfka1x3n7")))

(define-public crate-treasury_prime_client-5.0.0 (c (n "treasury_prime_client") (v "5.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1w24c1cy455ghfd80fk36spjy6msa44b5mv6akxsq9ga5lc6a2cl")))

