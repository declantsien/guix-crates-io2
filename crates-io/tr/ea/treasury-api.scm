(define-module (crates-io tr ea treasury-api) #:use-module (crates-io))

(define-public crate-treasury-api-0.1.0 (c (n "treasury-api") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "treasury-id") (r "=0.1.0") (d #t) (k 0)))) (h "0kr58m9wsc5bfj2mm6971zafqj4fn0agazzpipk76vsg4kgir13c")))

(define-public crate-treasury-api-0.2.0 (c (n "treasury-api") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "treasury-id") (r "=0.1.0") (d #t) (k 0)))) (h "0hfq02v58rdfmhpxlw4nq8k14v3qzl97kkb9kll1sdsq58apgclv")))

