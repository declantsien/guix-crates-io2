(define-module (crates-io tr pc trpc-rs-macros) #:use-module (crates-io))

(define-public crate-trpc-rs-macros-0.0.1 (c (n "trpc-rs-macros") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1fi8a9rfbygjmjlprkrh0gbgakaqfv1na48sizjc6fg231vd4vzi") (y #t)))

(define-public crate-trpc-rs-macros-0.0.2 (c (n "trpc-rs-macros") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("parsing"))) (d #t) (k 0)))) (h "10l66zndj60rya94f3r8lwynh0cl9cmgdy9n964clmybqlql5q0x") (y #t)))

