(define-module (crates-io tr io triomphe) #:use-module (crates-io))

(define-public crate-triomphe-0.1.0 (c (n "triomphe") (v "0.1.0") (d (list (d (n "nodrop") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)))) (h "1pf6gsfhwkd9qghqx6f64mj96c6xj40mnm58f9cikfzzf4mcxm22")))

(define-public crate-triomphe-0.1.1 (c (n "triomphe") (v "0.1.1") (d (list (d (n "nodrop") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)))) (h "1fjkb8anfviwqp148dkk35rs9gf7c7jj8h96mkdxm11rhw1ab8zx")))

(define-public crate-triomphe-0.1.2 (c (n "triomphe") (v "0.1.2") (d (list (d (n "memoffset") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (k 0)))) (h "0z6hc5hri7047kvxcac1j1mbd2n3rmrdf7cc7n1mm7ngach8g7bf") (f (quote (("std") ("default" "serde" "stable_deref_trait" "std"))))))

(define-public crate-triomphe-0.1.3 (c (n "triomphe") (v "0.1.3") (d (list (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (k 0)) (d (n "unsize") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0m0c0k24bnbcnpqras5jkcyp27n1wzk5ab1a09c5qn09xj0dpga6") (f (quote (("std") ("default" "serde" "stable_deref_trait" "std"))))))

(define-public crate-triomphe-0.1.4 (c (n "triomphe") (v "0.1.4") (d (list (d (n "arc-swap") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (k 0)) (d (n "unsize") (r "^1.1") (o #t) (d #t) (k 0)))) (h "10iyh1rysas2278m0lj9n9hxg666l15hz5359ipxk369yd8g1zim") (f (quote (("std") ("default" "serde" "stable_deref_trait" "std"))))))

(define-public crate-triomphe-0.1.5 (c (n "triomphe") (v "0.1.5") (d (list (d (n "arc-swap") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (k 0)) (d (n "unsize") (r "^1.1") (o #t) (d #t) (k 0)))) (h "028gi43zzxzq9npq4q9mrrz7vwf221426r0gw1h743a14qmk4pn4") (f (quote (("std") ("default" "serde" "stable_deref_trait" "std"))))))

(define-public crate-triomphe-0.1.6 (c (n "triomphe") (v "0.1.6") (d (list (d (n "arc-swap") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (k 0)) (d (n "unsize") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1hjfw1dgvywsmhn3x66k94vh6mzv0fp5cf9rq5d4mbdmm7ssp87d") (f (quote (("std") ("default" "serde" "stable_deref_trait" "std"))))))

(define-public crate-triomphe-0.1.7 (c (n "triomphe") (v "0.1.7") (d (list (d (n "arc-swap") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (k 0)) (d (n "unsize") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0brxpsiabqhzjd6pi5lk4rqm668vm6yrvicdq8vbky9m1f0b7qbz") (f (quote (("std") ("default" "serde" "stable_deref_trait" "std"))))))

(define-public crate-triomphe-0.1.8 (c (n "triomphe") (v "0.1.8") (d (list (d (n "arc-swap") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (k 0)) (d (n "unsize") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1nvqprc384bm15xyrygl8nf4yakd1j2aqzv59mqrscww4gcrpvpi") (f (quote (("std") ("default" "serde" "stable_deref_trait" "std"))))))

(define-public crate-triomphe-0.1.9 (c (n "triomphe") (v "0.1.9") (d (list (d (n "arc-swap") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (k 0)) (d (n "unsize") (r "^1.1") (o #t) (d #t) (k 0)))) (h "03xc02d5bhpr6d36srxd0c2l2n25h2val1wh9b2v0gxdmyc81vhf") (f (quote (("std") ("default" "serde" "stable_deref_trait" "std"))))))

(define-public crate-triomphe-0.1.10 (c (n "triomphe") (v "0.1.10") (d (list (d (n "arc-swap") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (k 0)) (d (n "unsize") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0294gy6dk0nw9s6xfbx34p6srlia7q4m4ma0nrr60cmc4wcagifh") (f (quote (("std") ("default" "serde" "stable_deref_trait" "std"))))))

(define-public crate-triomphe-0.1.11 (c (n "triomphe") (v "0.1.11") (d (list (d (n "arc-swap") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (k 0)) (d (n "unsize") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1crf71hndy3fc68x8v4aikkdjynp4n5sdhq28sck8x7frx8bd7l5") (f (quote (("std") ("default" "serde" "stable_deref_trait" "std"))))))

(define-public crate-triomphe-0.1.12 (c (n "triomphe") (v "0.1.12") (d (list (d (n "arc-swap") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (k 0)) (d (n "unsize") (r "^1.1") (o #t) (d #t) (k 0)))) (h "140dp3fhpdqxj5xjsjvbkpwmik8w0cjdzyl6mhvfnplrp7xv8b0v") (f (quote (("std") ("default" "serde" "stable_deref_trait" "std"))))))

