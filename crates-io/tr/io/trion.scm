(define-module (crates-io tr io trion) #:use-module (crates-io))

(define-public crate-trion-0.7.0 (c (n "trion") (v "0.7.0") (h "0zn0h9c708zqfjz26cz9476cpdb516lc6zaga9wlf5pqxw7fdq1x") (y #t)))

(define-public crate-trion-0.7.1 (c (n "trion") (v "0.7.1") (h "0ybrsrs14sjyqxsqskjd9v1qzv0vm30pkjmklwdlqx4dzdckn0nw")))

(define-public crate-trion-0.7.2 (c (n "trion") (v "0.7.2") (h "1fk2dlbzar83sawxgk466sh8jj4h2l08ysxnaiqs7r0smfy6qf1z")))

