(define-module (crates-io tr av travis-after-all) #:use-module (crates-io))

(define-public crate-travis-after-all-1.0.0 (c (n "travis-after-all") (v "1.0.0") (d (list (d (n "curl") (r "^0.2.16") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1wgfqgy6j3v8mri446nmwnbc9b2m4grcmhyg027rnki89mzc3ac7")))

(define-public crate-travis-after-all-2.0.0 (c (n "travis-after-all") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.5") (d #t) (k 0)))) (h "0ycnfzyzyvp87jb1857wy4swn2labsf2599px31m5yaq7v3ypnpp")))

