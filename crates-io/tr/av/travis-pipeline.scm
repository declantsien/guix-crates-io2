(define-module (crates-io tr av travis-pipeline) #:use-module (crates-io))

(define-public crate-travis-pipeline-0.6.1 (c (n "travis-pipeline") (v "0.6.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "shellwords") (r "^1.0.0") (d #t) (k 0)))) (h "1rqk120143b19ylmxk60bd3bfsqdp1387h4q6n3338iqhzgw019s")))

