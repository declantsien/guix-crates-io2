(define-module (crates-io tr av traversal) #:use-module (crates-io))

(define-public crate-traversal-0.1.0 (c (n "traversal") (v "0.1.0") (h "1f7xidncsai6nzqi4rjm5axgcqnacyac365g0p76hrs3x4dcv74x")))

(define-public crate-traversal-0.1.1 (c (n "traversal") (v "0.1.1") (h "0xw0psvn2yyvfjx4wp2q1f8rqx8hm99dkxgh208yi16ys3pvcik3") (y #t)))

(define-public crate-traversal-0.1.2 (c (n "traversal") (v "0.1.2") (h "168cs331pnp4qn9id828sdgzjhjfv2icnr8aij78nz2isx2rgv7h")))

