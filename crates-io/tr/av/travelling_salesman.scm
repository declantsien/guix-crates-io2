(define-module (crates-io tr av travelling_salesman) #:use-module (crates-io))

(define-public crate-travelling_salesman-0.0.1 (c (n "travelling_salesman") (v "0.0.1") (h "0kbss13h438zb9i032z03l602n9bi9vasm3y9s3p0dvdf0q4acr8")))

(define-public crate-travelling_salesman-0.0.2 (c (n "travelling_salesman") (v "0.0.2") (h "1j8qwiy1ffjg6ajx7w3c91008x4r2m9grvnnlmplxjrbw66g83rz")))

(define-public crate-travelling_salesman-0.0.3 (c (n "travelling_salesman") (v "0.0.3") (h "11cf111sk9j5bxmlrl4np1z4fc6z4sxdanmn6wjwbik2xj8y9463")))

(define-public crate-travelling_salesman-0.0.4 (c (n "travelling_salesman") (v "0.0.4") (h "1s92ilnlqjmbgl1wa6hfapx6g5cal7mh85csnbyripfx0dpz6p82")))

(define-public crate-travelling_salesman-0.0.5 (c (n "travelling_salesman") (v "0.0.5") (h "0cn4yylah3vwvaqh6yghpx15cwagnwzhdrzx9rxf8hiir1iw14gj")))

(define-public crate-travelling_salesman-0.0.6 (c (n "travelling_salesman") (v "0.0.6") (h "1kar2hzr55h8pqsm8ymj7hh4ymm5yp7i7lqj2mcs3q78v3wkjg0y")))

(define-public crate-travelling_salesman-0.0.7 (c (n "travelling_salesman") (v "0.0.7") (d (list (d (n "metaheuristics") (r "^0.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "1ilwv3p2k6kz41v5bi5hhcaj849rjybwmdh22bx7wdsnp70b9mrk")))

(define-public crate-travelling_salesman-0.0.8 (c (n "travelling_salesman") (v "0.0.8") (d (list (d (n "metaheuristics") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "0lcmdfvcrdsw56wdp5vl3ny65236q3gc377q11yyfmsh97jmn4d8")))

(define-public crate-travelling_salesman-0.0.9 (c (n "travelling_salesman") (v "0.0.9") (d (list (d (n "metaheuristics") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "0al4q3vxgy42pwffg010z5lkhivwbd6qvcxd0hgsf61bwx8a0rs2")))

(define-public crate-travelling_salesman-0.0.10 (c (n "travelling_salesman") (v "0.0.10") (d (list (d (n "metaheuristics") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "05iqklkr3scbbim1pm4ri227hi8fvmf0nnaycgk1mp0cs5jcj8jm")))

(define-public crate-travelling_salesman-0.0.11 (c (n "travelling_salesman") (v "0.0.11") (d (list (d (n "metaheuristics") (r "^0.0.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "1iixq6flm4hrkrhy2q8kp2797nky0ck9y3arkiqlhi2wl6x53rr2")))

(define-public crate-travelling_salesman-0.0.12 (c (n "travelling_salesman") (v "0.0.12") (d (list (d (n "metaheuristics") (r "^0.0.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "120wvdb7k98dgdq2ggl5nznsj25bh67yznv33i179z2djjjy3z39")))

(define-public crate-travelling_salesman-0.0.13 (c (n "travelling_salesman") (v "0.0.13") (d (list (d (n "metaheuristics") (r "^0.0.11") (d #t) (k 0)) (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "029an781mf3l2mmczcgkammqn4hyzb6zrf7aixrz549w3qn4l2vv")))

(define-public crate-travelling_salesman-0.0.14 (c (n "travelling_salesman") (v "0.0.14") (d (list (d (n "metaheuristics") (r "^0.0.12") (d #t) (k 0)) (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "05fkhy1qhv216qwwg3kd5xv0z2wh9yb6zx91casr8q7sah238ips")))

(define-public crate-travelling_salesman-0.0.15 (c (n "travelling_salesman") (v "0.0.15") (d (list (d (n "metaheuristics") (r "^0.0.12") (d #t) (k 0)) (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "1g8cikijsic0q2vw9yj4xldppywxvclmds9x9f3iw75cx4hylbir")))

(define-public crate-travelling_salesman-1.0.16 (c (n "travelling_salesman") (v "1.0.16") (d (list (d (n "metaheuristics") (r "^0.0.12") (d #t) (k 0)) (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "1sy4xnlwayp9v9s8ypk3zxlr3jj19nslq8mkfhz863rq1a4k6g14")))

(define-public crate-travelling_salesman-1.0.17 (c (n "travelling_salesman") (v "1.0.17") (d (list (d (n "metaheuristics") (r "^1.0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "0ccjwvpad4syz1bq7nkrg52lx0p1y9rzfyqbnnky5p6i6cvfj2c9")))

(define-public crate-travelling_salesman-1.1.18 (c (n "travelling_salesman") (v "1.1.18") (d (list (d (n "metaheuristics") (r "^1.0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "0gm2ymavhgph26y575jrgv3a4l0hn3yic2i7pa1625jc922kpfll")))

(define-public crate-travelling_salesman-1.1.19 (c (n "travelling_salesman") (v "1.1.19") (d (list (d (n "metaheuristics") (r "=1.1.19") (d #t) (k 0)) (d (n "rand") (r "=0.8.4") (d #t) (k 0)) (d (n "time") (r "=0.3.5") (d #t) (k 0)))) (h "1njy88k0v6hagn49nhyywiyssys3sbi3ad3bbif7jlvl4746hxzr")))

(define-public crate-travelling_salesman-1.1.20 (c (n "travelling_salesman") (v "1.1.20") (d (list (d (n "metaheuristics") (r "^1.1.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3.7") (d #t) (k 0)))) (h "1cfcfbfzhjk4sk1wwzawzpxdwxq79bjnyy7alw39bgp9r6wn9a80")))

(define-public crate-travelling_salesman-1.1.21 (c (n "travelling_salesman") (v "1.1.21") (d (list (d (n "metaheuristics") (r "^1.1.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3.7") (d #t) (k 0)))) (h "1wyq6ailyb774i03j7dw9la5lmwv84xhf9j6vx48gl3bl73cbyi4")))

(define-public crate-travelling_salesman-1.1.22 (c (n "travelling_salesman") (v "1.1.22") (d (list (d (n "metaheuristics") (r "^1.1.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3.7") (d #t) (k 0)))) (h "1200ymx704ylxrzqglrjw7aq5l34r5kk441swd579rz1l6zs1ad2")))

