(define-module (crates-io tr av traverse) #:use-module (crates-io))

(define-public crate-traverse-0.0.1 (c (n "traverse") (v "0.0.1") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "09ma8gf1hpc8z09a5wvmx38rvb9xigdvms25h344l7sl3xwbspm2")))

(define-public crate-traverse-0.0.2 (c (n "traverse") (v "0.0.2") (h "09li64ng37zjvhdwifpmgw8z63c35pm0glrbwy3la7hbkyxih83d")))

(define-public crate-traverse-0.0.3 (c (n "traverse") (v "0.0.3") (h "1pncabwk36g3n3ws1vv4fqzlc414g43wh3dn7264jfp24iqig9kz")))

(define-public crate-traverse-0.0.4 (c (n "traverse") (v "0.0.4") (h "0xy8a2999aqr8h2zkvd4smlhysp0zclxv3pnmsw3mfc5wgvhq3xj")))

(define-public crate-traverse-0.0.5 (c (n "traverse") (v "0.0.5") (h "0qfm9sqwlr6wc6pqhsc0a4npw5m9hjlfd5daygaphyss4sqiix67")))

(define-public crate-traverse-0.0.6 (c (n "traverse") (v "0.0.6") (h "11xml38pkg5hmcbrmq2wkx0i488iqsrjbimyksng088m1hfh7c9h")))

(define-public crate-traverse-0.0.7 (c (n "traverse") (v "0.0.7") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "01qx37fp9r7bvvmjqnssn7dz6ff4pls7k7m0n1c77djqvy3hqwkn")))

(define-public crate-traverse-0.0.8 (c (n "traverse") (v "0.0.8") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "016jqx9ydsm8pxffcm59gi3ndnn4wq1dnfx43hgxsv8gv1siznal")))

(define-public crate-traverse-0.0.9 (c (n "traverse") (v "0.0.9") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "128a9gfbdmjpzh1f6q7r148jbidcp1hrd125vw2p5zyw2lrphgjs")))

(define-public crate-traverse-0.0.10 (c (n "traverse") (v "0.0.10") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1l090k5rkjkiszpi4m67m9ak2s835g38lymxcacfafn5zk9bcx1k")))

(define-public crate-traverse-0.0.11 (c (n "traverse") (v "0.0.11") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0a9kjwy10k1c6nz4vwfrqyb197gi6d3d4xrrd4g8yjizmsg62kn4")))

(define-public crate-traverse-0.0.12 (c (n "traverse") (v "0.0.12") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "121y6sazhkhgfmkvsl5qnflnyp1q7w5f0wpqz5yjyy15w7jw824l")))

