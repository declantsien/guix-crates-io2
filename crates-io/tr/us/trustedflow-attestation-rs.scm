(define-module (crates-io tr us trustedflow-attestation-rs) #:use-module (crates-io))

(define-public crate-trustedflow-attestation-rs-0.1.0-dev20240308 (c (n "trustedflow-attestation-rs") (v "0.1.0-dev20240308") (d (list (d (n "trustedflow-attestation-sys") (r "^0.1.0-dev20240308") (d #t) (k 0)))) (h "1hk4f8a03zmn5s5y1rkx7bwd5iblrmxfj36m34dx2219dik0zc6w")))

(define-public crate-trustedflow-attestation-rs-0.1.0-dev240308 (c (n "trustedflow-attestation-rs") (v "0.1.0-dev240308") (d (list (d (n "trustedflow-attestation-sys") (r "^0.1.0-dev20240308") (d #t) (k 0)))) (h "1g69wc9h9246w8mxnl4yrnl7jc31lgmpmsyf0w606fscyy12y5x6")))

(define-public crate-trustedflow-attestation-rs-0.1.1-dev240311 (c (n "trustedflow-attestation-rs") (v "0.1.1-dev240311") (d (list (d (n "sdc_apis") (r "^0.2.1-dev20240222") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "trustedflow-attestation-sys") (r "^0.1.1-dev240311") (d #t) (k 0)))) (h "1lkn6drdxfvsfgx13qxgnn7jxz57cbr565l9ln063bknnihw1mcs") (y #t)))

(define-public crate-trustedflow-attestation-rs-0.1.1-dev240313 (c (n "trustedflow-attestation-rs") (v "0.1.1-dev240313") (d (list (d (n "sdc_apis") (r "^0.2.1-dev20240222") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "trustedflow-attestation-sys") (r "^0.1.1-dev240313") (d #t) (k 0)))) (h "023g0yf6pcsybi101ahf12564l0jwz23v58dypzpl1yri5ldyjqn")))

(define-public crate-trustedflow-attestation-rs-0.1.2-dev240313 (c (n "trustedflow-attestation-rs") (v "0.1.2-dev240313") (d (list (d (n "sdc_apis") (r "^0.2.1-dev20240222") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "trustedflow-attestation-sys") (r "^0.1.1-dev240313") (d #t) (k 0)))) (h "16msa149n281a0k9b0q9d68a40alyyczaz6g05px1clls84p9r85")))

