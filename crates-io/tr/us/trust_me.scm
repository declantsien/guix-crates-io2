(define-module (crates-io tr us trust_me) #:use-module (crates-io))

(define-public crate-trust_me-0.1.0 (c (n "trust_me") (v "0.1.0") (h "18pm3kmcajdl4lrcqv79cj5jcsg7siqf2k8ymhflbp5pp8jkwrwn")))

(define-public crate-trust_me-0.1.1 (c (n "trust_me") (v "0.1.1") (h "17l30ssskd0mqg2y3r76h1fpgqhisx2yjb1vkhnkagcgzr7s0f4y")))

