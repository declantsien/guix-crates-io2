(define-module (crates-io tr us trust-graph-distro) #:use-module (crates-io))

(define-public crate-trust-graph-distro-0.4.5 (c (n "trust-graph-distro") (v "0.4.5") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0k8ld6dhldmjw1ga1mkxdbsj3c4kgg7lds7r0dl2md0h3zm28kyq")))

(define-public crate-trust-graph-distro-0.4.6 (c (n "trust-graph-distro") (v "0.4.6") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1sncpb79pm9b0g1f1bqbzgl8xs8k9zckm40wi9mp7372h85nlax1")))

(define-public crate-trust-graph-distro-0.4.7 (c (n "trust-graph-distro") (v "0.4.7") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1m9rjh68kw0jyih3hnml8n62g2s27k4pwajw40hfp48s8bsvcjns")))

(define-public crate-trust-graph-distro-0.4.8 (c (n "trust-graph-distro") (v "0.4.8") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "065hiyg5zxfrpcw6b2sx2icdfrcyjyr74gryrjs2kzfnxq4bf3g3")))

(define-public crate-trust-graph-distro-0.4.9 (c (n "trust-graph-distro") (v "0.4.9") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1fi74lng897nrcxhqq2hhjj8ba8d71gxwz09b9wabah4vrl90vcz")))

(define-public crate-trust-graph-distro-0.4.10 (c (n "trust-graph-distro") (v "0.4.10") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0ih55wmk5g4v36wc3sikv2nwrpnpw0h8g134iczffw6y1g6h6sap")))

(define-public crate-trust-graph-distro-0.4.11 (c (n "trust-graph-distro") (v "0.4.11") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0lj2g9ij4ib4s24vcyl6d6swpw8ql07wn35chvk2klh4d2j6g3b8")))

