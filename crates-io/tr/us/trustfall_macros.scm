(define-module (crates-io tr us trustfall_macros) #:use-module (crates-io))

(define-public crate-trustfall_macros-0.0.1 (c (n "trustfall_macros") (v "0.0.1") (h "0cips5bx27vjg2ly3jr3ypyr5g80w934y4kkfqgd0sgpdk1gw8a8") (y #t)))

(define-public crate-trustfall_macros-0.0.2 (c (n "trustfall_macros") (v "0.0.2") (h "01xjsmkaqs0yxb5qmafcqvrs6b3947k6r40lplzjs00mrjicdxiq")))

