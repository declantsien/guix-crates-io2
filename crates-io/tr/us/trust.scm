(define-module (crates-io tr us trust) #:use-module (crates-io))

(define-public crate-trust-0.0.0 (c (n "trust") (v "0.0.0") (h "0ms0s5hlrl70d07syv5kcg85jlzjl59yh6m9hnswwcn9d83k7k49")))

(define-public crate-trust-0.0.1 (c (n "trust") (v "0.0.1") (h "1gvh55p058pnh0xkklfz7m3s35cdi72q0p9g6jwhmgdwjhrczajd")))

(define-public crate-trust-0.0.2 (c (n "trust") (v "0.0.2") (h "1d7hc5vz7vn6q757s6z7hg4knfn4snw30r1m73i5lm3rii2kggl7")))

