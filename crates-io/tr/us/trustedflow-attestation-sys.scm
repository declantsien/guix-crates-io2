(define-module (crates-io tr us trustedflow-attestation-sys) #:use-module (crates-io))

(define-public crate-trustedflow-attestation-sys-0.1.0-dev20240222 (c (n "trustedflow-attestation-sys") (v "0.1.0-dev20240222") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0ab6fl9y12iqyng389y5kp9wryn0hnr788w1khl8bmqy0qbk6pan")))

(define-public crate-trustedflow-attestation-sys-0.1.0-dev20240308 (c (n "trustedflow-attestation-sys") (v "0.1.0-dev20240308") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "17prdlllgysbpndnhwkypcsk3b536cqixlrcl2rjhvhkgx1nqhnr")))

(define-public crate-trustedflow-attestation-sys-0.1.1-dev240313 (c (n "trustedflow-attestation-sys") (v "0.1.1-dev240313") (h "04i1x3dm6qdx10dcwcnh7qkir8ip31kbzp47mrqrr1py6d8jwhhj")))

(define-public crate-trustedflow-attestation-sys-0.1.1-dev240311 (c (n "trustedflow-attestation-sys") (v "0.1.1-dev240311") (h "0f23l4bf40hwlhapr6226gfz4bim1ljahgpjxa4ngdvx861qm5bn") (y #t)))

