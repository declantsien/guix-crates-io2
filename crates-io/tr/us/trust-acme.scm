(define-module (crates-io tr us trust-acme) #:use-module (crates-io))

(define-public crate-trust-acme-0.1.0 (c (n "trust-acme") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bcder") (r "^0.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls"))) (k 0)) (d (n "ring") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "trust-dns") (r "^0.16.0-alpha.2") (f (quote ("dnssec-ring"))) (k 0)) (d (n "untrusted") (r "^0.6") (d #t) (k 0)))) (h "1h771ab74ccvaz4ci1dm0dnzyc4mvl2n23m760im7173jzri34kr")))

