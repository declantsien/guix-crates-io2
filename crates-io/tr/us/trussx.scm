(define-module (crates-io tr us trussx) #:use-module (crates-io))

(define-public crate-trussx-0.1.0 (c (n "trussx") (v "0.1.0") (h "003pvxbihr3z8v31acyq5frrzsa3giff18z40a2k53w7h161qaq1")))

(define-public crate-trussx-0.1.1 (c (n "trussx") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "16n7qp2bzi15xsi9smsv7smf9n0rw4y1fk5jg8p68b2y2hi1pial")))

(define-public crate-trussx-0.1.2 (c (n "trussx") (v "0.1.2") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "structural-shapes") (r "^0.1.0") (d #t) (k 0)))) (h "1s871pi5qxyb0m6yfvx9bm0qfqsssfv5j8v8kazawzl0ssrbmqp3")))

(define-public crate-trussx-0.1.3 (c (n "trussx") (v "0.1.3") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "structural-shapes") (r "^0.1.5") (d #t) (k 0)))) (h "1rqsih26jiyrvqpvr826dapjrm3pzbvic2wqbrljdm2bfvzahz6c")))

(define-public crate-trussx-0.1.4 (c (n "trussx") (v "0.1.4") (d (list (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "structural-shapes") (r "^0.2.2") (d #t) (k 0)) (d (n "uom") (r "^0.31.1") (d #t) (k 0)))) (h "11f3ibzvbdrasqkpajxqgnnknzp8xlak6mzhczi2lc95yz273wq9")))

