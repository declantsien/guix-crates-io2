(define-module (crates-io tr us trusty_web) #:use-module (crates-io))

(define-public crate-trusty_web-0.1.0 (c (n "trusty_web") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1k67cxcwgdz6nd9yfsi39g7pvjcc4qg9ahphsl90h0fwbvlmgfpn")))

