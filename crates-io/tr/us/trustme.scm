(define-module (crates-io tr us trustme) #:use-module (crates-io))

(define-public crate-trustme-1.0.0 (c (n "trustme") (v "1.0.0") (h "1l2g4658mff14sj3q58far6fzbj3k9wxzqf2w08h3ggavrlq0scv")))

(define-public crate-trustme-1.0.1 (c (n "trustme") (v "1.0.1") (h "0qgd4cn9g8l6mq72w3fz38zhmx6hp9p6ir74h5wqlklzvk8bbyrp")))

