(define-module (crates-io tr us trustrl) #:use-module (crates-io))

(define-public crate-trustrl-0.1.0 (c (n "trustrl") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "runtime-format") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0h6g3pfc9k0m2r4w3hzwndqhg8582dykxf8klxq6c3da90dbbirm")))

