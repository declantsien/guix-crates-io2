(define-module (crates-io tr us trustfall_derive) #:use-module (crates-io))

(define-public crate-trustfall_derive-0.0.1 (c (n "trustfall_derive") (v "0.0.1") (h "1q4l9hzczzik59wimwlr7pwpla531a5ai1a6q4by7anmykfkkxsz")))

(define-public crate-trustfall_derive-0.2.0 (c (n "trustfall_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trustfall_core") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1j6zhqmwhg9y0m3n7dg3h6hc5ynwy0bl1i3kmirr55l55b6f3crb")))

(define-public crate-trustfall_derive-0.2.1 (c (n "trustfall_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trustfall") (r "^0.2.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15ahfkrcbad41193fc3fmrlf3954ls051jaqzbdpkvxj60hhkp4p")))

(define-public crate-trustfall_derive-0.3.0 (c (n "trustfall_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1pv7hyr0lx98qbc12znhf40aswv1d2hai5c5vy7iw10fssh46ra8")))

(define-public crate-trustfall_derive-0.3.1 (c (n "trustfall_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1v394pb8fkg1q79bs75y2ngav2bc7lmpsp1a4ffncfrvyas4xc9z") (r "1.65")))

