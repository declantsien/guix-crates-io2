(define-module (crates-io tr us truster) #:use-module (crates-io))

(define-public crate-truster-0.1.0 (c (n "truster") (v "0.1.0") (h "1pbnbdm3ydg7vs4aijbm8frcwlw97vk54skxyqna4vf57d3jwlan")))

(define-public crate-truster-0.2.0 (c (n "truster") (v "0.2.0") (h "0mryjmrv6ph0js38a2klzhls58hd5qzaxhnd0lh9d73rjjbvaiia")))

(define-public crate-truster-0.2.1 (c (n "truster") (v "0.2.1") (h "1p31nkc9sjv6xvkswhr28lhf6pvs0522mxjq7vgfclbwzcxlcj55")))

(define-public crate-truster-0.3.0 (c (n "truster") (v "0.3.0") (h "1czp32cvj01rf7vp9bgr3mz96zasdkljfy3yg541vb1cim5vhvzk")))

(define-public crate-truster-0.3.1 (c (n "truster") (v "0.3.1") (h "1fva7j32rmqy59wcjw1lk541j3cgn609zbzd7x7yjbnndrhkzxj9")))

(define-public crate-truster-0.3.2 (c (n "truster") (v "0.3.2") (h "07n14pamdk4n640xwssyr9sipc2j4nlbxfjjfpasq61081c2pmn2")))

(define-public crate-truster-0.3.3 (c (n "truster") (v "0.3.3") (h "0myyyr8n3gn3ddhcza7rpa0aazi9d3digakh5ksivcwfbjwxchvp")))

(define-public crate-truster-0.4.0 (c (n "truster") (v "0.4.0") (h "07pkyaflr4rhpw96gfvbllc3nh41wq3nwfdqms670vk33f8wlj8c")))

(define-public crate-truster-0.4.1 (c (n "truster") (v "0.4.1") (h "1fprd9ji1xl94l8v1f63jj2jkfbvrj4iwg04r5i63mc15kzppgrs")))

