(define-module (crates-io tr us trust-graph-distro-test) #:use-module (crates-io))

(define-public crate-trust-graph-distro-test-0.4.1 (c (n "trust-graph-distro-test") (v "0.4.1") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0q7y3rdiwdqg4j98q504aac56ziys0ijqgjbhq8klynbm03q0zw6")))

(define-public crate-trust-graph-distro-test-0.4.1-1 (c (n "trust-graph-distro-test") (v "0.4.1-1") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0knhyqbq0v3q81ia03zbd8ibh5hgi2rdsxrmnmnnn6xgad987l6g")))

(define-public crate-trust-graph-distro-test-0.4.1-2 (c (n "trust-graph-distro-test") (v "0.4.1-2") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "02qwqk7ljwa10qjkgywbi57lgpb5hnfpmfv6ynpdh8j55ldj1jli")))

(define-public crate-trust-graph-distro-test-0.4.1-3 (c (n "trust-graph-distro-test") (v "0.4.1-3") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0914cvqwia6s6nznh9ki8fd5i9143sbx35dlfh53z80rr7ay2gq1")))

(define-public crate-trust-graph-distro-test-0.4.5-4 (c (n "trust-graph-distro-test") (v "0.4.5-4") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1m38vw6rmn54hxrsk68hlp9w4q9jj68avhfq0k2x68a73jhxmwfi")))

