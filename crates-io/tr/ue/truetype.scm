(define-module (crates-io tr ue truetype) #:use-module (crates-io))

(define-public crate-truetype-0.0.1 (c (n "truetype") (v "0.0.1") (h "057ydxf454sq86mhz2pr5xb4jdc63p2rfp3a7lavg6gdgprz5y3z")))

(define-public crate-truetype-0.1.0 (c (n "truetype") (v "0.1.0") (h "1wi8k6n8jrzaznwr98619fdfvibvwrvis6lknhwy0g32j72mlrsk")))

(define-public crate-truetype-0.2.0 (c (n "truetype") (v "0.2.0") (h "01swbyzxqrqvym3kd90x7n1ghwkgv78s65x6icyqrj8cgj4vyspm")))

(define-public crate-truetype-0.2.1 (c (n "truetype") (v "0.2.1") (h "0cf3wz11k8p8z4nhvmy5v1z74669rzbhps2sw2nzr1s6hm1xgm2s")))

(define-public crate-truetype-0.2.2 (c (n "truetype") (v "0.2.2") (h "052zlcs68wrfmmvibngk7mh5vcbdh8s7k3mhzsjbhhlgi2bkbqgy")))

(define-public crate-truetype-0.3.0 (c (n "truetype") (v "0.3.0") (h "0x7s52r1nkivwxw9nr31505yhdfjm2yrh5c8w5m7v04cj57scxvn")))

(define-public crate-truetype-0.4.0 (c (n "truetype") (v "0.4.0") (h "03c69sy1nfjrqyfgvzbppp63x5msk96xz6kaymfrg5242vlrdpyn")))

(define-public crate-truetype-0.5.0 (c (n "truetype") (v "0.5.0") (h "18aqzwc0lgm0spcay743ckvx8w39v8syifhnyp9zwag0qyrqgg99")))

(define-public crate-truetype-0.6.0 (c (n "truetype") (v "0.6.0") (h "1rl0jqazfgna35sjfw8308v9f40p2dqy0q6shh1lnj4yg94sfl8v")))

(define-public crate-truetype-0.7.0 (c (n "truetype") (v "0.7.0") (h "1nkvl75cbcj9x3b05iz3rgmdchxcjzagwqshdbfzvp0qmz7lxh0a")))

(define-public crate-truetype-0.8.0 (c (n "truetype") (v "0.8.0") (h "0w9xhp9a340dl3l5xfilbiwncrpssjk1c57q6fq2ciirfmgz2aan")))

(define-public crate-truetype-0.8.1 (c (n "truetype") (v "0.8.1") (h "0a6calyqqm2hgczl3gnv8mlivvbi4rfbymdwmkf9dylkdrhjx4rb")))

(define-public crate-truetype-0.9.0 (c (n "truetype") (v "0.9.0") (h "0i2mwzj69vqm4256g9j048hl7849ccsl5zvwrxypp6q6jdw3d8i3")))

(define-public crate-truetype-0.9.1 (c (n "truetype") (v "0.9.1") (h "0l31d86y67mzz1d2sgz812pvcw2nn68hcx5viihcldj7wvqi9vsy")))

(define-public crate-truetype-0.10.0 (c (n "truetype") (v "0.10.0") (h "1y28xjm95wisx8dp0x3ymgg649nd4hwsxi53lpa5wcnr8hn8rp88")))

(define-public crate-truetype-0.10.1 (c (n "truetype") (v "0.10.1") (h "16h8fq9k4qyvdhwr6518fgpn05r46pwxidzikdhw24lxqr79fp51")))

(define-public crate-truetype-0.10.2 (c (n "truetype") (v "0.10.2") (h "143jh8b7r9vrwkvgpr2zc9fs3a5j4c2mxhs2rnkbhz92vcc8w8k4")))

(define-public crate-truetype-0.10.3 (c (n "truetype") (v "0.10.3") (h "0m82vqxgn9lsxdkbznwafp2sbs0kx20y139s7ilkcy7gf57a0hd7")))

(define-public crate-truetype-0.10.4 (c (n "truetype") (v "0.10.4") (h "0vla46c5m4rnk170cdnz6i1av58hcdm95y0pr5ammym721kmmniw")))

(define-public crate-truetype-0.11.0 (c (n "truetype") (v "0.11.0") (h "00db5c98facbvl66dskr0h2sjaq3ci6cg77098pc08grv2qll1v1")))

(define-public crate-truetype-0.11.1 (c (n "truetype") (v "0.11.1") (h "1gzic1356bp0cmjghd98g4rzs2j04v0gl2cd8f46dy0lk7f1dzq9")))

(define-public crate-truetype-0.12.0 (c (n "truetype") (v "0.12.0") (h "1n5pck0nrnvyz5hgr6f1bdx657x9zzmg96yjdr614i54ramqzbvp")))

(define-public crate-truetype-0.13.0 (c (n "truetype") (v "0.13.0") (h "1gfjpdwm21yj6lkvs0jzq79vz2hrwm5jybpxzrlz1bm12l838vi7")))

(define-public crate-truetype-0.13.1 (c (n "truetype") (v "0.13.1") (h "0s8yrv59gal5ax5yzxxsn1xr19dnr82gzbhdxkb8pz6x06ymyccj")))

(define-public crate-truetype-0.14.0 (c (n "truetype") (v "0.14.0") (h "1gycs2q951xl06cfk94nz0bzb3pkm0masls1a3mjfqhdjff6isqj")))

(define-public crate-truetype-0.14.1 (c (n "truetype") (v "0.14.1") (h "0z6san40z36z2gsa0hgqfqbybqschyh8k26f79yjq4d440jffarj")))

(define-public crate-truetype-0.14.2 (c (n "truetype") (v "0.14.2") (h "1m1djgv1lghm9w2vc9z779rmyvmpkvi1n5ns2vk8524z8jk77ymd")))

(define-public crate-truetype-0.14.3 (c (n "truetype") (v "0.14.3") (h "0i87pj48alb0bfgsni66q1d02zbibyyhk6qffmw67pbpvyypm9hm")))

(define-public crate-truetype-0.15.0 (c (n "truetype") (v "0.15.0") (h "00llwrdgmb9q73f38lj5xsim3v818qiy3p3ja5wdsiiaqyzqkjz5")))

(define-public crate-truetype-0.15.1 (c (n "truetype") (v "0.15.1") (h "0849f8b883ifjw2vy59dxr5fnkxsvpx89x983ys23bzm90jdlqfn")))

(define-public crate-truetype-0.16.0 (c (n "truetype") (v "0.16.0") (h "1k1hqk9sj936m4c01yjb6qjrd3j1j7dpws5wwhq42p73slm8asvw")))

(define-public crate-truetype-0.17.0 (c (n "truetype") (v "0.17.0") (h "0gw7kh8x8snfjpsy5pf8jj5cz6ndlvi3wbb5qmnsiy8d2k9nzhms")))

(define-public crate-truetype-0.18.0 (c (n "truetype") (v "0.18.0") (h "133pknwdfzgj1kvp0nvwdnf0n920s0536fs4galcwqh24yqvizzn")))

(define-public crate-truetype-0.19.0 (c (n "truetype") (v "0.19.0") (h "1ag6a1qhxxg8pz586l60srk1610hhpwzh19dbpjvl2jz8sy84jsl")))

(define-public crate-truetype-0.19.1 (c (n "truetype") (v "0.19.1") (h "0l59bbv0dhw4gzvmwbxlpdq460hqc3rl693pk6gd2k4rcdz7jia3")))

(define-public crate-truetype-0.20.0 (c (n "truetype") (v "0.20.0") (h "1v49z3vbgzjkrjq31h448h5azv8kf7mqhmvi62i197hiwa9fkfa3")))

(define-public crate-truetype-0.21.0 (c (n "truetype") (v "0.21.0") (h "1rslzasq5k2qd8y5d3km04lmv3pczj199wskzml7lprg85psmb08")))

(define-public crate-truetype-0.22.0 (c (n "truetype") (v "0.22.0") (h "15997d2kcnxnxlclvg2xdna13lx4h1li8bp1qfydsfz1fhwvcxjr")))

(define-public crate-truetype-0.23.0 (c (n "truetype") (v "0.23.0") (h "0i0zm5lfazmg412y2ymnhx83czymymv2iz00ix19hfmh6zgjpkn9")))

(define-public crate-truetype-0.23.1 (c (n "truetype") (v "0.23.1") (h "1vggxcmpchy3ydxi4m4998a1par4ncirz514is0vic5khmgx3192")))

(define-public crate-truetype-0.24.0 (c (n "truetype") (v "0.24.0") (h "1pdw08ddplwpjys3d6cqfkiixh3ipgi1sxhn9mhmywhldmypq5l7")))

(define-public crate-truetype-0.25.0 (c (n "truetype") (v "0.25.0") (h "0rk77cig801pw9lkv6rjahvnbqk6w7aczm67cr5d3d0j02sgn7jr")))

(define-public crate-truetype-0.26.0 (c (n "truetype") (v "0.26.0") (h "0xxqxacp3b9m5jn1m55ych31bsfbns35fqhsvk4xmmik0qsk1v5c")))

(define-public crate-truetype-0.26.1 (c (n "truetype") (v "0.26.1") (h "0vhv2nxrr5gpjisf17032g60s4k0fxry29psprs7qdpynw2j5lx6")))

(define-public crate-truetype-0.26.2 (c (n "truetype") (v "0.26.2") (h "1v1gisnn1vb3122a56phadhygpc527827vzbzz89kjkr5515fiba")))

(define-public crate-truetype-0.27.0 (c (n "truetype") (v "0.27.0") (h "0dk17zr4iq8f51bq15x6kxxqq5fjhmzisy5hgbz1gkggprkiqzki")))

(define-public crate-truetype-0.28.0 (c (n "truetype") (v "0.28.0") (h "0qxm51mjy9qgw0b76s3msxk1q4lh7phz3bblgly7vfvlsy49b3a7") (y #t)))

(define-public crate-truetype-0.29.0 (c (n "truetype") (v "0.29.0") (h "11ig46q9n4vz9rrsk3jsh772yi7qybndvn7wahwdvsfbhl749r0a")))

(define-public crate-truetype-0.30.0 (c (n "truetype") (v "0.30.0") (h "0pinknsv47av2lk4mssj71y9b0w5h1xhlandq0y82i9jlgyykgca")))

(define-public crate-truetype-0.30.1 (c (n "truetype") (v "0.30.1") (h "0c2pj8lqchb04kpcz156i3bqy2ykhh51i3wwcdz6fmky3b7sffh6")))

(define-public crate-truetype-0.30.2 (c (n "truetype") (v "0.30.2") (h "0nws0962axfqggr5zs4piacn2768kqd6nmawq0c68kjpjqkqw20w")))

(define-public crate-truetype-0.30.3 (c (n "truetype") (v "0.30.3") (h "1pj4xwq0a6myhqzk8rq3rjkg6c6bj1zk9fvf9m37h5hy85vqgwq4")))

(define-public crate-truetype-0.31.0 (c (n "truetype") (v "0.31.0") (h "1vb6dl9pzl26rjhm8n7adf2wywv8r6ri2n7zg52dz4kmfnprwv41")))

(define-public crate-truetype-0.31.1 (c (n "truetype") (v "0.31.1") (h "08yn5m2ys914m7siqi5h7nfr7yp9jmw4h35h15n9hiacah3hhcmz") (f (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.32.0 (c (n "truetype") (v "0.32.0") (d (list (d (n "typeface") (r "^0.1") (d #t) (k 0)))) (h "0anx5nwmgrffbyqwqxnjplp4hgy8vlqjhkzva3s97j64gv09g9aq") (f (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.33.0 (c (n "truetype") (v "0.33.0") (d (list (d (n "typeface") (r "^0.1") (d #t) (k 0)))) (h "0pp3i5yfzcsffi4qa52bxdxid6aga6qv1crg73204x4sc5c021jg") (f (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.34.0 (c (n "truetype") (v "0.34.0") (d (list (d (n "typeface") (r "^0.1") (d #t) (k 0)))) (h "0r97ias0p9s0ffw0sg7w5a36cbym73h1a9spia7ifgknwgr4skjv") (f (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.35.0 (c (n "truetype") (v "0.35.0") (d (list (d (n "typeface") (r "^0.1") (d #t) (k 0)))) (h "075y7dfm0fxd1k9vh0fdwf98bcx93kp6wx9km9grpmh5jybpk3wa") (f (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.36.0 (c (n "truetype") (v "0.36.0") (d (list (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "1fzzqfip995fg7lq5c4p1qwqnrqhz9ln00hah8qnvksy5286n8l0") (f (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.37.0 (c (n "truetype") (v "0.37.0") (d (list (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "1m9i8fzxh12c7wk3xgzwxc21cyhhz150wmw678l03yh5nhrlyhzm") (f (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.38.0 (c (n "truetype") (v "0.38.0") (d (list (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "1p4qy1949l9cs97jdrwv4yy97r3zmjidq084yyskm2459q7q5rns") (f (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.38.1 (c (n "truetype") (v "0.38.1") (d (list (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "0ivmha62dp1bbk580xp8lfrhdpfnqxjq6msm3dghsjwwfl65v81j") (f (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.39.0 (c (n "truetype") (v "0.39.0") (d (list (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "1056aqd26ca5gvkj20dydnnl1w75604c46vgjnwpfadwzm0kfb00") (f (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.39.1 (c (n "truetype") (v "0.39.1") (d (list (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "1i0zc9q5j9g94z6c0fh5mgj4gggnbyv2bb56rzi0brc6rw87gz8p") (f (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.40.0 (c (n "truetype") (v "0.40.0") (d (list (d (n "typeface") (r "^0.2.3") (d #t) (k 0)))) (h "1k83khnqljxcpvydb6hqaa3ny52xfgw4j9ki584g7fr14ypqlqkw") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.40.1 (c (n "truetype") (v "0.40.1") (d (list (d (n "typeface") (r "^0.2.4") (d #t) (k 0)))) (h "05sxnh62k0hqd7yzvlqxh7xaa5cvlrnly6fvnszzvvalgr94klyy") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.41.0 (c (n "truetype") (v "0.41.0") (d (list (d (n "typeface") (r "^0.2.4") (d #t) (k 0)))) (h "1r8fx11js9dr3p6sahvhbph510k5kqrll1nm8czhsl7m661yl8cm") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.41.1 (c (n "truetype") (v "0.41.1") (d (list (d (n "typeface") (r "^0.2.6") (d #t) (k 0)))) (h "1ad7cg73rja3wflg7clsvxlbki3ysdin812k2w43n2k9l2j9mr0q") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42.0 (c (n "truetype") (v "0.42.0") (d (list (d (n "typeface") (r "^0.2.6") (d #t) (k 0)))) (h "0z6p7ckmlix44j8j88lximiavqfiq6gzx87lsm5gg4lcbr95v95n") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42.1 (c (n "truetype") (v "0.42.1") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "1lihqs1z6xap65bzpaisc0iq6i39c18hs9avf8qbvnxx2kc0z8lf") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42.2 (c (n "truetype") (v "0.42.2") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "05ph8hmmjfnm14f95gfvv7z4mrh5wggcskkdz5zx7nmmdj3f9vng") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42.3 (c (n "truetype") (v "0.42.3") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "1n776xsldgmdysj50aynpxsdcym8rl8qg0033di9llr9dms769sp") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42.4 (c (n "truetype") (v "0.42.4") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "1f0dz8igb404mgmz3nj6qybxn9khk51z2yswr9684chkp0pixhyb") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42.5 (c (n "truetype") (v "0.42.5") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "16zy8nhf4ibck4488pr28rwjy942a8i62lf9b2vc20p99k082b7z") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42.6 (c (n "truetype") (v "0.42.6") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "02qhqb6gfkx1afby4k93db3rg761jh7p84xcik7k462syf52glcq") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.43.0 (c (n "truetype") (v "0.43.0") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "1ma539sm58hd7hgwl155q0qzz0vp9dhijdp0flglgacb5y8vjdz7") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.43.1 (c (n "truetype") (v "0.43.1") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "1yy1w7c41blkqmnk345hdaxb5pcr1a5slw5m1d06y4ilh8ml6mz3") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.43.2 (c (n "truetype") (v "0.43.2") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "1mh44ig7jgkl0zxrh7rrjpdmwwqy153xba781k80c8pji83ncsnx") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.43.3 (c (n "truetype") (v "0.43.3") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "1fxvrpglq1ihyq1scqjqcv5hccyh1c25gwyi3nc30v3h2kr8ipyi") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.43.4 (c (n "truetype") (v "0.43.4") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "19anyy8av8j3kx5nvvs2jn65k2k3swhkd1nsmcdq1if8n7jqn4sk") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.43.5 (c (n "truetype") (v "0.43.5") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "19417jyw7jvfdpk0ph9vsh49zwm3aqrfyqkjlhx8snjh3x07f7rc") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.44.0 (c (n "truetype") (v "0.44.0") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "1icbasz5jyfypqs0nzxza2m2xybhklxvi1zp6ffw0y82g5vplqiy") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.44.1 (c (n "truetype") (v "0.44.1") (d (list (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "0y1r1fq6pjrqz52y71dzvmanbm8y3p8svjvsslcwcqh3p85kxbw2") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.45.0 (c (n "truetype") (v "0.45.0") (d (list (d (n "typeface") (r "^0.4.0") (d #t) (k 0)))) (h "1l6x932850jl51dynnp4w19vkn1rdyagym7vms2hja7ha80vp4l1") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.45.1 (c (n "truetype") (v "0.45.1") (d (list (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "06h0yj0xdl1cnvnhirk0gfxw4icw2iwfc879q6micpdz765x1gcm") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.46.0 (c (n "truetype") (v "0.46.0") (d (list (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "10w1kqgpfsx6jr13prdxbva7wbc1sdwkqwxqy3fynj5sxjfrdlxz") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47.0 (c (n "truetype") (v "0.47.0") (d (list (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "1xwwz3bylz8hlcmvdzhfjdzpkapx64q5dgrw6m202jqpvbjarkla") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47.1 (c (n "truetype") (v "0.47.1") (d (list (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "13c5crp63c73jh3rc79l5d5qpvfilm3bdgmbiamjckkc6wnmh0cs") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47.2 (c (n "truetype") (v "0.47.2") (d (list (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "123aiidjz3b394kbcj9nw37zxq6n57m1gf6fzdi73y3x25ckdyva") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47.3 (c (n "truetype") (v "0.47.3") (d (list (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "05r49npxgbj3qr61g3gcl89zmxgp00l8dj2jj59fq35m430ivwnf") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47.4 (c (n "truetype") (v "0.47.4") (d (list (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "01c2wm8r4pqasrnwvvphpj43v47nb201d58h9xygpzaii4bvs2zq") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags")))) (y #t)))

(define-public crate-truetype-0.47.5 (c (n "truetype") (v "0.47.5") (d (list (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "12r6ysby7pr62rch7151wzgdd9vrkf9p9haghjmrk84yzkvdq9ps") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47.6 (c (n "truetype") (v "0.47.6") (d (list (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "192c78vggdvzxsx9ilb9wnmg12adz17as501h7jk527fm2w8gf0g") (f (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

