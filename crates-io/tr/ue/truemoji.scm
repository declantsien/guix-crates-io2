(define-module (crates-io tr ue truemoji) #:use-module (crates-io))

(define-public crate-truemoji-0.1.0 (c (n "truemoji") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "truemoji-core") (r "^0.1.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1k317rwny5z0n00vqrvmiak970vkvn4qv7r798fdnrb7fj0s4c8h") (y #t)))

