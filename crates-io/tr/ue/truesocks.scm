(define-module (crates-io tr ue truesocks) #:use-module (crates-io))

(define-public crate-truesocks-1.0.0 (c (n "truesocks") (v "1.0.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "socks" "gzip" "deflate" "brotli"))) (d #t) (k 0)) (d (n "reqwest-middleware") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest-retry") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zbczk4ypy4y18af0iagvc6wncmabzd8yaz77r6p1l2wy58hn9gm")))

