(define-module (crates-io tr ue truelayer-extensions) #:use-module (crates-io))

(define-public crate-truelayer-extensions-0.1.0 (c (n "truelayer-extensions") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "1v39qk5bk3nzzg7gbf1i2213dda8fb39wnmg2f3lz1n2wbzxrdsr")))

(define-public crate-truelayer-extensions-0.1.1 (c (n "truelayer-extensions") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "0wdki49n9swy21299j2rykjry65r032x5px6nfp7lcikn8n142al")))

(define-public crate-truelayer-extensions-0.1.2 (c (n "truelayer-extensions") (v "0.1.2") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "1qs1x8nhfr914kzn1af394zpgfri7xk4vb0809fzmr6ibz73dxv9")))

