(define-module (crates-io tr ue truetree) #:use-module (crates-io))

(define-public crate-truetree-0.1.0 (c (n "truetree") (v "0.1.0") (h "1mhvgkcl0qx9wd68h41sjgw5sg9m0fbz3bl5wpnw4bxjp0m2zpyr")))

(define-public crate-truetree-0.1.1 (c (n "truetree") (v "0.1.1") (h "0fq4zdnxdwbfd3ix0m0vpdkcm3m60s687mb8klcl6nhcl671la6v")))

(define-public crate-truetree-0.1.2 (c (n "truetree") (v "0.1.2") (h "10fag0zwz8nf7y8z7cm0cx995fcrwkz96d6z1swa66wgqrsspdf5")))

(define-public crate-truetree-0.1.3 (c (n "truetree") (v "0.1.3") (h "0kw2cqd8y34mqcsss3jpm6mzhdazqzifrb2s4fdnyhxqqngyc52y")))

