(define-module (crates-io tr ya tryagain) #:use-module (crates-io))

(define-public crate-tryagain-0.1.0 (c (n "tryagain") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "macros" "time"))) (o #t) (d #t) (k 0)))) (h "1b9a1sxa87ha8bz8pggymb462smi934yd1fzd74vr8x4a5bs6b88") (f (quote (("runtime-tokio" "tokio" "pin-project") ("runtime-async-std" "async-std" "pin-project") ("default" "runtime-tokio"))))))

