(define-module (crates-io tr ei treiber_stack) #:use-module (crates-io))

(define-public crate-treiber_stack-1.0.0 (c (n "treiber_stack") (v "1.0.0") (d (list (d (n "arc-swap") (r "^1.6") (d #t) (k 0)))) (h "1qrf4j68z3h1rdij88y1sxa240rzkjqfr7jgib0x4mlmx5vhnkb7")))

(define-public crate-treiber_stack-1.0.1 (c (n "treiber_stack") (v "1.0.1") (d (list (d (n "arc-swap") (r "^1.6") (d #t) (k 0)))) (h "1lnl5zdn204alv6gzy4nwnxf5cj4rbv5zxvqv07bncd2a16vwqcr")))

(define-public crate-treiber_stack-1.0.2 (c (n "treiber_stack") (v "1.0.2") (d (list (d (n "arc-swap") (r "^1.6") (d #t) (k 0)))) (h "0cp4s38lp2kd7zkaxcwvprxxv7rai30qxynl4cqrafmqmbs2xcnp")))

