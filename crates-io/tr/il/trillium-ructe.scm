(define-module (crates-io tr il trillium-ructe) #:use-module (crates-io))

(define-public crate-trillium-ructe-0.1.0 (c (n "trillium-ructe") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ructe") (r "^0.13.4") (d #t) (k 0)) (d (n "trillium") (r "^0.1.4") (d #t) (k 0)))) (h "0y6i0qqra0yjvsw9lvr5w0j95qnlvny1y8dfjmsljzs9r4k3gpv3")))

(define-public crate-trillium-ructe-0.1.1 (c (n "trillium-ructe") (v "0.1.1") (d (list (d (n "ructe") (r "^0.13.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "trillium") (r "^0.1.4") (d #t) (k 0)))) (h "1gdjwwks7zh5if0jrm0dab4dpc210m04q1kpda01c512vvsnsfrh")))

(define-public crate-trillium-ructe-0.2.0 (c (n "trillium-ructe") (v "0.2.0") (d (list (d (n "ructe") (r "^0.13.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "14xm2qdzd2njz1n0zm4278hsj9dg0czv6qik16w97f2h3f05vwwi")))

(define-public crate-trillium-ructe-0.3.0 (c (n "trillium-ructe") (v "0.3.0") (d (list (d (n "ructe") (r "^0.13.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1cqvakja83223b43pc7ljxjrfsx9l803asdrydk2iynrbiyxsz9s")))

(define-public crate-trillium-ructe-0.3.1 (c (n "trillium-ructe") (v "0.3.1") (d (list (d (n "ructe") (r "^0.13.4") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "0y02giw5cbnnishirq7bjk2g3dg1wnkl2bvm14a9cbmiipx3ps0k")))

(define-public crate-trillium-ructe-0.4.0 (c (n "trillium-ructe") (v "0.4.0") (d (list (d (n "ructe") (r "^0.14.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1q3dwzhxnv2p0h3684x2npd91qisz0cy7yapd8zkwkvg5l8qjqxd")))

