(define-module (crates-io tr il trillium-prometheus) #:use-module (crates-io))

(define-public crate-trillium-prometheus-0.1.0 (c (n "trillium-prometheus") (v "0.1.0") (d (list (d (n "prometheus") (r "^0.13.3") (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)) (d (n "trillium") (r "^0.2.8") (d #t) (k 0)) (d (n "trillium-router") (r "^0.3.5") (d #t) (k 0)) (d (n "trillium-smol") (r "^0.2.1") (d #t) (k 2)) (d (n "trillium-testing") (r "^0.4.2") (f (quote ("smol"))) (d #t) (k 2)))) (h "06jwcv95ify8gps33x35471w1zkjwri1qb94h509kvqp711nh1d5")))

