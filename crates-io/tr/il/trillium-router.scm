(define-module (crates-io tr il trillium-router) #:use-module (crates-io))

(define-public crate-trillium-router-0.1.0 (c (n "trillium-router") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "routefinder") (r "^0.3.0") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)))) (h "1pb1gy22z2rrif1divbyjz1ys7c4b0c04j12zn5drgy277l5za4q")))

(define-public crate-trillium-router-0.1.1 (c (n "trillium-router") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "routefinder") (r "^0.3.1") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)))) (h "1gvh4cr06si9xiz2ynnk8zr7g119ir20ccwma3zia2l1w3r42rbn")))

(define-public crate-trillium-router-0.2.0 (c (n "trillium-router") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "routefinder") (r "^0.3.1") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)))) (h "166fif0fyq0iahp8wkamif449zpws47daxv81lprlxvdmlsxqsfi")))

(define-public crate-trillium-router-0.3.0 (c (n "trillium-router") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "routefinder") (r "^0.4.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1b1fdmfw24dirng99y2pc7hfxlbjdq4z9x9zcab2z2acqd0bx6zp")))

(define-public crate-trillium-router-0.3.1 (c (n "trillium-router") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "routefinder") (r "^0.5") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1x6yn0ndql7qcryg40nm4ngy1swmlak09qynxa9i8v0z3zv3rzai")))

(define-public crate-trillium-router-0.3.2 (c (n "trillium-router") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "routefinder") (r "^0.5.1") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1a8p6a9fs94wv3xzjydpz16dk262vd9i8dz08jczhb8vw3gnxsr0")))

(define-public crate-trillium-router-0.3.3 (c (n "trillium-router") (v "0.3.3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "routefinder") (r "^0.5.1") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1zxllyvqi2v0zfqjbs1nml2m4wyl0i5x94fx6lmdr25p2cajxrnr")))

(define-public crate-trillium-router-0.3.4 (c (n "trillium-router") (v "0.3.4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "routefinder") (r "^0.5.2") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1rxbps8aagz7f9v0l6akp1h3wxwwv22ggagxaj5wp8whqrqrrjc3")))

(define-public crate-trillium-router-0.3.5 (c (n "trillium-router") (v "0.3.5") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "routefinder") (r "^0.5.3") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1mvcn7725h71xmznmml0hlcn1pp614q25a2wiqqfim5v2zcaqg59")))

(define-public crate-trillium-router-0.3.6 (c (n "trillium-router") (v "0.3.6") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "routefinder") (r "^0.5.3") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)))) (h "0ncn9sl81a1h2rskcjcjbcp73b8awsi6z10smspsgd6l374sknki")))

(define-public crate-trillium-router-0.4.0 (c (n "trillium-router") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "routefinder") (r "^0.5.4") (f (quote ("memchr"))) (d #t) (k 0)) (d (n "trillium") (r "^0.2.17") (d #t) (k 0)))) (h "1ki5c42kz76jmimr9f1bs1x339x9539x5vpmyq5i469rz7b15iqg")))

(define-public crate-trillium-router-0.4.1 (c (n "trillium-router") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "routefinder") (r "^0.5.4") (f (quote ("memchr"))) (d #t) (k 0)) (d (n "trillium") (r "^0.2.19") (d #t) (k 0)))) (h "1gkx2bc14lw7bgdpbz6cyjkhjc2285qh9zb5s7fdf09isqhfsyka")))

