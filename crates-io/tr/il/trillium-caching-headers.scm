(define-module (crates-io tr il trillium-caching-headers) #:use-module (crates-io))

(define-public crate-trillium-caching-headers-0.1.0 (c (n "trillium-caching-headers") (v "0.1.0") (d (list (d (n "etag") (r "^3.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "httpdate") (r "^1.0.1") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "0kg1d54bqphr9d5k2i0r15dalzsckdfzr5pnjs9sv8h581swj70d")))

(define-public crate-trillium-caching-headers-0.2.0 (c (n "trillium-caching-headers") (v "0.2.0") (d (list (d (n "etag") (r "^4.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "httpdate") (r "^1.0.2") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "08h9c17953sw8kn77zvph85d7gwa6dk8vgrbxblvlssxdjf23hkv")))

(define-public crate-trillium-caching-headers-0.2.1 (c (n "trillium-caching-headers") (v "0.2.1") (d (list (d (n "etag") (r "^4.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "httpdate") (r "^1.0.2") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "00qaixyr7wn3wsgzd4yg46h9hw4s8n8jbynly1v1bqvas85mpfxs")))

(define-public crate-trillium-caching-headers-0.2.2 (c (n "trillium-caching-headers") (v "0.2.2") (d (list (d (n "etag") (r "^4.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "httpdate") (r "^1.0.3") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)))) (h "05gdxhjv6rw82q706wvxj0f1xg132rg1d8bq7c03zk6i9y8fq2pw")))

(define-public crate-trillium-caching-headers-0.2.3 (c (n "trillium-caching-headers") (v "0.2.3") (d (list (d (n "etag") (r "^4.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "httpdate") (r "^1.0.3") (d #t) (k 0)) (d (n "trillium") (r "^0.2.19") (d #t) (k 0)))) (h "1swfiniiinxpqdyd4k6vixdjdqfhv1w63nciaibnspj7if6an72k")))

