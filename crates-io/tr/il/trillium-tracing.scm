(define-module (crates-io tr il trillium-tracing) #:use-module (crates-io))

(define-public crate-trillium-tracing-0.1.0 (c (n "trillium-tracing") (v "0.1.0") (d (list (d (n "smol") (r "^1.2.5") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.14") (d #t) (k 2)) (d (n "trillium") (r "^0.2.3") (d #t) (k 0)) (d (n "trillium-router") (r "^0.3.3") (d #t) (k 2)) (d (n "trillium-smol") (r "^0.2.1") (d #t) (k 2)))) (h "1k2qcddpbirgn2jd2l9vi21n1cljwsw4w7my20hwplyzhb9l7fcm")))

