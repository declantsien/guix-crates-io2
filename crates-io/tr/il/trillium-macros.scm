(define-module (crates-io tr il trillium-macros) #:use-module (crates-io))

(define-public crate-trillium-macros-0.0.1 (c (n "trillium-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0a354pcfmaynzqw7bkv9bmyxbah6n90qvi3lc51n58fsvgjynx1l")))

(define-public crate-trillium-macros-0.0.2 (c (n "trillium-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("visit" "parsing"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0b050ng9377qgy6rwkvzw2740ll1d2vj1hndwivx9phydmfaib1h")))

(define-public crate-trillium-macros-0.0.3 (c (n "trillium-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1wm2r0qa3xscw22r72pzqjibkv07b2mvxzqbk8qvdj8nd1571sva")))

(define-public crate-trillium-macros-0.0.4 (c (n "trillium-macros") (v "0.0.4") (d (list (d (n "futures-lite") (r "^1.13.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "192kwfq97xbavczsc7a3rxjh8l73l3vw14lpmsz6cgmw6xzvy6bd")))

(define-public crate-trillium-macros-0.0.5 (c (n "trillium-macros") (v "0.0.5") (d (list (d (n "futures-lite") (r "^2.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1frv11rzns1497syw9hdllj0cxja0ki7vgq4fvnwzw4324w58q4i")))

(define-public crate-trillium-macros-0.0.6 (c (n "trillium-macros") (v "0.0.6") (d (list (d (n "futures-lite") (r "^2.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0v0zxm8c30kx3gkqv26ri4m6ydz6wb2bim02ra42nqrkdmj3fvli")))

