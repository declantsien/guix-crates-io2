(define-module (crates-io tr il trillium-basic-auth) #:use-module (crates-io))

(define-public crate-trillium-basic-auth-0.1.0 (c (n "trillium-basic-auth") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "trillium") (r "^0.2") (d #t) (k 0)))) (h "0hbh6kdv8fmpfziwma4fakbc5hr8z5200f2rs8wkfl3zkrk0p2pg")))

(define-public crate-trillium-basic-auth-0.1.1 (c (n "trillium-basic-auth") (v "0.1.1") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2") (d #t) (k 0)))) (h "0jbyrav1wl7p4mvm2pd0qdsl4kmb3br22n8zjrq4cl4qnx3ryg89")))

