(define-module (crates-io tr il trillium-static-compiled) #:use-module (crates-io))

(define-public crate-trillium-static-compiled-0.1.0 (c (n "trillium-static-compiled") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mime-db") (r "^1.4.0") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)))) (h "1sfdah94i0w42djyih3fa2dhc7y1a84jppm4dbzi4sahpg7gkzya")))

(define-public crate-trillium-static-compiled-0.3.0 (c (n "trillium-static-compiled") (v "0.3.0") (d (list (d (n "include_dir") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mime-db") (r "^1.4.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "0yx0h532gd8aw1k39swhkq10nr4m553nassbscazg6i6xh0ziwqi")))

(define-public crate-trillium-static-compiled-0.3.1 (c (n "trillium-static-compiled") (v "0.3.1") (d (list (d (n "include_dir") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.3") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1ak2v72pdhrc43z7vj66mvnjw3r03hjcrsqkigpss8ia2arrdi2y")))

(define-public crate-trillium-static-compiled-0.4.0 (c (n "trillium-static-compiled") (v "0.4.0") (d (list (d (n "httpdate") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.3") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)) (d (n "trillium-include-dir") (r "^0.1.0") (d #t) (k 0)))) (h "0d73fj7wcgq0nvz0ifh170fs4v6lihm2w6h5m2zc65y4wqgqvhav")))

(define-public crate-trillium-static-compiled-0.5.0 (c (n "trillium-static-compiled") (v "0.5.0") (d (list (d (n "httpdate") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.3") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)) (d (n "trillium-static-compiled-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0s7k0g60m5rs13w2jnj1s75d655c1br1s4rh6ldlqc0c3hsvv3pa")))

(define-public crate-trillium-static-compiled-0.5.1 (c (n "trillium-static-compiled") (v "0.5.1") (d (list (d (n "httpdate") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)) (d (n "trillium-static-compiled-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1gxxdc4mls8br2vd59pbklpg4ddzgysfijn32ywzkw2qh6ppgbkz")))

(define-public crate-trillium-static-compiled-0.5.2 (c (n "trillium-static-compiled") (v "0.5.2") (d (list (d (n "httpdate") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "trillium") (r "^0.2.19") (d #t) (k 0)) (d (n "trillium-static-compiled-macros") (r "^0.1.1") (d #t) (k 0)))) (h "181wgydxhbp88pdph9pvpbr57pxcky5pbh9zm50ywr09mqzazdm6")))

