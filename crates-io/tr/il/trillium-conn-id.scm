(define-module (crates-io tr il trillium-conn-id) #:use-module (crates-io))

(define-public crate-trillium-conn-id-0.1.0 (c (n "trillium-conn-id") (v "0.1.0") (d (list (d (n "extension-trait") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1vpas6giyj42nipfghdpqcdpwl3qqfxnri9p4d7cjabfj61zb264")))

(define-public crate-trillium-conn-id-0.2.0 (c (n "trillium-conn-id") (v "0.2.0") (d (list (d (n "extension-trait") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "194p57y62ai6v6fx24z1isnsc63va8b8l8pniymic95nx06a2rjv")))

(define-public crate-trillium-conn-id-0.2.1 (c (n "trillium-conn-id") (v "0.2.1") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1w6aycr87mbz2hn1rb8kx7dr1dzq7675fcdpcg815fnkqxf24fgc")))

(define-public crate-trillium-conn-id-0.2.2 (c (n "trillium-conn-id") (v "0.2.2") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1630y3i045vp2khbj852bc1cszhxhn7svbhmlwv1wrsip2jzsf2k")))

(define-public crate-trillium-conn-id-0.2.3 (c (n "trillium-conn-id") (v "0.2.3") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "trillium") (r "^0.2.19") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1wh8b2196nzd3znjyjnrra8mj4p64fjii855vs49ix883smmi64n")))

