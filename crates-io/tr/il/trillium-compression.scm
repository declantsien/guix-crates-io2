(define-module (crates-io tr il trillium-compression) #:use-module (crates-io))

(define-public crate-trillium-compression-0.1.0 (c (n "trillium-compression") (v "0.1.0") (d (list (d (n "async-compression") (r "^0.3.8") (f (quote ("brotli" "gzip" "futures-io"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1f0l651yr901ba6k7021m9s6gpy6irf1mccsgdlmd5v6fgvzma7y")))

(define-public crate-trillium-compression-0.1.1 (c (n "trillium-compression") (v "0.1.1") (d (list (d (n "async-compression") (r "^0.4.5") (f (quote ("brotli" "gzip" "zstd" "futures-io"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "futures-lite") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)))) (h "1y4v7jps6s7nah55sfmbdajald60q9m4cgj430wbkw9vbysm8x1z")))

(define-public crate-trillium-compression-0.1.2 (c (n "trillium-compression") (v "0.1.2") (d (list (d (n "async-compression") (r "^0.4.5") (f (quote ("brotli" "gzip" "zstd" "futures-io"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "futures-lite") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "trillium") (r "^0.2.19") (d #t) (k 0)))) (h "08zc4nlxn2s7ybwjx4l6g8lrfplxr1hc446c72hq6n2i74345s6q")))

