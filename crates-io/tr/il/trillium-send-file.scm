(define-module (crates-io tr il trillium-send-file) #:use-module (crates-io))

(define-public crate-trillium-send-file-0.1.0 (c (n "trillium-send-file") (v "0.1.0") (d (list (d (n "async-fs") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "async_std_crate") (r "^1.10.0") (o #t) (d #t) (k 0) (p "async-std")) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.3") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)) (d (n "trillium-smol") (r "^0.2.0") (d #t) (k 2)) (d (n "trillium-testing") (r "^0.3.1") (f (quote ("smol"))) (d #t) (k 2)))) (h "14hw9i0i1vnz33d10faa3lrnklmmr5xfick5212h8w0094kd00sc") (f (quote (("smol" "async-fs") ("default") ("async-std" "async_std_crate"))))))

