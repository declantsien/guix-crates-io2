(define-module (crates-io tr il trillium-sse) #:use-module (crates-io))

(define-public crate-trillium-sse-0.1.0 (c (n "trillium-sse") (v "0.1.0") (d (list (d (n "broadcaster") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "16ah8hfcd2nfm2bc1pwl54cr5fv3985ni5rqmy83pai5wmxcjjff")))

(define-public crate-trillium-sse-0.1.1 (c (n "trillium-sse") (v "0.1.1") (d (list (d (n "broadcaster") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-lite") (r "^2.1.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)))) (h "1djaczjlkb08gkni2j0ps7yipqsxx88m8dj865ndwarmk27mffy9")))

(define-public crate-trillium-sse-0.1.2 (c (n "trillium-sse") (v "0.1.2") (d (list (d (n "broadcaster") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-lite") (r "^2.1.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.19") (d #t) (k 0)))) (h "1wgs206wvgkc2acpdzq4x38g8l4mabapz6xf9p4bh6ib6yr50c7g")))

