(define-module (crates-io tr il trillium-native-tls) #:use-module (crates-io))

(define-public crate-trillium-native-tls-0.1.0 (c (n "trillium-native-tls") (v "0.1.0") (d (list (d (n "async-native-tls") (r "^0.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.7") (d #t) (k 0)) (d (n "trillium-tls-common") (r "^0.1.0") (d #t) (k 0)))) (h "1glgdm3rxvxzyjyqicgw280yfp4i1r7m5wwwpqxjp6pc7a25jhak")))

(define-public crate-trillium-native-tls-0.3.0 (c (n "trillium-native-tls") (v "0.3.0") (d (list (d (n "async-native-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "trillium-server-common") (r "^0.4.0") (d #t) (k 0)))) (h "0xf7gfx39w2lplv4mb67054hk4b3j8rchqji3gywq9gm0s438d5k")))

(define-public crate-trillium-native-tls-0.3.1 (c (n "trillium-native-tls") (v "0.3.1") (d (list (d (n "async-native-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)) (d (n "test-harness") (r "^0.2.0") (d #t) (k 2)) (d (n "trillium-server-common") (r "^0.4.6") (d #t) (k 0)))) (h "19vf6cjlcm03qxj5a5ndx46hp5zjmk36d0iy1c83ax64qbbqhvpv")))

(define-public crate-trillium-native-tls-0.3.2 (c (n "trillium-native-tls") (v "0.3.2") (d (list (d (n "async-native-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)) (d (n "test-harness") (r "^0.2.0") (d #t) (k 2)) (d (n "trillium-server-common") (r "^0.4.7") (d #t) (k 0)))) (h "14gqnx6h1vbf19bngb4zkb818vwswmkndslnrm5v0wa2mzn00yzg")))

(define-public crate-trillium-native-tls-0.3.3 (c (n "trillium-native-tls") (v "0.3.3") (d (list (d (n "async-native-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)) (d (n "test-harness") (r "^0.2.0") (d #t) (k 2)) (d (n "trillium-server-common") (r "^0.5.1") (d #t) (k 0)))) (h "0fm0xz4n12byvryrz9gzf94jcvzb5w06nzhvsqjz3xlr0dpzk3lq")))

(define-public crate-trillium-native-tls-0.4.0 (c (n "trillium-native-tls") (v "0.4.0") (d (list (d (n "async-native-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)) (d (n "test-harness") (r "^0.2.0") (d #t) (k 2)) (d (n "trillium-server-common") (r "^0.5.2") (d #t) (k 0)))) (h "1j3g2an9hlv9nibw3vikavrjab443lizz2g2d4iijf3kdyipssqm")))

