(define-module (crates-io tr il trillium-askama) #:use-module (crates-io))

(define-public crate-trillium-askama-0.1.0 (c (n "trillium-askama") (v "0.1.0") (d (list (d (n "askama") (r "^0.10.5") (d #t) (k 0)) (d (n "mime-db") (r "^1.4.0") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)))) (h "18z6nh44j51cx11kddvyc64c28z7g07x8ixdhycinma33ixahp6b")))

(define-public crate-trillium-askama-0.2.0 (c (n "trillium-askama") (v "0.2.0") (d (list (d (n "askama") (r "^0.10.5") (d #t) (k 0)) (d (n "mime-db") (r "^1.4.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "15vi1z5d7yfk9n3wc2gaysl342c465sq1ppdzjm8f8bbycibxvg2")))

(define-public crate-trillium-askama-0.3.0 (c (n "trillium-askama") (v "0.3.0") (d (list (d (n "askama") (r "^0.11.0") (d #t) (k 0)) (d (n "mime-db") (r "^1.6.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "071k64z1b2p7ccc8sllchwcpbc7gl12cpfca7ab3y5zcb9l9rrmc")))

(define-public crate-trillium-askama-0.3.1 (c (n "trillium-askama") (v "0.3.1") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "mime-db") (r "^1.7.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)))) (h "03vr1805llqacgxw70hxy0n67b95qpywr2dr5hx2g1g813n9zvif")))

(define-public crate-trillium-askama-0.3.2 (c (n "trillium-askama") (v "0.3.2") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "trillium") (r "^0.2.13") (d #t) (k 0)))) (h "07jcdd2r0j7lph18asjxsxii8q64dgj2wy417kmbmwbg4jsyx97h")))

