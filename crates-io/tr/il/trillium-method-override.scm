(define-module (crates-io tr il trillium-method-override) #:use-module (crates-io))

(define-public crate-trillium-method-override-0.1.0 (c (n "trillium-method-override") (v "0.1.0") (d (list (d (n "querystrong") (r "^0.1.1") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)))) (h "19cbdlvi0w6przhcvx3kzxjs4srs91l6jbdjk908xp1q6q5npb3y")))

(define-public crate-trillium-method-override-0.2.0 (c (n "trillium-method-override") (v "0.2.0") (d (list (d (n "querystrong") (r "^0.1.1") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1s4189vbyvrswdkifflq4jaqjxbjg7k3ps9n5h38n9482gigl0h2")))

(define-public crate-trillium-method-override-0.2.1 (c (n "trillium-method-override") (v "0.2.1") (d (list (d (n "querystrong") (r "^0.3.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)))) (h "0w7rw8ji0lfgfkkngv1cfdaj16m888k6xfxyk3zsp6sswczl86d7")))

