(define-module (crates-io tr il trillium-tera) #:use-module (crates-io))

(define-public crate-trillium-tera-0.1.0 (c (n "trillium-tera") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mime-db") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "tera") (r "^1.10.0") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)))) (h "0si6m03knavp0gw9jwc3lwrcfy3d4y3xfyz9qkbfviw081q6934n")))

(define-public crate-trillium-tera-0.2.0 (c (n "trillium-tera") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mime-db") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "tera") (r "^1.12.1") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "108mak7xlzdmrxr8c19mn83g2rqg5cskmgwglll7hiy9axsr9ywf")))

(define-public crate-trillium-tera-0.3.0 (c (n "trillium-tera") (v "0.3.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mime-db") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "0lc1kim05p2j08sk068znqhw24jxzgl9y1hx03z0rppq7ffhbbch")))

(define-public crate-trillium-tera-0.3.1 (c (n "trillium-tera") (v "0.3.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mime-db") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)))) (h "0flhnmbxmjzh33zrarq5jim2b9qa3cjxzg0y2ky30g6r7cmn1jlj")))

(define-public crate-trillium-tera-0.3.2 (c (n "trillium-tera") (v "0.3.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "trillium") (r "^0.2.13") (d #t) (k 0)))) (h "1m1qg4a3hpc8dhx8mcv8jz7q3xridnvgf2b42rk2hda0c8bkgiwl")))

