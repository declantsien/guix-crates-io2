(define-module (crates-io tr il trillium-sessions) #:use-module (crates-io))

(define-public crate-trillium-sessions-0.1.0 (c (n "trillium-sessions") (v "0.1.0") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)) (d (n "trillium-cookies") (r "^0.1.0") (d #t) (k 0)))) (h "11hfs5bg0jkgqj1ak9cijxklpi3lqhl9sk7fhrhhdhya5nkmxyvb")))

(define-public crate-trillium-sessions-0.2.0 (c (n "trillium-sessions") (v "0.2.0") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)) (d (n "trillium-cookies") (r "^0.2.0") (d #t) (k 0)))) (h "00x7as2xa2pcvrbmg6kip7x6dm54r1vrfs4ffil7w1j68f589g39")))

(define-public crate-trillium-sessions-0.3.0 (c (n "trillium-sessions") (v "0.3.0") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)) (d (n "trillium-cookies") (r "^0.3.0") (d #t) (k 0)))) (h "0ppj1ssr43np1drs0k33zkjsgp95vpcljjvj6nkl1nrpnrdnlwga")))

(define-public crate-trillium-sessions-0.4.0 (c (n "trillium-sessions") (v "0.4.0") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)) (d (n "trillium-cookies") (r "^0.3.0") (d #t) (k 0)))) (h "0lg2l5da6zxqji0xb6w517rv2sks7s4c7scbshw6xw1gw132n9ij") (y #t)))

(define-public crate-trillium-sessions-0.4.1 (c (n "trillium-sessions") (v "0.4.1") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)) (d (n "trillium-cookies") (r "^0.4.0") (d #t) (k 0)))) (h "1yaa8l1yjhkf8qafy68hjif7v3m9gshjxzvm1zy6f8pvz0aiwx8s")))

(define-public crate-trillium-sessions-0.4.2 (c (n "trillium-sessions") (v "0.4.2") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)) (d (n "trillium-cookies") (r "^0.4.0") (d #t) (k 0)))) (h "0ihx1d84yihpl2bjdmvij9rkzv6bh7817vg93i26fg79jlligxfh")))

(define-public crate-trillium-sessions-0.4.3 (c (n "trillium-sessions") (v "0.4.3") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)) (d (n "trillium-cookies") (r "^0.4.1") (d #t) (k 0)))) (h "04swqq8psm2li3v9dzapgx52fhy83snaabw91fa6lwhjprijzvv1")))

(define-public crate-trillium-sessions-0.4.4 (c (n "trillium-sessions") (v "0.4.4") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "trillium") (r "^0.2.19") (d #t) (k 0)) (d (n "trillium-cookies") (r "^0.4.2") (d #t) (k 0)))) (h "1ii0shhc5q3ia96s6h3sl0bj39vnpz8f9pk9amaip1afv34snc8k")))

