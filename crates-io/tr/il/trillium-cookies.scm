(define-module (crates-io tr il trillium-cookies) #:use-module (crates-io))

(define-public crate-trillium-cookies-0.1.0 (c (n "trillium-cookies") (v "0.1.0") (d (list (d (n "cookie") (r "^0.15.0") (f (quote ("percent-encode" "secure"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)))) (h "1x8vm6nr2ql55kqskxiivppjx9m5zigkarlnf9ps4ghy5fdpcmy5")))

(define-public crate-trillium-cookies-0.2.0 (c (n "trillium-cookies") (v "0.2.0") (d (list (d (n "cookie") (r "^0.15.1") (f (quote ("percent-encode" "secure"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "18xn7mxn8hji4v1gwpshicinchz9jgslcms2iiivswbyx6gj01ay")))

(define-public crate-trillium-cookies-0.3.0 (c (n "trillium-cookies") (v "0.3.0") (d (list (d (n "cookie") (r "^0.16.0") (f (quote ("percent-encode" "secure"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "0ppr6v58frsp5yznqcxgf0l2gj9f530sndmfwfnaci653z9l4yqa")))

(define-public crate-trillium-cookies-0.4.0 (c (n "trillium-cookies") (v "0.4.0") (d (list (d (n "cookie") (r "^0.17.0") (f (quote ("percent-encode" "secure"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "01bk7wl8787d27kx30ddrv9ii5gr3dd9d3adx7vx2y8ksa1431mc")))

(define-public crate-trillium-cookies-0.4.1 (c (n "trillium-cookies") (v "0.4.1") (d (list (d (n "cookie") (r "^0.18.0") (f (quote ("percent-encode" "secure"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)))) (h "1xqzaq5wac5x7vam1mm9jnz64bfswm59r5ssvxypa806nclj5jw7")))

(define-public crate-trillium-cookies-0.4.2 (c (n "trillium-cookies") (v "0.4.2") (d (list (d (n "cookie") (r "^0.18.0") (f (quote ("percent-encode" "secure"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "trillium") (r "^0.2.19") (d #t) (k 0)))) (h "0h0ji7iip9b3yi6v6aj7n4j8c9733vnapf2j3fhczck15x5zfcdi")))

