(define-module (crates-io tr il trillium-include-dir) #:use-module (crates-io))

(define-public crate-trillium-include-dir-0.1.0 (c (n "trillium-include-dir") (v "0.1.0") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "trillium-include-dir-impl") (r "=0.1.0") (d #t) (k 0)))) (h "1f9kckif9m6cs49fabjlans3mx0flc77zxh6vknzlpn4daymv82m") (f (quote (("search" "glob") ("example-output") ("default" "search"))))))

(define-public crate-trillium-include-dir-0.1.1 (c (n "trillium-include-dir") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "trillium-include-dir-impl") (r "=0.1.1") (d #t) (k 0)))) (h "0kmwg49yljrc07g844yrzg5gzm5ij9v8vaqqa2ikmmhp6rxwj5sj") (f (quote (("search" "glob") ("example-output") ("default" "search"))))))

