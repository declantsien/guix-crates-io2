(define-module (crates-io tr il trillium-head) #:use-module (crates-io))

(define-public crate-trillium-head-0.1.0 (c (n "trillium-head") (v "0.1.0") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)))) (h "04my3cf3yxphl8aqznyfx9l75c103wjpa6zshhpad5v8dipr8bfn")))

(define-public crate-trillium-head-0.2.0 (c (n "trillium-head") (v "0.2.0") (d (list (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "0an595jg8wmi8mk09nyc3kqyvdrb4yx61n274idga4fys9191jad")))

(define-public crate-trillium-head-0.2.1 (c (n "trillium-head") (v "0.2.1") (d (list (d (n "trillium") (r "^0.2.12") (d #t) (k 0)))) (h "06ycd165szzxz0vhffn2h5fb0qr1iky2nwy4gfsr3hr76wknhd5p")))

(define-public crate-trillium-head-0.2.2 (c (n "trillium-head") (v "0.2.2") (d (list (d (n "trillium") (r "^0.2.19") (d #t) (k 0)))) (h "0svsliybfq6dfpq1db76r5z0rmxp1v9fm7wnrnvfhrawmqiil3y8")))

