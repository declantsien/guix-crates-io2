(define-module (crates-io tr il trillium-error) #:use-module (crates-io))

(define-public crate-trillium-error-0.1.0 (c (n "trillium-error") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0y214g7b488927rc54wy5yjwh3r485d0cld6p0c4pbnnqrqvzn7j")))

