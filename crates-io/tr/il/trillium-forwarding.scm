(define-module (crates-io tr il trillium-forwarding) #:use-module (crates-io))

(define-public crate-trillium-forwarding-0.1.0 (c (n "trillium-forwarding") (v "0.1.0") (d (list (d (n "cidr") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "trillium") (r "^0.1.0") (d #t) (k 0)))) (h "0cij4ikzl95r42vird908p0fnnn4r03zf7ilss3bwr6i95j1ykcl")))

(define-public crate-trillium-forwarding-0.2.0 (c (n "trillium-forwarding") (v "0.2.0") (d (list (d (n "cidr") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1ixc313fwgfj6vc6cbg0057zh31d4k8qrbk92677xsf7jm9gga85")))

(define-public crate-trillium-forwarding-0.2.1 (c (n "trillium-forwarding") (v "0.2.1") (d (list (d (n "cidr") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "017jy31ys8byl2w6jsrzv8cm5ya4wjwz6cdqd1l2gvqfzizd2hbi")))

(define-public crate-trillium-forwarding-0.2.2 (c (n "trillium-forwarding") (v "0.2.2") (d (list (d (n "cidr") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1xp1xc36hhy4p9g5rkiy63jwja0g90g9djy93fjxdh3ih8nzj009")))

(define-public crate-trillium-forwarding-0.2.3 (c (n "trillium-forwarding") (v "0.2.3") (d (list (d (n "cidr") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "trillium") (r "^0.2.12") (d #t) (k 0)))) (h "0x3x8dkacrblqpa0dkvwrhy5slz7rqjyh5c9zl2wsy94glkx7jgh")))

(define-public crate-trillium-forwarding-0.2.4 (c (n "trillium-forwarding") (v "0.2.4") (d (list (d (n "cidr") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "trillium") (r "^0.2.19") (d #t) (k 0)))) (h "1h207jhfwl571sdzpvc3y8zymzizjv2907gr9v8rlr20xsp9cdv8")))

