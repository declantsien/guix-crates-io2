(define-module (crates-io tr il trillium-redirect) #:use-module (crates-io))

(define-public crate-trillium-redirect-0.1.0 (c (n "trillium-redirect") (v "0.1.0") (d (list (d (n "trillium") (r "^0.2.0") (d #t) (k 0)))) (h "1wh682nfdkw4a3g71l85hskwwahy2xpa1zba0n8yncfqd63phdzs")))

(define-public crate-trillium-redirect-0.1.1 (c (n "trillium-redirect") (v "0.1.1") (d (list (d (n "trillium") (r "^0.2.12") (d #t) (k 0)))) (h "17g8y7rfxkqanqa9iyaqwhkfmwa7c81hydffri568jz9hiy0ys92")))

(define-public crate-trillium-redirect-0.1.2 (c (n "trillium-redirect") (v "0.1.2") (d (list (d (n "trillium") (r "^0.2.19") (d #t) (k 0)))) (h "1zywdkpp161xqmxa195j3nbd4fwhc8p5499fa01zgg810nxiwdjw")))

