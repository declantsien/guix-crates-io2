(define-module (crates-io tr il trillium-tls-common) #:use-module (crates-io))

(define-public crate-trillium-tls-common-0.1.0 (c (n "trillium-tls-common") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "056pi07wl0h1fb9zb7kpa9307x7h9sv937ijzf3dbnlg5yb29h15")))

