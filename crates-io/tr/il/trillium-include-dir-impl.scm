(define-module (crates-io tr il trillium-include-dir-impl) #:use-module (crates-io))

(define-public crate-trillium-include-dir-impl-0.1.0 (c (n "trillium-include-dir-impl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1g70yjy27a7lpr7d7njzpcsl5jfcx98pbbi7sax7yddm2632a9dq")))

(define-public crate-trillium-include-dir-impl-0.1.1 (c (n "trillium-include-dir-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0jr0w46hmdl8qzhpcllz0als8zq4nyfbsl9qy767w64mfghphkvh")))

