(define-module (crates-io tr uc truc) #:use-module (crates-io))

(define-public crate-truc-0.1.0 (c (n "truc") (v "0.1.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "truc_runtime") (r "^0.1.0") (d #t) (k 0)))) (h "1nc99c1rzjanmmagmcxfffymlqj1pqk6jdk5ss3hghnknw9wwwsv")))

(define-public crate-truc-0.1.1 (c (n "truc") (v "0.1.1") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "truc_runtime") (r "^0.1.1") (d #t) (k 0)))) (h "10bapki6yl4h4r2xjqmgrx3wxaj8iqr92972d3y2l19jazpwdxkw") (r "1.56.1")))

