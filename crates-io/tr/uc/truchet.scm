(define-module (crates-io tr uc truchet) #:use-module (crates-io))

(define-public crate-truchet-0.1.0 (c (n "truchet") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 2)))) (h "1ia5482r88mmni3fx9sn70j6rp0dn8j5hnqcp3w1c1dhgn6i5rb4")))

(define-public crate-truchet-0.1.1 (c (n "truchet") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 2)))) (h "06806l70bpmhgp0smx84075arxnrbd83k359d70w128azga0a1d6")))

