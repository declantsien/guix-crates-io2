(define-module (crates-io tr uc truc_runtime) #:use-module (crates-io))

(define-public crate-truc_runtime-0.1.0 (c (n "truc_runtime") (v "0.1.0") (h "01cnkv3d7b785kahg0ljc1dvz5sx02lmxk2v06pzwfl9nxx70yfi")))

(define-public crate-truc_runtime-0.1.1 (c (n "truc_runtime") (v "0.1.1") (h "17rh0cih3w2bw40f8kw122zcwdwx4flpqml2lzkwyvw80hc38s1z") (r "1.56.1")))

