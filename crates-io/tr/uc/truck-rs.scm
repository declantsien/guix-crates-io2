(define-module (crates-io tr uc truck-rs) #:use-module (crates-io))

(define-public crate-truck-rs-0.1.0 (c (n "truck-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0pmp559b59m9577szs6dn18n5c5bx90xb59r9sb5vys7c20qvjir")))

(define-public crate-truck-rs-0.2.0 (c (n "truck-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0954m72gcc7kzww5skc6k09a0jjq5qaiscqi1l9nsg5r4as7b884")))

(define-public crate-truck-rs-0.2.5 (c (n "truck-rs") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "14g8qv4151vryih6fifkm6rnh9cz3jskx83dpq9hfra9y0lxbm8c")))

