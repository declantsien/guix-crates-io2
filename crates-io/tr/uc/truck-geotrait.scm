(define-module (crates-io tr uc truck-geotrait) #:use-module (crates-io))

(define-public crate-truck-geotrait-0.1.0 (c (n "truck-geotrait") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.3") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "truck-base") (r "^0.2.0") (d #t) (k 0)))) (h "0c9bj5h95914zjvi5k3ycs298x7ybpbjyy83641dvjl46b07r6d5")))

(define-public crate-truck-geotrait-0.2.0 (c (n "truck-geotrait") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2.6") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "truck-base") (r "^0.3.0") (d #t) (k 0)))) (h "101msnz9rnfmamiwirx9ibk63ci13kkn4m5irvm0jaqsr1ra70dq")))

(define-public crate-truck-geotrait-0.3.0 (c (n "truck-geotrait") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "truck-base") (r "^0.4.0") (d #t) (k 0)) (d (n "truck-geoderive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "14xxbm7vncmarzrxyh1b5swja67vlab4gnkijpa9x5p3jwpzkdax") (f (quote (("derive" "truck-geoderive") ("default"))))))

