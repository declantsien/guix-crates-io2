(define-module (crates-io tr uc truck-geometry) #:use-module (crates-io))

(define-public crate-truck-geometry-0.1.0 (c (n "truck-geometry") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "truck-base") (r "^0.1.0") (d #t) (k 0)))) (h "1y30n47q7587y3q9ymgkrz3xx7lsrmj85769r34g5dpphy56wshx")))

(define-public crate-truck-geometry-0.1.1 (c (n "truck-geometry") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "truck-base") (r "^0.1.1") (d #t) (k 0)))) (h "1ggpmh70mjix1l6hqlyan721hpk0zhillgidh10aylpkw9gqbdk6")))

(define-public crate-truck-geometry-0.2.0 (c (n "truck-geometry") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.3") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "truck-base") (r "^0.2.0") (d #t) (k 0)) (d (n "truck-geotrait") (r "^0.1.0") (d #t) (k 0)))) (h "15jrgir47lynijwdhyyav0lp6isq1nkp5qm95s4a4hz3as2wdx47")))

(define-public crate-truck-geometry-0.3.0 (c (n "truck-geometry") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.6") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "truck-base") (r "^0.3.0") (d #t) (k 0)) (d (n "truck-geotrait") (r "^0.2.0") (d #t) (k 0)))) (h "1x6282clafj2c3ml0a109b2i4d6shhq08ghnh4spxgrf1gw1ngs7")))

(define-public crate-truck-geometry-0.4.0 (c (n "truck-geometry") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "truck-base") (r "^0.4.0") (d #t) (k 0)) (d (n "truck-geotrait") (r "^0.3.0") (d #t) (k 0)))) (h "19b937rfvzclzjbh0a03ly56r8s9l1w2kbgnz9qvnrk561d20v24")))

