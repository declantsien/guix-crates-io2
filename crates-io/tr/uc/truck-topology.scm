(define-module (crates-io tr uc truck-topology) #:use-module (crates-io))

(define-public crate-truck-topology-0.1.0 (c (n "truck-topology") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "truck-base") (r "^0.1.0") (d #t) (k 0)))) (h "1ksqrprrggfrirpc3af08lx1cc8dzv22hyq1vj47x2m5bfw25s9y")))

(define-public crate-truck-topology-0.1.1 (c (n "truck-topology") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "truck-base") (r "^0.1.0") (d #t) (k 0)))) (h "162kfxjmmb6i6qck69jb1qkmvh0qhjqp9m0l078mcm3scg05pp95")))

(define-public crate-truck-topology-0.2.0 (c (n "truck-topology") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "truck-base") (r "^0.1.1") (d #t) (k 0)))) (h "03biflq8xmjwvc2vc0v3afcpclkjpnwv2a7mv6dn2zdsxlq5s3gi")))

(define-public crate-truck-topology-0.3.0 (c (n "truck-topology") (v "0.3.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "truck-base") (r "^0.2.0") (d #t) (k 0)) (d (n "truck-geotrait") (r "^0.1.0") (d #t) (k 0)))) (h "00k920rgavqqi3k8laq1a1svpihzzlmfhfqfc66w6ww3grsha1kp")))

(define-public crate-truck-topology-0.4.0 (c (n "truck-topology") (v "0.4.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "truck-base") (r "^0.3.0") (d #t) (k 0)) (d (n "truck-geotrait") (r "^0.2.0") (d #t) (k 0)))) (h "15wf1371sp3jcip7hkz10fkc69iyz69fa29cc63n8ligglbx3zji")))

(define-public crate-truck-topology-0.5.0 (c (n "truck-topology") (v "0.5.0") (d (list (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "truck-base") (r "^0.4.0") (d #t) (k 0)) (d (n "truck-geotrait") (r "^0.3.0") (d #t) (k 0)))) (h "1r4qfqcpb5zg1r0ji0halmbbigm2c1jf6lmxqlrwn2m39j4qpmr5")))

