(define-module (crates-io tr uc truck-base) #:use-module (crates-io))

(define-public crate-truck-base-0.1.0 (c (n "truck-base") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y8fhdn1r8ph59p62l9v84nd5zp4hb5nic9kip48290cra3xsdxk")))

(define-public crate-truck-base-0.1.1 (c (n "truck-base") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.18.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)))) (h "025l9fryywpki0yj5qm9q1svl54ypkqrd66issfx7sngbkxrsisz")))

(define-public crate-truck-base-0.2.0 (c (n "truck-base") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.18.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zaqyk0s9jrpv9fhvz5m40jbd9qw359mb91f3h8fvg3bf26wjg1s")))

(define-public crate-truck-base-0.3.0 (c (n "truck-base") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.18.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "matext4cgmath") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "106rkxv97gvdnqxmpjgswvzj1vqbf2phhgxd57bi7a365ryk8lv1")))

(define-public crate-truck-base-0.4.0 (c (n "truck-base") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.18.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "matext4cgmath") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)))) (h "12d5favimvlwbw2x4fg2lq43x6q8xx1z6g5kgdikqbqfx07aqils")))

