(define-module (crates-io tr uc truck-polymesh) #:use-module (crates-io))

(define-public crate-truck-polymesh-0.1.0 (c (n "truck-polymesh") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "obj") (r "^0.10.2") (d #t) (k 0)) (d (n "truck-base") (r "^0.1.0") (d #t) (k 0)) (d (n "truck-topology") (r "^0.1.0") (d #t) (k 0)))) (h "07ii4rfdb1yjj6qar2g85chhls9a8h2b9w8gnrbgy45ymnclflbd")))

(define-public crate-truck-polymesh-0.2.0 (c (n "truck-polymesh") (v "0.2.0") (d (list (d (n "obj") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "truck-base") (r "^0.1.1") (d #t) (k 0)))) (h "003yx89sb8hirdqcz89za8fgif90br7kiqmh4mf2nsmdjmmps7w0")))

(define-public crate-truck-polymesh-0.2.1 (c (n "truck-polymesh") (v "0.2.1") (d (list (d (n "obj") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "truck-base") (r "^0.1.1") (d #t) (k 0)))) (h "0mj5rj7rhv8wjd3wsa0g9bv6vlf98n1lv49gmqychgk3qx4qcnv7")))

(define-public crate-truck-polymesh-0.3.0 (c (n "truck-polymesh") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "truck-base") (r "^0.2.0") (d #t) (k 0)) (d (n "truck-geotrait") (r "^0.1.0") (d #t) (k 0)))) (h "1zikpkl5gmqnwvd76vn2aqffxladpifdyp6m4nil7afdaff5ycj3")))

(define-public crate-truck-polymesh-0.4.0 (c (n "truck-polymesh") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "truck-base") (r "^0.3.0") (d #t) (k 0)) (d (n "truck-geotrait") (r "^0.2.0") (d #t) (k 0)))) (h "0ilnl53rzwkwm6ldgxnjwmary800nirs6jbpy7xgsbfjqirkilkv")))

(define-public crate-truck-polymesh-0.5.0 (c (n "truck-polymesh") (v "0.5.0") (d (list (d (n "bytemuck") (r "^1.12.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "truck-base") (r "^0.4.0") (d #t) (k 0)) (d (n "truck-geotrait") (r "^0.3.0") (d #t) (k 0)))) (h "0pc889i0d6mhrvxcyf8x90jjvyrzcr2vj68a1n2x6j9w6lfx3rz2")))

