(define-module (crates-io tr as trashed) #:use-module (crates-io))

(define-public crate-trashed-1.0.0 (c (n "trashed") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "trash") (r "^2.1.5") (f (quote ("coinit_multithreaded"))) (d #t) (k 0)))) (h "1qal5ddlyh4qkmjpk8jlyhyn0qszmndfcxa8ww18jlzwsbyw8kms")))

