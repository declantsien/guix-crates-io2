(define-module (crates-io tr as trashy-xml) #:use-module (crates-io))

(define-public crate-trashy-xml-0.1.0 (c (n "trashy-xml") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1d5l3lpm21mjpd15a347ba28anxvrs3nlnp1nxh5bnc37qn67b3s")))

(define-public crate-trashy-xml-0.1.1 (c (n "trashy-xml") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "19j189g46a6mc00caw0bx3d7f21i24kbya4xhfb4vpnk6iid453s")))

(define-public crate-trashy-xml-0.2.1 (c (n "trashy-xml") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1icz0pbzbz3h2gjq8zq69y5zgbvkqangfc46n8gxhzz8cy93isbl")))

(define-public crate-trashy-xml-0.2.2 (c (n "trashy-xml") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "06a6bnb2vrnq2aava36iqpsanl6xjlvk4dcwrqpdlznx4q421wfs")))

(define-public crate-trashy-xml-0.3.0 (c (n "trashy-xml") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 0)))) (h "00vc612dvq3x03fxzjgw0xmp8gy8zyy6vh74b3g5zdlq6c1xcxsd")))

(define-public crate-trashy-xml-0.4.0 (c (n "trashy-xml") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "1kl0bcximyf7sgjpd6rl0k81rvrppa39zbgxihxp29kh5bcjzvcb")))

(define-public crate-trashy-xml-0.4.1 (c (n "trashy-xml") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "1nlaz7ydhplhk6ziwf2az5w6mig34j005icyf3iadw4s4n5gjk4f")))

(define-public crate-trashy-xml-0.5.0 (c (n "trashy-xml") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "1rl0fgkr6n6r6cg372f09b6x6nggqvdiwwry5fhfm1h11sz7ac86")))

(define-public crate-trashy-xml-0.5.1 (c (n "trashy-xml") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "08mb29wpkn6zg46jc1k29haaya8g4aa4k8avfbgb1sbn9qygnfw4")))

(define-public crate-trashy-xml-0.5.2 (c (n "trashy-xml") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "1fv762dhqghi1y8w7clfbzpbx7zz6li4aw0flnv1rzfai2rslx10")))

(define-public crate-trashy-xml-0.5.3 (c (n "trashy-xml") (v "0.5.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "14pfyvvm3981gmg4jxx82a41zk51b70mg2fb6nachhx4mm40wrlk")))

(define-public crate-trashy-xml-0.6.0 (c (n "trashy-xml") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "00llym1n25k7vxnm1lqrcl79qvf38pmhgkdf0qq8cwcsnxqanw62")))

(define-public crate-trashy-xml-0.7.0 (c (n "trashy-xml") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "0iaxrgsx1bx6x4m5x5kfrkyyhxm14sw4430q2hskiz42jidpkzwj")))

(define-public crate-trashy-xml-0.10.0 (c (n "trashy-xml") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.25") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "005n9wl8nz7yr2cgr22fiwx2f8j3gx165pm4bprkdidb39m388b3")))

(define-public crate-trashy-xml-0.10.1 (c (n "trashy-xml") (v "0.10.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.25") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0ljhsb5y5vzk957zx5mqil279xinwrw5d5vz3h2fg0amhglqqh1y")))

(define-public crate-trashy-xml-0.11.0 (c (n "trashy-xml") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.25") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "08962jpfhs7235rd0cn18vgflc2fs7bc5a9kdw69s5fis0wpsyyf")))

