(define-module (crates-io tr as trash_lib) #:use-module (crates-io))

(define-public crate-trash_lib-0.1.0 (c (n "trash_lib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 2)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0j19bajh873jn4p3v2mrxrk17z71rih89l4xy1i1lq3pz8s5x5s4")))

