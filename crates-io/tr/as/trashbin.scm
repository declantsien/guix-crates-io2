(define-module (crates-io tr as trashbin) #:use-module (crates-io))

(define-public crate-trashbin-0.1.2 (c (n "trashbin") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sincere") (r "^0.7.0-alpha.1") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "00z7zrhv1hwci0023kp79zphl7qw5sh8vx0zzlb8vza2hl1ilaj8")))

(define-public crate-trashbin-0.2.0 (c (n "trashbin") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.7.0") (d #t) (k 0)))) (h "08xh1pp561d8xk2qf5h5119yvxx7mnggn57pjplvds3bq9jzr8wy")))

(define-public crate-trashbin-0.2.1 (c (n "trashbin") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.7.0") (d #t) (k 0)))) (h "188nr23wvclqwdywjlxqqkbnm2mmyhwa0fn0c671xk7wfw2c84j0")))

(define-public crate-trashbin-0.3.0 (c (n "trashbin") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0r8prsnmh3g7jg7na8w14c0rmmydjjpwkf9pwxinld81n16dffsk")))

(define-public crate-trashbin-0.3.1 (c (n "trashbin") (v "0.3.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "19f7c1hmybmhlji3vn3fcg0xa3saxmvqrwsj12mjqbbbjsbgdsdx")))

(define-public crate-trashbin-0.3.2 (c (n "trashbin") (v "0.3.2") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0ijqd8ms7lrj5pk8w81b759dqv1lr6jxqcmlj7624bwy8pli2d5j")))

(define-public crate-trashbin-0.3.3 (c (n "trashbin") (v "0.3.3") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1bd7c3mic9lww3vy96a2j0ncn3lszc7i88apqgjhfxxsxisp8rmr")))

(define-public crate-trashbin-0.3.4 (c (n "trashbin") (v "0.3.4") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1fra49ngv8ljz7y4ri2hbfnb4ba8kbk17jbq345w7nx7qn0ccaig")))

(define-public crate-trashbin-0.4.0 (c (n "trashbin") (v "0.4.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1lzdd5kk2yr371jvr0rfmwiv9c663mm2ymvymhhs8v6w7yn6lwm2")))

