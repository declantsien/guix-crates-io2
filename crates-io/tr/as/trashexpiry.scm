(define-module (crates-io tr as trashexpiry) #:use-module (crates-io))

(define-public crate-trashexpiry-0.1.0 (c (n "trashexpiry") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0kvvxd1wrlcankjy4kd929w40bp56ysqzwqfb740wywqkplbavms")))

(define-public crate-trashexpiry-0.1.1 (c (n "trashexpiry") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "1fz71zv9g39nv2z6z3ab18hilrd5bank2p16kb3ksbbkvv753176")))

