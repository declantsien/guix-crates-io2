(define-module (crates-io tr as trash_dumper) #:use-module (crates-io))

(define-public crate-trash_dumper-1.0.0 (c (n "trash_dumper") (v "1.0.0") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1x3vnvjgch9qxrmqkfrnl2fvmmciv9dqm1287q6gkg2yqrqap5ci")))

(define-public crate-trash_dumper-1.0.1 (c (n "trash_dumper") (v "1.0.1") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1k8vkirbjnyvmpzr62cnpbda2g6slpcy47kk8lbwa8c70vgfk7sc")))

(define-public crate-trash_dumper-1.0.2 (c (n "trash_dumper") (v "1.0.2") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1ss7jfll6vckvix8rcwl9im55ns6kxvhzvs7bxyn34qani3riliz")))

