(define-module (crates-io tr as trashmap) #:use-module (crates-io))

(define-public crate-trashmap-0.1.0 (c (n "trashmap") (v "0.1.0") (h "0j93404cdcqcfpkfhzv8jkn7nlsxdyv5vqdwm3vdvf8vkmab74sc")))

(define-public crate-trashmap-0.1.1 (c (n "trashmap") (v "0.1.1") (h "0m2wk2p74vyszkd0qhhlfks6hcnb3hg1lllfb2qlr2mvamz5yrr1")))

(define-public crate-trashmap-0.1.2 (c (n "trashmap") (v "0.1.2") (h "1nzqgvgbyv5l1lfz1xd89s83zr7y203znnppsyngck94v973m71m")))

(define-public crate-trashmap-0.1.3 (c (n "trashmap") (v "0.1.3") (h "1sqq0b761pdliix94vxankqnf8zmwh492ya6ycifv9x57ihkbbp1")))

