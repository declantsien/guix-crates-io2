(define-module (crates-io tr as trassel) #:use-module (crates-io))

(define-public crate-trassel-0.1.0 (c (n "trassel") (v "0.1.0") (d (list (d (n "durations") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0z7sj7482l2342p0w0hjwhdgk648axhq600j03jnhgrmavdannk0")))

(define-public crate-trassel-0.2.0 (c (n "trassel") (v "0.2.0") (d (list (d (n "durations") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1vg8673bxc8ll7naqgdxy8m64h8m1kmwqn8w63j28avfld6h1jzx")))

(define-public crate-trassel-0.2.1 (c (n "trassel") (v "0.2.1") (d (list (d (n "durations") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "054q2mx578bpkg2518x93r90clsl4m8s1rqwniamm8wwi81mydva")))

(define-public crate-trassel-0.3.1 (c (n "trassel") (v "0.3.1") (d (list (d (n "durations") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1bd7vq0drsq6gh6pyfvx2qlashlybbsph6k0dyh3ybbvkhi781qb")))

(define-public crate-trassel-0.3.2 (c (n "trassel") (v "0.3.2") (d (list (d (n "durations") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0kb3qg0m79kffr2q852l2n62sdxbnw51l3nkr7immglcw06b2pz6")))

(define-public crate-trassel-0.3.3 (c (n "trassel") (v "0.3.3") (d (list (d (n "durations") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "001rq9xjlp6rkcqpcb7bfxqkn8y34gbycyx3psqbqw8gs3c5ijjq")))

(define-public crate-trassel-0.4.0 (c (n "trassel") (v "0.4.0") (d (list (d (n "durations") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0qrycdl9s2r5azki3mxscx1gvsymigrrrblq1l3q7yy9pkchcyzk")))

(define-public crate-trassel-0.4.1 (c (n "trassel") (v "0.4.1") (d (list (d (n "durations") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0jid7g2wkl40y5m2i0f0gs69sc48v6w962wzgw5lkynjrpk2bgj7")))

(define-public crate-trassel-1.0.0 (c (n "trassel") (v "1.0.0") (d (list (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "14ckgnmy0ppqzmp0j1l8vlvc0m5g7bwdraa8nndcqxnx91x9kszr")))

