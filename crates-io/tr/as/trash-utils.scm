(define-module (crates-io tr as trash-utils) #:use-module (crates-io))

(define-public crate-trash-utils-0.1.0 (c (n "trash-utils") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)))) (h "046nc4b6gdnf00sypi5pjgs8j20lrv1yfwdimij5m11nxvf80lci")))

(define-public crate-trash-utils-0.1.1 (c (n "trash-utils") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)))) (h "01gsf5r1y5qjb8qpr76hjb2c6xiy6qyrccvpyfiwx3zqkd0d8whb")))

(define-public crate-trash-utils-0.2.0 (c (n "trash-utils") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)))) (h "1iajgfxpz830bcn8l22pflpas6lsxym8mmcvqnpwplrngiwhsa7n")))

