(define-module (crates-io tr as trasy) #:use-module (crates-io))

(define-public crate-trasy-0.1.0 (c (n "trasy") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2.0") (d #t) (k 0)))) (h "0z8nq2j5wj661xcxy9363p715zgacqacyv3h0agyvp5nvc5q8dah")))

(define-public crate-trasy-0.1.1 (c (n "trasy") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2.0") (d #t) (k 0)))) (h "17jrv53caz1dds75ggqz67iwgzlhnpmz70nzyq9irzc8m950vh10")))

(define-public crate-trasy-0.1.2 (c (n "trasy") (v "0.1.2") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2.0") (d #t) (k 0)))) (h "0qcmq2bbqj59a21zzn7gz0sm989z10xnjhgzfwj5881cd14j4gk5")))

(define-public crate-trasy-0.1.3 (c (n "trasy") (v "0.1.3") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2.0") (d #t) (k 0)))) (h "0s87c9311jw1x3g9yrajl2jm09yz70k0jr6d6licfhba4h25d1nj")))

(define-public crate-trasy-0.1.4 (c (n "trasy") (v "0.1.4") (d (list (d (n "opentelemetry") (r "^0.22") (f (quote ("trace"))) (d #t) (k 0)) (d (n "opentelemetry-otlp") (r "^0.15") (f (quote ("http-proto" "reqwest-client"))) (d #t) (k 0)) (d (n "opentelemetry_sdk") (r "^0.22") (f (quote ("rt-tokio"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing-opentelemetry") (r "^0.23") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("registry" "env-filter"))) (d #t) (k 0)))) (h "1bnzdrjdgqjvj5wg64khypyaqspdbn6jdylnhz22p8pd1bxlf4wz")))

