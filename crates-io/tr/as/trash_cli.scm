(define-module (crates-io tr as trash_cli) #:use-module (crates-io))

(define-public crate-trash_cli-0.1.0 (c (n "trash_cli") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "0gaibdkamfa41r9ai3ig1mby1s7v8j967w0gjfkhnmfc2nvcrkrq")))

