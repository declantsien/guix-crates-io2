(define-module (crates-io tr ev trevor) #:use-module (crates-io))

(define-public crate-trevor-0.1.0 (c (n "trevor") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "166jx6xcih6ia9jccnpw2zb9hhsx56j4kh5a7c7syzzvxbkqygv6")))

(define-public crate-trevor-0.1.1 (c (n "trevor") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0y1h52b39jrkm8fz9szv7yaq9xlqgm5701jir53b63sp4n7vcqhn")))

