(define-module (crates-io tr ev trev) #:use-module (crates-io))

(define-public crate-trev-0.1.5 (c (n "trev") (v "0.1.5") (h "0x67rj10xhwd5f6waj9b1wnnivh40inad37dp93mpcyr5y8kazkn")))

(define-public crate-trev-0.1.6 (c (n "trev") (v "0.1.6") (h "15zlq0m86mjqy7rgh76csvjhqshdl7iczkqg1qcimlbifcz4a645")))

