(define-module (crates-io tr ev trevordmiller) #:use-module (crates-io))

(define-public crate-trevordmiller-1.0.0 (c (n "trevordmiller") (v "1.0.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0cwyb9054jpaqqm4jjb13k296nmxli79iypzq5iiymdyz3p6rqs3")))

(define-public crate-trevordmiller-1.0.1 (c (n "trevordmiller") (v "1.0.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "115x06w3yi2yvi5y82vnlph7li2jzsczxfgk91sgvnlrrq77zyd7")))

(define-public crate-trevordmiller-1.0.2 (c (n "trevordmiller") (v "1.0.2") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "029asn4kxhv7y8j27h1ka93p4l9vxsdq233ia5a03ls8ag9psnil")))

(define-public crate-trevordmiller-1.0.3 (c (n "trevordmiller") (v "1.0.3") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "1v5axkz03sfy1izkw471jwf8ih0dk4m4zd1ydxdmrv82ynbg5hrs")))

(define-public crate-trevordmiller-1.0.4 (c (n "trevordmiller") (v "1.0.4") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "17xg38qjnjzfm5xgkksc2jn0z58pw6g4k71yf9ck8bl933s29l04")))

(define-public crate-trevordmiller-1.0.5 (c (n "trevordmiller") (v "1.0.5") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0kyqx2yh1zarwhiwb9db8dxy246fxklfqjbxvak34mm9wnj4i6xg")))

(define-public crate-trevordmiller-1.0.6 (c (n "trevordmiller") (v "1.0.6") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "16cg4ap0v8581kx3k7i0c65bi8d00ski72ayq8vwhzcg2ib0z8v5")))

(define-public crate-trevordmiller-1.0.7 (c (n "trevordmiller") (v "1.0.7") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "10c7cav9m8j8lk4j3n94s89yv44bqdb7ipx1q377ljzmdfprdqrx")))

(define-public crate-trevordmiller-1.0.8 (c (n "trevordmiller") (v "1.0.8") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0m016dk3ylq0i79p9mnz5dlwldkadxqbi05lcd25ipzd4yr7ww4w")))

(define-public crate-trevordmiller-1.1.0 (c (n "trevordmiller") (v "1.1.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "19pcq0nyxzpn5md0hg3y0kqvrqh4filf277a66p3chmg50aivs2p")))

(define-public crate-trevordmiller-1.1.1 (c (n "trevordmiller") (v "1.1.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "1riq4fsbk96sx16q74fbwmyab25q5j8idbx4wqncav336giyi1py")))

(define-public crate-trevordmiller-1.1.2 (c (n "trevordmiller") (v "1.1.2") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "19fcn75rfgirmspqaiw34dwrh6n1rag54lr0cnmq69si7ikjz1qr")))

(define-public crate-trevordmiller-1.1.3 (c (n "trevordmiller") (v "1.1.3") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "04xx4h4i2cw0bikxy3zw7cnzwcvk10fd41ihipdbgbfbn204dsrp")))

(define-public crate-trevordmiller-1.1.4 (c (n "trevordmiller") (v "1.1.4") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0i3kbrcs6l2ksswv4v3wwr10hi2b3msf80ljbfi7xyyqnsriynna")))

