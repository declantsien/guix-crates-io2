(define-module (crates-io tr en trench) #:use-module (crates-io))

(define-public crate-trench-0.1.0 (c (n "trench") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0gb687cbl158sw624b374pk9ddc44g5c7s3d2skjl91ks2wdjm7l")))

(define-public crate-trench-0.1.1 (c (n "trench") (v "0.1.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0x5bq9pbckxl1lcd3ga0m83visd391frg0j8a6jc4dbj7r7r3z02")))

(define-public crate-trench-0.1.2 (c (n "trench") (v "0.1.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1r7m96zb11rrcxbhnrf5zd7zw36rd03m67kvs23vaxg83alqrmwa")))

(define-public crate-trench-0.1.3 (c (n "trench") (v "0.1.3") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0lsmrj19ha5yag0xv7kzkzaaq9crnnabgl4xln2n562wd9dx9zj3")))

(define-public crate-trench-0.3.0 (c (n "trench") (v "0.3.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0am9ya2kpnb87pb524zkh5gfm19linycrkb00cvbsbjsh81dvww8")))

