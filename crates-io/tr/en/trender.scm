(define-module (crates-io tr en trender) #:use-module (crates-io))

(define-public crate-trender-0.0.1 (c (n "trender") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1ly7gznzyfv7nqq5h0inzcw3k46hmxmqwavfajb7vd7d29rhhmaf")))

(define-public crate-trender-0.0.2 (c (n "trender") (v "0.0.2") (d (list (d (n "crossterm") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "0avpzbsfff0a6xjq1r3nbfkwmwbpm9fh4wx0703k85idd2jby081")))

