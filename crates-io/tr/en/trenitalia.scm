(define-module (crates-io tr en trenitalia) #:use-module (crates-io))

(define-public crate-trenitalia-0.1.0 (c (n "trenitalia") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)))) (h "1s87b9yndcj3c6ahbimi09cbxryyvrl0wapa9j7fympbdxd7q6xz")))

(define-public crate-trenitalia-0.1.1 (c (n "trenitalia") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "iso_currency") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)))) (h "0aypvd50z5ky0mvmi8yqs5ld1awq1r5s0sl71ass71isy3shhax0")))

(define-public crate-trenitalia-0.1.2 (c (n "trenitalia") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "iso_currency") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)))) (h "1xrxw1s3pild9q2hpbqlalz16968272ff85naxi8s7yr66sjvyng") (y #t)))

(define-public crate-trenitalia-0.1.3 (c (n "trenitalia") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "iso_currency") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)))) (h "1sbx7nki31vax89jqjjxh8qrm4h2rv398h4glp1fkxy1mshjrllf")))

(define-public crate-trenitalia-0.1.4 (c (n "trenitalia") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "iso_currency") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)))) (h "0q3c59vnixcqns7yn536zamn7vl3xslhhrkbn5ni4lm05sgab3pc")))

(define-public crate-trenitalia-0.2.0 (c (n "trenitalia") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "iso_currency") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)))) (h "1l7spr53wr6jgal5dpknxbkyqr37lj920qm00pbm01yqybjhzka8")))

(define-public crate-trenitalia-0.3.0 (c (n "trenitalia") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)))) (h "04vvrwbj0lvzylwrcvqk302vb2czrl9gk44g8r3awb2zqwl9xpbd")))

(define-public crate-trenitalia-0.3.1 (c (n "trenitalia") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)))) (h "1c1iffnxx4cvxfwk0sgr177r5i1r7js411wm4kialbb8zavml2m0") (y #t)))

(define-public crate-trenitalia-0.3.2 (c (n "trenitalia") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)))) (h "16k8x3m8c481x47q6d12fxrs33zfsfdzf93n3fxbvn7395a52sx7")))

(define-public crate-trenitalia-0.3.3 (c (n "trenitalia") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)) (d (n "ureq") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)))) (h "06kiad2cikdf735aw0pc6147gmnij26h5hil3zdvjlph9p6pckb3")))

(define-public crate-trenitalia-0.3.4 (c (n "trenitalia") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)) (d (n "ureq") (r "^0.11") (f (quote ("default" "json"))) (d #t) (k 0)))) (h "13bpfaqnlzagyr0jhrqalk534s66kj6j8xvgnviqds63x7sddk8k")))

(define-public crate-trenitalia-0.3.5 (c (n "trenitalia") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (f (quote ("default" "json"))) (d #t) (k 0)))) (h "0f6v3f1ikxjhsv6d0qjhibdsxbhqwsa8qmh6sx8qa48n43drklrz")))

(define-public crate-trenitalia-0.3.6 (c (n "trenitalia") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.11") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (f (quote ("default" "json"))) (d #t) (k 0)))) (h "1x5d90nhl1mh3cckkvm0g2nrz0kaivyhynchxs2p7hjnla8pxzqn")))

