(define-module (crates-io tr en trend-rs) #:use-module (crates-io))

(define-public crate-trend-rs-0.2.0 (c (n "trend-rs") (v "0.2.0") (d (list (d (n "async-stream") (r "^0.3.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("full"))) (d #t) (k 0)))) (h "0igab6l3a369db3nk34xwndnv0py6s0phk8hc0fhlkwjs7nfry85")))

