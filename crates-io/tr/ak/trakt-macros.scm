(define-module (crates-io tr ak trakt-macros) #:use-module (crates-io))

(define-public crate-trakt-macros-0.1.0 (c (n "trakt-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xlw9ih95lwwxp8wkmnwnyk218vj68yaa70sr5c7jrs5acpnhf2v")))

(define-public crate-trakt-macros-0.1.1 (c (n "trakt-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0z6am545mdd7g5sbihp5wywv4k9y8liqfzvb3wwq1ca6pzpas5jx")))

(define-public crate-trakt-macros-0.1.2 (c (n "trakt-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "16lmwglfhhm0y87pchfbq3vbp1kzqznmsz85ssdkp93sqzv1wk64")))

(define-public crate-trakt-macros-0.1.3 (c (n "trakt-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "117cn993rmnkzj378kv0qh3b1c8jx7f6l3d50bw26nfr3z5zdgqa")))

(define-public crate-trakt-macros-0.1.4 (c (n "trakt-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0hx09vhryrvgng03mad5587amip5fpa94z7v6zffic21vjx3mm44")))

