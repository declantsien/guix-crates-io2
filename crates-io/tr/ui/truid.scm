(define-module (crates-io tr ui truid) #:use-module (crates-io))

(define-public crate-truid-0.1.0 (c (n "truid") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "06kj2szxzb7crqxq2s41lzqi23ryssqc75qq808rknbb3agbnmz4")))

