(define-module (crates-io tr ua truant) #:use-module (crates-io))

(define-public crate-truant-1.0.13 (c (n "truant") (v "1.0.13") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0znmlvpz66dz4dvg738v9qch0jfmhj5cbabmhbqxdfqp8mq8q3vc")))

(define-public crate-truant-1.2.0 (c (n "truant") (v "1.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01k8ini2nia8b7d4mb6g6jszagmhhmjzripfp7nkzzpr72mq3wy0")))

