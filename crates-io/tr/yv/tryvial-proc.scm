(define-module (crates-io tr yv tryvial-proc) #:use-module (crates-io))

(define-public crate-tryvial-proc-0.1.0 (c (n "tryvial-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g9ibyxa5hn58sk2knfzjyk9nclr5plxa0ai53799phnys2qnd15") (y #t)))

(define-public crate-tryvial-proc-0.1.1 (c (n "tryvial-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18bh0gc9j257zk3cj0hqshr7pv91k9wzfxzy2d6ahys7zgqz09vg") (y #t)))

(define-public crate-tryvial-proc-0.1.2 (c (n "tryvial-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)))) (h "1chis1774hzfx6pjny3n15z4falc4j02kggbflm4lwi5q2kkp9aa")))

(define-public crate-tryvial-proc-0.2.0 (c (n "tryvial-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)))) (h "0jfis116bffy4kbbdzncqlkpd3agp7hygny4plkffnqxsxgj9mvq")))

