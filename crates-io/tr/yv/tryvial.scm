(define-module (crates-io tr yv tryvial) #:use-module (crates-io))

(define-public crate-tryvial-0.1.0 (c (n "tryvial") (v "0.1.0") (d (list (d (n "tryvial-proc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0izxgwf51aimqrlrcapgcx7q2sz63mjl7xmhajhw64sr3has22hh") (f (quote (("default" "proc-macro")))) (y #t) (s 2) (e (quote (("proc-macro" "dep:tryvial-proc"))))))

(define-public crate-tryvial-0.1.1 (c (n "tryvial") (v "0.1.1") (d (list (d (n "tryvial-proc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "130wchhg68g62w7lg65rl3g9rn8c2av6cgx7jfrhq6fv0ih2fywv") (f (quote (("default" "proc-macro")))) (y #t) (s 2) (e (quote (("proc-macro" "dep:tryvial-proc"))))))

(define-public crate-tryvial-0.1.2 (c (n "tryvial") (v "0.1.2") (d (list (d (n "tryvial-proc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1x1ml56jndvq8k7b7ybn59kggg9p0mzl4cmvhr6b3m6sfwl5zylk") (f (quote (("default" "proc-macro")))) (s 2) (e (quote (("proc-macro" "dep:tryvial-proc"))))))

(define-public crate-tryvial-0.2.0 (c (n "tryvial") (v "0.2.0") (d (list (d (n "tryvial-proc") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1vk4jhnq65zc00gcqqxs433wmi1vjrwpwykxzi22z9nm69pvxszd") (f (quote (("default" "proc-macro")))) (s 2) (e (quote (("proc-macro" "dep:tryvial-proc"))))))

