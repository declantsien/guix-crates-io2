(define-module (crates-io tr ie trie-generic) #:use-module (crates-io))

(define-public crate-trie-generic-0.1.0 (c (n "trie-generic") (v "0.1.0") (h "184f055y481v091lk1ni0fblq440nyw1mlr6fskwnw6b0as3z5ik")))

(define-public crate-trie-generic-0.1.1 (c (n "trie-generic") (v "0.1.1") (h "0pq83bda5gkd1fb95kqy129i0xaka0wh86071ni4hrxrxivp059b")))

(define-public crate-trie-generic-0.1.2 (c (n "trie-generic") (v "0.1.2") (h "0ivchbl2dj3f1ngxq2ml3vnpp7ak6kfcw8cvlms5zq4m8pkff7f3")))

(define-public crate-trie-generic-0.1.3 (c (n "trie-generic") (v "0.1.3") (h "1hhqy3bdv8j67bsk0vijv0q4sfcakldahzczd86nrd85gxdxgyd7")))

(define-public crate-trie-generic-0.1.4 (c (n "trie-generic") (v "0.1.4") (h "1l56m3ay8pxf6jwgbq1y7qmq13ggak9w0in6f7v3yiv96v35zy7c")))

(define-public crate-trie-generic-0.1.5 (c (n "trie-generic") (v "0.1.5") (h "034nxxyhfm4il46py4ws37savkczz58ps4cpl5gj94hk582m7ln7")))

(define-public crate-trie-generic-0.1.6 (c (n "trie-generic") (v "0.1.6") (h "09qv24j9sgj4s2qp7jnvp5nhx7jrfcgrm11l4prsay8z0bngdcg1")))

(define-public crate-trie-generic-0.1.7 (c (n "trie-generic") (v "0.1.7") (h "1j7mv2casabqjclsamy6splz35iiyvkxij8g942p9ncpr6139dvd")))

(define-public crate-trie-generic-1.1.8 (c (n "trie-generic") (v "1.1.8") (h "0aaw9zd028rn4agyb47jg8ry0mhy339ckazmibwvspbh5kmzd3yg")))

