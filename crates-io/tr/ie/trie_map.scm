(define-module (crates-io tr ie trie_map) #:use-module (crates-io))

(define-public crate-trie_map-0.1.0 (c (n "trie_map") (v "0.1.0") (h "040m6f8g87vfql5wlgwrnchh9bichg2rg7v09wi5czzzs1sxpdwi")))

(define-public crate-trie_map-0.2.0 (c (n "trie_map") (v "0.2.0") (h "0188fg1sny7p6xq8r3dhgi6wrd9jv16hjhd7b5l564835ims0lpz")))

