(define-module (crates-io tr ie trie_db_impl_vsdb) #:use-module (crates-io))

(define-public crate-trie_db_impl_vsdb-0.1.0 (c (n "trie_db_impl_vsdb") (v "0.1.0") (d (list (d (n "backend") (r "^0.1.1") (d #t) (k 0) (p "hash-db-impl-vsdb")) (d (n "ruc") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-trie") (r "^19.0") (d #t) (k 0)) (d (n "vsdb") (r "^0.51.1") (d #t) (k 0)))) (h "0w7ib8h4yd58bp3c3hif8r0kwjlxsipcv1bjn6mrvshj8nyjp583") (y #t)))

(define-public crate-trie_db_impl_vsdb-0.2.0 (c (n "trie_db_impl_vsdb") (v "0.2.0") (d (list (d (n "backend") (r "^0.1.2") (d #t) (k 0) (p "hash-db-impl-vsdb")) (d (n "ruc") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-trie") (r "^19.0") (d #t) (k 0)) (d (n "vsdb") (r "^0.51.1") (d #t) (k 0)))) (h "12ah96ivkc3vmyjmj9w8gwa74x892w5y3ghns5pld8js0vnvbj3c") (y #t)))

(define-public crate-trie_db_impl_vsdb-0.2.1 (c (n "trie_db_impl_vsdb") (v "0.2.1") (d (list (d (n "backend") (r "^0.1.2") (d #t) (k 0) (p "hash-db-impl-vsdb")) (d (n "ruc") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-trie") (r "^19.0") (d #t) (k 0)) (d (n "vsdb") (r "^0.51.1") (d #t) (k 0)))) (h "1ql2x8mwz348vxa640xnyknjhihnc42230q8y62qlig8y51flr2v") (y #t)))

(define-public crate-trie_db_impl_vsdb-0.3.1 (c (n "trie_db_impl_vsdb") (v "0.3.1") (d (list (d (n "backend") (r "^0.2.0") (d #t) (k 0) (p "hash-db-impl-vsdb")) (d (n "ruc") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-trie") (r "^19.0") (d #t) (k 0)) (d (n "vsdb") (r "^0.51.1") (d #t) (k 0)))) (h "16vqiaf19nxsv25pr56k4pxiab7s39m8dsh9r91fj9n4sgmv0pn5") (y #t)))

(define-public crate-trie_db_impl_vsdb-0.3.2 (c (n "trie_db_impl_vsdb") (v "0.3.2") (d (list (d (n "backend") (r "^0.2.0") (d #t) (k 0) (p "hash-db-impl-vsdb")) (d (n "ruc") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-trie") (r "^19.0") (d #t) (k 0)) (d (n "vsdb") (r "^0.51.1") (d #t) (k 0)))) (h "101m36qynqdk98ynd4yjv89m1vdm88bgcqspq2yz1fg269yhhafm") (y #t)))

(define-public crate-trie_db_impl_vsdb-0.4.0 (c (n "trie_db_impl_vsdb") (v "0.4.0") (d (list (d (n "backend") (r "^0.3.1") (k 0) (p "hash-db-impl-vsdb")) (d (n "ruc") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-trie") (r "^19.0") (d #t) (k 0)))) (h "0fmdmdhc33qc1kvb98qm3qaypnr3q29z20ng2igab8i4fbpxngvx") (f (quote (("sled_engine" "backend/sled_engine") ("rocks_engine" "backend/rocks_engine") ("msgpack_codec" "backend/msgpack_codec") ("json_codec" "backend/json_codec") ("default" "sled_engine" "bcs_codec") ("bcs_codec" "backend/bcs_codec")))) (y #t)))

