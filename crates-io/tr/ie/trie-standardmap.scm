(define-module (crates-io tr ie trie-standardmap) #:use-module (crates-io))

(define-public crate-trie-standardmap-0.1.0 (c (n "trie-standardmap") (v "0.1.0") (d (list (d (n "ethcore-bytes") (r "^0.1.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.3") (d #t) (k 0)) (d (n "keccak-hash") (r "^0.1.1") (d #t) (k 0)) (d (n "rlp") (r "^0.2.2") (d #t) (k 0)))) (h "0swyp58wccxaz2dv21wmvplizgf2xksvvf3zqr5w027gp1v8i9cq")))

(define-public crate-trie-standardmap-0.1.1 (c (n "trie-standardmap") (v "0.1.1") (d (list (d (n "ethereum-types") (r "^0.4") (d #t) (k 0)) (d (n "keccak-hash") (r "^0.1") (d #t) (k 0)) (d (n "parity-bytes") (r "^0.1") (d #t) (k 0)) (d (n "rlp") (r "^0.2.4") (d #t) (k 0)))) (h "0pzah2s97vmb7pf7igdbiv83bb4jf34cwvvdc5chhym5fqlza9hf")))

(define-public crate-trie-standardmap-0.9.0 (c (n "trie-standardmap") (v "0.9.0") (d (list (d (n "criterion") (r "^0.1.2") (d #t) (k 0)) (d (n "hash-db") (r "^0.9.0") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.2.0") (d #t) (k 0)))) (h "1rvc6m60qg4zxhjkyip917hp4qlzv1szyy7f2ad70jj00valrpw6")))

(define-public crate-trie-standardmap-0.9.1 (c (n "trie-standardmap") (v "0.9.1") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 0)) (d (n "hash-db") (r "^0.9.0") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.2.1") (d #t) (k 0)))) (h "0vk1vyrg1hhmjzmd2d3y142adxqkgnax5zm6yv9r66g6n5zk7nih")))

(define-public crate-trie-standardmap-0.11.0 (c (n "trie-standardmap") (v "0.11.0") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 0)) (d (n "hash-db") (r "^0.11.0") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.11.0") (d #t) (k 0)))) (h "1chsyvq1s6g8a4a0hag3y0gyn6nlcimnxwgkpsqsq0l19d82jiqf")))

(define-public crate-trie-standardmap-0.12.0 (c (n "trie-standardmap") (v "0.12.0") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 0)) (d (n "hash-db") (r "^0.12.0") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.12.0") (d #t) (k 0)))) (h "11kcv1jgch7c4qphmwsmqddhjy3rmn9zsrlfhx5999rf9zsi8qq0")))

(define-public crate-trie-standardmap-0.12.2 (c (n "trie-standardmap") (v "0.12.2") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 0)) (d (n "hash-db") (r "^0.12.2") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.12.2") (d #t) (k 0)))) (h "0ys799vamcavp46fzj5r7pqq7gjll7ipib03mwx8zwq5mxvl5qp4")))

(define-public crate-trie-standardmap-0.12.3 (c (n "trie-standardmap") (v "0.12.3") (d (list (d (n "hash-db") (r "^0.12.2") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.12.2") (d #t) (k 0)))) (h "0lczl9yzg6g32kp49p1h08rymdc5wpzjsaw7v3x6w6a600s4papb") (y #t)))

(define-public crate-trie-standardmap-0.12.4 (c (n "trie-standardmap") (v "0.12.4") (d (list (d (n "hash-db") (r "^0.12.4") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.12.4") (d #t) (k 0)))) (h "1lr49c3z5vvly3j20bps3afy0x52gs9h7hrbs5bfv5rslsqpyy20")))

(define-public crate-trie-standardmap-0.14.0 (c (n "trie-standardmap") (v "0.14.0") (d (list (d (n "hash-db") (r "^0.14.0") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.14.0") (d #t) (k 0)))) (h "0m1vvd3gvs5qbp7nwxvzq7n6azml4rz1z0fxgwjcgp6wwnp7jnxn")))

(define-public crate-trie-standardmap-0.15.0 (c (n "trie-standardmap") (v "0.15.0") (d (list (d (n "hash-db") (r "^0.15.0") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.15.0") (d #t) (k 0)))) (h "1cprpkg9pwizsngpv4fapbjx1si2pqj4ccwipi0dd104q19s3zb4")))

(define-public crate-trie-standardmap-0.15.2 (c (n "trie-standardmap") (v "0.15.2") (d (list (d (n "hash-db") (r "^0.15.2") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.15.2") (d #t) (k 0)))) (h "1j3469q1340kg2wp66lx6csy7xh921pi5qb8df7csa5b42jin5n3")))

(define-public crate-trie-standardmap-0.16.0 (c (n "trie-standardmap") (v "0.16.0") (d (list (d (n "hash-db") (r "^0.16.0") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.16.0") (d #t) (k 0)))) (h "05lma4w5i09hy0cadqn46hr1p6p3vfzky2z1gwyzirps6arsyjk8")))

