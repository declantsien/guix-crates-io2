(define-module (crates-io tr ie trie_rcv) #:use-module (crates-io))

(define-public crate-trie_rcv-1.0.0 (c (n "trie_rcv") (v "1.0.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)))) (h "0w0x4aj5m660d9y6asq1wksgbkv22gwf45n7pa19ygp89v0xqrxs")))

(define-public crate-trie_rcv-1.0.1 (c (n "trie_rcv") (v "1.0.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)))) (h "01brsfaghi5nvdmdqs9x2955mdkmvynaqw5k4c89img5pzfzi5ms")))

(define-public crate-trie_rcv-1.1.0 (c (n "trie_rcv") (v "1.1.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "09mw70z1fvhyf6k49iqqab97dnb8psc8vv0v3ccsp5kc1dmjsw46")))

(define-public crate-trie_rcv-1.1.1 (c (n "trie_rcv") (v "1.1.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "1qmjx65a5yzz9gxl7nva32fffw0g4mv3s1flq827w7r5b6dvgjbv")))

(define-public crate-trie_rcv-1.1.2 (c (n "trie_rcv") (v "1.1.2") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "0ka1pxgg9jkiy425h0b5vzxa9xpky623bd46602wq6dcmswj362c")))

(define-public crate-trie_rcv-1.1.3 (c (n "trie_rcv") (v "1.1.3") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "10wi4mcv34wklfsjj6gikb8avhbv5ika8h1sw54cmk6d729m3sqs")))

(define-public crate-trie_rcv-1.1.4 (c (n "trie_rcv") (v "1.1.4") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "04sga5y9ggrqjklz4caxdqx072glj3wgwagjdqvv0y7crj71qnvv")))

(define-public crate-trie_rcv-1.2.0 (c (n "trie_rcv") (v "1.2.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "06w6dlk0f38q9qimqifq0sbhchqfa119aszw9zr817wpg81rm44s")))

