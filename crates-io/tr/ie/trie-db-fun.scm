(define-module (crates-io tr ie trie-db-fun) #:use-module (crates-io))

(define-public crate-trie-db-fun-0.28.0 (c (n "trie-db-fun") (v "0.28.0") (d (list (d (n "hash-db") (r "^0.16.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.1.0") (o #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (f (quote ("union" "const_new"))) (d #t) (k 0)))) (h "0jxn1caimj7y5vsnsx5ldcfmxqrlkfns8g1i39wm8ypjzrfphak5") (f (quote (("std" "hash-db/std" "rustc-hex") ("default" "std"))))))

(define-public crate-trie-db-fun-100.28.0 (c (n "trie-db-fun") (v "100.28.0") (d (list (d (n "hash-db") (r "^0.16.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.1.0") (o #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (f (quote ("union" "const_new"))) (d #t) (k 0)))) (h "16ccxnyv0034p7byzrbilq3fllajl8drm774dqfydb6xlpp87q7h") (f (quote (("std" "hash-db/std" "rustc-hex") ("default" "std"))))))

