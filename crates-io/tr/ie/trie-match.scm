(define-module (crates-io tr ie trie-match) #:use-module (crates-io))

(define-public crate-trie-match-0.1.0 (c (n "trie-match") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02cf6jy6329yjvykbshv5mf74770kl4ly8diqghbavpqbyw9h8if") (r "1.60")))

(define-public crate-trie-match-0.1.1 (c (n "trie-match") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sgdz7sl9pqacbfj1wqjfq9h1cb5jap1nk40m49b14409asqksrl") (r "1.60")))

(define-public crate-trie-match-0.1.2 (c (n "trie-match") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qyp754nfl2l5qkr3lm0phwk10pz09q84qplcgjg4wnwggz09gbx") (f (quote (("cfg_attribute")))) (r "1.65")))

(define-public crate-trie-match-0.2.0 (c (n "trie-match") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15czb2nxb3lhmvw9jj0541wjx6dx3b730wl2n623qgl6i4966g6j") (f (quote (("cfg_attribute")))) (r "1.65")))

