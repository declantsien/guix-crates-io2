(define-module (crates-io tr ie trie_lib) #:use-module (crates-io))

(define-public crate-trie_lib-0.1.0 (c (n "trie_lib") (v "0.1.0") (h "1cpzc4vjbhzbyzy2b2jlcl8z8hx0jn34j73xnm5wjxw151r7dwdf") (y #t)))

(define-public crate-trie_lib-0.1.1 (c (n "trie_lib") (v "0.1.1") (h "0337xfg7bqqzlw56bflkr9nbq0zykwaljirxvivarhh9sy22pfgw") (y #t)))

(define-public crate-trie_lib-0.2.0 (c (n "trie_lib") (v "0.2.0") (h "1h3xgbkcwkr28jlhgaacz8vdkjvbgra7is5svmkxaydlmjqbdyf2") (y #t)))

