(define-module (crates-io tr ie trie) #:use-module (crates-io))

(define-public crate-trie-0.0.1 (c (n "trie") (v "0.0.1") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "0g547sxvd1f9mf8kax66181j5kd29lmc71kj1nnsvkvxrgz8g860")))

(define-public crate-trie-0.0.2 (c (n "trie") (v "0.0.2") (d (list (d (n "ordered_iter") (r "*") (o #t) (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0ji2p4m7qix3plpkss2wzvgjll99cv1qz0w1mgf0y89hrcdjyy3m") (f (quote (("default" "ordered_iter"))))))

(define-public crate-trie-0.0.3 (c (n "trie") (v "0.0.3") (d (list (d (n "ordered_iter") (r "*") (o #t) (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1a0xahjsih0xwm7rd65rqmwxahw8dsh9mjzy6ggyng6qiv3yimyk") (f (quote (("default" "ordered_iter"))))))

(define-public crate-trie-0.1.0 (c (n "trie") (v "0.1.0") (d (list (d (n "ordered_iter") (r "*") (o #t) (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "11qdmw9hxbiafdkl4cb8a6z274b52gpj6v34kjyra06xrgd7v1vs") (f (quote (("default" "ordered_iter"))))))

(define-public crate-trie-0.2.0 (c (n "trie") (v "0.2.0") (d (list (d (n "ordered_iter") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0sm744qdqv9chci6bnlr9n0lgib35i6038ljlgrbr07wz5q62rzg") (f (quote (("default" "ordered_iter"))))))

(define-public crate-trie-0.2.1 (c (n "trie") (v "0.2.1") (d (list (d (n "ordered_iter") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "108x7lk93j3631m5z42zgx46a7sg0gyawvpjplws2r7zk13i5lw9") (f (quote (("default" "ordered_iter"))))))

