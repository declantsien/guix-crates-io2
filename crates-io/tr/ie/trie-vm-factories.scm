(define-module (crates-io tr ie trie-vm-factories) #:use-module (crates-io))

(define-public crate-trie-vm-factories-0.1.0 (c (n "trie-vm-factories") (v "0.1.0") (d (list (d (n "account-db") (r "^0.1.0") (d #t) (k 0)) (d (n "tetsy-keccak-hasher") (r "^0.1.1") (d #t) (k 0)) (d (n "tetsy-trie-db") (r "^0.19.2") (d #t) (k 0)) (d (n "tetsy-vm") (r "^0.1.0") (d #t) (k 0)) (d (n "vapcore-wasm") (r "^0.1.0") (d #t) (k 0)) (d (n "vaptrie") (r "^0.1.1") (d #t) (k 0) (p "patricia-trie-vapory")) (d (n "vvm") (r "^0.1.0") (d #t) (k 0)))) (h "1b42pc9np9dca4jppmbn6q8nmp1cx9kwi6izj07mw3gcfkmdnmph")))

