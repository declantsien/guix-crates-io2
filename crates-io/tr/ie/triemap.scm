(define-module (crates-io tr ie triemap) #:use-module (crates-io))

(define-public crate-triemap-0.1.0 (c (n "triemap") (v "0.1.0") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "05kwada7c1drzaxygg5wk91qp48cmbj9ylkzh32m7jnwqgdzbm0n")))

