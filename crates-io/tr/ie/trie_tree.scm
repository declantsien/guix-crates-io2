(define-module (crates-io tr ie trie_tree) #:use-module (crates-io))

(define-public crate-trie_tree-0.1.0 (c (n "trie_tree") (v "0.1.0") (h "1vl4krvp9ga85d7q8qc57ywmlz12nasbdggp0lphvdl2c6k0wd30")))

(define-public crate-trie_tree-0.1.1 (c (n "trie_tree") (v "0.1.1") (h "15ri5088yi8x5c9q9lcwn7s4vljsi5hvrd10lyb3w1b7d7i5fygg")))

(define-public crate-trie_tree-0.1.2 (c (n "trie_tree") (v "0.1.2") (h "0a8pbf828wadr1vf90kjjqyr552h20dinx4gn7ngyyjxglp5lrc1")))

(define-public crate-trie_tree-0.1.3 (c (n "trie_tree") (v "0.1.3") (h "19l9clp0yypcnn5fj5083s62qi1l3ksipnp4f7idsb600f82aim6")))

