(define-module (crates-io tr ie trie-root) #:use-module (crates-io))

(define-public crate-trie-root-0.9.0 (c (n "trie-root") (v "0.9.0") (d (list (d (n "hash-db") (r "^0.9.0") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak-hasher") (r "^0.2.0") (d #t) (k 2)) (d (n "trie-standardmap") (r "^0.9.0") (d #t) (k 2)))) (h "011gn6svh9xi3alh70v46cqgxnrd6mivdfzbnzqhaqzx4nlhhv2x") (f (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-trie-root-0.9.1 (c (n "trie-root") (v "0.9.1") (d (list (d (n "hash-db") (r "^0.9.0") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak-hasher") (r "^0.2.1") (d #t) (k 2)) (d (n "trie-standardmap") (r "^0.9.1") (d #t) (k 2)))) (h "06ygpmn1wg7205hbxs7hyzb1v4f4a1ssbcwdm3fx52qx8c9w7532") (f (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-trie-root-0.11.0 (c (n "trie-root") (v "0.11.0") (d (list (d (n "hash-db") (r "^0.11.0") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak-hasher") (r "^0.11.0") (d #t) (k 2)) (d (n "trie-standardmap") (r "^0.11.0") (d #t) (k 2)))) (h "0hz54nw8vdq4gp7s20b24524lfchc0l24zkaqj72bwssf3rgxip3") (f (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-trie-root-0.12.0 (c (n "trie-root") (v "0.12.0") (d (list (d (n "hash-db") (r "^0.12.0") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak-hasher") (r "^0.12.0") (d #t) (k 2)) (d (n "reference-trie") (r "^0.12.0") (d #t) (k 2)) (d (n "trie-standardmap") (r "^0.12.0") (d #t) (k 2)))) (h "0c0lg12lnqd4cxj0hh6diksncklvcp6q70r0nm04m2rbmzn34mp4") (f (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-trie-root-0.12.2 (c (n "trie-root") (v "0.12.2") (d (list (d (n "hash-db") (r "^0.12.2") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak-hasher") (r "^0.12.2") (d #t) (k 2)) (d (n "trie-standardmap") (r "^0.12.2") (d #t) (k 2)))) (h "1n8viggb5wfvzw4n4572490f6mhvbviiii6xf4paq60l9w6f58ng") (f (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-trie-root-0.12.3 (c (n "trie-root") (v "0.12.3") (d (list (d (n "hash-db") (r "^0.12.3") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak-hasher") (r "^0.12.3") (d #t) (k 2)) (d (n "trie-standardmap") (r "^0.12.3") (d #t) (k 2)))) (h "1rjcp341vsn442kc8lxmabjcbs6l3kpzcf1lw9pb1fgg1ld37dss") (f (quote (("std" "hash-db/std") ("default" "std")))) (y #t)))

(define-public crate-trie-root-0.12.4 (c (n "trie-root") (v "0.12.4") (d (list (d (n "hash-db") (r "^0.12.4") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak-hasher") (r "^0.12.4") (d #t) (k 2)) (d (n "trie-standardmap") (r "^0.12.4") (d #t) (k 2)))) (h "07agxlx4h7lg4lyfdllby2cmyjqadljpr7q0r7d6z2qla7c5sp28") (f (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-trie-root-0.14.0 (c (n "trie-root") (v "0.14.0") (d (list (d (n "hash-db") (r "^0.14.0") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak-hasher") (r "^0.14.0") (d #t) (k 2)) (d (n "trie-standardmap") (r "^0.14.0") (d #t) (k 2)))) (h "1x04zgqxn3ajwzfmjh38gkg36f7nbhw3gwg7zxr139y6gcp4nvr2") (f (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-trie-root-0.15.0 (c (n "trie-root") (v "0.15.0") (d (list (d (n "hash-db") (r "^0.15.0") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak-hasher") (r "^0.15.0") (d #t) (k 2)) (d (n "trie-standardmap") (r "^0.15.0") (d #t) (k 2)))) (h "0rvqrkxfq64bj6vp344y4kx8rc9wsdz83m76i5n6h3g5cjm0w6y3") (f (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-trie-root-0.15.2 (c (n "trie-root") (v "0.15.2") (d (list (d (n "hash-db") (r "^0.15.2") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak-hasher") (r "^0.15.2") (d #t) (k 2)) (d (n "trie-standardmap") (r "^0.15.2") (d #t) (k 2)))) (h "02swkiw0ahwp9kvb6yqi8pssrpjs9dfbxmfrcmijgscg3iy9yxqb") (f (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-trie-root-0.16.0 (c (n "trie-root") (v "0.16.0") (d (list (d (n "hash-db") (r "^0.15.2") (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "keccak-hasher") (r "^0.15.2") (d #t) (k 2)) (d (n "trie-standardmap") (r "^0.15.2") (d #t) (k 2)))) (h "1kb8kylp5046rpkh8zxkc50pp2kiasgvjw3s47bl84icdm832ab5") (f (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-trie-root-0.17.0 (c (n "trie-root") (v "0.17.0") (d (list (d (n "hash-db") (r "^0.15.2") (k 0)))) (h "149qg26yd4jzw9kyszb3zj9ln1j2j1knrvhnajd3rv8i775cadls") (f (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-trie-root-0.18.0 (c (n "trie-root") (v "0.18.0") (d (list (d (n "hash-db") (r "^0.16.0") (k 0)))) (h "02vcs14s98nbsh7y15v56zajs8xrdj8fs03r8vxgb65byl733vfl") (f (quote (("std" "hash-db/std") ("default" "std"))))))

