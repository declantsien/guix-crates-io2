(define-module (crates-io tr ie triehash-vapory) #:use-module (crates-io))

(define-public crate-triehash-vapory-0.2.0 (c (n "triehash-vapory") (v "0.2.0") (d (list (d (n "tetsy-keccak-hasher") (r "^0.1.1") (d #t) (k 0)) (d (n "tetsy-triehash") (r "^0.8.5") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "1xkvm77jwwq2bdk48ijbaq8xg9nrp97xcnclwq3alpvsrci7c7gz")))

