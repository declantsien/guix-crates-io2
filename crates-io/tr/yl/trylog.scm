(define-module (crates-io tr yl trylog) #:use-module (crates-io))

(define-public crate-trylog-0.1.0 (c (n "trylog") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0p81m90i9s5mbbm7ihsbzlabpfwspwjgcxfd3q45sn056kf00ywv")))

(define-public crate-trylog-0.2.0 (c (n "trylog") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0any7zbbqxrj1w1yfkn42c8nl266w3zhskl8rbx1l8c4fwdvm4zn")))

(define-public crate-trylog-0.3.0 (c (n "trylog") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (d #t) (k 2)))) (h "12mq3dqhp5qx5g3nrji56kmi4gn6gj2scdh1n8kdn0k1qlm3p336")))

(define-public crate-trylog-0.3.1 (c (n "trylog") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (d #t) (k 2)))) (h "1cf7ba032vb0c96va7dqsc3msgmgpfhzxnik48y2zgin3d7mag2w")))

