(define-module (crates-io tr eb treblecross) #:use-module (crates-io))

(define-public crate-treblecross-0.1.0 (c (n "treblecross") (v "0.1.0") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)) (d (n "lib_treblecross") (r "^0.1.0") (d #t) (k 0)))) (h "059s5v385c3xw0vfj5qj9mdl0bq5vw1cw7i3kwn98i8zpm9j6b32")))

(define-public crate-treblecross-0.2.0 (c (n "treblecross") (v "0.2.0") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "lib_treblecross") (r "^0.2.0") (d #t) (k 0)))) (h "0dhg2ir7bvayfslk5cpz8jhrijsllq3mrl81h1f6zwvycnp85jwp")))

