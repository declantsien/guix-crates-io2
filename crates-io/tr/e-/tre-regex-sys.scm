(define-module (crates-io tr e- tre-regex-sys) #:use-module (crates-io))

(define-public crate-tre-regex-sys-0.1.0 (c (n "tre-regex-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)))) (h "1yzksg916rw0f4i65nly9jc3mhzy91rf4cf3jxgvimlwcnwxqsac") (y #t) (r "1.60.0")))

(define-public crate-tre-regex-sys-0.1.1 (c (n "tre-regex-sys") (v "0.1.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)))) (h "0in1ycxw6m4pxpzk9fyn4pf7x2rgx02xjhg639p7jikr55s91d29") (y #t) (r "1.60.0")))

(define-public crate-tre-regex-sys-0.2.0 (c (n "tre-regex-sys") (v "0.2.0") (d (list (d (n "autotools") (r "^0.2") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1q7yr6irvdpbkma0b8cwzz6ykcpql1v4wrzidslrb5zd75jk2xab") (f (quote (("wchar") ("vendored" "autotools" "fs_extra") ("default" "approx" "vendored") ("approx")))) (y #t) (r "1.60.0")))

(define-public crate-tre-regex-sys-0.2.1 (c (n "tre-regex-sys") (v "0.2.1") (d (list (d (n "autotools") (r "^0.2") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "162mwn8kyvyydqhnpd29nvjcawji7dqssssa2xv7qprhd98qyv8g") (f (quote (("wchar") ("vendored" "autotools" "fs_extra") ("default" "approx" "vendored") ("approx")))) (r "1.60.0")))

(define-public crate-tre-regex-sys-0.2.2 (c (n "tre-regex-sys") (v "0.2.2") (d (list (d (n "autotools") (r "^0.2") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1s8pzgb8s5k066p02afc3n2kjdrxjv7j1x2vvlkcdsrzvq9357ir") (f (quote (("wchar") ("vendored" "autotools" "fs_extra") ("default" "approx" "vendored") ("approx")))) (r "1.60.0")))

(define-public crate-tre-regex-sys-0.3.0 (c (n "tre-regex-sys") (v "0.3.0") (d (list (d (n "autotools") (r "^0.2") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "12k71hqlf6jdinbj5xpnndx1p9dwbqxiijz7i3hf7g3q01hk4nwi") (f (quote (("wchar") ("vendored" "autotools" "fs_extra") ("default" "approx" "vendored") ("approx")))) (r "1.60.0")))

(define-public crate-tre-regex-sys-0.3.1 (c (n "tre-regex-sys") (v "0.3.1") (d (list (d (n "autotools") (r "^0.2") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0kzvmgcq8nhivxpydf6jyzc0k3sfjxn4pf2amqh67zshc8c02zha") (f (quote (("wchar") ("vendored" "autotools" "fs_extra") ("default" "approx" "vendored") ("approx")))) (r "1.60.0")))

(define-public crate-tre-regex-sys-0.4.0 (c (n "tre-regex-sys") (v "0.4.0") (d (list (d (n "autotools") (r "^0.2") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "0qjmnrqcmkh91m3c2mgpnrspz38saxdxb2kw2dy6g4q75cw4w1lc") (f (quote (("wchar") ("vendored" "autotools" "fs_extra") ("default" "approx" "vendored") ("approx")))) (r "1.70.0")))

