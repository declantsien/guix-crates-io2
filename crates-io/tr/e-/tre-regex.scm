(define-module (crates-io tr e- tre-regex) #:use-module (crates-io))

(define-public crate-tre-regex-0.1.0 (c (n "tre-regex") (v "0.1.0") (d (list (d (n "tre-regex-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1y73d1zfzvj2zmg2ia48bzyh50fk5xkkbl4bg663nlfqxb46fqb7") (f (quote (("wchar" "tre-regex-sys/wchar") ("vendored" "tre-regex-sys/vendored") ("default" "vendored" "approx") ("approx" "tre-regex-sys/approx")))) (y #t) (r "1.65.0")))

(define-public crate-tre-regex-0.1.1 (c (n "tre-regex") (v "0.1.1") (d (list (d (n "tre-regex-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1pvarahpmwlg82mh0x0nj0dmw409n08s7ncihgca4fvg5f03ijg6") (f (quote (("wchar" "tre-regex-sys/wchar") ("vendored" "tre-regex-sys/vendored") ("default" "vendored" "approx") ("approx" "tre-regex-sys/approx")))) (r "1.65.0")))

(define-public crate-tre-regex-0.2.0 (c (n "tre-regex") (v "0.2.0") (d (list (d (n "tre-regex-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "04cp5ndwwjh9ma9z1aqyg1l0f0fblx05s114vyy7d0ygkn8p5x5m") (f (quote (("vendored" "tre-regex-sys/vendored") ("default" "vendored" "approx" "wchar") ("approx" "tre-regex-sys/approx")))) (s 2) (e (quote (("wchar" "tre-regex-sys/wchar" "dep:widestring")))) (r "1.65.0")))

(define-public crate-tre-regex-0.2.1 (c (n "tre-regex") (v "0.2.1") (d (list (d (n "tre-regex-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "030m90m7i7ssvr79fhla39hhz86la5r66x6fcljc22bxnzxy2w2c") (f (quote (("vendored" "tre-regex-sys/vendored") ("default" "vendored" "approx" "wchar") ("approx" "tre-regex-sys/approx")))) (s 2) (e (quote (("wchar" "tre-regex-sys/wchar" "dep:widestring")))) (r "1.65.0")))

(define-public crate-tre-regex-0.3.0 (c (n "tre-regex") (v "0.3.0") (d (list (d (n "tre-regex-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "0wn4kpcgz8nl141mn8r1fv9c9czl8aac6zr69gqnz738r9k9nciw") (f (quote (("vendored" "tre-regex-sys/vendored") ("default" "vendored" "approx" "wchar") ("approx" "tre-regex-sys/approx")))) (s 2) (e (quote (("wchar" "tre-regex-sys/wchar" "dep:widestring")))) (r "1.65.0")))

(define-public crate-tre-regex-0.4.0 (c (n "tre-regex") (v "0.4.0") (d (list (d (n "tre-regex-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "12vg03nrxhb4yvhm5yslg31x8wfl9bikbqy3rrzljz54xjzw1jdg") (f (quote (("vendored" "tre-regex-sys/vendored") ("default" "vendored" "approx" "wchar") ("approx" "tre-regex-sys/approx")))) (s 2) (e (quote (("wchar" "tre-regex-sys/wchar" "dep:widestring")))) (r "1.70.0")))

(define-public crate-tre-regex-0.4.1 (c (n "tre-regex") (v "0.4.1") (d (list (d (n "tre-regex-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "0m0yajmr8w934fbvjh0jnkfm0mmyn9krqgxph0qq7q4mf53xbfhk") (f (quote (("vendored" "tre-regex-sys/vendored") ("default" "vendored" "approx" "wchar") ("approx" "tre-regex-sys/approx")))) (s 2) (e (quote (("wchar" "tre-regex-sys/wchar" "dep:widestring")))) (r "1.70.0")))

