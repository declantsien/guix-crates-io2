(define-module (crates-io tr ir trireme) #:use-module (crates-io))

(define-public crate-trireme-0.1.1 (c (n "trireme") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "naumachia") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wk6j39f00w4vldyl603d589zbxzvhz33xhsjawsn7jynzj1vd72")))

(define-public crate-trireme-0.1.2 (c (n "trireme") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "naumachia") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vr6nl3kdkn9vl62dicgy3c1jhmbsda5b66dw7qcfc5daxn90pfr")))

(define-public crate-trireme-0.1.3 (c (n "trireme") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "naumachia") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "05v889r13jcgzpw8xh32gyymmrcqg9x7hh1mh3ahw11vnc2ylgnv")))

(define-public crate-trireme-0.1.4 (c (n "trireme") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "naumachia") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qyzm9kj7mpc4yyz16g0f1w2sqfsl5n8z3a8hgkm1pgg1aaq80rx")))

(define-public crate-trireme-0.1.6 (c (n "trireme") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "naumachia") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "08wsm98a0f1q5f1l3xvn3jkllvkb8wgfiszk7dbzl3qyjpag629h")))

