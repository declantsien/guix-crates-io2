(define-module (crates-io tr y- try-catch) #:use-module (crates-io))

(define-public crate-try-catch-0.1.0 (c (n "try-catch") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "14bdypx5rbwl5a26plnavdwffakfw4scjfzi200sk8vn0bkmn99i")))

(define-public crate-try-catch-0.2.0 (c (n "try-catch") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1ppg28hi887f64lm7i8bllpbljlbp7jwcxz9pgjf0fs0lr5xpmq6")))

(define-public crate-try-catch-0.2.1 (c (n "try-catch") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0f6i02cfmn1r3p3slniv3gpb7slxpj20ij55mdjyqv2nwdcrplk1")))

(define-public crate-try-catch-0.2.2 (c (n "try-catch") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0c26wn1rq0rxnr1dnibd2dvwaxc3gglrpg8d72svlz8526shgb7q")))

