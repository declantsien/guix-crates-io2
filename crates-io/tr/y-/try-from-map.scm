(define-module (crates-io tr y- try-from-map) #:use-module (crates-io))

(define-public crate-try-from-map-0.1.0 (c (n "try-from-map") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0a95vni34jjh4asdcscdia7b1zcwhyh7wrx8gvy4xjkp77hmqa8x")))

(define-public crate-try-from-map-0.1.1 (c (n "try-from-map") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1nz32685jmi2a4811yd9l6ww4qm7k45ya6747sppl7vd962l3jcm")))

(define-public crate-try-from-map-0.1.2 (c (n "try-from-map") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "15zf11rnbl3l5hcc6myfnkh78906vjvj4x2qiw0yp63s7nnimsiy")))

(define-public crate-try-from-map-0.2.0 (c (n "try-from-map") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "00d4wcbc8ywjs339af5wsrwmv4dy1gcpl8gr4by7ax8rw8l03sr9")))

(define-public crate-try-from-map-0.2.1 (c (n "try-from-map") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1sij9nkrfgf0zf0ha2jvjms1j7ykq6ykgg7lcxqbkmni1dl2r7cy")))

