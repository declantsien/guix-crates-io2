(define-module (crates-io tr y- try-traits) #:use-module (crates-io))

(define-public crate-try-traits-0.1.0 (c (n "try-traits") (v "0.1.0") (h "08y2mcsha5pa45g6jjrn2j14fddgnyqfv19yn2cddxzbcxn9y5kq") (f (quote (("nightly_clamp" "nightly") ("nightly"))))))

(define-public crate-try-traits-0.1.1 (c (n "try-traits") (v "0.1.1") (h "1y618l34wclwj8z5x7lki04cqm54znfbskn3psmxl9rrnpby6wwk") (f (quote (("nightly_clamp" "nightly") ("nightly"))))))

