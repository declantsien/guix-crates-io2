(define-module (crates-io tr y- try-ascii) #:use-module (crates-io))

(define-public crate-try-ascii-1.0.0 (c (n "try-ascii") (v "1.0.0") (h "1rm6j1l1jrfj934rmjzz31avhjdfydyacb4ic58b510pz0y676dj")))

(define-public crate-try-ascii-1.1.0 (c (n "try-ascii") (v "1.1.0") (h "0xl53n97n34h23njs33k5p76ccv7z62krj0gybjx20wc93rh251c")))

