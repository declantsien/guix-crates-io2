(define-module (crates-io tr y- try-unwrap) #:use-module (crates-io))

(define-public crate-try-unwrap-0.1.0 (c (n "try-unwrap") (v "0.1.0") (h "1f62kfpnfw0h8ciwn3wv2h872bh1c6dybz427s21ji4rp5f59vf2")))

(define-public crate-try-unwrap-1.0.0 (c (n "try-unwrap") (v "1.0.0") (h "0nj8w2ddiq8l6a2r45zjnwqw11qdc1j17ij33326mzcakq0dwq1w")))

