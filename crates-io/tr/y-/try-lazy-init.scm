(define-module (crates-io tr y- try-lazy-init) #:use-module (crates-io))

(define-public crate-try-lazy-init-0.0.1 (c (n "try-lazy-init") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.26") (d #t) (k 2)))) (h "13mqsnkhl7cv6d3ijl1bz2kjwhw076kynq32nmlvhihqwi2bcxdz")))

(define-public crate-try-lazy-init-0.0.2 (c (n "try-lazy-init") (v "0.0.2") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.26") (d #t) (k 2)))) (h "0i8pn4g1xcf7l5k16p3f6a94q3sb7rbvngivsyjdikxkfr4rbnn8")))

