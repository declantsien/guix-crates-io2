(define-module (crates-io tr y- try-to-release-me) #:use-module (crates-io))

(define-public crate-try-to-release-me-0.1.5 (c (n "try-to-release-me") (v "0.1.5") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("error" "display"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 2)))) (h "106921f7ifa3rcbd1l9qyd6k6718pxvy21nlxcpaxnk7id3gjhji") (f (quote (("integration-tests") ("default"))))))

