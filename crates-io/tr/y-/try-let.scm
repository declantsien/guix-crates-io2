(define-module (crates-io tr y- try-let) #:use-module (crates-io))

(define-public crate-try-let-0.1.0 (c (n "try-let") (v "0.1.0") (h "1pplk241bw62rnv17zkrdk579n6f9ihgw6n78yman5pjdi58h33l")))

(define-public crate-try-let-0.1.1 (c (n "try-let") (v "0.1.1") (h "0kfhci3ll2sbpzvd0393j7pppdv0phcdf2d1vw1a7sld8hjzcjn1")))

(define-public crate-try-let-0.1.2 (c (n "try-let") (v "0.1.2") (h "1wj24samrfjwm9hcd5chgwcixf9dfp4y5yi78rq4gq5cs8a7cjzb")))

(define-public crate-try-let-0.1.3 (c (n "try-let") (v "0.1.3") (h "0fybzh2piyshdj9wfm5fpc5sxm2pygwdlspgc00lj1biwyw4ln9l")))

(define-public crate-try-let-0.1.5 (c (n "try-let") (v "0.1.5") (h "01p4d5q4vjm3yhchi6b13iwy59f9l5iq0x67flqglcv13ziykvl8")))

(define-public crate-try-let-0.2.0 (c (n "try-let") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0vv16kkflzi0k97rha13pd48bs8q18ihhy191bg3z5dw4z73qp7s")))

