(define-module (crates-io tr y- try-partialord) #:use-module (crates-io))

(define-public crate-try-partialord-0.1.0 (c (n "try-partialord") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0d1pbaznkq0c3q3cpfrc650rdch20alfgj65zx83a3dbjgla78bw") (f (quote (("std") ("default" "std"))))))

(define-public crate-try-partialord-0.1.1 (c (n "try-partialord") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0cv19755wssaxici5sn3b1pkvkl4ca4h95p9r07473pn2xvg0x05") (f (quote (("std") ("default" "std"))))))

(define-public crate-try-partialord-0.1.2 (c (n "try-partialord") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "05s9akd0rc1qyf10jjnijqj7k7w97fnzp2f62ixvr23g3xw3knsx") (f (quote (("std") ("default" "std"))))))

(define-public crate-try-partialord-0.1.3 (c (n "try-partialord") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "03rrxjnrxp89xxi773vpnk753cmqj71vpsgc96dyniv48jpjnks9") (f (quote (("std") ("default" "std"))))))

