(define-module (crates-io tr y- try-from-discrim) #:use-module (crates-io))

(define-public crate-try-from-discrim-1.0.0 (c (n "try-from-discrim") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (d #t) (k 0)))) (h "0i29vq8r47np8xfrwmqrd5b48bcb6maf3pn6l217i3s158pphj3l")))

