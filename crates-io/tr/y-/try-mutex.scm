(define-module (crates-io tr y- try-mutex) #:use-module (crates-io))

(define-public crate-try-mutex-0.1.0 (c (n "try-mutex") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.3.11") (f (quote ("stable"))) (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "1fj9c0qc7ibymxkzdl7dngb80c10av15j07x2yilkabrv6k7i3wq") (y #t)))

(define-public crate-try-mutex-0.1.1 (c (n "try-mutex") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "^0.3.11") (f (quote ("stable"))) (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "1xwiy4k992hgckxgd5xahpddnikzwiwmq8g01dnlwmqani6l6vry") (y #t)))

(define-public crate-try-mutex-0.2.0 (c (n "try-mutex") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1mh3nld6z0rn1gq3bqkayliy4rkyaxdfinncgvq80smg78nk6fdv") (y #t)))

(define-public crate-try-mutex-0.3.0 (c (n "try-mutex") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0k3ccvwbr85q4bvb99d5d2fzhc9m7y0dgcxwqkdd2brb84pslhxs")))

(define-public crate-try-mutex-0.4.0 (c (n "try-mutex") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0fy8dy2qnf2j6jiv3rv2zlf5pi0fhfhk20x9fimmdkgpqr7cvyqy")))

(define-public crate-try-mutex-0.4.1 (c (n "try-mutex") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1i7sljpqa54m8mcwqyf6ip84jkg08n38j4a0c6p3yyi1xi9n88qw")))

(define-public crate-try-mutex-0.4.2 (c (n "try-mutex") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "08bp1a5kbziaaz7i365h3r7l104pvjmgf02izz38qgmb7c80ivkm")))

