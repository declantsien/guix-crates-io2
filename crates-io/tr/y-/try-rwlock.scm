(define-module (crates-io tr y- try-rwlock) #:use-module (crates-io))

(define-public crate-try-rwlock-0.1.0 (c (n "try-rwlock") (v "0.1.0") (h "0phkavkh0gdrwfwas729vfwvf8kr67jbq44vzp1zyc56v4s350zv")))

(define-public crate-try-rwlock-0.1.1 (c (n "try-rwlock") (v "0.1.1") (h "1r5905l1nhmd9gxzxnlllzrndvvynxmvm510vby065pw3my2jfha")))

(define-public crate-try-rwlock-0.1.2 (c (n "try-rwlock") (v "0.1.2") (h "0njxsrvsx2s9lwhr8mindynjpan49b7fqp42ffwq9ms3bd9k7fzb")))

