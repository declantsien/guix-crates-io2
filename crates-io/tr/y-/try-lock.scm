(define-module (crates-io tr y- try-lock) #:use-module (crates-io))

(define-public crate-try-lock-0.1.0 (c (n "try-lock") (v "0.1.0") (h "1hp76pyzyxhcxxjacf083gpp6gf8cqwkg188yy02i2a3axqs8apf")))

(define-public crate-try-lock-0.2.0 (c (n "try-lock") (v "0.2.0") (h "1djzwr2wd11r0a9mwf6jqnk38c2isjb2y5mdgad0ihxbrkcg4c9a")))

(define-public crate-try-lock-0.2.1 (c (n "try-lock") (v "0.2.1") (h "0ia51p76hhf80jmxx895gjh4ji8n21rn2rdyc39p5rzv2wm576qi")))

(define-public crate-try-lock-0.2.2 (c (n "try-lock") (v "0.2.2") (h "10p36rx6pqi9d0zr876xa8vksx2m66ha45myakl50rn08dxyn176")))

(define-public crate-try-lock-0.2.3 (c (n "try-lock") (v "0.2.3") (h "0hkn1ksmg5hdqgqdw1ahy5qk69f4crh2psf0v61qphyrf777nm2r")))

(define-public crate-try-lock-0.2.4 (c (n "try-lock") (v "0.2.4") (h "1vc15paa4zi06ixsxihwbvfn24d708nsyg1ncgqwcrn42byyqa1m")))

(define-public crate-try-lock-0.2.5 (c (n "try-lock") (v "0.2.5") (h "0jqijrrvm1pyq34zn1jmy2vihd4jcrjlvsh4alkjahhssjnsn8g4")))

