(define-module (crates-io tr y- try-guard) #:use-module (crates-io))

(define-public crate-try-guard-0.1.0 (c (n "try-guard") (v "0.1.0") (h "0qf3xyd04mjpq4g5vz9c3hsp0k8y7b1l652vcky4nc7ljnal0a0i") (f (quote (("try-trait") ("default" "try-trait"))))))

(define-public crate-try-guard-0.1.1 (c (n "try-guard") (v "0.1.1") (h "0ab9m8cz190mk2pd5nav7gwy2hp556z8ll9pavs1ji8j0wilck8n") (f (quote (("try-trait") ("default" "try-trait"))))))

(define-public crate-try-guard-0.2.0 (c (n "try-guard") (v "0.2.0") (h "1yks5bhy9npi7v1jjx9gk65npj2qfj3ghk3j9z8jb4x8mam0j3ll") (f (quote (("test-nightly") ("default"))))))

