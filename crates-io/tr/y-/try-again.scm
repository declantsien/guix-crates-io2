(define-module (crates-io tr y- try-again) #:use-module (crates-io))

(define-public crate-try-again-0.1.0 (c (n "try-again") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "088li2anm2g80jsnkc0iazalpl4fyjcsc6awxjnaf3l0f3aq8ll7") (f (quote (("sync") ("default" "sync" "async" "async-tokio") ("async")))) (s 2) (e (quote (("async-tokio" "async" "dep:tokio")))) (r "1.56")))

(define-public crate-try-again-0.1.1 (c (n "try-again") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0w86q1d19xl020riwywg9mncv60bwhv1609xgbnakamddvbcy1hh") (f (quote (("sync") ("default" "sync" "async" "async-tokio") ("async")))) (s 2) (e (quote (("async-tokio" "async" "dep:tokio")))) (r "1.56")))

