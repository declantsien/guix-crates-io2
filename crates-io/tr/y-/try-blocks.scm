(define-module (crates-io tr y- try-blocks) #:use-module (crates-io))

(define-public crate-try-blocks-0.1.0 (c (n "try-blocks") (v "0.1.0") (h "1ml3akaanadyqyzbp93ryldv9jqmx0ilv6iry9xwjfaniv2a07cm") (y #t)))

(define-public crate-try-blocks-0.1.1 (c (n "try-blocks") (v "0.1.1") (h "0mbwp8n7wy9q63jlgmdkg58a616gp3d5n541543ll4xdciynqwhx") (y #t)))

(define-public crate-try-blocks-0.1.2 (c (n "try-blocks") (v "0.1.2") (h "0r2qgnjx829g4lm1r0qrr6avgb428chqyyfcab5c3bpbj2vapxbr") (y #t)))

(define-public crate-try-blocks-0.1.3 (c (n "try-blocks") (v "0.1.3") (h "0nyaljhgl9aw3c7aixsqyr724ky78x492fc3ms83a45gnpvi5f16") (y #t)))

(define-public crate-try-blocks-0.1.4 (c (n "try-blocks") (v "0.1.4") (h "0ymyfbw4whlwyvmamfxbqzm2147xsc9w281v24zfhnn05j4wfv19")))

