(define-module (crates-io tr ai trait-bound-pretty) #:use-module (crates-io))

(define-public crate-trait-bound-pretty-0.1.0 (c (n "trait-bound-pretty") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0.6.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.19") (d #t) (k 0)))) (h "0ch05ygz56rgv1hk1gb3dyikjwid289d90ivjgmgfscvmdq807qh")))

