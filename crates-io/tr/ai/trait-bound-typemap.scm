(define-module (crates-io tr ai trait-bound-typemap) #:use-module (crates-io))

(define-public crate-trait-bound-typemap-0.1.0 (c (n "trait-bound-typemap") (v "0.1.0") (d (list (d (n "multi-trait-object") (r "^0.2.0") (d #t) (k 0)))) (h "1hp1sxhhqcjn5y8n0jy477y0a3mkm3w04k44ymbal8x4l9zxm837")))

(define-public crate-trait-bound-typemap-0.2.0 (c (n "trait-bound-typemap") (v "0.2.0") (d (list (d (n "multi-trait-object") (r "^0.2.0") (d #t) (k 0)))) (h "03xy33waj203cw1arm8gfgnyp0b1rj1kmbrixwnminzrcgf4ffgx")))

(define-public crate-trait-bound-typemap-0.3.0 (c (n "trait-bound-typemap") (v "0.3.0") (d (list (d (n "multi-trait-object") (r "^0.2.0") (d #t) (k 0)))) (h "10z31dg6dk2xa5h140a0qc8s9641fvryph7zylakcb10s9gvlxcm")))

(define-public crate-trait-bound-typemap-0.3.1 (c (n "trait-bound-typemap") (v "0.3.1") (d (list (d (n "multi-trait-object") (r "^0.2.0") (d #t) (k 0)))) (h "0kfzlpazifs0g8sjbjlrfgyssmmb5094yrx4rcr7jlpw9zi4az02")))

(define-public crate-trait-bound-typemap-0.3.2 (c (n "trait-bound-typemap") (v "0.3.2") (d (list (d (n "multi-trait-object") (r "^0.2.0") (d #t) (k 0)))) (h "0qyimqrzhxdby76yjr5k9hzvvzwrbzan7wrkg9jvh5iy8cv45jix")))

(define-public crate-trait-bound-typemap-0.3.3 (c (n "trait-bound-typemap") (v "0.3.3") (d (list (d (n "multi-trait-object") (r "^0.2.0") (d #t) (k 0)))) (h "0ry7c3mw2yw5008yr3rg0jgj25bzrayg6z9kmaql23iwlxdxyc9n")))

