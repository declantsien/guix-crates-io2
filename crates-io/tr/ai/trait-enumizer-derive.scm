(define-module (crates-io tr ai trait-enumizer-derive) #:use-module (crates-io))

(define-public crate-trait-enumizer-derive-0.1.0 (c (n "trait-enumizer-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1kmw3b37zgq80v8np6bf8hidacvzi8963f8mpiy7393klqbn66ci")))

(define-public crate-trait-enumizer-derive-0.1.1 (c (n "trait-enumizer-derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "02da7lpff4m72rq3gxmmrp1p7z4dkkpnk21aknr59nr5h3d78j7c") (f (quote (("std") ("alloc"))))))

