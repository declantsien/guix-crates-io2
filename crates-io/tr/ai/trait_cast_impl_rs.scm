(define-module (crates-io tr ai trait_cast_impl_rs) #:use-module (crates-io))

(define-public crate-trait_cast_impl_rs-0.2.0 (c (n "trait_cast_impl_rs") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "venial") (r "^0.4.0") (d #t) (k 0)))) (h "02r4hcpxn58sz63dx7avr9xnvv6cf21r5m3lnm8b47m4ha57b3jb") (f (quote (("downcast_unchecked") ("default"))))))

(define-public crate-trait_cast_impl_rs-0.2.1 (c (n "trait_cast_impl_rs") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "venial") (r "^0.4.0") (d #t) (k 0)))) (h "0m0cq7b1ip5sqvaimwrm8acxh39wv604xzhkldy2wkb1b1hgyxvg")))

