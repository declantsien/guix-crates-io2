(define-module (crates-io tr ai traitlit) #:use-module (crates-io))

(define-public crate-traitlit-0.1.0 (c (n "traitlit") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1plkw9w05hmxbf2rb6a96gahg7402b90k67qnp32gn30xbryy3vr")))

(define-public crate-traitlit-0.2.0 (c (n "traitlit") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1i72r60dd27v5ndc423cjfkzvz68zrlyjd09drfq751m48p87xq7")))

(define-public crate-traitlit-0.2.1 (c (n "traitlit") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "06z9mwz3cw1q1k36h0p6kyjv7g184qzxiz7pc7ns23ychazr2y91")))

(define-public crate-traitlit-0.2.2 (c (n "traitlit") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "19vwj1wv1mv1n5frmmik1ji64prrfaq49j8spj8v9y7a4j2n6wz8")))

