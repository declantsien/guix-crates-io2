(define-module (crates-io tr ai traiter) #:use-module (crates-io))

(define-public crate-traiter-0.0.0 (c (n "traiter") (v "0.0.0") (h "07cxa3kfikh5bxrazvkq17xncgyxn7cbmh82qgg3jqjsp42fh2gl")))

(define-public crate-traiter-0.1.0 (c (n "traiter") (v "0.1.0") (h "0fjjlkx8ffc6cc87b0whac1497yba3yfpj33anwsdy2z9nhihw5n") (f (quote (("std") ("numbers") ("collections"))))))

(define-public crate-traiter-1.0.0 (c (n "traiter") (v "1.0.0") (h "12z2984ycfrhf5jk1xa3ps9nw0lcafg1mkiwpjy88lri2dlajjg3") (f (quote (("std") ("numbers") ("collections"))))))

(define-public crate-traiter-1.1.0 (c (n "traiter") (v "1.1.0") (h "0d04qv2gr4yw7rnjhbjjv1nw174iw01jhp14nnpd81b55kvs6k95") (f (quote (("std") ("numbers") ("collections"))))))

(define-public crate-traiter-2.0.0 (c (n "traiter") (v "2.0.0") (h "000mylf2xqd79aj802w7a785hgzm4sx6fb8wjsks0sf34q1qr3y7") (f (quote (("std") ("numbers") ("collections"))))))

(define-public crate-traiter-2.0.1 (c (n "traiter") (v "2.0.1") (h "05d31ykniyqghr0qhrgnhslavv6rbyzh711ww0r4yh7y5jsw4js6") (f (quote (("std") ("numbers") ("collections"))))))

(define-public crate-traiter-2.0.2 (c (n "traiter") (v "2.0.2") (h "1izmhx91nj1q374flvaz4nlzfz5m1jgj07389zkz7alhwh7wx7qh") (f (quote (("std") ("numbers") ("collections"))))))

(define-public crate-traiter-3.0.0 (c (n "traiter") (v "3.0.0") (h "0332bkk19f61d968jj66mvh0mmg3r56rzirsldx5s2bbg0648cag") (f (quote (("std") ("numbers") ("collections"))))))

(define-public crate-traiter-3.1.0 (c (n "traiter") (v "3.1.0") (h "1m5nhwsn7li3hi2hl48bzw76ml3l3gbci7h9wh3niglxk1241ldh") (f (quote (("std") ("numbers") ("collections"))))))

(define-public crate-traiter-4.0.0 (c (n "traiter") (v "4.0.0") (h "11cl0wgjvn344q9fq4wss8kcqpx5k79niqz69cqhbdjcnjkykpas") (f (quote (("std") ("numbers") ("collections"))))))

