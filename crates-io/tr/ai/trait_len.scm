(define-module (crates-io tr ai trait_len) #:use-module (crates-io))

(define-public crate-trait_len-0.2.3 (c (n "trait_len") (v "0.2.3") (d (list (d (n "pyo3") (r "^0.13.2") (f (quote ("extension-module" "abi3-py36"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (o #t) (d #t) (k 0)))) (h "0rpnyabywfxxpdsfq9dhprg4w5fs711qm92n9f80ah1d7z6418y9") (f (quote (("serde_crates" "serde_json" "serde") ("default"))))))

