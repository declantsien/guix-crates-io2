(define-module (crates-io tr ai trait_exerci) #:use-module (crates-io))

(define-public crate-trait_exerci-0.1.0 (c (n "trait_exerci") (v "0.1.0") (h "12628x77sgi16nkllskq5765rszrgcbppd6c7yk8g6v3zknibyzm")))

(define-public crate-trait_exerci-0.1.1 (c (n "trait_exerci") (v "0.1.1") (h "1bmlrd2kj3whmylp06kb34n0nbswhxl0y4481cd9jxp6bb7af4l5")))

(define-public crate-trait_exerci-0.1.2 (c (n "trait_exerci") (v "0.1.2") (h "09gx1jirijbag1pmiks7l03x237w2di9cbzjwr4piiwn380rw67y")))

(define-public crate-trait_exerci-0.2.0 (c (n "trait_exerci") (v "0.2.0") (h "186gvrg06yzvvhsyyz0s9mzk1asz6dsmawivbzd0m5dz0hynj3y9")))

(define-public crate-trait_exerci-0.2.1 (c (n "trait_exerci") (v "0.2.1") (h "0kzzbxh156lz980j5n4w7hajkmrz64vk4jj45pychqvbifyfr5vv")))

(define-public crate-trait_exerci-0.2.2 (c (n "trait_exerci") (v "0.2.2") (h "03yxi8kz47kcwj2lq70lcw6as5z827jb6bhl7x2pac54m80h2ci4")))

(define-public crate-trait_exerci-0.2.3 (c (n "trait_exerci") (v "0.2.3") (h "0a53c4fa277qfkvknmiylyacgmpri1fbi5cx2rg05hgiq6dhi3d2")))

(define-public crate-trait_exerci-0.2.4 (c (n "trait_exerci") (v "0.2.4") (h "09m7mhak73wx4qghhl9v6xiqhrk1wzhpy9k6k1ywsbf1r9wymi2m")))

(define-public crate-trait_exerci-0.2.5 (c (n "trait_exerci") (v "0.2.5") (h "09hcyvbz062iq0srnmhramn8frfaf5iij281my534m26whwvzzw7")))

(define-public crate-trait_exerci-0.2.6 (c (n "trait_exerci") (v "0.2.6") (h "1zvbrs7j52n2g97qsnx392clcs9ca15gc8vmxgwicf78ih8x1ynd")))

(define-public crate-trait_exerci-0.2.7 (c (n "trait_exerci") (v "0.2.7") (h "1q9cm59s7yw4i4kvn6n4y5b5a6lw3zjbcbx2yf3vb7ld07y052kg")))

(define-public crate-trait_exerci-0.2.8 (c (n "trait_exerci") (v "0.2.8") (h "0v64q7nk27xdq9j9a9zn4nkcarqldff6875axc0s8nwf12jvb3zq")))

(define-public crate-trait_exerci-0.2.9 (c (n "trait_exerci") (v "0.2.9") (h "1f5f95f6whhx9wy6i2nj7b9d4hzgrvgb5mdn4qhk3yklyhr8g3zq")))

(define-public crate-trait_exerci-0.3.0 (c (n "trait_exerci") (v "0.3.0") (h "1qhd1jy2y1r84ixqhywhdaffrapmii2dcx74jpqbk970xxfi08gl")))

