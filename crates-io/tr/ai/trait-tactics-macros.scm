(define-module (crates-io tr ai trait-tactics-macros) #:use-module (crates-io))

(define-public crate-trait-tactics-macros-0.1.0 (c (n "trait-tactics-macros") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "0565qfms2zwhj2m0m2a3yaq5jdcxlpm6iwia9fg2mj100qc933p3") (f (quote (("num-traits"))))))

