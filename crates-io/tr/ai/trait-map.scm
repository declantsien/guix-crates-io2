(define-module (crates-io tr ai trait-map) #:use-module (crates-io))

(define-public crate-trait-map-0.1.0 (c (n "trait-map") (v "0.1.0") (h "0wjv5f6scy02bsz43an9qari3cchb87258bhdaxgv1smgwj1x7m6")))

(define-public crate-trait-map-0.1.1 (c (n "trait-map") (v "0.1.1") (h "065agp07x291a2hjbx0yhjsjjw3flpdcqz59x9qvl73iby5xh2a9")))

(define-public crate-trait-map-0.2.0 (c (n "trait-map") (v "0.2.0") (h "1lwqwdvh3wnnswaq53yw5j41yqsq3y5f9wdnfkwa24140fpgfj0x")))

(define-public crate-trait-map-0.3.0 (c (n "trait-map") (v "0.3.0") (d (list (d (n "trait-map-derive") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "1my5f12m56w47rj254a60nqw3iscpawgn15dafpq1p9xhw70li6r") (f (quote (("derive" "trait-map-derive"))))))

(define-public crate-trait-map-0.3.1 (c (n "trait-map") (v "0.3.1") (d (list (d (n "trait-map-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0d4j2hmn4nfsinkx7x4rpsj3mfsqjjwiccnnr1afrqlbn4qdalpm") (f (quote (("derive" "trait-map-derive"))))))

