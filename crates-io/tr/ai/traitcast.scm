(define-module (crates-io tr ai traitcast) #:use-module (crates-io))

(define-public crate-traitcast-0.1.0 (c (n "traitcast") (v "0.1.0") (d (list (d (n "anymap") (r "0.12.*") (d #t) (k 0)) (d (n "inventory") (r "0.1.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "1r9f1pc1a60p550kxsnhqfy5xpy5kipx1zmqv9zwinx9nii47bqn")))

(define-public crate-traitcast-0.1.1 (c (n "traitcast") (v "0.1.1") (d (list (d (n "anymap") (r "0.12.*") (d #t) (k 0)) (d (n "inventory") (r "0.1.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "1c352qbqbyn3vyf707qdpa9vb2n160n71yhl9gcnhczzc5caq41j")))

(define-public crate-traitcast-0.1.2 (c (n "traitcast") (v "0.1.2") (d (list (d (n "anymap") (r "0.12.*") (d #t) (k 0)) (d (n "inventory") (r "0.1.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "060q1s7vf0n9x6a2zcq0132vi9k4dpw78ismn5ycjssaca7wskfr")))

(define-public crate-traitcast-0.1.3 (c (n "traitcast") (v "0.1.3") (d (list (d (n "anymap") (r "0.12.*") (d #t) (k 0)) (d (n "inventory") (r "0.1.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "1jrq2l851bcbymkqmgvwnxcmp5d9jb656g3mawzh1bb03ll0awwl")))

(define-public crate-traitcast-0.2.0 (c (n "traitcast") (v "0.2.0") (d (list (d (n "anymap") (r "0.12.*") (d #t) (k 0)) (d (n "inventory") (r "0.1.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "0gns34ah1zknx8n6lvygd1x7ihs2nxpxscs870rgszcnv47v0d4g")))

(define-public crate-traitcast-0.2.1 (c (n "traitcast") (v "0.2.1") (d (list (d (n "anymap") (r "0.12.*") (d #t) (k 0)) (d (n "inventory") (r "0.1.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)))) (h "0yihi20ppgcv6g4s1c40w9ri4s117r0zsx8g9sky8xncihg3wxnz")))

(define-public crate-traitcast-0.5.0 (c (n "traitcast") (v "0.5.0") (d (list (d (n "inventory") (r "0.1.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "traitcast_core") (r "0.2.*") (f (quote ("use_inventory"))) (d #t) (k 0)))) (h "0h47cn40my6ar922gfn8vqs9l6h35w7m6x9qngfg4psfd7g1q2zq")))

