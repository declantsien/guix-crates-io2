(define-module (crates-io tr ai trait-make) #:use-module (crates-io))

(define-public crate-trait-make-0.1.0 (c (n "trait-make") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "14nd81fjnlab9wrgks9h1ls6n2ax57nxjx8dwq1id3v4gdmd1jwn") (r "1.75")))

