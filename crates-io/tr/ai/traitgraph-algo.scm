(define-module (crates-io tr ai traitgraph-algo) #:use-module (crates-io))

(define-public crate-traitgraph-algo-0.6.0 (c (n "traitgraph-algo") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "traitgraph") (r "^0.6.0") (d #t) (k 0)))) (h "1v3yphr6nl2azq8jlpwykh5jya9h1yxby9yh7wqb4hlyw50k68ns") (r "1.49.0")))

(define-public crate-traitgraph-algo-0.6.1 (c (n "traitgraph-algo") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "traitgraph") (r "^0.6.0") (d #t) (k 0)))) (h "1a8mpwhhvvx5vmy58s5v93rd91qddkwhw5ys3alg8kp693c5498i") (r "1.49.0")))

(define-public crate-traitgraph-algo-0.6.2 (c (n "traitgraph-algo") (v "0.6.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "traitgraph") (r "^0.6.1") (d #t) (k 0)))) (h "02x7wfy9xi57d4dxbhl6aj2rcr43jf7lz5f42fr99sk9w6py9l5p") (r "1.49.0")))

(define-public crate-traitgraph-algo-0.7.0 (c (n "traitgraph-algo") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^0.7.0") (d #t) (k 0)))) (h "1cmdhp7kdhxqgmqhyzksmlsvz9myk8564zr4ckyxf1m5n3hlyz07") (r "1.49.0")))

(define-public crate-traitgraph-algo-0.7.1 (c (n "traitgraph-algo") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^0.7.0") (d #t) (k 0)))) (h "19rsz5gv8vk8bs9cjbpqq9x3yylg4863x80czb2dmdcrdfgbkzkh") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.49.0")))

(define-public crate-traitgraph-algo-0.8.0 (c (n "traitgraph-algo") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^0.7.0") (d #t) (k 0)))) (h "1jmbs9zbrk91r3f1pxxz8hbdxm272rwpx1r0z36dqw3a7l87v8c3") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.49.0")))

(define-public crate-traitgraph-algo-0.9.0 (c (n "traitgraph-algo") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^0.7.0") (d #t) (k 0)))) (h "1lpa1xn6vi26x11vmwgd0f5lg1kwshq0q6m4kjkyxrph0g2g01k4") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.58.1")))

(define-public crate-traitgraph-algo-0.10.0 (c (n "traitgraph-algo") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^0.10.0") (d #t) (k 0)))) (h "18qcchmkynz99a5j3wcgi90g9zhwzi22r8x7jcbrmj8lpldvh8cf") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-0.10.1 (c (n "traitgraph-algo") (v "0.10.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^0.10.0") (d #t) (k 0)))) (h "1sd3f5xgmcb21viwxs06d0cka7lzvgzga5zlhnvaryxln5avrmfw") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-1.0.0 (c (n "traitgraph-algo") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^1.0.0") (d #t) (k 0)))) (h "0b1j3cjr1snmbx0zxmaxy2wh8333lm9xrf0c7g5gx52k4xpzx2q3") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-2.0.0 (c (n "traitgraph-algo") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "13zx1kf9d4p6l29ji18g5qhf83fvzq9jcjr07rcp4a8328sxlkdv") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-3.0.0 (c (n "traitgraph-algo") (v "3.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "1zpyn9fk2vc21g9hqcksfsbb8xban8qbba292ns62i10rr839gwk") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-4.0.0 (c (n "traitgraph-algo") (v "4.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "1isqpsl151bdvh1p7kfmy106208i0xx67n5z1dsfrbbf34bj2byq") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-5.0.0 (c (n "traitgraph-algo") (v "5.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "180lfci4lgy2a7fq9k5ysqy9k6cvvvj3c7mv34kh9scwspc5cgnk") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-5.1.0 (c (n "traitgraph-algo") (v "5.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "1gbj2zrjlwmkdpmski0wivg328bv0gls2q8xz21dfbysriqb4rds") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-5.1.1 (c (n "traitgraph-algo") (v "5.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "0lrkni1jap2wng0r9w5l7kzv84xd1whm5km97mfv80x6gqrszn60") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-5.2.0 (c (n "traitgraph-algo") (v "5.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "122bk3qgxpx1bgy108i0ppvc8bz0sssk40rb4vmfrd183la0g0k8") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-5.3.0 (c (n "traitgraph-algo") (v "5.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "1bs49jlv5flrzj6ipxzrl0n4y15i69hxhf6nbzlcf84fxld6cp4r") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-5.4.0 (c (n "traitgraph-algo") (v "5.4.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "066rpai86hhrxlvc4yz14gb7w4lg05916n9rv2k1m5i4a4ay66q8") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.56.1")))

(define-public crate-traitgraph-algo-6.0.0 (c (n "traitgraph-algo") (v "6.0.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^3.0.0") (d #t) (k 0)))) (h "0b3gx95sqashxz51wwrrcwrxkkm7pf6q86x13qvfxiyr85z8ybdz") (f (quote (("hashbrown_dijkstra_node_weight_array" "hashbrown")))) (r "1.65.0")))

(define-public crate-traitgraph-algo-6.1.0 (c (n "traitgraph-algo") (v "6.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^3.0.0") (d #t) (k 0)))) (h "0q3jq49is0697hfq38jr614x6b1rdxhpsf7npxw0gf7d4n7nvlf4") (f (quote (("hashbrown_dijkstra_node_weight_array")))) (r "1.65.0")))

(define-public crate-traitgraph-algo-7.0.0-alpha.1 (c (n "traitgraph-algo") (v "7.0.0-alpha.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0-alpha.2") (d #t) (k 0)))) (h "14asxf06m56akai72z498d6mahxg0dxgjd62d5s2hzkw94ngxg34") (f (quote (("hashbrown_dijkstra_node_weight_array")))) (r "1.65.0")))

(define-public crate-traitgraph-algo-7.0.0-alpha.2 (c (n "traitgraph-algo") (v "7.0.0-alpha.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0-alpha.3") (d #t) (k 0)))) (h "1hf2f6flwixz7vig7i4ja00j728awvy47vb901xcrxk5x563hq6j") (f (quote (("hashbrown_dijkstra_node_weight_array")))) (r "1.65.0")))

(define-public crate-traitgraph-algo-7.0.0-alpha.3 (c (n "traitgraph-algo") (v "7.0.0-alpha.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0-alpha.4") (d #t) (k 0)))) (h "12sljcq25p5v89qwi19319vy5dms7500rbk4d4j4majr99pq8cr8") (f (quote (("hashbrown_dijkstra_node_weight_array")))) (r "1.65.0")))

(define-public crate-traitgraph-algo-7.0.0-alpha.4 (c (n "traitgraph-algo") (v "7.0.0-alpha.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0-alpha.5") (d #t) (k 0)))) (h "0m9abhxbxgbgy3fah2l0ycfw0svkh2x517z02b8b63bsmr8a5j1x") (f (quote (("hashbrown_dijkstra_node_weight_array")))) (r "1.65.0")))

(define-public crate-traitgraph-algo-7.0.0-alpha.5 (c (n "traitgraph-algo") (v "7.0.0-alpha.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0-alpha.6") (d #t) (k 0)))) (h "18w1k03b0giqm60ilsyxkbizb6dlkcnla3k4d158nhksai50dng3") (f (quote (("hashbrown_dijkstra_node_weight_array")))) (r "1.65.0")))

(define-public crate-traitgraph-algo-7.0.0 (c (n "traitgraph-algo") (v "7.0.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0") (d #t) (k 0)))) (h "1qf9rnhqzqy2c7nbqngf18wbk8z334k6s96lj679fqgjw7s6pfva") (f (quote (("hashbrown_dijkstra_node_weight_array")))) (r "1.65.0")))

(define-public crate-traitgraph-algo-8.0.0 (c (n "traitgraph-algo") (v "8.0.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^5.0.0") (d #t) (k 0)))) (h "1ygvhh7cm07vndcgibv70bxms6zkb3kqlgc9i7vs8jcafcmsmg14") (f (quote (("hashbrown_dijkstra_node_weight_array")))) (r "1.65.0")))

(define-public crate-traitgraph-algo-8.1.0 (c (n "traitgraph-algo") (v "8.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "traitgraph") (r "^5.0.0") (d #t) (k 0)))) (h "1lrpf16zvws49lpqhwvzg0lxm2mbasr4jx7kqdjdxa6afc02f83v") (f (quote (("hashbrown_dijkstra_node_weight_array")))) (r "1.65.0")))

