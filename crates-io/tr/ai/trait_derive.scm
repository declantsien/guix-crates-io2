(define-module (crates-io tr ai trait_derive) #:use-module (crates-io))

(define-public crate-trait_derive-0.1.0 (c (n "trait_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "trait_derive_core") (r "^0.1.0") (d #t) (k 0)))) (h "1fk39lb3gq8cbjmlxqf4cv3q0pkcivnanyz8pb93g1whs0cwwilr")))

