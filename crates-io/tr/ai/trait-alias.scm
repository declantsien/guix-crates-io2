(define-module (crates-io tr ai trait-alias) #:use-module (crates-io))

(define-public crate-trait-alias-0.1.0 (c (n "trait-alias") (v "0.1.0") (d (list (d (n "num") (r "^0") (d #t) (k 0)) (d (n "numb") (r "^0") (d #t) (k 0)))) (h "07b2s3y41bxrqna5cjngzsjqgxymchwy4y16b5mwx111lzzmjab9")))

(define-public crate-trait-alias-0.1.1 (c (n "trait-alias") (v "0.1.1") (d (list (d (n "num") (r "^0") (d #t) (k 0)) (d (n "numb") (r "^0") (d #t) (k 0)))) (h "0rh9nv1p98gag1rmmppz804056d4q18wlwwkin4wc51752a4q28k")))

