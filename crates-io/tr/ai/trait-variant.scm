(define-module (crates-io tr ai trait-variant) #:use-module (crates-io))

(define-public crate-trait-variant-0.0.0 (c (n "trait-variant") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "0l5g8ryp3g3wgbcyvapq077jfk36cz83ia8jsvl224k9r7idgnax")))

(define-public crate-trait-variant-0.0.1 (c (n "trait-variant") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "1sd8vv31p9qf9276vzqb802k6xp8wi00qjf6rzq2lh03dpq05mxk")))

(define-public crate-trait-variant-0.1.0 (c (n "trait-variant") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "00bckbzjxqlnaksyli2qrjw9jmp8qjrjpw5hx492w06b5h8874ba") (r "1.75")))

(define-public crate-trait-variant-0.1.1 (c (n "trait-variant") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "19jgbwxw42iqmlxh5855pbk3i01b7fvrzy52h5fr55qy0rvs9qa5") (r "1.75")))

(define-public crate-trait-variant-0.1.2 (c (n "trait-variant") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "19vpbnbcsxdiznwdw854pd0vya7rm7v7hnl3nh741621603pg5vh") (r "1.75")))

