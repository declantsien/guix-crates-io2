(define-module (crates-io tr ai train-map) #:use-module (crates-io))

(define-public crate-train-map-0.1.0 (c (n "train-map") (v "0.1.0") (h "08dbhckw1cwlgmmv59ik5pp33gxpsv3j6w2bp4c3pf468x2gywzh")))

(define-public crate-train-map-0.1.1 (c (n "train-map") (v "0.1.1") (h "0gzwkfkvqmj272j5m3xjdk5x5q4mgzmhfs16k3vp0zaxc4ysddmm")))

