(define-module (crates-io tr ai traitcast_core) #:use-module (crates-io))

(define-public crate-traitcast_core-0.2.0 (c (n "traitcast_core") (v "0.2.0") (d (list (d (n "anymap") (r "0.12.*") (d #t) (k 0)) (d (n "inventory") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "inventory") (r "0.1.*") (d #t) (k 2)))) (h "0jncwdvrfm5r11xmvbf6lqm6kbagkf1cbkyqhhdgcqrrm3saifxa") (f (quote (("use_inventory" "inventory"))))))

