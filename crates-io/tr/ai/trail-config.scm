(define-module (crates-io tr ai trail-config) #:use-module (crates-io))

(define-public crate-trail-config-0.1.1 (c (n "trail-config") (v "0.1.1") (d (list (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "0fppnfglwjgbn0c8wki80cz2jbvn3nj7k2jhdi9kqc4ykk7bix6r")))

(define-public crate-trail-config-0.1.2 (c (n "trail-config") (v "0.1.2") (d (list (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "0ysx7ixa3glv3m5zgnb4940a35206y00jkv7xgpr041s32dd7432")))

(define-public crate-trail-config-0.1.3 (c (n "trail-config") (v "0.1.3") (d (list (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "0rxd86xfm5qm4154kyavqsswbjynxkshi4pxzdlfk6fqgrqr8q3n")))

(define-public crate-trail-config-0.1.4 (c (n "trail-config") (v "0.1.4") (d (list (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "10xd2b56clcn3rw6g54s6zn4sqqr1qjf3ql5j9kv47j1srzw1hc1")))

