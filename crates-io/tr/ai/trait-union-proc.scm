(define-module (crates-io tr ai trait-union-proc) #:use-module (crates-io))

(define-public crate-trait-union-proc-0.1.0 (c (n "trait-union-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1iy57ibj2v88fwmrjvb06dvfjkqbyx1i5c76z0h5691yv01al6ln")))

(define-public crate-trait-union-proc-0.1.1 (c (n "trait-union-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0c524bnl2rh0cx9ldpd9w9a1lwfkj42fmp0qy99200pclcpkkfpp")))

(define-public crate-trait-union-proc-0.1.2 (c (n "trait-union-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0p7f57fwaclpd8g638n13p3lhfjdbj179nv7ni9spvjbj45m9191")))

(define-public crate-trait-union-proc-0.1.3 (c (n "trait-union-proc") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0f4q8fvyqv5sz5yb5vllzgsklxj22697arzr24d1bawirx3z2ba3")))

(define-public crate-trait-union-proc-0.1.4 (c (n "trait-union-proc") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0z2rlaan2k4qjv0m1hzwrwpm28bpn1hjz4cjavks3l5h7adqbvnj")))

