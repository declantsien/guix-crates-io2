(define-module (crates-io tr ai traildb-sys) #:use-module (crates-io))

(define-public crate-traildb-sys-0.6.0 (c (n "traildb-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "1p2vjh1m52ap6pp3p9dnzhyjmq7bchg6i70cnjclzqsjb16rszvz")))

(define-public crate-traildb-sys-0.6.1 (c (n "traildb-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "029zy5f0vimjm2asc118dish247q1imgwwqxamw92p579brxl1xr")))

(define-public crate-traildb-sys-0.6.2 (c (n "traildb-sys") (v "0.6.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "1b67f5z9rgcswazg0hcpg1k7pchj19n0y4b53yab846lb2nvcplb")))

(define-public crate-traildb-sys-0.6.3 (c (n "traildb-sys") (v "0.6.3") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "055mpm7wl6m2mv3c6m53ghrjnga4g95hakmnh99lx96vwwccxkvr")))

