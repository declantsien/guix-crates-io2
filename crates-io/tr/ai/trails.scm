(define-module (crates-io tr ai trails) #:use-module (crates-io))

(define-public crate-trails-0.0.1 (c (n "trails") (v "0.0.1") (d (list (d (n "druid") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "simple_logger") (r "^2.2") (d #t) (k 0)))) (h "0rfcppm8hzvyv887vl3nk4rggfyy7lajh03qc5scvl6ls11a06d7")))

