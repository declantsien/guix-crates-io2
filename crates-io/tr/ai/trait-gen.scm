(define-module (crates-io tr ai trait-gen) #:use-module (crates-io))

(define-public crate-trait-gen-0.1.0 (c (n "trait-gen") (v "0.1.0") (d (list (d (n "num") (r "^0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1gzdvz4ch4fgyykg5qf7lp5g1y4s2mx9aj2fbyd9242gazqvxzdy")))

(define-public crate-trait-gen-0.1.1 (c (n "trait-gen") (v "0.1.1") (d (list (d (n "num") (r "^0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "07aqw3zrpchs0ybdjcfzc51acha15mlip7hhlxsmhpdpg4zw1629")))

(define-public crate-trait-gen-0.1.2 (c (n "trait-gen") (v "0.1.2") (d (list (d (n "num") (r "^0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0zmr9yngk6fag5k00c791j9i38zr7smxsvsiwyfr3dx5v075wmzq")))

(define-public crate-trait-gen-0.1.3 (c (n "trait-gen") (v "0.1.3") (d (list (d (n "num") (r "^0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "164gp63mbivp0hbysspizzcpd0xsfshxh385rm4h348l5q2g0n9l")))

(define-public crate-trait-gen-0.1.4 (c (n "trait-gen") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1a2qljnx75q9f3n3wk36nldm8kjynp63vvrfpv5m70vip3nsx931")))

(define-public crate-trait-gen-0.1.5 (c (n "trait-gen") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1l43xa42f5s1pvk0vpampjs3r0w64jysj8b9ql844554bsppckhb")))

(define-public crate-trait-gen-0.1.6 (c (n "trait-gen") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "18rljffpcdc4g76ppd1wmwk19g8ka4g8x2z4lxbn63hlgkw375cd")))

(define-public crate-trait-gen-0.1.7 (c (n "trait-gen") (v "0.1.7") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "02wg5wp4nr9311qcl7f5ca0mr4ymjn0nsbdxglim4c3qsc5fck37")))

(define-public crate-trait-gen-0.2.0 (c (n "trait-gen") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0d950j0zc9myy6hq7mr48yx8abwdq6f5qravmd1zh3qwvxzhxsf1") (r "1.58.0")))

(define-public crate-trait-gen-0.2.1 (c (n "trait-gen") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1q47ix7qn4x4pi3nsr6rgq8jcpqdzpnnh9kb5v3fwghgnaj0ymf6") (r "1.58.0")))

(define-public crate-trait-gen-0.2.2 (c (n "trait-gen") (v "0.2.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1b96bcfzcw5ph4a2lrz5n8qvn7w5s384sq7v2xhajfcq60cbxa1k") (r "1.58.0")))

(define-public crate-trait-gen-0.3.0 (c (n "trait-gen") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0j3yir5l64vm412dh1bsfflz214jrrjkyacaicl6s8072gd5hd42") (f (quote (("in_format")))) (r "1.58.0")))

(define-public crate-trait-gen-0.3.1 (c (n "trait-gen") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1wpiascyw77zfbw85ba14dvy10jg8in76vsslav1a9ayz1pvar59") (f (quote (("in_format")))) (r "1.58.0")))

(define-public crate-trait-gen-0.3.2 (c (n "trait-gen") (v "0.3.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0pvi045l6qa4b093qm4q6fihidrl2gh3yjn9f0r5n5gl5wbaixjl") (f (quote (("in_format")))) (r "1.58.0")))

