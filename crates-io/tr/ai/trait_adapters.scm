(define-module (crates-io tr ai trait_adapters) #:use-module (crates-io))

(define-public crate-trait_adapters-0.1.0 (c (n "trait_adapters") (v "0.1.0") (d (list (d (n "trait_adapters_macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1kxbfqbvy0zh5zynp6z8yn7k18hyshxbdilvbv5mj3yy60yyvdf0") (f (quote (("macros" "trait_adapters_macros") ("default"))))))

