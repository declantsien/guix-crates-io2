(define-module (crates-io tr ai traitobject) #:use-module (crates-io))

(define-public crate-traitobject-0.0.1 (c (n "traitobject") (v "0.0.1") (h "05h6c3rdwvb8wv7iwj7dkxvdm3km6nb32a2nw5yclzxvi5vfpsh7")))

(define-public crate-traitobject-0.0.2 (c (n "traitobject") (v "0.0.2") (h "0v636yv1x2gzz7dgm193k71k681071h0wcynz2n7d17cvw73cmin")))

(define-public crate-traitobject-0.0.3 (c (n "traitobject") (v "0.0.3") (h "1n09xxspc2xlmw5cqa0afs3lkqbbkbg1b7bgva15ija7zya3ghlx")))

(define-public crate-traitobject-0.1.0 (c (n "traitobject") (v "0.1.0") (h "0yb0n8822mr59j200fyr2fxgzzgqljyxflx9y8bdy3rlaqngilgg")))

