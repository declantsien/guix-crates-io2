(define-module (crates-io tr ai trait_tests) #:use-module (crates-io))

(define-public crate-trait_tests-0.1.0 (c (n "trait_tests") (v "0.1.0") (h "0zp1lxl5c43xw5q707d98ydyinm61rwlry70h1flv5ggna6k5y2p")))

(define-public crate-trait_tests-0.1.1 (c (n "trait_tests") (v "0.1.1") (h "13z53wz777jx7q3v8q6z289qxxlzzd4szrhwa24bk99pkr9yq51n")))

(define-public crate-trait_tests-0.1.2 (c (n "trait_tests") (v "0.1.2") (h "0lh8wgh1d1y1r78ljc5pq2b2r8jrdzf9yph8z1bmc6v52n5bg4rs")))

(define-public crate-trait_tests-0.2.0 (c (n "trait_tests") (v "0.2.0") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1mqh19afn6w8ds15xbjmqhy3c7x1qlbh8miiq5yhn5jr10cgi6hy") (f (quote (("default" "syn/full"))))))

(define-public crate-trait_tests-0.2.1 (c (n "trait_tests") (v "0.2.1") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0r4d29j83w9cdckpfbd659325wzpxajc1ngdsvbjb77cl4ldfrcv") (f (quote (("default" "syn/full"))))))

(define-public crate-trait_tests-0.3.0 (c (n "trait_tests") (v "0.3.0") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1hjzvr82sf14gfn7md106q2zr7kqfwyq055yw51594g06aysa5kk") (f (quote (("default" "syn/full"))))))

(define-public crate-trait_tests-0.3.1 (c (n "trait_tests") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0bdp8d0y0shajbkvm2qapa32v4wpr5p0a06rp4wiv0i74l8xb0jg") (f (quote (("default" "syn/full"))))))

(define-public crate-trait_tests-0.3.2 (c (n "trait_tests") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0s71zkmqg0mrnbkxv28yg2mkk5mj9aqri6p0kjczdg8gfffwnm74") (f (quote (("default" "syn/full"))))))

(define-public crate-trait_tests-0.3.3 (c (n "trait_tests") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "13b5hw4kr21pkflyyh4fzg9i8znc3fw0vrqjs76bz3fgdf3k66ih") (f (quote (("default" "syn/full"))))))

