(define-module (crates-io tr ai traitor) #:use-module (crates-io))

(define-public crate-traitor-0.0.0 (c (n "traitor") (v "0.0.0") (d (list (d (n "dynasmrt") (r "^0.2.3") (d #t) (k 0)) (d (n "traitor-derive") (r "^0.0.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "0l5d5wsnx687kj5rw0ypy3wfnkh267fwrxjhlpmbc5alhlmn8682") (f (quote (("default"))))))

