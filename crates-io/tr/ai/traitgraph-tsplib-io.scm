(define-module (crates-io tr ai traitgraph-tsplib-io) #:use-module (crates-io))

(define-public crate-traitgraph-tsplib-io-0.6.0 (c (n "traitgraph-tsplib-io") (v "0.6.0") (d (list (d (n "traitgraph") (r "^0.6.0") (d #t) (k 0)))) (h "1ws9q1b2q554p7wiwgxylyjlbxnrkb6dcsd1cay5ypvmajsrpnyb") (r "1.49.0")))

(define-public crate-traitgraph-tsplib-io-0.6.2 (c (n "traitgraph-tsplib-io") (v "0.6.2") (d (list (d (n "traitgraph") (r "^0.6.1") (d #t) (k 0)))) (h "1qk2rmzjj5v4v172xcrrkb3vlzk3hvhawwrb80rll4097vpri1mw") (r "1.49.0")))

(define-public crate-traitgraph-tsplib-io-0.7.0 (c (n "traitgraph-tsplib-io") (v "0.7.0") (d (list (d (n "traitgraph") (r "^0.7.0") (d #t) (k 0)))) (h "1c5m7xiblhml5gnkhb9qq0b915zy7sz117ndpbps0wjb9v2s1kzy") (r "1.49.0")))

(define-public crate-traitgraph-tsplib-io-0.10.0 (c (n "traitgraph-tsplib-io") (v "0.10.0") (d (list (d (n "traitgraph") (r "^0.10.0") (d #t) (k 0)))) (h "0xvx8kgfk52pnwd0winzxa1gxbhjpmwim3j24zb7hb5yrw98j8rh") (r "1.56.1")))

(define-public crate-traitgraph-tsplib-io-1.0.0 (c (n "traitgraph-tsplib-io") (v "1.0.0") (d (list (d (n "traitgraph") (r "^1.0.0") (d #t) (k 0)))) (h "1p2f6r0mfhpz77my91k0jcb1snbdbmiknx6saq4dnlg2286p7ak3") (r "1.56.1")))

(define-public crate-traitgraph-tsplib-io-2.0.0 (c (n "traitgraph-tsplib-io") (v "2.0.0") (d (list (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "1yw1zadzwv77wdqqha5j92zzg52hni7w6ry81w9181nsic5sq93i") (r "1.56.1")))

(define-public crate-traitgraph-tsplib-io-3.0.0 (c (n "traitgraph-tsplib-io") (v "3.0.0") (d (list (d (n "traitgraph") (r "^3.0.0") (d #t) (k 0)))) (h "1yc72d2a07xwfkl74nv4ar23nfibh4brc36p4aqqj02b7rn2xki4") (r "1.65.0")))

(define-public crate-traitgraph-tsplib-io-4.0.0 (c (n "traitgraph-tsplib-io") (v "4.0.0") (d (list (d (n "traitgraph") (r "^4.0.0") (d #t) (k 0)))) (h "173bkxxpqfa7wwvkhajann1ixr06gslph3izn8wrykfgwaj6s1xv") (r "1.65.0")))

(define-public crate-traitgraph-tsplib-io-5.0.0 (c (n "traitgraph-tsplib-io") (v "5.0.0") (d (list (d (n "traitgraph") (r "^5.0.0") (d #t) (k 0)))) (h "1icfilc01kfcligd54mmgxd5vc9vgz80fqxz3bcn2f47kasqzapf") (r "1.65.0")))

