(define-module (crates-io tr ai traitgraph-dimacs-io) #:use-module (crates-io))

(define-public crate-traitgraph-dimacs-io-0.6.0 (c (n "traitgraph-dimacs-io") (v "0.6.0") (d (list (d (n "traitgraph") (r "^0.6.0") (d #t) (k 0)))) (h "092f7h9i55msn373dncmynj6wgm5jq8ravz4xcbb6yl4v8y333vj") (r "1.49.0")))

(define-public crate-traitgraph-dimacs-io-0.6.2 (c (n "traitgraph-dimacs-io") (v "0.6.2") (d (list (d (n "traitgraph") (r "^0.6.1") (d #t) (k 0)))) (h "0rs3qxws00w54k0xcw63zp5mzbwkxk4547a2fci1zf46pjk9h556") (r "1.49.0")))

(define-public crate-traitgraph-dimacs-io-0.7.0 (c (n "traitgraph-dimacs-io") (v "0.7.0") (d (list (d (n "traitgraph") (r "^0.7.0") (d #t) (k 0)))) (h "0y5bqk4kwzikbs73xb5kp4rhl8rn8mxj805d2irhw7jw8kqbk341") (r "1.49.0")))

(define-public crate-traitgraph-dimacs-io-0.10.0 (c (n "traitgraph-dimacs-io") (v "0.10.0") (d (list (d (n "traitgraph") (r "^0.10.0") (d #t) (k 0)))) (h "15ibgmmbwvqkjiy7gqvfn14zn7a5zxvra1i5dwh1jxk6zwb8ifpl") (r "1.56.1")))

(define-public crate-traitgraph-dimacs-io-1.0.0 (c (n "traitgraph-dimacs-io") (v "1.0.0") (d (list (d (n "traitgraph") (r "^1.0.0") (d #t) (k 0)))) (h "1xxsqk819dg7hlcfgvsbhzk78gdvpldgxvlas1fc0a5fqhszxxx2") (r "1.56.1")))

(define-public crate-traitgraph-dimacs-io-2.0.0 (c (n "traitgraph-dimacs-io") (v "2.0.0") (d (list (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "0zskrqpk9h6lm0gfilnp1cz9fgmysa8yni9c8kac97z8pk69mc4b") (r "1.56.1")))

(define-public crate-traitgraph-dimacs-io-3.0.0 (c (n "traitgraph-dimacs-io") (v "3.0.0") (d (list (d (n "traitgraph") (r "^3.0.0") (d #t) (k 0)))) (h "0kmdjmj06833n51zal4a4y906jqhxgan5mm3q76bf0canfpw4416") (r "1.65.0")))

(define-public crate-traitgraph-dimacs-io-4.0.0 (c (n "traitgraph-dimacs-io") (v "4.0.0") (d (list (d (n "traitgraph") (r "^4.0.0") (d #t) (k 0)))) (h "0xvvb10mg62l39m48qq0i2kh37xplrnjqnlqyrll5libhghv532g") (r "1.65.0")))

(define-public crate-traitgraph-dimacs-io-5.0.0 (c (n "traitgraph-dimacs-io") (v "5.0.0") (d (list (d (n "traitgraph") (r "^5.0.0") (d #t) (k 0)))) (h "1m9qnkg8ndkdgbbj4i8waa8qch9c1imv6fvmll8qwqimi9lzk3cj") (r "1.65.0")))

