(define-module (crates-io tr ai trait-tactics) #:use-module (crates-io))

(define-public crate-trait-tactics-0.1.0 (c (n "trait-tactics") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)) (d (n "trait-tactics-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1nycc2d627g0hp0yg552nrzv94ay09z2xkmz02z0z4gi58964v5y") (s 2) (e (quote (("num-traits" "dep:num-traits" "trait-tactics-macros/num-traits"))))))

