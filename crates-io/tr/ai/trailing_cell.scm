(define-module (crates-io tr ai trailing_cell) #:use-module (crates-io))

(define-public crate-trailing_cell-0.1.0 (c (n "trailing_cell") (v "0.1.0") (d (list (d (n "bus") (r "^1.4.0") (d #t) (k 0)))) (h "1462ava9s7yvg192chhzkihlkij0dp06bffywrnw0xj0fqky5h32")))

(define-public crate-trailing_cell-0.1.1 (c (n "trailing_cell") (v "0.1.1") (d (list (d (n "bus") (r "^1.4.0") (d #t) (k 0)))) (h "0m5m6h5w04fbjbwk3axgis68pypnxr9bbk3zypvmqcyhpg7s9ql2")))

(define-public crate-trailing_cell-0.2.0 (c (n "trailing_cell") (v "0.2.0") (d (list (d (n "bus") (r "^1.4.0") (d #t) (k 0)))) (h "18bndx53c825nidqwfqwpc4qyx692wf9f08gf5q219mgd0xinndq")))

(define-public crate-trailing_cell-0.2.1 (c (n "trailing_cell") (v "0.2.1") (d (list (d (n "bus") (r "^1.4.0") (d #t) (k 0)))) (h "07wbqbs3nb15s5a0gd9hd7pr27f4ya2q5rhrjzs6fx80b6vvhfck")))

(define-public crate-trailing_cell-0.3.0 (c (n "trailing_cell") (v "0.3.0") (d (list (d (n "bus") (r "^1.4.0") (d #t) (k 0)))) (h "1g8m3s2x6s1cqai8jx542g9jzbvbblf2sbazb266bimh6wp30mh8")))

