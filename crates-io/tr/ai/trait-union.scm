(define-module (crates-io tr ai trait-union) #:use-module (crates-io))

(define-public crate-trait-union-0.1.0 (c (n "trait-union") (v "0.1.0") (d (list (d (n "trait-union-proc") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "08aflc9q7ybq1j00ainvc8j7s1mcd6pwwl8qlvybakbhdz8f2pcm")))

(define-public crate-trait-union-0.1.1 (c (n "trait-union") (v "0.1.1") (d (list (d (n "trait-union-proc") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "07w05hs76w5xy7z62mlva13n6155m1knjw5w16bqky8rwsb4xv50")))

(define-public crate-trait-union-0.1.2 (c (n "trait-union") (v "0.1.2") (d (list (d (n "trait-union-proc") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0dk9xj7jwcfd08rz7p1rxpyhkdr0gysldz5ln1kywvsxiapwcl2n")))

(define-public crate-trait-union-0.1.3 (c (n "trait-union") (v "0.1.3") (d (list (d (n "trait-union-proc") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1vkmcifs7gphjjd07c8dsjbpasjcqywpm3b54wjsn1ryqaxgdrs2")))

(define-public crate-trait-union-0.1.4 (c (n "trait-union") (v "0.1.4") (d (list (d (n "trait-union-proc") (r "=0.1.4") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0ck0x4997mrqgzyiqgmmy0501ydnvc7k71zhrk9s5cxf19r51pgf")))

