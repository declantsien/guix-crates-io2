(define-module (crates-io tr ai trait-stack) #:use-module (crates-io))

(define-public crate-trait-stack-0.1.0 (c (n "trait-stack") (v "0.1.0") (h "1ld6kc27794ka7r97vf44wwz04fgivlw26289c3lfqksxflbr6c0") (y #t)))

(define-public crate-trait-stack-0.1.1 (c (n "trait-stack") (v "0.1.1") (h "1a6vp5x2m8y8jw7ns4mnh89wcwj98i9w7n1r50s603m610d1ll7c") (y #t)))

(define-public crate-trait-stack-0.1.2 (c (n "trait-stack") (v "0.1.2") (h "1wwp76s7nd5k3cw42avfk0np4vhvabcs26l1114vbb64b34c8yaf") (y #t)))

(define-public crate-trait-stack-0.1.3 (c (n "trait-stack") (v "0.1.3") (h "1s543nj6f4v4bmlli8l43cmsn709i756dlvj8sjw4jl358b8hvfh")))

