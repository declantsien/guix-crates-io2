(define-module (crates-io tr ai trait-match-proc-macro) #:use-module (crates-io))

(define-public crate-trait-match-proc-macro-0.1.0 (c (n "trait-match-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17k6kcqgdgsd8sb7qjxswy32151jvxfh0xik0p6dip2k2iz168l9")))

