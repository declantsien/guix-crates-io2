(define-module (crates-io tr ai trait-async) #:use-module (crates-io))

(define-public crate-trait-async-0.1.0 (c (n "trait-async") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zfx6fx6g56pybkxsamim2s7kkx7i5kiks6jd2v7pgxpmkbi10kq") (f (quote (("support_old_nightly"))))))

(define-public crate-trait-async-0.1.1 (c (n "trait-async") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0y4zzqj47jdhxniy7lnndbpkc3chf13k5ia1afl3wqswnkm4vkyd") (f (quote (("support_old_nightly"))))))

(define-public crate-trait-async-0.1.2 (c (n "trait-async") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0437fsj86r68ym5fwss8d2nmyp061jgx4jvxdw5zpddn8j8j60y0") (f (quote (("support_old_nightly"))))))

(define-public crate-trait-async-0.1.24 (c (n "trait-async") (v "0.1.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "17b92fi0ginsgs3p77n4mq1y1iv19s5xj8kj7g4r9r9ff5acds6z") (f (quote (("support_old_nightly"))))))

