(define-module (crates-io tr ai traitify-core) #:use-module (crates-io))

(define-public crate-traitify-core-0.1.0 (c (n "traitify-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0daxv371r0sfb8yzi4x05ksn30nj74a776pan476w141in8g1rnh")))

