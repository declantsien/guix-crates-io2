(define-module (crates-io tr ai traitful) #:use-module (crates-io))

(define-public crate-traitful-0.1.0 (c (n "traitful") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "proc-macro" "printing" "extra-traits" "clone-impls"))) (k 0)))) (h "1fvla3l0zgz6cm0vy0i88k5yk6rd13sb683vyq76i7ypi3ypkl4p")))

(define-public crate-traitful-0.2.0 (c (n "traitful") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "proc-macro" "printing" "extra-traits" "clone-impls"))) (k 0)))) (h "0z8q775lp183y5wf4sw741az7x04c2idays8bp16xx870p7qv2pw")))

(define-public crate-traitful-0.2.1 (c (n "traitful") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "proc-macro" "printing" "extra-traits" "clone-impls"))) (k 0)))) (h "16hd5h5bazwngv7kanq0mayrji6avgrn00j10lk37l0g39pl1p51")))

(define-public crate-traitful-0.3.0 (c (n "traitful") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "proc-macro" "printing" "extra-traits" "clone-impls"))) (k 0)))) (h "09a4kj5s4db83xqf5c7py8j0js3g10037infqfgrpdqzmlpf4mnq")))

