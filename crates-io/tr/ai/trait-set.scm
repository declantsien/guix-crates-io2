(define-module (crates-io tr ai trait-set) #:use-module (crates-io))

(define-public crate-trait-set-0.1.0 (c (n "trait-set") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1v72d9clmx23vx7dl7dqdr4qwwh6p9lxkwrfl0m9q2am3qi1zpq4")))

(define-public crate-trait-set-0.2.0 (c (n "trait-set") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1wh8zx7nijgy0snl8ni74i4281wqlpzik54sz9if69687j3lqp47")))

(define-public crate-trait-set-0.3.0 (c (n "trait-set") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "09b6afmx2z5p2jjxlm4i36n4k3vynihnk5ym41y6sk5lkaf2x7mp")))

