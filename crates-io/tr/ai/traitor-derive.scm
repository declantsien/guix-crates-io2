(define-module (crates-io tr ai traitor-derive) #:use-module (crates-io))

(define-public crate-traitor-derive-0.0.0 (c (n "traitor-derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "11zr6d1mzkxanvl2jva7j6vhr1nzsmxvwws72ksj5prjvrgrhccb")))

