(define-module (crates-io tr ai trait-based-collection-macros) #:use-module (crates-io))

(define-public crate-trait-based-collection-macros-0.1.0 (c (n "trait-based-collection-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "080f1faxghy1h8ny9xcfl52v3fsg0c8jgiil0p6rmzj9ayccfgvs")))

