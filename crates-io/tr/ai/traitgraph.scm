(define-module (crates-io tr ai traitgraph) #:use-module (crates-io))

(define-public crate-traitgraph-0.1.0-alpha.1 (c (n "traitgraph") (v "0.1.0-alpha.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "0hiyrmdwj9rn3kr721xcsnih6r1yrcd5wcxljsnpxh7144iz9pa1")))

(define-public crate-traitgraph-0.1.0-rc.1 (c (n "traitgraph") (v "0.1.0-rc.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "1gqz8zjfjv5g1r5lffqijfc6n9wamy5dzvf9qb7q6wl79rykhny5")))

(define-public crate-traitgraph-0.1.0 (c (n "traitgraph") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "18v2038b47avlkqf0m0z9wwx23l8sdbsiswd3jpr4v5jaiw1436j")))

(define-public crate-traitgraph-0.2.0 (c (n "traitgraph") (v "0.2.0") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.2.0") (d #t) (k 0)))) (h "0igxng605j5lvvacvygab1vvmh9izg56nyqcl6n7bkgdhxkzhpqn")))

(define-public crate-traitgraph-0.3.0 (c (n "traitgraph") (v "0.3.0") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.3.0") (d #t) (k 0)))) (h "1fwsw1wvr09jzarkxq0a2kidmjk4kfkk3yq4i0chrxkclq47zqyy")))

(define-public crate-traitgraph-0.3.1 (c (n "traitgraph") (v "0.3.1") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.3.1") (d #t) (k 0)))) (h "0z83mkifl9jlqc9xy62hfrgvzrv4hzq483ivyvdypnqh15mq4c76")))

(define-public crate-traitgraph-0.3.1-alpha.1 (c (n "traitgraph") (v "0.3.1-alpha.1") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.3.1-alpha.0") (d #t) (k 0)))) (h "0si0zwzhp63vnj9d359ypz5pgfy68q74pdrqs5arnaypsadmj6ni")))

(define-public crate-traitgraph-0.4.0 (c (n "traitgraph") (v "0.4.0") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "1rx03g03my9rzirwnqx15w79r1bkc494xrjjv2dbv1ch488s26h8")))

(define-public crate-traitgraph-0.4.1 (c (n "traitgraph") (v "0.4.1") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "0xap52m1bw92vk22az8q1kqvhycs7v8k8jr5s9x59dd1g8ai9qlb")))

(define-public crate-traitgraph-0.4.4 (c (n "traitgraph") (v "0.4.4") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "0qyfr6l3x7s0kkk608bwvazpawfvjik4ph8c45qd914aqbpsf7z2")))

(define-public crate-traitgraph-0.4.5 (c (n "traitgraph") (v "0.4.5") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "11yd1rprxrqm0y8vdzzj9ci4sxalhb5lfrzsda74w5q4v327h4ij")))

(define-public crate-traitgraph-0.4.6 (c (n "traitgraph") (v "0.4.6") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "0cshrryjhgqyj8ql7qgkm2flkf01f33lqm6kb6kzblmnd8qbspr6")))

(define-public crate-traitgraph-0.4.7 (c (n "traitgraph") (v "0.4.7") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "1md5c3zz4ai6z8vrq45aiawp05s1398vybl4kcxvvap88vpv8rkv")))

(define-public crate-traitgraph-0.4.8 (c (n "traitgraph") (v "0.4.8") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "1dbcfv6ylm9incpw0kl44fx2a724vqy7zw51j2x1bg89hax8rn8x")))

(define-public crate-traitgraph-0.4.9 (c (n "traitgraph") (v "0.4.9") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "0whsc68kn6nziy8nj6y3m0y7fjci18h6hh93xk46rpscwmmh8kv2")))

(define-public crate-traitgraph-0.4.10 (c (n "traitgraph") (v "0.4.10") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "1k0ybbdlw6dsh3iblkhqa3b5l8lpblwk5p2914x799rbb5glwb9p")))

(define-public crate-traitgraph-0.5.0 (c (n "traitgraph") (v "0.5.0") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "025zir3b8xgcgk5x0mhgq7hb1rlc4ns7530pjlxkj0q6bhil0xfl")))

(define-public crate-traitgraph-0.5.1 (c (n "traitgraph") (v "0.5.1") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "03k3xffx1wjx5adw1p06j5y30igvmi2g6smfn7wkkvl8bnixll1j")))

(define-public crate-traitgraph-0.6.0 (c (n "traitgraph") (v "0.6.0") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitsequence") (r "^0.6.0") (d #t) (k 0)))) (h "1f7gyzrmwcai6x68bgf2cz3l524zjmbl96b8gn5r3q023vs43icc") (r "1.49.0")))

(define-public crate-traitgraph-0.6.1 (c (n "traitgraph") (v "0.6.1") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitsequence") (r "^0.6.0") (d #t) (k 0)))) (h "1y5bkqhlb8ysbkzh2mpggn6j1wmkc2jw6251gdd0i3ayjh0ab6dh") (r "1.49.0")))

(define-public crate-traitgraph-0.7.0 (c (n "traitgraph") (v "0.7.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.6.0") (d #t) (k 0)))) (h "13bn7fy8kihva4wv2rmiqqq0hp0z6mnk4pbjjy2s14lbarwjvxi7") (r "1.49.0")))

(define-public crate-traitgraph-0.10.0 (c (n "traitgraph") (v "0.10.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.10.0") (d #t) (k 0)))) (h "0qhph9617ia2plzqwppwynz5jzfarkafvpqnvsp80lhbhf714qng") (r "1.56.1")))

(define-public crate-traitgraph-1.0.0 (c (n "traitgraph") (v "1.0.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitsequence") (r "^1.0.0") (d #t) (k 0)))) (h "0jk7ympnl9kvkk16kl9h6c2f04g6xkjdj3ivckwqfz4vq3zvabcs") (r "1.56.1")))

(define-public crate-traitgraph-1.1.0 (c (n "traitgraph") (v "1.1.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitsequence") (r "^1.0.0") (d #t) (k 0)))) (h "1sgm1xxr5wc2qra06x454vkq4k8i9w7vd8gyhljsnja1zyhkk4im") (r "1.56.1")))

(define-public crate-traitgraph-1.2.0 (c (n "traitgraph") (v "1.2.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitsequence") (r "^1.0.0") (d #t) (k 0)))) (h "0b7rz424i54vgswnmqbn6ijzjgfhk5dw3lyb26fj03wcb3izkzky") (r "1.56.1")))

(define-public crate-traitgraph-1.3.0 (c (n "traitgraph") (v "1.3.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitsequence") (r "^1.0.0") (d #t) (k 0)))) (h "0a6bsxka48m4c6drv7n59izyxi3i93sam2vszacnsncrbrjks57r") (r "1.56.1")))

(define-public crate-traitgraph-1.4.0 (c (n "traitgraph") (v "1.4.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitsequence") (r "^1.0.0") (d #t) (k 0)))) (h "1sxz2rybkcxnpila2d3nh7icsy177b1bgfllwprkidqx2mjk0lml") (r "1.56.1")))

(define-public crate-traitgraph-1.5.0 (c (n "traitgraph") (v "1.5.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitsequence") (r "^1.0.0") (d #t) (k 0)))) (h "1n1n2zgrx8fqpql6ndnkf4l0jpris8v7bgg7scnc9z6sjjaajc9p") (r "1.56.1")))

(define-public crate-traitgraph-2.0.0 (c (n "traitgraph") (v "2.0.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitsequence") (r "^1.0.0") (d #t) (k 0)))) (h "16wafpqcvy70vwrjnxn22agq759p58pym9zb5c4773hfrylxhcpi") (r "1.56.1")))

(define-public crate-traitgraph-2.1.0 (c (n "traitgraph") (v "2.1.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.0.0") (d #t) (k 0)))) (h "07ms75qi5hy3m01mrhip1vnam0dymq3bd71x9ld9zi4dwpn6knd1") (r "1.56.1")))

(define-public crate-traitgraph-2.2.0 (c (n "traitgraph") (v "2.2.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.0.0") (d #t) (k 0)))) (h "18cqr2jliarha5hfsigkk314vgcscbr6zirfbv68ia87bgl41h40") (r "1.56.1")))

(define-public crate-traitgraph-3.0.0 (c (n "traitgraph") (v "3.0.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "1jsbhm54a97jvc7jpnnb8m2vg8w0q3wlgx13az1hl8y11pwfp3pv") (r "1.65.0")))

(define-public crate-traitgraph-3.1.0 (c (n "traitgraph") (v "3.1.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "11jha82w4wigfmknc85cj593ry0174182gjig2n1pjrp2q0d3jqy") (r "1.65.0")))

(define-public crate-traitgraph-6.0.0-alpha.1 (c (n "traitgraph") (v "6.0.0-alpha.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "05m2zvakrs9yjpazc48rarmj1dl3zv18jvg5cg6dwnk3wq6f4fiv") (y #t) (r "1.65.0")))

(define-public crate-traitgraph-4.0.0-alpha.1 (c (n "traitgraph") (v "4.0.0-alpha.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "1sp55w78gqjjrx3y0zhiz6afl7fiv0v8m0p7hsd0a4sbkidg08cw") (r "1.65.0")))

(define-public crate-traitgraph-4.0.0-alpha.2 (c (n "traitgraph") (v "4.0.0-alpha.2") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "1a7wh2xib0crsn7ghsyvbc99m3sjjrcrjsmpvi0h1dlrcpfgwb2g") (r "1.65.0")))

(define-public crate-traitgraph-4.0.0-alpha.3 (c (n "traitgraph") (v "4.0.0-alpha.3") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "1bzbr5hllrn891finv5hbz0hrq653qc9mdkckv87wz5jq30p0nbf") (r "1.65.0")))

(define-public crate-traitgraph-4.0.0-alpha.4 (c (n "traitgraph") (v "4.0.0-alpha.4") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "1d6jnlfrkrsk92qwvf6mdhz0dgwfmqwjb2m55w8lkg9rsd5wyi50") (r "1.65.0")))

(define-public crate-traitgraph-4.0.0-alpha.5 (c (n "traitgraph") (v "4.0.0-alpha.5") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "0lbqbqnfhsg1h99z632pk18pypw8h8q7nv94rp3bmfakwsyyrc9s") (r "1.65.0")))

(define-public crate-traitgraph-4.0.0-alpha.6 (c (n "traitgraph") (v "4.0.0-alpha.6") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "10lv4k3595cwxrlpzh0gad9w4wkax3ij1dj086v8285wq1d8v0mm") (r "1.65.0")))

(define-public crate-traitgraph-4.0.0 (c (n "traitgraph") (v "4.0.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "08jl06djmgli5qv60gyg2cbagysb2dc1hskpjga9liv1z0wa3hdy") (r "1.65.0")))

(define-public crate-traitgraph-5.0.0 (c (n "traitgraph") (v "5.0.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "traitsequence") (r "^2.0.0") (d #t) (k 0)))) (h "1x3qb8h2iqay3v2q9wcav9wxx0r45hcnzwfg9ba7ip30m7s36sd6") (r "1.65.0")))

