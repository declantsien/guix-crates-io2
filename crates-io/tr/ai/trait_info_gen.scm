(define-module (crates-io tr ai trait_info_gen) #:use-module (crates-io))

(define-public crate-trait_info_gen-0.1.0 (c (n "trait_info_gen") (v "0.1.0") (d (list (d (n "base32ct") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0zd6s9brkv6iwd4rfk0km5q8fdjshi1maf2gwh64msdm3vb1f4qn")))

