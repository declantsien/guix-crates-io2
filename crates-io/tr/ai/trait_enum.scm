(define-module (crates-io tr ai trait_enum) #:use-module (crates-io))

(define-public crate-trait_enum-0.1.0 (c (n "trait_enum") (v "0.1.0") (h "15331s9fqgndw66q52pwsnifrk17lbq19v4bd8h2741jxva7sqmc") (f (quote (("std") ("default" "std"))))))

(define-public crate-trait_enum-0.1.1 (c (n "trait_enum") (v "0.1.1") (h "07jagp0msd5zf2br53679rpypyqyhzlkqj1v7a7dy0349g7gilb4") (f (quote (("std") ("default" "std"))))))

(define-public crate-trait_enum-0.2.0 (c (n "trait_enum") (v "0.2.0") (h "0wmigsgirll3h6bi5zxbgwnb8wn4x7wn5jxkgg2ymkvm11dm1yc2") (f (quote (("std") ("default" "std"))))))

(define-public crate-trait_enum-0.3.0 (c (n "trait_enum") (v "0.3.0") (h "1gn32imc072yw3wpl6qmh9yygck73hjyw6h054axv9h8qx2mm6d9") (f (quote (("std") ("default" "std"))))))

(define-public crate-trait_enum-0.4.0 (c (n "trait_enum") (v "0.4.0") (h "12ikndaaz9zaa5ad5b1pv8vk7xc51pcnbvplixij4vyfb3f39xjw") (f (quote (("std") ("default" "std"))))))

(define-public crate-trait_enum-0.5.0 (c (n "trait_enum") (v "0.5.0") (h "1iqg622ii9q6ajqc9n4vvfialkk2ygzsz3756787c7y7sd0xf38k") (f (quote (("std") ("default" "std"))))))

