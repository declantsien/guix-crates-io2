(define-module (crates-io tr ai trait_variable) #:use-module (crates-io))

(define-public crate-trait_variable-0.1.0 (c (n "trait_variable") (v "0.1.0") (h "0hna99sx3x9cvf0pbfc0pdx4qnq4sj9van6y0pva7lcbrr83xkgc")))

(define-public crate-trait_variable-0.2.0 (c (n "trait_variable") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "035yvpvp77h9w5qgl7wifbmmwqaqm5a6zil89jbkifw65b4p6nf1")))

(define-public crate-trait_variable-0.3.3 (c (n "trait_variable") (v "0.3.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "trait-variable-macros") (r "^0.3.3") (d #t) (k 0)))) (h "1das3adciwd3zfpkx0xm44gw8c4wx0c2lmmc3n7r6ma0315rqlqv")))

(define-public crate-trait_variable-0.3.5 (c (n "trait_variable") (v "0.3.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1digk090s45mrxg92qmxvnkp1x17fc4ybc5vghdpxscmbvkgwiy2")))

(define-public crate-trait_variable-0.4.0 (c (n "trait_variable") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15w5bs3wjzrjphnnkbywnc21fdw8b3v5hb7275cgilp88yjwn4mp")))

(define-public crate-trait_variable-0.4.1 (c (n "trait_variable") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0454272fxxvdkzsccxnsmijckn97kzb8d6haaisbrxndj8758xc3")))

(define-public crate-trait_variable-0.4.2 (c (n "trait_variable") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mfpx0fkxqvnfk6j0c3swg8ijz8pdvzmbv42jfvv4phxr60w1sz8")))

(define-public crate-trait_variable-0.4.3 (c (n "trait_variable") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vyx49aian4mppwdyf81y98zalhnxwrw9pihmgsl9s4dn0i8n31x")))

(define-public crate-trait_variable-0.5.0 (c (n "trait_variable") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "1zhxxr9p0w45x16anyhk4pyma43hwgsrkzj798xc0c343brkjxg7")))

(define-public crate-trait_variable-0.6.0 (c (n "trait_variable") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "0awpxv036k052vpbmqyf7qnz94jfnjz9kjsqihwsrf53dsrzhw0m")))

(define-public crate-trait_variable-0.7.0 (c (n "trait_variable") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit" "full" "parsing"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0knh1lm1ykpxp98m4wcvn1pndgxffbv0j6nwly5mb3aqq774damg")))

