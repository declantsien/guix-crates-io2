(define-module (crates-io tr ai trait_eval) #:use-module (crates-io))

(define-public crate-trait_eval-0.1.0 (c (n "trait_eval") (v "0.1.0") (h "1dzb20mgwv78g9wmvair588f83hzp7i7ln1gbim1f5lm2qac01q3")))

(define-public crate-trait_eval-0.1.1 (c (n "trait_eval") (v "0.1.1") (h "0aqn9cnjiz6biz00xccv5vbb1pxh23h9mg34g7rig8gx2bbkdhbj")))

(define-public crate-trait_eval-0.1.2 (c (n "trait_eval") (v "0.1.2") (h "1bmh6qkzalid8azcxzi1gn9z0gf53gdr2bzma6s3cdjjyik8apps")))

(define-public crate-trait_eval-0.1.3 (c (n "trait_eval") (v "0.1.3") (h "0jdns5fvvv4xjnm6hgib8wlxjsr5dilwgib67sla3rdw3iw6siwi")))

