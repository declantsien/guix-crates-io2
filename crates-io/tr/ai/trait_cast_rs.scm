(define-module (crates-io tr ai trait_cast_rs) #:use-module (crates-io))

(define-public crate-trait_cast_rs-0.2.0 (c (n "trait_cast_rs") (v "0.2.0") (d (list (d (n "trait_cast_impl_rs") (r "^0.2.0") (d #t) (k 0)))) (h "1yr245acx6lzk0hm81bx59wxabwks42zvma6853bv2v3phhbhgr9") (f (quote (("min_specialization") ("downcast_unchecked" "trait_cast_impl_rs/downcast_unchecked") ("default" "alloc") ("alloc"))))))

(define-public crate-trait_cast_rs-0.2.1 (c (n "trait_cast_rs") (v "0.2.1") (d (list (d (n "const_sort_rs") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "trait_cast_impl_rs") (r "^0.2.0") (d #t) (k 0)))) (h "17ip2fia320j6h45krhh6qkygrns9hs6mal15fhf6yl2n0k1i8a5") (f (quote (("min_specialization") ("downcast_unchecked") ("default" "alloc") ("alloc")))) (s 2) (e (quote (("const_sort" "dep:const_sort_rs"))))))

(define-public crate-trait_cast_rs-0.2.2 (c (n "trait_cast_rs") (v "0.2.2") (d (list (d (n "const_sort_rs") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "trait_cast_impl_rs") (r "^0.2.0") (d #t) (k 0)))) (h "1cgcd16gakyqppzmpn5qd1gg6s2fpyv1dybcn2hph60kajzm68x0") (f (quote (("min_specialization") ("downcast_unchecked") ("default" "alloc") ("alloc")))) (s 2) (e (quote (("const_sort" "dep:const_sort_rs"))))))

(define-public crate-trait_cast_rs-0.2.3 (c (n "trait_cast_rs") (v "0.2.3") (d (list (d (n "const_sort_rs") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "trait_cast_impl_rs") (r "^0.2.1") (d #t) (k 0)))) (h "1iw6xy7vnsanyiwz84yn6rykwlz6xxzr3w5g8cs4ham9p2n5x1j8") (f (quote (("min_specialization") ("downcast_unchecked") ("default" "alloc") ("alloc")))) (s 2) (e (quote (("const_sort" "dep:const_sort_rs"))))))

(define-public crate-trait_cast_rs-0.2.4 (c (n "trait_cast_rs") (v "0.2.4") (d (list (d (n "const_sort_rs") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "trait_cast_impl_rs") (r "^0.2.1") (d #t) (k 0)))) (h "1mcqfgvhif6cwqlkqxfxw2c9h875p2nq1ysblvg3rw0rmfaihcz8") (f (quote (("min_specialization") ("downcast_unchecked") ("default" "alloc") ("alloc")))) (s 2) (e (quote (("const_sort" "dep:const_sort_rs"))))))

