(define-module (crates-io tr ai trailer) #:use-module (crates-io))

(define-public crate-trailer-0.1.0 (c (n "trailer") (v "0.1.0") (h "1bmkbccxr4879yzkg1qn26xxwx78fnx4vs3zvm9959jag7hn150q")))

(define-public crate-trailer-0.1.1 (c (n "trailer") (v "0.1.1") (h "06g8g427qj983j6hiin4q0gaaixcxppg56ccfhi9a15fmpv13mq0")))

(define-public crate-trailer-0.1.2 (c (n "trailer") (v "0.1.2") (h "15yfy88f609xcrgq9ra7538gd0y1b3sk5ip23d974wj0hyykf7fn")))

