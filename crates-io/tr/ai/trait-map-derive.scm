(define-module (crates-io tr ai trait-map-derive) #:use-module (crates-io))

(define-public crate-trait-map-derive-0.1.0 (c (n "trait-map-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1pk0y9sx630xnfhiqpjq207k27pvmhyf4rbnz9c1x3a41cd4r510")))

(define-public crate-trait-map-derive-0.1.1 (c (n "trait-map-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1hrgrr1k1a9ip8ys9rdx1halmblpmj7i30wysiwsg4lkw3p2iwfc")))

(define-public crate-trait-map-derive-0.1.2 (c (n "trait-map-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0agym7ffp2cnap1906pg5j9cdwg3q6fpra1f623aqih2glmdxgdd")))

