(define-module (crates-io tr ai trail_lines) #:use-module (crates-io))

(define-public crate-trail_lines-0.1.0 (c (n "trail_lines") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "0cmcn26j35flx1m8lhw23h11rp7hfxj7avw0zw41fpmx5kcn4l25")))

