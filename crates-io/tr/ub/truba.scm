(define-module (crates-io tr ub truba) #:use-module (crates-io))

(define-public crate-truba-0.1.2 (c (n "truba") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 0)) (d (n "typemap-ors") (r "^1.0") (d #t) (k 0)))) (h "1dn9yivz5kyiw0vy2fwz4mfvyqbqaqassrfvvdbcw6x1mic17wmd")))

(define-public crate-truba-0.1.3 (c (n "truba") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 0)) (d (n "typemap-ors") (r "^1.0") (d #t) (k 0)))) (h "1h1jzym00ycqdz50h1k0znlm9sbm7ny69r37zs7lxpchs0ng3fy4")))

(define-public crate-truba-0.1.4 (c (n "truba") (v "0.1.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 0)) (d (n "typemap-ors") (r "^1.0") (d #t) (k 0)))) (h "1gs5xyfy9fbyg60dq10m9dx077mkhpm176l25glgy1gwpk096my6")))

(define-public crate-truba-0.1.5 (c (n "truba") (v "0.1.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 0)) (d (n "typemap-ors") (r "^1.0") (d #t) (k 0)))) (h "1kwdrygx9p7j1n0xvxavyxmi3ngss1r07qvm9s0wb0k3ff3z78iw")))

(define-public crate-truba-0.1.6 (c (n "truba") (v "0.1.6") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 0)) (d (n "typemap-ors") (r "^1.0") (d #t) (k 0)))) (h "0sl14qv9rfdny0l9s60k8wgc7x8y4m7azb5j91mbmwvaxnndgwpf")))

