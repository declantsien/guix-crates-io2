(define-module (crates-io tr op tropy) #:use-module (crates-io))

(define-public crate-tropy-0.1.0 (c (n "tropy") (v "0.1.0") (d (list (d (n "hsl") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0wglwhp8bsv87nwi62663gl4qsx3zi2vp6wyidxyirxqz4yx8p6c") (f (quote (("default" "binary") ("binary"))))))

(define-public crate-tropy-0.1.1 (c (n "tropy") (v "0.1.1") (d (list (d (n "hsl") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "03rdgpm4y91vgdy0wrlqvnvrbf3jjfjyvpwmshk41yy5g90hqm0q") (f (quote (("default" "binary") ("binary"))))))

(define-public crate-tropy-0.1.3 (c (n "tropy") (v "0.1.3") (d (list (d (n "hsl") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1kzljz8jrsm2k7zlhby4hjchlw9zzi5ablsigi9xpx9zxs2mb8jz") (f (quote (("default" "binary") ("binary"))))))

