(define-module (crates-io tr op troppydash_grrs) #:use-module (crates-io))

(define-public crate-troppydash_grrs-0.1.0 (c (n "troppydash_grrs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "11l3h6srwknb3w78n4plyxyvw6x27gvhy0ra3gbxs14qwzhvrakw")))

