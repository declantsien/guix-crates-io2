(define-module (crates-io tr op trope) #:use-module (crates-io))

(define-public crate-trope-0.1.0 (c (n "trope") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "config") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.0") (d #t) (k 0)))) (h "00d5p3f3y29p1cqgx9cb4ms4ia1nb1rm5skjnxd74fq93j5m0rw8")))

(define-public crate-trope-0.1.1 (c (n "trope") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "config") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.0") (d #t) (k 0)))) (h "1sfrjb5swif4scn5837rmh6kpbrjav3ysiwp1pjj965cgchcq742")))

