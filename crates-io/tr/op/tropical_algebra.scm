(define-module (crates-io tr op tropical_algebra) #:use-module (crates-io))

(define-public crate-tropical_algebra-0.0.1 (c (n "tropical_algebra") (v "0.0.1") (h "1s77fszb866cpw2y97axnwalr4vkfl3k8cjsa9ynfzidbb1bxsaj")))

(define-public crate-tropical_algebra-0.0.2 (c (n "tropical_algebra") (v "0.0.2") (h "19cbgpbch788y2hhiyki7jgs1xz6ag1y7dvxh41fc5y39asjz3k5")))

(define-public crate-tropical_algebra-0.0.3 (c (n "tropical_algebra") (v "0.0.3") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1vrnm9ygxyvy8lk2m6lrprjh1apz5xgcxbg360ddw9mihlkqz4ma")))

(define-public crate-tropical_algebra-0.0.4 (c (n "tropical_algebra") (v "0.0.4") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "00b0amms0lig3c8nh5zgvzshkhljkl78k5dhxl5y6lrg0r0fj06d")))

