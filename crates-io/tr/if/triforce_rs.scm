(define-module (crates-io tr if triforce_rs) #:use-module (crates-io))

(define-public crate-triforce_rs-0.0.1 (c (n "triforce_rs") (v "0.0.1") (d (list (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 2)))) (h "0vinnn1y50f3lf4q6f9s6dwp8491blfwkc2x1n708mb82k65y0fl")))

(define-public crate-triforce_rs-0.0.2 (c (n "triforce_rs") (v "0.0.2") (d (list (d (n "diesel") (r "^1.4") (f (quote ("postgres"))) (d #t) (k 0)))) (h "05rz1wam63pi0kh6slf5xqhn26rywd7zm4q1x04j6zvwi5pys0pw")))

