(define-module (crates-io tr if trifid_api_migration) #:use-module (crates-io))

(define-public crate-trifid_api_migration-0.1.0 (c (n "trifid_api_migration") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.11.0") (f (quote ("sqlx-postgres" "runtime-actix-rustls"))) (d #t) (k 0)))) (h "0hlmkjs8qlhrdnrr201sx0iylrj36d1rc0jwq7s17lf4577kjgcx")))

(define-public crate-trifid_api_migration-0.1.1 (c (n "trifid_api_migration") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.12") (f (quote ("sqlx-postgres" "runtime-actix-rustls"))) (d #t) (k 0)))) (h "0884154p4anrim8p862vzz2rm4amv86f0n6rg30dnagnis2lj4vz")))

(define-public crate-trifid_api_migration-0.2.0 (c (n "trifid_api_migration") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.12") (f (quote ("sqlx-postgres" "runtime-actix-rustls"))) (d #t) (k 0)))) (h "0wydwjva98s1s6vwyld8jh4w1jgvp894r1rxhbxsl6mx1v2ynb7b")))

