(define-module (crates-io tr if trifid_api_entities) #:use-module (crates-io))

(define-public crate-trifid_api_entities-0.1.0 (c (n "trifid_api_entities") (v "0.1.0") (d (list (d (n "sea-orm") (r "^0") (d #t) (k 0)))) (h "1f2pf4fmwx217y7bx0zl4zrz0yj0rnk1sy8dhp3z2hdnwq1a1w49")))

(define-public crate-trifid_api_entities-0.1.1 (c (n "trifid_api_entities") (v "0.1.1") (d (list (d (n "sea-orm") (r "^0.12") (d #t) (k 0)))) (h "1y0g40ar59bqkjgcg4gwh9xq6fyya7cbi1fg4352zabf8bm0ncgj")))

(define-public crate-trifid_api_entities-0.2.0 (c (n "trifid_api_entities") (v "0.2.0") (d (list (d (n "sea-orm") (r "^0.12") (d #t) (k 0)))) (h "02nbv9vrkdzy0n10xi6lsiz8nb20463b4hx4wgacag9k39s47shj")))

