(define-module (crates-io tr an transparent_proxy) #:use-module (crates-io))

(define-public crate-transparent_proxy-0.1.0 (c (n "transparent_proxy") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qf6ygd7mm0r6x9candwbzx5ghhggivhgjkq1dlqsy748gxn0swq")))

(define-public crate-transparent_proxy-0.2.0 (c (n "transparent_proxy") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ilnsq6h8qpwxlb7z1hswrkh12bxkvwsplivfbbw4h43h1z3snmn")))

(define-public crate-transparent_proxy-0.3.0 (c (n "transparent_proxy") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt" "macros" "net" "rt-multi-thread" "io-util"))) (d #t) (k 0)))) (h "1n8py9casx0ryi7sbkkfq54jpdfn17czk3phpmfmi01njc6x3330") (f (quote (("socks5") ("http" "base64") ("default" "http" "socks5"))))))

(define-public crate-transparent_proxy-0.4.0 (c (n "transparent_proxy") (v "0.4.0") (d (list (d (n "base64") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("rt" "macros" "net" "rt-multi-thread" "io-util"))) (d #t) (k 0)))) (h "17scmiy4gva52f69wc17ccvhy536vxiv5szac76s57rqjqfyh0p6") (f (quote (("socks5") ("http" "base64") ("default" "http" "socks5"))))))

