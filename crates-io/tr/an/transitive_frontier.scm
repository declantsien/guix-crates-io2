(define-module (crates-io tr an transitive_frontier) #:use-module (crates-io))

(define-public crate-transitive_frontier-0.1.0 (c (n "transitive_frontier") (v "0.1.0") (d (list (d (n "guppy") (r "^0.6.2") (d #t) (k 0)) (d (n "horrorshow") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "19ydrskhcaks6y8gg8hng7khg6j6a9mrvxfjcwy3mprxlr1fdp92")))

(define-public crate-transitive_frontier-0.1.1 (c (n "transitive_frontier") (v "0.1.1") (d (list (d (n "guppy") (r "^0.6.2") (d #t) (k 0)) (d (n "horrorshow") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1kvhb54i6g5z5vnvdg3w0f9aa3yxd2zz6sl73hqaskzfihi8dk9z")))

