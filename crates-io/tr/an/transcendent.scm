(define-module (crates-io tr an transcendent) #:use-module (crates-io))

(define-public crate-transcendent-0.1.0 (c (n "transcendent") (v "0.1.0") (h "0advxn6v9r6gylvgybxc7z49m2hnyw91bdd66b78bzv3mn4s1vsb")))

(define-public crate-transcendent-0.2.0 (c (n "transcendent") (v "0.2.0") (h "1nhcns81h2y1cad6wv87ygx40l3hp6yhx4igihbwzyijvifz9v0m")))

(define-public crate-transcendent-0.3.0 (c (n "transcendent") (v "0.3.0") (d (list (d (n "darth-rust") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07d64qmb35vx6ggfkhhw4fbnnwdaayx9di6dcq7af6vgakx43mqa") (f (quote (("zeus") ("poseidon") ("hermes") ("hades") ("default" "zeus") ("athena"))))))

(define-public crate-transcendent-0.3.2 (c (n "transcendent") (v "0.3.2") (d (list (d (n "darth-rust") (r "^4.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g3z397bv8l0k7g5bws0211yghbzjj1zrwpkvwicrqnc04fp4fm4") (f (quote (("zeus") ("poseidon") ("hermes") ("hades") ("default" "hermes") ("athena"))))))

