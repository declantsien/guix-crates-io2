(define-module (crates-io tr an transitive) #:use-module (crates-io))

(define-public crate-transitive-0.1.0 (c (n "transitive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "10r4q54jzdgmw0i3kc0b30fpsvj66ncn7mfdgvbl34f3v1w1lrgy") (y #t)))

(define-public crate-transitive-0.1.1 (c (n "transitive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "10jv0dy51iy5fpaj4pmjflwsdb66vsq04dk79f7fam8x06nxfbkw")))

(define-public crate-transitive-0.2.0 (c (n "transitive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1ixm0wscx9hyd58agvgnq121zcpycv84c21y3l10q0dsblayjgw7")))

(define-public crate-transitive-0.3.0 (c (n "transitive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "13bsdr301j1dg6hzk4vivbvf1yvnw2xc6dxslsrdwcyli26h34yg")))

(define-public crate-transitive-0.4.0 (c (n "transitive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "17cv3d7lzw096pljgax689f1yassx1264cl1p7ilxwlc8d6fz0y5") (y #t)))

(define-public crate-transitive-0.4.1 (c (n "transitive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0ddly5nlyfn45yyw18wn0wp9xcqlg81q30dxdbvjip9809zyhp0j")))

(define-public crate-transitive-0.4.2 (c (n "transitive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1p2ffm7lwdq047l5pniqcy3f6hxhmc2v3niz1870mbmq8fd1vga9")))

(define-public crate-transitive-0.4.3 (c (n "transitive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0mx7dnbbhypxy5sjd1xxfhhzr5fahbvq7j6qckz12p3h57mik9jv")))

(define-public crate-transitive-0.5.0 (c (n "transitive") (v "0.5.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0mzc08sgal3qlhxx7vnpznywsz26g5yp3ipcpawfsvqj85j727p3")))

(define-public crate-transitive-1.0.0 (c (n "transitive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1hf654fc832hm9d1q6q53qclqsfns2lz9h3ddw4g10wp8x661wm1")))

(define-public crate-transitive-1.0.1 (c (n "transitive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1d152jya0iwkjj0z791hp2lg5j3q4p30awncs8bm5x68mq6ri9nf")))

