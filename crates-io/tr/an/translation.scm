(define-module (crates-io tr an translation) #:use-module (crates-io))

(define-public crate-translation-1.0.0 (c (n "translation") (v "1.0.0") (d (list (d (n "gettext") (r "^0.4") (d #t) (k 0)) (d (n "rust-embed") (r "^5") (d #t) (k 0)) (d (n "tr") (r "^0.1") (f (quote ("gettext"))) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnls"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0f17na9djx0yy207piyb5irw7bpy71h0kqspd872vv8dd3yb9jj3")))

(define-public crate-translation-1.0.1 (c (n "translation") (v "1.0.1") (d (list (d (n "gettext") (r "^0.4") (d #t) (k 0)) (d (n "rust-embed") (r "^5") (d #t) (k 0)) (d (n "tr") (r "^0.1") (f (quote ("gettext"))) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnls"))) (d #t) (t "cfg(windows)") (k 0)))) (h "14za5q5kprqmrkbir496bsys8jawiclrrd5iv4f5rj2zcqqmky1c")))

(define-public crate-translation-1.0.2 (c (n "translation") (v "1.0.2") (d (list (d (n "gettext") (r "^0.4") (d #t) (k 0)) (d (n "rust-embed") (r "^5") (d #t) (k 0)) (d (n "tr") (r "^0.1") (f (quote ("gettext"))) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnls"))) (d #t) (t "cfg(windows)") (k 0)))) (h "16ykl1il6wj769akpp33ysyjnar45s8i8179b24dbpxd27z9giyj")))

