(define-module (crates-io tr an trans-schema-derive) #:use-module (crates-io))

(define-public crate-trans-schema-derive-0.1.0 (c (n "trans-schema-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yfamzahrxpvqdiag6vidj57kp1r840bg9w8l9hsqqdm1znhkhr3")))

(define-public crate-trans-schema-derive-0.1.1 (c (n "trans-schema-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jzc22f4sjhx8qqh8s2w9phsyjhak8blcngbcv1kyqc0zh4icpal")))

(define-public crate-trans-schema-derive-0.1.2 (c (n "trans-schema-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nwc9krs8vb8c5gxby1scpci3n5504mjkby4zmf2qjs4bi4saqbs")))

(define-public crate-trans-schema-derive-0.3.0-alpha.0 (c (n "trans-schema-derive") (v "0.3.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0s0i8avwhakv23q1jmbvxs9hfaxn3y7wjd31ad377xja4j63zk3b")))

(define-public crate-trans-schema-derive-0.3.0-alpha.1 (c (n "trans-schema-derive") (v "0.3.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wpp4x0q7kpy7k0ksrii0krmji4k7p21nfygflbbggy4jxmyf7f8")))

