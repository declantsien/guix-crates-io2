(define-module (crates-io tr an transcript) #:use-module (crates-io))

(define-public crate-transcript-0.1.0 (c (n "transcript") (v "0.1.0") (h "19znd0xb6p8mav1vifzlh6jbvxmvrdiyxlhpz10k9pdwqkxlvm44")))

(define-public crate-transcript-0.1.1 (c (n "transcript") (v "0.1.1") (h "16s24l2xz59aw648yql8nx50rc65ddmzkfwn6ww2nc5633xaf18c")))

(define-public crate-transcript-0.1.11 (c (n "transcript") (v "0.1.11") (h "1y9d62qzqnnjsz4ai9wfhmcllvn7bparf4a8iidrg0622rw710p4")))

(define-public crate-transcript-0.1.12 (c (n "transcript") (v "0.1.12") (h "15rddwhg7xlyxr714pd7iimya8sw0q2dxw3zhq7i4i8d2chplfcg")))

