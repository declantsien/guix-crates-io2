(define-module (crates-io tr an transaction-processor) #:use-module (crates-io))

(define-public crate-transaction-processor-0.0.0 (c (n "transaction-processor") (v "0.0.0") (h "0650xp67xbfpdibzdpi94yyi5jq55m0lid37y6c81vz6bhvlmkkx")))

(define-public crate-transaction-processor-0.0.1 (c (n "transaction-processor") (v "0.0.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0hnjwg1xyyypz8jnijxmwd06riim3wyaksh8g14h50zfd04jjrpp")))

