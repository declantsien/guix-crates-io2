(define-module (crates-io tr an transvoxel-data) #:use-module (crates-io))

(define-public crate-transvoxel-data-0.1.1 (c (n "transvoxel-data") (v "0.1.1") (h "17dml1cq5bbjqa7rgcv7c65ar0pprznrb0p42hg4167w1b37qbv9")))

(define-public crate-transvoxel-data-0.1.2 (c (n "transvoxel-data") (v "0.1.2") (h "0va580cdpzx5ycq6bc5xbvn3mslqr3iq9v9s0cjlhyd62zp3kmcq")))

(define-public crate-transvoxel-data-0.2.0 (c (n "transvoxel-data") (v "0.2.0") (h "04zawhc5nk61xgdnl0cxsmp6vdmsiy809vhfwbwbn7ab5g230678")))

(define-public crate-transvoxel-data-0.2.1 (c (n "transvoxel-data") (v "0.2.1") (h "0d1scddfsj97qgqdlfivclgxsg5p031sdpnjjil8mcgqfz4svj1i")))

