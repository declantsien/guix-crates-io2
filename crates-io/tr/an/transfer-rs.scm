(define-module (crates-io tr an transfer-rs) #:use-module (crates-io))

(define-public crate-transfer-rs-0.3.0 (c (n "transfer-rs") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)))) (h "1skx6k0mmk4bakwbpy8j7z8z04ghjrci4x5xgmy6vskkayp0hcd8") (f (quote (("build-binary" "clap")))) (y #t)))

