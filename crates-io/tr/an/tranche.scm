(define-module (crates-io tr an tranche) #:use-module (crates-io))

(define-public crate-tranche-0.1.0 (c (n "tranche") (v "0.1.0") (h "0099dv9k9fs7r17ibv3yi6af787vma8gn8bdnip1v7bnsz9hgm6l") (f (quote (("std"))))))

(define-public crate-tranche-0.1.1 (c (n "tranche") (v "0.1.1") (d (list (d (n "passive") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0gl51nkn3p8rg46454blb2cpr8qc0h3c93gkaw26p6ssg2w0zljg") (f (quote (("std"))))))

