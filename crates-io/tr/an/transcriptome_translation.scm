(define-module (crates-io tr an transcriptome_translation) #:use-module (crates-io))

(define-public crate-transcriptome_translation-0.1.0 (c (n "transcriptome_translation") (v "0.1.0") (d (list (d (n "memmap") (r "^0.2.3") (d #t) (k 0)) (d (n "nom") (r "~1.2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.12") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.12") (d #t) (k 0)) (d (n "radix_trie") (r "^0.0.8") (d #t) (k 0)))) (h "0b0w6xd4i6lwrfd0kkymsb1nfvbxcmi8l2n5j3rix60yxw0mxcxg") (y #t)))

