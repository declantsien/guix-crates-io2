(define-module (crates-io tr an transportations_library) #:use-module (crates-io))

(define-public crate-transportations_library-0.1.0 (c (n "transportations_library") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "round") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s6w6sck2ydg6m0mc7qzipcc7nlscsbbzbabpg93qg8inkcjsvay")))

