(define-module (crates-io tr an transporter) #:use-module (crates-io))

(define-public crate-transporter-0.1.0 (c (n "transporter") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "typed-builder") (r "^0.6") (d #t) (k 0)))) (h "1qn2776vnirpi7k6dj5fd5c7nhqfxcfmv5v4832jv8mhynqk8x5g")))

(define-public crate-transporter-0.1.1 (c (n "transporter") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "typed-builder") (r "^0.6") (d #t) (k 0)))) (h "1nbd67a8f6ljxi8qbhdrwxw811jpf0rxd4zxgivm94c438m31ywp")))

