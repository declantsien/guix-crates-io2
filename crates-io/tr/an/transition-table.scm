(define-module (crates-io tr an transition-table) #:use-module (crates-io))

(define-public crate-transition-table-0.0.1 (c (n "transition-table") (v "0.0.1") (h "11lkwngji2hnkippa76wp4hdnjj3z5hl3q4rvpk3zzby46rhfkdq")))

(define-public crate-transition-table-0.0.2 (c (n "transition-table") (v "0.0.2") (h "04invwx9209vqpxhm0srpkdf9ihvdk70hs379rh5jjw35n304lc6")))

(define-public crate-transition-table-0.0.3 (c (n "transition-table") (v "0.0.3") (h "0p55nv6kbv7dj05cxj2i28i76ixkzyifixhnz9pbi84gmii3h5if")))

