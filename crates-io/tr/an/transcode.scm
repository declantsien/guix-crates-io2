(define-module (crates-io tr an transcode) #:use-module (crates-io))

(define-public crate-transcode-0.0.1 (c (n "transcode") (v "0.0.1") (d (list (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0zkayv1ik91xp3j4z25if1sdfcwjs69y36yc843nhl7b6ddggvfq")))

(define-public crate-transcode-0.1.0 (c (n "transcode") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1yjmi2aa62asffi8pp2wn31vl9vq05ckgcp1d7q3gn3m6bp5pli6")))

(define-public crate-transcode-0.3.0 (c (n "transcode") (v "0.3.0") (d (list (d (n "getopts") (r ">= 0.2") (d #t) (k 0)) (d (n "regex") (r ">= 0.1.77") (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 0)))) (h "0pglfqaz4qda6jk2ijncnmlzr1wnx6n40m6xwlmb0m72j3wqp72y")))

