(define-module (crates-io tr an transcript-trait) #:use-module (crates-io))

(define-public crate-transcript-trait-0.1.0 (c (n "transcript-trait") (v "0.1.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "merlin") (r "^3") (o #t) (d #t) (k 0)))) (h "1hjkds5zfdpnj5amn743zah6794qjv1hypamzy1vrvfl3j2hv3s3") (y #t) (s 2) (e (quote (("merlin" "dep:merlin"))))))

(define-public crate-transcript-trait-0.1.1 (c (n "transcript-trait") (v "0.1.1") (d (list (d (n "blake2") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "merlin") (r "^3") (o #t) (d #t) (k 0)))) (h "1yvsrfqkjd83bw3pkc3wqw2ypzfahgxsi4g4cb8sdcki0rpfhqqs") (f (quote (("recommended" "blake2")))) (y #t) (s 2) (e (quote (("merlin" "dep:merlin"))))))

