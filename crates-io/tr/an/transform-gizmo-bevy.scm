(define-module (crates-io tr an transform-gizmo-bevy) #:use-module (crates-io))

(define-public crate-transform-gizmo-bevy-0.1.0 (c (n "transform-gizmo-bevy") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_math") (r "^0.13") (f (quote ("mint"))) (d #t) (k 0)) (d (n "transform-gizmo") (r "^0.1.0") (d #t) (k 0)))) (h "1j72hzf720llgxqmk277f9wb9zxmyp38dbmv2phi1y0mdi9a1814") (r "1.77.0")))

