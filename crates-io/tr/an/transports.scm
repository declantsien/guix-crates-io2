(define-module (crates-io tr an transports) #:use-module (crates-io))

(define-public crate-transports-0.1.2 (c (n "transports") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1qxjmx676i1a4aygr4nq652s5h9ygxykcj22hw2m7xf6zrl2a22q")))

