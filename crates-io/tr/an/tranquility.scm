(define-module (crates-io tr an tranquility) #:use-module (crates-io))

(define-public crate-tranquility-0.0.1 (c (n "tranquility") (v "0.0.1") (h "1kx9xcz0k536dkzdjg4m0fv9ja5q6wq8vvd777zcx4ncsngw5ns7")))

(define-public crate-tranquility-0.0.2 (c (n "tranquility") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "macros"))) (d #t) (k 0)))) (h "1f20q8rcg0a8bl8l507kq10cxii5l4gq56ilmm5nfqpzdr7lamx3")))

(define-public crate-tranquility-0.0.3 (c (n "tranquility") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "macros"))) (d #t) (k 0)))) (h "0c8hz3q7nhkpzjg2h3l56b307gz7k1gzwxyb2k88y0swl2v8z0m9")))

(define-public crate-tranquility-0.0.4 (c (n "tranquility") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00cay8mpgn2mwlah6a0g2w88pbcia80qwym3ldd2g24jd8mddp77")))

(define-public crate-tranquility-0.0.5 (c (n "tranquility") (v "0.0.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i8x9369ba71a32hx6pa2hjh7dl36djcx0ingg6mk1b8yyik29jv")))

(define-public crate-tranquility-0.0.6 (c (n "tranquility") (v "0.0.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11cvw5q3ajrkqw29arnqp79qaxq0457g4q01jgkvqd59cymhn6ls")))

(define-public crate-tranquility-0.0.7 (c (n "tranquility") (v "0.0.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i9zlqn6v0fy3jv9034pinma9ldnw41kgrc6vdawkpd3q7p5d8w1")))

(define-public crate-tranquility-0.0.8 (c (n "tranquility") (v "0.0.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07l746sl2n7496sn239lvvay3vwypcykn2in087ydmwb6q4mg8fp")))

