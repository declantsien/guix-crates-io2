(define-module (crates-io tr an transliterate1234) #:use-module (crates-io))

(define-public crate-transliterate1234-0.1.0 (c (n "transliterate1234") (v "0.1.0") (h "0c247q2zn38jysisdjxwdhs2pxra9v90gcsk74zydf17sa5967i5")))

(define-public crate-transliterate1234-0.1.1 (c (n "transliterate1234") (v "0.1.1") (h "1mvvl6r67213bclxnwxys09v8vh4rjkwyv0qbjn9i2fy7zngx4vl")))

