(define-module (crates-io tr an transmog-bincode) #:use-module (crates-io))

(define-public crate-transmog-bincode-0.1.0-dev.1 (c (n "transmog-bincode") (v "0.1.0-dev.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.1") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.1") (f (quote ("test-util"))) (d #t) (k 2)))) (h "1g77jiy3wailvrhrhbg7zas4278pzg4f7aq46k82bc1ap9a15hqr")))

(define-public crate-transmog-bincode-0.1.0-dev.2 (c (n "transmog-bincode") (v "0.1.0-dev.2") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.2") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.2") (f (quote ("test-util"))) (d #t) (k 2)))) (h "15w0b4f5ncq3hi0lp108g73pxl2xmjykxsdgxb7rmkvbqgi5zn9d")))

(define-public crate-transmog-bincode-0.1.0 (c (n "transmog-bincode") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (f (quote ("test-util"))) (d #t) (k 2)))) (h "1kmrvns54ns1plhk4ham2n5mh6l2mnh0wlal7flazjlhz8vnvzlz")))

