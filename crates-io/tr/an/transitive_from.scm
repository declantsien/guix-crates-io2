(define-module (crates-io tr an transitive_from) #:use-module (crates-io))

(define-public crate-transitive_from-0.1.0 (c (n "transitive_from") (v "0.1.0") (h "0cpn9z773w8gjccdbq9jir93gav2hnab9qlgi23zw3rwvwc6j1xn")))

(define-public crate-transitive_from-0.1.1 (c (n "transitive_from") (v "0.1.1") (h "0qzkby29679pvfz9q5z1b50nifnxmjdlpqx1gpdbpj0r6dcbaz9d")))

(define-public crate-transitive_from-0.1.2 (c (n "transitive_from") (v "0.1.2") (h "1d5b5dbz10bm3n0wq1jca64m9djni0mqcnznmyl78f7l3j1zyd3w")))

(define-public crate-transitive_from-0.1.3 (c (n "transitive_from") (v "0.1.3") (h "0pvikrv8yx91jc3mkamzlrv85xz9wddg6w8w05d9cmcfj4nbqcx0")))

