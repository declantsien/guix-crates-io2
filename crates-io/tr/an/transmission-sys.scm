(define-module (crates-io tr an transmission-sys) #:use-module (crates-io))

(define-public crate-transmission-sys-0.1.0 (c (n "transmission-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)))) (h "03ddrp4q2r1402wrqsnqsa4zr0dncqx813k9wqrw2cgmrdbf4jif")))

(define-public crate-transmission-sys-0.1.1 (c (n "transmission-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)))) (h "0dv1wxx5rv48ry91jj262xwjr0drnia0df132ivwkrp3lrwdxi39")))

(define-public crate-transmission-sys-0.1.2 (c (n "transmission-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)))) (h "0zm82x2l2wf90w9fwzm7b3wb58wqkapwjdya2x3cb47k9z7k96yj")))

(define-public crate-transmission-sys-0.1.3 (c (n "transmission-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)))) (h "0690x0i4vq6hc97q1fxb1sg49b4cyjxb5bdjn821qsp50wh6mbjn")))

(define-public crate-transmission-sys-0.1.4 (c (n "transmission-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0lxj17kb1mbaw4x8c74lkal43c5pv35sm7n5hx8qgcky6xh0m9qp") (y #t)))

(define-public crate-transmission-sys-0.1.5 (c (n "transmission-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1zphf9sqp1ix4fziwdrd4azswll93m3wirsm7jcv2lvp0j85l9hg")))

(define-public crate-transmission-sys-0.2.0 (c (n "transmission-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "libevent-sys") (r "^0.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "1szbjl7w1j1yhsg7cjr02hbjvxy788qcbckdr2hhl2vg9s388ljy")))

(define-public crate-transmission-sys-0.2.1 (c (n "transmission-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "libevent-sys") (r "^0.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "1sr54rfvfgni8qlmkm59n71ayb57gd2q41pm9j0yr5bj1nvav7g9") (f (quote (("docs-only"))))))

(define-public crate-transmission-sys-0.2.2 (c (n "transmission-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "libevent-sys") (r "^0.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "19115qaq7q673ifvq15qlh3cwzf1dnmldbdl0fkms9vn1qyjh3pk") (f (quote (("docs-only"))))))

(define-public crate-transmission-sys-0.3.0 (c (n "transmission-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "libevent-sys") (r "^0.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "0n6s854nimk4vy6k8rdysxc9gxgv6qq22kiq2x83yygwqiwq4y6d") (f (quote (("static") ("docs-only"))))))

(define-public crate-transmission-sys-0.3.1 (c (n "transmission-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "libevent-sys") (r "^0.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "0gmjhwbprqp4w211adwwxg22bqq1vd741a179ddx8s8dbqlbr8nh") (f (quote (("static") ("docs-only"))))))

(define-public crate-transmission-sys-0.3.2 (c (n "transmission-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "libevent-sys") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "1npqcdm515gdc4rd5s4yrmfqqw9cfda3vbxb37gqczmxpm942ban") (f (quote (("static") ("docs-only"))))))

