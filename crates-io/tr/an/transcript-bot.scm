(define-module (crates-io tr an transcript-bot) #:use-module (crates-io))

(define-public crate-transcript-bot-0.1.0 (c (n "transcript-bot") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "openai-api-fork") (r "^0.2.1") (d #t) (k 0)) (d (n "pw-telegram-bot-fork") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("stream" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 0)))) (h "15f8k5qqzb0k1nh2f8hv8zba6dyrk72mkpsx21mv0k5zazcfbv1b")))

