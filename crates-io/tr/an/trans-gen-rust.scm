(define-module (crates-io tr an trans-gen-rust) #:use-module (crates-io))

(define-public crate-trans-gen-rust-0.1.0 (c (n "trans-gen-rust") (v "0.1.0") (d (list (d (n "include_dir") (r "^0.2") (d #t) (k 0)) (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "0ia1cagqj891kv3m8yzghc2kfmkn5qxz67lbg2lbs7mzzvm1778w")))

(define-public crate-trans-gen-rust-0.1.1 (c (n "trans-gen-rust") (v "0.1.1") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "1rryqdg8dp951axrk93zlbalrg4w1xa4nr5a95xblkbf4ra22va0")))

(define-public crate-trans-gen-rust-0.3.0-alpha.0 (c (n "trans-gen-rust") (v "0.3.0-alpha.0") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1ihblh98gz8fl55h3xsr5nz4imphihf80l0v0rzn2kf2661pw6nz")))

(define-public crate-trans-gen-rust-0.3.0-alpha.1 (c (n "trans-gen-rust") (v "0.3.0-alpha.1") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "169pgcsi6wix2hiifvgr4p12ajw64b94w423qpdzprhhq0brbjmc")))

(define-public crate-trans-gen-rust-0.3.0-alpha.2 (c (n "trans-gen-rust") (v "0.3.0-alpha.2") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1xfbq1vrvbz9ng56662b469ywnb500q2n7h72k2mcv0jn3h6kapf")))

