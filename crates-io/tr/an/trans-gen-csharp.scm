(define-module (crates-io tr an trans-gen-csharp) #:use-module (crates-io))

(define-public crate-trans-gen-csharp-0.1.0 (c (n "trans-gen-csharp") (v "0.1.0") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "0m4v0jm2dbng7s513pn5ykls0hn5x2xni7wrc3krqvfbwyqrs6y3")))

(define-public crate-trans-gen-csharp-0.3.0-alpha.0 (c (n "trans-gen-csharp") (v "0.3.0-alpha.0") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1hyzgyy2b2c0282fznd7wkkqd5kn7ms4zgh5j61gqdbw4xyrcwh9")))

(define-public crate-trans-gen-csharp-0.3.0-alpha.1 (c (n "trans-gen-csharp") (v "0.3.0-alpha.1") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "196s31x0y9x4pgvyq7rsizaavahl0s15zkf8k3p5xh3wpmz1m2r5")))

(define-public crate-trans-gen-csharp-0.3.0-alpha.2 (c (n "trans-gen-csharp") (v "0.3.0-alpha.2") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "06hrrzzxpd72h916vhiz40rkgn6c21nxrw2s4l0bminc4mzfnrlp")))

