(define-module (crates-io tr an transmog-pot) #:use-module (crates-io))

(define-public crate-transmog-pot-0.1.0-dev.1 (c (n "transmog-pot") (v "0.1.0-dev.1") (d (list (d (n "pot") (r "^1.0.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.1") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.1") (f (quote ("test-util"))) (d #t) (k 2)))) (h "0i1rp0y33c3gjr7qqy43qb4cvw74ar0khv74f0i7m4f6cz2izhlk")))

(define-public crate-transmog-pot-0.1.0-dev.2 (c (n "transmog-pot") (v "0.1.0-dev.2") (d (list (d (n "pot") (r "^1.0.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.2") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.2") (f (quote ("test-util"))) (d #t) (k 2)))) (h "166scnr5cz8xnjsz4s3lckr1qfgihdw6fnx3ikgsxs8banmlsrpc")))

(define-public crate-transmog-pot-0.1.0 (c (n "transmog-pot") (v "0.1.0") (d (list (d (n "pot") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (f (quote ("test-util"))) (d #t) (k 2)))) (h "0j5cfc17292szhhxazc4x1ry6wawdwqh4svw44xd608q84is065y")))

(define-public crate-transmog-pot-0.2.0 (c (n "transmog-pot") (v "0.2.0") (d (list (d (n "pot") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (f (quote ("test-util"))) (d #t) (k 2)))) (h "134qdqj9abjxjmbvgjnjyw3ha3lqzafdmxnqm227ac786crmbfw3")))

(define-public crate-transmog-pot-0.3.0 (c (n "transmog-pot") (v "0.3.0") (d (list (d (n "pot") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (f (quote ("test-util"))) (d #t) (k 2)))) (h "0kli3zkl6fvk4adklzpk7x7ql2bk6bif0p2vikbhzyikkgyzaxzp")))

