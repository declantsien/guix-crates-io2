(define-module (crates-io tr an transient-hashmap) #:use-module (crates-io))

(define-public crate-transient-hashmap-0.1.0 (c (n "transient-hashmap") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1l3k0fr5kajfxamxpbbpr9ala44jvb2gixh8xnhxqbhq2rqwrxqm")))

(define-public crate-transient-hashmap-0.3.0 (c (n "transient-hashmap") (v "0.3.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0r0jpcal2w167a22dpbcawd4psr905zw14zvmaj0jldcvqmz9f88")))

(define-public crate-transient-hashmap-0.4.0 (c (n "transient-hashmap") (v "0.4.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0ky2g5nyb4zhijkmnzrxn31dxvf8ldpmxsnkkaky26w1y3458lki")))

(define-public crate-transient-hashmap-0.4.1 (c (n "transient-hashmap") (v "0.4.1") (h "0gj3vjj9vhhjqf8ac3xkpk7bd44prz6khairrvgmx8rks28v3d5f")))

