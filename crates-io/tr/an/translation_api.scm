(define-module (crates-io tr an translation_api) #:use-module (crates-io))

(define-public crate-translation_api-0.1.0 (c (n "translation_api") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "ecb") (r "^0.1.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_user_agent") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0shhbq0adx85gcy2ymjwv32pn0l0cnlc368y6223g5vk0jcl469z")))

