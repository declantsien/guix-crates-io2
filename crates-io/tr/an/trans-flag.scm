(define-module (crates-io tr an trans-flag) #:use-module (crates-io))

(define-public crate-trans-flag-0.1.0 (c (n "trans-flag") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1ii8bvljvfwjp8dgr0hbkjxq9b1wp290pxqsv95mlq1qwcxz96z3")))

