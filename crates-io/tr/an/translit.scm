(define-module (crates-io tr an translit) #:use-module (crates-io))

(define-public crate-translit-0.1.0 (c (n "translit") (v "0.1.0") (h "1qglcz2hhmfdzygxyhbq88ygrh8yf7y2ydw38pxdvrs453bg0znw")))

(define-public crate-translit-0.2.0 (c (n "translit") (v "0.2.0") (h "0qh9lvs2rri4fbdhl2f6x7cj7z3886rgzynnxyrjhvfk8sdcklxq")))

(define-public crate-translit-0.2.1 (c (n "translit") (v "0.2.1") (h "15jla1qi85v8lpbrwx15ic4la8qzf2i47bl83aak3lxjzfjw78s1")))

(define-public crate-translit-0.3.0 (c (n "translit") (v "0.3.0") (h "1prhlxcjrz60nai54z3sljlv7fqqsrm1w60bafrn6g8sjhcw5lvf")))

(define-public crate-translit-0.4.0 (c (n "translit") (v "0.4.0") (h "16hjbmcifamwilb6bf5jr5xhihsx3bbm732h209pi86jzp2c2nh4")))

(define-public crate-translit-0.4.1 (c (n "translit") (v "0.4.1") (h "1xglm3cmwaw3crsx633q2blcmy60kzkklbcjf85xbkinqxcgpvjy")))

(define-public crate-translit-0.5.0 (c (n "translit") (v "0.5.0") (h "0qqhlxhgwv1wfjxd2hqiqd8d1yflgdnxkkyvlrci32jvs5y0l7p2")))

