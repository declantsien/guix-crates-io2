(define-module (crates-io tr an trans-gen-java) #:use-module (crates-io))

(define-public crate-trans-gen-java-0.1.0 (c (n "trans-gen-java") (v "0.1.0") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "1p7y78xw3s2kcajwwq5w0lq9zgb8iscgb76851zkp1lj54jbgdvc")))

(define-public crate-trans-gen-java-0.3.0-alpha.0 (c (n "trans-gen-java") (v "0.3.0-alpha.0") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0392rzhgd58snvdxbgphsgdasgb7im972fhxq7s84f8g7ykwxmm4")))

(define-public crate-trans-gen-java-0.3.0-alpha.1 (c (n "trans-gen-java") (v "0.3.0-alpha.1") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1fkzpkidm31mp8fiv39sjs7g9imidcf1lx0h46fm4rbs8sz27laf")))

(define-public crate-trans-gen-java-0.3.0-alpha.2 (c (n "trans-gen-java") (v "0.3.0-alpha.2") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1xbwdi3zzcdb3nzlps3b0qjmfbzv86flppfpr2wxq46g7a81f1rp")))

