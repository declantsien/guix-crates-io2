(define-module (crates-io tr an transit_model_collection) #:use-module (crates-io))

(define-public crate-transit_model_collection-0.1.0 (c (n "transit_model_collection") (v "0.1.0") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gpcc0h6scdh2jfms0r97g8m3ijgiyxqc141qy2kxb9zd570wmv5")))

(define-public crate-transit_model_collection-0.2.0 (c (n "transit_model_collection") (v "0.2.0") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "081sgpnpkn5ppcwmzgq8imaa29ynqzzk0sbg4n4689qbj9lqpxyy") (y #t)))

(define-public crate-transit_model_collection-0.1.1 (c (n "transit_model_collection") (v "0.1.1") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y2vfnnr0zxgh8a1wzxnbnlvbpa7cwizlqcxy42wr461bjqcbv48")))

(define-public crate-transit_model_collection-0.1.2 (c (n "transit_model_collection") (v "0.1.2") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17ahpzqydycp1gxcqg6zc4514cp1lbn19az8m4587ik6iaz19sw5")))

(define-public crate-transit_model_collection-0.1.3 (c (n "transit_model_collection") (v "0.1.3") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fzshqmnkfgqkzppcl7bn2d1dfd392319jzaj017mwvf73d2hvxa")))

(define-public crate-transit_model_collection-0.1.4 (c (n "transit_model_collection") (v "0.1.4") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j1jszlb6b2rph3byxn10dzxxvk65dl3nvafmk9mkj9vdd7jkbbk")))

