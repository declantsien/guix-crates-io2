(define-module (crates-io tr an transparentlog_core) #:use-module (crates-io))

(define-public crate-transparentlog_core-0.0.1 (c (n "transparentlog_core") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "maybe-owned") (r "^0.3.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "110vdjxsg1c5903k81yfja1kpgy0fs0i4qiamxqyds49qr90lzml") (r "1.58.1")))

