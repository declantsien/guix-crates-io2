(define-module (crates-io tr an transmute-bytes) #:use-module (crates-io))

(define-public crate-transmute-bytes-0.1.0 (c (n "transmute-bytes") (v "0.1.0") (h "006mr2c510raajvzxyw7qj8281lfx0rlh2yd1c0236ma6hk75ry5")))

(define-public crate-transmute-bytes-0.1.1 (c (n "transmute-bytes") (v "0.1.1") (h "12vwvsfm9hb6xa7f33ing25hbh29jxn6rx2il4sjyngdalaszg4v")))

