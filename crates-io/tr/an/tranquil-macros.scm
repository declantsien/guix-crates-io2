(define-module (crates-io tr an tranquil-macros) #:use-module (crates-io))

(define-public crate-tranquil-macros-0.1.0 (c (n "tranquil-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k6arvd676z4jj65azkrg3qhxcnc7nl4knvbmc282sz88h5b2w13")))

(define-public crate-tranquil-macros-0.2.0 (c (n "tranquil-macros") (v "0.2.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1zzx1wbi2y0h54spkfca6mcxanwim8c1bcmgchsfssli9haaq1ir")))

