(define-module (crates-io tr an transform-gizmo) #:use-module (crates-io))

(define-public crate-transform-gizmo-0.0.0 (c (n "transform-gizmo") (v "0.0.0") (h "11m2hwkp9ybzg4dral271lpc9s8vc1swkkn64sxirl2lqfmgzxgg") (y #t)))

(define-public crate-transform-gizmo-0.1.0 (c (n "transform-gizmo") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.7") (d #t) (k 0)) (d (n "ecolor") (r "^0.27.2") (d #t) (k 0)) (d (n "emath") (r "^0.27.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "enumset") (r "^1.1.3") (d #t) (k 0)) (d (n "epaint") (r "^0.27.2") (d #t) (k 0)) (d (n "glam") (r "^0.27.0") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "1n8nyhgna5kpz7picixzhq2hdpykyzp2s72h4rbhk6gmvhs02w5l") (r "1.77.0")))

