(define-module (crates-io tr an trans-gen-core) #:use-module (crates-io))

(define-public crate-trans-gen-core-0.1.0 (c (n "trans-gen-core") (v "0.1.0") (d (list (d (n "include_dir") (r "^0.2") (d #t) (k 0)) (d (n "trans") (r "^0.1.0") (d #t) (k 0)) (d (n "trans-schema") (r "^0.1.0") (d #t) (k 0)))) (h "1bgx5nxjbl7fxjci195pdgr81cprcs7pd4marxr2yfmaj088flv6")))

(define-public crate-trans-gen-core-0.1.1 (c (n "trans-gen-core") (v "0.1.1") (d (list (d (n "include_dir") (r "^0.5") (d #t) (k 0)) (d (n "trans") (r "^0.1.0") (d #t) (k 0)) (d (n "trans-schema") (r "^0.1.0") (d #t) (k 0)))) (h "182al05b54qwrrmzvk8l0346lyrayyzczi3iz2adlx48faw0hcax")))

(define-public crate-trans-gen-core-0.1.2 (c (n "trans-gen-core") (v "0.1.2") (d (list (d (n "include_dir") (r "^0.5") (d #t) (k 0)) (d (n "trans-schema") (r "^0.1.0") (d #t) (k 0)))) (h "03j07458k3s4i550my4n5c3pmryd2yws8zhmjayvv1xiq88iijsq")))

(define-public crate-trans-gen-core-0.1.3 (c (n "trans-gen-core") (v "0.1.3") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "trans-schema") (r "^0.1.0") (d #t) (k 0)))) (h "01vf9159l4p2b34qhcz74z8w4dmhjp0k08hkpaja772n4xg7g50f")))

(define-public crate-trans-gen-core-0.3.0-alpha.0 (c (n "trans-gen-core") (v "0.3.0-alpha.0") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "trans-schema") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1dgfc9f9247n77milya8cpfxip5hlmm14ynh4sdl87aykh56f2ml")))

(define-public crate-trans-gen-core-0.3.0-alpha.1 (c (n "trans-gen-core") (v "0.3.0-alpha.1") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "trans") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "trans-schema") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "05xzpb3i8hyy0nwafs0xmlck61js8b5sshfihm84cv8b79zvf6zf")))

(define-public crate-trans-gen-core-0.3.0-alpha.2 (c (n "trans-gen-core") (v "0.3.0-alpha.2") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "trans") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0bjmds3vgjzg2w6q6cxwrcip7c3qdwp1qma3f1xy14s4bxihipy1")))

