(define-module (crates-io tr an trans) #:use-module (crates-io))

(define-public crate-trans-0.1.0 (c (n "trans") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.1.0") (d #t) (k 0)))) (h "13k5k0w307n6n6f1j2k4npzqkwqzb64p0nihzg34hhs2p22pwszw")))

(define-public crate-trans-0.2.0 (c (n "trans") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.2.0") (d #t) (k 0)))) (h "04i5ls6f0jh7w7kkgfhy4yrz06h30iwzgf3xd5npvckdy0b1phwj")))

(define-public crate-trans-0.3.0-alpha.0 (c (n "trans") (v "0.3.0-alpha.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "07bv8p4linf27y2v5q071fk77xdd8cdf13f89d0fp9v4s7fdjcc7")))

(define-public crate-trans-0.3.0-alpha.1 (c (n "trans") (v "0.3.0-alpha.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1vj6pw25nxavwnjdshz1kmkn0ihchyvv0q927w43wqbpgsjipkjp")))

(define-public crate-trans-0.3.0-alpha.2 (c (n "trans") (v "0.3.0-alpha.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0g1cfaysgxg31smxdrlr12g0knks0iihvlhzgvwy2vg9h4bfns3b")))

(define-public crate-trans-0.3.0 (c (n "trans") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1j8hw9hvha4g62s239ax0w6lgbgf5hmnxsz3jrq5k0vg7qf2y473")))

(define-public crate-trans-0.4.0-alpha.0 (c (n "trans") (v "0.4.0-alpha.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.4.0-alpha") (d #t) (k 0)))) (h "0vbi4bajd0fg3dikz6rc2hfamjln9wbqhwirgd4jfl7gzkh5rdsr")))

(define-public crate-trans-0.4.0-alpha.1 (c (n "trans") (v "0.4.0-alpha.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.4.0-alpha") (d #t) (k 0)))) (h "15kd08cwhjlkixmwv3jhv0xxc9lhbisq2amk4zm2arxhsb288g46")))

(define-public crate-trans-0.4.0-alpha.2 (c (n "trans") (v "0.4.0-alpha.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.4.0-alpha") (d #t) (k 0)))) (h "0k4jcdw2c02lq6yg7qzn9mm45z6b08xrk2jhd6y0m9vnrmclpmmz")))

(define-public crate-trans-0.4.0-alpha.3 (c (n "trans") (v "0.4.0-alpha.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.4.0-alpha") (d #t) (k 0)))) (h "1xicr2a4s4x7mcd24r4y3zjzzh9d16w0j4hgb2m9r9m2jmzm8jqh")))

(define-public crate-trans-0.4.0 (c (n "trans") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.4.0") (d #t) (k 0)))) (h "1acv20yl41q9arhqnamxxlfa9kpqiqsazik9w733vq6b9w1srgws")))

(define-public crate-trans-0.5.0-alpha.0 (c (n "trans") (v "0.5.0-alpha.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "trans-derive") (r "^0.5.0-alpha") (d #t) (k 0)))) (h "1ah3fj7kzjg16zlc7l8gwmg6r26fcclwq3ivdks3lvsq8hlsjn83")))

