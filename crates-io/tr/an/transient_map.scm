(define-module (crates-io tr an transient_map) #:use-module (crates-io))

(define-public crate-transient_map-0.1.0 (c (n "transient_map") (v "0.1.0") (h "09gvgaz9k18mqkrmkm9xkf126w8xvrcy66qpilw5w4wjjw95j8w4")))

(define-public crate-transient_map-0.1.1 (c (n "transient_map") (v "0.1.1") (h "0jxfxhhi1acpyyxk6n238j5wphp62fwmjcc70zv2f95pi2q5cm3n") (y #t)))

(define-public crate-transient_map-0.1.2 (c (n "transient_map") (v "0.1.2") (h "04l7l110wzw3cxiwak9cyc6w40z5pwgkhal1zipg64s1wcnbc4mw")))

