(define-module (crates-io tr an transducers) #:use-module (crates-io))

(define-public crate-transducers-0.0.1 (c (n "transducers") (v "0.0.1") (h "1sk9w4bnn3q9lrm3w0g2pjdnw8w76qaafx1pzxcid57lnlpfyi5c")))

(define-public crate-transducers-0.0.2 (c (n "transducers") (v "0.0.2") (h "1qmd9mapjy4nq03rwad443dq853bdd8d5zkwiamk7v6wmzqnhbvy")))

