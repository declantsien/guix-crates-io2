(define-module (crates-io tr an translatepmd) #:use-module (crates-io))

(define-public crate-translatepmd-1.0.0 (c (n "translatepmd") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "pmd_code_table") (r "^0.1.0") (d #t) (k 0)) (d (n "pmd_farc") (r "^1.0.1") (d #t) (k 0)) (d (n "pmd_message") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1z93nmasin3a2gb95j4ywrpik6qqrgirr49fvc6nnm0ijvp8fjb1")))

