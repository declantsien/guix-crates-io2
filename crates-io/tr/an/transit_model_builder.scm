(define-module (crates-io tr an transit_model_builder) #:use-module (crates-io))

(define-public crate-transit_model_builder-0.1.0 (c (n "transit_model_builder") (v "0.1.0") (d (list (d (n "transit_model") (r "^0.4") (d #t) (k 0)))) (h "16ak497amyi5yga4pc1wr6ilwnpp7mip24bsr1jqkwfr2px2bamh")))

(define-public crate-transit_model_builder-0.2.0 (c (n "transit_model_builder") (v "0.2.0") (d (list (d (n "transit_model") (r "^0.5") (d #t) (k 0)))) (h "0fgb48ijg53in37js2lf5gd512knz6lbqcbylnjzmrdwaby4fpjk")))

(define-public crate-transit_model_builder-0.3.0 (c (n "transit_model_builder") (v "0.3.0") (d (list (d (n "transit_model") (r "^0.6") (d #t) (k 0)))) (h "1j96qayj36x2sbql7f1s8k7d8yyqka4jwh3c00909k6jkh603k8q")))

