(define-module (crates-io tr an trans-gen-python) #:use-module (crates-io))

(define-public crate-trans-gen-python-0.1.0 (c (n "trans-gen-python") (v "0.1.0") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "1vd22npg5yryi893w82qvrvv262iqqr2rnz8qyjja3nqqy5byicx")))

(define-public crate-trans-gen-python-0.3.0-alpha.0 (c (n "trans-gen-python") (v "0.3.0-alpha.0") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "07gpyv7475g8yk9i4yvnl7kc0q85n25h1fch6ffhsi2qrqydywmk")))

(define-public crate-trans-gen-python-0.3.0-alpha.1 (c (n "trans-gen-python") (v "0.3.0-alpha.1") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "19md97jx9lm8119lxiwl8zc02y04cawbasn1wihfvlikss854ra5")))

(define-public crate-trans-gen-python-0.3.0-alpha.2 (c (n "trans-gen-python") (v "0.3.0-alpha.2") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "049nh4i6ym9z7kvrwilhrknv9l5np152nq525rknf3i8b9dn4jkg")))

