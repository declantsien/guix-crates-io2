(define-module (crates-io tr an transdirect-api) #:use-module (crates-io))

(define-public crate-transdirect-api-0.1.2 (c (n "transdirect-api") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "restson") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.2") (f (quote ("time_0_3"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "19g5003ah1rzzsi3gr9an095lihzs0hfq3njq7r4gymc5ckl0cnx")))

(define-public crate-transdirect-api-0.2.0 (c (n "transdirect-api") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "restson") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.2") (f (quote ("time_0_3"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "0h2nd8nxvam9565vz8mzaprvdckflqxi23hyrq6n91aj90p86iw8")))

