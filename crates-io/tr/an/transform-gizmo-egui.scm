(define-module (crates-io tr an transform-gizmo-egui) #:use-module (crates-io))

(define-public crate-transform-gizmo-egui-0.1.0 (c (n "transform-gizmo-egui") (v "0.1.0") (d (list (d (n "eframe") (r "^0.27.2") (d #t) (k 2)) (d (n "egui") (r "^0.27.2") (d #t) (k 0)) (d (n "transform-gizmo") (r "^0.1.0") (d #t) (k 0)))) (h "0hwr9jw000cj4wm8m8ignq6bd6xqm83snh6v727kr981g2xsrp6w") (r "1.77.0")))

