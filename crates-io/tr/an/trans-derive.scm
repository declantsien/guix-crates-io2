(define-module (crates-io tr an trans-derive) #:use-module (crates-io))

(define-public crate-trans-derive-0.1.0 (c (n "trans-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12pbd347bx93ama1yy1pcvk4kqq0xqvxz4w18q5y0a2llvzrji29")))

(define-public crate-trans-derive-0.2.0 (c (n "trans-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bs95410kwld5jv2582jimi11g86kahlg1hzrkhcbajh3dycd5a5")))

(define-public crate-trans-derive-0.2.1 (c (n "trans-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1w9llvqprpm1p8hcbxwk6c9wdh39kzjarh6k0rkkancd2c2snfy8")))

(define-public crate-trans-derive-0.3.0-alpha.0 (c (n "trans-derive") (v "0.3.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13ws1gz9n5f6jvx1fzxbv1ifhsjxcnyy7w3l23p1l79pnx61ds9h")))

(define-public crate-trans-derive-0.3.0-alpha.1 (c (n "trans-derive") (v "0.3.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hqmlmllvrkdwkbwgm0ld2n182dp2h216b02c0gb5wvfxspwfipk")))

(define-public crate-trans-derive-0.3.0-alpha.2 (c (n "trans-derive") (v "0.3.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jgbflfgvi49c3k20vhyqhd2saajdz79b8dn13a8gd2yn2cnqj7z")))

(define-public crate-trans-derive-0.3.0 (c (n "trans-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "092dg4hfvj2jrwrr1llgvq4jvplqr36plq0kk8algh23d72qs98v")))

(define-public crate-trans-derive-0.4.0-alpha.0 (c (n "trans-derive") (v "0.4.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0km3nyw8viwwvh04lcsmq2yiywiqwjfv3l8fsfcbmh80qz134ngi")))

(define-public crate-trans-derive-0.4.0-alpha.1 (c (n "trans-derive") (v "0.4.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0q6f7054g3dbi0d57d5sl48pwcwbj27bv671s9v6k29097hijlw6")))

(define-public crate-trans-derive-0.4.0-alpha.2 (c (n "trans-derive") (v "0.4.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14b96pm01063fhc1qlhhynflmblhapfm63gzv8my6bm70r30mln3")))

(define-public crate-trans-derive-0.4.0-alpha.4 (c (n "trans-derive") (v "0.4.0-alpha.4") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mxywcprg7fzysggrxiyaabnipi195zwsd1997zskqfifv4n0w1m")))

(define-public crate-trans-derive-0.4.0 (c (n "trans-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xn5ay5cq5f04zh1gsvckppw62vynvqjq06fa0f37vqjbc8yha40")))

(define-public crate-trans-derive-0.5.0-alpha.0 (c (n "trans-derive") (v "0.5.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nr4y6a1v4cg4r6rim5r82p4l3d0s0b5sga28wdfk09ynhrkx6rn")))

