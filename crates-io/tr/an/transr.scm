(define-module (crates-io tr an transr) #:use-module (crates-io))

(define-public crate-transr-0.1.0 (c (n "transr") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "18n80fbhnljmxx4ns1srxjnw4pwxfxxixr2cyrx7c6xwby63sr4r")))

(define-public crate-transr-0.1.1 (c (n "transr") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "1135zc8dh2pafcc9dl87bxlxdiz1spm071kbdr0w9izjyh5i012l")))

(define-public crate-transr-0.2.0 (c (n "transr") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "07naxwqcd4lacq5g1smr924x709ibwwxznj4na8r714x0gidq3kd")))

(define-public crate-transr-0.2.1 (c (n "transr") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "0d1s7qcdii4c7g8vzlci5ynma3v7zwsca2kyihcchppddx6y6cfz")))

(define-public crate-transr-0.2.2 (c (n "transr") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "051cvbxqi4k79wappmazbr8c1s3qf92hnknnia86pf4r4lv90cnc")))

(define-public crate-transr-0.2.3 (c (n "transr") (v "0.2.3") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "1v8n7zb6xjx5nfwbs3ri1ggvhgfvq8ydq3qsix1h2dr8vhbpyai0")))

(define-public crate-transr-0.2.4 (c (n "transr") (v "0.2.4") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "1d8zzsag5m5mjlfik5l8q7bf9d5zvr7lzcjk9cg9lb8b0gk84hgj")))

(define-public crate-transr-0.2.5 (c (n "transr") (v "0.2.5") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "0vcmhd9d1ylqjriyr80g0kl83jgqqmfc51k80ci6sh73590bjh0y")))

