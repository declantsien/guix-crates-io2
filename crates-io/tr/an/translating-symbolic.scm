(define-module (crates-io tr an translating-symbolic) #:use-module (crates-io))

(define-public crate-translating-symbolic-0.1.0 (c (n "translating-symbolic") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)))) (h "178nbz838cdjfhjibvz4p7nsilclmhhm03vpfin48aj8vm8h7apr")))

(define-public crate-translating-symbolic-0.1.1 (c (n "translating-symbolic") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)))) (h "0m7pc9cpzlp470pm0508b3858y5g57b1h46zqn89br4h2w02crqx")))

(define-public crate-translating-symbolic-0.1.2 (c (n "translating-symbolic") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "iter_tools") (r "^0.1.4") (d #t) (k 0)))) (h "0zqri25bqcl2w70ly5rpmkmcgdd9fd2a1z4qdzh306kkxayy5vhz")))

