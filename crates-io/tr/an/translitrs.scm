(define-module (crates-io tr an translitrs) #:use-module (crates-io))

(define-public crate-translitrs-0.2.0 (c (n "translitrs") (v "0.2.0") (d (list (d (n "lazy_static") (r ">=1.4.0") (d #t) (k 0)) (d (n "pandoc_ast") (r ">=0.8.0") (d #t) (k 0)) (d (n "regex") (r ">=1.5.4") (d #t) (k 0)) (d (n "subslice") (r ">=0.2.3") (d #t) (k 0)))) (h "0ixvlh4knwmb4q921z7a36n7l8kxyc7vcj8230vpgcvrfiazqcwr") (r "1.57")))

(define-public crate-translitrs-0.2.1 (c (n "translitrs") (v "0.2.1") (d (list (d (n "lazy_static") (r ">=1.4.0") (d #t) (k 0)) (d (n "pandoc_ast") (r ">=0.8.2") (d #t) (k 0)) (d (n "regex") (r ">=1.7.1") (d #t) (k 0)) (d (n "subslice") (r ">=0.2.3") (d #t) (k 0)))) (h "0pg868gzydizyfsmv17whhdy56w3hpvcbbs8jdbcmw9cp0jpaabr") (r "1.57")))

(define-public crate-translitrs-0.2.2 (c (n "translitrs") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pandoc_ast") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "subslice") (r "^0.2") (d #t) (k 0)))) (h "0ng5xjf3ignm42ipaydbsya6rp92g4j9nfhy0pkvaqp9gldsimi6") (f (quote (("default")))) (s 2) (e (quote (("pandoc" "dep:pandoc_ast")))) (r "1.57")))

