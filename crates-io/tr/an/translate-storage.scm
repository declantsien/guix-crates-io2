(define-module (crates-io tr an translate-storage) #:use-module (crates-io))

(define-public crate-translate-storage-0.0.1 (c (n "translate-storage") (v "0.0.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "locale_config") (r ">= 0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "16w716s76bxhq5ascfvlrm9fibk0i164ckrpjabanccy0xy11ka0")))

