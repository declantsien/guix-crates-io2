(define-module (crates-io tr an transmission) #:use-module (crates-io))

(define-public crate-transmission-0.1.0 (c (n "transmission") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transmission-sys") (r "^0.3") (d #t) (k 0)))) (h "15259b28qcvjmy7nax849cwrbx9szm237ykj1pxcix5dq6y9aphz") (y #t)))

(define-public crate-transmission-0.1.1 (c (n "transmission") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transmission-sys") (r "^0.3") (d #t) (k 0)))) (h "0s5832c0rl1l055whghfwkxzjxl2bc44h2xy3r63azzjhv3hgs4l") (f (quote (("docs-only" "transmission-sys/docs-only"))))))

(define-public crate-transmission-0.1.2 (c (n "transmission") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transmission-sys") (r "^0.3") (d #t) (k 0)))) (h "19bkg787fjps9f4yswpqnf9rbcnzl8cglmgm1skccaxxpihp0vip") (f (quote (("docs-only" "transmission-sys/docs-only"))))))

(define-public crate-transmission-0.2.0 (c (n "transmission") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transmission-sys") (r "^0.3") (d #t) (k 0)))) (h "158jnh9j4ygizgv3r3wgh4h8fg6p41n6yv6msknnkh8fy5gyadaq") (f (quote (("docs-only" "transmission-sys/docs-only"))))))

(define-public crate-transmission-0.2.1 (c (n "transmission") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transmission-sys") (r "^0.3") (d #t) (k 0)))) (h "0jpshfxvrfnz3jqiyl5dimll7wp8diwban2v66rrm25q2sla9a03") (f (quote (("docs-only" "transmission-sys/docs-only"))))))

(define-public crate-transmission-0.3.0 (c (n "transmission") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transmission-sys") (r "^0.3") (d #t) (k 0)))) (h "12qpgmyvjvcz55wg597mmapz2464hzz6c1aiavmyjm1fz7rg038b") (f (quote (("docs-only" "transmission-sys/docs-only"))))))

(define-public crate-transmission-0.3.1 (c (n "transmission") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transmission-sys") (r "^0.3") (d #t) (k 0)))) (h "0j07ghz7kd40wcjc3j34q9zzazh62l3rjzmzqvqxc7aax7yrz5yn") (f (quote (("docs-only" "transmission-sys/docs-only"))))))

