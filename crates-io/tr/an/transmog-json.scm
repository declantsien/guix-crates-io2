(define-module (crates-io tr an transmog-json) #:use-module (crates-io))

(define-public crate-transmog-json-0.1.0-dev.2 (c (n "transmog-json") (v "0.1.0-dev.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.2") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.2") (f (quote ("test-util"))) (d #t) (k 2)))) (h "17hdhvif95h241xarnsrjpw2z2i2p9f5nzi1lf3bk5vg3w8ksd1d")))

(define-public crate-transmog-json-0.1.0 (c (n "transmog-json") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (f (quote ("test-util"))) (d #t) (k 2)))) (h "0ik62dksh5vpm4f20sydrv2zzy0bwwvfhpbln3jhyypl6zv23zvq")))

