(define-module (crates-io tr an transvec) #:use-module (crates-io))

(define-public crate-transvec-0.1.0 (c (n "transvec") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7.0") (d #t) (k 0)) (d (n "chunk_iter") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0bar2x6v8b7dgbfhfjymsldyqg063fvg550b55bndgfiwfw3xls8")))

(define-public crate-transvec-0.2.0 (c (n "transvec") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.7.0") (d #t) (k 0)) (d (n "chunk_iter") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0pw9b8h2fibcxqwjv9gm1zbq86j67iyn5kgmnvyxrhiiz1dnbbqm") (y #t)))

(define-public crate-transvec-0.2.1 (c (n "transvec") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0nh045v4kv4sj94k33259rzh7j7zp79s5jqsxjmpyiar5hav02jl") (y #t)))

(define-public crate-transvec-0.2.2 (c (n "transvec") (v "0.2.2") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19mbxzab06lqlrd1k5db5vmll1gdympbvraamaxb3n448z41xk18") (y #t)))

(define-public crate-transvec-0.2.3 (c (n "transvec") (v "0.2.3") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v0qimh0n58hb5h97w1lgg9wln9sqpvxi2nqdahi18g1lvbpl2wr") (y #t)))

(define-public crate-transvec-0.2.4 (c (n "transvec") (v "0.2.4") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "151niap1izv6pfqm3vkbxr196dnk031w9j8ppadff2p5jfbfggjb")))

(define-public crate-transvec-0.3.0 (c (n "transvec") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mbi0kwrzj19bpzvxjf0vqzxm93wqlb4niwqrvf11nnaaxsrl6a8")))

(define-public crate-transvec-0.3.1 (c (n "transvec") (v "0.3.1") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "1wmdf21gzcq6bya46yi2xadnlxf65ijrhwpd8cqn1dlcifrjz8rk") (f (quote (("default" "allocator_api") ("allocator_api"))))))

(define-public crate-transvec-0.3.2 (c (n "transvec") (v "0.3.2") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "0fmxz2sg4m9l3wvxdnjnzrabja906g3izg96qxqy0cljvijcv18m") (f (quote (("std") ("default" "allocator_api" "std") ("allocator_api"))))))

(define-public crate-transvec-0.3.3 (c (n "transvec") (v "0.3.3") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "1rxmxlna4f36rz9dlawm69vl5nhgzx83i02mwjy1ihl4jfg02454") (f (quote (("std") ("default" "allocator_api" "std") ("allocator_api"))))))

(define-public crate-transvec-0.3.4 (c (n "transvec") (v "0.3.4") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "1x9a0ld7zjqxa2r23cwmdvyhd1ginrjm2dbqg5apqhah7ljahpxh") (f (quote (("std") ("default" "allocator_api" "std") ("allocator_api"))))))

(define-public crate-transvec-0.3.5 (c (n "transvec") (v "0.3.5") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "0p38iv8ihdfzlk7zc7x1s9rbjx2wa4kis225dq313ml6jm7l8x65") (f (quote (("std") ("default" "allocator_api" "std") ("allocator_api"))))))

(define-public crate-transvec-0.3.6 (c (n "transvec") (v "0.3.6") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "0k8gcnwhr07rg44f4dg2pqp0z77gk1d5wza5bkcvgjih3nzzicgx") (f (quote (("std") ("default" "allocator_api" "std") ("allocator_api"))))))

(define-public crate-transvec-0.3.7 (c (n "transvec") (v "0.3.7") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "0kv6zampasvvpc5033fb85bzcgc54ylglgpyd9n8i82z3ma7csps") (f (quote (("std") ("default" "allocator_api" "std") ("allocator_api"))))))

