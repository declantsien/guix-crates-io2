(define-module (crates-io tr an trans-gen-cpp) #:use-module (crates-io))

(define-public crate-trans-gen-cpp-0.1.0 (c (n "trans-gen-cpp") (v "0.1.0") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "05rhygzl0l03d06p9pxs51j89v0x38xrnzsc5bac3v5hazrjhy7w")))

(define-public crate-trans-gen-cpp-0.1.1 (c (n "trans-gen-cpp") (v "0.1.1") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "1ifvnvm5jv8771hvazc20hyvg01rscqimgfzr6w79i5xzw38nkcp")))

(define-public crate-trans-gen-cpp-0.3.0-alpha.0 (c (n "trans-gen-cpp") (v "0.3.0-alpha.0") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0cz2ir8yg6mw0cmd8z9ays3pa7f59kfz2x2nwvi6r8w9dcjb4ifz")))

(define-public crate-trans-gen-cpp-0.3.0-alpha.1 (c (n "trans-gen-cpp") (v "0.3.0-alpha.1") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0b4v6b406w7dsvfhci9zsh9nvgx69xpd56jpgsaf2g65zlws2mm0")))

(define-public crate-trans-gen-cpp-0.3.0-alpha.2 (c (n "trans-gen-cpp") (v "0.3.0-alpha.2") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "160xn46nyhk41ba150vdrd7g2pqcz8klyd9pai9rwf7cm50agr01")))

