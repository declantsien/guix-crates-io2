(define-module (crates-io tr an transpose) #:use-module (crates-io))

(define-public crate-transpose-0.1.0 (c (n "transpose") (v "0.1.0") (h "07ndndq94c5hsqz5kphjs0cmnw688axysp4yn1xpqqmh1dc22gk4")))

(define-public crate-transpose-0.2.0 (c (n "transpose") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "strength_reduce") (r "^0.2.1") (d #t) (k 0)))) (h "186093zibsbcphflhswg0is63vsbbgn11z2vpzb1yspa3pviwcf3")))

(define-public crate-transpose-0.2.1 (c (n "transpose") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "strength_reduce") (r "^0.2.1") (d #t) (k 0)))) (h "17z34pdylj8mh2nbiai6cmrfrkah0mlgs9z2mr1ymdlqm80ckycm")))

(define-public crate-transpose-0.2.2 (c (n "transpose") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "strength_reduce") (r "^0.2.1") (d #t) (k 0)))) (h "08wvmxfw91375vsrnzxa206lyxykhc9bqk5f72qzy9rps14jslp6")))

(define-public crate-transpose-0.2.3 (c (n "transpose") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "strength_reduce") (r "^0.2.1") (d #t) (k 0)))) (h "0zp74v7jrjg4jr654dncdj6hqvacicsywyhc62jawgxwhvnimmhs")))

