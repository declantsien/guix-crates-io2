(define-module (crates-io tr an transaction-stm) #:use-module (crates-io))

(define-public crate-transaction-stm-0.1.0 (c (n "transaction-stm") (v "0.1.0") (d (list (d (n "stm") (r "^0.2.4") (d #t) (k 0)) (d (n "transaction") (r "^0.1.0") (d #t) (k 0)))) (h "18wsq7b2b89yp6gmsjpw79ks9756w35n226m1h84ffmcn8ws6ydk")))

(define-public crate-transaction-stm-0.2.0 (c (n "transaction-stm") (v "0.2.0") (d (list (d (n "stm") (r "^0.2.4") (d #t) (k 0)) (d (n "transaction") (r "^0.2.0") (d #t) (k 0)))) (h "1753j4pw1mqzvy9yqpvdgg2b1aza1vvvnhibnlmv7g9pjfcqlcry")))

