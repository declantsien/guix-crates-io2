(define-module (crates-io tr an trans-gen-dlang) #:use-module (crates-io))

(define-public crate-trans-gen-dlang-0.1.0 (c (n "trans-gen-dlang") (v "0.1.0") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "1scyz0k2ya13w3jbbw0h9sfiwp0b99qjl1cb97bprx0nbg0vn11w")))

(define-public crate-trans-gen-dlang-0.3.0-alpha.0 (c (n "trans-gen-dlang") (v "0.3.0-alpha.0") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "01w49rx9ck98iji7snzzammjxkw83sd6x6gqvijxdkdsq1v4xnav")))

(define-public crate-trans-gen-dlang-0.3.0-alpha.1 (c (n "trans-gen-dlang") (v "0.3.0-alpha.1") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1da9w4cn80p81r92vhcdsjbmkp4dkrc8qn4qd91rf59j4z8xc3gp")))

(define-public crate-trans-gen-dlang-0.3.0-alpha.2 (c (n "trans-gen-dlang") (v "0.3.0-alpha.2") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "09dyi9k5nf4rv3larsz8ym2f8qmszn4hv2l4344kygw8jfnrqs6h")))

