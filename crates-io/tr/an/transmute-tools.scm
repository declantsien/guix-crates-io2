(define-module (crates-io tr an transmute-tools) #:use-module (crates-io))

(define-public crate-transmute-tools-0.1.0 (c (n "transmute-tools") (v "0.1.0") (d (list (d (n "memoffset") (r "^0.6.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("full"))) (d #t) (k 0)))) (h "1s9420b4zy42pkcb9mhh7nfkiqlpn1n97dwx4v696h5zrp311whk")))

