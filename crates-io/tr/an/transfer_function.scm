(define-module (crates-io tr an transfer_function) #:use-module (crates-io))

(define-public crate-transfer_function-0.1.0 (c (n "transfer_function") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "array-matrix") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "14x0aw9cyxacpnsp1bslpkkw8dh28xmqn2q16c43ky3waii4kr1i")))

(define-public crate-transfer_function-0.1.1 (c (n "transfer_function") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "06l6fazl6j000ivfwy883zgq5j2r4h5iyri53avsi51df52xcih1")))

(define-public crate-transfer_function-0.1.2 (c (n "transfer_function") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "011wzj4p89rbfknf5ylq9pbdw7xxbvrw277xc9mjcr3ajz3yjn4v")))

