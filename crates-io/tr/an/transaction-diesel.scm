(define-module (crates-io tr an transaction-diesel) #:use-module (crates-io))

(define-public crate-transaction-diesel-0.1.0 (c (n "transaction-diesel") (v "0.1.0") (d (list (d (n "diesel") (r "^0.12.1") (d #t) (k 0)) (d (n "transaction") (r "^0.1.0") (d #t) (k 0)))) (h "19cxvnic04iicikzpcz5xmk477srjj8mxyf1r3qrsb64jhz3i0p3")))

(define-public crate-transaction-diesel-0.2.0 (c (n "transaction-diesel") (v "0.2.0") (d (list (d (n "diesel") (r ">= 0.12.0, <= 0.13") (d #t) (k 0)) (d (n "transaction") (r "^0.2.0") (d #t) (k 0)))) (h "0xy8yjp96fi0qp9n8wi85mvlgg2fpi7vrpj5d5cz12dnm6zcnpw7")))

(define-public crate-transaction-diesel-0.2.1 (c (n "transaction-diesel") (v "0.2.1") (d (list (d (n "diesel") (r ">= 0.12.0, < 0.16") (d #t) (k 0)) (d (n "transaction") (r "^0.2.1") (d #t) (k 0)))) (h "18mdzj5xmnski5jcbv1fz28mchxsj6xhskzfiksrrr8jjycrwrp3")))

(define-public crate-transaction-diesel-0.2.2 (c (n "transaction-diesel") (v "0.2.2") (d (list (d (n "diesel") (r ">= 0.12.0, < 1.0") (d #t) (k 0)) (d (n "transaction") (r "^0.2.1") (d #t) (k 0)))) (h "07igpqidgvizphmfikh513247m5w3pqx4iy9bcwwcyjbci8xnk8i")))

