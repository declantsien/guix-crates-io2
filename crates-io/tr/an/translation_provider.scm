(define-module (crates-io tr an translation_provider) #:use-module (crates-io))

(define-public crate-translation_provider-0.1.0 (c (n "translation_provider") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1hasxckhzjj8b5njcqhf1rs3h00j8fsd5kagz1rgpzif5fbgcqvc") (f (quote (("default" "serde"))))))

(define-public crate-translation_provider-0.1.1 (c (n "translation_provider") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0ynvhzhcr7aa31ivm6zxlz0y49qwak233qyagcsmqx565bz33yw6") (f (quote (("default" "serde"))))))

(define-public crate-translation_provider-0.2.0 (c (n "translation_provider") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0m61xn3nzlympdyyb2mn4aqr8ccb7msq292psclin8910nd4j2k8") (f (quote (("default" "serde"))))))

