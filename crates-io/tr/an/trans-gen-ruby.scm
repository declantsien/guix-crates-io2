(define-module (crates-io tr an trans-gen-ruby) #:use-module (crates-io))

(define-public crate-trans-gen-ruby-0.1.0 (c (n "trans-gen-ruby") (v "0.1.0") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "0sc8w67sav6y4yl8335z2df7gr440j3h6gbdk6zcddv4s7cyl6lp")))

(define-public crate-trans-gen-ruby-0.3.0-alpha.0 (c (n "trans-gen-ruby") (v "0.3.0-alpha.0") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "01l2yhy30d4d7vz7jkllm6shrs0jchkaac4d43c9zn3ihmqzkw96")))

(define-public crate-trans-gen-ruby-0.3.0-alpha.1 (c (n "trans-gen-ruby") (v "0.3.0-alpha.1") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0vg1ips5vvm891cc20p62jkwca6w2i4xdjy771mqbscsnckxvsdm")))

(define-public crate-trans-gen-ruby-0.3.0-alpha.2 (c (n "trans-gen-ruby") (v "0.3.0-alpha.2") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "12j7cpcqk9nmnp66byppznnzsgkrqjc7mqmz45cv6bfczb6h4apn")))

