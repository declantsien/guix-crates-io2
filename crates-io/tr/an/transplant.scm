(define-module (crates-io tr an transplant) #:use-module (crates-io))

(define-public crate-transplant-0.1.0 (c (n "transplant") (v "0.1.0") (d (list (d (n "memchr") (r "^2.2.1") (d #t) (k 0)))) (h "1v3d23xjj966s2rn6i5qv86nhqkc3dx0jv1v8sh7cxwyr24irvap")))

(define-public crate-transplant-0.1.1 (c (n "transplant") (v "0.1.1") (d (list (d (n "memchr") (r "^2.2.1") (d #t) (k 0)))) (h "1cq9l5h70lbhrn90mgw0g17garfh3ssaxrwkjb19cih5jgj2n1dv")))

(define-public crate-transplant-0.2.0 (c (n "transplant") (v "0.2.0") (d (list (d (n "aho-corasick") (r "^0.7.6") (d #t) (k 0)))) (h "18apdh6g44if90p5dk7l5lcj1mphr2x3mq1z50adh3lizwxi0pxs")))

