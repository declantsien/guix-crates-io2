(define-module (crates-io tr an transit) #:use-module (crates-io))

(define-public crate-transit-0.1.0 (c (n "transit") (v "0.1.0") (h "067gq7nq6np96hmpiv5l9ig65n4sxhn6h03idcwicc8chpngayvz")))

(define-public crate-transit-0.2.0 (c (n "transit") (v "0.2.0") (d (list (d (n "rmp") (r "*") (d #t) (k 0)) (d (n "rmp-serde") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)))) (h "054xjb04ssxq3xfzgi136h91p6rn70yvgs584h9yy1n5lr1vgb8c")))

(define-public crate-transit-0.3.0 (c (n "transit") (v "0.3.0") (d (list (d (n "rmp") (r "*") (o #t) (d #t) (k 0)) (d (n "rmp-serde") (r "*") (o #t) (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_json") (r "*") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 2)))) (h "0ss3g7qvyv9saifsfgfbz5inn53bhnbi7ls1s49q00xiia084n0x") (f (quote (("msgpack_serialization" "rmp" "rmp-serde") ("json_serialization" "serde_json") ("default"))))))

