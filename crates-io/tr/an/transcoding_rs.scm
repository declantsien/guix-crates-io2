(define-module (crates-io tr an transcoding_rs) #:use-module (crates-io))

(define-public crate-transcoding_rs-0.1.0 (c (n "transcoding_rs") (v "0.1.0") (d (list (d (n "chardetng") (r "^0.1.14") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (f (quote ("fast-legacy-encode"))) (d #t) (k 0)))) (h "02zrzkgd5csr6pnlilclcpy7pskah5ynzqysapvzdcblmgqcda9y")))

(define-public crate-transcoding_rs-0.1.1 (c (n "transcoding_rs") (v "0.1.1") (d (list (d (n "chardetng") (r "^0.1.14") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (f (quote ("fast-legacy-encode"))) (d #t) (k 0)))) (h "1cagz50z27a0lzg61mx90gnirml96dm7d0hh1p75hqry4iljr55z")))

