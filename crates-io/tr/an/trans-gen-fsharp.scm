(define-module (crates-io tr an trans-gen-fsharp) #:use-module (crates-io))

(define-public crate-trans-gen-fsharp-0.1.0 (c (n "trans-gen-fsharp") (v "0.1.0") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "11pn0m47mfx87kjwn85c4ykl5rq5c86n10g8jx1rzwgr1x0k81bg")))

(define-public crate-trans-gen-fsharp-0.3.0-alpha.0 (c (n "trans-gen-fsharp") (v "0.3.0-alpha.0") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0yn1g78rkaa3jljk0cp5mpiksjq69pzb4273ahhdkq74vdvf21w4")))

(define-public crate-trans-gen-fsharp-0.3.0-alpha.1 (c (n "trans-gen-fsharp") (v "0.3.0-alpha.1") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1y9k4pz8zplbzdigy6y0cpdxllz2jl05s3v9wrlss7vpi99r7adx")))

(define-public crate-trans-gen-fsharp-0.3.0-alpha.2 (c (n "trans-gen-fsharp") (v "0.3.0-alpha.2") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0av4sh4kx959nx0nfsbm8r36xcqf3p2fbgfghn7fanc5zays6fwv")))

