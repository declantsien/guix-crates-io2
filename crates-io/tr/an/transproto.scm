(define-module (crates-io tr an transproto) #:use-module (crates-io))

(define-public crate-transproto-0.1.0 (c (n "transproto") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "dtoa") (r "^0.4") (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)))) (h "0plhn044hl0x7l9lib1gd2nv9h9fjhpljk3lwgksrll1r09ay1j2")))

