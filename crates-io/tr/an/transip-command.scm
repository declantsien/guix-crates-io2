(define-module (crates-io tr an transip-command) #:use-module (crates-io))

(define-public crate-transip-command-0.1.3 (c (n "transip-command") (v "0.1.3") (d (list (d (n "pest") (r "^2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1bn3y8szzjszihajy8mizq4cgyzf7rzbix0rb4rlav3q1ck0a21h")))

(define-public crate-transip-command-0.1.4 (c (n "transip-command") (v "0.1.4") (d (list (d (n "pest") (r "^2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1lhzxjqsy9nnlw8y2l9sam1ijf2858knzh6qkqbc58mp92fbh8jz")))

(define-public crate-transip-command-0.2.0 (c (n "transip-command") (v "0.2.0") (d (list (d (n "pest") (r "^2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0xa2zpkbqsdwqr8k0xsfhlh2hq9vkx31jywafhpc9vp6l6qarw14")))

(define-public crate-transip-command-0.2.1 (c (n "transip-command") (v "0.2.1") (d (list (d (n "pest") (r "^2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1wszfhg3mb7lblwv0x7qhw42zk1d6kqc9fwp44r1j5p0hbgj5qvy")))

(define-public crate-transip-command-0.3.0 (c (n "transip-command") (v "0.3.0") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1x7gba14va4r6jvlqxpbmd4r4y7i2vd24dppciz42dsd12ihksfk")))

(define-public crate-transip-command-0.3.1 (c (n "transip-command") (v "0.3.1") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "183h5pkpn7y92yw3x7cvbbdf499yfyhrya7dpq33lyvl1lbbmaqh")))

(define-public crate-transip-command-0.3.2 (c (n "transip-command") (v "0.3.2") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0l43wmgrgly4a0k2gv36m0qg1h7pilk0l9g85i7ipdaa47pqzb8s") (f (quote (("propagation"))))))

(define-public crate-transip-command-0.3.3 (c (n "transip-command") (v "0.3.3") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0hp069rmw2m0q20375bnfy5bmm8kl16ams5wzrhpv7qq8gwdlpgi") (f (quote (("propagation"))))))

(define-public crate-transip-command-0.3.4 (c (n "transip-command") (v "0.3.4") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "03883pv2fz4gw3ls6r1by5z14j59p60njxlc7c221j9zb13bm2kq") (f (quote (("propagation"))))))

(define-public crate-transip-command-0.3.5 (c (n "transip-command") (v "0.3.5") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1nv4k6526bk62ng5hinga5kciw4ql12al4ygnhjcdgr5rsvx9zhs") (f (quote (("propagation"))))))

(define-public crate-transip-command-0.3.6 (c (n "transip-command") (v "0.3.6") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0qqp877xwggx5w72avfg7ixhsnqahpjc0z1ia5w7cw5qxjmcsp9l") (f (quote (("propagation"))))))

(define-public crate-transip-command-0.3.8 (c (n "transip-command") (v "0.3.8") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0sk07flnx99zy323as1prz7bkmgq2agnwr7vnkaix3nawzksv69v") (f (quote (("propagation"))))))

(define-public crate-transip-command-0.3.10 (c (n "transip-command") (v "0.3.10") (d (list (d (n "strum") (r "^0.26.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1sgmfmagimwv6bww8xgpa88abglc6bvc7hhyqdhvcpymcydy7ync") (f (quote (("propagation")))) (r "1.64.0")))

(define-public crate-transip-command-0.3.14 (c (n "transip-command") (v "0.3.14") (d (list (d (n "strum") (r "^0.26.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "19dwrgbzr12sw378isx2n1wdpq44m23vzig96x2sxad61cywzi0b") (f (quote (("propagation")))) (r "1.64.0")))

(define-public crate-transip-command-0.4.0 (c (n "transip-command") (v "0.4.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 0)))) (h "0yajwjkkijqns5rydl2h78b8bccg6ah2is0rz6l6nqj71yfaixvy") (f (quote (("propagation"))))))

(define-public crate-transip-command-4.0.1 (c (n "transip-command") (v "4.0.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 0)))) (h "08imsyljxsvcvwm60bq9q51himp85zhan6xs1wp0gap688q4bqa4") (f (quote (("propagation")))) (y #t)))

(define-public crate-transip-command-0.4.1 (c (n "transip-command") (v "0.4.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 0)))) (h "08cwkxx5k62g5wkr4j8s15miwc83awm0apb1sipbx3va0zyrh9gj") (f (quote (("propagation"))))))

