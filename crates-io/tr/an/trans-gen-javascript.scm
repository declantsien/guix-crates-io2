(define-module (crates-io tr an trans-gen-javascript) #:use-module (crates-io))

(define-public crate-trans-gen-javascript-0.1.0 (c (n "trans-gen-javascript") (v "0.1.0") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "0j8026wxgy79l2pq9lh1vy0ndf6gnabmbbz9jxj9zvvifxqs48m8")))

(define-public crate-trans-gen-javascript-0.3.0-alpha.0 (c (n "trans-gen-javascript") (v "0.3.0-alpha.0") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0b4mb3xsw9myryyh952k4ia2nxp6va1rsifdnqclrdvazgnnws8a")))

(define-public crate-trans-gen-javascript-0.3.0-alpha.1 (c (n "trans-gen-javascript") (v "0.3.0-alpha.1") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "01xv7d3dv42hqqi9sa44pgcf6rb5837dsfms9pw7qf3gr2azr9pd")))

(define-public crate-trans-gen-javascript-0.3.0-alpha.2 (c (n "trans-gen-javascript") (v "0.3.0-alpha.2") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "058pgr2690an94kx6ac228kgj742a9h2x5gx4qrjlpswyx8ysn6g")))

