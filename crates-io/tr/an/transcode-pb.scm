(define-module (crates-io tr an transcode-pb) #:use-module (crates-io))

(define-public crate-transcode-pb-0.1.0 (c (n "transcode-pb") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "dtoa") (r "^0.4") (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)))) (h "0y373p5vs7vm0a9av0n6d89yxmdzlgx87bxy9b2crzd1ka07ccbg") (y #t)))

