(define-module (crates-io tr an transactional_iterator) #:use-module (crates-io))

(define-public crate-transactional_iterator-0.1.0 (c (n "transactional_iterator") (v "0.1.0") (h "17pzlj9p9ckdhvlbi49l6mlh4lj8d3n8qm0xgihnj2n9flwkvqhi")))

(define-public crate-transactional_iterator-0.2.0 (c (n "transactional_iterator") (v "0.2.0") (h "0ba2hazzv1pgshdxqsaas5il6qgar2q8b2bmwcszjjiyn1z5fr7n") (f (quote (("panic_on_drop") ("default" "panic_on_drop")))) (r "1.65.0")))

(define-public crate-transactional_iterator-0.3.0 (c (n "transactional_iterator") (v "0.3.0") (d (list (d (n "sealed") (r "^0.5.0") (d #t) (k 0)))) (h "1rbf61jr4p39pvs1p8gsrysjiaapq0ldd4grvjny5f1379nbh3b9") (r "1.65.0")))

(define-public crate-transactional_iterator-0.3.1 (c (n "transactional_iterator") (v "0.3.1") (h "1wq2rh89qknabzq5rvj4dkwhb4mz016fv8axd1dpg6ziky37nvq4") (r "1.65.0")))

(define-public crate-transactional_iterator-0.4.0 (c (n "transactional_iterator") (v "0.4.0") (h "175j64aps5x2rg3n123d7cvkx8cwbbhhbif78m91d6k9b9s3pb93") (r "1.65.0")))

