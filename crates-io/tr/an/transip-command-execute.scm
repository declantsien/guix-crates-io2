(define-module (crates-io tr an transip-command-execute) #:use-module (crates-io))

(define-public crate-transip-command-execute-0.1.4 (c (n "transip-command-execute") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "transip") (r "^0.1.4") (d #t) (k 0)) (d (n "transip-command") (r "^0.1.4") (d #t) (k 0)))) (h "1mrkkrlaxb9v08i8fh7aisajqnq4y0cprnjq0fsx2752c33ch02k") (y #t)))

