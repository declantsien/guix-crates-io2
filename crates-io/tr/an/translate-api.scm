(define-module (crates-io tr an translate-api) #:use-module (crates-io))

(define-public crate-translate-api-0.1.0 (c (n "translate-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "11hm0hkh23glrnzgj60mjf6pmswmbdiazwrak0mxykb4i0w5zxkz")))

