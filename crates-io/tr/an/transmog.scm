(define-module (crates-io tr an transmog) #:use-module (crates-io))

(define-public crate-transmog-0.0.0-reserve.0 (c (n "transmog") (v "0.0.0-reserve.0") (h "0rmawas2mhric2qb6i55sprahmga5fc7agv4j935wia4hyj095v7")))

(define-public crate-transmog-0.1.0-dev.1 (c (n "transmog") (v "0.1.0-dev.1") (h "17s5ziznkvwb6yrkl20gnm86piy5ns7w5waf9haf88l8d54ig0mf") (f (quote (("test-util") ("default"))))))

(define-public crate-transmog-0.1.0-dev.2 (c (n "transmog") (v "0.1.0-dev.2") (h "1gb43k405v0jyqhb6q9nzqklj4lycs7rdr5gv0aqrbl3p0kjxf4l") (f (quote (("test-util") ("default"))))))

(define-public crate-transmog-0.1.0 (c (n "transmog") (v "0.1.0") (h "1b7gqh7rzivqx3b5g66iiq8i844b9q49yc67h4ga8801p9fg19yk") (f (quote (("test-util") ("default"))))))

