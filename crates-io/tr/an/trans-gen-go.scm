(define-module (crates-io tr an trans-gen-go) #:use-module (crates-io))

(define-public crate-trans-gen-go-0.1.0 (c (n "trans-gen-go") (v "0.1.0") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "0kw43n9v29pj94n1yl9bjblfk76ff7h67brd9pb695qpnf0azz8p")))

(define-public crate-trans-gen-go-0.3.0-alpha.0 (c (n "trans-gen-go") (v "0.3.0-alpha.0") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1fys12m3f390sapay6vaz6d1d18imhpy30xasa6q48gls6xivyph")))

(define-public crate-trans-gen-go-0.3.0-alpha.1 (c (n "trans-gen-go") (v "0.3.0-alpha.1") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1ig1ggpwf1bfzl4a6lvdlfmm2lqxix6nwhq406x6m64zzsfingn3")))

(define-public crate-trans-gen-go-0.3.0-alpha.2 (c (n "trans-gen-go") (v "0.3.0-alpha.2") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1j15i0cni9fizj62qhvsblrs83aqhb2h6gh8ypf4wdy736yp1rmg")))

