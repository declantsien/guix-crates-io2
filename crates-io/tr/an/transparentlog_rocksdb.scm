(define-module (crates-io tr an transparentlog_rocksdb) #:use-module (crates-io))

(define-public crate-transparentlog_rocksdb-0.0.1 (c (n "transparentlog_rocksdb") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maybe-owned") (r "^0.3.4") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.0.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "transparentlog_core") (r "^0.0.1") (d #t) (k 0)))) (h "1hky2djamk7vkg3ib8ir3xbhvmm7c5aha3a2pc6iaj2cc54ncn8d") (r "1.58.1")))

