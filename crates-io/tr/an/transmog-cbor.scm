(define-module (crates-io tr an transmog-cbor) #:use-module (crates-io))

(define-public crate-transmog-cbor-0.1.0-dev.1 (c (n "transmog-cbor") (v "0.1.0-dev.1") (d (list (d (n "ciborium") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.1") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.1") (f (quote ("test-util"))) (d #t) (k 2)))) (h "07gywsf8q2b38mz9p9n9lv64wqvmrc66qx8rj9l8xfm4yigr4fk1")))

(define-public crate-transmog-cbor-0.1.0-dev.2 (c (n "transmog-cbor") (v "0.1.0-dev.2") (d (list (d (n "ciborium") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.2") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.2") (f (quote ("test-util"))) (d #t) (k 2)))) (h "11ylpjvk8al5amk6dg87l3gpkhnqpgcym6rbjcl5hdcx84cv5clx")))

(define-public crate-transmog-cbor-0.1.0 (c (n "transmog-cbor") (v "0.1.0") (d (list (d (n "ciborium") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (f (quote ("test-util"))) (d #t) (k 2)))) (h "0dxsw5z1gp7x1aflg7il2hsb3w5xssz3vvvll96pxfc1gwg5l1mf")))

