(define-module (crates-io tr an transaction-pool) #:use-module (crates-io))

(define-public crate-transaction-pool-1.9.0 (c (n "transaction-pool") (v "1.9.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.2") (f (quote ("heapsizeof"))) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)))) (h "0mzdl0yv1qpclhq8rx5a9ybky6844cwwzjj5mls9r7rqvwskhc13")))

(define-public crate-transaction-pool-1.12.0 (c (n "transaction-pool") (v "1.12.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)) (d (n "trace-time") (r "^0.1") (d #t) (k 0)))) (h "09kb8smqsmwjfv023ljbjlzl2x4pvp8fb03ix5b20a7bmlx0fqfx")))

(define-public crate-transaction-pool-1.12.1 (c (n "transaction-pool") (v "1.12.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)) (d (n "trace-time") (r "^0.1") (d #t) (k 0)))) (h "05rbsqd68dzrnz135f1z0cv3cmpn0apv6gl9k55anjfz7mkzn7my")))

(define-public crate-transaction-pool-1.12.3 (c (n "transaction-pool") (v "1.12.3") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)) (d (n "trace-time") (r "^0.1") (d #t) (k 0)))) (h "0rxkyb76kx4iiz91hp3aqnjvb54v61wgk5sg4vlx6fghv54w07qg")))

(define-public crate-transaction-pool-1.13.2 (c (n "transaction-pool") (v "1.13.2") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)) (d (n "trace-time") (r "^0.1") (d #t) (k 0)))) (h "0c0s5qy1sj2n4c96c66kgwhaclvs0r2sqmi0yvi5fjj0x878gf7x")))

(define-public crate-transaction-pool-1.13.3 (c (n "transaction-pool") (v "1.13.3") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "trace-time") (r "^0.1") (d #t) (k 0)))) (h "1lpivr72bj6fxmayd44vg5is6xz61cdgajxgszqmhhxi4r8nx1p5")))

(define-public crate-transaction-pool-2.0.0 (c (n "transaction-pool") (v "2.0.0") (d (list (d (n "ethereum-types") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "trace-time") (r "^0.1") (d #t) (k 0)))) (h "0rpnqq21zzqz3y6layfp053y340482ichfyhkpjac6lk4cqvvn7q")))

(define-public crate-transaction-pool-2.0.1 (c (n "transaction-pool") (v "2.0.1") (d (list (d (n "ethereum-types") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "trace-time") (r "^0.1") (d #t) (k 0)))) (h "1d246paq99zncbmifryiqafg52f6yg85shdrx8xmqy1j5r4dqjj5")))

(define-public crate-transaction-pool-2.0.2 (c (n "transaction-pool") (v "2.0.2") (d (list (d (n "ethereum-types") (r "^0.8.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "trace-time") (r "^0.1") (d #t) (k 0)))) (h "0qwnnpb6x87znn2ygpaqihnwzjilj1b6157qv3kpia0iqwjfxfyq")))

(define-public crate-transaction-pool-2.0.3 (c (n "transaction-pool") (v "2.0.3") (d (list (d (n "ethereum-types") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "trace-time") (r "^0.1") (d #t) (k 0)))) (h "016xfflqw0dg006nfqhml8ahfgz1m66f1f4kzx2a1fqj8jchdnsm")))

