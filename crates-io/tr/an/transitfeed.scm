(define-module (crates-io tr an transitfeed) #:use-module (crates-io))

(define-public crate-transitfeed-0.1.0 (c (n "transitfeed") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.1") (d #t) (k 0)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "0hgqds72rgz61fkbimdccq6j2q4glwxk0rd8z198ia8c1dz25ncb")))

(define-public crate-transitfeed-0.1.1 (c (n "transitfeed") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.1") (d #t) (k 0)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "1lv628vnysxrpq35yj4g8v6lsdza5nxbqajiyckab2flwk1hpw6z")))

(define-public crate-transitfeed-0.1.2 (c (n "transitfeed") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.1") (d #t) (k 0)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "0d0s8di0n2lfyzmhg4kzxajgapdpmvxqp6k2qmi7xqrm2mgzxvc3")))

(define-public crate-transitfeed-0.1.3 (c (n "transitfeed") (v "0.1.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "0glk89hhvz3gd38kpj8xwjn4mb2dw5q8j66pkxrfhbda6n1n2n5g")))

(define-public crate-transitfeed-0.1.4 (c (n "transitfeed") (v "0.1.4") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 2)))) (h "14fvkbv1i9b3j2f5lxvyz1x3ayfmqasfws6kj090zx2d1zyy2rqy")))

(define-public crate-transitfeed-0.2.0 (c (n "transitfeed") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "0slwr6d9bd2vrx2nv6q8i3a98pvdj21kcydmq4qdp3bg99n5sqpx")))

(define-public crate-transitfeed-0.3.0 (c (n "transitfeed") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "10f0rxlgr0m92iljcyq2bl0vmlm5r29qpc9bi66nd1xm0g0q7h40")))

