(define-module (crates-io tr an transliterate) #:use-module (crates-io))

(define-public crate-transliterate-0.1.0 (c (n "transliterate") (v "0.1.0") (d (list (d (n "bogobble") (r "^0.1.1") (d #t) (k 0)))) (h "099x2cri56bicgadi5disz9xlzznzyinyfbw5shljw5w2hyl6b1i")))

(define-public crate-transliterate-0.1.1 (c (n "transliterate") (v "0.1.1") (d (list (d (n "bogobble") (r "^0.1.1") (d #t) (k 0)))) (h "1cg6ri5ax5nwwans8zdfznzm6z1s7441g45zbb91f6bd9n6cls03")))

