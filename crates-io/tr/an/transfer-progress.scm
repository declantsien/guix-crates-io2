(define-module (crates-io tr an transfer-progress) #:use-module (crates-io))

(define-public crate-transfer-progress-0.1.0 (c (n "transfer-progress") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "progress-streams") (r "^1.1.0") (d #t) (k 0)))) (h "0398zmw3vdbxpyfsbqfmfbr7jbf3p3gzgm0rs4s9gp9cngjlpw0r") (f (quote (("default" "bytesize"))))))

(define-public crate-transfer-progress-0.1.1 (c (n "transfer-progress") (v "0.1.1") (d (list (d (n "bytesize") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "progress-streams") (r "^1.1.0") (d #t) (k 0)))) (h "095957vjlxnxahylcl76ygig0k0hwvxcc3k1jvbhwh4ch5sx6f5c") (f (quote (("default" "bytesize"))))))

(define-public crate-transfer-progress-0.1.2 (c (n "transfer-progress") (v "0.1.2") (d (list (d (n "bytesize") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "progress-streams") (r "^1.1.0") (d #t) (k 0)))) (h "1ibfivwpdyksfwlmiywiizfzxwwbjfyv38drybzf59v3f0c5n8cl") (f (quote (("default" "bytesize"))))))

