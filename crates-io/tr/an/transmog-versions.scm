(define-module (crates-io tr an transmog-versions) #:use-module (crates-io))

(define-public crate-transmog-versions-0.1.0-dev.1 (c (n "transmog-versions") (v "0.1.0-dev.1") (d (list (d (n "ordered-varint") (r "^0.1.0-dev.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.1") (d #t) (k 0)))) (h "043f46qlqhv07am6byazpz9fagm7sy69fayzh9inkfqr9bbhapa2")))

(define-public crate-transmog-versions-0.1.0-dev.2 (c (n "transmog-versions") (v "0.1.0-dev.2") (d (list (d (n "ordered-varint") (r "^0.1.0-dev.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0-dev.2") (d #t) (k 0)))) (h "15qfw3s3a85q0wixwy9m4py8y62b14d92w5cnq2n33j7zhvr5637")))

(define-public crate-transmog-versions-0.1.0 (c (n "transmog-versions") (v "0.1.0") (d (list (d (n "ordered-varint") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (d #t) (k 0)))) (h "0x4qq4ab96b8h6d2504lh1yfbccmdgh4cbqnykwd9fajq3big4a2")))

(define-public crate-transmog-versions-0.1.1 (c (n "transmog-versions") (v "0.1.1") (d (list (d (n "ordered-varint") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "transmog") (r "^0.1.0") (d #t) (k 0)))) (h "0p0k2jh4xl332zwbr9vbk59id5bnz7831nvrzdg0rv5x85kzwl49")))

