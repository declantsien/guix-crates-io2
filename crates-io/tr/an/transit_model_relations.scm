(define-module (crates-io tr an transit_model_relations) #:use-module (crates-io))

(define-public crate-transit_model_relations-0.1.0 (c (n "transit_model_relations") (v "0.1.0") (d (list (d (n "derivative") (r "^1.0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "transit_model_collection") (r "^0.1") (d #t) (k 0)) (d (n "transit_model_procmacro") (r "^0.1") (d #t) (k 0)))) (h "03c0jxxw76l3dafwq0ssvzq78ywrhvci2r4jck94pgbg05qd8s73")))

(define-public crate-transit_model_relations-0.1.1 (c (n "transit_model_relations") (v "0.1.1") (d (list (d (n "derivative") (r "^1.0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "transit_model_collection") (r "^0.1") (d #t) (k 0)) (d (n "transit_model_procmacro") (r "^0.1") (d #t) (k 2)))) (h "0s2h2y8l7pbpjdniadbn8dvmc7dl7rsfx2x1k5gwmi7zyvinis0f")))

(define-public crate-transit_model_relations-0.1.2 (c (n "transit_model_relations") (v "0.1.2") (d (list (d (n "derivative") (r "^1.0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "transit_model_procmacro") (r "^0.1") (d #t) (k 2)) (d (n "typed_index_collection") (r "^1") (d #t) (k 0)))) (h "0ab0ssglcb94if9q4v1r8pbmv1mc6m7byhjfq3brig3s5ppipaxv")))

