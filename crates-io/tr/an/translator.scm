(define-module (crates-io tr an translator) #:use-module (crates-io))

(define-public crate-translator-0.1.0 (c (n "translator") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (d #t) (k 0)))) (h "1zpraszxbz5j67isv1q2cb5xh4rrncnamxvx6bm1hw1325gssalm")))

(define-public crate-translator-0.1.1 (c (n "translator") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (d #t) (k 0)))) (h "1ky27plnchajcaq6dnq3casl8n0hfclz1ajhxp7w8aimc9nkd8g9")))

(define-public crate-translator-0.2.1 (c (n "translator") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (d #t) (k 0)))) (h "0f5bjqvqp328ydfl8a6z2pmml15ivphblyywkr9550926za8c1c1")))

(define-public crate-translator-0.3.0 (c (n "translator") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (d #t) (k 0)))) (h "0dc5ri6is3bsncv3bizhv2f3mkq24gcq7rk8iimvpz0xz5kjh7h2")))

(define-public crate-translator-0.3.1 (c (n "translator") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (d #t) (k 0)))) (h "02ba1srxs0g3ksvq6yrvnxbanj6wy6m9hgz0pva98crlbingwfvs")))

