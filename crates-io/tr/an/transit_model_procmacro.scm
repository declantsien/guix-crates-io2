(define-module (crates-io tr an transit_model_procmacro) #:use-module (crates-io))

(define-public crate-transit_model_procmacro-0.1.0 (c (n "transit_model_procmacro") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0hhaij9csix89ync9kq16jka7njnb0cdyiiyzdmclch1inwc0cch")))

(define-public crate-transit_model_procmacro-0.1.1 (c (n "transit_model_procmacro") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "typed_index_collection") (r "^1") (d #t) (k 2)))) (h "0s9w0wp7vmr3x4qq7g9b2f3454gzmkiw82f9hp4f6ww5v29jpj0p")))

