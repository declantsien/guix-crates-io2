(define-module (crates-io tr an transmute) #:use-module (crates-io))

(define-public crate-transmute-0.1.0 (c (n "transmute") (v "0.1.0") (h "1xhvwb61j2dg9pm0m342j8gcgbly5wmfk0vjdjxcj6dzjrc9sjxj") (y #t)))

(define-public crate-transmute-0.1.1 (c (n "transmute") (v "0.1.1") (h "0zbgsm00kcxh2bnf5mr9a1xja95issrgxvi6hgwlbrjgzg5abgz6")))

