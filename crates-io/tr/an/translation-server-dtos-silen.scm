(define-module (crates-io tr an translation-server-dtos-silen) #:use-module (crates-io))

(define-public crate-translation-server-dtos-silen-0.1.0 (c (n "translation-server-dtos-silen") (v "0.1.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bdmzxpr1v2a1d8f2gqzp278x93xzlk56a8hkb1y7mj67pr2pbn6")))

(define-public crate-translation-server-dtos-silen-0.1.1 (c (n "translation-server-dtos-silen") (v "0.1.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gqznml6qhppqb5j011b11a8l2ygczskay7wlzgqchaljzfs6ad9")))

(define-public crate-translation-server-dtos-silen-0.1.2 (c (n "translation-server-dtos-silen") (v "0.1.2") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b4il3pnnd6y5gj8956if3nhgnf3z824d1q9vpqa631zkz0asdvq")))

(define-public crate-translation-server-dtos-silen-0.1.3 (c (n "translation-server-dtos-silen") (v "0.1.3") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14j2zwfmj039d9ccvfmym8f19zp5qdymcnap8mmspqsp7gvi204c")))

