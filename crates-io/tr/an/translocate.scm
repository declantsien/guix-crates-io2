(define-module (crates-io tr an translocate) #:use-module (crates-io))

(define-public crate-translocate-0.1.0 (c (n "translocate") (v "0.1.0") (d (list (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "13aj03anz433c88vhijkhvdb5pzm8hy7b3jz9kpmzf42w40xliqb")))

(define-public crate-translocate-0.2.0 (c (n "translocate") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rh6y3i08vmr7pk1wh9m1mrs5dcg400ajdw03xgkh06x4pi0zk6s")))

(define-public crate-translocate-0.3.0 (c (n "translocate") (v "0.3.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "1ph9i2dq17i6xvpjkh8jq3yq0y1vbqsszw0qwial03zpp9vkmq5q")))

(define-public crate-translocate-0.5.0 (c (n "translocate") (v "0.5.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "14palcb4w4l345065r0a245r0y9i7xsijf0265ydfn7ip9lfsl8c") (y #t)))

(define-public crate-translocate-0.5.1 (c (n "translocate") (v "0.5.1") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "1qbk1ir4hxi4f5waaqls2hzrffsbzd6d38k1k9r6iixqwblqpzxp")))

(define-public crate-translocate-0.5.2 (c (n "translocate") (v "0.5.2") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "186r343asvc5rlad80inialll43abnsii8mzs5g55b5nl4a71qjy")))

(define-public crate-translocate-0.6.0 (c (n "translocate") (v "0.6.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "1s6wgv2qkwnh4ab805ffl3ykqx8lzjwfkar8hkrjr8z5v8xn3svw")))

(define-public crate-translocate-0.7.0 (c (n "translocate") (v "0.7.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "14x21wriswqyn3dp13g22w511fgmhdzj10axy0fp214bcmycx8iz") (r "1.63")))

(define-public crate-translocate-0.8.0 (c (n "translocate") (v "0.8.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "0l7b59wb0p4g2dp4q5zxbs9b90vchclb74v9q53r9dxpb5q2wrgb") (r "1.63")))

