(define-module (crates-io tr an translate_core) #:use-module (crates-io))

(define-public crate-translate_core-0.1.0 (c (n "translate_core") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)))) (h "0363k0mvqa50156l43bpzdl0wxc3whhcdlss1h9pv6b1crwm0gfx")))

(define-public crate-translate_core-0.1.1 (c (n "translate_core") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)))) (h "1liyk68f063vi7kngb8vv6bq738z9n34hx4c161m7xqrhpanbw9g")))

(define-public crate-translate_core-0.1.2 (c (n "translate_core") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)))) (h "132zvfdhdb4gqx0mrwnimsrvvpgb95705qisn1s4pr8km73z0rhg")))

(define-public crate-translate_core-0.1.21 (c (n "translate_core") (v "0.1.21") (d (list (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)))) (h "08363v1463qqqzj2hjk21i4la1fj3m4laa8qqs5y0m5six4f7sc1")))

