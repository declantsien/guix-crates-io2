(define-module (crates-io tr an trans-gen-scala) #:use-module (crates-io))

(define-public crate-trans-gen-scala-0.1.0 (c (n "trans-gen-scala") (v "0.1.0") (d (list (d (n "trans-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "0pxjp2i30xa74r12kaxvj1si8091lpxry1x2xs84hjs7q0wh0r69")))

(define-public crate-trans-gen-scala-0.3.0-alpha.0 (c (n "trans-gen-scala") (v "0.3.0-alpha.0") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0nvnd6jm6h5j8af8nndi30s5blqzkgi034905ydflwyqbvykmnrw")))

(define-public crate-trans-gen-scala-0.3.0-alpha.1 (c (n "trans-gen-scala") (v "0.3.0-alpha.1") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0cghknb7kh9x31vqyv5fy127mx5bdisnflnvjvfd20i7c98kg3lx")))

(define-public crate-trans-gen-scala-0.3.0-alpha.2 (c (n "trans-gen-scala") (v "0.3.0-alpha.2") (d (list (d (n "trans-gen-core") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "038iw03qcxzncnwjrhq5jykprci0k3dmk27gllfid1f580mgbmv7")))

