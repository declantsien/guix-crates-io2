(define-module (crates-io tr an transgender-flag) #:use-module (crates-io))

(define-public crate-transgender-flag-0.1.0 (c (n "transgender-flag") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "093s9c3936xdb6xncb3c48fyqya0zqndf2h7br85d6d8x2lnpzf3")))

