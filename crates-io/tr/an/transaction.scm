(define-module (crates-io tr an transaction) #:use-module (crates-io))

(define-public crate-transaction-0.1.0 (c (n "transaction") (v "0.1.0") (d (list (d (n "mdo") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1aaw069iyvqm01kzvi2phfhg1xmd6j7azvdzh0zv5yfzvlbgd38s")))

(define-public crate-transaction-0.2.0 (c (n "transaction") (v "0.2.0") (d (list (d (n "mdo") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1322s2irljfhlmka1wqzfl0n7v2l7lrjvhz18b5xw7rwj1ckw2z7") (y #t)))

(define-public crate-transaction-0.2.1 (c (n "transaction") (v "0.2.1") (d (list (d (n "mdo") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1ivndamz6ybakwm41w62zpfq415fq7afrgwsany2hfr716rmphfm")))

