(define-module (crates-io tr ib tribufu-types) #:use-module (crates-io))

(define-public crate-tribufu-types-0.1.0 (c (n "tribufu-types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("serde" "rustc-serialize"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "mintaka-error") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)))) (h "0ycrwknzb1p3p45323plp33qyx8dn7jcwayhsh9p7j8ijdy78ib8")))

