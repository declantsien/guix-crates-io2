(define-module (crates-io tr ib tribe) #:use-module (crates-io))

(define-public crate-tribe-0.1.0 (c (n "tribe") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0pdlf5rxk2j8d1mzq0zvgj3pc2mfq3j42wk8byvgcpy9l6g9pg4j")))

(define-public crate-tribe-0.2.0 (c (n "tribe") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "handlebars") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0wa99hn1n2mdijj8m2qgcrizzfk5lcxh33nydhfkdnqxbg1w0qxs")))

