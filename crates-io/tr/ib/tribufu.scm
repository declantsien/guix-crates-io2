(define-module (crates-io tr ib tribufu) #:use-module (crates-io))

(define-public crate-tribufu-0.0.1 (c (n "tribufu") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xf5cmnjy85999cyaykyaaflg13jj5x29ygr2rdqlb8p8yv8lkcm")))

(define-public crate-tribufu-0.0.2 (c (n "tribufu") (v "0.0.2") (h "0d8m87jsz19bqm41dppzdhxvgdjdlfp82k3bznfi1cmlshp9a498")))

(define-public crate-tribufu-0.1.0 (c (n "tribufu") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tribufu-api") (r "^0.1.0") (d #t) (k 0)) (d (n "tribufu-constants") (r "^0.1.0") (d #t) (k 0)) (d (n "tribufu-types") (r "^0.1.0") (d #t) (k 0)))) (h "191yc0r35yw5cplaxgvl4sbd83cfwmbrwi1lz9hkj0kd9dpl9dra")))

