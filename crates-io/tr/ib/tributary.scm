(define-module (crates-io tr ib tributary) #:use-module (crates-io))

(define-public crate-tributary-0.1.0 (c (n "tributary") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1lh6z4dgfsg1gjs2ywmxyz49j4jx50454n86dkdh6bcaar3b9qnw") (f (quote (("coverage"))))))

