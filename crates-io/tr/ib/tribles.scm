(define-module (crates-io tr ib tribles) #:use-module (crates-io))

(define-public crate-tribles-0.1.0 (c (n "tribles") (v "0.1.0") (h "0bb00km2wy7myad2nscfczd956w7fgn4jjdmh7kqpzyp9542dbks")))

(define-public crate-tribles-0.1.1 (c (n "tribles") (v "0.1.1") (h "0xyzkpd43mvagbx5x6h4ww71jpw8svixvn0611f02dxr96zzrck2")))

(define-public crate-tribles-0.1.2 (c (n "tribles") (v "0.1.2") (h "0pbrysn48laj2r6r56w7g8bra7hiby4prnac1y7w1zb8gk4kj35d")))

