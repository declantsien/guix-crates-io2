(define-module (crates-io tr ad trade_aggregation_derive) #:use-module (crates-io))

(define-public crate-trade_aggregation_derive-0.2.0 (c (n "trade_aggregation_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0v196zxvyi3wik861a34na7w1q1r2nw8yassj5pk66xvgi28a615")))

(define-public crate-trade_aggregation_derive-0.2.1 (c (n "trade_aggregation_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1fbvicqsjmslgnf52z1m1gdqm5xgsrsnk5g3ja26v4bl9vns8cg2")))

(define-public crate-trade_aggregation_derive-0.3.0 (c (n "trade_aggregation_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1p545iiyjwggx6k7npvm8mqq5b5whjiz60ad56i9nv8axbzdls0g")))

(define-public crate-trade_aggregation_derive-0.3.1 (c (n "trade_aggregation_derive") (v "0.3.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10n91qdzzlrwwl5ihndrybaj6zlxxkvz6jn42zw6k5rrcq1wgjg0")))

(define-public crate-trade_aggregation_derive-0.3.2 (c (n "trade_aggregation_derive") (v "0.3.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nsn0vv0cjkyxjhrhhy3hzl33hipp0kkhm4c0gllwd3g7wbfmbzs")))

(define-public crate-trade_aggregation_derive-0.4.0 (c (n "trade_aggregation_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1y1cyjqq278yivjn410vsrn8ljd2gjaa7xv4cxy59s31k274m60w")))

