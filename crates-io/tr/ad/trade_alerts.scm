(define-module (crates-io tr ad trade_alerts) #:use-module (crates-io))

(define-public crate-trade_alerts-0.1.0 (c (n "trade_alerts") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "supabase_rs") (r "^0.2.3") (d #t) (k 0)))) (h "061dgd99hs0rdd6lzrplk1fzl2lqiaw6g2vs8jylcjv80xmsy54h")))

(define-public crate-trade_alerts-0.1.1 (c (n "trade_alerts") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "supabase_rs") (r "^0.2.3") (d #t) (k 0)))) (h "1fx837g6cmmwifmgl9yhsm5cb02cg4p3mh0z4iblvvksldb0jc16")))

(define-public crate-trade_alerts-0.1.2 (c (n "trade_alerts") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "supabase_rs") (r "^0.2.3") (d #t) (k 0)))) (h "05rjg56z5g0nvccfalmx2ilq7jsd5bpi3ns36ddd3v5414wjj87w")))

(define-public crate-trade_alerts-0.1.3 (c (n "trade_alerts") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "supabase_rs") (r "^0.2.3") (d #t) (k 0)))) (h "1nial0j1pxcm7rq1gvkshqipdfj558zcpjjclfpgr3mqzzq0x7r9")))

(define-public crate-trade_alerts-0.1.4 (c (n "trade_alerts") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "supabase_rs") (r "^0.2.3") (d #t) (k 0)))) (h "19jvq6857ry0wq99bz9l2h8n275h4r1y09c8l6b0ghnfdcnl75df")))

