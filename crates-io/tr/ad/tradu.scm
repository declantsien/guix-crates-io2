(define-module (crates-io tr ad tradu) #:use-module (crates-io))

(define-public crate-tradu-0.1.0 (c (n "tradu") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0x356gqn5dhd3r04243bkm7kl74vvrjs9y4psrx5gkmyl68kkra4")))

