(define-module (crates-io tr ad tradukisto) #:use-module (crates-io))

(define-public crate-tradukisto-0.1.0 (c (n "tradukisto") (v "0.1.0") (h "0xwa8cfmbw7l6p6kmpqnxi76cayknr0ii6im2n7g4l71jr7nb4rn")))

(define-public crate-tradukisto-0.1.1 (c (n "tradukisto") (v "0.1.1") (h "0r9kpfp9r29xikm4b6nbvq3imlrjggpfq68r8yszza28fj680h0r")))

