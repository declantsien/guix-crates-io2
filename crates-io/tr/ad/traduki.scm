(define-module (crates-io tr ad traduki) #:use-module (crates-io))

(define-public crate-traduki-0.1.0 (c (n "traduki") (v "0.1.0") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "06vkjlzd975wvxyhbb8sablxhc7gyx98y3p0g7wibsw7r0h260xl")))

(define-public crate-traduki-0.1.1 (c (n "traduki") (v "0.1.1") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0iwqry5jqapnvmb0zd1yy8yqx0lcffmxyij65a4b9jiz52wp4mad")))

(define-public crate-traduki-0.1.2 (c (n "traduki") (v "0.1.2") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "00kr95x28nwvw3pzxrdfnxsmj7h64wrqbbmhb55ysxqbpxzqmrxw")))

