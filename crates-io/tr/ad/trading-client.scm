(define-module (crates-io tr ad trading-client) #:use-module (crates-io))

(define-public crate-trading-client-0.1.0 (c (n "trading-client") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "1ywh7bnajdxnj4lhjkn17a0mks29h7kx6b3fhq4vvapb0xxyhf18")))

(define-public crate-trading-client-0.1.1 (c (n "trading-client") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "05fkimxj6dgpj9dlpsmzrd5ghdd9v19l9zcjzl42v7xc153r08lf")))

