(define-module (crates-io tr ad trade_vision) #:use-module (crates-io))

(define-public crate-trade_vision-0.1.0 (c (n "trade_vision") (v "0.1.0") (h "0n9d9zn3w94k4nllryc90kkm2aa12bg4rranwl2b9kypyraj0bci") (y #t)))

(define-public crate-trade_vision-0.1.1 (c (n "trade_vision") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.19.0") (f (quote ("rustls-tls-native-roots"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.19.0") (f (quote ("rustls-tls-native-roots"))) (d #t) (k 0)))) (h "150zl6vj1xhcyq9b76lajbyac55fp3k1r8c1y511f2l6dk45y4ds")))

