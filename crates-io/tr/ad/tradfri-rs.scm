(define-module (crates-io tr ad tradfri-rs) #:use-module (crates-io))

(define-public crate-tradfri-rs-0.1.0 (c (n "tradfri-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "17szqc0pwggw018h6vrnhx40ca7zamn5rl5c49vf6jpgpwipk0kx")))

