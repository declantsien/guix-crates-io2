(define-module (crates-io tr ad trading_sessions) #:use-module (crates-io))

(define-public crate-trading_sessions-0.1.0 (c (n "trading_sessions") (v "0.1.0") (d (list (d (n "polars") (r "^0.37") (f (quote ("lazy"))) (d #t) (k 0)))) (h "1mbvspvl57qygsw0scq147mi10ibhmrmw1zrnv1b1i62wg8izvys")))

(define-public crate-trading_sessions-0.1.1 (c (n "trading_sessions") (v "0.1.1") (d (list (d (n "polars") (r "^0.37") (f (quote ("lazy"))) (d #t) (k 0)))) (h "1aghidbmfzq42r89g1wxrdvdx9lq1159hj7d91dd71lhdch4bfp9")))

(define-public crate-trading_sessions-0.1.2 (c (n "trading_sessions") (v "0.1.2") (d (list (d (n "polars") (r "^0.35.0") (f (quote ("lazy"))) (d #t) (k 0)))) (h "1ax0av6qaig6jwhr6azz45f03x3ix0y24pbhsg1hjjf8vmddj24b")))

(define-public crate-trading_sessions-0.1.3 (c (n "trading_sessions") (v "0.1.3") (d (list (d (n "polars") (r "^0.35.0") (f (quote ("lazy"))) (d #t) (k 0)))) (h "1v7k3jr7lnwhw6jm607i9s8ilhf9k88aa43bpdv8in9fncq3x0gl")))

