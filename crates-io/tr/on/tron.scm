(define-module (crates-io tr on tron) #:use-module (crates-io))

(define-public crate-tron-0.0.0 (c (n "tron") (v "0.0.0") (h "1njyg7ih48pw5kkf44kfs80ndrj52206m7zbwa6bbaxr9ls3jx0k")))

(define-public crate-tron-0.1.3 (c (n "tron") (v "0.1.3") (h "1fj0sjy622vnk3xwsw5563siypdsx7c6wzjmij354mz1cqbi0r5f")))

