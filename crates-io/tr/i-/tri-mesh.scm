(define-module (crates-io tr i- tri-mesh) #:use-module (crates-io))

(define-public crate-tri-mesh-0.1.0 (c (n "tri-mesh") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)))) (h "0lcxqra7gnvw8yl3dm4x90n56jdyhgwshhncz110lsgny69r1w9d")))

(define-public crate-tri-mesh-0.1.1 (c (n "tri-mesh") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)))) (h "1wv78lyc1hrh2q1y10x38md3gm4vm5qmipi1wxi3yldiihr4phrc")))

(define-public crate-tri-mesh-0.1.2 (c (n "tri-mesh") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)))) (h "02kr2qy8b41ar7z5kcdknblaghc2mh0ybzknvagbpqsg329vbq1d")))

(define-public crate-tri-mesh-0.2.0 (c (n "tri-mesh") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "dust") (r "^0.1.0") (d #t) (k 2)) (d (n "wavefront_obj") (r "^5.1.0") (d #t) (k 0)))) (h "02dzy61l5j17d2ygzazf7vknam21qvs9y5546g09wgwks4glhsng")))

(define-public crate-tri-mesh-0.3.0 (c (n "tri-mesh") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "dust") (r "^0.1.0") (d #t) (k 2)) (d (n "wavefront_obj") (r "^5.1.0") (d #t) (k 0)))) (h "0vq6jzs5nhp4pivbz24q2brf00blcfxysh3rfw8lb2niy4hscxhs")))

(define-public crate-tri-mesh-0.4.0 (c (n "tri-mesh") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "dust") (r "^0.1.0") (d #t) (k 2)) (d (n "wavefront_obj") (r "^5.1.0") (d #t) (k 0)))) (h "1950b3zijq5fdbnir6zxha746sjra2np3i1pskjlh6gvl8cv0fws")))

(define-public crate-tri-mesh-0.5.0 (c (n "tri-mesh") (v "0.5.0") (d (list (d (n "bincode") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "dust") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wavefront_obj") (r "^5.1") (o #t) (d #t) (k 0)))) (h "19g80s8f6kcbgv17p0y6qk46lxif2ddms35jan93y4w9fify1ihy") (f (quote (("obj-io" "wavefront_obj") ("default" "obj-io" "3d-io") ("3d-io" "serde" "bincode"))))))

(define-public crate-tri-mesh-0.6.0 (c (n "tri-mesh") (v "0.6.0") (d (list (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "three-d-asset") (r "^0.4") (d #t) (k 0)) (d (n "three-d-asset") (r "^0.4") (f (quote ("obj"))) (d #t) (k 2)))) (h "0khvs2al84i19fi8pga4wlbhnf8li3p8nwzkmsq0hf1fk7az6zl0") (f (quote (("default"))))))

