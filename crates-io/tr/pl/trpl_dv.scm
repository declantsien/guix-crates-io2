(define-module (crates-io tr pl trpl_dv) #:use-module (crates-io))

(define-public crate-trpl_dv-0.1.0 (c (n "trpl_dv") (v "0.1.0") (h "0wnmmp5vg72cgx6afbnzp6z5a1pbixd902c3h0gjizwa6hg4dj1l") (y #t)))

(define-public crate-trpl_dv-1.0.0 (c (n "trpl_dv") (v "1.0.0") (h "1zbbbr5x2zv8xy5ddkcgiqflh1z6ra97744ah413i32lh5azja3g")))

