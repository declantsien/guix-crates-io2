(define-module (crates-io tr pl trpl_cain) #:use-module (crates-io))

(define-public crate-trpl_cain-0.1.0 (c (n "trpl_cain") (v "0.1.0") (h "1yv18350k073wjf8gz93pyw8wjsnsh0vykgggimp40ppvc4amvbc")))

(define-public crate-trpl_cain-0.2.0 (c (n "trpl_cain") (v "0.2.0") (h "0r82g1xygixp390x1y52smnjwq1w5vxk9y86ac6w55y8zfzqpbg9")))

