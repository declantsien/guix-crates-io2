(define-module (crates-io tr ac tractor-macros) #:use-module (crates-io))

(define-public crate-tractor-macros-0.0.1 (c (n "tractor-macros") (v "0.0.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0im3fapdwb3i1094b2a5drynrvnb91mkcwgb5ik4w9pp0fagdn43")))

(define-public crate-tractor-macros-0.0.4 (c (n "tractor-macros") (v "0.0.4") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0nyqplhx7n8qr1rbhvdycia2yslv2x9ljxdkq3f4qns4knf0xl34")))

(define-public crate-tractor-macros-0.0.6 (c (n "tractor-macros") (v "0.0.6") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1sdiazkn1d2kxk4j0lj61n9nfplilwza453lgq6a3yhrknjhdpd9")))

