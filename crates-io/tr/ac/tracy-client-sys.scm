(define-module (crates-io tr ac tracy-client-sys) #:use-module (crates-io))

(define-public crate-tracy-client-sys-0.7.0 (c (n "tracy-client-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "04jh27ldfvf3ad11m51ff25wn46kapkpkr3f2slfmxzsznd1r81s")))

(define-public crate-tracy-client-sys-0.7.1 (c (n "tracy-client-sys") (v "0.7.1") (d (list (d (n "cc") (r "^1") (k 1)))) (h "0b03h9a9h7hq4vzc0fn1v8b1gkg3da3s3nwlbbmy6jf997hnzj45") (f (quote (("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable"))))))

(define-public crate-tracy-client-sys-0.7.2 (c (n "tracy-client-sys") (v "0.7.2") (d (list (d (n "cc") (r "^1") (k 1)))) (h "13pqkk2jsrxjpnzs3z387fzwfsp2mm0w03z78pmwz3zh2f82jpx1") (f (quote (("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable")))) (y #t)))

(define-public crate-tracy-client-sys-0.8.0 (c (n "tracy-client-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "0xg45h7wrn1xbkvzvpbay1df1w1ixwvi054bg20qkfm9gnrnh9ms") (f (quote (("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable"))))))

(define-public crate-tracy-client-sys-0.8.1 (c (n "tracy-client-sys") (v "0.8.1") (d (list (d (n "cc") (r "^1") (k 1)))) (h "1b8j17a7w0a5bjg9rp997qpkjvd58yx0jrvq5vdzaqllxamaa7xb") (f (quote (("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable")))) (y #t)))

(define-public crate-tracy-client-sys-0.9.0 (c (n "tracy-client-sys") (v "0.9.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "17r7479p1l5zdd3wsl8iwbbnf3m9mqz67wkw4j6qb46kv8v4czn0") (f (quote (("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable"))))))

(define-public crate-tracy-client-sys-0.10.0 (c (n "tracy-client-sys") (v "0.10.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "1wdahvk9lc6sg0sknjgn2nixfp0wfl0g89llb5kam23c6rjna6rv") (f (quote (("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable"))))))

(define-public crate-tracy-client-sys-0.11.0 (c (n "tracy-client-sys") (v "0.11.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "18l67jx96m5bd3gq6inl59q5fhzcj9maiaxm588bcmc1hzvz1rhy") (f (quote (("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable"))))))

(define-public crate-tracy-client-sys-0.12.0 (c (n "tracy-client-sys") (v "0.12.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "1c9wi6lm46dq4vdbm8bf8xr6fv9ffq40whw20d335kha4nlv672q") (f (quote (("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable"))))))

(define-public crate-tracy-client-sys-0.13.0 (c (n "tracy-client-sys") (v "0.13.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "0a8zhp4vnk2l4bmd1njp3r4wkbv9ky7iqg7lsnwn4vzn2bwfdgj7") (f (quote (("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable"))))))

(define-public crate-tracy-client-sys-0.14.0 (c (n "tracy-client-sys") (v "0.14.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "0krqwx3id4z22wbzps6b6ygrq44kr6ss6ak35q58f38rn9b424mc") (f (quote (("ondemand") ("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable"))))))

(define-public crate-tracy-client-sys-0.15.0 (c (n "tracy-client-sys") (v "0.15.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "0dc4vaqiy25rvak42d4rirknk8582rk5vprh0dq8xkhdz1cjvahh") (f (quote (("ondemand") ("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable"))))))

(define-public crate-tracy-client-sys-0.16.0 (c (n "tracy-client-sys") (v "0.16.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "0v9bnarxg89jp42zdfvmicj3a03rham1sjqapdzcwihdgzky89k2") (f (quote (("ondemand") ("noexit") ("lowres-timer") ("enable") ("delayed-init") ("default" "enable"))))))

(define-public crate-tracy-client-sys-0.17.0 (c (n "tracy-client-sys") (v "0.17.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "0frzv3absk1kkmkjx439ly03l495c4rh52ss7zlidzyaxkgvj4lh") (f (quote (("timer-fallback") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("ondemand") ("fibers") ("enable") ("default" "enable" "system-tracing" "context-switch-tracing" "sampling" "code-transfer" "broadcast") ("context-switch-tracing") ("code-transfer") ("broadcast"))))))

(define-public crate-tracy-client-sys-0.17.1 (c (n "tracy-client-sys") (v "0.17.1") (d (list (d (n "cc") (r "^1") (k 1)))) (h "0y8ws4rxh9pl4pwvwb1il3q7w4cmdc2702y072xphc78ala0538p") (f (quote (("timer-fallback") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("ondemand") ("fibers") ("enable") ("default" "enable" "system-tracing" "context-switch-tracing" "sampling" "code-transfer" "broadcast") ("context-switch-tracing") ("code-transfer") ("broadcast"))))))

(define-public crate-tracy-client-sys-0.18.0 (c (n "tracy-client-sys") (v "0.18.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "17779k11imfr73fnfavz5nhgd4z9s09asnl7kk7m65a3lg2zyb85") (f (quote (("timer-fallback") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("ondemand") ("fibers") ("enable") ("default" "enable" "system-tracing" "context-switch-tracing" "sampling" "code-transfer" "broadcast") ("context-switch-tracing") ("code-transfer") ("broadcast"))))))

(define-public crate-tracy-client-sys-0.19.0 (c (n "tracy-client-sys") (v "0.19.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "1ifrc4rbjzgwn2a7xr4p9i463i2zg5nmpzb9fismzz1w7ahdpjvz") (f (quote (("timer-fallback") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("ondemand") ("fibers") ("enable") ("default" "enable" "system-tracing" "context-switch-tracing" "sampling" "code-transfer" "broadcast") ("context-switch-tracing") ("code-transfer") ("broadcast"))))))

(define-public crate-tracy-client-sys-0.20.0 (c (n "tracy-client-sys") (v "0.20.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "1xnnz70gpmv7i3g12fkrn9r0wdidv3w39cd0cnz163g443mqmkz8") (f (quote (("timer-fallback") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("ondemand") ("manual-lifetime") ("fibers") ("enable") ("delayed-init") ("default" "enable" "system-tracing" "context-switch-tracing" "sampling" "code-transfer" "broadcast") ("context-switch-tracing") ("code-transfer") ("broadcast"))))))

(define-public crate-tracy-client-sys-0.21.0 (c (n "tracy-client-sys") (v "0.21.0") (d (list (d (n "cc") (r "^1") (k 1)))) (h "06bqxy2z5l41j14z4y68i2wzsnsqd8hbcnzhdf5x0f9273ygb68d") (f (quote (("timer-fallback") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("ondemand") ("manual-lifetime") ("fibers") ("enable") ("delayed-init") ("default" "enable" "system-tracing" "context-switch-tracing" "sampling" "code-transfer" "broadcast") ("context-switch-tracing") ("code-transfer") ("broadcast"))))))

(define-public crate-tracy-client-sys-0.21.1 (c (n "tracy-client-sys") (v "0.21.1") (d (list (d (n "cc") (r "^1.0.82") (k 1)))) (h "1sy2fy39jbcxf4hginfcwxm6alma6afzwcqakgxr18557rln9rwh") (f (quote (("timer-fallback") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("ondemand") ("manual-lifetime") ("fibers") ("enable") ("delayed-init") ("default" "enable" "system-tracing" "context-switch-tracing" "sampling" "code-transfer" "broadcast") ("context-switch-tracing") ("code-transfer") ("broadcast"))))))

(define-public crate-tracy-client-sys-0.21.2 (c (n "tracy-client-sys") (v "0.21.2") (d (list (d (n "cc") (r "^1.0.82") (k 1)))) (h "0nj5h311wg4qkdw0v8s1390ad29597qxcvfp8135aj7h7bm1bf9c") (f (quote (("timer-fallback") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("ondemand") ("manual-lifetime") ("fibers") ("enable") ("delayed-init") ("default" "enable" "system-tracing" "context-switch-tracing" "sampling" "code-transfer" "broadcast" "callstack-inlines") ("context-switch-tracing") ("code-transfer") ("callstack-inlines") ("broadcast"))))))

(define-public crate-tracy-client-sys-0.22.0 (c (n "tracy-client-sys") (v "0.22.0") (d (list (d (n "cc") (r "^1.0.82") (k 1)))) (h "0xppip59nk9gminpj6lp1lwqgddc0zyazn80fd2p0ami3g6b3c1x") (f (quote (("timer-fallback") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("ondemand") ("manual-lifetime") ("fibers") ("enable") ("delayed-init") ("default" "enable" "system-tracing" "context-switch-tracing" "sampling" "code-transfer" "broadcast" "callstack-inlines") ("context-switch-tracing") ("code-transfer") ("callstack-inlines") ("broadcast"))))))

(define-public crate-tracy-client-sys-0.22.1 (c (n "tracy-client-sys") (v "0.22.1") (d (list (d (n "cc") (r "^1.0.83") (k 1)))) (h "17bc4zbsvix9vim2kw1vfn6y3zzc1spzg9vijqvf9c2147bpx307") (f (quote (("timer-fallback") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("ondemand") ("manual-lifetime") ("fibers") ("enable") ("delayed-init") ("default" "enable" "system-tracing" "context-switch-tracing" "sampling" "code-transfer" "broadcast" "callstack-inlines") ("context-switch-tracing") ("code-transfer") ("callstack-inlines") ("broadcast")))) (r "1.70.0")))

(define-public crate-tracy-client-sys-0.22.2 (c (n "tracy-client-sys") (v "0.22.2") (d (list (d (n "cc") (r "^1.0.83") (k 1)))) (h "10h8msq85b7rhfg2vg22g2iizbk4c6fcq0jiadad37gs1mhls44x") (f (quote (("timer-fallback") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("ondemand") ("manual-lifetime" "delayed-init") ("flush-on-exit") ("fibers") ("enable") ("delayed-init") ("default" "enable" "system-tracing" "context-switch-tracing" "sampling" "code-transfer" "broadcast" "callstack-inlines") ("context-switch-tracing") ("code-transfer") ("callstack-inlines") ("broadcast")))) (r "1.70.0")))

