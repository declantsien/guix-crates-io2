(define-module (crates-io tr ac tracing-rc-derive) #:use-module (crates-io))

(define-public crate-tracing-rc-derive-0.1.0 (c (n "tracing-rc-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "044kl4ahbv5whs5kj9wj1dkkwid6z23q8cifq27wx7sai5kl43rw")))

(define-public crate-tracing-rc-derive-0.1.1 (c (n "tracing-rc-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "130202si1wq6knsrk3ia35n6cg5j19gk1a953rj1d86kadi5vg3m") (f (quote (("sync"))))))

