(define-module (crates-io tr ac tracing_android_trace) #:use-module (crates-io))

(define-public crate-tracing_android_trace-0.1.0 (c (n "tracing_android_trace") (v "0.1.0") (d (list (d (n "android_trace") (r "^0.1.0") (t "cfg(target_os = \"android\")") (k 0)) (d (n "smallvec") (r "^1.13.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.32") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("std" "fmt" "smallvec"))) (k 0)))) (h "0imb6pz16vgpnmn4nbrwk71hxpq299ch9fvbpz9f9pbj9xz5w6bh") (f (quote (("default" "api_level_23") ("api_level_29" "android_trace/api_level_29") ("api_level_23" "android_trace/api_level_23")))) (r "1.77")))

