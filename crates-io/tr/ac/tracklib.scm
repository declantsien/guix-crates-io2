(define-module (crates-io tr ac tracklib) #:use-module (crates-io))

(define-public crate-tracklib-0.1.0 (c (n "tracklib") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.2") (d #t) (k 0)))) (h "1rdnz6za4syvcd8immv7rnl6lg3g54wan7kgp7bbij8hwklz3xlp")))

(define-public crate-tracklib-0.2.0 (c (n "tracklib") (v "0.2.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.2") (d #t) (k 0)))) (h "0lhx5srwxwaif4yw1xnkp7xkhkv3mq2b3mhzf1nawxb88vpi5i53")))

(define-public crate-tracklib-0.3.0 (c (n "tracklib") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.2") (d #t) (k 0)))) (h "08k2idy5zjbh3c256dlr578ci8gni2s9v384llp9v5hyfhjn1yjm")))

