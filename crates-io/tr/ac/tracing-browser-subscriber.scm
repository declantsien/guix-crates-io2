(define-module (crates-io tr ac tracing-browser-subscriber) #:use-module (crates-io))

(define-public crate-tracing-browser-subscriber-0.1.0 (c (n "tracing-browser-subscriber") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.35") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (f (quote ("registry"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.81") (d #t) (k 0)))) (h "12pnnbqij0i7z0d4c7bsy4wisc3k2iwhjf18h5ajpmkpj4k5x9c4")))

(define-public crate-tracing-browser-subscriber-0.2.0 (c (n "tracing-browser-subscriber") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1.35") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (f (quote ("registry"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 0)))) (h "1kdq0gcsifhgycn2wdmpd9lrzwxdsrbi9jkq4r6wd962dryv279w")))

