(define-module (crates-io tr ac tracing-slf4j) #:use-module (crates-io))

(define-public crate-tracing-slf4j-0.1.0 (c (n "tracing-slf4j") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 1)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1f29lfwq18a08g8lfa6jczk74m0yav791c77dpyncia4v9j2vyzl")))

