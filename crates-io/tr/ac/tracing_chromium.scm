(define-module (crates-io tr ac tracing_chromium) #:use-module (crates-io))

(define-public crate-tracing_chromium-0.1.0 (c (n "tracing_chromium") (v "0.1.0") (d (list (d (n "gettid") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tracing_facade") (r "^0.1") (d #t) (k 0)))) (h "1591hvabq1agbmshz5bdn643wx3a3qdbslkq4v0jq6a0k3s8vivg")))

