(define-module (crates-io tr ac tracy-gizmos-attributes) #:use-module (crates-io))

(define-public crate-tracy-gizmos-attributes-0.0.1 (c (n "tracy-gizmos-attributes") (v "0.0.1") (d (list (d (n "tracy-gizmos") (r "^0.0.5") (d #t) (k 2)))) (h "057l7305s1vjkc49zslaplpaj3fwbmf97v1mswrs89rcfbpx548m")))

(define-public crate-tracy-gizmos-attributes-0.0.2 (c (n "tracy-gizmos-attributes") (v "0.0.2") (d (list (d (n "tracy-gizmos") (r "0.0.*") (d #t) (k 2)))) (h "0k1f0jxpq6s5gnv4n71h2bp2cr6aj160kqs73k80vk94ymvyznhc")))

(define-public crate-tracy-gizmos-attributes-0.0.3 (c (n "tracy-gizmos-attributes") (v "0.0.3") (d (list (d (n "tracy-gizmos") (r "0.0.*") (d #t) (k 2)))) (h "0hs7ag27yckfa5yr6yfzbsxs1c625x96ymzsnj3a7j27yl4lc5wp")))

