(define-module (crates-io tr ac tracker-macros) #:use-module (crates-io))

(define-public crate-tracker-macros-0.1.0 (c (n "tracker-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15i8r6sqfyv4vxf04zdwf5kvc7jqp3mh1hxalr8i31iqsws91a3v")))

(define-public crate-tracker-macros-0.1.1 (c (n "tracker-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06fi6vbz95kgkajpcxhs11fh2sy8arrz5v42scdv78qa974j43yj")))

(define-public crate-tracker-macros-0.1.2 (c (n "tracker-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v5pmgl930l5yf8bra0ya0pcqbacw20dxmxnknrll6457mknr6wk")))

(define-public crate-tracker-macros-0.1.3 (c (n "tracker-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "004p90c9cf14pp3qqc9lnc4kbnjc4y7kvkc346q3p46rxxfgm8km")))

(define-public crate-tracker-macros-0.1.4 (c (n "tracker-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qi7qa5g2qzbas0ghvsrry6cbclx8xv6w4gzvhr3zm66z7xb4kah")))

(define-public crate-tracker-macros-0.1.5 (c (n "tracker-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12vclms82swiqghx65i7n207gjg9sbmy4swh7dwrj4h8gx8d5qcg")))

(define-public crate-tracker-macros-0.1.6 (c (n "tracker-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13fdyv9r4laybll84wr4p4c7gbwwpaa4j6kbw760l3np1v07v9cc")))

(define-public crate-tracker-macros-0.1.7 (c (n "tracker-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mv061381y0wda3vc6dbbxicga3zi0sqfb6hw1b16k3p4vj6vb0c")))

(define-public crate-tracker-macros-0.2.0 (c (n "tracker-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q77x40l2ncp3d7a8zci7h9mjhcc8r7dncmiq2dwfrbzrgk8dc1j")))

(define-public crate-tracker-macros-0.2.1 (c (n "tracker-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ps5n0iscj8b3rj04yf97brgxmsryxxyfp90k0rdmvz0zd39f0na")))

