(define-module (crates-io tr ac tracing-perf) #:use-module (crates-io))

(define-public crate-tracing-perf-0.1.0 (c (n "tracing-perf") (v "0.1.0") (d (list (d (n "minstant") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0cicy1s3ch6xqnwhir4ljpcnbqvmhm0h50czpb0qz87nkxgvl2rj")))

(define-public crate-tracing-perf-0.2.0 (c (n "tracing-perf") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "minstant-crate") (r "^0.1") (o #t) (d #t) (k 0) (p "minstant")) (d (n "tracing") (r "^0.1") (k 0)))) (h "05nrsj3zdlgvd8q6r504jv38rbnrd7bfb8xywgj82r8fnxbqgq0f") (f (quote (("start-print-order" "indexmap") ("minstant" "minstant-crate") ("docsrs" "document-features") ("default" "start-print-order"))))))

(define-public crate-tracing-perf-0.2.1 (c (n "tracing-perf") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "minstant-crate") (r "^0.1") (o #t) (d #t) (k 0) (p "minstant")) (d (n "tracing") (r "^0.1") (k 0)))) (h "09zn110pcy89jzsxd8i4slvhn3pfcqsn2dinjqav557amyrjr71h") (f (quote (("start-print-order" "indexmap") ("minstant" "minstant-crate") ("docsrs" "document-features") ("default" "start-print-order"))))))

(define-public crate-tracing-perf-0.2.2 (c (n "tracing-perf") (v "0.2.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "minstant") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "09km78j9r9wwjdkaby2hwzif0gf1vamrashi6ak3vzp0q39iwi2v") (f (quote (("start-print-order" "indexmap") ("docsrs" "document-features") ("default" "start-print-order")))) (s 2) (e (quote (("minstant" "dep:minstant"))))))

