(define-module (crates-io tr ac track_album_number_genius) #:use-module (crates-io))

(define-public crate-track_album_number_genius-0.1.0 (c (n "track_album_number_genius") (v "0.1.0") (d (list (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "0j44s8ai97d42pgb1qgab3b64j4j0vfnan1blqvchvsdb60k3w0h")))

(define-public crate-track_album_number_genius-1.0.0 (c (n "track_album_number_genius") (v "1.0.0") (d (list (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "0gyz88ni7n0sijpzg5bjnxn1wwsydgwbwlrxkqwgh29flmc1rav8")))

(define-public crate-track_album_number_genius-1.0.1 (c (n "track_album_number_genius") (v "1.0.1") (d (list (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "0n5cvd0kdpw3774prddyhnd0w267m4pxshax1c1fn15b2g0y48m4")))

(define-public crate-track_album_number_genius-1.0.2 (c (n "track_album_number_genius") (v "1.0.2") (d (list (d (n "html2text") (r "^0.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "0b4yflxhcl7k6n6b0bid2h34bhim8p2nh16gws7383rvy28q8qv8")))

(define-public crate-track_album_number_genius-1.0.3 (c (n "track_album_number_genius") (v "1.0.3") (d (list (d (n "html2text") (r "^0.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "0p00v1yqzf28sjif9l41fndfnz0k7hgpgz00c8m8wbmpjpdpdymv")))

