(define-module (crates-io tr ac tracing-worker) #:use-module (crates-io))

(define-public crate-tracing-worker-0.1.0 (c (n "tracing-worker") (v "0.1.0") (d (list (d (n "tracing") (r "^0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0") (d #t) (k 0)) (d (n "worker") (r "^0") (d #t) (k 0)))) (h "02570mdssg045ynvh68npgl95b9cihhskvr40xa5zp18bj7l5m6c")))

(define-public crate-tracing-worker-0.1.1 (c (n "tracing-worker") (v "0.1.1") (d (list (d (n "tracing") (r "^0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0") (d #t) (k 0)) (d (n "worker") (r "^0") (d #t) (k 0)))) (h "18na5x3vs86p4xikdmk02x74aggw1zfq01kjdlxbhira2x0nkcnb")))

(define-public crate-tracing-worker-0.1.2 (c (n "tracing-worker") (v "0.1.2") (d (list (d (n "tracing") (r "^0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0") (d #t) (k 0)) (d (n "worker") (r "^0") (d #t) (k 0)))) (h "1l2096nm5lkfs133m4s4blc0zhivh47a0m6gr5639zkll7lc9crw")))

(define-public crate-tracing-worker-0.1.3 (c (n "tracing-worker") (v "0.1.3") (d (list (d (n "tracing") (r "^0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0") (d #t) (k 0)) (d (n "worker") (r "^0") (d #t) (k 0)))) (h "1xp16rxf8s6b8l3501kzrciqfv2rcydrw26y2jdc2d2zfpsn1cji")))

(define-public crate-tracing-worker-0.1.4 (c (n "tracing-worker") (v "0.1.4") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "worker") (r "^0") (d #t) (k 0)))) (h "0djgspfrp0f1mm0560ha0ra721vhnip2i5waw40n6l6kbv68jwpm")))

(define-public crate-tracing-worker-0.1.5 (c (n "tracing-worker") (v "0.1.5") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "worker") (r "^0") (d #t) (k 0)))) (h "165qjw6f9dlybr79651zkq5637g5cf4b2kqi7pvqkhqk1p3h7z7m")))

(define-public crate-tracing-worker-0.1.6 (c (n "tracing-worker") (v "0.1.6") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "worker") (r "^0") (d #t) (k 0)))) (h "1c81sl756ag0wz2g0mrsx11bg63y1r6hvmcv6k0zrn95jhy2fwfj")))

(define-public crate-tracing-worker-0.1.7 (c (n "tracing-worker") (v "0.1.7") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "worker") (r "^0") (d #t) (k 0)))) (h "1hak7ji1wkni4v78xvvmmrz76xavpng9djszrr80w1vgsh1v2c0i")))

(define-public crate-tracing-worker-0.1.8 (c (n "tracing-worker") (v "0.1.8") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "worker") (r "^0") (d #t) (k 0)))) (h "0iz5dcdhzandy58wlvcnyc8f12yx89n13x0a697iwqgh5mqqjv26")))

(define-public crate-tracing-worker-0.1.9 (c (n "tracing-worker") (v "0.1.9") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "worker") (r "^0") (d #t) (k 0)))) (h "1v5jsvmq7pzdjy2y7f30m5izqndgbjy2ah3za2bb6q21s1jb0xjx")))

