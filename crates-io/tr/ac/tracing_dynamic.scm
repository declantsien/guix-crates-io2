(define-module (crates-io tr ac tracing_dynamic) #:use-module (crates-io))

(define-public crate-tracing_dynamic-0.1.0 (c (n "tracing_dynamic") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt" "json" "registry"))) (d #t) (k 2)))) (h "11p7jxx9aqrrb6sk1gzbsqw7ljgsw37qjkp4qnhcnlvh4yyqflpr")))

(define-public crate-tracing_dynamic-0.2.0 (c (n "tracing_dynamic") (v "0.2.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt" "json" "registry"))) (d #t) (k 2)))) (h "00xr1m3zsv9hzz1in5m5km6jhjjxwz0i67a2ll5n4342wqagjd4r")))

(define-public crate-tracing_dynamic-0.3.0 (c (n "tracing_dynamic") (v "0.3.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt" "json" "registry"))) (d #t) (k 2)))) (h "0qpcx8grrfx73j8g3z88932yfirixw858hff55lw88assgy9ifax")))

(define-public crate-tracing_dynamic-0.3.1 (c (n "tracing_dynamic") (v "0.3.1") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt" "json" "registry"))) (d #t) (k 2)))) (h "1hks4kkmwf286qjrksk3rk6xcxl96x4x0fbf7xk6y4pfdrnk0sv3")))

