(define-module (crates-io tr ac tracing-fmt-smart-writer) #:use-module (crates-io))

(define-public crate-tracing-fmt-smart-writer-0.2.0 (c (n "tracing-fmt-smart-writer") (v "0.2.0") (d (list (d (n "androidy-log") (r "^1") (f (quote ("std"))) (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "tracing-core") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.2") (f (quote ("fmt"))) (k 0)) (d (n "web-log") (r "^1") (f (quote ("std"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "17haiwkv6bmg9i25l9ih7hgxy155ri5p7i3nh55rrqigkrq47wjx")))

(define-public crate-tracing-fmt-smart-writer-0.3.0 (c (n "tracing-fmt-smart-writer") (v "0.3.0") (d (list (d (n "tracing-core") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt"))) (k 0)) (d (n "web-log") (r "^1") (f (quote ("std"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "androidy-log") (r "^1") (f (quote ("std"))) (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0pr56p034dfrww8mad75v1lwxbyqqcd5908iz5cq7a4mh9r5vh91")))

