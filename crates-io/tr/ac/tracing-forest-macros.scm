(define-module (crates-io tr ac tracing-forest-macros) #:use-module (crates-io))

(define-public crate-tracing-forest-macros-0.1.0 (c (n "tracing-forest-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "01i2rk80s4d85slygg4hvxx0znf9nanacijgyj18gajb58b8xy5y") (f (quote (("sync") ("derive" "syn/derive") ("attributes"))))))

(define-public crate-tracing-forest-macros-0.1.2 (c (n "tracing-forest-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1bz8401pv02y1gadwx170r2f50i2zqqjd4n8xkiagr9fhfd001nc") (f (quote (("sync") ("derive" "syn/derive") ("attributes"))))))

(define-public crate-tracing-forest-macros-0.1.3 (c (n "tracing-forest-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "1vlkbj0d20hlva4l542c3vqky2gbhxpac3wlwdr5xaqdjzmp5zs6") (f (quote (("sync") ("derive" "syn/derive") ("attributes"))))))

