(define-module (crates-io tr ac tracing-callgraph) #:use-module (crates-io))

(define-public crate-tracing-callgraph-0.1.0-alpha.0 (c (n "tracing-callgraph") (v "0.1.0-alpha.0") (d (list (d (n "petgraph") (r "^0.5.1") (f (quote ("graphmap"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.16") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.8") (k 0)) (d (n "tracing-subscriber") (r "^0.2.8") (f (quote ("registry"))) (k 2)))) (h "0d170p3q9cb2pvp6whnqa1wmwjzw0any6k6xg97kwp60b75d8ji3") (f (quote (("smallvec" "tracing-subscriber/smallvec") ("default" "smallvec"))))))

(define-public crate-tracing-callgraph-0.1.0-alpha.1 (c (n "tracing-callgraph") (v "0.1.0-alpha.1") (d (list (d (n "petgraph") (r "^0.5.1") (f (quote ("graphmap"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.18") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.10") (k 0)) (d (n "tracing-subscriber") (r "^0.2.10") (f (quote ("registry"))) (k 2)))) (h "1yaxqbv8g46c3jj1x9p15ykxay00i0jnqxqf1kfb5gjc1j6k2k6p") (f (quote (("smallvec" "tracing-subscriber/smallvec") ("default" "smallvec"))))))

