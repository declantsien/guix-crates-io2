(define-module (crates-io tr ac tracelogging) #:use-module (crates-io))

(define-public crate-tracelogging-0.1.0 (c (n "tracelogging") (v "0.1.0") (d (list (d (n "tracelogging_macros") (r ">=0.1.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)) (d (n "windows") (r ">=0.39") (d #t) (k 2)))) (h "1bfwr3h9dngr34h3dkgl9y51bak13fy6lz35bxmsx3rd505z9gfj") (f (quote (("etw") ("default" "etw" "macros")))) (s 2) (e (quote (("macros" "dep:tracelogging_macros")))) (r "1.63")))

(define-public crate-tracelogging-1.0.1 (c (n "tracelogging") (v "1.0.1") (d (list (d (n "tracelogging_macros") (r "=1.0.1") (o #t) (d #t) (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)) (d (n "windows") (r ">=0.39") (d #t) (k 2)))) (h "1xsacx0mld3ivi3vamynridind2x727nx3fzf9xmhks1zyb9g2v0") (f (quote (("etw") ("default" "etw" "macros")))) (s 2) (e (quote (("macros" "dep:tracelogging_macros")))) (r "1.63")))

(define-public crate-tracelogging-1.0.2 (c (n "tracelogging") (v "1.0.2") (d (list (d (n "tracelogging_macros") (r "=1.0.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)) (d (n "windows") (r ">=0.39") (d #t) (k 2)))) (h "1baid7gy3y2xpf5qm05wvs295l7ix6jlciqsb3bf6wrxyh8wyva3") (f (quote (("etw") ("default" "etw" "macros")))) (s 2) (e (quote (("macros" "dep:tracelogging_macros")))) (r "1.63")))

(define-public crate-tracelogging-1.1.0 (c (n "tracelogging") (v "1.1.0") (d (list (d (n "tracelogging_macros") (r "=1.1.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)) (d (n "windows") (r ">=0.39") (d #t) (k 2)))) (h "06hrmnwn8bvs122ch69li0ivnnl06jcyi392qr12bxmnxaw1m5ri") (f (quote (("etw") ("default" "etw" "macros")))) (s 2) (e (quote (("macros" "dep:tracelogging_macros")))) (r "1.63")))

(define-public crate-tracelogging-1.2.0 (c (n "tracelogging") (v "1.2.0") (d (list (d (n "tracelogging_macros") (r "=1.2.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)) (d (n "windows") (r ">=0.39") (d #t) (k 2)))) (h "03fn4q6blk1xdpf53clg196m34dar3295b90pvly7r33np0pxx87") (f (quote (("etw") ("default" "etw" "macros")))) (s 2) (e (quote (("macros" "dep:tracelogging_macros")))) (r "1.63")))

(define-public crate-tracelogging-1.2.1 (c (n "tracelogging") (v "1.2.1") (d (list (d (n "tracelogging_macros") (r "=1.2.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)) (d (n "windows") (r ">=0.39") (d #t) (k 2)))) (h "0ki5k2fm5p4fvwlay7vjg0762kl9sdw1rc64s8yp5i8iq88ww1q2") (f (quote (("etw") ("default" "etw" "macros")))) (s 2) (e (quote (("macros" "dep:tracelogging_macros")))) (r "1.63")))

(define-public crate-tracelogging-1.2.2 (c (n "tracelogging") (v "1.2.2") (d (list (d (n "tracelogging_macros") (r "=1.2.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)) (d (n "windows") (r ">=0.39") (d #t) (k 2)))) (h "0155hpz01ji0n5iqd84gndqg8sd37ia2bzj7v2dvmgzylwjzgc7w") (f (quote (("kernel_mode") ("etw") ("default" "etw" "macros")))) (s 2) (e (quote (("macros" "dep:tracelogging_macros")))) (r "1.63")))

