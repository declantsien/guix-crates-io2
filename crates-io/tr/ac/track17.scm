(define-module (crates-io tr ac track17) #:use-module (crates-io))

(define-public crate-track17-0.1.0 (c (n "track17") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "hyper") (r "^0") (d #t) (k 0)) (d (n "hyper-tls") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio-core") (r "^0") (d #t) (k 0)))) (h "1qivpbj0jbnbf4hfswrxhyzgijxng6rf00rp0ayhx2kc0pyx7d8f")))

