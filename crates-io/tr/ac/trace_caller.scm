(define-module (crates-io tr ac trace_caller) #:use-module (crates-io))

(define-public crate-trace_caller-0.1.0 (c (n "trace_caller") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.45") (d #t) (k 0)))) (h "152a95r68bllfrzchcp9wz4kiqzflgy9hic2q9hkblrnp3sshy0g")))

(define-public crate-trace_caller-0.1.1 (c (n "trace_caller") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.45") (d #t) (k 0)))) (h "1yj8ylj1fdvlqvyiq5wk768cyv0z6bj859v95l6w8avb96xq7j00")))

(define-public crate-trace_caller-0.2.0 (c (n "trace_caller") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3.45") (d #t) (k 0)) (d (n "trace_caller_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0wzl26121vchr4ykr1nrzyp46vc25gfgv1g08izlk6sil16mn5lm")))

