(define-module (crates-io tr ac tracing-tools) #:use-module (crates-io))

(define-public crate-tracing-tools-0.1.0 (c (n "tracing-tools") (v "0.1.0") (d (list (d (n "anyhow") (r "~1.0.40") (d #t) (k 0)) (d (n "futures-lite") (r "~1.11.3") (d #t) (k 0)) (d (n "tracing") (r "~0.1.25") (d #t) (k 0)) (d (n "tracing-futures") (r "~0.2.5") (d #t) (k 0)))) (h "1h7hvp6h8w5q6ygpx10wrq9n6ssn9g4khmlsjmclkg83rv4p7q88")))

(define-public crate-tracing-tools-0.1.1 (c (n "tracing-tools") (v "0.1.1") (d (list (d (n "anyhow") (r "~1.0.40") (d #t) (k 0)) (d (n "futures-lite") (r "~1.11.3") (d #t) (k 0)) (d (n "tracing") (r "~0.1.25") (d #t) (k 0)) (d (n "tracing-futures") (r "~0.2.5") (d #t) (k 0)))) (h "1xhhvlzvk5k5ajias4541a31qymyvd7xkpi5wqhgq4x3y8j08w36")))

(define-public crate-tracing-tools-0.2.0 (c (n "tracing-tools") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.5") (d #t) (k 0)))) (h "03zkn3b1sq4x1j8hi8w5h44azabi643frszxp4gvbs3jwvdf97xv")))

(define-public crate-tracing-tools-0.3.0 (c (n "tracing-tools") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.5") (d #t) (k 0)))) (h "1wrin6v9qvbfd6hcxpzc7895vlfrccyzpzx3872k0v0y5z9k9l6j")))

(define-public crate-tracing-tools-0.4.0 (c (n "tracing-tools") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.5") (d #t) (k 0)))) (h "0m5pfrxy5r6w2zgbjm9vmmah741fybmfiwrbhgza6nz8f8jplqpg")))

(define-public crate-tracing-tools-0.5.0 (c (n "tracing-tools") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.5") (d #t) (k 0)))) (h "0vp24dfcpak2nrxnd5l1lnjxbq5axxqi7z5r8d1wmgcagnr9q9sh")))

(define-public crate-tracing-tools-0.6.0 (c (n "tracing-tools") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.5") (d #t) (k 0)))) (h "1kx4036dvf6p2w12csq6vd0qvzc7bn4rf4il8j3522vq7zjqmh66")))

