(define-module (crates-io tr ac tracing-wasm) #:use-module (crates-io))

(define-public crate-tracing-wasm-0.1.0 (c (n "tracing-wasm") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (f (quote ("registry"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0qkr1mm2g4w3gzlcdaprfx1xn5wjp5js13q7q7k9s49g7m6kk5px")))

(define-public crate-tracing-wasm-0.2.0 (c (n "tracing-wasm") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (f (quote ("registry"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "11a40dcf3xkhmw9l6ig3gdp6cxwnasl78pvim3y4f1gpdmq43rwa")))

(define-public crate-tracing-wasm-0.2.1 (c (n "tracing-wasm") (v "0.2.1") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("registry"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "01vfcarjds5n94vz72fxnzxz4nznd3zhhhcgsyi0yhkll5iwcxa5") (f (quote (("mark-with-rayon-thread-index" "rayon"))))))

