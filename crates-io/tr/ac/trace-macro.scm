(define-module (crates-io tr ac trace-macro) #:use-module (crates-io))

(define-public crate-trace-macro-0.1.0 (c (n "trace-macro") (v "0.1.0") (h "097qbi3qhrkpfsm9f5bckbn27dpc2jd2qii2iqhdzlh8zi1zxhxr")))

(define-public crate-trace-macro-0.2.0 (c (n "trace-macro") (v "0.2.0") (h "1l2i1b7zfciw154f6fi50dk6qijp53cikr47q6nyd9mgr3pwsi2y")))

(define-public crate-trace-macro-0.3.0 (c (n "trace-macro") (v "0.3.0") (h "1wdzb17y6z7j3qd0wjbnyikvnbx8p0a8gb64xb35yxmy3hj4vqsq")))

(define-public crate-trace-macro-0.4.0 (c (n "trace-macro") (v "0.4.0") (h "0zprsa967gi91h87b94zbbmcfp2sbrc1g657nwpyji949b8s4lmy")))

(define-public crate-trace-macro-0.5.0 (c (n "trace-macro") (v "0.5.0") (h "0qnic7nprwr4i4z636vdhfji8m3qd3q90lhhvw4gkxn7vd60rmnj")))

(define-public crate-trace-macro-0.6.0 (c (n "trace-macro") (v "0.6.0") (h "1q2j5ysvfd8i32y62iwyx1xqjck0z4s3gc3slgm00wk6pp0cqi55")))

(define-public crate-trace-macro-0.7.0 (c (n "trace-macro") (v "0.7.0") (h "06y3xiy253cbsr3k3cl48r6kqacc3slv8i1kjfb5m2jc3n630896")))

(define-public crate-trace-macro-0.8.0 (c (n "trace-macro") (v "0.8.0") (h "0r7dbzdlkh2kcs24g3xnqg12nva21z7jj6v55d58vsy8a8f5gb94")))

(define-public crate-trace-macro-1.0.0 (c (n "trace-macro") (v "1.0.0") (h "0v82s02rcxfqh402x9fzp4chkqj355802qy7fd3pgcyv2gvid89w")))

(define-public crate-trace-macro-1.1.0 (c (n "trace-macro") (v "1.1.0") (h "1mkabficlw11fdycrw1jla42jyisl07pcn441av5as9mpj4fma96")))

(define-public crate-trace-macro-1.1.1 (c (n "trace-macro") (v "1.1.1") (h "0nf4bi21b81lw41pcm2wm7jsxz5br82q552yvmks7wrll3p30zva")))

