(define-module (crates-io tr ac tracing-span-tree) #:use-module (crates-io))

(define-public crate-tracing-span-tree-0.1.0 (c (n "tracing-span-tree") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "1n7vbx7rxk7nc77221dr308zrg5mg4acc37j5q168xqdayqjcxw7")))

(define-public crate-tracing-span-tree-0.1.1 (c (n "tracing-span-tree") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0hsw7ssd08l9j1w1w2ix4461rx19qgx29jpii9a0bm980gd9r063")))

