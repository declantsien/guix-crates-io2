(define-module (crates-io tr ac tracing_json_span_fields) #:use-module (crates-io))

(define-public crate-tracing_json_span_fields-0.1.0 (c (n "tracing_json_span_fields") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("parsing" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0p5hqy2qd0zyyas5ywllfzb8bmjw3ncn19cw9lk7bxbmlhqlaxrk")))

(define-public crate-tracing_json_span_fields-0.1.1 (c (n "tracing_json_span_fields") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("parsing" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0k4c65yzlhg8p3vf1dvx0cs3jvmcffj13j3c8gxa16lhcjsg6cab")))

