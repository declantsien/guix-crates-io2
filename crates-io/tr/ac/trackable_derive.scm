(define-module (crates-io tr ac trackable_derive) #:use-module (crates-io))

(define-public crate-trackable_derive-0.1.0 (c (n "trackable_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 2)))) (h "0mn9v9jqq1q4yqjwj2psb3vwgxraccjhkrvbpa1k73vb599nv985")))

(define-public crate-trackable_derive-0.1.1 (c (n "trackable_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 2)))) (h "1j6qnjlkh1xfsk30xl08sqzl3vzmzjn9nlz81l5khmml9xgcpwnj")))

(define-public crate-trackable_derive-0.1.2 (c (n "trackable_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 2)))) (h "116hsb25ykw2jgys009gapfw6j40y556nzbii7ibsh6j9pan4h0g")))

(define-public crate-trackable_derive-0.1.3 (c (n "trackable_derive") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 2)))) (h "0p766ybvc0plb21c85v1h0yqaacnj8dyxbkpxw24hpxa5jdhpkzd")))

(define-public crate-trackable_derive-1.0.0 (c (n "trackable_derive") (v "1.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 2)))) (h "0bzqh11n1k29cghjmb4dn426hpqy3nbyn1qgzqngiqj7b1f27szb")))

