(define-module (crates-io tr ac tracing-opentelemetry-fmt) #:use-module (crates-io))

(define-public crate-tracing-opentelemetry-fmt-0.0.1 (c (n "tracing-opentelemetry-fmt") (v "0.0.1") (d (list (d (n "opentelemetry") (r "^0.17.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-opentelemetry") (r "^0.17.4") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "1z8mpnnh270dnjnz6sy3njl1h3k6ijw7549fwi5a4mq06vcvd68m")))

