(define-module (crates-io tr ac trackable) #:use-module (crates-io))

(define-public crate-trackable-0.1.0 (c (n "trackable") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1qwifgdm7k0xhplvmiygkzvzln9sarzc5b05skyaycaxjpzvzysz")))

(define-public crate-trackable-0.1.1 (c (n "trackable") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0cpj0n28ir3s21jls8fy6z63iss7zrm2gdlvw9r6yzgmvwk8l3z0")))

(define-public crate-trackable-0.1.2 (c (n "trackable") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0r201b92ki4zyfc21gavsqkc3dw5mbacmy920fv01mdqr06i4qyn")))

(define-public crate-trackable-0.1.3 (c (n "trackable") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1b1m3sxnwbslfn50v5qgqj80lxxi1bid92dylpnznnm5s850qlb2")))

(define-public crate-trackable-0.1.4 (c (n "trackable") (v "0.1.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "135yrmc2nanmhr66qirqy39s8mh5vlqjdxl82npg5zgahjdzxnnr")))

(define-public crate-trackable-0.1.5 (c (n "trackable") (v "0.1.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1cs96b5qc90ixk24w3yfnlh4bii95j0kyza8bmy07x0ch4yp0l97")))

(define-public crate-trackable-0.1.6 (c (n "trackable") (v "0.1.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "085nxpvjy8n91jwhshb6x802vmax1nld7435rxxv1pq0vr6k6bb6")))

(define-public crate-trackable-0.1.7 (c (n "trackable") (v "0.1.7") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0nfzicnsd3rkd6qvqbbvb29smklv8ng4z8vw4m2b1ph52viyaznn")))

(define-public crate-trackable-0.1.8 (c (n "trackable") (v "0.1.8") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0nm2wcmlib0z88xfwp66j4ikfxzainhssmby600m62i4fq4gvpk5")))

(define-public crate-trackable-0.2.0 (c (n "trackable") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0cyyfimbfk9y0lqpj66a2njkh88j9fazgn5npss6vm1aycj6a230")))

(define-public crate-trackable-0.2.1 (c (n "trackable") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "04dbqiwvnyqlbr9mn14c2dxvvgqd5awfgw88z7fh52cidaz6m7ad")))

(define-public crate-trackable-0.2.2 (c (n "trackable") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1y3s4lqnb104i3xnj2b8m7c6s7h0ywxzw8gw6silbdg73jbsgg7i")))

(define-public crate-trackable-0.2.3 (c (n "trackable") (v "0.2.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "08d143zdxwn77nnr37issqsnkap72j45ks6pw6v6iwwzyi35q3hv")))

(define-public crate-trackable-0.2.4 (c (n "trackable") (v "0.2.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0zzjkjn84x4i1y92frgspi1iw9b9cz7xpjmq7xpqclmlr8jgsv3d")))

(define-public crate-trackable-0.2.5 (c (n "trackable") (v "0.2.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "195g6fld8k3lslw3ismr286k32g5frsb0k3mrklf97hbldgpkld7")))

(define-public crate-trackable-0.2.6 (c (n "trackable") (v "0.2.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1fvknd4mg9prhxr5dmxiyirq2rqhd6rrc0s2pw9882ia75jyz3kb")))

(define-public crate-trackable-0.2.7 (c (n "trackable") (v "0.2.7") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "130yngqbnvb2isc6w4wnyksxzdzdyb1zdnrwn36z3dsjs5lkmjdw")))

(define-public crate-trackable-0.2.8 (c (n "trackable") (v "0.2.8") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0sl4kc7ls1f3810wy9z4zqailywlidfxg2bqb4bqaqgl5f5kbshz")))

(define-public crate-trackable-0.2.9 (c (n "trackable") (v "0.2.9") (h "18gr8n5qq40qg76mrv50n5ypka376sgq39kwnlqsz3mw0v18l0bk")))

(define-public crate-trackable-0.2.10 (c (n "trackable") (v "0.2.10") (h "14xaj7ckrgxa8frm6icnqzaq7zzny1jvykpx01pj8zyarqpv4712")))

(define-public crate-trackable-0.2.11 (c (n "trackable") (v "0.2.11") (h "0nb4xwrnnz35q8jgzabs30ca57rx1a3jnbc6pwvyk2x7abykpnv4")))

(define-public crate-trackable-0.2.12 (c (n "trackable") (v "0.2.12") (h "0w559vmi9rphdplrnkpdwrwqdqajyjaqqmrqsydwd0cy0nd4k7jm")))

(define-public crate-trackable-0.2.13 (c (n "trackable") (v "0.2.13") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0mamaqlqbvbbxbzz1m034i48h1gfy3av9k9nz2nbc2l5v6zvvfb2") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-0.2.14 (c (n "trackable") (v "0.2.14") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0wzmv7pjrfk73b5vk7fqw0l2dp7rzzq0xs6f47q086kw3jq391b2") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-0.2.15 (c (n "trackable") (v "0.2.15") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0vhcgm7hr0jkkgjc2rc0ymz5jssz2p7p78lf88sa6qn4f0facr40") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-0.2.16 (c (n "trackable") (v "0.2.16") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0jabsh6qcfwvm2yqvf9jsjwbdshir6vcwda49ycxd5fry2a8ns8g") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-0.2.17 (c (n "trackable") (v "0.2.17") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1a183k5v9a82n4xid0pb6siv7bf2yp439p1qd24f49gyvkjbyw9g") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-0.2.18 (c (n "trackable") (v "0.2.18") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "09ka6h2rxgkp4kpxlb4ac4sj5inlbfdlbr50fhw3n1p2ysw5k1jg") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-0.2.19 (c (n "trackable") (v "0.2.19") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "trackable_derive") (r "^0.1") (d #t) (k 0)))) (h "00rb1ydab2917d30rf1c9bhrvbw7pyj6q7hr5k89y0hrqybh9s9z") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-0.2.20 (c (n "trackable") (v "0.2.20") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "trackable_derive") (r "^0.1") (d #t) (k 0)))) (h "1h443ds9jpgdmb8yynbs1m03qlkr3h43iq0c4s467hs1g452b6b7") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-0.2.21 (c (n "trackable") (v "0.2.21") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "trackable_derive") (r "^0.1") (d #t) (k 0)))) (h "0balyzd4fpjrdn9l0zh1xgjy8fa9jvd3llg81xib63wwqkbak5ix") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-0.2.22 (c (n "trackable") (v "0.2.22") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "trackable_derive") (r "^0.1") (d #t) (k 0)))) (h "1anaj1r1969sg3wsawpbv0pi8dpvl5ja1zzxxcz021f1j3yckfjk") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-0.2.23 (c (n "trackable") (v "0.2.23") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "trackable_derive") (r "^0.1") (d #t) (k 0)))) (h "19ggf85y140xyqfgjib50jb3j1fb49c9d54pmh73cxdhacy5qiqi") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-1.0.0 (c (n "trackable") (v "1.0.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "trackable_derive") (r "^1") (d #t) (k 0)))) (h "1c5xqp2k9yf5is3pwc7xwf2kd3khdkan93s5072r5p99s49nxyrh") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-1.1.0 (c (n "trackable") (v "1.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "trackable_derive") (r "^1") (d #t) (k 0)))) (h "0zg9ayn4wnx667aada88nd3s75d2jimk6bbysk0k7cnn2x5f5ywz") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-0.2.24 (c (n "trackable") (v "0.2.24") (d (list (d (n "trackable1") (r "^1.1") (d #t) (k 0) (p "trackable")) (d (n "trackable_derive") (r "^1") (d #t) (k 0)))) (h "0cnxf15w0n24rhkhccccw1rijk47baa20jf05j8arf80ffgbp2mr") (f (quote (("serialize" "trackable1/serialize"))))))

(define-public crate-trackable-1.2.0 (c (n "trackable") (v "1.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "trackable_derive") (r "^1") (d #t) (k 0)))) (h "0rri43qmmxc2nl7kbx370iliv3ypmnwcydyhhs1lx3kijcd2lzh1") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-trackable-1.3.0 (c (n "trackable") (v "1.3.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "trackable_derive") (r "^1") (d #t) (k 0)))) (h "1bk96cvr587cdhz8xl7jhkqn7vksyg41grbpx77gi7mrmcad2nxi") (f (quote (("serialize" "serde" "serde_derive"))))))

