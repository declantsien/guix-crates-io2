(define-module (crates-io tr ac tracy-gizmos-sys) #:use-module (crates-io))

(define-public crate-tracy-gizmos-sys-0.10.0 (c (n "tracy-gizmos-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1fcln2jw51pga7nmzyqglkd9mi0ylimnngbrm28djrn185gifsq5") (f (quote (("vsync") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("no-exit") ("hw-counters") ("default") ("crash-handler") ("context-switch") ("code-transfer") ("callstack-inlines") ("broadcast"))))))

(define-public crate-tracy-gizmos-sys-0.11.0 (c (n "tracy-gizmos-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05k8vp2ril0jc3nw3i96haxbanrbpr20m35khyffg7jbykrwffc6") (f (quote (("vsync") ("system-tracing") ("sampling") ("only-localhost") ("only-ipv4") ("no-exit") ("hw-counters") ("default") ("crash-handler") ("context-switch") ("code-transfer") ("callstack-inlines") ("broadcast"))))))

