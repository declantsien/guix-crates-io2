(define-module (crates-io tr ac tracing-orchestra) #:use-module (crates-io))

(define-public crate-tracing-orchestra-0.1.0 (c (n "tracing-orchestra") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-orchestra-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1xgfd11882kam0hvq4c07cyis4z9jlb0bgny8q5c7apkrl2i8i28") (r "1.72")))

(define-public crate-tracing-orchestra-0.1.1 (c (n "tracing-orchestra") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-orchestra-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1av18i78c0h4g9slv6yv6hxmi8asbmal0r3cbnmgj1cl5d11cdvh") (r "1.72")))

(define-public crate-tracing-orchestra-0.2.1 (c (n "tracing-orchestra") (v "0.2.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-orchestra-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1n67xb8swifhj3grmgs2pq9zw00jrh9xkwwq76cxzy592c94iqsp") (r "1.72")))

