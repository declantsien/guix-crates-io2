(define-module (crates-io tr ac tracing_vec) #:use-module (crates-io))

(define-public crate-tracing_vec-0.1.0 (c (n "tracing_vec") (v "0.1.0") (h "0c1agrn74bfk16nlls8jd7gfw6w9zlgy4mn4hbds3mcdablx4jcr")))

(define-public crate-tracing_vec-0.1.1 (c (n "tracing_vec") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q8wiqjvrggd29z1m3swrykngnpzz03bnj2jayfkf2ka0rc68x2f")))

