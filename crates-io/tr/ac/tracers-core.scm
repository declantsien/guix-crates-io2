(define-module (crates-io tr ac tracers-core) #:use-module (crates-io))

(define-public crate-tracers-core-0.1.0 (c (n "tracers-core") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "strum") (r "^0.16.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.16.0") (d #t) (k 0)))) (h "0q03xhkd6cvhf2d77afjhg5kdfsmxgjmj7dzypyz4lrsrpmkn1q7") (f (quote (("dynamic") ("default"))))))

