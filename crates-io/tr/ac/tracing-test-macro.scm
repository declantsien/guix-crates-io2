(define-module (crates-io tr ac tracing-test-macro) #:use-module (crates-io))

(define-public crate-tracing-test-macro-0.1.0 (c (n "tracing-test-macro") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (k 2)))) (h "0j4c68l3gr8lzbla3rmpxhwnj5f219gwxnjyyzbhsblxhymxp6n4")))

(define-public crate-tracing-test-macro-0.2.0 (c (n "tracing-test-macro") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16712v8srymnw8zaymnv2ss44brcbzhbzwjyc5qqxjpx10y9fa87") (y #t)))

(define-public crate-tracing-test-macro-0.2.1 (c (n "tracing-test-macro") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0779wqikbfhflhhhgmf0jiwdpykhqfhx96s6gjaywb2fbsixq0a8")))

(define-public crate-tracing-test-macro-0.2.2 (c (n "tracing-test-macro") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bkijgj9dxwpwp4pi7rkd6diii5ldzn04rs6p2isg9mdbg3ad7vi")))

(define-public crate-tracing-test-macro-0.2.3 (c (n "tracing-test-macro") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "022a49c17fqpmkgda6hn5mk5na8i2dxqpcx4vqggrab95nqj8hvl") (f (quote (("no-env-filter"))))))

(define-public crate-tracing-test-macro-0.2.4 (c (n "tracing-test-macro") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "022w5m923lz4dmxd1xh04ywywvrya2fk7aqjg2bkmrz2z32c32r5") (f (quote (("no-env-filter"))))))

