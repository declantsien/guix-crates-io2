(define-module (crates-io tr ac tracers-libelf-sys) #:use-module (crates-io))

(define-public crate-tracers-libelf-sys-0.1.0 (c (n "tracers-libelf-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "failure") (r "^0.1.6") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (o #t) (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.25") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "06kz68gh0q3dd80wja68bjvll5h7j6cqj28dn7d2qyn114bd1h60") (f (quote (("required" "libz-sys" "libc") ("enabled" "libz-sys" "libc") ("default")))) (l "elf")))

