(define-module (crates-io tr ac tracing-causality) #:use-module (crates-io))

(define-public crate-tracing-causality-0.0.0 (c (n "tracing-causality") (v "0.0.0") (h "010wlzlqjmscmdaiys51dqril6xixdgh9bnmyfvrnr3qkqxrwawd")))

(define-public crate-tracing-causality-0.1.0 (c (n "tracing-causality") (v "0.1.0") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "17gdrf3hqfwm5ba85rxf11gsaiyzy8q9kcnwdkhg765rjdaa4s6s")))

