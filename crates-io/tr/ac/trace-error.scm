(define-module (crates-io tr ac trace-error) #:use-module (crates-io))

(define-public crate-trace-error-0.1.0 (c (n "trace-error") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.0") (d #t) (k 0)))) (h "1ff7wbmlz35jfbmx3h5fjmiyyn3rnarybimrwfmrgidp2xw61syi")))

(define-public crate-trace-error-0.1.1 (c (n "trace-error") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.0") (d #t) (k 0)))) (h "12f3hg04zcdihl5vlxji564rmy5w6jln7vv9was1680rc5ki3xcy")))

(define-public crate-trace-error-0.1.2 (c (n "trace-error") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.0") (d #t) (k 0)))) (h "175jkfzkp4vk4h91d0hrvr9iqldxbi318afm6a8z37c96skajm9x")))

(define-public crate-trace-error-0.1.3 (c (n "trace-error") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3.0") (d #t) (k 0)))) (h "0ggvr7c8cvdl755hs8svqyqlia4bdirsv463v6zq3a91gxwmnlms")))

(define-public crate-trace-error-0.1.4 (c (n "trace-error") (v "0.1.4") (d (list (d (n "backtrace") (r "^0.3.0") (d #t) (k 0)))) (h "02cikzcz3h8glfjl3rc5wjd864an3dyl4zpvkgp85nxrwi67ljkx")))

(define-public crate-trace-error-0.1.5 (c (n "trace-error") (v "0.1.5") (d (list (d (n "backtrace") (r "^0.3.0") (d #t) (k 0)))) (h "12yfg47z5c6z0if560f0842wkhz594zy9jyd4cq59iqhd9x0kc2q")))

