(define-module (crates-io tr ac trace2_macro) #:use-module (crates-io))

(define-public crate-trace2_macro-0.1.0 (c (n "trace2_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "0mnk1i34s4aqdxr3apg5950n4gwi9qsykq5zlg5svjcqjwz62mr4")))

