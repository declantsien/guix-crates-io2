(define-module (crates-io tr ac tracing-ext-ffi-subscriber) #:use-module (crates-io))

(define-public crate-tracing-ext-ffi-subscriber-0.1.0 (c (n "tracing-ext-ffi-subscriber") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.23") (d #t) (k 1)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0x50v19v3j184xrzfj7zgayv19szvah5nkfc3cdaz32l7hzrk55h")))

(define-public crate-tracing-ext-ffi-subscriber-0.1.1-pre1 (c (n "tracing-ext-ffi-subscriber") (v "0.1.1-pre1") (d (list (d (n "cbindgen") (r "^0.23") (d #t) (k 1)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0nvj1z3nldi3ldkxc7zk2v55dg43pcpranbigbh0cqm07jc7sh1b")))

(define-public crate-tracing-ext-ffi-subscriber-0.2.0 (c (n "tracing-ext-ffi-subscriber") (v "0.2.0") (d (list (d (n "cbindgen") (r "^0.24") (d #t) (k 1)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "17ijh8rfa0n0rwr5cib4qwf8jklxhhzbc8jrwnpdyx4j8falakll")))

