(define-module (crates-io tr ac tracing-allocator) #:use-module (crates-io))

(define-public crate-tracing-allocator-0.1.0 (c (n "tracing-allocator") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0jl2gljchbz1dk6fqyv7d6ahkb1v6c96w0w0gicz03sbw32lai9m")))

