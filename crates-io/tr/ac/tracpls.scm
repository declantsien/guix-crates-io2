(define-module (crates-io tr ac tracpls) #:use-module (crates-io))

(define-public crate-tracpls-0.1.0 (c (n "tracpls") (v "0.1.0") (d (list (d (n "bscscan") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j9bwwzxk1kbyw4yybbfhj54pc5ih5p4mzgjxa7hzfwgk4ady5w9")))

(define-public crate-tracpls-0.1.1 (c (n "tracpls") (v "0.1.1") (d (list (d (n "bscscan") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zcz9gknnxm13i5d3jzi9fjhqyjldafvn5qml37jjw9r1zjnafai")))

(define-public crate-tracpls-0.2.0 (c (n "tracpls") (v "0.2.0") (d (list (d (n "bscscan") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1v4mxh2h3y9xs4aghmckqjkyqj4vb9zyv2rqf7kdc2mwrl3qgz2y")))

(define-public crate-tracpls-0.2.1 (c (n "tracpls") (v "0.2.1") (d (list (d (n "bscscan") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1kqnvwkk23hcg33jwabd4662r96svw4m9al1gaydp5rhnsx8d1hd")))

(define-public crate-tracpls-0.2.2 (c (n "tracpls") (v "0.2.2") (d (list (d (n "bscscan") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1lsbl7n1b0gh6bbfr8nxrldwrwj2szv5624312c7q6k3mk5ril6r")))

(define-public crate-tracpls-0.3.0 (c (n "tracpls") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "evmscan") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1900ifnzgs5d8vxvqk80siqdifjcqhbgdnmiclgyib59cz3gry0m")))

(define-public crate-tracpls-0.3.1 (c (n "tracpls") (v "0.3.1") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "evmscan") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0qhqb0im0nfqci6zwrvgjnm8lyfc9k0sfjqx7mrx4y21gmkbdm4f")))

