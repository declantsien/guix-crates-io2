(define-module (crates-io tr ac trace) #:use-module (crates-io))

(define-public crate-trace-0.0.1 (c (n "trace") (v "0.0.1") (h "1h2mkp88jh0zihhxm5byc0ijr8vih85p1z0abgipmy3j2dkd3gpl")))

(define-public crate-trace-0.0.2 (c (n "trace") (v "0.0.2") (h "1zl0ylf76fzr6zkmq86zyazc88xrw8zjpg3iwxn1pvb7qvncyb13")))

(define-public crate-trace-0.0.3 (c (n "trace") (v "0.0.3") (h "15427v7dnkaccrmp64y0lx5g7lpj5dnrjhyzn7rh432nkpff66iw")))

(define-public crate-trace-0.0.4 (c (n "trace") (v "0.0.4") (h "008rxg4w125dr0qmcl1yjiliw5slygffrrcwmjaz0av1c4byqj24")))

(define-public crate-trace-0.1.0 (c (n "trace") (v "0.1.0") (h "1bjxjp7292jbbra46w3xg3bkzb8v6hhf1lv825wk7vpzw2a3nv1j")))

(define-public crate-trace-0.1.1 (c (n "trace") (v "0.1.1") (h "1ddkdrzbr3m8af6v8klxdr711zgr46shfsfajcmqa4ia1k50pnb9")))

(define-public crate-trace-0.1.2 (c (n "trace") (v "0.1.2") (h "0z0gv3423ad1b51w3n4a39ld42h3wwq7ibv4h4wgfd95g4z1n1vl")))

(define-public crate-trace-0.1.3 (c (n "trace") (v "0.1.3") (h "0j7ig5xsmdcpv48dcyw8fz877rdx72h8h1mmiyndpjwxi3bkqcvy")))

(define-public crate-trace-0.1.4 (c (n "trace") (v "0.1.4") (h "1vb1lickxnmihifhc14cm9fr7fv15ajn70yvhw1qkywk26mbjw6b")))

(define-public crate-trace-0.1.5 (c (n "trace") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1ndlcdqfw6fwjh5wamkqm3ajqblldc8faq3hh0k0d6qdkhpj5lpz")))

(define-public crate-trace-0.1.6 (c (n "trace") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "gag") (r "^0.1.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lr00mzk9p3485g4ls9238i0dqfqjijj9fhb4ya2f9j7ppm20gsr")))

(define-public crate-trace-0.1.7 (c (n "trace") (v "0.1.7") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1byr9lhh0rl4mwcbxn06r0xjys0hvfzn49v6808rvl8lw54c1l4s")))

