(define-module (crates-io tr ac tracing-shared) #:use-module (crates-io))

(define-public crate-tracing-shared-0.1.0 (c (n "tracing-shared") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0kfgcvy2y7nrlns1xyxdm973ndscp7s7cj1ab4h8m678f074z9wa") (f (quote (("default")))) (y #t) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-tracing-shared-0.1.1 (c (n "tracing-shared") (v "0.1.1") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^5") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "15ii50s01n6jf542yxg26vgs46ygmjz33vhb5cwj9l8m45w7nzal") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-tracing-shared-0.1.3 (c (n "tracing-shared") (v "0.1.3") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "11gaafbd1p68ca01k516sq7hgra57gsj2mnk0bdh0r8q8ad2j45p") (f (quote (("default" "log")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-tracing-shared-0.1.4 (c (n "tracing-shared") (v "0.1.4") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0wkrzfnd87fbq5613vgw58razq7z9km6hgs5s7hhm4nnm1m28375") (f (quote (("default" "log")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-tracing-shared-0.1.5 (c (n "tracing-shared") (v "0.1.5") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0p08xjdw72l8kk7vly0fbrkgqn82fh3xzv02qx92zj665a7fjv5s") (f (quote (("default" "log")))) (s 2) (e (quote (("tokio" "dep:tokio") ("log" "dep:log"))))))

