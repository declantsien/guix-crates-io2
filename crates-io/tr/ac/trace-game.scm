(define-module (crates-io tr ac trace-game) #:use-module (crates-io))

(define-public crate-trace-game-1.0.0 (c (n "trace-game") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "1x0j0wavrhylpkcgx2qq3w3318k469ms5izx8nsaq8vnx5fsidnv") (y #t)))

(define-public crate-trace-game-1.0.1 (c (n "trace-game") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "1fqil10gd989l8nq0s3ddmkjdby3l9jax221g4fi6v75m1ykrk43")))

(define-public crate-trace-game-1.0.2 (c (n "trace-game") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "0zxz5q09jyip4n7vkgdas4rgbgyihb41a86s55racw9v30i5nssz")))

