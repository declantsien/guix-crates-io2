(define-module (crates-io tr ac tracing-json) #:use-module (crates-io))

(define-public crate-tracing-json-0.0.0 (c (n "tracing-json") (v "0.0.0") (h "0wjjr5c8miwa21hzhvv51jazw097n585b2qhxyk14yw7z8bwxin8")))

(define-public crate-tracing-json-0.1.0 (c (n "tracing-json") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("arbitrary_precision"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.13") (f (quote ("log" "std"))) (k 0)) (d (n "tracing") (r "^0.1.13") (f (quote ("log" "std" "attributes"))) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.5") (f (quote ("registry" "fmt"))) (k 0)))) (h "0i4iv4sggzycg557n11zwa8ib9f4paqksngq7br8d630rygnxqws")))

