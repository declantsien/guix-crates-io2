(define-module (crates-io tr ac trace-tools-attributes) #:use-module (crates-io))

(define-public crate-trace-tools-attributes-0.1.0 (c (n "trace-tools-attributes") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (k 0)) (d (n "quote") (r "^1.0.9") (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits" "parsing" "printing" "derive" "proc-macro" "clone-impls"))) (k 0)))) (h "0i6wn0i6xs3d5dihqalk2hn6gnrl30vnbcwhf8q7pd5rjjm3vq57")))

