(define-module (crates-io tr ac tracing-mutex) #:use-module (crates-io))

(define-public crate-tracing-mutex-0.1.0 (c (n "tracing-mutex") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "08d712hblqkimz01bip7yr3wdr8vpb8ca34d63bx9flnbw4yx1kc") (y #t)))

(define-public crate-tracing-mutex-0.1.1 (c (n "tracing-mutex") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "01x0lmykflq7198ibivrfwh8vlamv02c5lkz3r60xbp7wm59sq0k")))

(define-public crate-tracing-mutex-0.1.2 (c (n "tracing-mutex") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1zwkcqdhql8h0z6172jjady2gf72ma7hnp42r6abca28zn1aq3b9")))

(define-public crate-tracing-mutex-0.2.0 (c (n "tracing-mutex") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lock_api") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0zblr6vs7bzmy6y9cijyn2g1dqsn50jflcpnngizsbkfc19qr782") (f (quote (("parkinglot" "parking_lot" "lockapi") ("lockapi" "lock_api"))))))

(define-public crate-tracing-mutex-0.2.1 (c (n "tracing-mutex") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lock_api") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "08hgf37xgax638slz85jrdp6vh9fiyxyxn4167pbbq7hb3523pxk") (f (quote (("parkinglot" "parking_lot" "lockapi") ("lockapi" "lock_api"))))))

(define-public crate-tracing-mutex-0.3.0 (c (n "tracing-mutex") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "lock_api") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1rdwk9w1ik6rj43agqlhxrghl3ajk99jgzwzf4qy28j6i23fwg3c") (f (quote (("parkinglot" "parking_lot" "lockapi") ("lockapi" "lock_api") ("default" "backtraces") ("backtraces")))) (r "1.70")))

