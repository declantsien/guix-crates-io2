(define-module (crates-io tr ac traceroute) #:use-module (crates-io))

(define-public crate-traceroute-0.1.0 (c (n "traceroute") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "socket") (r "^0.0.7") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "12s25yb90dwwqazczcz0rzji7576vghky6fvs3bglqwk045lpq16")))

(define-public crate-traceroute-0.1.1 (c (n "traceroute") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "socket") (r "^0.0.7") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1rkppf5kfc7m4qxvdhmmxldqqjcnpps411gfmcigr7y1wz04yd8k")))

