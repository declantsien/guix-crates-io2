(define-module (crates-io tr ac tracing-init) #:use-module (crates-io))

(define-public crate-tracing-init-0.1.0 (c (n "tracing-init") (v "0.1.0") (d (list (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter" "local-time"))) (d #t) (k 0)))) (h "0zysfd6aj5xgg49r5zxmdsrqr684wb3gvmnsbr89bn833f9rxz1d")))

