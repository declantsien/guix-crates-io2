(define-module (crates-io tr ac tracing-unwrap) #:use-module (crates-io))

(define-public crate-tracing-unwrap-0.0.1 (c (n "tracing-unwrap") (v "0.0.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "06cpvgql3jlsp055wcss83zwak9kpv7c7rnbf38xr8jr7knpbs2a") (f (quote (("panic-quiet") ("default" "panic-quiet"))))))

(define-public crate-tracing-unwrap-0.9.0 (c (n "tracing-unwrap") (v "0.9.0") (d (list (d (n "tracing") (r "^0.1") (k 0)))) (h "12nzy9mjb7ifan6bjzq5xcpgn8mlq5gdxlih0swgxfrymf9n00d5") (f (quote (("panic-quiet") ("default" "panic-quiet"))))))

(define-public crate-tracing-unwrap-0.9.1 (c (n "tracing-unwrap") (v "0.9.1") (d (list (d (n "tracing") (r "^0.1") (k 0)))) (h "04pfqa435w6lhdyfrs6f2c7myidq1a2fhkrvqyisk4s6k4g5zdfs") (f (quote (("panic-quiet") ("default" "panic-quiet"))))))

(define-public crate-tracing-unwrap-0.9.2 (c (n "tracing-unwrap") (v "0.9.2") (d (list (d (n "tracing") (r "^0.1") (k 0)))) (h "0jppa01pkpq2fpxzkj1y5ib36fzi0wv7xznnh5zvsmjp8zqsa23r") (f (quote (("panic-quiet") ("default" "panic-quiet"))))))

(define-public crate-tracing-unwrap-0.10.0 (c (n "tracing-unwrap") (v "0.10.0") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-test") (r "^0.2.3") (f (quote ("no-env-filter"))) (d #t) (k 2)))) (h "0bn0jz3pc657df6md83lnh43xf3v9ywlzqnh6zmxlf0by794p4vp") (f (quote (("panic-quiet") ("log-location") ("default" "panic-quiet"))))))

(define-public crate-tracing-unwrap-1.0.0 (c (n "tracing-unwrap") (v "1.0.0") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-test") (r "^0.2") (f (quote ("no-env-filter"))) (d #t) (k 2)))) (h "09i3i65qhhmjpm4f2ncgnphl1y3c026ga0d80avsd8756z9vm30h") (f (quote (("panic-quiet") ("log-location") ("default" "panic-quiet")))) (y #t)))

(define-public crate-tracing-unwrap-1.0.1 (c (n "tracing-unwrap") (v "1.0.1") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-test") (r "^0.2") (f (quote ("no-env-filter"))) (d #t) (k 2)))) (h "0m03ndlr81bzqzb00m43v23qih5vjv3fyvrd69qaxxcppqak9qy4") (f (quote (("panic-quiet") ("log-location") ("default" "panic-quiet"))))))

