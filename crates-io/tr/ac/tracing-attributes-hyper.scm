(define-module (crates-io tr ac tracing-attributes-hyper) #:use-module (crates-io))

(define-public crate-tracing-attributes-hyper-0.0.0 (c (n "tracing-attributes-hyper") (v "0.0.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "tracing"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("std" "attributes"))) (d #t) (k 2)) (d (n "tracing-attributes-http") (r "^0") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0vr178rkgizkf5w84wkhfgp6x7j29r1illhqsacw15ij6vx6v94z")))

