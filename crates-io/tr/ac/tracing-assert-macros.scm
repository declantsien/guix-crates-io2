(define-module (crates-io tr ac tracing-assert-macros) #:use-module (crates-io))

(define-public crate-tracing-assert-macros-0.1.3 (c (n "tracing-assert-macros") (v "0.1.3") (d (list (d (n "tracing-assert-core") (r "^0.1.3") (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 2)))) (h "1yp5c00fqb4vsx4xykq2jr5jcy1dv3d392924zb79mzc3dxgmbya")))

(define-public crate-tracing-assert-macros-0.1.4-beta.30.0 (c (n "tracing-assert-macros") (v "0.1.4-beta.30.0") (d (list (d (n "tracing-assert-core") (r "^0.1.4-beta.30.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 2)))) (h "0njpgfvb6gfn162pamllnvvvhnds5a2amb3rz67c778m32m3y3j6")))

(define-public crate-tracing-assert-macros-0.1.4 (c (n "tracing-assert-macros") (v "0.1.4") (d (list (d (n "tracing-assert-core") (r "^0.1.4") (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 2)))) (h "0if9yynqyc6q3vqni4zi1jazadq19xv2j4lsqsaljqxikmzv398n")))

