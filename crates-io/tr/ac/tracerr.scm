(define-module (crates-io tr ac tracerr) #:use-module (crates-io))

(define-public crate-tracerr-0.1.0 (c (n "tracerr") (v "0.1.0") (d (list (d (n "dm") (r "^0.99.2") (d #t) (k 0) (p "derive_more")) (d (n "failure") (r "^0.1.6") (f (quote ("backtrace"))) (o #t) (k 0)))) (h "021nyfgs6gzxs8hcpp8mxw7f65mlh499wmb7f09nnxx661zdwsyk")))

(define-public crate-tracerr-0.1.1 (c (n "tracerr") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (f (quote ("backtrace"))) (o #t) (k 0)))) (h "0lxxiqxss55j9k3gvbh7fify05ps6rv2kvg0xp58wm0bv2z7bvk4")))

(define-public crate-tracerr-0.1.2 (c (n "tracerr") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (f (quote ("backtrace"))) (o #t) (k 0)))) (h "03awvqnix4q4r07vzki3cmzyg46342rk3l89pxxhnxf97w5c6d1m")))

(define-public crate-tracerr-0.2.0 (c (n "tracerr") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.2") (f (quote ("display"))) (k 0)))) (h "0xgpisx5lskg4fif3kbklz3xkx8a987mfd9vz21fk39wq51x1s89")))

(define-public crate-tracerr-0.3.0 (c (n "tracerr") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.8") (f (quote ("as_mut" "as_ref" "display"))) (k 0)) (d (n "sealed") (r "^0.3") (d #t) (k 0)))) (h "1rj18cwawrzsrvpqww7fjjgss65fgbqjvw4aq333cd83fp7bwhjw") (r "1.56")))

