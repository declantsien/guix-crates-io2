(define-module (crates-io tr ac tracked) #:use-module (crates-io))

(define-public crate-tracked-0.0.0 (c (n "tracked") (v "0.0.0") (h "17s5203aywqw3s5gqqc92qfnzlny6rmkn7zlvgl9mxabq29rwk81") (y #t)))

(define-public crate-tracked-0.1.0 (c (n "tracked") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tracked-impl") (r "=0.1.0") (d #t) (k 0)))) (h "19s6wyd6dciw5wjwnzwqnzj01kasjvjmr0ddhia2384gxpvzr5iy") (r "1.56")))

(define-public crate-tracked-0.1.1 (c (n "tracked") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracked-impl") (r "=0.1.1") (d #t) (k 0)))) (h "1pi6bv49h3bq6isp71npsg8j0jmdcr3wf44s8yqq6ap7ry368afs") (r "1.56")))

(define-public crate-tracked-0.2.0 (c (n "tracked") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracked-impl") (r "=0.2.0") (d #t) (k 0)))) (h "0qxh8bv7m0cm2jwh090d5dgjyplj6nrfvxwyfpzm6p6bgrc7mybw") (r "1.56")))

(define-public crate-tracked-0.2.1 (c (n "tracked") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracked-impl") (r "=0.2.1") (d #t) (k 0)))) (h "1g38dfq427hpp6x0iakshaw27p6cfayjrdbik36zv4vrjs4iyxyp") (r "1.56")))

(define-public crate-tracked-0.3.0 (c (n "tracked") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracked-impl") (r "=0.3.0") (d #t) (k 0)))) (h "0r8qc1bzg4nanvas2cgirzdfyy5mg5li9h6mbfz3ifqsfh6vm88w") (r "1.56")))

(define-public crate-tracked-0.4.0 (c (n "tracked") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracked-impl") (r "=0.4.0") (d #t) (k 0)))) (h "1is2rwv1fmnabcf9lb6hlml3pd1n0k7k3mb40fs7qylaxsn7gdp4") (r "1.56")))

(define-public crate-tracked-0.5.0 (c (n "tracked") (v "0.5.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracked-impl") (r "=0.5.0") (d #t) (k 0)))) (h "1ihya12s7xpdvim3azlgxnymviczlp319ra89aq39ljcf59il72y") (f (quote (("default" "serde")))) (r "1.56")))

(define-public crate-tracked-0.5.1 (c (n "tracked") (v "0.5.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracked-impl") (r "=0.5.1") (d #t) (k 0)))) (h "0msadwdl88ykk9whws46jk4ycvm6s9098xwai55jm532f7bpwd5c") (f (quote (("default" "serde")))) (r "1.56")))

(define-public crate-tracked-0.5.2 (c (n "tracked") (v "0.5.2") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracked-impl") (r "=0.5.2") (d #t) (k 0)))) (h "0rm67h7jzkk1s1wykdrlgwib3ly6zkq9ilhv0bfcmznffszkq0s3") (f (quote (("default" "serde")))) (r "1.56")))

(define-public crate-tracked-0.5.3 (c (n "tracked") (v "0.5.3") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracked-impl") (r "=0.5.3") (d #t) (k 0)))) (h "1yw2jm8xgq2zxr5sy108jzc6chhqrwhmp19gzpnwk3vvwd9czqiq") (f (quote (("default" "serde")))) (r "1.56")))

(define-public crate-tracked-0.5.4 (c (n "tracked") (v "0.5.4") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracked-impl") (r "=0.5.4") (d #t) (k 0)))) (h "1zrgainywz8h6kw0302x46z23a2ak66yxj8767g9xg5bcrg0bpra") (f (quote (("default" "serde")))) (r "1.56")))

