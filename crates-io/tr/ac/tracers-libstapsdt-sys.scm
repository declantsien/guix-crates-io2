(define-module (crates-io tr ac tracers-libstapsdt-sys) #:use-module (crates-io))

(define-public crate-tracers-libstapsdt-sys-0.1.0 (c (n "tracers-libstapsdt-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "failure") (r "^0.1.6") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tracers-libelf-sys") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "05m51b9phvavyf9z8xcsccj84wacl6893s25mnmlz3zpx65casg6") (f (quote (("required" "tracers-libelf-sys/required" "libc") ("enabled" "tracers-libelf-sys/enabled" "libc") ("default")))) (l "stapsdt")))

