(define-module (crates-io tr ac track-macro) #:use-module (crates-io))

(define-public crate-track-macro-0.0.0 (c (n "track-macro") (v "0.0.0") (h "0r6jklq41hgsqxrd90sx4xnc4c78x37kxsjgvm4vhyp7cd2mqm4k")))

(define-public crate-track-macro-0.1.0 (c (n "track-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "12psh8icn2pcmq2v0f143129h0qzfkiyhrkna9z1djiikg57pdvh")))

