(define-module (crates-io tr ac tracing-foramtters) #:use-module (crates-io))

(define-public crate-tracing-foramtters-0.1.0 (c (n "tracing-foramtters") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "gethostname") (r "^0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-actix-web") (r "^0.6.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1ijmzwz64wmcjkkyy7m6vwq4mfzvcyjkvp03czkyifyca1vxq891") (f (quote (("syslog") ("default")))) (y #t) (s 2) (e (quote (("bunyan" "dep:serde" "dep:serde_json"))))))

