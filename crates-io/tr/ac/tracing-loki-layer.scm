(define-module (crates-io tr ac tracing-loki-layer) #:use-module (crates-io))

(define-public crate-tracing-loki-layer-0.1.0 (c (n "tracing-loki-layer") (v "0.1.0") (d (list (d (n "httpmock") (r "^0.6.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "rustls" "blocking"))) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.31") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1j6wcri6bnlczy283qcq4a1vw8185hs2pfxwg9k1gck4249hmjkb")))

