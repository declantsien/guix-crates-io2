(define-module (crates-io tr ac tracing-line-filter) #:use-module (crates-io))

(define-public crate-tracing-line-filter-0.1.0 (c (n "tracing-line-filter") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "020x5y81q545f93vjwfq2i60sayn7khha4fapfk0yzjg2cz6b6ai")))

