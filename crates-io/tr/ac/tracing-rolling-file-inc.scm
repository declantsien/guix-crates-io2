(define-module (crates-io tr ac tracing-rolling-file-inc) #:use-module (crates-io))

(define-public crate-tracing-rolling-file-inc-0.0.1 (c (n "tracing-rolling-file-inc") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0kcjzs39v85i8klwnpy612knmv156p9d10lsf2vf0kz1imd87v8c") (y #t)))

(define-public crate-tracing-rolling-file-inc-0.0.2 (c (n "tracing-rolling-file-inc") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0j5bw13lf56xqx2f1c069kjhwm8xkx2y90k1ympsglwhdx5358sn")))

