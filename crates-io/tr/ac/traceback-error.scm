(define-module (crates-io tr ac traceback-error) #:use-module (crates-io))

(define-public crate-traceback-error-0.1.0 (c (n "traceback-error") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^0.1.0") (d #t) (k 0) (p "unique-paste")) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1zkqpxnjdrxdvyhbljp1lgx8x0iblymbqgm21kkpnzxczlnl02z6") (r "1.31")))

(define-public crate-traceback-error-0.1.1 (c (n "traceback-error") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^0.1.0") (d #t) (k 0) (p "unique-paste")) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "18h6j9qx616wfv6ikfqr3clkivzw3x1wx3rd228m1563k991ar91") (r "1.31")))

(define-public crate-traceback-error-0.1.2 (c (n "traceback-error") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^0.1.0") (d #t) (k 0) (p "unique-paste")) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1ngmcg43293psk29l897sb60p3qjhs48lq1ywbsm0gqnmjg320ya") (r "1.31")))

(define-public crate-traceback-error-0.1.3 (c (n "traceback-error") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^0.1.0") (d #t) (k 0) (p "unique-paste")) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0r7lji0pgnf8049k4clrp6n099z36m4r9zlrxqf6fh0siir3qgv0") (r "1.31")))

(define-public crate-traceback-error-0.1.4 (c (n "traceback-error") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^0.1.0") (d #t) (k 0) (p "unique-paste")) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0qj9lmqvya4adms6zq6y8nppzghlcxjy972jsz020sf2pm86i16y") (r "1.31")))

(define-public crate-traceback-error-0.1.5 (c (n "traceback-error") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^0.1.0") (d #t) (k 0) (p "unique-paste")) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1j2hk095mlh6qx391rab92cj05ak2vif755ax45qzkxc3pi2bbqa") (r "1.31")))

(define-public crate-traceback-error-0.1.6 (c (n "traceback-error") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^0.1.0") (d #t) (k 0) (p "unique-paste")) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "04n4y7xm94wmk6qgb4vbj6393hajwjh7jyxcv9akhh7vh6skqavv") (r "1.31")))

(define-public crate-traceback-error-0.1.7 (c (n "traceback-error") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^0.1.0") (d #t) (k 0) (p "unique-paste")) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1bqsskg7wv3kpaig7zd6a65ms79b943acldfhcpdp1f4x9xvr0cg") (r "1.31")))

(define-public crate-traceback-error-0.1.8 (c (n "traceback-error") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^0.1.0") (d #t) (k 0) (p "unique-paste")) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "05954i57vddngahzl188y4b7z8n1g24i7mga6b4b317v6wpxfcs8") (r "1.31")))

(define-public crate-traceback-error-0.1.9 (c (n "traceback-error") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^0.1.0") (d #t) (k 0) (p "unique-paste")) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1dfvlc2rhk812v2q0jr7kl1lgm69g6bnk6d7nhzmk6mj9500hbqw") (r "1.31")))

