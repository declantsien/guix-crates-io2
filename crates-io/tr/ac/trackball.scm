(define-module (crates-io tr ac trackball) #:use-module (crates-io))

(define-public crate-trackball-0.1.0 (c (n "trackball") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.25") (d #t) (k 0)))) (h "0fdvqxh6kgrz520xrnk45579idpajj7xcrp7rkb5x2nlmb4zarng")))

(define-public crate-trackball-0.1.1 (c (n "trackball") (v "0.1.1") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "nalgebra") (r "^0.25") (d #t) (k 0)))) (h "103dwyjc6h2rfyhfklb20xvjp68c96c9dj1bc3q8ngdsgi8n3zv5")))

(define-public crate-trackball-0.1.2 (c (n "trackball") (v "0.1.2") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "nalgebra") (r "^0.25") (d #t) (k 0)))) (h "1cy74xvpnmvfq4r8g0i9igzfp6s72kd6izk0464b1bq3dqjfw15b")))

(define-public crate-trackball-0.2.0 (c (n "trackball") (v "0.2.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "nalgebra") (r "^0.25") (d #t) (k 0)))) (h "07nkn1mgh0s2xa1mlc61w71ns88mv1w462w5x5qq4jv0p3i8mlf6")))

(define-public crate-trackball-0.2.1 (c (n "trackball") (v "0.2.1") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "nalgebra") (r "^0.25") (d #t) (k 0)))) (h "1ypycr31cf7z9rf3x7060r9dy4zs55zi8jblhg6bq0k2081n60c7")))

(define-public crate-trackball-0.2.2 (c (n "trackball") (v "0.2.2") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "nalgebra") (r "^0.25") (d #t) (k 0)))) (h "1a2szyxl3njc7z51yv5061cld2gsay56vkpf5ab7zyqskcl6qvyg")))

(define-public crate-trackball-0.2.3 (c (n "trackball") (v "0.2.3") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "nalgebra") (r "^0.25") (d #t) (k 0)))) (h "0pz6yxzdnk4fzj64znd2n3d17srdzkmy55j75d4dp9gc7xzw305m")))

(define-public crate-trackball-0.3.0 (c (n "trackball") (v "0.3.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "nalgebra") (r "^0.26") (d #t) (k 0)))) (h "07s6x4ydr5glxl5yxrixnxzrwwjxjh397yjl5vb1vlwq7y4ra4lc")))

(define-public crate-trackball-0.4.0 (c (n "trackball") (v "0.4.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "nalgebra") (r "^0.26") (d #t) (k 0)))) (h "10msi3qcgbp3w5vb6z0j9l5n1ryzq43i0hh1x2qnc1kwlx9p0xgx")))

(define-public crate-trackball-0.5.0 (c (n "trackball") (v "0.5.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "nalgebra") (r "^0.26") (d #t) (k 0)))) (h "0g7gkhsng6cz8mkpmdjan9hmxywrcbqswjy2xxhk5h8xrvx07zqf")))

(define-public crate-trackball-0.5.1 (c (n "trackball") (v "0.5.1") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "nalgebra") (r "^0.26") (d #t) (k 0)))) (h "1xw133959kj2388awvhg7p1aycs10c2w4s2bm6vvqmm7s6rnginl")))

(define-public crate-trackball-0.6.0 (c (n "trackball") (v "0.6.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "heapless") (r "^0.7") (k 0)) (d (n "nalgebra") (r "^0.26") (f (quote ("libm"))) (k 0)))) (h "0ff8dw11ylgqqdkp5ym7rpnqdb0406qqdgmr8wqd62h3fmq1f6qr")))

(define-public crate-trackball-0.7.0 (c (n "trackball") (v "0.7.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "heapless") (r "^0.7") (k 0)) (d (n "nalgebra") (r "^0.29") (f (quote ("libm"))) (k 0)))) (h "0qbm1sf18sls3dgmivz72sdff8mm3flsj81i5jw75v8g7nljvhfz")))

(define-public crate-trackball-0.8.0 (c (n "trackball") (v "0.8.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "heapless") (r "^0.7") (k 0)) (d (n "nalgebra") (r "^0.30") (f (quote ("libm"))) (k 0)))) (h "11x90ibsp2plir1z70kj9rndzkikg8ksmi1znblfamxa87siym1g")))

(define-public crate-trackball-0.8.1 (c (n "trackball") (v "0.8.1") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "heapless") (r "^0.7") (k 0)) (d (n "nalgebra") (r "^0.30") (f (quote ("libm"))) (k 0)))) (h "0yg01z0hky6dilfrid486sxpp2gxay9wczwwmz98ibgyj0ff7raj")))

(define-public crate-trackball-0.9.0 (c (n "trackball") (v "0.9.0") (d (list (d (n "cc") (r "^1.0.73") (o #t) (d #t) (k 1)) (d (n "heapless") (r "^0.7.10") (k 0)) (d (n "nalgebra") (r "^0.31.0") (f (quote ("libm-force"))) (k 0)))) (h "0wny2ihfnkyf0aq4hi0pw0d26vr7d8y9ppq7hyq4ar1wm5mxpm8n")))

(define-public crate-trackball-0.10.0 (c (n "trackball") (v "0.10.0") (d (list (d (n "approx") (r "^0.5.1") (k 0)) (d (n "cc") (r "^1.0.79") (o #t) (d #t) (k 1)) (d (n "heapless") (r "^0.7.16") (k 0)) (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm-force"))) (k 0)) (d (n "rkyv") (r "^0.7.42") (o #t) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (o #t) (k 0)))) (h "1rwcadjwyc0ni1h109gq34h2zhcwagqsgvpprddan6403iyafcff") (f (quote (("glam" "nalgebra/convert-glam024")))) (s 2) (e (quote (("serde" "dep:serde" "nalgebra/serde-serialize-no-std") ("rkyv" "dep:rkyv" "nalgebra/rkyv-serialize-no-std"))))))

(define-public crate-trackball-0.11.0 (c (n "trackball") (v "0.11.0") (d (list (d (n "approx") (r "^0.5.1") (k 0)) (d (n "cc") (r "^1.0.82") (o #t) (d #t) (k 1)) (d (n "heapless") (r "^0.7.16") (k 0)) (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm-force"))) (k 0)) (d (n "rkyv") (r "^0.7.42") (o #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (k 0)) (d (n "simba") (r "^0.8.1") (f (quote ("libm"))) (k 0)))) (h "13q44x73kcgiw0ykv9nldd7nbv4yaw9p1h3vzk4i0v4m9biii7jv") (f (quote (("glam" "nalgebra/convert-glam024")))) (s 2) (e (quote (("serde" "dep:serde" "nalgebra/serde-serialize-no-std" "simba/serde_serialize") ("rkyv" "dep:rkyv" "nalgebra/rkyv-serialize-no-std" "simba/rkyv-serialize"))))))

(define-public crate-trackball-0.11.1 (c (n "trackball") (v "0.11.1") (d (list (d (n "approx") (r "^0.5.1") (k 0)) (d (n "cc") (r "^1.0.82") (o #t) (d #t) (k 1)) (d (n "heapless") (r "^0.7.16") (k 0)) (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm-force"))) (k 0)) (d (n "rkyv") (r "^0.7.42") (o #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (k 0)) (d (n "simba") (r "^0.8.1") (f (quote ("libm"))) (k 0)))) (h "1qja9r83szvgnjk0fnhai7bl1mvlh73dd0xlqkw0ypg2sai76xva") (f (quote (("glam" "nalgebra/convert-glam024")))) (s 2) (e (quote (("serde" "dep:serde" "nalgebra/serde-serialize-no-std" "simba/serde_serialize") ("rkyv" "dep:rkyv" "nalgebra/rkyv-serialize-no-std" "simba/rkyv-serialize"))))))

(define-public crate-trackball-0.11.2 (c (n "trackball") (v "0.11.2") (d (list (d (n "approx") (r "^0.5.1") (k 0)) (d (n "cc") (r "^1.0.82") (o #t) (d #t) (k 1)) (d (n "heapless") (r "^0.7.16") (k 0)) (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm-force"))) (k 0)) (d (n "rkyv") (r "^0.7.42") (o #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (k 0)) (d (n "simba") (r "^0.8.1") (f (quote ("libm"))) (k 0)))) (h "1pigdbnhjj1x78fcfz2ik0xd4nyjgrkyc8mc5ylsk60bb14q03kj") (f (quote (("glam" "nalgebra/convert-glam024")))) (s 2) (e (quote (("serde" "dep:serde" "nalgebra/serde-serialize-no-std" "simba/serde_serialize") ("rkyv" "dep:rkyv" "nalgebra/rkyv-serialize-no-std" "simba/rkyv-serialize")))) (r "1.70.0")))

(define-public crate-trackball-0.12.0 (c (n "trackball") (v "0.12.0") (d (list (d (n "approx") (r "^0.5.1") (k 0)) (d (n "cc") (r "^1.0.86") (o #t) (d #t) (k 1)) (d (n "heapless") (r "^0.8.0") (k 0)) (d (n "nalgebra") (r "^0.32.4") (f (quote ("libm-force"))) (k 0)) (d (n "rkyv") (r "^0.7.44") (o #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (k 0)) (d (n "simba") (r "^0.8.1") (f (quote ("libm"))) (k 0)))) (h "1mib5aa9mam69g36ysdjq74s7d7cw9cxpy847ncwbb6yrg26pkyv") (f (quote (("glam" "nalgebra/convert-glam025")))) (s 2) (e (quote (("serde" "dep:serde" "nalgebra/serde-serialize-no-std" "simba/serde_serialize") ("rkyv" "dep:rkyv" "nalgebra/rkyv-serialize-no-std" "simba/rkyv-serialize")))) (r "1.71.0")))

