(define-module (crates-io tr ac tracer) #:use-module (crates-io))

(define-public crate-tracer-0.1.0 (c (n "tracer") (v "0.1.0") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "docopt_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "14v0iqri6ipgr5miv7g026scg0ybx6jq6x1ghnixagzna4q45swy") (y #t)))

