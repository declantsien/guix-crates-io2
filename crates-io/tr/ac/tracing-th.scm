(define-module (crates-io tr ac tracing-th) #:use-module (crates-io))

(define-public crate-tracing-th-0.1.0 (c (n "tracing-th") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "th_rs") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1hlzb0rv7xw5z7snxmfa6hhvl9zjl1d73jxwl2axqbwif442197w")))

(define-public crate-tracing-th-0.1.1 (c (n "tracing-th") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "th_rs") (r "^0.1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "07k13zccblx04glki1d5xg4var3v73a9qrh9g70b1n9n8fkj74xv")))

