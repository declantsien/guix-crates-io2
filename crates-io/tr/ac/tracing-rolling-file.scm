(define-module (crates-io tr ac tracing-rolling-file) #:use-module (crates-io))

(define-public crate-tracing-rolling-file-0.1.0 (c (n "tracing-rolling-file") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0g969v4a2dhblj9irdzw844rddlw5qxl06da7kz60v3hks1zhk48") (y #t)))

(define-public crate-tracing-rolling-file-0.1.1 (c (n "tracing-rolling-file") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "07vzi0l8zdlhj5b8x6mrwhvq5dl44hy6mw11g6skrsf5s2rz3pvw")))

(define-public crate-tracing-rolling-file-0.1.2 (c (n "tracing-rolling-file") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "05nd4bkvry58wc4rgjlgmwldpdd7dv5kl6hhd8yn5k1nwpzp7pww")))

