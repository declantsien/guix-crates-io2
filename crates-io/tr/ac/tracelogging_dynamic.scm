(define-module (crates-io tr ac tracelogging_dynamic) #:use-module (crates-io))

(define-public crate-tracelogging_dynamic-0.1.0 (c (n "tracelogging_dynamic") (v "0.1.0") (d (list (d (n "tracelogging") (r ">=0.1.0") (k 0)))) (h "0rdidhsixvlna1lyhgcrsfg8jwz3j633p4anfw0br8ks2s94zfha") (f (quote (("etw" "tracelogging/etw") ("default" "etw")))) (r "1.63")))

(define-public crate-tracelogging_dynamic-1.0.1 (c (n "tracelogging_dynamic") (v "1.0.1") (d (list (d (n "tracelogging") (r "=1.0.1") (k 0)))) (h "16cz68vc6a8v01hzkg2vjvzknwl7py8wmm56w9c78w438zaxkl1y") (f (quote (("etw" "tracelogging/etw") ("default" "etw")))) (r "1.63")))

(define-public crate-tracelogging_dynamic-1.0.2 (c (n "tracelogging_dynamic") (v "1.0.2") (d (list (d (n "tracelogging") (r "=1.0.2") (k 0)))) (h "11ycnm8dfh96y00lk4i48hl9sdgd74sg8zlrs8n8gv2zs2zyvlm8") (f (quote (("etw" "tracelogging/etw") ("default" "etw")))) (r "1.63")))

(define-public crate-tracelogging_dynamic-1.1.0 (c (n "tracelogging_dynamic") (v "1.1.0") (d (list (d (n "tracelogging") (r "=1.1.0") (k 0)))) (h "14lydq409a6m8iysdn5zwyvkx4bglq0xkhqkizhnbcp3m5zf4mr8") (f (quote (("etw" "tracelogging/etw") ("default" "etw")))) (r "1.63")))

(define-public crate-tracelogging_dynamic-1.2.0 (c (n "tracelogging_dynamic") (v "1.2.0") (d (list (d (n "tracelogging") (r "=1.2.0") (k 0)))) (h "1vdpmdnx30khq8b38ibdlaszaygfpdmy9dd8gf4s16nhazkap640") (f (quote (("etw" "tracelogging/etw") ("default" "etw")))) (r "1.63")))

(define-public crate-tracelogging_dynamic-1.2.1 (c (n "tracelogging_dynamic") (v "1.2.1") (d (list (d (n "tracelogging") (r "=1.2.1") (k 0)))) (h "0wvhrrfyjqrpzzwjg2c8pbvz7hhlr3x8b1pw4gd5knbxfhyxk90w") (f (quote (("etw" "tracelogging/etw") ("default" "etw")))) (r "1.63")))

(define-public crate-tracelogging_dynamic-1.2.2 (c (n "tracelogging_dynamic") (v "1.2.2") (d (list (d (n "tracelogging") (r "=1.2.2") (k 0)))) (h "1lry87yf8182h76bhf04sxbdk055n85dzaajm78avkysk5zzwi6d") (f (quote (("etw" "tracelogging/etw") ("default" "etw")))) (r "1.63")))

