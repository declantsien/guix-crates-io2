(define-module (crates-io tr ac trackr) #:use-module (crates-io))

(define-public crate-trackr-0.1.0 (c (n "trackr") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "bitflags") (r "^2") (d #t) (k 2)) (d (n "trackr_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "0z0k242qd4j7vn67sl6hy3xj9h72kdcs8alqpxqqmsbfknmsg0w2")))

(define-public crate-trackr-0.1.1 (c (n "trackr") (v "0.1.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "bitflags") (r "^2") (d #t) (k 2)) (d (n "trackr_derive") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ljw51drjv3gx3ifk67hdi6022sw44x66hrp7rrkay8r56jc4sxv")))

