(define-module (crates-io tr ac traceparent) #:use-module (crates-io))

(define-public crate-traceparent-0.0.1 (c (n "traceparent") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)))) (h "11c65v4zvp0cazcpfzlaw8sqglp2s4v7vn2gdk0hl77w0hkclrc6")))

(define-public crate-traceparent-0.0.2 (c (n "traceparent") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)))) (h "1zkf77m0w10w23yycxnmws29v51p6w9zzk4avwmkhyav9s690pqx")))

(define-public crate-traceparent-0.0.3 (c (n "traceparent") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)))) (h "1mh7f0kiqg3jll8bjlam7wwzx42g89ppsqkbwpqhyfr5qkx6i60h")))

