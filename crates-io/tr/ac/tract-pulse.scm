(define-module (crates-io tr ac tract-pulse) #:use-module (crates-io))

(define-public crate-tract-pulse-0.11.0 (c (n "tract-pulse") (v "0.11.0") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "^0.11.0") (d #t) (k 0)))) (h "1088davfgbxc5dn1syb91bnndy4aflcj7l7zb34qipvsfcmdv73y")))

(define-public crate-tract-pulse-0.11.1 (c (n "tract-pulse") (v "0.11.1") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "^0.11.1") (d #t) (k 0)))) (h "00sbsvnid1qy7n5xvdl4rz328r7yxyj8ha4nq3v578w3ra1hny78")))

(define-public crate-tract-pulse-0.11.2 (c (n "tract-pulse") (v "0.11.2") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "^0.11.2") (d #t) (k 0)))) (h "168q8035d8zbmhz91mlblv2pp4gb81iwml12blc455yda4x1yqdy")))

(define-public crate-tract-pulse-0.12.0 (c (n "tract-pulse") (v "0.12.0") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "^0.12.0") (d #t) (k 0)))) (h "06drydd70895538v099gpbr61v3zja3wivy9hxd1zryj4npxqa29") (y #t)))

(define-public crate-tract-pulse-0.12.1 (c (n "tract-pulse") (v "0.12.1") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "^0.12.1") (d #t) (k 0)))) (h "0rb19c1bqgg3vs8y2hhakzdc3hziidd4scbzxsl5cj7jnq6kfsma")))

(define-public crate-tract-pulse-0.12.2 (c (n "tract-pulse") (v "0.12.2") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "^0.12.2") (d #t) (k 0)))) (h "05j7bajmsgaigm46xhvl5qc84ard6kzy0pg1qgmnm6znah711m61")))

(define-public crate-tract-pulse-0.12.3 (c (n "tract-pulse") (v "0.12.3") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "^0.12.3") (d #t) (k 0)))) (h "0kd841n7mdhi2q60gx22cnr59v807im7gqs2gsfzpx663x6376xl") (y #t)))

(define-public crate-tract-pulse-0.12.4 (c (n "tract-pulse") (v "0.12.4") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "^0.12.4") (d #t) (k 0)))) (h "0czb4nhyv026bklf1jixc9az304n14kpw6a49ak2vdqzvpflcr9a")))

(define-public crate-tract-pulse-0.12.5 (c (n "tract-pulse") (v "0.12.5") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "^0.12.5") (d #t) (k 0)))) (h "17wpjhw3d8yy6wqrdi2clk1gbf57p2rccr3qh3ba4rnhm7g5446x")))

(define-public crate-tract-pulse-0.13.0 (c (n "tract-pulse") (v "0.13.0") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.13.0") (d #t) (k 0)))) (h "02i2m7y5r35aim2i3kbi1413bwn4s21alwfnwhjbn25x1kjzfsjb")))

(define-public crate-tract-pulse-0.13.1 (c (n "tract-pulse") (v "0.13.1") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.13.1") (d #t) (k 0)))) (h "1b2lb6c53d7a1374hmjrhbrn7x9rc2phf13q5r6yqwnhkgyh5x0c")))

(define-public crate-tract-pulse-0.13.2 (c (n "tract-pulse") (v "0.13.2") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.13.2") (d #t) (k 0)))) (h "1wpihd99xvim2z8b0k2zcjbhdd9yvzxlgz7pigf7mvqrrkw4h0rw")))

(define-public crate-tract-pulse-0.14.0 (c (n "tract-pulse") (v "0.14.0") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.14.0") (d #t) (k 0)))) (h "1if1qxcmw4xbwdwc0ihm2w4x3h7bf7qh9dgd25yl1gn9jlgimvr8")))

(define-public crate-tract-pulse-0.14.1 (c (n "tract-pulse") (v "0.14.1") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.14.1") (d #t) (k 0)))) (h "0xj3bgxaddr6bvyncn4a9fh8rsjy0xfvijfm6b4fgr8kp3736vp4")))

(define-public crate-tract-pulse-0.14.2 (c (n "tract-pulse") (v "0.14.2") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.14.2") (d #t) (k 0)))) (h "110hpz49p3a1p41j6zi475dv0gzbfjz2bdi0k8i1hd8b7rm1v7g6")))

(define-public crate-tract-pulse-0.15.0 (c (n "tract-pulse") (v "0.15.0") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.15.0") (d #t) (k 0)))) (h "0xam3a86rv5lcz635bsc9ywnacsq3nsyd3n1mvavyh1inj39y9i1")))

(define-public crate-tract-pulse-0.15.1 (c (n "tract-pulse") (v "0.15.1") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.15.1") (d #t) (k 0)))) (h "0nz3w23mrp17ljyv6g41d4p9xk0k2awjbpn8acmbw0bbgkwgvirv")))

(define-public crate-tract-pulse-0.15.2 (c (n "tract-pulse") (v "0.15.2") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.15.2") (d #t) (k 0)))) (h "04z0xvfakrizqkal7h4ihc6izf6sm34l5xzks37my2f43wi1shs8")))

(define-public crate-tract-pulse-0.15.3 (c (n "tract-pulse") (v "0.15.3") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.15.3") (d #t) (k 0)))) (h "1cc1bjkiwzj271531msiphnk5yq6b9pmj2sykkh3v073skssyvfd")))

(define-public crate-tract-pulse-0.15.4 (c (n "tract-pulse") (v "0.15.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.15.4") (d #t) (k 0)))) (h "09f5ixrf38z2626d1ldywaci4hxd9hcyb6yskv89a5bbhsfg07ks")))

(define-public crate-tract-pulse-0.15.5 (c (n "tract-pulse") (v "0.15.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.15.5") (d #t) (k 0)))) (h "0v9n1pvk763spfxrplp083y304lql369kh4hvhw7d3qx3hfhr4c9")))

(define-public crate-tract-pulse-0.15.7 (c (n "tract-pulse") (v "0.15.7") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.15.7") (d #t) (k 0)))) (h "0anbqqggmwssjylrfqnwafhzl9xa797f0mc92mvzkv7l60ghg378")))

(define-public crate-tract-pulse-0.15.8 (c (n "tract-pulse") (v "0.15.8") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.15.8") (d #t) (k 0)))) (h "133qd52ds5jxc3v0vh8i8l2n125f2nxjjwb421pyrv7lwwq3xxwm")))

(define-public crate-tract-pulse-0.16.0 (c (n "tract-pulse") (v "0.16.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.16.0") (d #t) (k 0)))) (h "0qcfsi8pwk1zixyhfd5ykr1vq3v06hh09bhhc7nsc0shccpjmj9q")))

(define-public crate-tract-pulse-0.16.1 (c (n "tract-pulse") (v "0.16.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.16.1") (d #t) (k 0)))) (h "0rhl8l6m0kdkhkcvfd1d588jyikfpap3d8hk2fi5rc4ia23w9h07")))

(define-public crate-tract-pulse-0.16.2 (c (n "tract-pulse") (v "0.16.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.16.2") (d #t) (k 0)))) (h "1gwylw6z57z981mmcmgb4asjrc7idysc2z7wfxqghag35smdxqql")))

(define-public crate-tract-pulse-0.16.3 (c (n "tract-pulse") (v "0.16.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.16.3") (d #t) (k 0)))) (h "1wgw1qdlkivzlg49dcw17xw0m0i9ghm6bg98brlxvlnqdfwr1rc7")))

(define-public crate-tract-pulse-0.16.4 (c (n "tract-pulse") (v "0.16.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.16.4") (d #t) (k 0)))) (h "0nc1xvl4aws8mmvy8qc2qkidlb095j5djqbf66wimq2m8zar1j43")))

(define-public crate-tract-pulse-0.16.5 (c (n "tract-pulse") (v "0.16.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.16.5") (d #t) (k 0)))) (h "079xp1wvhhwhsh6fj3fkhkismpwdswiy0ijr0js2bnahx5zd5cmj")))

(define-public crate-tract-pulse-0.16.6 (c (n "tract-pulse") (v "0.16.6") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.16.6") (d #t) (k 0)))) (h "168fylz18gmygflzpbyv4b896mc4gg18qvd0vlpn2m4g9ys2f4qi")))

(define-public crate-tract-pulse-0.16.7 (c (n "tract-pulse") (v "0.16.7") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.16.7") (d #t) (k 0)))) (h "15a8vpjlk0489l7y9qvaddcp7qsn6grnmwfkpl16kmgndbd1n5il")))

(define-public crate-tract-pulse-0.16.8 (c (n "tract-pulse") (v "0.16.8") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.16.8") (d #t) (k 0)))) (h "17r7h2c31knrz6ljzvlwnq0xv91118gmlxnxc7kdfk6jzsk9jwsz")))

(define-public crate-tract-pulse-0.16.9 (c (n "tract-pulse") (v "0.16.9") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.16.9") (d #t) (k 0)))) (h "0ibd1fhxhihk4cg001fdkh77l9h8hm5r058mdxyx7m8xamwdaagd")))

(define-public crate-tract-pulse-0.17.0 (c (n "tract-pulse") (v "0.17.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.17.0") (d #t) (k 0)))) (h "0jrzil7jr801hbw74sp5yjvi05rdmxkgbhy5iq47c0pdxzkvha36")))

(define-public crate-tract-pulse-0.17.1 (c (n "tract-pulse") (v "0.17.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.17.1") (d #t) (k 0)))) (h "0dammk0xnd0mv0h2id82bhdrrwlmja7z16ar32jhdaa9xrpq3diq")))

(define-public crate-tract-pulse-0.17.2 (c (n "tract-pulse") (v "0.17.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.17.2") (d #t) (k 0)))) (h "0d1326z9v3mqvf8v9qhy7p5baxzm2vcbppgx83wy6wji3sywph0a")))

(define-public crate-tract-pulse-0.17.3 (c (n "tract-pulse") (v "0.17.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.17.3") (d #t) (k 0)))) (h "1cr89g44lr4i23833lkly1lm7j4bzcjir14yi52kck16ycd06iqs")))

(define-public crate-tract-pulse-0.17.4 (c (n "tract-pulse") (v "0.17.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.17.4") (d #t) (k 0)))) (h "1xqadcqdanfbri003ra6d0y4dwjgd576z1rahvn29j6cg2k6v7si")))

(define-public crate-tract-pulse-0.17.7 (c (n "tract-pulse") (v "0.17.7") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.17.7") (d #t) (k 0)))) (h "1dvl7dk8f5ab57dy656aiy6znqx9f9gmj890bch2sqckbzqxqpga")))

(define-public crate-tract-pulse-0.18.0 (c (n "tract-pulse") (v "0.18.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.18.0") (d #t) (k 0)))) (h "0f9bc0xyixxxp9gw7mdzv1qg3gj2fhakkk7aj69r9nknih4hw3da")))

(define-public crate-tract-pulse-0.17.8 (c (n "tract-pulse") (v "0.17.8") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.17.8") (d #t) (k 0)))) (h "1wva37jzypcqq5gm3yv7ppcinyd77qydxw47nrnjhch7v1ha1ii7")))

(define-public crate-tract-pulse-0.17.9 (c (n "tract-pulse") (v "0.17.9") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.17.9") (d #t) (k 0)))) (h "0vdvigsdw4dkrwb02hn1p8ikzk10mxm9q5ffdc8zs2fb7q6y7wc5")))

(define-public crate-tract-pulse-0.18.1 (c (n "tract-pulse") (v "0.18.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.18.1") (d #t) (k 0)))) (h "0c3ignzbwia444yds0g6zlqq2kcizsqij8dkbshbdq46jdvrbbxp")))

(define-public crate-tract-pulse-0.18.2 (c (n "tract-pulse") (v "0.18.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.18.2") (d #t) (k 0)))) (h "0a8izlhiyjmcql4g8pi5l2ng0aqis57rc9c5rl5rrxsrispfwwpk")))

(define-public crate-tract-pulse-0.18.3 (c (n "tract-pulse") (v "0.18.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.18.3") (d #t) (k 0)))) (h "1gdl5k3cq321p135ph65mdfz42kcr61qkm938y6qqk0hqzd3qm47")))

(define-public crate-tract-pulse-0.18.4 (c (n "tract-pulse") (v "0.18.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.18.4") (d #t) (k 0)))) (h "1pndbmsgqqahgd35vf6p16nkgvwd3qkzn3370fqbrxlfpzaawxqc")))

(define-public crate-tract-pulse-0.18.5 (c (n "tract-pulse") (v "0.18.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.18.5") (d #t) (k 0)))) (h "0x6llpckhk8g3lm5im9pmkpq2c6yfm3fpdp2mppz6pfvabbz9rwn")))

(define-public crate-tract-pulse-0.19.0-alpha.8 (c (n "tract-pulse") (v "0.19.0-alpha.8") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.0-alpha.8") (d #t) (k 0)))) (h "15r0xkw0sqspvwi6r6mi5l26ghn6yrnsrzihllbr4ggjwx73vb04")))

(define-public crate-tract-pulse-0.19.0-alpha.14 (c (n "tract-pulse") (v "0.19.0-alpha.14") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "^0.19.0-alpha.14") (d #t) (k 0)))) (h "18qbs7k6x6fnwd8fswvzyr4mkxa5qf4s6xpwa69cg27yy6bz6v2w")))

(define-public crate-tract-pulse-0.19.0-alpha.17 (c (n "tract-pulse") (v "0.19.0-alpha.17") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.0-alpha.17") (d #t) (k 0)))) (h "1vj2inglip6rhpsz5wxidni22vfdavnbvx1bmarz7nydx8d7yznj")))

(define-public crate-tract-pulse-0.19.0-alpha.18 (c (n "tract-pulse") (v "0.19.0-alpha.18") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.0-alpha.18") (d #t) (k 0)))) (h "1wcxdc340cj1shbxg96a7ayd9xqnx2ffiwdschjd70vhnipcw89a")))

(define-public crate-tract-pulse-0.19.0-alpha.19 (c (n "tract-pulse") (v "0.19.0-alpha.19") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.0-alpha.19") (d #t) (k 0)))) (h "07jwiiffd99dzjjn7l0ha4ym8b8lvqb8dhfcdd4czgbj7hknw566")))

(define-public crate-tract-pulse-0.19.0 (c (n "tract-pulse") (v "0.19.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.0") (d #t) (k 0)))) (h "1sijx84jav112dmb1zrqj6fc000fgsjjpgkr24g29729vm74cpgd")))

(define-public crate-tract-pulse-0.19.1 (c (n "tract-pulse") (v "0.19.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.1") (d #t) (k 0)))) (h "0vg9nanxcsnz87bpy8q9a7pc8bwfrg85c6yc8pwj6g1h9ls3vcab")))

(define-public crate-tract-pulse-0.19.2 (c (n "tract-pulse") (v "0.19.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.2") (d #t) (k 0)))) (h "1j9vvv398ilpx1528dyyzc2z5142vxplkch8pfbkxl7b72s6v9sm")))

(define-public crate-tract-pulse-0.19.3 (c (n "tract-pulse") (v "0.19.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.3") (d #t) (k 0)))) (h "0mprmnyrlzybkwilf2qlppbfg1fplkq2zkgac93yxy3yymvkgg0a") (y #t)))

(define-public crate-tract-pulse-0.19.5 (c (n "tract-pulse") (v "0.19.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.5") (d #t) (k 0)))) (h "0vf4vcsmdshgvbnhsk05vp4590s544p10zn8da5gdpzfjqha73wy")))

(define-public crate-tract-pulse-0.19.6 (c (n "tract-pulse") (v "0.19.6") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.6") (d #t) (k 0)))) (h "020rm4haklxz24fjgzkkg17d23k1pwzh0iqmdbd96p7p476q939y") (y #t)))

(define-public crate-tract-pulse-0.19.7 (c (n "tract-pulse") (v "0.19.7") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.7") (d #t) (k 0)))) (h "0mdj34q35pjfclgkfsg5f2p96ihcn1qj5pw63px0256vwc0f1zy9")))

(define-public crate-tract-pulse-0.19.8 (c (n "tract-pulse") (v "0.19.8") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.8") (d #t) (k 0)))) (h "03mk9kd5vyndyfw0l2j6wlz9p8hp1xcd787dc8mgys6n8m0lf95d")))

(define-public crate-tract-pulse-0.19.10 (c (n "tract-pulse") (v "0.19.10") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.10") (d #t) (k 0)))) (h "05gz0bgsja0lxx0n6cmwj42rzwhbyrjbj8vwnv5i3vicwlwhlh60")))

(define-public crate-tract-pulse-0.19.11 (c (n "tract-pulse") (v "0.19.11") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.11") (d #t) (k 0)))) (h "1z66fzz925qfvc3psfn9n4z37af2vz43h6zncy08mcsxxmf8180r")))

(define-public crate-tract-pulse-0.19.12 (c (n "tract-pulse") (v "0.19.12") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.12") (d #t) (k 0)))) (h "0n6nkydkkr2xfkwcdhq9p4bpkqb6ri2n1pjsvba5dnfgwldw06nc")))

(define-public crate-tract-pulse-0.19.13 (c (n "tract-pulse") (v "0.19.13") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.13") (d #t) (k 0)))) (h "0g3ndkwl5xqg3kqppvi754fsx33497hg91xkpma9hz0pchr6x219")))

(define-public crate-tract-pulse-0.19.14 (c (n "tract-pulse") (v "0.19.14") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.14") (d #t) (k 0)))) (h "1f4hgmk2h04c5gy33lhvdh2cf3115xwz0lx6cp559gzm8qlw20zi")))

(define-public crate-tract-pulse-0.19.15 (c (n "tract-pulse") (v "0.19.15") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.15") (d #t) (k 0)))) (h "1zxfswv8lqk4zq8hj7kq6xk3jz0wmwf3wh2wsvn8bnhk62wmanyg")))

(define-public crate-tract-pulse-0.20.0 (c (n "tract-pulse") (v "0.20.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.0") (d #t) (k 0)))) (h "1jvmzhnm9lgp7wi1h9dfra5wq7d0svdj4y26y3abg8a48x727wsk")))

(define-public crate-tract-pulse-0.20.2 (c (n "tract-pulse") (v "0.20.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.2") (d #t) (k 0)))) (h "1pifscs01i199ai7qh0iry2ns4rhrrsbkl6xhkiz8g9sywkdz5j8")))

(define-public crate-tract-pulse-0.20.3 (c (n "tract-pulse") (v "0.20.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.3") (d #t) (k 0)))) (h "00by71schjj74x98yb46lkjfwgnrk793245f8mg0q7n7yz4v49j2")))

(define-public crate-tract-pulse-0.20.4 (c (n "tract-pulse") (v "0.20.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.4") (d #t) (k 0)))) (h "1av9a0xphlgw3cki3h0gwla1g5kwdajr96allxqv0plg6jjzin95")))

(define-public crate-tract-pulse-0.20.5 (c (n "tract-pulse") (v "0.20.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.5") (d #t) (k 0)))) (h "0xbgbflvmbb34c4gp26jmi29d67ygliy6q9yf5ggxgsrwgz36dkj")))

(define-public crate-tract-pulse-0.20.6 (c (n "tract-pulse") (v "0.20.6") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.6") (d #t) (k 0)))) (h "00y5fbpg3nx4c3zm87fl9019988b6g3g4sln1n263z987hrxjbjg")))

(define-public crate-tract-pulse-0.19.16 (c (n "tract-pulse") (v "0.19.16") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.19.16") (d #t) (k 0)))) (h "1p92iz5kgfpr7vz1bmlbbw2q02dy7prb5fx1rb3x713crg6mwbsh")))

(define-public crate-tract-pulse-0.20.7 (c (n "tract-pulse") (v "0.20.7") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.7") (d #t) (k 0)))) (h "1hwhdbzxv8h9j1km9k9bwcl6409lyfhdjdj8mk3kl9f5inlzg59g")))

(define-public crate-tract-pulse-0.20.12 (c (n "tract-pulse") (v "0.20.12") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.12") (d #t) (k 0)))) (h "1chl193dwz2zsx63ijxbxlz2akiw7p7abriia5msns7ynhlzi0z5")))

(define-public crate-tract-pulse-0.20.13 (c (n "tract-pulse") (v "0.20.13") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.13") (d #t) (k 0)))) (h "0wivahs72s3m4gznhgi5as7q0n7dwz0r4hvp3jafq6v9ggd716xj")))

(define-public crate-tract-pulse-0.20.14 (c (n "tract-pulse") (v "0.20.14") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.14") (d #t) (k 0)))) (h "02zkz26kal0rpgcg37371mbd6kj4wdpj8mk13r29kkz0cpn225ir")))

(define-public crate-tract-pulse-0.20.15 (c (n "tract-pulse") (v "0.20.15") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.15") (d #t) (k 0)))) (h "0m8hxm2k8mg5gc5f6gri57iavm63ha7ckhq1c2ysd9bc2vvd39zg")))

(define-public crate-tract-pulse-0.20.16 (c (n "tract-pulse") (v "0.20.16") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.16") (d #t) (k 0)))) (h "094jqvadv71w9by9zyfyr7w0a85ic8zyaphlil877qz4ksjlm0qy")))

(define-public crate-tract-pulse-0.20.17 (c (n "tract-pulse") (v "0.20.17") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.17") (d #t) (k 0)))) (h "10g7j6mls35nyd4pfj5zzx81q84arazrhwjjbrzymmlqx8h9zs3j")))

(define-public crate-tract-pulse-0.20.18 (c (n "tract-pulse") (v "0.20.18") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.18") (d #t) (k 0)))) (h "0inl23jcisb9q1q224mx8is4gl922cyyr3h5hf93pz68dbvjh6pk")))

(define-public crate-tract-pulse-0.20.19 (c (n "tract-pulse") (v "0.20.19") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.19") (d #t) (k 0)))) (h "1y32120986zckv5wvlyp1gc48357pc8hq4mziyhm6izwa7lhhsvx")))

(define-public crate-tract-pulse-0.20.20 (c (n "tract-pulse") (v "0.20.20") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.20") (d #t) (k 0)))) (h "1a705xaflbxnkycz2xs404w94apfzwifw7vg76v66r0jg4n91anl")))

(define-public crate-tract-pulse-0.20.21 (c (n "tract-pulse") (v "0.20.21") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.21") (d #t) (k 0)))) (h "1ywsnwch5wi545dz46di42vgbpis1ga2ifm016hgpxn26a16dvaa")))

(define-public crate-tract-pulse-0.20.22 (c (n "tract-pulse") (v "0.20.22") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.20.22") (d #t) (k 0)))) (h "0c3dp7czlals19n5kwq7s34z74byiyxi0jxv64z47kgpm2ygp4ys")))

(define-public crate-tract-pulse-0.21.0 (c (n "tract-pulse") (v "0.21.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.21.0") (d #t) (k 0)))) (h "0aszq0fjb3mc2xbixzr98y3mddg0dwn41j9b6l9bcwn9phjcya0b")))

(define-public crate-tract-pulse-0.21.1 (c (n "tract-pulse") (v "0.21.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.21.1") (d #t) (k 0)))) (h "095rpqns8zd7s6cfx99gllbsmws0n718bf70874dg4p9cnzp1h3n")))

(define-public crate-tract-pulse-0.21.2 (c (n "tract-pulse") (v "0.21.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.21.2") (d #t) (k 0)))) (h "18sn2k68hgxa749pnszb2797drxkpn5461mwzjs934pnnqnnhj59") (y #t)))

(define-public crate-tract-pulse-0.21.3 (c (n "tract-pulse") (v "0.21.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.21.3") (d #t) (k 0)))) (h "15rhfrvfm0kc0shv9xy2v0ycsns1l7dvxg2zms83ikbk77q992jw")))

(define-public crate-tract-pulse-0.21.4 (c (n "tract-pulse") (v "0.21.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.21.4") (d #t) (k 0)))) (h "1b7q1i99sbga2ryazlzam7ri8k7kchk1azx4w467czi0dqrh9kax")))

(define-public crate-tract-pulse-0.21.5 (c (n "tract-pulse") (v "0.21.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tract-pulse-opl") (r "=0.21.5") (d #t) (k 0)))) (h "13yrkqnmrn0rqpvzi815kd4i1vzibl7npj0562v9j71z3z8bwzbj")))

