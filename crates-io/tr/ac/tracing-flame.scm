(define-module (crates-io tr ac tracing-flame) #:use-module (crates-io))

(define-public crate-tracing-flame-0.1.0 (c (n "tracing-flame") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tracing") (r "^0.1.12") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.3") (f (quote ("registry" "fmt"))) (k 0)))) (h "0m7sv016lvnxxrvrlw9zcmfs87sgq4g3lgrqa9wl6yv63kj0ylmx") (f (quote (("smallvec" "tracing-subscriber/smallvec") ("default" "smallvec"))))))

(define-public crate-tracing-flame-0.2.0 (c (n "tracing-flame") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tracing") (r "^0.1.12") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("registry" "fmt"))) (k 0)))) (h "1ad34bhy9gsj0ijn56jsvizydash6zcybbls29g1i2a7w5z13bhb") (f (quote (("smallvec" "tracing-subscriber/smallvec") ("default" "smallvec")))) (r "1.42.0")))

