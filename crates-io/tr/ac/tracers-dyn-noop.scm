(define-module (crates-io tr ac tracers-dyn-noop) #:use-module (crates-io))

(define-public crate-tracers-dyn-noop-0.1.0 (c (n "tracers-dyn-noop") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 1)) (d (n "tracers-core") (r "^0.1.0") (f (quote ("dynamic"))) (d #t) (k 0)))) (h "0aqb6sah62yiwqx24s483srm4jzl12fyp0cmpwg9acwcpi1bpv6l")))

