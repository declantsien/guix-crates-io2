(define-module (crates-io tr ac tracing-record-hierarchical) #:use-module (crates-io))

(define-public crate-tracing-record-hierarchical-0.1.0 (c (n "tracing-record-hierarchical") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes" "std"))) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("registry"))) (k 2)))) (h "188b466jqxklbgryaylqjp0kfpck1a982z8l5477zmw5pqplvcn8") (r "1.70")))

(define-public crate-tracing-record-hierarchical-0.1.1 (c (n "tracing-record-hierarchical") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes" "std"))) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("registry"))) (k 2)))) (h "1l7r7ryg9jz5zbrb3kb9wzffd6qc7fsm542r8lyyzzhcnwvbjw28") (r "1.70")))

