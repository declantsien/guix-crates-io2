(define-module (crates-io tr ac tracing-serde-wire) #:use-module (crates-io))

(define-public crate-tracing-serde-wire-0.1.0 (c (n "tracing-serde-wire") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)) (d (n "tracing-serde-structured") (r "^0.1") (k 0)))) (h "11b1mxb8c7y1y9jvbzd5fw175g583gpd4y88qclha7acc1fflcy5")))

(define-public crate-tracing-serde-wire-0.1.1 (c (n "tracing-serde-wire") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)) (d (n "tracing-serde-structured") (r "^0.1") (k 0)))) (h "1564iibp5qgbxjgmymi9jd37xwzfs40zsrakhx6hg747mn2ad27l")))

