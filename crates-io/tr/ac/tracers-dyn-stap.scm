(define-module (crates-io tr ac tracers-dyn-stap) #:use-module (crates-io))

(define-public crate-tracers-dyn-stap-0.1.0 (c (n "tracers-dyn-stap") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "tracers-core") (r "^0.1.0") (f (quote ("dynamic"))) (d #t) (k 0)) (d (n "tracers-libstapsdt-sys") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0d9h36k4iijqv03ckdpi0czhf9v34mdg453vhig68gncgb29ql2k") (f (quote (("required" "tracers-libstapsdt-sys/required") ("enabled" "tracers-libstapsdt-sys/enabled") ("default")))) (l "tracers-dyn-stap")))

