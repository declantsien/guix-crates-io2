(define-module (crates-io tr ac tracing-logstash) #:use-module (crates-io))

(define-public crate-tracing-logstash-0.1.0 (c (n "tracing-logstash") (v "0.1.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.2") (f (quote ("fmt"))) (k 0)))) (h "0v45n7ll56hxyv1r2hpqqsy8x26qmvqizq3jb1jbw8g203rp330j")))

(define-public crate-tracing-logstash-0.2.0 (c (n "tracing-logstash") (v "0.2.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt"))) (k 0)))) (h "0rqww1nryp1v34jlpwwnh3y3k9q0saxm1lv55syn4yp1s2dw1ld6")))

(define-public crate-tracing-logstash-0.3.0 (c (n "tracing-logstash") (v "0.3.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std" "formatting"))) (k 0)) (d (n "tracing-core") (r "^0") (k 0)) (d (n "tracing-subscriber") (r "^0") (f (quote ("fmt"))) (k 0)))) (h "1b0df6fp94zb0hgkwb2m97rb38vwj92vb1nad7kbbr9k3rb6jnb1")))

(define-public crate-tracing-logstash-0.3.1 (c (n "tracing-logstash") (v "0.3.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std" "formatting"))) (k 0)) (d (n "tracing-core") (r "^0") (k 0)) (d (n "tracing-subscriber") (r "^0") (f (quote ("fmt"))) (k 0)))) (h "077fibsircfqqjiqp133riq6k98xvl3hiva8z8bkxf4l9yl038yd")))

(define-public crate-tracing-logstash-0.4.0 (c (n "tracing-logstash") (v "0.4.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std" "formatting"))) (k 0)) (d (n "tracing-core") (r "^0") (k 0)) (d (n "tracing-subscriber") (r "^0") (f (quote ("fmt"))) (k 0)))) (h "1ll5jrfxv4wvv3hcwjrhn3xwxfgwllmiasi41ndpybjwl2khnn3l")))

(define-public crate-tracing-logstash-0.5.0 (c (n "tracing-logstash") (v "0.5.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std" "formatting"))) (k 0)) (d (n "tracing-core") (r "^0") (k 0)) (d (n "tracing-subscriber") (r "^0") (f (quote ("fmt"))) (k 0)))) (h "1v8sj8v0jzvsvd7gfwxbrh9y5g9bif10xbnaj48gn3m2hn03jwbk")))

(define-public crate-tracing-logstash-0.6.1 (c (n "tracing-logstash") (v "0.6.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std" "formatting"))) (k 0)) (d (n "tracing-core") (r "^0") (k 0)) (d (n "tracing-subscriber") (r "^0") (f (quote ("fmt"))) (k 0)))) (h "0jaa7kzpl0mq4dj9111gsgqx511f6xc6m0ki3v4x9jpy6sa3ql4g")))

(define-public crate-tracing-logstash-0.6.4 (c (n "tracing-logstash") (v "0.6.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std" "formatting"))) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros" "parsing"))) (d #t) (k 2)) (d (n "tracing") (r "^0") (d #t) (k 2)) (d (n "tracing-core") (r "^0") (k 0)) (d (n "tracing-subscriber") (r "^0") (f (quote ("fmt"))) (k 0)))) (h "12cr3i0c75lhq39s3wg3wjb93nscnfi87yc3dkgnqxvqx6brsflh")))

(define-public crate-tracing-logstash-0.7.0 (c (n "tracing-logstash") (v "0.7.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std" "formatting"))) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros" "parsing"))) (d #t) (k 2)) (d (n "tracing") (r "^0") (d #t) (k 2)) (d (n "tracing-core") (r "^0") (k 0)) (d (n "tracing-subscriber") (r "^0") (f (quote ("fmt"))) (k 0)))) (h "063wij6353y1zjn4akjkz1pd116p7wnrw2nqm8iawk5jax3hp469")))

