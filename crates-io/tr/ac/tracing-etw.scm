(define-module (crates-io tr ac tracing-etw) #:use-module (crates-io))

(define-public crate-tracing-etw-0.1.0 (c (n "tracing-etw") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-attributes") (r "^0.1") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2") (f (quote ("fmt" "registry"))) (k 0)) (d (n "win_etw_macros") (r "0.1.*") (d #t) (k 0)) (d (n "win_etw_provider") (r "0.1.*") (d #t) (k 0)))) (h "13fv714mlv7h6a3lbxmj2fzxbscsi9pr3pazwl79wwlzdygj0p5w") (y #t)))

