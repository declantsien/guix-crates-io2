(define-module (crates-io tr ac tracing-chrome) #:use-module (crates-io))

(define-public crate-tracing-chrome-0.1.0 (c (n "tracing-chrome") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.11") (d #t) (k 0)))) (h "0nnlc7hffp93l6f0k4ii5ai11vijp8ci82acmc5cwnnhzwfwkw51")))

(define-public crate-tracing-chrome-0.2.0 (c (n "tracing-chrome") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.11") (d #t) (k 0)))) (h "0cry654g8j575z85g9c3fcdkblvh05ppjf9d7anas3lb3nnhkwk8")))

(define-public crate-tracing-chrome-0.3.0 (c (n "tracing-chrome") (v "0.3.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.15") (d #t) (k 0)))) (h "0pzahh58kigb0sv9l2klk37ql11p98rpsqf797j84wpd3badqh9j")))

(define-public crate-tracing-chrome-0.3.1 (c (n "tracing-chrome") (v "0.3.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.15") (d #t) (k 0)))) (h "013qcma4fkwsfhqv9s0dxh0yhvfvgdbrnkmgdl98mwsswwjn7dd0")))

(define-public crate-tracing-chrome-0.4.0 (c (n "tracing-chrome") (v "0.4.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (d #t) (k 0)))) (h "0p21p3afpl30q3m03553rp3xm4xh4qvndwk522skyl5yrzq178kz")))

(define-public crate-tracing-chrome-0.5.0 (c (n "tracing-chrome") (v "0.5.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (d #t) (k 0)))) (h "1iw3namvrrszw2vxq3kkirxdcnji7jcpr7mpb0kx4z2c4j233cgw")))

(define-public crate-tracing-chrome-0.6.0 (c (n "tracing-chrome") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (d #t) (k 0)))) (h "0z15cw59l7gfm12618c7chja53cp7ijp9yaywrawgsa779m1zb6i")))

(define-public crate-tracing-chrome-0.7.0 (c (n "tracing-chrome") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.30") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "19nk357vpyalkqya315hlzbp1fjvvldgdfa7w1c1k5q1fx2icl46")))

(define-public crate-tracing-chrome-0.7.1 (c (n "tracing-chrome") (v "0.7.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.30") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "06pl66wzskcb0wkl2z99vw500ajlmmqv06dzpckzazvz8kakqss9")))

(define-public crate-tracing-chrome-0.7.2 (c (n "tracing-chrome") (v "0.7.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.9.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.32") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0977zy46gpawva2laffigxr2pph8v0xa51kfp6ghlifnsn7762mz")))

