(define-module (crates-io tr ac tracing-android) #:use-module (crates-io))

(define-public crate-tracing-android-0.1.0 (c (n "tracing-android") (v "0.1.0") (d (list (d (n "android_log-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.17") (d #t) (k 0)))) (h "014kl966dvqxf5a8qcss7c0pb99jh0sni9qq3rnkccx9bsjmxxjk")))

(define-public crate-tracing-android-0.1.1 (c (n "tracing-android") (v "0.1.1") (d (list (d (n "android_log-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.17") (d #t) (k 0)))) (h "0albsdyiykikcjnmnv15yx0yj7rjcxx87h51x0yh544lq3knjv0h")))

(define-public crate-tracing-android-0.1.2 (c (n "tracing-android") (v "0.1.2") (d (list (d (n "android_log-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 0)))) (h "11lg8sa80r1n8a4mgch66c9kxibc0cf84jvl72dqiri6r2mfz3nb")))

(define-public crate-tracing-android-0.1.3 (c (n "tracing-android") (v "0.1.3") (d (list (d (n "android_log-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 0)))) (h "0mb1s9za7nn9aci4wprs0h39d61w1sqyz44ff6hm2nfichqmc5cl")))

(define-public crate-tracing-android-0.1.4 (c (n "tracing-android") (v "0.1.4") (d (list (d (n "android_log-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 0)))) (h "0s4rpp9s0qrflhzan5rfqb5fi1xargy883zl6zfdh1gxwjj246z1")))

(define-public crate-tracing-android-0.1.6 (c (n "tracing-android") (v "0.1.6") (d (list (d (n "android_log-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.3") (d #t) (k 0)))) (h "0z3in8k6qjxckkzzzvcqpjzyf9z5ra839bmz3klb5cxrx2qbjw4l")))

(define-public crate-tracing-android-0.2.0 (c (n "tracing-android") (v "0.2.0") (d (list (d (n "android_log-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.6") (d #t) (k 0)))) (h "1h465qbcw3hjp4y3jfj74hddhygydbr3y4g7x869r838z3l2nq8j")))

