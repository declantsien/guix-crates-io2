(define-module (crates-io tr ac trackable_s3_stream) #:use-module (crates-io))

(define-public crate-trackable_s3_stream-0.1.0 (c (n "trackable_s3_stream") (v "0.1.0") (d (list (d (n "aws-config") (r "^0.51.0") (d #t) (k 2)) (d (n "aws-sdk-s3") (r "^0.21.0") (d #t) (k 0)) (d (n "aws-smithy-http") (r "^0.51.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "hyper") (r "^0.14.23") (f (quote ("stream"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 2)) (d (n "tokio") (r "^1.23.0") (f (quote ("fs" "macros"))) (d #t) (k 0)))) (h "0ijaqiqvj9dbdpn1qsg91cmny7lmddjwjz8rxhir5jl01i5wwrk4")))

(define-public crate-trackable_s3_stream-0.2.0 (c (n "trackable_s3_stream") (v "0.2.0") (d (list (d (n "aws-config") (r "^0.51.0") (d #t) (k 2)) (d (n "aws-sdk-s3") (r "^0.21.0") (d #t) (k 0)) (d (n "aws-smithy-http") (r "^0.51.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "hyper") (r "^0.14.23") (f (quote ("stream"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 2)) (d (n "tokio") (r "^1.23.0") (f (quote ("fs" "macros"))) (d #t) (k 0)))) (h "1ilgnf5c3b3dvi73dawwal3sdln6jk1n0cr7ncnll7cb7bdd8z80")))

