(define-module (crates-io tr ac trackermeta) #:use-module (crates-io))

(define-public crate-trackermeta-0.1.0 (c (n "trackermeta") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "009vqxkdq7i2384lxv31vsy9kfdn0jh6nxrp97ibllp8d0zjx10p")))

(define-public crate-trackermeta-0.1.1 (c (n "trackermeta") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "1zisyk0vz3w6hv31fzz0y632c3fjn9g1d8f3h05sz4pvwywmipj3") (f (quote (("overridable"))))))

(define-public crate-trackermeta-0.1.2 (c (n "trackermeta") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "00hxjg2apacmw1dk821cgnl0wvsvfbpmp6bfpyyn9xjknrbyix40") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.1.3 (c (n "trackermeta") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "087bk49zyj9gaxqgwbmm3k7qilhgpxhcs7j7819lqz14zcg7ziy1") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.2.0 (c (n "trackermeta") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "1zinsf5qzp8v1dqncp61nsl1msa9hlzkp1g97vmn8xnnl483l9hi") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.2.1 (c (n "trackermeta") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "11qc7gjsqnlhw3gyqk9vwc1pygdxyaz6k1c6m73g4vbgzvy5a1xa") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.2.2 (c (n "trackermeta") (v "0.2.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "0bvasvd9ng4i5yi3glwqj0bqwn05j5frc3pxis2yim0nm570jfra") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.2.3 (c (n "trackermeta") (v "0.2.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "1pcwrvqifnkbv657x4c3p8ha20whcf5dp4bj6lal1qldyv7c7rwf") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.3.0 (c (n "trackermeta") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "0cp5m8922vrh3va3f2dk21yfhzyx53dvlplmc2hh76fn8prjb36a") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.3.1 (c (n "trackermeta") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "1shwn8mml4j4s6l8a4mxppapyp3lim6sjrvgh03ab0y4zhx2b7z7") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.4.0 (c (n "trackermeta") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "009x2kkiwrvj5ssg8czgbhga0crk79q9j5b4bvq1y964mcqnxniy") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.4.1 (c (n "trackermeta") (v "0.4.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "0919rii2xcx1zsicgf1wla8qlmhfhljhbsrs7jn4jj5qmax268dv") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.4.2 (c (n "trackermeta") (v "0.4.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "0vdkny5bw99ysfr2jvd9h723zwpj5z933xs99k3g7zlp49bgynhc") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.4.3 (c (n "trackermeta") (v "0.4.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "1n7f9i874xv13apfshbclmfnxnd586hwaksh8dc0ig6am2xdhyvz") (f (quote (("overridable") ("infinity-retry"))))))

(define-public crate-trackermeta-0.5.0 (c (n "trackermeta") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1.1") (d #t) (k 0)) (d (n "tl") (r "^0.7.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "12s573m63wxqvrg3kaz4v7b6dxv2p2gv536ska4mairkxrp8aqmi") (f (quote (("infinity-retry"))))))

(define-public crate-trackermeta-0.5.1 (c (n "trackermeta") (v "0.5.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1") (d #t) (k 0)) (d (n "tl") (r "^0.7") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "0b6fiizrihsl016gxvd6lfvslrsajmxgv6z9alzjh7a013hbq1ax") (f (quote (("infinity-retry"))))))

