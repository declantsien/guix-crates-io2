(define-module (crates-io tr ac tracing-ez) #:use-module (crates-io))

(define-public crate-tracing-ez-0.1.0 (c (n "tracing-ez") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "14cb4janjix27iwmxkjklq025piran0bc2921wb033bdc69v9vwf")))

(define-public crate-tracing-ez-0.2.0 (c (n "tracing-ez") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1yv2akdg0jjnz9b68k7k09rcwqdh22ssgmpj21gwlazhhwh49c74")))

(define-public crate-tracing-ez-0.3.0 (c (n "tracing-ez") (v "0.3.0") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1diq54fniayr0508bm23268n7j4qr00chvp69xpx28dlcaw8pqxx")))

