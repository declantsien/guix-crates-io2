(define-module (crates-io tr ac tractor) #:use-module (crates-io))

(define-public crate-tractor-0.0.1 (c (n "tractor") (v "0.0.1") (d (list (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)))) (h "1ppkf75wan04470zwhnb1ldxyvwdhgqvclcll7vfjfbjvybqkzv2")))

(define-public crate-tractor-0.0.2 (c (n "tractor") (v "0.0.2") (d (list (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "tractor-macros") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0vr611wxnggrm5vp4l73l549kzj4iwyh0hlgdbnbx8dscjhj2sb2") (f (quote (("macros" "tractor-macros") ("default" "macros"))))))

(define-public crate-tractor-0.0.3 (c (n "tractor") (v "0.0.3") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tractor-macros") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "1rk2i6nfv3xdnjgn97pznhzijrsp42f4jayrp2hk3fvwxfs7skj5") (f (quote (("macros" "tractor-macros") ("default" "macros"))))))

(define-public crate-tractor-0.0.4 (c (n "tractor") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tractor-macros") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "0d3gw3l1gq97lh5p1v70icjmcldg5gkjf11qz7bz7qs00f74cm1i") (f (quote (("macros" "tractor-macros") ("default" "macros"))))))

(define-public crate-tractor-0.0.5 (c (n "tractor") (v "0.0.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tractor-macros") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "0k9fvyccjxgwf42abqyixamzbgziqb65lx52c41fh27cbvbhdxqc") (f (quote (("macros" "tractor-macros") ("default" "macros"))))))

(define-public crate-tractor-0.0.6 (c (n "tractor") (v "0.0.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tractor-macros") (r "^0.0.6") (o #t) (d #t) (k 0)))) (h "0h84gzany2bysnkq6z2qidqn698gx26aa6zpz8i4damm3mpckj4c") (f (quote (("macros" "tractor-macros") ("default" "macros"))))))

