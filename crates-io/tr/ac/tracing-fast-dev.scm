(define-module (crates-io tr ac tracing-fast-dev) #:use-module (crates-io))

(define-public crate-tracing-fast-dev-0.0.1 (c (n "tracing-fast-dev") (v "0.0.1") (d (list (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1zx99kxp66hscmd6rh3jd67b0l31c1ms80p367m4dcrdf01psp9p") (y #t)))

(define-public crate-tracing-fast-dev-0.0.2 (c (n "tracing-fast-dev") (v "0.0.2") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0xcw4fknv4brcdhd4548r8l21akbipjd2ks67qhlx7bi9cc62yd8") (y #t)))

(define-public crate-tracing-fast-dev-0.0.3 (c (n "tracing-fast-dev") (v "0.0.3") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1a38qzx92kwkj8jfjlx2m1h0s0dk5klxrzvdshkvq59p5yj6hpyx") (y #t)))

(define-public crate-tracing-fast-dev-0.1.0 (c (n "tracing-fast-dev") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "11ssscn3d8s80dnmcgdndqjq39rigb9lcvck1bizyj3nhx2dgcnf") (y #t)))

(define-public crate-tracing-fast-dev-0.2.0 (c (n "tracing-fast-dev") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1akj5k8ms8xmsg8sks6krsz3fammq891zb7fsfi4mklkfyz4plpn")))

(define-public crate-tracing-fast-dev-1.0.0 (c (n "tracing-fast-dev") (v "1.0.0") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1l9vz56bxkzn811pj3jphs08h9lbaa005vy9svpnbc2zj9frrfv7")))

