(define-module (crates-io tr ac tracing-assert-core) #:use-module (crates-io))

(define-public crate-tracing-assert-core-0.1.4-beta.26.0 (c (n "tracing-assert-core") (v "0.1.4-beta.26.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (d #t) (k 0)))) (h "1hvbx7kv8knijwjhr3145n9ai1ax63d3sqj2xc92w3alwp4x3l31")))

(define-public crate-tracing-assert-core-0.1.4-beta.27.0 (c (n "tracing-assert-core") (v "0.1.4-beta.27.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (d #t) (k 0)))) (h "016jf5ixzi4qy75jmdh8b9z9k99c82bfyppvmcwaxq00cfmpaljj")))

(define-public crate-tracing-assert-core-0.1.3 (c (n "tracing-assert-core") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (d #t) (k 0)))) (h "0ibh1hkkyfifc9x91yxyaz8dlq6y3y9rfz6vnqw9p605nqx4m8ny")))

(define-public crate-tracing-assert-core-0.1.4-beta.30.0 (c (n "tracing-assert-core") (v "0.1.4-beta.30.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (d #t) (k 0)))) (h "15ry42mw7agcx9ml743m1d68li37ydwd9bwq3i55ydqabd4zbdsi")))

(define-public crate-tracing-assert-core-0.1.4 (c (n "tracing-assert-core") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (d #t) (k 0)))) (h "0fc9ngcv2ab8ljsrvzy7x92p5lfpah2jkqlvrhcjqlfwbv2981ik")))

