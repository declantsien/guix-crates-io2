(define-module (crates-io tr ac tracefp) #:use-module (crates-io))

(define-public crate-tracefp-0.0.1 (c (n "tracefp") (v "0.0.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 2)))) (h "1xq3ga9gxp9l8avhcgs59mlvmhsr54jprbai2lc9xhmpswxvx1k0") (f (quote (("memory-access-check") ("default" "memory-access-check"))))))

