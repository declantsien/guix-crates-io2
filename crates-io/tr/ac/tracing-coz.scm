(define-module (crates-io tr ac tracing-coz) #:use-module (crates-io))

(define-public crate-tracing-coz-0.1.0 (c (n "tracing-coz") (v "0.1.0") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "coz") (r "^0.1.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.9") (d #t) (k 0)))) (h "1vq04iv2mynf149baaj3msx08cirxfghzdh2x0ivi7l98yf0g6q8") (y #t)))

(define-public crate-tracing-coz-0.1.1 (c (n "tracing-coz") (v "0.1.1") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "coz") (r "^0.1.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.9") (d #t) (k 0)))) (h "1n2zf93ci2igrfsqmzm0y9h5aj7ch5g1k4zjp3d0vny2jh2s7wlm")))

