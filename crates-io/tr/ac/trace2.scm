(define-module (crates-io tr ac trace2) #:use-module (crates-io))

(define-public crate-trace2-0.1.0 (c (n "trace2") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "trace2_macro") (r "^0.1") (d #t) (k 0)))) (h "1gxbzf7m14k2ci6vgwc9ryw3d6ync0nk30wm2vy51kysrccdhj7a")))

