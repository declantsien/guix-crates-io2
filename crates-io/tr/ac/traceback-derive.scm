(define-module (crates-io tr ac traceback-derive) #:use-module (crates-io))

(define-public crate-traceback-derive-0.1.0 (c (n "traceback-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)))) (h "0rzp347idvjq5qfh6pf359q23dyrrv33ldfw8mcsr93323hshvav") (r "1.31")))

(define-public crate-traceback-derive-0.1.1 (c (n "traceback-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)))) (h "1wh73dgd11blg97bk39zash3jzm4pp87ip8q3jb8kdck7g54a3by") (r "1.31")))

