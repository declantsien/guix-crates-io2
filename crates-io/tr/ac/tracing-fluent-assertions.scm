(define-module (crates-io tr ac tracing-fluent-assertions) #:use-module (crates-io))

(define-public crate-tracing-fluent-assertions-0.1.0 (c (n "tracing-fluent-assertions") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.29") (k 0)) (d (n "tracing-core") (r "^0.1.21") (k 0)) (d (n "tracing-subscriber") (r "^0.2.25") (k 0)))) (h "0aq24x0wnizrggwiwic7207f6f92syr6jng3x9m0vin56kn60n64")))

(define-public crate-tracing-fluent-assertions-0.1.1 (c (n "tracing-fluent-assertions") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-core") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.2") (k 0)))) (h "1sr2fn9amnxbyllq555rfnsrsq4xgp5bszwarlbbnmbfijgdsg2g")))

(define-public crate-tracing-fluent-assertions-0.1.2 (c (n "tracing-fluent-assertions") (v "0.1.2") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-core") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.2") (k 0)))) (h "0a6x21r3prvhs3nnv6j2yjr4m9gz6crp2jc0c1xp360myx96h153")))

(define-public crate-tracing-fluent-assertions-0.1.3 (c (n "tracing-fluent-assertions") (v "0.1.3") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-core") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.2") (k 0)))) (h "0753gm84p7mxqmcfy6b6ncc7wc97gldxs8xbi43wcqgl2v8lkic6")))

(define-public crate-tracing-fluent-assertions-0.2.0 (c (n "tracing-fluent-assertions") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-core") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.2") (k 0)))) (h "0a2ypn97a1wqcnhddbk0xiz52w98cjfqnk8cs18xgqv9p399l2a4")))

(define-public crate-tracing-fluent-assertions-0.3.0 (c (n "tracing-fluent-assertions") (v "0.3.0") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-core") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.3") (k 0)))) (h "1xparzr0n2svgqgwc13s2cqwifkbb65k0dp80m1n3vngdf61mphj")))

