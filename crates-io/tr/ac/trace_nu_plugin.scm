(define-module (crates-io tr ac trace_nu_plugin) #:use-module (crates-io))

(define-public crate-trace_nu_plugin-0.3.0 (c (n "trace_nu_plugin") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "io-util" "io-std" "process" "macros" "fs"))) (d #t) (k 0)))) (h "1lizkai2djw5q52d8dv8zwf4xd10752niv7v6f63c9gzs8gdqnmw")))

(define-public crate-trace_nu_plugin-0.3.1 (c (n "trace_nu_plugin") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "io-util" "io-std" "process" "macros" "fs"))) (d #t) (k 0)))) (h "1wk1f04rg4r1sm928nr7jwrcx1b2bki50555i7l5fsq8bgjy9ib6")))

