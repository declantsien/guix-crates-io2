(define-module (crates-io tr ac tracing-subscriber-wasm) #:use-module (crates-io))

(define-public crate-tracing-subscriber-wasm-0.1.0 (c (n "tracing-subscriber-wasm") (v "0.1.0") (d (list (d (n "gloo") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "16582fzxdfp5m0n8sgksw57bqbaana75161xwp4ccwq1k204x03r")))

