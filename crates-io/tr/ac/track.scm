(define-module (crates-io tr ac track) #:use-module (crates-io))

(define-public crate-track-0.0.0 (c (n "track") (v "0.0.0") (h "1x2y7p42cv4qs3fl0vwxild96mlg1b350ajz9zwx5nv6i9m1i4lx")))

(define-public crate-track-0.1.0 (c (n "track") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde-diff") (r "^0.2.0") (d #t) (k 0)) (d (n "track-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0n0dwrpp5a4j216hnrj25pnl1slyynra31jq6zrgkrmhays7aqh7") (f (quote (("rmp-serialization" "rmp-serde") ("default" "bincode-serialization") ("bincode-serialization" "bincode"))))))

