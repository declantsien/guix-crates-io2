(define-module (crates-io tr ac tracing-actix) #:use-module (crates-io))

(define-public crate-tracing-actix-0.0.1 (c (n "tracing-actix") (v "0.0.1") (d (list (d (n "actix") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.9") (d #t) (k 0)))) (h "03s8yy8mq5sy0vr63y7223ifhckmsydj1cxb1hzp9n8p0sxnch0r")))

(define-public crate-tracing-actix-0.1.0 (c (n "tracing-actix") (v "0.1.0") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0128qzz8s7w844gam91ai8hj412yr83mcrjiswk2gr4z2yb2218r")))

(define-public crate-tracing-actix-0.2.0 (c (n "tracing-actix") (v "0.2.0") (d (list (d (n "actix") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0zdhd8k9v8kvwz5hadv5df9c83c12jbxdlv2dqlc070svsd7ydk8")))

(define-public crate-tracing-actix-0.3.0 (c (n "tracing-actix") (v "0.3.0") (d (list (d (n "actix") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "12hcf4xd9rjp8bpjvh1xi1lq3910x9r1pffqx7bhv95mfv422lwa")))

(define-public crate-tracing-actix-0.4.0 (c (n "tracing-actix") (v "0.4.0") (d (list (d (n "actix") (r "^0.13") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1rv1l8z9zfmxm060rn0l78zss67vxcjbw8623xnwiq66hxp365y8")))

