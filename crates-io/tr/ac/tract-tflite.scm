(define-module (crates-io tr ac tract-tflite) #:use-module (crates-io))

(define-public crate-tract-tflite-0.20.12 (c (n "tract-tflite") (v "0.20.12") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.20.12") (d #t) (k 0)))) (h "01xb94cqnsw29xnzi83rbzbrqcrwp9ncdv2s7s271zmc3y84ls46")))

(define-public crate-tract-tflite-0.20.13 (c (n "tract-tflite") (v "0.20.13") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.20.13") (d #t) (k 0)))) (h "0y26c5p1zvsqgh5y2rf68nvpispa6bzpc7dbfn03sngbfv4ngqib")))

(define-public crate-tract-tflite-0.20.14 (c (n "tract-tflite") (v "0.20.14") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.20.14") (d #t) (k 0)))) (h "1k196cw9v3457glayin6d5v02vxz4r803i36kvd0lw3w7x9r6man")))

(define-public crate-tract-tflite-0.20.15 (c (n "tract-tflite") (v "0.20.15") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.20.15") (d #t) (k 0)))) (h "182nj8pyr1zbkx7735791zpi1xlnvlyqy9lbgfg7mh86g2vcjlz2")))

(define-public crate-tract-tflite-0.20.16 (c (n "tract-tflite") (v "0.20.16") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.20.16") (d #t) (k 0)))) (h "04ixwldci3k5ks8sa32idd5s8nbina7v2cn5iikxlm94n338ay7l")))

(define-public crate-tract-tflite-0.20.17 (c (n "tract-tflite") (v "0.20.17") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.20.17") (d #t) (k 0)))) (h "0jq5p6rplnb3fmnpc0pbd38l9ihynxhpn1lczxx5s0iml7r5qvjw")))

(define-public crate-tract-tflite-0.20.18 (c (n "tract-tflite") (v "0.20.18") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.20.18") (d #t) (k 0)))) (h "0gx9b7jxxjy0pgjhpsjk1csizh1zwlgacv13wf96q4920qc7axnr")))

(define-public crate-tract-tflite-0.20.19 (c (n "tract-tflite") (v "0.20.19") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.20.19") (d #t) (k 0)))) (h "12lg638xmn6zhf0nip7kp9y84j3hhz5rwncxj3dkr4m0c2wj38h7")))

(define-public crate-tract-tflite-0.20.20 (c (n "tract-tflite") (v "0.20.20") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.20.20") (d #t) (k 0)))) (h "0005h8sw96kwszcjwqmc52cj331szw358ayigmjwnwq1rybclqbi")))

(define-public crate-tract-tflite-0.20.21 (c (n "tract-tflite") (v "0.20.21") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.20.21") (d #t) (k 0)))) (h "1hx4m7k0f4bqivsr89rw04pw875vrg5wzpzbqqn7m0j8y89wrs74")))

(define-public crate-tract-tflite-0.20.22 (c (n "tract-tflite") (v "0.20.22") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.20.22") (d #t) (k 0)))) (h "1p40r6nxsxmbxvxmzpv457k0a97c0lh9x4a2riwn2g5mzp591kp7")))

(define-public crate-tract-tflite-0.21.0 (c (n "tract-tflite") (v "0.21.0") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.21.0") (d #t) (k 0)))) (h "0lkd0hi1682y42rlj5b6p1zyaw0rwi49y1dwxv27djlsqvl1mbmd")))

(define-public crate-tract-tflite-0.21.1 (c (n "tract-tflite") (v "0.21.1") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.21.1") (d #t) (k 0)))) (h "0fjdfnphivfrk7zxbcg5yx2c88kgjx6k5qbw4bkjl9q9b6y3mbww")))

(define-public crate-tract-tflite-0.21.2 (c (n "tract-tflite") (v "0.21.2") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.21.2") (d #t) (k 0)))) (h "1xpf1h5qcxg0v40xrpzaab2n3vy2q487rnqixa672mbvi93ky3zq") (y #t)))

(define-public crate-tract-tflite-0.21.3 (c (n "tract-tflite") (v "0.21.3") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.21.3") (d #t) (k 0)))) (h "0z5mhf4kziwjgnafyx34v7g6jaik41148qqnjbxxdkv4mgns53aw")))

(define-public crate-tract-tflite-0.21.4 (c (n "tract-tflite") (v "0.21.4") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.21.4") (d #t) (k 0)))) (h "1rxpc1galyr5z15xaz286vl26nq752x6cy9np71xs5ppxbafg91g")))

(define-public crate-tract-tflite-0.21.5 (c (n "tract-tflite") (v "0.21.5") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "tract-core") (r "=0.21.5") (d #t) (k 0)))) (h "0b6fdv1f8wzdlqvr088m681h5svaf3nrchx7ax622mjbb26w8pby")))

