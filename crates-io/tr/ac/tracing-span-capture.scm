(define-module (crates-io tr ac tracing-span-capture) #:use-module (crates-io))

(define-public crate-tracing-span-capture-0.0.1 (c (n "tracing-span-capture") (v "0.0.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "1kgbqm4ssr8qsw0i0cj25p3cm77zqd6cx999yfij7cm9k4zkvm0f") (r "1.56")))

