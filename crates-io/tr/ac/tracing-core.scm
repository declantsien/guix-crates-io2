(define-module (crates-io tr ac tracing-core) #:use-module (crates-io))

(define-public crate-tracing-core-0.0.0 (c (n "tracing-core") (v "0.0.0") (h "16vx3wzi94f5q8ikbmcw1dm726jqf0i49r89dw2wkp0yflh3yqxy")))

(define-public crate-tracing-core-0.1.0 (c (n "tracing-core") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "0jqb6xsdldw7aalzj3ldbjrk427cmhcgy5sznbdf5nfxwp538pvx")))

(define-public crate-tracing-core-0.1.1 (c (n "tracing-core") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1040lsdyzh2sx62b98qrzijwmkzcidn7fq42a82yssbg6pp1fyv4")))

(define-public crate-tracing-core-0.1.2 (c (n "tracing-core") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "01fa73wzw2m5ybi3kkd52dgrw97mgc3i6inmhwys46ab28giwnxi")))

(define-public crate-tracing-core-0.1.3 (c (n "tracing-core") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1fn6zd269h4vddm2792mi9cy2zcnif4h1zbczwalwf6fbpvvvs8b")))

(define-public crate-tracing-core-0.1.4 (c (n "tracing-core") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (t "cfg(not(feature = \"std\"))") (k 0)) (d (n "spin") (r "^0.5") (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "0kyp5wfiwy8ffv3zsag214mqyw3v2phk9ydzqxqnapzj28bfk4m0") (f (quote (("std") ("default" "std"))))))

(define-public crate-tracing-core-0.1.5 (c (n "tracing-core") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (t "cfg(not(feature = \"std\"))") (k 0)) (d (n "spin") (r "^0.5") (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "1n5n6gkbral0k1mhd4k5hljvkhj6gr5kvy97h72hqw7ilpigajp9") (f (quote (("std") ("default" "std"))))))

(define-public crate-tracing-core-0.1.6 (c (n "tracing-core") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (t "cfg(not(feature = \"std\"))") (k 0)) (d (n "spin") (r "^0.5") (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "0sk47prqhrq9ikch89s0aq6mnxriq13b102ig4sark8nmax8x32j") (f (quote (("std") ("default" "std"))))))

(define-public crate-tracing-core-0.1.7 (c (n "tracing-core") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (t "cfg(not(feature = \"std\"))") (k 0)) (d (n "spin") (r "^0.5") (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "07dgaq67w82iik5jy2lzrbg72jcqls7ysd8ysav5kj90qm3kd4dw") (f (quote (("std") ("default" "std"))))))

(define-public crate-tracing-core-0.1.8 (c (n "tracing-core") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "07r1bj5ipp7q1jv3hnxxsa8qxn59ip1xh9pwbhf067faqqd53yyp") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.9 (c (n "tracing-core") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "0y0rcvvqq89yaiz0qdx88byxgz8j6hsm9slq8d5vvf3jwc8nz90k") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.10 (c (n "tracing-core") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "05dpa21valy0c6nj5ybn951774icxhdb70cwq0ida7088yd3ma0a") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.11 (c (n "tracing-core") (v "0.1.11") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "13qpxhzyjxcc8fsypkawzll9krvjzramr2diyf30gqcasbq7bbll") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.12 (c (n "tracing-core") (v "0.1.12") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "1my8nfnc8m37pcx236iza50pwg3a5j6d3ihndxl9g9lg09d4nwxj") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.13 (c (n "tracing-core") (v "0.1.13") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "1yalb7sa6nqq9rl8sjl9m12qfdfv4msi3w280ry03g4yyn5gk4ym") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.14 (c (n "tracing-core") (v "0.1.14") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "0i9mig6849il9wspwgds08mgipwkrh3kg0vdlcyb85n34ckncqyv") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.15 (c (n "tracing-core") (v "0.1.15") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "1db14j2c5qnib2pxd5gllql9sc0nlhy01irby4h9ps84k1w003jg") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.16 (c (n "tracing-core") (v "0.1.16") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "16hisz8nvbav9q6r5lbar2baac097n33q7xqssifwsphy70ldksv") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.17 (c (n "tracing-core") (v "0.1.17") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "0pvbgv301vw6dq4krc14yqbyyixb42lcs4s57xw05llkgy9f63gm") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.18 (c (n "tracing-core") (v "0.1.18") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "0lm0li6lx75jgbbgxsb45439257sqb0j7828caf2hjqsigwi9zx9") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.19 (c (n "tracing-core") (v "0.1.19") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "1y4x3hxns8k9gfkz8lnbx23wxvp5bwkmrvbj6066xf8g7zs1g99c") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.20 (c (n "tracing-core") (v "0.1.20") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "1krsibdghzcg5rnl4g74lxnl23j2bamkwsbd1hdwh8b1q845c4j6") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.21 (c (n "tracing-core") (v "0.1.21") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "1r262wskhm6wmc5i2bxz44nglyzqaq3x50s0h5q0ffdq6xbdckhz") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-tracing-core-0.1.22 (c (n "tracing-core") (v "0.1.22") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "valuable") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)))) (h "08wssa1n70vg02nfw6ykfzjhind88ws8vjqi64nsfch6718wpkq3") (f (quote (("std" "lazy_static") ("default" "std" "valuable/std")))) (r "1.42.0")))

(define-public crate-tracing-core-0.1.23 (c (n "tracing-core") (v "0.1.23") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "valuable") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)))) (h "0v3hy8bg4s3b4axz6l6adb3g73rf0795s5nqji6w629cljgnccda") (f (quote (("std" "lazy_static") ("default" "std" "valuable/std")))) (r "1.49.0")))

(define-public crate-tracing-core-0.1.24 (c (n "tracing-core") (v "0.1.24") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "valuable") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)))) (h "1vmlkd6nip65vcwcq5zlwch9ljighjp75vj8nphwjmrgxs2jji4h") (f (quote (("std" "lazy_static") ("default" "std" "valuable/std")))) (r "1.49.0")))

(define-public crate-tracing-core-0.1.25 (c (n "tracing-core") (v "0.1.25") (d (list (d (n "lazy_static") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "valuable") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)))) (h "0412vfjd97rnrsjxjpbvljhxmma2lxhvajz5x0v0y58v4krykz3d") (f (quote (("std" "lazy_static") ("default" "std" "valuable/std")))) (r "1.49.0")))

(define-public crate-tracing-core-0.1.26 (c (n "tracing-core") (v "0.1.26") (d (list (d (n "lazy_static") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "valuable") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)))) (h "0bq7c1y28hi7mli25pj9iljam4vcnlqk7zf2k3a8c67822kqqk7m") (f (quote (("std" "lazy_static") ("default" "std" "valuable/std")))) (r "1.49.0")))

(define-public crate-tracing-core-0.1.27 (c (n "tracing-core") (v "0.1.27") (d (list (d (n "once_cell") (r "^1.12") (o #t) (d #t) (k 0)) (d (n "valuable") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)))) (h "08fr2y0nm0as3ar22xw3qspwjfbx1a4byzp8wmf9d93qi1dmj2bp") (f (quote (("std" "once_cell") ("default" "std" "valuable/std")))) (r "1.49.0")))

(define-public crate-tracing-core-0.1.28 (c (n "tracing-core") (v "0.1.28") (d (list (d (n "once_cell") (r "^1.12") (o #t) (d #t) (k 0)) (d (n "valuable") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)))) (h "1rsw94rhkqwsrd45n7pbx21g6myc39hyvanj4brp9wpj76z5hwvv") (f (quote (("std" "once_cell") ("default" "std" "valuable/std")))) (r "1.49.0")))

(define-public crate-tracing-core-0.1.29 (c (n "tracing-core") (v "0.1.29") (d (list (d (n "once_cell") (r "^1.13.0") (o #t) (d #t) (k 0)) (d (n "valuable") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)))) (h "1xr2dqar64fj4y43vy0xvaxs6n3xssd3z0jbf408lmbn60qa9vjs") (f (quote (("std" "once_cell") ("default" "std" "valuable/std")))) (r "1.49.0")))

(define-public crate-tracing-core-0.1.30 (c (n "tracing-core") (v "0.1.30") (d (list (d (n "once_cell") (r "^1.13.0") (o #t) (d #t) (k 0)) (d (n "valuable") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)))) (h "0fi1jz3jbzk3n7k379pwv3wfhn35c5gcwn000m2xh7xb1sx07sr4") (f (quote (("std" "once_cell") ("default" "std" "valuable/std")))) (r "1.49.0")))

(define-public crate-tracing-core-0.1.31 (c (n "tracing-core") (v "0.1.31") (d (list (d (n "once_cell") (r "^1.13.0") (o #t) (d #t) (k 0)) (d (n "valuable") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)))) (h "16pp28izw9c41m7c55qsghlz07r9ark8lzd3x6ig3xhxg89vhm89") (f (quote (("std" "once_cell") ("default" "std" "valuable/std")))) (r "1.56.0")))

(define-public crate-tracing-core-0.1.32 (c (n "tracing-core") (v "0.1.32") (d (list (d (n "once_cell") (r "^1.13.0") (o #t) (d #t) (k 0)) (d (n "valuable") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)))) (h "0m5aglin3cdwxpvbg6kz0r9r0k31j48n0kcfwsp6l49z26k3svf0") (f (quote (("std" "once_cell") ("default" "std" "valuable/std")))) (r "1.56.0")))

