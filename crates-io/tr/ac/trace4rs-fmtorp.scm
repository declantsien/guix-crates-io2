(define-module (crates-io tr ac trace4rs-fmtorp) #:use-module (crates-io))

(define-public crate-trace4rs-fmtorp-0.1.0 (c (n "trace4rs-fmtorp") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "1pcj3pzzg7jvrsz5d8yrjb9gdqmz9l0rshbsplfq0zgsrc9mnxz5")))

(define-public crate-trace4rs-fmtorp-0.2.0 (c (n "trace4rs-fmtorp") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "1cq190k3p1m6ls3hnskyd0vrzscjjsfrn1vzgwq1rvgg88rr95v8")))

(define-public crate-trace4rs-fmtorp-0.2.1 (c (n "trace4rs-fmtorp") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "0c70xq30xdgvm4493fxc4x8lnlqy48ni68r5jdzphjzg40av0szy")))

(define-public crate-trace4rs-fmtorp-0.3.0 (c (n "trace4rs-fmtorp") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "17f0ifdjah0kgikppmziqc9m3fzmykff30g4s0bpk77kqc0mc1np")))

(define-public crate-trace4rs-fmtorp-0.3.1 (c (n "trace4rs-fmtorp") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "1j20w32r9p2w3vzrm10w848zxcavsk530ywzk6wglyzjw8mgk31j")))

(define-public crate-trace4rs-fmtorp-0.4.1 (c (n "trace4rs-fmtorp") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "07ysj1s2jk8cbg89sjih59vqq5q1pxql00f0xkwkqcmdxi0q15i9")))

(define-public crate-trace4rs-fmtorp-0.4.2 (c (n "trace4rs-fmtorp") (v "0.4.2") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "0yvkmd5cwlhw1aiyw58z3q62klmvwmp1509frb70jk2021n04csk")))

(define-public crate-trace4rs-fmtorp-0.4.3 (c (n "trace4rs-fmtorp") (v "0.4.3") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "1y53ba6rf3j3719y6qw0dwn8z5ph4idg16249rck38n59in6mh9i")))

(define-public crate-trace4rs-fmtorp-0.4.4 (c (n "trace4rs-fmtorp") (v "0.4.4") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "06vb1ha1557g5l0zky97xh7fk148aml8480jpxflpapmdl46swym")))

(define-public crate-trace4rs-fmtorp-0.4.6 (c (n "trace4rs-fmtorp") (v "0.4.6") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "1yfyvj1j3k8z34kzbjs95yxsdkvqir1qbdpq0ahww0f5pxv0vdhm")))

(define-public crate-trace4rs-fmtorp-0.5.0 (c (n "trace4rs-fmtorp") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "0ccxvylz286q9cwrx11a74p0nv55d0hqfqa4aa73mwhlgrir4fhn")))

(define-public crate-trace4rs-fmtorp-0.5.1 (c (n "trace4rs-fmtorp") (v "0.5.1") (d (list (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "03mzx1w90jv8hmwx1ih8ii56v9gnspwz3fx65fbddx79fiindg6k")))

(define-public crate-trace4rs-fmtorp-0.6.0-rc (c (n "trace4rs-fmtorp") (v "0.6.0-rc") (d (list (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("std" "fmt" "time" "local-time" "registry"))) (d #t) (k 0)))) (h "08ibjl10hagrg3bbsihgkiynmw0c9r7zn6n3wfqbw3s4lf2cixd8")))

