(define-module (crates-io tr ac tracking-params) #:use-module (crates-io))

(define-public crate-tracking-params-0.1.0 (c (n "tracking-params") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "01xgxn3qszxx1adhw9akgal2l0ism8rl468mf2y3h9ml96h82ba2") (y #t)))

(define-public crate-tracking-params-0.1.1 (c (n "tracking-params") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "05rdg8y48vddjj8i7ysbr749ixp4x6llwzi8v7fw0kpka940ri71") (y #t)))

(define-public crate-tracking-params-0.1.2 (c (n "tracking-params") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0kfgrb837n3qfnz9jdb1mk01h35db0136in3r8ybfg1vhblxkw8m")))

(define-public crate-tracking-params-0.1.3 (c (n "tracking-params") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1zp8a85vsv9m365fg8jhnj1bplsrxbm123x2knfi4ksq83nqlzp9")))

(define-public crate-tracking-params-0.1.5 (c (n "tracking-params") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0n8hm23rsh0a02g9c0cwf9fjxns1xxpda5sjl09p173q4gzlzyjn")))

(define-public crate-tracking-params-0.1.6 (c (n "tracking-params") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "18jr0rhbjjn4z778g3094zbp2ny87yppmn1wq6fnf2f5wkqi39l0")))

