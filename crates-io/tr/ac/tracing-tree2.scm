(define-module (crates-io tr ac tracing-tree2) #:use-module (crates-io))

(define-public crate-tracing-tree2-0.3.0 (c (n "tracing-tree2") (v "0.3.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("registry" "fmt" "std"))) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "0bpf80ylmrb837cq1hgmdy66n0pjpa1g707gn794ss6lx994ai9z") (f (quote (("default" "tracing-log"))))))

