(define-module (crates-io tr ac tracers-macros) #:use-module (crates-io))

(define-public crate-tracers-macros-0.1.0 (c (n "tracers-macros") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "tracers-macros-hack") (r "^0.1.0") (d #t) (k 0)))) (h "050gf6bjwihwn3afmq7x75g54mac1nk48v62zpxyz4s10clc2r5s")))

