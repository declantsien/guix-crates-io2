(define-module (crates-io tr ac tracy-rs) #:use-module (crates-io))

(define-public crate-tracy-rs-0.1.0 (c (n "tracy-rs") (v "0.1.0") (d (list (d (n "minidl") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0iqh32y8n73cajy3nrdnj5as0qa6vj14hyhhmpzga6mnd6213ap3") (f (quote (("enable_profiler" "minidl") ("default"))))))

(define-public crate-tracy-rs-0.1.1 (c (n "tracy-rs") (v "0.1.1") (d (list (d (n "minidl") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1s2247p6brgd8mhhf9476kvm8krawdxi7gvwbi0mihnl2nbl7fyv") (f (quote (("enable_profiler" "minidl") ("default"))))))

(define-public crate-tracy-rs-0.1.2 (c (n "tracy-rs") (v "0.1.2") (d (list (d (n "minidl") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1n38r1bfbf2pv24dizc3xlwqdfq9dymsj8r77azkmaxhiap7lq6f") (f (quote (("enable_profiler" "minidl") ("default"))))))

