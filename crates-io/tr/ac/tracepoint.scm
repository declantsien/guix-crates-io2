(define-module (crates-io tr ac tracepoint) #:use-module (crates-io))

(define-public crate-tracepoint-0.4.0 (c (n "tracepoint") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(target_os = \"linux\")") (k 0)))) (h "1l7il1mqb4wm1nyhyixwrvsdl0nban09xa7x0f1dqnlh6fgnylwj") (f (quote (("user_events") ("default" "user_events")))) (r "1.63")))

