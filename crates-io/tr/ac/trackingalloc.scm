(define-module (crates-io tr ac trackingalloc) #:use-module (crates-io))

(define-public crate-trackingalloc-0.1.0 (c (n "trackingalloc") (v "0.1.0") (h "0ljs9y36lrcg10b1s6ykp6kfhxs7i78mnh6b7v8ygg7gxqnm14gl")))

(define-public crate-trackingalloc-0.2.0 (c (n "trackingalloc") (v "0.2.0") (h "1q634668bxwwpg4b8zpl16lsldlrc5in4f518qqc7jw6l1pkb4ya")))

