(define-module (crates-io tr ac tracing-serde) #:use-module (crates-io))

(define-public crate-tracing-serde-0.0.0 (c (n "tracing-serde") (v "0.0.0") (h "01zqlhdvv3fxwgsnfscw8cddbwqq8zh75nlbvav6wbg38mk41mwp")))

(define-public crate-tracing-serde-0.1.0 (c (n "tracing-serde") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.2") (d #t) (k 0)))) (h "1swvkc9fpi05lhcsishqbr8m3q5k1jlpf86vp5ysny73an1flpfn")))

(define-public crate-tracing-serde-0.1.1 (c (n "tracing-serde") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.2") (d #t) (k 0)))) (h "0ybfv0bzx542xkqzhcjx33knbs92zyvxjrf7iwkfvq0niwpvmk5n")))

(define-public crate-tracing-serde-0.1.2 (c (n "tracing-serde") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.2") (d #t) (k 0)))) (h "12xjirg0b3cparjdhkd9pksrmv33gz7rdm4gfkvgk15v3x2flrgv")))

(define-public crate-tracing-serde-0.1.3 (c (n "tracing-serde") (v "0.1.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.22") (d #t) (k 0)) (d (n "valuable-serde") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0)) (d (n "valuable_crate") (r "^0.1.0") (o #t) (t "cfg(tracing_unstable)") (k 0) (p "valuable")))) (h "1qfr0va69djvxqvjrx4vqq7p6myy414lx4w1f6amcn0hfwqj2sxw") (f (quote (("valuable" "valuable_crate" "valuable-serde" "tracing-core/valuable")))) (r "1.42.0")))

