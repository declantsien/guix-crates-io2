(define-module (crates-io tr ac trace-time) #:use-module (crates-io))

(define-public crate-trace-time-0.1.0 (c (n "trace-time") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0hgsp8j12zs1kbysiaxaxs7697ppb8xbdvipwz35gsc2cpd0gsjs")))

(define-public crate-trace-time-0.1.1 (c (n "trace-time") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1j8c68y6q6729ks70vijvmak30l2882ayyvm7qb1x6gi1cpjzs6v")))

(define-public crate-trace-time-0.1.2 (c (n "trace-time") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "05g7aikmz1yljvvdiiqzr68j6ffzyf73yv5yj7ma3fgfhi0g1bf9")))

(define-public crate-trace-time-0.1.3 (c (n "trace-time") (v "0.1.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "106lr44kfs75kgsp19fvgky2pd8advc0vm4x2sai9djpgclpa7ka")))

