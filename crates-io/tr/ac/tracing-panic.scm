(define-module (crates-io tr ac tracing-panic) #:use-module (crates-io))

(define-public crate-tracing-panic-0.1.0 (c (n "tracing-panic") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0qsc5mv8dkn0xj3cnifqxzj8m3qmfvqdgwq76plxliw260zhay80")))

(define-public crate-tracing-panic-0.1.1 (c (n "tracing-panic") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "09dd09yghriwhf0lbka5l8852d6zr9ivwx9djb4r35h4rqq01y7a")))

(define-public crate-tracing-panic-0.1.2 (c (n "tracing-panic") (v "0.1.2") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1iacpnck54clabsyjrbzyvvl1s2lnprkl909jfghjdwq2y52kwbv") (f (quote (("default" "capture-backtrace") ("capture-backtrace"))))))

