(define-module (crates-io tr ac tracker) #:use-module (crates-io))

(define-public crate-tracker-0.1.0 (c (n "tracker") (v "0.1.0") (d (list (d (n "tracker-macros") (r "^0.1") (d #t) (k 0)))) (h "1gz037p0bwqkbf2mjbbpg3g7v3zqm19gzlrvb48d7w05k4z8z29j")))

(define-public crate-tracker-0.1.1 (c (n "tracker") (v "0.1.1") (d (list (d (n "tracker-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1swjafky18ipkhxwppss38zzfgc03i21594lwd9pmbr4gz9qjvg6")))

(define-public crate-tracker-0.1.2 (c (n "tracker") (v "0.1.2") (d (list (d (n "tracker-macros") (r "^0.1.7") (d #t) (k 0)))) (h "0x7i98c3rzpdgzazvcff2pxnw5pmq6nlcv1bp62yp7xfi2gs2n2l")))

(define-public crate-tracker-0.2.0 (c (n "tracker") (v "0.2.0") (d (list (d (n "tracker-macros") (r "^0.2") (d #t) (k 0)))) (h "1zahl5v5wylzyi7hghdwgcf04h3p2xb8k0x8hfsxib3zkibxsj29")))

(define-public crate-tracker-0.2.1 (c (n "tracker") (v "0.2.1") (d (list (d (n "tracker-macros") (r "^0.2.1") (d #t) (k 0)))) (h "1y3bgw08n87ssxs1gfn0ifcj2hmz5vk9rdsmpzv8f09pbv8kd5pz")))

