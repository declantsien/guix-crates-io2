(define-module (crates-io tr ac tracing-journald) #:use-module (crates-io))

(define-public crate-tracing-journald-0.1.0 (c (n "tracing-journald") (v "0.1.0") (d (list (d (n "tracing-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.5") (d #t) (k 0)))) (h "1mm2x3m47r84zr524l2yafwwi86rhwspi7mwl9nwnpvs5gnz1qcg")))

(define-public crate-tracing-journald-0.2.0 (c (n "tracing-journald") (v "0.2.0") (d (list (d (n "tracing-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "01q3xfzk0p72bn5ga5jlbbf3sk2dqzpx85wym88hbl5jnjkg8dbw")))

(define-public crate-tracing-journald-0.2.1 (c (n "tracing-journald") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.107") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "164dp5k9s8lm8bp1i15w3hv2pvymn0vb024pnw8c2sd19ixglq1n") (r "1.42.0")))

(define-public crate-tracing-journald-0.2.2 (c (n "tracing-journald") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.107") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "11p8slsq4lf7f4xc76q2n57ajn8zmbnfsk6iry6ckq7dla3aaqwq") (r "1.42.0")))

(define-public crate-tracing-journald-0.2.3 (c (n "tracing-journald") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.107") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0g4cjqkdrncnqd8ixkc25c3grxdgp1ykqz1xcyak3cnam763p9in") (r "1.49.0")))

(define-public crate-tracing-journald-0.2.4 (c (n "tracing-journald") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.107") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0q7462r9ych1mxn069zs42fkcswkv3xb5iiyjh15gsgl5549z90v") (r "1.49.0")))

(define-public crate-tracing-journald-0.3.0 (c (n "tracing-journald") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.107") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1zgkf2cx6m60qzgwsph83cbzma98b4vs5nshm2b3hg7wx1s6lcds") (r "1.49.0")))

