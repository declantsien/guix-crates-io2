(define-module (crates-io tr ac tract-pulse-opl) #:use-module (crates-io))

(define-public crate-tract-pulse-opl-0.11.0 (c (n "tract-pulse-opl") (v "0.11.0") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.11.0") (d #t) (k 0)))) (h "01jlhsr9skykk3kwbra565fijsr6y8gakz32n8v74v4szh03dbkc")))

(define-public crate-tract-pulse-opl-0.11.1 (c (n "tract-pulse-opl") (v "0.11.1") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.11.1") (d #t) (k 0)))) (h "01dk0lfk8vnjz6ppdq9d4jxam7la43byb9d2dlvlrdvhbx3244rz")))

(define-public crate-tract-pulse-opl-0.11.2 (c (n "tract-pulse-opl") (v "0.11.2") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.11.2") (d #t) (k 0)))) (h "0kchkqc9xg7k70kcy5v4x50zs4dmbk70caw1pc7amk8qfz3vpg49")))

(define-public crate-tract-pulse-opl-0.12.0 (c (n "tract-pulse-opl") (v "0.12.0") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.12.0") (d #t) (k 0)))) (h "1aqnpgkr44789spcnnkzljlfxdq9pv4q9vhwmw43lnxz9wsrww8y") (y #t)))

(define-public crate-tract-pulse-opl-0.12.1 (c (n "tract-pulse-opl") (v "0.12.1") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.12.1") (d #t) (k 0)))) (h "0mkq6qa1g3pqv9g4a96wjljs4gccs4kk7zm1nxkianfkbcrc8xnw")))

(define-public crate-tract-pulse-opl-0.12.2 (c (n "tract-pulse-opl") (v "0.12.2") (d (list (d (n "ctor") (r "=0.1.16") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.12.2") (d #t) (k 0)))) (h "1wj7iyfsqcvaj0n1xv5xdvamqhsdq8za1mrp8gjcw3dnc2kvp6jv")))

(define-public crate-tract-pulse-opl-0.12.3 (c (n "tract-pulse-opl") (v "0.12.3") (d (list (d (n "ctor") (r "=0.1.16") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.12.3") (d #t) (k 0)))) (h "1wb2s264c5gb4si6cs9bsnhaz9bz9f6s62yriwigyybsb4lnzl1b") (y #t)))

(define-public crate-tract-pulse-opl-0.12.4 (c (n "tract-pulse-opl") (v "0.12.4") (d (list (d (n "ctor") (r "=0.1.16") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.12.4") (d #t) (k 0)))) (h "0cyaga07ppvkwcb0bcypilfva1z3fai6x09mmqydy343036qq7z0")))

(define-public crate-tract-pulse-opl-0.12.5 (c (n "tract-pulse-opl") (v "0.12.5") (d (list (d (n "ctor") (r "=0.1.16") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.12.5") (d #t) (k 0)))) (h "046h8r8c97s0lnvqfjxhbrk8dnix8zn9q7d9f2zvslayy22as6g8")))

(define-public crate-tract-pulse-opl-0.13.0 (c (n "tract-pulse-opl") (v "0.13.0") (d (list (d (n "ctor") (r "=0.1.16") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.13.0") (d #t) (k 0)))) (h "16vpwyd7fsmmi7vijf3ah4g77lrvmy0dn58wdjmyz9s1p2dpy1pz")))

(define-public crate-tract-pulse-opl-0.13.1 (c (n "tract-pulse-opl") (v "0.13.1") (d (list (d (n "ctor") (r "=0.1.16") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.13.1") (d #t) (k 0)))) (h "1s0a3mf4wv50jkcl5ppz9nwrr68n3dc1wpvqvmvh7ym8zy40qfh7")))

(define-public crate-tract-pulse-opl-0.13.2 (c (n "tract-pulse-opl") (v "0.13.2") (d (list (d (n "ctor") (r "=0.1.16") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.13.2") (d #t) (k 0)))) (h "0wwdy5d6z8chhqvji3247acgb8hklybvgybvm95m0mnhapnbiay9")))

(define-public crate-tract-pulse-opl-0.14.0 (c (n "tract-pulse-opl") (v "0.14.0") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.14.0") (d #t) (k 0)))) (h "0c9wfn7ds2mdbpxj5drprawgq2pbnyc8zna9kzcrmg2dx9w6km5k")))

(define-public crate-tract-pulse-opl-0.14.1 (c (n "tract-pulse-opl") (v "0.14.1") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.14.1") (d #t) (k 0)))) (h "0bj29dr1xmrl799s0fg6m3p5wsw8m0m62fp1i46avh3k7sz6hrw1")))

(define-public crate-tract-pulse-opl-0.14.2 (c (n "tract-pulse-opl") (v "0.14.2") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.14.2") (d #t) (k 0)))) (h "1hcc7xkjryk75zxiaadvhfb8ld936hwdg2gv4sk5l8npxbrdl1mz")))

(define-public crate-tract-pulse-opl-0.15.0 (c (n "tract-pulse-opl") (v "0.15.0") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.15.0") (d #t) (k 0)))) (h "0kw9rgr6icjfafh6jvnnqxd3i8cv5mllznb5fnx7833ylrvf6440")))

(define-public crate-tract-pulse-opl-0.15.1 (c (n "tract-pulse-opl") (v "0.15.1") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.15.1") (d #t) (k 0)))) (h "0axm61ypsi7ibhi1hgakcqvk3sja90jvcph5kazzn9mc2g6yzgmd")))

(define-public crate-tract-pulse-opl-0.15.2 (c (n "tract-pulse-opl") (v "0.15.2") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.15.2") (d #t) (k 0)))) (h "1hk8f95cbf36kwks4wwlykgsaxn9ffs1pi7xdplafisg50vix0cr")))

(define-public crate-tract-pulse-opl-0.15.3 (c (n "tract-pulse-opl") (v "0.15.3") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.15.3") (d #t) (k 0)))) (h "1l1dnblfbs58024xvc0m64szifal2r4lizzjgypxklsl9y0hnas3")))

(define-public crate-tract-pulse-opl-0.15.4 (c (n "tract-pulse-opl") (v "0.15.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.15.4") (d #t) (k 0)))) (h "19w891s4vj3a0364wc0p7ymry2fz776xkvqdwszwkhp7iq7c3qv2")))

(define-public crate-tract-pulse-opl-0.15.5 (c (n "tract-pulse-opl") (v "0.15.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.15.5") (d #t) (k 0)))) (h "01cz4mq7b351vyhckf8kpr4lqici3nbikdmix00978xw3bhc9n0j")))

(define-public crate-tract-pulse-opl-0.15.7 (c (n "tract-pulse-opl") (v "0.15.7") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.15.7") (d #t) (k 0)))) (h "1gzybzcbjiq6i03cvddh59dvx3h2fik2166xf1zq6dbssi7qmlp1")))

(define-public crate-tract-pulse-opl-0.15.8 (c (n "tract-pulse-opl") (v "0.15.8") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.15.8") (d #t) (k 0)))) (h "1kk26ix9hfwgfscbgjyv9wpldkhkfssb5x81wzm5wyxn0xji14zg")))

(define-public crate-tract-pulse-opl-0.16.0 (c (n "tract-pulse-opl") (v "0.16.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.16.0") (d #t) (k 0)))) (h "1fmq3y38xrqjafvc6pa5hik6xd6mw1gwz4nprrsv2ln8ii3gyjrq")))

(define-public crate-tract-pulse-opl-0.16.1 (c (n "tract-pulse-opl") (v "0.16.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.16.1") (d #t) (k 0)))) (h "1fgvf0gwvl7v53z61idj99ij48i8y1a4fc6id5mn2nys448hzkj8")))

(define-public crate-tract-pulse-opl-0.16.2 (c (n "tract-pulse-opl") (v "0.16.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.16.2") (d #t) (k 0)))) (h "0hs7bmx6nmm5q8ayix9pkwc472vnm7p5clbdhg10ixkhidsvghdp")))

(define-public crate-tract-pulse-opl-0.16.3 (c (n "tract-pulse-opl") (v "0.16.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.16.3") (d #t) (k 0)))) (h "1jbyscxw0hzjpdyhc7dis26cc0k9mnfakhcdw1zxxlli1m6s3j36")))

(define-public crate-tract-pulse-opl-0.16.4 (c (n "tract-pulse-opl") (v "0.16.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.16.4") (d #t) (k 0)))) (h "0s2brij8699pb22w3v166jqasa7d7ja10rv7sxsl2kg0vpw8hzi6")))

(define-public crate-tract-pulse-opl-0.16.5 (c (n "tract-pulse-opl") (v "0.16.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.16.5") (d #t) (k 0)))) (h "0xh4xhss7v06530a8grzyz9lwqlsccc9qvc5iwwj9cmmkylxldvc")))

(define-public crate-tract-pulse-opl-0.16.6 (c (n "tract-pulse-opl") (v "0.16.6") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.16.6") (d #t) (k 0)))) (h "0fpqj702da9lvqafh2fgdbxgh46346yspklsgxcw133fvm49ib74")))

(define-public crate-tract-pulse-opl-0.16.7 (c (n "tract-pulse-opl") (v "0.16.7") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.16.7") (d #t) (k 0)))) (h "0q637fm9kjglfzq267xrldykxpl0z2wzy9aw21lnnrixfwkfayyf")))

(define-public crate-tract-pulse-opl-0.16.8 (c (n "tract-pulse-opl") (v "0.16.8") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.16.8") (d #t) (k 0)))) (h "0l0h7a3v2p8p8p9yi8j3xp05nkpp86y4zl4g1fayxd3818wgrlsi")))

(define-public crate-tract-pulse-opl-0.16.9 (c (n "tract-pulse-opl") (v "0.16.9") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.16.9") (d #t) (k 0)))) (h "1jz8nhb8lqv7r185rc55sydxgk7hkr0ma15z77y2vgdy13a4a05d")))

(define-public crate-tract-pulse-opl-0.17.0 (c (n "tract-pulse-opl") (v "0.17.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.17.0") (d #t) (k 0)))) (h "0m6nvlzi1xvh0kqzn4c4v64q5hr54hq4szsy68dx57l29jmz6dlp")))

(define-public crate-tract-pulse-opl-0.17.1 (c (n "tract-pulse-opl") (v "0.17.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.17.1") (d #t) (k 0)))) (h "0vw5gjskhriiywj77i8hac2q0v909iap7hxwdq4c7s3q3z7qdizv")))

(define-public crate-tract-pulse-opl-0.17.2 (c (n "tract-pulse-opl") (v "0.17.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.17.2") (d #t) (k 0)))) (h "0fwdv33xb470g5pc82knh4ffdmss0sb68zrb47xj9n1b13j887mk")))

(define-public crate-tract-pulse-opl-0.17.3 (c (n "tract-pulse-opl") (v "0.17.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.17.3") (d #t) (k 0)))) (h "0zw3z0b7w5cvzg3mirasgzsm7indp6cgpmmb453sv2mih6awiwpp")))

(define-public crate-tract-pulse-opl-0.17.4 (c (n "tract-pulse-opl") (v "0.17.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.17.4") (d #t) (k 0)))) (h "1ywir3bqk7q8s97hwbbgcky5dphxn9sh0phy6h7yn8vmydyi7rl7")))

(define-public crate-tract-pulse-opl-0.17.7 (c (n "tract-pulse-opl") (v "0.17.7") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.17.7") (d #t) (k 0)))) (h "13q040l1d1qcanrn749in2629qaws3w4ga9r1dv793k89x769nkb")))

(define-public crate-tract-pulse-opl-0.18.0 (c (n "tract-pulse-opl") (v "0.18.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.18.0") (d #t) (k 0)))) (h "1wzaz7cy0wy4zj266xq32j37bmbya9w2jkwjn1s5025hv9wk7423")))

(define-public crate-tract-pulse-opl-0.17.8 (c (n "tract-pulse-opl") (v "0.17.8") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.17.8") (d #t) (k 0)))) (h "0kvg1nia7sqb76dlpl5401w8db170nl0mwqzs0bvmhrdgfqn6hqm")))

(define-public crate-tract-pulse-opl-0.17.9 (c (n "tract-pulse-opl") (v "0.17.9") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.17.9") (d #t) (k 0)))) (h "0jlfqv00d085jb4k1lfwi37ps8bfj2yldx4xys30lkmc7g8pzisz")))

(define-public crate-tract-pulse-opl-0.18.1 (c (n "tract-pulse-opl") (v "0.18.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.18.1") (d #t) (k 0)))) (h "1bf9rvzq76q0012az8kxdb6r9dgmmdqwx065rglsyj0jrvlidj74")))

(define-public crate-tract-pulse-opl-0.18.2 (c (n "tract-pulse-opl") (v "0.18.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.18.2") (d #t) (k 0)))) (h "0ipgypq1d02ij7blhn7wg4kzpbsvz85mhcgaxjkz44m3dwcjvzpj")))

(define-public crate-tract-pulse-opl-0.18.3 (c (n "tract-pulse-opl") (v "0.18.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.18.3") (d #t) (k 0)))) (h "10k4qs8vsq110hsngppggr6g4lfjwqzd56w1lxlb2flqkmh12jki")))

(define-public crate-tract-pulse-opl-0.18.4 (c (n "tract-pulse-opl") (v "0.18.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.18.4") (d #t) (k 0)))) (h "0jf9j6a8wfs4v08rm481w3k22cidd50z8c2sfjj04g3y2zhxrdxf")))

(define-public crate-tract-pulse-opl-0.18.5 (c (n "tract-pulse-opl") (v "0.18.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.18.5") (d #t) (k 0)))) (h "0pm9fw2vlar5yv94mvj0xi73s3qqsimck3rszhp0rrbijm1db69s")))

(define-public crate-tract-pulse-opl-0.19.0-alpha.8 (c (n "tract-pulse-opl") (v "0.19.0-alpha.8") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.0-alpha.8") (d #t) (k 0)))) (h "1dvybic4wbkivpms4m5z9cnf0yli5kad7mr3w0lqm6p0bh5fkapb")))

(define-public crate-tract-pulse-opl-0.19.0-alpha.14 (c (n "tract-pulse-opl") (v "0.19.0-alpha.14") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.19.0-alpha.14") (d #t) (k 0)))) (h "0lygcn2ynwfaxsj4yih0dmx6f63r6jzpxz4nbf2bchh3zm136rr2")))

(define-public crate-tract-pulse-opl-0.19.0-alpha.17 (c (n "tract-pulse-opl") (v "0.19.0-alpha.17") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.0-alpha.17") (d #t) (k 0)))) (h "0y7a8h5nynnbxysxdfj6zrsx7r08fivbl27iaak9483vbjp34n8x")))

(define-public crate-tract-pulse-opl-0.19.0-alpha.18 (c (n "tract-pulse-opl") (v "0.19.0-alpha.18") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.0-alpha.18") (d #t) (k 0)))) (h "1mlza9g56y16bqpy5h4d1v4imkdavp65khdzgjnzccdasdwkscb1")))

(define-public crate-tract-pulse-opl-0.19.0-alpha.19 (c (n "tract-pulse-opl") (v "0.19.0-alpha.19") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.0-alpha.19") (d #t) (k 0)))) (h "0irnxblwsvp6k06lmvra0a08hsywl5dnjdzyp9ihz4s9czi9p086")))

(define-public crate-tract-pulse-opl-0.19.0 (c (n "tract-pulse-opl") (v "0.19.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.0") (d #t) (k 0)))) (h "1m6s34cy903zslig18xq7yiqfdf331rvjv306m06y3yc0bilwm6c")))

(define-public crate-tract-pulse-opl-0.19.1 (c (n "tract-pulse-opl") (v "0.19.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.1") (d #t) (k 0)))) (h "0pbsxn6dpcgrr58wk53j2vc8gi1d0bjr3ny7jj6fy6z4zqdj19vy")))

(define-public crate-tract-pulse-opl-0.19.2 (c (n "tract-pulse-opl") (v "0.19.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.2") (d #t) (k 0)))) (h "1mvm98hcmgxjpgq2c20yx7c6nddmiza1v2gnxp11im7pg3dpxkd2")))

(define-public crate-tract-pulse-opl-0.19.3 (c (n "tract-pulse-opl") (v "0.19.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.3") (d #t) (k 0)))) (h "0khl5nngffz94r369lrgfpryixi53kcp9c3w6yqaslhaldpac2jw") (y #t)))

(define-public crate-tract-pulse-opl-0.19.5 (c (n "tract-pulse-opl") (v "0.19.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.5") (d #t) (k 0)))) (h "14ghyjg581kj8k54iv2l9jlnbi64adyf8gi78fjr2sqc7bmhrblz")))

(define-public crate-tract-pulse-opl-0.19.6 (c (n "tract-pulse-opl") (v "0.19.6") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.6") (d #t) (k 0)))) (h "1zbv1wi15wjpmipi49ypl1namwfrn8am5zn08rg593c0f1ygsdpg") (y #t)))

(define-public crate-tract-pulse-opl-0.19.7 (c (n "tract-pulse-opl") (v "0.19.7") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.7") (d #t) (k 0)))) (h "0w8z6iyzsr75vcf1zzf866wq7hydbg6dvqzyy7ippsfnkhzamc1a")))

(define-public crate-tract-pulse-opl-0.19.8 (c (n "tract-pulse-opl") (v "0.19.8") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.8") (d #t) (k 0)))) (h "06bvc84sp2kk96yp1n29hzbld19z9mr6y3wd9sjjxn19vixjjvlh")))

(define-public crate-tract-pulse-opl-0.19.10 (c (n "tract-pulse-opl") (v "0.19.10") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.10") (d #t) (k 0)))) (h "0ps90j3ch7fc3884s9iz64f32650kkrhvvx3vwczrnkrc3wsxmv5")))

(define-public crate-tract-pulse-opl-0.19.11 (c (n "tract-pulse-opl") (v "0.19.11") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.11") (d #t) (k 0)))) (h "01jiw039zxscrwk4qr1yi7iiwhipwqy7f979v2bv07b1gd17124p")))

(define-public crate-tract-pulse-opl-0.19.12 (c (n "tract-pulse-opl") (v "0.19.12") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.12") (d #t) (k 0)))) (h "1ap41mcxav6i5jr7hf758arwcw8qjm765ybfpm7yjr673gl9il3q")))

(define-public crate-tract-pulse-opl-0.19.13 (c (n "tract-pulse-opl") (v "0.19.13") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.13") (d #t) (k 0)))) (h "1hiygrgnz0dl7rj7gic26vg31ac3fxp8q0pgczpjj2w7ahwsmmng")))

(define-public crate-tract-pulse-opl-0.19.14 (c (n "tract-pulse-opl") (v "0.19.14") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.14") (d #t) (k 0)))) (h "01iq6cyniqknhdy7ixj61g6lj486iqhnrswdmawb3b2hh2j6p1r5")))

(define-public crate-tract-pulse-opl-0.19.15 (c (n "tract-pulse-opl") (v "0.19.15") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.15") (d #t) (k 0)))) (h "13jxgmxal1iny94c9vlkq36lkny0i0p72gb2bwfz424xkb7ph80p")))

(define-public crate-tract-pulse-opl-0.20.0 (c (n "tract-pulse-opl") (v "0.20.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.0") (d #t) (k 0)))) (h "1q191vj9vkb0zcw9988dgc43pxg7zli4wchn92kd2plihhfixlh0") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.2 (c (n "tract-pulse-opl") (v "0.20.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.2") (d #t) (k 0)))) (h "0ksdpmm1c6134kbryvkmdva0cgqrfz5h5bq6dfygmx0hdqrvqwci") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.3 (c (n "tract-pulse-opl") (v "0.20.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.3") (d #t) (k 0)))) (h "0nazwd4hpzg97pf1b11vmmq2m67cs23b5j86xm47l5x762zsbp9d") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.4 (c (n "tract-pulse-opl") (v "0.20.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.4") (d #t) (k 0)))) (h "1w5ni8pdyp0c2ggs04dp6jnnkaxfxmra5l2xp9syld98nf0wkf9g") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.5 (c (n "tract-pulse-opl") (v "0.20.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.5") (d #t) (k 0)))) (h "1xqb8rn8984wkqnkr7whsk84wj66akbi9d2p4wbm4x374wviqm2l") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.6 (c (n "tract-pulse-opl") (v "0.20.6") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.6") (d #t) (k 0)))) (h "0yhpjvimxflcjbba07fj0cf2ny52wydfl7f8cmn0qx9f1alqcal3") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.19.16 (c (n "tract-pulse-opl") (v "0.19.16") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.19.16") (d #t) (k 0)))) (h "12gd0sd93cnhj62n5jc79sjfqakprvagibja8nvbilhdshwgiz55")))

(define-public crate-tract-pulse-opl-0.20.7 (c (n "tract-pulse-opl") (v "0.20.7") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.7") (d #t) (k 0)))) (h "0qfajhcdkk7af1cshpj6hp87wkh04963w6y1jd4n614l1fids6a5") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.12 (c (n "tract-pulse-opl") (v "0.20.12") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.12") (d #t) (k 0)))) (h "1f8iiffaaygf9qkriwv4bajwax95i9np64afwdnrdn1qrx6fqrxg") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.13 (c (n "tract-pulse-opl") (v "0.20.13") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.13") (d #t) (k 0)))) (h "0jjcdfzl7ywdshc79zkgfkb15qsfk7wqh5chr6sabgpayalj5rbc") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.14 (c (n "tract-pulse-opl") (v "0.20.14") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.14") (d #t) (k 0)))) (h "0akyma6ps1f5d31d7glnhdhqwwy844lrbb4fn745jbay55y8kyiw") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.15 (c (n "tract-pulse-opl") (v "0.20.15") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.15") (d #t) (k 0)))) (h "1m5nxlp245fsy5hxplg7yhgh6434swz8x59p3wf28j6iwm6vy1rb") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.16 (c (n "tract-pulse-opl") (v "0.20.16") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.16") (d #t) (k 0)))) (h "1rasyi2k0vs00nkkfl0wizij65gssavva3wdsfx4hz36b7k4b3db") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.17 (c (n "tract-pulse-opl") (v "0.20.17") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.17") (d #t) (k 0)))) (h "0lm4zhkjv95ifjn2vfxfhjvlqp3ixihh720ra6k17hws3yg7xc30") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.18 (c (n "tract-pulse-opl") (v "0.20.18") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.18") (d #t) (k 0)))) (h "1hijvjwm3qwi9cc4ff26qp36sjkdjr51ss69sjhm5bbb8yzcg37s") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.19 (c (n "tract-pulse-opl") (v "0.20.19") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.19") (d #t) (k 0)))) (h "10aidiki6kjsrckjhch2l91kzn1vh06rs21fn0ib6jw7dg5q1k63") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.20 (c (n "tract-pulse-opl") (v "0.20.20") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.20") (d #t) (k 0)))) (h "185scw1j667aa9z1amrxjmn8x2bk9nq4xjf7hw9zzl990p405i8l") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.21 (c (n "tract-pulse-opl") (v "0.20.21") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.21") (d #t) (k 0)))) (h "187arw17bi0r8pycsdbnz8xx8hd84fld2fy2i9pswnc77bhz6dl8") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.20.22 (c (n "tract-pulse-opl") (v "0.20.22") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.20.22") (d #t) (k 0)))) (h "0cxykmnp58jws6x38w94w7yvw6s3lsjqk8v46qwznk8nzc141ir5") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.21.0 (c (n "tract-pulse-opl") (v "0.21.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.21.0") (d #t) (k 0)))) (h "1lzl7ny84a1qd2wk0mkdwrdf9ah2cavvx7wci7grabg6n7s07rz9") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.21.1 (c (n "tract-pulse-opl") (v "0.21.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.21.1") (d #t) (k 0)))) (h "1dl2nzjp728r17lrmyqgfjbr1x1aagvid1xm36qn2xwaivgvqfnq") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.21.2 (c (n "tract-pulse-opl") (v "0.21.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.21.2") (d #t) (k 0)))) (h "1xq1f70mdjpy4f542sdvpvc1grsl1v6j5s0z639ikym6rxzi26ic") (f (quote (("complex" "tract-nnef/complex")))) (y #t)))

(define-public crate-tract-pulse-opl-0.21.3 (c (n "tract-pulse-opl") (v "0.21.3") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.21.3") (d #t) (k 0)))) (h "0aqdymijr7589wkgf8sip30w4zidv5v39wj9ylnlhx4flhsj805p") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.21.4 (c (n "tract-pulse-opl") (v "0.21.4") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.21.4") (d #t) (k 0)))) (h "1g489jwxip1fz7848rhrp7qkc1jjy5kzzhrm4h731qvapgmjmi5c") (f (quote (("complex" "tract-nnef/complex"))))))

(define-public crate-tract-pulse-opl-0.21.5 (c (n "tract-pulse-opl") (v "0.21.5") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "=0.21.5") (d #t) (k 0)))) (h "0z5ia12m2ha2dlg7rrzak9pjizz0g3qg7v02dwip1mvc6b7a94ix") (f (quote (("complex" "tract-nnef/complex"))))))

