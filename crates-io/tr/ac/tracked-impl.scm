(define-module (crates-io tr ac tracked-impl) #:use-module (crates-io))

(define-public crate-tracked-impl-0.1.0 (c (n "tracked-impl") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0kw6ilrs2kawqsdl7b5zz14j9hf5risdyg2399hr7m9fb4v6slm2") (r "1.56")))

(define-public crate-tracked-impl-0.1.1 (c (n "tracked-impl") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("github"))) (o #t) (d #t) (k 0)))) (h "0dx50d26d90sjnfysxz1myb7caf4czfvx7p1hhj9y8s1jmc301wm") (f (quote (("test" "todo-or-die")))) (r "1.56")))

(define-public crate-tracked-impl-0.2.0 (c (n "tracked-impl") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("github"))) (d #t) (k 2)))) (h "13ry7r8cqf8hvkhxgb1w3y19qhzcvhwiwdnas8a7fvwkcjj7239g") (r "1.56")))

(define-public crate-tracked-impl-0.2.1 (c (n "tracked-impl") (v "0.2.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("github"))) (d #t) (k 2)))) (h "05ig7i8g2wgkbarrjhzm42j8s470r96m0p8d9h5246zybwhvwd9p") (r "1.56")))

(define-public crate-tracked-impl-0.3.0 (c (n "tracked-impl") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("github"))) (d #t) (k 2)))) (h "1dncy6inpb4hzwm0xjddmli4fn0gfwkqc5cqbqvsc5qnsm9nl066") (r "1.56")))

(define-public crate-tracked-impl-0.4.0 (c (n "tracked-impl") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("github"))) (d #t) (k 2)))) (h "1m1rk4ncrhw0867xryc3xm7v28hwhv49ihc9qzfvmck7yhd4pay2") (r "1.56")))

(define-public crate-tracked-impl-0.5.0 (c (n "tracked-impl") (v "0.5.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("github"))) (d #t) (k 2)))) (h "0jfq9vca6qrxk3v09wl4vl3xck92yahl9n4qgc46jxlggxwa39bq") (r "1.56")))

(define-public crate-tracked-impl-0.5.1 (c (n "tracked-impl") (v "0.5.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("github"))) (d #t) (k 2)))) (h "08jhnd68b9376zj20x5m0k372n0ms2h57ggk32hc05x96gyslg9q") (r "1.56")))

(define-public crate-tracked-impl-0.5.2 (c (n "tracked-impl") (v "0.5.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("github"))) (d #t) (k 2)))) (h "1ll86irjhjynh2bhdf57rilpfa2bnxyidzbfhwb2zrv9f98m7q1h") (r "1.56")))

(define-public crate-tracked-impl-0.5.3 (c (n "tracked-impl") (v "0.5.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("github"))) (d #t) (k 2)))) (h "0zvxdaz0fh690vm8yi5915v080mfpfr0glc1fy2vdw9cd03nc43w") (r "1.56")))

(define-public crate-tracked-impl-0.5.4 (c (n "tracked-impl") (v "0.5.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("github"))) (d #t) (k 2)))) (h "00x0xsms6nyx2h8zp2zxqshdcglsd6rrrj8ncrnqzx9jh8chgy4d") (r "1.56")))

