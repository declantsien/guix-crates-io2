(define-module (crates-io tr ac trackr_derive) #:use-module (crates-io))

(define-public crate-trackr_derive-0.1.0 (c (n "trackr_derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1mmqnlxqxdcaxifrkl9waa2752bkrwpn8pnkz8a1mqc7kqi123vj")))

(define-public crate-trackr_derive-0.1.1 (c (n "trackr_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1lqg03y83gsq5aspvpr2xin5927cb057xl43170v4qf8fap1bmf2")))

