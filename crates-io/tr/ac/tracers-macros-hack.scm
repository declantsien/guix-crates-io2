(define-module (crates-io tr ac tracers-macros-hack) #:use-module (crates-io))

(define-public crate-tracers-macros-hack-0.1.0 (c (n "tracers-macros-hack") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracers-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "tracers-core") (r "^0.1.0") (d #t) (k 0)))) (h "0vj85ykgwv57gpkdpixg422pms3pm8adjcbdh9yv8y95pyhl4wxk")))

