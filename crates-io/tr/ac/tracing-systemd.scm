(define-module (crates-io tr ac tracing-systemd) #:use-module (crates-io))

(define-public crate-tracing-systemd-0.1.0 (c (n "tracing-systemd") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "sd-journal") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1zvdmrvvlzk2nzvhch9d29y5ik33hqjg7ff74av6giaf32b7hdwc") (f (quote (("default" "colored"))))))

