(define-module (crates-io tr ac tracing_facade) #:use-module (crates-io))

(define-public crate-tracing_facade-0.1.0 (c (n "tracing_facade") (v "0.1.0") (d (list (d (n "scopeguard") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kq05b7397aa9jg7dl8j79aryx6s6rf47jqshykzdf1rhsif8h2j")))

