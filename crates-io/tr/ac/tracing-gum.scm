(define-module (crates-io tr ac tracing-gum) #:use-module (crates-io))

(define-public crate-tracing-gum-0.0.0 (c (n "tracing-gum") (v "0.0.0") (h "1qdjs3yk4dd6nmkcbm6a6894x3sflxg193hgymq2gmfxfdvgffh2") (y #t)))

(define-public crate-tracing-gum-0.1.0-dev.6 (c (n "tracing-gum") (v "0.1.0-dev.6") (d (list (d (n "gum-proc-macro") (r "=0.1.0-dev.6") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "jaeger") (r "=0.1.0-dev.6") (d #t) (k 0) (p "polkadot-node-jaeger")) (d (n "polkadot-primitives") (r "=0.1.0-dev.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "1bz8vv6ixrn3qgm073zazicalvwq6cxf6s66xvx5q5wp531dgqpw")))

(define-public crate-tracing-gum-1.0.0 (c (n "tracing-gum") (v "1.0.0") (d (list (d (n "gum-proc-macro") (r "^1.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "jaeger") (r "^1.0.0") (d #t) (k 0) (p "polkadot-node-jaeger")) (d (n "polkadot-primitives") (r "^1.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "04fvnq8kjld7fgd2jc2gnxw32g13b20q2mhwi6vhc5khavjhh619")))

(define-public crate-tracing-gum-2.0.0 (c (n "tracing-gum") (v "2.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^2.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "jaeger") (r "^2.0.0") (d #t) (k 0) (p "polkadot-node-jaeger")) (d (n "polkadot-primitives") (r "^2.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "1jmm7qlzwxyixa59qx49i7hkklm6if61xh3kwfsjk5yvh9169l6k")))

(define-public crate-tracing-gum-3.0.0-dev.1 (c (n "tracing-gum") (v "3.0.0-dev.1") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "=3.0.0-dev.1") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "jaeger") (r "=3.0.0-dev.1") (d #t) (k 0) (p "polkadot-node-jaeger")) (d (n "polkadot-primitives") (r "=3.0.0-dev.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "1n4j745qcp0fmd6vk1085k646qam0whh1s7x8pf4ka37qg7rp32v")))

(define-public crate-tracing-gum-3.0.0 (c (n "tracing-gum") (v "3.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^3.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "jaeger") (r "^3.0.0") (d #t) (k 0) (p "polkadot-node-jaeger")) (d (n "polkadot-primitives") (r "^3.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "19rp55ng301w58lq0dh8hi0qffls5wjkff89yhilz8r37mg5139q")))

(define-public crate-tracing-gum-4.0.0 (c (n "tracing-gum") (v "4.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^4.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "jaeger") (r "^4.0.0") (d #t) (k 0) (p "polkadot-node-jaeger")) (d (n "polkadot-primitives") (r "^4.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "1dkp20hjlyazgyiqnr0x290pbvragigz82zcinzsvn9msrdmbh1j")))

(define-public crate-tracing-gum-5.0.0 (c (n "tracing-gum") (v "5.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^4.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "polkadot-primitives") (r "^5.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "0bi3i4hxqz4r58mc4ibr9mjr58zxaz4l7vqv8jpsmmb0nrcwd568")))

(define-public crate-tracing-gum-6.0.0 (c (n "tracing-gum") (v "6.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^4.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "polkadot-primitives") (r "^6.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "0xnqmdzsvhlm925wlqgx8mkd163vxl8v94gqlyvwa94ivmk5vzhi")))

(define-public crate-tracing-gum-7.0.0 (c (n "tracing-gum") (v "7.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^5.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "polkadot-primitives") (r "^7.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "1c4hqm4nbdx7mp7ikk52zgqjgbdwfg590yzmvn4jk1qfvafls4sz")))

(define-public crate-tracing-gum-8.0.0 (c (n "tracing-gum") (v "8.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^5.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "polkadot-primitives") (r "^8.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "11p2hz4821cfbj37f4j1lihinqrg341ahp0vza35f4hxw5zsz44n")))

(define-public crate-tracing-gum-9.0.0 (c (n "tracing-gum") (v "9.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^5.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "polkadot-primitives") (r "^9.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "08i46s6qka94wrmv5byjbi9l82la353cnnvagyj73i333cr410df")))

(define-public crate-tracing-gum-10.0.0 (c (n "tracing-gum") (v "10.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^5.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "polkadot-primitives") (r "^10.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "034najxyh9g0arfxzy3h4x5nvi7zjd0lfmisr72azwq3w63fc7s6")))

(define-public crate-tracing-gum-11.0.0 (c (n "tracing-gum") (v "11.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^5.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "polkadot-primitives") (r "^11.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "1vfj3p7lnwlpgwgskgxc4p9qnpnnz9p4vf5f0k8xh4zl2adpw1a8")))

(define-public crate-tracing-gum-12.0.0 (c (n "tracing-gum") (v "12.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^5.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "polkadot-primitives") (r "^12.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "1i60hwl8wam79rm5x0bkljl41700m215hmcyr5qgc0d87n6f6dyh")))

(define-public crate-tracing-gum-13.0.0 (c (n "tracing-gum") (v "13.0.0") (d (list (d (n "coarsetime") (r "^0.1.22") (d #t) (k 0)) (d (n "gum-proc-macro") (r "^5.0.0") (d #t) (k 0) (p "tracing-gum-proc-macro")) (d (n "polkadot-primitives") (r "^13.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "1rr0pflc0vl8mmnp94rj4sg4x818xl0hbxpna8qq7l1ls9ci32si")))

