(define-module (crates-io tr ac tracelogging_macros) #:use-module (crates-io))

(define-public crate-tracelogging_macros-0.1.0 (c (n "tracelogging_macros") (v "0.1.0") (h "1p3hqb0hd8ff4h7w8gfkrxhnvslzyqdsil6c559pwz60cy971cvh") (r "1.63")))

(define-public crate-tracelogging_macros-1.0.1 (c (n "tracelogging_macros") (v "1.0.1") (h "1vyz5qq5hzjc99db1rm9zfj1lg561n5qxh5r9l5wjq6lv23g7mn5") (r "1.63")))

(define-public crate-tracelogging_macros-1.0.2 (c (n "tracelogging_macros") (v "1.0.2") (h "0asjz63kpdqmig7ri3q07rvpfq1gdpxrd1zm677zyia3hy5xw4qg") (r "1.63")))

(define-public crate-tracelogging_macros-1.1.0 (c (n "tracelogging_macros") (v "1.1.0") (h "07clr4vrih0x89n3s6f3d58avhbxzws5fyz2raprynnrfb1aanls") (r "1.63")))

(define-public crate-tracelogging_macros-1.2.0 (c (n "tracelogging_macros") (v "1.2.0") (h "0pk84nv3br69kvh97r7ik1bn55pkqhcmlzy12ymvknb73n0950xn") (r "1.63")))

