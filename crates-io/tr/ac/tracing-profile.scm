(define-module (crates-io tr ac tracing-profile) #:use-module (crates-io))

(define-public crate-tracing-profile-0.1.0 (c (n "tracing-profile") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1ja6k7l66cq65bz3xj02wfwygm1s5mixkj17g298jgmwpk0lnx3f") (f (quote (("panic"))))))

(define-public crate-tracing-profile-0.2.0 (c (n "tracing-profile") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0difg4rrbhffzdp0vfpfccfigamma28pd7l8la72sw2aca0a0vcm") (f (quote (("panic"))))))

(define-public crate-tracing-profile-0.3.0 (c (n "tracing-profile") (v "0.3.0") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1c8yyrp6wk5mwxbjqd4ls5ga6dbpavnjjn1pvqszby6balhypvcc") (f (quote (("panic"))))))

