(define-module (crates-io tr ac track8-program) #:use-module (crates-io))

(define-public crate-track8-program-1.0.0 (c (n "track8-program") (v "1.0.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.9.0") (d #t) (k 0)))) (h "1arf3846ybg2bl9r4pb0lc2r99qpd0hy1arg94famjl532dmil0f")))

