(define-module (crates-io tr ac tracing-logfmt) #:use-module (crates-io))

(define-public crate-tracing-logfmt-0.1.0 (c (n "tracing-logfmt") (v "0.1.0") (d (list (d (n "time") (r "^0.3.7") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1hi5n43qmqhiy0yfqpfrhrg6y6zwkmph1gcgj739r5zmsqbqz71i")))

(define-public crate-tracing-logfmt-0.1.1 (c (n "tracing-logfmt") (v "0.1.1") (d (list (d (n "time") (r "^0.3.7") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "18rrjwnv54jmmrq4xjdlqb1wvqx8pf5hmc68kl3pzmshhwmy7ich")))

(define-public crate-tracing-logfmt-0.1.2 (c (n "tracing-logfmt") (v "0.1.2") (d (list (d (n "time") (r "^0.3.7") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1bl9fd6vk9h4rzp89livf0nwwgsh5hzfbbpa9wky8g47gj9pjhm8")))

(define-public crate-tracing-logfmt-0.2.0 (c (n "tracing-logfmt") (v "0.2.0") (d (list (d (n "time") (r "^0.3.7") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1jx8p0i2358xq5m5x4r8acwgwqmvl4sd9gzrncrs96s16v0sr2n0")))

(define-public crate-tracing-logfmt-0.3.0 (c (n "tracing-logfmt") (v "0.3.0") (d (list (d (n "time") (r "^0.3.7") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1x1z29criibxg2fc3j4ll23zd5974ng7c8dvhnn57w1i4zgi6z5m")))

(define-public crate-tracing-logfmt-0.3.1 (c (n "tracing-logfmt") (v "0.3.1") (d (list (d (n "time") (r "^0.3.7") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "006acc7g92alw7rk0z582q1ya535g33sz7yjc0c20kklp5zp25sm")))

(define-public crate-tracing-logfmt-0.3.2 (c (n "tracing-logfmt") (v "0.3.2") (d (list (d (n "time") (r "^0.3.7") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0yx6fmw4hcnn4ym0ah9q3wmi164wsm4wpglflwq7nwnp7rwb4byf")))

(define-public crate-tracing-logfmt-0.3.3 (c (n "tracing-logfmt") (v "0.3.3") (d (list (d (n "nu-ansi-term") (r "^0.49") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt"))) (k 0)))) (h "0rfhqx94i6f6ppxjhs4pc3di05dw3pp2686937zy9r5c80pb9fl4") (s 2) (e (quote (("ansi_logs" "dep:nu-ansi-term"))))))

(define-public crate-tracing-logfmt-0.3.4 (c (n "tracing-logfmt") (v "0.3.4") (d (list (d (n "nu-ansi-term") (r "^0.49") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt"))) (k 0)))) (h "1rwv27czwf5jhkyvz6b2ad65mp5qhszk1i9f20m239fayray9f12") (s 2) (e (quote (("ansi_logs" "dep:nu-ansi-term"))))))

