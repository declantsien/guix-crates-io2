(define-module (crates-io tr ac tracing-actix-web2) #:use-module (crates-io))

(define-public crate-tracing-actix-web2-1.0.0 (c (n "tracing-actix-web2") (v "1.0.0") (d (list (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "02zrb7z2cdnpxqwiqi9vqmk0kqy05mfkwjk8pila90l0aag0w9fx")))

(define-public crate-tracing-actix-web2-2.0.0 (c (n "tracing-actix-web2") (v "2.0.0") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1xy5vjsv5vhlw0rwzjc8yd6ih8y70w3bvr5llawvihd1nbck0l66")))

