(define-module (crates-io tr ac tracing-for-pyo3-logging) #:use-module (crates-io))

(define-public crate-tracing-for-pyo3-logging-0.0.0 (c (n "tracing-for-pyo3-logging") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "pyo3") (r "^0.16") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0cnvq2js6xgcsavfhpmjpjkqg8l8h7bdmih9mq93s0c7l46j73cr") (f (quote (("log" "tracing/log") ("default"))))))

(define-public crate-tracing-for-pyo3-logging-0.0.1 (c (n "tracing-for-pyo3-logging") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "pyo3") (r "^0.17") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0q78rgm89wvdpsihw5s4xa8icfyyqhpmlkbf2y4bfwpn9wm6za68") (f (quote (("log" "tracing/log") ("default")))) (r "1.58.0")))

(define-public crate-tracing-for-pyo3-logging-0.0.2 (c (n "tracing-for-pyo3-logging") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "pyo3") (r "^0.18") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "18i63l2gxv1n94blcqbalk214jgkw4mbwndas0gy9f68hlfpqgy5") (f (quote (("log" "tracing/log") ("default")))) (r "1.58.0")))

(define-public crate-tracing-for-pyo3-logging-0.0.3 (c (n "tracing-for-pyo3-logging") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "pyo3") (r "^0.19") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1b376dphpl6q55vg86hqa209272sc6nzr7x9fqm84x0w55ddm5cj") (f (quote (("log" "tracing/log") ("default")))) (r "1.58.0")))

(define-public crate-tracing-for-pyo3-logging-0.0.4 (c (n "tracing-for-pyo3-logging") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "pyo3") (r "^0.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1m6sp71mhw3y83qdibgxqslndgn707czslnf1x5lg9345gxlf81i") (f (quote (("log" "tracing/log") ("default")))) (r "1.58.0")))

