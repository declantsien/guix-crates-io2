(define-module (crates-io tr ac tracing-allocations) #:use-module (crates-io))

(define-public crate-tracing-allocations-0.1.0 (c (n "tracing-allocations") (v "0.1.0") (d (list (d (n "tokio") (r "^1.15") (f (quote ("full" "tracing"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.31") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (f (quote ("fmt"))) (d #t) (k 2)))) (h "0kk4l8v80kbrpswqz03f1j46qxhwg2ijn3wg7j8f3i5314rnld8a")))

