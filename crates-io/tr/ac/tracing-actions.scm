(define-module (crates-io tr ac tracing-actions) #:use-module (crates-io))

(define-public crate-tracing-actions-0.1.0 (c (n "tracing-actions") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thread_local") (r "^1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)))) (h "0vdqa7d83l96q4fyijz0c2p3qrzx0xxbc1gnkpcg8rikijn5k6r8")))

(define-public crate-tracing-actions-0.1.2 (c (n "tracing-actions") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thread_local") (r "^1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)))) (h "0qjk1b699azr0nf1n1fy115m5mgc7jpb9n3cb0689dvw5c5kv5nf")))

(define-public crate-tracing-actions-0.2.0 (c (n "tracing-actions") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thread_local") (r "^1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)))) (h "1i27y0frhj8rwam4vyq5kwvmj8614hmgp6krya9hl0wjyd4bfhbk")))

(define-public crate-tracing-actions-0.3.0 (c (n "tracing-actions") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thread_local") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)))) (h "18md0byji2zlfgwvn9v5v8xjw37wakm04nhr7afh2gxg5gnk7hsv")))

(define-public crate-tracing-actions-0.3.1 (c (n "tracing-actions") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thread_local") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1") (d #t) (k 0)))) (h "1pawmlzd7agmp9grpm4zgppq0w8fifa708fc5q775mwgmm0c90z0")))

