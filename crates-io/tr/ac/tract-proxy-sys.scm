(define-module (crates-io tr ac tract-proxy-sys) #:use-module (crates-io))

(define-public crate-tract-proxy-sys-0.20.18 (c (n "tract-proxy-sys") (v "0.20.18") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "tract-ffi") (r "=0.20.18") (d #t) (k 0)))) (h "0za6wh7rpghsg3q8n3101xsyhckd845f2rs28pk4b2x86l190n63") (r "1.65")))

(define-public crate-tract-proxy-sys-0.20.19 (c (n "tract-proxy-sys") (v "0.20.19") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "tract-ffi") (r "=0.20.19") (d #t) (k 0)))) (h "1aaam2pjysnk8yw7v9xm93qcmfgdzp8630kl6pw3m3kmfhakd3p2") (r "1.65")))

(define-public crate-tract-proxy-sys-0.20.20 (c (n "tract-proxy-sys") (v "0.20.20") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "tract-ffi") (r "=0.20.20") (d #t) (k 0)))) (h "09xlm3ri6c556l2pbq9xq5ir01g6553yvd2dj0zlzmk077gmrw5n") (r "1.65")))

(define-public crate-tract-proxy-sys-0.20.21 (c (n "tract-proxy-sys") (v "0.20.21") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "tract-ffi") (r "=0.20.21") (d #t) (k 0)))) (h "19zyxjliazq4s8xlpb9dzfwhsbrx651a688p5bh3kzwjb3sgbb62") (r "1.65")))

(define-public crate-tract-proxy-sys-0.20.22 (c (n "tract-proxy-sys") (v "0.20.22") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "tract-ffi") (r "=0.20.22") (d #t) (k 0)))) (h "12bkjwpa1jxs0hx4f92ymcrrdar8gka8n2fk2lnqv21yxi45kivv") (r "1.65")))

(define-public crate-tract-proxy-sys-0.21.0 (c (n "tract-proxy-sys") (v "0.21.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (d #t) (k 1)) (d (n "tract-ffi") (r "=0.21.0") (d #t) (k 0)))) (h "1kky0wlhl03ddafdl5npdhsphz3hl6mp37xpwdys7hia667d87ql") (r "1.75")))

(define-public crate-tract-proxy-sys-0.21.1 (c (n "tract-proxy-sys") (v "0.21.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (d #t) (k 1)) (d (n "tract-ffi") (r "=0.21.1") (d #t) (k 0)))) (h "16a0xh3q07fymz4vrcviqfvz4vrcr9lsq5i8ry0rp4vng5pjv6hj") (r "1.75")))

(define-public crate-tract-proxy-sys-0.21.2 (c (n "tract-proxy-sys") (v "0.21.2") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (d #t) (k 1)) (d (n "tract-ffi") (r "=0.21.2") (d #t) (k 0)))) (h "00cpk0apyfnrx96ni61cwkprd1zk3yc44fyssbh0r9ja5ifi680w") (y #t) (r "1.75")))

(define-public crate-tract-proxy-sys-0.21.3 (c (n "tract-proxy-sys") (v "0.21.3") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (d #t) (k 1)) (d (n "tract-ffi") (r "=0.21.3") (d #t) (k 0)))) (h "1cx89mcx301w2q9irsdss5l71g0y11ncsq5xxg6pyrf4fxm8gm19") (r "1.75")))

(define-public crate-tract-proxy-sys-0.21.4 (c (n "tract-proxy-sys") (v "0.21.4") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (d #t) (k 1)) (d (n "tract-ffi") (r "=0.21.4") (d #t) (k 0)))) (h "0p57phndgvnnj88gcx3dcq413i08v42w9nwidwlg0r20r18h6ggp") (r "1.75")))

(define-public crate-tract-proxy-sys-0.21.5 (c (n "tract-proxy-sys") (v "0.21.5") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (d #t) (k 1)) (d (n "tract-ffi") (r "=0.21.5") (d #t) (k 0)))) (h "07ppb2k9qfwyrs0cjy7rf0i4fwlwl1p9i4nnqddil68vqhnrzb7n") (r "1.75")))

