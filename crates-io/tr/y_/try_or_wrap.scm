(define-module (crates-io tr y_ try_or_wrap) #:use-module (crates-io))

(define-public crate-try_or_wrap-0.0.2 (c (n "try_or_wrap") (v "0.0.2") (h "0dyyw7m2pwvha2w89xnhd6flxbkwgivr91bb1ga351d02xn26425")))

(define-public crate-try_or_wrap-0.0.3 (c (n "try_or_wrap") (v "0.0.3") (h "17c357cy4g0bkinqgxjly7lmva73kc32q2y804irr8zfijzwrdxa")))

(define-public crate-try_or_wrap-0.0.5 (c (n "try_or_wrap") (v "0.0.5") (h "0bgjalm3f9wxk8cj235dm022l4bddc6dqh1wcaav6vzxvpavai46")))

