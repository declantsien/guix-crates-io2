(define-module (crates-io tr y_ try_buf) #:use-module (crates-io))

(define-public crate-try_buf-0.1.0 (c (n "try_buf") (v "0.1.0") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0crbg0k72x440pcgf4qj30r2yp21l3ph3fp7gsw0wq1k46pjmqnc")))

(define-public crate-try_buf-0.1.1 (c (n "try_buf") (v "0.1.1") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1ipaw7qv8fggm6biwshqnl0zg16rh79h4yyx9g7s7njqlvyyqm1z") (y #t)))

(define-public crate-try_buf-0.1.2 (c (n "try_buf") (v "0.1.2") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "154bfdmgwislkc3wfbg4b9qgvs22kmk5i8f4vpvg8zi4fgzvzw53")))

(define-public crate-try_buf-0.1.3 (c (n "try_buf") (v "0.1.3") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0v03j6f9dh2my7xp4h7lx44x509pim3c9a02r4gjgw6yfbhvrwgf")))

