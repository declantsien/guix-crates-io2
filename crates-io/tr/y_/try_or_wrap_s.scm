(define-module (crates-io tr y_ try_or_wrap_s) #:use-module (crates-io))

(define-public crate-try_or_wrap_s-0.1.0 (c (n "try_or_wrap_s") (v "0.1.0") (h "1z37qrqjz1fc7p8apch9gq7vgkiccy52w40frbgdp5majhy6kjnf")))

(define-public crate-try_or_wrap_s-0.2.0 (c (n "try_or_wrap_s") (v "0.2.0") (h "19cmmj0kkha5d3fwz0x4xgsrg642dn5dnayand39sx32fycc1nai")))

