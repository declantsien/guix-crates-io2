(define-module (crates-io tr y_ try_zip) #:use-module (crates-io))

(define-public crate-try_zip-0.1.0 (c (n "try_zip") (v "0.1.0") (h "0gcvqbp1dy6yw75cjwfmcw5wlf823h14izx0c7dxirlf2kmjzwl3")))

(define-public crate-try_zip-0.2.0 (c (n "try_zip") (v "0.2.0") (h "0hcbgs1k9jm20ai5y21195ci8jj6hzf6x4j0ij1409rjp7d7ig4c")))

(define-public crate-try_zip-0.2.1 (c (n "try_zip") (v "0.2.1") (h "1gc48pb3p6513325c4652limncx8jn91x6xsyd66c93g8ibcdkpf")))

