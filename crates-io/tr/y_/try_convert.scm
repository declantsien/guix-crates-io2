(define-module (crates-io tr y_ try_convert) #:use-module (crates-io))

(define-public crate-try_convert-0.1.0 (c (n "try_convert") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 2)))) (h "19k6as9kgn0sfsaikljarkf4fkfpbjb3z1c0gwn2z286ai3wf4s2") (f (quote (("thiserror") ("default" "thiserror"))))))

(define-public crate-try_convert-0.1.1 (c (n "try_convert") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 2)))) (h "0jd9yw9sjlcz7lrbcg2sba0rk1mi1a3j212gq67d1fhf03rd9zwj") (f (quote (("thiserror") ("default" "thiserror"))))))

