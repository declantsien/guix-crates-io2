(define-module (crates-io tr y_ try_as_macros) #:use-module (crates-io))

(define-public crate-try_as_macros-0.1.0 (c (n "try_as_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "try_as_traits") (r "^0.1.0") (d #t) (k 0)))) (h "0gcqskxp04zdl2hvx961ya0bzrjax7fn2wrspgspmmfv3p1l629v")))

