(define-module (crates-io tr y_ try_ops) #:use-module (crates-io))

(define-public crate-try_ops-0.1.0 (c (n "try_ops") (v "0.1.0") (h "1iljcnb6gd0js6wcykmmgg4i72qadnq41nqhcc0dgs5j10gcnxcl")))

(define-public crate-try_ops-0.1.1 (c (n "try_ops") (v "0.1.1") (h "0rdmiliar22bflnvb0kr9rrjaww5afxfn51i6n5b351ayjins4vq")))

