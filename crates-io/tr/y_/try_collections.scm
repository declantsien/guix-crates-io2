(define-module (crates-io tr y_ try_collections) #:use-module (crates-io))

(define-public crate-try_collections-0.1.0 (c (n "try_collections") (v "0.1.0") (h "0xf7zjxr79bsi8xmlrvcx4q9y0ars5ndnihgd3clp8ph4bli6lhw")))

(define-public crate-try_collections-0.1.1 (c (n "try_collections") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.8.2") (f (quote ("raw"))) (d #t) (k 0)))) (h "0m78jdy288ng1jbbjxlrzk14zscrqh79y0kxj9fi3vskiy4flq4y")))

