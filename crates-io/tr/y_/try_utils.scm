(define-module (crates-io tr y_ try_utils) #:use-module (crates-io))

(define-public crate-try_utils-0.1.0 (c (n "try_utils") (v "0.1.0") (h "049pv2hs4d4cpm5yp5gwrzg50m0j099hqgb1203kphii1vdxk315")))

(define-public crate-try_utils-0.1.2 (c (n "try_utils") (v "0.1.2") (h "0wihbpzr7qrmjacif3hyc1x43id4xz43zfmf591fmnvgsi73ipn4")))

