(define-module (crates-io tr y_ try_match) #:use-module (crates-io))

(define-public crate-try_match-0.1.0 (c (n "try_match") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "try_match_inner") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "07asa158mn6c0nsx5cqvkvxhp8k0wqpqiy3vmfnysnfjskaia6mx") (f (quote (("std") ("implicit_map" "try_match_inner") ("default" "implicit_map" "std"))))))

(define-public crate-try_match-0.2.0 (c (n "try_match") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "try_match_inner") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0pjwflbqzn8a1q60dl1bg3vgg0bnjcmh1kgbqc4d6825g1p2x55h") (f (quote (("std") ("implicit_map" "try_match_inner") ("default" "implicit_map" "std"))))))

(define-public crate-try_match-0.2.1 (c (n "try_match") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (o #t) (d #t) (k 0)) (d (n "try_match_inner") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0j1r2rrfglz7rifpw8jk9y6bw3d16mpg45b77v9r88z8bf6iyqhn") (f (quote (("std") ("implicit_map" "proc-macro-hack" "try_match_inner") ("default" "implicit_map" "std"))))))

(define-public crate-try_match-0.2.2 (c (n "try_match") (v "0.2.2") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (o #t) (d #t) (k 0)) (d (n "try_match_inner") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "070fdzpmkzlffljrrpg3j4a1cvqj53wsm215hars2wjywgapk0hk") (f (quote (("std") ("implicit_map" "proc-macro-hack" "try_match_inner") ("default" "implicit_map" "std"))))))

(define-public crate-try_match-0.2.3 (c (n "try_match") (v "0.2.3") (d (list (d (n "if_rust_version") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5.11") (o #t) (d #t) (k 0)) (d (n "try_match_inner") (r "=0.3.0") (o #t) (d #t) (k 0)))) (h "1a6x88wyva8nrrh5x0gmhlfnavszfsbk9a5v9b8li2zz0kyxp3mv") (f (quote (("std") ("implicit_map" "proc-macro-hack" "try_match_inner") ("default" "implicit_map" "std"))))))

(define-public crate-try_match-0.3.0 (c (n "try_match") (v "0.3.0") (d (list (d (n "try_match_inner") (r "=0.4.0") (o #t) (d #t) (k 0)))) (h "0jsq3jj49aq24ls6wknfk90g4n718sw6k1n9vj8wax6cfka9r7vq") (f (quote (("std") ("implicit_map" "try_match_inner") ("default" "implicit_map"))))))

(define-public crate-try_match-0.4.0 (c (n "try_match") (v "0.4.0") (d (list (d (n "try_match_inner") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "087ra5lrqr8w1j7dmbi83jbb9jiibrcg6qf3082842gcml3p2qmm") (f (quote (("unstable") ("std") ("implicit_map" "try_match_inner") ("default" "implicit_map") ("_doc_cfg")))) (r "1.56")))

(define-public crate-try_match-0.4.1 (c (n "try_match") (v "0.4.1") (d (list (d (n "try_match_inner") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1r1bkqw8ix7dk89lb2vl619sx2gszf1jcmwfs8q9x1g884ckrbk1") (f (quote (("unstable") ("std") ("implicit_map" "try_match_inner") ("default" "implicit_map") ("_doc_cfg")))) (r "1.56")))

