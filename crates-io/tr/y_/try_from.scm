(define-module (crates-io tr y_ try_from) #:use-module (crates-io))

(define-public crate-try_from-0.1.0 (c (n "try_from") (v "0.1.0") (h "03biqhpm57wazim8asxhbak7imwhhrylcmyjvhsazdldyrkadyn5")))

(define-public crate-try_from-0.2.0 (c (n "try_from") (v "0.2.0") (h "1ycvpcd4kacpj4n53v2fwk1fj6r00hdhknrrb3mmlrwnvn56bw0j")))

(define-public crate-try_from-0.2.1 (c (n "try_from") (v "0.2.1") (h "0lszjrjkcrkq4hp7dbpg0ksv7c3ssv0dgc18vr3maxar3fr1zss8")))

(define-public crate-try_from-0.2.2 (c (n "try_from") (v "0.2.2") (h "0481awlrf5j37mlh4pa0a890lqn93i8v9gk1a9lfigvxx7ipwflj")))

(define-public crate-try_from-0.3.0 (c (n "try_from") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "0skqyin60vv4vi5nbqqimq8nhk0xiqsg5ylymh9d6iv922hrbmf9") (f (quote (("no_std"))))))

(define-public crate-try_from-0.3.1 (c (n "try_from") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "1b0kl2mnir48pcxp4lmj5nxf4ci8022zfls12ws4gwlwkdzm52p4") (f (quote (("no_std"))))))

(define-public crate-try_from-0.3.2 (c (n "try_from") (v "0.3.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "12wdd4ja7047sd3rx70hv2056hyc8gcdllcx3a41g1rnw64kng98") (f (quote (("no_std"))))))

