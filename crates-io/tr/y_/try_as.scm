(define-module (crates-io tr y_ try_as) #:use-module (crates-io))

(define-public crate-try_as-0.1.0 (c (n "try_as") (v "0.1.0") (d (list (d (n "try_as_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "try_as_traits") (r "^0.1.0") (d #t) (k 0)))) (h "1nx0hpqs735vl23yyisjs52x8yhhmiph6lblxbd63ndhpkgnqij4")))

