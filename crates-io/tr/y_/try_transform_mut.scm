(define-module (crates-io tr y_ try_transform_mut) #:use-module (crates-io))

(define-public crate-try_transform_mut-0.1.0 (c (n "try_transform_mut") (v "0.1.0") (h "1phx4j3d19sd0rq0wvpmh5p1j9v0yp5ym8z1ppjzklkrwwj43ik5")))

(define-public crate-try_transform_mut-0.1.1 (c (n "try_transform_mut") (v "0.1.1") (h "0bcja5qgwgj37cf1z8b8vkxp09vl4c7a3jsxg6ir3f5kfrws9i05")))

