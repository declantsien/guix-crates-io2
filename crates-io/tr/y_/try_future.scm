(define-module (crates-io tr y_ try_future) #:use-module (crates-io))

(define-public crate-try_future-0.1.0 (c (n "try_future") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)))) (h "0hgxcq1ikybgkzh2n9c1n73zh1bzgh0010sq12g8rsn9z2ppadyd")))

(define-public crate-try_future-0.1.1 (c (n "try_future") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)))) (h "0xw612fs47h28j2ggqg3814f92018xp4kclqf8phgqblkcissqjl")))

(define-public crate-try_future-0.1.2 (c (n "try_future") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)))) (h "0i85i3x87nc69zx1b7x7f6ymscbhn1z6bb7qy7w9kasd429im6dm")))

(define-public crate-try_future-0.1.3 (c (n "try_future") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)))) (h "1mxabd69nhcp8l7gkdhd8f2kxhxpfgvykil6yc39xwiqxs342i9h")))

