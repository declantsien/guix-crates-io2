(define-module (crates-io tr y_ try_default) #:use-module (crates-io))

(define-public crate-try_default-1.0.0 (c (n "try_default") (v "1.0.0") (h "0gh4n9r8p324a3syj0xr8cqhpf0p7f2xxzmzprrhbvb1cl20cmc5")))

(define-public crate-try_default-1.0.1 (c (n "try_default") (v "1.0.1") (h "022rywc5dh1zmnkbi7lwa7plihqvb30sj1xzvbb5dgjpl46kv4w2")))

