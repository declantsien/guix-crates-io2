(define-module (crates-io tr y_ try_fold_impl) #:use-module (crates-io))

(define-public crate-try_fold_impl-0.1.0 (c (n "try_fold_impl") (v "0.1.0") (h "166wmnfx0bj20aizw0zf5yy6p1j0lfxiqkv3qfr80pxa7rs292bz") (y #t)))

(define-public crate-try_fold_impl-0.1.1 (c (n "try_fold_impl") (v "0.1.1") (h "0ijkbmy9z54wnglkgsh4vi2bzrgy6kxvw9zawgy1sydw8c3pyvh2") (y #t)))

(define-public crate-try_fold_impl-0.1.2 (c (n "try_fold_impl") (v "0.1.2") (h "10272dzd5n4xqb5wxbp3pgkmd4laf4jdhkkv16wg11bw7y80v3b5")))

(define-public crate-try_fold_impl-0.2.0 (c (n "try_fold_impl") (v "0.2.0") (h "0b2pgzhxb5zl5cql7w4711ls36si8rrpq4cm4pv7wrfwiwzsn7h5")))

(define-public crate-try_fold_impl-0.2.1 (c (n "try_fold_impl") (v "0.2.1") (h "10b960p03fjg3pfvaszs6gg1nfizdqarm0vgrpmk7p1fn964hgy4")))

