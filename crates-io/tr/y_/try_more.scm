(define-module (crates-io tr y_ try_more) #:use-module (crates-io))

(define-public crate-try_more-0.1.0 (c (n "try_more") (v "0.1.0") (h "01bybwzc2h1mm9zm2gnr86dggbgks9hm1by5swqjp4wrpwr0nz4q")))

(define-public crate-try_more-0.1.1 (c (n "try_more") (v "0.1.1") (h "1c219bfysy080m95l9sha5aa714qjjxlks9p97h89p4ddr2ns8m0")))

