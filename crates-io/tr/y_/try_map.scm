(define-module (crates-io tr y_ try_map) #:use-module (crates-io))

(define-public crate-try_map-0.1.1 (c (n "try_map") (v "0.1.1") (h "14mi01fk3kil8bnv5rqx8ijngyamfcb6h9lplywmp29kk7hmyn5x")))

(define-public crate-try_map-0.2.0 (c (n "try_map") (v "0.2.0") (h "0fsa9fxb2880acm3faxg4khn9zn885vqdga5xj5rsmy3r70gk56p")))

(define-public crate-try_map-0.3.0 (c (n "try_map") (v "0.3.0") (h "1ychd04d210csd0sc3bqjc50ga4sq24mlmydsd2nwbdwfz9dcjyn")))

(define-public crate-try_map-0.3.1 (c (n "try_map") (v "0.3.1") (h "106mz6cxmgf5m34ww6h45g0ags2j92zc153xy4nbphdmgk82c5pv")))

