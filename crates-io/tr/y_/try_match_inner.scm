(define-module (crates-io tr y_ try_match_inner) #:use-module (crates-io))

(define-public crate-try_match_inner-0.1.0 (c (n "try_match_inner") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0n6fqpwk3qj4ipwjfdjby3960n012qqkyxln4x1qxjnv7097i0xr")))

(define-public crate-try_match_inner-0.1.1 (c (n "try_match_inner") (v "0.1.1") (d (list (d (n "proc-macro-error") (r ">= 0.3.1, < 2.0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pz0w9h39ikj2xmkh3pcy4wqjdpqzr60gl716xxg7pr5x8yvysxm")))

(define-public crate-try_match_inner-0.2.0 (c (n "try_match_inner") (v "0.2.0") (d (list (d (n "proc-macro-error") (r ">=0.3.1, <2.0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bpqbs5kahgpcjaqpsh7k4kisa8j43mddc9gdlqyfbl47pfw0xdw")))

(define-public crate-try_match_inner-0.3.0 (c (n "try_match_inner") (v "0.3.0") (d (list (d (n "proc-macro-error") (r ">=0.3.1, <2.0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1xg8649bkzzciirpppwvnd29ij3awvp4b3x3xczwf9kjk01n8bx7")))

(define-public crate-try_match_inner-0.4.0 (c (n "try_match_inner") (v "0.4.0") (d (list (d (n "proc-macro-error") (r ">=0.3.1, <2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1aa9vcar6mi8zlf9498fyc8lr0v5m4650710sih1mpqvpisnnzk0")))

(define-public crate-try_match_inner-0.5.0 (c (n "try_match_inner") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0008jazl3cabiggx3x2zyzi6gy43yh57jkys5mmf1bv4bx7alzpg")))

(define-public crate-try_match_inner-0.5.1 (c (n "try_match_inner") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0kpkpwf8fy8r27xh5fb1gdg8frjmjj4795rwnbncm6172c9igadh")))

