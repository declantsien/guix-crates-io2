(define-module (crates-io tr y_ try_opt) #:use-module (crates-io))

(define-public crate-try_opt-0.1.0 (c (n "try_opt") (v "0.1.0") (h "189x3lylfn0zk5yr5nbakn09dm8njf4s7zcqwqg258a0w2yhqnlk")))

(define-public crate-try_opt-0.1.1 (c (n "try_opt") (v "0.1.1") (h "1c8d6y430h5ggjjsnqh9vxd025kgwd79zfpkffc1yma0pbkp22yk")))

(define-public crate-try_opt-0.2.0 (c (n "try_opt") (v "0.2.0") (h "1h2v7q57y7a8jrxinb67hwkiv5pm93jvxkjl6jgl6zc6jxj2qa4f")))

