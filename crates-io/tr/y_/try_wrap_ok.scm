(define-module (crates-io tr y_ try_wrap_ok) #:use-module (crates-io))

(define-public crate-try_wrap_ok-0.1.0 (c (n "try_wrap_ok") (v "0.1.0") (h "0yjcsxxcc32bk5c7bkhv1ihxcmmn58v9avg2n3000s7s2h0l0nv0") (y #t)))

(define-public crate-try_wrap_ok-0.1.1 (c (n "try_wrap_ok") (v "0.1.1") (h "0klyrip7rfhc54a8caqlp25kb52vsqhg7008lzdnbyqs2n9xwi3q")))

