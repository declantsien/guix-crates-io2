(define-module (crates-io tr y_ try_all) #:use-module (crates-io))

(define-public crate-try_all-0.0.1 (c (n "try_all") (v "0.0.1") (h "1klmr4svb37hgxmna1czaq97qnya4yrcj1jlky35y1ia9b71fdgn")))

(define-public crate-try_all-0.0.2 (c (n "try_all") (v "0.0.2") (h "1b3bhx2zlqab6x7074lq27nz3phl0l84fd3c6vjl9w7jpnpcyvig")))

