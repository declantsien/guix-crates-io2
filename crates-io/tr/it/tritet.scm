(define-module (crates-io tr it tritet) #:use-module (crates-io))

(define-public crate-tritet-0.1.0 (c (n "tritet") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "00klv925j3gbbxxy6lzdj4xldb2wd8djwkyjrg954lz5889gmind")))

(define-public crate-tritet-0.2.0 (c (n "tritet") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "plotpy") (r "^0.3") (d #t) (k 0)))) (h "0k99pnac572c9nnvis8pk2a5glh3kh3cl1vwdkl4i5zlcg5dalcd")))

(define-public crate-tritet-0.2.1 (c (n "tritet") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "plotpy") (r "^0.4") (d #t) (k 0)))) (h "1rmc0fwmhyllrr8pmahsbgfslmnhxdzsbhgh3k4cmsim2v47y28i")))

(define-public crate-tritet-0.3.0 (c (n "tritet") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "plotpy") (r "^0.5") (d #t) (k 0)))) (h "04njxmylnafv8frdmnwd9nlnq3b6qhvfyl12dpv9gmjg9y7wxs9r")))

(define-public crate-tritet-0.3.1 (c (n "tritet") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "plotpy") (r "^0.5") (d #t) (k 0)))) (h "1a41yy3gygj6alrp1vpimx02bpilfd9l7ynwq51vvisa69qfr3lf")))

(define-public crate-tritet-0.4.0 (c (n "tritet") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "plotpy") (r "^0.5") (d #t) (k 0)))) (h "1hr5zyr7rhc9147m8s6ycp194v281k0l6v0zxzi5jw14j13m2101")))

