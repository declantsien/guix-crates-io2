(define-module (crates-io tr it triton-rs) #:use-module (crates-io))

(define-public crate-triton-rs-0.1.0 (c (n "triton-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "triton-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0kabznc6wcy010mbacr9dj32xvf7c9wl1kmvjiv27c5qyxvy14bz")))

(define-public crate-triton-rs-0.1.1 (c (n "triton-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "triton-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1y9151wrddw96pp8mdsl8jyvgc0mfzgvr6gsbqzr9br2dnk75wkn")))

(define-public crate-triton-rs-0.2.0 (c (n "triton-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "triton-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0l0893gbzqirjdr7nfyxw8x3h09y6azmyrrwcc3my8zyz2sgcd6h")))

