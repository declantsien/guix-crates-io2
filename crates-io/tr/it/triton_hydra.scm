(define-module (crates-io tr it triton_hydra) #:use-module (crates-io))

(define-public crate-triton_hydra-0.0.1 (c (n "triton_hydra") (v "0.0.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02ac2py8wzwj448jd57gr5w7f0gzqlnn3icjv5ngqjyfdx17w83s") (y #t)))

