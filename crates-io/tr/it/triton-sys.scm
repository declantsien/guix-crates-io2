(define-module (crates-io tr it triton-sys) #:use-module (crates-io))

(define-public crate-triton-sys-0.1.0 (c (n "triton-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0cqmbh6yr1nn3430x6y8hgcj49kr920gzh3hbrdjq5bm0sm56cg0")))

(define-public crate-triton-sys-0.1.1 (c (n "triton-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0cdg1kziv9gljnfm6bc8lvhsjijpkmh26ray5pv37lg9vsb847aj")))

(define-public crate-triton-sys-0.2.0 (c (n "triton-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0lvy0d5xc4ncyik98fil71549i92xvzniwnrh9hp47hf0sssrx8h")))

