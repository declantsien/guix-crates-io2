(define-module (crates-io tr it trit) #:use-module (crates-io))

(define-public crate-trit-0.1.0 (c (n "trit") (v "0.1.0") (h "1hylq2jb970m1wshp9jw83ylfk08ayrd73bac2pbbi1880g3m4la")))

(define-public crate-trit-0.1.1 (c (n "trit") (v "0.1.1") (h "1n7pg48sf0scxdsl5lb4cmlyq0s64cv80rhkcb39qbapfz7mza84")))

(define-public crate-trit-0.1.2 (c (n "trit") (v "0.1.2") (h "01ln8xzbxlgccgsi6yzwcrvd1a2jalc8lplny127wlkxzmwwswgk")))

(define-public crate-trit-0.1.3 (c (n "trit") (v "0.1.3") (h "1z7vrm4d32hdygp1c0iwy2wgrrnd1q903cc6gndpii3wkjp9rk5l")))

