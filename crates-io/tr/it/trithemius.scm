(define-module (crates-io tr it trithemius) #:use-module (crates-io))

(define-public crate-trithemius-2.0.0 (c (n "trithemius") (v "2.0.0") (d (list (d (n "image") (r "^0.22.0") (d #t) (k 0)))) (h "02s3nbbjm2g6rfb3k1l28ysgswi13aycfj5bnrw71f5fp67r5zmw")))

(define-public crate-trithemius-2.0.1 (c (n "trithemius") (v "2.0.1") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0v8qx34ab2jflscpvxplx5qx6zyymkvqay05kfd0jac4wwal72vi")))

(define-public crate-trithemius-2.0.2 (c (n "trithemius") (v "2.0.2") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0vdsvd0jqgxwn6lfakwm8qvwa9wx0nfn8gkqzrp0fay30gxfm6vq")))

