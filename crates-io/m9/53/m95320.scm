(define-module (crates-io m9 #{53}# m95320) #:use-module (crates-io))

(define-public crate-m95320-1.0.0 (c (n "m95320") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "rppal") (r "^0.12.0") (f (quote ("hal"))) (d #t) (k 2)))) (h "0jqslwbrhqfbavk8p7w9rdz3z8x1zil4nmj23xj0h0kf14l8jq3l")))

(define-public crate-m95320-1.0.1 (c (n "m95320") (v "1.0.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "rppal") (r "^0.12.0") (f (quote ("hal"))) (d #t) (k 2)))) (h "0hx9599f05rzb24r66qim5y9isynhzlaxa3izbq7mrvyc5x68fv8")))

(define-public crate-m95320-1.0.2 (c (n "m95320") (v "1.0.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "rppal") (r "^0.12.0") (f (quote ("hal"))) (d #t) (k 2)))) (h "1rjzwh6a14qzk9f0msici8p9j1w5x85pgmdpbig8k5isafi2bd0g")))

(define-public crate-m95320-1.0.3 (c (n "m95320") (v "1.0.3") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "port-expander") (r "^0.3.0") (d #t) (k 2)) (d (n "rppal") (r "^0.12.0") (f (quote ("hal"))) (d #t) (k 2)))) (h "10zr63wm22vprr7wnws9w2q7zz7agrdqg07b7kabwy1mr813d0wl")))

(define-public crate-m95320-1.1.0 (c (n "m95320") (v "1.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "port-expander") (r "^0.3.0") (d #t) (k 2)) (d (n "rppal") (r "^0.12.0") (f (quote ("hal"))) (d #t) (k 2)))) (h "0gzzgavy3h94i1025bzl8w4gzj29iwv3d8fwl3zpqm84rcx9v9xs")))

(define-public crate-m95320-1.2.0 (c (n "m95320") (v "1.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "port-expander") (r "^0.3.0") (d #t) (k 2)) (d (n "rppal") (r "^0.12.0") (f (quote ("hal"))) (d #t) (k 2)))) (h "1cz1my3sqc9lpb3j0jmg9rcykf1sqrsghgpid9z64rl97pxz1d6w")))

(define-public crate-m95320-1.3.0 (c (n "m95320") (v "1.3.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "port-expander") (r "^0.3.0") (d #t) (k 2)) (d (n "rppal") (r "^0.12.0") (f (quote ("hal"))) (d #t) (k 2)))) (h "0x08zmsi34mb04ksnf3ki9xqvifkihzqwjki4rbsrgi1lgidbbi2")))

