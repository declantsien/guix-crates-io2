(define-module (crates-io uu _r uu_relpath) #:use-module (crates-io))

(define-public crate-uu_relpath-0.0.1 (c (n "uu_relpath") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1iqa07yg0limza6dhlkf98xiidaxj6lnr0b3y3bzdr6bx2py9c80")))

(define-public crate-uu_relpath-0.0.2 (c (n "uu_relpath") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "17kw1gwyf89w7ffl8mlgn79p4jzdnwvwq1318zqphbg4b57l3wx9")))

(define-public crate-uu_relpath-0.0.3 (c (n "uu_relpath") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1q6wmbyjjx5bw8p5b2x4jx432plq44zywqwzg6a86l3x68703zmc")))

(define-public crate-uu_relpath-0.0.4 (c (n "uu_relpath") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "149y8ffiji72fm3wsxzqdldr1cbqc6grj041sxprvqimjdda4bzp")))

(define-public crate-uu_relpath-0.0.5 (c (n "uu_relpath") (v "0.0.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1jmnizs1v8qf4jng4maiglk912g6qih6zc46y8pzpjyhbihnk5n5")))

(define-public crate-uu_relpath-0.0.6 (c (n "uu_relpath") (v "0.0.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "09ndw6gvdpr493plq8ik581pkily2gkg3alggaw43j0jigqqiazx")))

(define-public crate-uu_relpath-0.0.7 (c (n "uu_relpath") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1pb3rsf6ykrn7g41yrazlg3qz7cnaisyvmd9p26q1irgwl5n22wd")))

(define-public crate-uu_relpath-0.0.8 (c (n "uu_relpath") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1rq4kwjlq3hygyhs9xw1nidakq6h1l8xqv9cshyd60g54pb9i87p")))

(define-public crate-uu_relpath-0.0.9 (c (n "uu_relpath") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "120wlpfssmz7qm6b1jaws11908dzvgkprqjx1y5l5jyrdh02ni84")))

(define-public crate-uu_relpath-0.0.12 (c (n "uu_relpath") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1bcvbs2l7wgllvsgq3c205z6mshwmwsbmy4pl5lcvf8njl9gi1cp")))

(define-public crate-uu_relpath-0.0.13 (c (n "uu_relpath") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "076i8swq4fzl772m67k2m6vj4h801bz8ymiqdn9rd41x39w9d3rj")))

(define-public crate-uu_relpath-0.0.14 (c (n "uu_relpath") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1ik4zmzyshww67apm6bb9zd75igq9g7wi99j87c4svx145lii7q5")))

(define-public crate-uu_relpath-0.0.15 (c (n "uu_relpath") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1djniy0p4g7ndqrpxrr415q76d0k89705i5zjmqb1r79rppckdsi")))

(define-public crate-uu_relpath-0.0.16 (c (n "uu_relpath") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "13nx1n3zwncrgfcmm0kafi7p3ml449n1xmyw0hz9x9w28v5l0nr9")))

(define-public crate-uu_relpath-0.0.17 (c (n "uu_relpath") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1z95ddb8w6v5sas4a9k13mahh8fb4jscl3bkssg7jy4y36q2f2rj")))

(define-public crate-uu_relpath-0.0.18 (c (n "uu_relpath") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "18yd61z7lbhs6y83h7jjq4n1ly38qibjakl0c1xwsjhqijj979pn")))

(define-public crate-uu_relpath-0.0.19 (c (n "uu_relpath") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0drkivflnv9sm63y1gi6kaynmds261ggnai7zd06ppdl6ziwyznd")))

(define-public crate-uu_relpath-0.0.20 (c (n "uu_relpath") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0vwfdi1fj8vi65dfb97dvaggmxl7x1164psmn8kjm39ncmgy39gn")))

(define-public crate-uu_relpath-0.0.21 (c (n "uu_relpath") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0qn19k5lz7f7rv664zvmw8mj91wfbgc6v5kbar64xkxphbkq8hqn")))

