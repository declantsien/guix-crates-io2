(define-module (crates-io uu _r uu_realpath) #:use-module (crates-io))

(define-public crate-uu_realpath-0.0.1 (c (n "uu_realpath") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "05cj3bmvdn0zs4qw5a2sbgscbx8i6q9yyhnxbcr8j2lmikbijf4g")))

(define-public crate-uu_realpath-0.0.2 (c (n "uu_realpath") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1w1vm9nyp2k3byf5dphi3bin9hqn8d8d1nk07412mmqq74scwl0q")))

(define-public crate-uu_realpath-0.0.3 (c (n "uu_realpath") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "10hnf8sgx3f8j28bclnzdl5lx16sr6knjl7ckx34pflm84fmf9p9")))

(define-public crate-uu_realpath-0.0.4 (c (n "uu_realpath") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0yr8j9whyprqvig0gjyczgpp1di2g09b7admx12zhjnz5yk3aabp")))

(define-public crate-uu_realpath-0.0.5 (c (n "uu_realpath") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0gcrfj511i04r4c7nlwsnzpslx62a7pdlfrgk8y81v0lfh0ay59r")))

(define-public crate-uu_realpath-0.0.6 (c (n "uu_realpath") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1mv1rfxk6ms44fnwhf9k8zpgbc8gxxn04wzf8lbjw66rif3hqaz1")))

(define-public crate-uu_realpath-0.0.7 (c (n "uu_realpath") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1il8ii623gq87zqwz2j050f79ggvf97hb2fxh568g8lfnmf2d41d")))

(define-public crate-uu_realpath-0.0.8 (c (n "uu_realpath") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1rb6zrmxfllwd3pyi9qzvp60gin14mnfdhgnvq1jm927v65iij8p")))

(define-public crate-uu_realpath-0.0.9 (c (n "uu_realpath") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "195023y01igy1v2cvz8k3i2w7lc34c7kw9nxvnplhfk0fb4fv73c")))

(define-public crate-uu_realpath-0.0.12 (c (n "uu_realpath") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1msgb9z1gn9pvmlx11lv40p53j8nnh9anrdw8ccckc38xhq82j3l")))

(define-public crate-uu_realpath-0.0.13 (c (n "uu_realpath") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1j0mxc9dhzcrxv9mmz55773qfqbf69sjddlhjzlidlkqkp6wk0hd")))

(define-public crate-uu_realpath-0.0.14 (c (n "uu_realpath") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1qsri7csnicl23xfj2sc5pbkz4j57ws6qxgwlrkhaqk0miwmphy7")))

(define-public crate-uu_realpath-0.0.15 (c (n "uu_realpath") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1a8z8lac88y3zd1vqxw4hqr0n8m2bjq3rh4r3df6sgm28f702zsn")))

(define-public crate-uu_realpath-0.0.16 (c (n "uu_realpath") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1ffv7c1lg6f1dy21dc00mk4pig14v7pvj2i0j2r7s0758d1qvgyk")))

(define-public crate-uu_realpath-0.0.17 (c (n "uu_realpath") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1swbj1ix5sk317fp8a57z5r1wmvad1w46iid0n6d397b9kcljilm")))

(define-public crate-uu_realpath-0.0.18 (c (n "uu_realpath") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0gf75z7a2irhjlc3dg3allvndsnp9qkqc3d7lzah6h6xjxzbf54f")))

(define-public crate-uu_realpath-0.0.19 (c (n "uu_realpath") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0gzrm0bbn1aljj8rjn199vfnfqnxsdmj8dskqjlb2irfqayp4301")))

(define-public crate-uu_realpath-0.0.20 (c (n "uu_realpath") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0v4x9gz5yklqm2057z7nal0rf3bd93v5wjn9s8bihvsyn17v478n")))

(define-public crate-uu_realpath-0.0.21 (c (n "uu_realpath") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0lr45qnzxbhis8y5b2qdl2ya5a5w6r18h6ml7xdap8byyskz4jqy")))

(define-public crate-uu_realpath-0.0.22 (c (n "uu_realpath") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1hb277mwhb30s80pdr0ka2fwf6xmwndvk5x08pjysq1hgl9z06xh")))

(define-public crate-uu_realpath-0.0.23 (c (n "uu_realpath") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1yvnrnsm66acjkx6yap6y5gq8ix268g3kvkk86g741q95andwkxc")))

(define-public crate-uu_realpath-0.0.24 (c (n "uu_realpath") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1hr1h07vrw6j9nr3jv24k05i1ahyb2imv061zjmp02wa1iw404aq")))

(define-public crate-uu_realpath-0.0.25 (c (n "uu_realpath") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1hn0isapz0z4pyqal83hffjw81rls1qhh2mxn6hprg39pjq9aavg")))

(define-public crate-uu_realpath-0.0.26 (c (n "uu_realpath") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0p6mm0fmqfnqsznrkxpnqfxnwq81dr8f29vap4wpygzc5zmrjdk6")))

