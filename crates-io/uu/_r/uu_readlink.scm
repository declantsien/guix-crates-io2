(define-module (crates-io uu _r uu_readlink) #:use-module (crates-io))

(define-public crate-uu_readlink-0.0.1 (c (n "uu_readlink") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1k7k391457gy7qabx0pwxg6wa71ck1wqyvyn6kzg4x0bnq3c72w9")))

(define-public crate-uu_readlink-0.0.2 (c (n "uu_readlink") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "167yd4sr3gb1bjys0mj09j98g223zqi05j09dv1kncbd9k9vin2j")))

(define-public crate-uu_readlink-0.0.3 (c (n "uu_readlink") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1dmgdyf1pd2ri7mgni59q7rk6nyci9q8k345mkafbmsns81v0c45")))

(define-public crate-uu_readlink-0.0.4 (c (n "uu_readlink") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0nhk78ljjyl25wly0jzv7c2j2c4jg7n9zjcsgl5xr2lsf7mkfsjm")))

(define-public crate-uu_readlink-0.0.5 (c (n "uu_readlink") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "065f0li44xxqf3fgp3chznhaqf7z1qr83h3n1dqjd3pvws4cbrjq")))

(define-public crate-uu_readlink-0.0.6 (c (n "uu_readlink") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1vxn1akj2qny69w3ijzv7n8cfnxdl0p4g4ckwqwlavjhpwahh1bj")))

(define-public crate-uu_readlink-0.0.7 (c (n "uu_readlink") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0q5r9lskijjza6wmv8d8bxiy0yc0i1bidc7sb2ciy5qx70a0zy5f")))

(define-public crate-uu_readlink-0.0.8 (c (n "uu_readlink") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "00l2dd37gspbyyhq1pnc7haif96gcchb9ymr6a5whx51nkf7vmhh")))

(define-public crate-uu_readlink-0.0.9 (c (n "uu_readlink") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "047ajywysk0xivrkb25ijc0xg1l4w2ajz67qp67ha6j5a6krddz7")))

(define-public crate-uu_readlink-0.0.12 (c (n "uu_readlink") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "039qf6k707lfvvlggja4fwrivyh9g1ambryyj1mqyvn7ylvba7c2")))

(define-public crate-uu_readlink-0.0.13 (c (n "uu_readlink") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1psyb1g7pvicgyza7nkf3fjkb0s22z2iw23529w1xk4mkaf6gsgs")))

(define-public crate-uu_readlink-0.0.14 (c (n "uu_readlink") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "125yxsrimgh81vhzxmm4n7y41bm2d034v77z177yy2z5v5y2dl4s")))

(define-public crate-uu_readlink-0.0.15 (c (n "uu_readlink") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "137p0c7r770w10djgw2k9kvq4d53qrnmr3qm2ca5y6ql8dbc498h")))

(define-public crate-uu_readlink-0.0.16 (c (n "uu_readlink") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0flcp8sakb6ng17pah9v404jc747byh0j01nq0qvhrr7rybfcdkv")))

(define-public crate-uu_readlink-0.0.17 (c (n "uu_readlink") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1nx31y583976km2vq0nj1vksjxma0lhm5c7lwdf3wja7kzj1c06n")))

(define-public crate-uu_readlink-0.0.18 (c (n "uu_readlink") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "015imdy7551jvrhhi7wqqdknbviv8hz9yg2m2c5a64b91cjxpfva")))

(define-public crate-uu_readlink-0.0.19 (c (n "uu_readlink") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1a5lpcsf9km0jj71hdcm6jk3x2y1mnn36h2y9cmx427f5998dpfn")))

(define-public crate-uu_readlink-0.0.20 (c (n "uu_readlink") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1f6vql84cxpq2jxpwk47fk6v0mcvwsiikzmw1h3w7qdwxmzx5s0f")))

(define-public crate-uu_readlink-0.0.21 (c (n "uu_readlink") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1592crynx68hidgzbw53nqmh3v4c0h5qyc61lp4wixhvrlf0xrb7")))

(define-public crate-uu_readlink-0.0.22 (c (n "uu_readlink") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "09maxrpbjbjd5zmya9dbsih6db94lc2li27bv8z291klyvkz2j3f")))

(define-public crate-uu_readlink-0.0.23 (c (n "uu_readlink") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "03n0g2s909fdswa3zhn2fm3zs0gpaxmpy23hr9kkyb0bdiid2k9p")))

(define-public crate-uu_readlink-0.0.24 (c (n "uu_readlink") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0n2l7h6qyjpc8bz63l6h8v0lan7pbpcdk7wcxdrnhs2g4xqmdj5l")))

(define-public crate-uu_readlink-0.0.25 (c (n "uu_readlink") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0w141nx08ygc59sqqwk9f6gj2daf0hn4iyq7hiz2amivxpxx85pf")))

(define-public crate-uu_readlink-0.0.26 (c (n "uu_readlink") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1qbjrwnri6jhsp5gwh3h0icm25dl8ljmfw3rlck339pkabzmjcly")))

