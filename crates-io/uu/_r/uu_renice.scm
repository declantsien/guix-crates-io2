(define-module (crates-io uu _r uu_renice) #:use-module (crates-io))

(define-public crate-uu_renice-0.0.1 (c (n "uu_renice") (v "0.0.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "uucore") (r "^0.0.24") (d #t) (k 0)))) (h "01zazha8nhp6n3agrs8qw32khkcdvd9s4sf3ii0gnyn29j31hjaf")))

