(define-module (crates-io uu _r uu_rmdir) #:use-module (crates-io))

(define-public crate-uu_rmdir-0.0.1 (c (n "uu_rmdir") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1by2hqc277xnb6k5m4rdv0wxgrzy3frws25wwc4gilrlp5vm57xj")))

(define-public crate-uu_rmdir-0.0.2 (c (n "uu_rmdir") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0y4si459dr7l4v09k9d30n8cqc6vjw6fm3akdqdrhhhj95w59gyk")))

(define-public crate-uu_rmdir-0.0.3 (c (n "uu_rmdir") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0jr5j8klfhwcyz4g59n2llb87jpwnbpdhfi7w8l68zsmdm9flr3b")))

(define-public crate-uu_rmdir-0.0.4 (c (n "uu_rmdir") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0w6n30hcb7vbxwgvl4wzj06nqbs7l3psbn7qc8sb1s48y651ay47")))

(define-public crate-uu_rmdir-0.0.5 (c (n "uu_rmdir") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1k8d0mybdhw6wnm73c608hbwxzzwbbmk5d1lz8mymk4cjwvwvvaw")))

(define-public crate-uu_rmdir-0.0.6 (c (n "uu_rmdir") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0qbcz4hqmy5xgh19r6ih18czp50pyxnmlylr1kx035axifdwh29d")))

(define-public crate-uu_rmdir-0.0.7 (c (n "uu_rmdir") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1yf9b54c17jjr56klhnbgp26sbff7a7dy5bmfjkxn4g9fsklgxlw")))

(define-public crate-uu_rmdir-0.0.8 (c (n "uu_rmdir") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0l9j8aiw8cbqr53g1sl1ksl1l9wxn9ik6hzlajw88hlrdd87f76h")))

(define-public crate-uu_rmdir-0.0.9 (c (n "uu_rmdir") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0ydp1q6sfaqxqc7ym9dg4y900dcx2xivjk4v14xyjjxnwzi6mk3l")))

(define-public crate-uu_rmdir-0.0.12 (c (n "uu_rmdir") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0b2kqmk6w6a5xh514ixwdyzg17b4izjnnia4l0jwmp93a1k5qwi6")))

(define-public crate-uu_rmdir-0.0.13 (c (n "uu_rmdir") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0s4xmncmv4s2458a9a0wxgib0ar34r51h3bd2xqv64wis1i1aca9")))

(define-public crate-uu_rmdir-0.0.14 (c (n "uu_rmdir") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1vimp6wp20782hrw990g5c6vxhcn88nx1f90hcc0q8pzyrhzync3")))

(define-public crate-uu_rmdir-0.0.15 (c (n "uu_rmdir") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "11v2l6a539h0zgfppa8l7dlchbcm3cfhvwxdjrz6vs3hs0x49xgl")))

(define-public crate-uu_rmdir-0.0.16 (c (n "uu_rmdir") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1ahg103d05j5iy0q81smdl3l0vm6vxfjk0argyiqc4w0lrprhw2i")))

(define-public crate-uu_rmdir-0.0.17 (c (n "uu_rmdir") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1nsdp6az2z54lx0vyzb1gsacrs4gfajh5435r0mi50wvprb27pwy")))

(define-public crate-uu_rmdir-0.0.18 (c (n "uu_rmdir") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1pp6l1n31yzhw2sjv53pp6hydj12496y9naawmgrk6gpbgg21mkq")))

(define-public crate-uu_rmdir-0.0.19 (c (n "uu_rmdir") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1590f8v8yy78niwcjhxvzn62ga7d8zmy7mn6jszbrk5v7fbs1riz")))

(define-public crate-uu_rmdir-0.0.20 (c (n "uu_rmdir") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1ihy6iby4c7sjlrsybvilz9fgxgmvanr78vbhih01m0ccvk8j8d2")))

(define-public crate-uu_rmdir-0.0.21 (c (n "uu_rmdir") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "18y7hs4aybfvvq930q4c17fmpgqsl316qlksq847h0y96ayay7nz")))

(define-public crate-uu_rmdir-0.0.22 (c (n "uu_rmdir") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0sb3yfs4g0lssa5519w05m1q1s1vxry2gfjg042g4v6fy5f2cp8z")))

(define-public crate-uu_rmdir-0.0.23 (c (n "uu_rmdir") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0mbb5fxzd9f2lii0d9pvf1nv5g0952jxh80sqhd12wpjw8aqns0h")))

(define-public crate-uu_rmdir-0.0.24 (c (n "uu_rmdir") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1f3bxdc6sa0dgyri2kdmkdrhnqic2gb8c3yg40akphhmkgmwd619")))

(define-public crate-uu_rmdir-0.0.25 (c (n "uu_rmdir") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1k6jggvq2266s9z0xcbg6i01j8n81vpbzyi01bfalfn1adddng6z")))

(define-public crate-uu_rmdir-0.0.26 (c (n "uu_rmdir") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0m52diq8rvkv58i6wgmqjv0d82ycid4n9sa421i1msp84if18s2d")))

