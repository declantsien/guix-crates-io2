(define-module (crates-io uu _a uu_arch) #:use-module (crates-io))

(define-public crate-uu_arch-0.0.1 (c (n "uu_arch") (v "0.0.1") (d (list (d (n "platform-info") (r "^0.0.1") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0d2qnfjr2wx43jygix0vpwk6851s1g5y90j3ymzc7f9gcnpmdg2f")))

(define-public crate-uu_arch-0.0.2 (c (n "uu_arch") (v "0.0.2") (d (list (d (n "platform-info") (r "^0.0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0az85qr1p7hx43akw2ibh1qf2x7fqx6010j4znrh8zjc90l2rm9q")))

(define-public crate-uu_arch-0.0.3 (c (n "uu_arch") (v "0.0.3") (d (list (d (n "platform-info") (r "^0.0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "00jdihd7arbgh0ng27zdb5wj6qxd3vbprxrajwv5n2abrglgpr8g")))

(define-public crate-uu_arch-0.0.4 (c (n "uu_arch") (v "0.0.4") (d (list (d (n "platform-info") (r "^0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0mhci25xrpmi2dsxxgf3j8qn39dbbbxy4wx3c4586qg8ncrp9fwn")))

(define-public crate-uu_arch-0.0.5 (c (n "uu_arch") (v "0.0.5") (d (list (d (n "platform-info") (r "^0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0ny4wdrig7mi9pi106hi7c22k3v60vddd9p9qcdkw6p702irb6yb")))

(define-public crate-uu_arch-0.0.6 (c (n "uu_arch") (v "0.0.6") (d (list (d (n "platform-info") (r "^0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0ghkz5apz7prdvwg98h9s6ykdjiff8xrl4cm2fd16r0yivhigdrr")))

(define-public crate-uu_arch-0.0.7 (c (n "uu_arch") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0y2i6hwjcvqdy0nww5hb5lkikad4ijdibp6968nrak0cp1ddql6l")))

(define-public crate-uu_arch-0.0.8 (c (n "uu_arch") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "09rxyvsqm829lsh2rmbpcgp3c5in54pp9m7jsclwx6rq3m2axypl")))

(define-public crate-uu_arch-0.0.9 (c (n "uu_arch") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0qncp8iwmavcixa8yakzmsjr90kkadpzb0hjka8v2n8s0g9gbiks")))

(define-public crate-uu_arch-0.0.12 (c (n "uu_arch") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "08x29iwy92f8z55ypgarf3sp2akssi22nlcnslaan5y41yxhi93s")))

(define-public crate-uu_arch-0.0.13 (c (n "uu_arch") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "13vm6nm1i37q5gqmf083bildhxa4ck68qmbgwi581p9ll0shzbks")))

(define-public crate-uu_arch-0.0.14 (c (n "uu_arch") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1n65pqcgv7bfc6hj7lnm30jdn9vgg23spcnas4yya6g9apilsc86")))

(define-public crate-uu_arch-0.0.15 (c (n "uu_arch") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^1.0.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0y3ylb1j9n6nvmlb5rbr4g08qbs3xy7h7wkgdpk2lg8xsf961zmv")))

(define-public crate-uu_arch-0.0.16 (c (n "uu_arch") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^1.0.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1hrffwm7haknvdm3iqc8w0y2yq5l9ci4hag5cd5h85ck1zz3hw7l")))

(define-public crate-uu_arch-0.0.17 (c (n "uu_arch") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^1.0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1406590v15l1dhxi6m15xx1dbzz69lkz6rg2jcs9l87ajkqap6hp")))

(define-public crate-uu_arch-0.0.18 (c (n "uu_arch") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^1.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0id7ynfd9hmc61qsqlvkbkfzw76d33m67a99qvy29z3hfcx2wg03")))

(define-public crate-uu_arch-0.0.19 (c (n "uu_arch") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "190n7xy9dhc4x4d43b2schznx1zjz2l2hks5zzpjf0hxvyasb6n8")))

(define-public crate-uu_arch-0.0.20 (c (n "uu_arch") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0fliczi2ik7i7xv093ak7jngwhfajl860vwkmxwhhrqffw76pnnx")))

(define-public crate-uu_arch-0.0.21 (c (n "uu_arch") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "012m86lbmwh4m7r8cvgdmiy85mjpdj9ijl5sbhfin9jiimj99sjy")))

(define-public crate-uu_arch-0.0.22 (c (n "uu_arch") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0qx8h3fpnrjx3gphgsqm73z6z3i1r55dhzjaysj4dqlnj6bfblr6")))

(define-public crate-uu_arch-0.0.23 (c (n "uu_arch") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "02parsazi25h3x1ryl7nwchv99bp9kh92ky7dhdjsn4bjxkagmil")))

(define-public crate-uu_arch-0.0.24 (c (n "uu_arch") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "06jr6qdxf44rcnbxryk4f8c5bvf0rx29yv9clcx1g31mzzcxh8n1")))

(define-public crate-uu_arch-0.0.25 (c (n "uu_arch") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0dwxpm62mp5l0mjlqvxfhlwi1h0gc4kadl17347ysyi27fv2vx8s")))

(define-public crate-uu_arch-0.0.26 (c (n "uu_arch") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1ljblly5qiq4qx0w7r4iqlannxwn4iixn79ar9v1fdf525ds6ppz")))

