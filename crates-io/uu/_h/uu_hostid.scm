(define-module (crates-io uu _h uu_hostid) #:use-module (crates-io))

(define-public crate-uu_hostid-0.0.1 (c (n "uu_hostid") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1jwznzkzaar4q152kpdswkg8n6c23m7s2m57dmdkqj33ck4av2id")))

(define-public crate-uu_hostid-0.0.2 (c (n "uu_hostid") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "17cs0p3nkdmxm5ns7wj4qfpd52hwk899iz9ck06qvvs5i0a7xrk8")))

(define-public crate-uu_hostid-0.0.3 (c (n "uu_hostid") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1bhl9ki8pj7qmbrk3q42qyhf68b01990hrv4s08yamza02w6nqjl")))

(define-public crate-uu_hostid-0.0.4 (c (n "uu_hostid") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0ygdcfcbyx4ia2xzn40xvyx6ic6hdqy6daf0lb17lq1xlvshczx9")))

(define-public crate-uu_hostid-0.0.5 (c (n "uu_hostid") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1iqzsmwpwsd9gwgida1vk3r1r0mifh57jg0sc5i23pn67fljmc8f")))

(define-public crate-uu_hostid-0.0.6 (c (n "uu_hostid") (v "0.0.6") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1xvmbrf8mxhqnbllpd8xak0pxwcbzj7c1j8ddn7iyzgwa9azpxxj")))

(define-public crate-uu_hostid-0.0.7 (c (n "uu_hostid") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0jjxddic0xw254ag5bn76vg0z6b5p31bwaiqyqrnbm11r58nqhzw")))

(define-public crate-uu_hostid-0.0.8 (c (n "uu_hostid") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0x8dipxgabvfcxfk9mfj409265mfvfx8fzjrbasa3851dnb7p8zb")))

(define-public crate-uu_hostid-0.0.9 (c (n "uu_hostid") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "04mm97dgrivhmp1k5dlpmpl6dyd1hx53fd0jd6avbnva6jz9iq8j")))

(define-public crate-uu_hostid-0.0.12 (c (n "uu_hostid") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0qm52r167kkjag20cd85dh5b79dmqgi0snvblm7q7594ghwm3g6y")))

(define-public crate-uu_hostid-0.0.13 (c (n "uu_hostid") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0hsfjfqxiibk8cs75dm1zvrfx7k8q2myzd8z2mkch5gbv2a3rd4r")))

(define-public crate-uu_hostid-0.0.14 (c (n "uu_hostid") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0c2j89xvdp2cml0mzv6ada8w83hgs7ip3mk86r6nzvlqgq9sknva")))

(define-public crate-uu_hostid-0.0.15 (c (n "uu_hostid") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1lkszk2nwnq14qznllz1mw8qn5rvm26y3hmw0054ra3mq70n6zah")))

(define-public crate-uu_hostid-0.0.16 (c (n "uu_hostid") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "18qi9nwg0k7anxd8ldw2bh4i1d32jcmflhk1xz9wkf201k68ww44")))

(define-public crate-uu_hostid-0.0.17 (c (n "uu_hostid") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "03hi401kfws7wvn465z664jf07yf1dxl1w0j8kpg0r18nzdv4vyi")))

(define-public crate-uu_hostid-0.0.18 (c (n "uu_hostid") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0gv7xzjgjgsd6cg7dz2mirv33lb4h0ryn2p2ckl4glgc924r56fl")))

(define-public crate-uu_hostid-0.0.19 (c (n "uu_hostid") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0k2p5qsirmf07v8bx9cyj7qg8zqd907gwh1s1fh8zja1ns7fz1l1")))

(define-public crate-uu_hostid-0.0.20 (c (n "uu_hostid") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "11wlpi2hg0hzb9glsmrlmkdibxw6l5dw4r64x4c0bxqvscv00kp7")))

(define-public crate-uu_hostid-0.0.21 (c (n "uu_hostid") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "073pxq3gzcrfaq6plk59xhzkrfqiaysmid1k60qd3wbg04nrz9jd")))

(define-public crate-uu_hostid-0.0.22 (c (n "uu_hostid") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1k0bhmahq5risyj3rq991yzymmn19d91i5zpipq3mvsajfs42msb")))

(define-public crate-uu_hostid-0.0.23 (c (n "uu_hostid") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1h0k3r8fki4ypdcbzxwgmzkfx4c8nn1vg6vsisrrd5hkrgx35l3j")))

(define-public crate-uu_hostid-0.0.24 (c (n "uu_hostid") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "05p2q415hj5f8xmky1a4c222g9mm3qv236wq36fgxfxn2vw2yqq7")))

(define-public crate-uu_hostid-0.0.25 (c (n "uu_hostid") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0xs659xbknl0xrzgkvqbms7cxgdzxa17dzj35i8vqn9858dhixd1")))

(define-public crate-uu_hostid-0.0.26 (c (n "uu_hostid") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1wfks9xjcsb4fpapdw3b9bv3c6z2cj5b5nc0qjz1770jm2cmlgfr")))

