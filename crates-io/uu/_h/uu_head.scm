(define-module (crates-io uu _h uu_head) #:use-module (crates-io))

(define-public crate-uu_head-0.0.1 (c (n "uu_head") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1gwlnp07wfwrmlji6waz39h4rzwni7fyalxb2rmd8pgrnk1yzr08")))

(define-public crate-uu_head-0.0.2 (c (n "uu_head") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1v5khisdwbc40b3i9byp90qiqp1x23psmaj6a031iks0y9ns43ym")))

(define-public crate-uu_head-0.0.3 (c (n "uu_head") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0yq43754w94ys77mjv6sbn00084n7l8bj7gy9psysfgmbw8xw1bs")))

(define-public crate-uu_head-0.0.4 (c (n "uu_head") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "11bnvnhf5gk4q92lj30fl2fsazqc83f7vrl1v09cnrqhybvkal72")))

(define-public crate-uu_head-0.0.5 (c (n "uu_head") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0ynbnx9drlgh6cm2l7h5bdwd54nwp4vlg32xpzcap410jix9nbcq")))

(define-public crate-uu_head-0.0.6 (c (n "uu_head") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "12xk28f2m7smvbp436v81gj4a3kmyq2fxpb0p131m9lppilws8gs")))

(define-public crate-uu_head-0.0.7 (c (n "uu_head") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("ringbuffer"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0948axm1pvqf6mrlfr7qfnj3v2is59dpflry7hnxmx3m61rd6mls")))

(define-public crate-uu_head-0.0.8 (c (n "uu_head") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("ringbuffer"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "00fjdrjq5ghd0k3k6ghlf6p1bmsjq34wy1hrifa9c1yc0jam66ms")))

(define-public crate-uu_head-0.0.9 (c (n "uu_head") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("ringbuffer"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "10ai26rwfgpkx7j11jp8w1hiv28ji9rmvdi1dcwk47n8jqz8hxjm")))

(define-public crate-uu_head-0.0.12 (c (n "uu_head") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("ringbuffer"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0f2n76lx8dmhrv7vwrjgn94k6ikqmgnkarxzpg14zmw6kn16da0m")))

(define-public crate-uu_head-0.0.13 (c (n "uu_head") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "0gbvn42844ws2qky3fbmkamvj3jnyxni53kv9bwmkv0416hbjsmv")))

(define-public crate-uu_head-0.0.14 (c (n "uu_head") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "0cdbnghw0zhy1zakk4hrsmxrh5m8fzzz495yasxdsfbxx4znky64")))

(define-public crate-uu_head-0.0.15 (c (n "uu_head") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "1rhnp4ywn53ggvchdww9lmdgyq3pgfka9drqmzarkcaizb7c2krs")))

(define-public crate-uu_head-0.0.16 (c (n "uu_head") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "110gdgxfxmvqa367pwz8adr8vgg0jbhzcrmlrpkill90hpyjxq1z")))

(define-public crate-uu_head-0.0.17 (c (n "uu_head") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "0sxw2w2cbg6865vmxfj43lirc4i7zyqxy4xylx5dblm7g8kw99qq")))

(define-public crate-uu_head-0.0.18 (c (n "uu_head") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "052ybxqkplvb4j09q5l8drj2lmpz1bwkn2c25qwl5bg2lg9dvj8q")))

(define-public crate-uu_head-0.0.19 (c (n "uu_head") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "1xcjlbw7swfdk3r819b85z1hxlq66lac0llr7f3wqznpq96ky2ii")))

(define-public crate-uu_head-0.0.20 (c (n "uu_head") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "00flhqhlijj0kfdzrbsrmsffny9q0mlzar2midlwzz14qikchv37")))

(define-public crate-uu_head-0.0.21 (c (n "uu_head") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "0701jggpjl4w90ynfxqvhgy3gkxhnxw6sc2agb7zwk97f6sj963a")))

(define-public crate-uu_head-0.0.22 (c (n "uu_head") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "0sal18an1y7x2i35l95lfrdlx682l2q83428h1fnhbfixsjdb8si")))

(define-public crate-uu_head-0.0.23 (c (n "uu_head") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "0043s7wxkr05a6mpgy60wx7hrvz50y17a9xz46zb3sfvnxc45p95")))

(define-public crate-uu_head-0.0.24 (c (n "uu_head") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ringbuffer" "lines"))) (d #t) (k 0) (p "uucore")))) (h "1c79qrarvfq3dxdjpwqg1pnx84xfb3gsw7h5ndw4l36zxgfvd3xy")))

(define-public crate-uu_head-0.0.25 (c (n "uu_head") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ringbuffer" "lines" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1h15jy497fvrfdijm7l9hpc8lpvw3gj3qkpcdnb393qj66abc8p0")))

(define-public crate-uu_head-0.0.26 (c (n "uu_head") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ringbuffer" "lines" "fs"))) (d #t) (k 0) (p "uucore")))) (h "0fhr8k9m881jaxnl92gjcq2r1a4mai30c983805pdxnjs9wc7aa4")))

