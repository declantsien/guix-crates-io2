(define-module (crates-io uu _m uu_mkdir) #:use-module (crates-io))

(define-public crate-uu_mkdir-0.0.1 (c (n "uu_mkdir") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1dck2znvf65rmmvcap18fpl2lgcid4dlxikv961b436z1qlihchy")))

(define-public crate-uu_mkdir-0.0.2 (c (n "uu_mkdir") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0pga9856amqnqgdmli8ca5wqhfl8mpmyqlgw7j1pbs5kn77cmri0")))

(define-public crate-uu_mkdir-0.0.3 (c (n "uu_mkdir") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "16i5dzzwybq4p7ynv5ajyy4dxphaxbfnwmsffizzpfrk8k3cdcgv")))

(define-public crate-uu_mkdir-0.0.4 (c (n "uu_mkdir") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1gg17ccks3djk1whx7qb3nppl0b2jgkhly79nfsaf2619hwiaqqc")))

(define-public crate-uu_mkdir-0.0.5 (c (n "uu_mkdir") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0105r20hrmyigv4ap0m8msrbrdy1kgv45bb4ziyg9haq4z6ykprf")))

(define-public crate-uu_mkdir-0.0.6 (c (n "uu_mkdir") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "14a113mxd3igcach2sdiha4marzcvs8idkd37vnj20iv0v43r1di")))

(define-public crate-uu_mkdir-0.0.7 (c (n "uu_mkdir") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1dz8ix8rqfxvjk44wnjsci60x4shxwc2gw3djpzhxya3628v89m9")))

(define-public crate-uu_mkdir-0.0.8 (c (n "uu_mkdir") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1laf22sgl3n4xmmbmw08f6paii06ppf45ddb4ns4vq3jslf3cbpj")))

(define-public crate-uu_mkdir-0.0.9 (c (n "uu_mkdir") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1r7i4rq4pbs31yaz4rl06i12mlkis9lkj9q1i6sv4dszwk8azpd6")))

(define-public crate-uu_mkdir-0.0.12 (c (n "uu_mkdir") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0j2hxs6ags7ca104mcznw8qx695kxxycli2ypql666n0zkk1c4wf")))

(define-public crate-uu_mkdir-0.0.13 (c (n "uu_mkdir") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "0b0wbs3lq1zxg0zwy3fzihdlynf95hfxangzlcza9mv5ihqd4339")))

(define-public crate-uu_mkdir-0.0.14 (c (n "uu_mkdir") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "1qs5sm3smhjznga4i7wjfvprj2wlp2b2srd5jiv06gzhgpqng0m6")))

(define-public crate-uu_mkdir-0.0.15 (c (n "uu_mkdir") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "1haqkgfrps00z8p3lcs4yyfjwlski2rxh147sv16mf2x433lrrc9")))

(define-public crate-uu_mkdir-0.0.16 (c (n "uu_mkdir") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "0k4jkpmgvwdnsp182qk6f1h7lgz59a872g5m7lwl5drskm4aa67a")))

(define-public crate-uu_mkdir-0.0.17 (c (n "uu_mkdir") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "1z99qy3zxl0pkq6zck51mc6dqd2wl5s93z3dfjsag2qn0dpa1shr")))

(define-public crate-uu_mkdir-0.0.18 (c (n "uu_mkdir") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "01wyszp8v2zi4ycxhi7j92c4jlxi7rf7khcwanj0dzk7chi92z19")))

(define-public crate-uu_mkdir-0.0.19 (c (n "uu_mkdir") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "0fh7bkcy0p4g3wq1xhids8lf2dg05vkh4swipb3yvr52bci78bj5")))

(define-public crate-uu_mkdir-0.0.20 (c (n "uu_mkdir") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "1hji18c7jfpll2f1qai1mwhqwqzb6q9bbbhdw1zh5s07f475fn53")))

(define-public crate-uu_mkdir-0.0.21 (c (n "uu_mkdir") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "0h78lzhj3w1w2kd7w8bns7a27d0yfj88wkm8nhjmkzvijr9njx5d")))

(define-public crate-uu_mkdir-0.0.22 (c (n "uu_mkdir") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "1ww85zcx867x2s99x8a67qp6vmhk22j0843hy1sw6kka0c59cxj7")))

(define-public crate-uu_mkdir-0.0.23 (c (n "uu_mkdir") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "0lf84gxjw1ii53xmlr4rnlgpqci5zw8scqf1qpmid3bkkry6bgyw")))

(define-public crate-uu_mkdir-0.0.24 (c (n "uu_mkdir") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "1cqwrslr2smsrs4dg9a1jbn01w3j5xzir86404qyivd1qdrknc4x")))

(define-public crate-uu_mkdir-0.0.25 (c (n "uu_mkdir") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "1bp4kkyqj1imbz746mrjvghzr2j6mjlhwyrq0mggdcin81ca82h4")))

(define-public crate-uu_mkdir-0.0.26 (c (n "uu_mkdir") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs" "mode"))) (d #t) (k 0) (p "uucore")))) (h "1hrnf8n6b31g19na336a2ci11wd6hkmij8569d14s7hjwgh9ava9")))

