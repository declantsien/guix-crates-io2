(define-module (crates-io uu _m uu_mv) #:use-module (crates-io))

(define-public crate-uu_mv-0.0.1 (c (n "uu_mv") (v "0.0.1") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0nlw9fkxzk3rv43z0flsgh88cv1j5305h9q5g6w0mjkh1zx732ws")))

(define-public crate-uu_mv-0.0.2 (c (n "uu_mv") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1d6r073512c7h8r4w5g91gjdgqd9gymhbc7kg43jcyvrk3xj2vwh")))

(define-public crate-uu_mv-0.0.3 (c (n "uu_mv") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "142y9djw9kmp8scb8skv1zddzzvx7npvpb7axf9f6dzldbdm654x")))

(define-public crate-uu_mv-0.0.4 (c (n "uu_mv") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "13bb21zkqypr2y7jvigkd9q2ngp17ywa8xvgba78f93frhy6qvcb")))

(define-public crate-uu_mv-0.0.5 (c (n "uu_mv") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "01irnc00n5x97ilgbyymcg1asz4j7wnkz2yykdil0ansb3sjnih9")))

(define-public crate-uu_mv-0.0.6 (c (n "uu_mv") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "10291rwyzpn5h791h8p1avcm9ccchsa5n9yj574xi37gnwyhpyh8")))

(define-public crate-uu_mv-0.0.7 (c (n "uu_mv") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0452vvsjx3dg1ih32lkdxxr1zfb5962w196fy6yjyschwcz8hrjz")))

(define-public crate-uu_mv-0.0.8 (c (n "uu_mv") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "17y96jaxzr60xm9nyavxilbd8b74jg10w4j61iy1z77rkgy7ac7s")))

(define-public crate-uu_mv-0.0.9 (c (n "uu_mv") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "05qw4np9hv7hjl5fci3g880dd7s8w64jcn2lmnv9d3lrapcyfmmc")))

(define-public crate-uu_mv-0.0.12 (c (n "uu_mv") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "06wqyfb64wqf3dwj48blivbm8y8gk8wshz9nj39m8h37qr0960wr")))

(define-public crate-uu_mv-0.0.13 (c (n "uu_mv") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1xan1yj2ka5h2pw8gnvicwrikmfdsqjyklzff2qbndc0zmyv9mr0")))

(define-public crate-uu_mv-0.0.14 (c (n "uu_mv") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0abffg02zjh6p7smp0ahh0m99syxp0ipgbjzpqkz1l2is5j4ydsx")))

(define-public crate-uu_mv-0.0.15 (c (n "uu_mv") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1d6hfn6s278bk3aamxmhhyxjcfklls7b02zcmigjqi5r74j7b0ph")))

(define-public crate-uu_mv-0.0.16 (c (n "uu_mv") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1jc4w3x4g0ad4hl5xmabb05dd57hcqn0222k5hi3jxssgb2swm8q")))

(define-public crate-uu_mv-0.0.17 (c (n "uu_mv") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "19dkwzzhpdxirzz3nb15z23qsq1jmh70y5jq3zkwxrna8gpdny6b")))

(define-public crate-uu_mv-0.0.18 (c (n "uu_mv") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "08aad7vb2q7ak39vx6rgxvzmig975zi5yx22wmm0nb6ngns5a004")))

(define-public crate-uu_mv-0.0.19 (c (n "uu_mv") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1dpj5yk14vkzg4nb3mfxsjmky3mxks02s2pzqnmh1jc3k65sz74l")))

(define-public crate-uu_mv-0.0.20 (c (n "uu_mv") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1likzn669qicmgnbg2mskvfw26j8xmfqfsml3907gijfybl8bmgr")))

(define-public crate-uu_mv-0.0.21 (c (n "uu_mv") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs" "update-control"))) (d #t) (k 0) (p "uucore")))) (h "09n00k1qyjr4xhyiasyk966lkdi3phnma2b5xaxnakpjjzx9h9lv")))

(define-public crate-uu_mv-0.0.22 (c (n "uu_mv") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs" "update-control"))) (d #t) (k 0) (p "uucore")))) (h "018m9lq1jjabwz3qcm5y332677k6lnyv28773pmsbqnsq5imdrlq")))

(define-public crate-uu_mv-0.0.23 (c (n "uu_mv") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs" "update-control"))) (d #t) (k 0) (p "uucore")))) (h "1z2cdcnf1q6zgzycimlbg0xyqh43v8x49vsr0sx1sbjdsdhpwc2y")))

(define-public crate-uu_mv-0.0.24 (c (n "uu_mv") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs" "fsxattr" "update-control"))) (d #t) (k 0) (p "uucore")))) (h "08qmdkxw3apa1yns5gi3iffi6jw18pgvyyb7f3cr23nvkq9p5i4f")))

(define-public crate-uu_mv-0.0.25 (c (n "uu_mv") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs" "fsxattr" "update-control"))) (d #t) (k 0) (p "uucore")))) (h "08ihjcpymzhkhmha26h54x4lp9nrwd5k2dr5r59mbs76fmygv68c")))

(define-public crate-uu_mv-0.0.26 (c (n "uu_mv") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs" "fsxattr" "update-control"))) (d #t) (k 0) (p "uucore")))) (h "16skqiy9gl532nhjfljs5kg6v8si7vg7d8z81v6c6nhwn040js2k")))

