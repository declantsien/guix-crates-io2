(define-module (crates-io uu _m uu_mkfifo) #:use-module (crates-io))

(define-public crate-uu_mkfifo-0.0.1 (c (n "uu_mkfifo") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "04zcl6dd6jdrdxwmi3lnlh925ipvv73czz1rw27ihyjwciq3m06a")))

(define-public crate-uu_mkfifo-0.0.2 (c (n "uu_mkfifo") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0jw537h3v6617axbkkwf95wnhdf17pks84i3a9c30krxx1hb1007")))

(define-public crate-uu_mkfifo-0.0.3 (c (n "uu_mkfifo") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1lkk922rpdcaf6jgpns78ak5b97sg44hjkkfx3iw5g68jainvs1k")))

(define-public crate-uu_mkfifo-0.0.4 (c (n "uu_mkfifo") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0zjbysn2r46f33w3sql12j47iplsialygpk9dhy44wfaf4hvrqjp")))

(define-public crate-uu_mkfifo-0.0.5 (c (n "uu_mkfifo") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "12vkjlhfmvar22dqsbiv475m2l8b5q3lx2a2pl8qyz1mdq2siglb")))

(define-public crate-uu_mkfifo-0.0.6 (c (n "uu_mkfifo") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "13a9xp55myd35lr7fr38vgqxl9iijw1z3448jbjp2kaqdlkiscfs")))

(define-public crate-uu_mkfifo-0.0.7 (c (n "uu_mkfifo") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "12svm0ymza5npvrm6yji7f9iyg6lj4w0swbfgcx6ihnrbrzznvzb")))

(define-public crate-uu_mkfifo-0.0.8 (c (n "uu_mkfifo") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0lasnzbxj8as92q402bw5dyyab0m5mrxhhfcss0fc0r7i9w6isp6")))

(define-public crate-uu_mkfifo-0.0.9 (c (n "uu_mkfifo") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "04i2cvvxvx8fblf97ci2qk9lr9hqclrz05iyb0wi517gd2nfkyj9")))

(define-public crate-uu_mkfifo-0.0.12 (c (n "uu_mkfifo") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1k2xw6m3g2dczb587sppanbfnq589znmqv115hpysqpym6a5cfgz")))

(define-public crate-uu_mkfifo-0.0.13 (c (n "uu_mkfifo") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "05b2hh9vh8mq46pqiscs1rrz8jf0zn47c0xxcrwmg4g402cbihkd")))

(define-public crate-uu_mkfifo-0.0.14 (c (n "uu_mkfifo") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "140zm1mybfa6f0smb28vnrdz9i08ddz4l2narlbhm5fpbz1sj01a")))

(define-public crate-uu_mkfifo-0.0.15 (c (n "uu_mkfifo") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "013q6lgg2i5bvwn5p1x1gnzs463p6brv43vgiln23327kw8fiyb2")))

(define-public crate-uu_mkfifo-0.0.16 (c (n "uu_mkfifo") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "14m91h3nykgjc3jj8jvylbjq99ci22dpgp74nhjw572h68rynymc")))

(define-public crate-uu_mkfifo-0.0.17 (c (n "uu_mkfifo") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "10j03pnjp68z170yymshvqssy9vfscbdindslgbdp5igj85lw35p")))

(define-public crate-uu_mkfifo-0.0.18 (c (n "uu_mkfifo") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0h0l2abjzcazpi0dmcm699sfv3zvc2hsqs2ym1kd7v33brvh5qly")))

(define-public crate-uu_mkfifo-0.0.19 (c (n "uu_mkfifo") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1bdm13vfr0f5fly6gmrca2pi88368a4asyq09g4k5i1b5i3vkipp")))

(define-public crate-uu_mkfifo-0.0.20 (c (n "uu_mkfifo") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1sidmrsqzwdj6wnl8rh4zb1d0bciflm04vhlmap4ncs0rnnqij6n")))

(define-public crate-uu_mkfifo-0.0.21 (c (n "uu_mkfifo") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1i0qwhbqbqh5kybaks1j9zn9c89dshc067wf0g8qyjfsmanbv48f")))

(define-public crate-uu_mkfifo-0.0.22 (c (n "uu_mkfifo") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0lwgvs1jpiq3kv0pmdpjkrhmdxxsjlp6575nlp81q3c45y46san4")))

(define-public crate-uu_mkfifo-0.0.23 (c (n "uu_mkfifo") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "076ix5gydcrdfawgr4drysdniqsvkgbyxq3fcid6dxqavyafw7ij")))

(define-public crate-uu_mkfifo-0.0.24 (c (n "uu_mkfifo") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0klqzbxz80gdwkmrp1wxzzxpgj9zfadngib5iml98qqr8hryqjhw")))

(define-public crate-uu_mkfifo-0.0.25 (c (n "uu_mkfifo") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "04f6lm8jl748fjbn5y61pd75m6psiind5rh1crxd4ib8h7gylw6w")))

(define-public crate-uu_mkfifo-0.0.26 (c (n "uu_mkfifo") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1biyaim3ll9qwavy4fgs2p4vz906y6jfrsazx7rxvp875jqqpss8")))

