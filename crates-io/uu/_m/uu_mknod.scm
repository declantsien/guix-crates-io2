(define-module (crates-io uu _m uu_mknod) #:use-module (crates-io))

(define-public crate-uu_mknod-0.0.1 (c (n "uu_mknod") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "042bsjgrzrbs5j71fzha4qr0jkky0gpix235l312bw6662sdwd98")))

(define-public crate-uu_mknod-0.0.2 (c (n "uu_mknod") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0gwqp1isxgw08mah010q4gp1zv5dp36y899lqkm1hvkl1klsvzwf")))

(define-public crate-uu_mknod-0.0.3 (c (n "uu_mknod") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0zn899jrzlzqq42lm0sx2nwsmmr55kg1d9dpck3dc5lv88g0hln7")))

(define-public crate-uu_mknod-0.0.4 (c (n "uu_mknod") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "15qxaiw23hgyq2kjl35mq1qlzhvgp4jbsl7p6zs23fjgyhdwx0hz")))

(define-public crate-uu_mknod-0.0.5 (c (n "uu_mknod") (v "0.0.5") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0zg94sm9pgggsv5jjaki2wnysq412k2cs59k0fnd5467j3rqyvgw")))

(define-public crate-uu_mknod-0.0.6 (c (n "uu_mknod") (v "0.0.6") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1h7rcf2l6b9qsgghkjlpr6b4icsmlqsmkw5f9xbil1jx4yrrjrs8")))

(define-public crate-uu_mknod-0.0.7 (c (n "uu_mknod") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0b3ypg99b7wvdpx381finrljnimk0dn41wdr25x1sqxi5d64lrv6")))

(define-public crate-uu_mknod-0.0.8 (c (n "uu_mknod") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1g9mav920v5i73x40ps9kspj0c17ysww30bipxacmp964bsicv32")))

(define-public crate-uu_mknod-0.0.9 (c (n "uu_mknod") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1fsjngdf85kafqsp512f741pm5w7qgw6isbg5bzij2syiqd8sra6")))

(define-public crate-uu_mknod-0.0.12 (c (n "uu_mknod") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0hxgf6cp2dl4d8zbyhilhxvsssmwqfgwgprmk3h04zwgyk5vh2di")))

(define-public crate-uu_mknod-0.0.13 (c (n "uu_mknod") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "1zb9gi6b5ra7m2f0za2312j3y57d3y6fzmzpp7rjyaxrzglsvf3z")))

(define-public crate-uu_mknod-0.0.14 (c (n "uu_mknod") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "1bihd5s06hwds1pag9wggdhywvs1286wpfacsqj9qka9x25gcafn")))

(define-public crate-uu_mknod-0.0.15 (c (n "uu_mknod") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "15rkl5f7p28fyf9zjcf0kgqmf6m1iivagacrid45n6c3vkvm3bb4")))

(define-public crate-uu_mknod-0.0.16 (c (n "uu_mknod") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "192lns28n90aflh5b92cdc52dz64h2i1jryp6bwgb5nj0ci7mijm")))

(define-public crate-uu_mknod-0.0.17 (c (n "uu_mknod") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "088b1n6di91aznja9gzvjplds71hf0ip9ywlcyc6fghmscnrbwi7")))

(define-public crate-uu_mknod-0.0.18 (c (n "uu_mknod") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "0qabc0bw6dsxqqsars6gm89dn26x87mhvna6iibxgzdygbf9mb2a")))

(define-public crate-uu_mknod-0.0.19 (c (n "uu_mknod") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "1hsmsipzbr59wll3gxh4afwbg03a7kb3gb5mzpxi88iwkpynszv1")))

(define-public crate-uu_mknod-0.0.20 (c (n "uu_mknod") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "1b6k0ppg90k74hprlqp8mzbr16dygq9dbqykgimymkvi4yv7ajfj")))

(define-public crate-uu_mknod-0.0.21 (c (n "uu_mknod") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "07h29nwhqcp2b3a2mcwbxf5yk31bh4al1m891zkz4pkj22h3ymdn")))

(define-public crate-uu_mknod-0.0.22 (c (n "uu_mknod") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "133wlm8q88wmrcbdv1wbvhd5is3zv658ss3axkzafi05maa2g6f2")))

(define-public crate-uu_mknod-0.0.23 (c (n "uu_mknod") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "1ncvd4h64yjllls4c125208is94ilfmp76dw16hkmp3cbphm942j")))

(define-public crate-uu_mknod-0.0.24 (c (n "uu_mknod") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "12w3nmg5snfk90c3f2dgzysfh9612nywad8kwzd1dnj0x5k8acpv")))

(define-public crate-uu_mknod-0.0.25 (c (n "uu_mknod") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "0yppcqvravkz3dfppfkyv4iv0snb21z9p98xi0smy5w38fkgxyw6")))

(define-public crate-uu_mknod-0.0.26 (c (n "uu_mknod") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("mode"))) (d #t) (k 0) (p "uucore")))) (h "0q75bdj6ccdckjg4mb60rq96pbxvd47khbq2z4y71fchnlibram8")))

