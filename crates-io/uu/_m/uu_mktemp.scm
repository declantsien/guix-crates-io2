(define-module (crates-io uu _m uu_mktemp) #:use-module (crates-io))

(define-public crate-uu_mktemp-0.0.1 (c (n "uu_mktemp") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.5") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0yq7f7y89ynp8f1hy728ma38nm13rak0d6q585aylqzrc0352apf")))

(define-public crate-uu_mktemp-0.0.2 (c (n "uu_mktemp") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0jnqp37gb4jn50jh89krrf8dhxx41lhmf6a3ps1c7gb1vg441049")))

(define-public crate-uu_mktemp-0.0.3 (c (n "uu_mktemp") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "16f5vzqd84yp4g4gydsn8idxah41bagfncb1m08gmp09yf2f9qw5")))

(define-public crate-uu_mktemp-0.0.4 (c (n "uu_mktemp") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "05dc33y5a40vj6af4b29ncjw9j90i721cs166bb526xfcnwcyj7x")))

(define-public crate-uu_mktemp-0.0.5 (c (n "uu_mktemp") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "142fm958dszyf1kjb5p8rmmma914r7p0yhf2fwbkc4dxj6wb6vg2")))

(define-public crate-uu_mktemp-0.0.6 (c (n "uu_mktemp") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0dbvfb41a7zxz94hhzmrkbvhbqn7lfw2lszkvj23wffhxqhgma0d")))

(define-public crate-uu_mktemp-0.0.7 (c (n "uu_mktemp") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "005ggmlyf4l2xh9dkd4bsxafaavy0mxhr7145aqqvhfcgzald019")))

(define-public crate-uu_mktemp-0.0.8 (c (n "uu_mktemp") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "09g37yrb8x6f7179kyy2r274f8smvqb16li0f8cq2qai2y1fs582")))

(define-public crate-uu_mktemp-0.0.9 (c (n "uu_mktemp") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "063bp05nzmizgsinqch9ss8dcjb8m44rfh67r21jhrsnlnxy2cv4")))

(define-public crate-uu_mktemp-0.0.12 (c (n "uu_mktemp") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "18g55k5rcl1xvkm06s7r1g7vn9qqs3gk3m5brdkbxa34kkwdmazw")))

(define-public crate-uu_mktemp-0.0.13 (c (n "uu_mktemp") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1n0gnnxp0hma4nq67gzk2qwaa7q3q2636dfwjai36i9sfrxw7chm")))

(define-public crate-uu_mktemp-0.0.14 (c (n "uu_mktemp") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0nf2zmnp7iahl8f5vc53za1h64q59d0ilxnb9pjwwvq0l6h25vk7")))

(define-public crate-uu_mktemp-0.0.15 (c (n "uu_mktemp") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1k3xd0pgm3392js17vddars6546wzlwxgzdnisp3c0za37b6w0vx")))

(define-public crate-uu_mktemp-0.0.16 (c (n "uu_mktemp") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "0wyr7ifmddsl9l9a1wk959ahnpjdd3vpqgnq8rrilabg6s78arg1")))

(define-public crate-uu_mktemp-0.0.17 (c (n "uu_mktemp") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "0wf0f8spjj5ka2k91clx2hj54vbvymc39i98jm7ijvn692y059zi")))

(define-public crate-uu_mktemp-0.0.18 (c (n "uu_mktemp") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0zf147fsl5549kg33ixxfg2mr7aa018hdj36dbd3q5b6j7cxm3mk")))

(define-public crate-uu_mktemp-0.0.19 (c (n "uu_mktemp") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1a2azfs4w8xcf44m5bwcdmj4cxyxql1ja76ck5bp3f8ikbq5y8zi")))

(define-public crate-uu_mktemp-0.0.20 (c (n "uu_mktemp") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1xygzp3xlfcpn6chlc9j0asjkv10hgwsnq766rmhpbahlr73s9db")))

(define-public crate-uu_mktemp-0.0.21 (c (n "uu_mktemp") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "04isrr6lmwk90c705bsfyxds8afpiygf6gw4zgdzp3ygdx94fl8k")))

(define-public crate-uu_mktemp-0.0.22 (c (n "uu_mktemp") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "03fz7mjmpvycdc67qychb21pv97pc62g30r927j8qsg881q57ma1")))

(define-public crate-uu_mktemp-0.0.23 (c (n "uu_mktemp") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1jrik3nawxcxxqbhlzjl9sb8xwwxmmb5lm7rkdi61hcyilh32i8m")))

(define-public crate-uu_mktemp-0.0.24 (c (n "uu_mktemp") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0j93smp4l3nx2d6hwwil0lyhfwn7sx4w001ldn1rfqn9504mpqn7")))

(define-public crate-uu_mktemp-0.0.25 (c (n "uu_mktemp") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0mxwqf62860gk9l64qjpdfcqcp540lvc966ifhw1as6p6sfajh7j")))

(define-public crate-uu_mktemp-0.0.26 (c (n "uu_mktemp") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0lw62lj347psf8nl026hvyp93vfdp9cvjw1zy7m8rhmx8jbhv2m2")))

