(define-module (crates-io uu _p uu_printf) #:use-module (crates-io))

(define-public crate-uu_printf-0.0.1 (c (n "uu_printf") (v "0.0.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0jnykk3j97zdprbnh54wb852w1jwr6vylzqbgsdkcsxq44di590h")))

(define-public crate-uu_printf-0.0.2 (c (n "uu_printf") (v "0.0.2") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1641zgq6awnrsjjkkx9j4yis80i29cmd67v0jcg8clm6phhpghk0")))

(define-public crate-uu_printf-0.0.3 (c (n "uu_printf") (v "0.0.3") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0njlhh2pv7d03p95qcx7w22vq7aipd1xdjyxwpkdl7gzcqk1arz2")))

(define-public crate-uu_printf-0.0.4 (c (n "uu_printf") (v "0.0.4") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0c661b2mfjidipkhxrx4c03bs4x4i6wi56i2amrj60jk1spgpf2j")))

(define-public crate-uu_printf-0.0.5 (c (n "uu_printf") (v "0.0.5") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "05qk8v45f98f7as5f4gvw1dbil35if9f8163fclpp6d9byj9c8b1")))

(define-public crate-uu_printf-0.0.6 (c (n "uu_printf") (v "0.0.6") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1ag59hzl0hsyc5flv6h4xid54s87m70v0pqv0g8vgknvkxnka6qz")))

(define-public crate-uu_printf-0.0.7 (c (n "uu_printf") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "16kqjndz12jcwmjfql6qnx777mckwgmsq7fn1f753sd0f5gsw0gf")))

(define-public crate-uu_printf-0.0.8 (c (n "uu_printf") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0gk96ymsq32mcdy8ayh8isz2ki2wj3ldgqwmskij8km68abrbsdi")))

(define-public crate-uu_printf-0.0.9 (c (n "uu_printf") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "170273zlaxrmvsch3zin2fjhcmlyhfhj2x0c8p86vmrx9pfgfzaa")))

(define-public crate-uu_printf-0.0.12 (c (n "uu_printf") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "04d5n0v5ca71wfiy4lz9zd8abm6585l36nvgfirhbq1py3ad6vlc")))

(define-public crate-uu_printf-0.0.13 (c (n "uu_printf") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "1bv0y129jzap5vr1rv5srbyy718j981c97zkxqb60kar6bjw81bb")))

(define-public crate-uu_printf-0.0.14 (c (n "uu_printf") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "0a0wna66mxhy67zigm0186ppx3va9pyv97aadl3z7c6n3sjinfg6")))

(define-public crate-uu_printf-0.0.15 (c (n "uu_printf") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "0zkzlsqqfd1bvqfzcxlinpyv87dmsb0yd5sjll425h9zbi0mlj2p")))

(define-public crate-uu_printf-0.0.16 (c (n "uu_printf") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "12299blaanp64f6ay195nr7b8v4faf3w04p3vhzyww56bzxi4rvi")))

(define-public crate-uu_printf-0.0.17 (c (n "uu_printf") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "02wnz46wmql707xymxm9rh2ai39iddywby2pvvzz4k3s1xsjgnkw")))

(define-public crate-uu_printf-0.0.18 (c (n "uu_printf") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "03d53mk1mlkgjxfk476asbf21r2g19vnv2znbc6iirxdi50c1xx3")))

(define-public crate-uu_printf-0.0.19 (c (n "uu_printf") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "06f1nz01zlxghsywv0xcj637r6ls1pi60s0vj4z3sz7rhvf3p67q")))

(define-public crate-uu_printf-0.0.20 (c (n "uu_printf") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "0w8znyk951h5laxqfr8q9196zg8q1b3s67idd9bs10dhzkmghbxd")))

(define-public crate-uu_printf-0.0.21 (c (n "uu_printf") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "08daxfdijilkyx674ihpbai7f1dhz50vmhjxzjilqpc6n4nv8jhb")))

(define-public crate-uu_printf-0.0.22 (c (n "uu_printf") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "1sflfh4rkqxfm0rdzyaabg58an7ay0bvl297mz3fllxqafb0xj5g")))

(define-public crate-uu_printf-0.0.23 (c (n "uu_printf") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("memo" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "00pd5c0yhkigi10lsvr28ynkn94qbqfnnspwnax9i22zsg4wvy4d")))

(define-public crate-uu_printf-0.0.24 (c (n "uu_printf") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("format" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "0macxibqs4g90h4prv1yr3chknhy3grw23hcw2249ggqx7v98dvk")))

(define-public crate-uu_printf-0.0.25 (c (n "uu_printf") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("format" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "19vrk01272k2lfsr0xhpqjafb9qm63qvmdh59zw8bjc1i0wkd75d")))

(define-public crate-uu_printf-0.0.26 (c (n "uu_printf") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("format" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "07cvrhln09qz4iqrdg6pw517ssws7i2lvq27sxm5w98cvdjbj220")))

