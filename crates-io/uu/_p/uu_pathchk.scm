(define-module (crates-io uu _p uu_pathchk) #:use-module (crates-io))

(define-public crate-uu_pathchk-0.0.1 (c (n "uu_pathchk") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1202xa2a9jlfm2py17vhyrdfbwdkhwsssmiwymjb4ky6jj1sn6m9")))

(define-public crate-uu_pathchk-0.0.2 (c (n "uu_pathchk") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "14gj7dn7jz4dic70g89cql97ycs2ag52d0bnqs9r9h3sf48in7j5")))

(define-public crate-uu_pathchk-0.0.3 (c (n "uu_pathchk") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1hf2xim8ajbhj5wjp8s1jl6jr7r8rx4hyxhi4mkcacz9zkj9bvms")))

(define-public crate-uu_pathchk-0.0.4 (c (n "uu_pathchk") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0kcpmv04rx35mz2jwnjs5ksdcf85v5799d67qd5jbih742zz9yk3")))

(define-public crate-uu_pathchk-0.0.5 (c (n "uu_pathchk") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "08r84lkv7z3nqiyqnciyjjr1svyh12p63l8z76dvlhk4qkg42nxa")))

(define-public crate-uu_pathchk-0.0.6 (c (n "uu_pathchk") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0nl9nfpx50pi35y2mihn19sj465n8a970syxm4s1rvz085nskz75")))

(define-public crate-uu_pathchk-0.0.7 (c (n "uu_pathchk") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "02rsy5g84vlzw0nqy75gwssra24q6c2cjcfyq58mgcnn2s45hc0x")))

(define-public crate-uu_pathchk-0.0.8 (c (n "uu_pathchk") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1n0bszx5l1bsdwky1r7iwfwf5afba97hb1cdhpgb6li3k8axb5ps")))

(define-public crate-uu_pathchk-0.0.9 (c (n "uu_pathchk") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0g274cib5js209qv1955gfybqriy4ad3718k4xpnqgqjf404j1wi")))

(define-public crate-uu_pathchk-0.0.12 (c (n "uu_pathchk") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1j34ig3p8lnr4zd4j39hs75b2yx2fcf8s35vi3c36y6afkbc47wp")))

(define-public crate-uu_pathchk-0.0.13 (c (n "uu_pathchk") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "14pc5jbbkyk4ld4x63vc7jj91aqpan5j9pa9jqs04vrqh7sbyid8")))

(define-public crate-uu_pathchk-0.0.14 (c (n "uu_pathchk") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "03b4289hbsgkb7frj9119lxzh0a0ijnwrvs0jyd6nabmw3561n4k")))

(define-public crate-uu_pathchk-0.0.15 (c (n "uu_pathchk") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0hgw5skhxj4c3c7fzs2wz5azq94fj3r33qilmxi5yamgjm9vs2x6")))

(define-public crate-uu_pathchk-0.0.16 (c (n "uu_pathchk") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "10ym0kpzlha6achy70q6ww1mqy3ycfhbq21nm1xaii0m67p8i2di")))

(define-public crate-uu_pathchk-0.0.17 (c (n "uu_pathchk") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "0y1lcncm1phz0n2d4h6ppbk6rkzbz4sv173xfqdl36zlz4hizwz6")))

(define-public crate-uu_pathchk-0.0.18 (c (n "uu_pathchk") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1a127w0wigd52b5562nl3qahxfdzsbwinlb7rg2s8sk4lc2ykm4m")))

(define-public crate-uu_pathchk-0.0.19 (c (n "uu_pathchk") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0cyaw31df9fkr6wip9dixw5h6340xf0nmib3dyddm32gvjmfzji1")))

(define-public crate-uu_pathchk-0.0.20 (c (n "uu_pathchk") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "04j6w8phv8rgaazqk3bbqz661avndcnm7rc7f8yd9zc5k4h90xdg")))

(define-public crate-uu_pathchk-0.0.21 (c (n "uu_pathchk") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0gkal5kb8vlnsrhxci5786bwb2iz599r1g4w34zi5jcq06hc92bw")))

(define-public crate-uu_pathchk-0.0.22 (c (n "uu_pathchk") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0zq0w8wwqpxhjbjdhl5w7l04af9qal7f43g5bjv624k96iqgfqv8")))

(define-public crate-uu_pathchk-0.0.23 (c (n "uu_pathchk") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0lr975cmzdpl8wjlicxx6jr8qjc9vis28d44yv6b17ymmdgfqsvk")))

(define-public crate-uu_pathchk-0.0.24 (c (n "uu_pathchk") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0xxsirj3bw57r5bxl8hfvf3ibykxsiagk5x1i5jz490fdbi4zp9b")))

(define-public crate-uu_pathchk-0.0.25 (c (n "uu_pathchk") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "06c1znig3964wrv1nvp4gcbwxz34m45392a8vl2h4yxv0lzdhgx1")))

(define-public crate-uu_pathchk-0.0.26 (c (n "uu_pathchk") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "150736lray91csgi6sidqa0hpkysar7cvnfgmh3wvcwmvia743ni")))

