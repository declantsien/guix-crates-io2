(define-module (crates-io uu _p uu_ptx) #:use-module (crates-io))

(define-public crate-uu_ptx-0.0.1 (c (n "uu_ptx") (v "0.0.1") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.7") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "10r0gnq2wgmbniyq47ng8w4arj4igcyh5z114yxpw8q10n30f62q")))

(define-public crate-uu_ptx-0.0.2 (c (n "uu_ptx") (v "0.0.2") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1jh8jjyy3fmalm27j04j7y9kwg273iq4mbamswxs945vxcnwb1yp")))

(define-public crate-uu_ptx-0.0.3 (c (n "uu_ptx") (v "0.0.3") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "08khfihshp0c72vad73j98g5j6blhra3m7v75984i8jblb9lqahw")))

(define-public crate-uu_ptx-0.0.4 (c (n "uu_ptx") (v "0.0.4") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0lssgmx6j3jm9c8qfwj8v7jgjmq8nf5fp3c5cw1ap0vqzdq7h9ig")))

(define-public crate-uu_ptx-0.0.5 (c (n "uu_ptx") (v "0.0.5") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "09ydxp8ggzls260bwq0dkp2qdwf2i5rq2jjljvkgcfyr7dw21m2a")))

(define-public crate-uu_ptx-0.0.6 (c (n "uu_ptx") (v "0.0.6") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "11z281982g606c1dwyz3r9735d8sa243f3yh145hr1wf9ykilr4y")))

(define-public crate-uu_ptx-0.0.7 (c (n "uu_ptx") (v "0.0.7") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1568dmk01dhhdhh459764f3g8hxh5g2vhxm5fvqw0rjh691aa2qb")))

(define-public crate-uu_ptx-0.0.8 (c (n "uu_ptx") (v "0.0.8") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "166w00w6i5b8xrinbjb2c9mp45n7n78pd9h0g5scw9v21x1gmz29")))

(define-public crate-uu_ptx-0.0.9 (c (n "uu_ptx") (v "0.0.9") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "01ascbvc29mx5617484p6i2zd3zq4zx9imck793i9dnlvnczg3p7")))

(define-public crate-uu_ptx-0.0.12 (c (n "uu_ptx") (v "0.0.12") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0c3irsbcbwl61rkssbfcw7bbv1fxrgnr94b8hybw0r9dc9ydbpp7")))

(define-public crate-uu_ptx-0.0.13 (c (n "uu_ptx") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "11yn5qqsd1vhrsn5sgcrb2v8rdksqg9zcppa1wcvlnm409ggfmly")))

(define-public crate-uu_ptx-0.0.14 (c (n "uu_ptx") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0pyv7kyl2zlizkj9qlfdpr7wsmvn0sgcqq83zsafp40nsvpvm09k")))

(define-public crate-uu_ptx-0.0.15 (c (n "uu_ptx") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0f2mbsmv1pd3cymxvzf13r6s042yf1fq6qbcwdlllawhkjm19rfn")))

(define-public crate-uu_ptx-0.0.16 (c (n "uu_ptx") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1y0hycp61fvhmrbcz25il1wqqp0fl5rddx8q8q7gpckbl4mwj9ck")))

(define-public crate-uu_ptx-0.0.17 (c (n "uu_ptx") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1k1gvn16bs7ayj57vl8x3l21zjnb16d7ajz8c2z6nprvj6yxl8n9")))

(define-public crate-uu_ptx-0.0.18 (c (n "uu_ptx") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0yp4rngqla5yn0hay49ggihhp9l432y35kr6i5a16qyirki7ymbl")))

(define-public crate-uu_ptx-0.0.19 (c (n "uu_ptx") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1wkkjifg58iqn620r9aji0z723v627kb2ppdbj468miqqs7xhzf8")))

(define-public crate-uu_ptx-0.0.20 (c (n "uu_ptx") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1lvai71sq311zyawi3ffh2fjcklas3l7dmii654c1f0j3ps9lj7r")))

(define-public crate-uu_ptx-0.0.21 (c (n "uu_ptx") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1ysarl3flsvmp725qya068rfp3pp5yji6r9i6bxfkygjkky098rj")))

(define-public crate-uu_ptx-0.0.22 (c (n "uu_ptx") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "04k6lkrj6ycfbg6f0q4kqr33ps0585i35cmfap59mx3zhjsrjrpp")))

(define-public crate-uu_ptx-0.0.23 (c (n "uu_ptx") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1qisrbplfhb92h7n33v9bhnyszzyx5l68m0pjr7rkqq1jnhqsn3f")))

(define-public crate-uu_ptx-0.0.24 (c (n "uu_ptx") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0ivpwdk4qdcc3lpw4wzvznwgnidhkx0w5caiyv0180lw2vhjnajk")))

(define-public crate-uu_ptx-0.0.25 (c (n "uu_ptx") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1zanbxlf964658jja9d4ija1i1yvnmlmsrvnvi9g0bjdvf1xrzfx")))

(define-public crate-uu_ptx-0.0.26 (c (n "uu_ptx") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0xsi827lcs56dky1pqccacs19qz4awvacrmpjiaczcx3dyh5kzwr")))

