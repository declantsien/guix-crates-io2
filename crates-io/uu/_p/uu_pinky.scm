(define-module (crates-io uu _p uu_pinky) #:use-module (crates-io))

(define-public crate-uu_pinky-0.0.1 (c (n "uu_pinky") (v "0.0.1") (d (list (d (n "uucore") (r "^0.0.4") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0g4761zzhsl96xb28fhyxj7lzv1garpyr83pvr33gb02la2ms5ba")))

(define-public crate-uu_pinky-0.0.2 (c (n "uu_pinky") (v "0.0.2") (d (list (d (n "uucore") (r ">=0.0.5") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1cx0qnxrdhqzkw06kabpbm7br70lw465vpfhglkvmpvwaiyn0r1r")))

(define-public crate-uu_pinky-0.0.3 (c (n "uu_pinky") (v "0.0.3") (d (list (d (n "uucore") (r ">=0.0.6") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0xc7pis4vccnlnli4rx4s0bbzq4ni7rcbi9y1h042ahbhxgf015h")))

(define-public crate-uu_pinky-0.0.4 (c (n "uu_pinky") (v "0.0.4") (d (list (d (n "uucore") (r ">=0.0.7") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1qlzqfrsxac7yhp8jarijnanrqyzfhqwbnbbn3x7kfsb45n02ryi")))

(define-public crate-uu_pinky-0.0.5 (c (n "uu_pinky") (v "0.0.5") (d (list (d (n "uucore") (r ">=0.0.8") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0ylwb5y919zb7jy58xazjwlwgd5am0mzdal425qafjhz26nya32y")))

(define-public crate-uu_pinky-0.0.6 (c (n "uu_pinky") (v "0.0.6") (d (list (d (n "uucore") (r ">=0.0.8") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "00dyaa263p1xspy6rh2zqi96g5hrs0qy75cxd4kffcvk8p50d560")))

(define-public crate-uu_pinky-0.0.7 (c (n "uu_pinky") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0hc3ys3ypfyjfw12wa9gqm531bnm7fzazf9xad5xvikmxq524hxi")))

(define-public crate-uu_pinky-0.0.8 (c (n "uu_pinky") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "00ba333pd6b4nhn1a8bsl9f9wqqs2nzjcaqszw9vf4hn8dl6wl95")))

(define-public crate-uu_pinky-0.0.9 (c (n "uu_pinky") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1z24c2gdcpvmdglla34ray3bs954n0v15ns3radwv4j1q4k4n521")))

(define-public crate-uu_pinky-0.0.12 (c (n "uu_pinky") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1x7iib6n9rxhj3vza3542j6dzpkzzzarp92lsw3k1prk2155nlq1")))

(define-public crate-uu_pinky-0.0.13 (c (n "uu_pinky") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "0wllx7861yhsljkz7617sffa7niv7pkhm47r74cl55jba7d7j8w0")))

(define-public crate-uu_pinky-0.0.14 (c (n "uu_pinky") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "1hlncw07yk7hw3k5msixm1wwc4xz8w5iln8qy5m9g23ykb97li50")))

(define-public crate-uu_pinky-0.0.15 (c (n "uu_pinky") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "040mmic4rgfjpgc1gdw79yxr338b84askawiixciky7vy9k9c5zi")))

(define-public crate-uu_pinky-0.0.16 (c (n "uu_pinky") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "0b38bs7i272qsfgmz1ajyd008jz7v19c02z77gzbmzhlbwib4w7c")))

(define-public crate-uu_pinky-0.0.17 (c (n "uu_pinky") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "1zkkzhd0jfg19l9782jlgp1wdxl0fh8vh2qqjgyh6mf80hg4ldfz")))

(define-public crate-uu_pinky-0.0.18 (c (n "uu_pinky") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "1d9035zfp6r0ylqmpq0frll6mpcdk02ds54p3qqpjnsbvjkyalvk")))

(define-public crate-uu_pinky-0.0.19 (c (n "uu_pinky") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "16ryg7nqyvxpvkfasqdm40laisal1r3390wr57kb393087pnxbmw")))

(define-public crate-uu_pinky-0.0.20 (c (n "uu_pinky") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "1cwbj9dkqspv7bf6hhrsv9rvm96r5flkaqapmi1zd4lya1lq2dc7")))

(define-public crate-uu_pinky-0.0.21 (c (n "uu_pinky") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "06fxwrg2lhmxf1r5x89zyda4pcpq535csxk0cxirbdq7nd7v8csh")))

(define-public crate-uu_pinky-0.0.22 (c (n "uu_pinky") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "1c6izgx66zhrmwrfxnqla6s9rak4kxlcdrvlfzv0q6rw0nb3bx18")))

(define-public crate-uu_pinky-0.0.23 (c (n "uu_pinky") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "0rhv9idyzg4x47az3k9g7g275g9ihnsrhmb6q771m5drv2iqd56w")))

(define-public crate-uu_pinky-0.0.24 (c (n "uu_pinky") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "1bwnlnsf452srx8nj2q8bgi8jrfg7yvci1j29797shrnwvwyivfm")))

(define-public crate-uu_pinky-0.0.25 (c (n "uu_pinky") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "16xmjq0zh141mg8448ibiwcrcid5kpl9a07p9dm96rcprjyly6hj")))

(define-public crate-uu_pinky-0.0.26 (c (n "uu_pinky") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx" "entries"))) (d #t) (k 0) (p "uucore")))) (h "1s92p9kkiywvynnd5yn9f6a1kdlhziyspnm5nz3m5qbf779d5l14")))

