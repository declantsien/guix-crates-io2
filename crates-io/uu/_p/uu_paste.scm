(define-module (crates-io uu _p uu_paste) #:use-module (crates-io))

(define-public crate-uu_paste-0.0.1 (c (n "uu_paste") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1218hlm0ll1zkhcwvijjb9qwbcsp46sly25s9mdpfa76ki3afbm9")))

(define-public crate-uu_paste-0.0.2 (c (n "uu_paste") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1c1msmbfqvkdnvaglccbwvx1ynrzawvykpbd075b1cx012l59x7n")))

(define-public crate-uu_paste-0.0.3 (c (n "uu_paste") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1rd7v94qaa91k5mlp26wr4q82l4xj5qxlas23fq5k63m43svb0w2")))

(define-public crate-uu_paste-0.0.4 (c (n "uu_paste") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1ylm705kdr404wapzj7mdbfqdbhj29q30pahdkkbp59gfn7g2hnh")))

(define-public crate-uu_paste-0.0.5 (c (n "uu_paste") (v "0.0.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "00882x3b52l8scawjbaif0fqnh5xwah3raxn4pdysrffcrislmmm")))

(define-public crate-uu_paste-0.0.6 (c (n "uu_paste") (v "0.0.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0yb7h4qq5886v94x31wf4786fbqw7pcq9ji7j6pqdhabh8y8jv77")))

(define-public crate-uu_paste-0.0.7 (c (n "uu_paste") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1fcd59r76pvaby4r0hd9q8kd2mjvzmgzxd42wsl3pr4nxmw3b922")))

(define-public crate-uu_paste-0.0.8 (c (n "uu_paste") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1id9y1pna5abwsf3ig9dklasi3ak2n6lb6qj65k3x5zvv4yg91yi")))

(define-public crate-uu_paste-0.0.9 (c (n "uu_paste") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1119ffg5l7s8j40nsadg20ab0gb67gnwxmcx14rvla7bwvnlnpks")))

(define-public crate-uu_paste-0.0.12 (c (n "uu_paste") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1mhppmw186xqdbrgqsal31rwr0ynn532a8wlm4p5b2nlqwrxgqy3")))

(define-public crate-uu_paste-0.0.13 (c (n "uu_paste") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1l1cwq1404v0hv5w5slwz0vwwdrqrzvljd2si054ykjvcq9j3k4b")))

(define-public crate-uu_paste-0.0.14 (c (n "uu_paste") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "174xn83wmgj4zkcl9qliplna9by14x7mlh0n9g51awi7icv448zh")))

(define-public crate-uu_paste-0.0.15 (c (n "uu_paste") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1vl1ax8wvn4rmmpggg8xp9494pkwib7ff5n114fp3w9cx8pmshc7")))

(define-public crate-uu_paste-0.0.16 (c (n "uu_paste") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "02x4q7pbhcnsb4ksgbv3m3g7hqhfm5d2w1j952izk63xvb2b4pk4")))

(define-public crate-uu_paste-0.0.17 (c (n "uu_paste") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "00445rca1v0lnqab26d10gpm6wgzgw7di7f49r96iqa1s8hgny2h")))

(define-public crate-uu_paste-0.0.18 (c (n "uu_paste") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1ww9sy1czw6ywslyi25c8rhj878fix73h9qa4r144fwv3bzy4hnd")))

(define-public crate-uu_paste-0.0.19 (c (n "uu_paste") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0na09aflnka739cxf52ms791rgpjcg5pa6i82lqhfvgrzyx72mkz")))

(define-public crate-uu_paste-0.0.20 (c (n "uu_paste") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "10s7wra4gs8ww6ghzr9vs3wms43wvfb5r6g7x33h14rdxffn5xxy")))

(define-public crate-uu_paste-0.0.21 (c (n "uu_paste") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "12041jc5c363hq33x8n9f9crj5anchpf07p0f8491vqp9wi4z68i")))

(define-public crate-uu_paste-0.0.22 (c (n "uu_paste") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "17y5iiikpx50afbn836pkfvhwa6vm6189i8m717xhxxw8wxfv2df")))

(define-public crate-uu_paste-0.0.23 (c (n "uu_paste") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1cyx7rn27vp7a8p0n860jjm8fdwr91v3fm6jsiz364mjpjp0ffw7")))

(define-public crate-uu_paste-0.0.24 (c (n "uu_paste") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1vv8m9sxqvhpz46ysh3vx26hagiw2s4nw6ilamn0xmgy1r70wwsn")))

(define-public crate-uu_paste-0.0.25 (c (n "uu_paste") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1v7vdfrx11cilb4sif59p608mz505z686pjd9n4ds3hlfraqkl0z")))

(define-public crate-uu_paste-0.0.26 (c (n "uu_paste") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1wgfa953f8v2sn8a8ap2a460nqifgci0nlsahwwhmp870rmwlk22")))

