(define-module (crates-io uu _p uu_pwd) #:use-module (crates-io))

(define-public crate-uu_pwd-0.0.1 (c (n "uu_pwd") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "009fmq8s8lznkp6n6k0rnhlnjd0rw8s9fn86y84i0bkg6igd98k0")))

(define-public crate-uu_pwd-0.0.2 (c (n "uu_pwd") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0dw9x4s6vjixy0hphi96pz3yl9m8d62vdq75gfcsk84hvva42fsd")))

(define-public crate-uu_pwd-0.0.3 (c (n "uu_pwd") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0psd0qzkm26y1q7jbfk0ad3261vn6pl2a65bbm0z6ccxc8disgsx")))

(define-public crate-uu_pwd-0.0.4 (c (n "uu_pwd") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "12z8bswr459f57jria0d5rsmpg4ks0yx0pknjr8j5x59z4jm3v0s")))

(define-public crate-uu_pwd-0.0.5 (c (n "uu_pwd") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "02adhsaialmdz6xhfgvg34qafz1m9zngh0l78hbbmr3zislssgh1")))

(define-public crate-uu_pwd-0.0.6 (c (n "uu_pwd") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1mw58nwbr8fj69a8rb53gb4wvqqd7wia1dg1q5kf864pyz9yhljh")))

(define-public crate-uu_pwd-0.0.7 (c (n "uu_pwd") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1aq4xd66mnnzi7lp83sgw3q9wcbk229v73zqarnv4ch4qjvydk9q")))

(define-public crate-uu_pwd-0.0.8 (c (n "uu_pwd") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "04pa4j1yx81l67fldkzvyw8j281nivzs2g1zxsr676lcqj8wbfv7")))

(define-public crate-uu_pwd-0.0.9 (c (n "uu_pwd") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1bm7ll54sh69bnhl2xb5sykkagcw42bkbq6f4ydkg7cgzam4khzc")))

(define-public crate-uu_pwd-0.0.12 (c (n "uu_pwd") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "149w4ikd9gparqb57bgyd6i2ppvz4pbvm3dxsgv4v2kbgshyx4vr")))

(define-public crate-uu_pwd-0.0.13 (c (n "uu_pwd") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1rf5g2ayjnrnqsngqg5am8sdawxs23gjcqjq83nxh7aldwa1w821")))

(define-public crate-uu_pwd-0.0.14 (c (n "uu_pwd") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1wnyi1m3sg6v4f0ar80sygxz2ymnlvriqawa3vrpjrdzw329llfg")))

(define-public crate-uu_pwd-0.0.15 (c (n "uu_pwd") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0zyxiqdgr8q9rmwq60lj8zwbxjjj3j0nyrb7kjkg8h3yp1w4kigx")))

(define-public crate-uu_pwd-0.0.16 (c (n "uu_pwd") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1xaxs3jgd3cfxcvwd8w7k1ybvf71ks33k8vfvi5flq71i8lhsq5b")))

(define-public crate-uu_pwd-0.0.17 (c (n "uu_pwd") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1k55wjqxxibb5g4b5sj28gb4z07d3jhzgpp5v76a91ykv0wmdwkm")))

(define-public crate-uu_pwd-0.0.18 (c (n "uu_pwd") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0w05jldc1sdd635fy2qlshrf79b2jws8ka89xggm7fh9b6i9rqw7")))

(define-public crate-uu_pwd-0.0.19 (c (n "uu_pwd") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1cy7llrnz2dvsk7wmz3418pcibi7bc066wf6wpj11ndl8qcmv5zv")))

(define-public crate-uu_pwd-0.0.20 (c (n "uu_pwd") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "09c1dnn7s6fcrbmdqw8kzvh4ii0macr3f5s6zv3fpjv7rzlw9332")))

(define-public crate-uu_pwd-0.0.21 (c (n "uu_pwd") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1h7393wrdl6gpvr5r2hw1mvnhv9s25vaix8zzrqpx1qj95g0ncnc")))

(define-public crate-uu_pwd-0.0.22 (c (n "uu_pwd") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1kbp0nr9pnywqsl3s9ggnjavvffpp32c5vvl2njpm4bdd8vh1d36")))

(define-public crate-uu_pwd-0.0.23 (c (n "uu_pwd") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "16m6576wdr1nhsdqqn7zhsy67d7v04brjw71lnr3zdj43qh6lnf1")))

(define-public crate-uu_pwd-0.0.24 (c (n "uu_pwd") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0zd33k4q81dz6920lhkgp1rhd8c61pa8x276gqhklhgiddacid0x")))

(define-public crate-uu_pwd-0.0.25 (c (n "uu_pwd") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1hj7hq1l4rnlm5kvqd0dsmkil8hx4pninfn1mkwdc8hw38lrjq58")))

(define-public crate-uu_pwd-0.0.26 (c (n "uu_pwd") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "00c5lby7p234rqbsxg946h8lwq7pf5d973hjpzcrzxrnkw23dw66")))

