(define-module (crates-io uu _p uu_printenv) #:use-module (crates-io))

(define-public crate-uu_printenv-0.0.1 (c (n "uu_printenv") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0mpdv9y9ifgr584h9bpsj3yj1ydpw85b0xmr99586vmc3mffrpki")))

(define-public crate-uu_printenv-0.0.2 (c (n "uu_printenv") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "15y4ca1sdq2yvr22k78n27hm1q9mi845g047j5gbwqi50x5rlm1l")))

(define-public crate-uu_printenv-0.0.3 (c (n "uu_printenv") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "13qs70295f2kkbh819bnhkln5c32z24vnnkv58v54javy7hki71p")))

(define-public crate-uu_printenv-0.0.4 (c (n "uu_printenv") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0p263pd00h7x1yykpiag7xb763fb74hsfvnk83916q1wzpk1y4yr")))

(define-public crate-uu_printenv-0.0.5 (c (n "uu_printenv") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1zgsz7frclk9zg8lkmgxp85gj2j4asqa0nxxfrl3x48gh5kjglad")))

(define-public crate-uu_printenv-0.0.6 (c (n "uu_printenv") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0xir2zkrsmp2fn4nw2grzzhfrjqdig1dg1msgaq0dxr28594a8l0")))

(define-public crate-uu_printenv-0.0.7 (c (n "uu_printenv") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1jx8x6b18f9fbs96pia4wkahva3bx9wiid02sbfhbfw528lhlq9a")))

(define-public crate-uu_printenv-0.0.8 (c (n "uu_printenv") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "04dlb1b8nlpanndkv26d4mnlb0ww9b0c01kl52b5hsx7wr1vl2cl")))

(define-public crate-uu_printenv-0.0.9 (c (n "uu_printenv") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0d0vbp42dwaqvg6wvwgplnb9cnag5pagybr6k7p63ldh07xgyz17")))

(define-public crate-uu_printenv-0.0.12 (c (n "uu_printenv") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "10yn8gkq7q303j57vbn8f0j5gzl8xskn2iwc7c0ikl1c6qk13x78")))

(define-public crate-uu_printenv-0.0.13 (c (n "uu_printenv") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "037xjrfw50m8f78jap1mbf9pmqci4n9w2w4c469w95ij161d8s68")))

(define-public crate-uu_printenv-0.0.14 (c (n "uu_printenv") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1qn1fh0haz5ry2852h6qvn9qmi5d3921rqpjnm2r9z6vg5kaq1w0")))

(define-public crate-uu_printenv-0.0.15 (c (n "uu_printenv") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0sidx9wfbrccjzjiwssi0nlxihpx9npj1yvxngbsv2236j2wjnc7")))

(define-public crate-uu_printenv-0.0.16 (c (n "uu_printenv") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "00hlp0r319phhafbmkzjcdj3zd2gdwmyqy45pwyj698y7wdq44j7")))

(define-public crate-uu_printenv-0.0.17 (c (n "uu_printenv") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "0hvmap7g1r3vgld4ag0vk94ndaqr69nsva32qi5r6w2v25hcxqsd")))

(define-public crate-uu_printenv-0.0.18 (c (n "uu_printenv") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1s0lxlvzqx56bw9a4jcw231vf4wwzrgg020pgr9kdmd8a5kc626r")))

(define-public crate-uu_printenv-0.0.19 (c (n "uu_printenv") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0p6yc7wi6fn9cm12a9va1fy2yz0qgbh49fv7qifrwspw031ldq0y")))

(define-public crate-uu_printenv-0.0.20 (c (n "uu_printenv") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1sb7gp5vhxixky805s4rhx3jz99n0ys5y9y73zwyjg2spplsx8mj")))

(define-public crate-uu_printenv-0.0.21 (c (n "uu_printenv") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0ywfqlxh1nj21z8mdadbk58xvqgababmdcfykbq2llxhzy44lw4i")))

(define-public crate-uu_printenv-0.0.22 (c (n "uu_printenv") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1lbv6yfqyyrbh7hmsac07lrqfcs0x7fbd6s3yyp93avfxcb51zna")))

(define-public crate-uu_printenv-0.0.23 (c (n "uu_printenv") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1dww9i16ca6bmcw5np5d4x0kvlwikdk47k3ih3bvd6cabqpzjbnw")))

(define-public crate-uu_printenv-0.0.24 (c (n "uu_printenv") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0hywmcjjgx47jz8abzvf2dq9fw39mnpn83bhdi27ym7w2k8513a5")))

(define-public crate-uu_printenv-0.0.25 (c (n "uu_printenv") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0abrll16rms9xjx6b321w2b7lallg8az0g2r1l050kmfwhhm3mma")))

(define-public crate-uu_printenv-0.0.26 (c (n "uu_printenv") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "061s0xp7c583vdsxx613ykdj09jawwfmqrxbz5c8ylf582hl6rbl")))

