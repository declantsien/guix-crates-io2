(define-module (crates-io uu _n uu_numfmt) #:use-module (crates-io))

(define-public crate-uu_numfmt-0.0.1 (c (n "uu_numfmt") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "09skgs8q70fafhz0vnfs07glc4q6rf1jrgfl7va3l67skiadypdj")))

(define-public crate-uu_numfmt-0.0.2 (c (n "uu_numfmt") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1x9zqh2w56dw3dv2vfpn4c3d31ypqhd2mk07wmmxcqyixl5i6lhc")))

(define-public crate-uu_numfmt-0.0.3 (c (n "uu_numfmt") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "13xrbpv0xq9n39nzdplxzwbkzhrmzin4lkmwv8sd3hpnq9vjq07z")))

(define-public crate-uu_numfmt-0.0.4 (c (n "uu_numfmt") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "065jih90zwpl4214s6qjqc1wwzmvyjva464yjc5ac47xj4hkcnq2")))

(define-public crate-uu_numfmt-0.0.5 (c (n "uu_numfmt") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1vmfp0w5pdn5361ihsv4j1g7zzzfgvg66z7knvhlr3x2pw5fzbnr")))

(define-public crate-uu_numfmt-0.0.6 (c (n "uu_numfmt") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "16c9978fwzxbdslgl4nr7wyc4zq1jbjhjlfyn79i12pivl9z3pv1")))

(define-public crate-uu_numfmt-0.0.7 (c (n "uu_numfmt") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1ahdvpqhwhf1kdsbf098dv3xwzsd4a1bz3f0c7x27bk1g6h1iyng")))

(define-public crate-uu_numfmt-0.0.8 (c (n "uu_numfmt") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0rvcikv5lmrwg4zza5nybqrndpcshwlvpba0d5bk536s547xh4i9")))

(define-public crate-uu_numfmt-0.0.9 (c (n "uu_numfmt") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0ri84hspdhggvz25w746pd343hdi6z11b6xdv7ppmh3r52i7lvdr")))

(define-public crate-uu_numfmt-0.0.12 (c (n "uu_numfmt") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0alx0wpdkv5qkm8j769w947dcf1qnp4mqln0slknhw8cqabr2vjd")))

(define-public crate-uu_numfmt-0.0.13 (c (n "uu_numfmt") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0r17gcl4vplcambq9589g0fwszjg0cvk6d7g6z3y728k6g2rlwph")))

(define-public crate-uu_numfmt-0.0.14 (c (n "uu_numfmt") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0vkc9ygdjriaa16jgg4xlcrvf035d6avib890dqkgxidk2gmqj0r")))

(define-public crate-uu_numfmt-0.0.15 (c (n "uu_numfmt") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1wy5z2jahncmsymqv93ld5jl32f3jgif194iqjscxz524zafc56i")))

(define-public crate-uu_numfmt-0.0.16 (c (n "uu_numfmt") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1pm6nvad8ia1biqwb7iiynl02b89bynikyz1nfw4czgffb6dqmlh")))

(define-public crate-uu_numfmt-0.0.17 (c (n "uu_numfmt") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "0ywzp2b7lfqqkn83nzs4ncny0ab4a9jadmmfw9cwpfhv4zlcg9y6")))

(define-public crate-uu_numfmt-0.0.18 (c (n "uu_numfmt") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "036p3grhp93bdb35i2z4y3f5gdy9sqkr89k980ii079gyfhx0i8y")))

(define-public crate-uu_numfmt-0.0.19 (c (n "uu_numfmt") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0120kl60b2866s2shc8fkw4ybf5rilzg4zgkrdncfg74dmdl5nkl")))

(define-public crate-uu_numfmt-0.0.20 (c (n "uu_numfmt") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "02hhck5x3n560kgk3bvmk1mv6b5zy8la9wr1b6km74bb2a7v1x6d")))

(define-public crate-uu_numfmt-0.0.21 (c (n "uu_numfmt") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "17lm4n1a83cwkaiipyvfwnw3f4w7xg3h1cfs1v72snrn2xls25m5")))

(define-public crate-uu_numfmt-0.0.22 (c (n "uu_numfmt") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "0kyrqrgg141wwzw0ix6l7j4q8jkwg2s6bs28c4k3jvwc58f52v51")))

(define-public crate-uu_numfmt-0.0.23 (c (n "uu_numfmt") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "1apgxw7vwkhxnk8pni8ksl4hn6anywkd2j82hmkxflahc57c9mlb")))

(define-public crate-uu_numfmt-0.0.24 (c (n "uu_numfmt") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "0327g2l0dqhg4i566p9869vhw0jknbf4h0bi4snm3rqn9157n5jm")))

(define-public crate-uu_numfmt-0.0.25 (c (n "uu_numfmt") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "04w387csas8igh5k3vz73gx3wivl8qpfbzf3dxpism9r0l3iq1jq")))

(define-public crate-uu_numfmt-0.0.26 (c (n "uu_numfmt") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "0j9zgxbjld63bm9g56hz5ck95308rar6r0wp5yjgga3chnia0y40")))

