(define-module (crates-io uu _n uu_nohup) #:use-module (crates-io))

(define-public crate-uu_nohup-0.0.1 (c (n "uu_nohup") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "06msmp03nhzznj59yfwgsiivw9haw0661jxchd2w1g7qpwzs7gzh")))

(define-public crate-uu_nohup-0.0.2 (c (n "uu_nohup") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0730sfnqbcfqmjvqaqp4hwlymvjxgnwri572bfskf1a78qgxs9b0")))

(define-public crate-uu_nohup-0.0.3 (c (n "uu_nohup") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1yqn8xg9n8iyysc3c5fciqlysp1hdmw20vvrx0fd3i9gp44w2srv")))

(define-public crate-uu_nohup-0.0.4 (c (n "uu_nohup") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0awhlpl1bb827afx39r7j55wdvpi3l5k9dzfkankq07zwjw6h0ar")))

(define-public crate-uu_nohup-0.0.5 (c (n "uu_nohup") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "000hrgda9jc7izablcybkdi5f4pf3ab47kpqga70rllfrsgy2njd")))

(define-public crate-uu_nohup-0.0.6 (c (n "uu_nohup") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1n0xdga4p04a94d7cnxi7p25zpipmvb98rlg4xn155dczjsm2bv6")))

(define-public crate-uu_nohup-0.0.7 (c (n "uu_nohup") (v "0.0.7") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "14bmhgnljaai73bwz42pg2c898gq7487zbg5v615xdi11765h5k6")))

(define-public crate-uu_nohup-0.0.8 (c (n "uu_nohup") (v "0.0.8") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0fn61p69fqfh5bgg4922pjj039nlzwarnrkj3a06y28x6vawrizi")))

(define-public crate-uu_nohup-0.0.9 (c (n "uu_nohup") (v "0.0.9") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1zgjkcygsjp84nxgpa4k0m6456s6ign3f81vjp1iyjsahc6hpf6l")))

(define-public crate-uu_nohup-0.0.12 (c (n "uu_nohup") (v "0.0.12") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1j7llbz4jyiczhbpmjmxpnpd076950rf7lql6c6in4dz114q5ss5")))

(define-public crate-uu_nohup-0.0.13 (c (n "uu_nohup") (v "0.0.13") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "149vgk4y3cizydkicp7z1l5pwsvvkjhwnsv0vj34csxdbl62dymm")))

(define-public crate-uu_nohup-0.0.14 (c (n "uu_nohup") (v "0.0.14") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0cjif6ff0d7hcbgqf57pf4is7w77zr51bgsx26j7smsg5arfb46p")))

(define-public crate-uu_nohup-0.0.15 (c (n "uu_nohup") (v "0.0.15") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "123f8bzlpfwwld5kj3gxj671libql9id6i0sxvqdz9j2hf0szmjh")))

(define-public crate-uu_nohup-0.0.16 (c (n "uu_nohup") (v "0.0.16") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1b4m0h70l4nrw0agkiqj53npxyaqwxjsv8hm7m2alx6ayay35fj4")))

(define-public crate-uu_nohup-0.0.17 (c (n "uu_nohup") (v "0.0.17") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1bm7cxkkbww0m6926dckqx44bsrcla7bm7h3aaj9g4bc93p35ryl")))

(define-public crate-uu_nohup-0.0.18 (c (n "uu_nohup") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0c2d1ra1xs1alqq4cdzg1wyjpl207vfi7c54pk2ak6s2slkhhxci")))

(define-public crate-uu_nohup-0.0.19 (c (n "uu_nohup") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0y1x9bcv0vdw4m0bqrjl2bfrndr6xac3adcr5i1m77rkfygigfcd")))

(define-public crate-uu_nohup-0.0.20 (c (n "uu_nohup") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1rh6vv27gd8b4kdcf39xkv2cpfdhhp0wwq8kb85042jmwzxy25zx")))

(define-public crate-uu_nohup-0.0.21 (c (n "uu_nohup") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0cnafaacbi6vwvw1c6glbz67rak2v8x67c7k0y5xjif3q81zlpfc")))

(define-public crate-uu_nohup-0.0.22 (c (n "uu_nohup") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "032rnarjcqscq7fyy5cd08jk7cqws57n5h13ia3fl0368awi1f87")))

(define-public crate-uu_nohup-0.0.23 (c (n "uu_nohup") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "16gcpabg1y30mdrldb8r8i9hqlnjlnyawn1b26nly14ylvn9kzqc")))

(define-public crate-uu_nohup-0.0.24 (c (n "uu_nohup") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "123rmldqgv2zyr22xka83pwqv35i46d3i360shipcxjfjib1df6c")))

(define-public crate-uu_nohup-0.0.25 (c (n "uu_nohup") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0in0yfcv04j6z5r39vvz0mdzn88ryxjmp0c3wsyvsgpnilwm2xxm")))

(define-public crate-uu_nohup-0.0.26 (c (n "uu_nohup") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1pjpi9ly6lkhwkn0nc9g2dj4sij9arls2q8y39m3dms62v018nh4")))

