(define-module (crates-io uu _n uu_nice) #:use-module (crates-io))

(define-public crate-uu_nice-0.0.1 (c (n "uu_nice") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0jgfyp02d4sr7rj9cjijc793gjnwg21bpdx400wn0qzg98a0pipq")))

(define-public crate-uu_nice-0.0.2 (c (n "uu_nice") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0r3x83lzmw4r9wl9y5799bgllvy9viqz65kljrnvl4na15f9mry3")))

(define-public crate-uu_nice-0.0.3 (c (n "uu_nice") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1br5n1jnz9c5f3sk9w70ll3wpn9fqlfp6cmb4psisgx4xydhhyhw")))

(define-public crate-uu_nice-0.0.4 (c (n "uu_nice") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "11ngmza8wi6x3zakd0y3pjwi5s7yfb7bsf3kb416klx1vhhyj9h3")))

(define-public crate-uu_nice-0.0.5 (c (n "uu_nice") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "nix") (r "<=0.13") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1j5rw9z9hr0pgqrrbd8p61xqw5dk95hgf0qjjspyidn17cav4cd0")))

(define-public crate-uu_nice-0.0.6 (c (n "uu_nice") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "nix") (r "<=0.13") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1nhr57p7jggnkckacrhi67hbwarfh3hbzw4bxdhzllw6bxnx1pj1")))

(define-public crate-uu_nice-0.0.7 (c (n "uu_nice") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "nix") (r "<=0.13") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0w4m5mg5naws91ixf0avx5w0h2ygyy32sqgvyr69a6bs7hfawjfw")))

(define-public crate-uu_nice-0.0.8 (c (n "uu_nice") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "09c69w6vz1axg6dh05v0iqvzfmgaa137x2ak1vvv3wd6svp21cg0")))

(define-public crate-uu_nice-0.0.9 (c (n "uu_nice") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "08d1ijrx774n0y1c6y5xhn7zbah93siyhf558n0alnhxgyhd8fsl")))

(define-public crate-uu_nice-0.0.12 (c (n "uu_nice") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "13j1jnb7y6af1kra15p3zc82jv04d0xvysmjh61flcjpfhf8aqid")))

(define-public crate-uu_nice-0.0.13 (c (n "uu_nice") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "04hkgf93gb9q1c0lvf5b83zn10zbifrmhnlff1pjx79amvdl4dwg")))

(define-public crate-uu_nice-0.0.14 (c (n "uu_nice") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0ad00x3vcfw6z6l7j2g1cayb1swyx6c9k76zvypf70zcaj69s6cf")))

(define-public crate-uu_nice-0.0.15 (c (n "uu_nice") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "nix") (r "^0.25") (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0nj9820jzddvypcbfyjgfd3wln3d7yr9069pzckdnxklcajgp4sj")))

(define-public crate-uu_nice-0.0.16 (c (n "uu_nice") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "nix") (r "^0.25") (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "19s41l6n762sa49v01f7bfmzdbpkcwk5fgxkhdrdd098nrk8q30r")))

(define-public crate-uu_nice-0.0.17 (c (n "uu_nice") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "nix") (r "^0.25") (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1l0b01cvgbc1rg4d3bbzhip9b2s2bas1lnx82q9ypccq6n0fn1qm")))

(define-public crate-uu_nice-0.0.18 (c (n "uu_nice") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26") (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1k0aw2s2c271r1i23brq5kwsd1xsmz620js2a4xyvq0jls5f7h3x")))

(define-public crate-uu_nice-0.0.19 (c (n "uu_nice") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "nix") (r "^0.26") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0pnbf57h39217vk2adbl0p8q3c9rdmpyh5p8pzgzllrpni895rgv")))

(define-public crate-uu_nice-0.0.20 (c (n "uu_nice") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "nix") (r "^0.26") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0kf5wm1yqfz8hyaypgx0sj15skxqz98167kily4lfc7bsm911wxs")))

(define-public crate-uu_nice-0.0.21 (c (n "uu_nice") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "nix") (r "^0.26") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0ddg3rpv91p3n7vm2zzc6ahzwivwhp01jsd41gi5dxljndvpsi2v")))

(define-public crate-uu_nice-0.0.22 (c (n "uu_nice") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "nix") (r "^0.27") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0p1v7kpjsw7fghvfgrqffhin4jikc1610z2n0bgmhb336g0xzaqs")))

(define-public crate-uu_nice-0.0.23 (c (n "uu_nice") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "nix") (r "^0.27") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0qjxv4bp9cywkjiqiwx67wl4kyzgsnmhqi4jq16lwm0afdv9wdzg")))

(define-public crate-uu_nice-0.0.24 (c (n "uu_nice") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "nix") (r "^0.27") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1fcjpvfdyhfwsm6hnqp26axxm0p05838g1l58a98v6md2cydba86")))

(define-public crate-uu_nice-0.0.25 (c (n "uu_nice") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "nix") (r "^0.28") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1pjf935z8ww43nw8r6f9gcrhy70xm18ys8c4433ilfwyafphrr2h")))

(define-public crate-uu_nice-0.0.26 (c (n "uu_nice") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "nix") (r "^0.28") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1maahrnmcdnzxlx9kx150lx250gnb18xw7vxh7ysjk9iaz9b7apd")))

