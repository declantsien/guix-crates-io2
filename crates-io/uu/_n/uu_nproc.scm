(define-module (crates-io uu _n uu_nproc) #:use-module (crates-io))

(define-public crate-uu_nproc-0.0.1 (c (n "uu_nproc") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0786584hw41mmzl9957qm7w1pcmrqh38kbf0ysgng52g3gf3d800")))

(define-public crate-uu_nproc-0.0.2 (c (n "uu_nproc") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "092zglxma292bhj5cy6x9glpzyjdwjvlrn1sikh2f3yah1n7i3qw")))

(define-public crate-uu_nproc-0.0.3 (c (n "uu_nproc") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0ia9ibimrjg1llpkbkcrmnpf614gvpq0cy08arh6vpanq1yi438i")))

(define-public crate-uu_nproc-0.0.4 (c (n "uu_nproc") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0ypfkxiwdndkhva4az6a58z32n1y1qlck3swcdr1v3847s1dhny9")))

(define-public crate-uu_nproc-0.0.5 (c (n "uu_nproc") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1zh9ihi27a5z39whv1v6sdhijcwhqlwcqnfdpjvd7cpfpfsq3lvf")))

(define-public crate-uu_nproc-0.0.6 (c (n "uu_nproc") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1l2rahfgqh8d485c5lgw0v3gjmpwj2xicy1mckhs96h83m8jniaf")))

(define-public crate-uu_nproc-0.0.7 (c (n "uu_nproc") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "16yyr9kg50wz9kq42gggn7mgv253m0c6szfjkybcg8kfbf6fgdp0")))

(define-public crate-uu_nproc-0.0.8 (c (n "uu_nproc") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0c1x24qlqy3l3n81z919rr2bjdsa6f060q3skm0grgkvl9a94rdv")))

(define-public crate-uu_nproc-0.0.9 (c (n "uu_nproc") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1f9lby6v8bxmp2iwckcn8r17vknrm200jbx2n6v70rkc1x78h5l1")))

(define-public crate-uu_nproc-0.0.12 (c (n "uu_nproc") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1zgd47i0fqsslpjpxbqx182pl54iksv4qibm0p0yibl7f379m9m2")))

(define-public crate-uu_nproc-0.0.13 (c (n "uu_nproc") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1mcx496cxj3qz8hp4sgkrz7zpqal28maffl35f50b0hd7bpvwv81")))

(define-public crate-uu_nproc-0.0.14 (c (n "uu_nproc") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0hbrhmxh0dlwn3nqjnw9y364k520d514g4sha4gngvg48x3jxygk")))

(define-public crate-uu_nproc-0.0.15 (c (n "uu_nproc") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0j9gz4r8fff780zfqb1fny4kasqjgf4v5bkqi2wg2jjqbf7snp2m")))

(define-public crate-uu_nproc-0.0.16 (c (n "uu_nproc") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1vnr07rpckyqg2i4dqp0qxs9mqpnbblmypkzwhxk9180ws8l5w3q")))

(define-public crate-uu_nproc-0.0.17 (c (n "uu_nproc") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1aivzqky8054x3a7zdkv17a3clnyajm6nqh25vl1q66pqwy9s6q5")))

(define-public crate-uu_nproc-0.0.18 (c (n "uu_nproc") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "01715kw20wvz35xkcfhk3q8hcj26ma53v551xxmqnk1ldgla5vva")))

(define-public crate-uu_nproc-0.0.19 (c (n "uu_nproc") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0j1a7z3mnj2d5n3ji13wacd9bak2pl6mhkc9b82x7g221hfz8909")))

(define-public crate-uu_nproc-0.0.20 (c (n "uu_nproc") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1cn8f9gp09cqmq7zp1pwhcvh9fx583sqy74xw2f6rc8qd66170r2")))

(define-public crate-uu_nproc-0.0.21 (c (n "uu_nproc") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0qz233s2apjdg5rv5bdmqb3mzabpy4gm87i893x0cykns28f7106")))

(define-public crate-uu_nproc-0.0.22 (c (n "uu_nproc") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0zaarx12yaphwdfdz951r9rwfwbznw57r8ir7bd6gi9hafq4k0ly")))

(define-public crate-uu_nproc-0.0.23 (c (n "uu_nproc") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1y69hh950p7cadjhxjicik340wwyxin63ncwvd9x0xrwzdg9pxvx")))

(define-public crate-uu_nproc-0.0.24 (c (n "uu_nproc") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "02vdzwrflpsscgsy22ky6rg4wpgr8xjpkvgm2mmf2k9ngcs59972")))

(define-public crate-uu_nproc-0.0.25 (c (n "uu_nproc") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0vg19gv30yr4487hddv11lw37hlsgpcw9dgfibah91awndaxi2rz")))

(define-public crate-uu_nproc-0.0.26 (c (n "uu_nproc") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1a0ccq3m7zyam20m7166g894yn07mmrkiqna6s92bxaryfavrd99")))

