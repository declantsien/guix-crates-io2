(define-module (crates-io uu _w uu_w) #:use-module (crates-io))

(define-public crate-uu_w-0.0.1 (c (n "uu_w") (v "0.0.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "utmpx") (r "^0.1") (d #t) (k 0)) (d (n "uucore") (r "^0.0.24") (f (quote ("utmpx"))) (d #t) (k 0)))) (h "18xsdy0k6jfi9rlyhli4jp51s0pyk1m917hiiis5wkvzdp5p07p2") (y #t)))

