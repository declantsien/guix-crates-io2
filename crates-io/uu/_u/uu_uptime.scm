(define-module (crates-io uu _u uu_uptime) #:use-module (crates-io))

(define-public crate-uu_uptime-0.0.1 (c (n "uu_uptime") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0lh2h8cjrdxbm6a3rmxiwd5150w8bnw4qv8hflmbykismjvf4d06")))

(define-public crate-uu_uptime-0.0.2 (c (n "uu_uptime") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0g8aa9i7k9jiis61j7bwkvfj9wsfmnbvnjl3828dppbmrp8w9zpl")))

(define-public crate-uu_uptime-0.0.3 (c (n "uu_uptime") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "113rw543lwi5szq9sxm54rwh6bcwq27ms0v747g6vz09nxr76caq")))

(define-public crate-uu_uptime-0.0.4 (c (n "uu_uptime") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0k6k1y8i2ffrrpaz7jf2gzf9gc2ka28ifqfw3k20pjbwcr07zc83")))

(define-public crate-uu_uptime-0.0.5 (c (n "uu_uptime") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0675p6azh3082wxrihrc6gd4zda3mkm9msidlyz7kxn4ik72alkk")))

(define-public crate-uu_uptime-0.0.6 (c (n "uu_uptime") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1ndc3dhhfhfwy8lsblsgrn4ivfnfanp6z77ksyp22ilvxkwbbcq5")))

(define-public crate-uu_uptime-0.0.7 (c (n "uu_uptime") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1pgvfgd3zhmc48f6y4acz0rcv03xpijmldp15dai9wz2vzsq65i3")))

(define-public crate-uu_uptime-0.0.8 (c (n "uu_uptime") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "16axfjpijsa31w8py0jz45ss6bapyavl1hd0dlglhrx5mr39aicl")))

(define-public crate-uu_uptime-0.0.9 (c (n "uu_uptime") (v "0.0.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1pq9z512gbwcqqc3md6ccxw3zy7fllsr3cp4pfc715z3ikgqr322")))

(define-public crate-uu_uptime-0.0.12 (c (n "uu_uptime") (v "0.0.12") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0zh8m7ary132x5p2kra00ri0jyab8b5wgm126j1frbxn3riqsf3i")))

(define-public crate-uu_uptime-0.0.13 (c (n "uu_uptime") (v "0.0.13") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "1zwg6yb18w6zjynq72q24v6m2zdyn7p9agmlkvhxbp588kggnmlc")))

(define-public crate-uu_uptime-0.0.14 (c (n "uu_uptime") (v "0.0.14") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0g4rnncv82bhj5fvxn9qp2gdq501i5v90wlgz1m1i6syq91wfac6")))

(define-public crate-uu_uptime-0.0.15 (c (n "uu_uptime") (v "0.0.15") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "1cwfz3klw7pfha9hal46xrqik0gnh7lykgjdcdx6293hm4m3372y")))

(define-public crate-uu_uptime-0.0.16 (c (n "uu_uptime") (v "0.0.16") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0nahm60gxp5y5r7bq46k5gf8zk6jgvgxvl5mhap7avhrhmy7jisr")))

(define-public crate-uu_uptime-0.0.17 (c (n "uu_uptime") (v "0.0.17") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0ryq551v542c0g85hvn4wvd6120wdmghm67b9z644pf379impv6b")))

(define-public crate-uu_uptime-0.0.18 (c (n "uu_uptime") (v "0.0.18") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "1xd2yajypmjrqv2krm309556ijg9ndw0ay7q024y845vca8gxjrx")))

(define-public crate-uu_uptime-0.0.19 (c (n "uu_uptime") (v "0.0.19") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0da3f435vd0jvsfwcfgng63qbab6l6bcwin9719plnrachbrym4l")))

(define-public crate-uu_uptime-0.0.20 (c (n "uu_uptime") (v "0.0.20") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0l3fw41za8w2z3phr8n62mq6dxqnqnhpwlcwnk5nc53wb74zdxfm")))

(define-public crate-uu_uptime-0.0.21 (c (n "uu_uptime") (v "0.0.21") (d (list (d (n "chrono") (r "^0.4.28") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0043jfra9hmbd1kdhlqc3djyrvrsscq42kv2dv4w653ymcmb9k4x")))

(define-public crate-uu_uptime-0.0.22 (c (n "uu_uptime") (v "0.0.22") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "15baw36prqpvil7zwkbrxvkrpfz0isszjjjz1mxbi3jmrszv57nv")))

(define-public crate-uu_uptime-0.0.23 (c (n "uu_uptime") (v "0.0.23") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "1gnxyqkpbmcvygrl98mxnr5ldhxahqidq8ny5r8n937f3pcdg5wz")))

(define-public crate-uu_uptime-0.0.24 (c (n "uu_uptime") (v "0.0.24") (d (list (d (n "chrono") (r "^0.4.32") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0g71wjz029j8rl90wi5kz0gx06hgyb2ycaxna9wcq36nn8gl4xmy")))

(define-public crate-uu_uptime-0.0.25 (c (n "uu_uptime") (v "0.0.25") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "02py6f6vf0nwanq0vhsbpman0pq8n2prwv25yar2z06h83180x0b")))

(define-public crate-uu_uptime-0.0.26 (c (n "uu_uptime") (v "0.0.26") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("libc" "utmpx"))) (d #t) (k 0) (p "uucore")))) (h "01i6ydjvxkr9nz3gr278gbfgzi9ng2ppr5g4kzakl9f5wcmfrsb8")))

