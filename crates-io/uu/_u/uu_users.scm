(define-module (crates-io uu _u uu_users) #:use-module (crates-io))

(define-public crate-uu_users-0.0.1 (c (n "uu_users") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "10ziva0fh4vda2dgvsc5733ql0vdfbacz4p3yz4zk63njqzqxzjg")))

(define-public crate-uu_users-0.0.2 (c (n "uu_users") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "09j5vylgdn4vi5frnx6l16ndgx8hxwijagyn9q79psph5cgrd7fk")))

(define-public crate-uu_users-0.0.3 (c (n "uu_users") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0gnx3izq1q765sza1kcfn653a0i58kzb7f53mm59183izy59l17b")))

(define-public crate-uu_users-0.0.4 (c (n "uu_users") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1nxf1q1q9jkcjlbfwihxrpknpsmi1a6rpv9whpczm66m7yqkpy91")))

(define-public crate-uu_users-0.0.5 (c (n "uu_users") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0cv078nsc1v1b0mqnqqgpj60aqzhxabfiyz0agq8bn1xcndzvjy9")))

(define-public crate-uu_users-0.0.6 (c (n "uu_users") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "15gmgdcf299m70fnib3h8fwk5zsck787pv4vpblllxs3f6p0ajyr")))

(define-public crate-uu_users-0.0.7 (c (n "uu_users") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1wlf34iammdjfl96m5s3vvfbsl3xnj5mygc3z7gw4zhvpzydh7y9")))

(define-public crate-uu_users-0.0.8 (c (n "uu_users") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0zrakgyjhwf46zsv81k3z7nkiy2971y0liz231l3qc0kdhik2sy2")))

(define-public crate-uu_users-0.0.9 (c (n "uu_users") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "06g6laz206p5g9yyzgjaqahlfpz0fnkyhyw1rh2i5jry2x0n6c15")))

(define-public crate-uu_users-0.0.12 (c (n "uu_users") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0d58v8rhh37x6qs1z8i183xm4m7hvfcabjpz70ab7hvsdaxb8da3")))

(define-public crate-uu_users-0.0.13 (c (n "uu_users") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "1y0z069w7gyfl0dl92fznw33y9mhiqcvimw1k5zbq17krmyyl0gc")))

(define-public crate-uu_users-0.0.14 (c (n "uu_users") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "1qcyabkf749ndh7957hqkx2cp16ajvk4l6cv4n00jdwfxal7rhq4")))

(define-public crate-uu_users-0.0.15 (c (n "uu_users") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "1bfrp6ilhcw2xwqmlfhxq7pa93mlsw68rzis75w09758yg7307kh")))

(define-public crate-uu_users-0.0.16 (c (n "uu_users") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "16f85zx8a9sx3x16f2v2yj69fxnwyx50l0fm0x10qhbf941djpv3")))

(define-public crate-uu_users-0.0.17 (c (n "uu_users") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0rzkpf2r9s2n43qpk4dfz7zawgqcpjqkmy8pp2jmh9hgrml6fjfn")))

(define-public crate-uu_users-0.0.18 (c (n "uu_users") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "184k3l48n93hzjpyzwvyqw7a0avma8di09zd9w1f7mj5ihklcisx")))

(define-public crate-uu_users-0.0.19 (c (n "uu_users") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0r20xaxdgmna1faljcaypa6hmgpxmwb0r3cl3hyjnkkgv7s7kvlb")))

(define-public crate-uu_users-0.0.20 (c (n "uu_users") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "1nzcr8lgl46fq0mqdardaiyjakj6jmf3r0mkjc3v5l1rp0sc8pv4")))

(define-public crate-uu_users-0.0.21 (c (n "uu_users") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0bj4z87hb86mjzl7n3s71yni1c6qsz3rzr9jfv30hixzrrykbwc8")))

(define-public crate-uu_users-0.0.22 (c (n "uu_users") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "1vb7z1cffsnxnn4d7dpjzgxrfwfb2j9cg0gxzyssx2p37xbhiwdg")))

(define-public crate-uu_users-0.0.23 (c (n "uu_users") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0ky0npncygy4l2lmpqqvkgp3xvs74v7ini8f560asczwaswqspfk")))

(define-public crate-uu_users-0.0.24 (c (n "uu_users") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "1flz36r2vs3sjkrqf8sw8miz0d7sdprismabz5rxsl30ks4g4r2i")))

(define-public crate-uu_users-0.0.25 (c (n "uu_users") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "066d0zkmnqnh2iyxq58xbbjp7j7ydrbm331z6p713rsvx2irpjsw")))

(define-public crate-uu_users-0.0.26 (c (n "uu_users") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("utmpx"))) (d #t) (k 0) (p "uucore")))) (h "0qgry5ik50fqvdfzchbvjvdspl8hpv9fczrmkc62fqbfkf7nj39c")))

