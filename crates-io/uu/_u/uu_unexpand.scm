(define-module (crates-io uu _u uu_unexpand) #:use-module (crates-io))

(define-public crate-uu_unexpand-0.0.1 (c (n "uu_unexpand") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "004yz3ff2vk8g2qdgs3xsy1dh20nadwqqg9f67f8y984nqhwc0pc")))

(define-public crate-uu_unexpand-0.0.2 (c (n "uu_unexpand") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1qcgazack6i4ihi47r4sb6xfzhr6lipyfcp1si5j6i7rszp74zj6")))

(define-public crate-uu_unexpand-0.0.3 (c (n "uu_unexpand") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0panqgc91ygwn59piig5w9cv2qj1a8pdbwqxfgl13wa9ymkrphmg")))

(define-public crate-uu_unexpand-0.0.4 (c (n "uu_unexpand") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "16gyny234y05qs1c0xyhzmzjic5h3x94dq182hcih1p5r8yxhicg")))

(define-public crate-uu_unexpand-0.0.5 (c (n "uu_unexpand") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1hsrzsvi2qc99v29m1gry81zkpwwnn23jk9pj5sg7pv4shv88x7a")))

(define-public crate-uu_unexpand-0.0.6 (c (n "uu_unexpand") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1f0qnxhz1k8n6n2gqmkmdyv7sgj005nqsg9v0yzbpi2qx4ynj1pc")))

(define-public crate-uu_unexpand-0.0.7 (c (n "uu_unexpand") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0yp5wn74im57n8cxksxrk9xzg7z1svig2p3wlgrp9hikw9wwm0rg")))

(define-public crate-uu_unexpand-0.0.8 (c (n "uu_unexpand") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0d8zb1k1w7s4p8d2dzzhnspz23sh6kjy8fm1nbg0ap51bqkah780")))

(define-public crate-uu_unexpand-0.0.9 (c (n "uu_unexpand") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0c8qxyqn7d0gqvpaw5gllnbpjiw36jq16j92mgqqh2d01s61876g")))

(define-public crate-uu_unexpand-0.0.12 (c (n "uu_unexpand") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0qg2bjan33m4ar03z9yw4sivis8hz370shnj7mlcwqi4mxj5k9nv")))

(define-public crate-uu_unexpand-0.0.13 (c (n "uu_unexpand") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "09wygi7rbvi99swfk6bg8li205j53vk7wfarpnjvrxfvqfl4iiaj")))

(define-public crate-uu_unexpand-0.0.14 (c (n "uu_unexpand") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1fl3w4mczh76lnm30bqbmhy82370pyi4b6hx288a6kgll36hcrsn")))

(define-public crate-uu_unexpand-0.0.15 (c (n "uu_unexpand") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "17rzak0gs2yxgnpiij3wa98f97f4i7l3z6xygh17habnvwkmifka")))

(define-public crate-uu_unexpand-0.0.16 (c (n "uu_unexpand") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "0lq48ph1skjcyfnww4s8awyw19fv87b0d1bqc34z06glfbil9g58")))

(define-public crate-uu_unexpand-0.0.17 (c (n "uu_unexpand") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "0a08r67jwb311qfnaph1m00rg7ag7njwmz98v4dcm0zgiqdkpsym")))

(define-public crate-uu_unexpand-0.0.18 (c (n "uu_unexpand") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0w68mwq73zwa60nfq6nghj00nj0ah7nnblndi33h3gahz25jx4gy")))

(define-public crate-uu_unexpand-0.0.19 (c (n "uu_unexpand") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1dl1djqpi902yifzbjjnp1qmmdz85ykipava8pbcmr1zg2c96pxs")))

(define-public crate-uu_unexpand-0.0.20 (c (n "uu_unexpand") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0dfibrsdjixdxvqaqrrdwai7d0f3ymmcp6n6f9gaqhyfx70ikv3s")))

(define-public crate-uu_unexpand-0.0.21 (c (n "uu_unexpand") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0jwjjgpal4xk34xnmap6rbwm59xjgx8bancp391p96hl9d0akyw4")))

(define-public crate-uu_unexpand-0.0.22 (c (n "uu_unexpand") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1fyzxzhwnfqfg84ymsdlnd9zxvk542ygmlglshlnfhdyz734qzfc")))

(define-public crate-uu_unexpand-0.0.23 (c (n "uu_unexpand") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "16dmy92mnf4rl7nsfvrknxlbfdq15dcbmbd23h8a3yysrds4l5ny")))

(define-public crate-uu_unexpand-0.0.24 (c (n "uu_unexpand") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1bw955lndnd9bv189xk1w2sx4xvvg9dj0n2gccc70cbj0fsj5zxl")))

(define-public crate-uu_unexpand-0.0.25 (c (n "uu_unexpand") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1fj9r3p63bh7yigsb4j5h42qqb86n7nd8gh4hjpi7an09sx2bpj2")))

(define-public crate-uu_unexpand-0.0.26 (c (n "uu_unexpand") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "13x6d3x2vwjyhcqnlk9xzy7b8srqlqby5wi9yg542pc24d0wrqn9")))

