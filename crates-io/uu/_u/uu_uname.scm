(define-module (crates-io uu _u uu_uname) #:use-module (crates-io))

(define-public crate-uu_uname-0.0.1 (c (n "uu_uname") (v "0.0.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "platform-info") (r "^0.0.1") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0zn9x1jdfqp1fs6p9f0f1xz8yp202iscdja162piv0apdhf42djk")))

(define-public crate-uu_uname-0.0.2 (c (n "uu_uname") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "platform-info") (r "^0.0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1f3s5dir0nw9pj1lj1314yabh0456z28nqkbx3qyb7yhz4691w0d")))

(define-public crate-uu_uname-0.0.3 (c (n "uu_uname") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "platform-info") (r "^0.0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "166xnjy16hscyc2lpsh85ghsg3x3bl0pprv9chb7ibkj05jg8dfc")))

(define-public crate-uu_uname-0.0.4 (c (n "uu_uname") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "platform-info") (r "^0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1fd0gfpd12w8lqj2pbh07bfp68lls3l6y005gb0f1c715krx7pni")))

(define-public crate-uu_uname-0.0.5 (c (n "uu_uname") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "platform-info") (r "^0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "10knd1796993nhjj9dfm0kzmiqpm3sz5xbfj6nnphd93hab490m0")))

(define-public crate-uu_uname-0.0.6 (c (n "uu_uname") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "platform-info") (r "^0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0b593f1sja65y77fyb5hp7kfrnx039r4g8vr7gmksdc4kawksg08")))

(define-public crate-uu_uname-0.0.7 (c (n "uu_uname") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "05fpn0xq33ihkflqwc4ir72an57z1331k48jmlp2wqqafmz8lzji")))

(define-public crate-uu_uname-0.0.8 (c (n "uu_uname") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0fmq2pqycjbih8ng9mpzrnakw1g5894bv7z5381bq41p3idqp86j")))

(define-public crate-uu_uname-0.0.9 (c (n "uu_uname") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0n2mbf1hycnpiw9mm1205lbxc3q28qfaw3ry78v01m9p7fvjfm1j")))

(define-public crate-uu_uname-0.0.12 (c (n "uu_uname") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "05xx2yzz4wvavvac842jr1i8q8xjbw5y4r2b2wwg5m83jxf42n4m")))

(define-public crate-uu_uname-0.0.13 (c (n "uu_uname") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1iiahnpcna29wxpx6q8xphag4bds0mlfi297gm3gaavv8lkz4clk")))

(define-public crate-uu_uname-0.0.14 (c (n "uu_uname") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1xlw39hzywb04z3038jl4bbczpz1s3gm325pfx24gk70fsck42ls")))

(define-public crate-uu_uname-0.0.15 (c (n "uu_uname") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^1.0.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "15d07x6afa6rm0rkalvnn17syxa74vzgvihblwsq2gbg65zm67hw")))

(define-public crate-uu_uname-0.0.16 (c (n "uu_uname") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^1.0.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "0mal1ylkh2rg8pnfpsabn4a6gdnid8qvdc0h5w5jwa1gvfmdmw2n")))

(define-public crate-uu_uname-0.0.17 (c (n "uu_uname") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^1.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1aqhwm7286y5cq7v9l1833qr3narlw3wgxd5nsnvwl9gi7sh5csg")))

(define-public crate-uu_uname-0.0.18 (c (n "uu_uname") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^1.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1l8603lgwv4p0qc3l5bgpfxj2l6agl9g9jca2gxmkby3ns6hcad4")))

(define-public crate-uu_uname-0.0.19 (c (n "uu_uname") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0wckr2vrvj51fzzfyxxpw0xw9ywsswnbvmny42c19p0qmpwgqhv8")))

(define-public crate-uu_uname-0.0.20 (c (n "uu_uname") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1dv0b0v614il9dvxnnzqyn1hnsrza9lz3kbknzyiimn5n2rlvmq3")))

(define-public crate-uu_uname-0.0.21 (c (n "uu_uname") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0fxhx7nm04d4bwh7sw031lbznq3r9r26l3yzjki6rqn8v85rdjjb")))

(define-public crate-uu_uname-0.0.22 (c (n "uu_uname") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0z36382r6js69ddazvwnbb3zxv052yl924kkm84hapjncfj2mv6k")))

(define-public crate-uu_uname-0.0.23 (c (n "uu_uname") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1mf91bnb0dd1mj80iz1d31gjiyarp0r98508jy0zgm5dsq513znz")))

(define-public crate-uu_uname-0.0.24 (c (n "uu_uname") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "17pxmqymv94vvf7zgfsr4albfwvmsjxbfisxlcliqghhsmawni5a")))

(define-public crate-uu_uname-0.0.25 (c (n "uu_uname") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1dbdxnmwf45wfl1jjb0la123qawkc3wirmn0wsykd5hrfcnq6lar")))

(define-public crate-uu_uname-0.0.26 (c (n "uu_uname") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "platform-info") (r "^2.0.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0kjz6ii33g8yr64ag7hpcm6vb7g3xyx2l8lgr8xk31pjnigi5x57")))

