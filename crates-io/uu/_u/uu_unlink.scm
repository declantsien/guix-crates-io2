(define-module (crates-io uu _u uu_unlink) #:use-module (crates-io))

(define-public crate-uu_unlink-0.0.1 (c (n "uu_unlink") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0cbyj7yaqyhp3bcv2nhf34xcn3vv7bb925219jidh7k1pyp4d8vm")))

(define-public crate-uu_unlink-0.0.2 (c (n "uu_unlink") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1qgywck4hvi12iyg4ky0qgcxc8xhdxjhwzhv4wqncbzac721xd7p")))

(define-public crate-uu_unlink-0.0.3 (c (n "uu_unlink") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0zp314iz712y7x0i5azpapwj2f7fmmz6gn1sn6zcnx92p1wl1ajg")))

(define-public crate-uu_unlink-0.0.4 (c (n "uu_unlink") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0kfxl4qdgx2xji1bm47jrwp6h2dhfs0a3141mryap0irs3x8ngi0")))

(define-public crate-uu_unlink-0.0.5 (c (n "uu_unlink") (v "0.0.5") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "09d6sn6lqwwgh4dwayj4hw0vrmbika596fd0pj1pv0r9mazxhv62")))

(define-public crate-uu_unlink-0.0.6 (c (n "uu_unlink") (v "0.0.6") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "11lscpblvr9dqxlxncrxw16m2g9xf6wv3gkx08nahcc707ppr34p")))

(define-public crate-uu_unlink-0.0.7 (c (n "uu_unlink") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0yph7iq94fa8jys8i2abvi5n4s07f9w38ajagwmvgpamdbbrdv5j")))

(define-public crate-uu_unlink-0.0.8 (c (n "uu_unlink") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0kfj1bhj78pgbswbv2fcmf52jiwadlprc0bn76xc3pidh6nwrjzs")))

(define-public crate-uu_unlink-0.0.9 (c (n "uu_unlink") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "11pp97fgd61fgjwq7qx5kdvb3a6yb19yg699rv92rhxnvl7vds9k")))

(define-public crate-uu_unlink-0.0.12 (c (n "uu_unlink") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1ks54hh3691mycb6zzqak9s05a80l07mibmqvrismcy9b5kd0zyx")))

(define-public crate-uu_unlink-0.0.13 (c (n "uu_unlink") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1by9qfs9n6y4q3a3bbfpsz98v14al9vzj00scp433sns82ch0wf2")))

(define-public crate-uu_unlink-0.0.14 (c (n "uu_unlink") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0dhn44qplpg358j2ddby8xsygng2sindpyp0b64w8pqgk7j47fb2")))

(define-public crate-uu_unlink-0.0.15 (c (n "uu_unlink") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "030bxjc3hfx6hw7f0f4v49hj2sm5difv9zqlihjgacv21scvp704")))

(define-public crate-uu_unlink-0.0.16 (c (n "uu_unlink") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1ax9glhigh91gxy0lwf5h9j2f03zxfqfsiy9vjg7fm6y76s7s63c")))

(define-public crate-uu_unlink-0.0.17 (c (n "uu_unlink") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "15lag2bhn9sn85glpc4vy90c6w1ca599cd3vhyggqkhbbsxajs1k")))

(define-public crate-uu_unlink-0.0.18 (c (n "uu_unlink") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1myp8v8fjvkay80mqf8dkdi8w7ll753hblsv0z9yhmhzv4jz9lik")))

(define-public crate-uu_unlink-0.0.19 (c (n "uu_unlink") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0gv0i75k1613k1lrp1zvmwy7k0k5h9cx73i8vnjs2a99f58kv86y")))

(define-public crate-uu_unlink-0.0.20 (c (n "uu_unlink") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "111yi0l7xjk6p9qpsp8v7px8wbgpamhdkga78f7x3r2wny9zlpb7")))

(define-public crate-uu_unlink-0.0.21 (c (n "uu_unlink") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0pv7qp7vw7rcnygwgcy7gal1ggaw20lrhm8jsz7pr2981br8yw0g")))

(define-public crate-uu_unlink-0.0.22 (c (n "uu_unlink") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "09x150zlk533bwz12f815hxlbwmzq4dcl6zzim69bl7hysc3wchs")))

(define-public crate-uu_unlink-0.0.23 (c (n "uu_unlink") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1mp4ycw01vj6p1p6nc0s9kdj05427ykjk8nswrv6471zl665rhki")))

(define-public crate-uu_unlink-0.0.24 (c (n "uu_unlink") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0lwzpd8hwsc3gxs2ay7kf2cfpzcfpmmiz2i2ffy0wxld5la87rcq")))

(define-public crate-uu_unlink-0.0.25 (c (n "uu_unlink") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0brqf9d909w9nx5k1zifai84g44bxi5cx9b4ggmlqjjvvl1qi5z1")))

(define-public crate-uu_unlink-0.0.26 (c (n "uu_unlink") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "07ks12call0cplanmdmwwhf4kby5l2r1c3pyz9arqakpxjg7k9ql")))

