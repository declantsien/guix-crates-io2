(define-module (crates-io uu _u uu_uniq) #:use-module (crates-io))

(define-public crate-uu_uniq-0.0.1 (c (n "uu_uniq") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "05p1dwdwmjr4j2gvh9a495bq0rn3xca6m61danpd8xahh0z6dgly")))

(define-public crate-uu_uniq-0.0.2 (c (n "uu_uniq") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "09sfn99mdmnc1v9lwrc7w7c9vx0jhdgwarzz9n1jlybasvl4v7pp")))

(define-public crate-uu_uniq-0.0.3 (c (n "uu_uniq") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0hkxamil31q6v91ygz3023rvifka04023njgvgsvrh4r8r5gmjn3")))

(define-public crate-uu_uniq-0.0.4 (c (n "uu_uniq") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0dx00kvj2cwky5gxmfjx00xjzcqgzama4wv6ia5lcrnal2q4irjx")))

(define-public crate-uu_uniq-0.0.5 (c (n "uu_uniq") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0l2x4m3264bfvka92l2ddaz5nzc1q329a55n3srbrmx44g8sf26k")))

(define-public crate-uu_uniq-0.0.6 (c (n "uu_uniq") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1k6ch765h7xsyjv7wjiwgkgisnydc28lflncn9iksyjrkgxhcfzc")))

(define-public crate-uu_uniq-0.0.7 (c (n "uu_uniq") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1slp8y1fana8dki2x09jr45s2q3gjjhgwj77qamiv9qvx8986hq2")))

(define-public crate-uu_uniq-0.0.8 (c (n "uu_uniq") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1y35njax4bbma0989388cyh5ykrdk5gpwr1xahgl23lvr59hqy7r")))

(define-public crate-uu_uniq-0.0.9 (c (n "uu_uniq") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "112ab2syrr226waxwl1y6wm7zpk3zqbpxlayzqyjpl5cp5bdg9jk")))

(define-public crate-uu_uniq-0.0.12 (c (n "uu_uniq") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0rskys26z41h5jl0vy1aywajhll67p5drv2xifs08gnjnz443imq")))

(define-public crate-uu_uniq-0.0.13 (c (n "uu_uniq") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1nsxwv3w71snlxyhfnsnqvvki8jdhlgdbv98dibbvymqhmvdfcfz")))

(define-public crate-uu_uniq-0.0.14 (c (n "uu_uniq") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "072lps2z3grjcssc9gz29q5hzikipm5fy9pg1hhf2zv07fc0rig7")))

(define-public crate-uu_uniq-0.0.15 (c (n "uu_uniq") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "09mzgdd3nlh9xsa5y97cvyrn55c5xmdy2p5kyr73cdqvhdw42hnk")))

(define-public crate-uu_uniq-0.0.16 (c (n "uu_uniq") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "19iqllifh5p628v9gcxzc6q88ks5z7w1rrcw91y4i73m5dj2lsgn")))

(define-public crate-uu_uniq-0.0.17 (c (n "uu_uniq") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1vq66q1hdpgc92ynzqrs6hprm0sbcvw2lmj9wg4yc2lcz91csy3z")))

(define-public crate-uu_uniq-0.0.18 (c (n "uu_uniq") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "07ji58wh498jfx45b51si0hi3q80klbjzcqcdidm96carlgm4y7i")))

(define-public crate-uu_uniq-0.0.19 (c (n "uu_uniq") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "048b1jmpbk3z4ix721vs64xl1y2n26np5lzb4f0v2rfnqdacm6xj")))

(define-public crate-uu_uniq-0.0.20 (c (n "uu_uniq") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "07rv934fq0j1crx4gl5nkf11p5l713hg3r0s58aj3jac3apsf5l6")))

(define-public crate-uu_uniq-0.0.21 (c (n "uu_uniq") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0f2xbrv8pf88kyqc0prhh2i3zp8g26sz1afdv133gs6ksg8b9cb7")))

(define-public crate-uu_uniq-0.0.22 (c (n "uu_uniq") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "157pqnnwgaswj91k8p9w4irrhpxaq1v101zz79bdmbl42as24j0s")))

(define-public crate-uu_uniq-0.0.23 (c (n "uu_uniq") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0b3zbmhnprbrmg1713rq3fwg251nihlchcjysgs5v8b4qd4496xx")))

(define-public crate-uu_uniq-0.0.24 (c (n "uu_uniq") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1zgxn7324nxq1hv4wkw8rd11gk81vb4sxqnfsha4347ww7scca2v")))

(define-public crate-uu_uniq-0.0.25 (c (n "uu_uniq") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0i1b73b9vqqr98hnl4h28k48w7r27lmigc5kywmf0539xi7a2dzc")))

(define-public crate-uu_uniq-0.0.26 (c (n "uu_uniq") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "01vx1laip00ndg32zbw5wms7pj0v8ja70gal0m4iwx78js96859h")))

