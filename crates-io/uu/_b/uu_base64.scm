(define-module (crates-io uu _b uu_base64) #:use-module (crates-io))

(define-public crate-uu_base64-0.0.1 (c (n "uu_base64") (v "0.0.1") (d (list (d (n "uucore") (r "^0.0.4") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "037rl2m1kn2hi1fsx1bfa3s2fy3whmx0fm0ni0fn8k78h6p6cwjy")))

(define-public crate-uu_base64-0.0.2 (c (n "uu_base64") (v "0.0.2") (d (list (d (n "uucore") (r ">=0.0.5") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0zbli81zr39mrdimia40gv2m5nl2imxnkfmp15wy8vrw2fxyx9s7")))

(define-public crate-uu_base64-0.0.3 (c (n "uu_base64") (v "0.0.3") (d (list (d (n "uucore") (r ">=0.0.6") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0xp9k6nspxx8bks1xx24jarg9aa2y15z4parhxqapzxq79l9f88a")))

(define-public crate-uu_base64-0.0.4 (c (n "uu_base64") (v "0.0.4") (d (list (d (n "uucore") (r ">=0.0.7") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "177n70wgr82xla1v6kafldin29mdaxmac6y67gf1xvxllx3f4gcc")))

(define-public crate-uu_base64-0.0.5 (c (n "uu_base64") (v "0.0.5") (d (list (d (n "uucore") (r ">=0.0.8") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0mbyj9v908naysnhxbj1i5hnpgm9gzgb06wgx6sd7flwymxfhbyd")))

(define-public crate-uu_base64-0.0.6 (c (n "uu_base64") (v "0.0.6") (d (list (d (n "uucore") (r ">=0.0.8") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "18npvl14avdchm878xlbd61bwf3r83rk2pi43v643jiw47f3xj38")))

(define-public crate-uu_base64-0.0.7 (c (n "uu_base64") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.6") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.9") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "04m5qdrzbf33kvrs1q1d6wlz9gyj864a4pf16jm2pdnx0xbi3hyp")))

(define-public crate-uu_base64-0.0.8 (c (n "uu_base64") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.8") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.10") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0lw7zzyyzmy1knvd0gwr2xl0zrc66ypg5m1zflg4a1im629dl13p")))

(define-public crate-uu_base64-0.0.9 (c (n "uu_base64") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.8") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "17wa4d90crg0san6jsm34ahc84lydnp6i8qy4hlryxx5q3pdhiw0")))

(define-public crate-uu_base64-0.0.12 (c (n "uu_base64") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.8") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "03frxjwsxf0d80xkzl05yj0v2ng31fjj01wkkr33kflkp0vrlqv0")))

(define-public crate-uu_base64-0.0.13 (c (n "uu_base64") (v "0.0.13") (d (list (d (n "uu_base32") (r ">=0.0.8") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1nhjj7hdhcy7nv15jnzzr6s15pzalxgmfcmqvjbjy0y74n2wh3br")))

(define-public crate-uu_base64-0.0.14 (c (n "uu_base64") (v "0.0.14") (d (list (d (n "uu_base32") (r ">=0.0.8") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1ihfn135xl37nzhxgg7a26db3bm4br8g1yymzfr05lm44ncpnsq3")))

(define-public crate-uu_base64-0.0.15 (c (n "uu_base64") (v "0.0.15") (d (list (d (n "uu_base32") (r ">=0.0.15") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.15") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0x8r6hl9jdszihzf9w3c91pbwfngj5s4dgcq5ayymlh9fd8daajm")))

(define-public crate-uu_base64-0.0.16 (c (n "uu_base64") (v "0.0.16") (d (list (d (n "uu_base32") (r ">=0.0.16") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.16") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "11ahj9znlkqvh09bynpdbarq1zpia5qwvjhsjbnyd275xj5p6a8r")))

(define-public crate-uu_base64-0.0.17 (c (n "uu_base64") (v "0.0.17") (d (list (d (n "uu_base32") (r ">=0.0.17") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.17") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "01mf1290x5a8lgbwdca8kgry68qaagl675v54gh6cqd8xpi2l9mp")))

(define-public crate-uu_base64-0.0.18 (c (n "uu_base64") (v "0.0.18") (d (list (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1idzsk3axvra32ysyb9j8li5n74pv8r7wz34a0vsybiga167qmzh")))

(define-public crate-uu_base64-0.0.19 (c (n "uu_base64") (v "0.0.19") (d (list (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "02rnasib9kfxl4j5i2ijcjvcmnfnykv56mbxzb30vyxyvq8rssz8")))

(define-public crate-uu_base64-0.0.20 (c (n "uu_base64") (v "0.0.20") (d (list (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0xyq0wppkc3gjg8sihkggdrk1kszzy4b7l1x76fsmls7mjka2a22")))

(define-public crate-uu_base64-0.0.21 (c (n "uu_base64") (v "0.0.21") (d (list (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0j80mxrg3lz4pva7dz3zaw7y3ydv5gri5s3m3bpbxy2kfc8541i0")))

(define-public crate-uu_base64-0.0.22 (c (n "uu_base64") (v "0.0.22") (d (list (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0c6mi9gadhciyaqwfl486w8pigka79l44ifjkw21hcvrdksp0rfm")))

(define-public crate-uu_base64-0.0.23 (c (n "uu_base64") (v "0.0.23") (d (list (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "12chyc6wj0k3jnmb2yms4ss5rgfn9vl9lds4y0nwbhc31swp3qzz")))

(define-public crate-uu_base64-0.0.24 (c (n "uu_base64") (v "0.0.24") (d (list (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0xxcs5zyw7pp52kjzm5mhpz5gqz7fqmczy1aapj8yqnzr79qrsj3")))

(define-public crate-uu_base64-0.0.25 (c (n "uu_base64") (v "0.0.25") (d (list (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0hv2j0n3pdd06h97464mvw3jm10zwlyxk782v20cja3zn5xh8xvs")))

(define-public crate-uu_base64-0.0.26 (c (n "uu_base64") (v "0.0.26") (d (list (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1gqs8gwllcafvdiswn8xfdyqnb4kh9b6wp1vcn7ldn85vlz4z86x")))

