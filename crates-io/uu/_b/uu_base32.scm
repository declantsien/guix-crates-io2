(define-module (crates-io uu _b uu_base32) #:use-module (crates-io))

(define-public crate-uu_base32-0.0.1 (c (n "uu_base32") (v "0.0.1") (d (list (d (n "uucore") (r "^0.0.4") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0fwx7iklal8g4yj8pc2xjpj8f55mp09l2bsvfsg4vcxldrs8hk6w")))

(define-public crate-uu_base32-0.0.2 (c (n "uu_base32") (v "0.0.2") (d (list (d (n "uucore") (r ">=0.0.5") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1cb31nz47a5id2p58slficppgwpc61xnnkx3jjn0z748rpn97r9z")))

(define-public crate-uu_base32-0.0.3 (c (n "uu_base32") (v "0.0.3") (d (list (d (n "uucore") (r ">=0.0.6") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1lfb4ass6yaf3i0m9dc1c4bigsmbk45qp4fn6qnhvvn36rwfrr4p")))

(define-public crate-uu_base32-0.0.4 (c (n "uu_base32") (v "0.0.4") (d (list (d (n "uucore") (r ">=0.0.7") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1lp1dvvqp25846xk16w4iyq03lhypgay1lcnqqcrzb1q2rbg0qy6")))

(define-public crate-uu_base32-0.0.5 (c (n "uu_base32") (v "0.0.5") (d (list (d (n "uucore") (r ">=0.0.8") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1bab9fqrd67536bf3h909wfazwdfgjp8cgpfgj695cyc4g3jcrg0")))

(define-public crate-uu_base32-0.0.6 (c (n "uu_base32") (v "0.0.6") (d (list (d (n "uucore") (r ">=0.0.8") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "16hry1s6ywgay71j7ib628b1aab4yz25jvr4swvifikm63ddpnal")))

(define-public crate-uu_base32-0.0.7 (c (n "uu_base32") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1gjd5qbrp85c8gblmjv94nbhn14qkq26cabr9zb63dmjwsysbdfb")))

(define-public crate-uu_base32-0.0.8 (c (n "uu_base32") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "14fa2j0kaqcjcw0z2d1z3c2bf6nvvjvdaqla1ir5v8n1anb8ddqf")))

(define-public crate-uu_base32-0.0.9 (c (n "uu_base32") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0lkcx6sl7l0755c72m1abp3hcjxrxg621mh3fy809nzrrgzv1xc4")))

(define-public crate-uu_base32-0.0.12 (c (n "uu_base32") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1x95hy4wdd5w8nh4jgaxzaba12a0b59hsl8yi6bif6ln4kfsq7jf")))

(define-public crate-uu_base32-0.0.13 (c (n "uu_base32") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0jvxqrdl2hdfv7snd83w8vznr1cqk62ap2iri1rn12qcy10afy3v")))

(define-public crate-uu_base32-0.0.14 (c (n "uu_base32") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0nslgxr8mvjyxd9ybavkzr75w8h2a5b6al9bvhn63rssxdndyblj")))

(define-public crate-uu_base32-0.0.15 (c (n "uu_base32") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1n3llamnj7x585d6n7fb3ak9j4c9w6gmg0fpd8d1zqkrihqa43w7")))

(define-public crate-uu_base32-0.0.16 (c (n "uu_base32") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1xfn1zpp5xdlnnzn2xwndr0g7ki18jf40lw7qxrsqi8g3532lh62")))

(define-public crate-uu_base32-0.0.17 (c (n "uu_base32") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "18s3257i7ypks44qz5d0vhd0qq0vvqv972plqppsvkn8cajpiqis")))

(define-public crate-uu_base32-0.0.18 (c (n "uu_base32") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1v6fd4nmplaja0jvjip37vyallql3v8ns1lgdc3yq14klhhyqx21")))

(define-public crate-uu_base32-0.0.19 (c (n "uu_base32") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "091hi2q5zh0k1p69y6s9xch60bllwkxcgwiyhnzj1r8zh5w26ri3")))

(define-public crate-uu_base32-0.0.20 (c (n "uu_base32") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1gf5hvys96pi6ylyc4v7jnbx0pnvzij3szsjy3jdv1yvxm2wrx4h")))

(define-public crate-uu_base32-0.0.21 (c (n "uu_base32") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1h37j3vlk2nmg80l1jg22pidnr68qpq9lkqpxzii12afg96cpd5c")))

(define-public crate-uu_base32-0.0.22 (c (n "uu_base32") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "03lbjh7f18bg0a3zfr3jigdvymk9l4byfvvgawzf6rhi0a7j9vq1")))

(define-public crate-uu_base32-0.0.23 (c (n "uu_base32") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "07d05w65qb8d9sljklk9dk9h9ghz6d3qaqfq9gf62hnzxq5bvrs8")))

(define-public crate-uu_base32-0.0.24 (c (n "uu_base32") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0glgbfryz9vf6016pqqf9p5rfsxa46fkw3lwq4dxlpmningxyqm8")))

(define-public crate-uu_base32-0.0.25 (c (n "uu_base32") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "11838v8nj8cmyjkf5z457sh1frya8jaqwbmadwali3pjd8vcnvq6")))

(define-public crate-uu_base32-0.0.26 (c (n "uu_base32") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1j3zws1y43k463hg72g94yk2mawdd05j4z0f1j3yjllymc8xi6lx")))

