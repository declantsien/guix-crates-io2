(define-module (crates-io uu _b uu_basename) #:use-module (crates-io))

(define-public crate-uu_basename-0.0.1 (c (n "uu_basename") (v "0.0.1") (d (list (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0fmklzaymphkawllg5pd4dnclz75iw8ym753sb5im58lfl6gh12f")))

(define-public crate-uu_basename-0.0.2 (c (n "uu_basename") (v "0.0.2") (d (list (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1qaaw9ll27wn3ca8lpq73g9pfznj8ic92366w7d0zydqhgz9h91p")))

(define-public crate-uu_basename-0.0.3 (c (n "uu_basename") (v "0.0.3") (d (list (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "16a2knrnxy7yi5lw2g0v561nzciy2s9mm9bfxf68wyqdfkgh15xx")))

(define-public crate-uu_basename-0.0.4 (c (n "uu_basename") (v "0.0.4") (d (list (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1hv92cnp40chzi47342wlpihngcgpmdsi75240p39j1awv2b2k5p")))

(define-public crate-uu_basename-0.0.5 (c (n "uu_basename") (v "0.0.5") (d (list (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1s8s3v6sh1n9wphf8z9r0bdbvw17mrmzs2f0vjxlh10zbrbkxm5x")))

(define-public crate-uu_basename-0.0.6 (c (n "uu_basename") (v "0.0.6") (d (list (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0n1h6pdmywa7g4h6914q0mgnlswyz1yy3asyf1raqf0byqkw06fz")))

(define-public crate-uu_basename-0.0.7 (c (n "uu_basename") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1rql3yq1w2d914i733cdqs8rwg7qnsi4c20i030fzd7yljwf0iyi")))

(define-public crate-uu_basename-0.0.8 (c (n "uu_basename") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0iblcd74qc9hkgw8pqsjr5ydz1ii710jvq3s689gmhhz1lfz389i")))

(define-public crate-uu_basename-0.0.9 (c (n "uu_basename") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1sjwwrkw7blxh2dwkv7lhcpxzylfkhjrkvq1qyw6kharhsxcl8h5")))

(define-public crate-uu_basename-0.0.12 (c (n "uu_basename") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1cliw5z7yjxzw7ri0dg0inpibaai2zdwmhw8cha465lmzh2l00hq")))

(define-public crate-uu_basename-0.0.13 (c (n "uu_basename") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "02c26xa7y82n90wwq34pb6g6pcgc3ks9lm3id7gir56518mdqqw4")))

(define-public crate-uu_basename-0.0.14 (c (n "uu_basename") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1kk6fxxkk24gz8zazy357z05m91irgqji7nkgam5j3vv21kc8x27")))

(define-public crate-uu_basename-0.0.15 (c (n "uu_basename") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0rkdn8q5dssrs7kvyna7vp744yjn1b4l45kmkcpg567fpizcnn0f")))

(define-public crate-uu_basename-0.0.16 (c (n "uu_basename") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1n4f46rz55lgca6y4n28bjjh152s7ksrlr9y1zzk3d4izx2zlk21")))

(define-public crate-uu_basename-0.0.17 (c (n "uu_basename") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1a389symlsrwkpdda1bhl22r8y5zsnzvqj085wbybx3jc5ank4kb")))

(define-public crate-uu_basename-0.0.18 (c (n "uu_basename") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "187zy4ad3ixgmcv7kqxpclbqscsvfvc8rxfff4a0dv2swzr44x4m")))

(define-public crate-uu_basename-0.0.19 (c (n "uu_basename") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "12ywpmkydhv7hpc5d53svcdzvllgksqckb7lyj74mral0ghqqhgf")))

(define-public crate-uu_basename-0.0.20 (c (n "uu_basename") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0b6wwawrhq3nbj6frlcj1slp1ra4dzb0d94sbcab4r8h44rhig4g")))

(define-public crate-uu_basename-0.0.21 (c (n "uu_basename") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0cl32jbql4c8wn1lwazkrw6cflgk11v7wpbd4q0v8kmbxjw0xvfy")))

(define-public crate-uu_basename-0.0.22 (c (n "uu_basename") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "06jizc6rbi5kmbdhx9lwxl0h6igw673bi01bflapxq33hfk59cl9")))

(define-public crate-uu_basename-0.0.23 (c (n "uu_basename") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1a0fh0jbkdwnmpm284a02v946amlpfc13wi5w7rh712msb51lwg4")))

(define-public crate-uu_basename-0.0.24 (c (n "uu_basename") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1biprxwdsw1p68v1kgd1hmckp0rg2qwbfz4p641cm0k0xd67hmjf")))

(define-public crate-uu_basename-0.0.25 (c (n "uu_basename") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0dnr173v3951ri7hamj7hh31df21qzr7zg0axxmfwkdjw43rn61g")))

(define-public crate-uu_basename-0.0.26 (c (n "uu_basename") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1mwiivgqgmjnih9jj4jsiqib72b74hqyvgmrk15vvmv7xdkqjp0r")))

