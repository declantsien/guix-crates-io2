(define-module (crates-io uu _b uu_basenc) #:use-module (crates-io))

(define-public crate-uu_basenc-0.0.8 (c (n "uu_basenc") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.8") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.10") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0byr5cdsqy55xwa7dzx4b2dvf892mlqb4z3hc8bfw4jsv4rvlfar")))

(define-public crate-uu_basenc-0.0.9 (c (n "uu_basenc") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.8") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1ld07i7g4xynmvn5zkmcmvnhk26cjirjwq0kfdc7k5j8zv3139xv")))

(define-public crate-uu_basenc-0.0.12 (c (n "uu_basenc") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.8") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0bicx6dm38nr1b8x0dhc2k3xdckx89rsbs24c7yiq4gjx7k1dpgp")))

(define-public crate-uu_basenc-0.0.13 (c (n "uu_basenc") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.8") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "06f1bhgwikyvagncldallx7fyjl2l8s6aybphhabjs8yglhymfgm")))

(define-public crate-uu_basenc-0.0.14 (c (n "uu_basenc") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.8") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.11") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0k4kdrsy2g3h3c1xhzz7ah3j2apnrb6ymf7h2qwdqihzvi580l72")))

(define-public crate-uu_basenc-0.0.15 (c (n "uu_basenc") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.15") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.15") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "08pyspvjycbxcrkk7skn7ia40rn8gshbfz0j6mgdyxnsyx3bkfsi")))

(define-public crate-uu_basenc-0.0.16 (c (n "uu_basenc") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.16") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.16") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0367wsy7gj4yx0cir4x45acb3w3wq7rfqs90bpql3hd1ayrlgzsy")))

(define-public crate-uu_basenc-0.0.17 (c (n "uu_basenc") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.17") (d #t) (k 0) (p "uu_base32")) (d (n "uucore") (r ">=0.0.17") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0g8wjpwrkv9j7994hw8lv32z37srs1781a5l4glv6iifxx0jmxgr")))

(define-public crate-uu_basenc-0.0.18 (c (n "uu_basenc") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "14ji6md72cbkvsrcr0v9vhd2idcnxv8xm3i0pvvhnra6sd4mqv5j")))

(define-public crate-uu_basenc-0.0.19 (c (n "uu_basenc") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0406qxn92qq7j8lr049xlxq82nnqg8i7ni7r15q5dmfy7n1y0bvk")))

(define-public crate-uu_basenc-0.0.20 (c (n "uu_basenc") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1zw832jgv5wlkr82cs0ckfdnf301vsiw5jr0rf3ixjgaimj6s2kd")))

(define-public crate-uu_basenc-0.0.21 (c (n "uu_basenc") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0zzhf79fn8g8ap05ymk6r7f5f9i4z6zrbpg9sylhxvbif0sdiyxa")))

(define-public crate-uu_basenc-0.0.22 (c (n "uu_basenc") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "0gwvjsikc0hafwsmvpnn9gac9g4i6z16ahxd2av6xrxw2xj2vhr8")))

(define-public crate-uu_basenc-0.0.23 (c (n "uu_basenc") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1c2d7vyksmpqh9zc4af0nlrgqsy57hv4mpwyhwfjlwzml5p9i0q8")))

(define-public crate-uu_basenc-0.0.24 (c (n "uu_basenc") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1c4lggpiv6xkn2qj0a7a2250xq0cclnhmijm619k6p39k7bqxqxq")))

(define-public crate-uu_basenc-0.0.25 (c (n "uu_basenc") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "1g5q8ldxwj20h1vjbrs06wvvkrw8z29mjrjnyly6fxd202n9bc1b")))

(define-public crate-uu_basenc-0.0.26 (c (n "uu_basenc") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uu_base32") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding"))) (d #t) (k 0) (p "uucore")))) (h "05dzfmbs6kmvyjvbqak4v3rna54w7r9zjk23nlrk8lqxlgvxmn19")))

