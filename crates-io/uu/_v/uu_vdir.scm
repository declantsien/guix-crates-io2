(define-module (crates-io uu _v uu_vdir) #:use-module (crates-io))

(define-public crate-uu_vdir-0.0.14 (c (n "uu_vdir") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "selinux") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.14") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "0ii1f9vhvgihvwnfk4awz2k8l723q0idkj4xwd178sdnw372znks")))

(define-public crate-uu_vdir-0.0.15 (c (n "uu_vdir") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "selinux") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.15") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1qsb51xsw4j6czxfddnmvcm0m6g3pidjnj93dq4dsr5s2ff3x0vq")))

(define-public crate-uu_vdir-0.0.16 (c (n "uu_vdir") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "selinux") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.16") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1yn1r4hifv9vva1scxg2sv9x9czslf8sdpmrmc75y582cz7jnpck")))

(define-public crate-uu_vdir-0.0.17 (c (n "uu_vdir") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1xsfyfq0rz3910y53zd1wfra43l2p6ypd7v7zl1qx630dg9bxxb4")))

(define-public crate-uu_vdir-0.0.18 (c (n "uu_vdir") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "00qs10xva2lpf6sdrqnrc1wx1n3hqv6hm2daddrrj9ihzchpqyvr")))

(define-public crate-uu_vdir-0.0.19 (c (n "uu_vdir") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "0ixjlvx1lxyz8h70nsy6qmp78c62zw0qihibqqnp8r3fi6h9cyxw")))

(define-public crate-uu_vdir-0.0.20 (c (n "uu_vdir") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "06165f1v14vc0swbip7zy6kk7b9y2v9j0fkn83flk1qs5rv033ji")))

(define-public crate-uu_vdir-0.0.21 (c (n "uu_vdir") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1gsr2ymlnlxy95w4f73is3dm3n82k4d8jnjh9rs7lk80aq5cdjzi")))

(define-public crate-uu_vdir-0.0.22 (c (n "uu_vdir") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "0yrhy41kanraqnsh4byc4x5c1kf7nwwx9kd8id94c1c51agbxdnn")))

(define-public crate-uu_vdir-0.0.23 (c (n "uu_vdir") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "028s6z25k3nsjmgfsmczkpd09kb1wbh9prc7b1ji92ksf542nzma")))

(define-public crate-uu_vdir-0.0.24 (c (n "uu_vdir") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "138yzhj2nbfki7klb10r54yiwps0mr0cbbgaplxn6jawidh0cqnw")))

(define-public crate-uu_vdir-0.0.25 (c (n "uu_vdir") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "120829r446999bdqp1jnq6xmyha7crhc2wvhcyz0i6r9wkv7xkvh")))

(define-public crate-uu_vdir-0.0.26 (c (n "uu_vdir") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "070hlqb1macj010pi6l073wx82hy6gp66d0xxlh75r9wnswjiybh")))

