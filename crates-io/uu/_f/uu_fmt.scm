(define-module (crates-io uu _f uu_fmt) #:use-module (crates-io))

(define-public crate-uu_fmt-0.0.1 (c (n "uu_fmt") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1pmnzlyq3jz4b248vasf1f6n4rpyfcz9c18ijjaj4i1kmmvl99z2")))

(define-public crate-uu_fmt-0.0.2 (c (n "uu_fmt") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1ic1532ix1jqfvj8fr6prdcbxmd627xn78xsqj11325hmkmrj2ii")))

(define-public crate-uu_fmt-0.0.3 (c (n "uu_fmt") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "16i68qa8cyl7i8047imprlhxx4y1pds7zzhp2ax4md7akbgigy4z")))

(define-public crate-uu_fmt-0.0.4 (c (n "uu_fmt") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0m1zqriqcbi5l97cx75jnbfqg78m7j7l4lpipl6pf56hdnhcivh0")))

(define-public crate-uu_fmt-0.0.5 (c (n "uu_fmt") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0hvqvdsjz9wgndp9g23lyfn8cb64h3sbh360g5p09w1fdxs3n5ly")))

(define-public crate-uu_fmt-0.0.6 (c (n "uu_fmt") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "027zzalldpnsa832va079f08q0q54nlaiqi2380fq97mq2cb85wi")))

(define-public crate-uu_fmt-0.0.7 (c (n "uu_fmt") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0qpm54a2g915b3hx7623wbdx0jvh8i3pbq4n2hrslrgpn4n9fdaq")))

(define-public crate-uu_fmt-0.0.8 (c (n "uu_fmt") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0wyj6b5zgrk1d4laivv23ym3lzch2s2d2rlsqhan7al26cvyw2zw")))

(define-public crate-uu_fmt-0.0.9 (c (n "uu_fmt") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0wpha12y4b8irpyygwfi8ajx1i704amgqi6jn3hawkqv754nx6ah")))

(define-public crate-uu_fmt-0.0.12 (c (n "uu_fmt") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1i1b2iz66y4vh8j3xx89nw05mpxx7dfxkyd4ybsbfpk7qbp8xvsg")))

(define-public crate-uu_fmt-0.0.13 (c (n "uu_fmt") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1pzpgi42s2fic5rfbdg3r2ph4cjydmfshw0acn8hmqm0s7kyfvv7")))

(define-public crate-uu_fmt-0.0.14 (c (n "uu_fmt") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "05yz2x1sw4yh0dmxfjl827a051qrf82j452bm5fnf0wpjk4jl5mr")))

(define-public crate-uu_fmt-0.0.15 (c (n "uu_fmt") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1lf07d4a6cnsq3n2gvrvc9gzkzwm2c3nn73z1lqljnnyfq68lva1")))

(define-public crate-uu_fmt-0.0.16 (c (n "uu_fmt") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "0snp00hqqip22c08hqnq5f60dfs1j9s4zknyfcsw267j9nh0fcrz")))

(define-public crate-uu_fmt-0.0.17 (c (n "uu_fmt") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1mgq8mnxwkg365vy4gbcljqd0j3q7siwn1xvrmxaifirn9wpr64z")))

(define-public crate-uu_fmt-0.0.18 (c (n "uu_fmt") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1d1n38rjd19liwzi2vz9r9d6g063bfk6wv5nx7a0x34an2gnal8b")))

(define-public crate-uu_fmt-0.0.19 (c (n "uu_fmt") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "03xvk4lz6p37wghnw6r829lzfv7kxqcg91nzhimm5zjl87m57rjf")))

(define-public crate-uu_fmt-0.0.20 (c (n "uu_fmt") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "01k9585caj0pfhd5smi65v6653hq52jkl8hyxl3gwcnp6197i3ik")))

(define-public crate-uu_fmt-0.0.21 (c (n "uu_fmt") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1w1lh29k2hbjwyabs5yr5dwc1azsw98v644zidlflzw1ikp7b3cm")))

(define-public crate-uu_fmt-0.0.22 (c (n "uu_fmt") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0lgww9vfkxvgqpacy9gz0rg01g0ay2yqdii2w2s2zm2nv2j6v6p3")))

(define-public crate-uu_fmt-0.0.23 (c (n "uu_fmt") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0b4d20wn6m7gcidbm1hmwfazn1mcnxg7755s5dzvjkm3f0p7qc1p")))

(define-public crate-uu_fmt-0.0.24 (c (n "uu_fmt") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1fy3zzn5snpz114c2zbjvyfgb4x77ms67ir20bilhmllxvrawv9s")))

(define-public crate-uu_fmt-0.0.25 (c (n "uu_fmt") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "18hyxh1gb4mwvpqqd1k1jdl1xfn0pf2nmi409lwjgdc8jv7sv73j")))

(define-public crate-uu_fmt-0.0.26 (c (n "uu_fmt") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "145gqr527djwhpy9nd45az8py9ilisnsm721r221v74hljbbh084")))

