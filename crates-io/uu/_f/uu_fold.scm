(define-module (crates-io uu _f uu_fold) #:use-module (crates-io))

(define-public crate-uu_fold-0.0.1 (c (n "uu_fold") (v "0.0.1") (d (list (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1wxbq79g7r4s1fr79b9qiqhq5b4jc8kvkqaylh6qmxy5sfyh6c7a")))

(define-public crate-uu_fold-0.0.2 (c (n "uu_fold") (v "0.0.2") (d (list (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1pf232526iwf67w6cc3yahr16lqs676b385bp12lmkhwmriw7si6")))

(define-public crate-uu_fold-0.0.3 (c (n "uu_fold") (v "0.0.3") (d (list (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1l4l8cvrcgia7bn1hmgakhyih1f0capj5gi39zg3p82by2d72lbn")))

(define-public crate-uu_fold-0.0.4 (c (n "uu_fold") (v "0.0.4") (d (list (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0dam92qwjdx8yfwmhqyi2h6ac2b79l0a43gasx9vc76l4n9nnb9n")))

(define-public crate-uu_fold-0.0.5 (c (n "uu_fold") (v "0.0.5") (d (list (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "04q13z439zky00003snrrib1n0iaypjnqay6sabvcwjq35l174rv")))

(define-public crate-uu_fold-0.0.6 (c (n "uu_fold") (v "0.0.6") (d (list (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1hh3rrvbsdbmxc9alnb493py8ni07sxralbi73fj517msmppaphb")))

(define-public crate-uu_fold-0.0.7 (c (n "uu_fold") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0msdyn49l6ck9f981f3ah2k0kl17wh11ch04242m9rgpy3kwngl8")))

(define-public crate-uu_fold-0.0.8 (c (n "uu_fold") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "086kh9c0bv59i1zx9nj2y9a6gjn7yf8nslhn1by3cjcghfrlhzsj")))

(define-public crate-uu_fold-0.0.9 (c (n "uu_fold") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0san4wsy0v9xwx4h7rc2pyxnrh9b0hrri061ad9mmhsf0i2bcpch")))

(define-public crate-uu_fold-0.0.12 (c (n "uu_fold") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "18xlg36snrziijci11jny5xqiagyd9rz4sc4mk07cqgv303hk2p7")))

(define-public crate-uu_fold-0.0.13 (c (n "uu_fold") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1n3c9gm40l6ii0g61ybvp8ilns1bc529wm09w5789n02cz5im729")))

(define-public crate-uu_fold-0.0.14 (c (n "uu_fold") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1mfapmcgqfdgxlii6g4ky27ld78sidvcbgqq11nj3jnmzpadcwy6")))

(define-public crate-uu_fold-0.0.15 (c (n "uu_fold") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1a173ii7rm017mmckrdb32zsw8xxbw3pgvfcdf9kmv0cdvfa15my")))

(define-public crate-uu_fold-0.0.16 (c (n "uu_fold") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1351292wbj41bx1qrx1fvcpwmkvg1gaf1ydn9v5nz9zav8aqnh96")))

(define-public crate-uu_fold-0.0.17 (c (n "uu_fold") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1j22932b9h7ifgpbmahld7ysrj1rdciqv0vxl4hwsk76ffxhlmri")))

(define-public crate-uu_fold-0.0.18 (c (n "uu_fold") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0dpqh01s1wg4f533cav7n6friq8pj4fd8jq6brv4h03k4g6zisdx")))

(define-public crate-uu_fold-0.0.19 (c (n "uu_fold") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1sm8g82qw6vlrmdiba78amyh747dw7x6mp0a59vn103hqxzbq5c5")))

(define-public crate-uu_fold-0.0.20 (c (n "uu_fold") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0f2a1q5jvm333sghpg897wl3x4ibj4hfsjyjsglxlwn51kyib93s")))

(define-public crate-uu_fold-0.0.21 (c (n "uu_fold") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0dsh5f8sz7dcgsl3inz7jm2gd259pm5dfqvcyyd3n1nk2cm6dqk2")))

(define-public crate-uu_fold-0.0.22 (c (n "uu_fold") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0fyx844k5jkw1fmdqxrfy3vg4zi16i8h8vfkmg7zblzl8bbrh52v")))

(define-public crate-uu_fold-0.0.23 (c (n "uu_fold") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1f3x2bspjk8g88r9c6q0w5rzwpqli5nijiwj8qc6y7q7r61fkc1f")))

(define-public crate-uu_fold-0.0.24 (c (n "uu_fold") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1xrf5f7bs6lqklkdnjpk6n4n1pljxybg6xmy6x1kajw85iy4pbkp")))

(define-public crate-uu_fold-0.0.25 (c (n "uu_fold") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0a6p6z9qh1xhszwkz5p0bfhvzr9vbd9nbfpf1jib42z20c6x0zwv")))

(define-public crate-uu_fold-0.0.26 (c (n "uu_fold") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1by29872w1dxhxzddsnnjmbffcz71anw01h3zapr7xpq9sk469p5")))

