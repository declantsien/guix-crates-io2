(define-module (crates-io uu pd uupdump) #:use-module (crates-io))

(define-public crate-uupdump-0.1.0 (c (n "uupdump") (v "0.1.0") (d (list (d (n "html_editor") (r "^0.4") (d #t) (k 0)) (d (n "rphtml") (r "^0.5") (d #t) (k 0)) (d (n "table-extract") (r "^0.2") (d #t) (k 0)))) (h "1ldwgf7bhsiv7ypa3rcdqz35gjvdfgx8f1z7k79xli6p85bn6pw6")))

(define-public crate-uupdump-0.1.1 (c (n "uupdump") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "html_editor") (r "^0.4") (d #t) (k 0)) (d (n "rphtml") (r "^0.5") (d #t) (k 0)) (d (n "skim") (r "^0.9") (d #t) (k 0)) (d (n "table-extract") (r "^0.2") (d #t) (k 0)))) (h "02x8y3y5hijikx7jsc9svggpvjzi1xnpfjbkakfzj295pnfd9f5h")))

(define-public crate-uupdump-0.1.2 (c (n "uupdump") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "html_editor") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rphtml") (r "^0.5") (d #t) (k 0)) (d (n "skim") (r "^0.9") (d #t) (k 0)) (d (n "table-extract") (r "^0.2") (d #t) (k 0)))) (h "19plsx2m52k9007k26436v1vcb1gzxvqcggi34b5d5rz4x7h3zh3")))

(define-public crate-uupdump-0.1.3 (c (n "uupdump") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "html_editor") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rphtml") (r "^0.5") (d #t) (k 0)) (d (n "somehow") (r "^0.1") (d #t) (k 0)) (d (n "table-extract") (r "^0.2") (d #t) (k 0)))) (h "00d9x7am15vjk67nnilimizib138iyzknwfh3hxw918adrvp2dli")))

