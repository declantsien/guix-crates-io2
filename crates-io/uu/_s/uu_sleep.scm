(define-module (crates-io uu _s uu_sleep) #:use-module (crates-io))

(define-public crate-uu_sleep-0.0.1 (c (n "uu_sleep") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("parse_time"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0373n58w1c8czi47a2ff6wcjcmafa4a8li7qcnrffvq7v9mc0nml")))

(define-public crate-uu_sleep-0.0.2 (c (n "uu_sleep") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("parse_time"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "03jh5wb3rjrvw92narf4qvscxijcz90yddnzp6lbcgncp2sdiknm")))

(define-public crate-uu_sleep-0.0.3 (c (n "uu_sleep") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("parse_time"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "181hniwi75i58m3a3k3m8d9iiahmxsapkb3c1gnkzrsj8xf99v80")))

(define-public crate-uu_sleep-0.0.4 (c (n "uu_sleep") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("parse_time"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0rd9dlfd7mx8wazf2hdlvdwdpddcck7qg6n41vi2vyxkg1d56n83")))

(define-public crate-uu_sleep-0.0.5 (c (n "uu_sleep") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("parse_time"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "14pvkl0q23jhkhzsan19l52nnr69hinciw2z7pd7wd4vy0v2w6n0")))

(define-public crate-uu_sleep-0.0.6 (c (n "uu_sleep") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("parse_time"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1rdd2ba8grpabh38xjagmiczh72adik449lm0cwy493wb5nyvxb8")))

(define-public crate-uu_sleep-0.0.7 (c (n "uu_sleep") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0m0dqqh9nvwyzg4w9r6470z2svlj5dljdx37as2vk2fc98j1ig3z")))

(define-public crate-uu_sleep-0.0.8 (c (n "uu_sleep") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0w1zlc7clwi886bxk0q3jsqpgrbzk42xz6s0n0klc1ma5d2ccji7")))

(define-public crate-uu_sleep-0.0.9 (c (n "uu_sleep") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1nrszpbbb82ahwzi1wmya25sd31x0srcvv1vd6i2dwxixw6xn9d2")))

(define-public crate-uu_sleep-0.0.12 (c (n "uu_sleep") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0j3x5bqk66xhrx3jw0w49lz7grml4hhvg5s93hnmgz0v6cas51n0")))

(define-public crate-uu_sleep-0.0.13 (c (n "uu_sleep") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0xrk4crhzx277jg94abjd6zcqw2bcqrq6gc7mfdf36vxgfb3jgfc")))

(define-public crate-uu_sleep-0.0.14 (c (n "uu_sleep") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1k0payq8nydj8i72vh60jf9mv0x35hdgg4br8k13dxfjd5vr0v6g")))

(define-public crate-uu_sleep-0.0.15 (c (n "uu_sleep") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1shvlw76ml6bzgpncjdf0ip2n61yznkzzp69fgx54gknsxk96gkc")))

(define-public crate-uu_sleep-0.0.16 (c (n "uu_sleep") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "15j505c6v4paksaxm3yxa2asf9x2wfxipfivg9rfrm609wvcr853")))

(define-public crate-uu_sleep-0.0.17 (c (n "uu_sleep") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "0fjwzzh9k0i9p88b9mlkxv4y3j0r1djy9f0v5zyhdr0nf82mwda9")))

(define-public crate-uu_sleep-0.0.18 (c (n "uu_sleep") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fundu") (r "^0.5.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0m30d640m6r637dlhhazd8nhpanyznla9rxv9n4312rqh3mamr06")))

(define-public crate-uu_sleep-0.0.19 (c (n "uu_sleep") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fundu") (r "^0.5.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0k9prmppj120v4pi0997xdfcrlcgab1832y4fhyqa929zq3ac0za")))

(define-public crate-uu_sleep-0.0.20 (c (n "uu_sleep") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fundu") (r "^1.2.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1xsa17733pgivm0dhfxjzz3a4qkgjfd9qvxk4ingvxvwr9hy687n")))

(define-public crate-uu_sleep-0.0.21 (c (n "uu_sleep") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fundu") (r "^2.0.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0xgwzljxihskn5z50y6jgs883dmdsnik5nxm8w5cni9455ls95wd")))

(define-public crate-uu_sleep-0.0.22 (c (n "uu_sleep") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fundu") (r "^2.0.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1wpas69snhaqfn6h50narankvj2jkmf8f1pl4w7gx8nd357cg9g1")))

(define-public crate-uu_sleep-0.0.23 (c (n "uu_sleep") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fundu") (r "^2.0.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0qasr8w1lcyzrq8x21s86856drqqfzfjrwvznaan06j45givk0rb")))

(define-public crate-uu_sleep-0.0.24 (c (n "uu_sleep") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fundu") (r "^2.0.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "03cr83gwxs6z7swbl2ip42szizaa7kfmlasgfmf5qmsndagrxfyg")))

(define-public crate-uu_sleep-0.0.25 (c (n "uu_sleep") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fundu") (r "^2.0.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0j93fzrhkc850bf687nynvna001c9h1x0kryz39apr8hp8c0fvwf")))

(define-public crate-uu_sleep-0.0.26 (c (n "uu_sleep") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "fundu") (r "^2.0.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0cqyyab1hsizf49flyrv7jxrvk8lf58xa8hyrsdj44l16avbwjw1")))

