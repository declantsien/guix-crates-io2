(define-module (crates-io uu _s uu_shred) #:use-module (crates-io))

(define-public crate-uu_shred-0.0.1 (c (n "uu_shred") (v "0.0.1") (d (list (d (n "filetime") (r "^0.2.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1z40i65gqxk0gwc82wmfs71033j67rx1nj7vk4h8vpbkw39sm42l")))

(define-public crate-uu_shred-0.0.2 (c (n "uu_shred") (v "0.0.2") (d (list (d (n "filetime") (r "^0.2.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "083fv251dq3cgzfyx5931xy0zlizp3zrprjlpj4snj6fkz43kj8f")))

(define-public crate-uu_shred-0.0.3 (c (n "uu_shred") (v "0.0.3") (d (list (d (n "filetime") (r "^0.2.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1fxymdzz25cd2fay1jhafsy34az6h4cchw7h511l2qmyyfpvd1mf")))

(define-public crate-uu_shred-0.0.4 (c (n "uu_shred") (v "0.0.4") (d (list (d (n "filetime") (r "^0.2.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1qwnp0ij50fh6jsglzqg57kz3ykr2xjdhmnxfhd0fh0lxfk3ichn")))

(define-public crate-uu_shred-0.0.5 (c (n "uu_shred") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "filetime") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0m2bldycg4zb568gc9gfhsmiqdy96vfrjh8jd40ynn3xllb540ma")))

(define-public crate-uu_shred-0.0.6 (c (n "uu_shred") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "filetime") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1pmxd7c6bx60ncf0rb50pq04wv3k3c00z8c6nfp90ip45i64s795")))

(define-public crate-uu_shred-0.0.7 (c (n "uu_shred") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "02rf3kf92ss45z1wyx0rwv66xnqn2fnwjqijp17yi4ygkam018ys")))

(define-public crate-uu_shred-0.0.8 (c (n "uu_shred") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0q59nrzmfljldh5d6hv2dcv1mcy9953zfjnzc43ibfhgzd726b6k")))

(define-public crate-uu_shred-0.0.9 (c (n "uu_shred") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0spgdg9vwkdvahyfnmag0p016c9s0z33xdfla73yk08w6v4xgbgm")))

(define-public crate-uu_shred-0.0.12 (c (n "uu_shred") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0zlynd918vqg6ag9zch2wxxxnlb14xhjzwwxwl1xggk2wm1gy7ym")))

(define-public crate-uu_shred-0.0.13 (c (n "uu_shred") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "184bfrqrrzvkj7h3pqi42aa2bsd0s7s42j8b2hprf10p3mdj45vp")))

(define-public crate-uu_shred-0.0.14 (c (n "uu_shred") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0xx77b2fhk3g7bl62vxlarb24qfnjfjd3w1akffaq6lzjlzyyrjf")))

(define-public crate-uu_shred-0.0.15 (c (n "uu_shred") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0yc9xw838ghz8ll25rnpp9q9ddjbygcg0kymkbhdyhcj3qy8b9i9")))

(define-public crate-uu_shred-0.0.16 (c (n "uu_shred") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "0v1gh4q4cv8qgmy1ip8ss65ksmqfzw44lhrs1yk0yzi4h34szfj2")))

(define-public crate-uu_shred-0.0.17 (c (n "uu_shred") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1k1l0wqkixaq7xfvr4m5z5pxpy4spsq44nrilpzr0qq25wx1x5nr")))

(define-public crate-uu_shred-0.0.18 (c (n "uu_shred") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1ba1802kdq3y7qhly3i4d5q6gpk7frk3nf35c845iihviji9cafj")))

(define-public crate-uu_shred-0.0.19 (c (n "uu_shred") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0lqslwrzyb5vxy0kvpcx5r1qgq95ibvvlsbn4fhznzw9fxdww7lw")))

(define-public crate-uu_shred-0.0.20 (c (n "uu_shred") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1qspfgjgb8friv3wkpqbd5c96msmcxkpa7kvkg3pqz30cc93pxw4")))

(define-public crate-uu_shred-0.0.21 (c (n "uu_shred") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "02h3ai58dciiiawczmj0q3fwxp4z6gwfiid063w61z1wlzm2q8hv")))

(define-public crate-uu_shred-0.0.22 (c (n "uu_shred") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0dapmhkapq3g5cy5ry4hsk043sibb5wl6sss8sb3y7kl9hsvqyf3")))

(define-public crate-uu_shred-0.0.23 (c (n "uu_shred") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0jm5aisigmqafrz9abpmmdhp77mawghd3l6s3x87xd70xxr81lw1")))

(define-public crate-uu_shred-0.0.24 (c (n "uu_shred") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1wp6grgqd9h82vd9ma8x4smvwgpyzy6adfsbcn986v1n39xs7nva")))

(define-public crate-uu_shred-0.0.25 (c (n "uu_shred") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1sw3ghwfli1qp2kp7jj6zydc44vv6qs2qm4w5822yn1iq9fwbv6b")))

(define-public crate-uu_shred-0.0.26 (c (n "uu_shred") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1s0sikh147y4r0x49lr3ks1srqzdpw74pwx4ngn0kxifjbhlqg2v")))

