(define-module (crates-io uu _s uu_stty) #:use-module (crates-io))

(define-public crate-uu_stty-0.0.15 (c (n "uu_stty") (v "0.0.15") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("term"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0mx4mpnpbi6mssaz2af12krxzaf1r5xi1nxky2hvs7rcbcwrkgk8")))

(define-public crate-uu_stty-0.0.16 (c (n "uu_stty") (v "0.0.16") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("term"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1zizsplgdn1b1rzzs7h5k9k9rx5kpbxy3ay30s2zxmjfww1vvp0h")))

(define-public crate-uu_stty-0.0.17 (c (n "uu_stty") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("term"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "11fz4ypw7vpilqzh96w7bzybbdmrxkrjawzdmlzchxpjvkyfhblc")))

(define-public crate-uu_stty-0.0.18 (c (n "uu_stty") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("term" "ioctl"))) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0lm9viqfwgd5zg0n9rv8wgh3997hr0md9sbaya7hff523vbvawp0")))

(define-public crate-uu_stty-0.0.19 (c (n "uu_stty") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("term" "ioctl"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1ipg4zz152a1xk55q0r75bd2yn838brnlfp6zsbkvapsi8zf0g2s")))

(define-public crate-uu_stty-0.0.20 (c (n "uu_stty") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("term" "ioctl"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1qy10nczih7z0gklzm9k28lcb2mcb0fylh44j174hdb5smg4hb70")))

(define-public crate-uu_stty-0.0.21 (c (n "uu_stty") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("term" "ioctl"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "15av0i7mm136327cr82r09iz5nk0wyh91ajspkk2md72pn612ki2")))

(define-public crate-uu_stty-0.0.22 (c (n "uu_stty") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("term" "ioctl"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0i0n9wdj85ka1vz1h4yf1zc2iyfyy8szwc3ak60lpyrrky60b82f")))

(define-public crate-uu_stty-0.0.23 (c (n "uu_stty") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("term" "ioctl"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0npn73wvbxhv2zriw0bsnvfbmshgaw49rrplysgda8lry07d71j7")))

(define-public crate-uu_stty-0.0.24 (c (n "uu_stty") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("term" "ioctl"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0731wcmk1lfgbgb3qbr8qbxmb9586hryichz3ga1i905wp4dbrq7")))

(define-public crate-uu_stty-0.0.25 (c (n "uu_stty") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("term" "ioctl"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1szpvrszb0fgsi9b5vh5js6wnmj50xjqymnw6303ldrafl2f22fp")))

(define-public crate-uu_stty-0.0.26 (c (n "uu_stty") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("term" "ioctl"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0k2472z4mnsdycjixbi68r9mg8x4apsvin504wrx1phfihg02nz5")))

