(define-module (crates-io uu _s uu_stdbuf_libstdbuf) #:use-module (crates-io))

(define-public crate-uu_stdbuf_libstdbuf-0.0.1 (c (n "uu_stdbuf_libstdbuf") (v "0.0.1") (d (list (d (n "cpp") (r "^0.4") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0kzypzd30xb9d8zxngds3y72s9ri18ig1p51qd2wyq1l6zafmf67")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.2 (c (n "uu_stdbuf_libstdbuf") (v "0.0.2") (d (list (d (n "cpp") (r "^0.4") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "07x5333zpqqdv2slk8h05m2i211gh2h54ky10xfxy3f2b576hyk6")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.3 (c (n "uu_stdbuf_libstdbuf") (v "0.0.3") (d (list (d (n "cpp") (r "^0.4") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0bldwf3yz2q46qh4nx77jm6467g97nf3l0lgfcjwl1r9v4in2irq")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.4 (c (n "uu_stdbuf_libstdbuf") (v "0.0.4") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0ybxnzbg0vwy9750kp8k2bwq6xyzfxzgk9afi0zq2sv541c7ykgd")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.5 (c (n "uu_stdbuf_libstdbuf") (v "0.0.5") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1ahz5n5lr5p9hvfc2kimiw7cza0vsvg8bnqgkwsdxdi1kay1b6n1")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.6 (c (n "uu_stdbuf_libstdbuf") (v "0.0.6") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0dbgn5vbvgc6q53mxn3lbkyl5jbxip89pax4z7l12hp28db69bar")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.7 (c (n "uu_stdbuf_libstdbuf") (v "0.0.7") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "19wq0f80piyhh0jbxl6c94clvijb5y4w5imzkn40a9vv1wcl2d5g")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.8 (c (n "uu_stdbuf_libstdbuf") (v "0.0.8") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "15x21s06h2mvcsdn4xvjsfyn9jfiza3zz4yyvakgxy6agldckccc")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.9 (c (n "uu_stdbuf_libstdbuf") (v "0.0.9") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1y6zfs4w6rqs80sy7z8gxbh27bg5cdmfvp6dchqjvryy6hnnbsvh")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.12 (c (n "uu_stdbuf_libstdbuf") (v "0.0.12") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "17bjkrhvkrprv2kbgdh233iws7pvn72fhdqvy3sqi13ajbw95m8g")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.13 (c (n "uu_stdbuf_libstdbuf") (v "0.0.13") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0bbmp3clj2w225y7xxcmc1v7jdq4wrfmm2502pc8xpp14fjkmdhd")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.14 (c (n "uu_stdbuf_libstdbuf") (v "0.0.14") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1jjx9kmln0gf172mpkjwlxha9r0qf5hylw739wgmc0ahyqrd5v1w")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.15 (c (n "uu_stdbuf_libstdbuf") (v "0.0.15") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1b3yhfkz6738l35k7kabkbq3jzkngrywd33h3xzyk41mpgrhgb0g")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.16 (c (n "uu_stdbuf_libstdbuf") (v "0.0.16") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "0rfxxl2iqr1klz9j7yhcqczb0z6sg18qx19850hyz015iydva8c6")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.17 (c (n "uu_stdbuf_libstdbuf") (v "0.0.17") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "0har2i8z4fxgqbhw8bxcz8hhzapy9mxmr2vkgvl62m2ll8pqd2j2")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.18 (c (n "uu_stdbuf_libstdbuf") (v "0.0.18") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "00x3s4gm34ms0j08gjbyfc6vfqh8287nqmcx1yiik8rpwg5zc9s8")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.19 (c (n "uu_stdbuf_libstdbuf") (v "0.0.19") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "049nwk3i8b2il43d7n8sqjlq1vdc5bnpxzlhpyi1qmn5y3sr8q7i")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.20 (c (n "uu_stdbuf_libstdbuf") (v "0.0.20") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "05gzazb5ska2i9a29xii5m8hsb0wsa779gyz8ljmmv8bgxha28n9")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.21 (c (n "uu_stdbuf_libstdbuf") (v "0.0.21") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0pis2sfn1wqrsn6ns1dfgdslvpj723g4l7sy203w6rbdpqjfqg8v")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.22 (c (n "uu_stdbuf_libstdbuf") (v "0.0.22") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "04qn4gqfx28kf5z9qk0x079qqkqhn677cfas5sbkyc25bfd8w5f5")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.23 (c (n "uu_stdbuf_libstdbuf") (v "0.0.23") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "07ypckliy53jg5cn2m2nsgxx8bimjwgca43a70amfhz8fw5pdmz5")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.24 (c (n "uu_stdbuf_libstdbuf") (v "0.0.24") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "1yqy8rp5b2ixzmc3k6mc688f7g3rp7krz6w7jc40yk8p1bngj983")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.25 (c (n "uu_stdbuf_libstdbuf") (v "0.0.25") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1x2xhxrwd5h430kz72ljqjfj1fm7fsqbqq15r679h3qv7anb0kzn")))

(define-public crate-uu_stdbuf_libstdbuf-0.0.26 (c (n "uu_stdbuf_libstdbuf") (v "0.0.26") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0xvm2ka0kcgljx3gsb6gsm2g604sbrfnhhhccj223gw3zv81f9c1")))

