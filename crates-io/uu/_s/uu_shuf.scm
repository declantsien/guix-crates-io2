(define-module (crates-io uu _s uu_shuf) #:use-module (crates-io))

(define-public crate-uu_shuf-0.0.1 (c (n "uu_shuf") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "05sr3i6r62qz2b70i2pdk8mf0kqbgmxx8198zdnj23ibxx58c2fx")))

(define-public crate-uu_shuf-0.0.2 (c (n "uu_shuf") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0cqr9pg6352pmc7ip3jy97drkdg9l4mhh7zylqd284ih3wi9q0wl")))

(define-public crate-uu_shuf-0.0.3 (c (n "uu_shuf") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "05qqmwigh9igwhd5xgzwgryvgwk3085441xnwsaav4dhlnlghyfj")))

(define-public crate-uu_shuf-0.0.4 (c (n "uu_shuf") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1g75dx72p6x4smq5g055w069gb0jaa1y0s83cr13750r09cp1j5y")))

(define-public crate-uu_shuf-0.0.5 (c (n "uu_shuf") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "144lx1rw1vzspkfl858s5ssvyd8gncqy4ypw8h7vd53px9561xyl")))

(define-public crate-uu_shuf-0.0.6 (c (n "uu_shuf") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "14rayhbc8f9xp0m01ia6i00siygwvhy9mhkn8arm0l699ab0wqj4")))

(define-public crate-uu_shuf-0.0.7 (c (n "uu_shuf") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0dg57fcmzs33ax46mmf1wa2gpm9h2lbw8v2kxq47887cribl1iah")))

(define-public crate-uu_shuf-0.0.8 (c (n "uu_shuf") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0y3lsh4nbmrlzg04crs9027cly8f7786d743nc5w0c3b89yl7zic")))

(define-public crate-uu_shuf-0.0.9 (c (n "uu_shuf") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "01wyzaggb4fxrgp49h5m750ly9v5xp3klvb04kp8adq7542bs6m6")))

(define-public crate-uu_shuf-0.0.12 (c (n "uu_shuf") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0fgr70yvmirwg1nnjwc62vrb8x676r2w33q7jiwjpx0lffv8rj50")))

(define-public crate-uu_shuf-0.0.13 (c (n "uu_shuf") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0gmd9wdsl3ncwnavhll2zj8659jsgdj847haxfsbvyvar95syjjr")))

(define-public crate-uu_shuf-0.0.14 (c (n "uu_shuf") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1z3ays08r533lmhvv0a36bfhnjjpqpkwwwr6yfc59war5ik3cxli")))

(define-public crate-uu_shuf-0.0.15 (c (n "uu_shuf") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0h6ji2aab8r5vxg066srn4pdj4kif9r56828g209v9fw9hij6j01")))

(define-public crate-uu_shuf-0.0.16 (c (n "uu_shuf") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1bncaxb5mqabr1l3857qzcrzgvq4bbgba88cgwbsb8xrj3a1vlq6")))

(define-public crate-uu_shuf-0.0.17 (c (n "uu_shuf") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "02xl28i2fmkq4h4pcnby4yp2cai4lxsaffxza6frz5y2gmywnsq0")))

(define-public crate-uu_shuf-0.0.18 (c (n "uu_shuf") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1535yxrnj8gfjn6hphph870i352f4rnn65hlshr4avz9xf3mj67z")))

(define-public crate-uu_shuf-0.0.19 (c (n "uu_shuf") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1kkgbh9f3v7n9a0m9a06jn9mxlw1jgv0h16796l2fl0pbq1n3rk2")))

(define-public crate-uu_shuf-0.0.20 (c (n "uu_shuf") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0g5fll9lr7zwh89l3ggnnr1wgqngx1nwj8aixr7g8cr01gh5lf57")))

(define-public crate-uu_shuf-0.0.21 (c (n "uu_shuf") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1qkbbyyh40slhy18nia45s91r137nfkcxmlkc99ivmpcq4pwr3xr")))

(define-public crate-uu_shuf-0.0.22 (c (n "uu_shuf") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1v7f2s6z98pcin3lll01jzmgxg00cvr3xz08pic9478myh2i7d69")))

(define-public crate-uu_shuf-0.0.23 (c (n "uu_shuf") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1ic4wq595v994b9m92xvas6grl5as06rs3xqz5jj10pkb7q7sln0")))

(define-public crate-uu_shuf-0.0.24 (c (n "uu_shuf") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0z9fgk449rhhwzvrslmjdz0yg6bq640p3m1ffrb51j1nmnridznf")))

(define-public crate-uu_shuf-0.0.25 (c (n "uu_shuf") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1qd74s5rslikk90w4r14scy6gc2v813wx6l0j6sqxi8ly1wi26h6")))

(define-public crate-uu_shuf-0.0.26 (c (n "uu_shuf") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "01jgkfsgfb3bm9vd35albmd3k3x18v906k13sl0n536lc8szm7jb")))

