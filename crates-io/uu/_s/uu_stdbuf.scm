(define-module (crates-io uu _s uu_stdbuf) #:use-module (crates-io))

(define-public crate-uu_stdbuf-0.0.1 (c (n "uu_stdbuf") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.1") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "19lj63v8wcxjgzb49b67qqx8dbr310kznyjqklw981p6cqccyw14")))

(define-public crate-uu_stdbuf-0.0.2 (c (n "uu_stdbuf") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.2") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1nskm09s4qmgdzfnb9hry0r9q7gp93hsl4yw9xc5bkkmgb9cd18y")))

(define-public crate-uu_stdbuf-0.0.3 (c (n "uu_stdbuf") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.3") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "19rsimgkings70ddg39n1lb2jyqr10sd2imasayhzva8prxs3lky")))

(define-public crate-uu_stdbuf-0.0.4 (c (n "uu_stdbuf") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.4") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1k913q3cyrniz6v8byxxinygshbf4chn0k8h1wmb5gy81cyzb9qs")))

(define-public crate-uu_stdbuf-0.0.5 (c (n "uu_stdbuf") (v "0.0.5") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.5") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0bga95z5wlqdr4pmhwpj2v8bnnh4w7zpmh4fhrbn2iadw5nwmbim")))

(define-public crate-uu_stdbuf-0.0.6 (c (n "uu_stdbuf") (v "0.0.6") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.6") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1zx7lislzgx4b20q0nbmpin15mg4w12vlhplxc5dhmd5hy4g8ppw")))

(define-public crate-uu_stdbuf-0.0.7 (c (n "uu_stdbuf") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.7") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1qzwl3j6mi01785wyyim074i41nv5yi1iy0yj96ml1f4kb8i2qc1")))

(define-public crate-uu_stdbuf-0.0.8 (c (n "uu_stdbuf") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.8") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0rphc1zdww1mjqv863njl964zcalm71wmpzrgqmc53iq4pqq1fmw")))

(define-public crate-uu_stdbuf-0.0.12 (c (n "uu_stdbuf") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.12") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "02vqfdf0lhs6yj6xx0mdfsrssdpzpgjbyww1js4kbw59i5kzcggy")))

(define-public crate-uu_stdbuf-0.0.13 (c (n "uu_stdbuf") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.13") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0xnbkr97scxk88xqyk42vhv2r07i9k1qdg19mrb4bvk8hvj2s8ma")))

(define-public crate-uu_stdbuf-0.0.14 (c (n "uu_stdbuf") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.14") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0avki1d996mhkrd8if0lgyn2wv1b43cg4wbhihrx7wdmc6lj2k6b")))

(define-public crate-uu_stdbuf-0.0.15 (c (n "uu_stdbuf") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.15") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "026fn79m590a9fnlr234nzmmvfhn6f50hmxffqcqm4xbkfzqfplm")))

(define-public crate-uu_stdbuf-0.0.16 (c (n "uu_stdbuf") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.16") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "06y1dhynpl8khs30lix9rnv5qq6yl8ygyb6v6q0chbvv6x3wzc9k")))

(define-public crate-uu_stdbuf-0.0.17 (c (n "uu_stdbuf") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.17") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "091awkprgfr01azzhgj4cyalzprp88gl0c3priipmaxg1lc28f9n")))

(define-public crate-uu_stdbuf-0.0.18 (c (n "uu_stdbuf") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.18") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0dl0nfc4yj1s5js1qj1i4npafnnqkfss4nwjd683ifjxmzg9lahz")))

(define-public crate-uu_stdbuf-0.0.19 (c (n "uu_stdbuf") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.19") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0xdi02srr708r2dmgincz3jw1qp0gs57vvya9dfk2x4rkklhjvv1")))

(define-public crate-uu_stdbuf-0.0.20 (c (n "uu_stdbuf") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.20") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "153lzlmi2rfg1hhkiqv4319yrsf826b9sk4wlm048bwbfchddh3n")))

(define-public crate-uu_stdbuf-0.0.21 (c (n "uu_stdbuf") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.21") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0qha00470dnxz5p0nq1l1n7spc1bqy8j66n3yxpvwznvihqmj7dj")))

(define-public crate-uu_stdbuf-0.0.22 (c (n "uu_stdbuf") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.22") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "135x0wkv61fr21mn75gpsqd6mdddkay5arp7vsipvnmyrydwy0hq")))

(define-public crate-uu_stdbuf-0.0.23 (c (n "uu_stdbuf") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.23") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "06l8gq3s1pjnahdxv20b9l915fnj5xwzdbbv1hvgnm9liib4q2bb")))

(define-public crate-uu_stdbuf-0.0.24 (c (n "uu_stdbuf") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.24") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1988i94g4zkhd0053byrj5rsfmwbky9gijdv54lvda8rajdy3c95")))

(define-public crate-uu_stdbuf-0.0.25 (c (n "uu_stdbuf") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.25") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0wnir1rrgbdsvq5px71r7wbpy5zx12m92lcwjr8hlr5szlvjjb78")))

(define-public crate-uu_stdbuf-0.0.26 (c (n "uu_stdbuf") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libstdbuf") (r "^0.0.26") (d #t) (k 1) (p "uu_stdbuf_libstdbuf")) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "18vskb6a1ajiria1svl93smv2samgsp25cf0gjk7wabkn6rhh82l")))

