(define-module (crates-io uu _s uu_seq) #:use-module (crates-io))

(define-public crate-uu_seq-0.0.1 (c (n "uu_seq") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0fd4xxz7pcqxni5189vksdlyxnrz1dnlajlyyacbic4b7mbi0d68")))

(define-public crate-uu_seq-0.0.2 (c (n "uu_seq") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0ys1imvrpjdk68nmbpdhcmpzihyii4mdsm89zkhlwyaa8s3f29l0")))

(define-public crate-uu_seq-0.0.3 (c (n "uu_seq") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "054y05np3xlqa4crfhx9g4sgapr859zbdykff7vrkgqnki6qvcp7")))

(define-public crate-uu_seq-0.0.4 (c (n "uu_seq") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "13796mzj2kannddmzh9j39pz06zdx2v2a21392w3gvchnwhk4qd9")))

(define-public crate-uu_seq-0.0.5 (c (n "uu_seq") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0rxmvck84nvrmd252yp0dwypm2w8hb70ixnjk779qd81ad1yvylm")))

(define-public crate-uu_seq-0.0.6 (c (n "uu_seq") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0kcrlwssjwzp8i4ilwnhw3zr4rj159gsq9m91ng3agaks0wh4k99")))

(define-public crate-uu_seq-0.0.7 (c (n "uu_seq") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0ripzq5vwilh3j7gzqjwhpygs3krclaivz84r8ry00zk1aq1d1hf")))

(define-public crate-uu_seq-0.0.8 (c (n "uu_seq") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0rw0wq6bc8s7vmfcd1hxkga3aya4ra2fkjp1f25vskvbra0nsn02")))

(define-public crate-uu_seq-0.0.9 (c (n "uu_seq") (v "0.0.9") (d (list (d (n "bigdecimal") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0z20hppxdwr6nb7c96vs9skbprrbxv3brasa7fm4ivxcmqfbq52h")))

(define-public crate-uu_seq-0.0.12 (c (n "uu_seq") (v "0.0.12") (d (list (d (n "bigdecimal") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0y3rpv14w8lm3vhwzv6wq3nlshhy17f457dzdc9w8z5yarhcm62g")))

(define-public crate-uu_seq-0.0.13 (c (n "uu_seq") (v "0.0.13") (d (list (d (n "bigdecimal") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "07b6vm4kkl0v0pg026sqr4pds5zm7gb8dfb4vvk80k0c78sf98sz")))

(define-public crate-uu_seq-0.0.14 (c (n "uu_seq") (v "0.0.14") (d (list (d (n "bigdecimal") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "099ipzf6nqdfl96p6gwbadr3gbw8rnzblrf7wb2j6aakb44fk9w4")))

(define-public crate-uu_seq-0.0.15 (c (n "uu_seq") (v "0.0.15") (d (list (d (n "bigdecimal") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "0w2bmivc65qg3k9b35ay3fpr8f3fgm65sfcpg20g6ipf4bba0vyb")))

(define-public crate-uu_seq-0.0.16 (c (n "uu_seq") (v "0.0.16") (d (list (d (n "bigdecimal") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "1pxzp6b24a2z6imvyx9a5j06rcp5lcsnic2gfkzpij7nkx6jsnpy")))

(define-public crate-uu_seq-0.0.17 (c (n "uu_seq") (v "0.0.17") (d (list (d (n "bigdecimal") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "1vg43hz2vfdcpi5z19i3dgpx74ijawap8kcvhqj12x8k1yfhjfci")))

(define-public crate-uu_seq-0.0.18 (c (n "uu_seq") (v "0.0.18") (d (list (d (n "bigdecimal") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "0bjvdh0k7nhl9h64bp8fkpnq59fw0phsabylm3sm0z40qdqaavla")))

(define-public crate-uu_seq-0.0.19 (c (n "uu_seq") (v "0.0.19") (d (list (d (n "bigdecimal") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "1r8j6z6qjnl4597mip7153pp20fkzmw3z79zbhgr4hpq837y2dlv")))

(define-public crate-uu_seq-0.0.20 (c (n "uu_seq") (v "0.0.20") (d (list (d (n "bigdecimal") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "013l9q3ld16d804lnw5da6vxjl5wmcmhw2p01z1pmrq70rz687ar")))

(define-public crate-uu_seq-0.0.21 (c (n "uu_seq") (v "0.0.21") (d (list (d (n "bigdecimal") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "18ancpy8xgz7qxwgr5prz9swgcv72kw4g3qlz96ji5kd2cpbvwbc")))

(define-public crate-uu_seq-0.0.22 (c (n "uu_seq") (v "0.0.22") (d (list (d (n "bigdecimal") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("memo"))) (d #t) (k 0) (p "uucore")))) (h "1jh2d28iq71w5izvm7iv4jdf8sfd623wh0ajdmnww4wmxiip9l18")))

(define-public crate-uu_seq-0.0.23 (c (n "uu_seq") (v "0.0.23") (d (list (d (n "bigdecimal") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("memo" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "0fh8kdav3aaygw90vjh2dhp6c25n19siqankq0s57n10548g67xn")))

(define-public crate-uu_seq-0.0.24 (c (n "uu_seq") (v "0.0.24") (d (list (d (n "bigdecimal") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("format" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "039dzg2wcf0gp4zdq3qphczfvfzlad7ikaxpd3c0symspclk81l7")))

(define-public crate-uu_seq-0.0.25 (c (n "uu_seq") (v "0.0.25") (d (list (d (n "bigdecimal") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("format" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "1ifjsm19k9iylwnp7c82l1k96b36h7p3nyxgcsz0avb0xqh5r4vf")))

(define-public crate-uu_seq-0.0.26 (c (n "uu_seq") (v "0.0.26") (d (list (d (n "bigdecimal") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("format" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "11b6qvkpd48ghh6gy7g291i6isrlzhm1k1vbwbp20klcv8idcll4")))

