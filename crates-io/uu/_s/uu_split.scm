(define-module (crates-io uu _s uu_split) #:use-module (crates-io))

(define-public crate-uu_split-0.0.1 (c (n "uu_split") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1smbklgssmd6kl24vx7zcfmvdp508pk427w6axn2pg1bwnf4cixm")))

(define-public crate-uu_split-0.0.2 (c (n "uu_split") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0a98gvvg9vs5dhd6mzwjfd7arfyaiy5xq10n03zd0mg4w7b1hdbf")))

(define-public crate-uu_split-0.0.3 (c (n "uu_split") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0q4k4cbsc07xmp6ywsgiw2ylcad5iqdi3jmkvdvrabfkfp0dl1f9")))

(define-public crate-uu_split-0.0.4 (c (n "uu_split") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "17s85hayli6343w4dx4wgl195nv43asj3qmb5zwllkw5g6adhmhs")))

(define-public crate-uu_split-0.0.5 (c (n "uu_split") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0jnpzhi3g66bllqnqkld270713fcn7y1wpsb6fbw7b6gqy1f4a34")))

(define-public crate-uu_split-0.0.6 (c (n "uu_split") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1513bj1nz3sd67q01j6g3877q17jgd9n1dcfskjcsv7qq3qvv3vy")))

(define-public crate-uu_split-0.0.7 (c (n "uu_split") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1xb4wv7pqanq24d9wqspikjv65ss4pqpzy113qhslkh6144yc5k7")))

(define-public crate-uu_split-0.0.8 (c (n "uu_split") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "13n164fyl3bc0vl077wzdbab7wy068r49h2ll63n2cr0j3rgdm8c")))

(define-public crate-uu_split-0.0.9 (c (n "uu_split") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0gif1fg3j7z0zz4kjyv6m2m5gnpc9m14z1hh7ppy2sjw0g1wdjki")))

(define-public crate-uu_split-0.0.12 (c (n "uu_split") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1d1723zx5cw8l294fgm3b21ggh8faj6i6lbhrylwvm5lh2bgviqw")))

(define-public crate-uu_split-0.0.13 (c (n "uu_split") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0wjd71imlp5ajn74f82rgw1jqazn04gn065ciqc0y4k2g1b96v25")))

(define-public crate-uu_split-0.0.14 (c (n "uu_split") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1hkgbsvxvbd18h66zhxl9jxxvj1i1c3sllnbv0xmasmymbb9q3lg")))

(define-public crate-uu_split-0.0.15 (c (n "uu_split") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0ynwrxc2b4j4yc1ck0sin5lz8j7l5kahvs185s8l6lx08777p24v")))

(define-public crate-uu_split-0.0.16 (c (n "uu_split") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0x3pvxr3z0sf3vd3rg3cp0q86vcpcacnw4dkw6vmb3p40mabsgff")))

(define-public crate-uu_split-0.0.17 (c (n "uu_split") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1ri3ay50did7nagwwcwnjvb5q6gira5x7sx5h6zmmyyp2xdb751f")))

(define-public crate-uu_split-0.0.18 (c (n "uu_split") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1c146dc9cg6dc670ixahcyx3z6k365k5c1pkra8n54mdibr1qvg1")))

(define-public crate-uu_split-0.0.19 (c (n "uu_split") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0zv7ksa5kxha1xdb7wzvax3bdjicxk36vqficysp2ppfd43dfy7d")))

(define-public crate-uu_split-0.0.20 (c (n "uu_split") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0qfv4qhf5fvr68zp2wx0sx98jh0wfrx2wca03gccv1cgm66c574l")))

(define-public crate-uu_split-0.0.21 (c (n "uu_split") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "19sagc6v7x7kgwd2axcm3zjaq0jmxqqbmksx0ghj7yw20pwnxf43")))

(define-public crate-uu_split-0.0.22 (c (n "uu_split") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1iazlw1dlmvg2vcfqnhjg2n461rf1vdcqx587nz1fw1dpwghp89b")))

(define-public crate-uu_split-0.0.23 (c (n "uu_split") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1djhdwf85c0wxbggic071h1zydk83m478m4lnkv8j96jclmbl2fy")))

(define-public crate-uu_split-0.0.24 (c (n "uu_split") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "041h16c2mf6b8x58a05zhbgy6bl0nfm47igczzm2z0055im7mdl9")))

(define-public crate-uu_split-0.0.25 (c (n "uu_split") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "198xzyg5im98bjb0jjakv5rrs6hia1v88w3zr0fr7zwzkf0krqpp")))

(define-public crate-uu_split-0.0.26 (c (n "uu_split") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1n626dllkr8897asfg6scmqh2smb5zd2kmx25a1nnxfk32ajmrz2")))

