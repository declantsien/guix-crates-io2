(define-module (crates-io uu _s uu_stat) #:use-module (crates-io))

(define-public crate-uu_stat-0.0.1 (c (n "uu_stat") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("entries" "libc"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0sn614496rsxjhv47g51q5g3q12dbf4s8hyx2rh9m35vqnczz8j6")))

(define-public crate-uu_stat-0.0.2 (c (n "uu_stat") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("entries" "libc"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0ii0rwa9irwp0nw5l2dvaz2jsqnqzhg1fb9cavahxca4k1y5plcd")))

(define-public crate-uu_stat-0.0.3 (c (n "uu_stat") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("entries" "libc"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0wfdfw9qrjlnv31jyyynnndilzzadyx5zwp414nf0f3rh6p279ls")))

(define-public crate-uu_stat-0.0.4 (c (n "uu_stat") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("entries" "libc"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0n0ixjgipa6ds4c83di1wqnc5jsfvgscrkqjdy0pb00yms3fr80b")))

(define-public crate-uu_stat-0.0.5 (c (n "uu_stat") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("entries" "libc"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1b8prax2cyrb9dqmzilsr1vl72w0ijryncbzsnrzhwlzjb45s939")))

(define-public crate-uu_stat-0.0.6 (c (n "uu_stat") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("entries" "libc"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0kq2prp0va2j6pckls9qa933h28pgxl3k5ylmks8w583virh41pj")))

(define-public crate-uu_stat-0.0.7 (c (n "uu_stat") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0rhbq2x70yriri1ha3hgib3jpvvgx3qpbnfdhb5mqljvsgyp8qv3")))

(define-public crate-uu_stat-0.0.8 (c (n "uu_stat") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "05s8qrjf3m0kc6jnf252q6skivl17qb7ygs3qckqm9rl085x3nhd")))

(define-public crate-uu_stat-0.0.9 (c (n "uu_stat") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "16cpmdbdv2lgs4k7zl8qs5qccl6hjpddxyd6kwniz6yaycyw33wf")))

(define-public crate-uu_stat-0.0.12 (c (n "uu_stat") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "07cifs7ksbdq3zqqqd2bdj63n5758rrj47nx1x7zjk6965l8fsqa")))

(define-public crate-uu_stat-0.0.13 (c (n "uu_stat") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "142rj9caq9aawpzl8pcqn98bllaqb6kfxn2ir7zjva2773ygmln2")))

(define-public crate-uu_stat-0.0.14 (c (n "uu_stat") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "14fzw012gwnv03y246ijh50pkxpnilpkigaqwjdiw92y0nd84cl6")))

(define-public crate-uu_stat-0.0.15 (c (n "uu_stat") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "04657icqw05z47mf18p6p7j5lc5hqdlhjsax3dxk51vjdkc90l43")))

(define-public crate-uu_stat-0.0.16 (c (n "uu_stat") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "1pkng9lcm7pr0b7ir11g4y06dds33lipdq1k61xyhan62qn56yzh")))

(define-public crate-uu_stat-0.0.17 (c (n "uu_stat") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "17xwn80i474vc2hzkffr635ihqj2gh3zjwh1152041d6q2wy1l3a")))

(define-public crate-uu_stat-0.0.18 (c (n "uu_stat") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "131pqz4crfsg20dr3zaayhk3w631k5h96h9wdbw9kj5qgj5v8ybv")))

(define-public crate-uu_stat-0.0.19 (c (n "uu_stat") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "02b1nbqyd2bgkx3b4xbsfxgakkyx95ih95v2csasa5fgy3i5q18z")))

(define-public crate-uu_stat-0.0.20 (c (n "uu_stat") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "0vczz4pdpixzi519m1ndn2iay4ml3ghrwqrhl4gijdbff3z4ckws")))

(define-public crate-uu_stat-0.0.21 (c (n "uu_stat") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "0y4axfqssn3xnbg21a5zsinl5g4nv8s3lk042awb7qgb6lch77ix")))

(define-public crate-uu_stat-0.0.22 (c (n "uu_stat") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "0yji44v8qcwv2ra3w6qfcqby8ljlbnwwqyxs7pnhngpl5gxppjc4")))

(define-public crate-uu_stat-0.0.23 (c (n "uu_stat") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "1sjxzfk11dbqd8hl54c0y5pn8zw03adaf0bpcgfg6q0ln2adw92d")))

(define-public crate-uu_stat-0.0.24 (c (n "uu_stat") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "1r329q83z3nc6fyyza6x5abfdhn80s9dqaw2hbgj2dg7wylw6v53")))

(define-public crate-uu_stat-0.0.25 (c (n "uu_stat") (v "0.0.25") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "18y39l6b7y050zvimdmi3xm0bwl44cirnmh4a0g5ppfxbpnykpaz")))

(define-public crate-uu_stat-0.0.26 (c (n "uu_stat") (v "0.0.26") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "libc" "fs" "fsext"))) (d #t) (k 0) (p "uucore")))) (h "0napdmbwik94hch4w6nvj0xqbhis5i2afxw07shfdz4mf8v9ynky")))

