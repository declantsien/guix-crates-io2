(define-module (crates-io uu _e uu_env) #:use-module (crates-io))

(define-public crate-uu_env-0.0.1 (c (n "uu_env") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0vmfqgh4vvafk3q65lll59c9wmr5c6zj70y16g616vsdzvbfdvh1")))

(define-public crate-uu_env-0.0.2 (c (n "uu_env") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1qij3nxn0w4k21y5sli0v9wyi6wj8af8sqcjrk0f2hjzv79xyz08")))

(define-public crate-uu_env-0.0.3 (c (n "uu_env") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0j014kdl2zdkji8kp7i3cc9lll0x9z2lrm50biynndml6pzhdais")))

(define-public crate-uu_env-0.0.4 (c (n "uu_env") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0j9ldnc3brama18nbwdcd9sa8nwxdb1awq4cizs3lyszfb6l5cbx")))

(define-public crate-uu_env-0.0.5 (c (n "uu_env") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "00qka8llwnna5vahgs63wj5b3q60g3x3i01clf239cwn0ihm2sbb")))

(define-public crate-uu_env-0.0.6 (c (n "uu_env") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1kb9z7pqcf15x4mvvaqd72cgm29dkw3yk9h72y2awhxindp96d3s")))

(define-public crate-uu_env-0.0.7 (c (n "uu_env") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0766dlaxixn72m3xpyxrd36wlw0i5lmvm52mwc3y60lp6d6nq46c")))

(define-public crate-uu_env-0.0.8 (c (n "uu_env") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1w7gigl86j41v61s76wzzwz3aazz5v64khgabdmljza9wxjdw99d")))

(define-public crate-uu_env-0.0.9 (c (n "uu_env") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0i0pa5vqhld68l9bkrzhh39mmw5lf35wj8f78wyc3a4678589kcq")))

(define-public crate-uu_env-0.0.12 (c (n "uu_env") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1n283bfdbac3ybws127yzhhx7vg2nb23ydjr88xgc21ja938lfcq")))

(define-public crate-uu_env-0.0.13 (c (n "uu_env") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.17.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0sp4yhvvph5xlpfjrbjk4yzc8jc84p1pb75mfhqkgkg0bh7a9fzr")))

(define-public crate-uu_env-0.0.14 (c (n "uu_env") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.17.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "0bwvij6qzgdn4i27qs1y6wpk31qzphcrqxly5zfxhyzrv0pf2ai0")))

(define-public crate-uu_env-0.0.15 (c (n "uu_env") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "0b8rlnmiyypi4qpda9y266dq84mdlbj6qvf0h98ds3qbjfi6jif7")))

(define-public crate-uu_env-0.0.16 (c (n "uu_env") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "0vsqxhl1anw6zrfgwhm375hg5c68n8nl54wjfzp9rywxs0h3pgjm")))

(define-public crate-uu_env-0.0.17 (c (n "uu_env") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("signal"))) (t "cfg(unix)") (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "1kingbywdn0z3w885wbk0y9khrfyfq8pansagzkj4mqccn0dkv1w")))

(define-public crate-uu_env-0.0.18 (c (n "uu_env") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("signal"))) (t "cfg(unix)") (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "09daxy703s3ib1c3fkhbrxnzspsb0bhrpfnvv6b7scl88qxfv8lp")))

(define-public crate-uu_env-0.0.19 (c (n "uu_env") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("signal"))) (t "cfg(unix)") (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "1whcjhyjvjcw4p01awpy0nvkrz1r1pqgpsbhnckv3zyzyka7b457")))

(define-public crate-uu_env-0.0.20 (c (n "uu_env") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("signal"))) (t "cfg(unix)") (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "1giq36b5lhh24d022709vfi9pkrwdrrsc5ckcl42m0nlyn4aqnms")))

(define-public crate-uu_env-0.0.21 (c (n "uu_env") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("signal"))) (t "cfg(unix)") (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "1h4glmzi9xz8rjhp10zf25nqpnqhrbgq3jh108is5w6irsagkyl9")))

(define-public crate-uu_env-0.0.22 (c (n "uu_env") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("signal"))) (t "cfg(unix)") (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "0f3c8m37khib323x8zqzscrlnif2s0ihx5nj2b8g0470y8fn8ldn")))

(define-public crate-uu_env-0.0.23 (c (n "uu_env") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("signal"))) (t "cfg(unix)") (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "00279d1mh8qxj0jp0ks55cijj3xzy51803k2k9q1ahmvmqn1wdd2")))

(define-public crate-uu_env-0.0.24 (c (n "uu_env") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("signal"))) (t "cfg(unix)") (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "0nxx4pc8jmybhxgzcw11s1h4gc27aiw096474466i4glbnb164rf")))

(define-public crate-uu_env-0.0.25 (c (n "uu_env") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("signal"))) (t "cfg(unix)") (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "0l82q1glmy6pikiky4m6654zvsn2b1lgqzbsss3vwmavn44q75ha")))

(define-public crate-uu_env-0.0.26 (c (n "uu_env") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("signal"))) (t "cfg(unix)") (k 0)) (d (n "rust-ini") (r "^0.21.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "1aszdfydw7xqpc13ddgg11ghidnhfwgaiwfib6i6kb3257f5y11n")))

