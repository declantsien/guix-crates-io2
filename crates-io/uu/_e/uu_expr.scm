(define-module (crates-io uu _e uu_expr) #:use-module (crates-io))

(define-public crate-uu_expr-0.0.1 (c (n "uu_expr") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "onig") (r "~4.3.2") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "189ksxzl75h092xs3s9x8gisaimgxvln9d0v0axfcg215klgx6f7")))

(define-public crate-uu_expr-0.0.2 (c (n "uu_expr") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "onig") (r "~4.3.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1qk1jiqry6k1z1p2lnqqkbmrsdpalcjsa6z12kqscw1f170c3yl8")))

(define-public crate-uu_expr-0.0.3 (c (n "uu_expr") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "onig") (r "~4.3.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1qff2lv804sly4ycw1qa465ym8gmxiyb3r102znbh949zwkh2s9w")))

(define-public crate-uu_expr-0.0.4 (c (n "uu_expr") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "onig") (r "~4.3.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0kmj83vla5mjwfg1h2xa16gls2mmvxxqcbqk7s4l32ikrq3n4v8y")))

(define-public crate-uu_expr-0.0.5 (c (n "uu_expr") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "onig") (r "~4.3.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1q1ripby8hfqm6614k6i062dlxr05ycj1gppcs5930nrmzymfbyy")))

(define-public crate-uu_expr-0.0.6 (c (n "uu_expr") (v "0.0.6") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "onig") (r "~4.3.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "03mg80jy00kbd3dwmli9x8m78gb8gnbsah59q299h7b6mypd4s69")))

(define-public crate-uu_expr-0.0.7 (c (n "uu_expr") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "onig") (r "~4.3.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1ma7a0zfdqsncy5rxh5x0ap99lxxia21vybhhqllg99qb2nbiy6h")))

(define-public crate-uu_expr-0.0.8 (c (n "uu_expr") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "onig") (r "~4.3.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1jwdymwdppz3jywhw3n6596i020anzjrbdb1zqdgwbzbhjag608l")))

(define-public crate-uu_expr-0.0.9 (c (n "uu_expr") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "onig") (r "~4.3.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1g47pf35frb0syyhnzybwfyhnq2yriq77cnm6dj2snljyfwa18kc")))

(define-public crate-uu_expr-0.0.12 (c (n "uu_expr") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "onig") (r "~4.3.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "18qy16fyazvmx4x0c80hfcskk09qbvv9rza44vkd00r2bzi2p87x")))

(define-public crate-uu_expr-0.0.13 (c (n "uu_expr") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "onig") (r "~6.3") (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0c612lmysp4hncadw4ynsd547wykf7zxvm8z63lx96jg111nx1ah")))

(define-public crate-uu_expr-0.0.14 (c (n "uu_expr") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "onig") (r "~6.3") (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0c1hjgy555jzdaih6vxh2qpm43fi187f9h2rznnlspxrczp1zyii")))

(define-public crate-uu_expr-0.0.15 (c (n "uu_expr") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "onig") (r "~6.3") (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0jb7v6mashx5n92lgdrp2033255lxb2z21hz0qb3ph96slz1qi81")))

(define-public crate-uu_expr-0.0.16 (c (n "uu_expr") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "onig") (r "~6.4") (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "12066qzvm9nh3pgw9lyhv3jrq1789yk3df3lv69r0cn43gh4vi11")))

(define-public crate-uu_expr-0.0.17 (c (n "uu_expr") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "onig") (r "~6.4") (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1ly49s655kq10jirc0z6zs8nmi8wy74ghapnchpi01yar1ks5g9r")))

(define-public crate-uu_expr-0.0.18 (c (n "uu_expr") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "onig") (r "~6.4") (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1rnncb7qlkd3i3lrl4vrriy89xyjpp83iil9v0pjqxiy454viiis")))

(define-public crate-uu_expr-0.0.19 (c (n "uu_expr") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "onig") (r "~6.4") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0f3if865dvpyralsyhgjj07cs1yh2c2qzkn0rqx56kg8b5m77qyd")))

(define-public crate-uu_expr-0.0.20 (c (n "uu_expr") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "onig") (r "~6.4") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "042h0w6v1bfsyjv90as0n5r9i2pyvgrakc4773x4jx1mnbd3i9mb")))

(define-public crate-uu_expr-0.0.21 (c (n "uu_expr") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "onig") (r "~6.4") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0fgmfvpcirh91n37rz3yik2a93xv47q7h3wg93n2xfgl4lyq9r7k")))

(define-public crate-uu_expr-0.0.22 (c (n "uu_expr") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "onig") (r "~6.4") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0ky9gls78n2vpa45wrbdvgs4ac9ih7j3gqpn94kf650yp54fnfld")))

(define-public crate-uu_expr-0.0.23 (c (n "uu_expr") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "onig") (r "~6.4") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1lwdkyfy4m23w8hj1q4phzfxzkrq3vdmcz95g4fg0adh9f43jaw8")))

(define-public crate-uu_expr-0.0.24 (c (n "uu_expr") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "onig") (r "~6.4") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1vj8r62vbvms27zf8jql3m3xwp69736sik97njls0lrjrbfalfh0")))

(define-public crate-uu_expr-0.0.25 (c (n "uu_expr") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "onig") (r "~6.4") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1f183s1isdjlp6f301nzblbwhxy0is55ww575018hj7bqljja5m1")))

(define-public crate-uu_expr-0.0.26 (c (n "uu_expr") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "onig") (r "~6.4") (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "08y08c7kr8bz4ppqa8cdqfqnf1kccchyg3fx35pasqfb9whfd8zq")))

