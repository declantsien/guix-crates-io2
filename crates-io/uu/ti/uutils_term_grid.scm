(define-module (crates-io uu ti uutils_term_grid) #:use-module (crates-io))

(define-public crate-uutils_term_grid-0.3.0 (c (n "uutils_term_grid") (v "0.3.0") (d (list (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "1ixvj893jrcvv76m7cgr99fc25la6xl2101qva6ni646aqm4b2dk") (r "1.70")))

(define-public crate-uutils_term_grid-0.4.0 (c (n "uutils_term_grid") (v "0.4.0") (d (list (d (n "textwrap") (r "^0.16.0") (f (quote ("unicode-width"))) (k 0)))) (h "0xpnglm2kllsn749sbcz7acia99z6hly1z9plb9wwdkdlc78wbsb") (r "1.70")))

(define-public crate-uutils_term_grid-0.5.0 (c (n "uutils_term_grid") (v "0.5.0") (d (list (d (n "ansi-width") (r "^0.1.0") (d #t) (k 0)))) (h "0yk03swjy484i00slziixqs45j69krl5gf0p9mm5935rw66ji3cz") (r "1.70")))

(define-public crate-uutils_term_grid-0.6.0 (c (n "uutils_term_grid") (v "0.6.0") (d (list (d (n "ansi-width") (r "^0.1.0") (d #t) (k 0)))) (h "1ys1y4bjdgwhvd3c9b2c8hx2dmnxjsgqgg3sll1mgfmlmnsfz7gq") (r "1.70")))

