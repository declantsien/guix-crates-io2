(define-module (crates-io uu _i uu_id) #:use-module (crates-io))

(define-public crate-uu_id-0.0.1 (c (n "uu_id") (v "0.0.1") (d (list (d (n "uucore") (r "^0.0.4") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0ks5l77ksnb12mr0zgcrlsabv09ziqshjjgsr9vxcmc2yxbmfl3c")))

(define-public crate-uu_id-0.0.2 (c (n "uu_id") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0v20k4hpqvq4wl2zcjxpdn71yk0db706l13cjds75k8v10m5jajj")))

(define-public crate-uu_id-0.0.3 (c (n "uu_id") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "12y2q4vdynvcdgxwv53g3vg2sliqqzsnbc6i9983zw67x5jf6lsw")))

(define-public crate-uu_id-0.0.4 (c (n "uu_id") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0wrb9pbppshf6zddn7wvxyldn4w2jxn38b9avl8y013f1snm8z80")))

(define-public crate-uu_id-0.0.5 (c (n "uu_id") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0dzbw2q1x3yy1nw8axhnrx6z3k78lwjn91z9l87ah6klvl8i06cj")))

(define-public crate-uu_id-0.0.6 (c (n "uu_id") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0xhx2y1hjpfyms8ppi5z06yz60ml5wbn68g7rcb1066pir6jym6c")))

(define-public crate-uu_id-0.0.7 (c (n "uu_id") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "09kh07mk3ns3a1zsr8blqyafkf60123p5r16ac96b420v5481ng3")))

(define-public crate-uu_id-0.0.8 (c (n "uu_id") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "selinux") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0b7b41fccasrsscwky1j6a0s5c46468zfimzj1fcci614mjklsr9") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.9 (c (n "uu_id") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "selinux") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0glv053i7304pmgdrhdmjbgfy0pp4d1lifd2cgczmfbn4534lxss") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.12 (c (n "uu_id") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "selinux") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0p59wdnjvxfh3hwrhgk1l6zfg4jszy0mal10mdi5cxlfkc2aq5vf") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.13 (c (n "uu_id") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "0h8c0ivsqmzv0nrxxmd9hi71ddsvc6ii29kahx9adhm7jjw5268r") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.14 (c (n "uu_id") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "1awghdw6i0s6lxr5s9akasal3vsdn3myariqai1g88pa2y8dv7ms") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.15 (c (n "uu_id") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "01zrzqdpwxcz0w2z8ypfkhj7m41biqf1gpgpdhcxl0x2hrlgc8b0") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.16 (c (n "uu_id") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "0g62n2rkg33gn9xpmbl7r97jwz13hap701f1nzzp01p03qnyr2xc") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.17 (c (n "uu_id") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "1jq4dmpl1n8h4d54vabs1ckk6zzknnpn4xp9qqwqndfa51hhdisj") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.18 (c (n "uu_id") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "02r0q5r6bxbkrp9fyjldyi7yr3sd5404m8crfqy4xf4yrskmqm1d") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.19 (c (n "uu_id") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "19zxj5g3d8i71s3pyy4wkk19svh2vhml7zk0h73d2af4w46is5j0") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.20 (c (n "uu_id") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "13s9hqmcjifaj3lcg4mfnzdfaj8azjiq8fwzk58q5i5har0dizrd") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.21 (c (n "uu_id") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "1sflriminnpprp5zkqg0prprqr6m0lspxzkacyrpn7s1hdyfgad6") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.22 (c (n "uu_id") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "0mb8mc1xknqwm62kx8hzrmw4viy0h3c9b2pr960vvsx3i8yxnac4") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.23 (c (n "uu_id") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "1xx6v21mfmlghf1g5f2ffzq6qhr86l63kzhyy3na0aakr8xw1iyc") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.24 (c (n "uu_id") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "092vk22qk7plqv7zv4gd1d2srrjk6grrcz7cgylbyrxv3llqfiva") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.25 (c (n "uu_id") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "0ljbx1x6zg11zlxpgnchm50vw4909v9kjqgjc78xq6jagbpxkb4z") (f (quote (("feat_selinux" "selinux"))))))

(define-public crate-uu_id-0.0.26 (c (n "uu_id") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "selinux") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "0ijz43ayknffvy97pil3a5icrig8p40p6dcqackdwfb163jj5ibk") (f (quote (("feat_selinux" "selinux"))))))

