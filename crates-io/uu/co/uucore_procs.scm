(define-module (crates-io uu co uucore_procs) #:use-module (crates-io))

(define-public crate-uucore_procs-0.0.1 (c (n "uucore_procs") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00xa3cfxk31005wjjphi2lp3vrf59iwvwx3nzmmr28rv0j15pj82") (f (quote (("default") ("debug" "syn/extra-traits"))))))

(define-public crate-uucore_procs-0.0.3 (c (n "uucore_procs") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hklqvc83yz851sgashnlba2c5vprjascnx39lpmvapc2bla37fa") (f (quote (("default") ("debug" "syn/extra-traits"))))))

(define-public crate-uucore_procs-0.0.4 (c (n "uucore_procs") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0095h3jrdyzzhbsxbskcrr5rcsdjfcf9888xsvzxm14li2ayg8hz") (f (quote (("default") ("debug" "syn/extra-traits"))))))

(define-public crate-uucore_procs-0.0.5 (c (n "uucore_procs") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gp7hzbwmmjzw2wqj0n2lbkb09zfflv01l6ajc6rvksyzy81rx37") (f (quote (("default") ("debug" "syn/extra-traits"))))))

(define-public crate-uucore_procs-0.0.6 (c (n "uucore_procs") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lqbxs7iiw2xwnhcrarc1csgmh004hw05xh7f73jkdaj14b2q3x3") (f (quote (("default") ("debug" "syn/extra-traits"))))))

(define-public crate-uucore_procs-0.0.7 (c (n "uucore_procs") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qllam6klqid46qgbrym142firjjdj9zxgg827mbll91b19cy15s") (f (quote (("default") ("debug" "syn/extra-traits"))))))

(define-public crate-uucore_procs-0.0.8 (c (n "uucore_procs") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xhgsh1rclhk36ar5r595vbrd4znw6hw1bp40jfcfv0nyqrd850d") (f (quote (("default") ("debug" "syn/extra-traits"))))))

(define-public crate-uucore_procs-0.0.12 (c (n "uucore_procs") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ap0xkah6ss7drzvgcma9m7li6wzw5js2rwb6gj5nvvqf5gyf174") (f (quote (("default") ("debug" "syn/extra-traits"))))))

(define-public crate-uucore_procs-0.0.13 (c (n "uucore_procs") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0vwq6bpm3k3bxmn2hsd6j50b3xdhs81xrfv48jb7rnilqr6dri0s")))

(define-public crate-uucore_procs-0.0.14 (c (n "uucore_procs") (v "0.0.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0mcsikw1lzyrav1hrrvisk0nd8b7z64rcqxh88axka2pvi6wn20a")))

(define-public crate-uucore_procs-0.0.15 (c (n "uucore_procs") (v "0.0.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1hqrv0cqv72chvr431rjhssdawpfn9fzlacyyjy7nwkjnhizg69r")))

(define-public crate-uucore_procs-0.0.16 (c (n "uucore_procs") (v "0.0.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0kbk5k9jic6dizj47izkpdwd6cy94ps2fdv90mq41srcxvsh4gbi")))

(define-public crate-uucore_procs-0.0.17 (c (n "uucore_procs") (v "0.0.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "07hhj08gql3y6fb13a48kqfi1i8lx1d1xdrranpj0hli2adq9cjk")))

(define-public crate-uucore_procs-0.0.18 (c (n "uucore_procs") (v "0.0.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "17dhqrnj1f6qrcx5l8400qrpp26b3gvk2kv9kmmapi060a386x0v")))

(define-public crate-uucore_procs-0.0.19 (c (n "uucore_procs") (v "0.0.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "uuhelp_parser") (r "^0.0.19") (d #t) (k 0)))) (h "0q6p31h71vkcm8hv8x09c9ig1vm7l5780h13y629y6932amhz5yh")))

(define-public crate-uucore_procs-0.0.20 (c (n "uucore_procs") (v "0.0.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "uuhelp_parser") (r "^0.0.20") (d #t) (k 0)))) (h "0ijd7nw9y4jxnjac93gf2qbp92jhn1y00n24x5d196ym9paqjrpx")))

(define-public crate-uucore_procs-0.0.21 (c (n "uucore_procs") (v "0.0.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "uuhelp_parser") (r "^0.0.21") (d #t) (k 0)))) (h "0mnmj4vgpcg7nwng9q4lzcgvfz9ngwwvdcn3np3pqf8xl65q4iq8")))

(define-public crate-uucore_procs-0.0.22 (c (n "uucore_procs") (v "0.0.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "uuhelp_parser") (r "^0.0.22") (d #t) (k 0)))) (h "1d7aicpsy0b2nxjn5f9nvbixiw1yrcxgkbxfyi4cz1ii4pwgf7hw")))

(define-public crate-uucore_procs-0.0.23 (c (n "uucore_procs") (v "0.0.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "uuhelp_parser") (r "^0.0.23") (d #t) (k 0)))) (h "066dbw7y5b5anxhsaih3pmjalc5dxblars30k4948847kn2cm6ny")))

(define-public crate-uucore_procs-0.0.24 (c (n "uucore_procs") (v "0.0.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "uuhelp_parser") (r "^0.0.24") (d #t) (k 0)))) (h "14p34jxmvjhq0qh2lawhh0a3wvj3ygfxvb2i7ddmrwfi0vmsxf9y")))

(define-public crate-uucore_procs-0.0.25 (c (n "uucore_procs") (v "0.0.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "uuhelp_parser") (r "^0.0.25") (d #t) (k 0)))) (h "02bkjwh4fp1cc8cmp6jqda8f48kpav9iqss424cb2lrfhfa1bxgp")))

(define-public crate-uucore_procs-0.0.26 (c (n "uucore_procs") (v "0.0.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "uuhelp_parser") (r "^0.0.26") (d #t) (k 0)))) (h "080x83pa6sizsb90ksg79mzg7qv9m5srz2ma1bdksbx4im43l8qs")))

