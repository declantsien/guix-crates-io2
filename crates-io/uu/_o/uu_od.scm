(define-module (crates-io uu _o uu_od) #:use-module (crates-io))

(define-public crate-uu_od-0.0.1 (c (n "uu_od") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "half") (r "^1.1.0, <1.4.0") (d #t) (k 0) (p "half")) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1id4xrxabwahv2lrbvdls56i0ld1bl49jkyca45hr6bjabm5jg8p")))

(define-public crate-uu_od-0.0.2 (c (n "uu_od") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0nph7c3gmv63x4cpb5hbr62k3acd6c6y5mkmn1ll49svh35dv901")))

(define-public crate-uu_od-0.0.3 (c (n "uu_od") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "09wrn2bnq99v4s15y1m1plav35wbxz1wbijsvcwcswqrjawibiv2")))

(define-public crate-uu_od-0.0.4 (c (n "uu_od") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "06swkymkc91q0ga1fjvaf1hfj2blnmw7qk6l061kc94f84y0ym10")))

(define-public crate-uu_od-0.0.5 (c (n "uu_od") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1jafmi0xypbsbm6d79nhcdk26z1kvfxqbnq0lbl2jwgasdl5aydy")))

(define-public crate-uu_od-0.0.6 (c (n "uu_od") (v "0.0.6") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1idgjg4zgrivgqrv9j37dzl1r66qrykw3v0wvkzdjl240p154wpd")))

(define-public crate-uu_od-0.0.7 (c (n "uu_od") (v "0.0.7") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "14qm4jddgswswd2d0fji8iq1vq3j9w1j75szqbnsp5lgjyd80cwf")))

(define-public crate-uu_od-0.0.8 (c (n "uu_od") (v "0.0.8") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "131m734cs85hivz1c3ifz74x7y4r2y06xhqglzb4917b9j4xfi79")))

(define-public crate-uu_od-0.0.9 (c (n "uu_od") (v "0.0.9") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1q84a6x568s8qpdzv7x08ahfvl4h4cf966228jjinwzi13x2r6vk")))

(define-public crate-uu_od-0.0.12 (c (n "uu_od") (v "0.0.12") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1wxch9wsvcfj266qv5m507sy52wvm7rq5aba9nhgdqvkqslkmkw4")))

(define-public crate-uu_od-0.0.13 (c (n "uu_od") (v "0.0.13") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "10cllirgh0ms59p6rhqpj0svrgr7d42i4jf2zdhb7rdbwjqvl6nc")))

(define-public crate-uu_od-0.0.14 (c (n "uu_od") (v "0.0.14") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1l7r6j9kvw08ra6h9yg05r0i7cqgnpl8yr9lqlscnldx8188js9k")))

(define-public crate-uu_od-0.0.15 (c (n "uu_od") (v "0.0.15") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0mwq5wdkjvkcd9q0fjhyarxk7kd7x5ajqr9k351g99xlcr0pa7dy")))

(define-public crate-uu_od-0.0.16 (c (n "uu_od") (v "0.0.16") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^2.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1fs7qq4f743yq1b63jkb0kw3iw2zmsi4jpl475b2syfnqrzpfk16")))

(define-public crate-uu_od-0.0.17 (c (n "uu_od") (v "0.0.17") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^2.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "152fspnrg7vrgp3nnqymgnc5vv2dva5x86frcz5pila2q3b4xd68")))

(define-public crate-uu_od-0.0.18 (c (n "uu_od") (v "0.0.18") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^2.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "09sjzjfjxdwjb4y0mv9lzdcb7ylrg53jlm13l5h17wyhbl6fgx7l")))

(define-public crate-uu_od-0.0.19 (c (n "uu_od") (v "0.0.19") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^2.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0jzbpx0jrrz6cxmv5gk0kq3pl5q9k7jpn0vdxvnirpm2acrkpjwl")))

(define-public crate-uu_od-0.0.20 (c (n "uu_od") (v "0.0.20") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^2.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "10jf3f84xnsx93wn4ibf8pc1abxs3wsafpz77hgnghc259fjfaw4")))

(define-public crate-uu_od-0.0.21 (c (n "uu_od") (v "0.0.21") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^2.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "07y8yrz7angx6hfr7lzl8iljg4hnzzk3wz2ppdjxs3x6r2f11fcl")))

(define-public crate-uu_od-0.0.22 (c (n "uu_od") (v "0.0.22") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^2.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "06xy7yxb1pbz15p8s9pgvnvszib5cfbi406p2cmw0rrzcaf4r072")))

(define-public crate-uu_od-0.0.23 (c (n "uu_od") (v "0.0.23") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^2.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "18ms7388lll51aw03fphnxs9zgl0m7ihv4zxzvwa200a1gl8j473")))

(define-public crate-uu_od-0.0.24 (c (n "uu_od") (v "0.0.24") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^2.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0034kkikj12d8wjcsv2jv8hm4l47vbifpvmqxk6radci00ss865r")))

(define-public crate-uu_od-0.0.25 (c (n "uu_od") (v "0.0.25") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^2.4") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "18h0gy71rb0zhb6vy8c57blhx0mgs8x9g7sakf632cmj2jdh2c1y")))

(define-public crate-uu_od-0.0.26 (c (n "uu_od") (v "0.0.26") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "half") (r "^2.4") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0z471adc45dnww7frlid231pkms59k49g3la51lrm65ckwj0mwg6")))

