(define-module (crates-io uu _d uu_dirname) #:use-module (crates-io))

(define-public crate-uu_dirname-0.0.1 (c (n "uu_dirname") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0jafsdc92ds3pydrza759khp3qbsn1c9iy96p7bhwsyw5clyyn14")))

(define-public crate-uu_dirname-0.0.2 (c (n "uu_dirname") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "055hp11cw6jpiynrxl6dknwq0dpdjmr8pyb9wy5606sqz40sd3d1")))

(define-public crate-uu_dirname-0.0.3 (c (n "uu_dirname") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1qcjg9ykkgjmnk6g21valm35gs70lb2p6gfvwn8877lw6g2v5dg9")))

(define-public crate-uu_dirname-0.0.4 (c (n "uu_dirname") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1lzq3818mcnfshk7r0zrgz5rz8943mx2knzbrw1jb3kadavypf3y")))

(define-public crate-uu_dirname-0.0.5 (c (n "uu_dirname") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1hj8kjdyrkld3i4x22psgjcpqr30l9makjxnifkxjxs6hqnp2s4a")))

(define-public crate-uu_dirname-0.0.6 (c (n "uu_dirname") (v "0.0.6") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "169idzhyrh51haiwq4frnjy5yqwzdjjpafkkc1wvav431cp3mng2")))

(define-public crate-uu_dirname-0.0.7 (c (n "uu_dirname") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "07xwkyg3bfxr16cwijggs5xiwclhq78qxrs6rm1p64ckls5sm2dl")))

(define-public crate-uu_dirname-0.0.8 (c (n "uu_dirname") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "05iczpblr9rbhcgac0cv5h1872rx20jyxyk1zxh5ghjmxlw14px6")))

(define-public crate-uu_dirname-0.0.9 (c (n "uu_dirname") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1wl6gplzmiyf8yn2n9kgg2zzi5m7csiadzy89b89r7ha1z2ypgrx")))

(define-public crate-uu_dirname-0.0.12 (c (n "uu_dirname") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1hgx8hh8r6w9d31qy3dwdkpylizpwqjwsphv4y6r5iqkrlkbzx6p")))

(define-public crate-uu_dirname-0.0.13 (c (n "uu_dirname") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1b4crnggdpxic19dlfki92fh4n3zng3xgsjcdw48g9w1r2iixb89")))

(define-public crate-uu_dirname-0.0.14 (c (n "uu_dirname") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0bssqxx80q9lnac39lc2z94xbb9nhqnv2pg2k64hx6m3bmnq6gi7")))

(define-public crate-uu_dirname-0.0.15 (c (n "uu_dirname") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0i5fcch4fwi6bf41nf6k4v8h1zd75sbzl8bpnka5j1kxi7v4z1aq")))

(define-public crate-uu_dirname-0.0.16 (c (n "uu_dirname") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "0c849vxy9cjb6xxg81h03yc7534rmkdrivs60xmi92xyxvv62zaz")))

(define-public crate-uu_dirname-0.0.17 (c (n "uu_dirname") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "13lyyp2jqaw13r32xxjx45fbaydcs5j627dcj4wk0a3jv3azjxkr")))

(define-public crate-uu_dirname-0.0.18 (c (n "uu_dirname") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1y36m4r9ymsb8cvqwlb0llivyjjs07rvy2ls4b6ny9vnhrfr6p2r")))

(define-public crate-uu_dirname-0.0.19 (c (n "uu_dirname") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0rh8xfy9vrncr088mi1kk0zpgs5i0adqs515ljrxc0wjk9q4sk1z")))

(define-public crate-uu_dirname-0.0.20 (c (n "uu_dirname") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1msr23hvkxjcwl29w5vf9a90b1ifnyv22rp450vmkkgx8caf9n0i")))

(define-public crate-uu_dirname-0.0.21 (c (n "uu_dirname") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "13hqg66jk1r71bxvgp1x1s99cfwjaxq9qkfdssb79q0q4ldb8sds")))

(define-public crate-uu_dirname-0.0.22 (c (n "uu_dirname") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0hm81px04xff8zx58j1pn2lmq0bab8cm7qmpagilr06ijkyjk00g")))

(define-public crate-uu_dirname-0.0.23 (c (n "uu_dirname") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "15dh2hhvp5ha95rd8az7z51xqvkr7gr9i4k9a9cbjmsdcjra8i7g")))

(define-public crate-uu_dirname-0.0.24 (c (n "uu_dirname") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0irs9gr8c54cwiqfa71l1yan0wkxdj02m21nc5r1bhybl3q5cafi")))

(define-public crate-uu_dirname-0.0.25 (c (n "uu_dirname") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0lk9lk0b2zx42iaj2wbbv593wwf2a17c1ra2favjsj0dymfn555x")))

(define-public crate-uu_dirname-0.0.26 (c (n "uu_dirname") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0y391rsmfpjj838xj4j1fiw0s9hnqah4q9yx987gzvzmqkrq6a5z")))

