(define-module (crates-io uu _d uu_dircolors) #:use-module (crates-io))

(define-public crate-uu_dircolors-0.0.1 (c (n "uu_dircolors") (v "0.0.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1xqwf4w8a0mk7cvgpxfzmr86qi08hh0fp31ya3yyypcjfyg0im21")))

(define-public crate-uu_dircolors-0.0.2 (c (n "uu_dircolors") (v "0.0.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1p8n55ympw8vq4h2s89hjhr8rr2785bd68y3j7hdr7rgnpyzzgrp")))

(define-public crate-uu_dircolors-0.0.3 (c (n "uu_dircolors") (v "0.0.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0vh8ay3hpsv842vy6cqp8cr1pigy6af7ra1jzyr4b7rsam71krgv")))

(define-public crate-uu_dircolors-0.0.4 (c (n "uu_dircolors") (v "0.0.4") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1rbalgkh9lpdkbsx105sqkwzfgvjv6fr22sxg5p5i2p5hhbibg9w")))

(define-public crate-uu_dircolors-0.0.5 (c (n "uu_dircolors") (v "0.0.5") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1gxas6pjfqlg4bn2hf2zmip0w2c8h9n7hxxv34wzjwiy7vgh6v5d")))

(define-public crate-uu_dircolors-0.0.6 (c (n "uu_dircolors") (v "0.0.6") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "19lgwpxcsdk29k96kvsqrzr0dmahx8mkng686slm4qjqhywf3h7j")))

(define-public crate-uu_dircolors-0.0.7 (c (n "uu_dircolors") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0gpbq0cm910g982sl319xidky50355dx3s9shnan3pi06skjyz5j")))

(define-public crate-uu_dircolors-0.0.8 (c (n "uu_dircolors") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0xyha1sb9xhf7walrylf358h768x8d6r4175qrbl4p0qi74qibdh")))

(define-public crate-uu_dircolors-0.0.9 (c (n "uu_dircolors") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "01rzsyrx906lnbsfwywby4y7j6wm3ln5rh1mkfvcw0hw2wninzlm")))

(define-public crate-uu_dircolors-0.0.12 (c (n "uu_dircolors") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0pg20rrz44ly9jpc8z0prrsvrw4yinf6ird59c3p8z7q6wzpzaxn")))

(define-public crate-uu_dircolors-0.0.13 (c (n "uu_dircolors") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0gjgf39b2bk9dwwlf4772n17i2bc96wjf6af6qsxizfkms9371n3")))

(define-public crate-uu_dircolors-0.0.14 (c (n "uu_dircolors") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "13l82a35bm6gvsy29a8q39kidq73b8m8wf7xbbnn34jbxh1gkdzq")))

(define-public crate-uu_dircolors-0.0.15 (c (n "uu_dircolors") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0glvk2z5cwsppb5k94j1yk67j24kxssydb2xg2r9xhbrfdkxqkzz")))

(define-public crate-uu_dircolors-0.0.16 (c (n "uu_dircolors") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "15hizabki9pkbqq9bnskm2lpam9x9wmnfzpcwd7936ilgdy1avcq")))

(define-public crate-uu_dircolors-0.0.17 (c (n "uu_dircolors") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1a042yxxz9nka37ayp5349416ck35a1q5xndzsxdin33h3wi5y2z")))

(define-public crate-uu_dircolors-0.0.18 (c (n "uu_dircolors") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0i4clkpilmz61q6vxfmqmijrb3prvnqp2194lybdfb061p0f27f6")))

(define-public crate-uu_dircolors-0.0.19 (c (n "uu_dircolors") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "14aqk84w7ih4pvc5xa77iga8k8dbak3639zixc4ajwl2y4ibzamc")))

(define-public crate-uu_dircolors-0.0.20 (c (n "uu_dircolors") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0y4zrm6zlkhpgnyj383kc3850r818r7x8kxmqslk84y4q8cm7i8j")))

(define-public crate-uu_dircolors-0.0.21 (c (n "uu_dircolors") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0jhp3vf3d8bnk9zv8skbd77bw4hrmxv921ylv3wczd41q7b911vg")))

(define-public crate-uu_dircolors-0.0.22 (c (n "uu_dircolors") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1mc8whrirvg9vjcp9239in4k42bnjdziag8gh59bkjlwhi31rqaz")))

(define-public crate-uu_dircolors-0.0.23 (c (n "uu_dircolors") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0bxn0fvd5xp2wgj0awgk0lmf7916azhikc3qy8kkvhphc2g1mqjr")))

(define-public crate-uu_dircolors-0.0.24 (c (n "uu_dircolors") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("colors"))) (d #t) (k 0) (p "uucore")))) (h "17iw9ql80ip5d7lj6zbzldpkrzf9pmicb38m4ghw7v87n5apsz3w")))

(define-public crate-uu_dircolors-0.0.25 (c (n "uu_dircolors") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("colors"))) (d #t) (k 0) (p "uucore")))) (h "1wrvvr8786ycblp2256131n2y5mxdm8lf17adg6w46kpmwfi49lf")))

(define-public crate-uu_dircolors-0.0.26 (c (n "uu_dircolors") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("colors"))) (d #t) (k 0) (p "uucore")))) (h "1smx9jxg2wpn1zrapn2jizs6papbdk3lpwxwz6w39zr453xmj29h")))

