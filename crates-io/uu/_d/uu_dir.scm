(define-module (crates-io uu _d uu_dir) #:use-module (crates-io))

(define-public crate-uu_dir-0.0.14 (c (n "uu_dir") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "selinux") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.14") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1rykicwn711zq7y9yyjvdnpzs5rxr5x4mwk3b97qxqrqnzczj6w5")))

(define-public crate-uu_dir-0.0.15 (c (n "uu_dir") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "selinux") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.15") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "05fzd1wvp1r6922fz91d2k3j6vnhq767r8w044dc8q1pgpj7fij6")))

(define-public crate-uu_dir-0.0.16 (c (n "uu_dir") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "selinux") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.16") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1r0s2arydqjrc5wmgipxaz28rkap7v968j4dj9vq0rc1r1vxn6q8")))

(define-public crate-uu_dir-0.0.17 (c (n "uu_dir") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.17") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1b14vjjxaw91cnx1a9xpg8bwx8qwzcggs49ywcs0md8gv4f1fd4m")))

(define-public crate-uu_dir-0.0.18 (c (n "uu_dir") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1idmznsgf7mazsvjgygdpj5r4pskfb4h47x07wrqag29h3nq8fdc")))

(define-public crate-uu_dir-0.0.19 (c (n "uu_dir") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "0hil9dzpsb919nfp9chricn9ks561g5zznnjbak4malsmad2kr2q")))

(define-public crate-uu_dir-0.0.20 (c (n "uu_dir") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "0pyia1c3rarqc4ylgajddidr3jfd4kc7clisi2a70haxfpd37qib")))

(define-public crate-uu_dir-0.0.21 (c (n "uu_dir") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "054mbfsq971r756sy2yrydk82p2k9by57z4v93hmch4ndyksymrs")))

(define-public crate-uu_dir-0.0.22 (c (n "uu_dir") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "1l5slz4pzkcm30834nkcz02b35bmyvps0v4xrrwmg91i6n5x99cl")))

(define-public crate-uu_dir-0.0.23 (c (n "uu_dir") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "1hc79m94pw09ixcxxf44x1jfrl9zp6ywy2cdlij9wdqjcizkv9ps")))

(define-public crate-uu_dir-0.0.24 (c (n "uu_dir") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "1i79n9666ywjgywra03n7m9988jgix2v2kzdfph22dj93fvk5z5j")))

(define-public crate-uu_dir-0.0.25 (c (n "uu_dir") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "0vrgj7sm89dafyk6p5f1j28lbzcbcc97q92qm1lj6ap9gi0llhkn")))

(define-public crate-uu_dir-0.0.26 (c (n "uu_dir") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo" "env"))) (d #t) (k 0)) (d (n "uu_ls") (r ">=0.0.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "quoting-style"))) (d #t) (k 0) (p "uucore")))) (h "0ssjnwhivaxbgslygksjskyn1jax7ys9ckqslncbs2pkn3dbqiap")))

