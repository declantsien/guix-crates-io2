(define-module (crates-io uu ge uugear_ffi) #:use-module (crates-io))

(define-public crate-uugear_ffi-0.0.1 (c (n "uugear_ffi") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.37.2") (d #t) (k 1)))) (h "0fgzh0pzi7skpfhjsqiwbaxwy1d6f0rdhw9f699p7avrnrjdr5gy")))

(define-public crate-uugear_ffi-0.0.2 (c (n "uugear_ffi") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.37.2") (d #t) (k 1)))) (h "1sm4i3vpirj0pa5zahfv4in4iw8d732mlf3xvim970x700vh195i")))

