(define-module (crates-io uu _l uu_logname) #:use-module (crates-io))

(define-public crate-uu_logname-0.0.1 (c (n "uu_logname") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0754pg85a8g2nszzjr59lv382dkj930152maji1s8haqi70nrf4b")))

(define-public crate-uu_logname-0.0.2 (c (n "uu_logname") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0ihsw5mza00h535swcfdg69vb28236lymvmsjbm743s87ds9bx4c")))

(define-public crate-uu_logname-0.0.3 (c (n "uu_logname") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0sdgh7hcaj29dj2bf73gpwxgwzl9c9sswrqdi89yrwasa8ff4kay")))

(define-public crate-uu_logname-0.0.4 (c (n "uu_logname") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "12nx0izxm1v54g345an8v5qhjwir2hkkq6l7jq7ddgjvsqb7c4q4")))

(define-public crate-uu_logname-0.0.5 (c (n "uu_logname") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0yaa2n5mgpv4l66n83qb306lvbmg628g39kk87cydycxkwgc08l1")))

(define-public crate-uu_logname-0.0.6 (c (n "uu_logname") (v "0.0.6") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0wjh5brz3bkfkr922mmnxq68ssx020rn7qcakkil4s0pp6c5q1v7")))

(define-public crate-uu_logname-0.0.7 (c (n "uu_logname") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "11nlsp5j977h7qx2nlgc5lsnqa9ibklmclh75qcrqqbh5r3g2ny5")))

(define-public crate-uu_logname-0.0.8 (c (n "uu_logname") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "10m89q1qkgq3rh5na3w20f0b3170alpwb4ziylmhdfkzcjlixc0s")))

(define-public crate-uu_logname-0.0.9 (c (n "uu_logname") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0ff7772x3g3x5scgscnzz2v8baqaypbbvyl4dxvwcxpaiqivcs9n")))

(define-public crate-uu_logname-0.0.12 (c (n "uu_logname") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "14cznsz6msgqi27vnv28am0zfq3kvlydx5wy155ns9449wrg607w")))

(define-public crate-uu_logname-0.0.13 (c (n "uu_logname") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "16vm5ddgk4g3miqrwyiz71hilvgs0jc3ivxfspbrcazjx726c2ny")))

(define-public crate-uu_logname-0.0.14 (c (n "uu_logname") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "092i5vyjzzn9g6wpnjkb3va3dra3ys0ijw6x12sdvx73abccbi6l")))

(define-public crate-uu_logname-0.0.15 (c (n "uu_logname") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1qs7bzznd2wrkscy9cyvxg4sy1cpmkgbdgswr93yv5iaigdcnsgz")))

(define-public crate-uu_logname-0.0.16 (c (n "uu_logname") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "15hdyiymhwry3r8617fn4insbkdvklsb2ji1ysxnzqfk7jlxqvd1")))

(define-public crate-uu_logname-0.0.17 (c (n "uu_logname") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "0k7ki3mw317k0ca1dfc2lfykbi56c25qbl5c67f1q1k0aahi0h1z")))

(define-public crate-uu_logname-0.0.18 (c (n "uu_logname") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1kw8lbz866h8jl3c43yvf7bwcqg7nr9imz46n9ihmxcb0fgqz1d0")))

(define-public crate-uu_logname-0.0.19 (c (n "uu_logname") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1sm2l7kd82fd84jvbkqbrn9a1g03yrji2c9mdk2x2083inzm79ir")))

(define-public crate-uu_logname-0.0.20 (c (n "uu_logname") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0r75ximsqkswqxq3zh4bbky16vym1b9cyaqgbf3mg85j54zs01lz")))

(define-public crate-uu_logname-0.0.21 (c (n "uu_logname") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0x0chvva4jyz80snffds79rw6h328bz128398ri3ns9j2bp67qsm")))

(define-public crate-uu_logname-0.0.22 (c (n "uu_logname") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1qiv5bx2iri1yndhw0mzfn4dyjyw6clr0dsy8slkdwfah0xdmvpp")))

(define-public crate-uu_logname-0.0.23 (c (n "uu_logname") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "06lkm1ihjx3igh8asz3c9zdk1b4f7r2acwgii03g5h0yhy0vkdjd")))

(define-public crate-uu_logname-0.0.24 (c (n "uu_logname") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1xaphfp3dh45ipfvv4yl274v80h384q411sh9yaxhqycnhpmmnwv")))

(define-public crate-uu_logname-0.0.25 (c (n "uu_logname") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0rz13wkr5gidcxpimldb38xbpy7fj81sml27j6js3yw9mkm2dwc3")))

(define-public crate-uu_logname-0.0.26 (c (n "uu_logname") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "10ln00s4x2ya2canrgvvmljb0fvawskfkfc8wadcsgqlkzhcxzi1")))

