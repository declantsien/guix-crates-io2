(define-module (crates-io uu _l uu_link) #:use-module (crates-io))

(define-public crate-uu_link-0.0.1 (c (n "uu_link") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1xsb1p5np4carbjmlsrdal4dhk0an7wnnik72ljqy89mf8bwcp5h")))

(define-public crate-uu_link-0.0.2 (c (n "uu_link") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "025ngw3h649fnbf12mpg4f2k6r4m575hb33xxr6nvp44xw8xq5cp")))

(define-public crate-uu_link-0.0.3 (c (n "uu_link") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0x93zfa5ijhqrc6pac397zal99hai6dd70qy3hhyys7ib01wiann")))

(define-public crate-uu_link-0.0.4 (c (n "uu_link") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0xci3n7xqxvmdh36vv1q30pia556sg5601zqraq9fb5vj9sqsn5i")))

(define-public crate-uu_link-0.0.5 (c (n "uu_link") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "10cq59vria3mi5834h0g8vayp65yw20x07ca5w9w05q671djgbkc")))

(define-public crate-uu_link-0.0.6 (c (n "uu_link") (v "0.0.6") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1jfs6r3vs5rh4iqrr7yz9rdks753wpch6h6p5y968dwiw579mqjd")))

(define-public crate-uu_link-0.0.7 (c (n "uu_link") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1facqcqppi0mg8w7bw5j1qkmfn34wawpd264s1b0vw4cd80km80s")))

(define-public crate-uu_link-0.0.8 (c (n "uu_link") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0b29msllvqra2489pih023yipxglpfzm9d5z5s917wx7skq0ii7g")))

(define-public crate-uu_link-0.0.9 (c (n "uu_link") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "04921ygb1qdibvsnb444l1c9maf8slpcs1px96161k9xfbwpmalv")))

(define-public crate-uu_link-0.0.12 (c (n "uu_link") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1x3inyrdfqabi3k750rrp4bmpv5m2dxj6s2m8xbh66dq3pi8sndz")))

(define-public crate-uu_link-0.0.13 (c (n "uu_link") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1n2waqffzy7fhl85vh9bqi3zcan5ib56q4d0f8c3n8wqwhylshvg")))

(define-public crate-uu_link-0.0.14 (c (n "uu_link") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1wrzy4ip8p2mdxf8l484r9zd0ajyhl40j557liaddmbqsb6hbf0w")))

(define-public crate-uu_link-0.0.15 (c (n "uu_link") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "048sm485fcg02rrk0ik0wzlw0xjz7nj466z1amvp2c7ivk618vv8")))

(define-public crate-uu_link-0.0.16 (c (n "uu_link") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "0kr75a04zvfk12krdzxskhmgyshyn9382l4jbll5z10iy7ngn63y")))

(define-public crate-uu_link-0.0.17 (c (n "uu_link") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "0g50wmf2l3g7ys33cr6frs8c9pj32jx7ysm0gv8r1y81hq2xdx1l")))

(define-public crate-uu_link-0.0.18 (c (n "uu_link") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1w4n0rs50a5gbahlq05kljzjj87y258cm9ga1c9f5hj1xvw5wkz5")))

(define-public crate-uu_link-0.0.19 (c (n "uu_link") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0dgx2wdwgbckxhrdpjvag2wlzm52n3fk2lzaln84swzijjmdb4d5")))

(define-public crate-uu_link-0.0.20 (c (n "uu_link") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1vjj65dizdm91hlqh8y886c3c4b6w3iq3hsfh6qssvvy71hcgp43")))

(define-public crate-uu_link-0.0.21 (c (n "uu_link") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0ckd51lpgyqva400jjhv6amhrzipck3h4rfi4d8pbk1isfdfzqz4")))

(define-public crate-uu_link-0.0.22 (c (n "uu_link") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "08ra138zj7min5jy6zkpfia20dg0zg7z5bli3mqzywvwphppgvd4")))

(define-public crate-uu_link-0.0.23 (c (n "uu_link") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0p7jdxal1bjr887f0253araib5cdpbdv4wk77z2dgdd89mmj79dq")))

(define-public crate-uu_link-0.0.24 (c (n "uu_link") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1fm111kfznm18fkvvgwkzr0pzw7qqp8d946sgqg1b2xh3ij7n722")))

(define-public crate-uu_link-0.0.25 (c (n "uu_link") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "05aikqc6k914rqifli0lbr0r42zavxrx4glc79q37vw4dg3139zs")))

(define-public crate-uu_link-0.0.26 (c (n "uu_link") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0pv363mj542jgdxwl7hk08yy6h5vg7vsw7rljpah9p3xf997f7sr")))

