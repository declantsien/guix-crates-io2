(define-module (crates-io uu _l uu_ln) #:use-module (crates-io))

(define-public crate-uu_ln-0.0.1 (c (n "uu_ln") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0sfdk5ynyv0cwl07ikzmwz6ib0wwdqr5k324g4s5m2gk51kzacav")))

(define-public crate-uu_ln-0.0.2 (c (n "uu_ln") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1pz8ir421wbm1nr1vy95acw1p81j20904fj1i7jbpsipy7s4jb09")))

(define-public crate-uu_ln-0.0.3 (c (n "uu_ln") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "19bks9dn3p61pvazqkzbrqr7k8xhqr44rmw7w96j6sfx4smi0fwd")))

(define-public crate-uu_ln-0.0.4 (c (n "uu_ln") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1fn6hmfz8v8wn8j14jdk8zgvzgwb5xxd4jjlnfvxq6rqsdm0r6w6")))

(define-public crate-uu_ln-0.0.5 (c (n "uu_ln") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "17icrvhxvxwfawhlf0bmdph9jdj3n5pnbhsf0fdi76y4r5pi55d9")))

(define-public crate-uu_ln-0.0.6 (c (n "uu_ln") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "02nsbxmwcrk0pp0wpjx9w6y97313znm4g93lz8qcc2bvnx1p05lr")))

(define-public crate-uu_ln-0.0.7 (c (n "uu_ln") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "16ivwf55jdlp4bmxwrfmlafm94l6w4lasqq3c1157z8dybbav5dc")))

(define-public crate-uu_ln-0.0.8 (c (n "uu_ln") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1fapa2acmg6hzc2p8bzprh9vb7w5x2g8cfzvw7j0vbqxx7dn2dkw")))

(define-public crate-uu_ln-0.0.9 (c (n "uu_ln") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0pnnak3yamgzx8k6vy1hyyd4ig68y0nhnjx057l9ym5v8xryzygn")))

(define-public crate-uu_ln-0.0.12 (c (n "uu_ln") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "17v6sy6v7rs0h3jkldn977g003vpv8v9w9id3i3arg111i0khvga")))

(define-public crate-uu_ln-0.0.13 (c (n "uu_ln") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1pfqfqmh6bihg9kww46y7937qlsxljcnl2px0xra6vbwcghlka30")))

(define-public crate-uu_ln-0.0.14 (c (n "uu_ln") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1d2n1lbspz5kn75dkys2ll0a9czjhmm0v5gygllb63a9q2j1qzfa")))

(define-public crate-uu_ln-0.0.15 (c (n "uu_ln") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "12yh0kpad8jsibrxh1mmkcbavg7vf8gj1bkvmdc6m7nz94mfjxa3")))

(define-public crate-uu_ln-0.0.16 (c (n "uu_ln") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "08kk7aaycqfrpfkhlwaivg9j1l390ashxsy3zzp6yq8rj276dh2c")))

(define-public crate-uu_ln-0.0.17 (c (n "uu_ln") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1zqy8wl6j96bn622if2bacrdms7m5b5d49lr6ay27hvxj73k7f37")))

(define-public crate-uu_ln-0.0.18 (c (n "uu_ln") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0cdiy7l5jjqcw191g6ch7688w1s9g7dfm98daav6xnd827z3333v")))

(define-public crate-uu_ln-0.0.19 (c (n "uu_ln") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "06z98dyfgnlbfi13r17nzix9f5lz49i502gfv7bplw66fm3wf3vr")))

(define-public crate-uu_ln-0.0.20 (c (n "uu_ln") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0nw8mlr4wzbkrdw1vqp3hc273v92sdbc4rnby328d5067schr4zm")))

(define-public crate-uu_ln-0.0.21 (c (n "uu_ln") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs"))) (d #t) (k 0) (p "uucore")))) (h "021q4rbv63ax0k28kcmz315h8m8yshp9r1xy6v754bpfqhdcsbhg")))

(define-public crate-uu_ln-0.0.22 (c (n "uu_ln") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1pv0ax7l749nychwd6naspc4agqw80qrvp77raazz66mslvijdky")))

(define-public crate-uu_ln-0.0.23 (c (n "uu_ln") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs"))) (d #t) (k 0) (p "uucore")))) (h "051pi5rz5qqslckqq43542x96q0kysq7cvkpp59qjjkmjxnr1f0c")))

(define-public crate-uu_ln-0.0.24 (c (n "uu_ln") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1zslidjn74x91ipzizrv4pi7k9smh3kwbv66fb64whs7b7lb4bg2")))

(define-public crate-uu_ln-0.0.25 (c (n "uu_ln") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs"))) (d #t) (k 0) (p "uucore")))) (h "0hc5b1zl5s5w7n7r3b1k734ksmrikjs2qsi8k7zk8aqz7nbz8mhv")))

(define-public crate-uu_ln-0.0.26 (c (n "uu_ln") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("backup-control" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1n8z2bq9m1q6bm8pwxcxijd6fg1gaazd4ii5byznbsg47r2y0x31")))

