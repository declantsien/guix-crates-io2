(define-module (crates-io uu _j uu_join) #:use-module (crates-io))

(define-public crate-uu_join-0.0.1 (c (n "uu_join") (v "0.0.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1a34c3wbrsddlvw5cvrrqwff4933171v0givmfh0wv6b9iym0sdp")))

(define-public crate-uu_join-0.0.2 (c (n "uu_join") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0shgvrmn9xasksv0ph6g5ih21zvvdba9bnp2872yb7kh4i6k38zw")))

(define-public crate-uu_join-0.0.3 (c (n "uu_join") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1xz1am6mbxwmvpggpma4q46fz8m9xi28fs4bb5wlrn36bjnr028n")))

(define-public crate-uu_join-0.0.4 (c (n "uu_join") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0jxvznzpjf6jnzbax65bzirzfj6ddnml900bffrj54r8cnxf7hhp")))

(define-public crate-uu_join-0.0.5 (c (n "uu_join") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1l19s9wx3cfwmn1yb0dffdq4igfqc3mk2s401bf8smy6dlvnhjzj")))

(define-public crate-uu_join-0.0.6 (c (n "uu_join") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1mnjgfdxicsvgm2i9w4mgpvk5xizsf8n7ns7bc1r45c65s121yzs")))

(define-public crate-uu_join-0.0.7 (c (n "uu_join") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "11wrvfdc0574b6795hfn75mc861idfs8dnaydvl6j19nzzqr5mha")))

(define-public crate-uu_join-0.0.8 (c (n "uu_join") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "16a77s5c0007arg0dx72ilgr20c5r3j9j9x0l9v10grx4vlbqlc5")))

(define-public crate-uu_join-0.0.9 (c (n "uu_join") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1idsgg231hs473w6k64vm1i7ga4zk340d8lp3n2mnilz9hbmbgxs")))

(define-public crate-uu_join-0.0.12 (c (n "uu_join") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0nj0jjrrf27c69scnj7rx6ia21nfwhcffzl89wkyig0170sn6n0y")))

(define-public crate-uu_join-0.0.13 (c (n "uu_join") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1r88c27dqp1rxgwg8d3npnx2i6a2njasifpsmda5mmwlwyz8zp2q")))

(define-public crate-uu_join-0.0.14 (c (n "uu_join") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1cp9idqwbmmj7ivz8k76c68c29qppngn1p5wv04y76v9qsb1pvjl")))

(define-public crate-uu_join-0.0.15 (c (n "uu_join") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "0sz45avqkvrwf9hzw2gcfahklgdnc2v7cn3a5gpx53g3fr0j3jg3")))

(define-public crate-uu_join-0.0.16 (c (n "uu_join") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1im0jfbwf07r2p969xdb1qx61caihrd542dslpcdr4q1li546bl9")))

(define-public crate-uu_join-0.0.17 (c (n "uu_join") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "00m3lza5zj37vz2b1k0hxhaczwi0n5mwzi99pr090d40fnrigk8y")))

(define-public crate-uu_join-0.0.18 (c (n "uu_join") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1rwbhpk31dcf3wiq7mgxc8nk9jgkgpy7m5yz8lzvf6sqff3yxdsc")))

(define-public crate-uu_join-0.0.19 (c (n "uu_join") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0jma7h5497xbyfypk7hicgwj50ay6hd0xkdsbl1dyqqp1rsa6r0y")))

(define-public crate-uu_join-0.0.20 (c (n "uu_join") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1rs7m8bplfivxm7x6yi5xq2drzkii4295l4qk95fzrs60wkdvf6k")))

(define-public crate-uu_join-0.0.21 (c (n "uu_join") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1kb8y9klgkvmdjgjx7m2svmyp4d0r4i6nq3jqff9561966m5phh2")))

(define-public crate-uu_join-0.0.22 (c (n "uu_join") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1w1rqyqv49608agpf6jp2n0w3r6nvp9zrfh632c75g1ccghqq1ji")))

(define-public crate-uu_join-0.0.23 (c (n "uu_join") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1xsvw8dx189kz7qwakzqzdk0lrnqn1wb46324dkvf8drdgld4d3n")))

(define-public crate-uu_join-0.0.24 (c (n "uu_join") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1gs43c51yq4qwza772la9vkp7nyvl903331v3vs75hdm3lw7iqk6")))

(define-public crate-uu_join-0.0.25 (c (n "uu_join") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "14y5rd1ib4a8wpmj2dh9l2hzizdp6379qhqb2md8509vadghlhcj")))

(define-public crate-uu_join-0.0.26 (c (n "uu_join") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "03nf6him8rlrb7fab5hyhi4ksam0c66jq2j4sszysa37csinr4f2")))

