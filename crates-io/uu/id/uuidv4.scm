(define-module (crates-io uu id uuidv4) #:use-module (crates-io))

(define-public crate-uuidv4-0.1.0 (c (n "uuidv4") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)))) (h "1mmvag0bq43ib5r4a3l8g8kg91hh7yf2141bb7wy9ybyjckgd0kw")))

(define-public crate-uuidv4-1.0.0 (c (n "uuidv4") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)))) (h "0hxdsd73c1czpma0n3rwn3yg9ykw17nwx0fax67vgdx341a0lnrw")))

