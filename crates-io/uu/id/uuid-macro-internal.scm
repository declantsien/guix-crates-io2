(define-module (crates-io uu id uuid-macro-internal) #:use-module (crates-io))

(define-public crate-uuid-macro-internal-0.0.0 (c (n "uuid-macro-internal") (v "0.0.0") (h "19mpzz8wv3gxixsx9ffn5745l4ankyhfflfy30bdjsrwx2awam7f")))

(define-public crate-uuid-macro-internal-1.0.0-alpha.1 (c (n "uuid-macro-internal") (v "1.0.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "10d8nzlb8wv8qw375g9nf07j18h9h1byv8jqpds9rg98qp8fsh48")))

(define-public crate-uuid-macro-internal-1.0.0 (c (n "uuid-macro-internal") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "06r6ngqrrr76jlcl904kir76fgbz3xskr1rbriqmfxmgbm77nd8r")))

(define-public crate-uuid-macro-internal-1.1.0 (c (n "uuid-macro-internal") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "1r2iq70dn8yir4rs5kky17h4ivs43zja1y3a7ci19hsh5j54xh07")))

(define-public crate-uuid-macro-internal-1.1.1 (c (n "uuid-macro-internal") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "1ib3zaf391fk9xq7pzshpj8skg0a78y8jr7pahy4a3lpglj8cdmf")))

(define-public crate-uuid-macro-internal-1.1.2 (c (n "uuid-macro-internal") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0zf96l5vby3r8qrr6ncmm1bqp0hhljy7xfvs4d8gl3lrln0p33sl")))

(define-public crate-uuid-macro-internal-1.2.0 (c (n "uuid-macro-internal") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0pbgady32w8gdd3da6j57ppm7y6s9735wlif78jj8kh8b3gpagqw")))

(define-public crate-uuid-macro-internal-1.2.1 (c (n "uuid-macro-internal") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0jkww3arqgqfm90l1ynyq531kjqn8mbbwyskg3qa4prp1idk1k24")))

(define-public crate-uuid-macro-internal-1.2.2 (c (n "uuid-macro-internal") (v "1.2.2") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "1n3nw8vydhm5l3d32j3wgdwfd68rg71m400y4ijyd4s5i7r8kg3k")))

(define-public crate-uuid-macro-internal-1.3.0 (c (n "uuid-macro-internal") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "1jbccm5pk216zrrf0ibd1j7m86lgmsyibs8d59ykhak5g2l01cy1")))

(define-public crate-uuid-macro-internal-1.3.1 (c (n "uuid-macro-internal") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "0dd2kjwv29r7a92siah6xqfi8jwg6k4wc815hkf0k4sf702sbs10")))

(define-public crate-uuid-macro-internal-1.3.2 (c (n "uuid-macro-internal") (v "1.3.2") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "0rnimcf298pshv7czwy99f25f0y0z5i76z762gwx3ync1hvq9n0q")))

(define-public crate-uuid-macro-internal-1.3.3 (c (n "uuid-macro-internal") (v "1.3.3") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "1nj53qzhk4xqw6rkz33q40rjs0mx9pdbjg11xvhydwiayicv8rrz")))

(define-public crate-uuid-macro-internal-1.3.4 (c (n "uuid-macro-internal") (v "1.3.4") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "1r2dp8147b34xb47p74mn31cqlg3m6qwy4wz6xvdl5sjw45m1a3x")))

(define-public crate-uuid-macro-internal-1.4.0 (c (n "uuid-macro-internal") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "1h621ixc88dm3pidx65g6q95nwa72j6vacdw6v5gnxch1flds546")))

(define-public crate-uuid-macro-internal-1.4.1 (c (n "uuid-macro-internal") (v "1.4.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "1x6cp8gpd1vwdpsif9xgwfpns9dwzj9fazgjr7imrmiv6cgvmqgp")))

(define-public crate-uuid-macro-internal-1.5.0 (c (n "uuid-macro-internal") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "1bxdhbapp23b5wshx2dipyn1vfrj7dickvysa0lyi7hlkfx6p31x")))

(define-public crate-uuid-macro-internal-1.6.0 (c (n "uuid-macro-internal") (v "1.6.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "0l302x0w9la63dr3z5c6w5z4ilwwy4m87bhsdkmf2i7dwkdcpz2p")))

(define-public crate-uuid-macro-internal-1.6.1 (c (n "uuid-macro-internal") (v "1.6.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "0pl474ljbn40f08a8asy26bhdlwz4cr1k88h0w80l15q7lzpz7pl")))

(define-public crate-uuid-macro-internal-1.7.0 (c (n "uuid-macro-internal") (v "1.7.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "0b9apalk5dqhpmvid5a4pp8qqa9xyj74b2knm8zddnjh3ap19fvs")))

(define-public crate-uuid-macro-internal-1.8.0 (c (n "uuid-macro-internal") (v "1.8.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "18n10d9himcn2a5lwc3hw8178j6hdk1pidxkk9nf71z6rfkvx0cq")))

