(define-module (crates-io uu id uuidtools) #:use-module (crates-io))

(define-public crate-uuidtools-0.1.0 (c (n "uuidtools") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v1" "v3" "v4" "v5"))) (d #t) (k 0)))) (h "00wyd1rapx1yg4jh82bis4k0p1jspg1x4y161wp4bcclq95kw6zn")))

(define-public crate-uuidtools-0.1.1 (c (n "uuidtools") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v1" "v3" "v4" "v5"))) (d #t) (k 0)))) (h "0fgd8ind0h8sy0hw6dx2i9xj227j61yy0jb8ahrvj19nqgy3vkrd")))

(define-public crate-uuidtools-0.1.2 (c (n "uuidtools") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v1" "v3" "v4" "v5" "v8"))) (d #t) (k 0)))) (h "198lwi3ks2h6988d8byr0mhy8xw7flqxlqh8pjvvisnnic5875m2")))

