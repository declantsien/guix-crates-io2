(define-module (crates-io uu id uuidv6) #:use-module (crates-io))

(define-public crate-uuidv6-0.1.0 (c (n "uuidv6") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.5") (d #t) (k 0)))) (h "01dwm4wlqp4j9fnmzncqca2bsfn3jjgl894nk653cddi9zbc7zmv")))

(define-public crate-uuidv6-0.1.1 (c (n "uuidv6") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.2.5") (d #t) (k 0)))) (h "0axhnir6mm2f38ia4j54pzaf8jg3c603sdgr4hclsc5y3v6r16bk")))

(define-public crate-uuidv6-0.1.2 (c (n "uuidv6") (v "0.1.2") (d (list (d (n "getrandom") (r "^0.2.5") (d #t) (k 0)))) (h "1a1hipj6i1xa0a5kd3363iyby00vp55q5587pxgd4qjailrg5qr3")))

(define-public crate-uuidv6-0.1.3 (c (n "uuidv6") (v "0.1.3") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\"))") (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (t "cfg(not(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\")))") (k 0)))) (h "116rr7v47gsw7d4c13c3widg3fl1wx5dv7andsn9d5ildj8kdwgl")))

