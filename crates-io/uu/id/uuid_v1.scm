(define-module (crates-io uu id uuid_v1) #:use-module (crates-io))

(define-public crate-uuid_v1-0.1.0 (c (n "uuid_v1") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "interfaces") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (d #t) (k 0)))) (h "0h7lb2r4zb0fa7qh2y0xam5kihvbpxp76rmr5pqbamrrqlz0f21i")))

(define-public crate-uuid_v1-0.1.1 (c (n "uuid_v1") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "interfaces") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (d #t) (k 0)))) (h "0vb97d4qpcfzsdcydcs92kq1q6nq6gm7d9g10lybjx9k37ra09mv")))

(define-public crate-uuid_v1-0.1.2 (c (n "uuid_v1") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "interfaces") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0p5238cvpm2ylxhx3f28cnkn972rf47m6s4qwwfgycy2k78iviyh")))

