(define-module (crates-io uu id uuid-sys) #:use-module (crates-io))

(define-public crate-uuid-sys-0.0.1 (c (n "uuid-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1a92rbphd1fz3mr2dxqymkm93g9rigm4f4nkawdzvyd9vc6dnph9")))

(define-public crate-uuid-sys-0.0.2 (c (n "uuid-sys") (v "0.0.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "03x8kqx7ns9pkcff9wd9cspjy3900lys61mc79nx0givyz6nz30i")))

(define-public crate-uuid-sys-0.1.1 (c (n "uuid-sys") (v "0.1.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "0ap6885z44dp9ayd9xydwa76hwxpyzsp42pckykn0a8rqxq4f47d")))

(define-public crate-uuid-sys-0.1.2 (c (n "uuid-sys") (v "0.1.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "0rf6r38vjsbjz0rz19064ljf2gcr39a2sqlxhwr642ba0zcj3j14")))

(define-public crate-uuid-sys-0.1.3 (c (n "uuid-sys") (v "0.1.3") (d (list (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1ln51rvcyjjfa6gf664v3nglq3z7a2y0kbr5vnxk735nscgylg1q")))

