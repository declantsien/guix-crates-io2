(define-module (crates-io uu id uuid_macro_impl) #:use-module (crates-io))

(define-public crate-uuid_macro_impl-0.1.0 (c (n "uuid_macro_impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.7") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1v2p8y9jzrcvqc4acdsgyipca1zih2jy09cxjp8sxkh9s05j9ak5") (y #t)))

