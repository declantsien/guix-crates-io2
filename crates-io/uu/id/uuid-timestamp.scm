(define-module (crates-io uu id uuid-timestamp) #:use-module (crates-io))

(define-public crate-uuid-timestamp-0.1.0 (c (n "uuid-timestamp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 2)) (d (n "uuid") (r "^1.2") (f (quote ("v7"))) (d #t) (k 2)) (d (n "uuid7") (r "^0.3") (d #t) (k 2)))) (h "19hywjc15x2vb8qdysk4zmv6b06xnw03qy0rp39yvw98b0bxxw43")))

