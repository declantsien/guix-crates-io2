(define-module (crates-io uu id uuid_v1_variant) #:use-module (crates-io))

(define-public crate-uuid_v1_variant-0.1.0 (c (n "uuid_v1_variant") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ln6cmkm01vyy57aypsivgbjh666hkgiq7bxpwkkh8b2fv18ignj")))

(define-public crate-uuid_v1_variant-0.1.1 (c (n "uuid_v1_variant") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0xlqv2pf019yg3cpf4m1mwl7ax41jgsk81rbp0p576gzjicxxnmw")))

(define-public crate-uuid_v1_variant-0.1.2 (c (n "uuid_v1_variant") (v "0.1.2") (d (list (d (n "eui48") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0c9jx0zp545dd8vbclnjrl9v80fn7y49mdv8gg3y1m2ffbw8hf9d")))

(define-public crate-uuid_v1_variant-0.1.3 (c (n "uuid_v1_variant") (v "0.1.3") (d (list (d (n "eui48") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1m4465lc2zvx6df2qjvjipzrab6nprprfm2klhzdspgkb42j6ncp")))

(define-public crate-uuid_v1_variant-0.1.4 (c (n "uuid_v1_variant") (v "0.1.4") (d (list (d (n "eui48") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0nrw381ak9s8hxg9q69wvwqwmzn4bwqnls03wwnyz9qansdvlbn3")))

