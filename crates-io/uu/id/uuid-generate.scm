(define-module (crates-io uu id uuid-generate) #:use-module (crates-io))

(define-public crate-uuid-generate-0.1.0 (c (n "uuid-generate") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "17gsjvj3n4s398vwaghfhps1mbci5v0fv4992jxrffvgf5ylxqjd")))

(define-public crate-uuid-generate-0.1.1 (c (n "uuid-generate") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1d87fvcf229fc5r4qv3hgg6chvs8jz3ibx3x19bb2wxmx1g6klvb")))

(define-public crate-uuid-generate-0.1.2 (c (n "uuid-generate") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lzn7c90y6phlzr9clnzzn95r774496l8phv63cpq4kqrnb8kryj")))

(define-public crate-uuid-generate-0.1.4 (c (n "uuid-generate") (v "0.1.4") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "19z4ia32m03cffn146ip80iqw5nrjjbg00z8zw86xnp4hajypbxv")))

