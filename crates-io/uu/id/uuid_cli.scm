(define-module (crates-io uu id uuid_cli) #:use-module (crates-io))

(define-public crate-uuid_cli-0.1.0 (c (n "uuid_cli") (v "0.1.0") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jgqg6cgx7jlj5bxif7ngz1y44nb5j19k308vm6rqgzy0pz9wqv7")))

(define-public crate-uuid_cli-0.1.1 (c (n "uuid_cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1cfp5qv5752khz9hlya3iv4fq3lckpmxwsyqydhjr079860mywqv")))

