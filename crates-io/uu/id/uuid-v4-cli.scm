(define-module (crates-io uu id uuid-v4-cli) #:use-module (crates-io))

(define-public crate-uuid-v4-cli-0.1.0 (c (n "uuid-v4-cli") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0984r29l48gn98rf52q8l0gkjygz975ifpipk4kp3j0bq9ildkiy")))

(define-public crate-uuid-v4-cli-0.1.1 (c (n "uuid-v4-cli") (v "0.1.1") (d (list (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "08asjsf3qs9bp5pnrhsfgmwf1j7r43nibljkmrap4v09m0zhw551")))

(define-public crate-uuid-v4-cli-0.1.2 (c (n "uuid-v4-cli") (v "0.1.2") (d (list (d (n "cbindgen") (r "^0.20") (d #t) (k 1)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1h061w4hjsxgzsymca2cqvzz1vw6a6qcb02dg9s0f361yvjkqshm")))

(define-public crate-uuid-v4-cli-0.2.0 (c (n "uuid-v4-cli") (v "0.2.0") (d (list (d (n "cbindgen") (r "^0.23") (d #t) (k 1)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^1.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jizb9j82sm3c3frym9mapiar39i7s0my78rrnmlc1mis5ff03rc")))

(define-public crate-uuid-v4-cli-0.2.1 (c (n "uuid-v4-cli") (v "0.2.1") (d (list (d (n "cbindgen") (r "^0.23") (d #t) (k 1)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^1.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1j8ng370aj1jjv4nspczq570psk19nmxv33hamfja3xlili471bs")))

(define-public crate-uuid-v4-cli-0.2.2 (c (n "uuid-v4-cli") (v "0.2.2") (d (list (d (n "cbindgen") (r "^0.24") (d #t) (k 1)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "15q2dnfskg467i0clj7hwhkjwhlcg4bsa0gv4gjw7zviwd3ppcwj")))

(define-public crate-uuid-v4-cli-0.3.0 (c (n "uuid-v4-cli") (v "0.3.0") (d (list (d (n "cbindgen") (r "^0.24") (d #t) (k 1)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1fwiww6ppaz7a4wzgqinb9gnv4gfzcawm8f9zl8i8bi42fbgm8h8")))

