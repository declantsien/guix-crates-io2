(define-module (crates-io uu id uuid-generate2) #:use-module (crates-io))

(define-public crate-uuid-generate2-0.2.0 (c (n "uuid-generate2") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "19h18da1wnwk6g138q38i67qdnwh08np1v5r1hhy9kvdkhj4s5xp")))

