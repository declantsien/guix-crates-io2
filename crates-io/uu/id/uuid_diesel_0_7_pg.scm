(define-module (crates-io uu id uuid_diesel_0_7_pg) #:use-module (crates-io))

(define-public crate-uuid_diesel_0_7_pg-0.1.0 (c (n "uuid_diesel_0_7_pg") (v "0.1.0") (d (list (d (n "diesel") (r "^1.3.3") (f (quote ("uuid" "postgres"))) (d #t) (k 0)) (d (n "diesel_derives") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ajvdsbi8asr6f9f5spvbg5ck86bzazll0hy1q5ck5hm3fg1skr2")))

(define-public crate-uuid_diesel_0_7_pg-0.1.1 (c (n "uuid_diesel_0_7_pg") (v "0.1.1") (d (list (d (n "diesel") (r "^1.3.3") (f (quote ("uuid" "postgres"))) (d #t) (k 0)) (d (n "diesel_derives") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)))) (h "07n5hkqzla2x5k1b1k7b0yb7v8jil1n44zlfr6dgy63nzkvgg6zp")))

(define-public crate-uuid_diesel_0_7_pg-0.1.2 (c (n "uuid_diesel_0_7_pg") (v "0.1.2") (d (list (d (n "diesel") (r "^1.3.3") (f (quote ("uuid" "postgres"))) (d #t) (k 0)) (d (n "diesel_derives") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)))) (h "13m1zswlwavg9b11yz114sc43ikfsyc4b4mjaqzpdjxbrhzwcr7x")))

(define-public crate-uuid_diesel_0_7_pg-0.1.3 (c (n "uuid_diesel_0_7_pg") (v "0.1.3") (d (list (d (n "diesel") (r "^1.3.3") (f (quote ("uuid" "postgres"))) (d #t) (k 0)) (d (n "diesel_derives") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)))) (h "0zyibpf66s9a73nyrw5js6i4hhilw319aywc027hz3fwrk1sccn5")))

