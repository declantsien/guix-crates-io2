(define-module (crates-io uu id uuid1) #:use-module (crates-io))

(define-public crate-uuid1-0.0.1 (c (n "uuid1") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v1" "v4" "use_std"))) (d #t) (k 0)))) (h "1g00qg4s9qavw9p8qgclw8cjpyc8c0ncgwpaf90nyvsq71x35siw") (f (quote (("serde" "uuid/serde"))))))

(define-public crate-uuid1-0.0.2 (c (n "uuid1") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v1" "v4" "std"))) (d #t) (k 0)))) (h "17ji0bxf4kkd6piq1vgyw9i1v46vip2pffxhv2niy0hj6fh16iid") (f (quote (("serde" "uuid/serde"))))))

