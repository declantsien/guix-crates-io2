(define-module (crates-io uu id uuidcell) #:use-module (crates-io))

(define-public crate-uuidcell-0.1.0 (c (n "uuidcell") (v "0.1.0") (d (list (d (n "oorandom") (r "^11.1.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1h6hmfrrlj32n00cz8nrjbcj9chig0lqwdx7an6xsn5lj0jcb9lx") (f (quote (("std" "rand") ("no_std" "oorandom"))))))

