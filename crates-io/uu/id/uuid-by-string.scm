(define-module (crates-io uu id uuid-by-string) #:use-module (crates-io))

(define-public crate-uuid-by-string-1.0.0 (c (n "uuid-by-string") (v "1.0.0") (d (list (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "1nh1v8y0vsxm0gdq7dsxpdspidl5d1gqay8nqjq4fybgsxpqbmcb")))

(define-public crate-uuid-by-string-2.0.0 (c (n "uuid-by-string") (v "2.0.0") (d (list (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "1ryzk12bi7f33sv79n37nqhi5i253ldmn4w1s2hqsx7nrsp06dxg") (f (quote (("namespaces" "default") ("default"))))))

(define-public crate-uuid-by-string-2.0.1 (c (n "uuid-by-string") (v "2.0.1") (d (list (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "05mn4wcaxi7c9sjwpcx963klfqnb6q8210fc1hh89cnxh0m7yl25") (f (quote (("namespaces" "default") ("default"))))))

(define-public crate-uuid-by-string-2.0.2 (c (n "uuid-by-string") (v "2.0.2") (d (list (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "05ml83023m2frivxkvmrg96axcwai7s021wzpvfv6gks2i17lshm") (f (quote (("namespaces" "default") ("default"))))))

(define-public crate-uuid-by-string-2.0.3 (c (n "uuid-by-string") (v "2.0.3") (d (list (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "00iyp360q7q6jwakgd1hhh1l1caqmn99b2mwf7kr2bjp5f1r3sma") (f (quote (("namespaces" "default") ("default"))))))

