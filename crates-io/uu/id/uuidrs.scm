(define-module (crates-io uu id uuidrs) #:use-module (crates-io))

(define-public crate-uuidrs-0.1.0 (c (n "uuidrs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1jca7whdslw1y83sfhz2k6q4jan1ihyaxr3v3fgbmjpycvqn805r")))

