(define-module (crates-io uu id uuid_extend) #:use-module (crates-io))

(define-public crate-UUID_extend-0.1.0 (c (n "UUID_extend") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0n411fn6ym3whb2qg8b3z05817wq0hbd9ww8dk16844fbkksgkd0") (y #t)))

(define-public crate-UUID_extend-0.1.1 (c (n "UUID_extend") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0cp501q3yz0hb7ry2rn3bgbm9740zay0l0wvi46vpydfpwjnw8s1") (y #t)))

(define-public crate-UUID_extend-0.1.2 (c (n "UUID_extend") (v "0.1.2") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jhnd1ghsbwaijsdry1s4chv1hnc0bjw18lp9ycd6ck4za1wvnjc")))

(define-public crate-UUID_extend-0.1.3 (c (n "UUID_extend") (v "0.1.3") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rzrb9k1g3k50hyjy4dymfl657klfvsvax3hc9hw1i1fw1ds7c1x")))

(define-public crate-UUID_extend-0.1.4 (c (n "UUID_extend") (v "0.1.4") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10wjf493ng1swwnbymdsw0wx4w3h053vidzifscg1cq6y8s5fziv")))

(define-public crate-UUID_extend-0.1.5 (c (n "UUID_extend") (v "0.1.5") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15d05kzlkwji0b7chxsl5y324y0s32sfswdidar8vjrywg7ad9hs")))

(define-public crate-UUID_extend-0.1.7 (c (n "UUID_extend") (v "0.1.7") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0c2ma797612lfx7ri6sgg6rzyc0h3dlkvmlfbvplb04mfaksiwwm")))

