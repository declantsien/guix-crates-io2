(define-module (crates-io uu id uuid-readable-rs) #:use-module (crates-io))

(define-public crate-uuid-readable-rs-0.1.0 (c (n "uuid-readable-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0wssxsjnypdb7hypq3s2r4ibn4nzczxmpqvsrm7dq34cvr07c4bq")))

(define-public crate-uuid-readable-rs-0.1.1 (c (n "uuid-readable-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0l28522jpb704xdb7mh3vzab3mljwhzlkjcb6abm7wryx12d6gb3")))

(define-public crate-uuid-readable-rs-0.1.2 (c (n "uuid-readable-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "03jxmcrxsmm8pdkcg92rgq336hgy7n4kdp5gfn4pbv459fpgk6j0")))

(define-public crate-uuid-readable-rs-0.1.3 (c (n "uuid-readable-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1admdgaz68zpq63ab4p65wch9fr1yfg6kxsqjgv48wv4m54is7dr")))

(define-public crate-uuid-readable-rs-0.1.4 (c (n "uuid-readable-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rc5pm4m2kqxlfv6d8c8141d4sk73nimik85s0ggdrncfb0q1nxw")))

(define-public crate-uuid-readable-rs-0.1.5 (c (n "uuid-readable-rs") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nzkmh9mc1gwq72h46a0nz8jdm0vqc0z0jjxxz0dmshfzdf1w3ki")))

