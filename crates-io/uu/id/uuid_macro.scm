(define-module (crates-io uu id uuid_macro) #:use-module (crates-io))

(define-public crate-uuid_macro-0.1.0 (c (n "uuid_macro") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)) (d (n "uuid_macro_impl") (r "^0.1.0") (d #t) (k 0)))) (h "0ssy9kr95z8h774wk2abpqw2hr1b2a17qx78cgf2f1kq800awxj4") (y #t)))

