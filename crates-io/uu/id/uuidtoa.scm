(define-module (crates-io uu id uuidtoa) #:use-module (crates-io))

(define-public crate-uuidtoa-0.1.0 (c (n "uuidtoa") (v "0.1.0") (d (list (d (n "uuid") (r "^0.6") (d #t) (k 0)))) (h "059hjyx9gkjpl8k8a0n3d9iy6rzygs8rpc8cvhhayjl93sp971wy") (f (quote (("default") ("bench"))))))

(define-public crate-uuidtoa-0.1.1 (c (n "uuidtoa") (v "0.1.1") (d (list (d (n "uuid") (r ">= 0.5, <= 0.6") (d #t) (k 0)))) (h "08rsfj58nrk3454hy62lf4ackl0dwz10z0x5877v25v06v4vl7rg") (f (quote (("default") ("bench"))))))

