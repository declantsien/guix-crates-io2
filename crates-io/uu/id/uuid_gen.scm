(define-module (crates-io uu id uuid_gen) #:use-module (crates-io))

(define-public crate-uuid_gen-0.1.0 (c (n "uuid_gen") (v "0.1.0") (d (list (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (k 0)))) (h "08zdpacj0kj73z2bjsrq2ayak5mimk31n64rdac7phmn0367wa3w")))

