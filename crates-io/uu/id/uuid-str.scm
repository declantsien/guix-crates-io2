(define-module (crates-io uu id uuid-str) #:use-module (crates-io))

(define-public crate-uuid-str-0.1.0 (c (n "uuid-str") (v "0.1.0") (d (list (d (n "uuid") (r "^1.5") (f (quote ("v4"))) (d #t) (k 0)))) (h "0m4ibsd48k9km8hqmvasvnj72fk3s5gpmcvc2alrl4z5qsdg8w32")))

