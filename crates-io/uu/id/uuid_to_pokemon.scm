(define-module (crates-io uu id uuid_to_pokemon) #:use-module (crates-io))

(define-public crate-uuid_to_pokemon-0.1.0 (c (n "uuid_to_pokemon") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "060i2a45w0qvw7xnhmc3j241frr8da7srwff5sg26a3afyqdcgwq")))

(define-public crate-uuid_to_pokemon-0.1.1 (c (n "uuid_to_pokemon") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "0vv7af83shmycir2vpgmc4j84r1q5angwnvdlbvn6if6nm1n5wwk")))

(define-public crate-uuid_to_pokemon-0.1.2 (c (n "uuid_to_pokemon") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 0)))) (h "1mxf0kpzvjj272s4hvjrcr18lv9j1yrz6im75ya7gvhgsyb6h0g7")))

(define-public crate-uuid_to_pokemon-0.2.0 (c (n "uuid_to_pokemon") (v "0.2.0") (d (list (d (n "uuid") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 2)))) (h "1fqwnylhmy04yxl5i4a16pv7k0ii2hzmj2pka6rw0j5x57jgljcp")))

(define-public crate-uuid_to_pokemon-0.3.0 (c (n "uuid_to_pokemon") (v "0.3.0") (d (list (d (n "uuid") (r "^0.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)))) (h "0y1day3ypyf2x05kj802bfbmc41pwpg8jwz3bsmlyqy3b7xkm3lz")))

(define-public crate-uuid_to_pokemon-0.3.1 (c (n "uuid_to_pokemon") (v "0.3.1") (d (list (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "0nw9cm5kbq4kcpsfz6hzb4l656kyf0da1xk1nzwhdk8la0zdxllc")))

