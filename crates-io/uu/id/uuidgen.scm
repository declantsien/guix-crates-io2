(define-module (crates-io uu id uuidgen) #:use-module (crates-io))

(define-public crate-uuidgen-0.1.0 (c (n "uuidgen") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 0)))) (h "1z8i0pvrr8nw9rqxr0bdydbblkpkybjsgzcyc6ins7w65b862rm1")))

