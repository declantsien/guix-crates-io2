(define-module (crates-io uu id uuid-tools) #:use-module (crates-io))

(define-public crate-uuid-tools-0.1.0 (c (n "uuid-tools") (v "0.1.0") (d (list (d (n "mac_address") (r "^1.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v1" "v3" "v4" "v5"))) (d #t) (k 0)))) (h "1qas21rag2jillsjlb69krzmp908dajk8ybqnhjrq1lh01akq8qb")))

(define-public crate-uuid-tools-0.1.1 (c (n "uuid-tools") (v "0.1.1") (d (list (d (n "mac_address") (r "^1.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v1" "v3" "v4" "v5"))) (d #t) (k 0)))) (h "16bz396fwz4wp05sxw8sp5jb7l86rra83p8b26wk8ww76clcvk3y")))

(define-public crate-uuid-tools-0.2.0 (c (n "uuid-tools") (v "0.2.0") (d (list (d (n "mac_address") (r "^1.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v1" "v3" "v4" "v5"))) (d #t) (k 0)))) (h "0cv4li4rw85vgxy1s60pnslagp2g3203rx3myk5hkn6hw1p48rfx")))

