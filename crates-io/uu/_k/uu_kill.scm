(define-module (crates-io uu _k uu_kill) #:use-module (crates-io))

(define-public crate-uu_kill-0.0.1 (c (n "uu_kill") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1x1rjj4s21kwx5qvijrzcrlaknxsyia3h079xdzmnnihvfx02697")))

(define-public crate-uu_kill-0.0.2 (c (n "uu_kill") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0s42nr8kzmxnks6swy6c5whi25yfgm9x13c631ywc8b7zjic04g6")))

(define-public crate-uu_kill-0.0.3 (c (n "uu_kill") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0xai62q0vccd9cq6xm2cgy1dxwgmqxwy7vqa3yxzrnqv5jjblid0")))

(define-public crate-uu_kill-0.0.4 (c (n "uu_kill") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "17vl5vrc7spmzwwshbq4j1ixr9wlgl9vwr055g84w7638z9ly9sn")))

(define-public crate-uu_kill-0.0.5 (c (n "uu_kill") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "17810yrwpa70ckac5scya7q6v0bvfxryznidw37wgq0smdd27hvp")))

(define-public crate-uu_kill-0.0.6 (c (n "uu_kill") (v "0.0.6") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "11c33xg9fwzkzdv3fww1a7v28fcys8vj9p7dgazdv0alfq2q5cr7")))

(define-public crate-uu_kill-0.0.7 (c (n "uu_kill") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0s0sza6v95aqazw71w8d0zhdhnrwgrsrd8hkm1x7jjxm1yyiyily")))

(define-public crate-uu_kill-0.0.8 (c (n "uu_kill") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0b47h03b3ax58388lkgkk90j01y24drzr03df11kf9j627nwn8lj")))

(define-public crate-uu_kill-0.0.9 (c (n "uu_kill") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1zy4bbi1vjzpmpdlr63mkfqlygk3m22ck8jhrc1hqlq8kyq3zj3d")))

(define-public crate-uu_kill-0.0.12 (c (n "uu_kill") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0libp9g3b9wha092892n7vlz7rv317769dylh9abn2g7cl4n4bpi")))

(define-public crate-uu_kill-0.0.13 (c (n "uu_kill") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "06k2kqpwfp460l27wkkh2x4wa1fjpy5bbvxjky076bnh59rssvxb")))

(define-public crate-uu_kill-0.0.14 (c (n "uu_kill") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (f (quote ("signal"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "1paz8w8jh4jnay0l1f339b30wrwwizrncpsv0j59ag3rgcah5qiw")))

(define-public crate-uu_kill-0.0.15 (c (n "uu_kill") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("signal"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "11w93934jh5xpxfdvp2illmjpv7ywf09bgf36rr1dvfykhz24q2m")))

(define-public crate-uu_kill-0.0.16 (c (n "uu_kill") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("signal"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "18lz20qsc04djd1bn1bh769kkf51vs72ffaq5cx5slg04psdhff3")))

(define-public crate-uu_kill-0.0.17 (c (n "uu_kill") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("signal"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "13vw4ci65vd5m60vr2dz4dknq08c4fq22bdpms0ym8a05l61inds")))

(define-public crate-uu_kill-0.0.18 (c (n "uu_kill") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("signal"))) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "13mjh8p2220z3q2wbgg8yzadzpgzg05gl6ivxm0yrn32qd758lz7")))

(define-public crate-uu_kill-0.0.19 (c (n "uu_kill") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("signal"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "03n02kfk609pr9fsphkrfkm0ipb2a1zyri36sfn9kp4frcdjmvfm")))

(define-public crate-uu_kill-0.0.20 (c (n "uu_kill") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("signal"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "04k56qdk4p9i9lagnps3jmrgvkfqm7wq9976sbzz486z4ggdkh2z")))

(define-public crate-uu_kill-0.0.21 (c (n "uu_kill") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("signal"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "0kxja2jn05fgbb3998cb5dgnmi6i3dibpjgdcc1mrpd2g4g5ais6")))

(define-public crate-uu_kill-0.0.22 (c (n "uu_kill") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("signal"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "14jbw97zpd2rvz4c74abnc1c9wl7fl7zhczgyx008mm58k2fbj71")))

(define-public crate-uu_kill-0.0.23 (c (n "uu_kill") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("signal"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "18qw127mj56bvjikfx80a8x9zb7s96lb5c8a7d4bs70yqsk9dq4p")))

(define-public crate-uu_kill-0.0.24 (c (n "uu_kill") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("signal"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "0y1ssxi3fljrdid503xv9i7wi0n2mba5rgxk70nq2bwg1y1rrka2")))

(define-public crate-uu_kill-0.0.25 (c (n "uu_kill") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("signal"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "13rha4gj3vv92p40n5r6bi6gqr8v2pjhn6yg976m544sw569s3d1")))

(define-public crate-uu_kill-0.0.26 (c (n "uu_kill") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("signal"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("signals"))) (d #t) (k 0) (p "uucore")))) (h "1vma8jyn970z7c2jdqrway3zki0bvns3ixpr0r7wf5z8r8wsa9xm")))

