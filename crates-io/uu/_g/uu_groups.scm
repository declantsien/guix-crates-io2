(define-module (crates-io uu _g uu_groups) #:use-module (crates-io))

(define-public crate-uu_groups-0.0.1 (c (n "uu_groups") (v "0.0.1") (d (list (d (n "uucore") (r "^0.0.4") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1ms7fysnnvs9f465s7vl8d8ix2x3jvysyljj877jfiwfbvrra98j")))

(define-public crate-uu_groups-0.0.2 (c (n "uu_groups") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "14aasy0261x18580w1dj6hfwdmlw5aynmilpshqnijydlp5n036s")))

(define-public crate-uu_groups-0.0.3 (c (n "uu_groups") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1b2vbrb1dkvf3ggpplha71zlczkc94kr7dk9r27kv96g94p8pjbg")))

(define-public crate-uu_groups-0.0.4 (c (n "uu_groups") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1cla9wcc5x5cddh25m3k2zda20rrivq8iprzirif7clyc05n1igv")))

(define-public crate-uu_groups-0.0.5 (c (n "uu_groups") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0bqb26366hc8d0bgdzc2zyqm5l9samg1lwx4gd47zny5fla82plk")))

(define-public crate-uu_groups-0.0.6 (c (n "uu_groups") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0annagba5cmz7z87x0b5p6jcab9nbvfcad7i5qsgzn04a0bwig6a")))

(define-public crate-uu_groups-0.0.7 (c (n "uu_groups") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0f1ikj7x27qly8lv25iblb8y8ssfxwww7m28jdmj9h5wvciq2mqg")))

(define-public crate-uu_groups-0.0.8 (c (n "uu_groups") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0bzq70xpy99wa37zrxla0776w9q2bv6jcfi3arg43xpgsy3bkmhv")))

(define-public crate-uu_groups-0.0.9 (c (n "uu_groups") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1881nih348mp31fbi8f8hlaiv1pnhpklh296bj884qa7x1h7hlby")))

(define-public crate-uu_groups-0.0.12 (c (n "uu_groups") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0vvl0bznyqw0bnqyks3bv38v2xjbx6brv3lxpnlmvfqq2vq1hlh4")))

(define-public crate-uu_groups-0.0.13 (c (n "uu_groups") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "0la95s3400cfnyd1f4fjg66d0m444gg4z9dncdnsx3ffn6hiq0hx")))

(define-public crate-uu_groups-0.0.14 (c (n "uu_groups") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "0dxas6kcp1vzcff1m0jvkvidyhv7h5fimx7cvv9hkbmfm4qnq3q8")))

(define-public crate-uu_groups-0.0.15 (c (n "uu_groups") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "0622pps1anarvrizjalqxgivkc868c64aypnail4h65q656hs272")))

(define-public crate-uu_groups-0.0.16 (c (n "uu_groups") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "13zpr4c7j2sw8k75iafypwv0di28xljrg25ig9nndj8z15zzkbh3")))

(define-public crate-uu_groups-0.0.17 (c (n "uu_groups") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "1acl7bxfxwyyrmng5gxzrdb9s5zjn2lalqp5szjvrv71857np62n")))

(define-public crate-uu_groups-0.0.18 (c (n "uu_groups") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "1y7bx5xszr3kw1dq6csqkv1cx8dskvh3pnbs5a66nn3g851h0wdq")))

(define-public crate-uu_groups-0.0.19 (c (n "uu_groups") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "1hljmw1zwvkncq363dj20q0hm807zz2hzk6lqcn3wigy89lfgk18")))

(define-public crate-uu_groups-0.0.20 (c (n "uu_groups") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "07p51i2l9nx0yj47xz44zhw0x81p4jn7pvf7pb2r211kq5lbj7k0")))

(define-public crate-uu_groups-0.0.21 (c (n "uu_groups") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "1qi6shkhdjzi09i0ai55lcvamfda6fzgai2r3sl9nfndy6mxkg11")))

(define-public crate-uu_groups-0.0.22 (c (n "uu_groups") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "0h09y232fwani974wsj9yplv06f895s0pbvrn8m12m4nglvv0fvs")))

(define-public crate-uu_groups-0.0.23 (c (n "uu_groups") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "0bfp6w5bdj1fgk3i9j4qqli6594f0qylnl3picrdkibdigzqvxi2")))

(define-public crate-uu_groups-0.0.24 (c (n "uu_groups") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "1lkzc3qgf8qk07mi5q6wkanhnaszsr8gv01cr8yjdyrpm7cfy04i")))

(define-public crate-uu_groups-0.0.25 (c (n "uu_groups") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "0ic6pkxz3d4ikmp1gx34nn500pskrraci9mhk9x3a7qhraygxrcd")))

(define-public crate-uu_groups-0.0.26 (c (n "uu_groups") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "process"))) (d #t) (k 0) (p "uucore")))) (h "10sq8qhksplm48l6zxk3hdkb5fggbm6sd1nixd8j0iz80dr603j7")))

