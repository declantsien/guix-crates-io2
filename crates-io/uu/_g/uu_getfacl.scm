(define-module (crates-io uu _g uu_getfacl) #:use-module (crates-io))

(define-public crate-uu_getfacl-0.0.1 (c (n "uu_getfacl") (v "0.0.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)) (d (n "uucore") (r "^0.0.24") (d #t) (k 0)) (d (n "xattr") (r "^1.3.1") (d #t) (k 0)))) (h "19dw57lq9db6jhijpiyhli4fw6id4qjl2m7bkmj51bh3qfjcd1wh") (y #t)))

