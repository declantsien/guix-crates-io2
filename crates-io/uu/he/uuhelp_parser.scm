(define-module (crates-io uu he uuhelp_parser) #:use-module (crates-io))

(define-public crate-uuhelp_parser-0.0.19 (c (n "uuhelp_parser") (v "0.0.19") (h "0cs421r41si4ff4pacrqx7r6qh9xfy26mj5icm7l2jz1jg8jag7h")))

(define-public crate-uuhelp_parser-0.0.20 (c (n "uuhelp_parser") (v "0.0.20") (h "1s98x2x6yz2c7ksywr5djqjiswy8wzikzlxyaa4yw7ll564sk10x")))

(define-public crate-uuhelp_parser-0.0.21 (c (n "uuhelp_parser") (v "0.0.21") (h "16c1y9bq74gkmh0xkdmkjg40c2ggh7r38849j9jqkbgh7ic3bnrp")))

(define-public crate-uuhelp_parser-0.0.22 (c (n "uuhelp_parser") (v "0.0.22") (h "19ji4019k9pfw0hjdznxx522d3k4zvldrqk3pfxd6lkdwcqigrkd")))

(define-public crate-uuhelp_parser-0.0.23 (c (n "uuhelp_parser") (v "0.0.23") (h "1w5gfljnxp3hlznxc25brhrr7nn6akvhxz0n9kxn3mg7s1fhcsg2")))

(define-public crate-uuhelp_parser-0.0.24 (c (n "uuhelp_parser") (v "0.0.24") (h "0yqla6xynawzi1j9m639rc3y9m15k45xdkcnb2k5q218h10ghhfq")))

(define-public crate-uuhelp_parser-0.0.25 (c (n "uuhelp_parser") (v "0.0.25") (h "0gsfy0dkwzlyyq4abwf0c7gks7m1qm0yzl58sl265n5dllr6hymc")))

(define-public crate-uuhelp_parser-0.0.26 (c (n "uuhelp_parser") (v "0.0.26") (h "0q2vjwlc1wrkh4h9zvhag6v7h7vvqcbmhi4w1iicfnqlnz3j6nj2")))

