(define-module (crates-io uu _c uu_chroot) #:use-module (crates-io))

(define-public crate-uu_chroot-0.0.1 (c (n "uu_chroot") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "05g4j4mjih44cp11mwkgyyghavgbw6ynwl5icclarxxbgi8w0mb8")))

(define-public crate-uu_chroot-0.0.2 (c (n "uu_chroot") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "164kyajhrrh9cih1r73qpam0q7i6gp513706kj07d0h0p9lkpxs1")))

(define-public crate-uu_chroot-0.0.3 (c (n "uu_chroot") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "12djzzflycp3i6qspn95pwawqs97m749gwgb7xf0iq2c97v8zxig")))

(define-public crate-uu_chroot-0.0.4 (c (n "uu_chroot") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1fil6k5yvw8mvbbs2vfvv134dxbly3ydl18v4xw97p6n3wdgc294")))

(define-public crate-uu_chroot-0.0.5 (c (n "uu_chroot") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1jxjj2snwnn3cphj2qy4a2bdwdh321xm86f2xrrvi5c04yx2awkr")))

(define-public crate-uu_chroot-0.0.6 (c (n "uu_chroot") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1y7y0yqpa7qbqgxfr1cbpl7hnq9f7c077429w3ajjgxr5547a8s9")))

(define-public crate-uu_chroot-0.0.7 (c (n "uu_chroot") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1hr3pizh79wbhqgk4myja4q1sbjqz0sc0hk3njvmvsi1716axm7x")))

(define-public crate-uu_chroot-0.0.8 (c (n "uu_chroot") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "13zf64acn66n1hdxvskyssnj86r3y0pv5pjyqnp6drv8m3a7qj9g")))

(define-public crate-uu_chroot-0.0.9 (c (n "uu_chroot") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1f3lhfjivkw4d2sks7i9qljmnwb9kl81m9g2xqmcc4q7k0d30gf2")))

(define-public crate-uu_chroot-0.0.12 (c (n "uu_chroot") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1kgglqhc4j7aq84ixhhan09rn1d154a8j9idfy3p9f2q29lcpb9r")))

(define-public crate-uu_chroot-0.0.13 (c (n "uu_chroot") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")))) (h "1337zx33ibh0lhd7vbg5yxmkgqz32fckf0g1y0bd41y8avyzdxxp")))

(define-public crate-uu_chroot-0.0.14 (c (n "uu_chroot") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")))) (h "09ljv16wzhglqk03a32ccxnq7d9byi1mcd9sznbfh01dbzy6iqp5")))

(define-public crate-uu_chroot-0.0.15 (c (n "uu_chroot") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("entries"))) (d #t) (k 0) (p "uucore")))) (h "01xc3lidhym8mhwx8g8zirlad51s247jw7kbwx8pj3v6a8kqbrql")))

(define-public crate-uu_chroot-0.0.16 (c (n "uu_chroot") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1mf7cxfgwa3imgx4kb2vdbdd5yjkva2hhml8pvhhpgczbxz1wpfy")))

(define-public crate-uu_chroot-0.0.17 (c (n "uu_chroot") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "0b0nz02gvppxzwwprz164pn9d42c5qd4y48yrls63sji9q0qx9ha")))

(define-public crate-uu_chroot-0.0.18 (c (n "uu_chroot") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "0mzm79hxa2af0g4bx6b1gh9bzbg438qa8hnwgrg3mwh49bhfpqn2")))

(define-public crate-uu_chroot-0.0.19 (c (n "uu_chroot") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1l9l2injhwmivzvi93kxmp62mx3r9zj46wrqn8x5h082wvkjrhdg")))

(define-public crate-uu_chroot-0.0.20 (c (n "uu_chroot") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "0jhf71l4f9cxndz1jpy5li64w70cl6ipznr6xrv2bl09c26k2fqf")))

(define-public crate-uu_chroot-0.0.21 (c (n "uu_chroot") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1kynka882wcyrs44jsjy3am528lp4gak3x8a51rv1pjrr7a01bzj")))

(define-public crate-uu_chroot-0.0.22 (c (n "uu_chroot") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "03hy7f8ncj43phglq3dhykv4hyghzfv148jk3q96kd6kdihqghfc")))

(define-public crate-uu_chroot-0.0.23 (c (n "uu_chroot") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "08f971dyw7jdlmgal7c601fg0qg08d2khdgriq94z1acn7rmry4v")))

(define-public crate-uu_chroot-0.0.24 (c (n "uu_chroot") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1v5s6xvla1zkydx1w86hh6bz75fza10jf5xp9vq4zarkyg6bhjsz")))

(define-public crate-uu_chroot-0.0.25 (c (n "uu_chroot") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1gvwi5q0yk6jj0qn6lix1igpzliw8r5n14g13n1zvrdiw1cf7jik")))

(define-public crate-uu_chroot-0.0.26 (c (n "uu_chroot") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")))) (h "1g91h3qkfljfipdp370bp9m0zwzkzcg4s895iw9skwy0shzkmyr1")))

