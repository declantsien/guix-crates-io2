(define-module (crates-io uu _c uu_comm) #:use-module (crates-io))

(define-public crate-uu_comm-0.0.1 (c (n "uu_comm") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0xskhhhjw7z2fdyfhh1rfavvhza12m5gm31m67qb3znx5qswz1gj")))

(define-public crate-uu_comm-0.0.2 (c (n "uu_comm") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1yb1pnfi5j0j74gza9w759hag6ihwbhvs6q30hracc1dddgb6h99")))

(define-public crate-uu_comm-0.0.3 (c (n "uu_comm") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0xncgcmyzi8d90f6hjl301xbwsxlxhx9nrlqi9zs9mjppl58yah3")))

(define-public crate-uu_comm-0.0.4 (c (n "uu_comm") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0l47i3kp9gjxymk547x324k6vm6s20d28v21fqz3nydm2k95h594")))

(define-public crate-uu_comm-0.0.5 (c (n "uu_comm") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "18y6kd8lycj7q29321cwb4j0nyhn0jbadaihbbd5bdbgxxm45k4j")))

(define-public crate-uu_comm-0.0.6 (c (n "uu_comm") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1xg1rbk96xcrqikjgia7czn2fsnipbijblfakdh0zjbbigkdbc0j")))

(define-public crate-uu_comm-0.0.7 (c (n "uu_comm") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1gj4i9lw200j67mz2rvhzwsgcfslxfcsgahhlwslxac5ls5wd0a7")))

(define-public crate-uu_comm-0.0.8 (c (n "uu_comm") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0lcd94hsciw336g396ssw7x44a87lz7x8z803sppvmnpkarrwqyy")))

(define-public crate-uu_comm-0.0.9 (c (n "uu_comm") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "026dsmcxp8kam27yh55rjr3acfgc90q3bxzfq6h8b3v7divvw83g")))

(define-public crate-uu_comm-0.0.12 (c (n "uu_comm") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "16ifqazjbgrs40mlf28wswhc7vcggsv0maa1k9s96dnyf1bzj4zd")))

(define-public crate-uu_comm-0.0.13 (c (n "uu_comm") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0lrw7yk8bn7hpcdsx9ld9i1xljz5g6m58rj4fbxzkz9xn0ds9y0r")))

(define-public crate-uu_comm-0.0.14 (c (n "uu_comm") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "17sqqn7kmx4835bmsx09dlv617h4pbmb2gqnkxx2w75hg5ibrr8k")))

(define-public crate-uu_comm-0.0.15 (c (n "uu_comm") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1y1qkwr99d0c0i9yncf0k8j8j3lv48qy91zm44j5xrccpn9237jr")))

(define-public crate-uu_comm-0.0.16 (c (n "uu_comm") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "0g9rcl2247nqcnjlhlkm79nhjsl7sqvbwf47dmahd06jgbdlyggx")))

(define-public crate-uu_comm-0.0.17 (c (n "uu_comm") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1kviy563si3a7lakqf5ynq0688196wzz7j2984ryk4jp2nx8wydj")))

(define-public crate-uu_comm-0.0.18 (c (n "uu_comm") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1kzf9003y9wmdg4snhs6gcg5qbxv8wpy963bv3fwrppbh9lgqxxb")))

(define-public crate-uu_comm-0.0.19 (c (n "uu_comm") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1mh0386b1yjkl430lkl7jwv9n5g2qjy013nsrbx2vx4kndyj3wkk")))

(define-public crate-uu_comm-0.0.20 (c (n "uu_comm") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0czxc45nhalipg034ia1vmvj5gbd3vaayn298b2abw1503mmyp74")))

(define-public crate-uu_comm-0.0.21 (c (n "uu_comm") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0yrqrfvgv5psfy488ng04h1za6aw2jkq23p54f27ivm878hv2ihs")))

(define-public crate-uu_comm-0.0.22 (c (n "uu_comm") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "17pa9ixbdkylbvr476ch3bzfhi7grlcinzxynfya4zfy27wqibav")))

(define-public crate-uu_comm-0.0.23 (c (n "uu_comm") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1r4bzbfh7w4ycsvvsndh3fmcn2v08b60zlhgqjn8fhbblabpjy94")))

(define-public crate-uu_comm-0.0.24 (c (n "uu_comm") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1ww89jkhjfcs7wr680pvqwazd9c4li4l2hxn4xxzlryasmfng848")))

(define-public crate-uu_comm-0.0.25 (c (n "uu_comm") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1syk4xkilcwxixavf8ddyz5zcs73kdyb5s4bhxva22p521ibsw89")))

(define-public crate-uu_comm-0.0.26 (c (n "uu_comm") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0y438xk9d5ai3lq44a6z050bsi5lg363zhqj3gnzpv4bh9nb77x9")))

