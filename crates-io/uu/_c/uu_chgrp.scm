(define-module (crates-io uu _c uu_chgrp) #:use-module (crates-io))

(define-public crate-uu_chgrp-0.0.1 (c (n "uu_chgrp") (v "0.0.1") (d (list (d (n "uucore") (r "^0.0.4") (f (quote ("entries" "fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "15rrhvw3yyxpprs2108va5h1aipac5kwzr2a7lh6ps9v24233i1i")))

(define-public crate-uu_chgrp-0.0.2 (c (n "uu_chgrp") (v "0.0.2") (d (list (d (n "uucore") (r ">=0.0.5") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0rm3kji60yc5al0z8jzm1p9n5zz3drikcvygyvnlaw6dhw56d6fc")))

(define-public crate-uu_chgrp-0.0.3 (c (n "uu_chgrp") (v "0.0.3") (d (list (d (n "uucore") (r ">=0.0.6") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0wswddsv6525ndaxhbb7flsdy3715w62ss8drsz9gcyr6ispm778")))

(define-public crate-uu_chgrp-0.0.4 (c (n "uu_chgrp") (v "0.0.4") (d (list (d (n "uucore") (r ">=0.0.7") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1q2l46z40k9bdjfdipdf7qfk4kx451mcaildz14nf8glngiw7lh2")))

(define-public crate-uu_chgrp-0.0.5 (c (n "uu_chgrp") (v "0.0.5") (d (list (d (n "uucore") (r ">=0.0.8") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1n103zy4a1cm2cfjxairab3m76k9jmqbx044hsv7y1dhdi7kn8d6")))

(define-public crate-uu_chgrp-0.0.6 (c (n "uu_chgrp") (v "0.0.6") (d (list (d (n "uucore") (r ">=0.0.8") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0040vcn11g6n5id1j4ii7p96isyqpjcnq9bc3cz3iwsm5a4fsb12")))

(define-public crate-uu_chgrp-0.0.7 (c (n "uu_chgrp") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1xya9697dvmcqvn8x2as61nfni4wawa3vmcmhpc1q8h8cfma2a3r")))

(define-public crate-uu_chgrp-0.0.8 (c (n "uu_chgrp") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0wd805infrgqzhf6b18ypxjxy0v0rrkgr08dp5mi85ybjah3a09g")))

(define-public crate-uu_chgrp-0.0.9 (c (n "uu_chgrp") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "12sazal7gswd308q7izb6pl31jvjkp1vccm42b8cjr9dbgazh7z8")))

(define-public crate-uu_chgrp-0.0.12 (c (n "uu_chgrp") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "05j4w1jbxq8s07szrp4vwis93hnrg7zrnci51v4b44wx0f2gnyjl")))

(define-public crate-uu_chgrp-0.0.13 (c (n "uu_chgrp") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "1i124msg83ab3qhvpl8fs4axs3clcc2q12jpdsmpy9j9fwzz0w3r")))

(define-public crate-uu_chgrp-0.0.14 (c (n "uu_chgrp") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "03v09l2z2z6ahmkgiy7b8q6kij63p08dzsmfhp9igj9fczarwjvf")))

(define-public crate-uu_chgrp-0.0.15 (c (n "uu_chgrp") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "1v1p0na6x03w9hrv4lwx584iq9r0zcsfpzn84nxpw1vwnjjz7r8i")))

(define-public crate-uu_chgrp-0.0.16 (c (n "uu_chgrp") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "0vi385ic7xaawq89dwla39f6q972lrzb76s4i96v7n0hxk67mivb")))

(define-public crate-uu_chgrp-0.0.17 (c (n "uu_chgrp") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "1iznwdkvpff4krdmkq0bi9g4v892wkbmnbh6vvys8ph7qyhi40yp")))

(define-public crate-uu_chgrp-0.0.18 (c (n "uu_chgrp") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "0pa4s4aw8nmyv3j3ns3999w7davdf311ghfxrzk9brgq7brpwwhc")))

(define-public crate-uu_chgrp-0.0.19 (c (n "uu_chgrp") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "0y5163jjp2yy8hm5vl5dy46h3iv4rh5f63jp677gwyc5ksnfh2yj")))

(define-public crate-uu_chgrp-0.0.20 (c (n "uu_chgrp") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "1wn03hwp2xxxgmh18h4znnaribkm3bafqh2wws9q2bpdx7qdig8a")))

(define-public crate-uu_chgrp-0.0.21 (c (n "uu_chgrp") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "1a4kxrkhnw7hj3nhpkhgj5qm17xs7y9br3rsw0j8hid6z71czjza")))

(define-public crate-uu_chgrp-0.0.22 (c (n "uu_chgrp") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "1dhwnx5r7pmgklr5j9gah6kx68ys61s189n578pq24sd2717xq9i")))

(define-public crate-uu_chgrp-0.0.23 (c (n "uu_chgrp") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "05vmkbbc8dx1pj9y32zpahx59g9bbislss95jfbl749qm50vvcav")))

(define-public crate-uu_chgrp-0.0.24 (c (n "uu_chgrp") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "1996kr9namrrh1qh6wv408pgpbhz6wmzd5r6fl4q76nhndiyqzl8")))

(define-public crate-uu_chgrp-0.0.25 (c (n "uu_chgrp") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "0pr3dfkjqj92izsjws05f7lvqzwp9vz9yms33hzbla8rfhgc8b0l")))

(define-public crate-uu_chgrp-0.0.26 (c (n "uu_chgrp") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("entries" "fs" "perms"))) (d #t) (k 0) (p "uucore")))) (h "0c3krfc7s7blnvm4pdmfns93ivh0j5fcr392sy071ri52lx27nqq")))

