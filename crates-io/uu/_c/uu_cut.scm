(define-module (crates-io uu _c uu_cut) #:use-module (crates-io))

(define-public crate-uu_cut-0.0.1 (c (n "uu_cut") (v "0.0.1") (d (list (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0dd2x6j8j2rz8012n3yj8ib2j8s8jwdh9mfvgz0l26yy7hrlgg5g")))

(define-public crate-uu_cut-0.0.2 (c (n "uu_cut") (v "0.0.2") (d (list (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0vdi26yrnp3ykmf3mrkibv98814sv12kbslkm4764vffsxqk9n14")))

(define-public crate-uu_cut-0.0.3 (c (n "uu_cut") (v "0.0.3") (d (list (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1k90ygylq89kscf12517khhxq608cnbiv6ic5449vvz4z943l53z")))

(define-public crate-uu_cut-0.0.4 (c (n "uu_cut") (v "0.0.4") (d (list (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "01jjmk9r0qzqzrhqffmmjylgdci5n175ryvbhq813yby7prif8pj")))

(define-public crate-uu_cut-0.0.5 (c (n "uu_cut") (v "0.0.5") (d (list (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1i2i62yfw3v619skysw45hn1zfpll8g49a1bkfma90sysj09h3vx")))

(define-public crate-uu_cut-0.0.6 (c (n "uu_cut") (v "0.0.6") (d (list (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1l15lld8fi5zja1j7cpcrp9mkyawp076j06y29dnb4vwx4f4zhd9")))

(define-public crate-uu_cut-0.0.7 (c (n "uu_cut") (v "0.0.7") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0drqlx70csdx2hid25dpa4dcca4651v4mfw2fqk5mcq0phapf69r")))

(define-public crate-uu_cut-0.0.8 (c (n "uu_cut") (v "0.0.8") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1nqs577cjj82jm7f87mv37r3lsjhk2kzgxfq6w2y98dakk3mfjvd")))

(define-public crate-uu_cut-0.0.9 (c (n "uu_cut") (v "0.0.9") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0x6b2y6d3zks7s75cfqml5dfq7l21b9zc5bdscvxwxvcwg5fkmf6")))

(define-public crate-uu_cut-0.0.12 (c (n "uu_cut") (v "0.0.12") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0vgz1cgfyl8zi8y5hpykl8sqmm6q6nkl07cgnn34q9s6vazvb5qr")))

(define-public crate-uu_cut-0.0.13 (c (n "uu_cut") (v "0.0.13") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1663mvbyd5l3n2sfmmb1a2n8b5f269j6lmlksqwgkl982ziv5gzk")))

(define-public crate-uu_cut-0.0.14 (c (n "uu_cut") (v "0.0.14") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "05p00rf125w5bziwwdacjpgqlcx7z68kdg4fh735hc4q3zlwa2ny")))

(define-public crate-uu_cut-0.0.15 (c (n "uu_cut") (v "0.0.15") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1m2w3xwayxm1d6sl6aad3cdyr2h31j42p94a9a9ja5xmkirf0kxg")))

(define-public crate-uu_cut-0.0.16 (c (n "uu_cut") (v "0.0.16") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1syrx2nzr0yh8f9r48925qk45i8mzap1zdg0hyk73g0zrg3jbzx8")))

(define-public crate-uu_cut-0.0.17 (c (n "uu_cut") (v "0.0.17") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bstr") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1aybpp0jj3n88fp0hpfpqaad8hd31rm5pvyhqs895rgys3x8a5jz")))

(define-public crate-uu_cut-0.0.18 (c (n "uu_cut") (v "0.0.18") (d (list (d (n "bstr") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.6") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "169pmyfaasdxv3nfwkkbwb8ch2n0mhwxqcavll5ikfdr3x064qc6")))

(define-public crate-uu_cut-0.0.19 (c (n "uu_cut") (v "0.0.19") (d (list (d (n "bstr") (r "^1.5") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.7") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1hrqckwn9mhd152asmvmzwlgpp7rbivx938qjssfvh05zydr2yly")))

(define-public crate-uu_cut-0.0.20 (c (n "uu_cut") (v "0.0.20") (d (list (d (n "bstr") (r "^1.6") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.7") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "14lzva6qyj2hrdsn1iy0m9nzl1q5d6x8rkby1pkyyi4jpr2fmy03")))

(define-public crate-uu_cut-0.0.21 (c (n "uu_cut") (v "0.0.21") (d (list (d (n "bstr") (r "^1.6") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.9") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "1haj250d2mwmnzcrsc020fzajgz2j7lzlvngxgn8yknlmy1s0vcz")))

(define-public crate-uu_cut-0.0.22 (c (n "uu_cut") (v "0.0.22") (d (list (d (n "bstr") (r "^1.7") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "0khb5qmdb0c7qsg1igdb5pajjn0y8sanlb8k8z4jx3ch8v7rx492")))

(define-public crate-uu_cut-0.0.23 (c (n "uu_cut") (v "0.0.23") (d (list (d (n "bstr") (r "^1.8") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "1i3p99qwpq7y10sfz2wfikkc84q7528hyf0swiaa11c1cb3diilb")))

(define-public crate-uu_cut-0.0.24 (c (n "uu_cut") (v "0.0.24") (d (list (d (n "bstr") (r "^1.9") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "0vlsfp78y10ds9k8ynqx0w97729a7ijzlmr00k9mk74v9vbfc2pc")))

(define-public crate-uu_cut-0.0.25 (c (n "uu_cut") (v "0.0.25") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "1sizi1kgfa61mksp490fmqnp1c9nwm10b3zhlrsdsw1idhi5n1a6")))

(define-public crate-uu_cut-0.0.26 (c (n "uu_cut") (v "0.0.26") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("ranges"))) (d #t) (k 0) (p "uucore")))) (h "1ci1irfk846fcvv8pmzj7cwjwb4v1gxckijv56rw8r3k3j4ylflz")))

