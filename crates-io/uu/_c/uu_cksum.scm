(define-module (crates-io uu _c uu_cksum) #:use-module (crates-io))

(define-public crate-uu_cksum-0.0.1 (c (n "uu_cksum") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0g1cy53hq0hdr5hjf983m45vfvlclh1y4dqhc51l2qqdw2dpdm94")))

(define-public crate-uu_cksum-0.0.2 (c (n "uu_cksum") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1s9kjpl7a1fhp26khvxz593mb3ma3d8k0nv8nikclrn1h693453q")))

(define-public crate-uu_cksum-0.0.3 (c (n "uu_cksum") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1pxap252byzn9h9phx1fjqhf1bsckya9bh837zc3vk1z23ds8n0y")))

(define-public crate-uu_cksum-0.0.4 (c (n "uu_cksum") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0v6a9ja0k2yiiqzl9ydw3v8sd7hfzqgh6hyprlpc5cp165m84p3d")))

(define-public crate-uu_cksum-0.0.5 (c (n "uu_cksum") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "04awghg670mhrkkl5j5sgl9nf3m5a0685jb8c8mbd2bbxmlh01lc")))

(define-public crate-uu_cksum-0.0.6 (c (n "uu_cksum") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0fxn1d5w5iwsfm3mk7qs7dxa7hdxmx9gqmnwwksvridplir1fy63")))

(define-public crate-uu_cksum-0.0.7 (c (n "uu_cksum") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1jh0rp3i5dwxjkzys3z622qyi1ghbn203li3rda2yjilm0brja4f")))

(define-public crate-uu_cksum-0.0.8 (c (n "uu_cksum") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1avw2dqhx3rqcswwx3cz9pi9c3cirr0ph0i64sl6k5mgxnbdplsj")))

(define-public crate-uu_cksum-0.0.9 (c (n "uu_cksum") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1f4vchs2396ipw2pqsd46nsllvy676v5xy4wydlvbzhd2rrmgwdf")))

(define-public crate-uu_cksum-0.0.12 (c (n "uu_cksum") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "10vabph7hk1qgppzwnza2va6i7hbj9ch634lwqcf44m09zdw9bgq")))

(define-public crate-uu_cksum-0.0.13 (c (n "uu_cksum") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1dvwwmv93x45pi8axgg6686a9f93sv07w9dvx7cbabfjkwn33y7q")))

(define-public crate-uu_cksum-0.0.14 (c (n "uu_cksum") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0c819xb0h9zfwx2ykn8rk2p715p5c75mb9h5vk9pdz16v2isypv2")))

(define-public crate-uu_cksum-0.0.15 (c (n "uu_cksum") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "003v6qlaxa5hjbkxw88f27f6578cby8i6bv4cmjfbrdm14h9id41")))

(define-public crate-uu_cksum-0.0.16 (c (n "uu_cksum") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "0366nazr2n7lr3cps3yjplp0rwdrsaglm4c5zggqggxsbdw03rbn")))

(define-public crate-uu_cksum-0.0.17 (c (n "uu_cksum") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "0c2r3k3flrhp41fwvspnjckcqgl13x3hr6gfrz4plpr62f78wqqq")))

(define-public crate-uu_cksum-0.0.18 (c (n "uu_cksum") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("sum"))) (d #t) (k 0) (p "uucore")))) (h "04dnkpz099fv4n912z9w3v6bvq8xy6b3abqfv5jz90hjsp7pbd9z")))

(define-public crate-uu_cksum-0.0.19 (c (n "uu_cksum") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("sum"))) (d #t) (k 0) (p "uucore")))) (h "0margrzlly3dcgkmx5iairqcn0rk3fslyjqwjhhgb45i2abxhnrs")))

(define-public crate-uu_cksum-0.0.20 (c (n "uu_cksum") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("sum"))) (d #t) (k 0) (p "uucore")))) (h "133ffhlssny9a2asxl7wbj3hfyh88nislxflirc7ws39h90n1vp1")))

(define-public crate-uu_cksum-0.0.21 (c (n "uu_cksum") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("sum"))) (d #t) (k 0) (p "uucore")))) (h "12wrh8m33277jpqvmyif5yndp9akgy4rf49y5iwzmi45dx6rxi8j")))

(define-public crate-uu_cksum-0.0.22 (c (n "uu_cksum") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("sum"))) (d #t) (k 0) (p "uucore")))) (h "03qqh6l7gf164lpaxc9rh3ipwlnqbz4b1111q8b71r4b27g8jyg2")))

(define-public crate-uu_cksum-0.0.23 (c (n "uu_cksum") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("sum"))) (d #t) (k 0) (p "uucore")))) (h "1fmvpqq6r2y11h4d7gg2a60qdck6m4nsxcjkgw5mqmn7k0c4irxi")))

(define-public crate-uu_cksum-0.0.24 (c (n "uu_cksum") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("sum"))) (d #t) (k 0) (p "uucore")))) (h "1lvq6dlqyyjj9pa8zlxwsiismmrd6pigvcvl9zlv1pqvas6mdq3s")))

(define-public crate-uu_cksum-0.0.25 (c (n "uu_cksum") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding" "sum"))) (d #t) (k 0) (p "uucore")))) (h "1pf705s75v38gg0asxmkh9cw721ziimyd3vg18x4a0y66rx5yw2y")))

(define-public crate-uu_cksum-0.0.26 (c (n "uu_cksum") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("encoding" "sum"))) (d #t) (k 0) (p "uucore")))) (h "0c3rsvqmgisp5nvbxndvqiws30ar45v58kpci58skpx2dsn96r3n")))

