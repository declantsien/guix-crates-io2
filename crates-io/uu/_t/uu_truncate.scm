(define-module (crates-io uu _t uu_truncate) #:use-module (crates-io))

(define-public crate-uu_truncate-0.0.1 (c (n "uu_truncate") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1rq43dfcj5wydlih8vka1jvmixs1is448wwvk5j988hsmpcyhq2m")))

(define-public crate-uu_truncate-0.0.2 (c (n "uu_truncate") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0dwr31djj7dlnav12hllycsg7vjlxdgr0xk2wf97q0z1rq9im0d7")))

(define-public crate-uu_truncate-0.0.3 (c (n "uu_truncate") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1vb8dnd2l356i43whjrfx38m761i85dqb9imdlm7h1axljjj0fi7")))

(define-public crate-uu_truncate-0.0.4 (c (n "uu_truncate") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1iw5blg3j6z50xnsvbbqy0bhjkd3swchpx5fgianwaf2vv44dfzk")))

(define-public crate-uu_truncate-0.0.5 (c (n "uu_truncate") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0lhlk59bwsv17flkff5fa184jiyhcwixr969vdffnjc4vl75jzhj")))

(define-public crate-uu_truncate-0.0.6 (c (n "uu_truncate") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "18r6w3aaa2fwz78a62ks2d0p837qjn3811jymqh5jbw5cp03z2fn")))

(define-public crate-uu_truncate-0.0.7 (c (n "uu_truncate") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "15qr0yb628pv4a3arwp6s1vvyad8l7lg9c71872iqgdmk5zn5bbw")))

(define-public crate-uu_truncate-0.0.8 (c (n "uu_truncate") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0w6hhim8hcm9c2zlhvvmabxn3ibvxy0a0mzwk4zcday8kng7x5ds")))

(define-public crate-uu_truncate-0.0.9 (c (n "uu_truncate") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "103b1wdzzqhk96p2pij8gzzvmnb02c00dih3avspjjl8vr86r7ih")))

(define-public crate-uu_truncate-0.0.12 (c (n "uu_truncate") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "177c7mv0xc3vkchli09pixvm52grv3fmqh6ja2pmb581mwy9888x")))

(define-public crate-uu_truncate-0.0.13 (c (n "uu_truncate") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1abqig61fg6f59d130jvrw0c7rd7m35c37bnrxc3rz56fki5nfhk")))

(define-public crate-uu_truncate-0.0.14 (c (n "uu_truncate") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0bgrk38kq3ghfgmq55frqn7bm762n23vq008g4y447fain16c06w")))

(define-public crate-uu_truncate-0.0.15 (c (n "uu_truncate") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1p8jpyqylifa1wlyfjd1s11iapgy5dgxy4ad7nyahd4skskhh647")))

(define-public crate-uu_truncate-0.0.16 (c (n "uu_truncate") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1l29dsnws7x9723kpp538amvrln4lbkxnpz7y6cjr8mxbndw3r7q")))

(define-public crate-uu_truncate-0.0.17 (c (n "uu_truncate") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1745rjp2wd1vnj9cicpi9v20085f7ql8cggfni7i36jgadg89nrc")))

(define-public crate-uu_truncate-0.0.18 (c (n "uu_truncate") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0lpc4x81al43vznzszrycb79lala6x11lcq6nq3vrbbb0kr9v87g")))

(define-public crate-uu_truncate-0.0.19 (c (n "uu_truncate") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1pl9hxyx2lzlrprcfxl7nbsvsjpf0nkhkadnp9syjvx3xr1sm8y3")))

(define-public crate-uu_truncate-0.0.20 (c (n "uu_truncate") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0rby0f2fngk8pf4mrrwhqpwpyk3agjbz6ka4kdnfcwg9p0zpgrxb")))

(define-public crate-uu_truncate-0.0.21 (c (n "uu_truncate") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "08606cx3azpb648wfr0fyq699yyr0r3cmqvdvsj28vy70qfgxdw6")))

(define-public crate-uu_truncate-0.0.22 (c (n "uu_truncate") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "03hyqff6mbz9ymfyfmwzqzyb5494mljkqxqhrdkzds209l3a0c0f")))

(define-public crate-uu_truncate-0.0.23 (c (n "uu_truncate") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "17dr497ihlwy8s7i130b0wf8k61rf1nndx4rfflsyz7bd964x2r0")))

(define-public crate-uu_truncate-0.0.24 (c (n "uu_truncate") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "10l5xidlcyas42qghzwjayxg697fd477b52ywpq984i3lcjf3ir6")))

(define-public crate-uu_truncate-0.0.25 (c (n "uu_truncate") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0vawylr76q52qzp9kywmhrpgyb4872lzi13j6jpb3k5lbp0q1q0h")))

(define-public crate-uu_truncate-0.0.26 (c (n "uu_truncate") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "09xd1j6ahccwnzb80z5abnvd4w3kpxzg8rcg8rydpka15cha4j4w")))

