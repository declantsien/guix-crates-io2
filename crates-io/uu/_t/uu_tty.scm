(define-module (crates-io uu _t uu_tty) #:use-module (crates-io))

(define-public crate-uu_tty-0.0.1 (c (n "uu_tty") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "1jd6fwfq86c8i4h12qkpnwvpm25g7k1342p5p593s6rlx99gcf7w")))

(define-public crate-uu_tty-0.0.2 (c (n "uu_tty") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1szdvfihsrj4kmm9s5l9c01a0wj702wzg1g20s6jdy2qsc42lh1x")))

(define-public crate-uu_tty-0.0.3 (c (n "uu_tty") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0bn0cmgmnf89mwmzzrkfc0k3wybp90f7a31fcs0qdcly1lkw5khz")))

(define-public crate-uu_tty-0.0.4 (c (n "uu_tty") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0qb5mwmbgg6q0qy2z2wmh1rywcbl7bv0g7csl85sw49yzay6rvvc")))

(define-public crate-uu_tty-0.0.5 (c (n "uu_tty") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1hcqcz50h3j5pjlf2rvfrm8w09g51pkmpj9p7f7nx8z21jg66iq1")))

(define-public crate-uu_tty-0.0.6 (c (n "uu_tty") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "035ivfkpgjvqy3pr97izksc9ylwqv2354phy3sjqphyaslayp9mc")))

(define-public crate-uu_tty-0.0.7 (c (n "uu_tty") (v "0.0.7") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "08hq2wr75gi6n1hjc97p3f9wiy8gmc4n42q61ikf8pliq53icdk1")))

(define-public crate-uu_tty-0.0.8 (c (n "uu_tty") (v "0.0.8") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "0qdjirfawgb95qjq18iaq1ffrb9sha6ppk0i4kx0vs3983k14aqf")))

(define-public crate-uu_tty-0.0.9 (c (n "uu_tty") (v "0.0.9") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0cp73p18zmcqj9mbg8a9dcn23hchsvah70zpvx5v353hslbzg058")))

(define-public crate-uu_tty-0.0.12 (c (n "uu_tty") (v "0.0.12") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0c44jqn56zmy58rc3rskl91cdbyhkm8g8zjs2rfc5ylybq4vif5j")))

(define-public crate-uu_tty-0.0.13 (c (n "uu_tty") (v "0.0.13") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0mjckncapjgdkwjyajrcm7wkrrwh2jppsnvzfvq3il91i6gfzrbf")))

(define-public crate-uu_tty-0.0.14 (c (n "uu_tty") (v "0.0.14") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1pr7fyxfmah7cyd6d6jl1jl8hsxafg94mjdfsp2c14kf5diwdiks")))

(define-public crate-uu_tty-0.0.15 (c (n "uu_tty") (v "0.0.15") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "08vgmkjs773sn8nc9wscpk99jc74vxs33w3l8wgg8627c5074nwk")))

(define-public crate-uu_tty-0.0.16 (c (n "uu_tty") (v "0.0.16") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1f7ym8dn3s2h4r1j2ysci1gs56mfcnrwc7xf7vgpfsmav2g8k37l")))

(define-public crate-uu_tty-0.0.17 (c (n "uu_tty") (v "0.0.17") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1giz0yxkmna2ssp8m1i36qfzy7z7cr6x26zlq5ry74kldpxw2r10")))

(define-public crate-uu_tty-0.0.18 (c (n "uu_tty") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.6") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("term"))) (k 0)) (d (n "uucore") (r ">=0.0.18") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "07d2agbfr44fng345079iiavim320v8whjhdxnqy50m4nwawxzr7")))

(define-public crate-uu_tty-0.0.19 (c (n "uu_tty") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.7") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("term"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1l9nkjj3g00hfvkdw29nvdywi64ql3iby3wsbk4qxqg0jpvxa1za")))

(define-public crate-uu_tty-0.0.20 (c (n "uu_tty") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.7") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("term"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0psgnnsx0cb6wi6h70dszlr4x196mwn83749kzj597198zmjm1ci")))

(define-public crate-uu_tty-0.0.21 (c (n "uu_tty") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.9") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("term"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "192dhbhcfzcy4383yamkq49ilj8c6wyiqp9bh2vx77xdmrrmz83a")))

(define-public crate-uu_tty-0.0.22 (c (n "uu_tty") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("term"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "0inq4rv0mqx1wai23fi6zpr3q3gs8y458gj340df1lcjpy628azf")))

(define-public crate-uu_tty-0.0.23 (c (n "uu_tty") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("term"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "01pa5l4fsmzbwhgbawxiwjz0ngwmb1li7nw659n947lwwdlzgfpx")))

(define-public crate-uu_tty-0.0.24 (c (n "uu_tty") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("term"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "1h40dnqpigw7cc9m7hsm3p986q2yv9qchgvfa3z94iivb0b5j6b2")))

(define-public crate-uu_tty-0.0.25 (c (n "uu_tty") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("term"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "03sdw6aqgk8jb3dpkcl17svxcl9f3cjjz4xprzdlajcxwddsmvr2")))

(define-public crate-uu_tty-0.0.26 (c (n "uu_tty") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("term"))) (k 0)) (d (n "uucore") (r ">=0.0.19") (f (quote ("fs"))) (d #t) (k 0) (p "uucore")))) (h "18dx0mhkvyswjjqvgkcq03jf6dw3sbabzswspijcgjgi3by4w5pa")))

