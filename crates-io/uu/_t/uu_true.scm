(define-module (crates-io uu _t uu_true) #:use-module (crates-io))

(define-public crate-uu_true-0.0.1 (c (n "uu_true") (v "0.0.1") (d (list (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0k7mwmaz3ndxjv472lr05llh2wbz92r182fxq2xs9kw76hndsnx8")))

(define-public crate-uu_true-0.0.2 (c (n "uu_true") (v "0.0.2") (d (list (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "140q827yryxrqljy1yflw2pmvpc1sgiq9qvz2vl6lb1q6jv9hjwy")))

(define-public crate-uu_true-0.0.3 (c (n "uu_true") (v "0.0.3") (d (list (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1iadry4j259bvrs750mx76si3ism4xv07w1i1796n4pmlwdqzwkb")))

(define-public crate-uu_true-0.0.4 (c (n "uu_true") (v "0.0.4") (d (list (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1mkhnbfkdyfmdq5xjhsnypvzw692d9lmazm9afhlv8b9k86knkmf")))

(define-public crate-uu_true-0.0.5 (c (n "uu_true") (v "0.0.5") (d (list (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0v4bna198b8ryz5zcfqm7ihlf20cnvsfb20pwvsyyf8cjv3xj5ws")))

(define-public crate-uu_true-0.0.6 (c (n "uu_true") (v "0.0.6") (d (list (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "11drk26g218rz90cl5phx22z5dqp3v6j2m4zvdf4x0jvq3a38xkj")))

(define-public crate-uu_true-0.0.7 (c (n "uu_true") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0maxwhla3gg1ikj4mz7l4j7llli61jyzz9fdyrps4xbaigiw8nf8")))

(define-public crate-uu_true-0.0.8 (c (n "uu_true") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "1a5ps56wrm2ir4xvi0n2jclxnvn64cngrbqm6h9cqn743sxnn7fl")))

(define-public crate-uu_true-0.0.9 (c (n "uu_true") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1a2j98aib665g05936brsgslgh4681c2fa1fckw2kp0n8lf8z4gb")))

(define-public crate-uu_true-0.0.12 (c (n "uu_true") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1gg01lds5hflbc4hh3g75l3l2z8c4pc1bbbs78hp3dbchq4wmzwf")))

(define-public crate-uu_true-0.0.13 (c (n "uu_true") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0pvblf06sfmdi4dfihsaqbzr0ma8b0vyg3w6kff7i07jnlw4vskn")))

(define-public crate-uu_true-0.0.14 (c (n "uu_true") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0zk7r93wr0a89z7mjxlrmqx34dk4bz739kl3wkpsx42xfp2b21bg")))

(define-public crate-uu_true-0.0.15 (c (n "uu_true") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1c079fww1b17chqnldw46px03iv7jkv4407i4im02ppd6gckrr4x")))

(define-public crate-uu_true-0.0.16 (c (n "uu_true") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1rg4bmzlk525q21accfbpg7znzjvg3yrpd9lw8mqg1lm5wiax8yc")))

(define-public crate-uu_true-0.0.17 (c (n "uu_true") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1g3rh8rrhxj1df7pxi1m1scz287rf8ns8zcl9gcx7cy950fa7b3g")))

(define-public crate-uu_true-0.0.18 (c (n "uu_true") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "1gzsfw4ap8hwhsy2vkjga1k8dydjd0vpnd4k6qzd9h7nfh3xc9gw")))

(define-public crate-uu_true-0.0.19 (c (n "uu_true") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0g5nv9w020m244xpxyfvfirbm1y70iwyvk3aqzygfl0szawlsbnp")))

(define-public crate-uu_true-0.0.20 (c (n "uu_true") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1cnpgwhkwifaki9f9k6azrmrfk3zasf9wqv331fq5wlb9ymwrlpa")))

(define-public crate-uu_true-0.0.21 (c (n "uu_true") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "05p488iqwqv1mjy7fr9dim7998caga9fpzcfqswxba97vh38s21w")))

(define-public crate-uu_true-0.0.22 (c (n "uu_true") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0n4r7dg6jaa1vr7yiq0j27qhlbid98hc81wskqd54a8qprm0pa0q")))

(define-public crate-uu_true-0.0.23 (c (n "uu_true") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "19vwgpxhsikjrx83k6llxw1sw6lkls235s7yy1slcqkjwg6322ly")))

(define-public crate-uu_true-0.0.24 (c (n "uu_true") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "17aa0fkl7f46rx9hsn7d1p96f87kpw7vm8ib57446mvywrjvs40x")))

(define-public crate-uu_true-0.0.25 (c (n "uu_true") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1f40f91n8972j9w8v0h6jmhn4rs6k66qi2lycl1nm9r4rahyv1y1")))

(define-public crate-uu_true-0.0.26 (c (n "uu_true") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1vj0284nc9byf6kk8y6y4fiabn5z1jc66da8zya6pc3qix3nb3zi")))

