(define-module (crates-io uu _t uu_tac) #:use-module (crates-io))

(define-public crate-uu_tac-0.0.1 (c (n "uu_tac") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "0kk9dd33nl2fh7b2hmvn7d5q7haxzajcs3yykflk5lnvm6s1wc0b")))

(define-public crate-uu_tac-0.0.2 (c (n "uu_tac") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "10sj0vnh1pxyayw4c8wflbb9d80bxfqa9bgy65fjac6ffzndyjp5")))

(define-public crate-uu_tac-0.0.3 (c (n "uu_tac") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1f6m0ax55x8d6x3gkj5azingiy6a5dgmd7wn512gs46ma8fbcmi9")))

(define-public crate-uu_tac-0.0.4 (c (n "uu_tac") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "105hmnlaf0s2n8878k8333j0fffpgc2n2i3gjy8hnp1ylgjcnw31")))

(define-public crate-uu_tac-0.0.5 (c (n "uu_tac") (v "0.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "170j71fnj9hpabzgpw723i2bjw2iayxjq2wpkk83ddp2qfcn6q3p")))

(define-public crate-uu_tac-0.0.6 (c (n "uu_tac") (v "0.0.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "11r6yv3l5d3gslzs1ynpj6473xm4yfija02ik7lf2j7kw8di58v2")))

(define-public crate-uu_tac-0.0.7 (c (n "uu_tac") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "0zp00bzahkvmwirxlv1zqsp8llv05h4cwy16rc7add2nvpy311ng")))

(define-public crate-uu_tac-0.0.8 (c (n "uu_tac") (v "0.0.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "10jyni6sb76ax0rml146n81qcjjcq4ka2gc0y97vny5i2a61r4zj")))

(define-public crate-uu_tac-0.0.9 (c (n "uu_tac") (v "0.0.9") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "1ba7z5p8z4cvxb9rcj2lif040lf5lywad6hff9dil5pf1c8hcxhd")))

(define-public crate-uu_tac-0.0.12 (c (n "uu_tac") (v "0.0.12") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0zks0dpaszl8i0br64d8n44l05mx1hl6s5gm87ys7afalpgdsjz1")))

(define-public crate-uu_tac-0.0.13 (c (n "uu_tac") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1nsdq2liy5p1p3ccba1m4nizps402vppqazg7xah5c0naxa8nf9l")))

(define-public crate-uu_tac-0.0.14 (c (n "uu_tac") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "0chccjask3jjpiz6w2sdhj9ypwz1sfq719l4p2fyaahxgb1hvc1h")))

(define-public crate-uu_tac-0.0.15 (c (n "uu_tac") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "173h2608646r2r2aamq1sfylwl1z4l10bfq3zmjg09wp58bd8pxw")))

(define-public crate-uu_tac-0.0.16 (c (n "uu_tac") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "1gqp6p9f5518wn3w18i5qc8j845ah57db2hrzfx08qc7vcs9n830")))

(define-public crate-uu_tac-0.0.17 (c (n "uu_tac") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "1yk5p915kyx68dazszgq5fhr6qsiwy4bgym7i5igkh410qvyrxwk")))

(define-public crate-uu_tac-0.0.18 (c (n "uu_tac") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0v0qxhd3cihwg1yq705mvxghqimn9br7gq9qrri0frvasph10z6f")))

(define-public crate-uu_tac-0.0.19 (c (n "uu_tac") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0dx7y9j5mfngbnlhf9i3bmnix0i0n4maqavlssdrdyj80fxxd2g4")))

(define-public crate-uu_tac-0.0.20 (c (n "uu_tac") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "05yyyvlj519km9b57w99zmamz9bpv460q53cp73nap0x6z0gxgn2")))

(define-public crate-uu_tac-0.0.21 (c (n "uu_tac") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "00b70kjgzwif58x06rz9vjgdbgcfhsqwa10zyzf5cbi3qxsjc6aa")))

(define-public crate-uu_tac-0.0.22 (c (n "uu_tac") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.10.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0f2pkq1fh275jnvcwid14yl99jx34v7w5ca389lfck3p1npfhqjn")))

(define-public crate-uu_tac-0.0.23 (c (n "uu_tac") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1a5p94dcsb575n9y7919dwx0hwn5ix6isi9a1744xl2pi1c0nmgx")))

(define-public crate-uu_tac-0.0.24 (c (n "uu_tac") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1hl494w86iryfbygzh38z96ql23rnm4rvp6cgnjyclk3swr2f4ak")))

(define-public crate-uu_tac-0.0.25 (c (n "uu_tac") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1cj5g0jfibg1k8hb7az3234jp69b3n5432kfv5y1w7g79g18x70c")))

(define-public crate-uu_tac-0.0.26 (c (n "uu_tac") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0z4bzvqpm0dyzqlysb3wwidbjjknai9xgyln9kjbwrcwgji93qrf")))

