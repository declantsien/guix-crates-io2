(define-module (crates-io uu _t uu_tr) #:use-module (crates-io))

(define-public crate-uu_tr-0.0.1 (c (n "uu_tr") (v "0.0.1") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r "^0.0.4") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r "^0.0.4") (d #t) (k 0) (p "uucore_procs")))) (h "00mx0jcshgz4246d0k5a5hvj6k1vhipdf5dfqlz37bwvsg0i7rrb")))

(define-public crate-uu_tr-0.0.2 (c (n "uu_tr") (v "0.0.2") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.5") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1inbq1jadzmckgm7krhnis44mcmq8m0zqid4cknncyf4hvfpzsdi")))

(define-public crate-uu_tr-0.0.3 (c (n "uu_tr") (v "0.0.3") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.6") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0is00nzyvrq53gs6m57lz77fwypx2sinbf1sd1sq1d2g1hcw55l3")))

(define-public crate-uu_tr-0.0.4 (c (n "uu_tr") (v "0.0.4") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.7") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "1ljazj3jrwsydvxr6vwd6bj7absbqzf41y8fp1ahwxzhcbjz1r0y")))

(define-public crate-uu_tr-0.0.5 (c (n "uu_tr") (v "0.0.5") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0df05ahc33pkibqa9ddyra81gvy6kvczqlkgm5gkbdvr4hvrnlb4")))

(define-public crate-uu_tr-0.0.6 (c (n "uu_tr") (v "0.0.6") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.8") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.5") (d #t) (k 0) (p "uucore_procs")))) (h "0w2m4xyapbnw8vj9gkkj46vndvqadiyy8infv61y8sw84ym4h6hr")))

(define-public crate-uu_tr-0.0.7 (c (n "uu_tr") (v "0.0.7") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.9") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.6") (d #t) (k 0) (p "uucore_procs")))) (h "1d3naswagr8cz26cphpmcmg8m1yd9a9hls4pc50cfd7b7r5f5nsl")))

(define-public crate-uu_tr-0.0.8 (c (n "uu_tr") (v "0.0.8") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.10") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.7") (d #t) (k 0) (p "uucore_procs")))) (h "16wf2vznx8h639zhh1v8kavfvrzivi82278rvkqicyddyzxysrgj")))

(define-public crate-uu_tr-0.0.9 (c (n "uu_tr") (v "0.0.9") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "0jvjhlzr7d1xlvzac4rkfcsqrzbshnm793zdk9m2x7394lvfk49j")))

(define-public crate-uu_tr-0.0.12 (c (n "uu_tr") (v "0.0.12") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")) (d (n "uucore_procs") (r ">=0.0.8") (d #t) (k 0) (p "uucore_procs")))) (h "099943mpvmwgpkxiwk1dcbsgqx1vxg0h783kslfv72gs50wvr9ix")))

(define-public crate-uu_tr-0.0.13 (c (n "uu_tr") (v "0.0.13") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1hqhxwzy407xi9sivhgwjnpbdc1h6fmxzj72livahdg81ppsf2zn")))

(define-public crate-uu_tr-0.0.14 (c (n "uu_tr") (v "0.0.14") (d (list (d (n "clap") (r "^3.1") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.11") (d #t) (k 0) (p "uucore")))) (h "1jh9vf3x2kndsp7dm9fi95gn00rys3nf7hlxffgsq4rf2lhn6nl4")))

(define-public crate-uu_tr-0.0.15 (c (n "uu_tr") (v "0.0.15") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.15") (d #t) (k 0) (p "uucore")))) (h "1d0k4l38dbx47x3g6wl64l3x8h9kk2z38j0giy3lqabvzczsx6nr")))

(define-public crate-uu_tr-0.0.16 (c (n "uu_tr") (v "0.0.16") (d (list (d (n "clap") (r "^3.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.16") (d #t) (k 0) (p "uucore")))) (h "0ph6ssjc2gg7lv9b8qlpi7b51x45zkjplp4ifsa5nq3p1xwv4367")))

(define-public crate-uu_tr-0.0.17 (c (n "uu_tr") (v "0.0.17") (d (list (d (n "clap") (r "^4.0") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.17") (d #t) (k 0) (p "uucore")))) (h "074p6g5fvh8rivsylyxdj2h288i4abndpgbqhl8z2yfgx7fc752f")))

(define-public crate-uu_tr-0.0.18 (c (n "uu_tr") (v "0.0.18") (d (list (d (n "clap") (r "^4.2") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.18") (d #t) (k 0) (p "uucore")))) (h "0knvfj3gdc1avc45dcw8didsn43iwg23sh2d7wkg8xxcs1p1hwrc")))

(define-public crate-uu_tr-0.0.19 (c (n "uu_tr") (v "0.0.19") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1f2gzy2hkr139l6w8fip49zl94qp6wx2l82rpy1mqv9dpsipryj3")))

(define-public crate-uu_tr-0.0.20 (c (n "uu_tr") (v "0.0.20") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0b0ff8fgp7b5kycp4lqac14l6wc0qnv9fmly51bjbpgvzlmbnh50")))

(define-public crate-uu_tr-0.0.21 (c (n "uu_tr") (v "0.0.21") (d (list (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "1mcw6x7rdr9i6rwq5c7a09g6l9dgr3jj7ii9wxpbxi8wj49ngy6a")))

(define-public crate-uu_tr-0.0.22 (c (n "uu_tr") (v "0.0.22") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0dh43wx82mjbmj1fgk7smyxhgyi5alpf999q7hwhsk4s792y2rla")))

(define-public crate-uu_tr-0.0.23 (c (n "uu_tr") (v "0.0.23") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "017yhxp45dp3c5k5q7s28n0wzfd3qfzwfkd6l2xw13sw7qnr439f")))

(define-public crate-uu_tr-0.0.24 (c (n "uu_tr") (v "0.0.24") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0myq292pl0xmcbai2dbkydz4whgxr28frj2n3pg14w9z8kqi9r3p")))

(define-public crate-uu_tr-0.0.25 (c (n "uu_tr") (v "0.0.25") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "117ahd67hm9mb3knr2q55sf5aig7v1y6xhi7zipfpp1qhgfl8m49")))

(define-public crate-uu_tr-0.0.26 (c (n "uu_tr") (v "0.0.26") (d (list (d (n "clap") (r "^4.4") (f (quote ("wrap_help" "cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "uucore") (r ">=0.0.19") (d #t) (k 0) (p "uucore")))) (h "0rjax341pzwiv60sjiyx2hbi7bxwfxzd50d3pvaskvfrjrzi2wz2")))

