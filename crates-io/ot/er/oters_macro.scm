(define-module (crates-io ot er oters_macro) #:use-module (crates-io))

(define-public crate-oters_macro-0.1.0 (c (n "oters_macro") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "derive"))) (d #t) (k 0)))) (h "051aai56a4v666w1hfj4hl0ffhc1bnxxf4hfs24g0x2byh6vryxa")))

(define-public crate-oters_macro-0.1.2 (c (n "oters_macro") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "derive"))) (d #t) (k 0)))) (h "1vfs5jdr23j0ixywja006nxfmxjjb8kxjib8zkd4nm0krr0s1cpz")))

(define-public crate-oters_macro-0.1.3 (c (n "oters_macro") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "derive"))) (d #t) (k 0)))) (h "1027g1dry981pn94xdji7i8nqb1rlrcmx2g37mwm7ih8apwmqj81")))

(define-public crate-oters_macro-0.1.4 (c (n "oters_macro") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "derive"))) (d #t) (k 0)))) (h "1dfd4kyiph8794znlasgy97ayph3aygy1pcpdkchniy8im6xdx0m")))

(define-public crate-oters_macro-0.1.5 (c (n "oters_macro") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "derive"))) (d #t) (k 0)))) (h "1nk4is72ms4m38d3l0w7kymqgqghy89017b1hx6f0m9cgvva8c4w")))

(define-public crate-oters_macro-0.1.6 (c (n "oters_macro") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "derive"))) (d #t) (k 0)))) (h "1w8884xfjpf487i0082myq2slpcq8pscapncyxw6fz5xlpcmd1ch")))

(define-public crate-oters_macro-0.1.7 (c (n "oters_macro") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "derive"))) (d #t) (k 0)))) (h "0k74186cm3sr75a20xv9rx0lw5dv2rpd14kndwazp9ry9d38938q")))

(define-public crate-oters_macro-0.1.8 (c (n "oters_macro") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("extra-traits" "full" "derive"))) (d #t) (k 0)))) (h "0j0z7mdjc1nc9a7qfgsvlwjj5f2bqhkmv9dij7l5r3xgy0qpsgrk")))

