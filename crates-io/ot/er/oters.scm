(define-module (crates-io ot er oters) #:use-module (crates-io))

(define-public crate-oters-0.1.0 (c (n "oters") (v "0.1.0") (d (list (d (n "oters_gui") (r "^0.1.0") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.0") (d #t) (k 0)) (d (n "oters_macro") (r "^0.1.0") (d #t) (k 0)))) (h "01r7d9rnm5wwpx2npqxwnlkc64sal9w78hgyxbdndphqsxx34r5p")))

(define-public crate-oters-0.1.1 (c (n "oters") (v "0.1.1") (d (list (d (n "oters_gui") (r "^0.1.1") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.0") (d #t) (k 0)) (d (n "oters_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0aw7bmcabamkcd42bii97yidf731xh435yaqgjl9xwj4wvi7s2ri")))

(define-public crate-oters-0.1.2 (c (n "oters") (v "0.1.2") (d (list (d (n "oters_gui") (r "^0.1.2") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.2") (d #t) (k 0)) (d (n "oters_macro") (r "^0.1.2") (d #t) (k 0)))) (h "10dchl31dx31zr85071i6i9c1jhw6rx343x9g414xmcsk96kk90q")))

(define-public crate-oters-0.1.3 (c (n "oters") (v "0.1.3") (d (list (d (n "oters_gui") (r "^0.1.3") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.3") (d #t) (k 0)) (d (n "oters_macro") (r "^0.1.3") (d #t) (k 0)))) (h "1jbqjzmz6i1fhqgrn47dazxm74bcbmxvyr42fzszxqygg9dlbamn")))

(define-public crate-oters-0.1.4 (c (n "oters") (v "0.1.4") (d (list (d (n "oters_gui") (r "^0.1.3") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.4") (d #t) (k 0)) (d (n "oters_macro") (r "^0.1.4") (d #t) (k 0)))) (h "022qdi0bsnmckz67xr1zswnzrqc51rq9p6sb38kza9ywr242az7c")))

(define-public crate-oters-0.1.5 (c (n "oters") (v "0.1.5") (d (list (d (n "oters_gui") (r "^0.1.5") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.5") (d #t) (k 0)) (d (n "oters_macro") (r "^0.1.5") (d #t) (k 0)))) (h "0nzc9f6g0xd1x2mv5isnqwwg19nmskj7vkgmbkjf7v616fcgc7xz")))

(define-public crate-oters-0.1.6 (c (n "oters") (v "0.1.6") (d (list (d (n "oters_gui") (r "^0.1.6") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.6") (d #t) (k 0)) (d (n "oters_macro") (r "^0.1.6") (d #t) (k 0)))) (h "1gyacw5pmmgvf358qs80z2ypgcwv9zgmm6khmg8y22hwkrwzpk4b")))

(define-public crate-oters-0.1.7 (c (n "oters") (v "0.1.7") (d (list (d (n "oters_gui") (r "^0.1.7") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.7") (d #t) (k 0)) (d (n "oters_macro") (r "^0.1.7") (d #t) (k 0)))) (h "1dlpfisg6a91c6qvsp08m48ibn9a7dpfkxggb9qw3b51g8nilzla")))

(define-public crate-oters-0.1.8 (c (n "oters") (v "0.1.8") (d (list (d (n "oters_gui") (r "^0.1.8") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.8") (d #t) (k 0)) (d (n "oters_macro") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0922lnx3991f2f71kwpnj0c88nwpl8if2y8awmmkxrzn3h50yj24")))

