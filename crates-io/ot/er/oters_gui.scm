(define-module (crates-io ot er oters_gui) #:use-module (crates-io))

(define-public crate-oters_gui-0.1.0 (c (n "oters_gui") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.0") (d #t) (k 0)))) (h "1zmvnaixcbcfvd6yg3cfp5pbzcq081xi63w1pq25fxc27kf40il6")))

(define-public crate-oters_gui-0.1.1 (c (n "oters_gui") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.0") (d #t) (k 0)))) (h "0rsj05jfdc85cpkwr7y87d16qgwa9z1bbwbzbclpjbj29740k84b")))

(define-public crate-oters_gui-0.1.2 (c (n "oters_gui") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.2") (d #t) (k 0)))) (h "1gi06c11k4n8cgf55dfwbg30xdhf9gry0qmvhjyzszmffz5rrlcz")))

(define-public crate-oters_gui-0.1.3 (c (n "oters_gui") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.3") (d #t) (k 0)))) (h "1b47wldys9cnhbmvh4w73r4pbpv7g5z3hq37zsk3ln5rqbwr59wy")))

(define-public crate-oters_gui-0.1.5 (c (n "oters_gui") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.5") (d #t) (k 0)))) (h "1fyrr2n5ijz2w2vikpb8mjjm8n77pl9d1ddq1fyd5h483xzwg6rf")))

(define-public crate-oters_gui-0.1.6 (c (n "oters_gui") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.6") (d #t) (k 0)))) (h "1mqwpl40cfsl12sy012ai6gq9xx7nvlimy010hvfgn8d9ni6083l")))

(define-public crate-oters_gui-0.1.7 (c (n "oters_gui") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.7") (d #t) (k 0)))) (h "1lnx4nafa15b2p1pcq07rw9vl60l03v32l4qpkmj54ps6d8102nh")))

(define-public crate-oters_gui-0.1.8 (c (n "oters_gui") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "oters_lang") (r "^0.1.8") (d #t) (k 0)))) (h "1m1mk8jipjdixs5vfiznyjp292ybmda9f8wysxxh6938nz0xszqw")))

