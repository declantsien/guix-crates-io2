(define-module (crates-io ot er oter) #:use-module (crates-io))

(define-public crate-oter-0.1.0 (c (n "oter") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dbase") (r "^0.5.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shapefile") (r "^0.6.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0mjqa47zgns09d10c3q04ci7mz995jq0vr68hi29wdfykk67gy5z")))

