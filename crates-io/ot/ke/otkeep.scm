(define-module (crates-io ot ke otkeep) #:use-module (crates-io))

(define-public crate-otkeep-0.1.0 (c (n "otkeep") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "paths-as-strings") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "162g9smhs4wiaqn18yxsq7rgb99ynfpv9700iwdriha41v8mg4aq")))

