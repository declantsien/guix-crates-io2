(define-module (crates-io ot _u ot_utils) #:use-module (crates-io))

(define-public crate-ot_utils-0.1.0 (c (n "ot_utils") (v "0.1.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "1ga2bj47l8i4akw1j0dq4ikxmmplm8nky6fhbqyvskl4871g23my")))

(define-public crate-ot_utils-0.1.1 (c (n "ot_utils") (v "0.1.1") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "08valyn6h8nv5a332b4is7p00bia0dsgsb1da75yvhg4wijc9yhw")))

(define-public crate-ot_utils-0.1.2 (c (n "ot_utils") (v "0.1.2") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "141xxnnavnhivx3s1f6xasd6nxmwkhlaaihc0v03qslx37bkasxl")))

(define-public crate-ot_utils-0.1.3 (c (n "ot_utils") (v "0.1.3") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "1r5whndm9qs1md159xpgjnnpqkm6siq52bwdr8lj7i1fv162xah7")))

(define-public crate-ot_utils-0.1.4 (c (n "ot_utils") (v "0.1.4") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "000x22r5xxvjakcxgvjb4vlb8s39ngqlrclpmi7w16m9fy2hdq7s")))

