(define-module (crates-io ot p2 otp2) #:use-module (crates-io))

(define-public crate-otp2-0.0.1 (c (n "otp2") (v "0.0.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "unix-time") (r "^0.1.5") (d #t) (k 0)))) (h "03va6k2gamxfj3nrrf68llbqlxypaijyj53xsrdz1f2pr9jp6qic")))

