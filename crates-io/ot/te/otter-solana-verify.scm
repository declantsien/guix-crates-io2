(define-module (crates-io ot te otter-solana-verify) #:use-module (crates-io))

(define-public crate-otter-solana-verify-1.0.0 (c (n "otter-solana-verify") (v "1.0.0") (h "18aysh6mr542jp3r9k9bs84jc3xxi7m1xha08gj94n6p8n14h6aw")))

(define-public crate-otter-solana-verify-1.0.1 (c (n "otter-solana-verify") (v "1.0.1") (h "166dzcqr62rf2clcc2zp5hkjg30d1i7va2y3rf0i40wk76js3y64")))

(define-public crate-otter-solana-verify-1.0.2 (c (n "otter-solana-verify") (v "1.0.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)))) (h "1d5aa9m9dp4g15922i01hypw41kl7ychv79lncrjp3cs080l8in0")))

