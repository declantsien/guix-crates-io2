(define-module (crates-io ot dr otdrs) #:use-module (crates-io))

(define-public crate-otdrs-0.1.0 (c (n "otdrs") (v "0.1.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06fn17zyhp6c84vm1gwj3b1k2998k8bm2gn8vkk67pkclx7fzp1z")))

(define-public crate-otdrs-0.2.0 (c (n "otdrs") (v "0.2.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12av5blqcfh2v755xm2bn1mc05lqfn864c32mms52kjavcv2qgmn")))

(define-public crate-otdrs-0.3.0 (c (n "otdrs") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mha74wj1vm4mnpyw2lv7kj712bynf61pm5h4g84568va77xvdvf")))

(define-public crate-otdrs-0.4.0 (c (n "otdrs") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "crc") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "151ccz5ylqqd9kzqmwwp5hmwm4ja8fpdvcssnn0vvijw71v49r55")))

(define-public crate-otdrs-0.4.1 (c (n "otdrs") (v "0.4.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "crc") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00n1gxma2azcvsla37kirqm7qi99zk4z2c6ay0nr6x0fanc9ffw4")))

(define-public crate-otdrs-0.4.2 (c (n "otdrs") (v "0.4.2") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06hzklfdywgwh8wvj75x5mw4gz6hardkjdy1g3gkjcgdl0h1ji04")))

(define-public crate-otdrs-1.0.0 (c (n "otdrs") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ryxx5k0dgd9kzfqd2027ii1ych0mn769dp0wlvn7nawlj67q7sm")))

