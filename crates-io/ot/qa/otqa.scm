(define-module (crates-io ot qa otqa) #:use-module (crates-io))

(define-public crate-otqa-1.0.0 (c (n "otqa") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1k8i5szw27f8766jxa17wfr9k8dgdijhzq6v1w6ylraj2x7mknw6")))

(define-public crate-otqa-1.0.1 (c (n "otqa") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "115ih641mmy25s49yvfh81azwhsmglhhhbwjfi165wazhy6vqg8p")))

(define-public crate-otqa-1.0.2 (c (n "otqa") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09079b8by3f56y3qgbin2jqijk36ixr91i25aaqr70av92qdcvl4")))

