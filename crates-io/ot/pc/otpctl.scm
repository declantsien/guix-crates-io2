(define-module (crates-io ot pc otpctl) #:use-module (crates-io))

(define-public crate-otpctl-0.0.1 (c (n "otpctl") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "otplib") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1y7b0jjw6qzgz8q9fbm756x7a4r1rjhw7pdpiyxamdak53z1kdmg")))

(define-public crate-otpctl-0.0.2 (c (n "otpctl") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "otplib") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "15i6l03qn2vc43z73m2487hb1v6dfq2zqfxdvh2g6crzak57pzpw")))

