(define-module (crates-io ot pc otpcli-tkennedy1) #:use-module (crates-io))

(define-public crate-otpcli-tkennedy1-1.0.1 (c (n "otpcli-tkennedy1") (v "1.0.1") (d (list (d (n "base32") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clipboard") (r "^0") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "keyring") (r "^0") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stoken") (r "^0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "14fqgp08bks4jryz590hgmz9n78w87f6409slrpxn6snl09cfllq") (f (quote (("rsa_stoken" "stoken") ("keychain" "keyring") ("default" "keychain" "copy") ("copy" "clipboard"))))))

