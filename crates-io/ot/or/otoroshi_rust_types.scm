(define-module (crates-io ot or otoroshi_rust_types) #:use-module (crates-io))

(define-public crate-otoroshi_rust_types-0.0.1 (c (n "otoroshi_rust_types") (v "0.0.1") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "02h2kb3vs2qhfci3mpjwn7rhp2b9bdb0q6k0h3byqzcxwljf59mw")))

