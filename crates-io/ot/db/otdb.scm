(define-module (crates-io ot db otdb) #:use-module (crates-io))

(define-public crate-otdb-1.0.0 (c (n "otdb") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "01hs5jglyqb34r9gsdz3925hb26bklvkkxb4vs7j1df7459p329l") (f (quote (("blocking"))))))

