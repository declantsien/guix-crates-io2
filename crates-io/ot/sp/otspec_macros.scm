(define-module (crates-io ot sp otspec_macros) #:use-module (crates-io))

(define-public crate-otspec_macros-0.1.0 (c (n "otspec_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "13ral68v137xl027afklxr4i1finwmnx61a4hn9l55csnxfas1q2")))

