(define-module (crates-io ot sp otspec) #:use-module (crates-io))

(define-public crate-otspec-0.1.0 (c (n "otspec") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.3") (d #t) (k 0)) (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "otspec_macros") (r "^0") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)))) (h "1yfpinni4a1zj4fn1czsyf6pkkki1kn03i0pinzc4nfhxsrk9b2r")))

