(define-module (crates-io ot x- otx-pool-plugin-protocol) #:use-module (crates-io))

(define-public crate-otx-pool-plugin-protocol-0.1.0 (c (n "otx-pool-plugin-protocol") (v "0.1.0") (d (list (d (n "ckb-types") (r "^0.108") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "otx-format") (r "=0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k0ggjsif3p4r174vnai2314wk27k8zff7jc8y6an09hli4plwhm")))

