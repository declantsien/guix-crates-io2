(define-module (crates-io ot x- otx-format) #:use-module (crates-io))

(define-public crate-otx-format-0.1.0 (c (n "otx-format") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ckb-jsonrpc-types") (r "^0.108") (d #t) (k 0)) (d (n "ckb-sdk") (r "^2.5.0") (d #t) (k 0)) (d (n "ckb-types") (r "^0.108") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "molecule") (r "=0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fclxfgvqylhmm2d1bw1znm9kq2alww9jscyaynf4jvn6r31fzi4")))

