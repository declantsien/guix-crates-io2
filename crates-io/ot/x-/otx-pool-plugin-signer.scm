(define-module (crates-io ot x- otx-pool-plugin-signer) #:use-module (crates-io))

(define-public crate-otx-pool-plugin-signer-0.1.0 (c (n "otx-pool-plugin-signer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ckb-jsonrpc-types") (r "^0.108") (d #t) (k 0)) (d (n "ckb-sdk") (r "^2.5.0") (d #t) (k 0)) (d (n "ckb-types") (r "^0.108") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "otx-format") (r "=0.1.0") (d #t) (k 0)) (d (n "otx-pool-config") (r "=0.1.0") (d #t) (k 0)) (d (n "otx-pool-plugin-protocol") (r "=0.1.0") (d #t) (k 0)) (d (n "otx-sdk") (r "=0.1.0") (d #t) (k 0)) (d (n "secp256k1") (r "^0.24") (f (quote ("recovery"))) (d #t) (k 0)))) (h "1h9mf6rfbsgbnxr05vi1blqi5k96qdcp61xpa5rc9zxmpylr3gvr")))

