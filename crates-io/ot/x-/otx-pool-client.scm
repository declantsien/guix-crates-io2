(define-module (crates-io ot x- otx-pool-client) #:use-module (crates-io))

(define-public crate-otx-pool-client-0.1.0 (c (n "otx-pool-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ckb-types") (r "^0.108") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^18.0") (d #t) (k 0)) (d (n "otx-format") (r "=0.1.0") (d #t) (k 0)) (d (n "otx-pool-plugin-atomic-swap") (r "=0.1.0") (d #t) (k 0)) (d (n "otx-pool-plugin-protocol") (r "=0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a4xvbyzvvjs8g5b9ld5wa3b1c9pbl9vhvdxnycmc8yj2mq2r6wm")))

