(define-module (crates-io ot x- otx-pool-config) #:use-module (crates-io))

(define-public crate-otx-pool-config-0.1.0 (c (n "otx-pool-config") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ckb-jsonrpc-types") (r "^0.108") (d #t) (k 0)) (d (n "ckb-types") (r "^0.108") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0cwhv55a2w0iiz57nrgqi4g8332fjs9z8w34wxx1an06yryaf7jd")))

