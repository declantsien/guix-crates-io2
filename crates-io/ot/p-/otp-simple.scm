(define-module (crates-io ot p- otp-simple) #:use-module (crates-io))

(define-public crate-otp-simple-0.1.0 (c (n "otp-simple") (v "0.1.0") (d (list (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 2)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "1bqp3bbj8jnvwj8bpdkbk5gy29h8kgl9vhc49248gw91cv2cynpk")))

