(define-module (crates-io ot p- otp-rs) #:use-module (crates-io))

(define-public crate-otp-rs-0.1.0 (c (n "otp-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)))) (h "0r63vwrh7ri3bwm9h4jyzlp96bklnsnfmzzadz96cs48d57mljn5") (y #t)))

(define-public crate-otp-rs-0.1.1 (c (n "otp-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)))) (h "15ha43fqsb1v8px2snhqd2k871n9fgfhxqm5xbcma81g5my2dysm")))

