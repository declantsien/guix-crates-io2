(define-module (crates-io ot ps otpshka) #:use-module (crates-io))

(define-public crate-otpshka-1.0.0 (c (n "otpshka") (v "1.0.0") (d (list (d (n "ring") (r "^0.17.0-alpha.7") (k 0)))) (h "0kc7g4pw23r5crfa19da7wzd6afc17gw5xzsnf12awbn9qiifw76") (f (quote (("std"))))))

(define-public crate-otpshka-1.0.1 (c (n "otpshka") (v "1.0.1") (d (list (d (n "lhash") (r "^0.3") (f (quote ("sha1" "sha256" "sha512"))) (d #t) (k 0)))) (h "09q93z5ajj9fhhki5qyspsr30if486mnvzpy5i35x4zkw16p29q9") (f (quote (("std")))) (y #t)))

(define-public crate-otpshka-1.0.2 (c (n "otpshka") (v "1.0.2") (d (list (d (n "lhash") (r "^1.0.1") (f (quote ("sha1" "sha256" "sha512"))) (d #t) (k 0)))) (h "19s9v2rw44dln0sj04v7car52ciysnpqhbdn74hrgip6k6msq442") (f (quote (("std"))))))

