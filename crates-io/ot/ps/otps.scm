(define-module (crates-io ot ps otps) #:use-module (crates-io))

(define-public crate-otps-0.1.0 (c (n "otps") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "1ng5a7pdwdsq38i6pygh0p2d9v72vw5jmjbq4gm709hdvlbkii0s")))

(define-public crate-otps-0.1.1 (c (n "otps") (v "0.1.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "1gqyk2f7xs9xsmxjvpcl6a2sn41ky0avxqjq1n3171mx7klkxcv9")))

