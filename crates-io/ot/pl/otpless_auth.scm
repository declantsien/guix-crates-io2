(define-module (crates-io ot pl otpless_auth) #:use-module (crates-io))

(define-public crate-otpless_auth-0.1.0 (c (n "otpless_auth") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rsa") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "02sdn36y2c3bjab6ganzgvi750i8xcc1mvcmy2qjg1816qnw99v0") (r "1.73.0")))

(define-public crate-otpless_auth-0.1.1 (c (n "otpless_auth") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rsa") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1y6v3lylkhhhw60izya1zcp7m60hagjk3lc1wl3viqm4sj0jcak4") (r "1.73.0")))

(define-public crate-otpless_auth-0.1.2 (c (n "otpless_auth") (v "0.1.2") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rsa") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1js1bjj1vgkflz1m2ffpfhdx61msqpdxxs1q0dcr1ipr4f0ark5j") (r "1.73.0")))

