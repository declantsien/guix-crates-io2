(define-module (crates-io ot pl otplib) #:use-module (crates-io))

(define-public crate-otplib-0.0.1 (c (n "otplib") (v "0.0.1") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "err-derive") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1wp5pmbwh3v39bxhdcj1cfxnrmjfyykav3561wcrjg1alcgxsq5l") (y #t)))

(define-public crate-otplib-0.0.2 (c (n "otplib") (v "0.0.2") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "err-derive") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1iskm9yvq67qkndn3130gd2f4zbgy4xyf88xsw5bapzvm2hys2vs")))

