(define-module (crates-io ot op otopr-derive) #:use-module (crates-io))

(define-public crate-otopr-derive-0.1.0 (c (n "otopr-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01m7qggswxs6927qb2an18l9qnwxqj7mk0dr9jzk44qnqbycwcvm")))

(define-public crate-otopr-derive-0.1.1 (c (n "otopr-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0adj7wi62m74ql9qx7y8yb137pyr1rg66i69lwbpwz1gn9nyqpwg")))

(define-public crate-otopr-derive-0.1.2 (c (n "otopr-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01slwlrrbk6krkqmlrrjicnxccb4w4gqq720ql29g58zb4n4asgh")))

(define-public crate-otopr-derive-0.2.0 (c (n "otopr-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "0hyr3dxgfgbjsszb34r76nmc80d0ng7s3mdc0h9qz0ar4x0yxbgf")))

(define-public crate-otopr-derive-0.2.1 (c (n "otopr-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "1gj7y6y0hqkyvaf1hg9ijs4sy65dx7zcgr35ll1bf75ab3hfic26")))

(define-public crate-otopr-derive-0.2.2 (c (n "otopr-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "1jix0j3pzia70c9k279bldysrrjjz7c7qd3mdxxszxqm03ymq531")))

(define-public crate-otopr-derive-0.3.0 (c (n "otopr-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "1yw3qf94xpfmr90ydpcbvk6s1sy6a48wrkn1x7ndag3ckvmnx9cw")))

(define-public crate-otopr-derive-0.3.1 (c (n "otopr-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "1v54v4mv2c5lyyxsczl434c35ch443yan1zzr8pxp0r8w5ps0841")))

(define-public crate-otopr-derive-0.3.2 (c (n "otopr-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "01g64d3lm6bnbpcvs5n3c166jly4dazi5hxv82bz3dmpn4q755vw")))

(define-public crate-otopr-derive-0.4.0 (c (n "otopr-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "0gy2zlnijh9ckybc79w3igsly8lgqkh44p9bwwimzzgrdqn2pl7c")))

