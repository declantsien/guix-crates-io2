(define-module (crates-io ot s- ots-core) #:use-module (crates-io))

(define-public crate-ots-core-0.2.0 (c (n "ots-core") (v "0.2.0") (d (list (d (n "bitflags") (r "^2") (k 0)) (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "macaddr") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "socket2") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0wg3lm7ba7l62x3n22kn8qph2ynqskbmw37fh89jy9jy4z5df2db") (f (quote (("default") ("all" "serde" "time" "chrono")))) (s 2) (e (quote (("serde" "dep:serde" "bitflags/serde" "uuid/serde"))))))

