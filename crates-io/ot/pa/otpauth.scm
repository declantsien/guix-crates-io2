(define-module (crates-io ot pa otpauth) #:use-module (crates-io))

(define-public crate-otpauth-0.1.0 (c (n "otpauth") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "031flcym814qs0xnk827c9rlc2cvwamq9wz5yrdm6i3383ysfr5k") (f (quote (("unstable"))))))

(define-public crate-otpauth-0.2.0 (c (n "otpauth") (v "0.2.0") (d (list (d (n "base32") (r "*") (d #t) (k 0)) (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "05h58qbs91gzx74jk37k05m0hf3aghpvfkbm47bswa0przl5ki0l") (f (quote (("unstable"))))))

(define-public crate-otpauth-0.2.1 (c (n "otpauth") (v "0.2.1") (d (list (d (n "base32") (r "*") (d #t) (k 0)) (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 2)))) (h "1accb9197zdfc81kbj1x50w7d1w9ycya6a5qrwc9nlwygs2mh5yy") (f (quote (("unstable"))))))

(define-public crate-otpauth-0.2.2 (c (n "otpauth") (v "0.2.2") (d (list (d (n "base32") (r "*") (d #t) (k 0)) (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 2)))) (h "1na6zdid61bxd9p8rq0czc7rzvnr3dx342yy1kqlwlf3lz6nxx2m") (f (quote (("unstable"))))))

(define-public crate-otpauth-0.2.3 (c (n "otpauth") (v "0.2.3") (d (list (d (n "base32") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "092npjv13jhlnvxxr7fadj52n949mz2yys1i93rdsz6n391m9b35") (f (quote (("unstable"))))))

(define-public crate-otpauth-0.2.4 (c (n "otpauth") (v "0.2.4") (d (list (d (n "base32") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0bnqkdc1xxiz133yhza2h8pzssnb2s0cgbn5cyzqh8i2jg30gvh6") (f (quote (("unstable"))))))

(define-public crate-otpauth-0.2.5 (c (n "otpauth") (v "0.2.5") (d (list (d (n "base32") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1bn6zw7k81867f16w1p1f7b3swaq71zywhshl7lman87hs2hilql") (f (quote (("unstable"))))))

(define-public crate-otpauth-0.2.6 (c (n "otpauth") (v "0.2.6") (d (list (d (n "base32") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1i94cm2bpyp2qsf75mgxl18amy7acil639jbr87j8x5y52nsynjg") (f (quote (("unstable"))))))

(define-public crate-otpauth-0.2.7 (c (n "otpauth") (v "0.2.7") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1bk4mzdj57babxw1ic0sm07jn2yfgfih695n4ls2bvwg0w8fv66j") (f (quote (("unstable"))))))

(define-public crate-otpauth-0.3.0 (c (n "otpauth") (v "0.3.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)))) (h "1zijmpij9z8w9qcc5grhspbif13xfhnfs2qvbach47gv243iqaik") (f (quote (("unstable"))))))

(define-public crate-otpauth-0.4.0 (c (n "otpauth") (v "0.4.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.5") (d #t) (k 0)))) (h "00n5rz0q5298p7xakq0fj2zvmj9m8ji4chv6j68m469nxyh3d9zj") (f (quote (("unstable"))))))

(define-public crate-otpauth-0.4.1 (c (n "otpauth") (v "0.4.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.5") (d #t) (k 0)))) (h "09df6my7g5grws3b8pfqiazbb61d3wi0d9sp7ch8hhx0r0yrm9sc") (f (quote (("unstable"))))))

