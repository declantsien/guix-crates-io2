(define-module (crates-io ot pa otpauth-uri) #:use-module (crates-io))

(define-public crate-otpauth-uri-1.0.0 (c (n "otpauth-uri") (v "1.0.0") (d (list (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "1aacbw3gpng95bljbi4310bs5jgwq7mijpw6pvi8hy8fll5144l6")))

(define-public crate-otpauth-uri-1.0.1 (c (n "otpauth-uri") (v "1.0.1") (d (list (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "128anp339w2rs5h2aqf4kbxqa0fxc4ygqcr0519h3ksrqhk50pz5")))

(define-public crate-otpauth-uri-1.0.2 (c (n "otpauth-uri") (v "1.0.2") (d (list (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "0d31y21ilnv0vc4ndyglappx8345q8mp7fqvj81a905jx4qhripj")))

