(define-module (crates-io ot to otto_vec_derive) #:use-module (crates-io))

(define-public crate-otto_vec_derive-0.0.1 (c (n "otto_vec_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0c6vv3iqvfihj1qx4daxilbflklwkkawjbmbnbakry4z5p199h3r")))

