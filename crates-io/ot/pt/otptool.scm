(define-module (crates-io ot pt otptool) #:use-module (crates-io))

(define-public crate-otptool-0.1.0 (c (n "otptool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "bardecoder") (r "^0.4.2") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1xi6khmrgq2jfxbz9n7g9c9c0z45drhm32qrb13hm4cbv6yff2r8")))

