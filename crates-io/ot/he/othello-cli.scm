(define-module (crates-io ot he othello-cli) #:use-module (crates-io))

(define-public crate-othello-cli-1.0.0 (c (n "othello-cli") (v "1.0.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wn6w6yqizbl56ql2f6cfap9ihjnp3cv9b97cvqgd42bzlxncmfq")))

(define-public crate-othello-cli-1.0.1 (c (n "othello-cli") (v "1.0.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rbmwvzcjsiglywzxm6qc4r1f6a3pf0r2xsrssyxx7rxim9nn3h9")))

(define-public crate-othello-cli-1.0.2 (c (n "othello-cli") (v "1.0.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0glwlvw0ifi2kgw2r4ry4i7j6mipm7amgz3vmfzi8xii00gzamka")))

(define-public crate-othello-cli-1.0.3 (c (n "othello-cli") (v "1.0.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06hf809n81kp2cj5hy810ifiw1r9fxv11jlkipgw69nrmmarc9ik")))

(define-public crate-othello-cli-1.1.0 (c (n "othello-cli") (v "1.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1mhsl4p37zk97dxy71z4z67zqw5175r8ndxcx1lkxc3b61swy1bq")))

(define-public crate-othello-cli-1.3.0 (c (n "othello-cli") (v "1.3.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0yv83ciddppyvv2yy1w0976whxy0ghx7xpsbcihf1shcwqwbfly3")))

(define-public crate-othello-cli-1.4.0 (c (n "othello-cli") (v "1.4.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1lmm999k9y3p32aiqhgml346zm2c5sb7jnkvbbhxv6xwqfy6qs6r")))

