(define-module (crates-io ot he othello-agent) #:use-module (crates-io))

(define-public crate-othello-agent-0.1.0 (c (n "othello-agent") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rq5ahi6g4gykky8srshclbi0hycdjm8wbj25hr0zc3pybahjax9")))

(define-public crate-othello-agent-0.1.1 (c (n "othello-agent") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15i88p2y26figb0rfw90h23p12hc0v6q8qn5ylbmyfhrs3x5f5hc")))

(define-public crate-othello-agent-0.1.2 (c (n "othello-agent") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0s6s9bvmcpwhscngzlzndyw7da7vz8b13wy52qn4fpw4scgn43ix")))

