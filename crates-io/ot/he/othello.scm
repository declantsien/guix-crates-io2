(define-module (crates-io ot he othello) #:use-module (crates-io))

(define-public crate-othello-0.1.0 (c (n "othello") (v "0.1.0") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "bitintr") (r "^0.3.0") (d #t) (k 1)) (d (n "build_const") (r "^0.2.1") (d #t) (k 1)))) (h "013fqs36ys4v2msd132syazz90b5xcwh5jrnvxzsrag3wr931z7q")))

(define-public crate-othello-0.1.1 (c (n "othello") (v "0.1.1") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "bitintr") (r "^0.3.0") (d #t) (k 1)) (d (n "build_const") (r "^0.2.1") (d #t) (k 1)))) (h "1mfa8m3h4yg8yib7cwj4jjpqxjfpq9akp7bhva51a5pk11brgbva")))

(define-public crate-othello-0.1.2 (c (n "othello") (v "0.1.2") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "bitintr") (r "^0.3.0") (d #t) (k 1)) (d (n "build_const") (r "^0.2.1") (d #t) (k 1)))) (h "13qj5gxj1gjgw5lsi8v3d5wln5xxiszb4ypq6jgwnb3l6d43mykm")))

