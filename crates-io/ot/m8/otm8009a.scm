(define-module (crates-io ot m8 otm8009a) #:use-module (crates-io))

(define-public crate-otm8009a-0.1.0 (c (n "otm8009a") (v "0.1.0") (d (list (d (n "embedded-display-controller") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0vfwwr6gxgx8fv7lriw6mx4zkc4ix0x4v1d0byg41r1xx2i5jq75")))

