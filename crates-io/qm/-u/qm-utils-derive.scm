(define-module (crates-io qm -u qm-utils-derive) #:use-module (crates-io))

(define-public crate-qm-utils-derive-0.0.2 (c (n "qm-utils-derive") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z028amw3j124w7nqipl0xi3vl0zyrycfff0wjj49j9ibh44radg") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.1 (c (n "qm-utils-derive") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08cjmbliivnbv79fvhm4zvsmhvfphqjiz36rjb3s7k2vazxpddqh") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.3 (c (n "qm-utils-derive") (v "0.0.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0drw4b012r8rlz3qw0pc8sifjyrn9nvvx0wwn1k7zbkjmql14fh1") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.4 (c (n "qm-utils-derive") (v "0.0.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h8h7864fzivpm7z8c6w0psw2g8h4y6nl59c099hws17vc4vnjrs") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.5 (c (n "qm-utils-derive") (v "0.0.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bl6is5gsyz0g89xhi7qs27hcp85r9ayi603m0a30l6qzxaz4x4l") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.6 (c (n "qm-utils-derive") (v "0.0.6") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hdf2j6966z9860xkfk49i79s0xv5dbyycakjqzvdm2h3dgqyvhz") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.7 (c (n "qm-utils-derive") (v "0.0.7") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jk74cx3wnf613zg2g22rdm3xsqjpy34j2c0h53lgwg12pg95p39") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.8 (c (n "qm-utils-derive") (v "0.0.8") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p31w06v8993p0y55hbbr6pwhng3vsxcma6pl7wiqs2674y4wddg") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.9 (c (n "qm-utils-derive") (v "0.0.9") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03wwn63wakqsy8i4ky2rkzix0mnhbl6r33n5zjk9fzcapqqx65gw") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.10 (c (n "qm-utils-derive") (v "0.0.10") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yf4j636g3n66ychd5ig91k9j3fp5i6bp9n2306cdmgghga3sa4q") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.11 (c (n "qm-utils-derive") (v "0.0.11") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b4jpycbvzn9dqvc54yjzf6yv229q7yprc5m2vcvzdhzgrh91jy6") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.12 (c (n "qm-utils-derive") (v "0.0.12") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xmbnq51zv6ci22v8qswnsrzvwrbz2f2r55l7kplpgx6dyllsbnj") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.13 (c (n "qm-utils-derive") (v "0.0.13") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pg5yv2h8m65m0jvxbhnxkqq3vp7lkf2p3zakn1093rr0shml65w") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.14 (c (n "qm-utils-derive") (v "0.0.14") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "011qw9x715lgj0kh4lgldw7h3hn93bi6i64k9bc5pdr0b471j9pa") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.15 (c (n "qm-utils-derive") (v "0.0.15") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fkkq03x36h4c6hv6xicsniai05fd0zvi3pbry4spdbzkyx92y3f") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.16 (c (n "qm-utils-derive") (v "0.0.16") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r0c5d870zhh3gr8lwm384fp3379a9sgw9m8hy1xjaqy2m3zisf8") (r "1.75.0")))

(define-public crate-qm-utils-derive-0.0.17 (c (n "qm-utils-derive") (v "0.0.17") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cfij4s2fbwgxickkm20bgl5yfy68mxla4ag4192ifh53i9jkdbq") (r "1.75.0")))

