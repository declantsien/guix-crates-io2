(define-module (crates-io qm -u qm-utils) #:use-module (crates-io))

(define-public crate-qm-utils-0.0.2 (c (n "qm-utils") (v "0.0.2") (d (list (d (n "qm-utils-derive") (r "^0.0.2") (d #t) (k 0)))) (h "0b5l2vd2583f8xgzk95c270vpq8c4pyhcsm0663abj5p59l5bjcg") (r "1.75.0")))

(define-public crate-qm-utils-0.0.1 (c (n "qm-utils") (v "0.0.1") (d (list (d (n "qm-utils-derive") (r "^0.0.1") (d #t) (k 0)))) (h "1h8lvzxkc37mlpk3v0r8zmk36j0vwac56nj8db99s8f679kvp9k7") (r "1.75.0")))

(define-public crate-qm-utils-0.0.3 (c (n "qm-utils") (v "0.0.3") (d (list (d (n "qm-utils-derive") (r "^0.0.3") (d #t) (k 0)))) (h "05aca8krhsb353vqazgajcbjv8l0na8hkxr93506fs4ja7aj1mx9") (r "1.75.0")))

(define-public crate-qm-utils-0.0.4 (c (n "qm-utils") (v "0.0.4") (d (list (d (n "qm-utils-derive") (r "^0.0.4") (d #t) (k 0)))) (h "1hh5zn385g9adc0il1pgvlnq8r8jq9vrkkn2a7gjy5vfkl9f18a9") (r "1.75.0")))

(define-public crate-qm-utils-0.0.5 (c (n "qm-utils") (v "0.0.5") (d (list (d (n "qm-utils-derive") (r "^0.0.5") (d #t) (k 0)))) (h "0fkkqbw8825f8f9b2jd2bny5q3767mw5692abcci244f8j7ws2sj") (r "1.75.0")))

(define-public crate-qm-utils-0.0.6 (c (n "qm-utils") (v "0.0.6") (d (list (d (n "qm-utils-derive") (r "^0.0.6") (d #t) (k 0)))) (h "0jybc814sr53ygzj7xviiikqldfrqz6m2p5kszzmwdh7iz5w4dhz") (r "1.75.0")))

(define-public crate-qm-utils-0.0.7 (c (n "qm-utils") (v "0.0.7") (d (list (d (n "qm-utils-derive") (r "^0.0.7") (d #t) (k 0)))) (h "104ri0lhbd5hqzqw2ah3c28qx3kjhnzisfhii33dgbphyq16bdj5") (r "1.75.0")))

(define-public crate-qm-utils-0.0.8 (c (n "qm-utils") (v "0.0.8") (d (list (d (n "qm-utils-derive") (r "^0.0.8") (d #t) (k 0)))) (h "0rhhn0dhcbfczk7s0hka4f8d0vgrpdjz1fpcm0482jsz66h9xwda") (r "1.75.0")))

(define-public crate-qm-utils-0.0.9 (c (n "qm-utils") (v "0.0.9") (d (list (d (n "qm-utils-derive") (r "^0.0.9") (d #t) (k 0)))) (h "0k0pqqvpdwxxrn7f3q2zh61hp7vi9w6f0kccyjarf5vvzvhzgykp") (r "1.75.0")))

(define-public crate-qm-utils-0.0.10 (c (n "qm-utils") (v "0.0.10") (d (list (d (n "qm-utils-derive") (r "^0.0.10") (d #t) (k 0)))) (h "1f4c4g6xkcvdgkq5dm7m5hb32q1vjl2cb1p8rkjqbgsyrzd3c4s0") (r "1.75.0")))

(define-public crate-qm-utils-0.0.11 (c (n "qm-utils") (v "0.0.11") (d (list (d (n "qm-utils-derive") (r "^0.0.11") (d #t) (k 0)))) (h "0rk1axipsq76zx4kq4kv5cylaxiiny98ic0i987fifp8c7la6csk") (r "1.75.0")))

(define-public crate-qm-utils-0.0.12 (c (n "qm-utils") (v "0.0.12") (d (list (d (n "qm-utils-derive") (r "^0.0.12") (d #t) (k 0)))) (h "0jb2q2qzr0wwqrkwy460flhd8v36vp2lzcy94kxzwazn10fa16i6") (r "1.75.0")))

(define-public crate-qm-utils-0.0.13 (c (n "qm-utils") (v "0.0.13") (d (list (d (n "qm-utils-derive") (r "^0.0.13") (d #t) (k 0)))) (h "189qq2yfd0xcyks801qkz0j0sjlpzcm9nl3c1q45ljf8bq8fhxnz") (r "1.75.0")))

(define-public crate-qm-utils-0.0.14 (c (n "qm-utils") (v "0.0.14") (d (list (d (n "qm-utils-derive") (r "^0.0.14") (d #t) (k 0)))) (h "0r0wkw3hn8jql0g1mrbwqlbysq91fp25s0flrydnsp3wq7m5fxim") (r "1.75.0")))

(define-public crate-qm-utils-0.0.15 (c (n "qm-utils") (v "0.0.15") (d (list (d (n "qm-utils-derive") (r "^0.0.15") (d #t) (k 0)))) (h "1dqs42x7wram16ysmcdi1kadl2kng63kyb4cd4mq28g2f0nd3zc7") (r "1.75.0")))

(define-public crate-qm-utils-0.0.16 (c (n "qm-utils") (v "0.0.16") (d (list (d (n "qm-utils-derive") (r "^0.0.16") (d #t) (k 0)))) (h "068g189qg5lz842ixkgxq800zm922ij51yl7zma18pm3awxfy8vy") (r "1.75.0")))

(define-public crate-qm-utils-0.0.17 (c (n "qm-utils") (v "0.0.17") (d (list (d (n "qm-utils-derive") (r "^0.0.17") (d #t) (k 0)))) (h "10pnm7jp59ca3lw6p9q83q9l5lgimr6wxld8jl2xps76vyk5a113") (r "1.75.0")))

