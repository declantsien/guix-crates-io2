(define-module (crates-io qm ap qmap) #:use-module (crates-io))

(define-public crate-qmap-0.1.0 (c (n "qmap") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0h2si90kyd5zkwyk1a7kh7nb1fgq6w48s90awzr43kz7hcxxdr0g")))

