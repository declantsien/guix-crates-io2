(define-module (crates-io qm -r qm-role-build) #:use-module (crates-io))

(define-public crate-qm-role-build-0.0.2 (c (n "qm-role-build") (v "0.0.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "16vx37hlsvan78qlaz7zy5b9n58w82i56vxj7x9fjayfzj8ddpi6") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.3 (c (n "qm-role-build") (v "0.0.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0hf29fa077hbp2fp74aj2jhb561fnjmp6w94cycbddmv1gg20x5m") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.4 (c (n "qm-role-build") (v "0.0.4") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0jl6rrnz2mfwn2h413sg0pj6mpv3g40mw8iqhpqranqxkjw1rb3l") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.5 (c (n "qm-role-build") (v "0.0.5") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0xwf51x0mqbwx8qcrzf4sj80yx0b3kjvaxhjii8cy27i7wpcj0jv") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.6 (c (n "qm-role-build") (v "0.0.6") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "09w5qdilhasxp7b5mph88cfg730pgl5jlqn26m347rr7lp130hrp") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.7 (c (n "qm-role-build") (v "0.0.7") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "17wxz0xnmi3ly7rv63p7rr2r1nxy9p8c0ndr6d7d493ww8qy56k2") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.8 (c (n "qm-role-build") (v "0.0.8") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0z6p34hbgrgpv6kqijjgk463s8q2m3gvllgnamjpzq0b63rd6shq") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.9 (c (n "qm-role-build") (v "0.0.9") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "1fqw5sym15g7a322x425pnbnsmhzi0ha240v2c1mwsflfl41g7hj") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.10 (c (n "qm-role-build") (v "0.0.10") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0n30c9g668mv85r51xpr2hrchkkzzd6hq88r3rgyyk2nwd482cg3") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.11 (c (n "qm-role-build") (v "0.0.11") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0jaclw704p7hfvyzhskcichfzqqv3fjbkflzhkvyff4q8qkx30n0") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.12 (c (n "qm-role-build") (v "0.0.12") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "17w7pr9w61jlbnkz7m99g92c6vlbl2c1hgmqjr9kpkma7ycvqfhr") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.13 (c (n "qm-role-build") (v "0.0.13") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0xdla71wzc66a43prqm7hwqcs9l59804q6c2szldvdf3y359xw43") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.14 (c (n "qm-role-build") (v "0.0.14") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0aa3rgzxdm2m28m2mkhagjcv6l3m3b5qmcspdmv8b1m0ddki5m8x") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.15 (c (n "qm-role-build") (v "0.0.15") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "1ncxm18n823j0hx3n0b9zb2sf12hg37h2zbxc5awqbkjigkix7br") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.16 (c (n "qm-role-build") (v "0.0.16") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "05xa58zrqfm5jfn0ans3ldcdk7q7mj44cg7dyigw9rg8zrp9pbnj") (r "1.75.0")))

(define-public crate-qm-role-build-0.0.17 (c (n "qm-role-build") (v "0.0.17") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "1s7ljnhlsq1g562p78srsw2vlhph6rm958laz77zks8w1bvajf1w") (r "1.75.0")))

