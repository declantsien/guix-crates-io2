(define-module (crates-io qm -r qm-role) #:use-module (crates-io))

(define-public crate-qm-role-0.0.2 (c (n "qm-role") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fgiaajaawpl9lg6gprz47prxn5ai1a270w81cpaa7yjkllafdxx") (r "1.75.0")))

(define-public crate-qm-role-0.0.3 (c (n "qm-role") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "120rvrqyikkb6hp5q5vjzdyi24ll7r2yz6hpmgycbiagfclvw0n9") (r "1.75.0")))

(define-public crate-qm-role-0.0.4 (c (n "qm-role") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rmf4zbbifi5x88xm5i8k9swp1vzwyzx56gfd47s7dm1w6k776mj") (r "1.75.0")))

(define-public crate-qm-role-0.0.5 (c (n "qm-role") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "04w3jk0dgvh068lwfpnkr18cx3bqiyz93z0f12aqk2k2rricrsgj") (r "1.75.0")))

(define-public crate-qm-role-0.0.6 (c (n "qm-role") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0s5315jbq35y0xmcm6zbwlhnh82alzzgcj24jd1b3slprhk6gsi5") (r "1.75.0")))

(define-public crate-qm-role-0.0.7 (c (n "qm-role") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "05lvb0zb1jm9gdz0y19s64s7jkgn4iivs8xnp0wk2x1c31r03ic5") (r "1.75.0")))

(define-public crate-qm-role-0.0.8 (c (n "qm-role") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g9zd7mz5rdgf5jkgndf7qhqma5f6l2vgqnmzs86r10af12jik30") (r "1.75.0")))

(define-public crate-qm-role-0.0.9 (c (n "qm-role") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1270wqr0wkaqn15364xbhphwh14byxx4zdsnpq8lmcff4hnmv0ih") (r "1.75.0")))

(define-public crate-qm-role-0.0.10 (c (n "qm-role") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0acxv04kxvqn6qrl9gqqvyadpgp93gqa8vlp1zc07nl7m98z726s") (r "1.75.0")))

(define-public crate-qm-role-0.0.11 (c (n "qm-role") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pqhkz02jrx2q5g9xn6wbcrizz08nx8g9czczn866c3yg24pcddj") (r "1.75.0")))

(define-public crate-qm-role-0.0.12 (c (n "qm-role") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xldzvmbbazyiz9i4565v3yq0c9rwsnkhgmyinkvy4zxfiy3hxrs") (r "1.75.0")))

(define-public crate-qm-role-0.0.13 (c (n "qm-role") (v "0.0.13") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rb77ldy6yxn205ijnfjqkpqww2l15k9wbwlmrdbna1v0wh5id8p") (r "1.75.0")))

(define-public crate-qm-role-0.0.14 (c (n "qm-role") (v "0.0.14") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "async-graphql") (r "^7.0.1") (f (quote ("bson" "chrono" "chrono-tz" "uuid" "graphiql" "time"))) (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yral4gmkx91wzlrsyknvwzmh5lybrx9amyrgi8dnsf73ncxw5bc") (r "1.75.0")))

(define-public crate-qm-role-0.0.15 (c (n "qm-role") (v "0.0.15") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "async-graphql") (r "^7.0.1") (f (quote ("bson" "chrono" "chrono-tz" "uuid" "graphiql" "time"))) (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "19qvfgjv9xcc2fmclm3xbzs4v3kk7bg2czx405l9yqlzlyz9vhgm") (r "1.75.0")))

(define-public crate-qm-role-0.0.16 (c (n "qm-role") (v "0.0.16") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "async-graphql") (r "^7.0.1") (f (quote ("bson" "chrono" "chrono-tz" "uuid" "graphiql" "time"))) (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gczspsala673z3frw0w8dr0ndpd8jr4fagfp7ggc00rpn25lw52") (r "1.75.0")))

(define-public crate-qm-role-0.0.17 (c (n "qm-role") (v "0.0.17") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "async-graphql") (r "^7.0.1") (f (quote ("bson" "chrono" "chrono-tz" "uuid" "graphiql" "time"))) (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bwixm1scgw9hq6rqq7b17k183457jksm0gq4xc8k5ly8npfdgzi") (r "1.75.0")))

