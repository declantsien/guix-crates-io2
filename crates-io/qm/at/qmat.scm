(define-module (crates-io qm at qmat) #:use-module (crates-io))

(define-public crate-qmat-0.1.0 (c (n "qmat") (v "0.1.0") (h "1qqx6qnzd3k64ha7v9s16jxb5ls4hmxybjmc3iwf4sdmw0i6xxpa")))

(define-public crate-qmat-0.2.0 (c (n "qmat") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "0nrhli5g8hrz2ww0sx4wlcd1wvfcf443lvahilgzpgx9crnydlzj")))

(define-public crate-qmat-0.3.0 (c (n "qmat") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "1bxhmdp99j5x40z3lxc6lq98c9z9mm8jwy4zcaflbm6wj4jia51f")))

(define-public crate-qmat-0.4.0 (c (n "qmat") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "15vkvxl6ha7ins8jalyqav0cx0a62ag3cga20a5wj6df7x21kr2r")))

(define-public crate-qmat-0.5.0 (c (n "qmat") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "0hk92wrn98rakgfgymy6p91ihmdjxy42cic9x7vjcm3fvsr10by4")))

(define-public crate-qmat-0.5.1 (c (n "qmat") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fdj30qhfpiq6w8si7m6mzypdygy4wn6nnlzxhfxi6s84bdr38k8")))

