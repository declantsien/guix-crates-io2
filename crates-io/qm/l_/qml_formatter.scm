(define-module (crates-io qm l_ qml_formatter) #:use-module (crates-io))

(define-public crate-qml_formatter-0.1.0 (c (n "qml_formatter") (v "0.1.0") (d (list (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "00mh839jx8w40im8n5hwk8l4rzbm9isrjf2ckrysnl5qw9baa6m5") (r "1.60")))

(define-public crate-qml_formatter-0.2.0 (c (n "qml_formatter") (v "0.2.0") (d (list (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0x3awsxngpyf5nbkwynnw71hcgjvjnq49ivblw67znxn7477bkbb") (r "1.60")))

