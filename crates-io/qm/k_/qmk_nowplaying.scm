(define-module (crates-io qm k_ qmk_nowplaying) #:use-module (crates-io))

(define-public crate-qmk_nowplaying-0.1.0-alpha.1 (c (n "qmk_nowplaying") (v "0.1.0-alpha.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (f (quote ("linux-static-hidraw"))) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "mpris") (r "^2.0.0-rc2") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)))) (h "10a9i253dlgrcs84k0rc7g7n2n0nxhx0wd2x0qbazzmgxc2bi6dg") (y #t)))

