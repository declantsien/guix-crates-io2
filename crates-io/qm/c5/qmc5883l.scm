(define-module (crates-io qm c5 qmc5883l) #:use-module (crates-io))

(define-public crate-qmc5883l-0.0.1 (c (n "qmc5883l") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)))) (h "1wnflda62l4dbv7dmxl3g636050vcrf9c6p7pqab402cyk7frhfk")))

