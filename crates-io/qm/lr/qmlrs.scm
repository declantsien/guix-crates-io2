(define-module (crates-io qm lr qmlrs) #:use-module (crates-io))

(define-public crate-qmlrs-0.0.1 (c (n "qmlrs") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0sbf4qqynxs8jafx396l8bscz30gyrjvyvr1fd3rsak5n28yvpcm")))

(define-public crate-qmlrs-0.1.0 (c (n "qmlrs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "04qplc0gq81sf7s71hjz9c57r40v7bxfyq17qjrfci3j6fahjvbk")))

(define-public crate-qmlrs-0.1.1 (c (n "qmlrs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0qxzm51238w5jlh2713mk6vfhz68hdc0wsbv9iw51w7nv7zscr65")))

