(define-module (crates-io qm k- qmk-oled-api) #:use-module (crates-io))

(define-public crate-qmk-oled-api-0.1.0-alpha.1 (c (n "qmk-oled-api") (v "0.1.0-alpha.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (f (quote ("linux-static-hidraw"))) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "mpris") (r "^2.0.0-rc2") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m735ym13iw7mg3315rqan8wsinav96ynxg4ja369z0g5mv6q0sj")))

(define-public crate-qmk-oled-api-0.1.0-alpha.2 (c (n "qmk-oled-api") (v "0.1.0-alpha.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (f (quote ("linux-static-hidraw"))) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vblr7vz6g9k7m6ycbyivx54csicdr40xv81cq42mzq34zfn22bd")))

