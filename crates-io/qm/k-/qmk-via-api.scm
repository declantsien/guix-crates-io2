(define-module (crates-io qm k- qmk-via-api) #:use-module (crates-io))

(define-public crate-qmk-via-api-0.0.1 (c (n "qmk-via-api") (v "0.0.1") (d (list (d (n "hidapi") (r "^2.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.0") (f (quote ("abi3-py38"))) (d #t) (k 0)))) (h "15hpqyqiv54f9y6iz043nxq8vmy2ya7rfml8b0kq8f7322rdvv4r")))

