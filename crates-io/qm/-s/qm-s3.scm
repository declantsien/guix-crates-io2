(define-module (crates-io qm -s qm-s3) #:use-module (crates-io))

(define-public crate-qm-s3-0.0.1 (c (n "qm-s3") (v "0.0.1") (h "0s2zgkfm2m39sp6yilc8li62rlz516irgn0rsf3v531ld1q0scx3") (r "1.75.0")))

(define-public crate-qm-s3-0.0.2 (c (n "qm-s3") (v "0.0.2") (h "1wna22xr0l0w3lqd5py60x6wgw361zw0jzs6diah1a9f461p5659") (r "1.75.0")))

(define-public crate-qm-s3-0.0.3 (c (n "qm-s3") (v "0.0.3") (h "0qla9x956gyy8qzwxb5zf0n76mr3yixca9bqi2dc1plq979qh2w4") (r "1.75.0")))

(define-public crate-qm-s3-0.0.4 (c (n "qm-s3") (v "0.0.4") (h "0wl75i2a7mvf2ri61822sx3fdy0ch579i9d90sqbmc80p8lizv5z") (r "1.75.0")))

(define-public crate-qm-s3-0.0.5 (c (n "qm-s3") (v "0.0.5") (h "06slsm9zzk7vpx0n8qc0rphqlaqm66c115cy3mby86z07fix91zq") (r "1.75.0")))

(define-public crate-qm-s3-0.0.6 (c (n "qm-s3") (v "0.0.6") (h "18fg2da5h4ixfqb9a5skgqsc8k6dxsidwyi9wz6bjbcplbvxkgn3") (r "1.75.0")))

(define-public crate-qm-s3-0.0.7 (c (n "qm-s3") (v "0.0.7") (h "1kf262d325pidrb4zm3zb00j85rv5sqfs13rda9wyql21lz862sp") (r "1.75.0")))

(define-public crate-qm-s3-0.0.8 (c (n "qm-s3") (v "0.0.8") (h "12rclrclknkmqhbb9fpabsxsnnl62bqi05c7vi2347v10djd9xjw") (r "1.75.0")))

(define-public crate-qm-s3-0.0.9 (c (n "qm-s3") (v "0.0.9") (h "0a2zhg9nk5h7ri090dphll64250dy0k2gkgwa636vzsnk9j7gpdy") (r "1.75.0")))

(define-public crate-qm-s3-0.0.10 (c (n "qm-s3") (v "0.0.10") (h "10almngz02xifg5rlcd012b55zs0n32hc7c7257dd20v3jxlgvfl") (r "1.75.0")))

(define-public crate-qm-s3-0.0.11 (c (n "qm-s3") (v "0.0.11") (h "1g22lfr73hn4b0plmjf7kzc6lfxbqgy2q9rd0l0m9ad7grh0ksl8") (r "1.75.0")))

(define-public crate-qm-s3-0.0.12 (c (n "qm-s3") (v "0.0.12") (h "036b6kysd88fxyv3yh32sjv5p0kkdskdba930mx6zzgnbprkw5yg") (r "1.75.0")))

(define-public crate-qm-s3-0.0.13 (c (n "qm-s3") (v "0.0.13") (h "1jd54hp0j67v8flg4j5c56106407aa122pzz4bp5k3hdqfqjr0xf") (r "1.75.0")))

(define-public crate-qm-s3-0.0.14 (c (n "qm-s3") (v "0.0.14") (h "0cmp9j1p37w8hralcy2zsf17blgcx7ga853zjkwz2ybgagik6w1s") (r "1.75.0")))

(define-public crate-qm-s3-0.0.15 (c (n "qm-s3") (v "0.0.15") (h "02iq30vf3fqbk3748kdby566bz3xkif5i68mrz41n3fdr2xfapp9") (r "1.75.0")))

(define-public crate-qm-s3-0.0.16 (c (n "qm-s3") (v "0.0.16") (h "1r1iqsfiw5461mgb1rmi5dpx7sy5v0aq3kk5m9sz6dsk929hq5zz") (r "1.75.0")))

(define-public crate-qm-s3-0.0.17 (c (n "qm-s3") (v "0.0.17") (h "0f88140r0kdz2s90lfp9cw5vlbbv6129qn9axi3xq42hiiq9bxsr") (r "1.75.0")))

