(define-module (crates-io qm et qmetaobject_impl) #:use-module (crates-io))

(define-public crate-qmetaobject_impl-0.0.1 (c (n "qmetaobject_impl") (v "0.0.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0w0h7kf9hrabv15035y1s5zya7d87244r8m4q3xb8xg70j8m3ilp")))

(define-public crate-qmetaobject_impl-0.0.2 (c (n "qmetaobject_impl") (v "0.0.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "157wf468pk795raxbbb0zaw2zffcf2djzw2canp2jw8sj91k1f9f")))

(define-public crate-qmetaobject_impl-0.0.3 (c (n "qmetaobject_impl") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0338c04jzya1149hydyfk08rrwsb3vhswswzpyr1fhylf13q2pns")))

(define-public crate-qmetaobject_impl-0.0.4 (c (n "qmetaobject_impl") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1gx8i6j5qnj6y4js8a1fck705l5d1pfnh0jlzhkphfv2qqxc11p4")))

(define-public crate-qmetaobject_impl-0.0.5 (c (n "qmetaobject_impl") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "054s1wpijyp2jq4xgwrpky886mqzh0k594zmf4z3sbidnlzkqjzq")))

(define-public crate-qmetaobject_impl-0.1.0 (c (n "qmetaobject_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "044vb1w277lygzbd781d7aik5zism86n9sgifvahdndn0fjs6n05")))

(define-public crate-qmetaobject_impl-0.1.1 (c (n "qmetaobject_impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "05w39jidf5ypyl51f16q4bnjs71h1q6ihckrsmqd38cp7c1bsqqr")))

(define-public crate-qmetaobject_impl-0.1.3 (c (n "qmetaobject_impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0646aqabv2m3vg05baw8k8rkqqbgh89h46br74k6lrkkcmyyhkbl")))

(define-public crate-qmetaobject_impl-0.1.4 (c (n "qmetaobject_impl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b1wa3c07hnjwsichljnl79i2dk0kv3cjw4i899kp21d3xwr780x")))

(define-public crate-qmetaobject_impl-0.2.0 (c (n "qmetaobject_impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1svlwdwvlx7n8saqpq3hqryvrvqianjjlgprywcgbf50b59xkd5w")))

(define-public crate-qmetaobject_impl-0.2.1 (c (n "qmetaobject_impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15l3vslla22ab9frpx5j6m0n2nvwiz6bc3h4h89ds8prrfxch8m4")))

(define-public crate-qmetaobject_impl-0.2.2 (c (n "qmetaobject_impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12lriysrbychsiig55qf4d4qxjp882axd5shhn1a82717j2byjaq")))

(define-public crate-qmetaobject_impl-0.2.3 (c (n "qmetaobject_impl") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gr1b0my0sgv46zxffaa59p60qd4lxhq6a5bw8qlf94gf5wygsnb")))

(define-public crate-qmetaobject_impl-0.2.4 (c (n "qmetaobject_impl") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kzih0fxi9llw3ap2s4v05irjjdvxgfrxiphfpffyba7fa1rjfhs")))

(define-public crate-qmetaobject_impl-0.2.5 (c (n "qmetaobject_impl") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ss6zdpzl103icxpcygy7v6bx8v8bk4dzzzhzh7rc7k9gmc3rxmj")))

(define-public crate-qmetaobject_impl-0.2.7 (c (n "qmetaobject_impl") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p91krpswg1i6h893xksx27r382vq6j6xiw4xwyph0m75jnbaffj")))

(define-public crate-qmetaobject_impl-0.2.9 (c (n "qmetaobject_impl") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0315sbv5bbng205glwiij5x6avvjn3994kys59dl3nlzsqxi01k8")))

(define-public crate-qmetaobject_impl-0.2.10 (c (n "qmetaobject_impl") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09vnlk0pbm9v9nndigi9y44camrq28mpz7im7sbddp07qyblihmg")))

