(define-module (crates-io pw as pwasm-libc) #:use-module (crates-io))

(define-public crate-pwasm-libc-0.1.0 (c (n "pwasm-libc") (v "0.1.0") (h "1kg624w1j9lbf6qd577mhncpzbwspy5x3qs7xvivr5rv4fnp02hn") (f (quote (("strict"))))))

(define-public crate-pwasm-libc-0.1.1 (c (n "pwasm-libc") (v "0.1.1") (h "0d02hwzkfddp2vccpzssppllbz88jky5frkldkckkk6kkbn5kl72") (f (quote (("strict"))))))

(define-public crate-pwasm-libc-0.2.0 (c (n "pwasm-libc") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "rlibc") (r "^1.0") (d #t) (k 0)))) (h "1qh0xh2cmmvrgkfwhnm5d641wd65jcjx89wj75zznvq8sbb0fwq2") (f (quote (("strict") ("ext_memops"))))))

(define-public crate-pwasm-libc-0.2.1 (c (n "pwasm-libc") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "1ccj5b63ys1zsasiq7iwp4dgsilb323qbh4i6nmsz13x8xk0y7gk") (f (quote (("strict") ("ext_memops"))))))

