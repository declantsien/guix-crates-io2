(define-module (crates-io pw as pwasm-alloc) #:use-module (crates-io))

(define-public crate-pwasm-alloc-0.1.0 (c (n "pwasm-alloc") (v "0.1.0") (d (list (d (n "pwasm-libc") (r "^0.1") (d #t) (k 0)))) (h "0mqkkbjgzpaqfjsrcsxlxzziilxaad6pzp4kvqngkgfppylyfqbs") (f (quote (("strict"))))))

(define-public crate-pwasm-alloc-0.2.0 (c (n "pwasm-alloc") (v "0.2.0") (d (list (d (n "wee_alloc") (r "^0.1") (d #t) (k 0)))) (h "1ar7fmq4qg5xq738pdrlk1w7w7yxjx6qvzr0yfhmhmb35dl8vdzh") (f (quote (("strict"))))))

(define-public crate-pwasm-alloc-0.3.0 (c (n "pwasm-alloc") (v "0.3.0") (d (list (d (n "wee_alloc") (r "^0.2") (d #t) (k 0)))) (h "08wjzwx0vwi0dhpfyr18mm4618832rapsy5sxh9fzxbqcpda4jnl") (f (quote (("strict"))))))

(define-public crate-pwasm-alloc-0.4.0 (c (n "pwasm-alloc") (v "0.4.0") (d (list (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0kw51kq28m89sbmbdlnl7k47cri2r5hh4vnblxwkcx94y9v8w9mz") (f (quote (("strict"))))))

(define-public crate-pwasm-alloc-0.4.1 (c (n "pwasm-alloc") (v "0.4.1") (d (list (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1fg3yygyf0m3493izsq5pvsl68j6akrs28jflspzwifb5vw1khi0") (f (quote (("strict"))))))

