(define-module (crates-io pw as pwasm-abi) #:use-module (crates-io))

(define-public crate-pwasm-abi-0.1.0 (c (n "pwasm-abi") (v "0.1.0") (d (list (d (n "bigint") (r "^4") (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "parity-hash") (r "^1") (k 0)) (d (n "rustc-hex") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.3") (k 0)))) (h "0dlnpqxrzmhf1ppzqaywbd7gnj11xbmjrkdvz5sd96cz0as9fg3a") (f (quote (("std" "rustc-hex" "bigint/std") ("default"))))))

(define-public crate-pwasm-abi-0.1.1 (c (n "pwasm-abi") (v "0.1.1") (d (list (d (n "bigint") (r "^4") (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "parity-hash") (r "^1") (k 0)) (d (n "rustc-hex") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.3") (k 0)))) (h "03n941mvf11k6lw4j4kw5rqhf3rzhymvkxnx0j5l4am6r6pp87fg") (f (quote (("std" "rustc-hex" "bigint/std") ("default"))))))

(define-public crate-pwasm-abi-0.1.2 (c (n "pwasm-abi") (v "0.1.2") (d (list (d (n "bigint") (r "^4") (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "parity-hash") (r "^1") (k 0)) (d (n "rustc-hex") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.3") (k 0)))) (h "16h1xi60xzch7ppsacysdl8v7s71zy5rs7hpi41pv0cy0zsb1ysf") (f (quote (("std" "rustc-hex" "bigint/std") ("default"))))))

(define-public crate-pwasm-abi-0.1.3 (c (n "pwasm-abi") (v "0.1.3") (d (list (d (n "bigint") (r "^4") (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "parity-hash") (r "^1") (k 0)) (d (n "rustc-hex") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.3") (k 0)))) (h "1y5dbfk6wsv55ijbkymgq57rv3ylgzmszqpaafr3rgg074hfw1kz") (f (quote (("std" "rustc-hex" "bigint/std") ("default"))))))

(define-public crate-pwasm-abi-0.1.4 (c (n "pwasm-abi") (v "0.1.4") (d (list (d (n "bigint") (r "^4") (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "parity-hash") (r "^1") (k 0)) (d (n "rustc-hex") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.3") (k 0)))) (h "06qbjkyvqs848x3ssh350khax5hida52ms3ribp4lvfdnz8bbwzd") (f (quote (("std" "rustc-hex" "bigint/std") ("default"))))))

(define-public crate-pwasm-abi-0.1.5 (c (n "pwasm-abi") (v "0.1.5") (d (list (d (n "bigint") (r "^4.4.0") (k 0)) (d (n "byteorder") (r "^1.2.3") (k 0)) (d (n "parity-hash") (r "^1.2.0") (k 0)) (d (n "rustc-hex") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1w1hlwvscd0fayi7p4xh7dh2kw2chv0dmnn4vhwzmbhii896320d") (f (quote (("std" "rustc-hex" "bigint/std") ("default"))))))

(define-public crate-pwasm-abi-0.1.6 (c (n "pwasm-abi") (v "0.1.6") (d (list (d (n "bigint") (r "^4.4.0") (k 0)) (d (n "byteorder") (r "^1.2.3") (k 0)) (d (n "pwasm-std") (r "^0.10") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0rgv1fnhcqx45a7809jrrsb4121afrrsf3qbgykfzch9rcf5295n") (f (quote (("std" "rustc-hex" "bigint/std" "pwasm-std/std") ("default"))))))

(define-public crate-pwasm-abi-0.1.7 (c (n "pwasm-abi") (v "0.1.7") (d (list (d (n "bigint") (r "^4.4.0") (k 0)) (d (n "byteorder") (r "^1.2.3") (k 0)) (d (n "pwasm-std") (r "^0.10") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0xz8qyllmmxc77v49v0q1d6ipz2rs8cbw926hv1ckzq7a8fd1jd0") (f (quote (("std" "rustc-hex" "bigint/std" "pwasm-std/std") ("default"))))))

(define-public crate-pwasm-abi-0.1.8 (c (n "pwasm-abi") (v "0.1.8") (d (list (d (n "bigint") (r "^4.4.0") (k 0)) (d (n "byteorder") (r "^1.2.3") (k 0)) (d (n "pwasm-std") (r "^0.10") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0xcxyx8lfbf9k5654g34dazk0ccx09ahr4a52lsp78k9b9m350vf") (f (quote (("std" "rustc-hex" "bigint/std" "pwasm-std/std") ("default"))))))

(define-public crate-pwasm-abi-0.1.9 (c (n "pwasm-abi") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.2.3") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "pwasm-std") (r "^0.10") (d #t) (k 0)) (d (n "uint") (r "^0.3") (k 0)))) (h "1f3rs1p3gmcvs5agcl2lrcmk18il82sc4y367j53g3j4p508l51h") (f (quote (("std" "uint/std" "pwasm-std/std") ("default"))))))

(define-public crate-pwasm-abi-0.1.10 (c (n "pwasm-abi") (v "0.1.10") (d (list (d (n "byteorder") (r "^1.2.3") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "pwasm-std") (r "^0.10") (d #t) (k 0)) (d (n "uint") (r "^0.3") (k 0)))) (h "1q469a41gxkiz6pgv8lqgp9nrgaq3ivnlkk7s608aciq9f354dba") (f (quote (("strict") ("std" "uint/std" "pwasm-std/std") ("default"))))))

(define-public crate-pwasm-abi-0.2.0 (c (n "pwasm-abi") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "pwasm-std") (r "^0.13") (d #t) (k 0)))) (h "0278rgwz2szijw43ynzxq36hrs2y3nf17alibfkbhr0w85pjglg4") (f (quote (("strict") ("std" "pwasm-std/std" "byteorder/std") ("default"))))))

(define-public crate-pwasm-abi-0.2.1 (c (n "pwasm-abi") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "pwasm-std") (r "^0.13") (d #t) (k 0)))) (h "12r8r21x950jqdza52axki1cxjq5mszhy9z6hr10fl06kvsr2cjm") (f (quote (("strict") ("std" "pwasm-std/std" "byteorder/std") ("default"))))))

(define-public crate-pwasm-abi-0.2.2 (c (n "pwasm-abi") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.2") (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "pwasm-std") (r "^0.13") (d #t) (k 0)))) (h "14wr5k1hwknhwvypj96s79fvwfl912d5hyn9g1srdqmh771axk1y") (f (quote (("strict") ("std" "pwasm-std/std" "byteorder/std") ("default"))))))

