(define-module (crates-io pw na pwnage) #:use-module (crates-io))

(define-public crate-pwnage-0.0.1 (c (n "pwnage") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "02s9p51xx7l2z01rhi8xa6h6mrmhl2qlq5ipj2y8vhfybmz5yk76")))

