(define-module (crates-io pw r_ pwr_airly) #:use-module (crates-io))

(define-public crate-pwr_airly-0.0.1 (c (n "pwr_airly") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06avqj3bfgq017r7yhjqhnvxk3rrsj7dbd8z1xnnqykkkfdrzsgf")))

(define-public crate-pwr_airly-0.1.0 (c (n "pwr_airly") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gz79c61ys0i8dij7y9519g3c5pl1pbjzwkfiy6nwlgrwfp9j6rg")))

(define-public crate-pwr_airly-0.2.0 (c (n "pwr_airly") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "1bdj5gsykiykkv0jmy9xa6n043h5znmclcpmfx54ircw4adqdjf2")))

