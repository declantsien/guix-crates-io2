(define-module (crates-io pw al pwalarmctl) #:use-module (crates-io))

(define-public crate-pwalarmctl-0.1.0 (c (n "pwalarmctl") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "0bw01599wvsa1lisz909042m3v97n2jj2jx8rxcwh5pg48kvkpnf") (r "1.74")))

