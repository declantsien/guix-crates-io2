(define-module (crates-io pw al pwallet) #:use-module (crates-io))

(define-public crate-pwallet-0.1.0 (c (n "pwallet") (v "0.1.0") (d (list (d (n "cok") (r "^0.1.9") (d #t) (k 0)) (d (n "doe") (r "^0.1.76") (d #t) (k 0)))) (h "0d2wnnhkbzf89hp1snwp3hy923mpj7yabshiljqrpz7w04fff2xk")))

(define-public crate-pwallet-1.0.0 (c (n "pwallet") (v "1.0.0") (d (list (d (n "cok") (r "^0.1.12") (d #t) (k 0)) (d (n "doe") (r "^0.1.85") (d #t) (k 0)))) (h "1byy3vn5pmhg7b7xqki0qxcp2za2vx2ygjvsldji1lh0w9dyfy5b")))

(define-public crate-pwallet-1.0.1 (c (n "pwallet") (v "1.0.1") (d (list (d (n "cok") (r "^0.1.13") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "doe") (r "^0.1.85") (d #t) (k 0)))) (h "1mmmqljqs429gsxmbkywccdp8cm9wr0pqvns43x2972y8zcgf4wp")))

