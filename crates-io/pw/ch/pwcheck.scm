(define-module (crates-io pw ch pwcheck) #:use-module (crates-io))

(define-public crate-pwcheck-0.1.0 (c (n "pwcheck") (v "0.1.0") (d (list (d (n "portable-pty") (r "^0.8.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fwk7pxwx50qmj3v7bqgb5y44w94lq5kx3yk63nw5zyz83fd7rin") (r "1.64")))

(define-public crate-pwcheck-0.2.0 (c (n "pwcheck") (v "0.2.0") (d (list (d (n "pam-client") (r "^0.5.0") (t "cfg(target_os = \"linux\")") (k 0)) (d (n "portable-pty") (r "^0.8.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1v1ikaq7024967kfvni0b1jg6gf0wn8h5arrz2lfzp45ka8qnlk2") (r "1.64")))

(define-public crate-pwcheck-0.2.1 (c (n "pwcheck") (v "0.2.1") (d (list (d (n "pam-client") (r "^0.5.0") (t "cfg(target_os = \"linux\")") (k 0)) (d (n "portable-pty") (r "^0.8.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0cp73ma4adcdryizic9qrmy8rdcrh7x6rj4md9xz0xnbm7b99lhw") (r "1.64")))

