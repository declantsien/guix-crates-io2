(define-module (crates-io pw ch pwchecker-rs) #:use-module (crates-io))

(define-public crate-pwchecker-rs-0.1.0 (c (n "pwchecker-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.1") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "02zmcj93nk9h45ydnqpzq73jm3qk5p67h9rk75gs6z3nsgns91af")))

(define-public crate-pwchecker-rs-0.1.1 (c (n "pwchecker-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.1") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "09abdx1y00zaj3fn7abs8q1qj0krf2yigwyl5svys33rw68k6kgd")))

(define-public crate-pwchecker-rs-0.1.2 (c (n "pwchecker-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.1") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0qj32j89vrz347qcz0vzv0vwr67n3948hvb7g8cjph11lajslm7j")))

(define-public crate-pwchecker-rs-0.1.3 (c (n "pwchecker-rs") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1dnv4qlwc19sqnqvnjjfvh33z5rww4hmqkqr93rafcbhj0q6s27n")))

(define-public crate-pwchecker-rs-0.1.4 (c (n "pwchecker-rs") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0gs6wfi7czmfgzmxpgdfl7568vpfa9bm8gxrz21zl4p809f8pnm2")))

(define-public crate-pwchecker-rs-0.1.5 (c (n "pwchecker-rs") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1sc0sr62gvlvi0bv7v7y5k5288v03h6vyfz9kbxbyfni8l9b2lx9")))

