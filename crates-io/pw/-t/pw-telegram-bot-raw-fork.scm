(define-module (crates-io pw -t pw-telegram-bot-raw-fork) #:use-module (crates-io))

(define-public crate-pw-telegram-bot-raw-fork-0.9.0 (c (n "pw-telegram-bot-raw-fork") (v "0.9.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0zdc4qabx7pqksimc5way7w0ff1j6nj65l4c1myq15jq1yy9fy2n")))

(define-public crate-pw-telegram-bot-raw-fork-0.9.1 (c (n "pw-telegram-bot-raw-fork") (v "0.9.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ww71l1iv6id3yskl1g9shagv2nk01knlkz2rryx2ff8a43d1s05")))

(define-public crate-pw-telegram-bot-raw-fork-0.9.2 (c (n "pw-telegram-bot-raw-fork") (v "0.9.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1fbn0bznsdmlaw54ff6ss357d1lnv7cfdi7i3gq7n754b6b3bjwn")))

