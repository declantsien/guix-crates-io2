(define-module (crates-io pw n- pwn-helper-macros) #:use-module (crates-io))

(define-public crate-pwn-helper-macros-0.1.0 (c (n "pwn-helper-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "06xfja8xy64l6wwqi09k9ciw9f9ykaa7fswmynabk0dq1jjm00ar")))

