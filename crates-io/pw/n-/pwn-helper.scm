(define-module (crates-io pw n- pwn-helper) #:use-module (crates-io))

(define-public crate-pwn-helper-0.1.0 (c (n "pwn-helper") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pwn-helper-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0h3k274h6mqpminc4k2gi2y3c9aqvgb7a8kcjbjzhxsq9419l6x3")))

