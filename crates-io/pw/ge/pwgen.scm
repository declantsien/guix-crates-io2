(define-module (crates-io pw ge pwgen) #:use-module (crates-io))

(define-public crate-pwgen-0.0.0 (c (n "pwgen") (v "0.0.0") (d (list (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0dvja1jyq65hdpjylybwhpcrwn1jfiy9ad0rjk5n4r2xblwy85z7") (y #t)))

(define-public crate-pwgen-0.1.0 (c (n "pwgen") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1ckrlq9qgskq5ydmyxy8yds7b2lr3r2vss2iidak00dwrrbry8xz")))

(define-public crate-pwgen-0.1.1 (c (n "pwgen") (v "0.1.1") (d (list (d (n "arboard") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0gi39pllv871idqavmdbjhbrbmdx7yw3a901vnfs4h2l38xv2hm3")))

(define-public crate-pwgen-0.1.2 (c (n "pwgen") (v "0.1.2") (d (list (d (n "arboard") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1mxx1nmkb7d5rmq1vmhfx1garghjszrc8pr135qzrsy22b8mp1d4")))

(define-public crate-pwgen-0.1.3 (c (n "pwgen") (v "0.1.3") (d (list (d (n "arboard") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1z6lra3r2040691mb7haiw3jdc9yjdkd1qlifd92phxid6lx8vly")))

