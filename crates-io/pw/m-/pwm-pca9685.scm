(define-module (crates-io pw m- pwm-pca9685) #:use-module (crates-io))

(define-public crate-pwm-pca9685-0.1.0 (c (n "pwm-pca9685") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0marlkxpwkdmk1q0hq43kc32g0qh49s077sp6if2pk7lwf2ilr7p")))

(define-public crate-pwm-pca9685-0.1.1 (c (n "pwm-pca9685") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1489cgdxfwrrl71i70wi2kjnfhn02ii6gnj2i49f96f094rvs68y")))

(define-public crate-pwm-pca9685-0.1.2 (c (n "pwm-pca9685") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0xk9s8fn37gk6hfgl46f2nq4qdi6z1lvinzv1bm644avsmlprgqw")))

(define-public crate-pwm-pca9685-0.2.0 (c (n "pwm-pca9685") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "0b9w9xayx60knfjz151adq7xhj5db2j1n57h5q6ml1dyszbah3ix")))

(define-public crate-pwm-pca9685-0.3.0 (c (n "pwm-pca9685") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "0f02bn21xv3k0fi1zjfm7i5qnlxmv123frq9gp0m9xvk1nlfjm20")))

(define-public crate-pwm-pca9685-0.3.1 (c (n "pwm-pca9685") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "11w3dmr29wy4y7696cp0fvs6fkxykms80iv38zbln2z5jv0ji1gk")))

(define-public crate-pwm-pca9685-1.0.0 (c (n "pwm-pca9685") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1") (o #t) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)) (d (n "maybe-async-cfg") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "01jrrsqg278bal7hyy0r002m4k53sqln1xcd21hhv88s6mfq5q1w") (f (quote (("std")))) (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

