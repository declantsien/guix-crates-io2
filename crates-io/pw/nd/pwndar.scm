(define-module (crates-io pw nd pwndar) #:use-module (crates-io))

(define-public crate-pwndar-0.1.0 (c (n "pwndar") (v "0.1.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (f (quote ("json"))) (d #t) (k 0)))) (h "18gs4d0bz7iz5hk0l18mmk8m5m9c6hcl9jlihn3bh6cvqzn72hk8")))

