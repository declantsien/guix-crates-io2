(define-module (crates-io pw nd pwnd) #:use-module (crates-io))

(define-public crate-pwnd-0.1.0 (c (n "pwnd") (v "0.1.0") (h "06qrw6way4d6llcxrirklvq4v3y02lkmpjdq7sn1hsncpilj68pk")))

(define-public crate-pwnd-0.1.1 (c (n "pwnd") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("io-util" "rt-multi-thread" "process" "net" "macros" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "1iaaimx0yqdf8j5za2ka2b3sx1jvi20qi44nhjjdvksm3c7ysk7k")))

