(define-module (crates-io pw sa pwsafe) #:use-module (crates-io))

(define-public crate-pwsafe-0.1.0 (c (n "pwsafe") (v "0.1.0") (d (list (d (n "block-modes") (r "^0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "twofish") (r "^0.1.0") (d #t) (k 0)))) (h "0rbgnsij6j6i3hk5qicm8s2crcrg37fmvbs9hsy0abl9nsswdbwr")))

(define-public crate-pwsafe-0.1.1 (c (n "pwsafe") (v "0.1.1") (d (list (d (n "block-modes") (r "^0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "twofish") (r "^0.1.0") (d #t) (k 0)))) (h "0an9knb8rz0nydzy8kgp8lrzyv8p6v85y18l8gyrh0hpip6h6n88")))

(define-public crate-pwsafe-0.1.2 (c (n "pwsafe") (v "0.1.2") (d (list (d (n "block-modes") (r "^0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "twofish") (r "^0.1.0") (d #t) (k 0)))) (h "0s031fbwxylskk1zvghgcy6ip075d3kvnb4s1ykxgfj583cjfrvv")))

