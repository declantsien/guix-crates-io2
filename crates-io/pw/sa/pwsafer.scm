(define-module (crates-io pw sa pwsafer) #:use-module (crates-io))

(define-public crate-pwsafer-0.1.3 (c (n "pwsafer") (v "0.1.3") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hmac") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9.6") (d #t) (k 0)) (d (n "twofish") (r "^0.6.0") (d #t) (k 0)))) (h "1malf2j93dbwa1syqnlnlpx42yb9p2a1pny03zp1n1y32bi0q12w")))

