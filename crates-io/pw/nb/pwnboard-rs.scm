(define-module (crates-io pw nb pwnboard-rs) #:use-module (crates-io))

(define-public crate-pwnboard-rs-1.0.0 (c (n "pwnboard-rs") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("serde_json" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "11007gvdahrxrnjbpffxprq1md7nikv9rw1pbdz3f7j17dza4f7f")))

(define-public crate-pwnboard-rs-1.0.1 (c (n "pwnboard-rs") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("serde_json" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0vwdy9gcnhb3b59hcfxgrjl0m3q3q4drq36iz64plz3lavx3pqpm")))

