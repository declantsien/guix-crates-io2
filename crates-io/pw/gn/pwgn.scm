(define-module (crates-io pw gn pwgn) #:use-module (crates-io))

(define-public crate-pwgn-0.1.0 (c (n "pwgn") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "password-gen") (r "^2.1.0") (d #t) (k 0)))) (h "1rxxbw1pp7sa0a95ygfs0zqrnf45l67kkglx8jdggasbvi6zc2wa")))

