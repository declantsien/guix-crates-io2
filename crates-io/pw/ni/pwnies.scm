(define-module (crates-io pw ni pwnies) #:use-module (crates-io))

(define-public crate-pwnies-0.0.1 (c (n "pwnies") (v "0.0.1") (h "1faryph45cbdimx3dab81rcwzhkk16ps50qydwxh3lzycr0pn662")))

(define-public crate-pwnies-0.0.2 (c (n "pwnies") (v "0.0.2") (h "12vv9zh7vhad26g5mrd52xiwsigsmqh4hr97aw6sd1gss8l23sbn")))

(define-public crate-pwnies-0.0.3 (c (n "pwnies") (v "0.0.3") (h "093a102fq7cpfvf0k86l5xlv1ymbbysrgsf1ky27w9yzm3gcyw0s")))

(define-public crate-pwnies-0.0.4 (c (n "pwnies") (v "0.0.4") (h "1fm3qwc6bd8jw8zhsq0qw02vwchdzlb2fd1scidldx8d0y39wk5y")))

(define-public crate-pwnies-0.0.5 (c (n "pwnies") (v "0.0.5") (h "1hpih3bydyni8hv8q5f6ph485kvrpwzbhvwvlg0wyczi0yn35pns")))

(define-public crate-pwnies-0.0.6 (c (n "pwnies") (v "0.0.6") (h "0k3893d27sjxf14vkbxh0fm2nkdh6k4v9zaczij5zzx2mgwzkd4a")))

(define-public crate-pwnies-0.0.7 (c (n "pwnies") (v "0.0.7") (h "134i123yqq7inxi1rs53zvwvavm7j68h933dfp3yk3v455mq5wm7")))

(define-public crate-pwnies-0.0.8 (c (n "pwnies") (v "0.0.8") (h "003lyd4vy6xc223b6vpg3zw3y5l821knadvflq4zcnr3smqvnllb")))

(define-public crate-pwnies-0.0.9 (c (n "pwnies") (v "0.0.9") (h "0yhpr515ycyvdw5gcfn14fbgs2lbx8wjykk6gjfnvc23w1z3cs8x")))

(define-public crate-pwnies-0.0.10 (c (n "pwnies") (v "0.0.10") (h "07pprqx1ccl1kcjllxsrcy63amhmya6cmm6cnggmx6yjycpa8m49")))

(define-public crate-pwnies-0.0.11 (c (n "pwnies") (v "0.0.11") (h "11krpld9nj9ap6rygji7xl6nysm8hss86df52n1a2hilxy4m1y5m")))

(define-public crate-pwnies-0.0.12 (c (n "pwnies") (v "0.0.12") (h "0kzpv40ywfr79s3mhmcrpbv7pvl5gwglflmkqw1k0cjzn09kzmb9")))

(define-public crate-pwnies-0.0.13 (c (n "pwnies") (v "0.0.13") (h "17v0xji0hqdagcq3pk4967hn89xl6jrkkc33ss87a7bapkmscmb4")))

(define-public crate-pwnies-0.0.14 (c (n "pwnies") (v "0.0.14") (h "1913wkkdyfgfpy52bx7pkkvx2x79mlxdsdg5100hvaa3q66rlvc0")))

