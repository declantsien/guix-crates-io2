(define-module (crates-io pw fr pwfr) #:use-module (crates-io))

(define-public crate-pwfr-0.0.0-none (c (n "pwfr") (v "0.0.0-none") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1jqa05yyz63c1ccpnwc4lr92k20kqxpyzdaqaq6is78fv918fczw")))

