(define-module (crates-io pw wi pwwizard) #:use-module (crates-io))

(define-public crate-pwwizard-0.1.0 (c (n "pwwizard") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09j5bn2y9jwn6cfn4k6g9yh923zcy7dkzcg0vblrx51hzb04rw87")))

(define-public crate-pwwizard-0.1.1 (c (n "pwwizard") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bz3s5mqzx7k4pwvj2in37slrh4zq4nbdl0fxp2cq94djhhgarxb")))

(define-public crate-pwwizard-0.1.2 (c (n "pwwizard") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ib6llc1pcr6aj4nj9jv70arfd8296xnkqdh101pw2zyh166vaq4")))

