(define-module (crates-io pw at pwat) #:use-module (crates-io))

(define-public crate-pwat-0.0.1 (c (n "pwat") (v "0.0.1") (d (list (d (n "rpassword") (r "^0.0.4") (d #t) (k 0)))) (h "0hspx3khbnkfympy13bgg714x8g1j3d8rg4zkzihs2y18hj6fkgm")))

(define-public crate-pwat-0.1.0 (c (n "pwat") (v "0.1.0") (d (list (d (n "rpassword") (r "^0.0.4") (d #t) (k 0)))) (h "0md8x8v5x1bwbp48yryv2bpl1vsvpvim6zfm364y4wy0k5g1y3ma")))

(define-public crate-pwat-0.1.1 (c (n "pwat") (v "0.1.1") (d (list (d (n "rpassword") (r "^0.0.4") (d #t) (k 0)))) (h "0rjmxy3540s699x1hirl1wm0ncmjkcyz17hn60y5f6gbbqkarsqy")))

