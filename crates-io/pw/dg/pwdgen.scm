(define-module (crates-io pw dg pwdgen) #:use-module (crates-io))

(define-public crate-pwdgen-1.0.0 (c (n "pwdgen") (v "1.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (f (quote ("colors"))) (k 0)))) (h "149aliqsq9d2l1kxsim6zh37bl6wlhw4wj2wi1wssgi7cjsyvaim") (y #t)))

(define-public crate-pwdgen-1.0.1 (c (n "pwdgen") (v "1.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (f (quote ("colors"))) (k 0)))) (h "0ynaflsdl3b0qjjixyrisw4b7dffd2kq3islaqwk2jvdmk7ff3x6") (y #t)))

(define-public crate-pwdgen-1.0.2 (c (n "pwdgen") (v "1.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (f (quote ("colors"))) (k 0)))) (h "0sg9bd305aywm7pmc2ljiq65rg68ff122r7q8wczn53892a7cwmf") (y #t)))

(define-public crate-pwdgen-1.0.3 (c (n "pwdgen") (v "1.0.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (f (quote ("colors"))) (k 0)))) (h "1ficaqb4d1062frib2jngq83ihgcw3mlhm8f2dix0xwdw4ymglqz") (y #t)))

(define-public crate-pwdgen-1.1.0 (c (n "pwdgen") (v "1.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (f (quote ("colors"))) (k 0)))) (h "17p9d2wn7ffm2chpgs34jj4zayb8jjqxw9pxj70nylnna3z4m1nc")))

