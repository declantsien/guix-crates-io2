(define-module (crates-io pw dg pwdg) #:use-module (crates-io))

(define-public crate-pwdg-0.1.0 (c (n "pwdg") (v "0.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1iln8rkj22s5pydrak7rn75p2mhz0qkb3dpcwl7hhlmrv5ib41fm")))

