(define-module (crates-io pw d- pwd-grp) #:use-module (crates-io))

(define-public crate-pwd-grp-0.1.0 (c (n "pwd-grp") (v "0.1.0") (d (list (d (n "derive-adhoc") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.143") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1r388amb1j2y7jsm49l8lbipiik6kcfds1j9i06l86ppxmpvslgh") (f (quote (("test-with-lmock") ("minimal-1") ("full" "default" "serde") ("default" "minimal-1")))) (r "1.54")))

(define-public crate-pwd-grp-0.1.1 (c (n "pwd-grp") (v "0.1.1") (d (list (d (n "derive-adhoc") (r ">=0.6, <0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.143") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0b1abvylq3q5pqi1vcrn896g53izgs5j3rzkdzgkna74swgw8mb9") (f (quote (("test-with-lmock") ("minimal-1") ("full" "default" "serde") ("default" "minimal-1")))) (r "1.54")))

