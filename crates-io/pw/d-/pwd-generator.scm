(define-module (crates-io pw d- pwd-generator) #:use-module (crates-io))

(define-public crate-pwd-generator-0.1.0 (c (n "pwd-generator") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yb0j1rkncmfz73slf0dsacrxhkf9v9z7sc42pkhvdfx9wiy5b21")))

