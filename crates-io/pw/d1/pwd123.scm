(define-module (crates-io pw d1 pwd123) #:use-module (crates-io))

(define-public crate-pwd123-0.1.0 (c (n "pwd123") (v "0.1.0") (d (list (d (n "blake3") (r "^1.5.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r23vn1clwdl1k6vvax1y1xffamdb4l1335yn5ifw14mq5rh545c")))

(define-public crate-pwd123-0.1.1 (c (n "pwd123") (v "0.1.1") (d (list (d (n "blake3") (r "^1.5.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nr98cmbf598p9h2102vac14klks0ssyhzjzf5zi49fnfkvlr1ns")))

