(define-module (crates-io pw in pwintln) #:use-module (crates-io))

(define-public crate-pwintln-0.1.3 (c (n "pwintln") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3.55") (d #t) (k 0)) (d (n "cstr") (r "^0.2.5") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "goblin") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "owoify") (r "^0.1.5") (d #t) (k 0)))) (h "05d6wg4i97cmnz0mb6c0zpq0xijww0ig090b5a9wnnw3fa8w38q2")))

