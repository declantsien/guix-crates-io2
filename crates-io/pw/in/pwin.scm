(define-module (crates-io pw in pwin) #:use-module (crates-io))

(define-public crate-pwin-0.1.0 (c (n "pwin") (v "0.1.0") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "0mi73i59lamsvfs4qar73sdnns9jcbrq143ia9a5ww0agmznp0k2") (y #t)))

(define-public crate-pwin-0.1.1 (c (n "pwin") (v "0.1.1") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1s7l2f9m5ch20a227xxgxf0s465971ky0899z170s1qdvyvpxh73") (y #t)))

(define-public crate-pwin-0.1.2 (c (n "pwin") (v "0.1.2") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1y3y5gv4kpdah66wdnnan6br9sx5dv8sv1cn653nc3ixg5isdjm2") (y #t)))

(define-public crate-pwin-0.1.3 (c (n "pwin") (v "0.1.3") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "19d9skwx3zkxshnh540ya44mg0hz5dmjalflv257zjswlmdcf6ah")))

(define-public crate-pwin-1.0.0 (c (n "pwin") (v "1.0.0") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1qn207p9sad8qz085hdkqsjgdh754s8nmfjbz1zf07avh3r7q12c")))

(define-public crate-pwin-1.0.1 (c (n "pwin") (v "1.0.1") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "15ccim2acyy14adakqld021maqp1w4bsdd8ypqz73g0vdsx1blkg")))

