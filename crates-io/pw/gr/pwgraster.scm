(define-module (crates-io pw gr pwgraster) #:use-module (crates-io))

(define-public crate-pwgraster-0.1.0 (c (n "pwgraster") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "memchr") (r "^1.0.1") (d #t) (k 0)) (d (n "png") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0bxwkwg6c20x41fdafpc4qdg0bdvivyqw592nqvwbybxfidvrrx1") (f (quote (("default"))))))

