(define-module (crates-io pw qu pwquality-sys) #:use-module (crates-io))

(define-public crate-pwquality-sys-0.2.0 (c (n "pwquality-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pk2wc6fp3x0yhkp41zpqpnkry0lmzs7gz5nhl9fbyhcjrp1wxny")))

