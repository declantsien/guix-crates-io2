(define-module (crates-io pw qu pwquality) #:use-module (crates-io))

(define-public crate-pwquality-0.1.0 (c (n "pwquality") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r4qv1srb9hmzafh7y48j7cvl2a4z47pdwgwb1qk0i5nnip9275y")))

(define-public crate-pwquality-0.2.0 (c (n "pwquality") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pwquality-sys") (r "^0.2") (d #t) (k 0)))) (h "0w675kqr15jdzmrgal67vzvz00iiy39iks2rfimp5wzrxyb9km45")))

