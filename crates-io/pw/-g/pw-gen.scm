(define-module (crates-io pw -g pw-gen) #:use-module (crates-io))

(define-public crate-pw-gen-0.1.0 (c (n "pw-gen") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.30") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2.4") (d #t) (k 0)))) (h "1vv8znv88jh06qbb34894hx516k5d92v4zjzyx4gn8p38ci464cv")))

(define-public crate-pw-gen-0.1.1 (c (n "pw-gen") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.30") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2.4") (d #t) (k 0)))) (h "17v9l804q10cym5ikz55ap6g1scgkw2i1ziq5w1gbjzb6pyn4w8n")))

(define-public crate-pw-gen-0.1.2 (c (n "pw-gen") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.30") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2.4") (d #t) (k 0)))) (h "0ww8y2glq6id0piynh3zy9g0c6ghmg1rfb61hhmn5d23sra865fx")))

