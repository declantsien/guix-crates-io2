(define-module (crates-io pw nr pwnr) #:use-module (crates-io))

(define-public crate-pwnr-0.1.0 (c (n "pwnr") (v "0.1.0") (d (list (d (n "checksec") (r "^0.0.8") (d #t) (k 0)) (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "ropr") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "05c8cigfrd622hy2665xlmmdl1z041jf2gjkimk82m27qgzfqb6z")))

(define-public crate-pwnr-0.1.1 (c (n "pwnr") (v "0.1.1") (d (list (d (n "checksec") (r "^0.0.8") (d #t) (k 0)) (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "ropr") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "03mfcrkz8gas958qkw469j5ddn6r07vpl7jmdfmf79cf508w2b23")))

(define-public crate-pwnr-0.1.2 (c (n "pwnr") (v "0.1.2") (d (list (d (n "checksec") (r "^0.0.8") (d #t) (k 0)) (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "ropr") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0fsivz2q9cjdd27n78zzc3mf4jbxyiqsdsnk2p3ads6ib2g9k2iy")))

