(define-module (crates-io pw rs pwrs) #:use-module (crates-io))

(define-public crate-pwrs-0.1.0 (c (n "pwrs") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "0r93b41d0czjfck3sc3p7n6qq7nv6lsnd61g4fkbnka85pnip9xa")))

(define-public crate-pwrs-1.0.0 (c (n "pwrs") (v "1.0.0") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1fp215b89wfhspifjz7wlhw0vsrrhh9dwd9yd4ig13ka805cs7f7")))

(define-public crate-pwrs-1.0.1 (c (n "pwrs") (v "1.0.1") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1ihr7vnfij2476rmlvx7842bf31yf2185hp7ck36dcs5ilkr4rn5")))

(define-public crate-pwrs-1.0.2 (c (n "pwrs") (v "1.0.2") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1s5n0ry54a538lgnp353qh1bhn257np143x5k2g69fg344cbx5zw")))

(define-public crate-pwrs-1.1.0 (c (n "pwrs") (v "1.1.0") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0kghx3xxpfq9rdgwr21nmzqgziick2ksyx5h7xpvby3cyy2fl47z")))

