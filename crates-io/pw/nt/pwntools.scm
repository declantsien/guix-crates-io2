(define-module (crates-io pw nt pwntools) #:use-module (crates-io))

(define-public crate-pwntools-0.1.0 (c (n "pwntools") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "elf-utilities") (r "^0.2.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0pijamqq29g1pn2digp51w2gfs64nxm3hlmm7ajrk2f6w91lqip0")))

(define-public crate-pwntools-0.1.1 (c (n "pwntools") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "elf-utilities") (r "^0.2.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "095gx0w6nvxjhy2qs5cw876rdb7l40sh8k87ri9vc9g77g1a90sq")))

(define-public crate-pwntools-0.1.2 (c (n "pwntools") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "elf-utilities") (r "=0.2.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1l99nxb49lm388ngk2sk0i0rr2aayhbs9x861gzh4w3ss72rk899")))

(define-public crate-pwntools-0.2.0 (c (n "pwntools") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "elf-utilities") (r "=0.2.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07ymnl3q690i60qhny1ixr11pjank0l56c7ljhnknswcdh5bmw0d")))

(define-public crate-pwntools-0.2.1 (c (n "pwntools") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "elf-utilities") (r "=0.2.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1dqyb68li8jp5s05xf2lzy2v8rzc0wc124la4pb44jcyw5gg3bin")))

(define-public crate-pwntools-0.3.0 (c (n "pwntools") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 2)) (d (n "elf-utilities") (r "=0.2.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("macros" "io-util" "io-std" "process"))) (d #t) (k 0)) (d (n "unicorn-engine") (r "^2.0.0-rc4") (d #t) (k 0)))) (h "1pcifbr53k1yfjyizsd7z59cyz6skx3ab038kz89cm8ws03ddljr")))

(define-public crate-pwntools-0.3.1 (c (n "pwntools") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 2)) (d (n "elf-utilities") (r "=0.2.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("macros" "io-util" "io-std" "process"))) (d #t) (k 0)) (d (n "unicorn-engine") (r "^2.0.0-rc5") (d #t) (k 0)))) (h "16lvx79fb36h8ay37q9bvfznbpjiiv7h2b6c17rxc4y2mmx25i9k")))

(define-public crate-pwntools-0.4.0 (c (n "pwntools") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "elf-utilities") (r "=0.2.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("macros" "io-util" "io-std" "process" "net"))) (d #t) (k 0)) (d (n "unicorn-engine") (r "^2.0.0-rc5") (o #t) (d #t) (k 0)))) (h "1286gmvskq1q21c3qy2s004jgarkm38gfw3f85f8qp0jlyzrs54n") (f (quote (("use-unicorn" "unicorn-engine") ("default" "use-unicorn"))))))

