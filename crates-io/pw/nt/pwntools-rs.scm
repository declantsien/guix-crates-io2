(define-module (crates-io pw nt pwntools-rs) #:use-module (crates-io))

(define-public crate-pwntools-rs-0.1.0 (c (n "pwntools-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)))) (h "0ibwrhw94pl8fbcnj5cnpcg27b1mvcjsjyw2c2bl4m9frhv0na3c") (y #t)))

