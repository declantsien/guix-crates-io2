(define-module (crates-io pw ds pwds) #:use-module (crates-io))

(define-public crate-pwds-0.1.0 (c (n "pwds") (v "0.1.0") (h "1bgwchj7x8i6jna3inn85kwnbg4v7gvibbjkcyg70h33zmvnpnrn")))

(define-public crate-pwds-0.2.0 (c (n "pwds") (v "0.2.0") (h "1mjgjlv8v1d3smld0d79n1zipn93f74hwax82bb1ih5xih5q9vg1")))

