(define-module (crates-io pw ne pwner) #:use-module (crates-io))

(define-public crate-pwner-0.1.0 (c (n "pwner") (v "0.1.0") (d (list (d (n "nix") (r "^0.17") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "io-util" "macros" "process" "rt-core" "sync" "time"))) (o #t) (d #t) (k 0)))) (h "1sd76iak0s1l0m3zpjrhfkq2hw20dfbz9dn2xvafkjmc1gbp3c8q") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-pwner-0.1.1 (c (n "pwner") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.17") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "process" "rt-core" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "macros"))) (d #t) (k 2)))) (h "12hmcck5d6kpbz3fyc0y04z3j6s4wpgqwg725gbk08py7gsyphya") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-pwner-0.1.2 (c (n "pwner") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.17") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "process" "rt-core" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "macros"))) (d #t) (k 2)))) (h "0lmj0hq3cqmixs0z4v8g7k5s2g0yyf1pwln6wn1iasib6dgpzcxx") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-pwner-0.1.3 (c (n "pwner") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.17") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "process" "rt-core" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "macros"))) (d #t) (k 2)))) (h "1mrkc7f53l9cjjywcn11ghqip4lf0qp62vvr8xil4zyn0hffa7dk") (f (quote (("default"))))))

(define-public crate-pwner-0.1.4 (c (n "pwner") (v "0.1.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "nix") (r "^0.19") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("io-util" "process" "rt" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("io-std" "macros"))) (d #t) (k 2)))) (h "09frfsp962rlw3y0a1cgfri2bck9jxnlmn0d66drkgazyh7fjljs") (f (quote (("default"))))))

(define-public crate-pwner-0.1.5 (c (n "pwner") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "nix") (r "^0.19") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("io-util" "process" "rt" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("io-std" "macros"))) (d #t) (k 2)))) (h "1255wiv6j17gyw7fxwzfp8vj26brbg5nvw2w88ndqj64zhl78g73") (f (quote (("default"))))))

(define-public crate-pwner-0.1.6 (c (n "pwner") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "nix") (r "^0.19") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "process" "rt" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "macros"))) (d #t) (k 2)))) (h "1w7722zcyxyxvvpxiwz6qqkm4sx8b4j3lcxjal6f9x9ragn7hl4b") (f (quote (("default")))) (y #t)))

(define-public crate-pwner-0.1.7 (c (n "pwner") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "process" "rt" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "macros"))) (d #t) (k 2)))) (h "0x4m2jxzfbjbvwmpsk9y195iyacaf07b6gqqnxbhkhzc2cza4vg3") (f (quote (("default"))))))

(define-public crate-pwner-0.1.8 (c (n "pwner") (v "0.1.8") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 2)) (d (n "nix") (r "^0.25.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("io-util" "process" "rt" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("io-std" "macros"))) (d #t) (k 2)))) (h "0ckb7crz9j04bmmnlwb7ky04iyziq7n1gxw5yl3g7l8rhqhl3gkw") (f (quote (("default"))))))

