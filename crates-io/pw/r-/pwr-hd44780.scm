(define-module (crates-io pw r- pwr-hd44780) #:use-module (crates-io))

(define-public crate-pwr-hd44780-0.0.1 (c (n "pwr-hd44780") (v "0.0.1") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (k 0)) (d (n "rppal") (r "^0.2") (d #t) (k 0)))) (h "03gmmidl0vk8hmiw16ikf2w5j1fbp346jxsza55arizgmdkvai7b") (y #t)))

(define-public crate-pwr-hd44780-0.0.2 (c (n "pwr-hd44780") (v "0.0.2") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (k 0)) (d (n "rppal") (r "^0.2") (d #t) (k 0)))) (h "0yappcgk3zxyyqkd0nqdqjn0k9pdd8jabprxprgqpnkmv68d5y8s")))

(define-public crate-pwr-hd44780-0.0.3 (c (n "pwr-hd44780") (v "0.0.3") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (k 0)) (d (n "rppal") (r "^0.2") (d #t) (k 0)))) (h "1mfsacsln40pmiqazqv702fv24s873if5hj08vnbz4l17f3l8v8m")))

(define-public crate-pwr-hd44780-0.0.4 (c (n "pwr-hd44780") (v "0.0.4") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (k 0)) (d (n "rppal") (r "^0.2") (d #t) (k 0)))) (h "1v5wf1bcd0wr38xyy1vn52ifl2a9kwpzgahyfkhc2qvq79g1nmcm")))

(define-public crate-pwr-hd44780-0.0.5 (c (n "pwr-hd44780") (v "0.0.5") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (k 0)) (d (n "rppal") (r "^0.2") (d #t) (k 0)))) (h "19sv092gyi9wnxbidxjmy1f08whhcq10w1gw15vg3qs4mp6fqfrm")))

(define-public crate-pwr-hd44780-0.1.0 (c (n "pwr-hd44780") (v "0.1.0") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (k 0)) (d (n "rppal") (r "^0.2") (d #t) (k 0)))) (h "1223pfn61gw1dixdh584ljk9lh1qzcwvmdagx0s9mhjvar4lcf5b")))

(define-public crate-pwr-hd44780-0.1.1 (c (n "pwr-hd44780") (v "0.1.1") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (k 0)) (d (n "rppal") (r "^0.2") (d #t) (k 0)))) (h "15zp562zaci8xkgypac4z3k3ymqrf2w4g63yl7xhr1gzfmf26x3k")))

(define-public crate-pwr-hd44780-0.1.2 (c (n "pwr-hd44780") (v "0.1.2") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (k 0)) (d (n "rppal") (r "^0.2") (d #t) (k 0)))) (h "0082kwh4v12yd8kcnizwhjkbpcp9aic37wk3wkz4qkdxa4mdd1wb")))

(define-public crate-pwr-hd44780-0.1.3 (c (n "pwr-hd44780") (v "0.1.3") (d (list (d (n "i2cdev") (r "^0.3") (d #t) (k 0)) (d (n "rppal") (r "^0.2") (d #t) (k 0)))) (h "1kzpzn379d6caq0l1s33vhagmlskxbyv36zpd978rg3vjilwzz79")))

