(define-module (crates-io pw fu pwfuzz-rs) #:use-module (crates-io))

(define-public crate-pwfuzz-rs-0.1.0 (c (n "pwfuzz-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01c0l3aw3z647hzp1gj7g2k1dc5922kvm4vi5p47dpiwidjws39x")))

(define-public crate-pwfuzz-rs-0.1.1 (c (n "pwfuzz-rs") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xh0w6jhn4v43arb045pna6bcsah8dyq9mv7xbp4w7sjc04shxay")))

(define-public crate-pwfuzz-rs-0.2.0 (c (n "pwfuzz-rs") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "06fjc5g89v0q87fv2v6gcm8ksq14b8v8pd5n4qnbj0l24dkn9rbc")))

