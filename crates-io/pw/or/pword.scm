(define-module (crates-io pw or pword) #:use-module (crates-io))

(define-public crate-pword-0.1.0 (c (n "pword") (v "0.1.0") (d (list (d (n "chbs") (r "^0.0.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rpassword") (r "^0.0.4") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.6.1") (d #t) (k 0)))) (h "08zdmy8zj4rlh4dkmrg9bl58mx485m35yr4ljmyy18fa0wc0x59k")))

(define-public crate-pword-0.1.1 (c (n "pword") (v "0.1.1") (d (list (d (n "chbs") (r "^0.0.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rpassword") (r "^0.0.4") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.6.1") (d #t) (k 0)))) (h "01ms3q156lpy2qg2sj3xqfap197hj23qiljdj4hr7fd2d3sfdwz3")))

(define-public crate-pword-0.1.2 (c (n "pword") (v "0.1.2") (d (list (d (n "chbs") (r "^0.0.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rpassword") (r "^0.0.4") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.6.1") (d #t) (k 0)))) (h "1aafvkzda42mgrhljl0wi6dk9z1slz03lr181j5nmgkmjyv14dg4")))

(define-public crate-pword-0.1.3 (c (n "pword") (v "0.1.3") (d (list (d (n "chbs") (r "^0.0.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rpassword") (r "^0.0.4") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.6.1") (d #t) (k 0)))) (h "1fmmjrq15jskfrpmzdh7i14hzybi5xcb1w4lm4lkq5xmb5ixzjif")))

