(define-module (crates-io pw df pwdf) #:use-module (crates-io))

(define-public crate-pwdf-0.1.0 (c (n "pwdf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "1h6npb3hrsx1zayfvkli21jy8d9j0gh118xj8nfgj761jmc9iyjl")))

(define-public crate-pwdf-0.1.1 (c (n "pwdf") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "1lp0291x37yyalwnimr5vid2xrhj2wipwwsab8y13m2aiwkl7dfr")))

