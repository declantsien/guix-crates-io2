(define-module (crates-io pw -v pw-volume) #:use-module (crates-io))

(define-public crate-pw-volume-0.4.0 (c (n "pw-volume") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.70") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "0f2ry45356lfp4ykiinzc1c3pfqp0j6ckmk731mp7mr9sh4nm4d5")))

