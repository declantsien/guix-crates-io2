(define-module (crates-io fm te fmterr) #:use-module (crates-io))

(define-public crate-fmterr-0.1.0 (c (n "fmterr") (v "0.1.0") (h "1qr44lp1rfmrgrn3hcsnw937iiqn3vcdkxf6b4an0k83ggqwiyla") (y #t)))

(define-public crate-fmterr-0.1.1 (c (n "fmterr") (v "0.1.1") (h "08j2y86pjmi8nhasg9f9ybk9lid6b32m99jmx7p2ar1jys6ly8ka")))

