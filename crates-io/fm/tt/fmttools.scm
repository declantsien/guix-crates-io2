(define-module (crates-io fm tt fmttools) #:use-module (crates-io))

(define-public crate-fmttools-0.1.0 (c (n "fmttools") (v "0.1.0") (h "06hm0acyng5pj70rhvz0xrwvcx9wcklp80lgmz3hbn271nkc8ry3")))

(define-public crate-fmttools-0.1.1 (c (n "fmttools") (v "0.1.1") (h "0rqcf5z91scx8xwjydwvrfdik9wg6labjrssnr16yfzxzvb770cr")))

(define-public crate-fmttools-0.1.2 (c (n "fmttools") (v "0.1.2") (h "0q3yi6hjr8r599sv1w0ia6d7dah6crrrac8ljh6rihrz4a0szvyq")))

(define-public crate-fmttools-0.2.0 (c (n "fmttools") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1r6gaavf2z0sgyxzg9g34pc3128d64bnsk6pbpg0vrgx12m1haa6")))

(define-public crate-fmttools-0.2.1 (c (n "fmttools") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "10hff736ghwcflc102z61zrhrfp6zx5hw25kijgrscp0007lkccp")))

(define-public crate-fmttools-0.2.2 (c (n "fmttools") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "117aw4zrdg5gdr8wnrv2d7hi8c4wcsnll9s99ydhn8baivfdi1mh")))

