(define-module (crates-io fm am fmamsg) #:use-module (crates-io))

(define-public crate-fmamsg-0.1.0 (c (n "fmamsg") (v "0.1.0") (h "1lv6db8m5wx5x9qa0v9laqbmrw55pfzmipdclm9gyqjn13ppvbbx") (y #t)))

(define-public crate-fmamsg-0.1.1 (c (n "fmamsg") (v "0.1.1") (h "0baia2m79g0lh5q551zfdjhbylk3k5y3gs2r23qvz4a2p1012s1f")))

