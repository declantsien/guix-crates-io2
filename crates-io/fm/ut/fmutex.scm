(define-module (crates-io fm ut fmutex) #:use-module (crates-io))

(define-public crate-fmutex-0.1.0 (c (n "fmutex") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)))) (h "0v08yf26la204yn38ydn2m6wm34frh77vl0cgdm140q60wblrs01")))

