(define-module (crates-io fm od fmodel-rust) #:use-module (crates-io))

(define-public crate-fmodel-rust-0.1.0 (c (n "fmodel-rust") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.49") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "tokio") (r "^1.0.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1g041zxwv2p128kkc22vik8vpk5504ixacdj8vg67rll7457jq5l")))

(define-public crate-fmodel-rust-0.2.0 (c (n "fmodel-rust") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "16v08z43dq46bwalh79n0xcdi2vbkqrirlgwdzxdnjqmkzp599qd")))

(define-public crate-fmodel-rust-0.3.0 (c (n "fmodel-rust") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1vnsi604wficf1wx2cvqf0i2ws735c2v78p4p8ckpbvl3hm5gs9m")))

(define-public crate-fmodel-rust-0.4.0 (c (n "fmodel-rust") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "00m8k7l8m67856kgkv4c03zf3mnrfh92sdd46kd15ns7ga0nnrlh")))

(define-public crate-fmodel-rust-0.5.0 (c (n "fmodel-rust") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "01988l6yqmvvc7gk8d54970s0j6y6l0s6pwd7f5vjmysc9775wbf")))

(define-public crate-fmodel-rust-0.6.0 (c (n "fmodel-rust") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1lxw7q9rg8al1hx1g84kazfab8576ngkmvp0k18fpfdpi26hyr08")))

(define-public crate-fmodel-rust-0.7.0 (c (n "fmodel-rust") (v "0.7.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0zxvrbm4qaymfyvfwjjy46114vqh19dr3gijwijsjc4885ixd0yj")))

