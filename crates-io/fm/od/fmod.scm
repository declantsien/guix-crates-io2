(define-module (crates-io fm od fmod) #:use-module (crates-io))

(define-public crate-fmod-0.9.5 (c (n "fmod") (v "0.9.5") (h "0jy6yrazwvwksijqzfkr0gs7zhkqvkwzh3m1pbh1417s17hk5zgi")))

(define-public crate-fmod-0.9.6 (c (n "fmod") (v "0.9.6") (h "0d6sgkr2b20ds88lc69v4lxzpf1k8gmdlhvprkz314q48v2kqrj2")))

(define-public crate-fmod-0.9.7 (c (n "fmod") (v "0.9.7") (h "0ihr1nl2wnrwywgv2vkcm5wvhp99qqimj5gavzcxad0xnfa4sja8")))

(define-public crate-fmod-0.9.8 (c (n "fmod") (v "0.9.8") (h "12mx5wnz2kk80fwlp6bdcj9n4agk2bj9v6dm49knmdw8ajadfk7q")))

(define-public crate-fmod-0.9.9 (c (n "fmod") (v "0.9.9") (h "0hfvh4vc7q2ybknllmzwn7gsr3qx9lycd9j1r6aggmbic9jnp52r")))

(define-public crate-fmod-0.9.10 (c (n "fmod") (v "0.9.10") (d (list (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "10dp7rn0hmjgpfjqsy3r66ki4fm2n3wdvfqwf3awzj5a4dg9zdv5")))

(define-public crate-fmod-0.9.11 (c (n "fmod") (v "0.9.11") (d (list (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "0x4aqljhw61siy63jyh99qb7sykjgx0kflzyf7a87c422rh6lzdj")))

(define-public crate-fmod-0.9.12 (c (n "fmod") (v "0.9.12") (d (list (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "12y8disr98qy51ckv31y7nfn3ksghamc2gbdnzhc4lxlgf76wasm")))

(define-public crate-fmod-0.9.13 (c (n "fmod") (v "0.9.13") (d (list (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "02apszxy1bdy39ciddk06a0qpiwkj9zbczk9anj8099n52s764rv")))

(define-public crate-fmod-0.9.14 (c (n "fmod") (v "0.9.14") (d (list (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "03p7wq1y97zq83kw0nx9n5nn1szmvnrmrryv5kj5zf4rdkyyv098")))

(define-public crate-fmod-0.9.15 (c (n "fmod") (v "0.9.15") (d (list (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "10vcsdxn6g5rm3i8b5a26r57lxg6vhkbkp1d0w7rb3i914lklfi2")))

(define-public crate-fmod-0.9.16 (c (n "fmod") (v "0.9.16") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "1nwpqpqi5id6prlz7nrgjv4ny9cqv860pv21rrzvbbi4awp9rnkd")))

(define-public crate-fmod-0.9.17 (c (n "fmod") (v "0.9.17") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "1nqj20d2zx77p8xzpdqjzxrdwwxig6d3v2c7sbfcldb7qhwlakpm")))

(define-public crate-fmod-0.9.18 (c (n "fmod") (v "0.9.18") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "0dvny7rjf84yh75ipggp3v42j8s1730abrqjfylk9kjfl3flj777")))

(define-public crate-fmod-0.9.19 (c (n "fmod") (v "0.9.19") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "c_vec") (r "~1.0") (d #t) (k 0)))) (h "1njc7m2cwwbvjnf05dsxh166bqjzg307pfpn2bhbqcvcn6064kj3")))

(define-public crate-fmod-0.9.20 (c (n "fmod") (v "0.9.20") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0vnb8jx6bb8ki47c60c6kch2rd8kq4jcwd4kcw4makpczm98b7yp")))

(define-public crate-fmod-0.9.21 (c (n "fmod") (v "0.9.21") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0p9cmrdsl79ail2i919flycbd9879cfk8ga5ii2rpm509mb5hl3s")))

(define-public crate-fmod-0.9.22 (c (n "fmod") (v "0.9.22") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "1bp0rv2sgaa2109bsdp033krxs7a19hw15msfzpibcn7pzfynhd0")))

(define-public crate-fmod-0.9.24 (c (n "fmod") (v "0.9.24") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0dvyv0r3flz12izkpyjzniap0jfv4gmn8z776rah2xs7cbbfylig")))

(define-public crate-fmod-0.10.0 (c (n "fmod") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "018cm2y8pww9dswqrkkmn6bw61xp2g5xhqrqcmkf66mcrac80n02")))

(define-public crate-fmod-0.10.1 (c (n "fmod") (v "0.10.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "08jchj889ydxgi0fiamq0pxfbk9w2d6pf69cfcrdfkyvl36arizv")))

(define-public crate-fmod-0.10.2 (c (n "fmod") (v "0.10.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "07gz8cvad45bdcxsmsx4rs0jgx2zvb3qxhmlk3ig46wp6xjnyy6g")))

