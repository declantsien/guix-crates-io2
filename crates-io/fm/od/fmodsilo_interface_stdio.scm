(define-module (crates-io fm od fmodsilo_interface_stdio) #:use-module (crates-io))

(define-public crate-fmodsilo_interface_stdio-0.1.0 (c (n "fmodsilo_interface_stdio") (v "0.1.0") (d (list (d (n "fmodsilo_server") (r "^0.1.0") (d #t) (k 0)))) (h "1ka1f33z66mp2z6msy0ni7hd5lcp9ywxz2qxzdkgg1qg4jxxfhx7")))

(define-public crate-fmodsilo_interface_stdio-0.1.1 (c (n "fmodsilo_interface_stdio") (v "0.1.1") (d (list (d (n "fmodsilo_server") (r "^0.1.1") (d #t) (k 0)))) (h "1sqsxzbasd1walvaa6hdqpx5b61fmj6z2qkx7xpr9ya2j9jyj18b")))

