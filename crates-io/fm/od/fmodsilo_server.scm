(define-module (crates-io fm od fmodsilo_server) #:use-module (crates-io))

(define-public crate-fmodsilo_server-0.1.0 (c (n "fmodsilo_server") (v "0.1.0") (d (list (d (n "lsp_json") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "09iyzky266nzf4fh7gg6am2yrd8jakxnrhkbnj7msh9bhskdcqxk")))

(define-public crate-fmodsilo_server-0.1.1 (c (n "fmodsilo_server") (v "0.1.1") (d (list (d (n "lsp_json") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "079hh3cv54kflhfw51i5q6a4bdyz0r4xzmx86fkgr5yc39adgzmc")))

