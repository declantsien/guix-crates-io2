(define-module (crates-io fm od fmod-sys) #:use-module (crates-io))

(define-public crate-fmod-sys-0.1.0 (c (n "fmod-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "0xm6lqyvm1qqi52rfymqcxh4vs6lrrfxr565l3jga227faaswpq1") (f (quote (("studio") ("default")))) (y #t)))

(define-public crate-fmod-sys-0.1.1 (c (n "fmod-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "0a2hb8n836pfnyxi784nm8r5mxwsxzzikbaacy7g3h7ibzpr97kv") (f (quote (("studio") ("default")))) (y #t)))

