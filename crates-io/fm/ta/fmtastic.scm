(define-module (crates-io fm ta fmtastic) #:use-module (crates-io))

(define-public crate-fmtastic-0.1.0 (c (n "fmtastic") (v "0.1.0") (h "1p58ygw8gpk1agnfbr5rkc32lp7misn58xm3n27y4pyhsn7piq01")))

(define-public crate-fmtastic-0.2.0 (c (n "fmtastic") (v "0.2.0") (h "018vhry0qnlpsc37chb6y0wz3q5cz7qfi8n2wxwkw8cvp1p7r6ny") (r "1.75.0")))

