(define-module (crates-io fm tp fmtparse) #:use-module (crates-io))

(define-public crate-fmtparse-0.1.0 (c (n "fmtparse") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.1") (d #t) (k 2)) (d (n "chumsky") (r "^0.9") (d #t) (k 0)))) (h "1jk8ypaw9ivjkbic644ad6010711ky1zs19gzv1yb12ialqajigm")))

(define-public crate-fmtparse-0.2.0 (c (n "fmtparse") (v "0.2.0") (d (list (d (n "ariadne") (r "^0.1") (d #t) (k 2)) (d (n "chumsky") (r "^0.9") (d #t) (k 0)))) (h "191byvw66wi48piz5j171rx983apidzd2g90p54g1zz1nvsj6y9v")))

