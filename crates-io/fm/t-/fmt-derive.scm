(define-module (crates-io fm t- fmt-derive) #:use-module (crates-io))

(define-public crate-fmt-derive-0.0.1 (c (n "fmt-derive") (v "0.0.1") (d (list (d (n "fmt-derive-proc") (r "=0.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1fladla3nm2c1lpaq4q0bpaiyqxz1wjj6j5g02ml5jd60ar330bn") (f (quote (("std") ("default" "std"))))))

(define-public crate-fmt-derive-0.0.2 (c (n "fmt-derive") (v "0.0.2") (d (list (d (n "fmt-derive-proc") (r "=0.0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1gwj7a1hr94pg46wzhvg99g3y03xajkhhlr10whvfpgn85dbgl30") (r "1.56.0")))

(define-public crate-fmt-derive-0.0.3 (c (n "fmt-derive") (v "0.0.3") (d (list (d (n "fmt-derive-proc") (r "=0.0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0r76sspdy50mmghgxk0njiqkv05b19arw9h7dpsxgdn1irm2p72s") (r "1.56.0")))

(define-public crate-fmt-derive-0.0.4 (c (n "fmt-derive") (v "0.0.4") (d (list (d (n "fmt-derive-proc") (r "=0.0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1rn93wh48n6h4kvbhhy8q018s33fbg7jdfyaxaq0n216vk00k389") (r "1.56.0")))

(define-public crate-fmt-derive-0.0.5 (c (n "fmt-derive") (v "0.0.5") (d (list (d (n "fmt-derive-proc") (r "=0.0.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1yn8x3zggrs4i4jzdwz58dahjrj8licfp7l6idhsj7f4nb7cddy5") (r "1.56.0")))

(define-public crate-fmt-derive-0.0.6 (c (n "fmt-derive") (v "0.0.6") (d (list (d (n "fmt-derive-proc") (r "=0.0.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1hajlvmhmgf1hwl15zg9l972kb5y600ny1nli9xw38nbrsh24prz") (r "1.60.0")))

(define-public crate-fmt-derive-0.1.0 (c (n "fmt-derive") (v "0.1.0") (d (list (d (n "fmt-derive-proc") (r "=0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "01r9yd6sz6alhb0cky4ca7isd0bz4d60nnzvp2wybsm2yiyrdnsh") (r "1.64.0")))

(define-public crate-fmt-derive-0.1.1 (c (n "fmt-derive") (v "0.1.1") (d (list (d (n "fmt-derive-proc") (r "=0.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "11fr1434qbxnq2y2grrcrsnly1k98s3rvwvvyk422ldvxxs7jmfi") (r "1.66.0")))

(define-public crate-fmt-derive-0.1.2 (c (n "fmt-derive") (v "0.1.2") (d (list (d (n "fmt-derive-proc") (r "=0.1.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0s81869syvx3p767ncqlr46gclcfjw9dnqmlz22mhi96bjsa9d0w") (r "1.69.0")))

