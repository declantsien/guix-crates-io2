(define-module (crates-io fm t- fmt-extra) #:use-module (crates-io))

(define-public crate-fmt-extra-0.0.1 (c (n "fmt-extra") (v "0.0.1") (h "0iq0rhi14zp1i9icrkdzdy6wbijcrkgnhl61vmllrxb6nxqjggf7")))

(define-public crate-fmt-extra-0.0.2 (c (n "fmt-extra") (v "0.0.2") (h "08gvk73ayl6hv64invis2ys8gzxz0b83h93j0z5cvq60w14smv0k")))

(define-public crate-fmt-extra-0.0.3 (c (n "fmt-extra") (v "0.0.3") (h "0jrvxpc9n5k94b56jwa13q0v72rw8zv4wdybxzwwfdwfvs7imw9n")))

(define-public crate-fmt-extra-0.1.0 (c (n "fmt-extra") (v "0.1.0") (h "0pd7j37zax6x8h657by9gkfrr9h9iqjp1bdvf3za3bsl4pw52xqs")))

(define-public crate-fmt-extra-0.1.1 (c (n "fmt-extra") (v "0.1.1") (h "1hm7hfm3627jcihn4niv4ym1kyk2sv26hgn7027ikcl5gmb6jvfl")))

(define-public crate-fmt-extra-0.1.2 (c (n "fmt-extra") (v "0.1.2") (h "1xmcrb9pf9d8kfg7rd3s5zc795bnq89khaav0dzrk21jqdkpbw5w")))

(define-public crate-fmt-extra-0.2.0 (c (n "fmt-extra") (v "0.2.0") (h "13f82r913z3viwvphwcz0qr7ragcyn5a9q0dvgasmdwm6fpf301p")))

(define-public crate-fmt-extra-0.2.1 (c (n "fmt-extra") (v "0.2.1") (h "1fgj6zp3wc2p23z9kp3ilhhkn730j2fqkcgv8w087gprn5qizw87")))

