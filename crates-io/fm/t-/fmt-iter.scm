(define-module (crates-io fm t- fmt-iter) #:use-module (crates-io))

(define-public crate-fmt-iter-0.0.0 (c (n "fmt-iter") (v "0.0.0") (d (list (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "pipe-trait") (r "^0.3.2") (d #t) (k 0)))) (h "07dfpiv9vkp2fmwkcr4ndmdji1k1ai3ycmrpi01y8x7ds0z77jxn")))

(define-public crate-fmt-iter-0.1.0 (c (n "fmt-iter") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "pipe-trait") (r "^0.3.2") (d #t) (k 0)))) (h "0ddxkm8c7759v9n7cqqby661idw9vvlbrrckh3i1nkfxwgg9saj5")))

(define-public crate-fmt-iter-0.2.0 (c (n "fmt-iter") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "pipe-trait") (r "^0.3.2") (d #t) (k 0)))) (h "08vg0857nlmlmfzqcr3k7q8nzg0qjiwli39vblff445nf4z0y09g")))

(define-public crate-fmt-iter-0.2.1 (c (n "fmt-iter") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "pipe-trait") (r "^0.3.2") (d #t) (k 2)))) (h "0456zjq0p10jw1vg5dgqd3bvmvyx5451s3w3v2270739fsfjiffh")))

