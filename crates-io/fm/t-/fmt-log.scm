(define-module (crates-io fm t- fmt-log) #:use-module (crates-io))

(define-public crate-fmt-log-0.1.0 (c (n "fmt-log") (v "0.1.0") (h "0h4i0fw4m69z2qqw9b9adjijr52l1xisqn31fplsp5p2l2wvmwyr")))

(define-public crate-fmt-log-0.1.1 (c (n "fmt-log") (v "0.1.1") (h "189z8kvciz1gsypwfqx535ccbwy2v1vbwvzc9771iyazj11xg77d")))

