(define-module (crates-io fm t- fmt-derive-proc) #:use-module (crates-io))

(define-public crate-fmt-derive-proc-0.0.1 (c (n "fmt-derive-proc") (v "0.0.1") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1w4y2dqq7vq52j5vb3zdk5qfnpyii6hf7v3g7ffyhs3bygfs5ccy")))

(define-public crate-fmt-derive-proc-0.0.2 (c (n "fmt-derive-proc") (v "0.0.2") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1nv7jbj65w20pjg1vqpgzlp6hivhpjz577gf795770967gw67rck") (r "1.56.0")))

(define-public crate-fmt-derive-proc-0.0.3 (c (n "fmt-derive-proc") (v "0.0.3") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "042v3fvdpisgiq78ngsnlzxa39riscn9hh4k6l7cwpc122l27hg7") (r "1.56.0")))

(define-public crate-fmt-derive-proc-0.0.4 (c (n "fmt-derive-proc") (v "0.0.4") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "144s5xz08nhgps42y9frg4x765wqkax4f06brbp1pb3d44pqc4la") (r "1.56.0")))

(define-public crate-fmt-derive-proc-0.0.5 (c (n "fmt-derive-proc") (v "0.0.5") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0c1h9rgkiq2l6mhgfl8gs7am7irbpf54bhsa7f7xqdmjp699zls8") (r "1.56.0")))

(define-public crate-fmt-derive-proc-0.0.6 (c (n "fmt-derive-proc") (v "0.0.6") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0vl925wvp6bgs16xvgic2gbbhbq806r12gdvisj7p7j71cl3iim5") (r "1.60.0")))

(define-public crate-fmt-derive-proc-0.1.0 (c (n "fmt-derive-proc") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1fa93f7yq63sbpm1s6ifqd09jg75l8kbx2a5d3wjdzyyqdy5m82s") (r "1.64.0")))

(define-public crate-fmt-derive-proc-0.1.1 (c (n "fmt-derive-proc") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0s44x8qr392knad0fp7szdq9n2xwqy578wns7yd53a7s4x1arjlx") (r "1.66.0")))

(define-public crate-fmt-derive-proc-0.1.2 (c (n "fmt-derive-proc") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "08qd72rl5l7vcflw7wm8qmfyhsvwx4hmpwakhz748hvkc03xl13p") (r "1.69.0")))

