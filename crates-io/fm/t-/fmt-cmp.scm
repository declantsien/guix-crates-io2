(define-module (crates-io fm t- fmt-cmp) #:use-module (crates-io))

(define-public crate-fmt-cmp-0.1.0 (c (n "fmt-cmp") (v "0.1.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 2)))) (h "0zbi35bzzdsz7cha67jrqkc29l1n3i78qb482fv3wjln6ixkblw6") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.41")))

(define-public crate-fmt-cmp-0.1.1 (c (n "fmt-cmp") (v "0.1.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 2)))) (h "05szgmy8p0gfsankyvm6n338pva85j57sm1ja8wv7kh7y7hvbf46") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.41")))

