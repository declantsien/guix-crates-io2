(define-module (crates-io fm #{-i}# fm-index) #:use-module (crates-io))

(define-public crate-fm-index-0.1.0 (c (n "fm-index") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fid") (r "^0.1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08np0svswjpxp37gnvzgqlgjqd78p85s1jlfhnf47mgwhbxrl0mw")))

(define-public crate-fm-index-0.1.1 (c (n "fm-index") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fid") (r "^0.1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c85nd02ainplgl5z295ilzpnlz27q47mhkxpj62c0m7zgp2w9nx")))

(define-public crate-fm-index-0.1.2 (c (n "fm-index") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fid") (r "^0.1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09xrdvyi48nj27mixc8rfiqsp0crfavivjkgwb2awr1k0j23zz5r")))

