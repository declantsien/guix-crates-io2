(define-module (crates-io fm t_ fmt_compare_nostd) #:use-module (crates-io))

(define-public crate-fmt_compare_nostd-0.1.0 (c (n "fmt_compare_nostd") (v "0.1.0") (h "0qbxqbsm26x3szf68w38gfnb1ar7vwf4d0i2kvm8pa7l61lrb9z2")))

(define-public crate-fmt_compare_nostd-0.1.1 (c (n "fmt_compare_nostd") (v "0.1.1") (h "1jyxj4sp8rpp5yb3m7651vpwalwx8f868685596xannc6jh42dvx")))

