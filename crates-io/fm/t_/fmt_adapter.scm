(define-module (crates-io fm t_ fmt_adapter) #:use-module (crates-io))

(define-public crate-fmt_adapter-0.1.0 (c (n "fmt_adapter") (v "0.1.0") (h "1wjbckqnw0rygxqkwcinxnn70vm2x1z46yg6cam3hd6mxapf289m") (y #t)))

(define-public crate-fmt_adapter-0.2.0 (c (n "fmt_adapter") (v "0.2.0") (h "02rhzw7s8xx9nyb9r1cbdjqfh1vzfhpxnrv59m01rp3mhga9dyld") (y #t)))

(define-public crate-fmt_adapter-0.2.1 (c (n "fmt_adapter") (v "0.2.1") (h "0hqixin703fy9d98bvri95k1mf0przi30641gk7wgg7giisxmw4v")))

