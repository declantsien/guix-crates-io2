(define-module (crates-io fm t_ fmt_ext) #:use-module (crates-io))

(define-public crate-fmt_ext-0.1.0 (c (n "fmt_ext") (v "0.1.0") (h "1vf0ac0mg814alsan477i685b0c3ixpps5326khpklmbzrsmm5af")))

(define-public crate-fmt_ext-0.1.1 (c (n "fmt_ext") (v "0.1.1") (h "1jmvz7dipc9jqid71ivikx9b9s321qcf1bs67l3pnw6hr7dx9yqf")))

(define-public crate-fmt_ext-0.1.2 (c (n "fmt_ext") (v "0.1.2") (h "1adsisz51s1ff7g5q4l9adidvjrp6sgmz01qlx7skkzg3g86npnm")))

