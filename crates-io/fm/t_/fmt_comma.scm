(define-module (crates-io fm t_ fmt_comma) #:use-module (crates-io))

(define-public crate-fmt_comma-0.1.0 (c (n "fmt_comma") (v "0.1.0") (h "1g4amg0v3ww0i8ghw8g1n3r3dy21crfhq54794bcr152c8q0li9x")))

(define-public crate-fmt_comma-0.1.1 (c (n "fmt_comma") (v "0.1.1") (h "13hf6h82ki4qy1jfwcw08p4c4971vazzdg7wc7avgb6psqi7why3")))

(define-public crate-fmt_comma-0.1.3 (c (n "fmt_comma") (v "0.1.3") (h "10inhwgnqxvaa2lxhvybla402dgal8n1i1v1yghz0w1d8l07f8ap")))

(define-public crate-fmt_comma-0.1.4 (c (n "fmt_comma") (v "0.1.4") (h "054idggy0a5b2rwb4brdfwq5z9m7ijx6qm0lq2bh0rc0kyrcnwcw")))

