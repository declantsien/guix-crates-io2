(define-module (crates-io fm tb fmtbuf) #:use-module (crates-io))

(define-public crate-fmtbuf-0.1.0 (c (n "fmtbuf") (v "0.1.0") (h "106j1jwgih5vnasvv5hbm28ch2mzm2ihk8ld83w7n1pizrnwf6fq") (f (quote (("std") ("default" "std"))))))

(define-public crate-fmtbuf-0.1.1 (c (n "fmtbuf") (v "0.1.1") (h "018dwrr6ill42v0gfjm0nr55c1fnhbwkfai46vijqg0sxjxyf2kk") (f (quote (("std") ("default" "std"))))))

(define-public crate-fmtbuf-0.1.2 (c (n "fmtbuf") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0xx9q8m0rib8gzrb0d797w11d4vj0y2mp6k4rcpk18yn55sq7p99") (f (quote (("std") ("default" "std")))) (r "1.31")))

