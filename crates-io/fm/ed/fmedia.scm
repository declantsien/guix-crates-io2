(define-module (crates-io fm ed fmedia) #:use-module (crates-io))

(define-public crate-fmedia-0.1.0 (c (n "fmedia") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1.1") (d #t) (k 0)))) (h "1xzv22lac7m6pk7lpcq5dfccma0hm9r2r8sjlizqzcdqg1rll8xk")))

(define-public crate-fmedia-0.2.0 (c (n "fmedia") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "12v1v6bnidqrkvqys8kjbbiy9yyd4q42k21vkw44lr18z2v064f1")))

