(define-module (crates-io fm -c fm-cli) #:use-module (crates-io))

(define-public crate-fm-cli-0.1.0 (c (n "fm-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0wq34d23y69v5rlmw8gn7p3cij6kwsa4xjh3i2al28p0cygp6vrb")))

(define-public crate-fm-cli-0.1.1 (c (n "fm-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1sc3izhc2vwxqbyssgzdd4a3wlv8wb5r2a4xhrriir6i54pcf09p")))

