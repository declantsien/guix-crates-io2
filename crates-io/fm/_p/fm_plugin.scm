(define-module (crates-io fm _p fm_plugin) #:use-module (crates-io))

(define-public crate-fm_plugin-0.1.0 (c (n "fm_plugin") (v "0.1.0") (d (list (d (n "directories") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)) (d (n "widestring") (r "^0") (d #t) (k 0)))) (h "1r535clpbj8nfbikhsn7gd4bs534qyf4i5p24d549grr8vfcb47d") (y #t)))

(define-public crate-fm_plugin-0.1.1 (c (n "fm_plugin") (v "0.1.1") (d (list (d (n "directories") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)) (d (n "widestring") (r "^0") (d #t) (k 0)))) (h "1dhcz7lk7zyv2x2rcfkybvazvjq4yivx5namjb76lkfz61zz5ig7")))

(define-public crate-fm_plugin-0.1.2 (c (n "fm_plugin") (v "0.1.2") (d (list (d (n "directories") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)) (d (n "widestring") (r "^0") (d #t) (k 0)))) (h "0jzx3vkpq5dvjppyb1v3ajlk6vby7zzas6bvb40f5rfb1p5brgby")))

(define-public crate-fm_plugin-0.1.3 (c (n "fm_plugin") (v "0.1.3") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1hp06h41jh36hl1r1bdxwrisd8dcyn44vmigr3grj84gfqm36ws9")))

(define-public crate-fm_plugin-0.1.4 (c (n "fm_plugin") (v "0.1.4") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1zx41skqa56lirlhkangakwmgjnq6qm811qd66bxy13whp2scvk3")))

(define-public crate-fm_plugin-0.1.5 (c (n "fm_plugin") (v "0.1.5") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "13g1ghzi647p2d0knhyn5v5qbilfkh65dcsl3h6mr1gp1m49hw6z")))

(define-public crate-fm_plugin-0.1.7 (c (n "fm_plugin") (v "0.1.7") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1vk95q714ilws088kqzykfh62mglwk077a7rb2vrv7nc72361dx1")))

(define-public crate-fm_plugin-0.1.8 (c (n "fm_plugin") (v "0.1.8") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "03cp3w070s348j89ps836avnciqmv6yjnjcp8jj24fc1m26r5vfg")))

(define-public crate-fm_plugin-0.1.9 (c (n "fm_plugin") (v "0.1.9") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0jmg8zd8dlfkxkars333wg6dmqbpcajgy0vxiv8yqxa7zjcdgwx7")))

(define-public crate-fm_plugin-0.1.10 (c (n "fm_plugin") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1lgnsmga9lhclsfffmd0rc2s5gy2194gm98c15cxw1cp9g4jjrgl")))

(define-public crate-fm_plugin-0.1.11 (c (n "fm_plugin") (v "0.1.11") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0rq6hbr68y06s2yy6177axjwf2qsg2kpbmmphrc13h139kccxkvq")))

(define-public crate-fm_plugin-0.1.12 (c (n "fm_plugin") (v "0.1.12") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "087hv92q0rmcknv19k8s4xfpxznx3rjwg5p5m64hn22glqvc5r9l")))

(define-public crate-fm_plugin-0.1.13 (c (n "fm_plugin") (v "0.1.13") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0gwk82lkqsn4prn3xv4chlbd2s8lixgp30r2xa1gsdx8qfkm7ng5")))

(define-public crate-fm_plugin-0.1.14 (c (n "fm_plugin") (v "0.1.14") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1irs8gqaa67lfz0y26a5nswk7rvnh7agxbnp1bazx3yqsh0mjqbn")))

(define-public crate-fm_plugin-0.1.15 (c (n "fm_plugin") (v "0.1.15") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "195gzsx5j2mb0vqnvvcj0g44ar2jlsjnyk4is6csb85f4na5s7w7")))

(define-public crate-fm_plugin-0.1.16 (c (n "fm_plugin") (v "0.1.16") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1p2n7zazwdprrvrgi2zl9krpzyrbasypqzkyphpgb21k776h05iw") (f (quote (("ffi") ("default" "ffi"))))))

(define-public crate-fm_plugin-0.1.17 (c (n "fm_plugin") (v "0.1.17") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "11zhlkrqyv2scgy5f3d5p15627s0iahs9jl9yzax7iarqqqnxw60") (f (quote (("ffi") ("default" "ffi"))))))

