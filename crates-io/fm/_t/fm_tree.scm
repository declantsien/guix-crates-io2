(define-module (crates-io fm _t fm_tree) #:use-module (crates-io))

(define-public crate-fm_tree-0.1.0 (c (n "fm_tree") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "1yia8azai2lbv595wsz49cf4pjdvsin556i4aiwzvh9yszasz092")))

