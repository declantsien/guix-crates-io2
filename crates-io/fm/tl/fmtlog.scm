(define-module (crates-io fm tl fmtlog) #:use-module (crates-io))

(define-public crate-fmtlog-0.1.0 (c (n "fmtlog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "16gqgbzqmnh2lss4060psyzdm3ps88z88n8mn9mc6pk15dx4s9hc")))

(define-public crate-fmtlog-0.1.1 (c (n "fmtlog") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "1dqw3s1qqjwgfsgbh8gvijmcmg8w9g8r4qimsq89vnqjqa7amp2q") (y #t)))

(define-public crate-fmtlog-0.1.2 (c (n "fmtlog") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "1xk06mfn9617zq1srafmffydywib88y9fnlpvjpjd01f1zpnwfz9")))

(define-public crate-fmtlog-0.1.3 (c (n "fmtlog") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "1n90hkq4pihxj6phjvnnl6g6svzjc732x18njxbyxrp8jdgrshjm") (f (quote (("default" "colored" "chrono")))) (y #t)))

(define-public crate-fmtlog-0.1.4 (c (n "fmtlog") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "0620mwpx2csf7lzl8np4aakjss1pabfp5v19nhzn8axacsnz470w") (f (quote (("default" "colored" "chrono"))))))

