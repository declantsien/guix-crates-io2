(define-module (crates-io fm i- fmi-test-data) #:use-module (crates-io))

(define-public crate-fmi-test-data-0.1.0 (c (n "fmi-test-data") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fetch-data") (r "^0.1") (d #t) (k 0)) (d (n "fmi") (r "^0.4.0") (f (quote ("fmi2" "fmi3"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "1szi19x847caxabbkb0q5m6vn84sd8ay37hv1bmh0cdn9w0qs72n")))

