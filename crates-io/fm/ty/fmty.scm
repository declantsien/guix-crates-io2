(define-module (crates-io fm ty fmty) #:use-module (crates-io))

(define-public crate-fmty-0.1.0 (c (n "fmty") (v "0.1.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "test-strategy") (r "^0.3") (d #t) (k 2)))) (h "1g7w03c8jhmrr6bjfn70pqgahxs093b2wzkfmhk5wr85pv3h48dh") (r "1.56.0")))

(define-public crate-fmty-0.1.1 (c (n "fmty") (v "0.1.1") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "test-strategy") (r "^0.3") (d #t) (k 2)))) (h "0s3h0bjvf2xsp5qxz44w7zak5a2qpq8xga6jz1iz4vjp8dvfaiqp") (r "1.56.0")))

