(define-module (crates-io fm to fmtor) #:use-module (crates-io))

(define-public crate-fmtor-0.1.0 (c (n "fmtor") (v "0.1.0") (h "1jagdnq6k97kmd5krqrzkafpydpcr23l6i9w0pd3970jiz0zhxdh")))

(define-public crate-fmtor-0.1.1 (c (n "fmtor") (v "0.1.1") (h "1fdlh4ai5gnhqz8rbx0xd5gb7z96r858d0grixkkq4nckahjpwxv")))

(define-public crate-fmtor-0.1.2 (c (n "fmtor") (v "0.1.2") (h "1lnh1a7wx7c6p6q2gdhhswn5dssnjxb161k47w7hp2f4s1ilzsj7")))

