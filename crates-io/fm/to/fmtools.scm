(define-module (crates-io fm to fmtools) #:use-module (crates-io))

(define-public crate-fmtools-0.1.0 (c (n "fmtools") (v "0.1.0") (d (list (d (n "obfstr") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1y7lsbbcjbfx52b2ld1d27h7yj63cqwk1hliipccyhf2gnvgjazp") (f (quote (("std") ("default" "std"))))))

(define-public crate-fmtools-0.1.1 (c (n "fmtools") (v "0.1.1") (d (list (d (n "obfstr") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1impsx1zd54cj02vw1qv5iskx0vjgb9wxb6wi8vwlmnfxgxlbr06") (f (quote (("std") ("default" "std"))))))

(define-public crate-fmtools-0.1.2 (c (n "fmtools") (v "0.1.2") (d (list (d (n "obfstr") (r "^0.4") (o #t) (d #t) (k 0)))) (h "06hx6lx24gpw63hlw6crjqx7mqrwzz70i6fnf2wzcizc9rnyv0zr") (f (quote (("std") ("default" "std"))))))

