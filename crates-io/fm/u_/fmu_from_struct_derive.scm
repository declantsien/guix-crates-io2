(define-module (crates-io fm u_ fmu_from_struct_derive) #:use-module (crates-io))

(define-public crate-fmu_from_struct_derive-0.1.0 (c (n "fmu_from_struct_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vspbr61mv6m77x4xxi61xipzxans1yxc5snzn8w62b3qxlfgh8q")))

(define-public crate-fmu_from_struct_derive-0.1.1 (c (n "fmu_from_struct_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jjax86r5ws7bp853fig4654i8hfr9knjfzrlx01vmbbjhsjxd5q")))

