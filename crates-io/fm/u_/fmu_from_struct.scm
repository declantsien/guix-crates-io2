(define-module (crates-io fm u_ fmu_from_struct) #:use-module (crates-io))

(define-public crate-fmu_from_struct-0.1.0 (c (n "fmu_from_struct") (v "0.1.0") (d (list (d (n "fmu_from_struct_derive") (r "^0.1.0") (d #t) (k 0)))) (h "16aywxgfa18waxakv8i6dk64vlcpgq8fp4rgk2c7rhfy77v0kwvw")))

(define-public crate-fmu_from_struct-0.1.1 (c (n "fmu_from_struct") (v "0.1.1") (d (list (d (n "fmu_from_struct_derive") (r "^0.1.1") (d #t) (k 0)))) (h "07j74v2cws3j8i9s1mkz151z5j716b5k461xk1ydb0mqq5dwyl64")))

