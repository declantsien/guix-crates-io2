(define-module (crates-io cv -c cv-contour) #:use-module (crates-io))

(define-public crate-cv-contour-0.2.0 (c (n "cv-contour") (v "0.2.0") (d (list (d (n "opencv") (r "^0.78.2") (f (quote ("highgui" "videoio" "video" "photo" "imgproc" "imgcodecs" "ml" "objdetect" "stitching" "calib3d" "features2d" "flann" "dnn"))) (k 0)) (d (n "shape-contour") (r "^0.2") (d #t) (k 0)) (d (n "shapelib-rs") (r "^0.2") (d #t) (k 0)))) (h "15ar48f6b5i4aac3n9ayadj2lngfmdlirn9nw6vf0b5ymxmys7rz")))

