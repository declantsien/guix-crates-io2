(define-module (crates-io cv -c cv-core) #:use-module (crates-io))

(define-public crate-cv-core-0.1.0 (c (n "cv-core") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "1djyq4g4143s8q0df9ybrcgw315n0f9chixscyx3z9wxmwy6crc6")))

(define-public crate-cv-core-0.1.1 (c (n "cv-core") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0naf8khb2aldyfcaaamppldmxlgk6j98wyxni6qhpnqjianlb385")))

(define-public crate-cv-core-0.1.2 (c (n "cv-core") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "1kpdk29gr7kwsfyhx80c1zsihy9jrxwk8vf3d12pw4wvk0wlh8pr")))

(define-public crate-cv-core-0.2.0 (c (n "cv-core") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "01633kwr46hr6s85kf8dw3f3d17f9gnhwdlj2dk40ndkn2llg3js")))

(define-public crate-cv-core-0.2.1 (c (n "cv-core") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0a5vzj24agl80lpw33022zxqd1r8iz0l0fqbx6pzil83dhr1ss7l")))

(define-public crate-cv-core-0.2.2 (c (n "cv-core") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0dxk3j4vizhcraspspl9gyx83mdkbjqcil29vmbiys99mvraxq6i")))

(define-public crate-cv-core-0.3.0 (c (n "cv-core") (v "0.3.0") (d (list (d (n "approx") (r "^0.3.2") (k 0)) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0gyj9y1mfv3ja7gxbkhv4i5xhiq0ckbkpviyf3vf1n1pmrgc7wk5")))

(define-public crate-cv-core-0.4.0 (c (n "cv-core") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0p6ay4v0n30s2wn8wdya5dzyxwz592kaqq36kgw5s06siabl3wdh")))

(define-public crate-cv-core-0.5.0 (c (n "cv-core") (v "0.5.0") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0b87447nicaywbkngds1ngin9zzpvvi2xzldrcpl2gprz1xah2zv")))

(define-public crate-cv-core-0.6.0 (c (n "cv-core") (v "0.6.0") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0kg8y5jkzb4yc7nbhxjg91g3q72268vzb1py2nhkmr36wzz6mh0z")))

(define-public crate-cv-core-0.6.1 (c (n "cv-core") (v "0.6.1") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0fn7sbrb2mzd73bms33qlw5b11ijn23g5phdpc08j1lgh10kv0zy")))

(define-public crate-cv-core-0.7.1 (c (n "cv-core") (v "0.7.1") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "07ij4svkb1sxyk2186cahvj2h04p5vklq86j1a8030b3fpvxshks")))

(define-public crate-cv-core-0.7.2 (c (n "cv-core") (v "0.7.2") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0p25m9d5zlhnnrf3v70snywal59zdw1prsya6a5ww61jl6w7p285")))

(define-public crate-cv-core-0.8.0 (c (n "cv-core") (v "0.8.0") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "1cnfvxgd6si85damwh593dlvkgr2nicvz65blddsw41684c1bnjr") (f (quote (("pinhole") ("default"))))))

(define-public crate-cv-core-0.9.0 (c (n "cv-core") (v "0.9.0") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.20.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "1w6kvnsb144mg96gqp820rvdrv8yrv6hfac2fwg8g1syysg781sp") (f (quote (("pinhole") ("default"))))))

(define-public crate-cv-core-0.9.1 (c (n "cv-core") (v "0.9.1") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.20.0") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0hfyr0x9kfi2l9sawcni5i9hn4wgn1ccx7x3zg36shpz1nb2jcrp") (f (quote (("pinhole") ("default"))))))

(define-public crate-cv-core-0.10.0 (c (n "cv-core") (v "0.10.0") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0661rf3zj8k7jsac53xnin89wfnva639f65adpgjskh6cs9j0c0b")))

(define-public crate-cv-core-0.11.0 (c (n "cv-core") (v "0.11.0") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "1q6p1bzm4zxmjpnm4afr05jpmn2yc3n81h6q964ygw4ckw8dg8gy") (f (quote (("alloc"))))))

(define-public crate-cv-core-0.12.0 (c (n "cv-core") (v "0.12.0") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "15pgmqbgk3lcwk0z3clkky2d4mgar9hvikn603zsmqvbjjz6kn8q") (f (quote (("alloc"))))))

(define-public crate-cv-core-0.13.0 (c (n "cv-core") (v "0.13.0") (d (list (d (n "derive_more") (r "^0.99.8") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.1") (k 0)) (d (n "num-traits") (r "^0.2.12") (k 0)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "1bv5k9nlb23q3dh3izm40k0f0lxvz675l0v8aqbv7kjzbhf1vrv9") (f (quote (("alloc"))))))

(define-public crate-cv-core-0.13.1 (c (n "cv-core") (v "0.13.1") (d (list (d (n "derive_more") (r "^0.99.8") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.1") (k 0)) (d (n "num-traits") (r "^0.2.12") (k 0)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "148dv0nnfpqdw6kzx224pcx71xl04ndzwipf9s21kbnadf8kn2kq") (f (quote (("alloc"))))))

(define-public crate-cv-core-0.13.2 (c (n "cv-core") (v "0.13.2") (d (list (d (n "derive_more") (r "^0.99.8") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.1") (k 0)) (d (n "num-traits") (r "^0.2.12") (k 0)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "14cnsxsb2jvkpyls7n0l535mqhzhg3zgg0jbn7pr2bny2vcfsi9s") (f (quote (("alloc"))))))

(define-public crate-cv-core-0.14.0 (c (n "cv-core") (v "0.14.0") (d (list (d (n "derive_more") (r "^0.99.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.1") (k 0)) (d (n "num-traits") (r "^0.2.12") (k 0)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "1dgsrcg2h6gwziavj4ndp03dl672awbkb3lvm4f4v841xr55jnif")))

(define-public crate-cv-core-0.15.0 (c (n "cv-core") (v "0.15.0") (d (list (d (n "derive_more") (r "^0.99.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.1") (k 0)) (d (n "num-traits") (r "^0.2.12") (k 0)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "124w5aw39rgpqkcr6c2k7bjhywj6rhmxr8ap09km2lg05492h5b6")))

