(define-module (crates-io cv ec cvec) #:use-module (crates-io))

(define-public crate-cvec-0.1.0 (c (n "cvec") (v "0.1.0") (d (list (d (n "coption") (r "0.1.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "0.3.*") (d #t) (k 0)))) (h "1rs08ncf69h18zp47myk2a86vk46d7cf822r2i4amha1zccls0sj") (f (quote (("self_rust_tokenize") ("default" "self_rust_tokenize"))))))

(define-public crate-cvec-0.1.1 (c (n "cvec") (v "0.1.1") (d (list (d (n "coption") (r "0.1.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "self-rust-tokenize") (r "0.3.*") (d #t) (k 0)))) (h "0627xgjqj3m3p6wvipj25pzxbsg3p8gnv7w54h525l33vsgprm6i") (f (quote (("self_rust_tokenize") ("default" "self_rust_tokenize"))))))

