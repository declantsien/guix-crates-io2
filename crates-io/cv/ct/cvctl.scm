(define-module (crates-io cv ct cvctl) #:use-module (crates-io))

(define-public crate-cvctl-0.1.0 (c (n "cvctl") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wasm-core") (r "^0.2.15") (f (quote ("trans"))) (d #t) (k 0)))) (h "19rskly21qcn6v7vb7vs0rscndwmqczpbn1jy9x7bvvvq1gbj51v")))

