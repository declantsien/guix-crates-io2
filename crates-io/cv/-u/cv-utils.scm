(define-module (crates-io cv -u cv-utils) #:use-module (crates-io))

(define-public crate-cv-utils-0.0.1 (c (n "cv-utils") (v "0.0.1") (d (list (d (n "lettre") (r "^0.10.2") (d #t) (k 0)))) (h "199d8s3p2q3sx93465xp9n7r63ika68pz0jf646c4bmzna7g3z7f")))

(define-public crate-cv-utils-0.0.2 (c (n "cv-utils") (v "0.0.2") (d (list (d (n "lettre") (r "^0.10.2") (d #t) (k 0)))) (h "0wkx5iyrwd9azh2v32hz9v3ll29ndr0rgxkqvarkj5g663sav3ql")))

(define-public crate-cv-utils-0.0.3 (c (n "cv-utils") (v "0.0.3") (d (list (d (n "lettre") (r "^0.10.2") (d #t) (k 0)))) (h "0vqm7qwx7wdf4ra5p8mn53basmfqkl22k7qa2f67d2lbsqlcw9ny")))

