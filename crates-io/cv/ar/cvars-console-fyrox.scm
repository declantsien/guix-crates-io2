(define-module (crates-io cv ar cvars-console-fyrox) #:use-module (crates-io))

(define-public crate-cvars-console-fyrox-0.1.0 (c (n "cvars-console-fyrox") (v "0.1.0") (d (list (d (n "cvars") (r "^0.3.0") (d #t) (k 0)) (d (n "cvars-console") (r "^0.1.0") (d #t) (k 0)) (d (n "fyrox-ui") (r "^0.20.0") (d #t) (k 0)))) (h "0x7rygnvdv45ld5vqbai3nipsbrhwfa842y1hk3fhxz7rw61xzz3")))

(define-public crate-cvars-console-fyrox-0.2.0 (c (n "cvars-console-fyrox") (v "0.2.0") (d (list (d (n "cvars") (r "^0.4.0") (d #t) (k 0)) (d (n "cvars-console") (r "^0.2.0") (d #t) (k 0)) (d (n "fyrox-ui") (r "^0.21.0") (d #t) (k 0)))) (h "1y0fn7yngkd2v6kfy6vvsj7y2crd7iifs98h5k01d7cc2l9wb9xj")))

(define-public crate-cvars-console-fyrox-0.3.0 (c (n "cvars-console-fyrox") (v "0.3.0") (d (list (d (n "cvars") (r "^0.4.0") (d #t) (k 0)) (d (n "cvars-console") (r "^0.2.0") (d #t) (k 0)) (d (n "fyrox-ui") (r "^0.22.0") (d #t) (k 0)))) (h "07bmq485xcihnzm897w8yx36kc3rz2y66s6zkzcx3k3afagmspip")))

(define-public crate-cvars-console-fyrox-0.4.0 (c (n "cvars-console-fyrox") (v "0.4.0") (d (list (d (n "cvars") (r "^0.4.0") (d #t) (k 0)) (d (n "cvars-console") (r "^0.2.0") (d #t) (k 0)) (d (n "fyrox-ui") (r "^0.23.0") (d #t) (k 0)))) (h "1jr9177s088lvxhxdsw7gddfsvdzy23110a5wqlbgyqx0dx3b1mw")))

(define-public crate-cvars-console-fyrox-0.5.0 (c (n "cvars-console-fyrox") (v "0.5.0") (d (list (d (n "cvars") (r "^0.4.0") (d #t) (k 0)) (d (n "cvars-console") (r "^0.2.0") (d #t) (k 0)) (d (n "fyrox-ui") (r "^0.24.0") (d #t) (k 0)))) (h "1zqjrm4kbz5afdi1j22j9kzzm1r0138i3dk43cdg2xzbf417z2yi")))

