(define-module (crates-io cv ar cvars) #:use-module (crates-io))

(define-public crate-cvars-0.0.0 (c (n "cvars") (v "0.0.0") (h "0aqs263rf51dzp3gh41ck5apc3apwl24s291kjz2pphd5zs1cgv7")))

(define-public crate-cvars-0.1.0 (c (n "cvars") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.21.1") (d #t) (k 2)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0lvaga3wyb669ksp0ziah42sjl4x4k57dwmf524180qi3mjf0f0z")))

(define-public crate-cvars-0.2.0 (c (n "cvars") (v "0.2.0") (d (list (d (n "cvars-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 2)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "044x8xdpqyvads79inm9sqg6gx3j45xd83mn0dd6b1c878mblbdc")))

(define-public crate-cvars-0.3.0 (c (n "cvars") (v "0.3.0") (d (list (d (n "cvars-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 2)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0idbmshkyn2rhyffdml6aqgnhh7fafri19j1x9ibv4apb7wlcpsm")))

(define-public crate-cvars-0.3.1 (c (n "cvars") (v "0.3.1") (d (list (d (n "cvars-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 2)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1cva3brlb8cc3s755brsrgjszknv3w9cqaqjkfsniqvcn0qmwwqw")))

(define-public crate-cvars-0.3.2 (c (n "cvars") (v "0.3.2") (d (list (d (n "cvars-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 2)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "16yx8z7px69w4apjfnxxkc9qkvinha6s83gdd4ladvp48kl4lhy7")))

(define-public crate-cvars-0.4.0 (c (n "cvars") (v "0.4.0") (d (list (d (n "cvars-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.25.0") (d #t) (k 2)))) (h "0l1880wkpzngb61nghbl8a09jryhx20j3jrgln5mbymaminpaf7s")))

(define-public crate-cvars-0.4.2 (c (n "cvars") (v "0.4.2") (d (list (d (n "cvars-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.25.0") (d #t) (k 2)))) (h "1yvyzcflwvi4g0l03vidf7ns9f4aks9lafh2x8j9ql8diasjiaq5")))

