(define-module (crates-io cv ar cvars-console-macroquad) #:use-module (crates-io))

(define-public crate-cvars-console-macroquad-0.1.0 (c (n "cvars-console-macroquad") (v "0.1.0") (d (list (d (n "cvars") (r "^0.3.0") (d #t) (k 0)) (d (n "cvars-console") (r "^0.1.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.24") (d #t) (k 0)))) (h "09gk23xsm9kq66llds1hf4pqvbfm3f83jrnal5aw56aasavri47f")))

(define-public crate-cvars-console-macroquad-0.2.0 (c (n "cvars-console-macroquad") (v "0.2.0") (d (list (d (n "cvars") (r "^0.4.0") (d #t) (k 0)) (d (n "cvars-console") (r "^0.2.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.24") (d #t) (k 0)))) (h "1gpj4yvm9ciz2x7jdv7cxgrqwq3sdax2vsi5lwj38d0qg9zssbgl")))

(define-public crate-cvars-console-macroquad-0.3.0 (c (n "cvars-console-macroquad") (v "0.3.0") (d (list (d (n "cvars") (r "^0.4.0") (d #t) (k 0)) (d (n "cvars-console") (r "^0.2.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.4.0") (d #t) (k 0)))) (h "1ak1gdqhhfhgq8pgsc580zxgiq3igv2sjxfpsxl2i7if035bxgm1")))

