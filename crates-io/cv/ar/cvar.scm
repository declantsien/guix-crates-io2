(define-module (crates-io cv ar cvar) #:use-module (crates-io))

(define-public crate-cvar-0.1.0 (c (n "cvar") (v "0.1.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)))) (h "1ir0ylw10zi7z8a899iy6hx567kzmp0idp9aj1b3rh8jzlfz4zak")))

(define-public crate-cvar-0.2.0 (c (n "cvar") (v "0.2.0") (h "0gmiygbb5hin14qvgh4yamkivdqbjs0jyxyjvlnah0r13fpkdz7i")))

(define-public crate-cvar-0.2.1 (c (n "cvar") (v "0.2.1") (h "06985a5bqnfnbzw2yw6p605ljm2y6ygvbd8p3s50irfcnzrri82k") (f (quote (("type_name")))) (y #t)))

(define-public crate-cvar-0.2.2 (c (n "cvar") (v "0.2.2") (h "0893p7xcbw2xa04b4g7wswcy8474ndfpcllyshs7whsriz6f8vqv") (f (quote (("type_name"))))))

(define-public crate-cvar-0.3.0 (c (n "cvar") (v "0.3.0") (h "1n0vxpl6aspg91gmcrcibcdvin2qxh4yprrflcxb1z41a3jy8i27") (f (quote (("type_name"))))))

(define-public crate-cvar-0.3.1 (c (n "cvar") (v "0.3.1") (h "1lp6vqx2m511zzldw5y83x2nbkdsfadbf0b5m8624mfgx7wwv0bp") (f (quote (("type_name"))))))

(define-public crate-cvar-0.4.0 (c (n "cvar") (v "0.4.0") (h "1igdwjh3ylg7s2bxfdzw68fhp57vixybqgz3y5ka334ld9jyr4mf") (f (quote (("type_name"))))))

(define-public crate-cvar-0.4.1 (c (n "cvar") (v "0.4.1") (h "0ihwyak5a5x0d9kknr0k1j1jvsqdhwn3m1kz3p4n0cx7gvlxbhk3") (f (quote (("type_name"))))))

