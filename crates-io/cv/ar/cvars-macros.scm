(define-module (crates-io cv ar cvars-macros) #:use-module (crates-io))

(define-public crate-cvars-macros-0.1.0 (c (n "cvars-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10szckg2ih8drchl79qxv5l4y2kzj225s2pjjdygfz92diqmck5q")))

(define-public crate-cvars-macros-0.1.1 (c (n "cvars-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0k630909r937kkpa2vmcmdn6cn7fr9flya199n47p986jqjxx6rv")))

(define-public crate-cvars-macros-0.2.0 (c (n "cvars-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dx2ndwx5mzwrardfc0r09zkn0xw446fzzciwjm7ak27h6j49p69")))

(define-public crate-cvars-macros-0.2.1 (c (n "cvars-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18rqap1zg6y0pxrndxfhd13nvxc2wjng9qsxyir8syxs1gq9lqsk")))

(define-public crate-cvars-macros-0.4.0 (c (n "cvars-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.25.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "188zr4dp8kxbilyb5l659v45qy383sigfm919z93clbf6r9i18ac")))

(define-public crate-cvars-macros-0.4.1 (c (n "cvars-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.25.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0z0rlrdhi1ihkpif8g6059jc1n9kai8phbmmgp822dwfx7pbrqdc")))

(define-public crate-cvars-macros-0.4.2 (c (n "cvars-macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.25.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0b688cf3a58h8a64n2qv73vs8hwfbhwhqr9nqygvqgns56m4n69b")))

