(define-module (crates-io cv ar cvars-console) #:use-module (crates-io))

(define-public crate-cvars-console-0.1.0 (c (n "cvars-console") (v "0.1.0") (d (list (d (n "cvars") (r "^0.3.0") (d #t) (k 0)))) (h "10rhpacpfq87rx01yih6pvy9q58dcvkz63dzxl3qhbli35z83a3b")))

(define-public crate-cvars-console-0.2.0 (c (n "cvars-console") (v "0.2.0") (d (list (d (n "cvars") (r "^0.4.0") (d #t) (k 0)))) (h "1acfjp25rhz23ihv3mq0q8slbkc4diivb94723sjbqraz7x7zc2f")))

