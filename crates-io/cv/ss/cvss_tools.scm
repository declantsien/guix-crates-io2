(define-module (crates-io cv ss cvss_tools) #:use-module (crates-io))

(define-public crate-cvss_tools-0.1.0 (c (n "cvss_tools") (v "0.1.0") (h "079lnc23049sppq7ga8yv6k5rbkwjw74p286lw189d67g7qsg00y")))

(define-public crate-cvss_tools-0.1.1 (c (n "cvss_tools") (v "0.1.1") (h "1737aihg4bzjm0a9nlr25niiawpskr846ykks7sfvh37r8bqiayk")))

(define-public crate-cvss_tools-0.1.2 (c (n "cvss_tools") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "108bi4rpj06h97p1gm8291blj95w4m8qrm64rp91zf2fi4vlgd1g")))

(define-public crate-cvss_tools-0.2.0 (c (n "cvss_tools") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "01klcnwv1wwsilbcyji9g7pavayv1aqzdhlws5bria2bmn0krpy4")))

(define-public crate-cvss_tools-0.3.0 (c (n "cvss_tools") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1fdrk78nizaxw1jq0dpsjmlxx5nbmqkzy5asv97lna47350qwg2g")))

