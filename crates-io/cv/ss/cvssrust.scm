(define-module (crates-io cv ss cvssrust) #:use-module (crates-io))

(define-public crate-cvssrust-0.1.0 (c (n "cvssrust") (v "0.1.0") (h "09iqp6ddrr9zwx5s0v0314jjkvqfm1gwb7jpnihks5x8n50y5x0z")))

(define-public crate-cvssrust-0.1.1 (c (n "cvssrust") (v "0.1.1") (h "0l61z78cj61lbbj9wm8v4ihrkkafla8dcvp1hslhnwc6dadpc7cn")))

(define-public crate-cvssrust-1.0.0 (c (n "cvssrust") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mhdld6z6xr9lkywk5cfd57947qrkrh8v76b6zpphj53189riwfp")))

(define-public crate-cvssrust-1.0.1 (c (n "cvssrust") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bxfig0qifi8cycdl2l10vkbips4a9a9shbm2asm2ybhf4bb61kn")))

