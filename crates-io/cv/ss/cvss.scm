(define-module (crates-io cv ss cvss) #:use-module (crates-io))

(define-public crate-cvss-0.1.0 (c (n "cvss") (v "0.1.0") (h "0n5mysxfxzm4mqrzqzb7zhpv6vgjsfqkkv0865n7mm80jipz6jdb") (f (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-0.2.0 (c (n "cvss") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "19xvqivrplyylqjnnhf4nrg4bs65j9kvv37q7051qrvd0yjbswy5") (f (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-0.3.0 (c (n "cvss") (v "0.3.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1ic1ppvgvz9bi0q3grmwi3hss5fb8b7fm2hmsv985gpjx30rg929") (f (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-1.0.0 (c (n "cvss") (v "1.0.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "078nwyhnd6agk7dm6hqxm6f2jr46sx5iq8c28ihwrsgimrcw09f4") (f (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-1.0.1 (c (n "cvss") (v "1.0.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0n4gvadiim38krhb00szz3jwg0n22miq08fqa8dk11fa5crdiv5j") (f (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-1.0.2 (c (n "cvss") (v "1.0.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0gxxzimyxwf6ka011n6cd296ax6qiwnx8n6mxzh2l55bpvd65642") (f (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-2.0.0 (c (n "cvss") (v "2.0.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "03q1nh4jy0cvgckji1jr1kz3j7gf2zg74240j8qi1qxhk7vs5iky") (f (quote (("v3") ("std") ("default" "std" "v3")))) (r "1.56")))

