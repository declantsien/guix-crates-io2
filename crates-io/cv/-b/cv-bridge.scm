(define-module (crates-io cv -b cv-bridge) #:use-module (crates-io))

(define-public crate-cv-bridge-0.1.0 (c (n "cv-bridge") (v "0.1.0") (h "0xdnid835h9kf0p75kp0vmxln0qyf6xsg4zx3kqq5mj1yx0d9fpl") (y #t)))

(define-public crate-cv-bridge-0.1.1 (c (n "cv-bridge") (v "0.1.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "opencv") (r "^0.74.2") (d #t) (k 0)) (d (n "rosrust_msg") (r "^0.1.6") (d #t) (k 0)))) (h "17fhfswm4w513h4bawijg970xgyk7r6bzc2mk5pv8bfwz7268dzk") (y #t)))

(define-public crate-cv-bridge-0.2.0 (c (n "cv-bridge") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "opencv") (r "^0.74.2") (d #t) (k 0)) (d (n "rosrust_msg") (r "^0.1.6") (d #t) (k 0)))) (h "0v2zrwak78r1hhvdwgpsjamhind04p95x78ihjyavwgq1x5lax5i") (y #t)))

(define-public crate-cv-bridge-0.3.0 (c (n "cv-bridge") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "opencv") (r "^0.74.2") (d #t) (k 0)) (d (n "rosrust_msg") (r "^0.1.6") (d #t) (k 0)))) (h "12zsm9mfa853y6wgvd67sgfg8z99ijvf881022g81yx9yzqwdh8v")))

(define-public crate-cv-bridge-0.3.1 (c (n "cv-bridge") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "opencv") (r "^0.74.2") (d #t) (k 0)) (d (n "rosrust_msg") (r "^0.1.6") (d #t) (k 0)))) (h "08fdlp6fr8965c0lijvmwcswlbj5wk1djcyg9fr6r8clmm8710jp")))

(define-public crate-cv-bridge-0.3.2 (c (n "cv-bridge") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "opencv") (r "^0.74.2") (d #t) (k 0)) (d (n "rosrust") (r "^0.9.10") (d #t) (k 0)))) (h "0mfqpr50dpih2gjklqbb4zld46l5cgml7hsp9g8y1z2z44f9iklj")))

(define-public crate-cv-bridge-0.3.3 (c (n "cv-bridge") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "opencv") (r "^0.76.4") (d #t) (k 0)) (d (n "rosrust") (r "^0.9.10") (d #t) (k 0)))) (h "0yxqhbvi7fwjy6hnnpmdiaqyqwa49fmsa4m7fbkz738gzklyv9bd")))

