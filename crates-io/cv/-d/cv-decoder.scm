(define-module (crates-io cv -d cv-decoder) #:use-module (crates-io))

(define-public crate-cv-decoder-0.1.0 (c (n "cv-decoder") (v "0.1.0") (d (list (d (n "c_str_macro") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "ofps") (r "^0.1") (d #t) (k 0)) (d (n "opencv") (r "^0.62") (f (quote ("clang-runtime"))) (d #t) (k 0)))) (h "0q4azk6zvg11c2nwrd7dixkmn188vnw779h9dyvlqs9rcsflsq9l")))

