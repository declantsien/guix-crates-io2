(define-module (crates-io cv mc cvmcount) #:use-module (crates-io))

(define-public crate-cvmcount-0.1.0 (c (n "cvmcount") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0256sh20vg72dapm8wxd0zrb07riq3a59hs3m5blk39sw444cnj4") (y #t)))

(define-public crate-cvmcount-0.1.1 (c (n "cvmcount") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0mra39g6d5abh5w1kd6lcyy3liwxnvnpb21lwj41584y685y4zvz") (y #t)))

(define-public crate-cvmcount-0.1.2 (c (n "cvmcount") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "00pkyjqd7kmkqkykyqpzkb0df9nhf7pl3j3fsg64y0ip4isx8zqp") (y #t)))

(define-public crate-cvmcount-0.1.3 (c (n "cvmcount") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "02q4v2d83krrp4jq6zl5a84npfx5bmpzrl65wh2b1mfrqdli0g14") (y #t)))

(define-public crate-cvmcount-0.1.4 (c (n "cvmcount") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1mz1wmhm38sffcxqc3pcq8ywfm9zczx432ly7dic9wi9lb7bmqq2") (y #t)))

(define-public crate-cvmcount-0.1.5 (c (n "cvmcount") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "07pi3v9zqg13bwxarvpfawgvplfyd7k8c09jkjkd0lkllgs5s5rn") (y #t)))

(define-public crate-cvmcount-0.1.6 (c (n "cvmcount") (v "0.1.6") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1j1gbnd9bry4i391mw7rr6h37jljghjbjjvvghc8mad13yix6ilg") (y #t)))

(define-public crate-cvmcount-0.1.7 (c (n "cvmcount") (v "0.1.7") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "102xwlq64zrg016g08hn9k38rm18nj4d2v8g40js6r5qbnff622b") (y #t)))

(define-public crate-cvmcount-0.1.8 (c (n "cvmcount") (v "0.1.8") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1v5l5hxkpmhbglziaynpi9fflgdjvvhhcnidy4nx1cpiqcp3llxw") (y #t)))

(define-public crate-cvmcount-0.1.9 (c (n "cvmcount") (v "0.1.9") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0hpcccd5mj7lnlffj5asmx359p8akz8aaxlnhdx0bfchhpdvfvxr") (y #t)))

(define-public crate-cvmcount-0.1.10 (c (n "cvmcount") (v "0.1.10") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0i8r887hi0ayxk41l5zn8rxbrxzh1vmjrbxk4yn8plxpv46yf0rx") (y #t)))

(define-public crate-cvmcount-0.1.11 (c (n "cvmcount") (v "0.1.11") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1gsy554fr6y1b9lf63j4nivd53kyrc0a4j6zdh8p4r5aar18j2ix")))

