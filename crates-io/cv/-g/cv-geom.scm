(define-module (crates-io cv -g cv-geom) #:use-module (crates-io))

(define-public crate-cv-geom-0.1.0 (c (n "cv-geom") (v "0.1.0") (d (list (d (n "cv-core") (r "^0.11.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)))) (h "1rfi83i2iphrzmx1pnsqik60aa7vw7m6vsalfq698sx9f60z7bw9")))

(define-public crate-cv-geom-0.1.1 (c (n "cv-geom") (v "0.1.1") (d (list (d (n "cv-core") (r "^0.11.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)))) (h "1x1qmp227d37wwwm4rpk9qnlrq1dq3q9h635xzv0k8dia9yj65qv")))

(define-public crate-cv-geom-0.2.0 (c (n "cv-geom") (v "0.2.0") (d (list (d (n "cv-core") (r "^0.12.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)))) (h "0i79ifcw0d456ns5aapcvn9292slsa2646jfkysfc9d0wd8a56ni")))

(define-public crate-cv-geom-0.2.1 (c (n "cv-geom") (v "0.2.1") (d (list (d (n "cv-core") (r "^0.12.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)))) (h "0krjfar1xili4fh8i4k53x7lxcba3cvhg1pg1nayx7gw6dxz22rm")))

(define-public crate-cv-geom-0.3.0 (c (n "cv-geom") (v "0.3.0") (d (list (d (n "cv-core") (r "^0.13.1") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)))) (h "0ihnfzqd4hlkffpkiip2s9ynln8pqjdk5q0xps1wd4ylj36bdjqd")))

(define-public crate-cv-geom-0.4.0 (c (n "cv-geom") (v "0.4.0") (d (list (d (n "cv-core") (r "^0.14.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)))) (h "0rs1j8wdl60l9wnsawm330vv9h9c2jiishpp8s5q9950pnn268ia")))

(define-public crate-cv-geom-0.5.0 (c (n "cv-geom") (v "0.5.0") (d (list (d (n "cv-core") (r "^0.14.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)))) (h "05f0q1fkgj7xzsxjxiba7h008x9hgnahdp8zv2i36779ay3v49h2")))

(define-public crate-cv-geom-0.5.1 (c (n "cv-geom") (v "0.5.1") (d (list (d (n "cv-core") (r "^0.14.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)))) (h "0vhpmr3dyd8zwzb31bds06mnkj28b67hc7i8l8g8lv2rdphgap6i")))

(define-public crate-cv-geom-0.6.0 (c (n "cv-geom") (v "0.6.0") (d (list (d (n "cv-core") (r "^0.14.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)))) (h "0an92q54qa82pqrv18fq61pr135hlr58287v0a6far9v83ifkphq")))

(define-public crate-cv-geom-0.7.0 (c (n "cv-geom") (v "0.7.0") (d (list (d (n "cv-core") (r "^0.15.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)))) (h "04whcs95kh0dv11rp9pv9w7rq8m2rsfa77q1ibxam01lhwnybj8b")))

