(define-module (crates-io cv rd cvrdt-exposition) #:use-module (crates-io))

(define-public crate-cvrdt-exposition-0.1.0 (c (n "cvrdt-exposition") (v "0.1.0") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)))) (h "1x622nik7ib1dymphgsrysknhw2wsssrrisl8d275yxssd7q3wwb")))

(define-public crate-cvrdt-exposition-0.1.1 (c (n "cvrdt-exposition") (v "0.1.1") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)))) (h "0wmhycy24skwq9cw4633s144na0ar6ljjnh4510vz6qhh3fsbsyc")))

(define-public crate-cvrdt-exposition-0.1.2 (c (n "cvrdt-exposition") (v "0.1.2") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)))) (h "18hz77lww1h9bbpl9j1153q8z066ij415lrf77vnldg0xkwa23ld")))

(define-public crate-cvrdt-exposition-0.1.3 (c (n "cvrdt-exposition") (v "0.1.3") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)))) (h "0bywwnw8r9dk8dkylc44fdyxjgcm2kcw7qq292bry30pfdkrbbag")))

(define-public crate-cvrdt-exposition-0.1.4 (c (n "cvrdt-exposition") (v "0.1.4") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)))) (h "0glnar8rghhisq137jjhpw8nz7x3jgbdpzcc4jrgbs5q4s6frm4g")))

(define-public crate-cvrdt-exposition-0.1.5 (c (n "cvrdt-exposition") (v "0.1.5") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "15nca72wq5x9yvssnhl8igji07zw85dl3yka27qpb12ch4pvqgr1")))

(define-public crate-cvrdt-exposition-0.1.6 (c (n "cvrdt-exposition") (v "0.1.6") (d (list (d (n "proptest") (r "^1.1.0") (d #t) (k 2)))) (h "1jdcghdlb5lypnl1qdbfa8v61n2wh8hxczpsqsny5qa12z8lkfhq")))

(define-public crate-cvrdt-exposition-0.2.0 (c (n "cvrdt-exposition") (v "0.2.0") (d (list (d (n "proptest") (r "^1.3.1") (d #t) (k 2)))) (h "1ccmqk28iw1yl2v8nnc4pma6b2liignsb9hqcgcbl1xy0il897jy")))

