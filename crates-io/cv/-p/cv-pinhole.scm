(define-module (crates-io cv -p cv-pinhole) #:use-module (crates-io))

(define-public crate-cv-pinhole-0.1.0 (c (n "cv-pinhole") (v "0.1.0") (d (list (d (n "cv-core") (r "^0.10.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "0qjck87lkrd7dwfq4xm6g2vnbj2fzgg7pvqzaq9mfbn4bkc4cszr")))

(define-public crate-cv-pinhole-0.1.1 (c (n "cv-pinhole") (v "0.1.1") (d (list (d (n "cv-core") (r "^0.10.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "18vl0w2hkipim8g6zjvqw79qy20pyhkh598dsb2p82y62wfn7r3r")))

(define-public crate-cv-pinhole-0.2.0 (c (n "cv-pinhole") (v "0.2.0") (d (list (d (n "cv-core") (r "^0.11.0") (d #t) (k 0)) (d (n "cv-geom") (r "^0.1.0") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "1skbmavz4dcv856c9n1jx24qp0figkv94pgg9czd44qn775i74gw")))

(define-public crate-cv-pinhole-0.3.0 (c (n "cv-pinhole") (v "0.3.0") (d (list (d (n "cv-core") (r "^0.12.0") (d #t) (k 0)) (d (n "cv-geom") (r "^0.2.0") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "0gd4fnpkrfn55hq31bphvl8rwvrd25xiwj6nkh6v8mqxcmcic6dz")))

(define-public crate-cv-pinhole-0.4.0 (c (n "cv-pinhole") (v "0.4.0") (d (list (d (n "cv-core") (r "^0.13.1") (d #t) (k 0)) (d (n "cv-geom") (r "^0.3.0") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (k 0)))) (h "1gv4p73r333ns96vwpsl2l0mskv7sabrvv7c1yv2yxp1asw1p6bs")))

(define-public crate-cv-pinhole-0.5.0 (c (n "cv-pinhole") (v "0.5.0") (d (list (d (n "cv-core") (r "^0.14.0") (d #t) (k 0)) (d (n "cv-geom") (r "^0.5.0") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.8") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (k 0)))) (h "1q814kk0cgwzihkgnb52qij570dfkmc8iwbl7hp0y7z73j1iipwa") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-cv-pinhole-0.6.0 (c (n "cv-pinhole") (v "0.6.0") (d (list (d (n "cv-core") (r "^0.15.0") (d #t) (k 0)) (d (n "cv-geom") (r "^0.7.0") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.8") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (k 0)))) (h "06g1d23wddarwgwc9iq5abg0x4l6m503v5km1139sq65m3w62hhf") (f (quote (("default") ("alloc"))))))

