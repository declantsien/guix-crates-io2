(define-module (crates-io cv t- cvt-utils) #:use-module (crates-io))

(define-public crate-cvt-utils-0.1.0 (c (n "cvt-utils") (v "0.1.0") (h "1f0qirdb5xcyvn6ww2wxz7njxvk12hvi41s4k9z25g96vrm36h1c")))

(define-public crate-cvt-utils-0.1.1 (c (n "cvt-utils") (v "0.1.1") (h "1f4mk7aq9qhim4rphi618q2f7dh4pi887aab8ydm77rwl71ls0bc")))

(define-public crate-cvt-utils-0.1.2 (c (n "cvt-utils") (v "0.1.2") (h "1bnmm27hql0i2dvcnvcliiwvyv4dsihabp6z15jx7r3jr4az9376")))

