(define-module (crates-io cv li cvlib) #:use-module (crates-io))

(define-public crate-cvlib-0.1.0 (c (n "cvlib") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bg0h1h1gvwms9l81x3sz3iv3s2f8v6xcs9s257b0yinasr8aaln")))

(define-public crate-cvlib-0.1.1 (c (n "cvlib") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1m6pdc984zs56kdpjkprwkh3iqfb51ay96ncbhrmgqk0c7biqm83")))

(define-public crate-cvlib-0.1.2 (c (n "cvlib") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1s96lg32pm3q2vnpbwg01vb10439lp09plnn1w45h742mdmaa3bm")))

