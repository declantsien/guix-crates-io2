(define-module (crates-io cv #{2-}# cv2-crate) #:use-module (crates-io))

(define-public crate-cv2-crate-0.1.0 (c (n "cv2-crate") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "sort") (r "^0.8.5") (d #t) (k 0)))) (h "0k11yh9s9cfp7dbip26bhrnnnn3nqcva0w746h95cl1knj4ir8hw")))

(define-public crate-cv2-crate-0.1.1 (c (n "cv2-crate") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "sort") (r "^0.8.5") (d #t) (k 0)))) (h "08g8kklkp42pb24xvv8abrkqgflx8c8f831jq8d5k3ymipk6245k")))

(define-public crate-cv2-crate-0.1.3 (c (n "cv2-crate") (v "0.1.3") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "sort") (r "^0.8.5") (d #t) (k 0)))) (h "1m9x2g04pil4vb9gm5k0jg92zj3wc7dkgpq8ls0m2p6zr26x1ci1")))

(define-public crate-cv2-crate-0.1.4 (c (n "cv2-crate") (v "0.1.4") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "sort") (r "^0.8.5") (d #t) (k 0)))) (h "05mwjl6apb0k1z1lh5ar3ng68ga1mlbbja8rpgzr6961fkrqrw9r")))

