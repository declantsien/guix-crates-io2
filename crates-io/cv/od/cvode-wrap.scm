(define-module (crates-io cv od cvode-wrap) #:use-module (crates-io))

(define-public crate-cvode-wrap-0.1.0 (c (n "cvode-wrap") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "sundials-sys") (r "^0.2.1") (f (quote ("cvodes"))) (k 0)))) (h "1812sbh5aclmhw51fp9yil53dl68p9rpzim24j7021d0bp6y5pyw")))

(define-public crate-cvode-wrap-0.1.1 (c (n "cvode-wrap") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "sundials-sys") (r "^0.2.1") (f (quote ("cvodes"))) (k 0)))) (h "132w12d7qy7fkxpn14xrmjzxvzz0pyjrzdam15k4l1yfab0wccxg")))

(define-public crate-cvode-wrap-0.1.2 (c (n "cvode-wrap") (v "0.1.2") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "sundials-sys") (r "^0.2.3") (f (quote ("cvodes"))) (k 0)))) (h "019ndz04l8axk6v6l4hj7hylivp2n4xv7wbffi9ydjm8gy5sv3rb")))

(define-public crate-cvode-wrap-0.1.3 (c (n "cvode-wrap") (v "0.1.3") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "sundials-sys") (r "^0.2.3") (f (quote ("cvodes"))) (k 0)))) (h "027dpvhs8aczzaxsvwxrz2rw0j5prlf1ywxqzjw43hp00aw40dv6")))

