(define-module (crates-io hg li hglib) #:use-module (crates-io))

(define-public crate-hglib-0.1.0 (c (n "hglib") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "0aivmhcyl4vnb1a742l4xa91sb0wqpqkqz1pjb3p9224anya085b")))

(define-public crate-hglib-0.1.1 (c (n "hglib") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "1yzg3sq6qw0bhhy0v2zw8kdb10d7x07kwhs43n350dq4cbd0fp94")))

