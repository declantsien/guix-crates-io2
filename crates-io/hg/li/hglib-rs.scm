(define-module (crates-io hg li hglib-rs) #:use-module (crates-io))

(define-public crate-hglib-rs-0.1.0 (c (n "hglib-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "1w8n232h6nwa1ixnkalqiqpx575lr2b06c91s5z7yr2wgpm32dy0")))

(define-public crate-hglib-rs-0.1.1 (c (n "hglib-rs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "1x329n728pm1hzx5dp3wm7m2g4pqpvgq58cgcxfpbg4hjzn4m888")))

