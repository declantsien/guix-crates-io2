(define-module (crates-io hg en hgen) #:use-module (crates-io))

(define-public crate-hgen-0.1.0 (c (n "hgen") (v "0.1.0") (h "0a0xckdifnm7bk0h5z9ly3dm4ysfz36c88g86w2x13s33g6cnr9j")))

(define-public crate-hgen-0.2.0 (c (n "hgen") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "059xkmkvdrg1i1wbxnkl1gwrwdpbcpc0f8krjgrzisakgjgavjfn")))

