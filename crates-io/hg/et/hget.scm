(define-module (crates-io hg et hget) #:use-module (crates-io))

(define-public crate-hget-0.0.0 (c (n "hget") (v "0.0.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iinm041hz8yf4hhxfkyqdxqihcvzvha5pac4yjldf0wrvkhpbgp")))

(define-public crate-hget-2.0.1 (c (n "hget") (v "2.0.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mrsnilpp7fqg9ca72slzsgby5k2mrrabhza6arwj2qqbj9fcjrr")))

