(define-module (crates-io hg -c hg-cpython) #:use-module (crates-io))

(define-public crate-hg-cpython-0.0.1 (c (n "hg-cpython") (v "0.0.1") (d (list (d (n "cpython") (r "~0.2.1") (k 0)) (d (n "hg-core") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "python27-sys") (r "~0.2.1") (o #t) (d #t) (k 0)) (d (n "python3-sys") (r "~0.2.1") (o #t) (d #t) (k 0)))) (h "1fikqci2bzm4wh5dbwf27l71wg9m0s59ldybvwdb07b7j6r6fy9w") (f (quote (("python3" "python3-sys" "cpython/python3-sys" "cpython/extension-module") ("python27" "cpython/python27-sys" "cpython/extension-module-2-7" "python27-sys") ("default" "python27"))))))

