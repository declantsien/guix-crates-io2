(define-module (crates-io s1 vm s1vm) #:use-module (crates-io))

(define-public crate-s1vm-0.1.0 (c (n "s1vm") (v "0.1.0") (d (list (d (n "bwasm") (r "^0.1.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.41") (d #t) (k 0)) (d (n "wasmi-validation") (r "^0.4") (d #t) (k 0)))) (h "07rgbx1nm0sn5p99l7dm0r129hadisyfyfc0z9dcsnkwjqvwsvm3")))

(define-public crate-s1vm-0.1.1 (c (n "s1vm") (v "0.1.1") (d (list (d (n "bwasm") (r "^0.1.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.41") (d #t) (k 0)) (d (n "wasmi-validation") (r "^0.4") (d #t) (k 0)))) (h "1lvzkvrkczp1v03q6ki6wyk8y6937kqnhwpn1zv8ac62yyvgsbbc")))

