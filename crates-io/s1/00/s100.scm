(define-module (crates-io s1 #{00}# s100) #:use-module (crates-io))

(define-public crate-s100-0.0.0 (c (n "s100") (v "0.0.0") (h "0j67gn5lay4aahkzn72564j0rxzh7gqzwh6indx1j3llzadnk79h") (y #t)))

(define-public crate-s100-0.1.1-alpha.1 (c (n "s100") (v "0.1.1-alpha.1") (d (list (d (n "iso8211") (r "^0.1.1-alpha.4") (d #t) (k 0)) (d (n "libxml") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "18z1pzdfhly8wyfdwxiwcj31p9bay9l6qp3jmk2hwc5xlby5kqn6") (y #t)))

