(define-module (crates-io qa lq qalqulator) #:use-module (crates-io))

(define-public crate-qalqulator-0.1.0 (c (n "qalqulator") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "gcd") (r "^2.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.4.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "09wkkgxanr3g5ipk7wj9qwfpx5pvfk4ppjqvb8d7g3hzj1ydh7jr")))

