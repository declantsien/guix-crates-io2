(define-module (crates-io qa f- qaf-build-utils) #:use-module (crates-io))

(define-public crate-qaf-build-utils-0.1.0 (c (n "qaf-build-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1wrxsw9wv6w918y10b0g4clgwa5dalf3q86gxf1fwdx7wfcq12ns")))

