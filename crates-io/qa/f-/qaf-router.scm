(define-module (crates-io qa f- qaf-router) #:use-module (crates-io))

(define-public crate-qaf-router-0.1.0 (c (n "qaf-router") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "matchit") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "12y83v1pw5x2g1cqc8b9h8d9hy4x3s5s471amiljdyinpj52b1c8")))

(define-public crate-qaf-router-0.1.1 (c (n "qaf-router") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "matchit") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "17nimp74khjgx19a9nc3h80wl1gc4hf4lazw8mbng5jbfprnkj02")))

