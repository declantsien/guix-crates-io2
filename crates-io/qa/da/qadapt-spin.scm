(define-module (crates-io qa da qadapt-spin) #:use-module (crates-io))

(define-public crate-qadapt-spin-1.0.0 (c (n "qadapt-spin") (v "1.0.0") (h "1axgxsakhi8l02vi6k6krhl0cas7rjgik3dak96d6d30kwz6ydn5")))

(define-public crate-qadapt-spin-1.0.1 (c (n "qadapt-spin") (v "1.0.1") (h "0vxiav7fwyz9dxv63cw1f1bra72crkxz45baqa2y9yv9hn7iqqcz")))

