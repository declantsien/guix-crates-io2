(define-module (crates-io qa da qadapt-macro) #:use-module (crates-io))

(define-public crate-qadapt-macro-0.3.0 (c (n "qadapt-macro") (v "0.3.0") (h "0dfbd3m4nsvznzwy11ah1r5pqarrq5jn7m984dyy4mxjmislvjv6")))

(define-public crate-qadapt-macro-0.4.0 (c (n "qadapt-macro") (v "0.4.0") (h "0v2jvfy4hd0911hmh9xi3m8r6jnpilmqb0ch4zkvy7k1hhw3m0az")))

(define-public crate-qadapt-macro-0.5.0 (c (n "qadapt-macro") (v "0.5.0") (h "183wffyf2ndmgac13c87pi4ij5ba0kihhd5rcrp464fdkvw6707h")))

(define-public crate-qadapt-macro-0.6.0 (c (n "qadapt-macro") (v "0.6.0") (h "1z0q9r2jjpgprjr4n181jaim56b25crw7aq7qvi0wkjnssld1jln")))

(define-public crate-qadapt-macro-0.7.0 (c (n "qadapt-macro") (v "0.7.0") (h "0widq5qhrjbfgizzs2aazlasrjq64z9hpdvi9zijm7s0i8wzd3c6")))

(define-public crate-qadapt-macro-0.7.1 (c (n "qadapt-macro") (v "0.7.1") (h "0r0i2dzhr5qj453w4cv1lkmwlsp4z4pnw054sgk79xwq2jsy23mr")))

(define-public crate-qadapt-macro-1.0.0 (c (n "qadapt-macro") (v "1.0.0") (h "07k6xd15kzj3ypmslqnfr07kgyvddj5fakyw6v1wksnz3fzahmxz")))

(define-public crate-qadapt-macro-1.0.1 (c (n "qadapt-macro") (v "1.0.1") (h "1pahny42hw11lhph8k1s5adwnqsyl06f9r1a4h6hqpgk32b01n41")))

(define-public crate-qadapt-macro-1.0.2 (c (n "qadapt-macro") (v "1.0.2") (h "02pg6cwsy48713ah4q0rjdnl054npysjyxwyb6sgy98sfkqx5xa4")))

(define-public crate-qadapt-macro-1.0.3 (c (n "qadapt-macro") (v "1.0.3") (h "16ywwwll652335cln5462hc747mh6v4xmj59k3jxjibb2k38dq7v")))

