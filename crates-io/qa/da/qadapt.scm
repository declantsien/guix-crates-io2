(define-module (crates-io qa da qadapt) #:use-module (crates-io))

(define-public crate-qadapt-0.1.0 (c (n "qadapt") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "05x5aalyxvwv04vyfqwc0na581vc5f2dl13pv57y24fcwkx2h1zl")))

(define-public crate-qadapt-0.2.0 (c (n "qadapt") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "1w8wkbmw5ifl19dh5jqg99kifg7ig9d7j6vzq716alhrgx6f6ikf")))

(define-public crate-qadapt-0.2.1 (c (n "qadapt") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "158037mgryhs7jb13scdn7jjgsi773pcqpglzhk066511l318wav")))

(define-public crate-qadapt-0.3.0 (c (n "qadapt") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qadapt-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)))) (h "0gv1wiy6yl0mq3dp0pyp04rxp2j94ckwp71915i4fjcqx9ikp1dm")))

(define-public crate-qadapt-0.4.0 (c (n "qadapt") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qadapt-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)))) (h "060rrj0i0j7sib0ksci8l19p9dmp7f01anyx109pn033l7d45kja")))

(define-public crate-qadapt-0.5.0 (c (n "qadapt") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qadapt-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)))) (h "0x353z1xpcd0c9aiv0b6zjfm9a748vc7r60zgkkigvphz0x0bk98")))

(define-public crate-qadapt-0.6.0 (c (n "qadapt") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qadapt-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)))) (h "1irzd9dp4xjyz37363bzs6gxdgkifaz7cii65d7n7rd9bp4l9lrv")))

(define-public crate-qadapt-0.7.0 (c (n "qadapt") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qadapt-macro") (r "^0.7.0") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)))) (h "1h3fsxbfxasf3bhd4286xjbchfyyy0641gywppljnc2mga445zrw")))

(define-public crate-qadapt-0.7.1 (c (n "qadapt") (v "0.7.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "qadapt-macro") (r "^0.7.1") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)))) (h "1pzqrdydcd5rdbvja08v3fv5lazkw79xcnal3qcl4vvsd75cspm2")))

(define-public crate-qadapt-1.0.0 (c (n "qadapt") (v "1.0.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qadapt-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "qadapt-spin") (r "^1.0.0") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)))) (h "070zshdi00i1268hh9864z72hrp003g5f1kz1xpcj1yhxvrv122a")))

(define-public crate-qadapt-1.0.1 (c (n "qadapt") (v "1.0.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "qadapt-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "qadapt-spin") (r "^1.0.0") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)))) (h "02i8gpc1plxya2h2pqp7aqx1zcj94i5sjqbl6yc2c3pm4b4f6xij")))

(define-public crate-qadapt-1.0.2 (c (n "qadapt") (v "1.0.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "qadapt-macro") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)))) (h "1hd0y5c0x36psanhmll4f2lxahnr4s2k63bb9v4mrwaw24a0jg48")))

(define-public crate-qadapt-1.0.3 (c (n "qadapt") (v "1.0.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "qadapt-macro") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)))) (h "07gvqyr8c37kwr4lfvyqsfccdsypgsq1wddrg2c4j7x3yq7bi5y5")))

(define-public crate-qadapt-1.0.4 (c (n "qadapt") (v "1.0.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "qadapt-macro") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "thread-id") (r "^3.3") (d #t) (k 0)))) (h "0z5lf3myvhc5zhwdpizwlak3s151cs4kpjpmri313f5rnhlmwp4p")))

