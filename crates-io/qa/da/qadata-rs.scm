(define-module (crates-io qa da qadata-rs) #:use-module (crates-io))

(define-public crate-qadata-rs-0.1.0 (c (n "qadata-rs") (v "0.1.0") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "clickhouse-rs") (r "^0.1.21") (d #t) (k 0)) (d (n "influxdb") (r "^0.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mifi-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.0") (f (quote ("sync"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 0)))) (h "0b8ywpwjjwh0v3f3vdw0vsdnfyqh42dpjv4iypl32ry7r30dx9kb")))

