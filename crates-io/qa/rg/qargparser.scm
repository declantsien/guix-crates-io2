(define-module (crates-io qa rg qargparser) #:use-module (crates-io))

(define-public crate-qargparser-0.5.0 (c (n "qargparser") (v "0.5.0") (d (list (d (n "qpprint") (r "^0.1.1") (d #t) (k 0)))) (h "0iss8rq8vq1rj15l1hm1njacssma7yh8gpm5bzjpfl0l3bls1yz8")))

(define-public crate-qargparser-0.5.2 (c (n "qargparser") (v "0.5.2") (d (list (d (n "qpprint") (r "^0.1.1") (d #t) (k 0)))) (h "19cjkibi4gw38764wj1pzg5fc9xq9ncsxcjwsy06p1fwsxx8mi83")))

(define-public crate-qargparser-0.5.3 (c (n "qargparser") (v "0.5.3") (d (list (d (n "qpprint") (r "^0.1.1") (d #t) (k 0)))) (h "1fnrmyfch3l41s0yh2586fkhh3fjkyzpisc68440ii19skrj10qx")))

(define-public crate-qargparser-0.5.4 (c (n "qargparser") (v "0.5.4") (d (list (d (n "qpprint") (r "^0.1.1") (d #t) (k 0)))) (h "0npmjgcvr2zx6avhij1y7alxadpdl9baccgp6i8a75knybvchnsd")))

(define-public crate-qargparser-0.5.5 (c (n "qargparser") (v "0.5.5") (d (list (d (n "qpprint") (r "^0.1.1") (d #t) (k 0)))) (h "143f8l44gfbvhrpyw544vjmhshcybsd04rb2bavqdhi6v5rrg8h8")))

(define-public crate-qargparser-0.5.6 (c (n "qargparser") (v "0.5.6") (d (list (d (n "qpprint") (r "^0.1.1") (d #t) (k 0)))) (h "1wjl4q8hrr13faprcbsfp8kxg1kmimgly9qbqqifar1g7inz69yg")))

