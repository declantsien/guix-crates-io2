(define-module (crates-io qa pi qapi-spec) #:use-module (crates-io))

(define-public crate-qapi-spec-0.0.1 (c (n "qapi-spec") (v "0.0.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "185rydkn2amcbjgxs2lqcs6swlh8zmwf9234sqj0jirzxxcpzvrp")))

(define-public crate-qapi-spec-0.2.1 (c (n "qapi-spec") (v "0.2.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "1s3m79ickrfy4w0bx8yvpcz51ag4kq88vkadvv0960j805jp9dv0")))

(define-public crate-qapi-spec-0.2.2 (c (n "qapi-spec") (v "0.2.2") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "02x76dba4dg5ns4gm8ffmjh0pkfn3bmzb874prc5smy5nbvv496j")))

(define-public crate-qapi-spec-0.3.0 (c (n "qapi-spec") (v "0.3.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "1qj3w5z4yjm2brkd9sndjnv4vq8phn3ysvkd2bpv40zwb00vqgrk")))

(define-public crate-qapi-spec-0.3.1 (c (n "qapi-spec") (v "0.3.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "1s59qm6knxsp69v5yn0wwpz47dj3iyyh3jv2lwpw0pza4jd92q5k")))

