(define-module (crates-io qa pi qapi-parser) #:use-module (crates-io))

(define-public crate-qapi-parser-0.0.1 (c (n "qapi-parser") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0z030w3rcfpg8vy54igj5060nwnvfx72byl011fk3r149sn8bghk")))

(define-public crate-qapi-parser-0.2.0 (c (n "qapi-parser") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0l2mh5rz3fbmy9886qiwnw7qpxl0rnbpblil1svaqiddc0q91vy3")))

(define-public crate-qapi-parser-0.2.1 (c (n "qapi-parser") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "1ajnmdpvwwp5x8zzx40k9652xf6v1slph49yvrpsx49bfdn06y4k")))

(define-public crate-qapi-parser-0.3.0 (c (n "qapi-parser") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "1lshp1gp1qasjh14z8d2bj95cs7p0bq1v1b6cqdr1bdkrqz67as1")))

(define-public crate-qapi-parser-0.5.0 (c (n "qapi-parser") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0z5hkm9fgjpwm6795f82zfb72zdzvcrxdhzlgqyv16wi7qwldvp2")))

(define-public crate-qapi-parser-0.6.0 (c (n "qapi-parser") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "1r4ld6bqkpmfhkah3491d7lgdjqrv52ykwqqs9rlnisa49w5ljcj")))

(define-public crate-qapi-parser-0.9.0 (c (n "qapi-parser") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0anpwyba8p5pzgp1w9pc8anikj1wc0v641r61sdhivp09i5gsz8n")))

(define-public crate-qapi-parser-0.9.1 (c (n "qapi-parser") (v "0.9.1") (d (list (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0cmkpzmplpfcmmn4cpzwwzdf7x6s0qfnd3z3iy9hlxqqzhkalqmg")))

(define-public crate-qapi-parser-0.10.0 (c (n "qapi-parser") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0xx6vix50dvzdf2pwm2269vg4l6ardnkgl03b3pm6ada8nqls140")))

