(define-module (crates-io qa sm qasm) #:use-module (crates-io))

(define-public crate-qasm-0.1.0 (c (n "qasm") (v "0.1.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "02fgb76lfiid4sd3j2qc53av4dz3wals63gqh2hsdi9szjd0xn9q") (y #t)))

(define-public crate-qasm-1.0.0 (c (n "qasm") (v "1.0.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0kki899j6rb1n2vv2bsv48gk7zi53h2xynsr6aw3m7za9shmjnx2")))

