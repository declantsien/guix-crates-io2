(define-module (crates-io qa ns qansdk) #:use-module (crates-io))

(define-public crate-qansdk-0.1.0 (c (n "qansdk") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "0l9xp2pgpqyl7f62pywsz7lfvdq0wynfqblxzsvbyz1zk3cir5r5")))

