(define-module (crates-io h5 #{7b}# h57bank) #:use-module (crates-io))

(define-public crate-h57bank-0.1.1 (c (n "h57bank") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rustbreak") (r "^2") (f (quote ("ron_enc"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hjblh4nnc3wmxifx4byfbm9sfnhpb0r14k159fbggrj3a3n4z6s")))

(define-public crate-h57bank-0.1.2 (c (n "h57bank") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rustbreak") (r "^2") (f (quote ("ron_enc"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xpaffdqq43glrsyfaxjl197m3kjp6mhdrzsf56iq95s5bs1i7xq")))

