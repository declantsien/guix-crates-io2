(define-module (crates-io xf lo xflow) #:use-module (crates-io))

(define-public crate-xflow-0.1.1 (c (n "xflow") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0318yb0v50b98kw887q5p9i6zpl81f8q7jqm2lpzag64m6zji7qg") (f (quote (("embedded"))))))

