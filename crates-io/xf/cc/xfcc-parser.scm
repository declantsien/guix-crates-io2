(define-module (crates-io xf cc xfcc-parser) #:use-module (crates-io))

(define-public crate-xfcc-parser-0.1.1 (c (n "xfcc-parser") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zw3704lp1wk4xxfh76rxv2yvxb0jlk6giad877z84w6lzbj1brm")))

(define-public crate-xfcc-parser-0.1.2 (c (n "xfcc-parser") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bxg1y1vyk931jx4m4kx80c670jwsam6k26wka13p18skzvy6z29")))

