(define-module (crates-io xf in xfind) #:use-module (crates-io))

(define-public crate-xfind-0.1.0 (c (n "xfind") (v "0.1.0") (d (list (d (n "memchr") (r "^2.4") (d #t) (k 0)))) (h "1d72f0vspw9sad6ddv9xbh4pyx2v2x1nzh0r7n7li5mcnkvf5lxp") (y #t)))

(define-public crate-xfind-0.1.1 (c (n "xfind") (v "0.1.1") (d (list (d (n "memchr") (r "^2.4") (d #t) (k 0)))) (h "0xhx7bw51lbgdgdnfsfwlcl1jvgk6plk2agdr29iswgiyy9p9yx9") (y #t)))

(define-public crate-xfind-0.1.2 (c (n "xfind") (v "0.1.2") (d (list (d (n "memchr") (r "^2.4") (d #t) (k 0)))) (h "18r4zgh4ws7r1qib3wfdyrghm5kpr714h3605v65bkrilbrxqha0") (y #t)))

(define-public crate-xfind-0.2.0 (c (n "xfind") (v "0.2.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 0)))) (h "1j60d453x6hl9ykaxmpzaybg033kzpr0jm0fciq5byhlkazfki0g")))

(define-public crate-xfind-0.2.1 (c (n "xfind") (v "0.2.1") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 0)))) (h "1kg4x1dipf1alhlsllls07s763glynv1kbdsf82nzy9d6v92kk11")))

(define-public crate-xfind-0.2.2 (c (n "xfind") (v "0.2.2") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 0)))) (h "09kygald4vsjqhrsf0hxhnflazqrwsh97s8zqbkzcxs8xw47rrzv")))

(define-public crate-xfind-0.2.3 (c (n "xfind") (v "0.2.3") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 0)))) (h "01mzbldrdlrq783f8270d1ckfz0430vmyg8hrlw0hy5g449rjg1n")))

(define-public crate-xfind-0.2.4 (c (n "xfind") (v "0.2.4") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 0)))) (h "1rpzkz8zmxl2id0kln7qgbf8jlxgrmydrv6dyygrbz1k4475mvc3")))

(define-public crate-xfind-0.2.5 (c (n "xfind") (v "0.2.5") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 0)))) (h "0ky9962ss7dj7zvm5av0dyvzrbm4mbmwpxhqsdmrch4h30s4p2np")))

(define-public crate-xfind-0.2.6 (c (n "xfind") (v "0.2.6") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 0)))) (h "156vc2ri90v0hz48yz2441kjmq6fph3gcacnkrir22fjfsim4h0m")))

(define-public crate-xfind-0.2.7 (c (n "xfind") (v "0.2.7") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 0)))) (h "1f3rwl87nkcisxb3f0plydsnh1x1n1plhfqkkhd4nb6fnch4alsr")))

