(define-module (crates-io xf la xflags-macros) #:use-module (crates-io))

(define-public crate-xflags-macros-0.1.0 (c (n "xflags-macros") (v "0.1.0") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "0sdngvzip1hc2abq4b38k9fnsjimzlarx7s3m6x833bf1i4zyb7f")))

(define-public crate-xflags-macros-0.1.1 (c (n "xflags-macros") (v "0.1.1") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "00jrp37a0kqrb42wmllhcry8dp0d5md0di5d2xbmww3gf3wnc1nf")))

(define-public crate-xflags-macros-0.1.2 (c (n "xflags-macros") (v "0.1.2") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "17nqddf555mxb9x1kk62bjmhqzj4a6gsbvg45qznb8291va0h8ds")))

(define-public crate-xflags-macros-0.1.3 (c (n "xflags-macros") (v "0.1.3") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1qfhvahn5f417zw8zpaj7627g4r7xnyz24q91pfxbsbcknlniqgq")))

(define-public crate-xflags-macros-0.1.4 (c (n "xflags-macros") (v "0.1.4") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1paxwap6vl8dgzwpap2d1f93hfac2vv794jvklh5xyd799dqzwaj")))

(define-public crate-xflags-macros-0.2.0 (c (n "xflags-macros") (v "0.2.0") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1xng9yg0a4q340r421yb02blfzcvwv5sygj17sc5283x5vgzfx0h")))

(define-public crate-xflags-macros-0.2.1 (c (n "xflags-macros") (v "0.2.1") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1jb2bq76kzzmq5rdyi3hzkq6x41l11fr1yn00f5ib5j9l4y7s0y8")))

(define-public crate-xflags-macros-0.2.2 (c (n "xflags-macros") (v "0.2.2") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1w2vlv6x6fcphywv74dgyddy4960pd1cpzhjj1l137w3v0454180")))

(define-public crate-xflags-macros-0.2.3 (c (n "xflags-macros") (v "0.2.3") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 2)))) (h "1k4zqrg18y1922jkbqk18nh8x917hbdk1sd06d3rmlba8w0h69ns")))

(define-public crate-xflags-macros-0.2.4 (c (n "xflags-macros") (v "0.2.4") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 2)))) (h "08hhj8bmh17bln3a6zyx8qkc9crk0nl0q5wbxpnqfwm9q9givla5")))

(define-public crate-xflags-macros-0.3.0-pre.1 (c (n "xflags-macros") (v "0.3.0-pre.1") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 2)))) (h "1zrv27w6qw4p2svwjxy3j6kvbc2s2kr59hzzn2k83yj009wp5dl4")))

(define-public crate-xflags-macros-0.3.0-pre.2 (c (n "xflags-macros") (v "0.3.0-pre.2") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 2)))) (h "1cxh421nd8638h57ndag8bslb19apgs36k8ywfr42fcmhsbl5a6k")))

(define-public crate-xflags-macros-0.3.0 (c (n "xflags-macros") (v "0.3.0") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 2)))) (h "028q4pa28mb0ga5pd64324k4w3cwykgwbw25vp9cmdlv0grdgyra")))

(define-public crate-xflags-macros-0.3.1 (c (n "xflags-macros") (v "0.3.1") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 2)))) (h "1a6123c3qy1sqjsvlc357b2w8vr161vnlyxqwsm96w4pm0y7p3pm")))

(define-public crate-xflags-macros-0.3.2 (c (n "xflags-macros") (v "0.3.2") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 2)))) (h "0md1sh3h0yadrhjz2mh50xc65p0kx8qh19jvqbva5zx7zva26937")))

(define-public crate-xflags-macros-0.4.0-pre.1 (c (n "xflags-macros") (v "0.4.0-pre.1") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 2)))) (h "0p239idnvp3c9vrn31znc5ibij98m3w5cwsqxmq1fc9nl70qmlcl")))

