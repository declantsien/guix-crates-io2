(define-module (crates-io xf la xflags) #:use-module (crates-io))

(define-public crate-xflags-0.1.0 (c (n "xflags") (v "0.1.0") (d (list (d (n "xflags-macros") (r "=0.1.0") (d #t) (k 0)))) (h "19pbgq3wmixjrnnk7s7xcqbnfx0pcv77dwpb12x4v7n8mird89mx")))

(define-public crate-xflags-0.1.1 (c (n "xflags") (v "0.1.1") (d (list (d (n "xflags-macros") (r "=0.1.1") (d #t) (k 0)))) (h "1ybj7khaiqpa77f5sdan54fs5yc07pbmziy1qywhh6546jjbkglb")))

(define-public crate-xflags-0.1.2 (c (n "xflags") (v "0.1.2") (d (list (d (n "xflags-macros") (r "=0.1.2") (d #t) (k 0)))) (h "1yzs6khbnhgy859xjb7yq289afnx2h48l6x44p5hdz4faawr4qhs")))

(define-public crate-xflags-0.1.3 (c (n "xflags") (v "0.1.3") (d (list (d (n "xflags-macros") (r "=0.1.3") (d #t) (k 0)))) (h "1nx5jq0cb35z9lqp3vzrlqkxpkbgyng1icp1npigh4xq1myb1d6x")))

(define-public crate-xflags-0.1.4 (c (n "xflags") (v "0.1.4") (d (list (d (n "xflags-macros") (r "=0.1.4") (d #t) (k 0)))) (h "0af7mfzg6c96r51dcqlh1v2m4fjbl486s4f5b8qdgiff8d5r2bi2")))

(define-public crate-xflags-0.2.0 (c (n "xflags") (v "0.2.0") (d (list (d (n "xflags-macros") (r "=0.2.0") (d #t) (k 0)))) (h "1fyl8jad233hia9c6a0d0mzdy429wpbzbyl402anrmxx1jsaax4v")))

(define-public crate-xflags-0.2.1 (c (n "xflags") (v "0.2.1") (d (list (d (n "xflags-macros") (r "=0.2.1") (d #t) (k 0)))) (h "12i0m43fmvwhlqid5xbr017c12j7jv3vlkjv04q428mpl3k6rbar")))

(define-public crate-xflags-0.2.2 (c (n "xflags") (v "0.2.2") (d (list (d (n "xflags-macros") (r "=0.2.2") (d #t) (k 0)))) (h "1jb3jch40l6ycj501zixg4yyqm3wi5rwxz8c5czh0bfg1z58anx2")))

(define-public crate-xflags-0.2.3 (c (n "xflags") (v "0.2.3") (d (list (d (n "xflags-macros") (r "=0.2.3") (d #t) (k 0)))) (h "0x9pww5fljpnaj2z3pf53vrhsqvk418p670d8a4777b8gvq58a1c")))

(define-public crate-xflags-0.2.4 (c (n "xflags") (v "0.2.4") (d (list (d (n "xflags-macros") (r "=0.2.4") (d #t) (k 0)))) (h "007fplsi2xqa6smzllrrbpjrwmca99n5hrgmydg2nnhsshggw51z")))

(define-public crate-xflags-0.3.0-pre.1 (c (n "xflags") (v "0.3.0-pre.1") (d (list (d (n "xflags-macros") (r "=0.3.0-pre.1") (d #t) (k 0)))) (h "1mpkl4sl2fjwj9ndbkhnpcmp69b3naxhzbmrapidbv4qx9zx6ry3")))

(define-public crate-xflags-0.3.0-pre.2 (c (n "xflags") (v "0.3.0-pre.2") (d (list (d (n "xflags-macros") (r "=0.3.0-pre.2") (d #t) (k 0)))) (h "02w1jzniqxsyl6pmzm2z5654dp51xi0a6wimgs2hiw0w6ncd1za0")))

(define-public crate-xflags-0.3.0 (c (n "xflags") (v "0.3.0") (d (list (d (n "xflags-macros") (r "=0.3.0") (d #t) (k 0)))) (h "0s6jxyqksqfk35vaikj6k43q7211c60nzqgddzli5a516589zwfb")))

(define-public crate-xflags-0.3.1 (c (n "xflags") (v "0.3.1") (d (list (d (n "xflags-macros") (r "=0.3.1") (d #t) (k 0)))) (h "1sn2y82vsjvmjd4j9rxg3pb28ig3dj7nphb9hciwml120mc4nmf4")))

(define-public crate-xflags-0.3.2 (c (n "xflags") (v "0.3.2") (d (list (d (n "xflags-macros") (r "=0.3.2") (d #t) (k 0)))) (h "0i490jzw7z9riqfnyginkc07j4k7isr19qq6055lamfyngxib7kx")))

(define-public crate-xflags-0.4.0-pre.1 (c (n "xflags") (v "0.4.0-pre.1") (d (list (d (n "xflags-macros") (r "=0.4.0-pre.1") (d #t) (k 0)))) (h "0gx6855vz986m54aji2xgkyglff7j9637vcpz5y2gdygabdw15s6")))

