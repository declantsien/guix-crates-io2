(define-module (crates-io xf et xfetch) #:use-module (crates-io))

(define-public crate-xfetch-0.1.0 (c (n "xfetch") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lru") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0qnaz530vb451v0nv048qdgddyqvjar4cqflm17wbr89x4dfyizc")))

(define-public crate-xfetch-1.0.0 (c (n "xfetch") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "lru") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1z5zi3j0ad81dfqpcaijw0alwz71hbr8f0q5l0cnjxqxyb1fgjvs")))

(define-public crate-xfetch-1.0.1 (c (n "xfetch") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "lru") (r "^0.5.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0wm054pqwwjvxwimj6rn1z3n4c12ys4423sn2scxf2439rfsh59y")))

