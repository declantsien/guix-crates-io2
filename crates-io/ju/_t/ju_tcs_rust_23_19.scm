(define-module (crates-io ju _t ju_tcs_rust_23_19) #:use-module (crates-io))

(define-public crate-ju_tcs_rust_23_19-0.1.0 (c (n "ju_tcs_rust_23_19") (v "0.1.0") (h "0p7rfqqhi2wmkkpr93x4iscx6h4zqa9rhjzg5lnqhzxpmqsj3g1y")))

(define-public crate-ju_tcs_rust_23_19-0.1.1 (c (n "ju_tcs_rust_23_19") (v "0.1.1") (h "0mmzqgmi4gq3f8gddyhkc3ns62sllbjqz187fznas3x4h0hvx2r8")))

