(define-module (crates-io ju nk junk) #:use-module (crates-io))

(define-public crate-junk-0.1.0 (c (n "junk") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.3") (d #t) (k 0)))) (h "05h37xp59zbzzys9mygx939dy31i4cdawgy5jx993fwzq6q41057")))

(define-public crate-junk-0.1.1 (c (n "junk") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.3") (d #t) (k 0)))) (h "15vfzixvpy9nn24pk985aczrmynb3h90dv7940js0pfsc71xihz3")))

(define-public crate-junk-0.1.2 (c (n "junk") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)))) (h "17cmmi14460a5ppbib9j0s707vqnshikww1ksazc8hb9zw7js7ig")))

