(define-module (crates-io ju nk junk_file) #:use-module (crates-io))

(define-public crate-junk_file-0.1.0 (c (n "junk_file") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 1)))) (h "0flzaalqr2b2ykgkwc1kwbx8fyhnq5bxg6ppfyzrlqsg45vxjm0j")))

(define-public crate-junk_file-0.1.1 (c (n "junk_file") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 1)))) (h "04pyjf813zfy6al0s9jnmdm5fnp7fkg21hmihg65kk773h207p4d")))

