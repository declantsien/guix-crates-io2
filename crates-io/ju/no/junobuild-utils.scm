(define-module (crates-io ju no junobuild-utils) #:use-module (crates-io))

(define-public crate-junobuild-utils-0.0.1 (c (n "junobuild-utils") (v "0.0.1") (d (list (d (n "candid") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0yz4phlg7h6zqn86asvh1pawagqjwp1l9fsaw9hk1jf9aird6vy1")))

