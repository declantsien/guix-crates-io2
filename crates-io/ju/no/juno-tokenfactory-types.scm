(define-module (crates-io ju no juno-tokenfactory-types) #:use-module (crates-io))

(define-public crate-juno-tokenfactory-types-0.0.1 (c (n "juno-tokenfactory-types") (v "0.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "token-bindings") (r "^0.10.2") (d #t) (k 0)))) (h "00wr7625wcqlyh30hl3003ix9n5knpps3q58cjc4lpp84gxpjxnn")))

(define-public crate-juno-tokenfactory-types-0.0.2 (c (n "juno-tokenfactory-types") (v "0.0.2") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "token-bindings") (r "^0.10.3") (d #t) (k 0)))) (h "0c0bxqjildlqmmapcl8xy91kap3bb6kvx8h5x2awi7dxdqnbrk3l")))

