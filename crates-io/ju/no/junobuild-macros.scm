(define-module (crates-io ju no junobuild-macros) #:use-module (crates-io))

(define-public crate-junobuild-macros-0.0.1 (c (n "junobuild-macros") (v "0.0.1") (d (list (d (n "candid") (r "^0.10.2") (d #t) (k 2)) (d (n "ic-cdk") (r "^0.12.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "0gzdz0dpd07qzdf6rjwwparngdkpsck0hdc6shh5xizymh1vhs47")))

(define-public crate-junobuild-macros-0.0.2 (c (n "junobuild-macros") (v "0.0.2") (d (list (d (n "candid") (r "^0.10.2") (d #t) (k 2)) (d (n "ic-cdk") (r "^0.12.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "1h8av9sjij141sknjzgfkzzhnwzwn3gllmkndpvg9qpkf9vfs744")))

