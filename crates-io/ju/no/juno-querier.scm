(define-module (crates-io ju no juno-querier) #:use-module (crates-io))

(define-public crate-juno-querier-0.1.0 (c (n "juno-querier") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "06j6czwbk9qr4hgy5n2fk1wi12d57q7zl0a36dps5iv77mhyvm21") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

