(define-module (crates-io ju no juno) #:use-module (crates-io))

(define-public crate-juno-0.1.0 (c (n "juno") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0475hvgy8z67f7sgpcyq2zwdnlzmxjf930hsc1lrsq5pcpdagdnk")))

(define-public crate-juno-0.1.1 (c (n "juno") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ghm8iasxcmbpd4b8d0gyi28inbqmq4n6bwr8lnawfg53crg7nih")))

(define-public crate-juno-0.1.2-beta (c (n "juno") (v "0.1.2-beta") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "10dmar7rxyw6m00bybpcci0csdcxpld24r89jlh80x1gqb5zx3i7")))

(define-public crate-juno-0.1.2 (c (n "juno") (v "0.1.2") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ipwkbl2yvkar5mwnapykcxv01ifq2pg7bzkwkhfljg4pmhwxqq1")))

(define-public crate-juno-0.1.2-1-beta (c (n "juno") (v "0.1.2-1-beta") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0yk4by7m3lilmknxvcjad7cgviwr8qis7pbixin2y19kjvyn59jb")))

(define-public crate-juno-0.1.3-1-beta (c (n "juno") (v "0.1.3-1-beta") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "02d1g1n26wshjlccdwwd7aasz96g9z0c41jc5h2rwl6zfpp9r1pa")))

(define-public crate-juno-0.1.3-2-beta (c (n "juno") (v "0.1.3-2-beta") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1p0giqjp9zhcqwigwysg6m35k6zbq0cn5baavw4gffvllbxmzg68")))

(define-public crate-juno-0.1.3-3-beta (c (n "juno") (v "0.1.3-3-beta") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1g66av6izv6q2j7rarxbn85kc5y3dg0w3z1s826q5snv1dghiyj9")))

(define-public crate-juno-0.1.3-4-beta (c (n "juno") (v "0.1.3-4-beta") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0bz77mymgd80gipxzdkc3whswjsglz13d7ilmjzjyqcw56v8x5d5")))

(define-public crate-juno-0.1.4-beta (c (n "juno") (v "0.1.4-beta") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nmk64rplhzc6c9x41c0c1j9vg80w08qx108sxrfc57hzigq6zl3")))

(define-public crate-juno-0.1.5-beta (c (n "juno") (v "0.1.5-beta") (d (list (d (n "async-std") (r "=1.5.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1mnpmbwss11gs6rx95zjpknsw0a7h9s49xclkxnpsjwh22kzqkpx")))

