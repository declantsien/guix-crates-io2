(define-module (crates-io ju de jude) #:use-module (crates-io))

(define-public crate-jude-0.1.0 (c (n "jude") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1wgacrbziscvp3r2lrrx4ap0sfly74pcvsry4fgmmpky0bqmjnxn")))

(define-public crate-jude-0.1.1 (c (n "jude") (v "0.1.1") (d (list (d (n "libloading") (r "0.8.*") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "02lrsyvcnyab9ffprg6hhfrz4w0k90nhzywcq3jsgwpgrh8232m9")))

(define-public crate-jude-0.1.2 (c (n "jude") (v "0.1.2") (d (list (d (n "libloading") (r "0.8.*") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1bvbskilvknlasasdfkff5kq1nib0771h9kwdhxc750cdddn8z7j")))

