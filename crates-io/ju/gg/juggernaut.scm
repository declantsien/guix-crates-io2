(define-module (crates-io ju gg juggernaut) #:use-module (crates-io))

(define-public crate-juggernaut-0.1.0 (c (n "juggernaut") (v "0.1.0") (h "1w6rd2yyc8qpqdyra7y9mkiwa4dx1bhgallc70zil9y1qbzj94q8")))

(define-public crate-juggernaut-0.2.0 (c (n "juggernaut") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "049sqwacmbd6p92ghrjks494qqdmfl0fwv8y485p64nnbj6c3r6s")))

(define-public crate-juggernaut-0.3.0 (c (n "juggernaut") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0pm2yy8nlq6v04ra03xl53199yjhwil2gmdsn6z8dc2lrx903k5b")))

(define-public crate-juggernaut-0.4.0 (c (n "juggernaut") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0ahs4ilp8dhxz296blz7f9z7iw9habch82mcv9mxadc3kmqpiawf")))

(define-public crate-juggernaut-0.5.0 (c (n "juggernaut") (v "0.5.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0wf3cr41pgbx2znx67isr6xwx757jyl0n52dm1ynpafj7qcnyisq")))

(define-public crate-juggernaut-0.6.0 (c (n "juggernaut") (v "0.6.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1g5plllymbazvllw6bdqmkhf1xbcnyjkjdzhqxf221z86nws7d0p")))

(define-public crate-juggernaut-0.7.0 (c (n "juggernaut") (v "0.7.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0647ppbhla4xzwyvx1fh93zv5x53llr5774jwwwz654iyrgrdmby")))

(define-public crate-juggernaut-0.8.0 (c (n "juggernaut") (v "0.8.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "14q0balq6bqwzwwr08ag1nqqxhws2j5l9ihgpkn8axzsx2jyql4m")))

(define-public crate-juggernaut-0.8.1 (c (n "juggernaut") (v "0.8.1") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0sxrhdgpxyqpgnplyzcj9h8hhzx34q7f1af4qhlvznchvzdvyl9l")))

(define-public crate-juggernaut-0.9.0 (c (n "juggernaut") (v "0.9.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "03a4kb2g9xmq78vb2wv392l4qh0qinkkcha43z6gmddfp5pimn4r")))

