(define-module (crates-io ju st just_state) #:use-module (crates-io))

(define-public crate-just_state-0.1.3 (c (n "just_state") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macros") (r "^0.1.1") (d #t) (k 0) (p "just_macros")) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "13xpf3w8lkliap544ahcgflzs0cblbkmclp2r8sjqqjyv4ci1vry")))

(define-public crate-just_state-0.1.4 (c (n "just_state") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macros") (r "^0.1.1") (d #t) (k 0) (p "just_macros")) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "0bwx5jzpffgs4x0hzb7snk6s0yx4cay9mgcfyhp60qa34jhihmyf")))

(define-public crate-just_state-0.2.0 (c (n "just_state") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macros") (r "^0.1.1") (d #t) (k 0) (p "just_macros")) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "1736rq5ci3n795in3vh33dxmp6s1a2w85wsjq85s16wxnlis5fn4")))

