(define-module (crates-io ju st just-watch) #:use-module (crates-io))

(define-public crate-just-watch-0.2.0 (c (n "just-watch") (v "0.2.0") (d (list (d (n "async-executor") (r "^1.5.1") (d #t) (k 2)) (d (n "event-listener") (r "^2.5.3") (d #t) (k 0)))) (h "08ll0a0q64gl4dbm838j6902imj66470ajkb11sn3l2lf33f4ara")))

