(define-module (crates-io ju st just-a-tag) #:use-module (crates-io))

(define-public crate-just-a-tag-0.1.0 (c (n "just-a-tag") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.171") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "0y74n7vsqzy7niq4br3jiivblhxy6bn1w46lbz1s86ylcm9a32dc") (f (quote (("unsafe")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-just-a-tag-0.1.1 (c (n "just-a-tag") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.171") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "0imsvf75hd7zawrfrdmczs28nkmwpqlv5ysiyiipsvp01d7prw3g") (f (quote (("unsafe")))) (s 2) (e (quote (("serde" "dep:serde"))))))

