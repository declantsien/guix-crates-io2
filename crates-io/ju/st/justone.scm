(define-module (crates-io ju st justone) #:use-module (crates-io))

(define-public crate-justone-0.1.0 (c (n "justone") (v "0.1.0") (h "0vsjmyphs90b876l2adsb5s42gyl54h1x64707w5nhizsjng9z24")))

(define-public crate-justone-0.2.0 (c (n "justone") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "filecmp") (r "^0.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1j789j1r7jmw9sjcpf019jl426in46rgdfzih2qg4jnyxf3r25h0")))

(define-public crate-justone-0.3.0 (c (n "justone") (v "0.3.0") (d (list (d (n "clap") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "filecmp") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "indicatif") (r ">=0.15.0, <0.16.0") (d #t) (k 0)) (d (n "twox-hash") (r ">=1.6.0, <2.0.0") (d #t) (k 0)) (d (n "walkdir") (r ">=2.0.0, <3.0.0") (d #t) (k 0)))) (h "1hjdzkk5aa6gmm00wllhgkxla2k6qh8ld59vs671zn9zpizvyabn")))

