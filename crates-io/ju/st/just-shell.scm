(define-module (crates-io ju st just-shell) #:use-module (crates-io))

(define-public crate-just-shell-0.1.0 (c (n "just-shell") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "0qajahvpkl9hmq2bds6847d1lcyd5immyx9fpacrs21la6w79x62")))

(define-public crate-just-shell-0.1.1 (c (n "just-shell") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "0m42w4z0lyc3s76rcj6n36xisyg0kgnlpam9y8qc363h9yva1v5x")))

(define-public crate-just-shell-0.1.2 (c (n "just-shell") (v "0.1.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "03h0nrakz4bqg7qbakwjcxilhk05i5ijqzppan1gvgv95q3yk4qh")))

