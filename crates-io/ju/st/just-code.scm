(define-module (crates-io ju st just-code) #:use-module (crates-io))

(define-public crate-just-code-0.1.1 (c (n "just-code") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "simple-home-dir") (r "^0.2.0") (f (quote ("expand_tilde"))) (d #t) (k 0)) (d (n "simple-home-dir") (r "^0.2.0") (f (quote ("expand_tilde"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)) (d (n "tuple-map") (r "^0.4.0") (d #t) (k 0)))) (h "1jds2dyr62fc2nii3c6dczbd9mzfs4s323i42ia53pnr7389dyyx")))

