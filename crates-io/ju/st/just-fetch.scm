(define-module (crates-io ju st just-fetch) #:use-module (crates-io))

(define-public crate-just-fetch-0.1.0 (c (n "just-fetch") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "19pji6hw031k7q2lax92f8g9vbxdj86p5yhc3pid566ylk9mvpc5")))

(define-public crate-just-fetch-0.1.1 (c (n "just-fetch") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "06l20rirqgg06zr64l3kdk649gvz4kyx3z97jgxck49jxk5adzbp")))

(define-public crate-just-fetch-0.1.2 (c (n "just-fetch") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "flate2") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (o #t) (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "1qkp2gjcgp0rvk88slmyfcb9wy9nask854gzdsf235k69pwdqngn")))

