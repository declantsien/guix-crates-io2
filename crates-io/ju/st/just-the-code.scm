(define-module (crates-io ju st just-the-code) #:use-module (crates-io))

(define-public crate-just-the-code-1.0.0 (c (n "just-the-code") (v "1.0.0") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (f (quote ("parse"))) (d #t) (k 0)))) (h "0dvlzns7lb60zi1h4h5xrlpmy0pr9fbr6avd92y3dw9bis2g41z8")))

