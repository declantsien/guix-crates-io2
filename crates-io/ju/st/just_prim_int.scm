(define-module (crates-io ju st just_prim_int) #:use-module (crates-io))

(define-public crate-just_prim_int-0.1.0 (c (n "just_prim_int") (v "0.1.0") (h "13ahyj2n8mk6b7kxc7c5spbq2j465mixg9bxbkjq06y5w2fa9kc6") (f (quote (("marker_trait_attr")))) (r "1.56.1")))

(define-public crate-just_prim_int-0.1.1 (c (n "just_prim_int") (v "0.1.1") (h "15bdp126qp3drcrc4xinras4bq6d165chx4gi2dghyjhy2pbc8lg") (f (quote (("marker_trait_attr")))) (r "1.56.1")))

