(define-module (crates-io ju st justerror) #:use-module (crates-io))

(define-public crate-justerror-0.1.0 (c (n "justerror") (v "0.1.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1ks3dkasn94xxzric69gm0zsdrm4bl7a4lh1mskj4p5bgp1xhvzq") (r "1.31")))

(define-public crate-justerror-1.0.0 (c (n "justerror") (v "1.0.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "10fa90w872ngjq2ahp70aqx1fy4x2ibm7nvg46h3qzn2sh0lgq6s") (r "1.31")))

(define-public crate-justerror-1.1.0 (c (n "justerror") (v "1.1.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0ah6w7jynrjg72ixzmwfl5h6mc07j4g0lwrjks47kxizil3v5ymy") (r "1.31")))

