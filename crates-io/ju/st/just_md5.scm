(define-module (crates-io ju st just_md5) #:use-module (crates-io))

(define-public crate-just_md5-0.1.0 (c (n "just_md5") (v "0.1.0") (h "1yb87n6pa09qrrww24yrr72ybq6lfxg9zq4rndj9h0ahp9pm6g4v") (y #t)))

(define-public crate-just_md5-0.1.1 (c (n "just_md5") (v "0.1.1") (h "157d5gfcxy09cpvwpaazpj1x3254ppar9hbhzjqn2srfqb03c6ba") (y #t)))

(define-public crate-just_md5-0.1.2 (c (n "just_md5") (v "0.1.2") (h "0ampspaa5lxpkrnfniig5pv7vzcmz1m8jgfzb4asv4gc4yvwc21a") (y #t)))

(define-public crate-just_md5-0.1.3 (c (n "just_md5") (v "0.1.3") (h "1qakdjqgrf1skavw5i9hr9p14cpl5dp6l2a0iwlr5wlzb42dk89f") (y #t)))

(define-public crate-just_md5-0.1.4 (c (n "just_md5") (v "0.1.4") (h "0b1jvisn50axnsvxq004mavv6rd9ah7cbvdsjihv08hhw4b7s65r") (y #t)))

