(define-module (crates-io ju st justify) #:use-module (crates-io))

(define-public crate-justify-0.1.0 (c (n "justify") (v "0.1.0") (d (list (d (n "wcwidth") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "0ba9cpidv66ai4k5mjls7h7vb8psapr94sp8bpgx8bar3ifbnj11") (f (quote (("unicode-width" "wcwidth") ("default"))))))

(define-public crate-justify-0.1.1 (c (n "justify") (v "0.1.1") (d (list (d (n "wcwidth") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "0mg9jbjmn58q0k0q4a163dn3nb2h8x9yzn133idpjdp7wc4b9md1") (f (quote (("unicode-width" "wcwidth") ("default"))))))

(define-public crate-justify-0.1.2 (c (n "justify") (v "0.1.2") (d (list (d (n "wcwidth") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "0cg27i1v5izjyfwigmm9famgdava07ijd35am4ycnj0d65jb88v7") (f (quote (("unicode-width" "wcwidth") ("default"))))))

(define-public crate-justify-0.1.3 (c (n "justify") (v "0.1.3") (d (list (d (n "unicode-width") (r "^0.1") (o #t) (d #t) (k 0)))) (h "05rhhnwnq8jypy4vjks097lfkfvx31z8s0i2b3yvd55djv8xxia7") (f (quote (("default"))))))

