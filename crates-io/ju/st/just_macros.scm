(define-module (crates-io ju st just_macros) #:use-module (crates-io))

(define-public crate-just_macros-0.1.1 (c (n "just_macros") (v "0.1.1") (h "0gz9rw6ix1n1sldyvng3l3dxqbwgdkkpzk06i03l6msi333fqm88") (y #t)))

(define-public crate-just_macros-0.1.2 (c (n "just_macros") (v "0.1.2") (h "18jbgs220yvrkbsnjd3hx33jdhkp355s7ya7nszwk8ar1jqii2ll") (y #t)))

(define-public crate-just_macros-0.2.0 (c (n "just_macros") (v "0.2.0") (h "16drs7a0b4fg77bq9wmj7ijjy4qjr3mw2gsvwjx95s4g74vip16l") (y #t)))

(define-public crate-just_macros-0.2.1 (c (n "just_macros") (v "0.2.1") (h "0hkfqar9c07iwmbwba7q7bz85hmb1cchgsrk8wj8ki9sakgklh1x") (y #t)))

(define-public crate-just_macros-0.2.2 (c (n "just_macros") (v "0.2.2") (h "1szayyjzyqxzgcd5c1pjd2djm7rppfbw756nipz32fgr3k4143ip") (y #t)))

(define-public crate-just_macros-0.3.0 (c (n "just_macros") (v "0.3.0") (h "15bfvdzjz3rylm7x0ppa26v9qr0fjciy34jb3lwh9yjc44l4wbaq") (y #t)))

(define-public crate-just_macros-1.1.1 (c (n "just_macros") (v "1.1.1") (h "0sksi7ba9j1qhf26kxkwfs3vjmlwk4ngh37qgm9v2nvdgjld7gfg") (y #t)))

(define-public crate-just_macros-1.2.3 (c (n "just_macros") (v "1.2.3") (h "0crpg7hv7n0dz426hh7w5897h86qlnw7rxgnxf78aab25rmffgq6")))

