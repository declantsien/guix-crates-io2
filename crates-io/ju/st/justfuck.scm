(define-module (crates-io ju st justfuck) #:use-module (crates-io))

(define-public crate-justfuck-0.1.0 (c (n "justfuck") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "1qwdgh6jjkfib73wz3gi0lid90qp42jlyg83h9jsps9xnpayqm0i")))

(define-public crate-justfuck-0.1.1 (c (n "justfuck") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "0q4lyj24yc95m2dnwp42yisnwa1cd3kj0np7b4r94bnpbys7dmmf")))

(define-public crate-justfuck-0.1.2 (c (n "justfuck") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "018i5vbsc5xacd8acpgd41694wlxlwy0ixbnxjvqliyb6b8fiwma")))

