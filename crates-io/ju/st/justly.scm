(define-module (crates-io ju st justly) #:use-module (crates-io))

(define-public crate-justly-0.1.0 (c (n "justly") (v "0.1.0") (h "1fwwm1xp3acbl1g88r4xj8fyxmdi3w1rayvyy6qcajwsplnq3anm")))

(define-public crate-justly-0.1.1 (c (n "justly") (v "0.1.1") (h "1i9vyq2fs0ad0rwx4wqdf1vrbm4kv8mqmhc4vf1irdwjh3bm8cib")))

