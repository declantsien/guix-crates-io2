(define-module (crates-io ju st justconfig) #:use-module (crates-io))

(define-public crate-justconfig-0.8.0 (c (n "justconfig") (v "0.8.0") (h "1sdg71s8mqafqgqdaz98w10nrn6dz0lg7xvqihs6q52i8zk429ym")))

(define-public crate-justconfig-0.8.1 (c (n "justconfig") (v "0.8.1") (h "0sila6sq4a5h0wkb619afc6cgw5zwim7azbcy248ld42zdxn7hjp")))

(define-public crate-justconfig-0.9.0 (c (n "justconfig") (v "0.9.0") (h "19dj1dvj2g5s94vff7mdx7v1q1w4ypa96dawypj80f830djac5vn")))

(define-public crate-justconfig-0.9.1 (c (n "justconfig") (v "0.9.1") (h "1c5jy8m0ix9g6qpgwdkgq1nn1z9h7dw98a4j24pjrz2jr1mzmwhn")))

(define-public crate-justconfig-0.9.2 (c (n "justconfig") (v "0.9.2") (h "0xcq0ndy84b6s5rz8186f9wbxipdji15xsna3alk1n77rivvrn3f")))

(define-public crate-justconfig-1.0.0 (c (n "justconfig") (v "1.0.0") (h "18psfmqgsp6xy9v9y1i8pa6gnaccp33p0ymqa794zdy9ry1s4fzp")))

(define-public crate-justconfig-1.0.1 (c (n "justconfig") (v "1.0.1") (h "0h443jcbl9zrx0wnp7q6kp12f0jivs4jp0scwc8ikbpdkzv63mg6")))

