(define-module (crates-io ju st justenoughmod) #:use-module (crates-io))

(define-public crate-JustEnoughMod-0.0.1 (c (n "JustEnoughMod") (v "0.0.1") (d (list (d (n "JustEnoughMod_core") (r "^0.0.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("wayland"))) (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "12qsg85pnmzlp21wzkbgyy8ah16r3kl74fpl73afj7p6pgghwpc7")))

