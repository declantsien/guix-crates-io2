(define-module (crates-io ju st just-argon2) #:use-module (crates-io))

(define-public crate-just-argon2-1.0.0 (c (n "just-argon2") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "049gyw533kwk56d2wrs14xd63z1h5sxzkfwxac2x7vwhn6hkg04h")))

(define-public crate-just-argon2-1.1.0 (c (n "just-argon2") (v "1.1.0") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "19jsgra3il21b2w065n0yz6md9dqy1swbsijlsfig8dvzlsz79i0")))

(define-public crate-just-argon2-1.2.0 (c (n "just-argon2") (v "1.2.0") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "0wbgprx2cxj106ifqbmffv6i0q698bwj749cgwih7kjzyzxsq4kf")))

