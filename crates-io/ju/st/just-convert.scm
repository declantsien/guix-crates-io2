(define-module (crates-io ju st just-convert) #:use-module (crates-io))

(define-public crate-just-convert-0.1.1 (c (n "just-convert") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1rv45dvghq1c4zrmwd9bw4awgj1wrxdyss3w5388bfznma98nkls")))

(define-public crate-just-convert-0.1.2 (c (n "just-convert") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1jxhnfkywifvdipbra96x7a72sh5n5jyiv3xlhyabg3rngikjhkn")))

(define-public crate-just-convert-0.1.3 (c (n "just-convert") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "19is5bpp1ssgwn9bcjqi3rbrkr9i91rdkl7vj0x8ydg4x3ras80b")))

(define-public crate-just-convert-0.1.4 (c (n "just-convert") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0m9al1934gdqmprmv74m673i34gmhnnas9vw6lgiw5631hhvgwli")))

(define-public crate-just-convert-0.1.5 (c (n "just-convert") (v "0.1.5") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "033hjw3zbqzrl4hzi6jp7ja10v7r2129bj0lzwkf851wmkhgdr6y")))

(define-public crate-just-convert-0.1.6 (c (n "just-convert") (v "0.1.6") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "010br9vklx3bmacwy79jbcwb1lk5fl0d4n9h70dmhlzl0962hp6c")))

