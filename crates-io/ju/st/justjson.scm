(define-module (crates-io ju st justjson) #:use-module (crates-io))

(define-public crate-justjson-0.1.0 (c (n "justjson") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1mb21aw6mqc1zzb5v8s66i9xjrx3p7fbhm38acpls032pbi1l13l") (y #t) (r "1.58")))

(define-public crate-justjson-0.1.1 (c (n "justjson") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0nxpy80hjhddia7aa1mdmgm0bc03yhw29f9vfa4bb8zf0q9h40wg") (r "1.58")))

(define-public crate-justjson-0.2.0 (c (n "justjson") (v "0.2.0") (d (list (d (n "heapless") (r "^0.7.16") (o #t) (d #t) (k 0)))) (h "1jgq4wvgpjqddbhf6q2jm1gs1vnh6r6n1f10jag24p3lgpnhcj6j") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (r "1.58")))

(define-public crate-justjson-0.2.1 (c (n "justjson") (v "0.2.1") (d (list (d (n "heapless") (r "^0.7.16") (o #t) (d #t) (k 0)))) (h "0x1k5ibky061grlcyzsr2mjizjfw1gcznknxzdx90x816m6hjqxz") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (r "1.58")))

(define-public crate-justjson-0.2.2 (c (n "justjson") (v "0.2.2") (d (list (d (n "heapless") (r "^0.7.16") (o #t) (d #t) (k 0)))) (h "06irsqvhhbn9v2d7764srbhx0gk3bl8sz96alvsma9w0hnm5rflv") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (r "1.58")))

(define-public crate-justjson-0.2.3 (c (n "justjson") (v "0.2.3") (d (list (d (n "heapless") (r "^0.7.16") (o #t) (d #t) (k 0)))) (h "10hxs6475dylkl8yzqx1a0bkzql44ncr2rjq3zcisb0vc9p0xbii") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-justjson-0.3.0 (c (n "justjson") (v "0.3.0") (d (list (d (n "heapless") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "0x8hm9l40g5wbwxmcayllishfbzp33y73i3mg6yhwpbpfjsm7qnp") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (r "1.65")))

