(define-module (crates-io ju ju juju) #:use-module (crates-io))

(define-public crate-juju-0.1.0 (c (n "juju") (v "0.1.0") (h "05w2lbqqhdq4w264cdk9ilj22f4bqw4y8bz3hyn9kyzkvvy1qz6v")))

(define-public crate-juju-0.1.2 (c (n "juju") (v "0.1.2") (h "1gc6yvrqvb1a35m21h5f0d0v7bwzpwfz73mhsz0bnwywz7b2iv5l")))

(define-public crate-juju-0.1.3 (c (n "juju") (v "0.1.3") (h "0v0xbv02ahbkplycm70bsfm1zzcdcmrc10n84jvrr0c2q230afh6")))

(define-public crate-juju-0.1.4 (c (n "juju") (v "0.1.4") (h "186bw91z9cibkpbjz2y37szi0yl6h2bfvk0nqkziml6kzzpf3jwy")))

(define-public crate-juju-0.1.5 (c (n "juju") (v "0.1.5") (h "14810bzjw7vka6jmyd8x3a9z1i8v5fwk4yppk3jikwvj9sv71iyd")))

(define-public crate-juju-0.2.0 (c (n "juju") (v "0.2.0") (h "12rvaq1jcrpzy6jcg1bdl5dnzhwk1i4rzmlzh04pr8z9209dx8r8")))

(define-public crate-juju-0.2.1 (c (n "juju") (v "0.2.1") (h "0vp8dxvs1qyx6mihc3inbc68cfkb033fpldpdy9g0amjpy9lm6wi")))

(define-public crate-juju-0.3.0 (c (n "juju") (v "0.3.0") (h "1gl75m4df8glyn4y2jlfssv0qa5jy00n17m64gmidfjd2jqxxg5a")))

(define-public crate-juju-0.3.1 (c (n "juju") (v "0.3.1") (h "0whwi4ghgbwhxba6v6fgclvpqns81n1rvm7px5h6rzpwj0nnkd6h")))

(define-public crate-juju-0.4.0 (c (n "juju") (v "0.4.0") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)))) (h "1d2v7d85ipvy0r8ylwp24kgl5x4mqh4yphb0hflvwhqfijfkiahl")))

(define-public crate-juju-0.5.0 (c (n "juju") (v "0.5.0") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "07nw7xnjgdj25xnmlcqcm5wkajc77b9bdg99paq5sr698jmicmvy")))

(define-public crate-juju-0.5.1 (c (n "juju") (v "0.5.1") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "1a3yxagbb4pgngryd15d6g4npy3ichnxlkx3pa2y4l58qpxpdx0y")))

(define-public crate-juju-0.5.2 (c (n "juju") (v "0.5.2") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "0f7jbrmbinj6mfx9ljmapkl855i29m7az8rxalrxj85fszpyisl7")))

(define-public crate-juju-0.5.3 (c (n "juju") (v "0.5.3") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "04s9rlwadqcmcb567wjgsfxag6gqczks5bsk3z09gj3jqxz4prgi")))

(define-public crate-juju-0.5.4 (c (n "juju") (v "0.5.4") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "15i3xh730hkkb8bb64ni6farycngjxpf4ma58cc6cnnx4i8344rf")))

(define-public crate-juju-0.5.5 (c (n "juju") (v "0.5.5") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "1p1qmjszvkc0rp9pwy03ggndpxfmjnrbzjlmyxqjf3snlcqlfca1")))

(define-public crate-juju-0.5.6 (c (n "juju") (v "0.5.6") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "177cai4sf7y9lc4lpy2x2ca3wbkyah5v7r2pad0xg3zc01d9p941")))

(define-public crate-juju-0.5.7 (c (n "juju") (v "0.5.7") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)))) (h "1haaf1nr7kamh40pm3r11qqipaj33kpbqh65kcq7hwm68zijakg4")))

(define-public crate-juju-0.5.8 (c (n "juju") (v "0.5.8") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)))) (h "11wc9nm02b95v2lmcmxl8ngjx07m57d1rq8j0kgq417xg6rl1qp7")))

(define-public crate-juju-0.5.9 (c (n "juju") (v "0.5.9") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "memchr") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)))) (h "1690nwr5fk88r16j5fhy76q76v2vv5gz5skwzrs78qb18hi90hsj")))

(define-public crate-juju-0.5.10 (c (n "juju") (v "0.5.10") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "memchr") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)))) (h "1yq5ahbvqzabllbcw4ign3k3brpiifgm40s00vcgcynj4k0cxlkg")))

(define-public crate-juju-0.5.11 (c (n "juju") (v "0.5.11") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "memchr") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)))) (h "04s0q1ql4lxv62j3d7zg5irl2z5lwar7kvrkfjxm872ihqyhc3za")))

(define-public crate-juju-0.5.12 (c (n "juju") (v "0.5.12") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "memchr") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)))) (h "0x9ikcbn5ckanydl7m0crf6qlk8k8brmvpzzxj1ffjj99vmpq55y")))

(define-public crate-juju-0.5.14 (c (n "juju") (v "0.5.14") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "memchr") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)))) (h "0ygn34w2gi3dmbn7nrak29fxh1pmb31gjrrylga488vr4s23a7b9")))

(define-public crate-juju-0.5.15 (c (n "juju") (v "0.5.15") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "memchr") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)))) (h "15n7dp4iaqcmr9yaq9b9w5jwdv27s167vf3zc12m3zvqsxcjqh9v")))

(define-public crate-juju-0.6.0 (c (n "juju") (v "0.6.0") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "chrono") (r "~0.3") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "memchr") (r "~1.0") (d #t) (k 0)) (d (n "rusqlite") (r "~0.10") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "~0.9") (d #t) (k 0)) (d (n "serde_derive") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~0.9") (d #t) (k 0)))) (h "0vc38206gd62gpbk2acj76wr4v0g3zp3rsivn0802nc7az0dbcfn")))

(define-public crate-juju-0.6.1 (c (n "juju") (v "0.6.1") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "chrono") (r "~0.3") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "memchr") (r "~1.0") (d #t) (k 0)) (d (n "rusqlite") (r "~0.10") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "~0.9") (d #t) (k 0)) (d (n "serde_derive") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~0.9") (d #t) (k 0)))) (h "1zqpsnyy8wk44rq9sv3hyrhnzzchmm48aqy0i3b258byx9n0id28")))

(define-public crate-juju-0.7.0 (c (n "juju") (v "0.7.0") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "chrono") (r "~0.3") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "memchr") (r "~1.0") (d #t) (k 0)) (d (n "rusqlite") (r "~0.10") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "~0.9") (d #t) (k 0)) (d (n "serde_derive") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~0.9") (d #t) (k 0)))) (h "1v1wgkil28gxw0ynlvl5dds36bghsxgdpjyb3bldnjgysq1cw664")))

(define-public crate-juju-1.0.0 (c (n "juju") (v "1.0.0") (d (list (d (n "charmhelpers") (r "~0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "memchr") (r "~1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.14.0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "~0.9") (d #t) (k 0)) (d (n "serde_derive") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~0.9") (d #t) (k 0)))) (h "0v4krrlsppm63imzdli67qq3qzw0nvqqmarz305k7yis1rllhfrk")))

