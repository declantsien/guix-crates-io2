(define-module (crates-io ju dy judy) #:use-module (crates-io))

(define-public crate-judy-0.0.1 (c (n "judy") (v "0.0.1") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)))) (h "111yalrpw30rfhmfilxsnaz0cx24nircpjy7hr9iknfqrjkqvsmh") (y #t)))

(define-public crate-judy-0.0.2 (c (n "judy") (v "0.0.2") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)))) (h "1x59nccyh2rcsxy4cb2p78f3aac0a0p5lr8300x9sa5m2xbcqwik")))

(define-public crate-judy-0.0.3 (c (n "judy") (v "0.0.3") (d (list (d (n "libc") (r "~0.2.6") (d #t) (k 0)))) (h "1q8rzgz44pf38ddzddb9y67vxslp6xm7k426gamzym2xly2qwgj4")))

