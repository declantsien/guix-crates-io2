(define-module (crates-io ju dy judy-wrap) #:use-module (crates-io))

(define-public crate-judy-wrap-0.0.1 (c (n "judy-wrap") (v "0.0.1") (d (list (d (n "judy-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yrfam5fxlabscsj5a4gdygpbvys9k6kqnkq8ky620q2dkxzv0yv")))

(define-public crate-judy-wrap-0.0.2 (c (n "judy-wrap") (v "0.0.2") (d (list (d (n "judy-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ibjwxg9150vkx0j0p2ny08fjhqskzssni2m7ng8ylwizlfy439l")))

(define-public crate-judy-wrap-0.0.3 (c (n "judy-wrap") (v "0.0.3") (d (list (d (n "judy-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v92mrc902j0zf48v9yd34q1g4z5306srjq6q24ph0r57knjr4gj")))

(define-public crate-judy-wrap-0.0.4 (c (n "judy-wrap") (v "0.0.4") (d (list (d (n "judy-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "035v8d2y2yarg53yalh7vwgivkq147hp4gizvbpjrd587bglvxfq")))

(define-public crate-judy-wrap-0.0.5 (c (n "judy-wrap") (v "0.0.5") (d (list (d (n "judy-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1492k34c7msp9vcpdjixbagmvw2mcc9hfy42rwgdkim0bg3hb9px")))

(define-public crate-judy-wrap-0.0.6 (c (n "judy-wrap") (v "0.0.6") (d (list (d (n "judy-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pdl38wspf0dyf5df4s584blsim1hx6jbs7dwa8z40h1hq2mrzvr")))

(define-public crate-judy-wrap-0.0.7 (c (n "judy-wrap") (v "0.0.7") (d (list (d (n "judy-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "166m3l3pi7207mciz96yvnhjwmx4w1pz81l70nw4cng5h34iclix")))

