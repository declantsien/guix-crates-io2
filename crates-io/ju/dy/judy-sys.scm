(define-module (crates-io ju dy judy-sys) #:use-module (crates-io))

(define-public crate-judy-sys-0.0.1 (c (n "judy-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08rvb126klnz4whhbaiq173b0kw9wqqgmd401akz4x2g16jpy99y") (y #t)))

(define-public crate-judy-sys-0.0.2 (c (n "judy-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xsxxlgw0lqfp6cmzyrcll89xxhnwi7qyvpvdbqx249wv026j05a")))

(define-public crate-judy-sys-0.0.3 (c (n "judy-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "065ssi898dhv44jmdjxiy8dl1pr6hnlk9kmjb2xa2r8ilvdiy6sv")))

(define-public crate-judy-sys-0.0.4 (c (n "judy-sys") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05w17s4b7nr3r5rdpn775asgjbjcixqjm9kw5gqxbfdrzm10gngv")))

