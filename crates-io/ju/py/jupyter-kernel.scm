(define-module (crates-io ju py jupyter-kernel) #:use-module (crates-io))

(define-public crate-jupyter-kernel-0.1.0 (c (n "jupyter-kernel") (v "0.1.0") (d (list (d (n "chan") (r "^0.1.14") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.5.3") (d #t) (k 0)) (d (n "zmq") (r "^0.7.0") (d #t) (k 0)))) (h "06c4chm2j28ix8lq02wg3zmiv4g9g9l72s2frwl2133s3frgligl")))

