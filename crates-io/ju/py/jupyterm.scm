(define-module (crates-io ju py jupyterm) #:use-module (crates-io))

(define-public crate-jupyterm-0.1.0 (c (n "jupyterm") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.12.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "067x3hh1mv8jnpmahlh0gxc4wbbqw606yvkzz96s6l02c9k6bayz")))

