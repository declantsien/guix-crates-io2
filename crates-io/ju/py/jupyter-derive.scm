(define-module (crates-io ju py jupyter-derive) #:use-module (crates-io))

(define-public crate-jupyter-derive-0.0.0 (c (n "jupyter-derive") (v "0.0.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0x3pa1362hyyd085ls9q56gqpi7dy14vrfisvxincsdg3bcb37a2")))

(define-public crate-jupyter-derive-0.0.1 (c (n "jupyter-derive") (v "0.0.1") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "1k7r8lk5f5ngg71gmib6w29j5y73iidfpbv4saf17y24nfxsy3b7")))

(define-public crate-jupyter-derive-0.0.2 (c (n "jupyter-derive") (v "0.0.2") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "07m2p8qm3jq0r58dx2sjp3p8pywyjchypri8wrpwjnrs5872psb8")))

