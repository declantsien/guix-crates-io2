(define-module (crates-io ju nc junco) #:use-module (crates-io))

(define-public crate-junco-1.3.0 (c (n "junco") (v "1.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "swiftnav") (r "^0.8") (f (quote ("chrono"))) (d #t) (k 0)))) (h "11g7csld4xmljv2xv18dk65inm6323s1wgfs1fdxwjsm35x363sz") (y #t)))

