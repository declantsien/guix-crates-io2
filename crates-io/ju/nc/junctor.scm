(define-module (crates-io ju nc junctor) #:use-module (crates-io))

(define-public crate-junctor-0.0.1 (c (n "junctor") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 0)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "07rc3plp9708h3w8jrlllaj0bmsp8lax86654y36hsjajfmi3xh8")))

