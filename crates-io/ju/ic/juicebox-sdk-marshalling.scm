(define-module (crates-io ju ic juicebox-sdk-marshalling) #:use-module (crates-io))

(define-public crate-juicebox-sdk-marshalling-0.0.1 (c (n "juicebox-sdk-marshalling") (v "0.0.1") (d (list (d (n "ciborium") (r "^0.2.0") (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("alloc" "derive"))) (k 0)))) (h "1as4x42yg51f65fzflrvlqb3mkw1g5jfk5zi0sianlkfrvx19nl5")))

