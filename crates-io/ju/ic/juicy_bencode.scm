(define-module (crates-io ju ic juicy_bencode) #:use-module (crates-io))

(define-public crate-juicy_bencode-0.1.0 (c (n "juicy_bencode") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1r3s9qczv1x8xd1nydq4r1bm07dgrwp28vp6sab2q9bjivsvdni3")))

(define-public crate-juicy_bencode-0.1.1 (c (n "juicy_bencode") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1rhqdbcgvcq1frj0dnkvk7sjsgqm3nbsidpfk1a0rj748pca13ax")))

(define-public crate-juicy_bencode-0.1.2 (c (n "juicy_bencode") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1jxnjmcsdjmypf4sbndwnkyff2jxg7hfn2nzhdmbh2q0mwml1nj5")))

