(define-module (crates-io ju ke juke) #:use-module (crates-io))

(define-public crate-juke-0.0.1 (c (n "juke") (v "0.0.1") (d (list (d (n "minifb") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0nsnn2kdr6sjm48l5sqw5w6pc8ffc1vpj7g8qlc4agr1ldyzql90")))

(define-public crate-juke-0.0.11 (c (n "juke") (v "0.0.11") (d (list (d (n "glam") (r "^0.20.5") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1n7d8idajijib2m167z15zp1n0jb4kad0sgajd92kx0c719l2l18")))

(define-public crate-juke-0.0.111 (c (n "juke") (v "0.0.111") (d (list (d (n "glam") (r "^0.20.5") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0qx7qgdma26p68scsy83hwbpn8307fshavqiaxsa577qajvzp66s") (y #t)))

(define-public crate-juke-0.0.12 (c (n "juke") (v "0.0.12") (d (list (d (n "egui") (r "^0.17") (d #t) (k 0)) (d (n "egui-winit") (r "^0.17") (f (quote ("links"))) (k 0)) (d (n "egui_wgpu_backend") (r "^0.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.12") (d #t) (k 0)))) (h "1mmspfmgyyzc5y4072kyswpsx4rlkyiag91vs3crv02virlk4hfj") (f (quote (("optimize" "log/release_max_level_warn") ("default" "optimize"))))))

