(define-module (crates-io ju ne june) #:use-module (crates-io))

(define-public crate-june-0.1.0 (c (n "june") (v "0.1.0") (d (list (d (n "actix") (r "^0.13.0") (d #t) (k 0)) (d (n "canary") (r "^0.3.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "maquio") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "1ihgiz39bg7725cbr072fj0dqzi6yawmywkj4bqcqdz0pw245x7c") (f (quote (("maquio-routing" "maquio"))))))

