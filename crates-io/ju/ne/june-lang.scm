(define-module (crates-io ju ne june-lang) #:use-module (crates-io))

(define-public crate-june-lang-0.1.0 (c (n "june-lang") (v "0.1.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "wasmtime") (r "^15.0.0") (d #t) (k 0)))) (h "1bdzf5iw5wb633dn3g5nm7nzm203lsb1xy19sjdndq5lj3s7xg57")))

