(define-module (crates-io ju li julia-sys) #:use-module (crates-io))

(define-public crate-julia-sys-0.1.0 (c (n "julia-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vyzav9fq6fynsnvb86134mvbcnr79h6q87nwdzjmr544pg2ysn4")))

(define-public crate-julia-sys-0.1.1 (c (n "julia-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d5l6yrds9wyk1xcpcxbmf3xb0yyvpm6miq06y4ybnx9viprlllh")))

(define-public crate-julia-sys-0.1.2 (c (n "julia-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zs8sq8y0i7rfkcfl0vwx44ypmfak4cp7z0w3p7vw6mw24cc6d41")))

(define-public crate-julia-sys-0.2.0 (c (n "julia-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lpmgg0sql5w7xa5vqxhqf1wh1bxj4iqd2zp6dyqcs2bfz0602bm")))

