(define-module (crates-io ju li julian_day_converter) #:use-module (crates-io))

(define-public crate-julian_day_converter-0.1.0 (c (n "julian_day_converter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0xzszfzbs5ir2rp4jba4wy745hdqpcx00xi2i3xl5ahirjc8ypc7") (y #t)))

(define-public crate-julian_day_converter-0.1.1 (c (n "julian_day_converter") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1ig2jhvm0a8spns54j5b3p51rv1plhcl7nln5xcxgfjl9mq47hss") (y #t)))

(define-public crate-julian_day_converter-0.1.2 (c (n "julian_day_converter") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1b4vbj855i404k8mhls91bjpv1ck7yyfpcc04wm5rf58xibff10x") (y #t)))

(define-public crate-julian_day_converter-0.1.3 (c (n "julian_day_converter") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0asxpfkm6gqlsyg92s65qcknfn45kzwa9jp3s6xaaql0ixwf0f0s") (y #t)))

(define-public crate-julian_day_converter-0.1.4 (c (n "julian_day_converter") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "162sqriayiszfhp1f4ryrq3k0w885qx5s46ql6x2b75i3f320xla") (y #t)))

(define-public crate-julian_day_converter-0.1.5 (c (n "julian_day_converter") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1iyjl668g0svnf0ryhkv5hjd7nmasgbl4bassimmayxfqryzkms9") (y #t)))

(define-public crate-julian_day_converter-0.1.6 (c (n "julian_day_converter") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0ypyirahx3zrbq7w23646v5jfw7k915w7iinkqvi3y18fwkagzhm") (y #t)))

(define-public crate-julian_day_converter-0.1.7 (c (n "julian_day_converter") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0lxacs54fsxy5m7lyamh4cxvw279hydfvpfx4z86v5qszk2k5nnm") (y #t)))

(define-public crate-julian_day_converter-0.1.8 (c (n "julian_day_converter") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0s1mv0pikwkd3rsqrjzdhbvgrmzc210hk5khi505zc5vz5pr2p1c") (y #t)))

(define-public crate-julian_day_converter-0.2.0 (c (n "julian_day_converter") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "03dvx9vg266wjz50169k5ksnbzwih21vydxh2jxxjv5q0ic3lb6z") (y #t)))

(define-public crate-julian_day_converter-0.2.1 (c (n "julian_day_converter") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0nkn2d2bd2kzgcxbzwsddyg7y9hf3mi5ifi0z06dbsz6vxmlsqi2") (y #t)))

(define-public crate-julian_day_converter-0.3.0 (c (n "julian_day_converter") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1sqjjgzqv9xrvhd3ckr4ssaf2za1csss07ya8qc81rrarrvbdzsk") (y #t)))

(define-public crate-julian_day_converter-0.3.1 (c (n "julian_day_converter") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0010ksrrmvz7v9a1avj5v54j6dpdpbnbk6i14gxa6p5zjgv5vqj7") (y #t)))

(define-public crate-julian_day_converter-0.3.2 (c (n "julian_day_converter") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "19g7qqhyprz6wfdipaywcw3v9zq0112ri43jv70nbv5fxhniv2rb")))

