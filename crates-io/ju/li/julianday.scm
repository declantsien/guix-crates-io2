(define-module (crates-io ju li julianday) #:use-module (crates-io))

(define-public crate-julianday-0.1.0 (c (n "julianday") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "162z1gxvimkcmc5vqg9famgfvisqx1cw4gr27pmjsd67mzysyxsq")))

(define-public crate-julianday-0.2.0 (c (n "julianday") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1hp8wzxv0220l0fkzr1nfwvlm8diw9z4nlrnmp3bzhsdcnglj1az")))

(define-public crate-julianday-1.0.0 (c (n "julianday") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "06c48aw51mdbilvkvp50cv31c96rqkzick66vmvlgd8f90a2g7zi")))

(define-public crate-julianday-1.0.1 (c (n "julianday") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "060bf7rpqb4gjj1bwzsnc9ynmlfrlmlx3710z6v0mbwx8km3kn0l")))

(define-public crate-julianday-1.1.0 (c (n "julianday") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1gclh048smn0w6bz47xjzf6a1sbnmmdb8r51k2wx6c03800f0r5s")))

(define-public crate-julianday-1.2.0 (c (n "julianday") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "00xhb29ffjmv5g8in7drdwxvc0jzqdrij6xhzxqj1kpjvzk8bmqh")))

