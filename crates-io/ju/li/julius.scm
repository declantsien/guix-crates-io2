(define-module (crates-io ju li julius) #:use-module (crates-io))

(define-public crate-julius-0.0.1 (c (n "julius") (v "0.0.1") (d (list (d (n "cfor") (r "*") (d #t) (k 0)))) (h "081bpy468bgh65mx384wjzvbv5flxyijgdz27ssvrp2y3px9sxk9")))

(define-public crate-julius-0.0.2 (c (n "julius") (v "0.0.2") (d (list (d (n "cfor") (r "*") (d #t) (k 0)))) (h "01w8ny4axi5g8j8piqm4zf2afq6pmj9ki1vha3krvnyi1rgv2k9s")))

(define-public crate-julius-1.0.0 (c (n "julius") (v "1.0.0") (d (list (d (n "c_vec") (r "*") (d #t) (k 0)) (d (n "cfor") (r "*") (d #t) (k 0)))) (h "0paxgy8zqw7yl5lsa22ahv7n1lp3r0ig7andgy7gyab6bjvdwqn4")))

