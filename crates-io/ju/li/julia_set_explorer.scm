(define-module (crates-io ju li julia_set_explorer) #:use-module (crates-io))

(define-public crate-julia_set_explorer-0.1.0 (c (n "julia_set_explorer") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "mandelbruhst-cli") (r "^0.1.0") (d #t) (k 0)) (d (n "minifb") (r "^0.24.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0w80kpx779qlia9i3w4vlrby7075hddwfj2s2ynbf8091i0v7514")))

