(define-module (crates-io ju li julia) #:use-module (crates-io))

(define-public crate-julia-0.1.0 (c (n "julia") (v "0.1.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "julia-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liner") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.5") (d #t) (k 0)))) (h "1wbi4k3qicxxdc9vmi275q2kw3yzhy27nrryvslg5h6qjfivfsmz")))

(define-public crate-julia-0.1.1 (c (n "julia") (v "0.1.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "julia-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liner") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.5") (d #t) (k 0)))) (h "031pf7k4mmmfh59yxnwj2y5kz0x6icvsgi1fa8ylmwijzxm3ql3l")))

(define-public crate-julia-0.1.2 (c (n "julia") (v "0.1.2") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "julia-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liner") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.5") (d #t) (k 0)))) (h "1x3v0r1qdwi6r1s274yzn06dpvm0vc2z4rymvfxslgn89zgazpaz")))

(define-public crate-julia-0.1.3 (c (n "julia") (v "0.1.3") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "julia-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liner") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.5") (d #t) (k 0)))) (h "06c656wxxasbs1ripisbggxpsk127511wgb5kvpz64r8bjy6snxm")))

(define-public crate-julia-0.1.4 (c (n "julia") (v "0.1.4") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "julia-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liner") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.5") (d #t) (k 0)))) (h "1l9p49bhjm7lvf95y2xwkjxf4m6nmw23pmsd7wpys2p1avp6prqr")))

(define-public crate-julia-0.2.0 (c (n "julia") (v "0.2.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "julia-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liner") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.5") (d #t) (k 0)))) (h "12h3jsymd7pap2cak8p9hhc5kdb74cfzx59y481d1g6pfjr41i0n")))

(define-public crate-julia-0.2.1 (c (n "julia") (v "0.2.1") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "julia-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liner") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.5") (d #t) (k 0)))) (h "0dgbk919cpm95y3wngki1w0s27z94q61v3xaj8j18v4912pbn1bj")))

(define-public crate-julia-0.2.2 (c (n "julia") (v "0.2.2") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "julia-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liner") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.5") (d #t) (k 0)))) (h "0949c9zvd23n7ib0qvrzryqn4kkndr1yclp79k24jl783sz7il4v")))

(define-public crate-julia-0.2.3 (c (n "julia") (v "0.2.3") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "julia-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liner") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.5") (d #t) (k 0)))) (h "1zd71p71jp6iwwjg9nsv8xvg5xwq3cgwcbdyd137mgqij4xa29gn")))

(define-public crate-julia-0.2.4 (c (n "julia") (v "0.2.4") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "julia-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liner") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.5") (d #t) (k 0)))) (h "0hbh9sxfgz21xv5wdqf474hjisb6dk9pkkghjffmd8shdz26qvnh")))

(define-public crate-julia-0.2.5 (c (n "julia") (v "0.2.5") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "julia-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liner") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.5") (d #t) (k 0)))) (h "1c6vfpwcpmqkk0s3f8aam2i5dlc1hvh2jyiq5g9n2s5szc1qd10x")))

