(define-module (crates-io ju li juliex) #:use-module (crates-io))

(define-public crate-juliex-0.3.0-alpha.1 (c (n "juliex") (v "0.3.0-alpha.1") (d (list (d (n "crossbeam") (r "^0.6.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0-alpha.13") (d #t) (k 2) (p "futures-preview")) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)) (d (n "romio") (r "^0.3.0-alpha.2") (d #t) (k 2)))) (h "0abbs9vp0y52g3dyy1w9d3by9cb81yxv7j0669fivw3nznm32szw")))

(define-public crate-juliex-0.3.0-alpha.2 (c (n "juliex") (v "0.3.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.6.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0-alpha.13") (d #t) (k 2) (p "futures-preview")) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)) (d (n "romio") (r "^0.3.0-alpha.2") (d #t) (k 2)))) (h "03d5c1a5ql7j9jlhqn4jcs2xlsfxsg1sx9i8nm2cx4s4970msjxj")))

(define-public crate-juliex-0.3.0-alpha.3 (c (n "juliex") (v "0.3.0-alpha.3") (d (list (d (n "crossbeam") (r "^0.6.0") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)) (d (n "romio") (r "^0.3.0-alpha.4") (d #t) (k 2)))) (h "0gcssx2cs73xnsqa2hgwjizvn6pfnjin8rp1b1hdn04qmrwc819n")))

(define-public crate-juliex-0.3.0-alpha.5 (c (n "juliex") (v "0.3.0-alpha.5") (d (list (d (n "crossbeam") (r "^0.6.0") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)) (d (n "romio") (r "^0.3.0-alpha.6") (d #t) (k 2)))) (h "1s4j7flgf3dcv8shmydl7myq30ml7hbd1l2gmf0ngnw7fv68zab7")))

(define-public crate-juliex-0.3.0-alpha.6 (c (n "juliex") (v "0.3.0-alpha.6") (d (list (d (n "crossbeam") (r "^0.6.0") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)) (d (n "romio") (r "^0.3.0-alpha.7") (d #t) (k 2)))) (h "0qs6318i4hgfxpj4ifqy50s9dbf0zz94dqchxa00kk05in7br9bv")))

(define-public crate-juliex-0.3.0-alpha.7 (c (n "juliex") (v "0.3.0-alpha.7") (d (list (d (n "crossbeam") (r "^0.6.0") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)) (d (n "romio") (r "^0.3.0-alpha.7") (d #t) (k 2)))) (h "0rj4vghs6s1za25j5q391ypxjibfqwxc234q4jd9kyah6jqpp48w")))

(define-public crate-juliex-0.3.0-alpha.8 (c (n "juliex") (v "0.3.0-alpha.8") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)) (d (n "romio") (r "^0.3.0-alpha.7") (d #t) (k 2)))) (h "1g4r23i7dkpid8zmkg6aiw73gkp7jagwhrjfi12yklyx4dczvp12")))

