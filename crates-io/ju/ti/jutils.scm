(define-module (crates-io ju ti jutils) #:use-module (crates-io))

(define-public crate-jutils-0.1.0 (c (n "jutils") (v "0.1.0") (h "1v9ky57wpqn9cj4llajhadqh8yyjjbd6n97a20s5aprkwfq20lyc")))

(define-public crate-jutils-0.1.1 (c (n "jutils") (v "0.1.1") (h "1bgrxj0ds3kzrikmn1ifsyak9al0r6rhm3fvw0xb7fa7achdxa08")))

(define-public crate-jutils-0.1.2 (c (n "jutils") (v "0.1.2") (h "190anq8v20yknbnr3y4zgnhz6y622mhsps964ajl45fwygq0k5g1")))

