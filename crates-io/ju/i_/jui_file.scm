(define-module (crates-io ju i_ jui_file) #:use-module (crates-io))

(define-public crate-jui_file-0.1.0 (c (n "jui_file") (v "0.1.0") (d (list (d (n "cbsk_base") (r "^0.1.5") (f (quote ("macro" "log"))) (d #t) (k 0)))) (h "0k4nyrz4a31dd8igsgxss6dbg9kjzalv7vbz7iwq05609w8jaqa1") (y #t)))

(define-public crate-jui_file-0.1.1 (c (n "jui_file") (v "0.1.1") (d (list (d (n "cbsk_base") (r "^0.1.5") (f (quote ("macro" "log"))) (d #t) (k 0)))) (h "04shmqpx4rxi85pzmdaih37dj4ivr48ajzbwl2hzcl3vcbqldp56") (y #t)))

(define-public crate-jui_file-0.1.2 (c (n "jui_file") (v "0.1.2") (d (list (d (n "cbsk_base") (r "^0.1.5") (f (quote ("macro" "log"))) (d #t) (k 0)))) (h "0rvdpdr4vjvg98nmqqg9dxngvjy9cxg1grriwsdq9c736cg875vn")))

(define-public crate-jui_file-0.1.3 (c (n "jui_file") (v "0.1.3") (d (list (d (n "cbsk_base") (r "^0.1.6") (f (quote ("macro" "log"))) (d #t) (k 0)))) (h "0wqxqx82v29ggaiizg1kl2w22hqgz5mzbi8sbnxfbyzsdknmr8bw")))

(define-public crate-jui_file-0.1.4 (c (n "jui_file") (v "0.1.4") (d (list (d (n "cbsk_base") (r "^0.1.7") (f (quote ("macro" "log"))) (d #t) (k 0)))) (h "16pqdj89ap16jbfp3aykwvgp27f2yw1lb5mdx3awvmzkch6vlk1m")))

(define-public crate-jui_file-0.1.5 (c (n "jui_file") (v "0.1.5") (d (list (d (n "cbsk_base") (r "^0.1.8") (f (quote ("macro" "log"))) (d #t) (k 0)))) (h "1s7v26c0d3khf1wnq5fa1x25aq7s5w7lkl19k58w6fbp53qypdn1")))

(define-public crate-jui_file-0.1.23 (c (n "jui_file") (v "0.1.23") (d (list (d (n "cbsk_base") (r "^1.1.0") (f (quote ("macro" "log"))) (d #t) (k 0)))) (h "150wvb62h4l97wwjxibvf4ynl4gss43szc8d0j5lblbv8dn6dkp9")))

(define-public crate-jui_file-0.1.24 (c (n "jui_file") (v "0.1.24") (d (list (d (n "cbsk_base") (r "^1.2.0") (f (quote ("macro" "log"))) (d #t) (k 0)))) (h "0vlqfpar2dzd1mgy7sbsvbvwf5cihadwvch8h6w7xz6hzzvl3qkn")))

