(define-module (crates-io ju ni junit-parser) #:use-module (crates-io))

(define-public crate-junit-parser-0.1.0 (c (n "junit-parser") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.20") (d #t) (k 0)))) (h "0xb9h3f2l0x2kjvbciqikjah3nx0s12rjcjghq73lf6zhcng920k")))

(define-public crate-junit-parser-0.1.1 (c (n "junit-parser") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.20") (d #t) (k 0)))) (h "13pfix2f0kmydpk7l00h75jmri2zzi25akdpl2g2kc4grf9vgf3d")))

(define-public crate-junit-parser-0.2.0 (c (n "junit-parser") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.22") (d #t) (k 0)))) (h "1d8c473nx89izrqg9sa2l69mpnri52r8fw3lvdkl4073yfngss4g")))

(define-public crate-junit-parser-1.0.0 (c (n "junit-parser") (v "1.0.0") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (f (quote ("escape-html"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04l1k28zgyhqrcjipj1awshjs4qlq1y6fic88sbsxknmdbgkls7f") (f (quote (("properties_as_vector") ("properties_as_hashmap") ("default" "properties_as_hashmap" "properties_as_vector")))) (s 2) (e (quote (("serde" "dep:serde") ("document-features" "dep:document-features")))) (r "1.56")))

(define-public crate-junit-parser-1.1.0 (c (n "junit-parser") (v "1.1.0") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (f (quote ("escape-html"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1p2d8nrzl6wdaqnwn1vv7xgmaxk1xaj89chs785hz4ja27sir6g0") (f (quote (("properties_as_vector") ("properties_as_hashmap") ("default" "properties_as_hashmap" "properties_as_vector")))) (s 2) (e (quote (("serde" "dep:serde") ("document-features" "dep:document-features")))) (r "1.56")))

(define-public crate-junit-parser-1.1.1 (c (n "junit-parser") (v "1.1.1") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (f (quote ("escape-html"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "190qvd9gjarnpxm5daxj2b65zj502747m63mjps7vrwpidxa7m0v") (f (quote (("properties_as_vector") ("properties_as_hashmap") ("default" "properties_as_hashmap" "properties_as_vector")))) (s 2) (e (quote (("serde" "dep:serde") ("document-features" "dep:document-features")))) (r "1.56")))

