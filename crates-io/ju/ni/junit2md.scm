(define-module (crates-io ju ni junit2md) #:use-module (crates-io))

(define-public crate-junit2md-0.1.0 (c (n "junit2md") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0v4ygqigz3fkf2jsdqcnsgawi8833wbvc6z57c5057y0m4yfahhh")))

