(define-module (crates-io ju ni juniper_tower) #:use-module (crates-io))

(define-public crate-juniper_tower-0.1.0 (c (n "juniper_tower") (v "0.1.0") (d (list (d (n "http") (r "^0.1.13") (d #t) (k 0)) (d (n "juniper") (r "^0.10.0") (f (quote ("serde_json"))) (d #t) (k 0)) (d (n "juniper") (r "^0.10.0") (f (quote ("expose-test-schema"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)) (d (n "tower-web") (r "^0.2.2") (d #t) (k 0)))) (h "0yqr0w6zb9aa11pzsgcwlaq45mr1yzrs0s7cyb4ka64xvy3zzdiv")))

(define-public crate-juniper_tower-0.1.1 (c (n "juniper_tower") (v "0.1.1") (d (list (d (n "http") (r "^0.1.13") (d #t) (k 0)) (d (n "juniper") (r "^0.10.0") (f (quote ("serde_json"))) (k 0)) (d (n "juniper") (r "^0.10.0") (f (quote ("expose-test-schema"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)) (d (n "tower-web") (r "^0.3.3") (k 0)))) (h "0b8v5lh5hqqkgaazha7sirp5gjdvm0jvfvq8znaxx85209rrf6a8")))

