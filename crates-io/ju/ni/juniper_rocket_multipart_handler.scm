(define-module (crates-io ju ni juniper_rocket_multipart_handler) #:use-module (crates-io))

(define-public crate-juniper_rocket_multipart_handler-0.1.0 (c (n "juniper_rocket_multipart_handler") (v "0.1.0") (d (list (d (n "juniper") (r "^0.15.7") (d #t) (k 0)) (d (n "juniper_rocket") (r "^0.8.2") (d #t) (k 0)) (d (n "multer") (r "^2.0.2") (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.1") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1bv3jyskg7fskaj7lb8zb2wp1blk9jn63hdr11ihn0hkjs8z0a20")))

