(define-module (crates-io ju ni junitxml2subunit) #:use-module (crates-io))

(define-public crate-junitxml2subunit-0.1.0 (c (n "junitxml2subunit") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "subunit-rust") (r "^0.1.1") (d #t) (k 0)))) (h "1pbpv8rz52ly9s01ami6swahgagzk32yciv3sjq3cgps3lqb9l86")))

(define-public crate-junitxml2subunit-1.0.0 (c (n "junitxml2subunit") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.31") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "subunit-rust") (r "^0.1.2") (d #t) (k 0)))) (h "17403lgx78j12kg2w9mdni9i7yb2wfz2sc4rsszba776xbx7mhqc")))

(define-public crate-junitxml2subunit-1.0.1 (c (n "junitxml2subunit") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17.2") (d #t) (k 0)) (d (n "subunit-rust") (r "^0.1.3") (d #t) (k 0)))) (h "12hagcnq5fqb7m2liz01l52ipcsla9jmjic74brjf2fzafi17ji3")))

(define-public crate-junitxml2subunit-1.1.0 (c (n "junitxml2subunit") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "subunit-rust") (r "^0.2.0") (d #t) (k 0)))) (h "0v59fi40nzsxwv0m5v4si1wd6hb49y3lnha35k5ayplbkbw89h22")))

