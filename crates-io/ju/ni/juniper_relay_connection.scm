(define-module (crates-io ju ni juniper_relay_connection) #:use-module (crates-io))

(define-public crate-juniper_relay_connection-0.1.0 (c (n "juniper_relay_connection") (v "0.1.0") (d (list (d (n "juniper") (r "^0.15") (d #t) (k 0)))) (h "1sfnmgsgj3m11h17h2ifknsx694v5zcps93albb8h8a4avjp51ca")))

(define-public crate-juniper_relay_connection-0.1.1 (c (n "juniper_relay_connection") (v "0.1.1") (d (list (d (n "juniper") (r "^0.15") (d #t) (k 0)))) (h "02wf1sc9qlbl0kich7kqn9w28i5amd1mb6078lknajmdb2mkp7p3")))

