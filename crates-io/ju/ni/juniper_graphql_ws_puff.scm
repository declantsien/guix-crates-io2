(define-module (crates-io ju ni juniper_graphql_ws_puff) #:use-module (crates-io))

(define-public crate-juniper_graphql_ws_puff-0.4.0-dev (c (n "juniper_graphql_ws_puff") (v "0.4.0-dev") (d (list (d (n "juniper") (r "^0.16.0-dev") (k 0) (p "juniper_puff")) (d (n "juniper_subscriptions") (r "^0.17.0-dev") (d #t) (k 0) (p "juniper_subscriptions_puff")) (d (n "serde") (r "^1.0.8") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "time"))) (k 0)))) (h "0p5x9mfdayc76hj55cbm1c8w91arwcd7nj8bdj53jr7jb0pxw1vr") (r "1.62")))

