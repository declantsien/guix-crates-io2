(define-module (crates-io ju ni juniper-eager-loading-code-gen) #:use-module (crates-io))

(define-public crate-juniper-eager-loading-code-gen-0.0.1 (c (n "juniper-eager-loading-code-gen") (v "0.0.1") (h "15z68hrg5cq2fn5n42ckgj0kicw72472av6dn6n8fk165mpf4g9b")))

(define-public crate-juniper-eager-loading-code-gen-0.0.2 (c (n "juniper-eager-loading-code-gen") (v "0.0.2") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1yzvm2c4ywz2nw4yrzdyx4wixk3bj5jcf6shmvcrg0hf79q06wi1")))

(define-public crate-juniper-eager-loading-code-gen-0.1.0 (c (n "juniper-eager-loading-code-gen") (v "0.1.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "18b0scrsnbk49h29lx3y8fddb5a8zcjsqrdspxbiycyjf9i5wwv7")))

(define-public crate-juniper-eager-loading-code-gen-0.1.1 (c (n "juniper-eager-loading-code-gen") (v "0.1.1") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1fpa8fmac7zjgpc22239w83dyswrkz268f50sccv5nsxpn8k03qk")))

(define-public crate-juniper-eager-loading-code-gen-0.1.2 (c (n "juniper-eager-loading-code-gen") (v "0.1.2") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1zad9yxxzrjivcirq60znhwkyadbpnr50s6pzi91qqk5f4fhrjvr")))

(define-public crate-juniper-eager-loading-code-gen-0.2.0 (c (n "juniper-eager-loading-code-gen") (v "0.2.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0phyc6mhglxc2cqxl8p7mpk33sg1nax9cnn7k7shaig2sv9ckmkr")))

(define-public crate-juniper-eager-loading-code-gen-0.3.0 (c (n "juniper-eager-loading-code-gen") (v "0.3.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "00w5d92sn967nljbx78qrg9qabh4iabr1im6s52qhgvab0588arn")))

(define-public crate-juniper-eager-loading-code-gen-0.3.1 (c (n "juniper-eager-loading-code-gen") (v "0.3.1") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "09m3kkhyvddw3lxh6pcg121jslx5hqc98x1bbvykc6pbh51sjgd9")))

(define-public crate-juniper-eager-loading-code-gen-0.4.0 (c (n "juniper-eager-loading-code-gen") (v "0.4.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lajq5f5si3q79x5znmiklj449f12biivkrd17jdkisna4b1dydq")))

(define-public crate-juniper-eager-loading-code-gen-0.4.1 (c (n "juniper-eager-loading-code-gen") (v "0.4.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lc75paw5vkfbsi2xqc72x263hkqjyqf5vw9igzaym34x530z2kc")))

(define-public crate-juniper-eager-loading-code-gen-0.4.2 (c (n "juniper-eager-loading-code-gen") (v "0.4.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04p3vmqzbldsgybv7cjm2h5piqigw8np0r1kpgxkry7lpq5w6ap2")))

(define-public crate-juniper-eager-loading-code-gen-0.5.0 (c (n "juniper-eager-loading-code-gen") (v "0.5.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ijgsndnb9vns20w34igxgdwj2s3qxi08b44bq0hdnf3h0s1sgfz")))

(define-public crate-juniper-eager-loading-code-gen-0.5.1 (c (n "juniper-eager-loading-code-gen") (v "0.5.1") (d (list (d (n "bae") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lb0a2rhxdmsayn9kjxc9vgbnlcaxgdp25blvb49zwhpix8j3yji")))

