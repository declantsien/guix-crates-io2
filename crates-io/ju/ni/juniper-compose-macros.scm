(define-module (crates-io ju ni juniper-compose-macros) #:use-module (crates-io))

(define-public crate-juniper-compose-macros-0.0.1 (c (n "juniper-compose-macros") (v "0.0.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1vrwdv9403mylpnhwmw0y481y38i1j22gpq2if58al8w0zk55b3v") (y #t)))

(define-public crate-juniper-compose-macros-0.0.2 (c (n "juniper-compose-macros") (v "0.0.2") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0whxdfkqwizzy90gphhgf0myp0grnq1zvqfy9lbnza5amws83n39")))

(define-public crate-juniper-compose-macros-0.0.3 (c (n "juniper-compose-macros") (v "0.0.3") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0v6skx68z19n6fhwdxfjpn1bhj6s8mix3xrms4kv6kmfl8arcqg2")))

