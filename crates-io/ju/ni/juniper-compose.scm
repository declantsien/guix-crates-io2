(define-module (crates-io ju ni juniper-compose) #:use-module (crates-io))

(define-public crate-juniper-compose-0.0.1 (c (n "juniper-compose") (v "0.0.1") (d (list (d (n "juniper") (r "^0.15.9") (d #t) (k 0)) (d (n "juniper-compose-macros") (r "^0.0.1") (d #t) (k 0)))) (h "1ysq6mkjgkjksayi4isyqqnr93f8x95dj2v1fxxlkfi0csxbl7dg") (y #t)))

(define-public crate-juniper-compose-0.0.2 (c (n "juniper-compose") (v "0.0.2") (d (list (d (n "juniper") (r "^0.15.9") (d #t) (k 0)) (d (n "juniper-compose-macros") (r "^0.0.2") (d #t) (k 0)))) (h "19ymyrk2i8bgfyvd73453nwrlrg1hkcvpf9fz30zldpmafwm4fhl")))

(define-public crate-juniper-compose-0.0.3 (c (n "juniper-compose") (v "0.0.3") (d (list (d (n "juniper") (r "^0.15.9") (d #t) (k 0)) (d (n "juniper-compose-macros") (r "^0.0.3") (d #t) (k 0)))) (h "1xdvccm8fsgp0k9k7vl8yhdpbbjnxrmi3502psjhy4kxm8cyfg97")))

