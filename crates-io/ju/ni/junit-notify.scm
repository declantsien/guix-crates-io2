(define-module (crates-io ju ni junit-notify) #:use-module (crates-io))

(define-public crate-junit-notify-0.1.0 (c (n "junit-notify") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "066kflzw1m5f2fjz6nwi0p6h65nyg82rns253qcn1bnmyqk7slvf")))

(define-public crate-junit-notify-1.0.0 (c (n "junit-notify") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "12jnzbcr4nla2sw07x21kzmf46yd1yq6p9ar5k9in32if7x6j7m6")))

