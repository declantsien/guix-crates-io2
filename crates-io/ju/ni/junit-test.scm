(define-module (crates-io ju ni junit-test) #:use-module (crates-io))

(define-public crate-junit-test-0.2.1 (c (n "junit-test") (v "0.2.1") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "03br4x9jlzbscm7h6vffvs6zdwj6j65c715ajrmrbb4z18d7kzhh")))

(define-public crate-junit-test-0.2.2 (c (n "junit-test") (v "0.2.2") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "15l1x56146ppwqg3b0kbxs7qy7f4837xapzml6yimfq4i5329ard")))

(define-public crate-junit-test-0.2.3 (c (n "junit-test") (v "0.2.3") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1h3j7r4in0gj3xjkx8l4zyi76hjazvr19nswqlf0r35h0gky6b84")))

(define-public crate-junit-test-0.2.4 (c (n "junit-test") (v "0.2.4") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1p1crqy26yd8i4ayb48w82nx6kcgnid82h6lks7lcn4h94w30i0r")))

