(define-module (crates-io ju ni juniper_codegen_puff) #:use-module (crates-io))

(define-public crate-juniper_codegen_puff-0.16.0-dev (c (n "juniper_codegen_puff") (v "0.16.0-dev") (d (list (d (n "derive_more") (r "^0.99.7") (d #t) (k 2)) (d (n "futures") (r "^0.3.22") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (f (quote ("extra-traits" "full" "parsing" "visit" "visit-mut" "derive" "printing"))) (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "02jhn06nacjwf1snzg521r9745qd9dphlaxg9mxnbdhdp91fd8c5") (r "1.62")))

