(define-module (crates-io ju ni juniper_serde) #:use-module (crates-io))

(define-public crate-juniper_serde-0.1.0 (c (n "juniper_serde") (v "0.1.0") (d (list (d (n "juniper") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "12w7hrnmhwvf80n0b221y38wmn6mjikakrrv10xkx404j9rcxsg7")))

