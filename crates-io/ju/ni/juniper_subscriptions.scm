(define-module (crates-io ju ni juniper_subscriptions) #:use-module (crates-io))

(define-public crate-juniper_subscriptions-0.15.0 (c (n "juniper_subscriptions") (v "0.15.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "juniper") (r "^0.15.0") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "0451m49q3gyx9n8mrphx91jlc6z2gmy6jbq8knd9g1rzh6kgi9jf")))

(define-public crate-juniper_subscriptions-0.15.1 (c (n "juniper_subscriptions") (v "0.15.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "juniper") (r "^0.15.1") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "0b88mf21ys35kz63jlnpyfgdmkj7yrsbrh3xyik7g5agp8s7h45k")))

(define-public crate-juniper_subscriptions-0.15.2 (c (n "juniper_subscriptions") (v "0.15.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "juniper") (r "^0.15.2") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "0g8h6vm0f7w2crvagad6vh04qlz73b5kcxs40hs12j7xppcr0im8")))

(define-public crate-juniper_subscriptions-0.15.3 (c (n "juniper_subscriptions") (v "0.15.3") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "juniper") (r "^0.15.3") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "0g6040fdk54md51pjbl4bg1684rspf5dm37n2gkfrqmcjpigmd6w")))

(define-public crate-juniper_subscriptions-0.15.4 (c (n "juniper_subscriptions") (v "0.15.4") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "juniper") (r "^0.15.4") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "1h039nrw6gskwf2an70dwijkkl933b33sf48may3mqqf0zndq3d2")))

(define-public crate-juniper_subscriptions-0.15.5 (c (n "juniper_subscriptions") (v "0.15.5") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "juniper") (r "^0.15.6") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "0fvd0ypxwzycs4bvch9c42x1n9fb774vfls6rrjbcsx806qwmjz0")))

(define-public crate-juniper_subscriptions-0.16.0 (c (n "juniper_subscriptions") (v "0.16.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "juniper") (r "^0.15.7") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "04wz8bixkxpaq0rwjjy6ssa4mv4b3n4f7bijfk0r3dhj3rmb50r9")))

(define-public crate-juniper_subscriptions-0.15.6 (c (n "juniper_subscriptions") (v "0.15.6") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "juniper") (r "^0.15.8") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "09ljgpbjs0233s41f4zqlwnzisbi313qv3wpchab0y5q6zfx4nrd")))

(define-public crate-juniper_subscriptions-0.17.0 (c (n "juniper_subscriptions") (v "0.17.0") (d (list (d (n "futures") (r "^0.3.22") (d #t) (k 0)) (d (n "juniper") (r "^0.16.0") (k 0)) (d (n "serde_json") (r "^1.0.18") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0b59f1xssarr0dwr3h37l9761ml8s08q68saj8qj3jnlkf1ql876") (r "1.73")))

