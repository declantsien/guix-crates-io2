(define-module (crates-io ju ni juniper_subscriptions_puff) #:use-module (crates-io))

(define-public crate-juniper_subscriptions_puff-0.17.0-dev (c (n "juniper_subscriptions_puff") (v "0.17.0-dev") (d (list (d (n "futures") (r "^0.3.22") (d #t) (k 0)) (d (n "juniper") (r "^0.16.0-dev") (k 0) (p "juniper_puff")) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0pcfp2v48r08y72gdn0cqy2f3b8255i7h66ic9pig0j4v6bn8iqj") (r "1.62")))

