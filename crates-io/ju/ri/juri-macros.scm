(define-module (crates-io ju ri juri-macros) #:use-module (crates-io))

(define-public crate-juri-macros-0.1.0 (c (n "juri-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1lm6li8l8lsirb5mhbbg7jyv1jpyp7dd97lfq70ijb1g6nl0pyq6")))

(define-public crate-juri-macros-0.2.0-alpha.1 (c (n "juri-macros") (v "0.2.0-alpha.1") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0rfsbnd4yxxpsvqj6a7j1fxymyadvn1103ia0wp4hw0db0gkx80a")))

(define-public crate-juri-macros-0.2.0-alpha.2 (c (n "juri-macros") (v "0.2.0-alpha.2") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0a57d823rfdyxlyw6xvj01mi5nn4l8kfdwaiqdph9jqryrbsc90d")))

