(define-module (crates-io ju na juna) #:use-module (crates-io))

(define-public crate-juNa-0.1.0 (c (n "juNa") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.8.1") (d #t) (k 0)))) (h "1jrxx6rairpwz4n0wx0hbpaaqkcgf482fcwr1h9jr13izyr8fimd") (y #t)))

(define-public crate-juNa-0.0.1 (c (n "juNa") (v "0.0.1") (d (list (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.8.1") (d #t) (k 0)))) (h "0ajg5sdk7wc4p64l6mvm94wkv59gs1rcw8k0nfqxy37pliwgxmjr")))

