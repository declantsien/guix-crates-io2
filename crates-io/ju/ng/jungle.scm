(define-module (crates-io ju ng jungle) #:use-module (crates-io))

(define-public crate-jungle-0.1.0 (c (n "jungle") (v "0.1.0") (h "0fkwm26lxv6p4dy0wph8rdxz6fj37ivasw2cdh88y4lld2in4pis")))

(define-public crate-jungle-0.1.1 (c (n "jungle") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.5.1") (d #t) (k 0)))) (h "1nj6wvagbcpf2qnzjpz38yrwr6mlkh3asijda8k7kdw2v679yvkk")))

