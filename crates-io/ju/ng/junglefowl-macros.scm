(define-module (crates-io ju ng junglefowl-macros) #:use-module (crates-io))

(define-public crate-junglefowl-macros-0.1.0 (c (n "junglefowl-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("full"))) (d #t) (k 0)))) (h "1absr0ry4w0xawkfdz1dbgbpvp80jyqa9nryy78s22lzclgj3is9")))

(define-public crate-junglefowl-macros-0.1.1 (c (n "junglefowl-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("full"))) (d #t) (k 0)))) (h "0asl2srkmhfr04g2qqa91l6x03761bgb6sfm6a0w2y0z0zfgxvki")))

