(define-module (crates-io ju ng junglefowl) #:use-module (crates-io))

(define-public crate-junglefowl-0.1.0 (c (n "junglefowl") (v "0.1.0") (d (list (d (n "junglefowl-macros") (r ">0.0") (d #t) (k 0)))) (h "1jh8s19r0jwx1qkqf8bfqawilnaykn4qhzxbyy5qhjcmf15l4y0j")))

(define-public crate-junglefowl-0.1.1 (c (n "junglefowl") (v "0.1.1") (d (list (d (n "junglefowl-macros") (r ">0.0") (d #t) (k 0)))) (h "1di4rkmv3x72fh38brll0dci2jfn8h54z8xhvww0h5xlxxadz8xf")))

