(define-module (crates-io ju mb jumbf) #:use-module (crates-io))

(define-public crate-jumbf-0.1.0 (c (n "jumbf") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (o #t) (d #t) (k 0)))) (h "1zl2iyl9r8ww5ph44kz43f8amvvs0n5f3azig268hl2ll71qlj6z") (f (quote (("parser" "nom" "thiserror") ("default" "parser")))) (r "1.74.0")))

(define-public crate-jumbf-0.2.0 (c (n "jumbf") (v "0.2.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "pretty_assertions_sorted") (r "^1.2.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (o #t) (d #t) (k 0)))) (h "1rrqsw8w1kxn4hl2pkjdf1vh6hfblc0hh3ljbjy0j6ks6mbik2nn") (f (quote (("parser" "nom" "thiserror") ("default" "parser")))) (r "1.74.0")))

(define-public crate-jumbf-0.2.1 (c (n "jumbf") (v "0.2.1") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "pretty_assertions_sorted") (r "^1.2.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (o #t) (d #t) (k 0)))) (h "0ijx6l81w39npl2wr67qh26dwx6j1m98wrwgpl8lqz8rknr7a023") (f (quote (("parser" "nom" "thiserror") ("default" "parser")))) (r "1.74.0")))

(define-public crate-jumbf-0.2.2 (c (n "jumbf") (v "0.2.2") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "pretty_assertions_sorted") (r "^1.2.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (o #t) (d #t) (k 0)))) (h "0i5w4xa8k4rslx5faxpknlg9f96ij5wn347mp0b768cc6i3d4kvy") (f (quote (("parser" "nom" "thiserror") ("default" "parser")))) (r "1.74.0")))

(define-public crate-jumbf-0.3.0 (c (n "jumbf") (v "0.3.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "pretty_assertions_sorted") (r "^1.2.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (o #t) (d #t) (k 0)))) (h "0wpdnr9r4615jf4jsl0cx0nbbn3783j38l4k46bj1mzxg15gzr5i") (f (quote (("parser" "nom" "thiserror") ("default" "parser")))) (r "1.74.0")))

(define-public crate-jumbf-0.4.0 (c (n "jumbf") (v "0.4.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "pretty_assertions_sorted") (r "^1.2.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (o #t) (d #t) (k 0)))) (h "10wqdrfyh9k7jjh3nkixkqhgyb3xjirl7bvw17s57n0ynsrbmc3i") (f (quote (("parser" "nom" "thiserror") ("default" "parser")))) (r "1.74.0")))

