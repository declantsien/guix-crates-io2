(define-module (crates-io ju xt juxt_md5) #:use-module (crates-io))

(define-public crate-juxt_md5-0.1.0 (c (n "juxt_md5") (v "0.1.0") (h "0l1ycyj19c2jh6i15cd8l47lc301ssbbm2fjpm7ck2kr6w06j0pq")))

(define-public crate-juxt_md5-0.1.1 (c (n "juxt_md5") (v "0.1.1") (h "1a02f5qqm2d6j2pq5hy0rcd53rc557y901sq5yp3xjgv6nfs0dzw")))

