(define-module (crates-io ju xt juxt_adler32) #:use-module (crates-io))

(define-public crate-juxt_adler32-0.1.0 (c (n "juxt_adler32") (v "0.1.0") (h "1f12h8cjbn08fhwaicb307hl2f1yvzgn3jkzkw7c0996phygivyz")))

(define-public crate-juxt_adler32-0.1.1 (c (n "juxt_adler32") (v "0.1.1") (h "1gbpapm8mzndzvrvh6lsx1q99y4jxdan85ksdx73rris74dc44c5")))

