(define-module (crates-io ju xt juxt_matrix) #:use-module (crates-io))

(define-public crate-juxt_matrix-0.1.0 (c (n "juxt_matrix") (v "0.1.0") (h "0v5bw5qplnx50bqk4d4b41blzcxnch54z6gmsj829rsmpbh7631w")))

(define-public crate-juxt_matrix-0.1.1 (c (n "juxt_matrix") (v "0.1.1") (h "0w2vishk1m14j5n8whiw75ccd003l5wa8l9jbm81byp9kcwij2l0")))

