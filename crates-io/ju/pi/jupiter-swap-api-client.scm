(define-module (crates-io ju pi jupiter-swap-api-client) #:use-module (crates-io))

(define-public crate-jupiter-swap-api-client-0.1.0 (c (n "jupiter-swap-api-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.16") (d #t) (k 0)))) (h "0qql508m9xvdrdgs50nrw7f38a02h3hn6w71sfyvwswiq06ys1qm")))

