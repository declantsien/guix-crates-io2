(define-module (crates-io ju pi jupiter-account) #:use-module (crates-io))

(define-public crate-jupiter-account-0.1.0 (c (n "jupiter-account") (v "0.1.0") (d (list (d (n "multiproof-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)))) (h "1ghfsq48kwgma54gfk9z0ivd30gby06mpf664pz4p99a5cfasypk")))

(define-public crate-jupiter-account-0.1.1 (c (n "jupiter-account") (v "0.1.1") (d (list (d (n "multiproof-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)))) (h "125lbzk365pir8kh22bx9qj2gppnlsn952247il4dlmjcay9p1mg")))

(define-public crate-jupiter-account-0.1.2 (c (n "jupiter-account") (v "0.1.2") (d (list (d (n "multiproof-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)))) (h "0n1xhj2829fpfivciggxhxi36bfp7c6007p9i6666y4fsldk8q89")))

(define-public crate-jupiter-account-0.1.3 (c (n "jupiter-account") (v "0.1.3") (d (list (d (n "multiproof-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)))) (h "1hacvyirsydxams5d47vqq9pxlxcjvkvh5j9da5wwz99s1w46jy5")))

(define-public crate-jupiter-account-0.1.4 (c (n "jupiter-account") (v "0.1.4") (d (list (d (n "multiproof-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)))) (h "1vbb9vrmak4pgd6ddw5y3pkbzdbf9650qg9nxf78cdzrsdsd7y25")))

(define-public crate-jupiter-account-0.1.5 (c (n "jupiter-account") (v "0.1.5") (d (list (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "multiproof-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0c2yrb0j237p6cgj5mxyfrgnhf1gsml2sdpvnxjk7m56sl73bpwm")))

(define-public crate-jupiter-account-0.1.6 (c (n "jupiter-account") (v "0.1.6") (d (list (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "multiproof-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0i287pd330q22i0cv6j2gdagj8xd2ww9spwaiq9yjcjlq7s82nbw")))

(define-public crate-jupiter-account-0.1.7 (c (n "jupiter-account") (v "0.1.7") (d (list (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "multiproof-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0f2ihzj3c558wypdl16llww9nskx8i024rrdr7qphdh3nh6wp0ld")))

(define-public crate-jupiter-account-0.1.8 (c (n "jupiter-account") (v "0.1.8") (d (list (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "multiproof-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0wbx0aryliiw9pdbwqf6wjjkhdcx0gx11ny87ma0mazcclpbsnma")))

(define-public crate-jupiter-account-0.1.9 (c (n "jupiter-account") (v "0.1.9") (d (list (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "multiproof-rs") (r "^0.1.8") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0cclinbszvjfg86k1z3sznhwir9yxvcv8nd7p0ap1bjxwij6wnrv")))

(define-public crate-jupiter-account-0.1.10 (c (n "jupiter-account") (v "0.1.10") (d (list (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "multiproof-rs") (r "^0.1.9") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1wc6mm1cvgzcxf8x9hqsxzws7xxsb5qwl5l6mz882ghw0ch2mzyr")))

