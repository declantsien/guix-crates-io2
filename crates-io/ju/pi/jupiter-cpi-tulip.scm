(define-module (crates-io ju pi jupiter-cpi-tulip) #:use-module (crates-io))

(define-public crate-jupiter-cpi-tulip-3.0.0 (c (n "jupiter-cpi-tulip") (v "3.0.0") (d (list (d (n "anchor-gen-tulip") (r "^0.4.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)))) (h "0kb73ra61zf4i5avpmsqnziiilq3i84yrljwbqqdmzsz6c9qyv4w") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-jupiter-cpi-tulip-3.0.1 (c (n "jupiter-cpi-tulip") (v "3.0.1") (d (list (d (n "anchor-gen-tulip") (r "^0.4.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)))) (h "0mz6vdr1fdcwqdk5zlz2mbvfvhi9kn25a6w1xsbd2g0nzk68xc31") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-jupiter-cpi-tulip-3.0.2 (c (n "jupiter-cpi-tulip") (v "3.0.2") (d (list (d (n "anchor-gen-tulip") (r "^0.4.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)))) (h "18vyg5r7k39y01fi4xmi28w6bbjh5gr91f47nnghyy46xpbs7s6g") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-jupiter-cpi-tulip-3.0.3 (c (n "jupiter-cpi-tulip") (v "3.0.3") (d (list (d (n "anchor-gen-tulip") (r "^0.4.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)))) (h "1iydyp9mcx712z5ac4f7xw32yvf78d5hh16zg939f5mr25liw3yc") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-jupiter-cpi-tulip-4.0.0 (c (n "jupiter-cpi-tulip") (v "4.0.0") (d (list (d (n "anchor-gen-tulip") (r "^0.4.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)))) (h "1xwjrb1bcm6zkhcyqxy22njy850459f8db1rax70lwqfvbq84ia8") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-jupiter-cpi-tulip-3.0.4 (c (n "jupiter-cpi-tulip") (v "3.0.4") (d (list (d (n "anchor-gen-tulip") (r "^0.4.1") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)))) (h "1lgyaqkq2b0i7l6s6zhzsrgl9y3qk7z82104j5lri866pyhjn3hg") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

