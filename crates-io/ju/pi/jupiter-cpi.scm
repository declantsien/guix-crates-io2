(define-module (crates-io ju pi jupiter-cpi) #:use-module (crates-io))

(define-public crate-jupiter-cpi-3.0.0 (c (n "jupiter-cpi") (v "3.0.0") (d (list (d (n "anchor-gen") (r "^0.2.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.22.1") (d #t) (k 0)))) (h "1fwvjd76sphnah106k62kz522dxs67xmp63j2if5vdm3mqbjr8nd") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-jupiter-cpi-4.0.0 (c (n "jupiter-cpi") (v "4.0.0") (d (list (d (n "anchor-gen") (r "^0.2.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.22.1") (d #t) (k 0)))) (h "1jx32qgbabs27x5q7kmn7ssdzjq47hlh2qdhdmy3467ih9801b70") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-jupiter-cpi-4.0.1 (c (n "jupiter-cpi") (v "4.0.1") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.22.1") (d #t) (k 0)))) (h "1svzs3lwysnk4hygqxlxkkwjwhlrsrf6pl59x9ya4rlml6aav735") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-jupiter-cpi-4.0.2 (c (n "jupiter-cpi") (v "4.0.2") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.15") (d #t) (k 0)))) (h "0sffp4aa2849gkz8nb1c0nkczird195c2vx1f6jgllglavdj60fc") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-jupiter-cpi-4.0.3 (c (n "jupiter-cpi") (v "4.0.3") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.15") (d #t) (k 0)))) (h "0cd6c40sjvyhpfbs4vkc1c9psfssl0bf975z7v4ddxd3iq75m68j") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

