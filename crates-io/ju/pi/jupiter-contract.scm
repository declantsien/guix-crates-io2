(define-module (crates-io ju pi jupiter-contract) #:use-module (crates-io))

(define-public crate-jupiter-contract-0.1.0 (c (n "jupiter-contract") (v "0.1.0") (d (list (d (n "jupiter-account") (r "^0.1.8") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "multiproof-rs") (r "^0.1.8") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0mpk2xmd5v7i429i7bbqjal2glrlcznzkc0wxg1zj86l8l39i9d9")))

(define-public crate-jupiter-contract-0.1.1 (c (n "jupiter-contract") (v "0.1.1") (d (list (d (n "jupiter-account") (r "^0.1.8") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "multiproof-rs") (r "^0.1.8") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "12b347h4rfv71hzly71vz995hrk296p0n0dkwifvd2mpkr9c989g")))

(define-public crate-jupiter-contract-0.1.2 (c (n "jupiter-contract") (v "0.1.2") (d (list (d (n "jupiter-account") (r "^0.1.10") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "multiproof-rs") (r "^0.1.9") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1vf9rv3sw9m0fw9yx4w1a2lbqxw2bvrlapq6vayg4nkgzx52rf04")))

