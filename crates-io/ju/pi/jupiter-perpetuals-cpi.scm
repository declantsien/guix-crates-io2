(define-module (crates-io ju pi jupiter-perpetuals-cpi) #:use-module (crates-io))

(define-public crate-jupiter-perpetuals-cpi-0.1.0 (c (n "jupiter-perpetuals-cpi") (v "0.1.0") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.26.0") (d #t) (k 0)))) (h "1miqv6mzwx44088dvrxnka2qm1w0qnl2g0apjqf0fs4xzk50rrzw") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-jupiter-perpetuals-cpi-0.1.1 (c (n "jupiter-perpetuals-cpi") (v "0.1.1") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.26.0") (d #t) (k 0)))) (h "09vx17ymirsasrplfpw8nvrcsf859ww780h3x3p5jh99bz3x9k3d") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

