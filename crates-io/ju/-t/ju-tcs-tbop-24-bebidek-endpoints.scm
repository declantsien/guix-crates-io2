(define-module (crates-io ju -t ju-tcs-tbop-24-bebidek-endpoints) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-bebidek-endpoints-0.1.0 (c (n "ju-tcs-tbop-24-bebidek-endpoints") (v "0.1.0") (h "1qmwi7zmmyjqnf60pwycc0rdfj3ggav7x59kwk5rzgqq3mfq5az1") (y #t)))

(define-public crate-ju-tcs-tbop-24-bebidek-endpoints-1.0.0 (c (n "ju-tcs-tbop-24-bebidek-endpoints") (v "1.0.0") (h "1n94an7z2y75j1bcf3pjy1xwcz4xpq2i4sv6y2nm1anmxh0qv52f")))

(define-public crate-ju-tcs-tbop-24-bebidek-endpoints-2.0.0 (c (n "ju-tcs-tbop-24-bebidek-endpoints") (v "2.0.0") (h "0jc1jcjsj48h7wb4kp8wr9nc0xf1p8qcakxj39lbb8mbshh9kbv2")))

