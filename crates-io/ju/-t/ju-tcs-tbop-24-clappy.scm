(define-module (crates-io ju -t ju-tcs-tbop-24-clappy) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-clappy-0.1.0 (c (n "ju-tcs-tbop-24-clappy") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (d #t) (k 0)))) (h "10s3chmnkadwk8hs0b7qnmafw40bsrgjavj16vddk2prj5dmhcf3")))

(define-public crate-ju-tcs-tbop-24-clappy-0.1.1 (c (n "ju-tcs-tbop-24-clappy") (v "0.1.1") (h "13f563yqmw2r9hcaxn09gyixc521xqi69xlaz4igqds1lp77h5sw")))

(define-public crate-ju-tcs-tbop-24-clappy-0.1.2 (c (n "ju-tcs-tbop-24-clappy") (v "0.1.2") (h "1rh447bmkq526mqajakxjvvimbgbqs834qpzd901ynii4jk8cv44")))

(define-public crate-ju-tcs-tbop-24-clappy-0.2.2 (c (n "ju-tcs-tbop-24-clappy") (v "0.2.2") (h "1j5dqmjvc775lcdr4v9w7b2xscprpsyfj85gm42vi4vka3vd51vc")))

