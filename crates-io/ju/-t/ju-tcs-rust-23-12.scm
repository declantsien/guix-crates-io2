(define-module (crates-io ju -t ju-tcs-rust-23-12) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-12-0.1.0 (c (n "ju-tcs-rust-23-12") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cycqshczdc0akx6d0hfz7iz8hh5yzw8gq29ww8y6qxy2f7i52xg")))

(define-public crate-ju-tcs-rust-23-12-1.1.0 (c (n "ju-tcs-rust-23-12") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-11") (r "^0.1.0") (d #t) (k 0)))) (h "05nzzj4qv8s7lzbdq90ddbf5gcqcvi8f3wb9x1s03hw1msrhmd97")))

(define-public crate-ju-tcs-rust-23-12-1.1.1 (c (n "ju-tcs-rust-23-12") (v "1.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-11") (r "^1.1.0") (d #t) (k 0)))) (h "06kx8r6jlvhnmx3phhhhryls938fvdbmnap68swsjki527x9g3r3")))

(define-public crate-ju-tcs-rust-23-12-1.1.2 (c (n "ju-tcs-rust-23-12") (v "1.1.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-11") (r "^1.1.0") (d #t) (k 0)))) (h "1f4a7jnjz5l8ynz24nwnq8ikxwinaafc6jv4qnxzh414rp33vvjg")))

