(define-module (crates-io ju -t ju-tcs-tbop-24-dcfk) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-dcfk-0.1.0 (c (n "ju-tcs-tbop-24-dcfk") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "04636zjb26xk86nd4bsj9jkkzv9wp69zbw72nbwcab1zsx9bmh4c")))

(define-public crate-ju-tcs-tbop-24-dcfk-0.1.1 (c (n "ju-tcs-tbop-24-dcfk") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-lib-dcfk") (r "^1.0.0") (d #t) (k 0)))) (h "0nlyic1dl3w6bnpd4nnqpv4d4kbqqbwy1mhv6rj49zm850dn1y35")))

(define-public crate-ju-tcs-tbop-24-dcfk-0.1.2 (c (n "ju-tcs-tbop-24-dcfk") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-lib-dcfk") (r "^1.0.0") (d #t) (k 0)))) (h "0k8cr1b3a9jbphnn051jbib3baxfyqaqnn00m709dzc7il1blyfl")))

(define-public crate-ju-tcs-tbop-24-dcfk-1.0.0 (c (n "ju-tcs-tbop-24-dcfk") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-lib-dcfk") (r "^1.0.0") (d #t) (k 0)))) (h "0vliybk0yqbb461sw3banlw3qndlh35l7795k13wlp8p0fg71m7s")))

