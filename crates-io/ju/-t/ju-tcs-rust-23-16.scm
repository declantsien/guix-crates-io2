(define-module (crates-io ju -t ju-tcs-rust-23-16) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-16-0.1.0 (c (n "ju-tcs-rust-23-16") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)))) (h "06i3lr26ag485af1blnw13g2qj50bn4ygybg432q5kif94xhll6b")))

(define-public crate-ju-tcs-rust-23-16-0.1.1 (c (n "ju-tcs-rust-23-16") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-15") (r "^0.1.0") (d #t) (k 0)))) (h "08sz25wx954ffkx28r6iw3kphy7010i8kcmha159ql6461q4nh0v")))

