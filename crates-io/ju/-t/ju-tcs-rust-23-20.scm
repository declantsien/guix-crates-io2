(define-module (crates-io ju -t ju-tcs-rust-23-20) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-20-0.1.0 (c (n "ju-tcs-rust-23-20") (v "0.1.0") (h "1gy496wk12lxq1z8yxw59h9994a2zq0cd3n8bhsglrfd8q756msr")))

(define-public crate-ju-tcs-rust-23-20-0.1.1 (c (n "ju-tcs-rust-23-20") (v "0.1.1") (h "0562jlp1r21sdrv46kag4iyyc1c1w28dvan5q5kjbgjh8bizc3b0")))

