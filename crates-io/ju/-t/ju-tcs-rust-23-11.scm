(define-module (crates-io ju -t ju-tcs-rust-23-11) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-11-0.1.0 (c (n "ju-tcs-rust-23-11") (v "0.1.0") (h "0pvjxbvqfbgnmwllibk699x47bcvdc3f86r124xaj8swamh075vd")))

(define-public crate-ju-tcs-rust-23-11-1.1.0 (c (n "ju-tcs-rust-23-11") (v "1.1.0") (h "0ahwvajdv2h79n39ibnj0hp3j2azmrih13mg7brc4avffpl80kwh")))

