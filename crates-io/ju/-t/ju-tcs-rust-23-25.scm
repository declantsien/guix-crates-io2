(define-module (crates-io ju -t ju-tcs-rust-23-25) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-25-0.1.0 (c (n "ju-tcs-rust-23-25") (v "0.1.0") (h "0mz0m4zksa3p8x7460y58fyhxmghyff53brpi46avs3q608mkr4g")))

(define-public crate-ju-tcs-rust-23-25-0.1.1 (c (n "ju-tcs-rust-23-25") (v "0.1.1") (h "17ffxr6f6la43l52fnhyza191dajkh3dvwxccdr78xb3fdikbkh7")))

(define-public crate-ju-tcs-rust-23-25-0.1.2 (c (n "ju-tcs-rust-23-25") (v "0.1.2") (h "1n0ipbgg76isksv04kxrgf4dgjib249mzz8zyf7v6zv9wc9dyd26")))

