(define-module (crates-io ju -t ju-tcs-rust-23-5) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-5-0.1.0 (c (n "ju-tcs-rust-23-5") (v "0.1.0") (h "1wik9x672g6l73l09dsm3dxajqpsg5isyxs7yi8qr8fvlywwaclm")))

(define-public crate-ju-tcs-rust-23-5-0.2.0 (c (n "ju-tcs-rust-23-5") (v "0.2.0") (h "0w2k6dllrvqkl5klzz4d7s81n2vi2bbdnn05bbgxwpvml75m8rxs")))

