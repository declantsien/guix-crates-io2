(define-module (crates-io ju -t ju-tcs-rust-23-27) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-27-0.1.0 (c (n "ju-tcs-rust-23-27") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "03cchk2y6c1afl2q10qrfsp6788f8pnjkk42d8brcq8c3y3r4c52")))

(define-public crate-ju-tcs-rust-23-27-0.1.1 (c (n "ju-tcs-rust-23-27") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-28") (r "^0.1.0") (d #t) (k 0)))) (h "119ja76w0x4ybgpsbln3vl6sg8llqkh53sjd9spsq7kqb2332mjx")))

