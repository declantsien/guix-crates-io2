(define-module (crates-io ju -t ju-tcs-rust-23-7) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-7-0.1.0 (c (n "ju-tcs-rust-23-7") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "14fw91cdpzslkppzvmhnbkjkp98h1wv45pwxqv3anpbxvnmja2w5")))

(define-public crate-ju-tcs-rust-23-7-0.1.1 (c (n "ju-tcs-rust-23-7") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-8") (r "^0.1.2") (d #t) (k 0)))) (h "131r9cygfmhvhsx8rys1v1ifhm2kj7mdhwvvlkphfk50h532fzah")))

