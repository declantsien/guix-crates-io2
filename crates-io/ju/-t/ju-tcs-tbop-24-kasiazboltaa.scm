(define-module (crates-io ju -t ju-tcs-tbop-24-kasiazboltaa) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-kasiazboltaa-0.1.0 (c (n "ju-tcs-tbop-24-kasiazboltaa") (v "0.1.0") (h "1c1k23ij8fhp9b29x61ymdxc0xzz48ixgamb6cav9d0ylscgi8s4")))

(define-public crate-ju-tcs-tbop-24-kasiazboltaa-0.2.0 (c (n "ju-tcs-tbop-24-kasiazboltaa") (v "0.2.0") (d (list (d (n "ju-tcs-tbop-24-jjaworska") (r "^0.2.0") (d #t) (k 0)))) (h "10dglm9nd9v9isi4y4c7zcis1m3vmzl7naygidykz5db8jd2iqzg")))

(define-public crate-ju-tcs-tbop-24-kasiazboltaa-0.3.0 (c (n "ju-tcs-tbop-24-kasiazboltaa") (v "0.3.0") (h "054xkhl2v881v0xz4jal3qc4qgnar4dcmz2m94ws4vna6jxmwd55")))

(define-public crate-ju-tcs-tbop-24-kasiazboltaa-0.3.1 (c (n "ju-tcs-tbop-24-kasiazboltaa") (v "0.3.1") (h "13ylja4zcgav2n07apvdzjhw6va1l9g4fqp0bkdd616xffs96c1p")))

