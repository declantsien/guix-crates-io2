(define-module (crates-io ju -t ju-tcs-rust-23-24) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-24-0.1.0 (c (n "ju-tcs-rust-23-24") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1110yi5qadfza7s39pqdgqxjls01h59h7k9vi7zbh465925hvpim")))

(define-public crate-ju-tcs-rust-23-24-0.2.0 (c (n "ju-tcs-rust-23-24") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "0in6n6ad2clcmjvcwxvqpfk1bgcq4gyxsa230fz5dd7pa45adnhq")))

(define-public crate-ju-tcs-rust-23-24-0.2.1 (c (n "ju-tcs-rust-23-24") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x8chgnqhb4zxfk1kn6q4s05ikkr5jgcy9bcn592jsl1vmgsd46i")))

(define-public crate-ju-tcs-rust-23-24-0.2.2 (c (n "ju-tcs-rust-23-24") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zb0qv1fygg0wnph9dg1n0qb96b587z4s1g6xs8wzcjbgliwhx2h")))

(define-public crate-ju-tcs-rust-23-24-0.2.3 (c (n "ju-tcs-rust-23-24") (v "0.2.3") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sjbdca839b8d2c9l5c8fsynb3nygjw7s3ndhkwz0f59cjd1cpwq")))

(define-public crate-ju-tcs-rust-23-24-0.2.4 (c (n "ju-tcs-rust-23-24") (v "0.2.4") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-23") (r "^0.1.0") (d #t) (k 0)))) (h "15hbrszr7dzwzh1h0naj4hxbsma6g8wcmpcy3l7v1v5lpqxr9j82")))

