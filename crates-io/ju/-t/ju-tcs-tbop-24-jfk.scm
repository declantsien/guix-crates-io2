(define-module (crates-io ju -t ju-tcs-tbop-24-jfk) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-jfk-0.1.0 (c (n "ju-tcs-tbop-24-jfk") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ph5p8daw2sva361rdbka6xhzfiknsjigirhdz37vjpfydglqsri")))

(define-public crate-ju-tcs-tbop-24-jfk-0.1.1 (c (n "ju-tcs-tbop-24-jfk") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-bb") (r "^0.1.0") (d #t) (k 0)))) (h "1pwvmhjrc4p7mzbn0wzgqmi6qjvcl4lzqjp5scknb7rsqmllfys0")))

(define-public crate-ju-tcs-tbop-24-jfk-0.1.2 (c (n "ju-tcs-tbop-24-jfk") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-bb") (r "^0.1.0") (d #t) (k 0)))) (h "023dnhy86a2q6nlawdh969c8gfj6nl7vi463vmh2293b27zafnyv")))

