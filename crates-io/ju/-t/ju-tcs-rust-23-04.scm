(define-module (crates-io ju -t ju-tcs-rust-23-04) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-04-0.1.0 (c (n "ju-tcs-rust-23-04") (v "0.1.0") (h "0giaac59qslpaknrm8hwnif46rnrbkmfm3f40qa1ydg6qcsq9hz8")))

(define-public crate-ju-tcs-rust-23-04-0.2.0 (c (n "ju-tcs-rust-23-04") (v "0.2.0") (h "0jdknwnqp5yhyldm1f7n39d17zdrfpl0m4sha0hw8kil0fk8359f")))

(define-public crate-ju-tcs-rust-23-04-0.2.1 (c (n "ju-tcs-rust-23-04") (v "0.2.1") (h "06rszlxbjl350qfq6qsak0b4dvavbs8gng3vz7x4is211iv82a0s") (y #t)))

(define-public crate-ju-tcs-rust-23-04-0.3.0 (c (n "ju-tcs-rust-23-04") (v "0.3.0") (h "0virsk6108d141gxr861lzjrzcgnrri5wn1x2477340rps0817ib") (y #t)))

(define-public crate-ju-tcs-rust-23-04-1.0.0 (c (n "ju-tcs-rust-23-04") (v "1.0.0") (h "0s9srkp705gwavfjc4lm99jnpikg31z29fcs88clr81nr09cym14")))

