(define-module (crates-io ju -t ju-tcs-rust-2023-14) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-2023-14-0.1.0 (c (n "ju-tcs-rust-2023-14") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "134nllzk09f063c8dfpcqnb8v6imkwsyq6bhdx1gl09vqvgblszj")))

(define-public crate-ju-tcs-rust-2023-14-1.0.0 (c (n "ju-tcs-rust-2023-14") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uj-tcs-rust-2023-13") (r "^0.1.0") (d #t) (k 0)))) (h "1pr0fvlw1s2gh0k1xcxsrblqx9ffibhqzxkyh69dw5bn610q0141")))

