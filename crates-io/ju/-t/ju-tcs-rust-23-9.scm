(define-module (crates-io ju -t ju-tcs-rust-23-9) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-9-0.1.0 (c (n "ju-tcs-rust-23-9") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pih6201hywyx8ssb4xa83vj5lx8a7js2grdf579gjr31sa3yqj1")))

(define-public crate-ju-tcs-rust-23-9-0.2.0 (c (n "ju-tcs-rust-23-9") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-rust-23-10") (r "^0.1.0") (d #t) (k 0)))) (h "1b2nqf2d2q9b1hhzssqaa8kvwraixz8ig0hk3s4wczh7rwf2031z")))

