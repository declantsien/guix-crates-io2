(define-module (crates-io ju -t ju-tcs-rust-23-30) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-30-0.1.0 (c (n "ju-tcs-rust-23-30") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lmqda3axmr7pjbpkw122ppfynllcv3h11snc29qhk0b140n3v6a")))

(define-public crate-ju-tcs-rust-23-30-0.2.0 (c (n "ju-tcs-rust-23-30") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-29") (r "^0.1.0") (d #t) (k 0)))) (h "0x10h6if50l3aml79knxicc273703syfxabdk7pfxsqjmg8cylh1")))

(define-public crate-ju-tcs-rust-23-30-0.2.1 (c (n "ju-tcs-rust-23-30") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-29") (r "^0.1.0") (d #t) (k 0)))) (h "1j8pkxh6y0mv99p3gxlww7nifmvkfk2l2d7k09xw9hb1wir1f7rb")))

