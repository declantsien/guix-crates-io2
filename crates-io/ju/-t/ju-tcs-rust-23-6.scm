(define-module (crates-io ju -t ju-tcs-rust-23-6) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-6-0.1.0 (c (n "ju-tcs-rust-23-6") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cmszyy894qksaa4ryagaizlh1h2z7p6cc48m27z5jyf7gndx9b0")))

(define-public crate-ju-tcs-rust-23-6-0.2.0 (c (n "ju-tcs-rust-23-6") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-5") (r "^0.2.0") (d #t) (k 0)))) (h "1b02ilz7ghqqpwr7y0gmzh5vad2b482vacvl4hg6zbz9g4npgnca")))

