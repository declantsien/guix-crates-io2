(define-module (crates-io ju -t ju-tcs-rust-23-1) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-1-0.1.0 (c (n "ju-tcs-rust-23-1") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p72w0aj0qqalkcfzrr1rf0kakynvdqqyr1qm985ff1kshq43b2l")))

(define-public crate-ju-tcs-rust-23-1-0.2.0 (c (n "ju-tcs-rust-23-1") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-2") (r "^0.1.0") (d #t) (k 0)))) (h "0bdk39vw1i4phjwhn2di5mmhy92k7k0kqf36si6xhm474mywmhc5")))

