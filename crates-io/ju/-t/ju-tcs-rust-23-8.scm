(define-module (crates-io ju -t ju-tcs-rust-23-8) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-8-0.1.0 (c (n "ju-tcs-rust-23-8") (v "0.1.0") (h "0szwm3rkv3wh5ad0iwn0xblh7ga2rcgavvyqahk7f7v9q7xmclfx")))

(define-public crate-ju-tcs-rust-23-8-0.1.1 (c (n "ju-tcs-rust-23-8") (v "0.1.1") (h "09g15bkzw1f51f60ji62z7rgjnqr9krcrr01xzn7hq21g6jc5i4c")))

(define-public crate-ju-tcs-rust-23-8-0.1.2 (c (n "ju-tcs-rust-23-8") (v "0.1.2") (d (list (d (n "ju-tcs-rust-23-7") (r "^0.1.0") (d #t) (k 0)))) (h "1j93y9llc9bf1mkfc5hhgsmx47kfdf0gfz1igjfdb3g15m53i2pw")))

