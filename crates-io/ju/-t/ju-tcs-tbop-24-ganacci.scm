(define-module (crates-io ju -t ju-tcs-tbop-24-ganacci) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-ganacci-0.1.0 (c (n "ju-tcs-tbop-24-ganacci") (v "0.1.0") (h "1yl5hw407irgg225rmg0s5jr59z6p9v2yy6rp3p4sd3lra748g7s")))

(define-public crate-ju-tcs-tbop-24-ganacci-0.2.0 (c (n "ju-tcs-tbop-24-ganacci") (v "0.2.0") (h "1dkhdrzhl6a1kw0s5wlq1nr7ixv3mass5q5lrjmlql6lph3hmv2j")))

(define-public crate-ju-tcs-tbop-24-ganacci-0.3.0 (c (n "ju-tcs-tbop-24-ganacci") (v "0.3.0") (h "09sgx6nw5p0mp9pxw887w7l1qix7wgq3s62zchx5x25w5278d5w6")))

