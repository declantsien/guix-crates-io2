(define-module (crates-io ju -t ju-tcs-rust-23-21) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-21-0.1.0 (c (n "ju-tcs-rust-23-21") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wsvajq8m6iqf6717wxh8mr3rlyncqbhgzzklk3g5yiqy6fdbhny")))

(define-public crate-ju-tcs-rust-23-21-0.1.1 (c (n "ju-tcs-rust-23-21") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-22") (r "^0.1.0") (d #t) (k 0)))) (h "03rrj3dlc242zd3d09kghj2c89l2fshnbzi4d5d3j9icyygyz821")))

