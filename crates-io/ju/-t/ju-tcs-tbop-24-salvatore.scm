(define-module (crates-io ju -t ju-tcs-tbop-24-salvatore) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-salvatore-0.1.0 (c (n "ju-tcs-tbop-24-salvatore") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yy52argn7b1g7mb1ksa9vfc2mz86sy2mjr4sd6gzamzmj5yq5sl")))

(define-public crate-ju-tcs-tbop-24-salvatore-0.2.0 (c (n "ju-tcs-tbop-24-salvatore") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-ganacci") (r "^0.1.0") (d #t) (k 0)))) (h "0hy0avavfrxggmgw6ibk44cdli34k3mfvngjbf6jdjzf6vx9wjrx")))

(define-public crate-ju-tcs-tbop-24-salvatore-1.0.0 (c (n "ju-tcs-tbop-24-salvatore") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-ganacci") (r "^0.2.0") (d #t) (k 0)))) (h "0qy253ifhqqcxc2c68113r8bqxj3v41609d9a985dg7zgqabs9ll")))

(define-public crate-ju-tcs-tbop-24-salvatore-1.0.1 (c (n "ju-tcs-tbop-24-salvatore") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-ganacci") (r "^0.2.0") (d #t) (k 0)))) (h "048ajn51c582ggv38m8npalw08yaxnwp39xp8igl10i64a9c07p9")))

