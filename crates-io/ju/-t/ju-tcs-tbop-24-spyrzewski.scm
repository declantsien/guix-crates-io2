(define-module (crates-io ju -t ju-tcs-tbop-24-spyrzewski) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-spyrzewski-0.1.0 (c (n "ju-tcs-tbop-24-spyrzewski") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h5c0940ql0h3ng4x17c9vaxnbq6ymkffk3kgg0pndqnidad2ghp")))

(define-public crate-ju-tcs-tbop-24-spyrzewski-0.2.0 (c (n "ju-tcs-tbop-24-spyrzewski") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-pyzik") (r "^0.2.0") (d #t) (k 0)))) (h "0as717wq2wb2w114fhwnqjlihv2rd6zv1xi9z79kpm0wrfag01p7")))

