(define-module (crates-io ju -t ju-tcs-tbop-24-pyzik) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-pyzik-0.1.0 (c (n "ju-tcs-tbop-24-pyzik") (v "0.1.0") (h "0fk1j4vxn30bcqangzf0jaycrlk1hs6mm7c4a8nf6s7s09dyg41h")))

(define-public crate-ju-tcs-tbop-24-pyzik-0.2.0 (c (n "ju-tcs-tbop-24-pyzik") (v "0.2.0") (h "11q34gcvdszqhw96rr7gky6cmii3a5108hs29w4i16snqwlhyjby")))

