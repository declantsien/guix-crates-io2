(define-module (crates-io ju -t ju-tcs-tbop-24-jjaworska) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-jjaworska-0.1.0 (c (n "ju-tcs-tbop-24-jjaworska") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i0rqwysmxpnxrmr1jfn4fg0kw62p538bppwzx04hdgkzv7wv3p7")))

(define-public crate-ju-tcs-tbop-24-jjaworska-0.2.0 (c (n "ju-tcs-tbop-24-jjaworska") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-kasiazboltaa") (r "^0.1.0") (d #t) (k 0)))) (h "0qnjjgdccgif7csjpikwnp28q86a5d1qsfpfx7x0vdjhdvc70s77")))

