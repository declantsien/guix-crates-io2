(define-module (crates-io ju -t ju-tcs-tbop-24-oloi-tailhead) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-oloi-tailhead-0.1.0 (c (n "ju-tcs-tbop-24-oloi-tailhead") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qncsax42d8n0iahmpifbz48aviqbmcw38r489ydgcqmv64cgwf3") (y #t)))

(define-public crate-ju-tcs-tbop-24-oloi-tailhead-0.1.1 (c (n "ju-tcs-tbop-24-oloi-tailhead") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-clappy") (r "^0.1.2") (d #t) (k 0)))) (h "0qibs0jsdhnvpp8ziinjwsm4d0h86qksf6bkbppgcbk0k2964mhm") (y #t)))

(define-public crate-ju-tcs-tbop-24-oloi-tailhead-0.1.2 (c (n "ju-tcs-tbop-24-oloi-tailhead") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-clappy") (r "^0.2.2") (d #t) (k 0)))) (h "01a7dpbrzkzk0ag7q0yz5zf1q8ba4l288n9hygdkifvzgiay48s0")))

