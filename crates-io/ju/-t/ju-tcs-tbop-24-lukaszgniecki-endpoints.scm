(define-module (crates-io ju -t ju-tcs-tbop-24-lukaszgniecki-endpoints) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-lukaszgniecki-endpoints-0.1.0 (c (n "ju-tcs-tbop-24-lukaszgniecki-endpoints") (v "0.1.0") (d (list (d (n "ju-tcs-tbop-24-bebidek-endpoints") (r "^0.1.0") (d #t) (k 0)))) (h "01q7bhc3jksws7id058v6kvkmalvgyrj94f50yzl9zj7zjxqlqqy")))

(define-public crate-ju-tcs-tbop-24-lukaszgniecki-endpoints-1.0.0 (c (n "ju-tcs-tbop-24-lukaszgniecki-endpoints") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-bebidek-endpoints") (r "^1.0.0") (d #t) (k 0)))) (h "0zjp8p6skkcphnxzz9y34vs37s7pf4wvisi6sjhmw2j2zz6xl5zp")))

(define-public crate-ju-tcs-tbop-24-lukaszgniecki-endpoints-2.0.0 (c (n "ju-tcs-tbop-24-lukaszgniecki-endpoints") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-tbop-24-bebidek-endpoints") (r "^2.0.0") (d #t) (k 0)))) (h "1shj0rhb36cyb1l8ayz75c7418knczsqmj177f3jqsbpzbk09bnx")))

