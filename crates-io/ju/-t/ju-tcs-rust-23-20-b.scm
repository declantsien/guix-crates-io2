(define-module (crates-io ju -t ju-tcs-rust-23-20-b) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-20-b-0.1.0 (c (n "ju-tcs-rust-23-20-b") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-20") (r "^0.1.1") (d #t) (k 0)))) (h "159zi95cg0x7q27afg7n9qh16hs1f9sgphb91k477rgn5i7ga3rr")))

