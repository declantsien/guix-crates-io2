(define-module (crates-io ju -t ju-tcs-tbop-24-lib-dcfk) #:use-module (crates-io))

(define-public crate-ju-tcs-tbop-24-lib-dcfk-0.1.0 (c (n "ju-tcs-tbop-24-lib-dcfk") (v "0.1.0") (h "1mjh1ijlq4a1ldlb48m0wmbpk5g3qcxj1y9c9s66p4q89dvvhk99")))

(define-public crate-ju-tcs-tbop-24-lib-dcfk-1.0.0 (c (n "ju-tcs-tbop-24-lib-dcfk") (v "1.0.0") (h "101pqccq1srvq9la3mapr9l0vdvz1iifqz64cwi7j55bgs9qlkn2")))

(define-public crate-ju-tcs-tbop-24-lib-dcfk-1.0.1 (c (n "ju-tcs-tbop-24-lib-dcfk") (v "1.0.1") (h "1l0zrq6n7ncy86nj4qp0wrr8b8q3lhzh87p0miih4aw3qr5m5ia6")))

