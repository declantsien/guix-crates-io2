(define-module (crates-io ju -t ju-tcs-rust-23-15) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-23-15-0.1.0 (c (n "ju-tcs-rust-23-15") (v "0.1.0") (h "12x7iyy7ay77my5n4cjf9hiwrva7gf7376nc5nrkgz946jcvrjl3") (y #t)))

(define-public crate-ju-tcs-rust-23-15-0.1.1 (c (n "ju-tcs-rust-23-15") (v "0.1.1") (h "0mvja4wc9n6v55hw7h2fdqmmld8qx6hw3dxzlax8k62irkra5xdd") (y #t)))

(define-public crate-ju-tcs-rust-23-15-0.1.2 (c (n "ju-tcs-rust-23-15") (v "0.1.2") (h "192hnbc5vf0waqb1d1976rri8l5r669lwf23c5n0ilvifqccyj2l") (y #t)))

(define-public crate-ju-tcs-rust-23-15-0.1.3 (c (n "ju-tcs-rust-23-15") (v "0.1.3") (h "10lgciacxwck7i4hrpqbaa5cgklpidz92813yjgaxzlc4bp1kri3") (y #t)))

