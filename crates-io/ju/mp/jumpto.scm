(define-module (crates-io ju mp jumpto) #:use-module (crates-io))

(define-public crate-jumpto-0.1.0 (c (n "jumpto") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)))) (h "02prbba40s3bcqav4lrhk740kh9g96rzh8f53bq5flbi3m2glbyf")))

