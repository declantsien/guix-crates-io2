(define-module (crates-io ju mp jumprope) #:use-module (crates-io))

(define-public crate-jumprope-0.1.0 (c (n "jumprope") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "ropey") (r "^1.3.1") (d #t) (k 0)))) (h "1g7kljw6iyccnws9xxxviyk3amip1wkahw3dx2abqhhx8nbs9as5")))

(define-public crate-jumprope-0.2.0 (c (n "jumprope") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2.3") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "ropey") (r "^1.3.1") (d #t) (k 0)))) (h "1133il6k2xbilc3in5gzzlic9lmkg3gs0769pcz1milkxi80mvd3")))

(define-public crate-jumprope-0.3.0 (c (n "jumprope") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "ropey") (r "^1.3.1") (d #t) (k 0)))) (h "1a5mjq1qbixylzn0r8fqlwpmmc7d2lim9n3z9dnjfz8q39771f4k") (f (quote (("default" "ddos_protection") ("ddos_protection"))))))

(define-public crate-jumprope-0.3.1 (c (n "jumprope") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "ropey") (r "^1.3.1") (d #t) (k 0)))) (h "0igq9k6av77n19k5n0jm9lsi7dy0y4yc32nklwipvwd8a1a9cma4") (f (quote (("default" "ddos_protection") ("ddos_protection"))))))

(define-public crate-jumprope-0.4.0 (c (n "jumprope") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "ropey") (r "^1.3.1") (d #t) (k 0)))) (h "0sp40inqyhdlpzc4cbj0518r1kswrvfljjqzdahbns7hkn1iixfr") (f (quote (("default" "ddos_protection") ("ddos_protection"))))))

(define-public crate-jumprope-0.5.0 (c (n "jumprope") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0nzxl14plpyi24vp92ns0irjd072ximc3sbv67xyfzyg5qb9mqc0") (f (quote (("wchar_conversion") ("default" "ddos_protection") ("ddos_protection"))))))

(define-public crate-jumprope-0.5.1 (c (n "jumprope") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0rk70svpf0amwaizga0zckfa9h3s98yqwx7dwb3k6y49bqy4zmv4") (f (quote (("wchar_conversion") ("default" "ddos_protection") ("ddos_protection"))))))

(define-public crate-jumprope-0.5.2 (c (n "jumprope") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "str_indices") (r "^0.3.1") (d #t) (k 0)))) (h "04a6hlsd2vp961cq2s3nx497ssv5mjs579d33dxp1dzvdgynj7nk") (f (quote (("wchar_conversion") ("default" "ddos_protection") ("ddos_protection"))))))

(define-public crate-jumprope-0.5.3 (c (n "jumprope") (v "0.5.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "str_indices") (r "^0.3.1") (d #t) (k 0)))) (h "1rx1xsh1qgiv61c8amjhh9bxj3mls7147mimr6azkqw8dxznps6y") (f (quote (("wchar_conversion") ("default" "ddos_protection") ("ddos_protection"))))))

(define-public crate-jumprope-1.0.0 (c (n "jumprope") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "str_indices") (r "^0.3.2") (d #t) (k 0)))) (h "07ksra6k25r5rns7d5lhhjxg3njxzp4rs5l7kphmylwa00ndnkm1") (f (quote (("wchar_conversion") ("default" "ddos_protection") ("ddos_protection") ("buffered"))))))

(define-public crate-jumprope-1.1.0 (c (n "jumprope") (v "1.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "str_indices") (r "^0.4.0") (d #t) (k 0)))) (h "0dfi0a2v3dbdrvr2fb95mzsd7z8v5241awmd61jdf9lckff8afsg") (f (quote (("wchar_conversion") ("default" "ddos_protection") ("ddos_protection") ("buffered"))))))

(define-public crate-jumprope-1.1.1 (c (n "jumprope") (v "1.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "str_indices") (r "^0.4.0") (d #t) (k 0)))) (h "18jm33lv6bkx7cimq6q6c753fmppxgiljpp4lnx6kawjj0i17zvn") (f (quote (("wchar_conversion") ("default" "ddos_protection") ("ddos_protection") ("buffered"))))))

(define-public crate-jumprope-1.1.2 (c (n "jumprope") (v "1.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "str_indices") (r "^0.4.0") (d #t) (k 0)))) (h "1y2fd90ynh0vlqwnx3mp9rfacx0mj922n0jv8ajx586xi3z79742") (f (quote (("wchar_conversion") ("line_conversion") ("default" "ddos_protection") ("ddos_protection") ("buffered"))))))

