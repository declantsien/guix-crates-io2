(define-module (crates-io ju mp jumphash) #:use-module (crates-io))

(define-public crate-jumphash-0.1.0 (c (n "jumphash") (v "0.1.0") (d (list (d (n "clippy") (r ">= 0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "siphasher") (r "~0") (d #t) (k 0)))) (h "12jaspj7c8wkbcm8q3kp8dh9rxfaz5vw9d03m2pr4gimjaq80jlw")))

(define-public crate-jumphash-0.1.1 (c (n "jumphash") (v "0.1.1") (d (list (d (n "clippy") (r ">= 0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "siphasher") (r "~0") (d #t) (k 0)))) (h "0zj129im3vhvxcn7mwaqa4nv53rhh7yrvbpkf71hv5qxp4a0jk5z")))

(define-public crate-jumphash-0.1.2 (c (n "jumphash") (v "0.1.2") (d (list (d (n "clippy") (r ">= 0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "siphasher") (r "~0.1.3") (d #t) (k 0)))) (h "0w4lvyqs81ddkwrqvxaldj553wl9gg31li9w2hiz77zkv1kwgcha")))

(define-public crate-jumphash-0.1.3 (c (n "jumphash") (v "0.1.3") (d (list (d (n "clippy") (r ">= 0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "siphasher") (r "~0.2") (d #t) (k 0)))) (h "0aq423p044d3r4qx60gsyz7mnm7fmrbz6xflc41cmln78ykybw02")))

(define-public crate-jumphash-0.1.4 (c (n "jumphash") (v "0.1.4") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "0x5wnvh36i7wifi29l85ckvhlp4g6g3wvw8gxx2716sc2nlb6qal")))

(define-public crate-jumphash-0.1.5 (c (n "jumphash") (v "0.1.5") (d (list (d (n "rand") (r "~0.6") (d #t) (k 0)) (d (n "siphasher") (r "~0.2") (d #t) (k 0)))) (h "02alah2l699ily2ijq77s0sqh9zyd41ybz87crlmbzp28glpfdva")))

(define-public crate-jumphash-0.1.6 (c (n "jumphash") (v "0.1.6") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1cjiwmjmvcbfn53q4y4mhp1gjs3j9mvshq5crnf18cs3nbd37ghi")))

(define-public crate-jumphash-0.1.8 (c (n "jumphash") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "182xj5l20xx8ih7pbw8ppbqmm8fs8lxhg1bbx8mb1hg6ar41vjyn")))

