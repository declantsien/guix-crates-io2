(define-module (crates-io ju mp jumpy) #:use-module (crates-io))

(define-public crate-jumpy-0.3.6 (c (n "jumpy") (v "0.3.6") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "08dclhvqz1qdvqsr4cgdbqd4iq20i7dpqb98igj8rys9z29dh07g")))

(define-public crate-jumpy-0.4.3 (c (n "jumpy") (v "0.4.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "dunce") (r "^1.0.4") (d #t) (k 0)))) (h "0hpaz3xljyvikippzl3apgjdqasmi0vpqdg31l0jymsvkhgd9r1f")))

