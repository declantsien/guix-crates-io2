(define-module (crates-io ju mp jumper) #:use-module (crates-io))

(define-public crate-jumper-0.1.0 (c (n "jumper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("sqlite" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00yjsdw06k255g5bc22704mrv482h8g7v6kgbh5d8gv8hdhd34xi")))

(define-public crate-jumper-0.1.1 (c (n "jumper") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("sqlite" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14f1jcqji74j7y8kvf6s2rph432s259l64r4ff2gr238n4hx75qw")))

(define-public crate-jumper-0.1.2 (c (n "jumper") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("sqlite" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c3hhq4npgx76x4lhrpqwi08aimgjsij4q381599xr8mcl7nmfyc")))

(define-public crate-jumper-0.1.3 (c (n "jumper") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("sqlite" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pcsq6q1ydi0a5k2p1fl7kljhbzx47hm89k3dwpqlzbz88h49iia")))

