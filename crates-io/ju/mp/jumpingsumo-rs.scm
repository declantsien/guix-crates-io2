(define-module (crates-io ju mp jumpingsumo-rs) #:use-module (crates-io))

(define-public crate-jumpingsumo-rs-0.0.1 (c (n "jumpingsumo-rs") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)))) (h "0smhwfwdkawrff69amf9c9jqx7bns5vfm4xzml31j6z9h08q1wlw")))

(define-public crate-jumpingsumo-rs-0.0.2 (c (n "jumpingsumo-rs") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arsdk-rs") (r "^0.0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1ij2754dznfakkknnlynvb10jns421fbi2ncsy8cgr0lz691sv17")))

