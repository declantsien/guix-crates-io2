(define-module (crates-io v4 l2 v4l2-sys) #:use-module (crates-io))

(define-public crate-v4l2-sys-1.0.0 (c (n "v4l2-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)))) (h "0wf77jqn9bghrlx7nfbfyma54grgk3pzn6kzncc9k2ryld5kq7bp")))

(define-public crate-v4l2-sys-1.0.1 (c (n "v4l2-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)))) (h "1fcvqn15sm312i47psbml76r4n9d6436yy0h78rny03ssg0aba4h")))

(define-public crate-v4l2-sys-1.1.0 (c (n "v4l2-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)))) (h "0mlzh4qay8ml4slpv65ydqj0zx4aqwpy7cb7kwam2i3r5gjighni")))

