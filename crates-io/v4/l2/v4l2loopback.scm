(define-module (crates-io v4 l2 v4l2loopback) #:use-module (crates-io))

(define-public crate-v4l2loopback-0.1.0 (c (n "v4l2loopback") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "01jbrlgcmn8k3a91hsyy2aanx71skjvnxyhrc58cmqn0j1rf465v")))

