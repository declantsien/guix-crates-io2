(define-module (crates-io v4 l2 v4l2r) #:use-module (crates-io))

(define-public crate-v4l2r-0.0.1 (c (n "v4l2r") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "clap") (r "^3.2") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 2)) (d (n "enumn") (r "^0.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl" "mman" "poll" "fs" "event"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "001pwidarwf5zl2l7qx323jmg8bkmmqikbhi1k32q3msv6yxh0pc")))

(define-public crate-v4l2r-0.0.2 (c (n "v4l2r") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "clap") (r "^3.2") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 2)) (d (n "enumn") (r "^0.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl" "mman" "poll" "fs" "event"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "004axcdim5nk41md25z6p588hx15pwk1caflv1i889dgc534l7wh")))

