(define-module (crates-io v4 l2 v4l2-sys-mit) #:use-module (crates-io))

(define-public crate-v4l2-sys-mit-0.1.0 (c (n "v4l2-sys-mit") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0j75mc4jrqimq5k246r8l88w96gh5gfzsgghx1649nq3gagyi031")))

(define-public crate-v4l2-sys-mit-0.2.0 (c (n "v4l2-sys-mit") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)))) (h "0ki535aipfnwvbzk7zwn8mw96yd8y8a44q4z4bxivbzldp035jg0")))

(define-public crate-v4l2-sys-mit-0.3.0 (c (n "v4l2-sys-mit") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "02wr26prs8x5qirnax7g6y42wqb9prmcgslkg3fcmfmrca1qfyb7")))

