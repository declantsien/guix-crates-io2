(define-module (crates-io v4 v- v4v-types) #:use-module (crates-io))

(define-public crate-v4v-types-0.1.0 (c (n "v4v-types") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0ikzifbpgrw5lb325nhq40p2yrck5ambfhgds957sirl5lx9mqkm") (y #t)))

