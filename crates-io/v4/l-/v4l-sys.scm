(define-module (crates-io v4 l- v4l-sys) #:use-module (crates-io))

(define-public crate-v4l-sys-0.1.0 (c (n "v4l-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "12d5ypvaa1kmcbiyxpw5lc4d691b93r4kpj4iffiwk7lkwk42g81") (l "v4l1 v4l2 4lconvert")))

(define-public crate-v4l-sys-0.2.0 (c (n "v4l-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)))) (h "1zj78arn0pani67qhqgbz4p8r9j3spr998bvmyha3838jrzw87gr") (l "v4l1 v4l2 4lconvert")))

(define-public crate-v4l-sys-0.3.0 (c (n "v4l-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0c7sjm9wzqlaf5dsf5y98ck618j6ib0wjibr25nygw3njjdlw88n") (l "v4l1 v4l2 v4lconvert")))

