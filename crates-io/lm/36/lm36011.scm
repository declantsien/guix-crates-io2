(define-module (crates-io lm #{36}# lm36011) #:use-module (crates-io))

(define-public crate-lm36011-0.1.0 (c (n "lm36011") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "152sifsz73rd9a23k1w3asz0rqik7r2nljkgddrcdbckbhpy1277")))

