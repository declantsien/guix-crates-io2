(define-module (crates-io lm gp lmgpt) #:use-module (crates-io))

(define-public crate-lmgpt-0.0.1 (c (n "lmgpt") (v "0.0.1") (d (list (d (n "lambda_mountain") (r "^0.0") (d #t) (k 0)))) (h "1qvq14mrg29xmkx0ayj1fyz0pdhnzqypqax8aqjn1pkn77vn43jg")))

(define-public crate-lmgpt-0.0.2 (c (n "lmgpt") (v "0.0.2") (d (list (d (n "lambda_mountain") (r "^0.0") (d #t) (k 0)))) (h "087k53b80pqlg0x7ind0fk9gbd5pik33rw04jpkjznirg11h7sdl")))

(define-public crate-lmgpt-0.1.0 (c (n "lmgpt") (v "0.1.0") (d (list (d (n "lambda_mountain") (r "^0.0") (d #t) (k 0)) (d (n "openai_api_rust") (r "^0.1.8") (d #t) (k 0)))) (h "16cswz4b8s50d707m2g2pf3fpk4i6m4y50kh7543a9g7xfzy61xs")))

(define-public crate-lmgpt-0.1.1 (c (n "lmgpt") (v "0.1.1") (d (list (d (n "lambda_mountain") (r "^0.0.16") (d #t) (k 0)) (d (n "openai_api_rust") (r "^0.1.8") (d #t) (k 0)))) (h "0ch16m76jdggxvs227lq3sdkz0xppwa0w8y9b0nvvxmcvyprb48x")))

(define-public crate-lmgpt-0.1.2 (c (n "lmgpt") (v "0.1.2") (d (list (d (n "lambda_mountain") (r "^0.0.16") (d #t) (k 0)) (d (n "openai_api_rust") (r "^0.1.8") (d #t) (k 0)))) (h "11l1kj3rvns476xyldv69grbs6qlhm5cnb51zjvxhm4j1rq63iqj")))

(define-public crate-lmgpt-0.1.3 (c (n "lmgpt") (v "0.1.3") (d (list (d (n "lambda_mountain") (r "^0.0.16") (d #t) (k 0)) (d (n "openai_api_rust") (r "^0.1.8") (d #t) (k 0)))) (h "15jjyracwjp3wzvdfa1mj5iv6yrwzic9v9r3v0nvgsmqpr1jk58l")))

(define-public crate-lmgpt-0.1.4 (c (n "lmgpt") (v "0.1.4") (d (list (d (n "lambda_mountain") (r "^0.0.16") (d #t) (k 0)) (d (n "openai_api_rust") (r "^0.1.8") (d #t) (k 0)))) (h "1pan6fkq9338x6x80x4dd6vrhn8r4f2j0a0mm5b9wkjcx6rk2z4k")))

(define-public crate-lmgpt-0.1.5 (c (n "lmgpt") (v "0.1.5") (d (list (d (n "lambda_mountain") (r "^0.0.16") (d #t) (k 0)) (d (n "openai_api_rust") (r "^0.1.8") (d #t) (k 0)))) (h "0ll5h3ahpik9wvhwgx8cc7b9l0k0zcx4s9gq63qcpr918amy4h6l")))

