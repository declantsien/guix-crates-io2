(define-module (crates-io lm ak lmake_readme) #:use-module (crates-io))

(define-public crate-lmake_readme-0.4.0 (c (n "lmake_readme") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "0r3q4whsfy12q4mqml0hl4w11wynjz9nbsajhpvf7g6w3wyrgdm1") (y #t)))

(define-public crate-lmake_readme-0.5.1 (c (n "lmake_readme") (v "0.5.1") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1a9m4ppdz50xpqdnm599yjpzzn5786ghg16fpc06xs4kh5zw70ds") (y #t)))

(define-public crate-lmake_readme-0.5.2 (c (n "lmake_readme") (v "0.5.2") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1cp5b6ib6zg9qrgc5nsfy0hv793yzsrqc6flwxpzy3kzxq5n5zbq") (y #t)))

(define-public crate-lmake_readme-0.5.4 (c (n "lmake_readme") (v "0.5.4") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1c5ncr3mynd8lqpnigh3k0jmbms3cn0q93zvkjr6vsni67902l7j") (y #t)))

