(define-module (crates-io lm ak lmake_semver) #:use-module (crates-io))

(define-public crate-lmake_semver-0.1.1 (c (n "lmake_semver") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "0pl50biw64203v0q4jk51zak542jr1hc0gscx8bmj35j1szaibky") (y #t)))

(define-public crate-lmake_semver-0.1.6 (c (n "lmake_semver") (v "0.1.6") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1l76d343s7bxz0xs8csbvpm3ma9x3s0w99hwrs2q76ll0lpmipkq") (y #t)))

(define-public crate-lmake_semver-0.1.11 (c (n "lmake_semver") (v "0.1.11") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1wmsiwqixh8j521f9qby215zwql16zq14nyh32js1l6lw9s9nfrw") (y #t)))

(define-public crate-lmake_semver-0.1.12 (c (n "lmake_semver") (v "0.1.12") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1r6nmr6bryr7h1zl5dj259z0fcb73klaf2g0m3ffpxn3r9yxvd3z") (y #t)))

