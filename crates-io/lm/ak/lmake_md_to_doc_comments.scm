(define-module (crates-io lm ak lmake_md_to_doc_comments) #:use-module (crates-io))

(define-public crate-lmake_md_to_doc_comments-0.5.4 (c (n "lmake_md_to_doc_comments") (v "0.5.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1f9fijq0kp7id36fb2ywigkl3alp64lvx91vg7c1qw8sy7g8avci") (y #t)))

(define-public crate-lmake_md_to_doc_comments-0.5.5 (c (n "lmake_md_to_doc_comments") (v "0.5.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1s7rmzha4bapvvmgmx2jhhc2xf6qb58mp79q8lvgl6q5723gcw2q") (y #t)))

(define-public crate-lmake_md_to_doc_comments-0.5.6 (c (n "lmake_md_to_doc_comments") (v "0.5.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "06klshdi751h141yg2h720y8z8dq1vg6q9nhhkqf1159s2q4fjgd") (y #t)))

