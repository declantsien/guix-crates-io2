(define-module (crates-io lm in lminc) #:use-module (crates-io))

(define-public crate-lminc-2.0.1 (c (n "lminc") (v "2.0.1") (d (list (d (n "uuid") (r "^1.2.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 2)))) (h "16cb4ml9vnz5y0lk4h4ns3hgx663s12fxffvdj3nbm1fxrwbflw6") (f (quote (("std" "alloc") ("extended") ("default" "std" "extended") ("alloc"))))))

