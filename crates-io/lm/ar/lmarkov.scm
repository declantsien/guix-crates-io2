(define-module (crates-io lm ar lmarkov) #:use-module (crates-io))

(define-public crate-lmarkov-0.1.0 (c (n "lmarkov") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0gmkqssjyxssxxvb5ib52zmj1zmj1y9khqsxw7gnbq7c35dd8pxw") (f (quote (("serialization" "serde" "serde_json"))))))

