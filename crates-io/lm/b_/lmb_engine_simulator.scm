(define-module (crates-io lm b_ lmb_engine_simulator) #:use-module (crates-io))

(define-public crate-lmb_engine_simulator-0.1.0 (c (n "lmb_engine_simulator") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.2") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.34") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jnlsnk2jb2yh48s2vmx1fnzv6qqc6y2p7a57dz4p3d95fmj238f")))

