(define-module (crates-io lm #{3s}# lm3s6965) #:use-module (crates-io))

(define-public crate-lm3s6965-0.1.0 (c (n "lm3s6965") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (f (quote ("device"))) (d #t) (k 0)))) (h "1f0ddrffrhhf0b3006f7dnrp53hxzq094b1vvbb7jm3a170cp2fx")))

(define-public crate-lm3s6965-0.1.1 (c (n "lm3s6965") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (f (quote ("device"))) (d #t) (k 0)))) (h "11r6m38qv7sizgqxgln94xj5blzq0m1qx6zsvyss0fbjf88q3yim")))

(define-public crate-lm3s6965-0.1.2 (c (n "lm3s6965") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (f (quote ("device"))) (d #t) (k 0)))) (h "1zb6196yzvkdp51i4n7sbrig4hv76ywndylgqwvz6b9qdhm7s1v2")))

(define-public crate-lm3s6965-0.1.3 (c (n "lm3s6965") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (f (quote ("device"))) (d #t) (k 0)))) (h "1bg02nwsdw5lk5d6yxhgg396mfyxs4na763jkyn0w5lmfhm09646")))

(define-public crate-lm3s6965-0.2.0 (c (n "lm3s6965") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (f (quote ("device"))) (d #t) (k 0)))) (h "0ijl97sqvnir78w393jkpdx6jhmdszqmx6gp9i1zvs7yc19yvmqk")))

