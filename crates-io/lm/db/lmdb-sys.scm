(define-module (crates-io lm db lmdb-sys) #:use-module (crates-io))

(define-public crate-lmdb-sys-0.3.0 (c (n "lmdb-sys") (v "0.3.0") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1dncn6pfix37rlrpbp3j0kbsjzgzyvma47nga1vghw6cvabfhzlb")))

(define-public crate-lmdb-sys-0.4.0 (c (n "lmdb-sys") (v "0.4.0") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0jayz3rzz8f1nvm78na23q1vhsxvwgrf8h61715pa72aayb2y2ww")))

(define-public crate-lmdb-sys-0.4.2 (c (n "lmdb-sys") (v "0.4.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0ry0xx4y5xghlrik0kbrf76hxbx3dsa02sl6hprfdxfaj4csimkz")))

(define-public crate-lmdb-sys-0.4.3 (c (n "lmdb-sys") (v "0.4.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1ny7cw6mhba8c47saha4p7yz4qdc1m9y0il73isbpc6hqw5f4z60")))

(define-public crate-lmdb-sys-0.5.0 (c (n "lmdb-sys") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1rfkrr4r347xgwp1d7s2hxlnjyxxzn8l752mm4if7nlr9snig749")))

(define-public crate-lmdb-sys-0.6.0 (c (n "lmdb-sys") (v "0.6.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1xw946lc7y29kgjk38154r1mwrw4kp1msiy0p82pn2j5yigsms7r")))

(define-public crate-lmdb-sys-0.7.0 (c (n "lmdb-sys") (v "0.7.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0kj6p4fcijxnnwhvfivyg9iyi9vfl1djjk4lkxi1pm1p0y1cc949")))

(define-public crate-lmdb-sys-0.7.1 (c (n "lmdb-sys") (v "0.7.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0mk68xqi3mqkvfpz6zcyn1hlhkv4wsfkj3asc42wwwpmkh4fhzqv")))

(define-public crate-lmdb-sys-0.7.2 (c (n "lmdb-sys") (v "0.7.2") (d (list (d (n "gcc") (r "= 0.3.51") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1s8sq72hx18lxvbfim681mx6gwvvpg9kami6wglw2vs1cwbqxd9x")))

(define-public crate-lmdb-sys-0.8.0 (c (n "lmdb-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1scc28phjcywr9jq78d01g35rig8l2bwydxcdzl5i27yij1r5cym")))

