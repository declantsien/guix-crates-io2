(define-module (crates-io lm db lmdb-master3-sys) #:use-module (crates-io))

(define-public crate-lmdb-master3-sys-0.1.0 (c (n "lmdb-master3-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "1grzd59yya0jbl2vrrw5lhz0b0w23dhqpwywqvq1h1g8mqkb5j88") (f (quote (("with-fuzzer-no-link" "vendored") ("with-fuzzer" "vendored") ("with-asan" "vendored") ("vendored") ("default"))))))

