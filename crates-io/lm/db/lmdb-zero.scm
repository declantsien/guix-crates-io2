(define-module (crates-io lm db lmdb-zero) #:use-module (crates-io))

(define-public crate-lmdb-zero-0.1.0 (c (n "lmdb-zero") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "lmdb-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "099k2rrj867df3fw9a8j21f5rgzx3hsvfi6p8a1bv8is5qm2myyp") (f (quote (("lax_alignment"))))))

(define-public crate-lmdb-zero-0.2.0 (c (n "lmdb-zero") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0rqy0ffqyfpjj8996nk8rcrqy5fyls85yx8znc55srg6inp2jz5l") (f (quote (("lax_alignment"))))))

(define-public crate-lmdb-zero-0.2.1 (c (n "lmdb-zero") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "16qx3lxzw7kg1fw0vb9pfvmaiwc37ym5gh3fd8ivfdzr32gaqdqp") (f (quote (("lax_alignment"))))))

(define-public crate-lmdb-zero-0.2.2 (c (n "lmdb-zero") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1c6yka9k3r49bhish5shhsf6pb77cl0sqkansz58ix4vmcmgjx9z") (f (quote (("lax_alignment"))))))

(define-public crate-lmdb-zero-0.3.0 (c (n "lmdb-zero") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0a7i2g79hybslg9jzs0h946rpcvhdmh91qicd4gs95npmzgw0gkv")))

(define-public crate-lmdb-zero-0.3.1 (c (n "lmdb-zero") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0hypk6and63w2mag0gqkyrywv3qjc9pmq2pi3jxkrdy3yqh9ljkq")))

(define-public crate-lmdb-zero-0.4.0 (c (n "lmdb-zero") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "19xd7l0x04bgknxssnp9sb36bjxi2idgh0j8xjiyzmpyxdra55za")))

(define-public crate-lmdb-zero-0.4.1 (c (n "lmdb-zero") (v "0.4.1") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "01045dsxyc9w1w81m6jhpxyjq2q2sr4in7vpm388yndrkn9pwg0f")))

(define-public crate-lmdb-zero-0.4.2 (c (n "lmdb-zero") (v "0.4.2") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "gcc") (r "= 0.3.39") (d #t) (k 2)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "= 0.2.18") (d #t) (k 2)) (d (n "liblmdb-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "= 0.3.4") (d #t) (k 2)))) (h "0r6yvaci0vi83v8z12rqzz4m6c2rq6sh93c4f07v36mspjhihnh0")))

(define-public crate-lmdb-zero-0.4.3 (c (n "lmdb-zero") (v "0.4.3") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "gcc") (r "= 0.3.39") (d #t) (k 2)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "= 0.2.18") (d #t) (k 2)) (d (n "liblmdb-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "= 0.3.4") (d #t) (k 2)))) (h "1ss1qf1421by5n5gj336n1n5mmcsnbxqhhlwlkyfvkq4m58qkfy4")))

(define-public crate-lmdb-zero-0.4.4 (c (n "lmdb-zero") (v "0.4.4") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "gcc") (r "= 0.3.39") (d #t) (k 2)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "= 0.2.18") (d #t) (k 2)) (d (n "liblmdb-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "= 0.3.4") (d #t) (k 2)))) (h "13f5bh2wws6i8dqikkm5l8ds8bd29prg2dagjci7q22vfkp6wh8k")))

