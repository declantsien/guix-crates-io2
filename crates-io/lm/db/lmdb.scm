(define-module (crates-io lm db lmdb) #:use-module (crates-io))

(define-public crate-lmdb-0.1.0 (c (n "lmdb") (v "0.1.0") (h "056zfkgjmv6n3qm74dqy23ffgjmvd0l552402mh1apbwvd0k9akk")))

(define-public crate-lmdb-0.3.0 (c (n "lmdb") (v "0.3.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "lmdb-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "18n5fy1jc3mkr99cg10yqpm2yy4b7q594zv1ld22h015v5i96kdz")))

(define-public crate-lmdb-0.4.0 (c (n "lmdb") (v "0.4.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "lmdb-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1cwgz58bcrrmm8yi37a7jiml3393skhylzh4zr6k91yl9ir3abmp")))

(define-public crate-lmdb-0.4.2 (c (n "lmdb") (v "0.4.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "lmdb-sys") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "1ks39jf3kyjzbpyhgsycmdkw29rmr3i1m1knzaf8q7l8jdkxzmqi")))

(define-public crate-lmdb-0.4.3 (c (n "lmdb") (v "0.4.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "lmdb-sys") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "04mrhs366h4mcw2da13r8kr0f4vh2isr1bf5d3pkcxp03jkk4sim")))

(define-public crate-lmdb-0.5.0 (c (n "lmdb") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lmdb-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0n0hak9lyb4kgrgw45dmfss7akj545vhxrzk68vhnfhzwhp93vqd")))

(define-public crate-lmdb-0.6.0 (c (n "lmdb") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lmdb-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "025lyiflrfrbnkc4jdr54dxfbw7d3bbg0dz07wxq4asr4fkzfppy")))

(define-public crate-lmdb-0.7.0 (c (n "lmdb") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lmdb-sys") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1g30mazpkfp34aln2cswn5dp7043r4b7kh8v8xysbdw8cn4brh68")))

(define-public crate-lmdb-0.7.1 (c (n "lmdb") (v "0.7.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lmdb-sys") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0q41627iisvnijq7xp3gxs1pjpykr2vngykk92abxj9j0l9sgzfx")))

(define-public crate-lmdb-0.7.2 (c (n "lmdb") (v "0.7.2") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lmdb-sys") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0pf62bw7nffj3jc8ph1dpx0rdwbi3609vzvp1svqc4rcapqppb24")))

(define-public crate-lmdb-0.8.0 (c (n "lmdb") (v "0.8.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lmdb-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0fc54in9i27v1gpxncay5s85lqx27m0r2vyrfylnljfnnpphh2av")))

