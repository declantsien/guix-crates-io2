(define-module (crates-io lm db lmdb-sys2) #:use-module (crates-io))

(define-public crate-lmdb-sys2-0.11.2 (c (n "lmdb-sys2") (v "0.11.2") (d (list (d (n "bindgen") (r "^0.66") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0346gqdq0hha5b3alf21llhisns8dismfjbfls27d8xzl8q6g7gc") (f (quote (("with-fuzzer-no-link") ("with-fuzzer") ("with-asan") ("mdb_idl_logn_9") ("mdb_idl_logn_8") ("mdb_idl_logn_15") ("mdb_idl_logn_14") ("mdb_idl_logn_13") ("mdb_idl_logn_12") ("mdb_idl_logn_11") ("mdb_idl_logn_10") ("default"))))))

