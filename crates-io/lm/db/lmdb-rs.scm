(define-module (crates-io lm db lmdb-rs) #:use-module (crates-io))

(define-public crate-lmdb-rs-0.0.7 (c (n "lmdb-rs") (v "0.0.7") (d (list (d (n "liblmdb-sys") (r "*") (d #t) (k 0)))) (h "0wa2ky02jhsnrhn2xkcwh2sbbnyi6kk8zbidn963ap6czz4hdl5p")))

(define-public crate-lmdb-rs-0.0.8 (c (n "lmdb-rs") (v "0.0.8") (d (list (d (n "liblmdb-sys") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.1.3") (d #t) (k 0)))) (h "1jmz7kwr4d39wz86z7hg24xdwk1vi5hlg1c7z8m64hip67k8y6i4")))

(define-public crate-lmdb-rs-0.1.0 (c (n "lmdb-rs") (v "0.1.0") (d (list (d (n "liblmdb-sys") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.1.3") (d #t) (k 0)))) (h "1fk8mfw9xmf315b1gfwjgjyaxx2812j4frndmk2jd6k7qla5dav3") (y #t)))

(define-public crate-lmdb-rs-0.1.1 (c (n "lmdb-rs") (v "0.1.1") (d (list (d (n "liblmdb-sys") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.1.5") (d #t) (k 0)))) (h "0jf9jq3jiw6v38agx3g168na9x7fvnl7fzclvjz6fxvmxa4ipqh3")))

(define-public crate-lmdb-rs-0.1.3 (c (n "lmdb-rs") (v "0.1.3") (d (list (d (n "liblmdb-sys") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.1.6") (d #t) (k 0)))) (h "1xqlw8davm1k1ajyq4d057s9kqp8rg78spv48pklwz304bj0c89z")))

(define-public crate-lmdb-rs-0.1.4 (c (n "lmdb-rs") (v "0.1.4") (d (list (d (n "liblmdb-sys") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.1.6") (d #t) (k 0)))) (h "15x3wmq8pi1rsh56yn6pmjiz8vyydjnn05bp8jf98n0xs1z6x3xl")))

(define-public crate-lmdb-rs-0.1.5 (c (n "lmdb-rs") (v "0.1.5") (d (list (d (n "liblmdb-sys") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.1.6") (d #t) (k 0)))) (h "023k8dhag4rahxwrxplmif1hxqjwnvriymi8fmqyqq8chjiyh8c3")))

(define-public crate-lmdb-rs-0.1.6 (c (n "lmdb-rs") (v "0.1.6") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "liblmdb-sys") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.1.0") (d #t) (k 0)))) (h "0wfbjafpm460cdsdysmgwgkkvzy9px4hs1fjl0kkm82vavng6yzs")))

(define-public crate-lmdb-rs-0.1.7 (c (n "lmdb-rs") (v "0.1.7") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.10") (d #t) (k 0)))) (h "19kmp9dlg5924yhgrw4jxg5b08q12fmibi2ba88sayzw392si7a6")))

(define-public crate-lmdb-rs-0.2.0 (c (n "lmdb-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.10") (d #t) (k 0)))) (h "13dygmgpk6ak0lfir6187w6mkpvhap6yhjad5pl1g600brc64g15")))

(define-public crate-lmdb-rs-0.2.1 (c (n "lmdb-rs") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.10") (d #t) (k 0)))) (h "1nwrbc121s2pr6a75jgh4v58i6ldvs56ljy0xaqhnivzqrws39xh")))

(define-public crate-lmdb-rs-0.2.2 (c (n "lmdb-rs") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.10") (d #t) (k 0)))) (h "00xjgsl56ql4qrixf0qcxlkgdwjvg1rjg5lkqyzsy3xzhdhks219")))

(define-public crate-lmdb-rs-0.2.3 (c (n "lmdb-rs") (v "0.2.3") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.10") (d #t) (k 0)))) (h "06wlbhfflcf267z01c9agsg9yhhg809kkfc7g903kxmqs6x7q2i0")))

(define-public crate-lmdb-rs-0.3.0 (c (n "lmdb-rs") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1q7c9vmqpg957wdpmqgsyddcm1cyjmhacikvaj0v718gbqrv04fv")))

(define-public crate-lmdb-rs-0.3.2 (c (n "lmdb-rs") (v "0.3.2") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "16illy272d5mi1dcq9g5skkd11z55yj0xbh8cnra7j6klfkg57l4")))

(define-public crate-lmdb-rs-0.3.3 (c (n "lmdb-rs") (v "0.3.3") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "09pqxl93r6ymh147myrj7nimiwawsiyjjnng2yzv7h02dnnhs0h9")))

(define-public crate-lmdb-rs-0.3.4 (c (n "lmdb-rs") (v "0.3.4") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1ys3784i4c8lmfmhf18azfsqjza40pif3n7hf5csw14f8gq2xgc1")))

(define-public crate-lmdb-rs-0.4.0 (c (n "lmdb-rs") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1rdp71cymrim0bwy28mm278viyc4mjndbad7q4464xz7gids30yh")))

(define-public crate-lmdb-rs-0.4.1 (c (n "lmdb-rs") (v "0.4.1") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1hsy37x548ybh8g3vkvjvmvsrza469i96xgi48ayjl8jga9m4h9m")))

(define-public crate-lmdb-rs-0.4.2 (c (n "lmdb-rs") (v "0.4.2") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "10hzic0w0k3jq954wbgqv0hry0mmb8q74pjidlh3jz981bhm5sd8")))

(define-public crate-lmdb-rs-0.4.3 (c (n "lmdb-rs") (v "0.4.3") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1j2wj8i11i0xy72skfa03c5grcximd3h53gqnh9lrznp3qz2xhzi")))

(define-public crate-lmdb-rs-0.4.4 (c (n "lmdb-rs") (v "0.4.4") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "045hcb0i2hy6a3lym60m1lg6am5081j6id33j1znb6qya4jj0gbf")))

(define-public crate-lmdb-rs-0.5.0 (c (n "lmdb-rs") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "17f2f5gjhq3ppca39fqma3kna10nfr3224jykgn9dlk02mr7xp6z")))

(define-public crate-lmdb-rs-0.6.0 (c (n "lmdb-rs") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1x7fhqj9axjahh16c9v97d9dqpdvw3q0wpibl2d8liy4bzyrrrnw")))

(define-public crate-lmdb-rs-0.6.1 (c (n "lmdb-rs") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.1.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "075rj4nj8cdbginf2yf0qjk9j9ykr475fad9i1xzqabl7s5xxynl")))

(define-public crate-lmdb-rs-0.7.0 (c (n "lmdb-rs") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0yi0jls2ccph2sx124nsa5p65fzrz5rh71aqrkxg7k7jq37c92fm")))

(define-public crate-lmdb-rs-0.7.1 (c (n "lmdb-rs") (v "0.7.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "00hjpcsw5kkb4bapk6a9df75w5qr6jxhp3pswzvyjpkgbjxpip9v")))

(define-public crate-lmdb-rs-0.7.2 (c (n "lmdb-rs") (v "0.7.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1dzd7kpshmm4zwqbgrwyipgipzi07mqigqs1q3lfarw57ln8fnzi")))

(define-public crate-lmdb-rs-0.7.3 (c (n "lmdb-rs") (v "0.7.3") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0aqw070dddqyzhxgcprgmnncmf8rqw0zd5pds6z3ivfnn5a6cnns")))

(define-public crate-lmdb-rs-0.7.4 (c (n "lmdb-rs") (v "0.7.4") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0rikb7cng1k0vzv2bygjpd3bd7f58lcqy5f325zs08c34bmk55ys")))

(define-public crate-lmdb-rs-0.7.5 (c (n "lmdb-rs") (v "0.7.5") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1vn1vs6w0w1i0amw2walr3r9b3wrdjyilfc5d8mal9jiav19khkm")))

(define-public crate-lmdb-rs-0.7.6 (c (n "lmdb-rs") (v "0.7.6") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liblmdb-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1rv7c7iwgbwx92igm2r3aza8phr0lvvwqd9gwjf655gp6fsfgvsa")))

