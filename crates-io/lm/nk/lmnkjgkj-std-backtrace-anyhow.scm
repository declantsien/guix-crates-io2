(define-module (crates-io lm nk lmnkjgkj-std-backtrace-anyhow) #:use-module (crates-io))

(define-public crate-lmnkjgkj-std-backtrace-anyhow-0.0.1 (c (n "lmnkjgkj-std-backtrace-anyhow") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1plil35bksnpfc9fv4mx36xxb2s7vfack46wdyk2ifljylwjxj3p") (f (quote (("std") ("default" "std")))) (y #t) (r "1.39")))

