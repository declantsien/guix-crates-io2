(define-module (crates-io lm #{73}# lm73) #:use-module (crates-io))

(define-public crate-lm73-0.1.0 (c (n "lm73") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "i2cdev") (r "^0.4") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4") (d #t) (k 2)))) (h "0lpyqx6nlhahvknnsdjawi3dxg2kixym19pf59hipal1n3dpzh0f")))

(define-public crate-lm73-0.1.1 (c (n "lm73") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.0") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "fpa") (r "^0.1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "163fq4p5xigssj7g2l5k3c78nqjl78cigxnpjp12llapychz20a7")))

