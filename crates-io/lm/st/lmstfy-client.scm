(define-module (crates-io lm st lmstfy-client) #:use-module (crates-io))

(define-public crate-lmstfy-client-0.1.0 (c (n "lmstfy-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xccdlb3rjivzymv3y35p9az0ppwynw0vms32xwjbx6k194w444a")))

(define-public crate-lmstfy-client-0.1.1 (c (n "lmstfy-client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zh11gkmdnh8g5d2hr2fdswld8ks1qjb0kjgn7yafshfgm7vqrch")))

(define-public crate-lmstfy-client-0.1.2 (c (n "lmstfy-client") (v "0.1.2") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 2)) (d (n "envmnt") (r "^0.8.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1lc1sjsx9gs74m6qah3s7xnmqgssk9gv9400cn5355hhl3wy8yq8")))

(define-public crate-lmstfy-client-0.1.3 (c (n "lmstfy-client") (v "0.1.3") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 2)) (d (n "envmnt") (r "^0.8.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1gar91a01v5qdqvkdrarvbz7b6c2lnyb7nn9mp9pgnzhb11wqpm6")))

(define-public crate-lmstfy-client-0.1.4 (c (n "lmstfy-client") (v "0.1.4") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 2)) (d (n "envmnt") (r "^0.8.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "035zi0dxkhka9va4wqvp1qi1kjjq4m81fi4ffhb1a6v16i5z28v7")))

