(define-module (crates-io lm c- lmc-assembly) #:use-module (crates-io))

(define-public crate-lmc-assembly-0.1.0 (c (n "lmc-assembly") (v "0.1.0") (h "1wj556l75mla9azpglh479wk1v4i4h5m9i88ws7qfpdr3hrw1wl9")))

(define-public crate-lmc-assembly-0.1.1 (c (n "lmc-assembly") (v "0.1.1") (h "1cvivwy1zws2amhzwml7czp3w32sg1dlcyswj9zzjacjglncmkz9")))

(define-public crate-lmc-assembly-0.1.2 (c (n "lmc-assembly") (v "0.1.2") (h "087xpjy5bxhd45x6akd1gi69kir07rfc7f57psrz7609lkwl4vz7")))

(define-public crate-lmc-assembly-0.1.3 (c (n "lmc-assembly") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "104fskpgf4svpzqjicx5lh9ld7h00wd9q9s5j2x3g7fx91k2pyh8") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive" "dep:serde_arrays"))))))

(define-public crate-lmc-assembly-0.1.4 (c (n "lmc-assembly") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0hc9f5j8b83n97c17dipz4y46liq22246cx1lv163qz430hsq21c") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive" "dep:serde_arrays"))))))

(define-public crate-lmc-assembly-0.1.5 (c (n "lmc-assembly") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07dl30cs3mrwnnm8z7siv6kv6d2yaabfvck9fiqzmkf5lnqph587") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive" "dep:serde_arrays"))))))

(define-public crate-lmc-assembly-0.1.6 (c (n "lmc-assembly") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zx7wmx5sg84p4biv8qqmlpcsb7yf6n4bal10wfp59nm9ig5ngpv") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive" "dep:serde_arrays"))))))

(define-public crate-lmc-assembly-0.1.7 (c (n "lmc-assembly") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "03cp0gv1ryzgfjsryi72391vbai8rl140365ym6j9j8xb4ljs6a0") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive" "dep:serde_arrays"))))))

