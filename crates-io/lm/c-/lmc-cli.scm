(define-module (crates-io lm c- lmc-cli) #:use-module (crates-io))

(define-public crate-lmc-cli-0.1.0 (c (n "lmc-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lmc-assembly") (r "^0.1") (d #t) (k 0)))) (h "0nk4fynqvyfhl61lh564wickh7sqrh1ypviw4yin7pgfazv2z0zc")))

(define-public crate-lmc-cli-0.1.1 (c (n "lmc-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lmc-assembly") (r "^0.1.6") (d #t) (k 0)))) (h "1gfh6b6s2r75ssfzvdz6caqmpjv673zs9i2x0z4lj4vmr4sw078l")))

(define-public crate-lmc-cli-0.1.2 (c (n "lmc-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lmc-assembly") (r "^0.1.7") (d #t) (k 0)))) (h "1cxgjg18r9drv5kym80y8i4w2wyfvc9bscdpy38il9kmrvz69qwn")))

