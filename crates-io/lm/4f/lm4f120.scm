(define-module (crates-io lm #{4f}# lm4f120) #:use-module (crates-io))

(define-public crate-lm4f120-0.1.0 (c (n "lm4f120") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "0qf6a9klshlxa8b57q8rj3c7y5kin347xkl54rxd6davb7ly2dlw")))

(define-public crate-lm4f120-0.1.1 (c (n "lm4f120") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "179xi9chwkrjqhh6chh0rx4v0cz8n4dzhj6ksh49dirrx3czzs46")))

(define-public crate-lm4f120-0.2.0 (c (n "lm4f120") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.1.4") (d #t) (k 0)) (d (n "embedded-serial") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "0hbb3maf8jrz8xvpm9fi8qfkxh7y3ki9xxhsgn0rwzrk33hq692n")))

(define-public crate-lm4f120-0.3.0 (c (n "lm4f120") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.1.4") (d #t) (k 0)) (d (n "embedded-serial") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "0536i85lzavv72gl0vcwa1hdrailpk2kbnmpr0x0w17x41wxj8l0")))

(define-public crate-lm4f120-0.4.0 (c (n "lm4f120") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.1.4") (d #t) (k 0)) (d (n "embedded-serial") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "0rsmv8q42lgy9vwyqkk7c7nz1pxrjdkvqsbzsyrwzkvg1b2wkay4")))

(define-public crate-lm4f120-0.5.0 (c (n "lm4f120") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.1.4") (d #t) (k 0)) (d (n "embedded-serial") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "10zynbbs85mgn1cfhnz8pzy7v33jkhi5d6zx6pq47mya0hi0arba")))

(define-public crate-lm4f120-0.5.1 (c (n "lm4f120") (v "0.5.1") (d (list (d (n "cortex-m") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-serial") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "tm4c123x") (r "^0.4.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0vgp3axjhxfbsqf8gw8hkm9hpp360ibym9rszj4mh6r8sl9md83w")))

(define-public crate-lm4f120-0.7.0 (c (n "lm4f120") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "embedded-serial") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "tm4c123x") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "1xsnwnnm9nd0xjbqcimkparga8pzvadis8safx2zd5kv9vk3l83b")))

(define-public crate-lm4f120-0.8.0 (c (n "lm4f120") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "embedded-serial") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "tm4c123x") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "10v8d6324xv5272nz0wd93pqggxw738vfmwa6g6pf6gn7l4kdv4l")))

(define-public crate-lm4f120-0.9.0 (c (n "lm4f120") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "tm4c123x") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "06ynsm05dvfjay34ifk6d7aq3d6lp1jqkl3zgk4zcyj0vifx8akb")))

(define-public crate-lm4f120-0.9.1 (c (n "lm4f120") (v "0.9.1") (d (list (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "tm4c123x") (r "^0.6") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "1dyj7f073wcfggggkxq9saagir72ldn1qa097s343jwyiai0d09k")))

(define-public crate-lm4f120-0.10.0 (c (n "lm4f120") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "tm4c123x") (r "^0.6") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "0ki8yi9h609bpjkscfkgn68k4il884nv13dccjg20vpd4qknnfi6")))

