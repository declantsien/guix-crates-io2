(define-module (crates-io lm br lmbr_sys) #:use-module (crates-io))

(define-public crate-lmbr_sys-0.1.0 (c (n "lmbr_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "lmbr_build") (r "^0.1") (d #t) (k 1)))) (h "0m7rg19xh1gxyq8z40ac9bn797mm6wa28r30qibf9w5xmqffrdxp") (f (quote (("lmbr_fw_azcore") ("lmbr_editor") ("default" "lmbr_editor" "lmbr_fw_azcore"))))))

