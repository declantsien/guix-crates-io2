(define-module (crates-io lm br lmbr) #:use-module (crates-io))

(define-public crate-lmbr-0.1.0 (c (n "lmbr") (v "0.1.0") (d (list (d (n "lmbr_logger") (r "^0.1") (d #t) (k 0)) (d (n "lmbr_sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "016c0dlzgavvz4jb3s1xz239pfh6l0xhm8la5wm0d1a2vn5fvzgw") (f (quote (("default" "lmbr_sys"))))))

