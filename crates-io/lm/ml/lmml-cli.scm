(define-module (crates-io lm ml lmml-cli) #:use-module (crates-io))

(define-public crate-lmml-cli-0.0.0 (c (n "lmml-cli") (v "0.0.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lmml") (r "^0.0.0") (d #t) (k 0)) (d (n "lmml-parser") (r "^0.0.0") (d #t) (k 0)))) (h "01anc01mblikbhd6sdn00ir0y76kcy47qbm13mmdcawmbg0xbggc")))

(define-public crate-lmml-cli-0.1.0 (c (n "lmml-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lmml") (r "^0.1.0") (d #t) (k 0)) (d (n "lmml-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)))) (h "013yglifx6x8kc1pq6k00ax3q57jy3cnjsj2191alpjh1h8qkzml")))

(define-public crate-lmml-cli-0.2.0 (c (n "lmml-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lmml") (r "^0.2.0") (d #t) (k 0)) (d (n "lmml-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)))) (h "1dsicx090ga5x61v76k4jq3ncidldmfbam9pjdi84mb3rhwxwds9")))

(define-public crate-lmml-cli-0.3.0 (c (n "lmml-cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lmml") (r "^0.3.0") (d #t) (k 0)) (d (n "lmml-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)))) (h "1snszknwafd1klxz3nnpjdsm753zszizlj37ic8g529wqz7hixpz")))

(define-public crate-lmml-cli-0.4.0 (c (n "lmml-cli") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lmml") (r "^0.4.0") (d #t) (k 0)) (d (n "lmml-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)))) (h "1nqzwbcgri763kx39qzpdg9g0f9z2yxvad6ljwhwz3k185qllf4w")))

(define-public crate-lmml-cli-0.4.1 (c (n "lmml-cli") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lmml") (r "^0.4.1") (d #t) (k 0)) (d (n "lmml-parser") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)))) (h "0pd2q62ry3rsmhx35yg2k0p27qb6wr5dyxb5j80717pzf00vb0ps")))

(define-public crate-lmml-cli-0.5.0 (c (n "lmml-cli") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lmml") (r "^0.5.0") (d #t) (k 0)) (d (n "lmml-parser") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)))) (h "1h207sin6p9z8a6cf3zydiy5q0c2f9m3ffhc0qgdprxxdp9c8hp3")))

(define-public crate-lmml-cli-0.5.1 (c (n "lmml-cli") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lmml") (r "^0.5.1") (d #t) (k 0)) (d (n "lmml-parser") (r "^0.5.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rodio") (r "^0.18.0") (d #t) (k 0)))) (h "0yf7rak71cxzg5lzcmcqldk05apfsgs3p4h0l73qyisiqipsfqdy")))

