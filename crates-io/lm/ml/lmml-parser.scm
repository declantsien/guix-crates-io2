(define-module (crates-io lm ml lmml-parser) #:use-module (crates-io))

(define-public crate-lmml-parser-0.0.0 (c (n "lmml-parser") (v "0.0.0") (d (list (d (n "lmml") (r "^0.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1p63wyzjwnbk5bc9n0pcsacajfxdckfl406a6i37iblpk9v4f32l")))

(define-public crate-lmml-parser-0.1.0 (c (n "lmml-parser") (v "0.1.0") (d (list (d (n "lmml") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "03hvf3hznjf5aj7s0b54v2ck5mas1kqwvpsb65hw9mx714q6dhmv")))

(define-public crate-lmml-parser-0.2.0 (c (n "lmml-parser") (v "0.2.0") (d (list (d (n "lmml") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0l2h6pq6zknapyzn66n2fvbs949klph609nr4byg28dlrff59l2s")))

(define-public crate-lmml-parser-0.3.0 (c (n "lmml-parser") (v "0.3.0") (d (list (d (n "lmml") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0qmpxvp95nwsi5p27arwzz712q0bv6lgymxcpiq1yydp75hk70n8")))

(define-public crate-lmml-parser-0.4.0 (c (n "lmml-parser") (v "0.4.0") (d (list (d (n "lmml") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0mpjz8ld3njhi0g5fcvs9vljhdrnll7if5xyzpm4v0i9xi38vvj3")))

(define-public crate-lmml-parser-0.4.1 (c (n "lmml-parser") (v "0.4.1") (d (list (d (n "lmml") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1vvkdnwaw5yywk1rchlsnbkzd8vsdf4kihkxdka490gd08axllmj")))

(define-public crate-lmml-parser-0.5.0 (c (n "lmml-parser") (v "0.5.0") (d (list (d (n "lmml") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0xjnri3krgjbmjr27wnknjl7gqfmcks0vgvpqb9ky32yqfsfmh53")))

(define-public crate-lmml-parser-0.5.1 (c (n "lmml-parser") (v "0.5.1") (d (list (d (n "lmml") (r "^0.5.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0rn98r26c59bvwk8hfn3gn3xqrg75hb6g08lqqk57xq6fhk949hs")))

