(define-module (crates-io lm #{75}# lm75) #:use-module (crates-io))

(define-public crate-lm75-0.1.0 (c (n "lm75") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "138mahjbchyxjba9p9p36wwvxbsl087h8g2aqix4p1a1mmh4l7bb")))

(define-public crate-lm75-0.1.1 (c (n "lm75") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "070bfg7lh8xn3s8vby41bqkdanwdkjya2a9zxqg4qw688a32m9yi")))

(define-public crate-lm75-0.1.2 (c (n "lm75") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1g91nx5m4sh0mbvfh5iyxfjfbd8640z104lrdmrvrlnykpz14v5w")))

(define-public crate-lm75-0.2.0 (c (n "lm75") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1dhdbp8plvdfnj1alr1mm39kfzbmz7i1sbccwjdppnnnlz3wsnph")))

(define-public crate-lm75-1.0.0 (c (n "lm75") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)))) (h "1w6xl3prw2h6v5sg35snlmf978n80wggklhcxp1k4gscd6ajs323")))

