(define-module (crates-io lm bd lmbd) #:use-module (crates-io))

(define-public crate-lmbd-0.1.0 (c (n "lmbd") (v "0.1.0") (h "1c8n8zwhshch4aivncz5zb56zdv3f51f3pm5nvmj4j837nqxz77c")))

(define-public crate-lmbd-0.1.1 (c (n "lmbd") (v "0.1.1") (h "1san55q70d9cz96mhvfj9kmvg4qyhn94ir6shbn5cbxl6iimj5kc")))

(define-public crate-lmbd-0.1.2 (c (n "lmbd") (v "0.1.2") (h "0lc1ihjq7wpyjbapsdnc4k9c3ggyl8kk1s7m59cm3jx6bqhfil82")))

(define-public crate-lmbd-0.1.3 (c (n "lmbd") (v "0.1.3") (h "1ypvhv595zwnnyp6rm8n455ai10sb409h4g50xgg7ssrxcjmyvr7")))

(define-public crate-lmbd-0.1.4 (c (n "lmbd") (v "0.1.4") (h "0b2pxd804pwzmcsar7p9lkybyqvgplp6nmr00w1a989fdl3qyzri")))

