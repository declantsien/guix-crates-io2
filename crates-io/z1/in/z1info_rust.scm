(define-module (crates-io z1 in z1info_rust) #:use-module (crates-io))

(define-public crate-z1info_rust-0.1.0 (c (n "z1info_rust") (v "0.1.0") (h "0n4bcjy997fpxw42mg5l74cckf8cl0687iw0nzy74v14cx9yn6zi") (y #t)))

(define-public crate-z1info_rust-0.1.1 (c (n "z1info_rust") (v "0.1.1") (h "1wn9pkkxcqa4pp5idprzf98rg4mikm2cvx6gd0nmvz98v2wcvb5z")))

(define-public crate-z1info_rust-0.1.2 (c (n "z1info_rust") (v "0.1.2") (h "0zbypki726vqzda3a1w9y7m65rq595q84c5vnkqw2yyc7nfcd1bk")))

(define-public crate-z1info_rust-0.1.3 (c (n "z1info_rust") (v "0.1.3") (h "1zzqyqgh7w0gpzx52016w4c1wvis8kxssc9xiv2yvx6bd7nppic4")))

(define-public crate-z1info_rust-0.1.4 (c (n "z1info_rust") (v "0.1.4") (h "15hx8ffm1rza9dwlwlc39hnpv9hmnk5qlq7vsm6j4h1l6xfz4549")))

