(define-module (crates-io rk il rkill) #:use-module (crates-io))

(define-public crate-rkill-0.1.0 (c (n "rkill") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "heim") (r "^0.1.0-rc.1") (f (quote ("process"))) (k 0)) (d (n "netstat2") (r "^0.9.1") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.2") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "08bd9n6nxw0dk3wavz9g32gsnsfifd386g6mc71np9smcmagsqjn")))

(define-public crate-rkill-0.1.1 (c (n "rkill") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "heim") (r "^0.1.0-rc.1") (f (quote ("process"))) (k 0)) (d (n "netstat2") (r "^0.9.1") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.2") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "0ai0h5fybf26rrhrn8zx26161k8fng6i6pr5avbbwwwkl7wkmiv8")))

