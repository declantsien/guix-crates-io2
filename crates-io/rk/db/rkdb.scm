(define-module (crates-io rk db rkdb) #:use-module (crates-io))

(define-public crate-rkdb-0.2.0 (c (n "rkdb") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (f (quote ("full" "extra-traits"))) (d #t) (k 1)))) (h "0xnaw6160wb4li4d74iaw31bk6s31mir0yx8aaznwbdg05xacpm3") (f (quote (("unchecked_utf8") ("api")))) (l "kdb")))

(define-public crate-rkdb-0.2.1 (c (n "rkdb") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (f (quote ("full" "extra-traits"))) (d #t) (k 1)))) (h "0j5j18zjxxa3sim5qd1z4bq4ym4ilhb2ww4z0cjr6i2nk30djlsa") (f (quote (("unchecked_utf8") ("api")))) (l "kdb")))

(define-public crate-rkdb-0.3.0 (c (n "rkdb") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (f (quote ("full" "extra-traits"))) (d #t) (k 1)))) (h "01qgbdr7vcw4d5vdm0k8yfc4ly8476ain1r3y7swp8ddbh1fggzf") (f (quote (("unchecked_utf8") ("api")))) (l "kdb")))

(define-public crate-rkdb-0.4.0 (c (n "rkdb") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (f (quote ("full" "extra-traits"))) (d #t) (k 1)))) (h "1f8gx8a63lysdh4zz7a7mzg49qa507xw5jwbwrgadq18amwys467") (f (quote (("unchecked_utf8") ("api")))) (l "kdb")))

