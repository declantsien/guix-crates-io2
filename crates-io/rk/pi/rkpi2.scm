(define-module (crates-io rk pi rkpi2) #:use-module (crates-io))

(define-public crate-rkpi2-0.1.0 (c (n "rkpi2") (v "0.1.0") (d (list (d (n "os_pipe") (r "^0.9.1") (d #t) (k 2)) (d (n "zstd") (r "^0.5.1") (d #t) (k 0)))) (h "1bd2sglgpr15mlzsjndix6jd3brpda65npj4zyxalx9zvlrh2v76")))

(define-public crate-rkpi2-0.1.2 (c (n "rkpi2") (v "0.1.2") (d (list (d (n "os_pipe") (r "^0.9.1") (d #t) (k 2)) (d (n "zstd") (r "^0.5.1") (d #t) (k 0)))) (h "04vp2nbgqhcxb1d1wagy2zblb8j1ybyfncgrgmb1ph8i3i5zf7zl")))

