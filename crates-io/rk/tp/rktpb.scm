(define-module (crates-io rk tp rktpb) #:use-module (crates-io))

(define-public crate-rktpb-1.0.0 (c (n "rktpb") (v "1.0.0") (d (list (d (n "comrak") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rocket") (r "^0.5") (d #t) (k 0)) (d (n "rocket_dyn_templates") (r "^0.1") (f (quote ("tera"))) (d #t) (k 0)) (d (n "syntect") (r "^5") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc") (d #t) (k 0)))) (h "1c22fbblpfxy9rj4a93m5s6i73lf3fsgsvq51nrazj7a1cq9fzkh")))

