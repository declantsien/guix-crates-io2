(define-module (crates-io rk #{33}# rk3399-pac) #:use-module (crates-io))

(define-public crate-rk3399-pac-0.1.0 (c (n "rk3399-pac") (v "0.1.0") (d (list (d (n "portable-atomic") (r "^0.3.16") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "03mz7zax6x1qrdapxrp7frzrg4hcfkh39rzh2ir383j92havf4pl")))

(define-public crate-rk3399-pac-0.1.1 (c (n "rk3399-pac") (v "0.1.1") (d (list (d (n "portable-atomic") (r "^0.3.16") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0bx9b04ja7hpvv5w870hywyv9a0zn3rnh6i2vqk10xrgqnwf6kw2")))

(define-public crate-rk3399-pac-0.1.2 (c (n "rk3399-pac") (v "0.1.2") (d (list (d (n "portable-atomic") (r "^0.3.16") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0gk6fm0inm30ijgfjiv0qnxsflxryzg2phwlhmqi39a1v23d3r86")))

(define-public crate-rk3399-pac-0.1.3 (c (n "rk3399-pac") (v "0.1.3") (d (list (d (n "portable-atomic") (r "^0.3.16") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0ajrky5d67v9ggyv914n46sz447h28qdr0r0qjf9kb047p6x9y97")))

(define-public crate-rk3399-pac-0.1.4 (c (n "rk3399-pac") (v "0.1.4") (d (list (d (n "portable-atomic") (r "^0.3.16") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0irxpspjbd6g2sjc7ykfykwaci0pm4pka5w05wq2waqmpak7mpy3")))

