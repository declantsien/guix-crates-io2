(define-module (crates-io rk #{33}# rk3399-rs) #:use-module (crates-io))

(define-public crate-rk3399-rs-0.1.0 (c (n "rk3399-rs") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0fl6svn4wdf7v61qz5dlfnpss53pzgix93a0c47n6xjka7k2c2d4") (y #t)))

(define-public crate-rk3399-rs-0.1.1 (c (n "rk3399-rs") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0f61sgad6f7hwr0ak67i6fli57sjg5srcjn5qagl2bcsdja9za7g") (y #t)))

(define-public crate-rk3399-rs-0.1.2 (c (n "rk3399-rs") (v "0.1.2") (d (list (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "10d02z991ps9rijhrscczxlbiafgcqlf3jzin7r7zrmc6m6g482x") (y #t)))

(define-public crate-rk3399-rs-0.1.3 (c (n "rk3399-rs") (v "0.1.3") (d (list (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0w72sd9gq4b5pfh25i26r6q153g8imfqavff5ym466wg59nflvs3") (y #t)))

(define-public crate-rk3399-rs-0.1.4 (c (n "rk3399-rs") (v "0.1.4") (d (list (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0lswlk45f589f8m6b5v9a3d2icg2rd49dg8g10fa2k289xschy0v") (y #t)))

(define-public crate-rk3399-rs-0.1.5 (c (n "rk3399-rs") (v "0.1.5") (d (list (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "03bdss74gxd0qgjzzza1q9rvvyl1q4n2rgskagn48bk0a1q88ka9") (y #t)))

(define-public crate-rk3399-rs-0.1.6 (c (n "rk3399-rs") (v "0.1.6") (d (list (d (n "portable-atomic") (r "^0.3.16") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "03vyl43251ylx5k6wkz79cmx65n2xx06lyg5p7gdnnxqcjj2kccd") (y #t)))

(define-public crate-rk3399-rs-0.1.7 (c (n "rk3399-rs") (v "0.1.7") (d (list (d (n "portable-atomic") (r "^0.3.16") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0iv01k1mmxb15l7prb8n5p69n1f0603vv1064b8x0zhzbd6w6mwg") (y #t)))

