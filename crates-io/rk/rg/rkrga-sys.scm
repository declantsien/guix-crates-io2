(define-module (crates-io rk rg rkrga-sys) #:use-module (crates-io))

(define-public crate-rkrga-sys-0.1.0 (c (n "rkrga-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1244k3p2n6xy0jsyi85213wsmwmdinh6vfq7n2zg5rr1swv8zal0")))

(define-public crate-rkrga-sys-0.1.1 (c (n "rkrga-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (o #t) (d #t) (k 1)))) (h "0nnk698advlmpa9ajlkypiqdp1zjzfmpqlmq4hs564xz9l1xjb6n") (f (quote (("use-bindgen" "bindgen") ("default"))))))

