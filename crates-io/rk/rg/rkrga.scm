(define-module (crates-io rk rg rkrga) #:use-module (crates-io))

(define-public crate-rkrga-0.1.0 (c (n "rkrga") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "rkrga-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0z3aphg68kn8747vkdibrph522b9k16jqq1vl5y6a8yjfvfyai9r")))

(define-public crate-rkrga-0.1.1 (c (n "rkrga") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "rkrga-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1p8194qmk4d37i8r6y3br1dvyzg2zagiarxpnrpw5irky7c7k08i") (f (quote (("use-bindgen" "rkrga-sys/use-bindgen") ("default"))))))

(define-public crate-rkrga-0.1.2 (c (n "rkrga") (v "0.1.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "rkrga-sys") (r "^0.1.1") (d #t) (k 0)))) (h "05f0x5mxc2zh7r80x04fssipczym42bdrxblr84a532h4imvwl8b") (f (quote (("v1_3_1" "v1_3_0") ("v1_3_0" "v1_2_6") ("v1_2_6") ("use-bindgen" "rkrga-sys/use-bindgen") ("default"))))))

