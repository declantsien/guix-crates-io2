(define-module (crates-io rk ms rkmstools) #:use-module (crates-io))

(define-public crate-rkmstools-0.1.0 (c (n "rkmstools") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "drm") (r "^0.5.0") (d #t) (k 0)) (d (n "edid") (r "^0.3.0") (d #t) (k 0)))) (h "06q9hqfvjpmwgckk4ri7xkhi9wwq3qy8jwjc8kbj1lyv0wxw3kpv")))

(define-public crate-rkmstools-0.1.1 (c (n "rkmstools") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "drm") (r "^0.5.0") (d #t) (k 0)) (d (n "edid") (r "^0.3.0") (d #t) (k 0)))) (h "1dg31cnrfy93z7n1fki2s63i7pq16wfp1wzqm38ijbmsywhbq51d")))

