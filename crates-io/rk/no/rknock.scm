(define-module (crates-io rk no rknock) #:use-module (crates-io))

(define-public crate-rknock-0.1.2 (c (n "rknock") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.0") (d #t) (k 0)))) (h "1avsbcdni7xlds1fz3aj2ic2q6in20jy132fma1dgzvf0c5h4xxm")))

(define-public crate-rknock-0.1.3 (c (n "rknock") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.0") (d #t) (k 0)))) (h "1i64k8d2qk7by77jwiq9qnkvgbqw796clkb5iyczc1j1727kn0pr")))

