(define-module (crates-io rk ee rkeep) #:use-module (crates-io))

(define-public crate-rkeep-0.2.0 (c (n "rkeep") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "keepass") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "0nfb1y681y2i061jif9mypcrg50miz78x3a50lf8fw9d6jzd6sjv")))

(define-public crate-rkeep-0.4.1 (c (n "rkeep") (v "0.4.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "keepass") (r "^0.4.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "17jg70q51796jvq52h4wvwb86q5wiwsyjnl7iq1gdf2nvr6pzzvi")))

