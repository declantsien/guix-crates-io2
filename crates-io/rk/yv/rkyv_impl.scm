(define-module (crates-io rk yv rkyv_impl) #:use-module (crates-io))

(define-public crate-rkyv_impl-0.1.0 (c (n "rkyv_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "0vgm37kgk9ql35v44n2kdppqlha8v7lsk26i6xx6gfnyildiim5c")))

(define-public crate-rkyv_impl-0.2.0 (c (n "rkyv_impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "0brfs41vcq9yx66701671q8d941nagiks4sdhvvw1wdxgwc0hn20")))

(define-public crate-rkyv_impl-0.2.1 (c (n "rkyv_impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "1qs3pr2v299bg8x8vlhzf4q84a656g90b077wcllbq4kakhx6b3q")))

(define-public crate-rkyv_impl-0.2.2 (c (n "rkyv_impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "0bj26f1igrfv35rknjlrypav1w881091sv8lj8cinc2pda8zj1s8")))

