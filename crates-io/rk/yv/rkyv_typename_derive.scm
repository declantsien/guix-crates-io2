(define-module (crates-io rk yv rkyv_typename_derive) #:use-module (crates-io))

(define-public crate-rkyv_typename_derive-0.0.0 (c (n "rkyv_typename_derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h6lksbkdxhdlsx73j64hspnwcc9vp9xvc3fcsylv2x4jkc2df8j")))

(define-public crate-rkyv_typename_derive-0.1.0 (c (n "rkyv_typename_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ssmpp1phfm1z8cnwbs15fkx9qs767qv5axsmnv8b6cw9d7xs9cj")))

(define-public crate-rkyv_typename_derive-0.1.1 (c (n "rkyv_typename_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00zm90n2pbscx0n86zcqy1mjz6lzgccjr4pgalzgjcwj4xl5zzh0")))

(define-public crate-rkyv_typename_derive-0.2.0 (c (n "rkyv_typename_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jnqzjh7xmfinmiwb533x3vas04j40n5pn12mszl1gfn1znx0mjn")))

(define-public crate-rkyv_typename_derive-0.2.1 (c (n "rkyv_typename_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01zrq6kdf5a0mgrdl15hxpncjbmpx3482makc9vhlwjnqcbwh262")))

(define-public crate-rkyv_typename_derive-0.3.0 (c (n "rkyv_typename_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zdjvb3pssk6j5wig6bpm1b6j0jx0q8f39ca34qnbw0g479hbv18")))

(define-public crate-rkyv_typename_derive-0.4.0 (c (n "rkyv_typename_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "055crx2f8zvjk3as7f2bbrln0kgqwrlwk46ah2ffpp52az0gn3cp")))

(define-public crate-rkyv_typename_derive-0.5.0 (c (n "rkyv_typename_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "038bz5j4b1h94frky348vfxwicppjasqry4jgp200gcq5h52cm0b")))

(define-public crate-rkyv_typename_derive-0.6.0 (c (n "rkyv_typename_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "064v7g4rqrf840a1qwkkw9law4iw40qlk9cvlrfjnhh2v27a81hj")))

(define-public crate-rkyv_typename_derive-0.6.7 (c (n "rkyv_typename_derive") (v "0.6.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zh4bipbnrm22ibck73gy3kr4v2wpx7hr7cm5ja0gqg7vbpgzlry")))

(define-public crate-rkyv_typename_derive-0.7.0-pre.1 (c (n "rkyv_typename_derive") (v "0.7.0-pre.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cprykxfw97cnvba0h1vxv2a23fvgmq43wccg4nf6rhcwpgisdnz")))

(define-public crate-rkyv_typename_derive-0.7.0-pre.2 (c (n "rkyv_typename_derive") (v "0.7.0-pre.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0brnvdzzp4v74kkal17cwkj8awmwds744ail0yafxc3h6s0mchl9")))

(define-public crate-rkyv_typename_derive-0.7.0 (c (n "rkyv_typename_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "099avn8gl0fk4y4jr2vfq7y7vv51ybs5c1gl0v7n0w43q5zvi969")))

(define-public crate-rkyv_typename_derive-0.7.1 (c (n "rkyv_typename_derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "146xlcb8dfrzlpv7q09nwavlcfwbf0xx81z0kgw115nsgk0nfdb8")))

(define-public crate-rkyv_typename_derive-0.7.4 (c (n "rkyv_typename_derive") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03wy404nnz3r1yhrpl0dcvxalsr1qqvn9qgk7kznyqsmqz68k7xx")))

(define-public crate-rkyv_typename_derive-0.7.5 (c (n "rkyv_typename_derive") (v "0.7.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13z2g57h2in1f579iyy6aa4ckzn55yv2b4xbdhz59njjdjbjndjb")))

(define-public crate-rkyv_typename_derive-0.7.20 (c (n "rkyv_typename_derive") (v "0.7.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rrvv9w92yj5lfisaa844fpcchg91casi4fxqwfqg9haz9dvm3hm")))

(define-public crate-rkyv_typename_derive-0.7.26 (c (n "rkyv_typename_derive") (v "0.7.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jbba2j5p93b61v0j6cjxis6c6nhf9hszknfsdlig7j7qasqi2f9")))

(define-public crate-rkyv_typename_derive-0.7.33 (c (n "rkyv_typename_derive") (v "0.7.33") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sy63ywazba1d7ljkiv52kk34lwv226smv7kl642am3jifvp1997")))

(define-public crate-rkyv_typename_derive-0.7.41 (c (n "rkyv_typename_derive") (v "0.7.41") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1303l18zwcslb45jzh1gyjk468vb98yhhz1b9pavwk8dvhs483bw")))

(define-public crate-rkyv_typename_derive-0.7.42 (c (n "rkyv_typename_derive") (v "0.7.42") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y9k0hz0sarr4l2b2h27dkibg5jbphb8vl50k26c091w4i8sm0ra")))

(define-public crate-rkyv_typename_derive-0.7.43 (c (n "rkyv_typename_derive") (v "0.7.43") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04gm0zdfcj3viqglgcki2h8shvhm57r87106xj6kg8saldq23bhd")))

(define-public crate-rkyv_typename_derive-0.7.44 (c (n "rkyv_typename_derive") (v "0.7.44") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0l9m989phasns8s4fnr1p30v7vjg4xbardk879gxanpbrqzryn05")))

