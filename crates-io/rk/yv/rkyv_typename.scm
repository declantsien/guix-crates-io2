(define-module (crates-io rk yv rkyv_typename) #:use-module (crates-io))

(define-public crate-rkyv_typename-0.0.0 (c (n "rkyv_typename") (v "0.0.0") (d (list (d (n "rkyv_typename_derive") (r "=0.0.0") (d #t) (k 0)) (d (n "rkyv_typename_derive") (r "^0.0") (d #t) (k 2)))) (h "1l7bpl0yw3z8mpi1livh50cr7ivj3xniarblg7zb0j53b19944xy") (f (quote (("std") ("default" "std") ("const_generics"))))))

(define-public crate-rkyv_typename-0.1.0 (c (n "rkyv_typename") (v "0.1.0") (d (list (d (n "rkyv_typename_derive") (r "=0.1.0") (d #t) (k 0)) (d (n "rkyv_typename_derive") (r "^0.1") (d #t) (k 2)))) (h "0kgvlsxxdnydglifcdqpqyxfzj4alp4yg0sx1gcmkrpdjv8i1jfc") (f (quote (("std") ("default" "std") ("const_generics"))))))

(define-public crate-rkyv_typename-0.1.1 (c (n "rkyv_typename") (v "0.1.1") (d (list (d (n "rkyv_typename_derive") (r "=0.1.1") (d #t) (k 0)) (d (n "rkyv_typename_derive") (r "^0.1.1") (d #t) (k 2)))) (h "1bk9hp27z2qjwyls5ijq47b106pnar7qh8cslh80dsk4xl0ygrfa") (f (quote (("std") ("default" "std") ("const_generics"))))))

(define-public crate-rkyv_typename-0.2.0 (c (n "rkyv_typename") (v "0.2.0") (d (list (d (n "rkyv_typename_derive") (r "=0.2.0") (d #t) (k 0)) (d (n "rkyv_typename_derive") (r "^0.2.0") (d #t) (k 2)))) (h "05k3j0q5lhlzi40hhyk1yy7y0rjf5dygqiy6dlvmzsrfnnrhy0di") (f (quote (("std") ("default" "std") ("const_generics"))))))

(define-public crate-rkyv_typename-0.2.1 (c (n "rkyv_typename") (v "0.2.1") (d (list (d (n "rkyv_typename_derive") (r "=0.2.1") (d #t) (k 0)) (d (n "rkyv_typename_derive") (r "^0.2.1") (d #t) (k 2)))) (h "0gkvnfzz91k4wycwjssa07s0mzkivpc146awl10c3pyadfxp163n") (f (quote (("std") ("default" "std") ("const_generics"))))))

(define-public crate-rkyv_typename-0.3.0 (c (n "rkyv_typename") (v "0.3.0") (d (list (d (n "rkyv_typename_derive") (r "=0.3.0") (d #t) (k 0)) (d (n "rkyv_typename_derive") (r "^0.3.0") (d #t) (k 2)))) (h "1qyll7alyil9h77pk8yfmp8vi62932v2mq1wr4m2aj0mdyjr4avx") (f (quote (("std") ("default" "std") ("const_generics"))))))

(define-public crate-rkyv_typename-0.4.0 (c (n "rkyv_typename") (v "0.4.0") (d (list (d (n "rkyv_typename_derive") (r "=0.4.0") (d #t) (k 0)))) (h "0yxcfl52k7f16ky7qz02ivwrxdb1jj9iyy192adka9gf2gs2qqj5") (f (quote (("std") ("default" "std") ("const_generics"))))))

(define-public crate-rkyv_typename-0.5.0 (c (n "rkyv_typename") (v "0.5.0") (d (list (d (n "rkyv_typename_derive") (r "=0.5.0") (d #t) (k 0)))) (h "1mvqgxzq0j9v2advy0d21cpd3ami1f9vpgwx87jgm3nc8y423ig1") (f (quote (("std") ("default" "std" "const_generics") ("const_generics"))))))

(define-public crate-rkyv_typename-0.6.0 (c (n "rkyv_typename") (v "0.6.0") (d (list (d (n "rkyv_typename_derive") (r "=0.6.0") (d #t) (k 0)))) (h "1drjfgc6w436ffp6g636f35cs84ym1wp44xfgkh9h65vs39dpzw1") (f (quote (("std") ("default" "std" "const_generics") ("const_generics"))))))

(define-public crate-rkyv_typename-0.6.7 (c (n "rkyv_typename") (v "0.6.7") (d (list (d (n "rkyv_typename_derive") (r "=0.6.7") (d #t) (k 0)))) (h "1l5w77h6r7q2shvsd6m6ydvng4mg09bncajglafj3w95hb19xf97") (f (quote (("std") ("default" "std" "const_generics") ("const_generics"))))))

(define-public crate-rkyv_typename-0.7.0-pre.1 (c (n "rkyv_typename") (v "0.7.0-pre.1") (d (list (d (n "rkyv_typename_derive") (r "=0.7.0-pre.1") (d #t) (k 0)))) (h "1nwicqswhc1q4hni7vpiwnk3h3ll965gariiq6c11jwlmbi8ps3k") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.0-pre.2 (c (n "rkyv_typename") (v "0.7.0-pre.2") (d (list (d (n "rkyv_typename_derive") (r "=0.7.0-pre.2") (d #t) (k 0)))) (h "1753xy88mdarbi84951vvwgclnvhvjb4vx2i5sslfsis2ckm4iq3") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.0 (c (n "rkyv_typename") (v "0.7.0") (d (list (d (n "rkyv_typename_derive") (r "=0.7.0") (d #t) (k 0)))) (h "03zzkmlmjwcwzy4wmhf9zfvafr7qjq51mx598ynw5l58dybhwcgy") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.1 (c (n "rkyv_typename") (v "0.7.1") (d (list (d (n "rkyv_typename_derive") (r "=0.7.1") (d #t) (k 0)))) (h "0c21ncbkb7p8i5qanaja21dpj74ybj3k3rmd00nck8gkhyinkgr8") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.4 (c (n "rkyv_typename") (v "0.7.4") (d (list (d (n "rkyv_typename_derive") (r "=0.7.4") (d #t) (k 0)))) (h "1ywyc6qhr4x565jrs0yxv9h62j2y3m6z24n2rgc7pmz3cbnji4yw") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.5 (c (n "rkyv_typename") (v "0.7.5") (d (list (d (n "rkyv_typename_derive") (r "=0.7.5") (d #t) (k 0)))) (h "08x9gyvb29dx1zclzs1fn0bw0895wsjayprrgmjk8v80hbmmisj1") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.20 (c (n "rkyv_typename") (v "0.7.20") (d (list (d (n "rkyv_typename_derive") (r "=0.7.20") (d #t) (k 0)))) (h "1pqimyvwscrmycak80bvnghdznyyif32yidyvij24z8y0b93bvn6") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.26 (c (n "rkyv_typename") (v "0.7.26") (d (list (d (n "rkyv_typename_derive") (r "=0.7.26") (d #t) (k 0)))) (h "1r4g7h8np2zgppwbzl300z28zxyw8gnzlsj40l4nqppy0myfqyp1") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.33 (c (n "rkyv_typename") (v "0.7.33") (d (list (d (n "rkyv_typename_derive") (r "=0.7.33") (d #t) (k 0)))) (h "1lp4aqd77hpfd54iha6zs5c8bkd5grmv14jq3d1krbbva42i8dxb") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.41 (c (n "rkyv_typename") (v "0.7.41") (d (list (d (n "rkyv_typename_derive") (r "^0.7.41") (d #t) (k 0)))) (h "0vps4zw4b6mlszb8jwlwjgxd146bi6494c8bhipi06lfxama563d") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.42 (c (n "rkyv_typename") (v "0.7.42") (d (list (d (n "rkyv_typename_derive") (r "^0.7.41") (d #t) (k 0)))) (h "1kbp5nfi44v572g1h2ip8fnd6xx44m7fd3kkz88m5vycvp65l9vb") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.43 (c (n "rkyv_typename") (v "0.7.43") (d (list (d (n "rkyv_typename_derive") (r "=0.7.43") (d #t) (k 0)))) (h "12vbsxidyh7xa9gk38jf80wlmgc1z3kwm412f29l0xc9nd54g874") (f (quote (("std") ("default" "std"))))))

(define-public crate-rkyv_typename-0.7.44 (c (n "rkyv_typename") (v "0.7.44") (d (list (d (n "rkyv_typename_derive") (r "=0.7.44") (d #t) (k 0)))) (h "1f320l7l1ldynrra7f4ffy2d09pi5jsnmj092c0cc825d6bb9wy3") (f (quote (("std") ("default" "std"))))))

