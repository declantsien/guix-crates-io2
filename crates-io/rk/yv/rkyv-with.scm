(define-module (crates-io rk yv rkyv-with) #:use-module (crates-io))

(define-public crate-rkyv-with-0.1.0 (c (n "rkyv-with") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "05zhmfi1cf05c62wss06hdp0phhiqpvg48gmk1xjah32ysp1wksf")))

(define-public crate-rkyv-with-0.1.1 (c (n "rkyv-with") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ibbg6p4mkifbibxy5j44z731afvjm4ckdpnffixymd8is497rpg")))

(define-public crate-rkyv-with-0.1.2 (c (n "rkyv-with") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "007mhfpcjkk4pciwfn8xh3rp95zngsv5f836kx6rjpgj9jh8yc5k")))

