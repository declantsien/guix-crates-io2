(define-module (crates-io rk yv rkyv_derive_test) #:use-module (crates-io))

(define-public crate-rkyv_derive_test-0.7.38 (c (n "rkyv_derive_test") (v "0.7.38") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04w8yq519rvpkszir6j5fc338i2zbxg6zn9m3x2nciicnslx783h") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive_test-0.7.39 (c (n "rkyv_derive_test") (v "0.7.39") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n00bl22nziji9zlqyyjqiczf5pappqiijblvcifa2hr29bhn93h") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive_test-0.7.38-test.2 (c (n "rkyv_derive_test") (v "0.7.38-test.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kj58l594077dry0kzsnn2986y4a249cq6fyqp1kqyxicc272wz5") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

