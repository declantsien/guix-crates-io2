(define-module (crates-io rk yv rkyv-owned-archive) #:use-module (crates-io))

(define-public crate-rkyv-owned-archive-0.0.0 (c (n "rkyv-owned-archive") (v "0.0.0") (d (list (d (n "aligned-buffer") (r "^0.0.4") (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.0") (k 2)) (d (n "rkyv") (r "^0.8.0-alpha.1") (k 0)))) (h "1b8fy1xyisplg72nrl0xvxgzr4zax3rnvkmfj49jpzpvv76864xa") (f (quote (("default" "bytecheck") ("bytecheck" "rkyv/bytecheck"))))))

(define-public crate-rkyv-owned-archive-0.0.1 (c (n "rkyv-owned-archive") (v "0.0.1") (d (list (d (n "aligned-buffer") (r "^0.0.5") (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.1") (k 2)) (d (n "rkyv") (r "^0.8.0-alpha.1") (k 0)))) (h "14bxp2d1wpvwrsi658ksnk22bnzk9w16pw3pavx2a0chx7pii012") (f (quote (("default" "bytecheck") ("bytecheck" "rkyv/bytecheck"))))))

(define-public crate-rkyv-owned-archive-0.0.2 (c (n "rkyv-owned-archive") (v "0.0.2") (d (list (d (n "aligned-buffer") (r "^0.0.6") (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.2") (k 2)) (d (n "rkyv") (r "^0.8.0-alpha.1") (k 0)))) (h "1v6vy3xl93l9xzmpd2b3swn1lhr5qjn83sd7fnh72iz7c8bdwa5a") (f (quote (("default" "bytecheck") ("bytecheck" "rkyv/bytecheck"))))))

(define-public crate-rkyv-owned-archive-0.0.3 (c (n "rkyv-owned-archive") (v "0.0.3") (d (list (d (n "aligned-buffer") (r "^0.0.7") (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.3") (k 2)) (d (n "rkyv") (r "^0.8.0-alpha.2") (k 0)))) (h "0kz147pk93ln4gc26id4aql6sjcl32x7kvlmxyx8jmbssb57gsl7") (f (quote (("default" "bytecheck") ("bytecheck" "rkyv/bytecheck"))))))

(define-public crate-rkyv-owned-archive-0.0.4 (c (n "rkyv-owned-archive") (v "0.0.4") (d (list (d (n "aligned-buffer") (r "^0.0.8") (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.4") (o #t) (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.4") (k 2)) (d (n "rkyv") (r "^0.8.0-alpha.2") (k 0)))) (h "022a7pmr8cy5mg2iqzc6qbwy9ls9j3n92049bsjggch7ql4iqm17") (f (quote (("default" "bytecheck") ("bytecheck" "rkyv/bytecheck")))) (s 2) (e (quote (("pool" "dep:aligned-buffer-pool"))))))

(define-public crate-rkyv-owned-archive-0.0.5 (c (n "rkyv-owned-archive") (v "0.0.5") (d (list (d (n "aligned-buffer") (r "^0.0.8") (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.5") (o #t) (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.5") (k 2)) (d (n "rkyv") (r "^0.8.0-alpha.2") (k 0)))) (h "04hjh1wf1iqqvdfn264kzbldzkfh2xa1yia7hg4kx525gs98g8bl") (f (quote (("default" "bytecheck") ("bytecheck" "rkyv/bytecheck")))) (s 2) (e (quote (("pool" "dep:aligned-buffer-pool"))))))

(define-public crate-rkyv-owned-archive-0.0.6 (c (n "rkyv-owned-archive") (v "0.0.6") (d (list (d (n "aligned-buffer") (r "^0.0.8") (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.5") (o #t) (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.5") (k 2)) (d (n "rkyv") (r "^0.8.0-alpha.2") (k 0)))) (h "08v58f6lz8jv5hacz1jsjvcp7i8q9lqi7dq58fz4jz940z1gnr6b") (f (quote (("default" "bytecheck") ("bytecheck" "rkyv/bytecheck")))) (s 2) (e (quote (("pool" "dep:aligned-buffer-pool"))))))

(define-public crate-rkyv-owned-archive-0.0.7 (c (n "rkyv-owned-archive") (v "0.0.7") (d (list (d (n "aligned-buffer") (r "^0.0.8") (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.5") (o #t) (k 0)) (d (n "aligned-buffer-pool") (r "^0.0.5") (k 2)) (d (n "rkyv") (r "^0.8.0-alpha.2") (k 0)))) (h "0mmdshsm1lhw1ayh4mwfd17wsigjakd5il90rj7lm7hmm10226kx") (f (quote (("default" "bytecheck") ("bytecheck" "rkyv/bytecheck")))) (s 2) (e (quote (("pool" "dep:aligned-buffer-pool"))))))

