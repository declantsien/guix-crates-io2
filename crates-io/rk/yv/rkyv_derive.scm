(define-module (crates-io rk yv rkyv_derive) #:use-module (crates-io))

(define-public crate-rkyv_derive-0.0.0 (c (n "rkyv_derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k1pp5ls6k7674w1drqhr2x6l86s0clqip969n6mkx6vmxf1m059")))

(define-public crate-rkyv_derive-0.1.0 (c (n "rkyv_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10shrak6dpm3x5sdp3xyqwn1lh1qiys2zcsv1rvkwcb7nl8l4gkl")))

(define-public crate-rkyv_derive-0.1.1 (c (n "rkyv_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08mczlpwxm0n7nwhsbynzyi9iz5gvl80waf5dskd4p9m6g6rg60c")))

(define-public crate-rkyv_derive-0.2.0 (c (n "rkyv_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13ib6r2y8879wvq02as3ylf9955ni3fgmjr7sam17c9wx1ym8z81")))

(define-public crate-rkyv_derive-0.2.1 (c (n "rkyv_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s5qkp1nkdhz5wnkggb9j2s61wv290dspxgd8bw1p7901pd6dsza") (f (quote (("strict") ("default"))))))

(define-public crate-rkyv_derive-0.3.0 (c (n "rkyv_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gkdcj22259xy339hk7r023wybc6bvl02mfj31rj47ygm5qmjw3x") (f (quote (("strict") ("default"))))))

(define-public crate-rkyv_derive-0.3.1 (c (n "rkyv_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jzxg1a8bxyghn8c8bdz7yw070wp3l9byfzfxv0gqv0ynn9lxw6x") (f (quote (("strict") ("default"))))))

(define-public crate-rkyv_derive-0.4.0 (c (n "rkyv_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "051zhzx3wslfdqybklkk1cb0h9hf2psd0fgdhqz070aspkv6k8cm") (f (quote (("strict") ("default"))))))

(define-public crate-rkyv_derive-0.5.0 (c (n "rkyv_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04xn0h05fl35glwk3jba7srxjcb9lj7x0mam3pvvdjdz1wxdr1bz") (f (quote (("strict") ("default"))))))

(define-public crate-rkyv_derive-0.5.1 (c (n "rkyv_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y3lsz22ar0awj63dvnhdw3cbpbzkba779j4af2x5kk78xqsnq44") (f (quote (("strict") ("default"))))))

(define-public crate-rkyv_derive-0.6.0 (c (n "rkyv_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fppv5aljgw0ffb98ga6jkfhbz58527pnwbak3vrb0qcmlmdlj68") (f (quote (("strict") ("default"))))))

(define-public crate-rkyv_derive-0.6.3 (c (n "rkyv_derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xnx436cx172v7mhvr1jppb8sy95w8k44y8pjji7c2zfaazwqgwy") (f (quote (("strict") ("default"))))))

(define-public crate-rkyv_derive-0.6.7 (c (n "rkyv_derive") (v "0.6.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mc7rnps41gdywahsffqlypsp9jqmp0r6hlh2nxm31bddfgli3xs") (f (quote (("strict") ("default"))))))

(define-public crate-rkyv_derive-0.7.0-pre.1 (c (n "rkyv_derive") (v "0.7.0-pre.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gxz24h6ih8frxmy952ndmxiyza75zwglfknjd9bizn1y8k3ksi3") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.0-pre.2 (c (n "rkyv_derive") (v "0.7.0-pre.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ra25hprdablwxgh0dawps80r9gj8cxmv9ijla7vh89cc1nk76fc") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.0 (c (n "rkyv_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qmq1fy24603l66vczbywl0g3jgqqnb8sq5sy4ai7z2bxzg8pdx4") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.1 (c (n "rkyv_derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zwkxb0mird60fjaxbn6hghrlcp878a8xhba7ychwjknibbjisq1") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.2 (c (n "rkyv_derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v65k4wzajbq7ipsqkrpb7vdqhn2va0mpkm0697xscslflpjmvdb") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.3 (c (n "rkyv_derive") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z37i54lshcly6mvycrchk6r91h0jxjq31w39qbh285r9ai38ciw") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.4 (c (n "rkyv_derive") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dachf9lc5iz563j2mmql8phv8zbiwr46szyzp4z96c0y9fapb6v") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.5 (c (n "rkyv_derive") (v "0.7.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wy6kg3zzj62mv29qwnh74z77cd2mdlac30qq8wssz2p2g7l9ybx") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.6 (c (n "rkyv_derive") (v "0.7.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gq1dff0hp13f7nlflbhrc0sp9k4qvlkm8wgnm5mnax2lpyvhw6r") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.7 (c (n "rkyv_derive") (v "0.7.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02bb5kyay0q8lvv249sv0wxmassh4fpl7rm0jxhy2j351c19953g") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.8 (c (n "rkyv_derive") (v "0.7.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r5gqyv5ff369ww7wh10mlxfs41a5s7wmjhk8mlq4lk858v8rkvy") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.9 (c (n "rkyv_derive") (v "0.7.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pm7lh3v43vn406a6zasnn60ghrv3dbflfjwbfilwb3dlzj76gwk") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.10 (c (n "rkyv_derive") (v "0.7.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fqk5fg2nbxd14s6w2d2xg551apxvhyxnczsdnzzvbaqfaz8wpb8") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.11 (c (n "rkyv_derive") (v "0.7.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l5raxp6zbcjrp4izm1b2hs6ysc8cgljwnab6ybr48b07c25x0iz") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.12 (c (n "rkyv_derive") (v "0.7.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0a2malpzy0hpqmrwncacz1jnf4s09rwggyfsbpi9dbqw6sw1z5qa") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.13 (c (n "rkyv_derive") (v "0.7.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0giy2qypyhs1s8h7m7w28jcmbddjcpwh5mc5gdfcvzb81p91b5jx") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.14 (c (n "rkyv_derive") (v "0.7.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i8nf0db08x209sxsajbs5fnymzpjyjqfzazjrbgskpsygnsssfd") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.15 (c (n "rkyv_derive") (v "0.7.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07k6rs13crgdrd5l5mspa18dk9s1z4qqal42wfb4hjjd6drc5yqa") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.16 (c (n "rkyv_derive") (v "0.7.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z5cxiqcaysbp9dvpkas51skga0n5332a8ic2lhymslgfz32spda") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.17 (c (n "rkyv_derive") (v "0.7.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04k9jg4c80dzgrj1iykbw6jn9imb4fi4x7ahwvj70bhm634gx8in") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.18 (c (n "rkyv_derive") (v "0.7.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dchll78kx0j94djh2rmfha24lf1qwc56iic44cc5g7zsqm9dz4p") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.19 (c (n "rkyv_derive") (v "0.7.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cqmpz7z25fmxxv7pnqc7bmnp1xwp0621s3jfq4nbchn80568fy7") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.20 (c (n "rkyv_derive") (v "0.7.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1prx39vv1f8s5dn1jy5x14xf7i4jkrr62k1ghkqg458gm15qrr9a") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.21 (c (n "rkyv_derive") (v "0.7.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "163vm2lcz5dfzw7qn0d8dlcag2qnwz27anpbz3a80pdgjyxi4gar") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.22 (c (n "rkyv_derive") (v "0.7.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dljx2sbdkbcjhjvjrblapyw1gzlb913c61d6sf42c0w5pzvl8p8") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.24 (c (n "rkyv_derive") (v "0.7.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h6sppc13ql55zxrk2zyg08q4xc8q56f2wa1ymz8pb2z473zkm7r") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.25 (c (n "rkyv_derive") (v "0.7.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08r7bn1vsk6b1grf6jvm2hkmrdvjkajm3l18vyzmg0h08sypgxk1") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.26 (c (n "rkyv_derive") (v "0.7.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13l8y456il2b9mb4jw8r1jb4gw353fs0z0c042gfbf3jy58clgnz") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.27 (c (n "rkyv_derive") (v "0.7.27") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c1wnyspd3fl3mh489dp9hkj4n9m363m9gz672l015186hp49s3w") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.28 (c (n "rkyv_derive") (v "0.7.28") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h3i9zacgx96sib68gp3bp4rni24ncpp5v9ajlh9fx0shr8fcry0") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.29 (c (n "rkyv_derive") (v "0.7.29") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18bmhj9pnr1nvb43dip3lc9b3vr1gvck4nybwv726jz8s1yl97bi") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.30 (c (n "rkyv_derive") (v "0.7.30") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1402j13ss9as9bcs9648kn94v2gvgk34zfpdrhcpqma8xqk0lpvr") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.31 (c (n "rkyv_derive") (v "0.7.31") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xj81mwpwypmc26bwdjv9z7hff096yhxpgl4ra8h5a06sqx43vfd") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.32 (c (n "rkyv_derive") (v "0.7.32") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mcwlcxiww8z7sjs15nykhr9q24f4w15cnlrbkxhnhr1wxr58rfa") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.33 (c (n "rkyv_derive") (v "0.7.33") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rijrn7qffql4b07pxs39vvzpmzhrb30gxknyk8v2qkwdbn416fc") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.34 (c (n "rkyv_derive") (v "0.7.34") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mi9m6397vmdi6jjpyxrr57d5z8vgynkdznf33nmx14krv4mn1zy") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.35 (c (n "rkyv_derive") (v "0.7.35") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nmslbbzd2sgkrbvjmmizvvphsdgmg1wjncwi1z8q6zql5ymbkx6") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.36 (c (n "rkyv_derive") (v "0.7.36") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "100ll0pkbbd37a8i0671n83z25rx369lqfiga8jb7g2xjbam5iqg") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.37 (c (n "rkyv_derive") (v "0.7.37") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00y4402kdkizyxw9hymw56p14c81w6ls2sxz2i4fh9hjymnp12g2") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.38 (c (n "rkyv_derive") (v "0.7.38") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pf9v1pszaxbb72hg8b79lv3h234drlkkgqs8c3a04a1w2g20p2h") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.39 (c (n "rkyv_derive") (v "0.7.39") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i1lmir3lm8zj8k1an7j2rchv1admqhysh6r6bfkcgmmi3fdmbkf") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.40 (c (n "rkyv_derive") (v "0.7.40") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xp49id7ql4afl80ws0a4wx58cvj7h7ad1hb92ljmz2dginfs9pz") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.41 (c (n "rkyv_derive") (v "0.7.41") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rmn5n2ayrs19g5x2hkj02nkj0haj354a7wqj5i5ahgb60j6f75c") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.42 (c (n "rkyv_derive") (v "0.7.42") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07alynj16yqlyprlwqd8av157rrywvid2dm7swbhl8swbf8npq5j") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.43 (c (n "rkyv_derive") (v "0.7.43") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cshvrmg2mrnlhky5iijnj53xm2x6nq1xbfvsvj6g3lc6ahn5i5m") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.7.44 (c (n "rkyv_derive") (v "0.7.44") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rbwvbxka171bvhj60yjaxn77ipi5d1nwknnp5i6ypp2ipzxzpd7") (f (quote (("strict") ("default") ("copy") ("archive_le") ("archive_be") ("arbitrary_enum_discriminant"))))))

(define-public crate-rkyv_derive-0.8.0-alpha.1 (c (n "rkyv_derive") (v "0.8.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "02nxi4j0zczyf70r52i7zzzwkl2r5az3sixr33px2m9kb0v6b4sx") (f (quote (("stable_layout") ("default") ("copy"))))))

(define-public crate-rkyv_derive-0.8.0-alpha.2 (c (n "rkyv_derive") (v "0.8.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0swy217cyv29byn1amas1a8lx643197hc1z5d2gvqi3niks3c237") (f (quote (("default") ("copy"))))))

