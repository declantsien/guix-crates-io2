(define-module (crates-io rk yv rkyv_dyn_derive) #:use-module (crates-io))

(define-public crate-rkyv_dyn_derive-0.0.0 (c (n "rkyv_dyn_derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c30yqsyyj068l3mw19fi906nbrws3y177ycz8z5689qmk5l4kcs")))

(define-public crate-rkyv_dyn_derive-0.1.0 (c (n "rkyv_dyn_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pmmxqr83d7nh00q5kqxl8snc8vndiafdc7hgkpnqay49zy3lrxi")))

(define-public crate-rkyv_dyn_derive-0.1.1 (c (n "rkyv_dyn_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a7r9iy19vnpmrdb3513pjybca478ixpivl1y0q1gycb5vlja4kk")))

(define-public crate-rkyv_dyn_derive-0.2.0 (c (n "rkyv_dyn_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06zma8lrmsplwyfi99pyxgcgxz9i3963050cah15bs0d8yvfdrsx")))

(define-public crate-rkyv_dyn_derive-0.2.1 (c (n "rkyv_dyn_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17xhjw74vip1wl3x2kzm43d6fv24q83fgkc0ja4pdlwn48vb6jg1")))

(define-public crate-rkyv_dyn_derive-0.3.0 (c (n "rkyv_dyn_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11qd0vkq81m3cgnwp8l2cv2q212069z5chxy740zyzh42nk42rd0")))

(define-public crate-rkyv_dyn_derive-0.3.1 (c (n "rkyv_dyn_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h56hc01w2cwns7x7qi45mmz9krn8x332q8zqbs1cbfyd511mmac")))

(define-public crate-rkyv_dyn_derive-0.4.0 (c (n "rkyv_dyn_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qvkp8zgz6nil4i4ih52v2zkvh2vp3kzrc1wc7jmzf76rmlqg5cl") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.5.0 (c (n "rkyv_dyn_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fmq12w3s824fwdgdz50bxxx0aggzmkvm3j7vqgs564r30jyxgjp") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.5.1 (c (n "rkyv_dyn_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l7p7chxxgjyj3fdx23zvgs3p7sxqxwy2ng60asljxjm22mahsh5") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.6.0 (c (n "rkyv_dyn_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y4p4awxrryzfa1mabd8l2gsi9mcfarnaxz99r2z1vbhj7354mkm") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.6.7 (c (n "rkyv_dyn_derive") (v "0.6.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xaxakigpk64d3wb72liwvw4rrl59kfd8z2039c452awj8pmka56") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.0-pre.1 (c (n "rkyv_dyn_derive") (v "0.7.0-pre.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nd04j92y4hhpzmj7cwl1w706ay5imvc0chbwpq4sx13g3v295lk") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.0-pre.2 (c (n "rkyv_dyn_derive") (v "0.7.0-pre.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02v2fnyfdnd8vj514kb7pghh6psr3kjwb0nd2k48c8lwwddy0nzk") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.0 (c (n "rkyv_dyn_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dbhiri7wagzywcrdq81y8bw2dr1d9iz2b1ndahjzp2jhinbnq7f") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.1 (c (n "rkyv_dyn_derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "010jklmvsq93gkj9qaplx9r8mgqxwlqblp1npph11mr0mfs4zviq") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.4 (c (n "rkyv_dyn_derive") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17yj4n6819nasaaibln5iyx4k6vmdcs37xgmifliyzgqy3zlfp80") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.5 (c (n "rkyv_dyn_derive") (v "0.7.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1clkmkfr5vj8d23w4516jw8337igancy96x3w3nrr4vc4skix0sv") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.6 (c (n "rkyv_dyn_derive") (v "0.7.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zfdpb550wqs154vr0dd554xn9syadfm1063yqdf50klj48jnk0q") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.20 (c (n "rkyv_dyn_derive") (v "0.7.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a6x0ms3wgl5l9gpxwb3zprh5z3cfv9g9g156rig7zcjckhlgw3c") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.27 (c (n "rkyv_dyn_derive") (v "0.7.27") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d8phc11asmrn58a877q32v6x1ws99qhkq1bd6jd9k93dj1y45q1") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.33 (c (n "rkyv_dyn_derive") (v "0.7.33") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lx7vgg5v2cmcfg1qilhqn56ibqh1hcfddw6y3qwhri93qr9pv5q") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.38 (c (n "rkyv_dyn_derive") (v "0.7.38") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12xr29s8pkb53bsshir77bysvs1jqc93gg83hsv8652mhrx9l1yq") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.39 (c (n "rkyv_dyn_derive") (v "0.7.39") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ky0srna3bh23lhyc847lzvv9ha7gm57q55h988yb9032r61i67j") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.40 (c (n "rkyv_dyn_derive") (v "0.7.40") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cxdkyqzva46kzazbwcy898rvqhiw29nkjjrvzqilc76m6jlanpx") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.41 (c (n "rkyv_dyn_derive") (v "0.7.41") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fv8fdlv6401abl51qz9jaz3dq8mlapf7b9plf1xvvdhin7824qx") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.42 (c (n "rkyv_dyn_derive") (v "0.7.42") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h21r2g0kfwzlksj92aa0q76wnyziz0wj8pc8pixz3qgmp2lzsli") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.43 (c (n "rkyv_dyn_derive") (v "0.7.43") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "023f2r8d51dsv900r0g7z0b439x6mfcshqgv00sm605q63a9iaij") (f (quote (("validation") ("default"))))))

(define-public crate-rkyv_dyn_derive-0.7.44 (c (n "rkyv_dyn_derive") (v "0.7.44") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "065d9c1jrph4gdd0ylc1ih7nxmfczby1z7zqpas55ndnrz4xj8bg") (f (quote (("validation") ("default"))))))

