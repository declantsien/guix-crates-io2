(define-module (crates-io rk ai rkaiq) #:use-module (crates-io))

(define-public crate-rkaiq-0.1.0 (c (n "rkaiq") (v "0.1.0") (d (list (d (n "gst") (r "^0.17") (d #t) (k 2) (p "gstreamer")) (d (n "gst-base") (r "^0.17") (d #t) (k 2) (p "gstreamer-base")) (d (n "gst-video") (r "^0.17") (d #t) (k 2) (p "gstreamer-video")) (d (n "rkaiq-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0sclzazgq2a0l59z1147lxpc1rbkrxh8c3ffrgyzl4pfkr9kpzqy") (f (quote (("rel_2_0") ("rel_1_0") ("default" "rel_2_0"))))))

