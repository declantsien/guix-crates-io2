(define-module (crates-io ri zz rizzle) #:use-module (crates-io))

(define-public crate-rizzle-0.1.0 (c (n "rizzle") (v "0.1.0") (d (list (d (n "rizzle_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "tokio-macros" "full"))) (d #t) (k 0)))) (h "1f7liiv8hk28lrbd7bsf6zrg5m8k309b4jqi747lm2zb95a010yq")))

(define-public crate-rizzle-0.1.1 (c (n "rizzle") (v "0.1.1") (d (list (d (n "rizzle_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "tokio-macros" "full"))) (d #t) (k 0)))) (h "039nj7khqxa2ik9knkkwljj0rgamy2kryl6bpk8i4ynfd92yay5h")))

(define-public crate-rizzle-0.1.2 (c (n "rizzle") (v "0.1.2") (d (list (d (n "rizzle_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "tokio-macros" "full"))) (d #t) (k 0)))) (h "0f8dvx87dr1kzpnnglp92l154v2npbh9dlqdrg45gb8px84011dv")))

(define-public crate-rizzle-0.1.3 (c (n "rizzle") (v "0.1.3") (d (list (d (n "rizzle_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0xj3jwi8515wmdsvw795fljjx2kbgbsga0a7adphpcp4937m3xng")))

(define-public crate-rizzle-0.1.4 (c (n "rizzle") (v "0.1.4") (d (list (d (n "rizzle_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0385n5a1y7yb8bagvysmkm3c0dsylh2ywmvkvzv1lw7k3rvyf742")))

