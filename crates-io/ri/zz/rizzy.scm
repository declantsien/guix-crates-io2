(define-module (crates-io ri zz rizzy) #:use-module (crates-io))

(define-public crate-rizzy-0.1.5 (c (n "rizzy") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1jhlnmrpyamsphn6f9zx6wwpyc19wflwilfwcfrx0hbg5b67bdms")))

