(define-module (crates-io ri zz rizzle_macros) #:use-module (crates-io))

(define-public crate-rizzle_macros-0.1.0 (c (n "rizzle_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dn41sar74nmnw2fmm4v1zgkql4dqz2iq336gs10lkzw2hfpyfqp")))

(define-public crate-rizzle_macros-0.1.1 (c (n "rizzle_macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0y8hca0rlb27vnm3bvd6iv3y1gd8iw6r1k2gpxi06n4xf3azal96")))

(define-public crate-rizzle_macros-0.1.2 (c (n "rizzle_macros") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0km47lzjhaq9isvsv821qih0nz5yd0bq0mlbhvx5zal32z19457r")))

