(define-module (crates-io ri na rinara) #:use-module (crates-io))

(define-public crate-rinara-0.1.0 (c (n "rinara") (v "0.1.0") (d (list (d (n "gfx-hal") (r "^0.3.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.8.0") (d #t) (k 0)) (d (n "winit") (r "^0.18") (d #t) (k 0)))) (h "1fj0s6am3wig422c8d37cr46x7ixckm7ysw78lqf0ycvc7b0ibmz") (y #t)))

