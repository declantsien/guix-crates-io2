(define-module (crates-io ri mc rimcol) #:use-module (crates-io))

(define-public crate-rimcol-0.1.0 (c (n "rimcol") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.0") (d #t) (k 0)) (d (n "lab") (r "^0.11.0") (d #t) (k 0)) (d (n "tensorflow") (r "^0.17.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "1kk61c06a89ixn54n6z6kw3mq43s2l4rzwj9k0yyfk3v6jp82wh9")))

(define-public crate-rimcol-0.1.1 (c (n "rimcol") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.0") (d #t) (k 0)) (d (n "lab") (r "^0.11.0") (d #t) (k 0)) (d (n "tensorflow") (r "^0.17.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "0jhk8iwnh6v038vdxqzkf1wrf6srxc3rkna2b3kfdf0irjw8frxa")))

