(define-module (crates-io ri gc rigctld) #:use-module (crates-io))

(define-public crate-rigctld-0.1.0 (c (n "rigctld") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("net" "io-util" "time" "process"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0rj487brkbzd4gfpkrvqxzqmhxh97gfq3hdkkvwcyxxq6zp3fnxv")))

