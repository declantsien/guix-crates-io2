(define-module (crates-io ri gr rigrep) #:use-module (crates-io))

(define-public crate-rigrep-1.0.0 (c (n "rigrep") (v "1.0.0") (h "0sm8na87sgp1ffa1hf51zd4s56qinrxb1pk7mfqq0w3a2b0kiyqy") (y #t)))

(define-public crate-rigrep-1.0.1 (c (n "rigrep") (v "1.0.1") (h "01mi9qh5brghgx87prhs35a9gid0rkyjggc1vxh53h4ajhwwf2cr")))

