(define-module (crates-io ri cq ricq-guild) #:use-module (crates-io))

(define-public crate-ricq-guild-0.1.0 (c (n "ricq-guild") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "dynamic-protobuf") (r "^0") (d #t) (k 0)) (d (n "prost") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "ricq") (r "^0.1.17") (d #t) (k 0)) (d (n "ricq-core") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "02n091ndjg66911628cys14zqhbaxh1a0j07g949y9q9997sy20g")))

