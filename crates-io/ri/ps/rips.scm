(define-module (crates-io ri ps rips) #:use-module (crates-io))

(define-public crate-rips-0.1.0 (c (n "rips") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0f42fhjxsns1l5s068d2q19i7l1vylx5jaln0jbl6bglx1s2ygkz")))

(define-public crate-rips-0.2.0 (c (n "rips") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0h84irlmhfxxsxhhrhgphfq6aixp8y74rbdwbf4dwrp0ddjjj3s8")))

(define-public crate-rips-0.3.0 (c (n "rips") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0fnir4p8r18blgl5m9dk504b3a63asicvkaksb0k0rp0cmc1icyh") (f (quote (("docs-rs"))))))

