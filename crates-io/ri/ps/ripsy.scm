(define-module (crates-io ri ps ripsy) #:use-module (crates-io))

(define-public crate-ripsy-0.1.0 (c (n "ripsy") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.20") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (o #t) (d #t) (k 0)) (d (n "ripsy-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1rlzwgq3xzxkl57cljzcdxskk9a282hv4hbb480y392n34n1y1xd") (s 2) (e (quote (("server" "dep:axum" "ripsy-macros/server") ("client" "dep:reqwest" "ripsy-macros/client"))))))

