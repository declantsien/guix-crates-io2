(define-module (crates-io ri ps ripsy-macros) #:use-module (crates-io))

(define-public crate-ripsy-macros-0.1.0 (c (n "ripsy-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "0zrcpibw37ngnjid3kxcb2m3b1rbhffdcmr72ib6ssr29v4czf3v") (f (quote (("server") ("client"))))))

