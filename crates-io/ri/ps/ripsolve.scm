(define-module (crates-io ri ps ripsolve) #:use-module (crates-io))

(define-public crate-ripsolve-0.0.1 (c (n "ripsolve") (v "0.0.1") (d (list (d (n "ndarray") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "11f07qkbz12nxw4dbgkrj6sc6rkwxkrhsjqgrqnnrxbcd548vv93")))

(define-public crate-ripsolve-0.0.2 (c (n "ripsolve") (v "0.0.2") (d (list (d (n "ndarray") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1x1d4kvkp50nslafb91l33w7ilqjq758108i3qwzw4049yylr947")))

