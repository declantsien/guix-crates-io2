(define-module (crates-io ri pf ripfmt) #:use-module (crates-io))

(define-public crate-ripfmt-0.1.0 (c (n "ripfmt") (v "0.1.0") (d (list (d (n "globset") (r "^0.4.14") (d #t) (k 0)) (d (n "ignore") (r "^0.4.22") (d #t) (k 0)))) (h "1j1007fw7fqwcvn1qllxwl3xy97pyq5mf6as0xmrlqfcg0wjma8m")))

