(define-module (crates-io ri pf ripfind) #:use-module (crates-io))

(define-public crate-ripfind-0.1.1 (c (n "ripfind") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "17k8zs3ysdwrg3yvrxavig880wzp53pigcnqjp9fifbpp9mnzr0z")))

(define-public crate-ripfind-0.2.0 (c (n "ripfind") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "08aqh0rpfb6zvz7a3pjpbs9rw29s8pwb572kq68xyfv18yzpli3v")))

(define-public crate-ripfind-0.2.1 (c (n "ripfind") (v "0.2.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "090vkyi8p3vxhx03y0x5dic266lbadfh20akpzph1alkk7bdz7vv")))

(define-public crate-ripfind-0.2.2 (c (n "ripfind") (v "0.2.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0z88h0jxl9p26baay6dl01b5zh9p7416w1i41jkyzx3sb8rqmlf3")))

(define-public crate-ripfind-0.2.3 (c (n "ripfind") (v "0.2.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0wcxw3xnx1s7rsmr10dgiyzzhay31igvz0219j5c3yz63mgjbz36")))

(define-public crate-ripfind-0.2.4 (c (n "ripfind") (v "0.2.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0bvx2c09dq3djp5hjil7lafbnv91jdn6z9p2bzapg94ynja64als")))

(define-public crate-ripfind-0.3.0 (c (n "ripfind") (v "0.3.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "121j3r9fzj6f9g1x48mz9vqg316xii23m82ny0hbn0mfdsrya5jl")))

(define-public crate-ripfind-0.3.1 (c (n "ripfind") (v "0.3.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1jdmmzdn0w992z85k5jy02cvlcmlmdc132ci3d8z0ya1qysx4yag")))

(define-public crate-ripfind-0.3.2 (c (n "ripfind") (v "0.3.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0mbxydbq4xr31a19r099vbrczqbi71pfizir56nn7agzq7gc1x5s")))

(define-public crate-ripfind-0.3.3 (c (n "ripfind") (v "0.3.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0h3d3d9amz0n3l42ir6xpvrxy77sr06di035gw7i9lk7q57fv1al")))

(define-public crate-ripfind-0.4.0 (c (n "ripfind") (v "0.4.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0wx4l9g9sf36vq4kjj632iv7y5fw9s472x4gh8sfbgvqq0x080dl")))

(define-public crate-ripfind-0.4.1 (c (n "ripfind") (v "0.4.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0myg1lvmws98ka5jgiq3h0j6ivvn0ciilwfj2znzwgdi6hpd1idi")))

(define-public crate-ripfind-0.4.2 (c (n "ripfind") (v "0.4.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1pmlp0zgm2qxwxxb6hf6s0gh9i167n96l2fb96s1sm6sbkvmfiyd")))

