(define-module (crates-io ri co ricochetrobots) #:use-module (crates-io))

(define-public crate-ricochetrobots-0.0.0 (c (n "ricochetrobots") (v "0.0.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0fcab57mnlas55lj6zlh8clywd6i8dc7jn52f1rn6ikchprvgb5s")))

