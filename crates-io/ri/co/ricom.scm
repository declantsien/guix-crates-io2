(define-module (crates-io ri co ricom) #:use-module (crates-io))

(define-public crate-ricom-0.1.0 (c (n "ricom") (v "0.1.0") (h "0yjv3s7d318nqvh1d7x0qvnqq708fzgh8ybiiqfxv7imphj6kk1d")))

(define-public crate-ricom-0.1.1 (c (n "ricom") (v "0.1.1") (h "0v650c8dp9cdnndh39z4zm2kbm03qkm7jdkcl14996jjk9ha7b5d")))

(define-public crate-ricom-0.1.2 (c (n "ricom") (v "0.1.2") (h "05wcxaclh54j282krsd59snv2aywwrm8y3q9l1jxs4b83720z1n5")))

