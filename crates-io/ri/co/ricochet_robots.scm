(define-module (crates-io ri co ricochet_robots) #:use-module (crates-io))

(define-public crate-ricochet_robots-0.1.0 (c (n "ricochet_robots") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1a1xw0wv6vmzdxxgbcbyandb62fl63b5zvf92f3p86qk5mz4avx2")))

(define-public crate-ricochet_robots-0.2.0 (c (n "ricochet_robots") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0v7k5zrp1rzcaa1h4pfy0a1ggipbfjhvpla9x5cls1g0bx8a063l")))

