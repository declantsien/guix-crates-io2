(define-module (crates-io ri gh rightscrape) #:use-module (crates-io))

(define-public crate-rightscrape-0.1.0 (c (n "rightscrape") (v "0.1.0") (d (list (d (n "select") (r "^0.4.3") (d #t) (k 0)) (d (n "stdinix") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.3") (d #t) (k 0)))) (h "0blzxw71dnlsglji7arflx0vk2k9rnaffyyl10l91a61gszd3shd")))

(define-public crate-rightscrape-0.1.1 (c (n "rightscrape") (v "0.1.1") (d (list (d (n "oops") (r "^0.1.0") (d #t) (k 0)) (d (n "select") (r "^0.4.3") (d #t) (k 0)) (d (n "stdinix") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.3") (d #t) (k 0)))) (h "0mbv77simbinfg5zw2j5h3yncjvb7xdp4v3iq4ir11pn88mk7jxz")))

(define-public crate-rightscrape-0.2.0 (c (n "rightscrape") (v "0.2.0") (d (list (d (n "oops") (r "^0.1.0") (d #t) (k 0)) (d (n "select") (r "^0.4.3") (d #t) (k 0)) (d (n "stdinix") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.3") (d #t) (k 0)))) (h "0sj68n93hnq76zrqf65m6583mx4krm48c6234l2pq8xfywg3kbg3")))

