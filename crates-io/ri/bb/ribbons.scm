(define-module (crates-io ri bb ribbons) #:use-module (crates-io))

(define-public crate-ribbons-0.1.0 (c (n "ribbons") (v "0.1.0") (h "150xs3iccfcqh9yjfl6illaqg3lb47bkqc8l2p9arqx8k8m6q2kr")))

(define-public crate-ribbons-0.1.1 (c (n "ribbons") (v "0.1.1") (h "0n57q5qf0kxkir8b8w7x3ngazp7j53ap2865wqlawzs8z2wdlp2p")))

(define-public crate-ribbons-0.1.2 (c (n "ribbons") (v "0.1.2") (h "0j8iggnvw7vfwyz9m7mj9xvbdp49g39smnang6wkq5qaxbwf2xaz")))

