(define-module (crates-io ri bb ribboned) #:use-module (crates-io))

(define-public crate-ribboned-0.1.0 (c (n "ribboned") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "svg2appicon") (r "^0.1.2") (d #t) (k 0)))) (h "126wij9pf2qxpzpzcjq0s158mxsw6igp44sn05ckywfz8nrpwmdj")))

