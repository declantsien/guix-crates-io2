(define-module (crates-io ri bb ribboncurls) #:use-module (crates-io))

(define-public crate-ribboncurls-0.1.0 (c (n "ribboncurls") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1b87yhk9hwsdavqfad00rczmrdp47vqf2qcp7qhd5g3s3n7i79mm") (r "1.64.0")))

