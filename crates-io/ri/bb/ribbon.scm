(define-module (crates-io ri bb ribbon) #:use-module (crates-io))

(define-public crate-ribbon-0.1.0 (c (n "ribbon") (v "0.1.0") (h "0gkzp065mj8djnp5jga0xiyv63ikcflmr3cq5ywagbgy0sa4j4ha")))

(define-public crate-ribbon-0.2.0 (c (n "ribbon") (v "0.2.0") (h "0jc0b7fn3j5gcd6i07pz43p1w4aik7dn1cwxxab08b66nggd14zj")))

(define-public crate-ribbon-0.2.1 (c (n "ribbon") (v "0.2.1") (h "16ra9xrp0fnx4jmi3g04hcbrv97xj2ch7jhk02x8chc36nmr3xca")))

(define-public crate-ribbon-0.2.2 (c (n "ribbon") (v "0.2.2") (h "0nsd9gs3shzvkh4g1hnby2xpyw918q7idd4rv01d1vlj8yj51f2l")))

(define-public crate-ribbon-0.2.3 (c (n "ribbon") (v "0.2.3") (h "0vjv3lwnib98bbm1xy434q7anhvnsyfvg0s7l1fkk6i6s8w5caqm")))

(define-public crate-ribbon-0.3.0 (c (n "ribbon") (v "0.3.0") (h "0qhq2117cs2jm5s84ag7gn53zi8ssg32ysc257p6nkn17ynz4m0p")))

(define-public crate-ribbon-0.4.0 (c (n "ribbon") (v "0.4.0") (h "0qpa4jgh9wl7s3mgj6sjdcck5b5sfzng2kn82s2z7fxfhym4cwr0")))

(define-public crate-ribbon-0.5.0 (c (n "ribbon") (v "0.5.0") (h "1y0si1k3x4yc5wdjk0kdyxkn1w89glm6pway3xn6lrnahh4g14a0")))

(define-public crate-ribbon-0.6.0 (c (n "ribbon") (v "0.6.0") (h "0lf20inxwikm038zcdvbijy67mh7qnnq7jgwa4lhwcxld31ff42x")))

(define-public crate-ribbon-0.7.0 (c (n "ribbon") (v "0.7.0") (h "1ymlzmcf1wax9js2l3h6im10g9pfbkl5ji4d39ym29xfp5yp8l1w")))

