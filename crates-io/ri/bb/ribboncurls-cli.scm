(define-module (crates-io ri bb ribboncurls-cli) #:use-module (crates-io))

(define-public crate-ribboncurls-cli-0.1.0 (c (n "ribboncurls-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (d #t) (k 0)) (d (n "ribboncurls") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)))) (h "1rkyc3ga71gsyxy970jh41r9jinfpkqyw2wa9srq3k625vfrnb3v")))

