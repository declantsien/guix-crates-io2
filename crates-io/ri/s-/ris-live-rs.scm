(define-module (crates-io ri s- ris-live-rs) #:use-module (crates-io))

(define-public crate-ris-live-rs-0.0.1 (c (n "ris-live-rs") (v "0.0.1") (d (list (d (n "bgp-models") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)) (d (n "tungstenite") (r "^0.16.0") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "06qpqgqfp78vyygkcj1kkbgyjc7lx35wzp18jfjda6189azlara5")))

(define-public crate-ris-live-rs-0.1.0 (c (n "ris-live-rs") (v "0.1.0") (d (list (d (n "bgp-models") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)) (d (n "tungstenite") (r "^0.16.0") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "10rysrs9rnncnll3m267hliw7gk2cj2h11pj9q2dxfmn0q020vm0")))

(define-public crate-ris-live-rs-0.1.1 (c (n "ris-live-rs") (v "0.1.1") (d (list (d (n "bgp-models") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tungstenite") (r "^0.12.0") (d #t) (k 0)))) (h "1akc4nh7n7if8nj3wpgxy31x643rwx9nab3q9jxsl01i6593di6j")))

(define-public crate-ris-live-rs-0.2.0 (c (n "ris-live-rs") (v "0.2.0") (d (list (d (n "bgp-models") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tungstenite") (r "^0.17.2") (d #t) (k 0)))) (h "0b38595dwn54qw5hcrvlqrqkc8f1xhc5hnh8ksyyyd13igy875zh")))

