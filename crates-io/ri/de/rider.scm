(define-module (crates-io ri de rider) #:use-module (crates-io))

(define-public crate-rider-0.1.0 (c (n "rider") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "sync"))) (d #t) (k 0)))) (h "0grxl2vgd5xd1n5a3cnn7g5izyjamaazllzkhjjyya31c4shkcri")))

(define-public crate-rider-0.1.1 (c (n "rider") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 2)))) (h "1fc7nmd4z300lpylqv12fc3vyf51sq4kdq4w2slma5h9i5mn6c7r")))

(define-public crate-rider-0.1.2 (c (n "rider") (v "0.1.2") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 2)))) (h "0mw30vvglm8mqhsr1yjkhfi5cq5995xnfjb4rp78x5db9zf553pa")))

