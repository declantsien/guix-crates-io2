(define-module (crates-io ri de rider2emacs) #:use-module (crates-io))

(define-public crate-rider2emacs-0.1.0 (c (n "rider2emacs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "12z58nfdk5jcv06jbx3is4mszyfxx7r25xdh5jgxw6yfzgyr3m4a")))

(define-public crate-rider2emacs-0.1.1 (c (n "rider2emacs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1x2g6rqqzwd672409yyc69kx6nj8w3hzd29qqrxyn4hl0nl23w5w")))

