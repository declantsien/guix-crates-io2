(define-module (crates-io ri fy rify) #:use-module (crates-io))

(define-public crate-rify-0.1.0 (c (n "rify") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "0kchf6zvwfq36pl1rkhl68i1sw2kbv3faakpflgc7bkwgchn5r3a") (f (quote (("std") ("js-library-wasm" "wasm-bindgen" "serde" "serde_json") ("default" "std" "js-library-wasm"))))))

(define-public crate-rify-0.5.0 (c (n "rify") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "15y0sc54gwgqkmgxfvpg2q8vy6h3gly5qi307l63sxc376vgi6wj") (f (quote (("std") ("js-library-wasm" "wasm-bindgen" "serde" "serde_json") ("default" "std" "js-library-wasm"))))))

(define-public crate-rify-0.5.1 (c (n "rify") (v "0.5.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "0fm39lhv6j42jfjdpzncw7xqr87ylnh5dwxagknl5islnid6j9y8") (f (quote (("std") ("js-library-wasm" "wasm-bindgen" "serde" "serde_json") ("default" "std" "js-library-wasm"))))))

(define-public crate-rify-0.6.0-rc.1 (c (n "rify") (v "0.6.0-rc.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1l2612kmw6bpgq3wzd9wcp1vq2s6kibk1fgkhnbj3v0pg05fwidg") (f (quote (("std") ("default"))))))

(define-public crate-rify-0.6.0 (c (n "rify") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0zmyh709rkjbjp99if8ysy1gbwxpq4jqikjln3ijd9ghy0aa1cd2") (f (quote (("std") ("default"))))))

(define-public crate-rify-0.7.0-rc.1 (c (n "rify") (v "0.7.0-rc.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "005ds2d02zbfr0zp8zlxv95b0v4vnjw56kfgdc32x1wcvx8109li") (f (quote (("std") ("default"))))))

(define-public crate-rify-0.7.0 (c (n "rify") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0pzrclpp8qzqcz8dbq70qai99fi89m3r074ixdkv3mxvr1hpj7xs") (f (quote (("std") ("default"))))))

(define-public crate-rify-0.7.1 (c (n "rify") (v "0.7.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1fncvyy96026kfdkh470b8sxc0fvb2h81sflfsvrj2aa3gfc357a") (f (quote (("std") ("default"))))))

