(define-module (crates-io ri pp ripple-address-codec) #:use-module (crates-io))

(define-public crate-ripple-address-codec-0.1.0 (c (n "ripple-address-codec") (v "0.1.0") (d (list (d (n "base-x") (r "^0.2.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "ring") (r "^0.16.18") (d #t) (k 0)))) (h "0hp3xb677qllk8hg9zi8w0pq2p7v8bhicyr8p6myqgifir1kyh6c")))

(define-public crate-ripple-address-codec-0.1.1 (c (n "ripple-address-codec") (v "0.1.1") (d (list (d (n "base-x") (r "^0.2.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "ring") (r "^0.16.18") (d #t) (k 0)))) (h "1zb3b9pkdwr7hg591pa9gc80x2vlbr48fgw42xf3a7qfvah6iwwz")))

