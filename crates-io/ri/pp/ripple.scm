(define-module (crates-io ri pp ripple) #:use-module (crates-io))

(define-public crate-ripple-0.1.0 (c (n "ripple") (v "0.1.0") (h "11cg8mip5fa0iyrdarqyi1r3licvbh2dkd5y4cd2qmylb0qrhfrh") (y #t)))

(define-public crate-ripple-0.1.1 (c (n "ripple") (v "0.1.1") (h "1gbsj9mkcy4gkypccb5czc1bc30x69av7mlfkrgnkrdhr3adjm6b") (y #t)))

(define-public crate-ripple-0.1.2 (c (n "ripple") (v "0.1.2") (h "03g6csll928vjp8yqa75b88hraj72jnfil5dk3nm4p7c8asvdmq7") (y #t)))

(define-public crate-ripple-0.0.1 (c (n "ripple") (v "0.0.1") (h "06j9clpy7mq8f4jbm8045znhskmviyg8pi4cfi8v4s5jyny2x649")))

(define-public crate-ripple-0.2.0 (c (n "ripple") (v "0.2.0") (h "1g452g4q1wh9img0yr032sny03fb0hsqidf6bndk94hk33wx3xp5")))

(define-public crate-ripple-0.2.1 (c (n "ripple") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.28.0") (f (quote ("serde-serialize" "sparse"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simba") (r "^0.5.1") (d #t) (k 0)))) (h "1j4118qyrm6lm60730y6jrlsw6kfva208amwl007wcmby6g64v9v") (f (quote (("mkl") ("ipp"))))))

