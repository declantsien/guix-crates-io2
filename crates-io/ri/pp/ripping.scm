(define-module (crates-io ri pp ripping) #:use-module (crates-io))

(define-public crate-ripping-0.1.0 (c (n "ripping") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "oping") (r "^0.3.3") (d #t) (k 0)))) (h "11m2bl372c7qpp6q8diq0jgmmbc4qg4j9rrnprkqrly9szcdncnn")))

(define-public crate-ripping-0.1.1 (c (n "ripping") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "oping") (r "^0.3.3") (d #t) (k 0)))) (h "1z7v9y7aza4w1j5i9vnlg3xvfk92vqmw3iq9xxajwy6xjrqhg6xd")))

(define-public crate-ripping-0.1.2 (c (n "ripping") (v "0.1.2") (d (list (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "oping") (r "^0.3.3") (d #t) (k 0)))) (h "177icap0h1540z02kgidhfdhylqj4b6iflgmy0xwb9a2w7fsjyj4")))

(define-public crate-ripping-0.1.3 (c (n "ripping") (v "0.1.3") (d (list (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "oping") (r "^0.3.3") (d #t) (k 0)))) (h "1y584mnazyws9g34d2dn58f2kwfk14r32ij7cf5hi305dzcf6bkh")))

(define-public crate-ripping-0.1.4 (c (n "ripping") (v "0.1.4") (d (list (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "oping") (r "^0.3.3") (d #t) (k 0)) (d (n "textplots") (r "^0.4.0") (d #t) (k 0)))) (h "0fjfkvxx939fmijals2syzy90wghqgbcqkhqqhmvnj430sv0i441")))

(define-public crate-ripping-0.1.5 (c (n "ripping") (v "0.1.5") (d (list (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "oping") (r "^0.3.3") (d #t) (k 0)) (d (n "textplots") (r "^0.4.0") (d #t) (k 0)))) (h "0srvy376hngmjhm6xdy2kzpz8gd7bm8dzdh30ikl0nxxmrhp92ji")))

(define-public crate-ripping-0.1.6 (c (n "ripping") (v "0.1.6") (d (list (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "oping") (r "^0.3.3") (d #t) (k 0)) (d (n "textplots") (r "^0.4.1") (d #t) (k 0)))) (h "0lv0rfynj8af57cfdjlnriw8z9b8y7lfy6q49lpqgl76b1kfpk6a")))

