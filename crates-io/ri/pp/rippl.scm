(define-module (crates-io ri pp rippl) #:use-module (crates-io))

(define-public crate-rippl-0.1.0 (c (n "rippl") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imgur") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "10dhqblmmrgk9v29875x09ax9757p28f15s4ksn0gwahjdl6ipkw")))

(define-public crate-rippl-0.2.0 (c (n "rippl") (v "0.2.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imgur2018") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0svzpnnpclckpf1sy4jnwlg3lc2sb2fzv385vjxvsv0v0bxyqyic")))

