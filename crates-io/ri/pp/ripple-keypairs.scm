(define-module (crates-io ri pp ripple-keypairs) #:use-module (crates-io))

(define-public crate-ripple-keypairs-0.1.0 (c (n "ripple-keypairs") (v "0.1.0") (d (list (d (n "base-x") (r "^0.2.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.19") (d #t) (k 0)) (d (n "ripemd160") (r "^0.9.1") (d #t) (k 0)) (d (n "ripple-address-codec") (r "^0.1.1") (d #t) (k 0)))) (h "0mk84c92qmlfrp2xr12msa35mh6yy0xz2l1mxcimvnxrf5ar8vd4")))

