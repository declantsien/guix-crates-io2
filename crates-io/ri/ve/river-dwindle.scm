(define-module (crates-io ri ve river-dwindle) #:use-module (crates-io))

(define-public crate-river-dwindle-0.1.0 (c (n "river-dwindle") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "1cqy3yr4rqsz0f0wqnzvpl82aldj305zmw8dpbfhcp625f6wv06i")))

(define-public crate-river-dwindle-0.1.1 (c (n "river-dwindle") (v "0.1.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "1a65aj44bladxn81rg8337bnbw442w1bpa7pxfs33y95vcfq75mq")))

(define-public crate-river-dwindle-0.1.2 (c (n "river-dwindle") (v "0.1.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "1calp5zy7f4i80bba3x0d3xawys77vp6347ci9lwkyj04gawkqah")))

(define-public crate-river-dwindle-0.1.3 (c (n "river-dwindle") (v "0.1.3") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "0yz8xq1yl4j0f0xji6dcj0y191jqlg6fm7417w2m3wwzm68cdyg2")))

(define-public crate-river-dwindle-0.1.4 (c (n "river-dwindle") (v "0.1.4") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "1bzqn9irj8g3f0v4pzcswcys4cyndm53b7ikyb9cm29ipa32ag2d")))

(define-public crate-river-dwindle-0.1.5 (c (n "river-dwindle") (v "0.1.5") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "0zz2zqhqha624v9rwb5qkwds43l8pm5jddrk8c8kxlywqf9xqpay")))

(define-public crate-river-dwindle-1.0.0 (c (n "river-dwindle") (v "1.0.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "0sy9v0gnk024qd4a5dlzlr52kpm7282ss9q7y7crcyzv2vkdnwcm")))

