(define-module (crates-io ri ve rivers) #:use-module (crates-io))

(define-public crate-rivers-0.0.1 (c (n "rivers") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "frunk") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.0") (d #t) (k 0)))) (h "0dlzrshncm2vwxggj1nbky89qp0r4bpp95fs6s6pv4nl086j4kcg")))

