(define-module (crates-io ri ve river-bsp-layout) #:use-module (crates-io))

(define-public crate-river-bsp-layout-1.1.0 (c (n "river-bsp-layout") (v "1.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "1b1wqb5gy4n6vbzj0h96rs12mvd2fa1zc0ndn955asgsxr7rdsfg")))

(define-public crate-river-bsp-layout-1.1.1 (c (n "river-bsp-layout") (v "1.1.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "1a8g3pikc4g8nfd27rj6368651mk93scw0781ly709yrds0r046d")))

(define-public crate-river-bsp-layout-1.1.2 (c (n "river-bsp-layout") (v "1.1.2") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "1cb3lkcs36imzsk01xp6pwdpf4k83xi3niwdmi7lzwncy4sfh7vh")))

(define-public crate-river-bsp-layout-2.0.0 (c (n "river-bsp-layout") (v "2.0.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "1bjddpa2fbizx05b4fzfv8qkn4nzi25p5s6nilhs8avbchv4c49d")))

