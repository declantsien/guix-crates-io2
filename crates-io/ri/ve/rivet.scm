(define-module (crates-io ri ve rivet) #:use-module (crates-io))

(define-public crate-rivet-0.0.0 (c (n "rivet") (v "0.0.0") (h "0igswc2wmw7d2sfvv5hyq3xwp74m3bp9lqj5s13gyfz8y1kkdw1l")))

(define-public crate-rivet-0.0.1 (c (n "rivet") (v "0.0.1") (d (list (d (n "nom") (r "^3") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1") (d #t) (k 0)) (d (n "string-interner") (r "^0.6") (d #t) (k 0)))) (h "1f5480kl25k04m759sfcjryw08jwjxdik12c9c7b4d44czibgdn1")))

