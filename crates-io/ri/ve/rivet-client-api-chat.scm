(define-module (crates-io ri ve rivet-client-api-chat) #:use-module (crates-io))

(define-public crate-rivet-client-api-chat-0.0.1 (c (n "rivet-client-api-chat") (v "0.0.1") (d (list (d (n "aws-smithy-client") (r "^0.41.0") (d #t) (k 0)) (d (n "aws-smithy-http") (r "^0.41.0") (d #t) (k 0)) (d (n "aws-smithy-json") (r "^0.41.0") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.41.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0qn57lyfr95l9qbn7ljcdzl25qs49y9hr69b5hdrzdlmfpfqmy3x") (f (quote (("rustls" "aws-smithy-client/rustls") ("rt-tokio" "aws-smithy-http/rt-tokio") ("native-tls" "aws-smithy-client/native-tls") ("default" "rt-tokio" "rustls"))))))

