(define-module (crates-io ri ve rive-models) #:use-module (crates-io))

(define-public crate-rive-models-0.1.0 (c (n "rive-models") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "iso8601-timestamp") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0rzj86y55qcp3s9c63vkfy39j0zqz6s8798y8bfkg77hx98gc9yd") (r "1.64")))

(define-public crate-rive-models-0.1.1 (c (n "rive-models") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "iso8601-timestamp") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ahryva2zv95p80c1xpndsr6kirzr7hhlpivrgj64ra30rfvgwli") (r "1.64")))

(define-public crate-rive-models-1.0.0 (c (n "rive-models") (v "1.0.0") (d (list (d (n "bitflags") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "iso8601-timestamp") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0x7wwi9m0hm4mg6gdbn8vn50kzppqwd8mckxac4rbmkzmwnw22d5") (r "1.64")))

(define-public crate-rive-models-1.1.0 (c (n "rive-models") (v "1.1.0") (d (list (d (n "bitflags") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "iso8601-timestamp") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1i6cs5m3175sh466wkn5mcijd00yqgsdinz3k50v9479snk9vyar") (r "1.64")))

(define-public crate-rive-models-1.2.0 (c (n "rive-models") (v "1.2.0") (d (list (d (n "bitflags") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "iso8601-timestamp") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15bh09jdjh4948fddgpfajljvjcjhphihyaa6yc41wz0jllr44iq") (r "1.64")))

(define-public crate-rive-models-1.2.1 (c (n "rive-models") (v "1.2.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "iso8601-timestamp") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0xi971fvg0mmx59b3ys6hcikkv2q00bq72vd7l11ppwxi31g7bwq") (r "1.64")))

