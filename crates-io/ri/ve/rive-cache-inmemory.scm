(define-module (crates-io ri ve rive-cache-inmemory) #:use-module (crates-io))

(define-public crate-rive-cache-inmemory-1.0.0 (c (n "rive-cache-inmemory") (v "1.0.0") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "rive-models") (r "^1") (d #t) (k 0)))) (h "0mrs489d72cg5187sw10fbya2rghgqkvyjwdw66k571z0zkj09ji") (r "1.64")))

