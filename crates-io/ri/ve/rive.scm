(define-module (crates-io ri ve rive) #:use-module (crates-io))

(define-public crate-rive-0.1.0 (c (n "rive") (v "0.1.0") (d (list (d (n "rive-gateway") (r "^0") (d #t) (k 0)) (d (n "rive-http") (r "^0") (d #t) (k 0)) (d (n "rive-models") (r "^0") (d #t) (k 0)))) (h "1rp047f9kv4hh1axabsrx3z97jn0pvdrxjha19vvnhmsbix3yf54") (r "1.64")))

(define-public crate-rive-0.1.1 (c (n "rive") (v "0.1.1") (d (list (d (n "rive-gateway") (r "^0") (d #t) (k 0)) (d (n "rive-http") (r "^0") (d #t) (k 0)) (d (n "rive-models") (r "^0") (d #t) (k 0)))) (h "1r42n61anykcz5y763qfk66m2nrigkagwr22vd8vf5scnr608q2w") (r "1.64")))

(define-public crate-rive-0.1.2 (c (n "rive") (v "0.1.2") (d (list (d (n "rive-gateway") (r "^0") (d #t) (k 0)) (d (n "rive-http") (r "^0") (d #t) (k 0)) (d (n "rive-models") (r "^0") (d #t) (k 0)))) (h "0bwpiajss2hxavh4zqqps774a8gkg1873rr48ynh21iywqmygaaq") (r "1.64")))

(define-public crate-rive-1.0.0 (c (n "rive") (v "1.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rive-gateway") (r "^1") (d #t) (k 0)) (d (n "rive-http") (r "^1") (d #t) (k 0)) (d (n "rive-models") (r "^1") (d #t) (k 0)))) (h "15k4mykg4mx7iz50jmvmrscm6s2l5mya37psrsvnrd8rwj0qc9k3") (r "1.64")))

(define-public crate-rive-1.0.1 (c (n "rive") (v "1.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rive-gateway") (r "^1") (d #t) (k 0)) (d (n "rive-http") (r "^1") (d #t) (k 0)) (d (n "rive-models") (r "^1") (d #t) (k 0)))) (h "0ih6allxqi7jrqy2674333frzp0hlnyhjvnzdcgva7f6qzdm3599") (r "1.64")))

(define-public crate-rive-1.0.2 (c (n "rive") (v "1.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rive-gateway") (r "^1") (d #t) (k 0)) (d (n "rive-http") (r "^1") (d #t) (k 0)) (d (n "rive-models") (r "^1") (d #t) (k 0)))) (h "0r71j4wdvsy6msqvhcwrbqhpjbpxmfd7dyl5li8qms49cv57ggm1") (r "1.64")))

(define-public crate-rive-1.1.0 (c (n "rive") (v "1.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rive-autumn") (r "^1") (d #t) (k 0)) (d (n "rive-gateway") (r "^1") (d #t) (k 0)) (d (n "rive-http") (r "^1") (d #t) (k 0)) (d (n "rive-models") (r "^1") (d #t) (k 0)))) (h "19dq096fsvhkpn7as8c0ggpkalad3dr2x7dshyx9fja4855daril") (r "1.64")))

(define-public crate-rive-1.2.0 (c (n "rive") (v "1.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rive-autumn") (r "^1") (d #t) (k 0)) (d (n "rive-cache-inmemory") (r "^1") (d #t) (k 0)) (d (n "rive-gateway") (r "^1") (d #t) (k 0)) (d (n "rive-http") (r "^1") (d #t) (k 0)) (d (n "rive-models") (r "^1") (d #t) (k 0)))) (h "18vxss982id54qp2azdqb06dnnq1gi1514q3cnfc2hfi5kjadx5g") (f (quote (("rustls-tls-webpki-roots" "rive-http/rustls-tls-webpki-roots" "rive-gateway/rustls-tls-webpki-roots" "rive-autumn/rustls-tls-webpki-roots") ("rustls-tls-native-roots" "rive-http/rustls-tls-native-roots" "rive-gateway/rustls-tls-native-roots" "rive-autumn/rustls-tls-native-roots") ("native-tls" "rive-http/native-tls" "rive-gateway/native-tls" "rive-autumn/native-tls") ("default" "native-tls")))) (r "1.64")))

