(define-module (crates-io ri ve river-luatile) #:use-module (crates-io))

(define-public crate-river-luatile-0.1.1 (c (n "river-luatile") (v "0.1.1") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mlua") (r "^0.8") (f (quote ("luajit"))) (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "1hsjslvf27nvkffc4x496sn8l9cchm3js3darcz5ifrghf2l5qx0")))

(define-public crate-river-luatile-0.1.2 (c (n "river-luatile") (v "0.1.2") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mlua") (r "^0.8") (f (quote ("luajit"))) (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1") (d #t) (k 0)))) (h "163acd9j2fvkzgcgjp96vrsplcqh4xsfgsjra7kdcfgv4kdl6hax")))

(define-public crate-river-luatile-0.1.3 (c (n "river-luatile") (v "0.1.3") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mlua") (r "^0.8") (f (quote ("luajit"))) (d #t) (k 0)) (d (n "river-layout-toolkit") (r "^0.1.4") (d #t) (k 0)))) (h "1pmjl3w10wy08nvkfd91lxcd07jw6618n77w8pqy9pivsp93hbfn")))

