(define-module (crates-io ri ve river-layout-toolkit) #:use-module (crates-io))

(define-public crate-river-layout-toolkit-0.1.0 (c (n "river-layout-toolkit") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.4") (d #t) (k 0)))) (h "029fh6z2f6vxywg4g7siagzbsdsk14szr444jl90c0sb5sskn7rs")))

(define-public crate-river-layout-toolkit-0.1.1 (c (n "river-layout-toolkit") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.7") (d #t) (k 0)))) (h "11i6bi2hcbx1d0638dz61fvnls8897v2k30c3br07sgwb3z8yip6")))

(define-public crate-river-layout-toolkit-0.1.2 (c (n "river-layout-toolkit") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.9") (d #t) (k 0)))) (h "0h0a9c8fkg5shjsgx9gpv1p4i1hlicwq3jklgm8dlxjsr2yidpkb")))

(define-public crate-river-layout-toolkit-0.1.3 (c (n "river-layout-toolkit") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.10") (d #t) (k 0)))) (h "0ql987xg670ksb6kw4yj0ihk3fs43ak6hnqhm6khqrmq5shllph0")))

(define-public crate-river-layout-toolkit-0.1.4 (c (n "river-layout-toolkit") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.10.4") (d #t) (k 0)))) (h "1lz8vjwha9g1qqhwlbdc9pmnjhbvfbvkysh9q1vcfai1l0nk5gd7")))

(define-public crate-river-layout-toolkit-0.1.5 (c (n "river-layout-toolkit") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.12") (d #t) (k 0)))) (h "017kifd9wrgrpr4vpn0v68siwkrmnsv4z36aggiqj8cdy28imh3s")))

(define-public crate-river-layout-toolkit-0.1.6 (c (n "river-layout-toolkit") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wayrs-client") (r "^1.0") (d #t) (k 0)))) (h "0550bvvargxglpb39xc08xdg4s879dqbipv1pxcyydzbkb0vjx8n")))

