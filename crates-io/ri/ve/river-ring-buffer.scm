(define-module (crates-io ri ve river-ring-buffer) #:use-module (crates-io))

(define-public crate-river-ring-buffer-0.1.0 (c (n "river-ring-buffer") (v "0.1.0") (h "15wr89d1hypdpcyyf2vlx4l6dll8lp3acmv0jjbpcgq0s45vdi69")))

(define-public crate-river-ring-buffer-0.2.0 (c (n "river-ring-buffer") (v "0.2.0") (h "1iyrn1svw8crpy7nwv1vwbfz9sy1gkk7ls4f9jfh723gji1ysdr6")))

(define-public crate-river-ring-buffer-0.3.0 (c (n "river-ring-buffer") (v "0.3.0") (h "1qykgi82d8kl49hf1ld6b9pwnbcbn06xmlmv09dv4p3iishm7gsn")))

(define-public crate-river-ring-buffer-0.4.0 (c (n "river-ring-buffer") (v "0.4.0") (h "06d21kl1nh18i4hjcdafzgdc79k4vzllhwnh8pyy1q4n951p6nkc")))

(define-public crate-river-ring-buffer-1.0.0 (c (n "river-ring-buffer") (v "1.0.0") (h "0n0blzc8w41y9xqp7gs1nq3v5lmb6qw5r7ysy9alj52j6q885y1v")))

