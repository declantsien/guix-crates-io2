(define-module (crates-io ri nk rink-sandbox) #:use-module (crates-io))

(define-public crate-rink-sandbox-0.6.1 (c (n "rink-sandbox") (v "0.6.1") (d (list (d (n "async-ctrlc") (r "^1.2.0") (f (quote ("stream"))) (d #t) (k 0)) (d (n "async-std") (r "^1.9") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cw2646qz2mlxaiasmyf6ryslyd6pi3i794mp16m9zy7ff7v8837")))

