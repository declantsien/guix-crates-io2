(define-module (crates-io ri gi rigit) #:use-module (crates-io))

(define-public crate-rigit-0.1.3 (c (n "rigit") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1v61wnq04n9w3wxsc182arljdxgi4p7mhma35a0q1camnlnx40l4") (y #t)))

(define-public crate-rigit-0.1.4 (c (n "rigit") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "13v2dlnk2lbghh4pzj2i4r04zdrflqsrxw4d31825hjhccj33201") (y #t)))

(define-public crate-rigit-0.1.5 (c (n "rigit") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0d8rrn3pzc24xgx6b77nqhj62mbhbhlhkzr3aha5f1w0a6hdi7w8")))

(define-public crate-rigit-0.1.6 (c (n "rigit") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "04j89b557man5phah3l2v7csqj79sp0x8m04qx348d4zbcbfi5sa")))

(define-public crate-rigit-0.1.7 (c (n "rigit") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1x3h9l770qmmb1zzxhcnz66pc735722nsm1l4pakmb334fy07c0x")))

(define-public crate-rigit-0.2.0 (c (n "rigit") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0a67dszxb1y3n49lrn0x2b24bsqzprx9q5krdc7y45g4wl9s1pjs")))

