(define-module (crates-io ri t- rit-mazy) #:use-module (crates-io))

(define-public crate-rit-mazy-0.1.0 (c (n "rit-mazy") (v "0.1.0") (h "0zmy6sipyx9cr633171xca2pz7ghr7zhc2097dkac33cb428a5zn")))

(define-public crate-rit-mazy-0.1.1 (c (n "rit-mazy") (v "0.1.1") (h "0qh6sj8190kmm2lxrz9x15niyh3ying4kiivd8f2hrfisrc9xzdh")))

(define-public crate-rit-mazy-0.1.2 (c (n "rit-mazy") (v "0.1.2") (h "0jyr1nc6s727fbv3r6xwd2dmnybs9b630wlvnzdfr2q36l3rv53r")))

(define-public crate-rit-mazy-0.1.3 (c (n "rit-mazy") (v "0.1.3") (h "097i9qii1s6rn76d4a67wy1165pc48b0rc6iqb98wqgqm5x3crkz")))

(define-public crate-rit-mazy-0.1.4 (c (n "rit-mazy") (v "0.1.4") (h "0ki59arf0b859b02kjxpx5r31jgggaj0j68833zndhsqdk284xal")))

(define-public crate-rit-mazy-0.1.5 (c (n "rit-mazy") (v "0.1.5") (h "1a5k46asmb0pvpzrmgvya6c8sri2yvrkx4z78lpag5734q9gyx7m")))

(define-public crate-rit-mazy-0.1.6 (c (n "rit-mazy") (v "0.1.6") (h "0lr6lhwihhkjd0r2p3j54ma3ljckfj62l20gi27nxj786higgzzp")))

(define-public crate-rit-mazy-0.2.0 (c (n "rit-mazy") (v "0.2.0") (h "0yl9v53w6h76gjyr3m9whsaiixcmbz2ycp5wl2721pq6irap9918")))

