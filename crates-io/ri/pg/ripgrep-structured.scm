(define-module (crates-io ri pg ripgrep-structured) #:use-module (crates-io))

(define-public crate-ripgrep-structured-0.1.1 (c (n "ripgrep-structured") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "grep") (r "^0.2.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("preserve_order" "raw_value"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1sljzbxdhrvkd85fw6k03jc1mh9jgnivbg1jcmfzfv7vx92qw683")))

