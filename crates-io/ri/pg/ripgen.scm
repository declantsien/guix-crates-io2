(define-module (crates-io ri pg ripgen) #:use-module (crates-io))

(define-public crate-ripgen-0.1.0 (c (n "ripgen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ripgen_lib") (r "^0.1.0") (f (quote ("dnsgen"))) (d #t) (k 0)))) (h "14mfcggc4409c9mwh14l15pihv5jzrnd32aakygzl88fyqwxnx0a")))

(define-public crate-ripgen-0.1.1 (c (n "ripgen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ripgen_lib") (r "^0.1.0") (f (quote ("dnsgen"))) (d #t) (k 0)))) (h "0c91kj9bycqj1kcb515xngch86n8cvlgcqw8i2626mnywn5bhs8f")))

(define-public crate-ripgen-0.1.2 (c (n "ripgen") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ripgen_lib") (r "^0.1.0") (f (quote ("dnsgen"))) (d #t) (k 0)))) (h "0cbjg7hqwsnri0r1xq7256iw19ydjrlgi4l186k969wgpza26mcs")))

(define-public crate-ripgen-0.1.3 (c (n "ripgen") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ripgen_lib") (r "^0.1") (f (quote ("dnsgen"))) (d #t) (k 0)))) (h "1l8cjk9jcm5d1vfrf2wb6fbxsbbd9w3dbbkgsj83g496cyjcpgw8")))

(define-public crate-ripgen-0.1.5 (c (n "ripgen") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ripgen_lib") (r "^0.1") (f (quote ("dnsgen"))) (d #t) (k 0)))) (h "1lnya31f2dnzcfyk3p3dzdp1h3wmfzbq5g2fb5vwzff27ixlyazn")))

