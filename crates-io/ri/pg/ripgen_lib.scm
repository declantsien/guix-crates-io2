(define-module (crates-io ri pg ripgen_lib) #:use-module (crates-io))

(define-public crate-ripgen_lib-0.1.0 (c (n "ripgen_lib") (v "0.1.0") (d (list (d (n "addr") (r "^0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10zm24kwhmzrhzjrkqiy6vzzmag17pdl4l80535qfi2x8r3q1025") (f (quote (("dnsgen" "regex" "lazy_static") ("default"))))))

(define-public crate-ripgen_lib-0.1.1 (c (n "ripgen_lib") (v "0.1.1") (d (list (d (n "addr") (r "^0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19x2h2w13v7797gagvqr58hi820ydf4kiw94b86yz8qvqy79bj6y") (f (quote (("dnsgen" "regex" "lazy_static") ("default"))))))

(define-public crate-ripgen_lib-0.1.2 (c (n "ripgen_lib") (v "0.1.2") (d (list (d (n "addr") (r "^0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ld98gw4kijqzy8clg4rw7pjsjvhpgjvda489g3fzbc6rs63gzvb") (f (quote (("dnsgen" "regex" "lazy_static") ("default"))))))

(define-public crate-ripgen_lib-0.1.3 (c (n "ripgen_lib") (v "0.1.3") (d (list (d (n "addr") (r "^0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15zl8wyfwp7rssv9z7h9j90zs9m0riwc7cyag7136qg1db4js86q") (f (quote (("dnsgen" "regex" "lazy_static") ("default"))))))

(define-public crate-ripgen_lib-0.1.4 (c (n "ripgen_lib") (v "0.1.4") (d (list (d (n "addr") (r "^0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0h9kkma5vjvjhxprk29sjkbym2rs196qn5hdmjgryv60b1qw0lz6") (f (quote (("dnsgen" "regex" "lazy_static") ("default"))))))

