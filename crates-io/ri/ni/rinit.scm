(define-module (crates-io ri ni rinit) #:use-module (crates-io))

(define-public crate-rinit-0.1.0 (c (n "rinit") (v "0.1.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)))) (h "0610cfwa1kgmvymyrffcrsgdnx5af603kbfxivwyrkjxdfxblz7f")))

(define-public crate-rinit-0.1.1 (c (n "rinit") (v "0.1.1") (d (list (d (n "libc") (r "^0") (d #t) (k 0)))) (h "04lavaa7hf22zafz8m0hmgms5hblmhvk3fn6zmssxfp77553mgm7")))

