(define-module (crates-io ri #{2p}# ri2p) #:use-module (crates-io))

(define-public crate-ri2p-1.0.0 (c (n "ri2p") (v "1.0.0") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "nom-unicode") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "10r70ql40njyza1527s57yzaswr1qvx75yng0d2ddmg44ydmk4zy")))

