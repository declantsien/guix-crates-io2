(define-module (crates-io ri so rison) #:use-module (crates-io))

(define-public crate-rison-0.1.0 (c (n "rison") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "10ckrwxrvfs22vvfh2j7cgk92wkfkzrpvn0cb4gdrfgamjpnk65y")))

