(define-module (crates-io ri ge rigel) #:use-module (crates-io))

(define-public crate-rigel-0.0.1 (c (n "rigel") (v "0.0.1") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.6.2") (d #t) (k 2)) (d (n "orion") (r "^0.4.2") (d #t) (k 2)) (d (n "ring") (r "^0.13.2") (d #t) (k 2)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "subtle") (r "^0.7.0") (d #t) (k 0)))) (h "094pbnhj037i3xihmdwfdlyy9hhwxbhx5sscyhzgcr7d1k7knx6c") (y #t)))

(define-public crate-rigel-0.0.2 (c (n "rigel") (v "0.0.2") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.6.2") (d #t) (k 2)) (d (n "orion") (r "^0.4.2") (d #t) (k 2)) (d (n "ring") (r "^0.13.2") (d #t) (k 2)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "subtle") (r "^0.7.0") (d #t) (k 0)))) (h "0mrl95vvr5vjz45z7f14bsc3q56gzanwgggwyb45bsc3fvchh8rc") (y #t)))

(define-public crate-rigel-0.0.3 (c (n "rigel") (v "0.0.3") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.6.2") (d #t) (k 2)) (d (n "orion") (r "^0.4.3") (d #t) (k 2)) (d (n "ring") (r "^0.13.2") (d #t) (k 2)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "subtle") (r "^0.7.0") (d #t) (k 0)))) (h "1bxz03hy6b8s4c9l8a8jb6q7i7lw320996w5086qcbd2c2ks2c64") (y #t)))

(define-public crate-rigel-0.0.4 (c (n "rigel") (v "0.0.4") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.6.2") (d #t) (k 2)) (d (n "orion") (r "^0.4.3") (d #t) (k 2)) (d (n "ring") (r "^0.13.2") (d #t) (k 2)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "subtle") (r "^0.7.0") (d #t) (k 0)))) (h "0mkabqn3y09q13h3ir6n9270k8s0rh5mlld2qsjvh5mv3mgn7lq1") (y #t)))

(define-public crate-rigel-0.0.5 (c (n "rigel") (v "0.0.5") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.6.2") (d #t) (k 2)) (d (n "orion") (r "^0.4.3") (d #t) (k 2)) (d (n "ring") (r "^0.13.2") (d #t) (k 2)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "subtle") (r "^0.7.0") (d #t) (k 0)))) (h "1jld5wy14q5i5sa2s2w58qhjlibsy8bg99hd0w5rfanpigg26ya2") (y #t)))

(define-public crate-rigel-0.0.6 (c (n "rigel") (v "0.0.6") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.6.2") (d #t) (k 2)) (d (n "orion") (r "^0.4.3") (d #t) (k 2)) (d (n "ring") (r "^0.13.2") (d #t) (k 2)) (d (n "seckey") (r "^0.9.1") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "subtle") (r "^0.7.0") (d #t) (k 0)))) (h "0q9i50rjhbarasnixby7z1awrnkcs4ngzj53rlwcpiz82mzpymdr") (y #t)))

(define-public crate-rigel-0.0.7 (c (n "rigel") (v "0.0.7") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.6.2") (d #t) (k 2)) (d (n "orion") (r "^0.4.3") (d #t) (k 2)) (d (n "ring") (r "^0.13.2") (d #t) (k 2)) (d (n "seckey") (r "^0.9.1") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "subtle") (r "^0.9.0") (d #t) (k 0)))) (h "0lcr088br5l0daahk7axwy94r4z50fjzmqcbc9jyvr8jlb92fi0l") (y #t)))

(define-public crate-rigel-0.0.8 (c (n "rigel") (v "0.0.8") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.6.2") (d #t) (k 2)) (d (n "orion") (r "^0.4.3") (d #t) (k 2)) (d (n "ring") (r "^0.13.2") (d #t) (k 2)) (d (n "seckey") (r "^0.9.1") (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "subtle") (r "^0.9.0") (k 0)))) (h "0bs431a7p294zs1kapkp3wrjrxffff0cd3fx4sib3fpwcs5bpvl4") (y #t)))

(define-public crate-rigel-0.0.9 (c (n "rigel") (v "0.0.9") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.6.3") (d #t) (k 2)) (d (n "orion") (r "^0.7.0") (d #t) (k 2)) (d (n "ring") (r "^0.13.2") (d #t) (k 2)) (d (n "seckey") (r "^0.9.1") (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "subtle") (r "^0.9.0") (k 0)))) (h "0l3g3vsmw1j8vi6h086wb2zj4wjqyz0kw4ck5cgn379p56ginibc") (y #t)))

(define-public crate-rigel-0.1.0 (c (n "rigel") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.6.3") (d #t) (k 2)) (d (n "orion") (r "^0.7.3") (d #t) (k 2)) (d (n "ring") (r "^0.13.2") (d #t) (k 2)) (d (n "seckey") (r "^0.9.1") (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "subtle") (r "^1.0.0") (k 0)))) (h "0k1srs34a7hiizykx3fxvvv5ikpnipwgqkqyi5wn22xc81g37z5h") (y #t)))

(define-public crate-rigel-0.1.1 (c (n "rigel") (v "0.1.1") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.6.3") (d #t) (k 2)) (d (n "orion") (r "^0.7.3") (d #t) (k 2)) (d (n "ring") (r "^0.13.2") (d #t) (k 2)) (d (n "seckey") (r "^0.9.1") (k 0)) (d (n "sha2") (r "^0.8.0") (k 0)) (d (n "subtle") (r "^1.0.0") (k 0)))) (h "1x926d44nym9b40sa1xmdcnlqpkl1czsm6f4r5yy3ykkdljszamf") (y #t)))

(define-public crate-rigel-0.2.0 (c (n "rigel") (v "0.2.0") (d (list (d (n "clear_on_drop") (r "^0.2.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.7.0") (d #t) (k 2)) (d (n "orion") (r "^0.11.0") (d #t) (k 2)) (d (n "ring") (r "^0.13.5") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (k 0)) (d (n "subtle") (r "^1.0.0") (f (quote ("nightly"))) (k 0)))) (h "0c3l38gzsw2bcl3kkccyr0a6znph4rcnhh7ban5ja0hmwcrnsmvn") (y #t)))

(define-public crate-rigel-0.2.1 (c (n "rigel") (v "0.2.1") (d (list (d (n "clear_on_drop") (r "^0.2.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.7.0") (d #t) (k 2)) (d (n "orion") (r "^0.11.0") (d #t) (k 2)) (d (n "ring") (r "^0.13.5") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (k 0)) (d (n "subtle") (r "^2") (f (quote ("nightly"))) (k 0)))) (h "0i0knsl06w5i2z2xin4sqyfhgg63zixscxkb0rrf13rhdwv6niqb") (y #t)))

(define-public crate-rigel-0.2.2 (c (n "rigel") (v "0.2.2") (d (list (d (n "clear_on_drop") (r "^0.2.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac") (r "^0.7.0") (d #t) (k 2)) (d (n "orion") (r "^0.11.0") (d #t) (k 2)) (d (n "ring") (r "^0.13.5") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (k 0)) (d (n "subtle") (r "^2") (f (quote ("nightly"))) (k 0)))) (h "117gpm5dyms3xsjv4bq7qislar70znq4r10vy2f0x6x76j51bj9i") (y #t)))

