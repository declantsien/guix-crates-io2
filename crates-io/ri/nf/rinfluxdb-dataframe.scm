(define-module (crates-io ri nf rinfluxdb-dataframe) #:use-module (crates-io))

(define-public crate-rinfluxdb-dataframe-0.2.0 (c (n "rinfluxdb-dataframe") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rinfluxdb-types") (r "=0.2.0") (d #t) (k 0)))) (h "186i5hnydv14abh0lfpsfylnfq18psnxa6sfkzjm92i0vn7kk2nm")))

