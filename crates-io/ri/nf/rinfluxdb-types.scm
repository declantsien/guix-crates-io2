(define-module (crates-io ri nf rinfluxdb-types) #:use-module (crates-io))

(define-public crate-rinfluxdb-types-0.2.0 (c (n "rinfluxdb-types") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1cihzyqpx90mpmj7y8853mwz42ylvibvg08cyylrm1a3jqrvizzh")))

