(define-module (crates-io ri nf rinfluxdb-polars) #:use-module (crates-io))

(define-public crate-rinfluxdb-polars-0.2.0 (c (n "rinfluxdb-polars") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "polars") (r "^0.16") (f (quote ("dtype-u64"))) (d #t) (k 0)) (d (n "rinfluxdb-types") (r "=0.2.0") (d #t) (k 0)))) (h "0nmr6bdrhp2nskgcx94f7v4z7cbpii4jxgjxkcskljzrr7f8xv32")))

