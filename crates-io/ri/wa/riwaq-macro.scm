(define-module (crates-io ri wa riwaq-macro) #:use-module (crates-io))

(define-public crate-riwaq-macro-0.1.0 (c (n "riwaq-macro") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "field_types") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "riwaq-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0114la4p9cli4b20rqi9hqli1vddhwfxv44gn2f72rm0hym3dyr0")))

