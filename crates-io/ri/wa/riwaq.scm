(define-module (crates-io ri wa riwaq) #:use-module (crates-io))

(define-public crate-riwaq-0.1.0 (c (n "riwaq") (v "0.1.0") (d (list (d (n "riwaq-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "0mzbnxwdqim7a3x6ipdh9wbnbmgdkjgya4jc90xwa8nn0fiis0dd")))

