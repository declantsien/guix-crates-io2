(define-module (crates-io ri te ritenn) #:use-module (crates-io))

(define-public crate-ritenn-0.1.0 (c (n "ritenn") (v "0.1.0") (d (list (d (n "flexbuffers") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1yja1cgq85j5x2vcz5j5bb29afh4k60hldisfpf9nkfwaglrjyiv") (f (quote (("serde_flexbuffers" "serde" "serde_derive" "flexbuffers"))))))

