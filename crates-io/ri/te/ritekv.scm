(define-module (crates-io ri te ritekv) #:use-module (crates-io))

(define-public crate-ritekv-0.0.0 (c (n "ritekv") (v "0.0.0") (h "0a803js1x0jryhn2rxmk5xs2im07zcnfyx565k2ky06v3kq1yjjq")))

(define-public crate-ritekv-0.1.0 (c (n "ritekv") (v "0.1.0") (d (list (d (n "griddle") (r "^0.4.1") (f (quote ("inline-more"))) (o #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "seahash") (r "^4.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0kww1zly6i85fwangd281skynclgl51kirym8n7n3624xk945ppm") (f (quote (("default" "amortized") ("amortized" "griddle"))))))

