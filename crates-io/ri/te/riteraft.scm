(define-module (crates-io ri te riteraft) #:use-module (crates-io))

(define-public crate-riteraft-0.1.0 (c (n "riteraft") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "raft") (r "^0.7") (f (quote ("prost-codec"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)))) (h "02j4hxpazg8pa8cdi46xsrwh7ccwb7rwc6y5ia2m5fncd2n1fk3s")))

