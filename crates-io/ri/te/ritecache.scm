(define-module (crates-io ri te ritecache) #:use-module (crates-io))

(define-public crate-ritecache-0.1.0 (c (n "ritecache") (v "0.1.0") (d (list (d (n "filetime") (r "^0.2.15") (d #t) (k 0)) (d (n "heapsize_") (r "^0.4.2") (o #t) (d #t) (k 0) (p "heapsize")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ritelinked") (r "^0.3.2") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0kvqhr4vm3hwcpdmjkjvjh0xm6s671rx263rzjnfd2yhmdjyvskp") (f (quote (("heapsize" "heapsize_") ("amortized" "ritelinked/ahash-amortized" "ritelinked/inline-more-amortized"))))))

