(define-module (crates-io ri te ritehash) #:use-module (crates-io))

(define-public crate-ritehash-0.1.0 (c (n "ritehash") (v "0.1.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "seahash") (r "^4") (d #t) (k 2)))) (h "1p4l1kd847mixvqbilfvkijv1v0lylwnz0m48jrwwhqq1gzw4jc5")))

(define-public crate-ritehash-0.2.0 (c (n "ritehash") (v "0.2.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "seahash") (r "^4") (d #t) (k 2)))) (h "035ybqn1wrfj7p33h5c8lgpgrhjifhym15vqxdk95l6dxc8mzsw0")))

