(define-module (crates-io ri ke riker-deadletter) #:use-module (crates-io))

(define-public crate-riker-deadletter-0.1.0 (c (n "riker-deadletter") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.0") (d #t) (k 0)))) (h "18x7mxr70rp53av252g4pcsg7zwi8513zkcbayldq4mg20q0nix3")))

(define-public crate-riker-deadletter-0.1.1 (c (n "riker-deadletter") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.1") (d #t) (k 0)))) (h "0q24ps41xfw0d4bbmyj5i42fbjjl7l5fx4lisw778i6apj4shhsr")))

(define-public crate-riker-deadletter-0.1.4 (c (n "riker-deadletter") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.4") (d #t) (k 0)))) (h "0n8p8j0dm5lwgid05g0ncpjvnjsjd6i8330l4p7pbckzxdvgdm0s")))

(define-public crate-riker-deadletter-0.1.5 (c (n "riker-deadletter") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.5") (d #t) (k 0)))) (h "0qc551x41n12k3jhl7ni1pfa6jd569lx7121srliiillxnbhi45v")))

(define-public crate-riker-deadletter-0.1.6 (c (n "riker-deadletter") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.6") (d #t) (k 0)))) (h "1fcc8ypl2qv5zrji6ga34ji7k0ags3bdg7g28999vw5039qnhydb")))

(define-public crate-riker-deadletter-0.1.7 (c (n "riker-deadletter") (v "0.1.7") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.7") (d #t) (k 0)))) (h "11l48hwr54mxdcgq6hv2l3casj22xbn0djwd2sj9criihj91wqz4")))

(define-public crate-riker-deadletter-0.1.8 (c (n "riker-deadletter") (v "0.1.8") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.8") (d #t) (k 0)))) (h "0lg31sf1a6q910gj4sa2l5hs3wh6nmc6mmhrnfyn31vjsj01k9j5")))

(define-public crate-riker-deadletter-0.1.9 (c (n "riker-deadletter") (v "0.1.9") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.9") (d #t) (k 0)))) (h "0whvhhpg5y8gjw9di6y51wgk7dlq7krlf0lf6ck2b2wsrzl13f9l")))

(define-public crate-riker-deadletter-0.2.0 (c (n "riker-deadletter") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.2.0") (d #t) (k 0)))) (h "147xnc9qhj27mqsvwvavvyyk7addzpqlnz5jpsy0s5r5kyxy8vj9")))

(define-public crate-riker-deadletter-0.2.1 (c (n "riker-deadletter") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.2.1") (d #t) (k 0)))) (h "0qmb81g73f4kjfgpm6pn6a4jy37vnnwx156gvanaqivv2wc2913c")))

(define-public crate-riker-deadletter-0.2.2 (c (n "riker-deadletter") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.2.2") (d #t) (k 0)))) (h "0z3s30527cqfzmvsqsqfmx5c2lchfs1gmkr7hr7vdnmshv1czy7r")))

(define-public crate-riker-deadletter-0.2.3 (c (n "riker-deadletter") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.2.3") (d #t) (k 0)))) (h "0cvqx5b3i76h3nm6vw75270nfxlg42f2k3vf3zsx38nlrymc4rav")))

(define-public crate-riker-deadletter-0.2.4 (c (n "riker-deadletter") (v "0.2.4") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.2.4") (d #t) (k 0)))) (h "1x291vmc5kbadkkkccsyg7ml6jzdc471qi3kz23aqkm8r3dzkasb")))

