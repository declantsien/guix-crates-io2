(define-module (crates-io ri ke riker-testkit) #:use-module (crates-io))

(define-public crate-riker-testkit-0.0.0 (c (n "riker-testkit") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0786awp0qaj369skgzwrh0glkjf1ib3s228fqb8mb4fbgdxylq00")))

(define-public crate-riker-testkit-0.1.0 (c (n "riker-testkit") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "13apimdxbjz0bf7hnsxi3hd16ispbz2xj3n2imlbxqa18d12lsd1")))

