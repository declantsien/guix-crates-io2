(define-module (crates-io ri ke riker-cqrs) #:use-module (crates-io))

(define-public crate-riker-cqrs-0.1.0 (c (n "riker-cqrs") (v "0.1.0") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.0") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.0") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "19s9w90sjczy9x1pb6mc8kkvzds7gi53410r5d429zy72rmfdv82")))

(define-public crate-riker-cqrs-0.1.5 (c (n "riker-cqrs") (v "0.1.5") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.5") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.5") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "0qmnsfqdhigxli66dygx6f5mncnghp8awnw5iv5x19qr0rbn63kv")))

(define-public crate-riker-cqrs-0.1.7 (c (n "riker-cqrs") (v "0.1.7") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.7") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.7") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1brhbipa1qbcgsagydkmj2lq02z2xid94splb9iclz72wzkizhsk")))

(define-public crate-riker-cqrs-0.1.8 (c (n "riker-cqrs") (v "0.1.8") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.8") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.8") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "0ycrxdr0h9lis2jml77vqa2ifm2vpw8lwmqvpqk8hdry9k6fqhi6")))

(define-public crate-riker-cqrs-0.1.9 (c (n "riker-cqrs") (v "0.1.9") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.1.9") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.9") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "0qkaiqnkf40zxv9z17cjg5ayviqbk0azs5vn3dlsx8w322hs51vq")))

(define-public crate-riker-cqrs-0.2.0 (c (n "riker-cqrs") (v "0.2.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.2.0") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.0") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1h32nv051zgwlqwmkbzhmamj61iwzvaw58mbrwmq0m5rf9pzmvll")))

(define-public crate-riker-cqrs-0.2.2 (c (n "riker-cqrs") (v "0.2.2") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.2.2") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.2") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "0gvjsq4r9g2faa136rv2rmzr572q7lpx4brqbj1fisrcrhh94ac7")))

(define-public crate-riker-cqrs-0.2.3 (c (n "riker-cqrs") (v "0.2.3") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "riker") (r "^0.2.3") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.3") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1ha6n02xibq51w6xd1fp72nx2691yq9gz5pdxzb8fj31rymrg1ra")))

