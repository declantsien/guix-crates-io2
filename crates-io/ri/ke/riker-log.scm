(define-module (crates-io ri ke riker-log) #:use-module (crates-io))

(define-public crate-riker-log-0.0.0 (c (n "riker-log") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.0") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "1bbk7052hfdppwg6ymcijvb6ck967wy0hjwk61chkj3k88kpki9z")))

(define-public crate-riker-log-0.1.0 (c (n "riker-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.0") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "1qzmkh1rghi7fgq8p5gj00afj0ri6za5adiv79a6vh6snr3xg88i")))

(define-public crate-riker-log-0.1.1 (c (n "riker-log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.1") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "0v75c76vyfd8xiswvdn2y4fdyjfdjri0va7qphm2admc2krsf0i5")))

(define-public crate-riker-log-0.1.4 (c (n "riker-log") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.4") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "15jc4dkpqg99k82zjaqf3xnnwmrymffyh72r07g5mgqmcd1ik8kj")))

(define-public crate-riker-log-0.1.5 (c (n "riker-log") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.5") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "1viqldh2bvm79nys48n3lcyvbkzysj0iywjpmnjmnymx314dm2nc")))

(define-public crate-riker-log-0.1.6 (c (n "riker-log") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.6") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "077fcvd70487b438gzwwi0a856v0pb9afi5xmmay6sy0mz95d1fi")))

(define-public crate-riker-log-0.1.7 (c (n "riker-log") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.7") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "1mhjnp35d2qdyaw3llf3nl34xzsnqchjml3svs9pkcrm8rpjsqx4")))

(define-public crate-riker-log-0.1.8 (c (n "riker-log") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.8") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "111vjjpd7iwnjz7gjg38k93fgjsk50727jgl3ix4s202m2ng81lh")))

(define-public crate-riker-log-0.1.9 (c (n "riker-log") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.1.9") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "0yp7l2vkji4zl3qv69r56w33vj46dbn1hwri3nbi102sjmsx709m")))

(define-public crate-riker-log-0.2.0 (c (n "riker-log") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.0") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "0dxqc6x0h08p4d5han3i16wn48xk1klgkyqph3kfp3khjvb37nkq")))

(define-public crate-riker-log-0.2.1 (c (n "riker-log") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.1") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "1cnqx644r6wfd5qlaz4kkl8gcrilmhzfhl6ax1agqyqiniil7aa5")))

(define-public crate-riker-log-0.2.2 (c (n "riker-log") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.2") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "0rcy085h9l1vxf5x72q34ij1x17w4b3fm4q9qjhw28rfdclhfzkr")))

(define-public crate-riker-log-0.2.3 (c (n "riker-log") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.3") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "0r34qh98sh0j061vds200lbwpwqkq4mni8zmx6yjdx6gjd2xzql4")))

(define-public crate-riker-log-0.2.4 (c (n "riker-log") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.4") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3.0") (d #t) (k 0)))) (h "16kxkhphlfv13hxqc6gkqh94jrwf2wv1acfs8h4r2hdwyczpc3jq")))

