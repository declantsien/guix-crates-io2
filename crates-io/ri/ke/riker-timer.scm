(define-module (crates-io ri ke riker-timer) #:use-module (crates-io))

(define-public crate-riker-timer-0.0.0 (c (n "riker-timer") (v "0.0.0") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0202ammg2frzc4jhqfq42vcfzk0pkrbhnf0h5xklmsgj89vss4kx")))

(define-public crate-riker-timer-0.1.0 (c (n "riker-timer") (v "0.1.0") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "1kg0v62ry6w1dbalbnq96jhd6sj8kdyyqjar8xhjl0rp22avph9q")))

(define-public crate-riker-timer-0.1.1 (c (n "riker-timer") (v "0.1.1") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0mgaa8xspyc3yzx6wbnmmyhsmdz00d0aaaij481jcwmippbrx7j2")))

(define-public crate-riker-timer-0.1.4 (c (n "riker-timer") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.4") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.1") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "10z57qfka1fpd06q672r4scs44lyz1sk7jx4g5kr8a5wnb9ws5h0")))

(define-public crate-riker-timer-0.1.5 (c (n "riker-timer") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.5") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.1") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "1pv5jlrwab4b0ycdlhxc1ci163qrhygrzziwl4ahgf94l1fy14mb")))

(define-public crate-riker-timer-0.1.6 (c (n "riker-timer") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.6") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.1") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cf609j10kxhb531w96y70w3n57cxlyv3r0vk2749pivy81c69jz")))

(define-public crate-riker-timer-0.1.7 (c (n "riker-timer") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.7") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.1") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "1m6s13b252c42syl3zzc0gpzkwi85knfcp6rch17nv3fbgn9rsx8")))

(define-public crate-riker-timer-0.1.8 (c (n "riker-timer") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.8") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.1") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "083hd8f2niacvwi2k053c2jzmbqp9cg2cqzgip2r45bc1b3mk43p")))

(define-public crate-riker-timer-0.1.9 (c (n "riker-timer") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.1.9") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.8") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0px6xfjr1zz63gxyg7vf07lx41n4830la1r2xds5k8fk0dx85aqm")))

(define-public crate-riker-timer-0.2.0 (c (n "riker-timer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.0") (d #t) (k 0)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bq1ilp8x4np8gfcsladaqi3smc0pzzc5n2vzzlkn18w2j3413gr")))

(define-public crate-riker-timer-0.2.2 (c (n "riker-timer") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.2") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.0") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "13rz4slgz6z01s8b08xck76hjna46hmiia4b65n0x9sy73434yyl")))

(define-public crate-riker-timer-0.2.3 (c (n "riker-timer") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.3") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.2") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "11fgnar5hqjl6hmkyqc9853n5vjz2zcb31pr30w5bh8irsbhhdk0")))

(define-public crate-riker-timer-0.2.4 (c (n "riker-timer") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.4") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.3") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ka4kv4ydq8d40724p9rcm9p5ffx77hjim53zsv10znx19ldm3xm")))

