(define-module (crates-io ri ke riker-mapvec) #:use-module (crates-io))

(define-public crate-riker-mapvec-0.1.0 (c (n "riker-mapvec") (v "0.1.0") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.0") (d #t) (k 0)))) (h "1mx6i8x5kqb6v6gdp894xnv85klhh2l0750nklnl1akmlizwq733")))

(define-public crate-riker-mapvec-0.1.1 (c (n "riker-mapvec") (v "0.1.1") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.1") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.0") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "0w90hyhyr0al135zczqrag1l6hwfvr51slqhzwl14c97hp9d9qm2")))

(define-public crate-riker-mapvec-0.1.4 (c (n "riker-mapvec") (v "0.1.4") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.4") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.1") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1bgwdbifj22bpqqdasq0ja5j9i1195355mv3wz9xgb5g72issqin")))

(define-public crate-riker-mapvec-0.1.5 (c (n "riker-mapvec") (v "0.1.5") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.5") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.1") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1860h67720zb56mhlvv2n2y3fjh70psf761bdy3kh1kh1rj27399")))

(define-public crate-riker-mapvec-0.1.6 (c (n "riker-mapvec") (v "0.1.6") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.6") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.1") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "089d8842nf4mbr3lkvnrrbdpbwdz7zyq89ak86i4sam0p4r8hzzl")))

(define-public crate-riker-mapvec-0.1.7 (c (n "riker-mapvec") (v "0.1.7") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.7") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.1") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1a81bs5q6m37nd7dhpd4m0m99yni63z5mzw7dyb2231cva3dljr2")))

(define-public crate-riker-mapvec-0.1.8 (c (n "riker-mapvec") (v "0.1.8") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "riker") (r "^0.1.8") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.1") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "16k5bhh1xjadg2i1qqvkz6zxkdi7j39pp35nnzlavc6rm9jskjm2")))

(define-public crate-riker-mapvec-0.1.9 (c (n "riker-mapvec") (v "0.1.9") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.1.9") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.8") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1gicd9sn3wfc306yscnawz6knjk6pjzvvgpbyzahsah40hfzlbs7")))

(define-public crate-riker-mapvec-0.2.0 (c (n "riker-mapvec") (v "0.2.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.0") (d #t) (k 0)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "15fxqzgz82f96c9dcyarpvphjg0xrbql24lmnmjiifcz8461n6z8")))

(define-public crate-riker-mapvec-0.2.2 (c (n "riker-mapvec") (v "0.2.2") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.2") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.0") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "09py5hhl6j7278nwmvwsw1iqn8cnj4cbi7lw4bz2gf4h2snp2sqk")))

(define-public crate-riker-mapvec-0.2.3 (c (n "riker-mapvec") (v "0.2.3") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.3") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.2") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "0z08b5jb3kvwz7k26xh7c18xf32dqkzv5i670gh7yphaa7jnh39g")))

(define-public crate-riker-mapvec-0.2.4 (c (n "riker-mapvec") (v "0.2.4") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "riker") (r "^0.2.4") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.3") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "09p7c8lkd3m049wkha949ks2hszsfb8zlwg05dwl5yzmld3abbwp")))

