(define-module (crates-io ri ke riker-patterns) #:use-module (crates-io))

(define-public crate-riker-patterns-0.1.0 (c (n "riker-patterns") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.0") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.0") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "0r94cxjbidww7506kfyiai6683y8y2rwlvfacgqlzrhl1xs45vn4")))

(define-public crate-riker-patterns-0.1.5 (c (n "riker-patterns") (v "0.1.5") (d (list (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.5") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.5") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1x75jlhcdzavwgrdy9yvbzavfxka3i4daplzahz7cdiq1mng4zg8")))

(define-public crate-riker-patterns-0.1.7 (c (n "riker-patterns") (v "0.1.7") (d (list (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.7") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.7") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1drripr5vw3q1qv4xhgvdbxp7j2n0mf8xxv97rzqgpy4034q1vav")))

(define-public crate-riker-patterns-0.1.8 (c (n "riker-patterns") (v "0.1.8") (d (list (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.8") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.8") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "0945iwmysd27d99h9bbbyb7lbag7spdddilh2jj8xj9zd6g6ly3l")))

(define-public crate-riker-patterns-0.1.9 (c (n "riker-patterns") (v "0.1.9") (d (list (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.9") (d #t) (k 0)) (d (n "riker-default") (r "^0.1.9") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "02rczblcw7hjr5b6j9jynp4ach8jc5qvym0kr6mvrpfv76h2sblf")))

(define-public crate-riker-patterns-0.2.0 (c (n "riker-patterns") (v "0.2.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.10") (d #t) (k 0)) (d (n "riker") (r "^0.2.0") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.0") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1dpkf4hwxg576prgrvd2qwk1lhrqdgnbps232k3h0wfx7jdb34p1")))

(define-public crate-riker-patterns-0.2.2 (c (n "riker-patterns") (v "0.2.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.10") (d #t) (k 0)) (d (n "riker") (r "^0.2.2") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.2") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "0phkqmmhk532b1pkbd8lwhyr6snjy2dybf0mzix4qzymf5cvjppl")))

(define-public crate-riker-patterns-0.2.3 (c (n "riker-patterns") (v "0.2.3") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.12") (d #t) (k 0)) (d (n "riker") (r "^0.2.3") (d #t) (k 0)) (d (n "riker-default") (r "^0.2.3") (d #t) (k 2)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1hjrzp0ag9x22f1jwymccch9ka65gfiikrkq2zcjkvvinwdfv4kn")))

(define-public crate-riker-patterns-0.3.1 (c (n "riker-patterns") (v "0.3.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "riker") (r "^0.3.1") (d #t) (k 0)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1cgixf0zff7di6n29dknq0l247kdxv64vvg3ijfjy336sc2x2l2m")))

(define-public crate-riker-patterns-0.3.2 (c (n "riker-patterns") (v "0.3.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "riker") (r "^0.3.2") (d #t) (k 0)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "0r12i4mwdhims7wps6p6d8l3hbmjhrbp3pba7pig4zm34hfw8hql")))

(define-public crate-riker-patterns-0.4.0 (c (n "riker-patterns") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "riker") (r "^0.4.0") (d #t) (k 0)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "05mqp7p310g5c8ji80y5nm54hngkwd6kpamg8bwr382grxz690dw")))

(define-public crate-riker-patterns-0.4.1 (c (n "riker-patterns") (v "0.4.1") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "riker") (r "^0.4.1") (d #t) (k 0)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "0f2i0h2ivyj2raakfi8qzm8lm7w55lzbgqmylh91arwxr5y6l2y7")))

(define-public crate-riker-patterns-0.4.2 (c (n "riker-patterns") (v "0.4.2") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "riker") (r "^0.4.2") (d #t) (k 0)) (d (n "riker-testkit") (r "^0.1.0") (d #t) (k 2)))) (h "1ngm04v95bbirinw54l5q0ymzfr566vagfz7cq4pbg1710g693i5")))

