(define-module (crates-io ri ke riker-dispatcher) #:use-module (crates-io))

(define-public crate-riker-dispatcher-0.1.0 (c (n "riker-dispatcher") (v "0.1.0") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.0") (d #t) (k 0)))) (h "1ahw785ld59512mgf2d884mqcamxkvkk3nhxh633651bv815m60a")))

(define-public crate-riker-dispatcher-0.1.1 (c (n "riker-dispatcher") (v "0.1.1") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.1") (d #t) (k 0)))) (h "18nmgk38lwx067vq530j709mzkcjixvfwz3rwqhf8dpjj4989p21")))

(define-public crate-riker-dispatcher-0.1.4 (c (n "riker-dispatcher") (v "0.1.4") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.4") (d #t) (k 0)))) (h "1cdlrz17p3kzm9lxwzgjjxv9pvkp32sxmsfh30311ymh1b3rzrz1")))

(define-public crate-riker-dispatcher-0.1.5 (c (n "riker-dispatcher") (v "0.1.5") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.5") (d #t) (k 0)))) (h "1b1n7i2w1vwhjk56bjvkwzcl4g60r6g7vfmba3px7d63a2ld0bx9")))

(define-public crate-riker-dispatcher-0.1.6 (c (n "riker-dispatcher") (v "0.1.6") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.6") (d #t) (k 0)))) (h "0gzwk6r20db75f5b2yp27y7flxv2hpx0258wqlsvxx719ad3yal1")))

(define-public crate-riker-dispatcher-0.1.7 (c (n "riker-dispatcher") (v "0.1.7") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.7") (d #t) (k 0)))) (h "0i83cxka6n4dr3qvzsq18saiz02rhy8gw9w9hqan8fzxsyz3b0fv")))

(define-public crate-riker-dispatcher-0.1.8 (c (n "riker-dispatcher") (v "0.1.8") (d (list (d (n "config") (r "^0.8") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.8") (d #t) (k 0)))) (h "0srlrah3haggrs9ghd49n1p579mdqss03qnyixhpdx5a3jwjqy8z")))

(define-public crate-riker-dispatcher-0.1.9 (c (n "riker-dispatcher") (v "0.1.9") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "riker") (r "^0.1.9") (d #t) (k 0)))) (h "1qb9sywqnr91p8rmzvlwlznifh5d60bjccljq4vzqhzf9hl53g6g")))

(define-public crate-riker-dispatcher-0.2.0 (c (n "riker-dispatcher") (v "0.2.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.10") (d #t) (k 0)) (d (n "riker") (r "^0.2.0") (d #t) (k 0)))) (h "1l3wg9dshb6xp8vsr6rf68qh45x7hwbmav2l2d8q80l2nppw8n8x")))

(define-public crate-riker-dispatcher-0.2.1 (c (n "riker-dispatcher") (v "0.2.1") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.10") (d #t) (k 0)) (d (n "riker") (r "^0.2.1") (d #t) (k 0)))) (h "1yk6m3pgskrylkp6r8ajz0id6cqc2sz446g4sd5ii6m0pz7ch82m")))

(define-public crate-riker-dispatcher-0.2.2 (c (n "riker-dispatcher") (v "0.2.2") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.10") (d #t) (k 0)) (d (n "riker") (r "^0.2.2") (d #t) (k 0)))) (h "07wjz00sq66wz3ylzaxm53gz09kqnycyrr4kwi2ns4kjzi6fr0bd")))

(define-public crate-riker-dispatcher-0.2.3 (c (n "riker-dispatcher") (v "0.2.3") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.12") (d #t) (k 0)) (d (n "riker") (r "^0.2.3") (d #t) (k 0)))) (h "1f57vnl43lamgg130vzhyylcfl71ickj4bhqra3082p567p5vqlv")))

(define-public crate-riker-dispatcher-0.2.4 (c (n "riker-dispatcher") (v "0.2.4") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.14") (d #t) (k 0)) (d (n "riker") (r "^0.2.4") (d #t) (k 0)))) (h "1b1y4z7j3wgh6mlykg347pr7d3gzg5gaasps29b2pxqvv4zdzva5")))

