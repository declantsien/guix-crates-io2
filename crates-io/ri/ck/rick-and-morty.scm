(define-module (crates-io ri ck rick-and-morty) #:use-module (crates-io))

(define-public crate-rick-and-morty-0.1.0 (c (n "rick-and-morty") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mockito") (r "^0.28") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.9") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0nc6885ircdimsc6gxnr8s2zh8zzjm71q4mz60m61qcm9hm54fhd")))

