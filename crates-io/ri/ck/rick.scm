(define-module (crates-io ri ck rick) #:use-module (crates-io))

(define-public crate-rick-0.1.0 (c (n "rick") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "open") (r "^3.2.0") (d #t) (k 0)) (d (n "skim") (r "^0.10.2") (d #t) (k 0)))) (h "02c949lhrfqq3m6gb499f66plyw2cvc7fsbazba6fwpya0ba05wr")))

(define-public crate-rick-0.2.0 (c (n "rick") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "jfs") (r "^0.8.0") (d #t) (k 0)) (d (n "open") (r "^3.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termimad") (r "^0.20.6") (d #t) (k 0)))) (h "0py9ngiv61awssf0gssahk9xkrymvvmjh1znpf24q07sc8ymgs0f")))

(define-public crate-rick-0.3.0 (c (n "rick") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "jfs") (r "^0.8.0") (d #t) (k 0)) (d (n "open") (r "^3.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termimad") (r "^0.20.6") (d #t) (k 0)))) (h "1ghv72dq27k87qvc8capmjpqqc7mnczvzqajjw7dmgv6k2xck3pf")))

