(define-module (crates-io ri ck rickroll) #:use-module (crates-io))

(define-public crate-rickroll-0.1.0 (c (n "rickroll") (v "0.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8") (f (quote ("v4" "fast-rng"))) (o #t) (d #t) (k 0)))) (h "00cs8cqg044p3cbcly574kpgvcj1dip1f01fl934mlinja7sx9g3") (f (quote (("offline" "uuid") ("default")))) (y #t)))

(define-public crate-rickroll-0.1.1 (c (n "rickroll") (v "0.1.1") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8") (f (quote ("v4" "fast-rng"))) (o #t) (d #t) (k 0)))) (h "1kbd3ybmd4whc21sa33wlnr8dy4gp0j93q64pw5mfwz0vkr12d23") (f (quote (("offline" "uuid") ("default"))))))

