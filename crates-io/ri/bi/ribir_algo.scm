(define-module (crates-io ri bi ribir_algo) #:use-module (crates-io))

(define-public crate-ribir_algo-0.0.0 (c (n "ribir_algo") (v "0.0.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1bgxjg0m12gnpp8v1chh9pydwhi3hwpyxlmsmcf1pa1c9qifzqh3")))

(define-public crate-ribir_algo-0.0.1-alpha.1 (c (n "ribir_algo") (v "0.0.1-alpha.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xp1gvcw35kvc9d4niicb9naxmwkpdqjaby25fbsqc4jf1i18vvf")))

(define-public crate-ribir_algo-0.0.1-alpha.2 (c (n "ribir_algo") (v "0.0.1-alpha.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1c1402lv6fm59sy0fggn0dd9a65rzdsxga077h65my109mcrwkw3")))

(define-public crate-ribir_algo-0.0.1-alpha.3 (c (n "ribir_algo") (v "0.0.1-alpha.3") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1b61i2gfc2f1ixgqwi4hmdfdbbwvln0yzz7qskfxw8sfaw96icbh")))

(define-public crate-ribir_algo-0.0.1-alpha.4 (c (n "ribir_algo") (v "0.0.1-alpha.4") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "02a6pksfh0w5m0bsrz1fih059vnph0fh1202322w7ik558qn575x")))

(define-public crate-ribir_algo-0.0.1-alpha.5 (c (n "ribir_algo") (v "0.0.1-alpha.5") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "19bmgdps8qi2wgjad57w2759cqizm4b3ksvzxnb3k870hzpvm6yy")))

(define-public crate-ribir_algo-0.1.0-alpha.0 (c (n "ribir_algo") (v "0.1.0-alpha.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0b845ga2vlcrs3h84k853z3983056l5lp40r4f433ww8y23qpbyk")))

(define-public crate-ribir_algo-0.1.0-beta.1 (c (n "ribir_algo") (v "0.1.0-beta.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1l2a15y3zzg5q9b005njgr8yxw0b15jg2clkaajnxdr4w3n2r5w3")))

(define-public crate-ribir_algo-0.1.0-beta.2 (c (n "ribir_algo") (v "0.1.0-beta.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "141xl9zinmxvcp4rrkqk1vch1ri0f38wf6gzc6fgqh63g83sin5b")))

(define-public crate-ribir_algo-0.1.0-beta.3 (c (n "ribir_algo") (v "0.1.0-beta.3") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0w5qpd0lb4dyql0jxz9yfwc1x0pjdm4c509b4r96mx9w48daj466")))

(define-public crate-ribir_algo-0.1.0-beta.4 (c (n "ribir_algo") (v "0.1.0-beta.4") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "12ki2hdll7jh7qabvy70ffxp9h0gz4mx0fmmwjrglai48yyzv049")))

(define-public crate-ribir_algo-0.1.0-beta.5 (c (n "ribir_algo") (v "0.1.0-beta.5") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0r0c39as7gkq7jk5vnxkzr7ylrad20b2ivfn0r8fm2w6c0d91akk")))

(define-public crate-ribir_algo-0.1.0-beta.6 (c (n "ribir_algo") (v "0.1.0-beta.6") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0l3v8ws753ybvvabimsj33rhdqhcprgal31r688v77wpij65w414")))

(define-public crate-ribir_algo-0.1.0-beta.7 (c (n "ribir_algo") (v "0.1.0-beta.7") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "086xnqqcbnbwyv2s36jwbbv2m54d41n0aajn3ymws2h7vfdm4dwr")))

(define-public crate-ribir_algo-0.2.0-alpha.1 (c (n "ribir_algo") (v "0.2.0-alpha.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "184wb200ik0bb1msj5cf4fphdh32wssx6mbsq7snrhmc3g7671ga")))

(define-public crate-ribir_algo-0.2.0-alpha.2 (c (n "ribir_algo") (v "0.2.0-alpha.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0mm52pslhxs2hdflzzcq8cyjq9yf0cs6mbw246100hwwilzxk8ff")))

(define-public crate-ribir_algo-0.2.0-alpha.3 (c (n "ribir_algo") (v "0.2.0-alpha.3") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1ci8pf857iak42j5bq3r5rkks61rdl5f123za8054l3qlnggrvqf")))

(define-public crate-ribir_algo-0.2.0-alpha.4 (c (n "ribir_algo") (v "0.2.0-alpha.4") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1n7ji66r155hhxmblgak4d169dcmm8j1g9pp33h7i8dnqkiyvfi4")))

(define-public crate-ribir_algo-0.2.0-alpha.5 (c (n "ribir_algo") (v "0.2.0-alpha.5") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1lw4l42ndbrvyqmnhp6dbn2yr87zr6x39qih7qw6kr7pimvhdbqi")))

(define-public crate-ribir_algo-0.2.0-alpha.6 (c (n "ribir_algo") (v "0.2.0-alpha.6") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1pp3ng7l5v6yhsxcsnl8j6ng002w670byd4dlq6dbw96f62f11vg")))

(define-public crate-ribir_algo-0.2.0-alpha.7 (c (n "ribir_algo") (v "0.2.0-alpha.7") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "19r8b1ksl28a488agaxsi1r3pf0lwigpg4731kl1bpwyg1zl0rx6")))

(define-public crate-ribir_algo-0.1.0 (c (n "ribir_algo") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1q95cmjipbikvablikxxg5n3ygv65xjwf0n78qj5gk69hzbz7gyx")))

(define-public crate-ribir_algo-0.2.0-beta.1 (c (n "ribir_algo") (v "0.2.0-beta.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0gg53lb81y6f60iz36f27x6qfv23j4i098qxy40qn0lrw6kppjb1")))

(define-public crate-ribir_algo-0.3.0-alpha.1 (c (n "ribir_algo") (v "0.3.0-alpha.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0sqzbf0as5hfmwfi66vlg18dlkbzpykz1ln2j994c0rqnk3bayi3")))

(define-public crate-ribir_algo-0.3.0-alpha.2 (c (n "ribir_algo") (v "0.3.0-alpha.2") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0zh2m0dlymkhlp9xypnilcfx17bazjcy7gmgjjfcg4m8g6a69amf")))

(define-public crate-ribir_algo-0.3.0-alpha.3 (c (n "ribir_algo") (v "0.3.0-alpha.3") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0znl2y1ndrbz3c9ikxfsyi4y0g3rgzblxzbyyygbp62v56qqcivn")))

(define-public crate-ribir_algo-0.3.0-alpha.4 (c (n "ribir_algo") (v "0.3.0-alpha.4") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0fjvg2zyp26j6s7p6d7chxfwdq32h3976d66ayg0qrh2qfwyn87g")))

(define-public crate-ribir_algo-0.3.0-alpha.5 (c (n "ribir_algo") (v "0.3.0-alpha.5") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1nyv4l800ddbdfw6g6v2cg7ifljyf4h4q1dxz3n6njkzwfz8hv7m")))

(define-public crate-ribir_algo-0.3.0-alpha.6 (c (n "ribir_algo") (v "0.3.0-alpha.6") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "triomphe") (r "^0.1.11") (d #t) (k 0)))) (h "17nr2ang20iiij4mpas1k7ggzf8qzagv5h04jwcn494n5i0ixk30")))

(define-public crate-ribir_algo-0.2.0 (c (n "ribir_algo") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1xfdp291v06l3jkxdnxjz4814pzjflimf5kx3bv3pr9c73935qkg")))

