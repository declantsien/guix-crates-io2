(define-module (crates-io ri bi ribir_material) #:use-module (crates-io))

(define-public crate-ribir_material-0.0.0 (c (n "ribir_material") (v "0.0.0") (d (list (d (n "ribir_core") (r "^0.0.0") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.0.0") (d #t) (k 0)))) (h "0nnvs1a4afal0w7yil8icyi71kd9xslyc1mmg9ip0nsjwxac2v27")))

(define-public crate-ribir_material-0.0.1-alpha.1 (c (n "ribir_material") (v "0.0.1-alpha.1") (d (list (d (n "ribir_core") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.0.1-alpha.1") (d #t) (k 0)))) (h "06sqdk755p36pkak8ygx85qpxpq6n1550dhj13p64b2mv50zm8m0")))

(define-public crate-ribir_material-0.0.1-alpha.2 (c (n "ribir_material") (v "0.0.1-alpha.2") (d (list (d (n "ribir_core") (r "^0.0.1-alpha.2") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.0.1-alpha.2") (d #t) (k 0)))) (h "1x8iniynph28xhkqajmif2r0wawjbqr9cpihlyfnvrsaawx7251v")))

(define-public crate-ribir_material-0.0.1-alpha.3 (c (n "ribir_material") (v "0.0.1-alpha.3") (d (list (d (n "ribir_core") (r "^0.0.1-alpha.3") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.0.1-alpha.3") (d #t) (k 0)))) (h "1sd3gq1yi1pxyrss1frlqgf65wvfgw50w6rm8djqin6cj9gh8mqm")))

(define-public crate-ribir_material-0.0.1-alpha.4 (c (n "ribir_material") (v "0.0.1-alpha.4") (d (list (d (n "ribir_core") (r "^0.0.1-alpha.4") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.0.1-alpha.4") (d #t) (k 0)))) (h "1whkfjyj57yr6nhs2hig8wkpwji64x93qxr58mayfq5a2371vmbr")))

(define-public crate-ribir_material-0.0.1-alpha.5 (c (n "ribir_material") (v "0.0.1-alpha.5") (d (list (d (n "ribir_core") (r "^0.0.1-alpha.5") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.0.1-alpha.5") (d #t) (k 0)))) (h "03y8npdldjh2f142ar6zpp3fram1lcn7mci0cf3w2p6il7wlrfbr")))

(define-public crate-ribir_material-0.1.0-alpha.0 (c (n "ribir_material") (v "0.1.0-alpha.0") (d (list (d (n "ribir_core") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "02yc5c7qx3nih6rl8mfmys15p33wvbv42hgwd1j7bz5bi62rv0ig")))

(define-public crate-ribir_material-0.1.0-beta.2 (c (n "ribir_material") (v "0.1.0-beta.2") (d (list (d (n "ribir_core") (r "^0.1.0-beta.2") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.1.0-beta.2") (d #t) (k 0)))) (h "0aimvjyznqbwy5w5dm0as0gj0lcwy98d274ccc87dyy29b4rpivj")))

(define-public crate-ribir_material-0.1.0-beta.4 (c (n "ribir_material") (v "0.1.0-beta.4") (d (list (d (n "ribir_core") (r "^0.1.0-beta.4") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.1.0-beta.4") (d #t) (k 0)))) (h "0d3s9ipqb14ddavvk9massv8z2nca6zsyn5fzz0z994axx52b6b7")))

(define-public crate-ribir_material-0.1.0-beta.5 (c (n "ribir_material") (v "0.1.0-beta.5") (d (list (d (n "ribir_core") (r "^0.1.0-beta.5") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.1.0-beta.5") (d #t) (k 0)))) (h "118rkkb91ryh21yfr7hsxp6lshb19nqssh7psmrb3jwpmrc89hm2")))

(define-public crate-ribir_material-0.1.0-beta.6 (c (n "ribir_material") (v "0.1.0-beta.6") (d (list (d (n "ribir_core") (r "^0.1.0-beta.6") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.1.0-beta.6") (d #t) (k 0)))) (h "14nfpn6j16fv324356ckz3cw8rl4xjb4f2x3i60m6zh7as63fdqz")))

(define-public crate-ribir_material-0.1.0-beta.7 (c (n "ribir_material") (v "0.1.0-beta.7") (d (list (d (n "ribir_core") (r "^0.1.0-beta.7") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.1.0-beta.7") (d #t) (k 0)))) (h "173z4rzzyaw2sxw8s47kh4ykpvgjfvnsp3wma213h4l07b9fssh9")))

(define-public crate-ribir_material-0.2.0-alpha.1 (c (n "ribir_material") (v "0.2.0-alpha.1") (d (list (d (n "ribir_core") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "12p7wql53xpd17f2mmbzi5ny9304wnw7256c7bx2d5pi76sb2j65")))

(define-public crate-ribir_material-0.2.0-alpha.2 (c (n "ribir_material") (v "0.2.0-alpha.2") (d (list (d (n "ribir_core") (r "^0.2.0-alpha.2") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.2.0-alpha.2") (d #t) (k 0)))) (h "1qryq348k81ammxl3km9jkn1hraf3v848nygc1alp542zp3dshqn")))

(define-public crate-ribir_material-0.2.0-alpha.3 (c (n "ribir_material") (v "0.2.0-alpha.3") (d (list (d (n "ribir_core") (r "^0.2.0-alpha.3") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.2.0-alpha.3") (d #t) (k 0)))) (h "1sz65raskn7j237pibzxlih1crrdplxgypm19gyjlqas0ijk1pwg")))

(define-public crate-ribir_material-0.2.0-alpha.4 (c (n "ribir_material") (v "0.2.0-alpha.4") (d (list (d (n "ribir_core") (r "^0.2.0-alpha.4") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.2.0-alpha.4") (d #t) (k 0)))) (h "0g7lx6qq31adp37v7n4h1hvv80dlz0vlr19wad38ah6dxsfsqxfp")))

(define-public crate-ribir_material-0.2.0-alpha.5 (c (n "ribir_material") (v "0.2.0-alpha.5") (d (list (d (n "ribir_core") (r "^0.2.0-alpha.5") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.2.0-alpha.5") (d #t) (k 0)))) (h "1f5sndd6l6g2zjara41dp5xd30ir1nkmmarb2j2f4alk22h7h738")))

(define-public crate-ribir_material-0.2.0-alpha.6 (c (n "ribir_material") (v "0.2.0-alpha.6") (d (list (d (n "ribir_core") (r "^0.2.0-alpha.6") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "02kj68nap1kxhjqcviawj0wgb8n0yvr0vmw91alyin53lyb404ay")))

(define-public crate-ribir_material-0.2.0-alpha.7 (c (n "ribir_material") (v "0.2.0-alpha.7") (d (list (d (n "ribir_core") (r "^0.2.0-alpha.7") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.2.0-alpha.7") (d #t) (k 0)))) (h "0c2c9y0qkw015mw966b3yx11bgyixgcpl5hnm4g1q5ihr55wh82j")))

(define-public crate-ribir_material-0.1.0 (c (n "ribir_material") (v "0.1.0") (d (list (d (n "ribir_core") (r "^0.1.0") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.1.0") (d #t) (k 0)))) (h "0ndymfps0rav2ngqp0wqn5zs2h6qb5h76cg00w4vjchy26lxxkjv")))

(define-public crate-ribir_material-0.2.0-beta.1 (c (n "ribir_material") (v "0.2.0-beta.1") (d (list (d (n "ribir_core") (r "^0.2.0-beta.1") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.2.0-beta.1") (d #t) (k 0)))) (h "0n19h7bf3mihvf64r1vsyghq19w34cmv4a6jcfnhi85fd4lr359n")))

(define-public crate-ribir_material-0.3.0-alpha.1 (c (n "ribir_material") (v "0.3.0-alpha.1") (d (list (d (n "ribir_core") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.3.0-alpha.1") (d #t) (k 0)))) (h "19r1dm6zfg5p50rka682fv8iy2ky0l2b6yqzfsxr4081yf99bg9d")))

(define-public crate-ribir_material-0.3.0-alpha.2 (c (n "ribir_material") (v "0.3.0-alpha.2") (d (list (d (n "ribir_core") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.3.0-alpha.2") (d #t) (k 0)))) (h "1wkx9gx4rf383q8ysqjgwvwzmk1vjhdaargqhqgnpzrxq5pxbjws")))

(define-public crate-ribir_material-0.3.0-alpha.3 (c (n "ribir_material") (v "0.3.0-alpha.3") (d (list (d (n "ribir_core") (r "^0.3.0-alpha.3") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.3.0-alpha.3") (d #t) (k 0)))) (h "0z7wws3y5dwdk7j02fv8a1pgmymf1xwll0s2jr2n13v2za1qz2j4")))

(define-public crate-ribir_material-0.3.0-alpha.4 (c (n "ribir_material") (v "0.3.0-alpha.4") (d (list (d (n "ribir_core") (r "^0.3.0-alpha.4") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.3.0-alpha.4") (d #t) (k 0)))) (h "1yci2xlyrwx4szrxfxf1i7j17p2d36hnnrkiriafgdsiaqhcxw65")))

(define-public crate-ribir_material-0.3.0-alpha.5 (c (n "ribir_material") (v "0.3.0-alpha.5") (d (list (d (n "ribir_core") (r "^0.3.0-alpha.5") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.3.0-alpha.5") (d #t) (k 0)))) (h "0l7p009h18r0ysykdckjvgd3p097c7smapw06mvd81379fvvf6wb")))

(define-public crate-ribir_material-0.2.0 (c (n "ribir_material") (v "0.2.0") (d (list (d (n "ribir_core") (r "^0.2.0") (d #t) (k 0)) (d (n "ribir_widgets") (r "^0.2.0") (d #t) (k 0)))) (h "08ygyibh2jci6mdv1fc3qxmf36byc5ahqvisa77qfi5hjcqwi0jy")))

