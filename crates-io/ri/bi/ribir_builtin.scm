(define-module (crates-io ri bi ribir_builtin) #:use-module (crates-io))

(define-public crate-ribir_builtin-0.0.0 (c (n "ribir_builtin") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1ynvfhvila8wna24nyw60s64vjmcgxys5ab3a30ngz60qbafrlys")))

(define-public crate-ribir_builtin-0.0.1-alpha.1 (c (n "ribir_builtin") (v "0.0.1-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0cra451ld3mf1isyqsai2lw18xygh01jnwhrh6g5r770qsillshd")))

(define-public crate-ribir_builtin-0.0.1-alpha.2 (c (n "ribir_builtin") (v "0.0.1-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0zlllv8zsawz9mpsmdnv10mpydg3h72b0nlb0yr0vn2hlgl47mgz")))

(define-public crate-ribir_builtin-0.0.1-alpha.3 (c (n "ribir_builtin") (v "0.0.1-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1dd6p51qjnvd1sjx6k834wnz9i0srzc16s0ar6si9w3g9yfq579f")))

(define-public crate-ribir_builtin-0.0.1-alpha.4 (c (n "ribir_builtin") (v "0.0.1-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0gzqdgkvsdx6lya9s8v3q4pg42f5j2dgaf8x6djgzp93bnd9lpa7")))

(define-public crate-ribir_builtin-0.0.1-alpha.5 (c (n "ribir_builtin") (v "0.0.1-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1gvcb6hlp3inmiisq5x4ma4h1yiv23ab3dqxkz2kdxqgfpgplpc4")))

(define-public crate-ribir_builtin-0.1.0-alpha.0 (c (n "ribir_builtin") (v "0.1.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "14pr53gl7fwaqxvd7r0jw62hi5kc41v91c0xzklmpv62aglmlp1w")))

(define-public crate-ribir_builtin-0.1.0-beta.1 (c (n "ribir_builtin") (v "0.1.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1c0hkz5i58ckg4q8gn12ixiqj8bwnpx3vgx9xiqkkvx7s3k4wjsf")))

(define-public crate-ribir_builtin-0.1.0-beta.2 (c (n "ribir_builtin") (v "0.1.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1gqqriq13bag754449ir0a50hd90i5mj2qr9x713739fj1279rv3")))

(define-public crate-ribir_builtin-0.1.0-beta.3 (c (n "ribir_builtin") (v "0.1.0-beta.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "14fkq7qaxnxkhc71qb153chpi9p470an6alxlh9r7khndkk811ly")))

(define-public crate-ribir_builtin-0.1.0-beta.4 (c (n "ribir_builtin") (v "0.1.0-beta.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0nrj85fv80mf17swxq3jqqdzi9a8g25r7f38q32skkzhfwjm299p")))

(define-public crate-ribir_builtin-0.1.0-beta.5 (c (n "ribir_builtin") (v "0.1.0-beta.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0a5x6a68nfhlx38pmwpn9mpr8h63f1x8kqr5dg4w7haiamnc0xwm")))

(define-public crate-ribir_builtin-0.1.0-beta.6 (c (n "ribir_builtin") (v "0.1.0-beta.6") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0l9nvvvhs05lxgg79b1l8zxhs3vq3c4scxlpfjf9vpachc5hjb29")))

(define-public crate-ribir_builtin-0.1.0-beta.7 (c (n "ribir_builtin") (v "0.1.0-beta.7") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0ys81h4r2nw34bz6i2g9gvrimvfb05rldhdghwyy5ng9z1ybch1s")))

(define-public crate-ribir_builtin-0.2.0-alpha.1 (c (n "ribir_builtin") (v "0.2.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0c6v1rv56cfrpbdl50044b5wxz5dm3is0ln7g6wk2w9lszb45apk")))

(define-public crate-ribir_builtin-0.2.0-alpha.2 (c (n "ribir_builtin") (v "0.2.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "077ilzjm9f4pyazr4wsvc89df38jf4fb3kl13sbfhd4dmbi4pfsk")))

(define-public crate-ribir_builtin-0.2.0-alpha.3 (c (n "ribir_builtin") (v "0.2.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0f697mwdvqagan1s05hhz8byd4p68qhgwzwijlx4lg7sx47w747n")))

(define-public crate-ribir_builtin-0.2.0-alpha.4 (c (n "ribir_builtin") (v "0.2.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0197sr9m0nzf4xa8wxg4n3qv3bhnwzwwyb6gi693rv64lq6qcxfw")))

(define-public crate-ribir_builtin-0.2.0-alpha.5 (c (n "ribir_builtin") (v "0.2.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0aav2algkcns18gcd4rg0ygh29h94jl83zgs57fg87a88cglyg78")))

(define-public crate-ribir_builtin-0.1.0 (c (n "ribir_builtin") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1wd3n12gb8sbiil2cfsxwnvv21jpnash4fvfybl48fv9l4s51xk0")))

