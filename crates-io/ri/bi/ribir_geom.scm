(define-module (crates-io ri bi ribir_geom) #:use-module (crates-io))

(define-public crate-ribir_geom-0.0.0 (c (n "ribir_geom") (v "0.0.0") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "152d9rr6620lpki45vwyl6q77dsvcw4jvhfrcjz9p1a4mg27wzkq")))

(define-public crate-ribir_geom-0.0.1-alpha.1 (c (n "ribir_geom") (v "0.0.1-alpha.1") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0g9clwkcnabsfnpplm5rjf051jh2xa8cf1i22yikc1h85q47ildf")))

(define-public crate-ribir_geom-0.0.1-alpha.2 (c (n "ribir_geom") (v "0.0.1-alpha.2") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0x6i661hv46ldl61dabw2rpdr81php63zfdrbi7lvkf0p6hvm7ic")))

(define-public crate-ribir_geom-0.0.1-alpha.3 (c (n "ribir_geom") (v "0.0.1-alpha.3") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "18px3z3b11wzd0j6dl4avsqyfb4fv6dqzl3k78knx49v1nkkxxb7")))

(define-public crate-ribir_geom-0.0.1-alpha.4 (c (n "ribir_geom") (v "0.0.1-alpha.4") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "1jqg8l3vc3jjl6cmk9nj1jfkxrk7i14fli77q3v0yghmv5fjgwan")))

(define-public crate-ribir_geom-0.0.1-alpha.5 (c (n "ribir_geom") (v "0.0.1-alpha.5") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "1dfhnqj6shkqf6d8z94c7l24wx48y33a8xqs5kfy37n6yqpr8mzr")))

(define-public crate-ribir_geom-0.1.0-alpha.0 (c (n "ribir_geom") (v "0.1.0-alpha.0") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0xisf1954z54j2788bam0g35ccgr41hsx85ilm1fh7kcs32997j2")))

(define-public crate-ribir_geom-0.1.0-beta.1 (c (n "ribir_geom") (v "0.1.0-beta.1") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0kq5pga0dy7jpq2cx7wr9g2w011vga3wbg8qzvlzzhkg0qywxx7r")))

(define-public crate-ribir_geom-0.1.0-beta.2 (c (n "ribir_geom") (v "0.1.0-beta.2") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0sh3npw88lqmi2gxpxzjx8r83ax832zgpbhh73mljp88v8vaad2m")))

(define-public crate-ribir_geom-0.1.0-beta.3 (c (n "ribir_geom") (v "0.1.0-beta.3") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "1azcjp8a25fvj63slz50f54m3d3mwnxan9v7fb9bjil6lc96lqh8")))

(define-public crate-ribir_geom-0.1.0-beta.4 (c (n "ribir_geom") (v "0.1.0-beta.4") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "1fpwxvyna86cab5vw5mqnwd5pvaazd0jmzg84nffx9phr63915pl")))

(define-public crate-ribir_geom-0.1.0-beta.5 (c (n "ribir_geom") (v "0.1.0-beta.5") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0naqk26bzg49wsb5fryahwjmgpcsmihcqb8x6f6yc2x3q230fb9v")))

(define-public crate-ribir_geom-0.1.0-beta.6 (c (n "ribir_geom") (v "0.1.0-beta.6") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "1ibbpsn5c24j0r02awzgvhlmmd94ri5bsd3rz0i3gi22kb4sk61n")))

(define-public crate-ribir_geom-0.1.0-beta.7 (c (n "ribir_geom") (v "0.1.0-beta.7") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "13z9kr98l2arqiclqcnc3v096gqfnw93idy3a31w9ka0h4i6qbic")))

(define-public crate-ribir_geom-0.2.0-alpha.1 (c (n "ribir_geom") (v "0.2.0-alpha.1") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "01i4mhqgk1izbvy65nlqd7md58c5k706m4n2142hv03aa3g0455c")))

(define-public crate-ribir_geom-0.2.0-alpha.2 (c (n "ribir_geom") (v "0.2.0-alpha.2") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0zig8jvg3nqfh084a4axgd5nkd6ca88h2960b0ffvlmxwscmg39d")))

(define-public crate-ribir_geom-0.2.0-alpha.3 (c (n "ribir_geom") (v "0.2.0-alpha.3") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "1ahjng343cvyyd68rrhi0qlpwisq6d401zdmrdknxmylkyzjp8fq")))

(define-public crate-ribir_geom-0.2.0-alpha.4 (c (n "ribir_geom") (v "0.2.0-alpha.4") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "1gm0anc21i24zf1g9l6wb36mdhs4krqbxpq2rs1p7y2kkl2wmrac")))

(define-public crate-ribir_geom-0.2.0-alpha.5 (c (n "ribir_geom") (v "0.2.0-alpha.5") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "1b9n02grh0dpl72hilkqvcxnsjvdzhyqdyi61r8n5w35da98b25a")))

(define-public crate-ribir_geom-0.2.0-alpha.6 (c (n "ribir_geom") (v "0.2.0-alpha.6") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "1kij1m6bxjmf4d2ifx083vwp6djfyrvacdjzphds3kqlyjw3vskd")))

(define-public crate-ribir_geom-0.2.0-alpha.7 (c (n "ribir_geom") (v "0.2.0-alpha.7") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0c6dsw9x8x9gbhcdx1p4nc4b56n70xq47m5ixjjndh3sn94pvf6w")))

(define-public crate-ribir_geom-0.1.0 (c (n "ribir_geom") (v "0.1.0") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0rf3r4wajy41rlzfzh2lqkpzyp35i10f6lahwm0vpn42hnx8l5y8")))

(define-public crate-ribir_geom-0.2.0-beta.1 (c (n "ribir_geom") (v "0.2.0-beta.1") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0jah6lcyqq0ymhnfb1333zl30rjcpjpf3lzd0hnxnbs5rym8zn7g")))

(define-public crate-ribir_geom-0.3.0-alpha.1 (c (n "ribir_geom") (v "0.3.0-alpha.1") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0zp42nrw0alb1kk7v221fxg5hpjf4w3ldhyw7krv9ll1fzrprgbv")))

(define-public crate-ribir_geom-0.3.0-alpha.2 (c (n "ribir_geom") (v "0.3.0-alpha.2") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "17p2r58fsiqq0kn7ni3iz088c95nwwas09ai0yyxzw8z2lz7y7mw")))

(define-public crate-ribir_geom-0.3.0-alpha.3 (c (n "ribir_geom") (v "0.3.0-alpha.3") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "05h3n0p3qx3vygmn47963rk86bawh5wmrjqk7i83rkmyq1xzb7b4")))

(define-public crate-ribir_geom-0.3.0-alpha.4 (c (n "ribir_geom") (v "0.3.0-alpha.4") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "10jnhr7i7j8kgyiqaqlcd1131ipjzcq5vz2fdhzgshasbyj3swbm")))

(define-public crate-ribir_geom-0.3.0-alpha.5 (c (n "ribir_geom") (v "0.3.0-alpha.5") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "0q9yxw0zgqgqw8dgi66y9i2fxb63475qw4cm56p08d1hfv9z2y8i")))

(define-public crate-ribir_geom-0.3.0-alpha.6 (c (n "ribir_geom") (v "0.3.0-alpha.6") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "1y4g90qbpv6jpvila36dzwidnd3wmknzlgk6vky77msbdmn53fc1")))

(define-public crate-ribir_geom-0.2.0 (c (n "ribir_geom") (v "0.2.0") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)))) (h "1m5l89gl84bpdbjcd2wapg00n5q2fz71wam83w4fwjkszjr8w1bh")))

