(define-module (crates-io ri on rion) #:use-module (crates-io))

(define-public crate-rion-0.0.1 (c (n "rion") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "jsonpath-plus") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09048akwvf077y66s9iz611qrasybwqqhl6hvz4g9pcd8ffyrwi6")))

(define-public crate-rion-0.0.2 (c (n "rion") (v "0.0.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "jsonpath-plus") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1l72rnynfj52asm3rs6hqnipsc6vsv9dy62zbmxdav2dw7q6czxf")))

