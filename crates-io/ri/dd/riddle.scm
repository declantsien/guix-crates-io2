(define-module (crates-io ri dd riddle) #:use-module (crates-io))

(define-public crate-riddle-0.0.0 (c (n "riddle") (v "0.0.0") (h "1zc603w1bz3liwfqmm0w8w7sjqlivvr2cnji5z3a4zmy1ix20jhw")))

(define-public crate-riddle-0.0.1 (c (n "riddle") (v "0.0.1") (d (list (d (n "metadata") (r "^0.44.0") (d #t) (k 0) (p "windows-metadata")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "full"))) (k 0)))) (h "1xz2yy3bj3brhj5hfx20b9ks5ih84mgspljxj9sk4jzi175wy9y1")))

(define-public crate-riddle-0.1.0 (c (n "riddle") (v "0.1.0") (d (list (d (n "windows-bindgen") (r "^0.52.0") (k 0)))) (h "1gy0m6phsdx7yb9caimmwg89rhv4f1lfhx4j7z211r24p1qs5m87") (r "1.56")))

(define-public crate-riddle-0.2.0 (c (n "riddle") (v "0.2.0") (d (list (d (n "windows-bindgen") (r "^0.53.0") (k 0)))) (h "10fpg3h6xbcwyyhc8ghqg8mj7q1dplxzq59zh71lcmj431irijir") (r "1.60")))

(define-public crate-riddle-0.3.0 (c (n "riddle") (v "0.3.0") (d (list (d (n "windows-bindgen") (r "^0.55") (k 0)))) (h "0bd0g8xqmjgfa79y836dlq030a6r2pk68r8a4nasmzwj0s8crzf2") (r "1.60")))

(define-public crate-riddle-0.56.0 (c (n "riddle") (v "0.56.0") (d (list (d (n "windows-bindgen") (r "^0.56.0") (k 0)))) (h "0hanph3krc7dml085kciic95j67bz3rmpvc1n0k4hyj02jhbpw0d") (r "1.60")))

