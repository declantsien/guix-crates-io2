(define-module (crates-io ri dd riddance) #:use-module (crates-io))

(define-public crate-riddance-0.0.0 (c (n "riddance") (v "0.0.0") (h "1m7jjwrkhlap605bpnjr37afrli8v9sj6dp2xr3rjy20g1cmhpqd")))

(define-public crate-riddance-0.1.0 (c (n "riddance") (v "0.1.0") (d (list (d (n "typenum") (r "^1.17.0") (f (quote ("const-generics"))) (d #t) (k 0)))) (h "15h9vyiyxqwspqq1xg97r9hbypapsf8lxfrmxdq4rp8y9kw9pwx7")))

(define-public crate-riddance-0.1.1 (c (n "riddance") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "typenum") (r "^1.17.0") (f (quote ("const-generics"))) (d #t) (k 0)))) (h "1kg76kka50h82j9ywrc3kdcyc7r2j3kzh5ci45g0j1vmy01l10vl")))

