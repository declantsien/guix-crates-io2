(define-module (crates-io ri ff riff-ani) #:use-module (crates-io))

(define-public crate-riff-ani-0.1.0 (c (n "riff-ani") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "riff") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1128y34dxwwlvprihrma4nx3bnsic2vvf5ndnfzvy9n0vyk3wsfk")))

