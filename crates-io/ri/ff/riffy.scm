(define-module (crates-io ri ff riffy) #:use-module (crates-io))

(define-public crate-riffy-0.1.0 (c (n "riffy") (v "0.1.0") (h "1ns96d3xrp4qaah5qvl5b92igb1hlff3njxnhxprf4x45jhncw5k")))

(define-public crate-riffy-0.1.1 (c (n "riffy") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "07qgkqp8rhkbz5mqkbmss1kr3z6ilnqqx8y706qj68rl8wg9z3wf")))

