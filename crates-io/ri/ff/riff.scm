(define-module (crates-io ri ff riff) #:use-module (crates-io))

(define-public crate-riff-0.1.0 (c (n "riff") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "02c04i7w9mnhfar2w2la8zl2qmxmxfcvc5zwxfq2vgmzyni68acb")))

(define-public crate-riff-0.1.1 (c (n "riff") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "12fbiqcvyxw6s126n7rx9xr2ck740fpkk29vrxagdsghd3i0xymz")))

(define-public crate-riff-1.0.0 (c (n "riff") (v "1.0.0") (h "1rfsx1yhms2a3smglz48lh2d9vzrzgd6xx9ihm9xy9mqkrp2las5")))

(define-public crate-riff-1.0.1 (c (n "riff") (v "1.0.1") (h "18d3g5zsw3l3778xq05ib3m0hlfflnjy8awf8yiz8lvdykas7cdr")))

(define-public crate-riff-2.0.0 (c (n "riff") (v "2.0.0") (h "1ic5s604dqvm1ja9yff7rlhrr51vfk9h0ryq2x8dg2398n218q1w")))

