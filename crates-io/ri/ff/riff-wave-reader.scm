(define-module (crates-io ri ff riff-wave-reader) #:use-module (crates-io))

(define-public crate-riff-wave-reader-0.1.0 (c (n "riff-wave-reader") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xpa2f4mhn4dzsd08mg1nvhnkikryvwl8l95sy5kf5zhmhq9cpyg")))

