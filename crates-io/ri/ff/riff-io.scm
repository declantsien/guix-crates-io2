(define-module (crates-io ri ff riff-io) #:use-module (crates-io))

(define-public crate-riff-io-0.1.0 (c (n "riff-io") (v "0.1.0") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "06bylidyzw55kazviy3sdjncfj9c4s4ih7k7yrsk459cawp11zk4")))

(define-public crate-riff-io-0.1.1 (c (n "riff-io") (v "0.1.1") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0fk8wfijvf5rsh4ps3fk434m8vzbgfdajiqmpgavbkwd9f0ipacq")))

(define-public crate-riff-io-0.1.2 (c (n "riff-io") (v "0.1.2") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0q4qwyfr2sl4401b5bjj7prpga97kdjvb5mh1sws9lqwihazbj9w")))

(define-public crate-riff-io-0.2.0 (c (n "riff-io") (v "0.2.0") (d (list (d (n "memmap2") (r "^0.6") (d #t) (k 0)))) (h "0rll162y188q4sfir26y8qnx3vj81rmlbiainj1hbsn3p4nnjzrz")))

