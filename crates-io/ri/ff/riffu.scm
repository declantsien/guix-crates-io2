(define-module (crates-io ri ff riffu) #:use-module (crates-io))

(define-public crate-riffu-2.0.0 (c (n "riffu") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1pniprpd50hq477bisak4ic4ychxz8wzclfq5l0ipkr6q3722xv1")))

(define-public crate-riffu-2.1.0 (c (n "riffu") (v "2.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1k9s9wbdxijzk6466s4gvciyiivlrbai5ra9398gn07qvfr2vi6f")))

(define-public crate-riffu-2.2.0 (c (n "riffu") (v "2.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "18y95k077pwaxdzx6d43pr9jlsla0prkxli3ikb8gbnbl62c6sba")))

(define-public crate-riffu-2.3.0 (c (n "riffu") (v "2.3.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "0587vjvmnsi9cwp6bmr0m92af6rdrd806yf9h4rjwqr4121w14fc")))

(define-public crate-riffu-2.4.0 (c (n "riffu") (v "2.4.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "19zng3gb58j1f6ivl2z1pp7pnrkrnllnw4i31y93wb99l84i4s8y")))

(define-public crate-riffu-2.5.0 (c (n "riffu") (v "2.5.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1512svanrfb9pqpvvzx0i6i9vjzr4yh1yql2vvixdk38ax924n2z")))

(define-public crate-riffu-3.0.0 (c (n "riffu") (v "3.0.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "1qp33ixh2mjrrp3kavmnlsphicarf6av7vrqra6004sxr4jp491m")))

(define-public crate-riffu-4.0.0 (c (n "riffu") (v "4.0.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "0dlid7kmvmd10bixg50v6chj8wxnki0vdpxmslgs0dv4x8036iba")))

(define-public crate-riffu-4.0.1 (c (n "riffu") (v "4.0.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "09mhyb709ylzx94fz7828wbq8132j08lzhhacg809d2rq3faq84s")))

