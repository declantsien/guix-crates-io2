(define-module (crates-io ri ff riff-wave) #:use-module (crates-io))

(define-public crate-riff-wave-0.0.1 (c (n "riff-wave") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)))) (h "0hi93ack146shqxn2nhxzsmhzmf8mjydf5q0jbzzgpbflm7j8z07")))

(define-public crate-riff-wave-0.0.2 (c (n "riff-wave") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)))) (h "0jfvhj82z9hkfmxysrdm0fl2mh57s5k50v8mwnn0nn2snibcj413")))

(define-public crate-riff-wave-0.0.3 (c (n "riff-wave") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "coreaudio-rs") (r "^0.5.0") (d #t) (t "cfg(target_os = \"macos\")") (k 2)))) (h "0blyg0vc1wmddklp3vv3p0vqw0wvlm0s38k0ppbi9z54vikm40gz")))

(define-public crate-riff-wave-0.1.0 (c (n "riff-wave") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "coreaudio-rs") (r "^0.5.0") (d #t) (t "cfg(target_os = \"macos\")") (k 2)) (d (n "decibel") (r "^0.1.2") (d #t) (k 2)))) (h "149ac9vndivzhhhc2l902h45m8lnfgbac3x9kvda0mcpmpr9aaa3")))

(define-public crate-riff-wave-0.1.1 (c (n "riff-wave") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "coreaudio-rs") (r "^0.5.0") (d #t) (t "cfg(target_os = \"macos\")") (k 2)) (d (n "decibel") (r "^0.1.2") (d #t) (k 2)))) (h "1x8bi4kh5ywg55931gd6v1p42pbbribcb6lmi8c04i82zc2apwm4")))

(define-public crate-riff-wave-0.1.2 (c (n "riff-wave") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "coreaudio-rs") (r "^0.5.0") (d #t) (t "cfg(target_os = \"macos\")") (k 2)) (d (n "decibel") (r "^0.1.2") (d #t) (k 2)))) (h "0hscyp7sa94r0m2hj8ham11y5kl5n1llqyqs9sjf5myhhz0ky7bs")))

(define-public crate-riff-wave-0.1.3 (c (n "riff-wave") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "coreaudio-rs") (r "^0.5.0") (d #t) (t "cfg(target_os = \"macos\")") (k 2)) (d (n "decibel") (r "^0.1.2") (d #t) (k 2)))) (h "1w9d6pl74bls0h6s1i09qjgl7cmmzl2wrb4ml1ckckjxdcm9lx1a")))

