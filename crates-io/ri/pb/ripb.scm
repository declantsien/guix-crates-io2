(define-module (crates-io ri pb ripb) #:use-module (crates-io))

(define-public crate-ripb-0.2.0 (c (n "ripb") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)))) (h "1gdbdg53wlxcsrixs8py66nvhvqr4ws222nvx474dagag6cxrz80")))

(define-public crate-ripb-0.3.0 (c (n "ripb") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)))) (h "03i74mgjg3ldvwcr5pd3y10rrxi1wiw1yivx6khpyy392nazhhps")))

(define-public crate-ripb-0.3.1 (c (n "ripb") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)))) (h "0rsc6rdbqcxqnx8ngvp5bk67z3irm4lk2qzj1jv49db52rrxkn5s")))

