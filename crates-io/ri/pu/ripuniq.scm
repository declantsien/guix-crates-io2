(define-module (crates-io ri pu ripuniq) #:use-module (crates-io))

(define-public crate-ripuniq-1.0.0 (c (n "ripuniq") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.86") (f (quote ("std"))) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-std" "io-util" "macros" "rt" "sync"))) (d #t) (k 0)))) (h "0hms85n1cf4icq63hwbb1dbxqqqwjlx2r8g1hliki4m88gaizckv")))

(define-public crate-ripuniq-1.1.0 (c (n "ripuniq") (v "1.1.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.86") (f (quote ("std"))) (k 0)) (d (n "getargs") (r "^0.5.0") (d #t) (k 0)))) (h "07ya3qshncy19iyfi3hg41jh3b18yhb9vz3lv3mwmqi42qbzyi9q")))

