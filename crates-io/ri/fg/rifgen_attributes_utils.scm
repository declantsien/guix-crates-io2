(define-module (crates-io ri fg rifgen_attributes_utils) #:use-module (crates-io))

(define-public crate-rifgen_attributes_utils-0.1.0 (c (n "rifgen_attributes_utils") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "0frav6mwwm0b4vglhdzm3zl5z9ab19nl9v3p4bnj837vdjrwnp1i")))

