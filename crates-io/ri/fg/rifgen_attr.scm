(define-module (crates-io ri fg rifgen_attr) #:use-module (crates-io))

(define-public crate-rifgen_attr-0.1.0 (c (n "rifgen_attr") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "0whjwxr68l49qyiszxhrf84pzxkzp9c3a0c7nbfr8v5rmph0bpyk")))

