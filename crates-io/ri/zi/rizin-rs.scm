(define-module (crates-io ri zi rizin-rs) #:use-module (crates-io))

(define-public crate-rizin-rs-0.1.0 (c (n "rizin-rs") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "1wa9a4j7jg8mnada2wva3y3wq6h5wpp34f3pryxpi5qrmmcygdb4") (y #t)))

(define-public crate-rizin-rs-0.1.1 (c (n "rizin-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "1k3c33afgfb10mya2ykz6v7bwxhp2vkfm5ksb8n4khgnp2xkx21y")))

(define-public crate-rizin-rs-0.1.2 (c (n "rizin-rs") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "0l01hvs7rgih3gjc997ij70iidfpg5rwzgwdmg5mnllv3i8j7ddy")))

