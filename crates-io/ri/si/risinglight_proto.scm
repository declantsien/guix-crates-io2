(define-module (crates-io ri si risinglight_proto) #:use-module (crates-io))

(define-public crate-risinglight_proto-0.1.1 (c (n "risinglight_proto") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)))) (h "1y580xng9c6yhgw1z5am819n5f59l4vnsjyhbf0mnj0y2fv7hnm8")))

(define-public crate-risinglight_proto-0.1.3 (c (n "risinglight_proto") (v "0.1.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)))) (h "0ds3pxd0idg949ks3xz06b8rwzm154h0l5d13y3i559h398ylplc")))

(define-public crate-risinglight_proto-0.2.0 (c (n "risinglight_proto") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.0") (d #t) (k 1)))) (h "0340w8w8x1rlz2fa4xmgh1aic1qfkf6n2liy2dqa7hr03135sza1")))

