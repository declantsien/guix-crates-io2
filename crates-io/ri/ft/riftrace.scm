(define-module (crates-io ri ft riftrace) #:use-module (crates-io))

(define-public crate-riftrace-0.1.0 (c (n "riftrace") (v "0.1.0") (h "1hi8yvyzpskc5b9s9s6lj8yzshxz5hhpwwkmhk02kapyk9g29ygm") (y #t)))

(define-public crate-riftrace-0.1.1 (c (n "riftrace") (v "0.1.1") (h "18wyg4x7f22d7wrgq43ddyq5k5q9arp6p2z5n5c66lmr989z9lrj")))

