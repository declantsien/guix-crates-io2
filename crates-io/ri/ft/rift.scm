(define-module (crates-io ri ft rift) #:use-module (crates-io))

(define-public crate-rift-0.1.0 (c (n "rift") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "try_from") (r "^0.2.0") (d #t) (k 0)))) (h "0919q9a9v4khadgz6hb8199z1hscjd4m3d1mvx84nrwywm9zcbhx")))

(define-public crate-rift-0.2.0 (c (n "rift") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "try_from") (r "^0.2.0") (d #t) (k 0)))) (h "0gki3j4pzna1daa4ly7y5wi2a69xnmh71y3asrgrdw50y6f2wd2r")))

(define-public crate-rift-0.4.0 (c (n "rift") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "try_from") (r "^0.2.0") (d #t) (k 0)))) (h "01bniabp2zmbsfvyi9b686r0mzrsqvmc88nna4x7sm45iqh95gcr")))

(define-public crate-rift-0.5.0 (c (n "rift") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "try_from") (r "^0.2.0") (d #t) (k 0)))) (h "1q8l2zd99cchhy7fnjlbjmq1ppgid9hsw8mfz01iqfcrjyw0i1yn")))

(define-public crate-rift-0.5.1 (c (n "rift") (v "0.5.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "try_from") (r "^0.2.0") (d #t) (k 0)))) (h "1a7wii01frxc2yd94xi5npp2nyc4clif0wkwjkmj7yslnpbwnpxq")))

