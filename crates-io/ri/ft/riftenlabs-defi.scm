(define-module (crates-io ri ft riftenlabs-defi) #:use-module (crates-io))

(define-public crate-riftenlabs-defi-0.1.0 (c (n "riftenlabs-defi") (v "0.1.0") (d (list (d (n "actual-serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0) (p "serde")) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.11.0") (k 0)) (d (n "bitcoincash") (r "^0.29") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1ihwwnbkfxi3mywzxl79f40z343il7v4j0g8pfby027qhd0i0baq") (f (quote (("serde" "actual-serde" "bitcoin_hashes/serde" "bitcoincash/serde"))))))

(define-public crate-riftenlabs-defi-0.1.2 (c (n "riftenlabs-defi") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.11.0") (f (quote ("serde"))) (k 0)) (d (n "bitcoincash") (r "^0.29") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1nq23mxm9ncaf2fgh2yhq36kwbas489ds4qkfbczpz9l26a3vpcl")))

