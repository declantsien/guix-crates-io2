(define-module (crates-io ri ft rift_insight) #:use-module (crates-io))

(define-public crate-rift_insight-0.0.1 (c (n "rift_insight") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16n27jmapziwfnrrnjmnan156pmzxld3sja288hkmrgs9b9305y5")))

(define-public crate-rift_insight-0.0.2 (c (n "rift_insight") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07x906klvmajsqpxmvmhszfy2mzh0is5fql7vlix1khv6m2ga49a")))

(define-public crate-rift_insight-0.0.3 (c (n "rift_insight") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19zd45nlcs4sdmnvxgv4blra0mk1fxcbgn7w9xyghz77f5kpzyhc")))

(define-public crate-rift_insight-0.0.4 (c (n "rift_insight") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07xr551fhyn3rsplndsr7n9rm9hx3qya1h7w09aam848nziqxiv8")))

(define-public crate-rift_insight-0.0.5 (c (n "rift_insight") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nzfrh0b9dmgyzxpfw116rkwr1g26z0vqqhzkghd5v7hvksvd37n")))

(define-public crate-rift_insight-0.0.6 (c (n "rift_insight") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jd1p4bdb5fyymvjwhqpg5wb8f6vb1l4s6qnrblcdd3qdi37i7lf")))

(define-public crate-rift_insight-0.0.7 (c (n "rift_insight") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18m87qm5lh7iazc9ysbf4mc97a6gr62qdsmf5h2mdxl9ma7pfl9x")))

