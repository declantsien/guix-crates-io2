(define-module (crates-io ri go rigolds1000z) #:use-module (crates-io))

(define-public crate-rigolds1000z-0.0.1 (c (n "rigolds1000z") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qca6qjc9kfrqq8k1f5zbxnl5fg40a51yxzx17fc7gisnpd7yh71")))

(define-public crate-rigolds1000z-0.0.2 (c (n "rigolds1000z") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0wbx1kmjgwdarv5wfp97g8glm3j5lqgpbq6qzblh2c5f8yw6b7fp")))

