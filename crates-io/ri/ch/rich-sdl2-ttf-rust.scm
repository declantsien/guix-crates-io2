(define-module (crates-io ri ch rich-sdl2-ttf-rust) #:use-module (crates-io))

(define-public crate-rich-sdl2-ttf-rust-0.1.0 (c (n "rich-sdl2-ttf-rust") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.4.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "15narzrv07apz71mylpsmca3lq02amihphfhdnkqlc6paxymwwgm") (y #t)))

(define-public crate-rich-sdl2-ttf-rust-0.1.1 (c (n "rich-sdl2-ttf-rust") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.4.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "079808php20g5x51b9lhjvj06h2zbwnwj5jnsvbn0rkr25cszqfh") (y #t)))

(define-public crate-rich-sdl2-ttf-rust-0.1.2 (c (n "rich-sdl2-ttf-rust") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.4.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1gxnflnjlwkkpc1qrjwpn78ipcald3a7qysk14fsv589zabmzg5r") (y #t)))

(define-public crate-rich-sdl2-ttf-rust-0.2.0 (c (n "rich-sdl2-ttf-rust") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.5.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1381yq9lmzdkbkb3q270ma96mrhn5z667y8pxald965a4p6ax0l4") (y #t)))

(define-public crate-rich-sdl2-ttf-rust-0.3.0 (c (n "rich-sdl2-ttf-rust") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1i2amjhfz3j3w2cxp8mf56pfidsld0flv59y9az92253yrkhhdhl") (y #t)))

(define-public crate-rich-sdl2-ttf-rust-0.3.1 (c (n "rich-sdl2-ttf-rust") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "07hfrl7lmgpkhmk0zy2692dfw192pxqrqb1h02yacnll23kspzwq") (y #t)))

(define-public crate-rich-sdl2-ttf-rust-0.4.0 (c (n "rich-sdl2-ttf-rust") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.7.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "08sdka180rb7xcdv8dj0lbvyvbk5jddnn5qiw327xfbp9b3zibyn") (y #t)))

(define-public crate-rich-sdl2-ttf-rust-0.5.0 (c (n "rich-sdl2-ttf-rust") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.7.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0cla9fjfdzl21jlly7vh7hvx3hkkybx1q0bf9apk4k3warcsxk0x") (y #t)))

(define-public crate-rich-sdl2-ttf-rust-0.6.0 (c (n "rich-sdl2-ttf-rust") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.8.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1r043danfi36s9168cf80mbwgb6a1wi9yyhzhziwynl2p352bb8i") (y #t)))

(define-public crate-rich-sdl2-ttf-rust-0.7.0 (c (n "rich-sdl2-ttf-rust") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.9.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0jcl7ac3zs0frjrbbas0yn9nlkw8ha8r6pg7clw52vv6ifrifq62") (y #t)))

(define-public crate-rich-sdl2-ttf-rust-0.8.0 (c (n "rich-sdl2-ttf-rust") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.10.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0d3ypfw30k2lqzmgfvx628m8n7rrkd6518kh28spcravw8g8cn55") (y #t) (r "1.56")))

