(define-module (crates-io ri ch richterm) #:use-module (crates-io))

(define-public crate-richterm-0.1.0 (c (n "richterm") (v "0.1.0") (h "1x43d4d33dxscgb3pg0c73bc0f9ir6xskya9lzpjvpmdkq947sap")))

(define-public crate-richterm-0.1.1 (c (n "richterm") (v "0.1.1") (h "1cyxr0msrzirhvpkcvaf2gb886plw1l4bxacayf03chxhkfxw6l9")))

(define-public crate-richterm-0.3.0 (c (n "richterm") (v "0.3.0") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "1iip6nlyd0ilaqpayqg9g1y3jp7dlr4facwfjayvg1qcldy8n0jr")))

(define-public crate-richterm-0.4.0 (c (n "richterm") (v "0.4.0") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "1xgcrvq9d15jd0q5a94wqik1wcy13mwwy4yssdjy78fa5cr1fapp")))

(define-public crate-richterm-0.5.0 (c (n "richterm") (v "0.5.0") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "0q96nclas9hbkh2wm2yzcblp3pb8358fyy80dmfnpdhqdhx1jqns")))

