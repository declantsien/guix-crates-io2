(define-module (crates-io ri ch rich-strong-server) #:use-module (crates-io))

(define-public crate-rich-strong-server-0.1.0 (c (n "rich-strong-server") (v "0.1.0") (d (list (d (n "rich-strong-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0inbiz1b0hxpbsn6xhxai1g29syd20m1nzbzq86vkqi71s1fxlxz")))

(define-public crate-rich-strong-server-0.1.2 (c (n "rich-strong-server") (v "0.1.2") (d (list (d (n "rich-strong-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0jzlq7w2iq4ry9cx2p4l54wmbbldww8fd7pcvzw7dgzzi6dykh04")))

