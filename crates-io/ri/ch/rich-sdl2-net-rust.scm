(define-module (crates-io ri ch rich-sdl2-net-rust) #:use-module (crates-io))

(define-public crate-rich-sdl2-net-rust-0.1.0 (c (n "rich-sdl2-net-rust") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.6.4") (d #t) (k 0)))) (h "1bkl9dfh1gvxms9fwf0sg9svsl2560jnykgswcnwxm6nqs4q2ay3")))

(define-public crate-rich-sdl2-net-rust-0.2.0 (c (n "rich-sdl2-net-rust") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.7.0") (d #t) (k 0)))) (h "0jmmxyk1k9y521fvxdsl5xvc87ab56rkcgx9bfzl60igj1j5c458")))

(define-public crate-rich-sdl2-net-rust-0.3.0 (c (n "rich-sdl2-net-rust") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.8.1") (d #t) (k 0)))) (h "1nwb4z1kp28dvgj4gi8m8b0l5jw8g8yf0j2w2dnv76yd7cj4wni9")))

(define-public crate-rich-sdl2-net-rust-0.4.0 (c (n "rich-sdl2-net-rust") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.9.0") (d #t) (k 0)))) (h "1qw5z5m8agakakhjj82j51a287pz53bni9rmdh0gmdqvj3paxlbs")))

(define-public crate-rich-sdl2-net-rust-0.5.0 (c (n "rich-sdl2-net-rust") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.10.0") (d #t) (k 0)))) (h "0f2ifi29sszp2asl8bssa3384vy5nvwb525fhwl7q8jifm436d7j") (r "1.56")))

