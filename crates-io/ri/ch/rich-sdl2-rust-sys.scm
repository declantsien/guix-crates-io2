(define-module (crates-io ri ch rich-sdl2-rust-sys) #:use-module (crates-io))

(define-public crate-rich-sdl2-rust-sys-0.1.0 (c (n "rich-sdl2-rust-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.22") (d #t) (k 1)) (d (n "git2") (r "^0.13.23") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.37") (d #t) (k 1)) (d (n "zip") (r "^0.5.13") (d #t) (k 1)))) (h "188y7fcgaraxhil7h4h6qq09qg8nzv3gj1knkrxzc96iw1jx18yr")))

(define-public crate-rich-sdl2-rust-sys-0.2.0 (c (n "rich-sdl2-rust-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "1mfcf7n849vgya23sy5l7f91sddp55d3f86gkjka2fsg0l1mxw5s")))

(define-public crate-rich-sdl2-rust-sys-0.2.1 (c (n "rich-sdl2-rust-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "1viknqpda4jjpz24rld7shi88nzn1xd6cgjpra2qh8vvr4dzljf1") (f (quote (("static") ("dynamic")))) (l "SDL2")))

(define-public crate-rich-sdl2-rust-sys-0.2.2 (c (n "rich-sdl2-rust-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.14.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "0pflnhpdqk3xzx6amcic8db1mxvf8hj8baz1xr442fw8ikpwg4bf") (f (quote (("vendor") ("static") ("dynamic") ("default" "dynamic")))) (l "SDL2")))

(define-public crate-rich-sdl2-rust-sys-0.2.3 (c (n "rich-sdl2-rust-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.14.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1ym457g2kp02nayxm9bz40l86qvigx4zyy4sz72n4x7yw672aw8b") (f (quote (("vendor") ("ttf") ("static") ("dynamic") ("default" "dynamic")))) (l "SDL2")))

(define-public crate-rich-sdl2-rust-sys-0.2.4 (c (n "rich-sdl2-rust-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.14.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)) (d (n "retry") (r "^1.3.1") (d #t) (k 1)))) (h "12kwand5g7lp1406rpdb93mbidgj0h1g63ld9hbxx5nl8nhxars7") (f (quote (("vendor") ("ttf") ("static") ("mixer") ("dynamic") ("default" "dynamic")))) (l "SDL2")))

(define-public crate-rich-sdl2-rust-sys-0.2.5 (c (n "rich-sdl2-rust-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "git2") (r "^0.14.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)) (d (n "retry") (r "^1.3.1") (d #t) (k 1)))) (h "1nvpvf3fdaq3wrp3l9jl07skv74043c1m7vrrilwrm99lry8sibf") (f (quote (("vendor") ("ttf") ("static") ("mixer") ("dynamic") ("default" "dynamic")))) (l "SDL2")))

(define-public crate-rich-sdl2-rust-sys-0.2.6 (c (n "rich-sdl2-rust-sys") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "git2") (r "^0.16.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)) (d (n "retry") (r "^2.0.0") (d #t) (k 1)))) (h "01vidpw280713lc6qbz3b1a61c63mxy8zciz8r78pnvf4qvfg01y") (f (quote (("vendor") ("ttf") ("static") ("mixer") ("dynamic") ("default" "dynamic")))) (l "SDL2")))

(define-public crate-rich-sdl2-rust-sys-0.2.7 (c (n "rich-sdl2-rust-sys") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "git2") (r "^0.16.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)) (d (n "retry") (r "^2.0.0") (d #t) (k 1)))) (h "0qa8pva6sbsbznkws60ryr0y0p7hfilmh2hjq0kfahacx7n0m4am") (f (quote (("vendor") ("ttf") ("static") ("net") ("mixer") ("image") ("dynamic") ("default" "dynamic")))) (l "SDL2")))

(define-public crate-rich-sdl2-rust-sys-0.13.1 (c (n "rich-sdl2-rust-sys") (v "0.13.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "git2") (r "^0.18.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "retry") (r "^2.0.0") (d #t) (k 1)))) (h "1k7kzs7x67pn41jfqjs290vr4zgvr1fjbmwdxvpw2mkkh5bk3ydy") (f (quote (("vendor") ("ttf") ("static") ("net") ("mixer") ("image") ("dynamic") ("default" "dynamic")))) (l "SDL2")))

(define-public crate-rich-sdl2-rust-sys-0.13.2 (c (n "rich-sdl2-rust-sys") (v "0.13.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "git2") (r "^0.18.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)) (d (n "retry") (r "^2.0.0") (d #t) (k 1)))) (h "0yf5kzikxrkwp4cjkx12qngppw7xzqn1rhr40zaskd8ynl3w5cih") (f (quote (("vendor") ("ttf") ("static") ("net") ("mixer") ("image") ("dynamic") ("default" "dynamic")))) (l "SDL2")))

