(define-module (crates-io ri ch richard-test-c1) #:use-module (crates-io))

(define-public crate-richard-test-c1-0.1.12 (c (n "richard-test-c1") (v "0.1.12") (h "1b4pi62mwjvj47pw9hxl109w4hms8pi1a91sczq4hgqh3k9fi5sq")))

(define-public crate-richard-test-c1-0.1.13 (c (n "richard-test-c1") (v "0.1.13") (h "0x4zm245wr7qr845cwac8fw73j1n3a5gvns49rsvdl35wlalfaw6")))

