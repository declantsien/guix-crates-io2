(define-module (crates-io ri ch rich-result) #:use-module (crates-io))

(define-public crate-rich-result-0.1.0 (c (n "rich-result") (v "0.1.0") (h "0lryj7kqip96rs8m8054bkappg00w2imkrdwqnv2xx3dx1k3nv52") (f (quote (("std"))))))

(define-public crate-rich-result-0.1.1 (c (n "rich-result") (v "0.1.1") (h "12v95qqsqy9m7qc6b5r8n63yisx97iffvvf5lzd0444468b74jcz") (f (quote (("std")))) (y #t)))

(define-public crate-rich-result-0.1.2 (c (n "rich-result") (v "0.1.2") (h "1h9r8g0gl1jinv154xd9ycrw0l6rfz2kxg50hlfqs20ccqf9pbm5") (f (quote (("std"))))))

