(define-module (crates-io ri ch richard-test) #:use-module (crates-io))

(define-public crate-richard-test-0.1.0 (c (n "richard-test") (v "0.1.0") (h "05vpa4f6hqpzzlx3268jgjg4m4788pr0x3d7mwyzwkarnzkdpkpr")))

(define-public crate-richard-test-0.1.1 (c (n "richard-test") (v "0.1.1") (h "0sv66afz36az7j7pvhh17kvygai4p7pybl49j6falyapw3n8s3i7")))

(define-public crate-richard-test-0.1.9 (c (n "richard-test") (v "0.1.9") (h "1ib13x96n7czrv2jyy9gj23iv53mlazf5pn492ldrhf2q751icfj")))

(define-public crate-richard-test-0.1.10 (c (n "richard-test") (v "0.1.10") (h "0awwdadgj0aa1q11n8b70hlvnkfqws3lqy7c85cf73vp31z11wrh")))

(define-public crate-richard-test-0.1.11 (c (n "richard-test") (v "0.1.11") (h "0wvi6rzn8ilpxvlgzqwmm8j4cwby0j173xynkn5ykbkz1l0yaadc")))

(define-public crate-richard-test-0.1.12 (c (n "richard-test") (v "0.1.12") (h "0pk5gyz8322i9zbj3hvn3n98my3zr9q00dj0nkvvvwd55xyrgj88")))

(define-public crate-richard-test-0.1.13 (c (n "richard-test") (v "0.1.13") (h "1l3xscbdzkwwqwidmx7y1l761vm7azwwdbs4653ykghg1qimrpz9")))

