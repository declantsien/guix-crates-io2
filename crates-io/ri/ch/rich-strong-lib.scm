(define-module (crates-io ri ch rich-strong-lib) #:use-module (crates-io))

(define-public crate-rich-strong-lib-0.1.0 (c (n "rich-strong-lib") (v "0.1.0") (d (list (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "1qnh0jhyv88v3cdxmhsvg82ss2ly12pr4p20s079j8nn19sfdgr9")))

(define-public crate-rich-strong-lib-0.1.1 (c (n "rich-strong-lib") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "configparser") (r "^3.0.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "0lcj73gd1iqg9vkwpx2krwncmfp7aqpi1bljjqikn3g968q62b0z")))

(define-public crate-rich-strong-lib-0.1.2 (c (n "rich-strong-lib") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "configparser") (r "^3.0.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "1hnc7jkzq39mgx63dly0dnv5kbvshhxc1impn1fhdz90bwswprg9")))

