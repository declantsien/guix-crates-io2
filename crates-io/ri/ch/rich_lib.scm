(define-module (crates-io ri ch rich_lib) #:use-module (crates-io))

(define-public crate-rich_lib-0.0.0 (c (n "rich_lib") (v "0.0.0") (h "1p7ji15v4cv7x7gw3rfdlvf927m2bk9xaz7vbgfyxd8ydj8kwn2l")))

(define-public crate-rich_lib-0.1.0 (c (n "rich_lib") (v "0.1.0") (h "1phidfmf066zyvkhlvpbnv6qrd4i7i5xxlq67wyc59abdc5ayz2c")))

(define-public crate-rich_lib-0.1.1 (c (n "rich_lib") (v "0.1.1") (h "1acawlkv16rg7w1rfsgas3655zqb8mq41mnzfw8k3j9khg43dgnz")))

