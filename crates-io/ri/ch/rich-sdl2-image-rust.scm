(define-module (crates-io ri ch rich-sdl2-image-rust) #:use-module (crates-io))

(define-public crate-rich-sdl2-image-rust-0.1.0 (c (n "rich-sdl2-image-rust") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.6.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "166k35qx0xs9kljjaz06vgvn4qh7hqwl2yv4m2dqxpn2gks0082h")))

(define-public crate-rich-sdl2-image-rust-0.2.0 (c (n "rich-sdl2-image-rust") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.7.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "14v87nnya13l43k0l1p4mwl99v3zdaf4j5dmbwxmm3vd7kvmf4jz")))

(define-public crate-rich-sdl2-image-rust-0.3.0 (c (n "rich-sdl2-image-rust") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.8.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "08x33gqdppz9zx7c2yg24r9jm42y8mgin0zq4aw3bbajn23rnddm")))

(define-public crate-rich-sdl2-image-rust-0.4.0 (c (n "rich-sdl2-image-rust") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.9.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "08jvrpni37mm8kz8hvk1ciswp3bq6nvqhjjgcx0z5frvzacfc6mr")))

(define-public crate-rich-sdl2-image-rust-0.5.0 (c (n "rich-sdl2-image-rust") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.21") (d #t) (k 1)) (d (n "rich-sdl2-rust") (r "^0.10.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0g742ciwahdhv4xhagpmgwx1myf379gsnxf2knnbcqccribbl00y") (r "1.56")))

