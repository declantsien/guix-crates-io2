(define-module (crates-io ri pe ripemd320) #:use-module (crates-io))

(define-public crate-ripemd320-0.8.0-rc1 (c (n "ripemd320") (v "0.8.0-rc1") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0xw5gvjw4bbb0yk9dgmfhyybas44q3mlpgbiwqiymw3rp5dix53h") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd320-0.8.0 (c (n "ripemd320") (v "0.8.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "12r0iqbabn2hf3d0bckb09w9i9rx0nn0wjyilbh8bdld0xbz678d") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd320-0.9.0 (c (n "ripemd320") (v "0.9.0") (d (list (d (n "block-buffer") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "14a1jms00p8m99xkqamh2lgh956xdvqipg8zd5w216zr4x1gpyag") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd320-0.10.0 (c (n "ripemd320") (v "0.10.0") (h "0v7ckdf6kbavxflsisd5bkh6w6i5jahm6b13s4y4s9dcb6lsw9i4")))

