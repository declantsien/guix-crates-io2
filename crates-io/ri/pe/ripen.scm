(define-module (crates-io ri pe ripen) #:use-module (crates-io))

(define-public crate-ripen-0.1.0 (c (n "ripen") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.20.1") (d #t) (k 0)))) (h "1xmlvqjn9mw1f8d6hqqxfzyn1ajxbd57b6pjxd2fj7lh1mqphv3m")))

