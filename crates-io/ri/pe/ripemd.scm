(define-module (crates-io ri pe ripemd) #:use-module (crates-io))

(define-public crate-ripemd-0.0.0 (c (n "ripemd") (v "0.0.0") (h "05x3fnwy277qbs3h0hla5bwid9x2q9m9fzyvjp6kpp7rih85a2gh")))

(define-public crate-ripemd-0.1.0 (c (n "ripemd") (v "0.1.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "16ap7vvnqf4l6vy7kjpb5p8brqfppanssm61jn1w8444395bcm7a") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd-0.1.1 (c (n "ripemd") (v "0.1.1") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "1lrfn968wi03846pw1nhk8jx11hh150af7shaf2w03ayrdafrb0z") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd-0.1.2 (c (n "ripemd") (v "0.1.2") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "0h584qn8c48w820w0z4hp5p03hp388hm6v8sk4knagvn9r3fxqkl") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd-0.1.3 (c (n "ripemd") (v "0.1.3") (d (list (d (n "digest") (r "^0.10.4") (d #t) (k 0)) (d (n "digest") (r "^0.10.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "17xh5yl9wjjj2v18rh3m8ajlmdjg1yj13l6r9rj3mnbss4i444mx") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std"))))))

