(define-module (crates-io ri pe ripemd256) #:use-module (crates-io))

(define-public crate-ripemd256-0.1.0 (c (n "ripemd256") (v "0.1.0") (d (list (d (n "block-buffer") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0bj9bcdrqw5m01c4kxyn0v9pxrlvjrv822gqrb3ps7xyw1nvf6s9") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd256-0.2.0 (c (n "ripemd256") (v "0.2.0") (h "14fph0k6hyga6flwmjx9blfixjj82d7lrwx69q1qw80ggcigb13k")))

