(define-module (crates-io ri pe ripeg) #:use-module (crates-io))

(define-public crate-ripeg-0.1.0 (c (n "ripeg") (v "0.1.0") (h "1l8c2ha18ca0yci1x8gx9f8hrccbk3nmxk7ahqdy9335plm7xb0d")))

(define-public crate-ripeg-0.1.1 (c (n "ripeg") (v "0.1.1") (h "0ng7mflalc7ckq1z0pwwxh58qwvh40snmlh9bfwjqryxirr1wd97")))

(define-public crate-ripeg-0.1.2 (c (n "ripeg") (v "0.1.2") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)))) (h "1k01wng33z3fa9i72lbj450dad7bcjxcxlpp4fiwc9m3qp3pg1v1")))

(define-public crate-ripeg-0.1.3 (c (n "ripeg") (v "0.1.3") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)))) (h "0czgnyqmfjiiwa0j5ji623cmvl47nj24q14dmvlyww1xw9kvg38i")))

