(define-module (crates-io ri pe ripemd128) #:use-module (crates-io))

(define-public crate-ripemd128-0.1.0 (c (n "ripemd128") (v "0.1.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "06zal1ar9bg5lghan7rvznr3bqlbici30irw3j1mjzp14z86aq3l") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd128-0.1.1 (c (n "ripemd128") (v "0.1.1") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "12s8lyzicl8nc177f5yqnk98dpsxi93n8bhr3sdgzdmckb1kc644") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd128-0.1.2 (c (n "ripemd128") (v "0.1.2") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0d0192m3zg9pidfq8qsz17rdwi7hr9jlciqpglw2lgxc92d373kd") (f (quote (("std" "digest/std") ("default" "std"))))))

