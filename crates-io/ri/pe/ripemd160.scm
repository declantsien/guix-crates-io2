(define-module (crates-io ri pe ripemd160) #:use-module (crates-io))

(define-public crate-ripemd160-0.1.0 (c (n "ripemd160") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.1") (d #t) (k 2)) (d (n "digest") (r "^0.1") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "07q8ifzbhfn540v14794mkwh5ii8bpqq9yrhs4q8clc4znb378j5")))

(define-public crate-ripemd160-0.2.0 (c (n "ripemd160") (v "0.2.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.1") (d #t) (k 2)) (d (n "digest") (r "^0.2") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "1zfrmr0wwxvsl7ba8alq226b3qp6pnwcdy16a5mnxa36h27nlpc4")))

(define-public crate-ripemd160-0.3.0 (c (n "ripemd160") (v "0.3.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2") (d #t) (k 2)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "1c6sdnrm11nhf9ffszfqi5kgalrg7xj71lp06zh929pyx1qj7kvc")))

(define-public crate-ripemd160-0.4.0 (c (n "ripemd160") (v "0.4.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "0x7n3slvlzs1h2ja7ifbmhxyam8mjlrkg345aa83h14ppkl2h646")))

(define-public crate-ripemd160-0.4.1 (c (n "ripemd160") (v "0.4.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "01wvj4dw2ai918pp8vmn5ign2kqlkwnjn9x00jk9drh5hffgvxhy")))

(define-public crate-ripemd160-0.5.0 (c (n "ripemd160") (v "0.5.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4.0-alpha") (d #t) (k 2)) (d (n "digest") (r "^0.5.0-rc1") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1km7kni4fk7sx1nyk9sh4ri9wbsjhsj4c302sp4kd8aks7z6j6xh")))

(define-public crate-ripemd160-0.5.1 (c (n "ripemd160") (v "0.5.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1f94jg8wcm4xr3g3jjmqrpi2dh3l6ibwhjn00296xcrbh0a6hll8")))

(define-public crate-ripemd160-0.5.2 (c (n "ripemd160") (v "0.5.2") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1swy9fsd9xr534p8kp5q14lfr1q8qj3czb5pb7sclqh7dfafkxb2")))

(define-public crate-ripemd160-0.6.0 (c (n "ripemd160") (v "0.6.0") (d (list (d (n "block-buffer") (r "^0.2") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "1s87iwbm6a64rbq268r8l3gb3khbii7a07pka1fyr2i1aiypfix3")))

(define-public crate-ripemd160-0.7.0 (c (n "ripemd160") (v "0.7.0") (d (list (d (n "block-buffer") (r "^0.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "1yw353179wj1az579cdysfx8pnj7q9r5mk7imzdcrblaqrnaaaj8")))

(define-public crate-ripemd160-0.8.0 (c (n "ripemd160") (v "0.8.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0jns4vb7p06pxxdrrqinwcn03qrm092l4v65zdxmg1xvvgh14ldd") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd160-0.9.0 (c (n "ripemd160") (v "0.9.0") (d (list (d (n "block-buffer") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0i9vlcv256igpj8vgza5f4srpgqz6p2a93nhxm9iz1lfyw7y0dvh") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd160-0.9.1 (c (n "ripemd160") (v "0.9.1") (d (list (d (n "block-buffer") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0lf2vji7dgf80f5mh6dcdlmdl1qa80jffg7pkcc17wxph764xjif") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-ripemd160-0.10.0 (c (n "ripemd160") (v "0.10.0") (h "0kxyf4h4fn9idn0vblj6jiid20y0hsn9pg9ph21z4536gqd408zr")))

