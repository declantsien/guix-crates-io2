(define-module (crates-io ri nc rincon) #:use-module (crates-io))

(define-public crate-rincon-0.1.0 (c (n "rincon") (v "0.1.0") (d (list (d (n "rincon_connector") (r "^0.1") (d #t) (k 2)) (d (n "rincon_core") (r "^0.1") (d #t) (k 2)) (d (n "rincon_session") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0g5dip1a5df2bfg4rygc1s2y18pap4g9vmagysk6ihvyl0fy3z26")))

