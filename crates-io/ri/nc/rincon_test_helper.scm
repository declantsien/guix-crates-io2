(define-module (crates-io ri nc rincon_test_helper) #:use-module (crates-io))

(define-public crate-rincon_test_helper-0.1.0 (c (n "rincon_test_helper") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.11") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "rincon_client") (r ">= 0.1, < 0.2") (d #t) (k 0)) (d (n "rincon_connector") (r "^0.1") (d #t) (k 0)) (d (n "rincon_core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "03jsd127a23m27dy5d0fpgaajkpfcamfs0yyi3g1bsrwav39zqfs")))

