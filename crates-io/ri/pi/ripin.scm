(define-module (crates-io ri pi ripin) #:use-module (crates-io))

(define-public crate-ripin-0.1.0 (c (n "ripin") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1wi56pmy5bfil1hldz53qykizym322sclvjfw62l2rm2p83rland")))

(define-public crate-ripin-0.1.1 (c (n "ripin") (v "0.1.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "04advm3dcqpckznpygjrg90v2b97dav5gcdmp1yimvi99c5dwj23")))

(define-public crate-ripin-0.1.2 (c (n "ripin") (v "0.1.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1jp5a64605y7s4fisrc9fnxbm9n7r27k6ljv5hjjlkbxblry7bvm")))

