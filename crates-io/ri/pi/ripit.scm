(define-module (crates-io ri pi ripit) #:use-module (crates-io))

(define-public crate-ripit-1.0.0 (c (n "ripit") (v "1.0.0") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1v4m0qg2df2d3iy5fpk1zbchfcqwq6nqqdcnkcw2z68xl06pfrlx")))

