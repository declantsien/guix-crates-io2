(define-module (crates-io ri tz ritz_impl) #:use-module (crates-io))

(define-public crate-ritz_impl-0.1.0 (c (n "ritz_impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "snax") (r "^0.2.0") (d #t) (k 0)))) (h "0ysx9nr19r00jx3pzbd5pyrn1f9qcd7qqjq9h3jsfk797hablyzs")))

