(define-module (crates-io ri tz ritz) #:use-module (crates-io))

(define-public crate-ritz-0.1.0 (c (n "ritz") (v "0.1.0") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro-nested") (r "^0.1.2") (d #t) (k 0)) (d (n "ritz_impl") (r "^0.1.0") (d #t) (k 0)))) (h "0bg5px7y64bv5n5nm85h7vqbaii1avrk57c14k6xl244l9wj9dg7")))

