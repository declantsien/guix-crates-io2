(define-module (crates-io ri gl riglet) #:use-module (crates-io))

(define-public crate-riglet-0.1.0 (c (n "riglet") (v "0.1.0") (h "1s3nadz8703zf5n8r817zdk1x23wv4pbr14g4qnpzz7l8ycxm6zl") (y #t)))

(define-public crate-riglet-0.1.1 (c (n "riglet") (v "0.1.1") (h "1w2c0lfgk8qzvi9bbzgbhs1ajj0wzfxbr3jmkzsz0snk0cyqynha") (y #t)))

(define-public crate-riglet-0.1.2 (c (n "riglet") (v "0.1.2") (h "0x80b048s57yrl6y7wnrj6bvxyd09rvcyipgkk39kc6h9gx0kp8x")))

(define-public crate-riglet-0.1.3 (c (n "riglet") (v "0.1.3") (h "0cfdh3z69r82magksb8m77v7da797mf240mz9lv3nykdgk7jr1s1")))

