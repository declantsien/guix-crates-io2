(define-module (crates-io ri mi rimilo) #:use-module (crates-io))

(define-public crate-rimilo-0.1.0 (c (n "rimilo") (v "0.1.0") (d (list (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 1)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "uneval") (r "^0.2") (d #t) (k 1)))) (h "1hbjl5frgf1i2p5nr1f958gyanz0mkaq75yj8pi86hcdxm5xrlyz")))

(define-public crate-rimilo-0.2.0 (c (n "rimilo") (v "0.2.0") (d (list (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 1)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0pwyjcx1a8ylkxzh99iakx3ycfryl3cg9plvfppfjr2n8sn0kmh8")))

(define-public crate-rimilo-0.2.1 (c (n "rimilo") (v "0.2.1") (d (list (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1bzl0w9gq58fcrm46l3kig63bf4v9h5hdlhif7y5msgpgwlap9s4")))

(define-public crate-rimilo-0.2.2 (c (n "rimilo") (v "0.2.2") (d (list (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "051maiv7p7kda0c2hnhq1iraiazan1mwxyk921la5fhj8ix9bf60")))

