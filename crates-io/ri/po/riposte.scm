(define-module (crates-io ri po riposte) #:use-module (crates-io))

(define-public crate-riposte-0.1.0 (c (n "riposte") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0-rc.2") (d #t) (k 0)) (d (n "hyper") (r "^1.0.0-rc.3") (f (quote ("server" "http1"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "net"))) (d #t) (k 0)))) (h "11x3n3c9pmhbixrvy4n98p0rriqp8iqj1zzjw2s2m3rpqfwsfdna")))

(define-public crate-riposte-0.1.1 (c (n "riposte") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0-rc.2") (d #t) (k 0)) (d (n "hyper") (r "^1.0.0-rc.3") (f (quote ("server" "http1"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "net"))) (d #t) (k 0)))) (h "0ckicn4ks02jichi0hpl82ls5q64ds2jcbhb3xxi734d4nw4sysr")))

(define-public crate-riposte-0.1.2 (c (n "riposte") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0-rc.2") (d #t) (k 0)) (d (n "hyper") (r "^1.0.0-rc.3") (f (quote ("server" "http1"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "net"))) (d #t) (k 0)))) (h "0lzs64qydf4x8ayxxmrp6a74hxjaw2grd4bvsfdh0vjp43cxv488")))

