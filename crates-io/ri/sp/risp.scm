(define-module (crates-io ri sp risp) #:use-module (crates-io))

(define-public crate-risp-0.1.0 (c (n "risp") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1kdglqsds2x087h6vpspavplcfmvabcp48q2l2ipkwqlq2gxil2r")))

(define-public crate-risp-0.2.0 (c (n "risp") (v "0.2.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "03a6ifff5yagdv6b8pxdb6lpzhci8hpxylay5h385l1k9nqymgzf")))

(define-public crate-risp-0.2.1 (c (n "risp") (v "0.2.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "09sszfb21i9mv67wdxija2s85clgl7f37nija1nq17mjjislfqnm")))

(define-public crate-risp-0.3.0 (c (n "risp") (v "0.3.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "17q39xswvd44dmif5rwrkiddg9byw0ahckrmv1a061x339hp20cn")))

(define-public crate-risp-0.4.0 (c (n "risp") (v "0.4.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0vd4zlb1h2rzr5snxv5lh45xkxi4rh62582gdnqwiwd63xjib3jy")))

(define-public crate-risp-0.5.0 (c (n "risp") (v "0.5.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "05g14nlp4kpakij3hgabgwabfwxw6bdrcz6zmjs9bs31y19nacka")))

(define-public crate-risp-0.6.0 (c (n "risp") (v "0.6.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "10kkmcyyxh5jcjk4l2rwgqnmj5rd4xqv3jbnmyj5v9sy6myi7wd6")))

(define-public crate-risp-0.6.1 (c (n "risp") (v "0.6.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1ysfj3lyhg8mn4xj5zdxs145x7qgwkkbcsnd5p1qnal7y84g1gi4")))

(define-public crate-risp-0.7.0 (c (n "risp") (v "0.7.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "17d6f6vd86iqxarvnrn0csczh614fv65mp73q0b9izk230wc8xv9")))

