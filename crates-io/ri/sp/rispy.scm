(define-module (crates-io ri sp rispy) #:use-module (crates-io))

(define-public crate-rispy-0.0.1 (c (n "rispy") (v "0.0.1") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)))) (h "1av0gg4rhyysh7fw4kckvw0kdsfvaw6pzjvhz3h47yb445dn3lg7") (f (quote (("build_deps" "man"))))))

(define-public crate-rispy-0.0.2 (c (n "rispy") (v "0.0.2") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)))) (h "1gv02kwd3cg9bq8q1w8mnsdzi9gndra5f02r353ag603xly6kdzw") (f (quote (("build_deps" "man"))))))

(define-public crate-rispy-0.0.3 (c (n "rispy") (v "0.0.3") (h "0pnkljs55q11sssm4p7wzwc6nx6bh18zny85wlc8hfrvcgf49xgh")))

(define-public crate-rispy-0.0.4 (c (n "rispy") (v "0.0.4") (h "02f2qndpk210kihs8ihhca428fffn9bi8ywb5c6l20cnhl3dd2i2")))

(define-public crate-rispy-0.0.6 (c (n "rispy") (v "0.0.6") (h "1wnv02rghm5bzx9w4hfkaqifwvjax282mdcf0dwjp8jznaxdmvcp")))

