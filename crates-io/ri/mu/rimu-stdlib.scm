(define-module (crates-io ri mu rimu-stdlib) #:use-module (crates-io))

(define-public crate-rimu-stdlib-0.2.0 (c (n "rimu-stdlib") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rimu-eval") (r "^0.2.0") (d #t) (k 0)) (d (n "rimu-meta") (r "^0.2.0") (d #t) (k 0)) (d (n "rimu-value") (r "^0.2.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.31.0") (d #t) (k 0)))) (h "02sjandbl24mm6dx60s64bpn9ycn1win68gnjpy1xm5h8f7769m9")))

