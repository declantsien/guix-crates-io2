(define-module (crates-io ri mu rimu-eval) #:use-module (crates-io))

(define-public crate-rimu-eval-0.1.0 (c (n "rimu-eval") (v "0.1.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rimu-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "rimu-meta") (r "^0.1.0") (d #t) (k 0)) (d (n "rimu-parse") (r "^0.1.0") (d #t) (k 2)) (d (n "rimu-value") (r "^0.1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0y7y265cwlzwx8mz6h0mphhiv3j245r07rqhnzmmsc68nhl34cd5")))

(define-public crate-rimu-eval-0.2.0 (c (n "rimu-eval") (v "0.2.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rimu-ast") (r "^0.2.0") (d #t) (k 0)) (d (n "rimu-meta") (r "^0.2.0") (d #t) (k 0)) (d (n "rimu-value") (r "^0.2.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "02p8rmvkp6hjkl53yh5faq24dq4fc1rqjlq773slvdxrid1q0319") (r "1.70.0")))

