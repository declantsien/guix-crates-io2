(define-module (crates-io ri mu rimu-repl) #:use-module (crates-io))

(define-public crate-rimu-repl-0.1.0 (c (n "rimu-repl") (v "0.1.0") (d (list (d (n "rimu") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "0skfi7kbr821i2xv8bs9ahdlf1ckz66a1x8h44y007dgzgaj5d09")))

(define-public crate-rimu-repl-0.2.0 (c (n "rimu-repl") (v "0.2.0") (d (list (d (n "rimu") (r "^0.2.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "19qwpq4xl98z8mc7vv6caain8kkv04wiggvfp5k20axlzljjc0ic") (r "1.70.0")))

