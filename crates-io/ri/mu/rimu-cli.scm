(define-module (crates-io ri mu rimu-cli) #:use-module (crates-io))

(define-public crate-rimu-cli-0.1.0 (c (n "rimu-cli") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clio") (r "^0.3.4") (f (quote ("clap-parse"))) (d #t) (k 0)) (d (n "rimu") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0aw39mh9nqpig9dalrwv8bnvcc3fqx3qii25cz85mhihy3yr0b85")))

(define-public crate-rimu-cli-0.2.0 (c (n "rimu-cli") (v "0.2.0") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clio") (r "^0.3.4") (f (quote ("clap-parse"))) (d #t) (k 0)) (d (n "rimu") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0ysbs5p1fmpjrkx98vw8x89j5rzs4g6qvg8r2796aq6nk6zhx8dn") (r "1.70.0")))

