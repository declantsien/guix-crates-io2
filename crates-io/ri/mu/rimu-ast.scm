(define-module (crates-io ri mu rimu-ast) #:use-module (crates-io))

(define-public crate-rimu-ast-0.1.0 (c (n "rimu-ast") (v "0.1.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rimu-meta") (r "^0.1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31.0") (d #t) (k 0)))) (h "0052zm22nbaa92kh9p9j2sq90z88gkkd2iacmxisl4hs2m6arwwq")))

(define-public crate-rimu-ast-0.2.0 (c (n "rimu-ast") (v "0.2.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rimu-meta") (r "^0.2.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31.0") (d #t) (k 0)))) (h "13dqph219yb77z2bxx3mi610ir9ikn5w233gc3pnc3bzs2rsnha1") (r "1.70.0")))

