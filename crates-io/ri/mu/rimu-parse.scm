(define-module (crates-io ri mu rimu-parse) #:use-module (crates-io))

(define-public crate-rimu-parse-0.1.0 (c (n "rimu-parse") (v "0.1.0") (d (list (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rimu-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "rimu-meta") (r "^0.1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0qxwv5hki95az1zy1bgzg4g3l9644xl30il3n63mc4sqyfwmrc22")))

(define-public crate-rimu-parse-0.2.0 (c (n "rimu-parse") (v "0.2.0") (d (list (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rimu-ast") (r "^0.2.0") (d #t) (k 0)) (d (n "rimu-meta") (r "^0.2.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1zclph0jjfsgqvaypbc2g89znwb9n55vf4r0fyaa3cbrjn9lxkzc") (r "1.70.0")))

