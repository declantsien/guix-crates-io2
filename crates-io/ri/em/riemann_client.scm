(define-module (crates-io ri em riemann_client) #:use-module (crates-io))

(define-public crate-riemann_client-0.1.0 (c (n "riemann_client") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.2.5") (d #t) (k 0)) (d (n "protobuf") (r "^0.0.9") (d #t) (k 0)) (d (n "simple_logger") (r "*") (d #t) (k 2)))) (h "1bxkrd0g0vpjkzyvw70j22sivsd9jjhqb1h2qmn4qahlnmh6v25x")))

(define-public crate-riemann_client-0.2.0 (c (n "riemann_client") (v "0.2.0") (d (list (d (n "libc") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.2.5") (d #t) (k 0)) (d (n "protobuf") (r "^0.0.9") (d #t) (k 0)) (d (n "simple_logger") (r "^0.1.0") (d #t) (k 2)))) (h "19kv92lx2sm4ay44kaxakv96dkzkkhzc2x50h4f4f4nbgilp9ci0")))

(define-public crate-riemann_client-0.2.1 (c (n "riemann_client") (v "0.2.1") (d (list (d (n "libc") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.2.5") (d #t) (k 0)) (d (n "protobuf") (r "^0.0.9") (d #t) (k 0)) (d (n "simple_logger") (r "^0.1.0") (d #t) (k 2)))) (h "11dl8vmc44kxljny3vv80c44yncja9al1cia839jybn05hm44026")))

(define-public crate-riemann_client-0.2.2 (c (n "riemann_client") (v "0.2.2") (d (list (d (n "libc") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.2.5") (d #t) (k 0)) (d (n "protobuf") (r "^0.0.9") (d #t) (k 0)) (d (n "simple_logger") (r "^0.1.0") (d #t) (k 2)))) (h "102in9ziwh67vvyfbn3iwa2ndqrcrf7d4jy6gh3zwb0xi8ndx4f8")))

(define-public crate-riemann_client-0.4.0 (c (n "riemann_client") (v "0.4.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.0") (d #t) (k 0)) (d (n "simple_logger") (r "^0.3.1") (d #t) (k 0)))) (h "1lmskmk5hgcjn2fnj1gr0hvsrwphzmr35r0ymmrjl0w67vg244q0")))

(define-public crate-riemann_client-0.5.0 (c (n "riemann_client") (v "0.5.0") (d (list (d (n "docopt") (r "^0.6.60") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (o #t) (d #t) (k 0)))) (h "1ymc2r25ss94qjlhd0mbgwa9lb7256zbg21bf6m2n7537a7nljra") (f (quote (("default" "docopt" "rustc-serialize"))))))

(define-public crate-riemann_client-0.5.1 (c (n "riemann_client") (v "0.5.1") (d (list (d (n "docopt") (r "^0.6.60") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (o #t) (d #t) (k 0)))) (h "0m1vvg62yjf44sfxvp99qbgl9q1afybrd9y8p3y0xk9anlzyrpyy") (f (quote (("default" "docopt" "rustc-serialize"))))))

(define-public crate-riemann_client-0.6.0 (c (n "riemann_client") (v "0.6.0") (d (list (d (n "docopt") (r "^0.6.60") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (o #t) (d #t) (k 0)))) (h "0piggidqad7h4dv69wb8kn9505g5yl7swx5ymp8fkamndqwfrj1f") (f (quote (("default" "docopt" "rustc-serialize"))))))

(define-public crate-riemann_client-0.7.0 (c (n "riemann_client") (v "0.7.0") (d (list (d (n "docopt") (r "^0.6.60") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (o #t) (d #t) (k 0)))) (h "0ghlzbizy9a00xg2jdkgf5r0nj5rxp1piz0npnz9b3swrfkymcj5") (f (quote (("default" "docopt" "rustc-serialize"))))))

(define-public crate-riemann_client-0.8.0 (c (n "riemann_client") (v "0.8.0") (d (list (d (n "docopt") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "protobuf") (r "^2.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)))) (h "0sffb796x187viwypfvc5c9hx4rpqlazga86n69m7gdcy6jwfljm") (f (quote (("default" "docopt" "serde"))))))

(define-public crate-riemann_client-0.9.0 (c (n "riemann_client") (v "0.9.0") (d (list (d (n "docopt") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "protobuf") (r "^2.20.0") (d #t) (k 0)) (d (n "rustls") (r "^0.19.0") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.21.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21.0") (d #t) (k 0)))) (h "0hkjkvgph2dwxinf3k90n14zbzwl2c1r41rqn9m3zdcck9dda18h") (f (quote (("default" "docopt" "serde"))))))

