(define-module (crates-io ri em riemann_cli) #:use-module (crates-io))

(define-public crate-riemann_cli-0.1.0 (c (n "riemann_cli") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.60") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.0") (d #t) (k 0)) (d (n "riemann_client") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "02hmmabb2vm2smvs6bqlnnwlj1wrwqivjnzz5liks0l2cqwah004") (y #t)))

