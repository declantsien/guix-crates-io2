(define-module (crates-io ri db ridb-jwt-client) #:use-module (crates-io))

(define-public crate-ridb-jwt-client-0.1.0 (c (n "ridb-jwt-client") (v "0.1.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.1.0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1dz5snvq7hkddlmy4h0bd09gb070fffdyr1zs8d5fclz4c21rjgj")))

