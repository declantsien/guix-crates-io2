(define-module (crates-io ri ne rinex-qc-traits) #:use-module (crates-io))

(define-public crate-rinex-qc-traits-0.1.0 (c (n "rinex-qc-traits") (v "0.1.0") (d (list (d (n "horrorshow") (r "^0.8") (d #t) (k 0)))) (h "1whdm4yki64piqhq1q45880x87pbg2bnsvyhf8q9ahylvbzph3a2") (f (quote (("default"))))))

(define-public crate-rinex-qc-traits-0.1.1 (c (n "rinex-qc-traits") (v "0.1.1") (d (list (d (n "horrorshow") (r "^0.8") (d #t) (k 0)))) (h "1acn9l9796s5rgs3j6d02w17a14f8p6d8mjcsmnqbxxrv2434lm3") (f (quote (("default"))))))

