(define-module (crates-io ri g- rig-core) #:use-module (crates-io))

(define-public crate-rig-core-0.0.0 (c (n "rig-core") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "mongodb") (r "^2.8.2") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "0plc2x87s62jk2l8kyl8kjjqlkqcx0ijbw2lk58rikcn50xci57c")))

