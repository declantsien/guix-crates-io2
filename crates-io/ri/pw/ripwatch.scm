(define-module (crates-io ri pw ripwatch) #:use-module (crates-io))

(define-public crate-ripwatch-0.1.0 (c (n "ripwatch") (v "0.1.0") (h "0lkikiyhssbfzghhlfxnc9lv8frb3z22byh02y8g6n8bp6wbsd4i")))

(define-public crate-ripwatch-0.1.1 (c (n "ripwatch") (v "0.1.1") (h "092zfq3w47wg1548lgbf68ypnyqcipafmsgaknc7587mdbzmx2yy")))

(define-public crate-ripwatch-0.1.2 (c (n "ripwatch") (v "0.1.2") (h "0zmh9phw1cfvjbshmq7k0v2h869drvmdpiac8dxw74hn9rmxmicz")))

