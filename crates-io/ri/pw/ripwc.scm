(define-module (crates-io ri pw ripwc) #:use-module (crates-io))

(define-public crate-ripwc-0.1.0 (c (n "ripwc") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1i31l6d7lin2fdjlapq992zca8izh254cspj6vq5zwpw9zlnc282")))

(define-public crate-ripwc-0.1.1 (c (n "ripwc") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0x3xwf080dz03a44a76h18ilh95i56zhrjnvgj9i87v82i47mppx")))

