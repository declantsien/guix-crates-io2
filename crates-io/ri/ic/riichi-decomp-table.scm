(define-module (crates-io ri ic riichi-decomp-table) #:use-module (crates-io))

(define-public crate-riichi-decomp-table-0.1.0 (c (n "riichi-decomp-table") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nanovec") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "0284aizs7m6hmrm3w9avm48kmkzj02a1fadgnxfh3c1f7rxjf8j9")))

