(define-module (crates-io ri ic riichi-elements) #:use-module (crates-io))

(define-public crate-riichi-elements-0.1.0 (c (n "riichi-elements") (v "0.1.0") (d (list (d (n "assert-json-diff") (r "^2.0") (d #t) (k 2)) (d (n "bitfield-struct") (r "^0.1.7") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (k 0)))) (h "0a7bf15h6hdqhsskymmxgy82giy4jfcmz64ay0r4scivyqb5g8ab") (f (quote (("std" "itertools/use_std" "serde/std" "strum/std") ("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

