(define-module (crates-io ri im riimote) #:use-module (crates-io))

(define-public crate-riimote-0.1.0 (c (n "riimote") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bluez-async") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hidapi") (r "^1.2") (f (quote ("linux-static-hidraw"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "0fznii435v60n0p2zjlxplliap9px0h113sljkj7fncc1a7n147i")))

