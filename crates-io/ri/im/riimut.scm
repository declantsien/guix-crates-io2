(define-module (crates-io ri im riimut) #:use-module (crates-io))

(define-public crate-riimut-1.0.0 (c (n "riimut") (v "1.0.0") (h "0zjlyx9z60wkaa7wm5gczw78y46r9gmrwmhjh6wj53py0rzlvxjw")))

(define-public crate-riimut-1.1.0 (c (n "riimut") (v "1.1.0") (h "1jv89sl8i2lzn3ccxpm55661hmrl3381fv55kq3z62yvj661c57h")))

(define-public crate-riimut-1.2.0 (c (n "riimut") (v "1.2.0") (h "1v34907cb18ij28brk26i3pqqpr2yic96y4axxkcvi54p4vp675f")))

(define-public crate-riimut-1.2.1 (c (n "riimut") (v "1.2.1") (h "0gvdhrkw87cadanhr3z0k6430w9wxzvlqgh4pdybn2hzk1asdiwi")))

