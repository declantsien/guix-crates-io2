(define-module (crates-io ri dg ridgeline) #:use-module (crates-io))

(define-public crate-ridgeline-0.1.0 (c (n "ridgeline") (v "0.1.0") (d (list (d (n "cpal") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "rustfft") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "1yyslng4hb1b6f1gqgzh6xxrq86ma8yckb8qck1z6ri9x4mvw5vm") (f (quote (("input" "cpal" "thiserror") ("default" "input"))))))

(define-public crate-ridgeline-0.2.0 (c (n "ridgeline") (v "0.2.0") (d (list (d (n "cpal") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "rustfft") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "1ak5in7ldllsp88zrhd2zg0ygf9p037cgk6hxg303h5qkw4rjk0p") (f (quote (("input" "cpal" "thiserror") ("default" "input"))))))

