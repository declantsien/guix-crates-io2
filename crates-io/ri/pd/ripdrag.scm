(define-module (crates-io ri pd ripdrag) #:use-module (crates-io))

(define-public crate-ripdrag-0.1.0 (c (n "ripdrag") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.4.8") (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.9") (d #t) (k 0)) (d (n "opener") (r "^0.5") (d #t) (k 0)))) (h "1ymjshs8lnl2b8q9l5gpk9ycxjp9l8lg7p1vc053dgyd7jb896p1")))

(define-public crate-ripdrag-0.1.1 (c (n "ripdrag") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.4.8") (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.9") (d #t) (k 0)) (d (n "opener") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0zx3b3sc611gnkghasxfc15xd3dmwzq6ixb1qyyrdk7awj9z2bm4")))

(define-public crate-ripdrag-0.1.2 (c (n "ripdrag") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.4.8") (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.9") (d #t) (k 0)) (d (n "opener") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1qfd3316k4rsd4ib39vgvdc2mkwm9h2l53q402x9nv5m4g8ympas")))

(define-public crate-ripdrag-0.1.3 (c (n "ripdrag") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.4.8") (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.9") (d #t) (k 0)) (d (n "opener") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "12whxr243rbrghqy01mnavcnqihy2fy1h417sksy1cxdnrywx8vi")))

(define-public crate-ripdrag-0.1.4 (c (n "ripdrag") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.4.8") (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.9") (d #t) (k 0)) (d (n "opener") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1zp30nhxwpnc2h1pzya1z0vcqqp764kg8lg849pyn4mb4ycgr89l")))

(define-public crate-ripdrag-0.1.5 (c (n "ripdrag") (v "0.1.5") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.4.8") (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.9") (d #t) (k 0)) (d (n "opener") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0is6d3kxw7x2sdn3akpfh5cda9wlss48j17dlzaza10a5bm3963j")))

(define-public crate-ripdrag-0.2.0 (c (n "ripdrag") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.4.8") (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.9") (d #t) (k 0)) (d (n "opener") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0lppnd8qcxyp0j987nz0nhfm646vfrdxw8ynb07g9bmn819bmikh")))

(define-public crate-ripdrag-0.2.1 (c (n "ripdrag") (v "0.2.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.4.8") (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.9") (d #t) (k 0)) (d (n "opener") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "01lz7050ipq592jvm3q1cvghsk4dl9jznl7i4nd1r2xyz4nlxj1c")))

(define-public crate-ripdrag-0.3.0 (c (n "ripdrag") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.6.2") (f (quote ("v4_8"))) (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.13.0") (d #t) (k 0)) (d (n "opener") (r "^0.5.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1m8m86idw7lqq4nrdswh6f578n5548wkwiy4yy70dbinw7ans711")))

(define-public crate-ripdrag-0.3.1 (c (n "ripdrag") (v "0.3.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.6.2") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.13.0") (d #t) (k 0)) (d (n "opener") (r "^0.5.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0252zhjlmlhpgg81mx8npgxfslrs1v5mkq2d9y7zlr3wacgrhg70")))

(define-public crate-ripdrag-0.3.2 (c (n "ripdrag") (v "0.3.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.6.2") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.13.0") (d #t) (k 0)) (d (n "opener") (r "^0.5.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1akvwksb1dk6c0mfgvj23lpa4xwdzrny7gbm5w8q7pj6ipb1nwzv")))

(define-public crate-ripdrag-0.4.0 (c (n "ripdrag") (v "0.4.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glib-macros") (r "^0.18.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.6") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "infer") (r "^0.13.0") (d #t) (k 0)) (d (n "opener") (r "^0.5.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0s6z829gj2g5w92992syd5ia82jhfyfng3cg11fcyjnn5is6i6js")))

(define-public crate-ripdrag-0.4.1 (c (n "ripdrag") (v "0.4.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glib-macros") (r "^0.18.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.6") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "opener") (r "^0.6.1") (d #t) (k 0)))) (h "027xk8r9hamvhhvgbrcddsdn7d0na76z7q1d78cv0lkhnnwpf8s4")))

(define-public crate-ripdrag-0.4.2 (c (n "ripdrag") (v "0.4.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glib-macros") (r "^0.18.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.6") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "opener") (r "^0.6.1") (d #t) (k 0)))) (h "1likr68i8d49bmlvm6s9hh63jkxp6gv5fxcbb3y1n1sgpv20ynpv")))

(define-public crate-ripdrag-0.4.3 (c (n "ripdrag") (v "0.4.3") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glib-macros") (r "^0.18.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.6") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "opener") (r "^0.6.1") (d #t) (k 0)))) (h "11rxbnab8vnrk04rrqs6519fimqcqzs36dkwiim9sqa9yycx14yy")))

(define-public crate-ripdrag-0.4.4 (c (n "ripdrag") (v "0.4.4") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glib-macros") (r "^0.18.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.6") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "opener") (r "^0.6.1") (d #t) (k 0)))) (h "06sdv17bq21shqyhakj9577yp1jspg7bzy7cq94n0myv08s27dj7")))

(define-public crate-ripdrag-0.4.5 (c (n "ripdrag") (v "0.4.5") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glib-macros") (r "^0.18.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.6") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "opener") (r "^0.6.1") (d #t) (k 0)))) (h "19lnx7gdm86ax5y1qr6pbr9cz15yzw51d3fs4qg6z6smx7d6jna6")))

(define-public crate-ripdrag-0.4.6 (c (n "ripdrag") (v "0.4.6") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glib-macros") (r "^0.18.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.6") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "opener") (r "^0.6.1") (d #t) (k 0)))) (h "1s6d5dfdcisi0bgnmg6s7wcrnqvxava6i6q0nq136kslx0cal7yp")))

(define-public crate-ripdrag-0.4.7 (c (n "ripdrag") (v "0.4.7") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glib-macros") (r "^0.19.3") (d #t) (k 0)) (d (n "gtk") (r "^0.6.6") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "opener") (r "^0.7.0") (d #t) (k 0)))) (h "1spk1as3vqg24q6iw69mihgk7y298snyvj2gdmw4ihnfv5ga2snx")))

(define-public crate-ripdrag-0.4.8 (c (n "ripdrag") (v "0.4.8") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glib-macros") (r "^0.19.3") (d #t) (k 0)) (d (n "gtk") (r "^0.6.6") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "opener") (r "^0.7.0") (d #t) (k 0)))) (h "03ba4dpplspdnnz46dsijgzvl1y7f29zcz0w7iwrnq2bhhvzjng3")))

