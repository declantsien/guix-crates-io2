(define-module (crates-io ri pd ripdeque) #:use-module (crates-io))

(define-public crate-ripdeque-0.0.1 (c (n "ripdeque") (v "0.0.1") (h "1r88454crcmyzx1n9ryavd4i33qls9g0h9dx3nhs1c9y88ympvq1")))

(define-public crate-ripdeque-0.0.2 (c (n "ripdeque") (v "0.0.2") (h "0b7j6aa53wc01y575kgf8gz3x954499c3gzkb3rcn5yaj989vf2g")))

