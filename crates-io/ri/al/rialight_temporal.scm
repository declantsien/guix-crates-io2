(define-module (crates-io ri al rialight_temporal) #:use-module (crates-io))

(define-public crate-rialight_temporal-0.1.0 (c (n "rialight_temporal") (v "0.1.0") (h "1pibq2hkfgi3c2xifclx262v0gizf5qy5lkc2xr0nxny4filc90w")))

(define-public crate-rialight_temporal-1.0.0 (c (n "rialight_temporal") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "1m3xg0kifb83qpzz6cp1a4izzmll4yr0a4issss449ifsclhd2b7")))

