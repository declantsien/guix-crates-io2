(define-module (crates-io ri al rialight_build) #:use-module (crates-io))

(define-public crate-rialight_build-1.0.0 (c (n "rialight_build") (v "1.0.0") (d (list (d (n "rialight_app_config") (r "^1") (d #t) (k 0)))) (h "0v1rw1cs4w7nl0dkjklaz8la2zhkg7laa8cfkjsj5r5h1xamndpa") (y #t)))

(define-public crate-rialight_build-1.1.0 (c (n "rialight_build") (v "1.1.0") (d (list (d (n "rialight_app_config") (r "^1") (d #t) (k 0)))) (h "00f8k19k8285czbxp77jgm4jyl26nkgx9prmpxp6m5iv958qn5f9") (y #t)))

