(define-module (crates-io ri al rialight_util) #:use-module (crates-io))

(define-public crate-rialight_util-1.0.0 (c (n "rialight_util") (v "1.0.0") (h "0zf4hjm673dsnyn60fygcsyxzymrxx7mm5rl334rd9av2bakx8zi") (y #t)))

(define-public crate-rialight_util-1.1.0 (c (n "rialight_util") (v "1.1.0") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0s3bsixj6jf268sw55hrq8fd555jv2z933hb15hdqd10vkgp6yxq") (y #t)))

(define-public crate-rialight_util-1.1.1 (c (n "rialight_util") (v "1.1.1") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1nnx6aba1vxc698xhn9qv5q0hksj5min8dzsv54vcaj1brcn5rcj") (y #t)))

(define-public crate-rialight_util-1.1.2 (c (n "rialight_util") (v "1.1.2") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0qbcg7zd21v35mxz8xdyqdf40f9p7zh4xris4wxvn1h7nvxd8xiy") (y #t)))

(define-public crate-rialight_util-1.1.3 (c (n "rialight_util") (v "1.1.3") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0saplzdj4qr13lss6g53nwfszwrq8zhy4xhp0ycs97vcvhgk0sw3") (y #t)))

(define-public crate-rialight_util-1.1.4 (c (n "rialight_util") (v "1.1.4") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "08k0w410vp42gyan0mapq7vgr3j3pk5slsl5x710iw555j60h50h") (y #t)))

(define-public crate-rialight_util-1.2.0 (c (n "rialight_util") (v "1.2.0") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1vpglh1lc58b9rc0gn5qs47q8v4faglav1nwvx2nwlrlllzi44c8") (y #t)))

(define-public crate-rialight_util-1.3.0 (c (n "rialight_util") (v "1.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0lqx0yrry2dghypb11p5a82gxrf6f2ymlfynrsnifbiizlsyiyw5") (y #t)))

(define-public crate-rialight_util-1.3.1 (c (n "rialight_util") (v "1.3.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0w5vwz7vh5a8v92y2bwkc65n179bh5i6zi3l2l23r6lrkxydnyl7") (y #t)))

(define-public crate-rialight_util-1.4.0 (c (n "rialight_util") (v "1.4.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "12w7iswb28vwy3pikyg9lmcfw0fbdsylgj50c9clc1958kacr3rw") (y #t)))

(define-public crate-rialight_util-1.5.0 (c (n "rialight_util") (v "1.5.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1jnss6cp4yws38zlwp4w7sw0w8j5j4ad9bq7yvf89qhn2cbj2q2j")))

(define-public crate-rialight_util-1.6.0 (c (n "rialight_util") (v "1.6.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "00qy883d112xsd0gbxgpw1ar2cm71b00rm00c3xsr7lqyh83lxax")))

(define-public crate-rialight_util-1.6.1 (c (n "rialight_util") (v "1.6.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0hi9bgbzaa9djmrfr9q104s2f20d667ihrnj0i3wlq96vv25gj75")))

