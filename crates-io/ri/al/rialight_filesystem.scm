(define-module (crates-io ri al rialight_filesystem) #:use-module (crates-io))

(define-public crate-rialight_filesystem-1.0.0 (c (n "rialight_filesystem") (v "1.0.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "sv_str") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)))) (h "0fs0fv6z7r1jchdslzs5sd9853smj7zcp4jp6wnrqxawl2j43wk4") (y #t)))

(define-public crate-rialight_filesystem-1.0.1 (c (n "rialight_filesystem") (v "1.0.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "sv_str") (r "^1.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)))) (h "1h4f3bqdy6cngrazh4ks2bnxq36y521hs29kaaq01lwkg1bplpw0") (y #t)))

(define-public crate-rialight_filesystem-1.1.0 (c (n "rialight_filesystem") (v "1.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "rialight_util") (r "^1") (d #t) (k 0)) (d (n "sv_str") (r "^1.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)))) (h "1zy45044jw6qacsz5wh4852liq36sfkkmxvjkrbdjsj0dsqnd2f9") (y #t)))

(define-public crate-rialight_filesystem-1.1.1 (c (n "rialight_filesystem") (v "1.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "rialight_util") (r "^1") (d #t) (k 0)) (d (n "sv_str") (r "^1.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)))) (h "1hnimm80ir0a10mk6jicc77vlpy1wcn7nrwq3xc3vgawv0hxhfah") (y #t)))

(define-public crate-rialight_filesystem-1.2.0 (c (n "rialight_filesystem") (v "1.2.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "gc") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "rialight_util") (r "^1") (d #t) (k 0)) (d (n "sv_str") (r "^1.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)))) (h "11np6n6xpynjdlq7ck1q4dxznjs5n2bjpqi3wnqhf4730da1mi3x") (y #t)))

(define-public crate-rialight_filesystem-1.3.0 (c (n "rialight_filesystem") (v "1.3.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.4.1") (d #t) (k 0)) (d (n "rialight_temporal") (r "^1") (d #t) (k 0)) (d (n "rialight_util") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)))) (h "0ssafp001rbad597i2052ak2xgna5c6glngarlg4917n7kzr1wdj")))

(define-public crate-rialight_filesystem-1.4.0 (c (n "rialight_filesystem") (v "1.4.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.4.1") (d #t) (k 0)) (d (n "rialight_temporal") (r "^1") (d #t) (k 0)) (d (n "rialight_util") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)))) (h "0wznmd9k4m5b1qkp1j3bgmk4faskkivwl8i4zbfzk90gn7jl6svd")))

