(define-module (crates-io ri al rialight_app_config) #:use-module (crates-io))

(define-public crate-rialight_app_config-1.0.0 (c (n "rialight_app_config") (v "1.0.0") (d (list (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1kyjmhdaydvq1sfz8q6yi94jivfr1zhzzgffxbqsla4ajbwipaqn") (y #t)))

