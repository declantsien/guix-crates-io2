(define-module (crates-io ri al rialight) #:use-module (crates-io))

(define-public crate-rialight-1.0.0 (c (n "rialight") (v "1.0.0") (h "1mys6h2s33bxigjm2fv8fympzn57wqskqbvw4867yf1cp7wdlxr6")))

(define-public crate-rialight-1.0.1 (c (n "rialight") (v "1.0.1") (d (list (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "rialight_filesystem") (r "^1") (d #t) (k 0)) (d (n "rialight_intl") (r "^1") (d #t) (k 0)) (d (n "rialight_temporal") (r "^1") (d #t) (k 0)) (d (n "rialight_util") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hr710342162l2pdby0xhddlp81fv1lw193nmmb6ampa17c9alyi")))

