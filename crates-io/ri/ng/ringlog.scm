(define-module (crates-io ri ng ringlog) #:use-module (crates-io))

(define-public crate-ringlog-0.1.0 (c (n "ringlog") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "metriken") (r "^0.1.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.6") (d #t) (k 0)))) (h "1gi8r0ssldwyvg2vz5zqmyn8cnz3wlky5phd67ff084z89mz90d4")))

(define-public crate-ringlog-0.1.1 (c (n "ringlog") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "metriken") (r "^0.1.1") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.6") (d #t) (k 0)))) (h "1wb0ma4zkvwlcmrmp4rcx3mpcqgd83s7j9wp73bl3b0b00klpnw2")))

(define-public crate-ringlog-0.2.0 (c (n "ringlog") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "metriken") (r "^0.2.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.6") (d #t) (k 0)))) (h "0ijagf13kx74c7vchzr1dyr5ckybnkkp6qz2vbgswgdl9lg3s3f9")))

(define-public crate-ringlog-0.3.0 (c (n "ringlog") (v "0.3.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "metriken") (r "^0.3.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.6") (d #t) (k 0)))) (h "151k3h77sfqfg7c845926kks13wc0njjvzakyydkqvlbcglnaxlq")))

(define-public crate-ringlog-0.3.1 (c (n "ringlog") (v "0.3.1") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "metriken") (r "^0.3.2") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.6") (d #t) (k 0)))) (h "0p8myqh1n3qc9jag3f1gcy7d4k312cqs22l330dx5j3kksr4905p")))

(define-public crate-ringlog-0.3.2 (c (n "ringlog") (v "0.3.2") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "metriken") (r "^0.3.3") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.6") (d #t) (k 0)))) (h "1p559il9flr0912dysg9hqlx2l5gqad7lhyg1g6vssa7y42ghnrj")))

(define-public crate-ringlog-0.4.0 (c (n "ringlog") (v "0.4.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "clocksource") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "metriken") (r "^0.3.3") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.6") (d #t) (k 0)))) (h "1pgwqw2fzijpvsv4zbh8yazcfmdlgfxmwd6q7hs1f18002vgxxk2")))

(define-public crate-ringlog-0.5.0 (c (n "ringlog") (v "0.5.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "clocksource") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "metriken") (r "^0.4.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.6") (d #t) (k 0)))) (h "14m4advh6jslhbr9fxmwskq4jn8wl1j5qhij1gkcwp87k1gdl4am")))

(define-public crate-ringlog-0.6.0 (c (n "ringlog") (v "0.6.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "clocksource") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "metriken") (r "^0.5.1") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.6") (d #t) (k 0)))) (h "0s86gqh24yhddgpvh5y23mp0g4i0g97p5awybl3yh7v5hl9jjh8c")))

