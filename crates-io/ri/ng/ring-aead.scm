(define-module (crates-io ri ng ring-aead) #:use-module (crates-io))

(define-public crate-ring-aead-0.1.0 (c (n "ring-aead") (v "0.1.0") (d (list (d (n "aead") (r "^0.2") (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "09n5a1vxyfiiyynsq7br8572wqlnrrx0951s8ghvj8z6mm5q7cr1") (f (quote (("default" "alloc") ("alloc" "aead/alloc")))) (y #t)))

(define-public crate-ring-aead-0.2.0 (c (n "ring-aead") (v "0.2.0") (d (list (d (n "aead") (r "=0.3.0") (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1z1406rplzb0f1wznk8nr2hx1hrmkyscq7y3yw50swqf2v0h5x2j") (f (quote (("default" "alloc") ("alloc" "aead/alloc")))) (y #t)))

(define-public crate-ring-aead-0.99.0 (c (n "ring-aead") (v "0.99.0") (h "0aznhlbjaal3vzwlhqmi88hs1f878s85dk0fdpdpwamnnfyyp2nm") (y #t)))

