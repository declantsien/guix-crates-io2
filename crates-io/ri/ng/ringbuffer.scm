(define-module (crates-io ri ng ringbuffer) #:use-module (crates-io))

(define-public crate-ringbuffer-0.1.0 (c (n "ringbuffer") (v "0.1.0") (h "1vsbgfs7fyamd5mjr7h3gl2bcgq0jvnpn2ral5mwnnd03wf3q1g6") (y #t)))

(define-public crate-ringbuffer-0.1.1 (c (n "ringbuffer") (v "0.1.1") (h "1zpcbk0g95pz3zpvd7vmpk9djfi3dxxjnbmrrkl9vi3pa36vxd35")))

(define-public crate-ringbuffer-0.2.0 (c (n "ringbuffer") (v "0.2.0") (h "0k66d46948gzn0slywmsrawvmfgd704whqc74ily837ahk0gmsf4")))

(define-public crate-ringbuffer-0.2.1 (c (n "ringbuffer") (v "0.2.1") (h "1kmiip73fhryjzqivv7nv9737zb6qyxnrbl9i7ackkdxfakybgrv")))

(define-public crate-ringbuffer-0.2.2 (c (n "ringbuffer") (v "0.2.2") (h "18l4lkl211c8bs29pvs3cb0lnjjwyc48kfd0qm31fbca4kj1ivj4")))

(define-public crate-ringbuffer-0.2.3 (c (n "ringbuffer") (v "0.2.3") (h "0flv5h92d6q2f2mbhqnl92ip2dh5rzzkjqbmfrdybmmsr5zg2ih2")))

(define-public crate-ringbuffer-1.0.0 (c (n "ringbuffer") (v "1.0.0") (d (list (d (n "generic-array") (r "^0.14.4") (o #t) (d #t) (k 0)))) (h "01wh2jca411biaap4cx6aw61ih8am5wciw2rq5lxnb2cmh0xrl3m") (f (quote (("generic_uninit") ("default" "alloc" "generic-array") ("const_generics") ("alloc")))) (y #t)))

(define-public crate-ringbuffer-0.3.0 (c (n "ringbuffer") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.14.4") (o #t) (d #t) (k 0)))) (h "01rlrww0393grcqkj0nmzdilw9807anq41p243c81q8yiz284gfj") (f (quote (("default" "alloc" "generic-array") ("const_generics") ("alloc")))) (y #t)))

(define-public crate-ringbuffer-0.4.0 (c (n "ringbuffer") (v "0.4.0") (d (list (d (n "array-init") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)) (d (n "generic-array") (r "^0.14.4") (o #t) (d #t) (k 0)))) (h "047d3zpprqma6ksd166y7bbficw1zg7r1n4jzyx3js28yw8i441b") (f (quote (("default" "alloc" "generic-array") ("const_generics" "array-init" "array-init/const-generics") ("alloc")))) (y #t)))

(define-public crate-ringbuffer-0.5.0 (c (n "ringbuffer") (v "0.5.0") (d (list (d (n "array-init") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "1mn7svj67g88kdn02p5m2i35wav8nypiacw6glm2z9v745m0rd10") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-ringbuffer-0.5.1 (c (n "ringbuffer") (v "0.5.1") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "1c22bh8zl1qxcr6s9x50ylb5cip7y6mlfx61m0wmqfrkaabbksnj") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-ringbuffer-0.6.0 (c (n "ringbuffer") (v "0.6.0") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "192bjv9q4sw08x0bk3dbzrxqsidyl2mm8jdh213djxk9kf9hd33s") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-ringbuffer-0.7.0 (c (n "ringbuffer") (v "0.7.0") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "0l871gz7csc0gn1kd6xkv9bjbb62c6q72kmsv7i36n85jwdjzrgl") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-ringbuffer-0.7.1 (c (n "ringbuffer") (v "0.7.1") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "18w86baap0y1j75r8bqxkb83mmpdsf3jjma1597ckvdcmg51886k") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-ringbuffer-0.8.0 (c (n "ringbuffer") (v "0.8.0") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "0xgwplxxjlfnrw9dc5wky66ppaby04vf0q5jxd2225bis0jaxb9z") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-ringbuffer-0.8.1 (c (n "ringbuffer") (v "0.8.1") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "0fj5ir1gv4216adrw30697apa4xb20d0p515ffxndx7iw9wc11z9") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-ringbuffer-0.8.2 (c (n "ringbuffer") (v "0.8.2") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "14nrh7xcangqbxh3ddw4fhqqysawhnkfyrg1ry6k1p2gr6lyppvj") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-ringbuffer-0.8.3 (c (n "ringbuffer") (v "0.8.3") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "1qwr8b9jjhp4mikmgmmrzxjhh53k80kqnygqhdvaly8xg4x9vr10") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-ringbuffer-0.8.4 (c (n "ringbuffer") (v "0.8.4") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "1l39nc4z9ncdzlrb3z5ahq7nbp9il0m53klxi77msn9719rh02mk") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-ringbuffer-0.8.5 (c (n "ringbuffer") (v "0.8.5") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "13b78c4vr2357pi46sc88a7x8nvmwdfcgj1y91jcq45xjgcpi9qa") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-ringbuffer-0.9.0 (c (n "ringbuffer") (v "0.9.0") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "13rl49didg22a3ihm314fn6v76wnzrvvbwqlljb7mxfk7hf3wxi4") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-ringbuffer-0.10.0 (c (n "ringbuffer") (v "0.10.0") (d (list (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "0w4ki2zalpv75ifj9aizq9f4fnhwqmwhrmra5w551fs6rca2a2ii") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-ringbuffer-0.11.0 (c (n "ringbuffer") (v "0.11.0") (d (list (d (n "const_panic") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "1l8dwrz529g4nd9r6xrwh8jbwcqf310d4rg2z871n1s8csf0kxrk") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-ringbuffer-0.11.1 (c (n "ringbuffer") (v "0.11.1") (d (list (d (n "const_panic") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "0s5xr9kymkpsvsdkpccyrqzzwwk2fxhb778n8a4hmfga6ml2ykq3") (f (quote (("default" "alloc") ("alloc")))) (r "1.59")))

(define-public crate-ringbuffer-0.12.0 (c (n "ringbuffer") (v "0.12.0") (d (list (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "0zmkxiq50781l3a6wxzjzbs3hzk12nyfmxc3qsra8mpca9agjwqr") (f (quote (("default" "alloc") ("alloc")))) (r "1.59")))

(define-public crate-ringbuffer-0.13.0 (c (n "ringbuffer") (v "0.13.0") (d (list (d (n "compiletest_rs") (r "^0.9.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1ijxq45xz9fk9m72cngpvd4wxk8gclfi90r5yi4xk2yigyrzpr5n") (f (quote (("default" "alloc") ("alloc")))) (r "1.59")))

(define-public crate-ringbuffer-0.14.0 (c (n "ringbuffer") (v "0.14.0") (d (list (d (n "compiletest_rs") (r "^0.10.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "10bv5d1s4dk0jk955i9jynvcyf40xl4fz3y6kvgrhlbpzmbj8k9g") (f (quote (("default" "alloc") ("alloc")))) (r "1.59")))

(define-public crate-ringbuffer-0.14.1 (c (n "ringbuffer") (v "0.14.1") (d (list (d (n "compiletest_rs") (r "^0.10.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0c5r9i70cysxykrgl91h63ykrvwdqi0gah3qkawjj33wifvj7k80") (f (quote (("default" "alloc") ("alloc")))) (y #t) (r "1.59")))

(define-public crate-ringbuffer-0.14.2 (c (n "ringbuffer") (v "0.14.2") (d (list (d (n "compiletest_rs") (r "^0.10.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0dxa3y0b1m61v4778wphy2z1mqn5f7xlg3agclja7ibax4w9dfjf") (f (quote (("default" "alloc") ("alloc")))) (r "1.59")))

(define-public crate-ringbuffer-0.15.0 (c (n "ringbuffer") (v "0.15.0") (d (list (d (n "compiletest_rs") (r "^0.10.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0lzd15aplym0rb037iv1h67gdssnvmqd2xn06ffgy1gjf67kdxix") (f (quote (("default" "alloc") ("alloc")))) (r "1.59")))

