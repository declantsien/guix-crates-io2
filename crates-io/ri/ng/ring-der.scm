(define-module (crates-io ri ng ring-der) #:use-module (crates-io))

(define-public crate-ring-der-0.16.19 (c (n "ring-der") (v "0.16.19") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "untrusted") (r "^0.7.1") (d #t) (k 0)))) (h "1mz9w86vvlcxb6yavab1gdlpv3q2p1hjl5cqxq7jf8z470hy4x5x") (y #t)))

(define-public crate-ring-der-0.16.19-1 (c (n "ring-der") (v "0.16.19-1") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "untrusted") (r "^0.7.1") (d #t) (k 0)))) (h "0m92976ykcnqsygi5d8m0lv7pb1f4r9mfzdhs77wzn3hx7yx2dxs") (y #t)))

