(define-module (crates-io ri ng ringhopper-macros) #:use-module (crates-io))

(define-public crate-ringhopper-macros-0.1.0 (c (n "ringhopper-macros") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "windows") (r "^0.40") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0jkgwigc92svi6lfclh2b7wx4d4d69d65zg9zha4vn9y4vch1c96") (y #t)))

