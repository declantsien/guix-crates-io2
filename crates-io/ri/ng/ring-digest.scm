(define-module (crates-io ri ng ring-digest) #:use-module (crates-io))

(define-public crate-ring-digest-0.1.0 (c (n "ring-digest") (v "0.1.0") (d (list (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "1bqk3yqq43ad8z0adw0ix8cxaw18yawv5x1l4pciv7hpkg9x4pfd") (y #t)))

