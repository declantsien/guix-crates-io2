(define-module (crates-io ri ng rings) #:use-module (crates-io))

(define-public crate-rings-0.1.0 (c (n "rings") (v "0.1.0") (d (list (d (n "sauron") (r "^0.60.5") (d #t) (k 0)))) (h "0sgq2pa17ill760m9m0vi2j8y3wwjkrnyjb4zf6bg7w0bqazdzs8")))

(define-public crate-rings-0.2.0 (c (n "rings") (v "0.2.0") (d (list (d (n "sauron") (r "^0.60.6") (d #t) (k 0)))) (h "0vcandas742wbsjhhjphbk80kz7cm94mfqawfqwwr5x7qf11sajz")))

