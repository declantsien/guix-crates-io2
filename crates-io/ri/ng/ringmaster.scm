(define-module (crates-io ri ng ringmaster) #:use-module (crates-io))

(define-public crate-ringmaster-0.0.1 (c (n "ringmaster") (v "0.0.1") (h "1qn7hszy0ajxvac0x84lrvvh2p3rpcw01mjzdv03rdx22w4qraf7")))

(define-public crate-ringmaster-0.1.0 (c (n "ringmaster") (v "0.1.0") (h "0if0hy7hcym3apmx64c9d74n4wg9gj85f25ap2cqzvpg1vinwr8y")))

(define-public crate-ringmaster-0.2.0 (c (n "ringmaster") (v "0.2.0") (d (list (d (n "tokio") (r "~0.2.22") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "~0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "116cll13agx7wkf6anc7iqnzc3lj3jqnr13rnfr83ix92h9xnjd7")))

(define-public crate-ringmaster-0.2.1 (c (n "ringmaster") (v "0.2.1") (d (list (d (n "tokio") (r "~0.2.22") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "~0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1aia5d69srsq84i5mgynbai6c57h3pl1czy2663gsrlz4cf168d5")))

(define-public crate-ringmaster-0.3.1 (c (n "ringmaster") (v "0.3.1") (d (list (d (n "tokio") (r "~0.2.22") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "~0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1c8ms7fi19plqqajz5n0r5kd17biqjlxbyymyh3plhi81d2jizcf")))

(define-public crate-ringmaster-0.3.2 (c (n "ringmaster") (v "0.3.2") (d (list (d (n "tokio") (r "~0.2.22") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "~0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1gqyhi0w2wy7vkf38b5dvnm6wl1vgsdb7xijdbcnl5xfnb86ycwv")))

(define-public crate-ringmaster-0.4.0 (c (n "ringmaster") (v "0.4.0") (d (list (d (n "tokio") (r "~0.2.22") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "~0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1qjwgzjfhsxi8lz1qa9180544ypmcckbwddm3h0a9gvfql0v5wky")))

(define-public crate-ringmaster-0.5.0 (c (n "ringmaster") (v "0.5.0") (d (list (d (n "tokio") (r "~0.2.22") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "~0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1l0bvs2rrhx6sis3i50g6d6n02f081kkk28xbpsmc901b43ssnk8")))

(define-public crate-ringmaster-0.6.0 (c (n "ringmaster") (v "0.6.0") (d (list (d (n "tokio") (r "~0.2.22") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "~0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0pfy7sa3p5qq11hm2ihg8bdzg50wr4ylid3wwl41dvdg1j6r2p8q")))

(define-public crate-ringmaster-0.8.0 (c (n "ringmaster") (v "0.8.0") (h "04xp9glvvy540y2x8mxkvf9x9yzc056bspivym1vkb493k3lanyg")))

(define-public crate-ringmaster-0.9.0 (c (n "ringmaster") (v "0.9.0") (h "002m1c621yf3zrkfwzsgbiiab9d4zsxqshdz16alldx19vyy0m7h")))

(define-public crate-ringmaster-0.9.1 (c (n "ringmaster") (v "0.9.1") (h "165br7rmwiabbjkjxn51hp2zwxm4f17fp84wmndx75d2h40nk84b")))

(define-public crate-ringmaster-0.10.0 (c (n "ringmaster") (v "0.10.0") (h "1h8qsn3wx7i8bagxnddzawbiz0swqkyggndgff1cx9xf0zc0847i")))

(define-public crate-ringmaster-0.10.1 (c (n "ringmaster") (v "0.10.1") (h "0gp5ga3i28bis92xx831akby5khygf0dq5cw26dw57lm8kyqyqpl")))

