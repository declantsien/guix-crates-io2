(define-module (crates-io ri ng ring-io) #:use-module (crates-io))

(define-public crate-ring-io-0.0.1 (c (n "ring-io") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "uring-sys") (r "^0.7.4") (d #t) (k 0)))) (h "1mx3bi70276h2n5l8yl5irayacfbxv5pd3z6yapl515hw0n5rmcw") (y #t)))

