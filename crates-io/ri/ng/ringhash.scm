(define-module (crates-io ri ng ringhash) #:use-module (crates-io))

(define-public crate-ringhash-0.0.0 (c (n "ringhash") (v "0.0.0") (h "03lgsc2sgd6lhljn674iyn5lwngn4c1xxkbgxq9z5837chgxvmz9")))

(define-public crate-ringhash-0.1.0 (c (n "ringhash") (v "0.1.0") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hly92lj2rha14i008rw3h184rhqhcrrpxxrkfqxgn2bz701ackh")))

(define-public crate-ringhash-0.1.1 (c (n "ringhash") (v "0.1.1") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0p09gi8hj7b39i6wygpsylvm245v1pijbdnyi07klsjgi7b7f7h9")))

