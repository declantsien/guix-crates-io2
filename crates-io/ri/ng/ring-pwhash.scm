(define-module (crates-io ri ng ring-pwhash) #:use-module (crates-io))

(define-public crate-ring-pwhash-0.1.0 (c (n "ring-pwhash") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r ">= 0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "188b14i7nwl53vgf1i116s6q8cz15ywzn34348laxf6n2fwd2b4h") (y #t)))

(define-public crate-ring-pwhash-0.1.1 (c (n "ring-pwhash") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r ">= 0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1z6iixkv95ib758xhc2k6i5hviag1m7r0zdb3xmlbfpxxvzac2qn") (y #t)))

(define-public crate-ring-pwhash-0.1.2 (c (n "ring-pwhash") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r ">= 0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1dv42wn6jzrf50yvh5v0b8p0dnqyfjbwi8pw7q65lk57a83yxhzy") (y #t)))

(define-public crate-ring-pwhash-0.2.0 (c (n "ring-pwhash") (v "0.2.0") (d (list (d (n "data-encoding") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r "^0.9") (d #t) (k 0)))) (h "0x5fb6bjypjanh4ji2467ds70bcmafwqmyii4klp2v3z9kk06vxd") (y #t)))

(define-public crate-ring-pwhash-0.11.0 (c (n "ring-pwhash") (v "0.11.0") (d (list (d (n "data-encoding") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r "^0.11") (d #t) (k 0)))) (h "1zysfnxf8danizn8h03ym4sxlk6mrbxfr7fr1w9mvhn5f7i3ig33") (y #t)))

(define-public crate-ring-pwhash-0.12.0 (c (n "ring-pwhash") (v "0.12.0") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (d #t) (k 0)))) (h "0ly0g3zm566zj8j39hm2sch396gkh38a8kzzl2i7dcvrczc78hrr") (y #t)))

(define-public crate-ring-pwhash-0.99.0 (c (n "ring-pwhash") (v "0.99.0") (h "0h486v5z6il6k7cj39ry2psdx6mvzyywl4dkzw6sfai97dfhvr0l") (y #t)))

