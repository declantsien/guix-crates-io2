(define-module (crates-io ri ng ringbuf-basedrop) #:use-module (crates-io))

(define-public crate-ringbuf-basedrop-0.1.0 (c (n "ringbuf-basedrop") (v "0.1.0") (d (list (d (n "basedrop") (r "^0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)))) (h "1s04v7gv641z2qqrnmqi5sgbyl4wgwbgr6bnv3xsmnsd3rj5wzwp") (f (quote (("std") ("default" "std") ("benchmark"))))))

(define-public crate-ringbuf-basedrop-0.1.1 (c (n "ringbuf-basedrop") (v "0.1.1") (d (list (d (n "basedrop") (r "^0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)))) (h "1zwcccysa26sq5xp42db28msfxnwgrayizglvri71qafq4gxvbr1") (f (quote (("std") ("default" "std") ("benchmark"))))))

