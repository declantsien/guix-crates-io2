(define-module (crates-io ri ng ringbuffer-iteration) #:use-module (crates-io))

(define-public crate-ringbuffer-iteration-0.1.0 (c (n "ringbuffer-iteration") (v "0.1.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0vsz85q4081rsv4ncz4kmv3b67ap3bb0ngigkj98a37aqisnk3jv")))

