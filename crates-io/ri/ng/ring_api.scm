(define-module (crates-io ri ng ring_api) #:use-module (crates-io))

(define-public crate-ring_api-0.1.0 (c (n "ring_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05bfh20xv4zvhs1sv3gfk0hqy2c1mgl75ylbc2w62fr36rc52ixz")))

