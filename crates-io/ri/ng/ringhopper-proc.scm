(define-module (crates-io ri ng ringhopper-proc) #:use-module (crates-io))

(define-public crate-ringhopper-proc-0.1.0 (c (n "ringhopper-proc") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0cspp2938dj1mliglynxk6lv0s5rd4n3s62ij8gabz833kb94xi6") (y #t)))

(define-public crate-ringhopper-proc-0.1.1 (c (n "ringhopper-proc") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ixqdswjlk8grw18s34lc56ywjc7dk25pdn1599z86zyqypcx5xb")))

(define-public crate-ringhopper-proc-0.1.2 (c (n "ringhopper-proc") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c1yvhmck3cq5d53c6h1mcxdghdr5p31n6i96w0kyg3ja1d2hbl6")))

(define-public crate-ringhopper-proc-0.1.3 (c (n "ringhopper-proc") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yqnais7khmj95y7mz5c033m7z5llkfhkf3h5fh75d124dan8ffj")))

