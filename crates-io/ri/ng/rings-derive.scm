(define-module (crates-io ri ng rings-derive) #:use-module (crates-io))

(define-public crate-rings-derive-0.2.6 (c (n "rings-derive") (v "0.2.6") (d (list (d (n "quote") (r "^1.0.28") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-macro-support") (r "^0.2.84") (o #t) (d #t) (k 0)) (d (n "wasmer") (r "^3.3.0") (o #t) (d #t) (k 0)))) (h "0f9s7bsk0mm83vybbr53p5k3yqs2rvq5ly2c1biwqb5ahwzk8jm3") (f (quote (("wasm" "wasm-bindgen-macro-support" "quote") ("default"))))))

(define-public crate-rings-derive-0.2.7 (c (n "rings-derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)) (d (n "wasm-bindgen-macro-support") (r "^0.2.84") (o #t) (d #t) (k 0)) (d (n "wasmer") (r "^3.3.0") (o #t) (d #t) (k 0)))) (h "12s7vchnikyvp1kmcnsfhxyvb21gfpz5lil7r1jk0ani28vibg23") (f (quote (("wasm" "wasm-bindgen-macro-support") ("default") ("core_crate"))))))

(define-public crate-rings-derive-0.3.0 (c (n "rings-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)) (d (n "wasm-bindgen-macro-support") (r "^0.2.84") (o #t) (d #t) (k 0)) (d (n "wasmer") (r "^3.3.0") (o #t) (d #t) (k 0)))) (h "0w101prv2nqi0fa9bdsw6b3d1i1y5j8p46apx5svfz2mazz4qr3n") (f (quote (("wasm" "wasm-bindgen-macro-support") ("default") ("core_crate"))))))

(define-public crate-rings-derive-0.5.1 (c (n "rings-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)) (d (n "wasm-bindgen-macro-support") (r "^0.2.84") (o #t) (d #t) (k 0)) (d (n "wasmer") (r "^3.3.0") (o #t) (d #t) (k 0)))) (h "029yfqa39bsb3jqiax3r52jvi8h7b1bnxsrzdlzgfm9xaryha6nd") (f (quote (("wasm" "wasm-bindgen-macro-support") ("default") ("core_crate"))))))

(define-public crate-rings-derive-0.6.0 (c (n "rings-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)) (d (n "wasm-bindgen-macro-support") (r "^0.2.84") (o #t) (d #t) (k 0)))) (h "1clf48mbrj9az9pz680imzqwn4i67hvazv3ip36sx8vqn9jdrihx") (f (quote (("wasm" "wasm-bindgen-macro-support") ("default") ("core_crate"))))))

(define-public crate-rings-derive-0.7.0 (c (n "rings-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)) (d (n "wasm-bindgen-macro-support") (r "^0.2.84") (o #t) (d #t) (k 0)))) (h "1gap0bxnn16n75l9rf3mwxlcwkxkcszccvfp3pgaw9yk0na0cfkr") (f (quote (("wasm" "wasm-bindgen-macro-support") ("default") ("core_crate"))))))

