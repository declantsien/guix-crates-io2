(define-module (crates-io ri ng ringbuf) #:use-module (crates-io))

(define-public crate-ringbuf-0.1.0 (c (n "ringbuf") (v "0.1.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)))) (h "1g13hjzhc8dm18bff38aarpjg779bxxxrz80dg21hcxffw59dr2z")))

(define-public crate-ringbuf-0.1.1 (c (n "ringbuf") (v "0.1.1") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0gfxff73bymbdyq2rw8ik9ais2gl27kfyykqlrsj2g8rmispnk5k") (f (quote (("default"))))))

(define-public crate-ringbuf-0.1.2 (c (n "ringbuf") (v "0.1.2") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "09zpyy3r5l5waidqc1k3n04pw2j873bqfs6qqqgjkg268l8a0w9g") (f (quote (("default"))))))

(define-public crate-ringbuf-0.1.3 (c (n "ringbuf") (v "0.1.3") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0m191f27amp1jghxr2shh83wl447n6ndskq9wf4v9fqaaji5qhzh") (f (quote (("default"))))))

(define-public crate-ringbuf-0.1.4 (c (n "ringbuf") (v "0.1.4") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0iv8kicriwncygk1xha3ywrpaxiy0j783160df0abw2z9p3md12q") (f (quote (("default"))))))

(define-public crate-ringbuf-0.1.5 (c (n "ringbuf") (v "0.1.5") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0w65brr96j4gw33ra9pljlad5i1mmvgmmq7sfl7ydkmr9v5mc206") (f (quote (("default"))))))

(define-public crate-ringbuf-0.1.6 (c (n "ringbuf") (v "0.1.6") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "12c7zgs6yv3ds5jg1j0qdjf83rdi2acj2xn8fr404a9zns0gs4g5") (f (quote (("default"))))))

(define-public crate-ringbuf-0.1.7 (c (n "ringbuf") (v "0.1.7") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1vzz2kdi6jh9h6z202paj15cxpng4n1y3ck65d594ldjy84b38l8") (f (quote (("default"))))))

(define-public crate-ringbuf-0.1.8 (c (n "ringbuf") (v "0.1.8") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0xzydvmy1iqrclfsbwnzr15vbnwhm3hklg7l44jra6yylr6lsx8i") (f (quote (("default")))) (y #t)))

(define-public crate-ringbuf-0.1.9 (c (n "ringbuf") (v "0.1.9") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0h8wifzjk5smgszhmwpkxmsw7nwdpwk81za3hgzlllzgkg5brifl") (f (quote (("default"))))))

(define-public crate-ringbuf-0.2.0 (c (n "ringbuf") (v "0.2.0") (h "0rj8w705gdix8cgkhv4b1kkm52qphpl2c2qnbpamiy00csbzv5rb") (f (quote (("default") ("benchmark")))) (y #t)))

(define-public crate-ringbuf-0.2.1 (c (n "ringbuf") (v "0.2.1") (h "1bjffxhg6vhbda25ansg3zqbi27z1w1baaq196cf7p7vgkc2javw") (f (quote (("default") ("benchmark"))))))

(define-public crate-ringbuf-0.2.2 (c (n "ringbuf") (v "0.2.2") (h "11kqx10a1a6vd4sldn7cj06chiml68s5icxnwa6fpanw4l0capb0") (f (quote (("default") ("benchmark"))))))

(define-public crate-ringbuf-0.2.3 (c (n "ringbuf") (v "0.2.3") (h "0qp0ma3bp645li3rfmzvkdjsniw0ma2ahi9dcsg0jh9k9j61s34k") (f (quote (("std") ("default" "std") ("benchmark"))))))

(define-public crate-ringbuf-0.2.4 (c (n "ringbuf") (v "0.2.4") (h "1bd954287jdppfcvxvzqmc1dyqk10fg9d8fcdnp0nn45ldfnlf95") (f (quote (("std") ("default" "std") ("benchmark"))))))

(define-public crate-ringbuf-0.2.5 (c (n "ringbuf") (v "0.2.5") (d (list (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)))) (h "015l6ksjxw8hnj5cm7rpfqn4whwg7cppj58h2lmxl421pfl2d3wh") (f (quote (("std") ("default" "std") ("benchmark"))))))

(define-public crate-ringbuf-0.2.6 (c (n "ringbuf") (v "0.2.6") (d (list (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)))) (h "1wxd2sb5b0kjwc5mcv8qrmzl0spfs0agznrxain3xhrr769g6q3c") (f (quote (("std") ("default" "std") ("benchmark"))))))

(define-public crate-ringbuf-0.2.7 (c (n "ringbuf") (v "0.2.7") (d (list (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)))) (h "0i3d757mlq3v5n7j1w3ha7n7fpxvqbxh6j2l4z0mn32a1x4n8c7x") (f (quote (("std") ("default" "std") ("benchmark"))))))

(define-public crate-ringbuf-0.2.8 (c (n "ringbuf") (v "0.2.8") (d (list (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)))) (h "18n2qmbvvxj9s775p6q2dv5s68ndbpvb7fr3mx5fg2gpa26z2npn") (f (quote (("std") ("default" "std") ("benchmark"))))))

(define-public crate-ringbuf-0.3.0-rc.0 (c (n "ringbuf") (v "0.3.0-rc.0") (d (list (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)))) (h "1c2abfrjlmqrxl8rjcda8k9ikkrjvs4zf63dbjkg8qlvmm2fk1lj") (f (quote (("std" "alloc") ("default" "alloc" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.3.0-rc.1 (c (n "ringbuf") (v "0.3.0-rc.1") (d (list (d (n "crossbeam-utils") (r "^0.8.11") (k 0)))) (h "1r1jglzzd6z5aiwphd69lga9m7vydvdhwlr4311sxd9cgr4j9f6w") (f (quote (("std" "alloc") ("default" "alloc" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.3.0 (c (n "ringbuf") (v "0.3.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)))) (h "00xhz6pm7i89qai7dhn0qh17ncmbr2k19yzysdvf6cj74lcaz2jy") (f (quote (("std" "alloc") ("default" "alloc" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.3.1 (c (n "ringbuf") (v "0.3.1") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)))) (h "1sx0r88qdprsh4xvp4idlrpvq28qaaqmc5afcq6bpxyqq7cqvrl9") (f (quote (("std" "alloc") ("default" "alloc" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.3.2 (c (n "ringbuf") (v "0.3.2") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)))) (h "02j2k5rdcnadg8216jrcqfxfvxnf6k73p5fnl9awhfp5r6wi1jlk") (f (quote (("std" "alloc") ("default" "alloc" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.3.3 (c (n "ringbuf") (v "0.3.3") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)))) (h "02lmklwymawzfrgp9wy60yk2v3lkyv2p5v0w40la3lhzim1fvavr") (f (quote (("std" "alloc") ("default" "alloc" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.4.0-rc.0 (c (n "ringbuf") (v "0.4.0-rc.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "once_mut") (r "^0.1.0") (d #t) (k 2)))) (h "0bslnqybjrngfhrfg1dgypwmm8z3hrsz8fdjwjz9rvx0nfic48fq") (f (quote (("test_local") ("std" "alloc") ("default" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.4.0-rc.1 (c (n "ringbuf") (v "0.4.0-rc.1") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "once_mut") (r "^0.1.0") (d #t) (k 2)))) (h "0980701sqz1wdb29sw431nkg3m8vjm3gy6pax7g9cd5ap4pa4vas") (f (quote (("test_local") ("std" "alloc") ("default" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.4.0-rc.2 (c (n "ringbuf") (v "0.4.0-rc.2") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "once_mut") (r "^0.1.0") (d #t) (k 2)))) (h "0y99al2rsph74q0vfddaahqk6gh6prb5cq0q6gb54xznwic7v3wv") (f (quote (("test_local") ("std" "alloc") ("default" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.4.0-rc.3 (c (n "ringbuf") (v "0.4.0-rc.3") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "once_mut") (r "^0.1.0") (d #t) (k 2)))) (h "1vy4mq7pjqqnvw5gr132af7670vk9v7wqgl48grd01zg0yl8igdx") (f (quote (("test_local") ("std" "alloc") ("default" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.4.0-rc.4 (c (n "ringbuf") (v "0.4.0-rc.4") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "once_mut") (r "^0.1.0") (d #t) (k 2)))) (h "0gx5zxr8w7h9abmrh5lpq3xqhxv4jlyszz7w6wskdh2acnvqps6h") (f (quote (("test_local") ("std" "alloc") ("default" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.4.0 (c (n "ringbuf") (v "0.4.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "once_mut") (r "^0.1.0") (d #t) (k 2)))) (h "0bwak0fsbp44kqlp9bv9ymqmgan5fq5wnxdk5qmgaqy7yhrbqhi5") (f (quote (("test_local") ("std" "alloc") ("default" "std") ("bench") ("alloc"))))))

(define-public crate-ringbuf-0.4.1 (c (n "ringbuf") (v "0.4.1") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "once_mut") (r "^0.1.0") (d #t) (k 2)))) (h "0dinbij3k77sb4hf4d3f0sfjav3yrw5gyga9jhr2wgdwcp4f8raw") (f (quote (("test_local") ("std" "alloc") ("default" "std") ("bench") ("alloc"))))))

