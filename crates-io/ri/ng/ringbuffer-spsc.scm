(define-module (crates-io ri ng ringbuffer-spsc) #:use-module (crates-io))

(define-public crate-ringbuffer-spsc-0.1.0 (c (n "ringbuffer-spsc") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)))) (h "0pspc6mllvxcqynnlq72xmb05sxzr0bsf8ljydaw7c54zqlh77ld")))

(define-public crate-ringbuffer-spsc-0.1.1 (c (n "ringbuffer-spsc") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)))) (h "09wfgisa9swsiwc3bh1flvdidjv0w5jyjhm60c4nndj7lnr6r4b0")))

(define-public crate-ringbuffer-spsc-0.1.2 (c (n "ringbuffer-spsc") (v "0.1.2") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)))) (h "1vkf05ws9qhzg6p40qnncklgaw730f59463zkc84lw93x8p6srwd")))

(define-public crate-ringbuffer-spsc-0.1.3 (c (n "ringbuffer-spsc") (v "0.1.3") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)))) (h "18dfrzzzlcz0c4il3fj2b08c4my4y1q53fmd4vryr4lcrnzabds0")))

(define-public crate-ringbuffer-spsc-0.1.4 (c (n "ringbuffer-spsc") (v "0.1.4") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)))) (h "02yx0cdyr72svm99nfm9agg2cfynljcsh3jkcg2499m0wd5b0ma1")))

(define-public crate-ringbuffer-spsc-0.1.5 (c (n "ringbuffer-spsc") (v "0.1.5") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)))) (h "1p97n95w7b9swrfydmxb3ynfj93gmszw8fs8pqcqcfjysw1i1hbm")))

(define-public crate-ringbuffer-spsc-0.1.6 (c (n "ringbuffer-spsc") (v "0.1.6") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)))) (h "0l4fm41mr94sj960v49i9x9q689k8y10fjdmnshqyv1820vs361w")))

(define-public crate-ringbuffer-spsc-0.1.7 (c (n "ringbuffer-spsc") (v "0.1.7") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)))) (h "1b9wqxmz1d82i0ibb0pwz34d6nrbinfhk6i5w5bj86hka75ilw1h") (y #t)))

(define-public crate-ringbuffer-spsc-0.1.8 (c (n "ringbuffer-spsc") (v "0.1.8") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)))) (h "1dk9dcydpa5hggyg0hwc8pp4kjnrl15b49zs5vcgj0im1b17grks")))

(define-public crate-ringbuffer-spsc-0.1.9 (c (n "ringbuffer-spsc") (v "0.1.9") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)))) (h "0wzpqj2i8m58k1hvv71v86qhyzan20nznyklw4p3d8k3ma7r7l9g")))

