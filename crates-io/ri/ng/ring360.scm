(define-module (crates-io ri ng ring360) #:use-module (crates-io))

(define-public crate-ring360-0.1.0 (c (n "ring360") (v "0.1.0") (h "0f5vb5w9h4fbq1w7kc46r4cdf0fi9cf9rzn1pjpqmjwm1nfj8w4x") (y #t)))

(define-public crate-ring360-0.2.0 (c (n "ring360") (v "0.2.0") (h "0pvy8q3b9blpmnkchakmvwni00zcy4335xylr9a88hqcsvqiagb1") (y #t)))

(define-public crate-ring360-0.2.1 (c (n "ring360") (v "0.2.1") (h "0yvc7yjij2kgzn3bniwmnxssmfryg2vdpkavzld173lspcxj5jyk") (y #t)))

(define-public crate-ring360-0.2.2 (c (n "ring360") (v "0.2.2") (h "15v9c58slfr2abvdhmcsn6y3fs3gj4xnqzj52a327hi1i4wws50j") (y #t)))

(define-public crate-ring360-0.2.3 (c (n "ring360") (v "0.2.3") (h "1wqdrby0059i1psqxm1k24mm9027lq2cg6f1f5zcx9vf5s2w314r") (y #t)))

(define-public crate-ring360-0.2.4 (c (n "ring360") (v "0.2.4") (h "1x83jnx9igavkwqwkrggyr13mns7g8rpnvl6p1wwggnm167dhibp") (y #t)))

(define-public crate-ring360-0.2.5 (c (n "ring360") (v "0.2.5") (h "0d8mbbja75phxkr995iyakxsgzai82sfcqh21q3r8xiqam381czh") (y #t)))

(define-public crate-ring360-0.2.6 (c (n "ring360") (v "0.2.6") (h "08bxnk1iczlb5vpldn2m78bmcw2vr7mwc0r83pv3ylhhax07ch20") (y #t)))

(define-public crate-ring360-0.2.7 (c (n "ring360") (v "0.2.7") (h "13f66kip8a8l84fqdizr9vdrng16jypwmhpyp02h0iy5h2bsjkm9") (y #t)))

(define-public crate-ring360-0.2.8 (c (n "ring360") (v "0.2.8") (h "1l9mhgwpd521ca4a6j7x25mxnkxqrv8aqsl8j0zdh8w1iha78ck8") (y #t)))

(define-public crate-ring360-0.2.9 (c (n "ring360") (v "0.2.9") (h "0xa6p01nn9ki2506rj0zgxlhr0m2kii1kx8h1kgm14gg6m4pki53") (y #t)))

(define-public crate-ring360-0.2.10 (c (n "ring360") (v "0.2.10") (h "1hi0ppldqjblcmddpmi550s7ga8xp4ii5hcyyd8xpwqawx0aw01g") (y #t)))

(define-public crate-ring360-0.2.11 (c (n "ring360") (v "0.2.11") (h "1z7m8j7ycd80nrklhmyrb5v64m974bqgkcsrb7lqvafd8n3bikdq")))

