(define-module (crates-io ri ng ringtail) #:use-module (crates-io))

(define-public crate-ringtail-0.1.0 (c (n "ringtail") (v "0.1.0") (h "1l31qbizggv3bpmlzp2czfla0zhvjkc92gfnp1lcgl5mrp2rm1zp")))

(define-public crate-ringtail-0.1.1 (c (n "ringtail") (v "0.1.1") (h "0vl18rshdab2ip39qc71nnmydw7f19224gwwn8wl0qsrqb13jfc2")))

(define-public crate-ringtail-0.2.0 (c (n "ringtail") (v "0.2.0") (h "1qcrhmc7gyv56c93fw6affnvxai20baiwi8qdsq016b3kdsxlym0")))

(define-public crate-ringtail-0.2.1 (c (n "ringtail") (v "0.2.1") (h "1bs4zsq5vwzz38pm41rkjyhcmh8293qxqkly2kw6r0zgihysm77h")))

(define-public crate-ringtail-0.2.2 (c (n "ringtail") (v "0.2.2") (h "0v25m4hrlkzy4i6yqxqfkhmi542pf56w6wrrvy6z2yycpr60qrb8")))

(define-public crate-ringtail-0.3.0 (c (n "ringtail") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0dxj953wfqmd7bhzici1bfjrlxz2lgpdjnr56fs34y4gkldmq891")))

