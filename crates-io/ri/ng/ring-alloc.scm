(define-module (crates-io ri ng ring-alloc) #:use-module (crates-io))

(define-public crate-ring-alloc-0.0.0 (c (n "ring-alloc") (v "0.0.0") (h "0vyv4aizm9s0rlvbv2wk2f3vycv5fifnchy6qyjipszqpzw35lah")))

(define-public crate-ring-alloc-0.1.0 (c (n "ring-alloc") (v "0.1.0") (d (list (d (n "allocator-api2") (r "^0.2.13") (d #t) (k 0)) (d (n "allocator-api2-tests") (r "^0.2.13") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1d6fyfjiqjzlbk0j3hw96lz2h35cw9hs2fkdkf05lzvap7klsfr9") (f (quote (("nightly" "allocator-api2/nightly" "allocator-api2-tests/nightly") ("default" "std") ("alloc")))) (s 2) (e (quote (("std" "alloc" "dep:parking_lot"))))))

(define-public crate-ring-alloc-0.2.0 (c (n "ring-alloc") (v "0.2.0") (d (list (d (n "allocator-api2") (r "^0.2.13") (d #t) (k 0)) (d (n "allocator-api2-tests") (r "^0.2.13") (d #t) (k 2)) (d (n "bumpalo") (r "^3.7") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1ddrlqblqy3bfvw5igbc9s1abmp1i4if10da1rfggp78z8jyy2h5") (f (quote (("nightly" "allocator-api2/nightly" "allocator-api2-tests/nightly" "bumpalo/allocator_api") ("default" "std") ("alloc")))) (s 2) (e (quote (("std" "alloc" "dep:parking_lot"))))))

