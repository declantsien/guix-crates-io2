(define-module (crates-io ri ng ringstack) #:use-module (crates-io))

(define-public crate-ringstack-0.1.0 (c (n "ringstack") (v "0.1.0") (h "0cpxb9pzhy7zazxh4s1h84399fbidabfki1dnccbq3j3sym73l9z")))

(define-public crate-ringstack-0.1.1 (c (n "ringstack") (v "0.1.1") (h "02l9nrc82ps11x1dbss54905a8a5gk8gnyka24wkbc1s0v36li4r")))

(define-public crate-ringstack-0.2.0 (c (n "ringstack") (v "0.2.0") (h "0b3mf07qpw94a4i5x9c5yi8a8b3qdqva9v4hnml7hbr87qb16sw9")))

