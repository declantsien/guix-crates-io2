(define-module (crates-io ri ng ring-vec) #:use-module (crates-io))

(define-public crate-ring-vec-0.1.0 (c (n "ring-vec") (v "0.1.0") (h "1y2i6hf8xh1c6b6zq8zbiafyipw97rn7rqk33qd4cvp2akq459cb")))

(define-public crate-ring-vec-0.1.1 (c (n "ring-vec") (v "0.1.1") (h "1w9rqv9c98cry5hc7a0z8i1wjlpvb5s1ii7879izhc0fmy48r8c9")))

