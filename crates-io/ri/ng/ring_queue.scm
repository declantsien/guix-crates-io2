(define-module (crates-io ri ng ring_queue) #:use-module (crates-io))

(define-public crate-ring_queue-0.1.0 (c (n "ring_queue") (v "0.1.0") (h "1j74rsq0bfdcpy9p8ngxwi543fj9bjlwvw960hj6bp5bv68i1fm4")))

(define-public crate-ring_queue-0.2.0 (c (n "ring_queue") (v "0.2.0") (h "15a3bsml7k20x3h59b2z2dcwhcqf1n23slvvbn8fb206qypr5mmj")))

