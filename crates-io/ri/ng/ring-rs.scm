(define-module (crates-io ri ng ring-rs) #:use-module (crates-io))

(define-public crate-ring-rs-0.0.0 (c (n "ring-rs") (v "0.0.0") (d (list (d (n "blake2") (r "^0.8") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0al0mgwbi7s61jl8kc4dg473kzkbyr2qgmy6c1fzz5v7yq1876rl")))

