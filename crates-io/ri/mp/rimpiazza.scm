(define-module (crates-io ri mp rimpiazza) #:use-module (crates-io))

(define-public crate-rimpiazza-0.1.0 (c (n "rimpiazza") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "040gnbv02ciqprnwb1bs0mnamrqlp15vx05zm8nyn412ng1if74i")))

(define-public crate-rimpiazza-0.1.1 (c (n "rimpiazza") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0c9f7hqsynpafx65wby9k4f54s7l9yxnqp4153lixngdhyk0lwav")))

(define-public crate-rimpiazza-0.2.0 (c (n "rimpiazza") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17l7w9pp16jy08sm23dxiks1izym8d59drzs7kv01npqddp846ww")))

