(define-module (crates-io ri fl rifle) #:use-module (crates-io))

(define-public crate-rifle-0.1.0 (c (n "rifle") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "starknet") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1v84amblphjbwpvziffz25msg8jl1hf0372hjnivk7gsj7cq6knj")))

