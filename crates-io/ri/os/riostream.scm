(define-module (crates-io ri os riostream) #:use-module (crates-io))

(define-public crate-riostream-0.1.0 (c (n "riostream") (v "0.1.0") (h "1whd2gykk7p40q1a2cr5dzd0nzk0x5hpp0nyf13sk6vjchyajsag")))

(define-public crate-riostream-0.1.1 (c (n "riostream") (v "0.1.1") (h "00bcpi4z4hyj8nr9x3yb0v842nh65n8dik0dacd5wgkrkp7iczp5")))

(define-public crate-riostream-0.1.2 (c (n "riostream") (v "0.1.2") (h "1iaiqiw1mi97sir6g2ij2ac1mn09r3w0mm0jai17k6dksbqrhmpm")))

(define-public crate-riostream-0.1.3 (c (n "riostream") (v "0.1.3") (h "1mbzlpdb3hga347qsnijpm2wi2dvsnnm0vxwhvxxbh2i6ssvlpa2")))

