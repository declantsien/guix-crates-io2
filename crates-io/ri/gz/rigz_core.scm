(define-module (crates-io ri gz rigz_core) #:use-module (crates-io))

(define-public crate-rigz_core-0.0.1 (c (n "rigz_core") (v "0.0.1") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a4jvjdf4bvzs6b0lii4lhx9q89rkcxfp8wy93jw3qpffc5f38mg")))

