(define-module (crates-io ri gz rigz_parse) #:use-module (crates-io))

(define-public crate-rigz_parse-0.0.1 (c (n "rigz_parse") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "pest") (r "^2.7.10") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zac9ypwx8jqw69dvhd1q14kmkgrlrwzbhxxm25s78pg7xgcbmz1")))

