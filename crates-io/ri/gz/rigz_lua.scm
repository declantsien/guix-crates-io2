(define-module (crates-io ri gz rigz_lua) #:use-module (crates-io))

(define-public crate-rigz_lua-0.0.1 (c (n "rigz_lua") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("lua54" "serialize"))) (d #t) (k 0)) (d (n "rigz_core") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)))) (h "0hwik9m1503br8gnzqsh0h5yfsbnd9v1hrwnz2dhz8s2s3k9py4m")))

