(define-module (crates-io ri gz rigz) #:use-module (crates-io))

(define-public crate-rigz-0.0.1 (c (n "rigz") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4.5.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rigz_core") (r "^0.0.1") (d #t) (k 0)) (d (n "rigz_runtime") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "06fg49vc1mar6qhrqnkh6znks70vz3l4s21rq60jpdld7gmdymfv")))

