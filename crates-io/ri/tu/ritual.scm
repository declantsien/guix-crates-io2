(define-module (crates-io ri tu ritual) #:use-module (crates-io))

(define-public crate-ritual-0.0.0 (c (n "ritual") (v "0.0.0") (d (list (d (n "clang") (r "^0.20.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.13.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "ritual_common") (r "^0.0.0") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "18b9jv99algsral86vrg7ypjh5r6s6l5d6ady47pla0n19m3889y")))

