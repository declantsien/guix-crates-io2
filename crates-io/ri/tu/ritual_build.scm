(define-module (crates-io ri tu ritual_build) #:use-module (crates-io))

(define-public crate-ritual_build-0.0.0 (c (n "ritual_build") (v "0.0.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.0.0") (d #t) (k 0)))) (h "0f2wrzm5x75bixxi5zdima5hyirsgppyxwhn0sv378ycxmwxv95i")))

(define-public crate-ritual_build-0.1.0 (c (n "ritual_build") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.1.0") (d #t) (k 0)))) (h "13k8sqh6icpa5sk6143bla77pr225kglgj14hv77gj4n3rjlgjb3")))

(define-public crate-ritual_build-0.1.1 (c (n "ritual_build") (v "0.1.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.1.0") (d #t) (k 0)))) (h "1fgv4ynqjn3aazbxs6ky7yzqqxwgcizj3icm4za474r676n9xzph")))

(define-public crate-ritual_build-0.2.0 (c (n "ritual_build") (v "0.2.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.2.0") (d #t) (k 0)))) (h "1dp4qngrxnm6vmg2pjzlfnsqif2v0k5lajiqv5zlhbxykf0xr55s")))

(define-public crate-ritual_build-0.3.0 (c (n "ritual_build") (v "0.3.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.3.0") (d #t) (k 0)))) (h "0dc84vrf0p53firj7p2h6im1r7rbbg6jf21xaxnnq5r0yxmw71lg")))

(define-public crate-ritual_build-0.4.0 (c (n "ritual_build") (v "0.4.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ritual_common") (r "^0.4.0") (d #t) (k 0)))) (h "19vyzd484r5zvy7r7m1dqjz3ypiwh716j0cqq3slcpyy2pbvc273")))

