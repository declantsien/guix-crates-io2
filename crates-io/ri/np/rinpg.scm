(define-module (crates-io ri np rinpg) #:use-module (crates-io))

(define-public crate-rinpg-0.1.0 (c (n "rinpg") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "1ry3w0rddl38358gjqxn6ac98fkw27bvasj7pv5ynpyi37x8djzw")))

(define-public crate-rinpg-0.1.1 (c (n "rinpg") (v "0.1.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "1n4dp7dwjdyhqij48fdvc862wkv79wch3bymkl4vcly3lqr73bf2")))

(define-public crate-rinpg-0.1.2 (c (n "rinpg") (v "0.1.2") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "0y4bqh0xi6ww4dxx3gq0sk94l9d277hm704c7dsixlcy65a8sckz")))

(define-public crate-rinpg-0.1.3 (c (n "rinpg") (v "0.1.3") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "0qa55iwgi0pk8mxjqig2sgzdnsdpw878iwxmncm3s94g8ygkqiky")))

(define-public crate-rinpg-0.1.4 (c (n "rinpg") (v "0.1.4") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "1wdrnlpjjffg5x9h4daparq0521rx7s8fa16piir66qyx7k0bny2")))

(define-public crate-rinpg-0.1.5 (c (n "rinpg") (v "0.1.5") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "16xq2zw8igqh535lq2cypmgi6fcz3ksbiv3xv5kif85dsc9ljfn3")))

(define-public crate-rinpg-0.1.6 (c (n "rinpg") (v "0.1.6") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "0b1vbq5jgfw2ad65dh11jksqi0sk8wp8zcnr1mmxdds8pnfyabcn")))

(define-public crate-rinpg-0.1.7 (c (n "rinpg") (v "0.1.7") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "1f67ipvn35wiyz48xdfrq6876b08xdbsrzqz0jgn34my0rbx95m1")))

(define-public crate-rinpg-0.1.8 (c (n "rinpg") (v "0.1.8") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "1dw4d4s9gglqzzvd8278yfd676ain7hmmx9xkmsav28msfjdmlma")))

(define-public crate-rinpg-0.1.9 (c (n "rinpg") (v "0.1.9") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "0lakv08r66cm71drbhxxhx45gg127nmf2maw0bh835z05mch1w2w")))

(define-public crate-rinpg-0.1.10 (c (n "rinpg") (v "0.1.10") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "02w4i267m53jn801bvwkyn3gpw5mcqhasffz213myc38y8c1wsym")))

(define-public crate-rinpg-0.1.11 (c (n "rinpg") (v "0.1.11") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)))) (h "1qsz1dd69xiza0lfd594kx1yr7ki82b4gq3sbb0fvn5whzbd3709")))

(define-public crate-rinpg-0.1.12 (c (n "rinpg") (v "0.1.12") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "0kayw4wc5hm607yaf5hr7l1xcj85cwjnprp2y4a03d0ifnjq8g6p")))

(define-public crate-rinpg-0.1.13 (c (n "rinpg") (v "0.1.13") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "16fh8n43blbxl12cgj28hzvh7q7bnd82hr4vk2xklg5vfq2blmqb")))

(define-public crate-rinpg-0.1.14 (c (n "rinpg") (v "0.1.14") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "1blfgkiim46s10h6c9q1vja1fwjaxqrw0nad4xyxpqk0f8xzxdkp")))

(define-public crate-rinpg-0.1.15 (c (n "rinpg") (v "0.1.15") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "0gd5jivgvypd62vidy33qq0dlnk4n29b97bpj6a7jfcmm7kq692q")))

(define-public crate-rinpg-0.1.16 (c (n "rinpg") (v "0.1.16") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "0bjkndzgikg79ggq1cbns1xg3r5149lngxalinh95qvm50963mq9")))

(define-public crate-rinpg-0.1.17 (c (n "rinpg") (v "0.1.17") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "0gasnghqd8k4xd37yqlfkniv70wxd1jpc7y4c1wjrw1i0wjir1yz")))

(define-public crate-rinpg-0.1.18 (c (n "rinpg") (v "0.1.18") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "0hsv6gahdpm44r0r94vwiq8azd848znsq3hn2dk0pgipp3p1ai42")))

(define-public crate-rinpg-0.1.19 (c (n "rinpg") (v "0.1.19") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "0ivhvjkcd6bh3ib20pls8w55ma2563gy337zd5lxryjp5qirnwp3")))

(define-public crate-rinpg-0.1.20 (c (n "rinpg") (v "0.1.20") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "1gr2bd808j97k1yxxc28qhrq0znh94zbdbbr0kd5ax59z4p0vnx5")))

(define-public crate-rinpg-0.1.21 (c (n "rinpg") (v "0.1.21") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "10x5vs0mayq16w54z5si4gc6ry5rkf5h8nc1hkxv70n0i7mb6qa2")))

(define-public crate-rinpg-0.1.22 (c (n "rinpg") (v "0.1.22") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "09vhbswspwnwzaxglhxadchcdlfnr5sf9rh5if8398gsdfqd5ff8")))

(define-public crate-rinpg-0.1.23 (c (n "rinpg") (v "0.1.23") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "0px7r3i4wqx6mawm0km1d6psk5fyv9p74wdmjs3cz0yyrdzavsnl")))

(define-public crate-rinpg-0.1.24 (c (n "rinpg") (v "0.1.24") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "1i0dnsnz0xkd4d2193rba5h4ip0lrbvq524rp94xgdjwcpnqvr9c")))

(define-public crate-rinpg-0.1.25 (c (n "rinpg") (v "0.1.25") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "10d52sbx98w8qhx5s81y3hy624s242hyklls6qlvpfdg5j04hawj")))

