(define-module (crates-io ri sc risc0-zkvm-host-sys) #:use-module (crates-io))

(define-public crate-risc0-zkvm-host-sys-0.1.0 (c (n "risc0-zkvm-host-sys") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-zkvm-prove") (r "^0.1") (d #t) (k 0)) (d (n "risc0-zkvm-verify") (r "^0.2") (d #t) (k 0)))) (h "0gk18hdqr9cl28lcgbzk97v0mcigfqvllpwn56s53mf10bwg2fqg") (l "risc0-zkvm-host-sys")))

(define-public crate-risc0-zkvm-host-sys-0.2.0 (c (n "risc0-zkvm-host-sys") (v "0.2.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-zkvm-prove") (r "^0.1") (d #t) (k 0)) (d (n "risc0-zkvm-verify") (r "^0.2") (d #t) (k 0)))) (h "19fn6fsl08i372s24q6bb3yjhp553jq5diwak5ayj3pw5c801dxx") (l "risc0-zkvm-host-sys")))

(define-public crate-risc0-zkvm-host-sys-0.4.0 (c (n "risc0-zkvm-host-sys") (v "0.4.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-zkvm-prove") (r "^0.4") (d #t) (k 0)) (d (n "risc0-zkvm-verify") (r "^0.4") (d #t) (k 0)))) (h "007yx96d2jn3mpiliihhvl11vky3prhiqv0k01bpxxqr4l434bgb") (l "risc0-zkvm-host-sys")))

(define-public crate-risc0-zkvm-host-sys-0.6.0 (c (n "risc0-zkvm-host-sys") (v "0.6.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-zkvm-prove") (r "^0.6") (d #t) (k 0)) (d (n "risc0-zkvm-verify") (r "^0.6") (f (quote ("cxx"))) (d #t) (k 0)))) (h "1vf3z2iwx8c0kw525vdf7hwdcvqhz7cr410vqbbj28idd24n97i3") (l "risc0-zkvm-host-sys")))

