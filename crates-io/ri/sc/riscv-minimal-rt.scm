(define-module (crates-io ri sc riscv-minimal-rt) #:use-module (crates-io))

(define-public crate-riscv-minimal-rt-0.5.0 (c (n "riscv-minimal-rt") (v "0.5.0") (d (list (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)) (d (n "riscv-minimal-rt-macros") (r "^0.1.5") (d #t) (k 0)))) (h "1l8rg8z0dld9j6q2bzxq5j7c9gzj3fcwvcynhmib1sb4y623zj5p") (f (quote (("interrupts") ("inline-asm" "riscv/inline-asm") ("default" "compressed-isa") ("compressed-isa"))))))

