(define-module (crates-io ri sc risc0-sys) #:use-module (crates-io))

(define-public crate-risc0-sys-0.13.0 (c (n "risc0-sys") (v "0.13.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.13.0") (k 1)) (d (n "risc0-core") (r "^0.13.0") (k 0)))) (h "1h4s20p7mk331mkjsk5f18n8fi12h35lh5bd17kp4qg0mdvgwx9p") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.14.0-rc.1 (c (n "risc0-sys") (v "0.14.0-rc.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.14.0-rc.1") (k 1)) (d (n "risc0-core") (r "^0.14.0-rc.1") (k 0)))) (h "1y2v1syn96x15razp3c7jsb16chmh75hxj1f2x4qhngws0zdbavz") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.14.0 (c (n "risc0-sys") (v "0.14.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.14.0") (k 1)) (d (n "risc0-core") (r "^0.14.0") (k 0)))) (h "0s8kax1la2pjkg6qb6zdwlc4zmlh0hfhd8r6na7xknlrf5r60s5r") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.15.0-rc.1 (c (n "risc0-sys") (v "0.15.0-rc.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.15.0-rc.1") (k 1)) (d (n "risc0-core") (r "^0.15.0-rc.1") (k 0)))) (h "1rdi56l48w1j73mc4iv7lvjyw0by6wk07mhrf1ri85cxn5d2qk6y") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.15.0 (c (n "risc0-sys") (v "0.15.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.15.0") (k 1)) (d (n "risc0-core") (r "^0.15.0") (k 0)))) (h "1iwd17w9vnqicdads52vvayrzb7z9c0k9j4dg708ql3n9fa6nzhb") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.15.1 (c (n "risc0-sys") (v "0.15.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.15.1") (k 1)) (d (n "risc0-core") (r "^0.15.1") (k 0)))) (h "1hbd26j7ikdyz1njk1a2r8dc3cy01n6m2lkyhja03fns2cid87mn") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.15.2 (c (n "risc0-sys") (v "0.15.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.15.2") (k 1)) (d (n "risc0-core") (r "^0.15.2") (k 0)))) (h "1rf5k5zgz85kfsp18aarq2q40l41djdfpsr58v8sf9ziy57sihzb") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.15.3 (c (n "risc0-sys") (v "0.15.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.15.3") (k 1)) (d (n "risc0-core") (r "^0.15.3") (k 0)))) (h "02mga0fqi11cfjb60kmd2skri6kcy1nq2qrmg7s78iff7256fa1p") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.16.0 (c (n "risc0-sys") (v "0.16.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.16.0") (k 1)) (d (n "risc0-core") (r "^0.16.0") (k 0)))) (h "0i25jr0gazmcac0p9433r9vlxlpabyfikx4iff991skd7l0mlwyk") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.16.1 (c (n "risc0-sys") (v "0.16.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.16.1") (k 1)) (d (n "risc0-core") (r "^0.16.1") (k 0)))) (h "0l4cymzfcxmpp8z08sc40qwl31yds01ap504lsxld1lxxmbz527z") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.17.0 (c (n "risc0-sys") (v "0.17.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.17.0") (k 1)) (d (n "risc0-core") (r "^0.17.0") (k 0)))) (h "0z192i0n99i2vfbbnhs4dqxhj6z4qxp7i5md55rdw381y8vgmn7c") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.18.0 (c (n "risc0-sys") (v "0.18.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.18.0") (k 1)) (d (n "risc0-core") (r "^0.18.0") (k 0)))) (h "1agkwh1c782fd0xp08cksvqd4wii9f8j4xzm030k57n7xg10ilyn") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.19.0-alpha.1 (c (n "risc0-sys") (v "0.19.0-alpha.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "risc0-build-kernel") (r "^0.19.0-alpha.1") (k 1)) (d (n "risc0-core") (r "^0.19.0-alpha.1") (k 0)))) (h "1ginxblnrhdhyqd1r6c4lk47ckryyy47a4mj6fghhxi2rnb22mhg") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.19.0-rc.1 (c (n "risc0-sys") (v "0.19.0-rc.1") (d (list (d (n "risc0-build-kernel") (r "^0.19.0-rc.1") (k 1)))) (h "07jf88p5jk45qs8piy32pzk3q3mpnlblw5zyd6kr0iass618szk0") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.19.0-rc.2 (c (n "risc0-sys") (v "0.19.0-rc.2") (d (list (d (n "risc0-build-kernel") (r "^0.19.0-rc.2") (k 1)))) (h "1qdvmxyrv3aqxqz9fxr5kzd9kflrrm0p9yq3pk995jlw8lg2h11l") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.19.0-rc.3 (c (n "risc0-sys") (v "0.19.0-rc.3") (d (list (d (n "risc0-build-kernel") (r "^0.19.0-rc.3") (k 1)))) (h "0mwjb4v234wnhf87xdliqwsn0df3rpzx0miazc38gdzc0gxagh8w") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.19.0-rc.4 (c (n "risc0-sys") (v "0.19.0-rc.4") (d (list (d (n "risc0-build-kernel") (r "^0.19.0-rc.4") (k 1)))) (h "0bd8wdh5z43z54xws71amwqak57nmvnad7jxgfdz1kxidfxnvy49") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.19.0 (c (n "risc0-sys") (v "0.19.0") (d (list (d (n "risc0-build-kernel") (r "^0.19.0") (k 1)))) (h "1ai7483r41ikkx6qsqi269r7597ahw3j81ncjsqy3x34dhc05w61") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.19.1-rc.1 (c (n "risc0-sys") (v "0.19.1-rc.1") (d (list (d (n "risc0-build-kernel") (r "^0.19.1-rc.1") (k 1)))) (h "16m0dwfwqx6wjf679c96mcqc8dr5yg1ka65w6y6wpbx89azk5w5b") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.19.1 (c (n "risc0-sys") (v "0.19.1") (d (list (d (n "risc0-build-kernel") (r "^0.19.1") (k 1)))) (h "1b2qqkf8nr085nsdvzm3i95wxwrbgk5d5cwg987qcfp5aly55852") (f (quote (("metal") ("default") ("cuda")))) (l "risc0-sys")))

(define-public crate-risc0-sys-0.20.0-rc.1 (c (n "risc0-sys") (v "0.20.0-rc.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^0.20.0-rc.1") (k 1)) (d (n "sppark") (r "^0.1") (o #t) (d #t) (k 0) (p "risc0-sppark")))) (h "0sd748axqqkhy4lqh1c31ihag2qa73b1sirb8q2dfyp7nb4i0plj") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-0.20.0-rc.2 (c (n "risc0-sys") (v "0.20.0-rc.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^0.20.0-rc.2") (k 1)) (d (n "sppark") (r "^0.1") (o #t) (d #t) (k 0) (p "risc0-sppark")))) (h "1w4gpb50kfa1sbr5nnc3kl80389p9wcpql8923a9p91q1d9hdlxi") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-0.20.0-rc.3 (c (n "risc0-sys") (v "0.20.0-rc.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^0.20.0-rc.3") (k 1)) (d (n "sppark") (r "^0.1") (o #t) (d #t) (k 0) (p "risc0-sppark")))) (h "1mhqj5pqsirhgnicgf14b3y9cr8s1dhql6bjpkw3fxly77ai6yc4") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-0.20.0-rc.4 (c (n "risc0-sys") (v "0.20.0-rc.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^0.20.0-rc.4") (k 1)) (d (n "sppark") (r "^0.1") (o #t) (d #t) (k 0) (p "risc0-sppark")))) (h "0knpfrshwbi7rldqnxwprvabg7plvsdv1i5frirhpcyp32bxax0y") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-0.20.0 (c (n "risc0-sys") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^0.20.0") (k 1)) (d (n "sppark") (r "^0.1") (o #t) (d #t) (k 0) (p "risc0-sppark")))) (h "1xvf53zj8a511q9w431kn63cfzzr0j2mgkpjb2adzmdm17hwq3si") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-0.20.1 (c (n "risc0-sys") (v "0.20.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^0.20.1") (k 1)) (d (n "sppark") (r "^0.1") (o #t) (d #t) (k 0) (p "risc0-sppark")))) (h "0nsf9q72nsqgb66n9bac377wxv810pcks824gqca0hn5rvmclh4c") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-0.21.0-rc.1 (c (n "risc0-sys") (v "0.21.0-rc.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^0.21.0-rc.1") (k 1)) (d (n "sppark") (r "^0.1") (o #t) (d #t) (k 0) (p "risc0-sppark")))) (h "12c5qj3153kbcav4di4vzh8xq73dcbgl0ma0mx07rs4d7lmdadc2") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-0.21.0-rc.2 (c (n "risc0-sys") (v "0.21.0-rc.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^0.21.0-rc.2") (k 1)) (d (n "sppark") (r "^0.1") (o #t) (d #t) (k 0) (p "risc0-sppark")))) (h "1hpf4j7ck1pqzlq3vdji27g3blgwds1d2z28pf3bij7ml7wydnh3") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-0.21.0 (c (n "risc0-sys") (v "0.21.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^0.21.0") (k 1)) (d (n "sppark") (r "^0.1") (o #t) (d #t) (k 0) (p "risc0-sppark")))) (h "18gyk4733436cvbjn1y55mn3x3d5g8m7k67zr7qkq7ljyhb8h5k1") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-1.0.0-rc.3 (c (n "risc0-sys") (v "1.0.0-rc.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^1.0.0-rc.3") (k 1)) (d (n "sppark") (r "^0.1") (o #t) (d #t) (k 0) (p "risc0-sppark")))) (h "0gghfwnrzcvw7ajzx6zr4alqqlvvmdh9ykfffwrmaw59dlp4ixcz") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-1.0.0-rc.4 (c (n "risc0-sys") (v "1.0.0-rc.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^1.0.0-rc.4") (k 1)) (d (n "sppark") (r "^0.1") (o #t) (d #t) (k 0) (p "risc0-sppark")))) (h "1pll5zdyr0q387bdywd65rpswchbgcj2ijbh0b3k87h2bx5470mq") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-1.0.0-rc.5 (c (n "risc0-sys") (v "1.0.0-rc.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^1.0.0-rc.5") (k 1)) (d (n "sppark") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "19x836g1m2mksc2148a73magch44pkq8ayb87i3bp3yyl8ajj9vg") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

(define-public crate-risc0-sys-1.0.0-rc.6 (c (n "risc0-sys") (v "1.0.0-rc.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cust") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "risc0-build-kernel") (r "^1.0.0-rc.6") (k 1)) (d (n "sppark") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "19xsd7lzkff0yilmr0ailbcc6h54w0gr2bzs7xb2c3nl3js783ks") (f (quote (("metal") ("default")))) (l "risc0-sys") (s 2) (e (quote (("cuda" "dep:cust" "dep:sppark"))))))

