(define-module (crates-io ri sc riscv-atomic-emulation-trap) #:use-module (crates-io))

(define-public crate-riscv-atomic-emulation-trap-0.1.0 (c (n "riscv-atomic-emulation-trap") (v "0.1.0") (d (list (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.1") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "1w19j0j7v7ydnh951j2knq1qx50bai4zmhxvk9fk0l09s4syvn49")))

(define-public crate-riscv-atomic-emulation-trap-0.1.1 (c (n "riscv-atomic-emulation-trap") (v "0.1.1") (d (list (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "04w3ph1idqf7hjwasvpg0iqagg7xgkcrzhzqdkavhbvq0w86xajd")))

(define-public crate-riscv-atomic-emulation-trap-0.2.0 (c (n "riscv-atomic-emulation-trap") (v "0.2.0") (d (list (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "02dhi90zp2ylswy2jjkn8yxv3471axasl9xhan5n19ajbcgifky1")))

(define-public crate-riscv-atomic-emulation-trap-0.3.0 (c (n "riscv-atomic-emulation-trap") (v "0.3.0") (d (list (d (n "riscv") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "1sl7b76sdmd6i7g8z600jzmp3bkhngsn72y50gv4c481cx7k6jc1")))

(define-public crate-riscv-atomic-emulation-trap-0.3.1 (c (n "riscv-atomic-emulation-trap") (v "0.3.1") (d (list (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "17r3s5fl905b9icijmw9bs0bnqcizqp8g0y66iimd3z5wv1733fs")))

(define-public crate-riscv-atomic-emulation-trap-0.4.0 (c (n "riscv-atomic-emulation-trap") (v "0.4.0") (h "1fnnamqqhfgafpl0z5mpl263islhdxzc6f5ldghyhqiacjjz8ffs")))

(define-public crate-riscv-atomic-emulation-trap-0.4.1 (c (n "riscv-atomic-emulation-trap") (v "0.4.1") (h "0hs1a1wg865qdqwlq5cy3smqvbq97yhs7ibcmp0383z7f1q14ybr")))

