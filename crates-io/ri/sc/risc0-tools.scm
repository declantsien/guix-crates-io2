(define-module (crates-io ri sc risc0-tools) #:use-module (crates-io))

(define-public crate-risc0-tools-1.0.0-rc.2 (c (n "risc0-tools") (v "1.0.0-rc.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^1.0.0-rc.2") (f (quote ("profiler"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "16hh09nbxgvj23myrd44d16qx2c58anjvcb2fsvxp0r1ja1486h0")))

(define-public crate-risc0-tools-0.12.0-rc.1 (c (n "risc0-tools") (v "0.12.0-rc.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.12.0-rc.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1n3pfq5mb65015h9k8pdmhz5fdj560b1vxy513slpppf09mkzmv8")))

(define-public crate-risc0-tools-0.12.0-rc.2 (c (n "risc0-tools") (v "0.12.0-rc.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.12.0-rc.2") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "17mx8arccyn3vizc9kswsh32albfw46vwhll0qn500lg4hb5r8zr")))

(define-public crate-risc0-tools-0.12.0 (c (n "risc0-tools") (v "0.12.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.12.0") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "11la8d3yp80pf6nmjvw2ap65bcv1dv29zsz3r1jg48k6669c9g16")))

(define-public crate-risc0-tools-0.13.0 (c (n "risc0-tools") (v "0.13.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.13.0") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0lqwykij9d2smjgspp5bz4npw2k32yls96djxgwg9532vmm87bki")))

(define-public crate-risc0-tools-0.14.0-rc.1 (c (n "risc0-tools") (v "0.14.0-rc.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.14.0-rc.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0v7s1lncxzi37lg8bw82ik0494p19y1if7p36b6xzj6b1nzkivnx")))

(define-public crate-risc0-tools-0.14.0 (c (n "risc0-tools") (v "0.14.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.14.0") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1qbyndjqv5y2bgmgy1xsazad8sip8dgplmnklyr9lsv92rabk03q")))

(define-public crate-risc0-tools-0.15.0-rc.1 (c (n "risc0-tools") (v "0.15.0-rc.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.15.0-rc.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1fiihy9jnxh5vk08zgv0wrha2c2ni78k89xvcvgassn6zgamcw1x")))

(define-public crate-risc0-tools-0.15.0 (c (n "risc0-tools") (v "0.15.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.15.0") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "18z13kjnwyx8gicixa3ckdfplfm6xkg0w3zmf8qn4vcgj4rylybk")))

(define-public crate-risc0-tools-0.15.1 (c (n "risc0-tools") (v "0.15.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.15.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0syvpim0gkdi5xnqa486rs22i7ilgn4l037hxs4fm4dkgg05m8hp")))

(define-public crate-risc0-tools-0.15.2 (c (n "risc0-tools") (v "0.15.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.15.2") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1bxjk7l9cfhwkg48rl1l4cmgmrhcaiw31zz30qrj1gq935ndjrz7")))

(define-public crate-risc0-tools-0.15.3 (c (n "risc0-tools") (v "0.15.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.15.3") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1kzh266bwjrkgs5105xmgnqlhjb01dyqhf8xf0dlszk5m889c6vf")))

(define-public crate-risc0-tools-0.16.0 (c (n "risc0-tools") (v "0.16.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.16.0") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0k1nh1f4hmfpaliykggz31q74xds83c9b29vq5jdvi9cxx6vkxwv")))

(define-public crate-risc0-tools-0.16.1 (c (n "risc0-tools") (v "0.16.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.16.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1r8nwhrcqz3wmqngp9k7rrdf806lr9y61ly0qhyrh4ksrj4vvjm4")))

(define-public crate-risc0-tools-0.17.0 (c (n "risc0-tools") (v "0.17.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.17.0") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "18zbsdl7bfx95a2nscycwzwnij9pzmg93qsgzmwrydxadyrvcbpi")))

(define-public crate-risc0-tools-0.18.0 (c (n "risc0-tools") (v "0.18.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.18.0") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0idnp19a2j4pf88f7yslj7m3lk2wk3m85lb74wg90hlqgca36p71")))

(define-public crate-risc0-tools-0.19.0-alpha.1 (c (n "risc0-tools") (v "0.19.0-alpha.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.19.0-alpha.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "05xylq4sigvs8brq161q9a7qfhfp4fkypw0i00p9va2ixsjxmyhk")))

(define-public crate-risc0-tools-0.19.0-rc.1 (c (n "risc0-tools") (v "0.19.0-rc.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.19.0-rc.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "04zxa74fqv04rrzfq7vwhrssdngnb8rmg1pg0r70ghyzda4wlggh")))

(define-public crate-risc0-tools-0.19.0-rc.2 (c (n "risc0-tools") (v "0.19.0-rc.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.19.0-rc.2") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0wrcsfb3dw9ny567r9igmj49g34jzm349b2h9wsyibwnx4wvmdzs")))

(define-public crate-risc0-tools-0.19.0-rc.3 (c (n "risc0-tools") (v "0.19.0-rc.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.19.0-rc.3") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "03hps2m7h0dh1cbg5k3fkxhfnpik0y8nbzsd63dgnwxsg6mqx1id")))

(define-public crate-risc0-tools-0.19.0-rc.4 (c (n "risc0-tools") (v "0.19.0-rc.4") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.19.0-rc.4") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0hjldw3ws4xlsmx12l5s5if50qs9mvvzfc4yd2zrqajp7gm0qh0i")))

(define-public crate-risc0-tools-0.19.0 (c (n "risc0-tools") (v "0.19.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.19.0") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0x1h7n33aj2k1lbfhafvnk4b425009r03ygr39xf9cwy4h6w01fg")))

(define-public crate-risc0-tools-0.19.1-rc.1 (c (n "risc0-tools") (v "0.19.1-rc.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.19.1-rc.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0ix8da49v6iwq817jy7ivqgqbk7fyyhw53kvhbvy4i0wm38dx7h4")))

(define-public crate-risc0-tools-0.19.1 (c (n "risc0-tools") (v "0.19.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.19.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1p4m5yg7iaswlfis8m0xy1sgah17ajp7nlgjmxvj419bj0jkxz2l")))

(define-public crate-risc0-tools-0.20.0-rc.1 (c (n "risc0-tools") (v "0.20.0-rc.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.20.0-rc.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1mrkprhk7aicbhrifdh4b2x0daqhxc8qjki2zgmihrlk0bc8mw87")))

(define-public crate-risc0-tools-0.20.0-rc.2 (c (n "risc0-tools") (v "0.20.0-rc.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.20.0-rc.2") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1v406cj075w87s7lpv8iza4fr2z178pvs9s9cbfnk94yigkqp9ry")))

(define-public crate-risc0-tools-0.20.0-rc.3 (c (n "risc0-tools") (v "0.20.0-rc.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.20.0-rc.3") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "15i5zxpgad4vfia453mzsjbzjp5x8p0dk49j3vwxl0m0lqx62xnq")))

(define-public crate-risc0-tools-0.20.0-rc.4 (c (n "risc0-tools") (v "0.20.0-rc.4") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.20.0-rc.4") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1c21nm8fg4kyljp9dzad8rxjgxvggfxbqxb80gqipylfb6lyl7n5")))

(define-public crate-risc0-tools-0.20.0 (c (n "risc0-tools") (v "0.20.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.20.0") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1kx4cna7b2f3zgbahy4knxvbgak7sywcn43a7ihlh5m862j4adzl")))

(define-public crate-risc0-tools-0.20.1 (c (n "risc0-tools") (v "0.20.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.20.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "19balb2vkscrm42y8kyifv7myqdkw4m8mp3p3bsga7mkhjgpyg3p")))

(define-public crate-risc0-tools-0.21.0-rc.1 (c (n "risc0-tools") (v "0.21.0-rc.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.21.0-rc.1") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1vvci9kccf7rl950922gq7r81w51k8n9pqf55vi8b99zh9va5kcf")))

(define-public crate-risc0-tools-0.21.0-rc.2 (c (n "risc0-tools") (v "0.21.0-rc.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.21.0-rc.2") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1h1lgmnlr3hkfsn5yd5ir67sjkkxjsxj1d1s8b8x67wwkkzy1df4")))

(define-public crate-risc0-tools-0.21.0 (c (n "risc0-tools") (v "0.21.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.21.0") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0qfcj0c669jwvp26r3gpr8z3xjcp3i9bn0xayfk3lawxz23c3xb8")))

(define-public crate-risc0-tools-1.0.0-rc.3 (c (n "risc0-tools") (v "1.0.0-rc.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^1.0.0-rc.3") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0354gh97fk4x30yxhw0gxf4vaasasgl1pl28vvam5wz36f27c0p4")))

(define-public crate-risc0-tools-1.0.0-rc.4 (c (n "risc0-tools") (v "1.0.0-rc.4") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^1.0.0-rc.4") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0w591gnlhdkkwmcm2z2hjqkb61rjg36ajfxrlap9vgs5p6nhw347")))

(define-public crate-risc0-tools-1.0.0-rc.5 (c (n "risc0-tools") (v "1.0.0-rc.5") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^1.0.0-rc.5") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0h433nyi5s4x822xg50p086q78lrhb6p25ijvvwzfbnyqbwfp4v2")))

(define-public crate-risc0-tools-1.0.0-rc.6 (c (n "risc0-tools") (v "1.0.0-rc.6") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm") (r "^1.0.0-rc.6") (f (quote ("default"))) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1r56is9pi2w89933jdk3pawwyl4zglvqlvxc9pj5ik4vwdwljad7")))

