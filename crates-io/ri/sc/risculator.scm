(define-module (crates-io ri sc risculator) #:use-module (crates-io))

(define-public crate-RISCulator-0.1.0 (c (n "RISCulator") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "14sabl8nxa7h2zay0qknal9fwnrylg7vg7xxn38l30izzxi4v6ly")))

