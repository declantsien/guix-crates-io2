(define-module (crates-io ri sc risc0-zkp-sys) #:use-module (crates-io))

(define-public crate-risc0-zkp-sys-0.7.0-dev.1 (c (n "risc0-zkp-sys") (v "0.7.0-dev.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-core-sys") (r "^0.7.0-dev.1") (d #t) (k 0)) (d (n "tbb") (r "^0.7.0-dev.1") (d #t) (k 0)))) (h "0qwki46dzz6x58fjwhy2amqn325by6c3h6h9b279cwpxm77hrf2w") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.7.0-dev.2 (c (n "risc0-zkp-sys") (v "0.7.0-dev.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-core-sys") (r "^0.7.0-dev.1") (d #t) (k 0)) (d (n "tbb") (r "^0.7.0-dev.1") (d #t) (k 0)))) (h "06dvdgvgd3jrbh2fqf2kw030c09shwaq1s01hh9d129jqaj66dn8") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.7.0-rc.1 (c (n "risc0-zkp-sys") (v "0.7.0-rc.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-core-sys") (r "^0.7.0-dev.1") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.0") (d #t) (k 0)))) (h "0hppjr09qmwb6rrhfbkxzsxsbrgm1fg9fx59gplq6l5f5wx0f7v1") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.7.0 (c (n "risc0-zkp-sys") (v "0.7.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-core-sys") (r "^0.7.0-dev.1") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.0") (d #t) (k 0)))) (h "0kl3mrm5m9x868d8b8qc33yq7qczvzs5ah2y1rz7lcfj93sjbig6") (y #t) (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.7.1 (c (n "risc0-zkp-sys") (v "0.7.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-core-sys") (r "^0.7") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.0") (d #t) (k 0)))) (h "1d51ls2c8jqlydhingg5wi683jyw1151lh64qy44w8dxqkqndmd7") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.7.2 (c (n "risc0-zkp-sys") (v "0.7.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-core-sys") (r "^0.7") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.1") (d #t) (k 0)))) (h "1000ckkwlfw0r2y0jywldj1rch08akl2xs061isg406qygx8cjy4") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.8.0 (c (n "risc0-zkp-sys") (v "0.8.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-core-sys") (r "^0.8") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.1") (d #t) (k 0)))) (h "1kg5baf7fvq181sdgi71hfcnlljmbxwq0fgf4770wiwdmk98cw05") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.9.0 (c (n "risc0-zkp-sys") (v "0.9.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-core-sys") (r "^0.9") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.1") (d #t) (k 0)))) (h "1kji7rgfs7dq1lb97vnf9k9vly2nmgai2fd2h0wr18b5bvjvlzpy") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.10.0 (c (n "risc0-zkp-sys") (v "0.10.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-core-sys") (r "^0.10") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.1") (d #t) (k 0)))) (h "10fk648d02zq631gy9djgz3d9nkm7sg7w6ll5kqvw1iijg5rifn6") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.11.0-rc.1 (c (n "risc0-zkp-sys") (v "0.11.0-rc.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-core-sys") (r "^0.11.0-rc.1") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.1") (d #t) (k 0)))) (h "1m5vj0zmv0dfzqfxdl6vaz4z51zgydkz55946nzg017vvmdhqygv") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.11.0-rc.2 (c (n "risc0-zkp-sys") (v "0.11.0-rc.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-core-sys") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.1") (d #t) (k 0)))) (h "1da2az9fdwhxzyqiyl444cf7z79rfa2wpqs3h45aj4vpx7717m83") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.11.0-rc.3 (c (n "risc0-zkp-sys") (v "0.11.0-rc.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-core-sys") (r "^0.11.0-rc.3") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.1") (d #t) (k 0)))) (h "14rnf4qxpsknlnsmjv6vrxxsi2dyxkqbys807nxr7lvim4gpqf6s") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.11.0-rc.4 (c (n "risc0-zkp-sys") (v "0.11.0-rc.4") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-core-sys") (r "^0.11.0-rc.4") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.1") (d #t) (k 0)))) (h "08qkvahcs59y8nygrw6c3d0g7ndpl71hjzf1wsxlqrcx3yhl56km") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.11.0 (c (n "risc0-zkp-sys") (v "0.11.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-core-sys") (r "^0.11.0-rc.4") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.1") (d #t) (k 0)))) (h "0mxmb54lwgv6rdjx7717cmgdm2am46hw7p8g7ykjclphxzqjm9i1") (l "risc0-zkp-sys")))

(define-public crate-risc0-zkp-sys-0.11.1 (c (n "risc0-zkp-sys") (v "0.11.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-core-sys") (r "^0.11") (d #t) (k 0)) (d (n "tbb-sys") (r "^1.1") (d #t) (k 0)))) (h "1l06ja2wj02b75481i1hwwvaw0d7azs8kb2dplskzmpff3r8xl08") (l "risc0-zkp-sys")))

