(define-module (crates-io ri sc riscv) #:use-module (crates-io))

(define-public crate-riscv-0.1.0 (c (n "riscv") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0b615l19rvqrqjrbb2sscqzqcmn4j1yax0vr43xxw8irhyridgfv")))

(define-public crate-riscv-0.1.1 (c (n "riscv") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0kz9yw68bf3199l3rk84g9pwy3wqx8pg5xf4n2gwp3sqkwzlxcip")))

(define-public crate-riscv-0.1.2 (c (n "riscv") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "02s518w4hdp25bbr51jgsbsw0z9r1q7z7s63s3y90aq960lm1hcm")))

(define-public crate-riscv-0.1.3 (c (n "riscv") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0j31a9lkhi8fzsp1cpr3vf99iglpgxfmkvy1bfdjmn3xn5ipdff6")))

(define-public crate-riscv-0.1.4 (c (n "riscv") (v "0.1.4") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0h8qg13176nbdy99787ri5285skjr2c4spfnl7qzkhxfbf234ki8")))

(define-public crate-riscv-0.2.0 (c (n "riscv") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)))) (h "1hmk9k1wblbxaw2nhf9fh2hby2ncqcrcnx4iy4xbrijiv9bm12mh")))

(define-public crate-riscv-0.3.0 (c (n "riscv") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)))) (h "0z0499d5917ihq3kqvw1bk7xqmqi3m523gpc6d2zhzj3gsxm527f") (f (quote (("inline-asm"))))))

(define-public crate-riscv-0.4.0 (c (n "riscv") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)))) (h "1xyyd2j1chl3925fk0yg2icy2nsnxmqsgfwbypyz740j92s3a9a3") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn"))))))

(define-public crate-riscv-0.5.0 (c (n "riscv") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)))) (h "1nv0n4ss21v2p00k7l9ja1q41l2ip86n1i2qhiq0h2jgqpgmxgs1") (f (quote (("inline-asm"))))))

(define-public crate-riscv-0.5.1 (c (n "riscv") (v "0.5.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)))) (h "1vn8spg1ld3q3p4vcafm6jlwfqynlxxz87s9ab3cpi8p57jm8k7g") (f (quote (("inline-asm"))))))

(define-public crate-riscv-0.5.2 (c (n "riscv") (v "0.5.2") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)))) (h "16y6zf68zvjhd2alb25xw5i1c38brzgp1441fij4l51zh1ryrwq0") (f (quote (("inline-asm"))))))

(define-public crate-riscv-0.5.3 (c (n "riscv") (v "0.5.3") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)))) (h "1c34gsqhikp73ksb1srnq0741lh9ygmvgpbw655b5vnbv3kr788q") (f (quote (("inline-asm"))))))

(define-public crate-riscv-0.5.4 (c (n "riscv") (v "0.5.4") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)))) (h "1sq2f3b6yzjvz7kk26l7spcq3w03xdp2cmyks6w8ggg0h778bdvb") (f (quote (("inline-asm"))))))

(define-public crate-riscv-0.5.5 (c (n "riscv") (v "0.5.5") (d (list (d (n "bare-metal") (r ">= 0.2.0, < 0.2.5") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)))) (h "1nlk9rwg86cy7vr3wyv8vi8l9009hbhxkansc2qzdbhwn6g0b6ky") (f (quote (("inline-asm"))))))

(define-public crate-riscv-0.5.6 (c (n "riscv") (v "0.5.6") (d (list (d (n "bare-metal") (r ">= 0.2.0, < 0.2.5") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)))) (h "1hy86z12v8qcrk4hkadzg9l0gn9jb53a1nhqxgacmfvdr1csh4ja") (f (quote (("inline-asm"))))))

(define-public crate-riscv-0.6.0 (c (n "riscv") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "13k1qhqhck5vxpxf73b8r61qmrbyi29k1drbivvx1s98sh2vgw52") (f (quote (("inline-asm"))))))

(define-public crate-riscv-0.7.0 (c (n "riscv") (v "0.7.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "1fla10m2qkcf7zqw91rrribpdaavkv6qbbzjz9q2n09igbfwq1v9") (f (quote (("inline-asm"))))))

(define-public crate-riscv-0.8.0 (c (n "riscv") (v "0.8.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)))) (h "0g1d9rdq1lbp6wfsbxr7alnjfza0hb9m0ir65ck2v7h606kmca2y") (r "1.59")))

(define-public crate-riscv-0.8.1 (c (n "riscv") (v "0.8.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)))) (h "1nmwp5jvfx8348nsh5n3fxmzj8hgm295s41kfmmfzg5i02zd5jx6") (y #t) (r "1.59")))

(define-public crate-riscv-0.9.0 (c (n "riscv") (v "0.9.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)))) (h "1590wgnirxm6d9z7bg6isjrr5qv4axd7aygy713q291l273lpz3k") (y #t) (r "1.59")))

(define-public crate-riscv-0.10.0 (c (n "riscv") (v "0.10.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)))) (h "0nhsx1qmcxv4q4f4fb1kg814l5bziylgcqrqn62b8gg05cmai7qn") (f (quote (("critical-section-single-hart" "critical-section/restore-state-bool")))) (y #t) (r "1.59")))

(define-public crate-riscv-0.10.1 (c (n "riscv") (v "0.10.1") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)))) (h "0kxwd4hbip26p82mgfvlkfynmp4b49xq4bpc64g8nxz3zb94acda") (f (quote (("critical-section-single-hart" "critical-section/restore-state-bool")))) (r "1.59")))

(define-public crate-riscv-0.11.0 (c (n "riscv") (v "0.11.0") (d (list (d (n "critical-section") (r "^1.1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "08q0k9aykjc9nwjddwf2pv4pzgmhvpfya29a7ha02a18ng8h2pjp") (f (quote (("s-mode") ("critical-section-single-hart" "critical-section/restore-state-bool")))) (r "1.60")))

(define-public crate-riscv-0.11.1 (c (n "riscv") (v "0.11.1") (d (list (d (n "critical-section") (r "^1.1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "1a9k1lrk9gasc6if2iqlrs35q4p9s78l7r6ydhk4d9qyyj5inp1g") (f (quote (("s-mode") ("critical-section-single-hart" "critical-section/restore-state-bool")))) (r "1.60")))

