(define-module (crates-io ri sc riscv-asm) #:use-module (crates-io))

(define-public crate-riscv-asm-0.0.1-wip (c (n "riscv-asm") (v "0.0.1-wip") (d (list (d (n "goblin") (r "^0.2") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1a9zzcjj8dypnzsz6b8i16lw17ihjhnsngnm0igwapn1zljrbq2c")))

(define-public crate-riscv-asm-0.0.2-wip (c (n "riscv-asm") (v "0.0.2-wip") (d (list (d (n "goblin") (r "^0.2") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "117fqsvhfy15zdv0dyy05s1n79527rcz7c3w6b6a7zywz0p70vfa")))

