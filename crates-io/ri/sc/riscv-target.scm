(define-module (crates-io ri sc riscv-target) #:use-module (crates-io))

(define-public crate-riscv-target-0.1.0 (c (n "riscv-target") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1g713x6j7ahyk16xbrg78cfdfd3c54fhxzai36chy7ipqx1lrmrf")))

(define-public crate-riscv-target-0.1.1 (c (n "riscv-target") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1li13dsa1mc0ax8sic6jcn2bl9qswhvkk4gbyd9v4wbfww6xrha4")))

(define-public crate-riscv-target-0.1.2 (c (n "riscv-target") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08pj6f1sbddd6idjn8c1wv121bzikw9qvzhcl9icz822va697al8")))

