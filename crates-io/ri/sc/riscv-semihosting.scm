(define-module (crates-io ri sc riscv-semihosting) #:use-module (crates-io))

(define-public crate-riscv-semihosting-0.1.0 (c (n "riscv-semihosting") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.11.0") (d #t) (k 0)))) (h "1ck9l9vrbq9hsgvpfxaqsm9w877p6s0k9sg4042szcarldb9cz3d") (f (quote (("u-mode") ("no-semihosting") ("jlink-quirks") ("default" "jlink-quirks")))) (r "1.60.0")))

