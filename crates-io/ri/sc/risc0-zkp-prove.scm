(define-module (crates-io ri sc risc0-zkp-prove) #:use-module (crates-io))

(define-public crate-risc0-zkp-prove-0.1.0 (c (n "risc0-zkp-prove") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-zkp-accel") (r "^0.1") (d #t) (k 0)) (d (n "risc0-zkp-verify") (r "^0.2") (f (quote ("cxx"))) (d #t) (k 0)))) (h "0ibh50m8q8bv4jgcl4v700x9q1jjy1ba286vjv7d6yjf57zinihn") (y #t) (l "risc0-zkp-prove")))

(define-public crate-risc0-zkp-prove-0.4.0 (c (n "risc0-zkp-prove") (v "0.4.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-zkp-accel") (r "^0.4") (d #t) (k 0)) (d (n "risc0-zkp-verify") (r "^0.4") (f (quote ("cxx"))) (d #t) (k 0)))) (h "0pwhdvc1fji756cjxz68pz8yidpfvv7z4h11jvjckkn43wn54qgv") (y #t) (l "risc0-zkp-prove")))

(define-public crate-risc0-zkp-prove-0.6.0 (c (n "risc0-zkp-prove") (v "0.6.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-zkp-accel") (r "^0.6") (d #t) (k 0)) (d (n "risc0-zkp-verify") (r "^0.6") (f (quote ("cxx"))) (d #t) (k 0)))) (h "0gqqzk3ng24f5v5x6f6x4p6757wg43p3a8458p0p37ksijw73ll9") (y #t) (l "risc0-zkp-prove")))

