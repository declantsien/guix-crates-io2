(define-module (crates-io ri sc risc0-core-sys) #:use-module (crates-io))

(define-public crate-risc0-core-sys-0.7.0-dev.1 (c (n "risc0-core-sys") (v "0.7.0-dev.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "04q9gsj2ia997644a1nx166nc1vyk6b6s4yn1ca8r0h137b8yw0z") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.7.0-dev.2 (c (n "risc0-core-sys") (v "0.7.0-dev.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "10m495ww3za6kih0sbl8xwq4qq6y1svpvp3lbhjxvz2hikkvc2x0") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.7.0-rc.1 (c (n "risc0-core-sys") (v "0.7.0-rc.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1v23csyzqzxkrsrfg16yf9p0f11s4b7aph0zjdsj9akzc40h8a3c") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.7.0 (c (n "risc0-core-sys") (v "0.7.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0sq37vl60hq790xzka5ax7x65lbn7szwk6ldk92qmbqvgk76cqpm") (y #t) (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.7.1 (c (n "risc0-core-sys") (v "0.7.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0ghq80x7dgcl5mbrbx8hxk4aqlvfyc01fcm3sal7si9zchjimpay") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.7.2 (c (n "risc0-core-sys") (v "0.7.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1znlsmh5v43355829ik8ircgkgbygf2kqm40vn8ai9ghv7dd2m19") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.8.0 (c (n "risc0-core-sys") (v "0.8.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1biwcl0y5v31k2xf7z4cgwd9lb142nlpdrwsz0zap6q3fib52r6g") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.9.0 (c (n "risc0-core-sys") (v "0.9.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "117pv7l5p5alcikaagwsh6j3r5ywibb6zq5clk5fz580n6na3zhp") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.10.0 (c (n "risc0-core-sys") (v "0.10.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0kjpx47kkz0fnv1s9vzcxiji8bjq3jmssgzl93sh2p19yg5pnkkx") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.11.0-rc.1 (c (n "risc0-core-sys") (v "0.11.0-rc.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0nkr1nqivd1zm7ia6pbygjfh6ywvvj9x8i9r32wk3ghb2vhb9a8m") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.11.0-rc.2 (c (n "risc0-core-sys") (v "0.11.0-rc.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0nx9avb3mxcxk2gvr86kza912pa36aklgyn1icw4aw21asj9z2q7") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.11.0-rc.3 (c (n "risc0-core-sys") (v "0.11.0-rc.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "16p5ad59rma23rnidjvs4gl469nnb7crfy0z24mg23y6pw359ljg") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.11.0-rc.4 (c (n "risc0-core-sys") (v "0.11.0-rc.4") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0spq8ca1dwp7szmn0vm8nk21gmsfmz4nv0wdxn0s0h8bxddlmj2y") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.11.0 (c (n "risc0-core-sys") (v "0.11.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "17b425hxl40mbii29brg98pavb6y47d52mkanihpkqy1qyh6xdr5") (l "risc0-core-sys")))

(define-public crate-risc0-core-sys-0.11.1 (c (n "risc0-core-sys") (v "0.11.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "11rsjsrvycam1k2g1wr6x0j7wc04xh0rswn1h9f0iwsyihljw2z6") (l "risc0-core-sys")))

