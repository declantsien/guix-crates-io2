(define-module (crates-io ri sc risc0-zkvm-core) #:use-module (crates-io))

(define-public crate-risc0-zkvm-core-0.1.0 (c (n "risc0-zkvm-core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "053rh1kavacmfgx4fhl686n8xs219mmyqc9qx4irzw75mi8zxc9m")))

(define-public crate-risc0-zkvm-core-0.2.0 (c (n "risc0-zkvm-core") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "114w5ynnrvv5x0ngdvgpb9brlqgzv16qrmwm3idr7n5xivjgamip") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-core-0.4.0 (c (n "risc0-zkvm-core") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0mj538dggbcyk8lbhdzgnnhfpdl7qi6viaa4awckixsbf5kx8c4h") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-core-0.6.0 (c (n "risc0-zkvm-core") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "12sx3qpcd666sfx7q5jk91g5aghxss01l0q1w3jllag5v9pwk63x") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-core-0.7.0-dev.1 (c (n "risc0-zkvm-core") (v "0.7.0-dev.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.7.0-dev.1") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1bi3y59bchja72g4sz4mr867cj9bcr2fvl8lx5vkqdhgrh9j35qv") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("pure" "dep:risc0-zkp-core"))))))

(define-public crate-risc0-zkvm-core-0.7.0-dev.2 (c (n "risc0-zkvm-core") (v "0.7.0-dev.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.7.0-dev.1") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0qsh4radr30bi5bvr1b4rm4apan234bzgscpljdkc8azb2vh24wx") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("pure" "dep:risc0-zkp-core"))))))

(define-public crate-risc0-zkvm-core-0.7.0-rc.1 (c (n "risc0-zkvm-core") (v "0.7.0-rc.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.7.0-dev.1") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "11ah5g7gznyakccf326kap6mxfsxv3i0ar3z0757kc1wl624h0lp") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("pure" "dep:risc0-zkp-core"))))))

(define-public crate-risc0-zkvm-core-0.7.0 (c (n "risc0-zkvm-core") (v "0.7.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.7.0-dev.1") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0gfsn0i1vg2qkhcg8jqqwiyjnqanlxkq6hdrri4wkvdcdgfls2v6") (f (quote (("std") ("default" "std")))) (y #t) (s 2) (e (quote (("pure" "dep:risc0-zkp-core"))))))

(define-public crate-risc0-zkvm-core-0.7.1 (c (n "risc0-zkvm-core") (v "0.7.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.7") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1p8nwrmg943md35ldb8mgs80y5vsindgax0yhxh75py1d5dl8c12") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("pure" "dep:risc0-zkp-core"))))))

(define-public crate-risc0-zkvm-core-0.7.2 (c (n "risc0-zkvm-core") (v "0.7.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.7") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "119px8k0vi3b1l487gkqkwpjpkz9rg3jq6yvv59a1amlw7dym2hp") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("pure" "dep:risc0-zkp-core"))))))

(define-public crate-risc0-zkvm-core-0.8.0 (c (n "risc0-zkvm-core") (v "0.8.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0f8mp8s52m7s0rsgdsdsiy8gnki575rrdah1fcwr6yfw5wqhv5wi") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("pure" "dep:risc0-zkp-core"))))))

(define-public crate-risc0-zkvm-core-0.9.0 (c (n "risc0-zkvm-core") (v "0.9.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.9") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1cafvn103520y2sn03cz67km55iykl2zai6n57z3j411py63w7r3") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("pure" "dep:risc0-zkp-core"))))))

(define-public crate-risc0-zkvm-core-0.10.0 (c (n "risc0-zkvm-core") (v "0.10.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.10") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0yc6rzx0lyz8iz4hmggn8h1l0l9mld5655kpi7z0mvjndld18xdf") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("pure" "dep:risc0-zkp-core"))))))

