(define-module (crates-io ri sc riscv-peripheral) #:use-module (crates-io))

(define-public crate-riscv-peripheral-0.1.0 (c (n "riscv-peripheral") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 2)) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)) (d (n "riscv-pac") (r "^0.1.1") (d #t) (k 0)))) (h "0n1k4phkcihjjp8hb9gf62bz318l3jj37yk81kfx8jql9vz9kiq7") (f (quote (("aclint-hal-async" "embedded-hal-async")))) (r "1.75")))

