(define-module (crates-io ri sc risc0-zkvm-serde) #:use-module (crates-io))

(define-public crate-risc0-zkvm-serde-0.1.0 (c (n "risc0-zkvm-serde") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0pwnq2alig50l7k85fx6ncjvyd8dvlkpmg7qjlxjifib3bnvvz9s") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-serde-0.2.0 (c (n "risc0-zkvm-serde") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1b1apbi68adc8mfkhnkki566pjki0alz76d5i5h13fy10qadpjvs") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-serde-0.4.0 (c (n "risc0-zkvm-serde") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1d8pkbmmxy29bilcbkk5a357m4cfnalx569ga56z108xsrx4rm6q") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-serde-0.6.0 (c (n "risc0-zkvm-serde") (v "0.6.0") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "110av5z98hihh0lg2mz8i2p2pf0cl3r7p3ph7yvjjqmbhbm7cp5w") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-serde-0.7.0-dev.1 (c (n "risc0-zkvm-serde") (v "0.7.0-dev.1") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1n1f3k5lfmzp8yxrk7x0hbxq1551hd35xflx2i2dgr6andhsp03x") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-serde-0.7.0-dev.2 (c (n "risc0-zkvm-serde") (v "0.7.0-dev.2") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0hhpjrcfm6m0irnhnrxa4mngifvp4bc1h9djm21vs82hfnic12qj") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-serde-0.7.0-rc.1 (c (n "risc0-zkvm-serde") (v "0.7.0-rc.1") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0wb22i6s0hrkwm9z9cgmr6p6vl0jin43zs4fbzvfldlbdy6hpxir") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-serde-0.7.0 (c (n "risc0-zkvm-serde") (v "0.7.0") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1qfyxyjfr1bvbyz5w0biajdvhsvrxhcw1vhhcsbipkmza3p4cdvb") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-risc0-zkvm-serde-0.7.1 (c (n "risc0-zkvm-serde") (v "0.7.1") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "00xvp7lbh75c7lsfqskx5bnvqlmdzzcznd0jzvfs7zbx85brzppi") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-serde-0.7.2 (c (n "risc0-zkvm-serde") (v "0.7.2") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0ll8z2c2iyvg4v2fdlw6d081c32d0m8bn90g96hl3g80b67wyn29") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-serde-0.8.0 (c (n "risc0-zkvm-serde") (v "0.8.0") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1avvgc4pmyad7qwj2834zkimn2i9srw78gk2r7dq1xkz76i2zqhc") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-serde-0.9.0 (c (n "risc0-zkvm-serde") (v "0.9.0") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "10yx307xnvz18xzygmm37caz0hi7vclwdl93qkhdw119damv4z9k") (f (quote (("std") ("default" "std"))))))

(define-public crate-risc0-zkvm-serde-0.10.0 (c (n "risc0-zkvm-serde") (v "0.10.0") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1r4l1kz3sr4v4gxv6briq0k0jq8d2p0fya29zfxcvzqzjvs0v579") (f (quote (("std") ("default" "std"))))))

