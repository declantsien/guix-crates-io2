(define-module (crates-io ri sc riscvsbi) #:use-module (crates-io))

(define-public crate-riscvsbi-0.1.0 (c (n "riscvsbi") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "riscv2") (r "^0.1.0") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)))) (h "06l324ln2jd56ih1m1kwz1g2p8ynvnm0x0fbn84fcs3b1jkvspdc") (y #t)))

(define-public crate-riscvsbi-0.1.1 (c (n "riscvsbi") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "riscv2") (r "^0.1.1") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)))) (h "00nv9199m12nwinn9m3iqnxwhlcn3xhck3jndng29yrwp3cpaslb") (y #t)))

(define-public crate-riscvsbi-0.1.2 (c (n "riscvsbi") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "riscv2") (r "^0.1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)))) (h "1zm6kqy84q25mysw0yzl3n8nvs27a18zyrz3m1kl5dv7xxsspqd3") (y #t)))

(define-public crate-riscvsbi-0.1.3 (c (n "riscvsbi") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "riscv2") (r "^0.1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)))) (h "1pz9hjvwvdpin8gp16w8br7y6kv9bcl6qxs5pr1axydsa1l9lwl0") (y #t)))

(define-public crate-riscvsbi-0.1.4 (c (n "riscvsbi") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "riscv2") (r "^0.1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)))) (h "0lyppbfrp7pqj4nkrbyq0cwffpj34321l5n1lfsv3fs2niqbjsgd") (y #t)))

