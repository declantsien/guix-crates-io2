(define-module (crates-io ri sc riscv-slic-macros) #:use-module (crates-io))

(define-public crate-riscv-slic-macros-0.1.0 (c (n "riscv-slic-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "19l1g9cabaq07iycbnvn466yhlsajr6dgdak67xzz1hlbydpainj") (f (quote (("ssoft-backend" "ssoft") ("ssoft") ("msoft") ("clint-backend" "msoft"))))))

