(define-module (crates-io ri sc risc0-build-ethereum) #:use-module (crates-io))

(define-public crate-risc0-build-ethereum-0.9.0 (c (n "risc0-build-ethereum") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "risc0-build") (r "^0.21") (k 0)) (d (n "risc0-zkp") (r "^0.21") (k 0)))) (h "0pxiga3prr9ss3ac979ii14cz3vy9y6rx6vgnna6h00604cqj3xi") (f (quote (("default"))))))

