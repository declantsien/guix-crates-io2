(define-module (crates-io ri sc riscv-regs) #:use-module (crates-io))

(define-public crate-riscv-regs-0.1.0 (c (n "riscv-regs") (v "0.1.0") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "0f7rgbcv9n2lxb5p1q180ip8dbmrz8f1j7g4jq6g607zc4qxnm1g")))

(define-public crate-riscv-regs-0.2.0 (c (n "riscv-regs") (v "0.2.0") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "0qzx30vygl9gch3l06w8n65d5klscbi15jr3gv6ay6260xzipzhn")))

(define-public crate-riscv-regs-0.3.0 (c (n "riscv-regs") (v "0.3.0") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "0hz4ajzsahj2n387fr1jsnj5nmgqqaj83h5q6m5y8gd2vs652jm0")))

