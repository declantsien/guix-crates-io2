(define-module (crates-io ri sc riscan-pro) #:use-module (crates-io))

(define-public crate-riscan-pro-0.1.0 (c (n "riscan-pro") (v "0.1.0") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "17ysagp5s7aprmh0gc6vra3q25vvdsvr0m1b2w6dj1hccd0q1qh6")))

(define-public crate-riscan-pro-0.2.0 (c (n "riscan-pro") (v "0.2.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.6") (d #t) (k 0)))) (h "0kk2vyhdl8qssjh7bvi2pq0dq65sc0qglkjwv6hkg7wgsn54g6kj")))

(define-public crate-riscan-pro-0.2.1 (c (n "riscan-pro") (v "0.2.1") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.6") (d #t) (k 0)))) (h "17nlm3mmci2v1an41z29r29qscqvfq6q8f23dy1397i1swq1vs7a")))

