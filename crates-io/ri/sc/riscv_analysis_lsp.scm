(define-module (crates-io ri sc riscv_analysis_lsp) #:use-module (crates-io))

(define-public crate-riscv_analysis_lsp-0.1.0-alpha (c (n "riscv_analysis_lsp") (v "0.1.0-alpha") (d (list (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "riscv_analysis") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)))) (h "09xp9xk6jx3ysygxb797kfn3zyl7b2sk75gixr3a7w0ip22in8bq")))

