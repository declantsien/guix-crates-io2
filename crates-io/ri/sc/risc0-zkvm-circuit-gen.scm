(define-module (crates-io ri sc risc0-zkvm-circuit-gen) #:use-module (crates-io))

(define-public crate-risc0-zkvm-circuit-gen-0.1.0 (c (n "risc0-zkvm-circuit-gen") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm-circuit") (r "^0.1") (d #t) (k 0)))) (h "1cyvfxaw2zxaliq2wb4ljad5x0i72y01h9pcvvj1rzsph3g0h0kr") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.2.0 (c (n "risc0-zkvm-circuit-gen") (v "0.2.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm-circuit") (r "^0.2") (d #t) (k 0)))) (h "05hpx4hx8faz5v7pci191f3ypig9dyxk9l52xa8nasd3ls2badx0") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.4.0 (c (n "risc0-zkvm-circuit-gen") (v "0.4.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm-circuit") (r "^0.4") (d #t) (k 0)))) (h "06c3kx593i8x73p454c1xl5bp67cqqmys0spqf5l2h17igl7s8a3") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.6.0 (c (n "risc0-zkvm-circuit-gen") (v "0.6.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "risc0-zkvm-circuit") (r "^0.6") (d #t) (k 0)))) (h "0a9v80m0j93g00y3wy0qnqxm57dv4jc7vlcqqgrbslwhqcg0i073") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.7.0-dev.1 (c (n "risc0-zkvm-circuit-gen") (v "0.7.0-dev.1") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.7.0-dev.1") (d #t) (k 1)))) (h "1pybvm0wym14fwqpcs10l20agxs3d1p0v62z6mvz17c5cp616gky") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.7.0-dev.2 (c (n "risc0-zkvm-circuit-gen") (v "0.7.0-dev.2") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.7.0-dev.1") (d #t) (k 1)))) (h "0k489cfmcxzj15zkf8qzqgw0diwxbrixp417ajxfaj8zgbgdwzza") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.7.0-rc.1 (c (n "risc0-zkvm-circuit-gen") (v "0.7.0-rc.1") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.7.0-dev.1") (d #t) (k 1)))) (h "1mbpzfx9mrna5l6fzclzraxpqmp5bfx4122yp9iy78d1z4cim55f") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.7.0 (c (n "risc0-zkvm-circuit-gen") (v "0.7.0") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.7.0-dev.1") (d #t) (k 1)))) (h "1dzjgjwf5zzgcr9b4354nn8k4wnnq8h3pmkcyzvp9jh2wg987a7j") (y #t) (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.7.1 (c (n "risc0-zkvm-circuit-gen") (v "0.7.1") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.7") (d #t) (k 1)))) (h "0f11fri5d1vicbh89f4jpygah7c41zr0c8szv88vf9srndhjmrbc") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.7.2 (c (n "risc0-zkvm-circuit-gen") (v "0.7.2") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.7") (d #t) (k 1)))) (h "1vvfy0vz2jb63zyg0l0935sr4a407jwfi7xhfdkpywhg7blxd4hn") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.8.0 (c (n "risc0-zkvm-circuit-gen") (v "0.8.0") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.8") (d #t) (k 1)))) (h "0msl6g5k8id85agj7ll48q9jyihm5ki4qg8a4yzllamzdjygmwln") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.9.0 (c (n "risc0-zkvm-circuit-gen") (v "0.9.0") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.9") (d #t) (k 1)))) (h "0agcmjxcl2xc2w4409hdnd1qzimibr3vjspwvl7hv7s6d12p9m8n") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.10.0 (c (n "risc0-zkvm-circuit-gen") (v "0.10.0") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.10") (d #t) (k 1)))) (h "0m2yariddfg610vasifrx78hyv1djyqkbxy8gcaacw9niphk2fi8") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.11.0-rc.1 (c (n "risc0-zkvm-circuit-gen") (v "0.11.0-rc.1") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.11.0-rc.1") (d #t) (k 1)))) (h "1khxq6l86gyrii79c3dzbapvwqlkj5f3zy145grp9q18xsjfyp5k") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.11.0-rc.2 (c (n "risc0-zkvm-circuit-gen") (v "0.11.0-rc.2") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.11.0-rc.2") (d #t) (k 1)))) (h "1533f9wgw1ipzi26wcmsw87048dlcnzxp98c9lb3yf3419pf8fp6") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.11.0-rc.3 (c (n "risc0-zkvm-circuit-gen") (v "0.11.0-rc.3") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.11.0-rc.3") (d #t) (k 1)))) (h "0zwznbrqscm9j0q50f2bv1i8jvhicfn5i5czdrlhaamzf6hyhq9k") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.11.0-rc.4 (c (n "risc0-zkvm-circuit-gen") (v "0.11.0-rc.4") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.11.0-rc.4") (d #t) (k 1)))) (h "14s811d8x35kg17jdhqjsi3fx79bwc554dnkz9iv5w5dvvp18a35") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.11.0 (c (n "risc0-zkvm-circuit-gen") (v "0.11.0") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.11.0-rc.4") (d #t) (k 1)))) (h "0hjwiivn6dpmld40igab7h47vkh4cjyyj6zh69nila5dxg1ks84c") (l "risc0-zkvm-circuit-gen")))

(define-public crate-risc0-zkvm-circuit-gen-0.11.1 (c (n "risc0-zkvm-circuit-gen") (v "0.11.1") (d (list (d (n "risc0-zkvm-circuit-sys") (r "^0.11") (d #t) (k 1)))) (h "0sqb5585zc4mql7sc07h8r0prh5siih6k5193nm04zjz6jji4frx") (l "risc0-zkvm-circuit-gen")))

