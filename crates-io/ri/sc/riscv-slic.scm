(define-module (crates-io ri sc riscv-slic) #:use-module (crates-io))

(define-public crate-riscv-slic-0.1.0 (c (n "riscv-slic") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.2") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)) (d (n "riscv-slic-macros") (r "^0.1.0") (d #t) (k 0)))) (h "01aqmjliyq65rwx27xlsv518l27jqy0m7r71zzqs9p4n582ihkdk") (f (quote (("ssoft-backend" "ssoft" "riscv-slic-macros/ssoft-backend") ("ssoft" "riscv/s-mode") ("msoft") ("clint-backend" "msoft" "riscv-slic-macros/clint-backend"))))))

(define-public crate-riscv-slic-0.1.1 (c (n "riscv-slic") (v "0.1.1") (d (list (d (n "critical-section") (r "^1.1.2") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)) (d (n "riscv-slic-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0nrq8paxasa7basfg4k584416rfbpqv8rmp9vbc8xfm8an0jzqf6") (f (quote (("ssoft-backend" "ssoft" "riscv-slic-macros/ssoft-backend") ("ssoft" "riscv/s-mode") ("msoft") ("clint-backend" "msoft" "riscv-slic-macros/clint-backend"))))))

