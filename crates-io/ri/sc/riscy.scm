(define-module (crates-io ri sc riscy) #:use-module (crates-io))

(define-public crate-riscy-0.1.0 (c (n "riscy") (v "0.1.0") (h "0n41ngn96l61zbqxk0wc57d9cms0i1526ajyaigg6r95ia0zdsy7")))

(define-public crate-riscy-0.2.0 (c (n "riscy") (v "0.2.0") (d (list (d (n "goblin") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "07bagn50wji1p78y1q3yllhn83zkv6h4ijk60xmzcak5z498fdf9")))

(define-public crate-riscy-0.3.0 (c (n "riscy") (v "0.3.0") (d (list (d (n "goblin") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1hqhfbg8h4xk3vfbxfwcfjlzpczqjnb5krj751adckgp2wx2byqr")))

(define-public crate-riscy-0.4.0 (c (n "riscy") (v "0.4.0") (d (list (d (n "goblin") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1grinx5p4sh8s1h14rbfimlz6mkcil81m924pydk8h4m3ccjj20l")))

