(define-module (crates-io ri sc riscv2) #:use-module (crates-io))

(define-public crate-riscv2-0.1.0 (c (n "riscv2") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "19g7a8jabpv3j8cn03mwy476bg2pfjpa448kwx37a3s4cgdnaasi") (y #t)))

(define-public crate-riscv2-0.1.1 (c (n "riscv2") (v "0.1.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "11392grk5kym5rcmy2c9xzllqgz3kb8qa2k4v0f3w5bs0fkh9nzz") (y #t)))

(define-public crate-riscv2-0.1.2 (c (n "riscv2") (v "0.1.2") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "0fkri94j1lf8895avc17j129na9nmw7z1brzragajjdc3x6iy4m8") (y #t)))

