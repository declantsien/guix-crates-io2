(define-module (crates-io ri sc riscv_emu_rust) #:use-module (crates-io))

(define-public crate-riscv_emu_rust-0.1.0 (c (n "riscv_emu_rust") (v "0.1.0") (h "0i757ivxmp49rw1ps659nb7170wgmxlvv4ky8k8k17j685fhqw7b")))

(define-public crate-riscv_emu_rust-0.2.0 (c (n "riscv_emu_rust") (v "0.2.0") (h "0zayp4xm6axpgy9lm2jrxj7g7301hn3h3bgvqvmmwrkc0wzlx24l")))

