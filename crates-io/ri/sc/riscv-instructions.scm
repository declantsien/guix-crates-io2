(define-module (crates-io ri sc riscv-instructions) #:use-module (crates-io))

(define-public crate-riscv-instructions-0.1.0 (c (n "riscv-instructions") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.2") (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)) (d (n "tex-parser") (r "=0.1.0") (k 0)))) (h "0wgq28nphdvl0g05rsz5l1x31yzpnsly4wabpxzw5mx1iv6ad66h") (f (quote (("trace" "peg/trace") ("tex-serde" "tex-parser/serde"))))))

