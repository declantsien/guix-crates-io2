(define-module (crates-io ri sc risc0-zkp-hal) #:use-module (crates-io))

(define-public crate-risc0-zkp-hal-0.10.0 (c (n "risc0-zkp-hal") (v "0.10.0") (d (list (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.10") (d #t) (k 0)))) (h "0xxp5j74ac4jz9rpf51yipnisag1qm5lp8qymkxl03wdzl6ffmpf")))

