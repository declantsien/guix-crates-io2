(define-module (crates-io ri sc riscv-pac) #:use-module (crates-io))

(define-public crate-riscv-pac-0.1.0 (c (n "riscv-pac") (v "0.1.0") (h "0m0qq433q7ha5r7r9ys8ywlr2chh17yk1lpa9125mzz12vnmipvw") (r "1.60")))

(define-public crate-riscv-pac-0.1.1 (c (n "riscv-pac") (v "0.1.1") (h "19mnis230l06v0kv2frkyy23v9lmx1fc7zcajd7l7bgx4fpv9khq") (r "1.60")))

