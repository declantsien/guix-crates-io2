(define-module (crates-io ri sc riscv-rt-macros) #:use-module (crates-io))

(define-public crate-riscv-rt-macros-0.1.5 (c (n "riscv-rt-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "riscv-rt") (r "^0.5.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.13") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "15dna7mpkxnn4ij5i5qc8gn8rq6m9ppzf8j7rhvwq0i93xz9dfp9")))

(define-public crate-riscv-rt-macros-0.1.6 (c (n "riscv-rt-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.13") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0lrxl8cmq9h9fz2arbqq53jwzqb9b6hvg1r0g1hc1plq30sgh99m")))

(define-public crate-riscv-rt-macros-0.2.0 (c (n "riscv-rt-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1bsdmznibivmpq9cb226ahpwvf3vr65gzrdkx9660bvwn7bhk1gk")))

(define-public crate-riscv-rt-macros-0.2.1 (c (n "riscv-rt-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0va81128rbx51gd8ckiraw6mgg4svbrqd4vaxy0nddyvcva01ld8")))

