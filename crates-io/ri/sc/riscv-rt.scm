(define-module (crates-io ri sc riscv-rt) #:use-module (crates-io))

(define-public crate-riscv-rt-0.1.1 (c (n "riscv-rt") (v "0.1.1") (d (list (d (n "r0") (r "^0.2.1") (d #t) (k 0)) (d (n "riscv") (r "^0.1.1") (d #t) (k 0)))) (h "1ka7vifp8var4xvlq0d0rm1rikk24z4ynmpcvaq5h25lq0vwr98z")))

(define-public crate-riscv-rt-0.1.2 (c (n "riscv-rt") (v "0.1.2") (d (list (d (n "r0") (r "^0.2.1") (d #t) (k 0)) (d (n "riscv") (r "^0.1.1") (d #t) (k 0)))) (h "0z447xg2bc70xgyi1l16d4fkqs3qlz055qacfd7vdrqdkrvyqmxy")))

(define-public crate-riscv-rt-0.1.3 (c (n "riscv-rt") (v "0.1.3") (d (list (d (n "r0") (r "^0.2.1") (d #t) (k 0)) (d (n "riscv") (r "^0.1.3") (d #t) (k 0)))) (h "1zagf1nz2li3rm5fg3m37mmh594x7vlzsg4ldfaxlf5fqg8yx9m9")))

(define-public crate-riscv-rt-0.2.0 (c (n "riscv-rt") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 1)) (d (n "r0") (r "^0.2.1") (d #t) (k 0)) (d (n "riscv") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)))) (h "1hy3pll3mi6c4317kry5l1bvhi7vj77hchaqmhjiip8bx18s6jy1")))

(define-public crate-riscv-rt-0.3.0 (c (n "riscv-rt") (v "0.3.0") (d (list (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv") (r "^0.3.0") (d #t) (k 0)))) (h "09d6p4a91psyyjib6i4a4wcgryk9nmqglwlc3g6cajinpb54rz8h") (f (quote (("inline-asm"))))))

(define-public crate-riscv-rt-0.4.0 (c (n "riscv-rt") (v "0.4.0") (d (list (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv") (r "^0.4.0") (d #t) (k 0)))) (h "1d46q3kdd80agylqj8jyfnappmxk5iar0kc60cc0nzxv700x5gii") (f (quote (("inline-asm" "riscv/inline-asm") ("const-fn" "riscv/const-fn"))))))

(define-public crate-riscv-rt-0.5.0 (c (n "riscv-rt") (v "0.5.0") (d (list (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.5") (d #t) (k 0)))) (h "0zf1b0jqqvg1qc3m8syq5h3a82yrm663y7a3ac59aialkb05335g") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-riscv-rt-0.6.0 (c (n "riscv-rt") (v "0.6.0") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 2)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)))) (h "1hsv5jxfdg6vxjglnsmjl2m9b24dgqpysv03kkh6x9hv7mrprxgn") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-riscv-rt-0.5.1 (c (n "riscv-rt") (v "0.5.1") (d (list (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.5") (d #t) (k 0)))) (h "10jcyyavkkqxhpmm5jwm0b1q56q29k0hh31avwdawrp1rah5ylsx") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-riscv-rt-0.6.1 (c (n "riscv-rt") (v "0.6.1") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 2)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)))) (h "0d16nviqbs1m11vv7czm150cj7rb8v04z0lcfsfngqqkhl6jxsrv") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-riscv-rt-0.7.0 (c (n "riscv-rt") (v "0.7.0") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.5.5") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)))) (h "0l12q8rbspdmcsq6xpn5hxhkg7n0sg3mxs90brmlx3jx4mg8ji2z") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-riscv-rt-0.7.1 (c (n "riscv-rt") (v "0.7.1") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.5.5") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "1cy7lm13hliaqafpimvr36bf5bd24rr7m9b20530wn33279pm94r") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-riscv-rt-0.6.2 (c (n "riscv-rt") (v "0.6.2") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 2)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)))) (h "06dk8ac5wdcb8dan0zsxz4a9lywphvsv0lifgkwjsn4680141dg5") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-riscv-rt-0.7.2 (c (n "riscv-rt") (v "0.7.2") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.5.5") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "1ywmr7qb6m93v8sbpw6q2gj41dj7n5gd5ly8i714hlc7v9r9irfr") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-riscv-rt-0.8.0 (c (n "riscv-rt") (v "0.8.0") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "1pcbm9m3lj4ppigixwvbiyy1y0flkz01jscr6mpmviqn48xmizmp") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-riscv-rt-0.8.1 (c (n "riscv-rt") (v "0.8.1") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "000fkm49xjc1bxfy7zd855502ikvvk4m33076rm1jmh3hpf2i7xb") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-riscv-rt-0.9.0 (c (n "riscv-rt") (v "0.9.0") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "1y638wnbyi2hq2fiih4mvmvami4j0ddnbhk3c4jqkvgypfndyzaj") (r "1.59")))

(define-public crate-riscv-rt-0.10.0 (c (n "riscv-rt") (v "0.10.0") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.9") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "1bjy0dvmpgwmcgxazmppjgzg1mr3dji5vxm6b2qyplcjadg4j63q") (f (quote (("s-mode")))) (y #t) (r "1.59")))

(define-public crate-riscv-rt-0.11.0 (c (n "riscv-rt") (v "0.11.0") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.10") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "12f0lnkadvnnmbax43a0l9dbhxyzd767gh4svzg29ppgkp454b0h") (f (quote (("s-mode")))) (r "1.59")))

(define-public crate-riscv-rt-0.12.0 (c (n "riscv-rt") (v "0.12.0") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "riscv") (r "^0.11.0") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.2.1") (d #t) (k 0)))) (h "02l8rymlia9x1nw95c5m9b7drwqp119b499yhf5ixaaxabag3nb0") (f (quote (("single-hart") ("s-mode")))) (l "riscv-rt") (r "1.60")))

(define-public crate-riscv-rt-0.12.1 (c (n "riscv-rt") (v "0.12.1") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "riscv") (r "^0.11.0") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.2.1") (d #t) (k 0)))) (h "10qkm2cz9pp3xkdrx92gi3lzw4gnwjfzw76ilpfvy064l1mdd52i") (f (quote (("single-hart") ("s-mode")))) (l "riscv-rt") (r "1.60")))

(define-public crate-riscv-rt-0.12.2 (c (n "riscv-rt") (v "0.12.2") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.2.1") (d #t) (k 0)))) (h "1zlx4b88frnznqn4rhsd17yqg8028jmakn45i0z1i0qkrwr5xly0") (f (quote (("single-hart") ("s-mode")))) (l "riscv-rt") (r "1.60")))

