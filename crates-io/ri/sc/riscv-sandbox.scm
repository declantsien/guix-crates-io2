(define-module (crates-io ri sc riscv-sandbox) #:use-module (crates-io))

(define-public crate-riscv-sandbox-0.1.0 (c (n "riscv-sandbox") (v "0.1.0") (h "15qxvlfsq4saazyl3qr3mrlb0jblzxd6nd407fqiq54a4rfii8i7")))

(define-public crate-riscv-sandbox-0.2.0 (c (n "riscv-sandbox") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.15.2") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)))) (h "07cngpc8pfxa13wqi3hyii5zcrizr717wnb7bd5x1dpmi1xgzis0")))

