(define-module (crates-io ri sc risc0-sppark) #:use-module (crates-io))

(define-public crate-risc0-sppark-0.1.0 (c (n "risc0-sppark") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "which") (r "^4.0") (d #t) (k 1)))) (h "01kkg250lb41m2c3dyzqwy2rp431fkzw2jv543scj0g5gzzx3qav") (l "sppark")))

