(define-module (crates-io ri sc risc0-core) #:use-module (crates-io))

(define-public crate-risc0-core-0.1.0 (c (n "risc0-core") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1shdxa2lnwdw0qlvmwi3gzsjfk8m056jhlff181w71d4ksip0jlb") (l "risc0-core")))

(define-public crate-risc0-core-0.4.0 (c (n "risc0-core") (v "0.4.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "14j6yspvsz3dppyy0l5wdw8csvjlrm9wlhl8xihaqnj99531kpc6") (l "risc0-core")))

(define-public crate-risc0-core-0.6.0 (c (n "risc0-core") (v "0.6.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1dba7r4ss2p8pvahdp3gdjnj30mznlrslbchqjlzwwgh5shph69k") (l "risc0-core")))

(define-public crate-risc0-core-0.13.0 (c (n "risc0-core") (v "0.13.0") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1pdr1mcdmfd8f3b3wh43rsba05xcpc6377yxv1bzrx13wyj5v5ka") (f (quote (("std"))))))

(define-public crate-risc0-core-0.14.0-rc.1 (c (n "risc0-core") (v "0.14.0-rc.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0vl0jpqnkws4fcscn4k0n5qsrzwrichp3a7an9x179ms5h8f3i8m") (f (quote (("std"))))))

(define-public crate-risc0-core-0.14.0 (c (n "risc0-core") (v "0.14.0") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1hfiwdx4gc20b2jymjg03ma5dsd2lqkq05wmvc96v0kajfzsbz3y") (f (quote (("std"))))))

(define-public crate-risc0-core-0.15.0-rc.1 (c (n "risc0-core") (v "0.15.0-rc.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0r97wj99an89icznpmzcw2if8qaldv65yqr1kb3rpm2llf02188j") (f (quote (("std"))))))

(define-public crate-risc0-core-0.15.0 (c (n "risc0-core") (v "0.15.0") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1s4scnirrrf6c7wlpsh9s31pdbdkj6w0bzmrsip5yap7l1xlp65p") (f (quote (("std"))))))

(define-public crate-risc0-core-0.15.1 (c (n "risc0-core") (v "0.15.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1vh45p4akvplda0qfzkimqfd9d2lj33868718vnfbwjlxdy4w7fs") (f (quote (("std"))))))

(define-public crate-risc0-core-0.15.2 (c (n "risc0-core") (v "0.15.2") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1036ndk64a6p35ds5r1c4pz6z5qmzqvpplvd43dnp1q418mbya2m") (f (quote (("std"))))))

(define-public crate-risc0-core-0.15.3 (c (n "risc0-core") (v "0.15.3") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "13wxlmd40shf8z3z3ddw40fnikmdhxr8lwsiih5clpawkv62z29q") (f (quote (("std"))))))

(define-public crate-risc0-core-0.16.0 (c (n "risc0-core") (v "0.16.0") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1w83amzlsyjx34a32mmddq1694y00insj88q2hblc0zphw0ml623") (f (quote (("std"))))))

(define-public crate-risc0-core-0.16.1 (c (n "risc0-core") (v "0.16.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0js9yw763lxkczkzqnp3zrbr6ma23ia99i18fwai2pw4spbfk81f") (f (quote (("std"))))))

(define-public crate-risc0-core-0.17.0 (c (n "risc0-core") (v "0.17.0") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1lq3hrhl93dmf0jdch5rbk739q7lnj5984sm14rmxi838hs39vqn") (f (quote (("std"))))))

(define-public crate-risc0-core-0.18.0 (c (n "risc0-core") (v "0.18.0") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0p044109kmg99pwp31lxfh65nyi8szi447zqyy1ys8pajgn5lq08") (f (quote (("std"))))))

(define-public crate-risc0-core-0.19.0-alpha.1 (c (n "risc0-core") (v "0.19.0-alpha.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1jb4hb4qws5hp2x8f5v0ssffj465slwfwd7wsgadpc9vag7nk976") (f (quote (("std"))))))

(define-public crate-risc0-core-0.19.0-rc.1 (c (n "risc0-core") (v "0.19.0-rc.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0w834lhqmc3jx51ybx0ldbx8j71mapjl9ymavdia8624l6fwgycp") (f (quote (("std"))))))

(define-public crate-risc0-core-0.19.0-rc.2 (c (n "risc0-core") (v "0.19.0-rc.2") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0485c9vs6zpil5qia8ap0v6zvda4hcd8hscfbishsafz04bmgg0c") (f (quote (("std"))))))

(define-public crate-risc0-core-0.19.0-rc.3 (c (n "risc0-core") (v "0.19.0-rc.3") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "10v2xy8vmskq4v9di3sdrpmgjwi099z0h5p11xrn0qky8a2w1432") (f (quote (("std"))))))

(define-public crate-risc0-core-0.19.0-rc.4 (c (n "risc0-core") (v "0.19.0-rc.4") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1nqbpxjjsb6wkppindqk2l4ds66cdlls3yvlhb7ccapp1zs6xm7m") (f (quote (("std"))))))

(define-public crate-risc0-core-0.19.0 (c (n "risc0-core") (v "0.19.0") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1chcx2dvc3jwdp9ws606qb2a8gmpbby7smp4cg7rx2jbrbvrz38z") (f (quote (("std"))))))

(define-public crate-risc0-core-0.19.1-rc.1 (c (n "risc0-core") (v "0.19.1-rc.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0dms0pbw9b67b35134862srrjbs2v8na2k2s2spqpkn4qv4isz6h") (f (quote (("std"))))))

(define-public crate-risc0-core-0.19.1 (c (n "risc0-core") (v "0.19.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0zf0ymdf0xqbv8xsfbma1wnna7lhjssiwllb11apj2zcsaw0nzj7") (f (quote (("std"))))))

(define-public crate-risc0-core-0.20.0-rc.1 (c (n "risc0-core") (v "0.20.0-rc.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0cqhdr4c7dbv60ms7rf6jdxiaa0wmxmnr3znvg6333rwmvnyasyf") (f (quote (("std"))))))

(define-public crate-risc0-core-0.20.0-rc.2 (c (n "risc0-core") (v "0.20.0-rc.2") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0561j73kzzm0x719cqlg4ssr2g0z02mhf3aanvx6p9id34lw9ghx") (f (quote (("std"))))))

(define-public crate-risc0-core-0.20.0-rc.3 (c (n "risc0-core") (v "0.20.0-rc.3") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1g7kr0nq183npxrnjb0pvpanssw1ll5xw2kqwyd85qjj1j1c08v0") (f (quote (("std"))))))

(define-public crate-risc0-core-0.20.0-rc.4 (c (n "risc0-core") (v "0.20.0-rc.4") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1rd7z1l1b02nhhv661kswx90iddzc82g7ch76balfbf69q1c7556") (f (quote (("std"))))))

(define-public crate-risc0-core-0.20.0 (c (n "risc0-core") (v "0.20.0") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "06rsgpv94d6pp0gfl40k1r9gi9iwl26dvwl5ffzsgcqw9wngmbcg") (f (quote (("std"))))))

(define-public crate-risc0-core-0.20.1 (c (n "risc0-core") (v "0.20.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "07k4a4zqa29xrx4ppkidli3k55mpz0idmqvkc0vnp20qrndwhydy") (f (quote (("std"))))))

(define-public crate-risc0-core-0.21.0-rc.1 (c (n "risc0-core") (v "0.21.0-rc.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0dfcjsf45433a9mi6ng9lbyg6g46f8kiwmll56vghgwqca8m237l") (f (quote (("std"))))))

(define-public crate-risc0-core-0.21.0-rc.2 (c (n "risc0-core") (v "0.21.0-rc.2") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0c0d2wa4bzd98g4ilnby5pi4la76ylc0sjly7x7jrmwm4xqdz7vv") (f (quote (("std"))))))

(define-public crate-risc0-core-0.21.0 (c (n "risc0-core") (v "0.21.0") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1d50vazcwg0m3afaib4mz0mv5ba3c6jrdkb65dbkphh3kp8cpq02") (f (quote (("std"))))))

(define-public crate-risc0-core-1.0.0-rc.3 (c (n "risc0-core") (v "1.0.0-rc.3") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "04jqnisbczsz49hvjkas5xr1h9j9hrz5js2ry2igc88xqnmib813") (f (quote (("std"))))))

(define-public crate-risc0-core-1.0.0-rc.4 (c (n "risc0-core") (v "1.0.0-rc.4") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1j6b16jlfih2aqlrs2d7i19200y6n2xxhg28g7agi25ckv5lbnh4") (f (quote (("std"))))))

(define-public crate-risc0-core-1.0.0-rc.5 (c (n "risc0-core") (v "1.0.0-rc.5") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0ax4jb3r1w2yy004n4xiqsx6byfk56r8qng196rm506kfbmfhs9f") (f (quote (("std"))))))

(define-public crate-risc0-core-1.0.0-rc.6 (c (n "risc0-core") (v "1.0.0-rc.6") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1b7bbj5xjzvkwa9aw6gw6da8ibw1wrsdmy8haflc9zkamk4gq869") (f (quote (("std"))))))

