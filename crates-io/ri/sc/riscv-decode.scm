(define-module (crates-io ri sc riscv-decode) #:use-module (crates-io))

(define-public crate-riscv-decode-0.1.0 (c (n "riscv-decode") (v "0.1.0") (h "1icy924dswyqqbmgvvg77fggzym21sqbm488y2kq00146dkycznj")))

(define-public crate-riscv-decode-0.2.0 (c (n "riscv-decode") (v "0.2.0") (h "1rjhakyiv93zl1vlz75fl3pjvxpgh2kjzdxr5angzi0y3886c14j")))

(define-public crate-riscv-decode-0.2.1 (c (n "riscv-decode") (v "0.2.1") (h "1gw8qllzlyazlcqbd8g2vpcxq90dbjj686174d6nmf8b1gfadixy")))

