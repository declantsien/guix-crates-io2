(define-module (crates-io ri sc risc0-zkp-verify) #:use-module (crates-io))

(define-public crate-risc0-zkp-verify-0.1.0 (c (n "risc0-zkp-verify") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 0)) (d (n "risc0-zkp-core") (r "^0.1") (d #t) (k 0)))) (h "0wx5v3mpgrfd5rkx0vigmspv4qjzrpcn5lgjqg4fvxb36cgy2la2")))

(define-public crate-risc0-zkp-verify-0.2.0 (c (n "risc0-zkp-verify") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.2") (d #t) (k 0)))) (h "1acv0amsfalnl6nmphdwdp0jpv87jbn6cx70knwqk3fjkmiad5pv") (f (quote (("default" "pure")))) (l "risc0-zkp-verify") (s 2) (e (quote (("pure" "dep:rand") ("cxx" "dep:cxx" "risc0-zkp-core/cxx"))))))

(define-public crate-risc0-zkp-verify-0.4.0 (c (n "risc0-zkp-verify") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.4") (d #t) (k 0)))) (h "1wxrs1ddl90s4kld8nqy08jwlkgn83kyca1pjamlykp0ap5x0s93") (f (quote (("default" "pure")))) (l "risc0-zkp-verify") (s 2) (e (quote (("pure" "dep:rand") ("cxx" "dep:cxx" "risc0-zkp-core/cxx"))))))

(define-public crate-risc0-zkp-verify-0.6.0 (c (n "risc0-zkp-verify") (v "0.6.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (k 0)) (d (n "risc0-zkp-core") (r "^0.6") (d #t) (k 0)))) (h "0viz7vy1scxh8m2cqzzmlb2q8rnpjgfvbs56c65hxjgikk88cqrc") (f (quote (("default" "pure")))) (l "risc0-zkp-verify") (s 2) (e (quote (("pure" "dep:rand") ("cxx" "dep:cxx" "risc0-zkp-core/cxx"))))))

