(define-module (crates-io ri sc risc0-zeroio-derive) #:use-module (crates-io))

(define-public crate-risc0-zeroio-derive-1.0.0-rc.1 (c (n "risc0-zeroio-derive") (v "1.0.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17lr6krxqs2lpg7a0ba4wfhbblyqrisybhxwvkl814rnpy2ykqjh") (f (quote (("debug-derive"))))))

(define-public crate-risc0-zeroio-derive-1.0.0-rc.2 (c (n "risc0-zeroio-derive") (v "1.0.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p54rylchyffy66j6g83y0687rs13k15w037mylhcd73ma538cls") (f (quote (("debug-derive"))))))

(define-public crate-risc0-zeroio-derive-0.12.0-rc.1 (c (n "risc0-zeroio-derive") (v "0.12.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x685l4apq5hcpagi3b2i9mjv9vvch8xsf50d2cjmvsc4ya0p02f") (f (quote (("debug-derive"))))))

(define-public crate-risc0-zeroio-derive-0.12.0-rc.2 (c (n "risc0-zeroio-derive") (v "0.12.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00fmvkiigjjskzqbyckvsc8gq8npqdwb5rpcm1fjxkbigdpir0qi") (f (quote (("debug-derive"))))))

(define-public crate-risc0-zeroio-derive-0.12.0 (c (n "risc0-zeroio-derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ld99qkkvfsnqq3rqf8yn9yzaxndm69ddzzawqfmps200nnhqw0b") (f (quote (("debug-derive"))))))

(define-public crate-risc0-zeroio-derive-0.13.0 (c (n "risc0-zeroio-derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08q7qlrpqa7zyzla04a98lg6w891kq3nca75hnpkkjkn3k4w9x53") (f (quote (("debug-derive"))))))

(define-public crate-risc0-zeroio-derive-0.14.0-rc.1 (c (n "risc0-zeroio-derive") (v "0.14.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nszh1ncgzxlyn0c9d7lwk6splwhjidlbng1i7cwjhfx7r0q61cy") (f (quote (("debug-derive"))))))

(define-public crate-risc0-zeroio-derive-0.14.0 (c (n "risc0-zeroio-derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zasqfnscrd1zbp6yag9ywvjxkfl21r0d48kq1gj1ngyg8gwmcpr") (f (quote (("debug-derive"))))))

