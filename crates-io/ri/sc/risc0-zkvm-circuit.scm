(define-module (crates-io ri sc risc0-zkvm-circuit) #:use-module (crates-io))

(define-public crate-risc0-zkvm-circuit-0.1.0 (c (n "risc0-zkvm-circuit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-zkp-core") (r "^0.2") (f (quote ("cxx"))) (d #t) (k 0)) (d (n "risc0-zkvm-platform") (r "^0.1") (d #t) (k 0)))) (h "1hpvm91f58251yqwacsxrm544zj9f6yrcnn3qipvfmj27q9j2isv") (l "risc0-zkvm-circuit")))

(define-public crate-risc0-zkvm-circuit-0.2.0 (c (n "risc0-zkvm-circuit") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-zkp-core") (r "^0.2") (f (quote ("cxx"))) (d #t) (k 0)) (d (n "risc0-zkvm-platform") (r "^0.2") (d #t) (k 0)))) (h "1dh9yb6bmn5jflr6ay4nvdxycxzhi4za4smyjmw7blw3cpnr9rqh") (l "risc0-zkvm-circuit")))

(define-public crate-risc0-zkvm-circuit-0.4.0 (c (n "risc0-zkvm-circuit") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-zkp-core") (r "^0.4") (f (quote ("cxx"))) (d #t) (k 0)) (d (n "risc0-zkvm-platform") (r "^0.4") (d #t) (k 0)))) (h "0m9jqnzqgfp11wcivj249dxc7xyqwk8fz7ki4inr56kbx17lkzm4") (l "risc0-zkvm-circuit")))

(define-public crate-risc0-zkvm-circuit-0.6.0 (c (n "risc0-zkvm-circuit") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "risc0-zkp-core") (r "^0.6") (f (quote ("cxx"))) (d #t) (k 0)) (d (n "risc0-zkvm-platform") (r "^0.6") (d #t) (k 0)))) (h "0mh43p21837qs1jv1fiq1xsdbibi4wsb5dmadcl6h3pvifwp4lca") (l "risc0-zkvm-circuit")))

(define-public crate-risc0-zkvm-circuit-0.11.0-rc.1 (c (n "risc0-zkvm-circuit") (v "0.11.0-rc.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-zkp") (r "^0.11.0-rc.1") (d #t) (k 0)))) (h "0kgdi4xdjldfahm1mnynywdlah927qmv4v467w4618m6gz73fvgp")))

(define-public crate-risc0-zkvm-circuit-0.11.0-rc.2 (c (n "risc0-zkvm-circuit") (v "0.11.0-rc.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-zkp") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "0ncmwlpyn8y9avzmgigcc5cx76cr67f9a9kv7ak01xj516j7vydj")))

(define-public crate-risc0-zkvm-circuit-0.11.0-rc.3 (c (n "risc0-zkvm-circuit") (v "0.11.0-rc.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-zkp") (r "^0.11.0-rc.3") (d #t) (k 0)))) (h "1dv24bhk0nh433fr1hfr87hn5ad2n6sgq9lz17br0gighgvgbsw8")))

(define-public crate-risc0-zkvm-circuit-0.11.0-rc.4 (c (n "risc0-zkvm-circuit") (v "0.11.0-rc.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-zkp") (r "^0.11.0-rc.4") (d #t) (k 0)))) (h "1vbrn7d3ffy3q4vx54w0cpm0wqrijg7wpfxxdfi81153g2yh8a4q")))

(define-public crate-risc0-zkvm-circuit-0.11.0 (c (n "risc0-zkvm-circuit") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-zkp") (r "^0.11.0-rc.4") (d #t) (k 0)))) (h "0r85pqkm0f00f5v71q4gv0pmvzq2r654kgqfsxg5mggv7bxk5j2x")))

(define-public crate-risc0-zkvm-circuit-0.11.1 (c (n "risc0-zkvm-circuit") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "risc0-zkp") (r "^0.11") (d #t) (k 0)))) (h "0cwk5sv7aa41j775xrfjmz3fk0j1h238w0hkymazd7lf2mj491yq")))

