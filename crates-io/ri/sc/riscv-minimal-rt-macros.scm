(define-module (crates-io ri sc riscv-minimal-rt-macros) #:use-module (crates-io))

(define-public crate-riscv-minimal-rt-macros-0.1.5 (c (n "riscv-minimal-rt-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.13") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1qiawpmiwnvznm5ngv7cysipiar8jrx6d9li2imi2m52kd5j60lz")))

