(define-module (crates-io ri sc riscy-isa) #:use-module (crates-io))

(define-public crate-riscy-isa-0.1.0 (c (n "riscy-isa") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "1zvsj07njang6c6cyldyf8i8kjlxhig888ih3bk09c19qirpz6ic")))

(define-public crate-riscy-isa-0.1.1 (c (n "riscy-isa") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "0hszzicc7q9fcd23jdafapq82y5jk0ay8yf0mn8v2a0cmph3rvzc")))

