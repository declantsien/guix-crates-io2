(define-module (crates-io ri sc risc0-zkvm-platform-sys) #:use-module (crates-io))

(define-public crate-risc0-zkvm-platform-sys-0.7.0-dev.1 (c (n "risc0-zkvm-platform-sys") (v "0.7.0-dev.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1plqp1kl6dl9qm3rvjw2ki0qn7bk6lb740181zcd47yv9rpw4yqy") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.7.0-dev.2 (c (n "risc0-zkvm-platform-sys") (v "0.7.0-dev.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0pvwmaxn5x8gaj0a6nibaiqcrljwlz1ixfb0a1l6dx86vmnc9y34") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.7.0-rc.1 (c (n "risc0-zkvm-platform-sys") (v "0.7.0-rc.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1iydw7dfmvcaab52xlbyx9kcl3z29zjrcrph57lm5s7r9b61dmyy") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.7.0 (c (n "risc0-zkvm-platform-sys") (v "0.7.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0mxz648rj11pd95dnk0v7wg0illxs95x2a7i25lnzl0h10pzvzi4") (y #t) (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.7.1 (c (n "risc0-zkvm-platform-sys") (v "0.7.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1n3fx3ms815294yqbqfbfpixbf4rh8yqzyjybbyhpwci1kn9sm7y") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.7.2 (c (n "risc0-zkvm-platform-sys") (v "0.7.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0lmg2cfsnivigc56k0rn3g78iik65lg5bajrc8hcrbj22zx713h3") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.8.0 (c (n "risc0-zkvm-platform-sys") (v "0.8.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0qzsri2m8b6zyph4n34a30p4kxcz89gbqivg2mwx96ggcv26iydh") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.9.0 (c (n "risc0-zkvm-platform-sys") (v "0.9.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1kg2fm40s73z4ydqyka7frdbv4gyjlzqzszv4b3n7rrjqvcb2fl2") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.10.0 (c (n "risc0-zkvm-platform-sys") (v "0.10.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "061dgi5abib12hi9ravvg548lmx1rm4x2dzgsb645jxpx7a5hiph") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.11.0-rc.1 (c (n "risc0-zkvm-platform-sys") (v "0.11.0-rc.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0ar3cpvf288pl813rwv1nn7vbdz90frwa6y3jm3l79cdbrsq4c1q") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.11.0-rc.2 (c (n "risc0-zkvm-platform-sys") (v "0.11.0-rc.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "053jbn22ysz7sh2ww5kf3ax70ngszpn5j3my7mbz93gm78kxf5gk") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.11.0-rc.3 (c (n "risc0-zkvm-platform-sys") (v "0.11.0-rc.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "12g9nk25rmphxwzqal1ijcvk6qz18yr4jhvmf8ljphkq7ipvnwqv") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.11.0-rc.4 (c (n "risc0-zkvm-platform-sys") (v "0.11.0-rc.4") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "15k73ws837wb5fqq9kkqrk5bgp7yzrwz4sjlw9bn2kf5adnlc7y3") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.11.0 (c (n "risc0-zkvm-platform-sys") (v "0.11.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "08y9cw594k29zh9yc7kmkh0f6s33r2n2j674m74lb7xi8v50sz1v") (l "risc0-zkvm-platform-sys")))

(define-public crate-risc0-zkvm-platform-sys-0.11.1 (c (n "risc0-zkvm-platform-sys") (v "0.11.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0d0a2135j0lj954a6pbmpsv07mh1nfzbp0d0dy70bvbqar3ql3ay") (l "risc0-zkvm-platform-sys")))

