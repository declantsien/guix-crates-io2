(define-module (crates-io ri sc riscu) #:use-module (crates-io))

(define-public crate-riscu-0.1.0 (c (n "riscu") (v "0.1.0") (d (list (d (n "byteorder") (r "~1.3.4") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "goblin") (r "~0.2.3") (d #t) (k 0)) (d (n "tempfile") (r "~3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "~1.0.22") (d #t) (k 0)))) (h "1n2gpf6r4m14w820va9zzsmb94biw7z8xm96mw9n3jvdc6b7j51c")))

(define-public crate-riscu-0.2.0 (c (n "riscu") (v "0.2.0") (d (list (d (n "byteorder") (r "~1.3.4") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "goblin") (r "~0.2.3") (d #t) (k 0)) (d (n "tempfile") (r "~3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "~1.0.22") (d #t) (k 0)))) (h "0l2rrfyrirpb0vilgdkhgkvclva4axjzcvv16bk27m1gjsx0c3yl")))

(define-public crate-riscu-0.3.0 (c (n "riscu") (v "0.3.0") (d (list (d (n "byteorder") (r "~1.3.4") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "goblin") (r "~0.2.3") (d #t) (k 0)) (d (n "tempfile") (r "~3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "~1.0.22") (d #t) (k 0)))) (h "17h3xap06r664zr0hj6rvdxljiypy1mb7qn1vqvcvf3djrhsj3nv")))

(define-public crate-riscu-0.3.1 (c (n "riscu") (v "0.3.1") (d (list (d (n "byteorder") (r "~1.3.4") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "goblin") (r "~0.2.3") (d #t) (k 0)) (d (n "tempfile") (r "~3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "~1.0.22") (d #t) (k 0)))) (h "187svmzlbb8d7l3ip50x8m79kc0rnz44yis97j1cq0vpiq1535yf")))

(define-public crate-riscu-0.3.2 (c (n "riscu") (v "0.3.2") (d (list (d (n "byteorder") (r ">=1.3.4, <1.4.0") (d #t) (k 0)) (d (n "cargo-husky") (r ">=1.0.0, <2.0.0") (f (quote ("user-hooks"))) (k 2)) (d (n "goblin") (r ">=0.2.3, <0.3.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <3.2.0") (d #t) (k 2)) (d (n "thiserror") (r ">=1.0.22, <1.1.0") (d #t) (k 0)))) (h "1rwfc9fqp8djhf7dk0d789da4iqn70scxh7vrsznr48fq4ccjmfp")))

(define-public crate-riscu-0.4.0 (c (n "riscu") (v "0.4.0") (d (list (d (n "byteorder") (r "~1.3.4") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "goblin") (r "~0.2.3") (d #t) (k 0)) (d (n "tempfile") (r "~3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "~1.0.22") (d #t) (k 0)))) (h "0wy4lykcglqs8fki2bk3kc68ng6hnh99p0qldxmf9mz9l0hia24d")))

(define-public crate-riscu-0.5.0 (c (n "riscu") (v "0.5.0") (d (list (d (n "byteorder") (r "~1.4.2") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "goblin") (r "~0.3.4") (d #t) (k 0)) (d (n "tempfile") (r "~3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "~1.0.24") (d #t) (k 0)))) (h "0953vhgjyfw8wfx0g8wbw6xy70kaxdlwmj6h7mib9jk89f3i85d7")))

