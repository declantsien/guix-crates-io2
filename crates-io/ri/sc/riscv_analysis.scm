(define-module (crates-io ri sc riscv_analysis) #:use-module (crates-io))

(define-public crate-riscv_analysis-0.1.0-alpha (c (n "riscv_analysis") (v "0.1.0-alpha") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1kfn8d80jgwzvgjx5mddc5id138h3iwwmd885xp5n990r15371pq")))

