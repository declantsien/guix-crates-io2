(define-module (crates-io ri pc ripcalc) #:use-module (crates-io))

(define-public crate-ripcalc-0.1.3 (c (n "ripcalc") (v "0.1.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "1dn78rbgzn8ni7b72ci97dvfs32l3fa93zzyz7b97amyhdjh92fj")))

(define-public crate-ripcalc-0.1.5 (c (n "ripcalc") (v "0.1.5") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "16036xpfqcm1arz2fxszfa52r7vf1dip82j34ivic0k96yjj3f57")))

(define-public crate-ripcalc-0.1.6 (c (n "ripcalc") (v "0.1.6") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "13gy1s6f1vp7lgx25zs8vdji3ccrd6xjqby35sf436j9lnq17c4f")))

(define-public crate-ripcalc-0.1.7 (c (n "ripcalc") (v "0.1.7") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0nyrsbpb9fixlyd726k5jgcx3s6fdi7xwmihzn8k8f7yy62mimsv")))

(define-public crate-ripcalc-0.1.9 (c (n "ripcalc") (v "0.1.9") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25") (d #t) (k 0)))) (h "1zdy14jzvpic3wqwvxli76vy0zgkqi2f2nq0chrcwi4f1xmcclwh")))

(define-public crate-ripcalc-0.1.10 (c (n "ripcalc") (v "0.1.10") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25") (d #t) (k 0)))) (h "1hpcb2rkpjwpfnhavibnjfm5l577cmcpy11fyvafs2l7brvfrlvc")))

(define-public crate-ripcalc-0.1.11 (c (n "ripcalc") (v "0.1.11") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "0hxqpc10cc8qpc38y273bs0wbgilp5l3j1bjav160rvgs6v46vwv")))

