(define-module (crates-io ri pc ripcd) #:use-module (crates-io))

(define-public crate-ripcd-0.1.0 (c (n "ripcd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1igqsyvdikzx3ffx723nl7agnka5chi2115md01sjiliax34qp5x")))

