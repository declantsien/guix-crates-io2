(define-module (crates-io ri pc ripcal) #:use-module (crates-io))

(define-public crate-ripcal-0.1.0 (c (n "ripcal") (v "0.1.0") (h "1r7gri1i5iipzi6zbyiy4z7hivqn1kfrahm2n3hwxfcixawlsj2r") (y #t)))

(define-public crate-ripcal-0.2.0 (c (n "ripcal") (v "0.2.0") (h "1h02dcg17lx3jfakjsqxnwani450i6da22hsfaybk2acq20q2kbz") (y #t)))

(define-public crate-ripcal-0.2.1 (c (n "ripcal") (v "0.2.1") (h "1ljmv9nmabygdybh5n6nabddyhj4pp3hmspp387h2nklm4b2fpvp") (y #t)))

(define-public crate-ripcal-0.3.0 (c (n "ripcal") (v "0.3.0") (h "1r5gichms65q1v6cjp21y7k8i7kxaqlg0nxmfzi79vnzww368mxa") (y #t)))

(define-public crate-ripcal-0.4.0 (c (n "ripcal") (v "0.4.0") (h "1ppvnx1grk4q1d3hrm2amz3ryrnf31mbfpsvmvfh8yhk3ymwprr9")))

(define-public crate-ripcal-0.5.0 (c (n "ripcal") (v "0.5.0") (h "0387h4dzrg8mga0lnsdbkyid44fdv3gw3ba4z2348zbrrfj0jyil") (y #t)))

(define-public crate-ripcal-0.5.1 (c (n "ripcal") (v "0.5.1") (h "1xi49h1sjrr2mm332mcs1ly941rpb9ah0j91iy1qdad1w3ssnz43") (y #t)))

(define-public crate-ripcal-0.5.2 (c (n "ripcal") (v "0.5.2") (h "1832clx1q4a5ilk8z0lfxqd0j960wj7l371pgqb18jsk225dkvnp")))

(define-public crate-ripcal-0.6.0 (c (n "ripcal") (v "0.6.0") (h "0rf4qrwn7205bcf05mv7svrlnchjk67z5r3xqcgxv44phs122zp9")))

