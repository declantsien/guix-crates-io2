(define-module (crates-io ri ce ricecomp) #:use-module (crates-io))

(define-public crate-ricecomp-0.1.0 (c (n "ricecomp") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1az7bk1saxv7gp99ab4ijgx9mbg89w9rqxkijhn2kmwmpsdxq9r2")))

(define-public crate-ricecomp-0.1.1 (c (n "ricecomp") (v "0.1.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vm8fa8qwjcq76bmcl41il663s76h9nsj4mb0hl0qvpzbp3j76mz")))

(define-public crate-ricecomp-0.1.2 (c (n "ricecomp") (v "0.1.2") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1p4wwwpqq8i1rhqgfajx25qij89y1s37k1d86k8dw6w9vjp2nyr2")))

