(define-module (crates-io ri bz ribzip2) #:use-module (crates-io))

(define-public crate-ribzip2-0.1.0 (c (n "ribzip2") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1a01pylhlkk8afqhczq7gpdgjcqms7nv3wgdpasp7cyhjllwnypq")))

(define-public crate-ribzip2-0.1.1 (c (n "ribzip2") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0n4j769k42j54vmdsjqa5kg1bb0gkmjll53kzblchpp4armab2ir")))

(define-public crate-ribzip2-0.1.3 (c (n "ribzip2") (v "0.1.3") (d (list (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "02nz3jrqn6ma350zh2461qfybi916514awnxgrcksglygr8xvwqk")))

(define-public crate-ribzip2-0.2.0 (c (n "ribzip2") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "11vylmj2yilcwqdgqfayqgcha8v8iffpn94bf65xgnd14scc27k9")))

(define-public crate-ribzip2-0.3.1 (c (n "ribzip2") (v "0.3.1") (d (list (d (n "libribzip2") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "130yqadyyc5bci546a4mcmdqndjqzlis9wq12hba1blszrn6j34h")))

(define-public crate-ribzip2-0.3.2 (c (n "ribzip2") (v "0.3.2") (d (list (d (n "libribzip2") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1yayw63plac6p0f0x5m471mb7ibqas72hxnqdmdydmam08dl5gm8")))

(define-public crate-ribzip2-0.4.0 (c (n "ribzip2") (v "0.4.0") (d (list (d (n "libribzip2") (r "^0.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1abqi2nif573r015jxf9ph844xihf31206mvn4pmn5r023wcn9yq")))

(define-public crate-ribzip2-0.5.0 (c (n "ribzip2") (v "0.5.0") (d (list (d (n "libribzip2") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0ymiigahb0vdzjhnvfi6nk4g9yr5d7i1mj7lk4rzil89cqs19arr")))

