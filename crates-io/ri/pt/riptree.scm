(define-module (crates-io ri pt riptree) #:use-module (crates-io))

(define-public crate-riptree-0.1.0 (c (n "riptree") (v "0.1.0") (h "132ksl7ijkgz1wabk435anjg4087sa2s86y3p2xfwgd0msq4qjk5")))

(define-public crate-riptree-0.2.0 (c (n "riptree") (v "0.2.0") (h "1v42iy2kwq3m1a3f7vqq6zj7qyw5srq7df0akx19k40zwshw9gja")))

(define-public crate-riptree-0.3.0 (c (n "riptree") (v "0.3.0") (h "09r27b3vija54mcz0xbpwy31i3z08hj2rsx6ipj8x9f6r0kb8gbc")))

(define-public crate-riptree-0.3.1 (c (n "riptree") (v "0.3.1") (h "0zpi0hy91fp0casangxp8n6m5rwg5i1xivj2xa9ws5yyi3kar9pp")))

(define-public crate-riptree-0.3.2 (c (n "riptree") (v "0.3.2") (h "095wkxmsyvwlq1n9d18dxnb0ap10w4hd86xfmllr7dnka96r579c")))

(define-public crate-riptree-0.3.3 (c (n "riptree") (v "0.3.3") (h "1k7x1hjb29vgbgbifyafbk7m9ssh4zb3cm58kd59mp9ladc0cabp")))

(define-public crate-riptree-0.3.4 (c (n "riptree") (v "0.3.4") (h "09ncrkfaky943zlfwpi5rn6wk4s8g7g12z0vkiljixn1n7wh9v47")))

(define-public crate-riptree-0.3.5 (c (n "riptree") (v "0.3.5") (h "0mdp1c4wbr1d7v7rrrfnz4x2m8974yvdw63whc4cpfb7icjdajc8")))

(define-public crate-riptree-0.3.6 (c (n "riptree") (v "0.3.6") (h "0jr45x8gggl66m2gskf588qm32hwv3b1na04apln4ch52isgbksw")))

(define-public crate-riptree-0.3.7 (c (n "riptree") (v "0.3.7") (h "08biqxcic8cr3hr6av1pynllmlsvr6bag0nxbi7bqa7yvxynrhk1")))

(define-public crate-riptree-0.3.8 (c (n "riptree") (v "0.3.8") (h "18nr7xqkx1bldk9ak3dhna5gk2bj79jllc9dj5mrd26hjp50lq7d")))

