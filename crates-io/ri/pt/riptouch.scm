(define-module (crates-io ri pt riptouch) #:use-module (crates-io))

(define-public crate-riptouch-0.1.0 (c (n "riptouch") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ili5kr8chwcwym6ag7kzgw9q39vfck7pkqfmxczm0b6bcmpg8sy")))

(define-public crate-riptouch-0.2.0 (c (n "riptouch") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hli7726n8f0a1a6lk1bzg29z5d9ddibcsq7vkc3bm8jkwdf9rn8")))

(define-public crate-riptouch-0.3.0 (c (n "riptouch") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r0kmaafkb3vzafhwxln84czhp89ic9rhdlnxs7xycigkm2d7i4i")))

(define-public crate-riptouch-0.4.0 (c (n "riptouch") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "12ivq6rn8l2glryh9jk4dgqgnsn1mrj76xmkq2kaf7pdd3ggynk3")))

