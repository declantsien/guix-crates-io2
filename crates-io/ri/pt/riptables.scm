(define-module (crates-io ri pt riptables) #:use-module (crates-io))

(define-public crate-riptables-0.1.0 (c (n "riptables") (v "0.1.0") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "rstring-builder") (r "^0.1.3") (d #t) (k 0)) (d (n "text-reader") (r "^0.1") (d #t) (k 0)))) (h "05bd1lvb81zxn93zzmbq3qpgmahxqnxslv962g2af628q6fglfk8")))

