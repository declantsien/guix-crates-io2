(define-module (crates-io ri ot riot) #:use-module (crates-io))

(define-public crate-riot-0.1.0 (c (n "riot") (v "0.1.0") (h "0ic529gzjx331f6bwx96sl587agksgkah98il3kirrbvv1wm3yj0")))

(define-public crate-riot-0.1.1 (c (n "riot") (v "0.1.1") (h "1iksv63sjx4ksgsk8bhwcbpkic16yx01ss58azra7gjcggba40wg")))

