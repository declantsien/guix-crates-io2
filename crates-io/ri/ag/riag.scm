(define-module (crates-io ri ag riag) #:use-module (crates-io))

(define-public crate-riag-0.1.0 (c (n "riag") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0z7nr0iq9zbqd00pavbnpkvchjcnbm8va4w71r23dqh3i4ajddrr")))

