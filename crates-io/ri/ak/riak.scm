(define-module (crates-io ri ak riak) #:use-module (crates-io))

(define-public crate-riak-0.1.0 (c (n "riak") (v "0.1.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "1j9yym2wyhpy8f5i8n5f3ypfa7s3qflxkbjc4lpdvpvfbdckbdg7") (y #t)))

(define-public crate-riak-0.1.7 (c (n "riak") (v "0.1.7") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "0b6yjrdcnajk8m58bicw52gmcjqrcwjm25aznvi0y02sabmvy1rc") (y #t)))

(define-public crate-riak-0.2.4 (c (n "riak") (v "0.2.4") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "0nxxgfsdw6y24j5m3lhfz3smqb690g0yb2h6nlwnwx5n6rvbnwn9") (y #t)))

(define-public crate-riak-0.2.5 (c (n "riak") (v "0.2.5") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "1w0lmsd29fi48ih7pkbx6w9d6mvk2j307n71y9fmc7lqyrli5257") (y #t)))

(define-public crate-riak-0.2.7 (c (n "riak") (v "0.2.7") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "1j687ir4bkiijyjcpn6vzv6zmvv21b12nhkdmwbipimccx7p06md") (y #t)))

(define-public crate-riak-0.3.0 (c (n "riak") (v "0.3.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "12dsamb30qwdqbxc61faqp2qm1r9nq6cz3zb6p1gk323f650v7d5") (y #t)))

(define-public crate-riak-0.3.2 (c (n "riak") (v "0.3.2") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.1.0") (d #t) (k 0)))) (h "07zm0562m67cj0r48i99clp45999zw83h42jab7qf35sxbgjwh2a") (y #t)))

(define-public crate-riak-0.3.3 (c (n "riak") (v "0.3.3") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.1.0") (d #t) (k 0)))) (h "0l17zgs1sdd027d4gb56pvbqxgby0fyd9l1nvg3xjlj33c5p12y8") (y #t)))

(define-public crate-riak-0.3.4 (c (n "riak") (v "0.3.4") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.1.0") (d #t) (k 0)))) (h "1n4f2qgnv9b725s9ja1yin4slbav2799dci5yzsrfmi4hvhdrz6d")))

