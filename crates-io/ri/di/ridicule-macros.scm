(define-module (crates-io ri di ridicule-macros) #:use-module (crates-io))

(define-public crate-ridicule-macros-0.2.0 (c (n "ridicule-macros") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "ridicule") (r "^0.2.0") (d #t) (k 2)))) (h "0fgjyl1z934nkpb5xqcy7svk98hw2hp6gvlric9vjwnqgh4z26nh") (y #t)))

(define-public crate-ridicule-macros-0.3.0 (c (n "ridicule-macros") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "ridicule") (r "^0.3.0") (d #t) (k 2)))) (h "1symz1apxn4qbry30dfbhmqmvrn5ydr72lnl28p46mfbkwf13lz6")))

