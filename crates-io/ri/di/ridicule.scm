(define-module (crates-io ri di ridicule) #:use-module (crates-io))

(define-public crate-ridicule-0.1.0 (c (n "ridicule") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "155b6639iwzqxcbp80a1w2j56aivc3hmxqbrhfms12ly8c9vn1m9") (y #t)))

(define-public crate-ridicule-0.2.0 (c (n "ridicule") (v "0.2.0") (d (list (d (n "predicates") (r "^3.0.1") (d #t) (k 0)) (d (n "ridicule-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0lc4zqbn06187ndqc677ic07kn5hysd8lhlxxbikvwn20yzkq135") (y #t)))

(define-public crate-ridicule-0.3.0 (c (n "ridicule") (v "0.3.0") (d (list (d (n "predicates") (r "^3.0.1") (d #t) (k 0)) (d (n "ridicule-macros") (r "^0.3.0") (d #t) (k 0)))) (h "19a6qza7hjq5b12k6n79mdnng53jbd3vmg2j75xxa8mpmy4d602c")))

