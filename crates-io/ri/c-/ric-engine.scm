(define-module (crates-io ri c- ric-engine) #:use-module (crates-io))

(define-public crate-ric-engine-0.1.0 (c (n "ric-engine") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "14lk8r8slhyv1h2d348v9zg6d98b470wzlhr0zg08ac0sp6l006w")))

