(define-module (crates-io ri c- ric-repl) #:use-module (crates-io))

(define-public crate-ric-repl-0.1.0 (c (n "ric-repl") (v "0.1.0") (d (list (d (n "ric-engine") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1iaxzv6d9c5ybgb75hvghl0z32166adlzw3x0ryvgpyjyiyayp1m")))

