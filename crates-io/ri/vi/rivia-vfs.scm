(define-module (crates-io ri vi rivia-vfs) #:use-module (crates-io))

(define-public crate-rivia-vfs-0.0.1 (c (n "rivia-vfs") (v "0.0.1") (h "08zbiz7y542h3iy85kgn6738pknrx0rdmylgikvl14qc3md90pk7")))

(define-public crate-rivia-vfs-0.1.1 (c (n "rivia-vfs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rivia") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1wfn4w0wgsw59w2019ah3agphr8x1j8vbk72608905y08i11mbh3") (y #t)))

(define-public crate-rivia-vfs-0.1.2 (c (n "rivia-vfs") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rivia") (r "^0.1") (d #t) (k 0)))) (h "1fqlchkpkka78hmi9385x9nhx5fs9hyfvx1l09kz7rygjllcf4bk") (y #t)))

(define-public crate-rivia-vfs-0.1.3 (c (n "rivia-vfs") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rivia") (r "^0.1") (d #t) (k 0)))) (h "1w0zb2gb9wc0jdjayxrp5s95jm4nxp61xjvqfd38spjb4p5rwlkx") (y #t)))

(define-public crate-rivia-vfs-0.1.4 (c (n "rivia-vfs") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rivia") (r "^0.1.4") (d #t) (k 0)))) (h "1givx1whffal6va2b1wfpxndsg0zkpfwc627b3lxmjda245pciwx")))

(define-public crate-rivia-vfs-0.1.5 (c (n "rivia-vfs") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rivia") (r "^0.1.5") (d #t) (k 0)))) (h "1chis6fg9qsabch0nh6l0h2mq5y9m4g7km6y81a0ww2p2zk45in4")))

(define-public crate-rivia-vfs-0.2.1 (c (n "rivia-vfs") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rivia") (r "^0.2.1") (d #t) (k 0)))) (h "1dyj4h34ybafnx4lcf1h4j2lxkqr60kzpd6h61rxxdyaqf1b1zay")))

(define-public crate-rivia-vfs-0.2.2 (c (n "rivia-vfs") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rivia") (r "^0.2.2") (d #t) (k 0)))) (h "0yn36igqxvwhpqjsslj9nrb94cjvj4zy0ma5dh96g5411ki5lf43")))

(define-public crate-rivia-vfs-0.2.3 (c (n "rivia-vfs") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rivia") (r "^0.2.4") (d #t) (k 0)))) (h "1gg9b3n7bh956jffpwnx859ikawrwpql9b84q4vy6bvxsi5999dm")))

(define-public crate-rivia-vfs-0.2.4 (c (n "rivia-vfs") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rivia") (r "^0.2.5") (d #t) (k 0)))) (h "0r9vg3c1k0kfx1cbr9g0qyqyx5p8havw22pjsiar0iwwya46hv4k")))

(define-public crate-rivia-vfs-0.2.5 (c (n "rivia-vfs") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rivia") (r "^0.2.6") (d #t) (k 0)))) (h "04fjdi9pd419z8nylylb78a14kmnklhzzdv5wvmzy7br9k87hd12")))

