(define-module (crates-io ri vi rivia) #:use-module (crates-io))

(define-public crate-rivia-0.0.1 (c (n "rivia") (v "0.0.1") (h "0mlkh9bn74h457cpl4mm7zbjssi69p9wp5bjxq0k88j4cys46i4k")))

(define-public crate-rivia-0.1.1 (c (n "rivia") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01kyhwmx3gpc71lk24hfxkaf8418srik6drfy4ni72fmv3gjdzw7")))

(define-public crate-rivia-0.1.2 (c (n "rivia") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ij5zpnzxz8b6ppyg7laxcv83mwxx59fi6bz03b8amkk48g2icxh")))

(define-public crate-rivia-0.1.3 (c (n "rivia") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0xxgrqz1i3a1p0al33rlvj9xld1bs21238xz28s2qigj1pma3n0a")))

(define-public crate-rivia-0.1.4 (c (n "rivia") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "18gvi98y7sw3fn773cdbd6pa0xnhgkz9rsrrnw7x5wkipx56idp0")))

(define-public crate-rivia-0.1.5 (c (n "rivia") (v "0.1.5") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0minixyv163f53lhqqgqk5avpsblhxlm6frh5cbv5q7j4wzlcx3v")))

(define-public crate-rivia-0.2.1 (c (n "rivia") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1vd2n2j4nyrk7dnh1npnrd20465dl7lfdzs4mszkqn8kr6fyr9s5")))

(define-public crate-rivia-0.2.2 (c (n "rivia") (v "0.2.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "03p0b771x27lrja3vgdsrv10xniij72x8daqh2agdnwp811fq83a")))

(define-public crate-rivia-0.2.4 (c (n "rivia") (v "0.2.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1zy89mv007zcxk8c71fhs64ybzmzgp7lxfqxawghm0v61hg2rqdl")))

(define-public crate-rivia-0.2.5 (c (n "rivia") (v "0.2.5") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "12725bv9ky4ihkcx8kizyhx57f831v9lq7xqqc20wcb8gh384ys4")))

(define-public crate-rivia-0.2.6 (c (n "rivia") (v "0.2.6") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0hsn2shphswcysl61zgrwf7m8hxab2vvhicsbiyy6m05cm83vhnk")))

