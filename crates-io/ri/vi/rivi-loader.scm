(define-module (crates-io ri vi rivi-loader) #:use-module (crates-io))

(define-public crate-rivi-loader-0.1.0 (c (n "rivi-loader") (v "0.1.0") (d (list (d (n "ash") (r "^0.32.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "gpu-allocator") (r "^0.8.0") (d #t) (k 0)) (d (n "metal") (r "^0.23.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rspirv") (r "^0.10.0") (d #t) (k 0)))) (h "1k96yaam52nkzld7p64hr8qwk6va606fj84hnv37r7rl5frsm7v5")))

(define-public crate-rivi-loader-0.1.1 (c (n "rivi-loader") (v "0.1.1") (d (list (d (n "ash") (r "^0.32.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "gpu-allocator") (r "^0.8.0") (d #t) (k 0)) (d (n "metal") (r "^0.23.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rspirv") (r "^0.10.0") (d #t) (k 0)))) (h "07zir87wc5lfbkx87g6308nmm8h0bldwl0c44jg45bhilrndaxni")))

(define-public crate-rivi-loader-0.1.2 (c (n "rivi-loader") (v "0.1.2") (d (list (d (n "ash") (r "^0.32.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "gpu-allocator") (r "^0.8.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rspirv") (r "^0.10.0") (d #t) (k 0)))) (h "0vyl31gx54kn4dmwwk6096fdv5pd7mwhrixznqm8kiwas2b7zz12")))

(define-public crate-rivi-loader-0.1.3 (c (n "rivi-loader") (v "0.1.3") (d (list (d (n "ash") (r "^0.33.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "gpu-allocator") (r "^0.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rspirv") (r "^0.11.0") (d #t) (k 0)))) (h "1hchcwp15jls42284bsbc8wpk6ccnimvfyk27w7acfn8mhank85c")))

(define-public crate-rivi-loader-0.1.4 (c (n "rivi-loader") (v "0.1.4") (d (list (d (n "ash") (r "^0.35.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "gpu-allocator") (r "^0.14.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rspirv") (r "^0.11.0") (d #t) (k 0)))) (h "0a8xqnfaxcmwx9278hcascyb3wkgkrfjf6bp2g7kmjy9vlm87a8c")))

(define-public crate-rivi-loader-0.1.5 (c (n "rivi-loader") (v "0.1.5") (d (list (d (n "ash") (r "^0.36.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "gpu-allocator") (r "^0.17.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rspirv") (r "^0.11.0") (d #t) (k 0)))) (h "05520vksgvly7w1sc15gw5958c906gm2i1s941wbvddq3sxzynpy")))

