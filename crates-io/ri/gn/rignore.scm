(define-module (crates-io ri gn rignore) #:use-module (crates-io))

(define-public crate-rignore-0.1.0 (c (n "rignore") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p2xs9pdwl7ldc96qz7sdvnmsczdy48aqirsb85iaxli42bayy1i")))

(define-public crate-rignore-1.0.0 (c (n "rignore") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.20") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n22mlxbj0bfdhyqcwjriibcnlglp19cq19vivlqyx9asx3aqg3y")))

(define-public crate-rignore-1.0.1 (c (n "rignore") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.20") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "007klfzwi6q7vyflszsllgpr66gxdd7bjn6jknz8rq3qhz6y3g15")))

(define-public crate-rignore-1.0.2 (c (n "rignore") (v "1.0.2") (d (list (d (n "clap") (r "^3.2.20") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1snrn0msyb5zq5gmdrc4y610f3a0y9ys4y4vrpah2qdg55kcvyn6")))

(define-public crate-rignore-1.0.3 (c (n "rignore") (v "1.0.3") (d (list (d (n "clap") (r "^3.2.20") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13w7khlxk6sa5210r0jv52n1sngm42w0v958h3mzwyyyxr072i6l")))

(define-public crate-rignore-1.0.4 (c (n "rignore") (v "1.0.4") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1svfad1ccm9r1pl4aw9r39ai6a04igk7s3wj70fwly4dwanqa2k5")))

(define-public crate-rignore-1.0.5 (c (n "rignore") (v "1.0.5") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p39rizfs112bm9s9gj1b34b7fp6v91y46m3igq27vphigp2kydc")))

(define-public crate-rignore-1.0.6 (c (n "rignore") (v "1.0.6") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0x17kl56xb0dbgp6k1h85a5lj0xgfdf1r5z1ibirk9f7gyrhcwcq")))

(define-public crate-rignore-1.1.0 (c (n "rignore") (v "1.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pbkq23ba2piv4712jbrzp8342cq883m0xvsgn4s3v8byg3gnpd7")))

(define-public crate-rignore-1.2.0 (c (n "rignore") (v "1.2.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pkblf4cjx742sp075xhfnc1zqicm1ds7kh8hcyzxkcflmv2bvxr")))

