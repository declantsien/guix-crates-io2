(define-module (crates-io ri am riam) #:use-module (crates-io))

(define-public crate-riam-0.1.0 (c (n "riam") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1b5pi4nvrh7ik3dn5x21sfc0jm4v5xhf3cyiijlpj9wfgfyhfs8h")))

