(define-module (crates-io ri vu rivulet) #:use-module (crates-io))

(define-public crate-rivulet-0.1.0 (c (n "rivulet") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (k 0)) (d (n "num-integer") (r "^0.1") (o #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "seahash") (r "^4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt-multi-thread" "macros"))) (k 2)) (d (n "vmap") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0lrnvll73iasrg5xpqaa6kmg3rc9q5r3r307ff07s91m9l06xlqf") (f (quote (("std" "num-integer" "vmap") ("default" "std"))))))

