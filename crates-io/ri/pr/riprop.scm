(define-module (crates-io ri pr riprop) #:use-module (crates-io))

(define-public crate-riprop-2.0.1 (c (n "riprop") (v "2.0.1") (d (list (d (n "anyhow") (r ">=1.0.0") (d #t) (k 0)) (d (n "xcb") (r ">=0.10") (d #t) (k 0)))) (h "0616479h4hfjqkwyzqng1i1jh6icg6fbpdf039cfp992kk8hfhf9")))

(define-public crate-riprop-3.0.0 (c (n "riprop") (v "3.0.0") (d (list (d (n "anyhow") (r ">=1.0.0") (d #t) (k 0)) (d (n "xcb") (r ">=0.10") (d #t) (k 0)))) (h "0jn4m1kn25nxx7cynigb8fqkib4hc5lbs619m85nb99x96kqrg9v")))

