(define-module (crates-io ri o_ rio_api) #:use-module (crates-io))

(define-public crate-rio_api-0.1.0 (c (n "rio_api") (v "0.1.0") (h "0hidvdvzkqg7vnlvmws0ixh5i2dldfkyw20kv4ghwcza5pzzaclv")))

(define-public crate-rio_api-0.2.0 (c (n "rio_api") (v "0.2.0") (h "0dfrqnxivcrfsxsi3w4qwmnki326f81k1py9gkv23b45ybdg5ibq")))

(define-public crate-rio_api-0.3.0 (c (n "rio_api") (v "0.3.0") (h "1x0mnz71d2yywx2jgwrvknlr8g9fbbp5ivhl5l6g3gas4g581xhh")))

(define-public crate-rio_api-0.3.1 (c (n "rio_api") (v "0.3.1") (h "0r1by383di400ndyf423vqh2arrj6bgwkfvilrzfx50a3i7kl6q7")))

(define-public crate-rio_api-0.4.0 (c (n "rio_api") (v "0.4.0") (h "0s0rwcvrw1snw3plnjr9cl9xqmb62cqr2qjkx874lc1j2icqrk4i") (f (quote (("generalized") ("default"))))))

(define-public crate-rio_api-0.4.1 (c (n "rio_api") (v "0.4.1") (h "1glkvx9f74d7h46blf4k0ixzifp41i1pqhngh4ds850mr1l83f8w") (f (quote (("generalized") ("default"))))))

(define-public crate-rio_api-0.4.2 (c (n "rio_api") (v "0.4.2") (h "1xms21zzcpg8p3wahvd8p62k6lj56gv6ak7m1yawz26n284paks5") (f (quote (("generalized") ("default"))))))

(define-public crate-rio_api-0.5.0 (c (n "rio_api") (v "0.5.0") (d (list (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "11xkywjicw6qpwq9jx18ynlx184jyw7hns77hknmd9ijbnxmfc2z") (f (quote (("sophia" "sophia_api") ("generalized") ("default"))))))

(define-public crate-rio_api-0.5.1 (c (n "rio_api") (v "0.5.1") (d (list (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "12jzd7snwxzfgj5k1ncq2ya874yfgpjfngxfg6y210sanbb814an") (f (quote (("sophia" "sophia_api") ("generalized") ("default"))))))

(define-public crate-rio_api-0.5.2 (c (n "rio_api") (v "0.5.2") (d (list (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "0i968nrxkqxas1aswm9f4cr96q7qk4490zylsxqakw45lg5j2h7g") (f (quote (("sophia" "sophia_api") ("generalized") ("default"))))))

(define-public crate-rio_api-0.5.3 (c (n "rio_api") (v "0.5.3") (d (list (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "1q5pp89zzxv5pwgd01vx6ywimmp8hq07983hasnc50bzka78zirz") (f (quote (("sophia" "sophia_api") ("generalized") ("default"))))))

(define-public crate-rio_api-0.6.0 (c (n "rio_api") (v "0.6.0") (d (list (d (n "sophia_api") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1wh613rwp2fpmy2q8hlxib9dcvg34dqxgn9alksw3i0d4hljxip7") (f (quote (("sophia" "sophia_api") ("generalized") ("default"))))))

(define-public crate-rio_api-0.6.1 (c (n "rio_api") (v "0.6.1") (d (list (d (n "sophia_api") (r ">=0.6, <=0.7") (o #t) (d #t) (k 0)))) (h "16dzrpl5fs7d6qvqj91590yn6gzygk8whhgd7qn22ngnfkpnzm9k") (f (quote (("sophia" "sophia_api") ("generalized") ("default"))))))

(define-public crate-rio_api-0.6.2 (c (n "rio_api") (v "0.6.2") (d (list (d (n "sophia_api") (r ">=0.6, <=0.8") (o #t) (d #t) (k 0)))) (h "1bmc1p4p2cgwj415zv1zmln9340cjfcwh9kvcsgfa3afmmrnvnbf") (f (quote (("sophia" "sophia_api") ("generalized") ("default"))))))

(define-public crate-rio_api-0.7.0 (c (n "rio_api") (v "0.7.0") (h "07qya9qydw39hifzw58xfv3ml4ldnm3adsipg759g3c1jdai00ij") (f (quote (("generalized") ("default"))))))

(define-public crate-rio_api-0.7.1 (c (n "rio_api") (v "0.7.1") (h "0xmwqgczr5kkzcyvfbapq2p74j87y7bw1kyivkiiqydnkzm9cqcz") (f (quote (("generalized") ("default"))))))

(define-public crate-rio_api-0.8.0 (c (n "rio_api") (v "0.8.0") (h "0xar9c240x3vcb85z9whvnlgd24ydl3j9332pl3dgmmfsj6ybg34") (f (quote (("generalized") ("default"))))))

(define-public crate-rio_api-0.8.2 (c (n "rio_api") (v "0.8.2") (h "0kpmzvgshbnh1cspc3sk6y3fc14h99zcbcal5nzach0gl0yi77rr") (f (quote (("generalized") ("default")))) (r "1.58")))

(define-public crate-rio_api-0.8.3 (c (n "rio_api") (v "0.8.3") (h "0ald5zrfmz2m4wrrmjs197zj5imdzx1r93gin8hiadfdsz323qa2") (f (quote (("generalized") ("default")))) (r "1.60")))

(define-public crate-rio_api-0.8.4 (c (n "rio_api") (v "0.8.4") (h "00v6d4a19gyp79b9vncm57cs0s630zk6kidkffdiz1bd1qgzl90r") (f (quote (("generalized") ("default")))) (r "1.60")))

