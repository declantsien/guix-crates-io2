(define-module (crates-io ri o_ rio_rt) #:use-module (crates-io))

(define-public crate-rio_rt-0.1.0-alpha.1 (c (n "rio_rt") (v "0.1.0-alpha.1") (h "0829az660rizdbaawx6zz8sr6xh9n8v3s7a8dapzim04cm6n8mm7")))

(define-public crate-rio_rt-0.1.0-alpha.2 (c (n "rio_rt") (v "0.1.0-alpha.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0jqvlia1dilgnnw5ri8w5n1jwz49y2vdy8w0bnjdzfidxqb5wfss")))

