(define-module (crates-io ri o_ rio_xml) #:use-module (crates-io))

(define-public crate-rio_xml-0.2.0 (c (n "rio_xml") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.15") (d #t) (k 0)) (d (n "rio_api") (r "^0.2") (d #t) (k 0)))) (h "0g77g3fql354vglvdnpz7z3iqx8r80kfmk7aacjnnm0zainqd4g5")))

(define-public crate-rio_xml-0.3.0 (c (n "rio_xml") (v "0.3.0") (d (list (d (n "quick-xml") (r "^0.16") (d #t) (k 0)) (d (n "rio_api") (r "^0.3") (d #t) (k 0)))) (h "1wm9g1bcxw2ra7r7yj096baik5zmaq0537x4rjyxd3z7bgc3cjnb")))

(define-public crate-rio_xml-0.4.0 (c (n "rio_xml") (v "0.4.0") (d (list (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "rio_api") (r "^0.4") (d #t) (k 0)))) (h "0p9hpkkhs9mw7caf438vf2988js3lfvb8hjwcbpldmd6dzdy7c4i")))

(define-public crate-rio_xml-0.4.1 (c (n "rio_xml") (v "0.4.1") (d (list (d (n "quick-xml") (r "^0.18") (d #t) (k 0)) (d (n "rio_api") (r "^0.4") (d #t) (k 0)))) (h "1aqwlv5kilcdhkpabkbbr7ay7l6rf6qkazkgr7lhjnq0nx9s68pm")))

(define-public crate-rio_xml-0.4.2 (c (n "rio_xml") (v "0.4.2") (d (list (d (n "quick-xml") (r "^0.18") (d #t) (k 0)) (d (n "rio_api") (r "^0.4") (d #t) (k 0)))) (h "0lzkmgw6vi8x0dpbg6qai1jq6snhs6m5m8np0v8i1kx43194gsh3")))

(define-public crate-rio_xml-0.5.0 (c (n "rio_xml") (v "0.5.0") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.18") (d #t) (k 0)) (d (n "rio_api") (r "^0.5") (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "1bx8p29k5vqrp8v0qf19qljjrjg0sij41dpcspxkym1vk2m78j41") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("default"))))))

(define-public crate-rio_xml-0.5.1 (c (n "rio_xml") (v "0.5.1") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.20") (d #t) (k 0)) (d (n "rio_api") (r "^0.5") (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "04f04klklg4gaqym468p03ax1rzl327gw9bbq4ck5d0dhn59yypm") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("default"))))))

(define-public crate-rio_xml-0.5.2 (c (n "rio_xml") (v "0.5.2") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.21") (d #t) (k 0)) (d (n "rio_api") (r "^0.5") (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "0l71yi1ln7jg0nmqmgsjc75i95m3gf3c246x9ryyljr69p22c2q2") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("default"))))))

(define-public crate-rio_xml-0.5.3 (c (n "rio_xml") (v "0.5.3") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "rio_api") (r "^0.5") (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "0fc5q769f83f3h77k999hijpz4biwsqna1z81pfvjib750p0m6h4") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("default"))))))

(define-public crate-rio_xml-0.6.0 (c (n "rio_xml") (v "0.6.0") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "rio_api") (r "^0.6") (d #t) (k 0)) (d (n "sophia_api") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r "^0.6") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "0p1dw4pjbd2wr5k21dq4fxf0wpsmir5wj0wnsjw9rsm3yxwhhian") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("default"))))))

(define-public crate-rio_xml-0.6.1 (c (n "rio_xml") (v "0.6.1") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "rio_api") (r "^0.6") (d #t) (k 0)) (d (n "sophia_api") (r ">=0.6, <=0.7") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r ">=0.6, <=0.7") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "1gl8nc26hr75w0mdk8ikj2wbfidja6kgd9j1b5gfi8av247v6ii6") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("default"))))))

(define-public crate-rio_xml-0.6.2 (c (n "rio_xml") (v "0.6.2") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r ">=0.1, <0.3") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "rio_api") (r "^0.6") (d #t) (k 0)) (d (n "sophia_api") (r ">=0.6, <=0.8") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r ">=0.6, <=0.8") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "1nvbkrmgcy79sl5i32hg2qx4kz18rdr28kwwm1f5jk35408c69v6") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("default"))))))

(define-public crate-rio_xml-0.7.0 (c (n "rio_xml") (v "0.7.0") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "rio_api") (r "^0.7") (d #t) (k 0)))) (h "0v79c8ppcvvp5yxbk58a7pk69k5bszb53rf0vl4jpgndwpgj1qs8") (f (quote (("default"))))))

(define-public crate-rio_xml-0.7.1 (c (n "rio_xml") (v "0.7.1") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "rio_api") (r "^0.7") (d #t) (k 0)))) (h "1bkdk6rmf9wqwzdhs27nmdi9xbhdzs1azs5gmpm0vlzvlqr9yz47") (f (quote (("default"))))))

(define-public crate-rio_xml-0.7.2 (c (n "rio_xml") (v "0.7.2") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "rio_api") (r "^0.7") (d #t) (k 0)))) (h "0vclm441v8nanqk8vqcg62z7skqclzazf4vbxn9s24qbdf3z2gja") (f (quote (("default"))))))

(define-public crate-rio_xml-0.7.3 (c (n "rio_xml") (v "0.7.3") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "rio_api") (r "^0.7") (d #t) (k 0)))) (h "1dpb4v3n6v6r89kyrhgldvj0iw8plxqdqynhhrzid78ybb0gppi6") (f (quote (("default"))))))

(define-public crate-rio_xml-0.8.0 (c (n "rio_xml") (v "0.8.0") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26") (d #t) (k 0)) (d (n "rio_api") (r "^0.8") (d #t) (k 0)))) (h "0hz9nl6c06s8x11vz0ap0r3417w2yrp4fhc3i7g1wvhbxn5inxf4") (f (quote (("default"))))))

(define-public crate-rio_xml-0.8.1 (c (n "rio_xml") (v "0.8.1") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27") (d #t) (k 0)) (d (n "rio_api") (r "^0.8") (d #t) (k 0)))) (h "1qr2ilb30znkyy2mjp7abkp268jkv2qy25x33inkqxgr50zcy1ym") (f (quote (("default"))))))

(define-public crate-rio_xml-0.8.2 (c (n "rio_xml") (v "0.8.2") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27") (d #t) (k 0)) (d (n "rio_api") (r "^0.8") (d #t) (k 0)))) (h "1y84456k5wz5p2yiprdlmvnaz3sn21kb3d3mklrfr910aq44f116") (f (quote (("default")))) (r "1.58")))

(define-public crate-rio_xml-0.8.3 (c (n "rio_xml") (v "0.8.3") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "rio_api") (r "^0.8") (d #t) (k 0)))) (h "1am2sk5zkdn24y0w7422513j03ymljwbk4jvwi6fm6azrjhwcf9r") (f (quote (("default")))) (r "1.60")))

(define-public crate-rio_xml-0.8.4 (c (n "rio_xml") (v "0.8.4") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "rio_api") (r "^0.8") (d #t) (k 0)))) (h "1hsqddsq826wx6j82ry8pwifx89w5s1bl4n64v1rs4bpp1bxmvfj") (f (quote (("default")))) (r "1.60")))

