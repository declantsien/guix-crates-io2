(define-module (crates-io ri o_ rio_turtle) #:use-module (crates-io))

(define-public crate-rio_turtle-0.1.0 (c (n "rio_turtle") (v "0.1.0") (d (list (d (n "rio_api") (r "^0.1") (d #t) (k 0)))) (h "0w1xyjhazx9qv4vh3pdlhcmjw9334nlz7nb1ax2f9yrmcd0402if")))

(define-public crate-rio_turtle-0.2.0 (c (n "rio_turtle") (v "0.2.0") (d (list (d (n "rio_api") (r "^0.2") (d #t) (k 0)))) (h "0wc5qd0r0jgvd1mcq0mn4qr85qibikpdf716rrng66nsihgjf05k")))

(define-public crate-rio_turtle-0.3.0 (c (n "rio_turtle") (v "0.3.0") (d (list (d (n "rio_api") (r "^0.3") (d #t) (k 0)))) (h "0dil0kskfapy362lpczkj222a3d7ff9ynjbv50d016m9i2sckj6n")))

(define-public crate-rio_turtle-0.4.0 (c (n "rio_turtle") (v "0.4.0") (d (list (d (n "rio_api") (r "^0.4") (d #t) (k 0)))) (h "1vrhshjpfihjq3jqwilvbvfhnlixaq5mm82m60khmvx1irlmxm65") (f (quote (("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.4.2 (c (n "rio_turtle") (v "0.4.2") (d (list (d (n "rio_api") (r "^0.4") (d #t) (k 0)))) (h "19idj9j7rbj9y5x12f7cij1rn7bbidcjcdi2mh3a3zlm3kfb0ns0") (f (quote (("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.5.0 (c (n "rio_turtle") (v "0.5.0") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "rio_api") (r "^0.5") (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "1j45axn6nk9bpbf63c4dmac05iysb6q64m8fw5xlkf8n1fkiiszv") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.5.1 (c (n "rio_turtle") (v "0.5.1") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "rio_api") (r "^0.5") (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "0rwc520cgp3ra3x12r5i96gicf1jw9r4xnwvlgfb6mf7mix8a58i") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.5.2 (c (n "rio_turtle") (v "0.5.2") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "rio_api") (r "^0.5") (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "1zicbjbdmvmpih2bq4jl1555mscp2nhlycqsqygg4v3w5lyqpi5l") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.5.3 (c (n "rio_turtle") (v "0.5.3") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "rio_api") (r "^0.5") (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r "^0.6.1") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "1p5k0ij447j377yhjvgxrpbbpnlfp1z61gkkb75ni1yz5z1hjvdq") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.6.0 (c (n "rio_turtle") (v "0.6.0") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "rio_api") (r "^0.6") (d #t) (k 0)) (d (n "sophia_api") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r "^0.6") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "1c4bicj0sf491i4ahq0wsgp4s95wx625mx4qzhp1h15k789hnvjn") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.6.1 (c (n "rio_turtle") (v "0.6.1") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.1") (d #t) (k 0)) (d (n "rio_api") (r "^0.6") (d #t) (k 0)) (d (n "sophia_api") (r ">=0.6, <=0.7") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r ">=0.6, <=0.7") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "1rvj0734bkndfn7ldq5v1ifjlgvf4cfxqjydy886ahs4nxinwvl9") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.6.2 (c (n "rio_turtle") (v "0.6.2") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r ">=0.1, <0.3") (d #t) (k 0)) (d (n "rio_api") (r "^0.6") (d #t) (k 0)) (d (n "sophia_api") (r ">=0.6, <=0.8") (o #t) (d #t) (k 0)) (d (n "sophia_api") (r ">=0.6, <=0.8") (f (quote ("test_macro"))) (d #t) (k 2)))) (h "1hlwiz9w1ig84lxad26xm7glxjdxd9l0bdzyp8nbv2glymffli8r") (f (quote (("sophia" "rio_api/sophia" "sophia_api") ("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.7.0 (c (n "rio_turtle") (v "0.7.0") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rio_api") (r "^0.7") (d #t) (k 0)))) (h "11vlbh1iq0amqjvd7d24m9vx0maljjr4bqyalisn920rvpw4g3zv") (f (quote (("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.7.1 (c (n "rio_turtle") (v "0.7.1") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rio_api") (r "^0.7") (d #t) (k 0)))) (h "0v14f0avibjm6qk7lywsanw2jkl77c8mv7v42x2psl05n1nfbyzf") (f (quote (("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.8.0 (c (n "rio_turtle") (v "0.8.0") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rio_api") (r "^0.8") (d #t) (k 0)))) (h "0vp4wg5j15mvqsvccin23hciv9c8wlini6i6zg2y3rhagwsndah2") (f (quote (("generalized" "rio_api/generalized") ("default"))))))

(define-public crate-rio_turtle-0.8.2 (c (n "rio_turtle") (v "0.8.2") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rio_api") (r "^0.8") (d #t) (k 0)))) (h "0nz9b1np0hicp08fvp02d8mbyg1y7nqbxi7f70cszf5p6fkq8vyp") (f (quote (("generalized" "rio_api/generalized") ("default")))) (r "1.58")))

(define-public crate-rio_turtle-0.8.3 (c (n "rio_turtle") (v "0.8.3") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rio_api") (r "^0.8") (d #t) (k 0)))) (h "1p6264aixncdp81fpr2hswk8qg8lhsvaqydwwkq843bm9kb0ifcs") (f (quote (("generalized" "rio_api/generalized") ("default")))) (r "1.60")))

(define-public crate-rio_turtle-0.8.4 (c (n "rio_turtle") (v "0.8.4") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rio_api") (r "^0.8") (d #t) (k 0)))) (h "05k0pbbhpjl5d7scjldcf9q1ma7ypb5byi1mgsf9pndg3sbmkv2w") (f (quote (("generalized" "rio_api/generalized") ("default")))) (r "1.60")))

