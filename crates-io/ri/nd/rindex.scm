(define-module (crates-io ri nd rindex) #:use-module (crates-io))

(define-public crate-rindex-0.1.0 (c (n "rindex") (v "0.1.0") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1vadkrk48h0sr238b59rg7ywc30bpxwip0pvi4n2vq6hn127p38f") (r "1.63")))

