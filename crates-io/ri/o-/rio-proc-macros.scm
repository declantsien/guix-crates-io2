(define-module (crates-io ri o- rio-proc-macros) #:use-module (crates-io))

(define-public crate-rio-proc-macros-0.0.18 (c (n "rio-proc-macros") (v "0.0.18") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "125qxshwcna7s11nrj0z9n2rkz2szd99id726709wdcqvygxrkxy")))

(define-public crate-rio-proc-macros-0.0.19 (c (n "rio-proc-macros") (v "0.0.19") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "0sny4vxys7hy6py43sq8gvwa6b26gfhsfws768bhd8ygjwqq1v6j")))

(define-public crate-rio-proc-macros-0.0.20 (c (n "rio-proc-macros") (v "0.0.20") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "0x4i33wn5y71l6cpq3c5lmwjrkqa7gvy8yzsy3xb264vdw7kj0sv")))

(define-public crate-rio-proc-macros-0.0.21 (c (n "rio-proc-macros") (v "0.0.21") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "1kfcmnxn0iiclhj7piyy5g7vq5mffpr2cl3al5mv599gjv21khr4")))

(define-public crate-rio-proc-macros-0.0.22 (c (n "rio-proc-macros") (v "0.0.22") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "0gq2x57plsbbb9idhpyxb7xj1qmin5qpqj3gnd7g3wvr5a4sg2n4")))

(define-public crate-rio-proc-macros-0.0.23 (c (n "rio-proc-macros") (v "0.0.23") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "14gpq6saz35ac6634ssr17rcp5y19ppg8f5bik4k5ram9c58jqby")))

(define-public crate-rio-proc-macros-0.0.24 (c (n "rio-proc-macros") (v "0.0.24") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "02nncsb5p9lj4anisgl5jc4hf3clyj4m2c4mkx21y80cwaidaqrn")))

(define-public crate-rio-proc-macros-0.0.25 (c (n "rio-proc-macros") (v "0.0.25") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "0h2bkh6czfrqfn99bvcmv4mw3zb52707i9bx553ndd034jgahfv4")))

(define-public crate-rio-proc-macros-0.0.26 (c (n "rio-proc-macros") (v "0.0.26") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "0fv8jxx7f44mib8bcnvh6s6sa5379mpgq1q7d6ryv2vh57sg9qb1")))

(define-public crate-rio-proc-macros-0.0.27 (c (n "rio-proc-macros") (v "0.0.27") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "06y0drwjxihr53x175xnwdm43hjcvsbfhkg4y95jc1j04555m3gf")))

(define-public crate-rio-proc-macros-0.0.28 (c (n "rio-proc-macros") (v "0.0.28") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "1cxjnn7vp4yq6c1jw00x44c9sdi56pbwb9nvlaq79ahlmvfpbgsd")))

(define-public crate-rio-proc-macros-0.0.29 (c (n "rio-proc-macros") (v "0.0.29") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "0kkbwzlm8xwv0xqxmc0vwasg8bcg2mimngc17a5smdcndnnj5p9i")))

(define-public crate-rio-proc-macros-0.0.30 (c (n "rio-proc-macros") (v "0.0.30") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "074iqx4qf26wqgp5fkzn6lzg4a49i7jdw20i7jjm87jhi7bjfqif")))

(define-public crate-rio-proc-macros-0.0.31 (c (n "rio-proc-macros") (v "0.0.31") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "1h0nfpm3sfcp5xs2d34zg9z59s93yl533c6xkwrl2vymj0h0lriw")))

(define-public crate-rio-proc-macros-0.0.32 (c (n "rio-proc-macros") (v "0.0.32") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "04raxyybjzgrwvjxba0cqdw9ncawwlrc2c3mxmmws0mvbk0cf1vb")))

(define-public crate-rio-proc-macros-0.0.33 (c (n "rio-proc-macros") (v "0.0.33") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "0vs2lh810ydq76v0w9zmb9f4c5bv3fgxa0cd3avj7qx55922a7k7")))

(define-public crate-rio-proc-macros-0.0.34 (c (n "rio-proc-macros") (v "0.0.34") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "0n3mral7al5sn0r8nrxxzjby0vix5023ghswn7l27m2cvcid3ffw")))

(define-public crate-rio-proc-macros-0.0.35 (c (n "rio-proc-macros") (v "0.0.35") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "0p9cp5qphdjq0m5kqdbgg9w3r9y5si802wz93hz0gx5541shqmf4")))

(define-public crate-rio-proc-macros-0.0.36 (c (n "rio-proc-macros") (v "0.0.36") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "10w1z7h0fw4vn53lalqcq01vmcanpqywmfd00xy5bgd7p5fnj8vi")))

(define-public crate-rio-proc-macros-0.0.37 (c (n "rio-proc-macros") (v "0.0.37") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "13bgj6z6pm14a776p28yszhh086g69sgbkxhl4bkcsid4sbwbkjl")))

(define-public crate-rio-proc-macros-0.0.38 (c (n "rio-proc-macros") (v "0.0.38") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "1lpzrdfc71lhmr5d0as3677h0xc9fm09922y6ck5gqvjnsgdrz1k")))

(define-public crate-rio-proc-macros-0.0.39 (c (n "rio-proc-macros") (v "0.0.39") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "0l7s64njrn4hn5m8wf7ad9n6h16g30z9xlhn7kz92kp4q9303jb4")))

