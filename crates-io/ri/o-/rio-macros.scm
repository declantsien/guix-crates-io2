(define-module (crates-io ri o- rio-macros) #:use-module (crates-io))

(define-public crate-rio-macros-0.1.0 (c (n "rio-macros") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1zv8gyhgqg2d0hlbglzvj4h6fi1fcwj5slkzhlljxc4y955hcn21")))

