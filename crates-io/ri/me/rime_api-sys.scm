(define-module (crates-io ri me rime_api-sys) #:use-module (crates-io))

(define-public crate-rime_api-sys-0.1.0 (c (n "rime_api-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0adn745vxyisnb76drkb8ynxizy7nwb2ln5y66m1bifx2rqp52bv") (y #t)))

(define-public crate-rime_api-sys-0.2.0 (c (n "rime_api-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "14gghyhp7csdwi90pg8yab3dv0g1pvj8i8kh8gx823izlnq5x55k") (y #t)))

(define-public crate-rime_api-sys-0.3.0 (c (n "rime_api-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "1smp924inmslf6l9a2s0mrfjpn9apr8gp1raj26615rfwzmvgjvw") (y #t)))

(define-public crate-rime_api-sys-0.0.0 (c (n "rime_api-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0xph8wv3yjzn1nj9b6zriyx4hhw0gf2c1887ryn2m1qif25rl75q") (y #t)))

(define-public crate-rime_api-sys-0.3.1 (c (n "rime_api-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0zp64p9yqh2whsljzf8kvk7k11r8pfwsliw8vqgra69li01hkgi0")))

