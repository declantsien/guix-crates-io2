(define-module (crates-io ri me rime-api) #:use-module (crates-io))

(define-public crate-rime-api-0.1.0 (c (n "rime-api") (v "0.1.0") (d (list (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)))) (h "17ww0nx4aimqd4nqqwy395mpilaby30whrpk2q076bhnkrz41l73")))

(define-public crate-rime-api-0.2.0 (c (n "rime-api") (v "0.2.0") (d (list (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1irsrzxclbp347w1lsdhnkv9n8q281sjn3skn1cji1bqwxrcsqj5")))

(define-public crate-rime-api-0.3.0 (c (n "rime-api") (v "0.3.0") (d (list (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1pvrqrmmgcfznwhp8r3bsjpphs86986975x8crw0fabdwpfkzh9k")))

(define-public crate-rime-api-0.4.1 (c (n "rime-api") (v "0.4.1") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1zghmjbkbdn2ik7ira2zh3br34snk6cny292x5v2mnxy2l5qg8mf")))

(define-public crate-rime-api-0.5.0 (c (n "rime-api") (v "0.5.0") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1bvk0w26f96f5cymznv8f39q34ppk575gyp9mbzz870ayknmdf0c")))

(define-public crate-rime-api-0.7.0 (c (n "rime-api") (v "0.7.0") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1f66bkydcvpjgjdvfs3jylf8s5aqif614xn21n5fsw0qn7ikvi22")))

(define-public crate-rime-api-0.8.0 (c (n "rime-api") (v "0.8.0") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0cwr658xf0apwpb1r6pzh18qp6sr7ys6pmss3hd00b98gij7vr8w")))

(define-public crate-rime-api-0.10.0 (c (n "rime-api") (v "0.10.0") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ccr6k8crabx2q86nylgr6bqzckiad59rcsj9559pjgsqm2prpw1")))

(define-public crate-rime-api-0.10.1 (c (n "rime-api") (v "0.10.1") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1wi2yyim4s18qcd2z479v9fgvn16n76qs1qyw207g1hkq9qgagx8")))

(define-public crate-rime-api-0.11.0 (c (n "rime-api") (v "0.11.0") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive" "serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0pw1rar9w4vk2r22qrqx140n5rfyjxrmgy44rdk6ayc24k6g9j78") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-rime-api-0.12.0 (c (n "rime-api") (v "0.12.0") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive" "serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "04aaz36ahmhs2bb37ds958yq53dqbqai06ihi0215qjdhabaka5v") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-rime-api-0.12.1 (c (n "rime-api") (v "0.12.1") (d (list (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "librime-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive" "serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "14g35aj44zyg3c7s6cp1k2qj7jmn8n2nlp7pgfadwmasj4n5glpk") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

