(define-module (crates-io ri n- rin-sys) #:use-module (crates-io))

(define-public crate-rin-sys-0.1.0 (c (n "rin-sys") (v "0.1.0") (h "1rmx0hr7x2angxjfb7nwci0z51drbvl8zs78lnwd9g8ds82qm6yd")))

(define-public crate-rin-sys-0.1.1 (c (n "rin-sys") (v "0.1.1") (h "04945gqsdvnd7maa7apxw2p3bvxl9rwxwwwrn19wbbib2rsgag6q")))

(define-public crate-rin-sys-0.1.2 (c (n "rin-sys") (v "0.1.2") (h "182b870ymlh3sjvmha5n0yhckrajam45d22xlw0sa5d3vb77mhq2")))

(define-public crate-rin-sys-0.1.3 (c (n "rin-sys") (v "0.1.3") (h "0p8hbhwzy6s0vxix5r5f5pjaxmac4m67251742fmp1n5v2gxn8h6")))

