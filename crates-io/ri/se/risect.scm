(define-module (crates-io ri se risect) #:use-module (crates-io))

(define-public crate-risect-0.1.0-alpha.1 (c (n "risect") (v "0.1.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)))) (h "1j1lzs3akkx7apiwb1qydlkfwzl9wcqhhs5qdydr32wagffam2n1")))

