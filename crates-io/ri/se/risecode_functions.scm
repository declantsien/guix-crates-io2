(define-module (crates-io ri se risecode_functions) #:use-module (crates-io))

(define-public crate-risecode_functions-0.1.0 (c (n "risecode_functions") (v "0.1.0") (h "11rgiidq5z2pfg5pkvczxxx3g4c98hnnahviylccjdhy9fqmm6yj")))

(define-public crate-risecode_functions-0.1.1 (c (n "risecode_functions") (v "0.1.1") (h "1n172sz4v1lv2mi82svr1ivnpkmdy76jlc922a1z76vym59z8lnl")))

