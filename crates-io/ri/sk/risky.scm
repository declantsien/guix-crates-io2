(define-module (crates-io ri sk risky) #:use-module (crates-io))

(define-public crate-risky-0.1.0 (c (n "risky") (v "0.1.0") (h "05pblramh25c2gs2pw5i65b5yb851hd841yqx58b7v2f29ykzvxf")))

(define-public crate-risky-0.1.1 (c (n "risky") (v "0.1.1") (h "1agjmwmqq8ik0zcv6w56vzl037jvfyawdqbmhcy9aqk4f9aqi5vc")))

(define-public crate-risky-0.1.2 (c (n "risky") (v "0.1.2") (h "06vsmm50h3jbv7fmyzy58b1cmbiz0fh6zxlpqpaknkp2f18yw8mb")))

(define-public crate-risky-0.2.0 (c (n "risky") (v "0.2.0") (h "0a8db84r9lsjfr5g2i31s8whp0c9qbjfj8m03hrhb02q4w4rcfmn")))

(define-public crate-risky-0.2.1 (c (n "risky") (v "0.2.1") (h "1id199bm9751z8r165jf2m17bz6n7rcxl0a5kqlqbxfcqnhlw154")))

(define-public crate-risky-0.2.2 (c (n "risky") (v "0.2.2") (h "1nz2mr7gk2gn2ilixxmc6zwk08cjscxyqn44hg5m7a86dmfff418")))

(define-public crate-risky-0.2.3 (c (n "risky") (v "0.2.3") (h "12cb4yfgsvcd50c8svm8cc21shskvj1xli07ybmah903qyzjc4z2")))

(define-public crate-risky-0.3.0 (c (n "risky") (v "0.3.0") (h "1jd2lr3h552r9kaw1zz67m735br8zfvdh09wwrbimq75zljj1znj")))

