(define-module (crates-io ri mg rimg) #:use-module (crates-io))

(define-public crate-rimg-0.1.0 (c (n "rimg") (v "0.1.0") (h "19fyn1yszqy8snqvglhpd80ams3z3k6plhn60kx13d6p9jmakwyc")))

(define-public crate-rimg-0.1.1 (c (n "rimg") (v "0.1.1") (h "19cgx37fiqhqwarmv27ids8glhqdvbilndwkqr6j6vkqpj23xl01")))

