(define-module (crates-io ri st ristory) #:use-module (crates-io))

(define-public crate-ristory-0.1.0 (c (n "ristory") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm" "serde"))) (k 0)))) (h "0myyypak18z8m0j6xal83b0k344c0nmhjcbd3vvaf93mx0gyxvwz")))

(define-public crate-ristory-0.1.1 (c (n "ristory") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm" "serde"))) (k 0)))) (h "0xz32ma8jnbb9v1i9z8bk12mfy3nc2h1nnmzpps9kk25nn3a5q3r")))

(define-public crate-ristory-0.1.2 (c (n "ristory") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.7") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm" "serde"))) (k 0)))) (h "0pzxr7k1f4b6v8wvhvyydz0gh4n7bh4ahd2hpr79py6bf92290w0")))

(define-public crate-ristory-0.1.3 (c (n "ristory") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.7") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm" "serde"))) (k 0)))) (h "0cw77rrc2cnj1jkvj6pljd9hpwvy8irwj5bm1av8r7614l1lgh6p")))

(define-public crate-ristory-0.1.4 (c (n "ristory") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.7") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm" "serde"))) (k 0)))) (h "1h83a9q94g6vdmc5062kwna8rrvg7zbjcl3j9fa35cvb6h9gr81q")))

(define-public crate-ristory-0.1.5 (c (n "ristory") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.7") (d #t) (k 0)) (d (n "tui") (r "^0.19") (f (quote ("crossterm" "serde"))) (k 0)))) (h "1r434filhdqzqcq1gmn2fh6bsqb3m85349qj2nq7b22x1ilq9wsj")))

(define-public crate-ristory-0.1.6 (c (n "ristory") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "tui") (r "^0.19") (f (quote ("crossterm" "serde"))) (k 0)))) (h "1dqfc13cgnipmf79mcmhyn0237a7rr9hmvjlp931n9cxy51v8vb0")))

(define-public crate-ristory-0.1.7 (c (n "ristory") (v "0.1.7") (d (list (d (n "crossterm") (r "^0.25") (f (quote ("serde"))) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "tui") (r "^0.19") (f (quote ("crossterm" "serde"))) (k 0)))) (h "1543gyywb7g2wqkkdypnjkqv0jsl2ir5hczg6aikr83vcrc7y7gz")))

