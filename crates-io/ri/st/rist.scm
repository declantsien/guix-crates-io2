(define-module (crates-io ri st rist) #:use-module (crates-io))

(define-public crate-rist-0.1.0 (c (n "rist") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.6") (d #t) (k 0)))) (h "1ap273jnc8cikcsgszp9nciha0njvkn9b3l5pj8m39lg39c7m2c7") (y #t)))

(define-public crate-rist-0.0.1 (c (n "rist") (v "0.0.1") (d (list (d (n "hyper") (r "^0.9.6") (d #t) (k 0)))) (h "1k2hdvk83bps5i91na4by0z1lm9410c4pa0wb1s5kmaww05mwaw8")))

