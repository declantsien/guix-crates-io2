(define-module (crates-io ri ll rillrate-protocol) #:use-module (crates-io))

(define-public crate-rillrate-protocol-0.1.0 (c (n "rillrate-protocol") (v "0.1.0") (h "0ikna9rz2amyn7np2nzf2gahwzyc0480qd6j6156fs52r328i9mj")))

(define-public crate-rillrate-protocol-0.36.0 (c (n "rillrate-protocol") (v "0.36.0") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "rill-engine") (r "^0.36.0") (o #t) (d #t) (k 0)) (d (n "rill-protocol") (r "^0.36.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "vectorize") (r "^0.2.0") (d #t) (k 0)))) (h "0anj4hd3na4ng3fp51j8y61jkjb6a8vicfcdygawg6v63wi17pm5") (f (quote (("engine" "rill-engine") ("default" "engine"))))))

