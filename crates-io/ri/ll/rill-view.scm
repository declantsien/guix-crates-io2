(define-module (crates-io ri ll rill-view) #:use-module (crates-io))

(define-public crate-rill-view-0.30.0-alpha.6 (c (n "rill-view") (v "0.30.0-alpha.6") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "rill-engine") (r "^0.30.0-alpha.6") (o #t) (d #t) (k 0)) (d (n "rill-protocol") (r "^0.30.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1n127p83ppnf4j2jvyhkn82x5zbs16yik0h44kaqnmg0f8vjqysq") (f (quote (("default" "rill-engine"))))))

(define-public crate-rill-view-0.30.0-alpha.7 (c (n "rill-view") (v "0.30.0-alpha.7") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.5.1") (f (quote ("serde"))) (k 0)) (d (n "rill-engine") (r "^0.30.0-alpha.7") (o #t) (d #t) (k 0)) (d (n "rill-protocol") (r "^0.30.0-alpha.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "vectorize") (r "^0.1.0") (d #t) (k 0)))) (h "1vkfmx7s4frgaswkp12zim5a3k19cpil2a2pdpa0lz29vsdqcc3j") (f (quote (("default" "rill-engine"))))))

