(define-module (crates-io ri ll rillo) #:use-module (crates-io))

(define-public crate-rillo-0.0.0 (c (n "rillo") (v "0.0.0") (h "0wjjxddmzkn2fs3s6x9nmjpf68b7ch6k0ryw7nydxilsv7kfsc5g")))

(define-public crate-rillo-0.1.0 (c (n "rillo") (v "0.1.0") (h "12f1l4pwwcq0bm78zwivplz2lv2apfm1516ssppy0h0z8fvsz2gg")))

(define-public crate-rillo-0.3.0 (c (n "rillo") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "11k57klw9gsbjcl61lnycihz8q49dpccm66ikwc2ds4sxb5rw0z2")))

(define-public crate-rillo-0.3.1 (c (n "rillo") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "180hnm17xgyfmpmbcfjig3cqzyci5sgsjzcykbwc55ryllhzcxhw")))

