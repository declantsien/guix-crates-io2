(define-module (crates-io ri ll rill-config) #:use-module (crates-io))

(define-public crate-rill-config-0.40.0 (c (n "rill-config") (v "0.40.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "meio") (r "^0.94.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1z58yalgpbzayrbahww6v9rnhvjh2733hms10z0nqckm9fxvii8p")))

(define-public crate-rill-config-0.41.0 (c (n "rill-config") (v "0.41.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "meio") (r "^0.95.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1hd4gyxg4bz15j6mrr61yfw6vy7rxavywqpk4dl5z69a771i488g")))

