(define-module (crates-io ri ll rillrate-py) #:use-module (crates-io))

(define-public crate-rillrate-py-0.2.0 (c (n "rillrate-py") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.1") (d #t) (k 0)) (d (n "rillrate") (r "^0.17.0") (d #t) (k 0)))) (h "0zic7d18l3hna3sdnq7myfn2glg7ng40pcckc44pcrfv76qmxaq5") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.2.8 (c (n "rillrate-py") (v "0.2.8") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.1") (d #t) (k 0)) (d (n "rillrate") (r "^0.17.0") (d #t) (k 0)))) (h "19m93ydji0w9kn9gnrmlgrxxlcjnmhhl11xf982f476zhp2nh1pn") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.2.9 (c (n "rillrate-py") (v "0.2.9") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.1") (d #t) (k 0)) (d (n "rillrate") (r "^0.17.0") (d #t) (k 0)))) (h "1fcsxdq20ih9l7ij94wssiscm8m8rza5v1iv2lfw1qz2xnpcc7k3") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.4.0 (c (n "rillrate-py") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.1") (d #t) (k 0)) (d (n "rillrate") (r "^0.18.0") (d #t) (k 0)))) (h "18kqhn945h1m0l78khp1kxq1r7j138m34bxjiz8ibycplkvqb7zq") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.19.0 (c (n "rillrate-py") (v "0.19.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.1") (d #t) (k 0)) (d (n "rillrate") (r "^0.19.0") (d #t) (k 0)))) (h "1qkzp7qb9ss89qpdydbr9s9p7p61qlsqcrpl68cqqnm486ynmxmz") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.20.0 (c (n "rillrate-py") (v "0.20.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.2") (d #t) (k 0)) (d (n "rillrate") (r "^0.20.0") (d #t) (k 0)))) (h "0w12brr5lmc33sxi427vp0k51jrqir6cb3wxpymbxry3qx5h6g1r") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.21.0 (c (n "rillrate-py") (v "0.21.0") (d (list (d (n "once_cell") (r "^1.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.2") (d #t) (k 0)) (d (n "rillrate") (r "^0.21.0") (d #t) (k 0)))) (h "1fkm0kqha3g3jy41q7l6n7bwmb0whq2i45l3108ai5hjxv6qc8n0") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.23.0 (c (n "rillrate-py") (v "0.23.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.2") (d #t) (k 0)) (d (n "rillrate") (r "^0.23.0") (d #t) (k 0)))) (h "09087amx2hjiav2dwrzydxpx00mbvxn8bgsxg6q6d6frcy6ss50l") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.23.1 (c (n "rillrate-py") (v "0.23.1") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.2") (d #t) (k 0)) (d (n "rillrate") (r "^0.23.1") (d #t) (k 0)))) (h "077ldghgh8knb8sy08f9161vx3j0h35gdzakbg3ivadidy5s9jip") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.24.0 (c (n "rillrate-py") (v "0.24.0") (d (list (d (n "pyo3") (r "^0.13.2") (d #t) (k 0)) (d (n "rillrate") (r "^0.24.0") (d #t) (k 0)))) (h "1nnhyny7vb72jayxdjhpwyvlvrgh4yix0ylvyxhxz6f5my0dmid2") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.37.0-rc.3 (c (n "rillrate-py") (v "0.37.0-rc.3") (d (list (d (n "pyo3") (r "^0.14.2") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.37.0-rc.3") (d #t) (k 0)) (d (n "rillrate") (r "^0.37.0-rc.3") (d #t) (k 0)))) (h "0z8046gd7fqz3g5i3fm0jc3jhj3pv68rxvznbj75qbpcxqydra9f") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.37.0-rc.4 (c (n "rillrate-py") (v "0.37.0-rc.4") (d (list (d (n "pyo3") (r "^0.14.2") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.37.0-rc.4") (d #t) (k 0)) (d (n "rillrate") (r "^0.37.0-rc.4") (d #t) (k 0)))) (h "1awbshis76p2v5jnjv17pw8r8sybjb6hj7ag38aszw7lqaxq3nqn") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.37.0 (c (n "rillrate-py") (v "0.37.0") (d (list (d (n "pyo3") (r "^0.14.2") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.37.0") (d #t) (k 0)) (d (n "rillrate") (r "^0.37.0") (d #t) (k 0)))) (h "11vcsysyvsprc8hh2ai92a7v4n5v3v2s100m34fxp6lryi0lg4ab") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.37.1 (c (n "rillrate-py") (v "0.37.1") (d (list (d (n "pyo3") (r "^0.14.2") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.37.0") (d #t) (k 0)) (d (n "rillrate") (r "^0.37.0") (d #t) (k 0)))) (h "13qdqc7mis1b59agnly27dqdnqizgq2x3vr1y59sr72vpfcc88v8") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.37.2 (c (n "rillrate-py") (v "0.37.2") (d (list (d (n "pyo3") (r "^0.14.2") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.37.0") (d #t) (k 0)) (d (n "rillrate") (r "^0.37.0") (d #t) (k 0)))) (h "0ifz7ivmqrw2mrkbzgzlvvp18p6f36gw675ajdgaymq8n9m8pnb4") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.37.3 (c (n "rillrate-py") (v "0.37.3") (d (list (d (n "pyo3") (r "^0.14.2") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.37.0") (d #t) (k 0)) (d (n "rillrate") (r "^0.37.0") (d #t) (k 0)))) (h "02skhk5327d6nbsahvwdlmnycm1gbaw8j0446g6n71brv5dz00zj") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.37.4 (c (n "rillrate-py") (v "0.37.4") (d (list (d (n "pyo3") (r "^0.14.2") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.37.0") (d #t) (k 0)) (d (n "rillrate") (r "^0.37.0") (d #t) (k 0)))) (h "1km9y3rid0qn0522552fa6hmkvcg4x79hc6qdc3lx2z6m8062gfg") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.37.5 (c (n "rillrate-py") (v "0.37.5") (d (list (d (n "pyo3") (r "^0.14.2") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.37.0") (d #t) (k 0)) (d (n "rillrate") (r "^0.37.0") (d #t) (k 0)))) (h "1nahf9aq9j2mcl1gvx6lnp8as5sl35hvc2bnclc2vkmn9xdx1jaz") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.38.0-rc.1 (c (n "rillrate-py") (v "0.38.0-rc.1") (d (list (d (n "pyo3") (r "^0.14.3") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.38.0-rc.1") (d #t) (k 0)) (d (n "rillrate") (r "^0.38.0-rc.1") (d #t) (k 0)))) (h "1h763nsdnfbpa079jj83a9sx0882a5whfksaz7vnv1svdjjqjhjz") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.38.0-rc.1.a (c (n "rillrate-py") (v "0.38.0-rc.1.a") (d (list (d (n "pyo3") (r "^0.14.3") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.38.0-rc.1") (d #t) (k 0)) (d (n "rillrate") (r "^0.38.0-rc.1") (d #t) (k 0)))) (h "1ynflhlm6xbvpvpcsimh8cdm9wpvi40cj255wb4xsq124wzfbybs") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.38.0-rc.2 (c (n "rillrate-py") (v "0.38.0-rc.2") (d (list (d (n "pyo3") (r "^0.14.3") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.38.0-rc.2") (d #t) (k 0)) (d (n "rillrate") (r "^0.38.0-rc.2") (d #t) (k 0)))) (h "02aca43svy23gd420gy4wwwql96z1wbxr0f6gpjs8p9z451lhvlx") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.38.0 (c (n "rillrate-py") (v "0.38.0") (d (list (d (n "pyo3") (r "^0.14.3") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.38.0") (d #t) (k 0)) (d (n "rillrate") (r "^0.38.0") (d #t) (k 0)))) (h "0x3ykdhif2a2zkks10l9lixlxgm4x38z3l6nbxmly449x58v3w1b") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.39.0-rc.1 (c (n "rillrate-py") (v "0.39.0-rc.1") (d (list (d (n "pyo3") (r "^0.14.3") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.39.0-rc.1") (d #t) (k 0)) (d (n "rillrate") (r "^0.39.0-rc.1") (d #t) (k 0)))) (h "0ynn2gb0xpyqka69575jygqgx48ks9zkniparsd42m92ym9k0nm0") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.39.0 (c (n "rillrate-py") (v "0.39.0") (d (list (d (n "pyo3") (r "^0.14.4") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.39.0") (d #t) (k 0)) (d (n "rillrate") (r "^0.39.0") (d #t) (k 0)))) (h "1az754h6j3svc2nnmpj0lhkx32hl17vcm8xyxnbm8v36dz9jzaxz") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-rillrate-py-0.40.0 (c (n "rillrate-py") (v "0.40.0") (d (list (d (n "pyo3") (r "^0.14.5") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.40.0") (d #t) (k 0)) (d (n "rillrate") (r "^0.40.0") (d #t) (k 0)))) (h "15v0cx1j1lrp1k7b9djiy73v23gb4ixll1w8f4rfhscz8b4sgb7i") (f (quote (("default" "pyo3/extension-module"))))))

