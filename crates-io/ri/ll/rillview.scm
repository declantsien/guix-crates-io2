(define-module (crates-io ri ll rillview) #:use-module (crates-io))

(define-public crate-rillview-0.1.0 (c (n "rillview") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.0") (d #t) (k 0)) (d (n "tui") (r "^0.12.0") (f (quote ("crossterm"))) (k 0)))) (h "0hkf7kkxhj7cgl8agksjl8zg33s4hbizjlf6fjcc5y7zr4hjrsy2")))

