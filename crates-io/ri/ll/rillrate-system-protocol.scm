(define-module (crates-io ri ll rillrate-system-protocol) #:use-module (crates-io))

(define-public crate-rillrate-system-protocol-0.2.0 (c (n "rillrate-system-protocol") (v "0.2.0") (d (list (d (n "rill-protocol") (r "^0.30.0-alpha.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "vectorize") (r "^0.1.0") (d #t) (k 0)))) (h "1vj2aqgn9z7crs1zb29rnrkj3md09p20jjri51q47saiypv6qzy2")))

