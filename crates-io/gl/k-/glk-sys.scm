(define-module (crates-io gl k- glk-sys) #:use-module (crates-io))

(define-public crate-glk-sys-0.1.0 (c (n "glk-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "14rax7y1mdgb3gqs11k2b3f28s0gi79dm1bin75apglfyzvm5jc2")))

(define-public crate-glk-sys-0.2.0 (c (n "glk-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05k05pz6zghj15mzaqgzpdg8qx4jqczm4qnjghc6kg2a0sz3rizm")))

