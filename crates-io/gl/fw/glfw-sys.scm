(define-module (crates-io gl fw glfw-sys) #:use-module (crates-io))

(define-public crate-glfw-sys-3.0.4 (c (n "glfw-sys") (v "3.0.4") (h "0z6kv0nxba50s57nf00sj4ygs9zhkphan717r6jlg6r5a0scb1gn")))

(define-public crate-glfw-sys-3.0.4-1 (c (n "glfw-sys") (v "3.0.4-1") (h "1b7cnmkrypvkdyzxy0c9fvry7wayx4nmdk5jkldmxb3mipy80dgm")))

(define-public crate-glfw-sys-3.0.41 (c (n "glfw-sys") (v "3.0.41") (h "11mzyhdvr4hchz6z60676m6540sjxdrzbdrq6g7nnvh966xdln71")))

(define-public crate-glfw-sys-3.0.42 (c (n "glfw-sys") (v "3.0.42") (h "0wn1wbijzbxl24sqdqrh0xjvhjsq5w7dsvq61nc7ki1chvd9xrnn")))

(define-public crate-glfw-sys-3.0.1 (c (n "glfw-sys") (v "3.0.1") (h "1ffnbfyazr43m7l9cjfxd72qp8jf1jzfrkznkjbc2x67wi9vyyfc")))

(define-public crate-glfw-sys-3.1.0 (c (n "glfw-sys") (v "3.1.0") (h "1yld1kj907nz6w71bijvndls4sndk43dqsbnjnnnp5jzvrj4cdj3")))

(define-public crate-glfw-sys-3.1.1 (c (n "glfw-sys") (v "3.1.1") (h "1npm6k955lb7ihf1g22i9k8bzn9n5bffai2j27hhsa1llf37y8pk")))

(define-public crate-glfw-sys-3.1.2 (c (n "glfw-sys") (v "3.1.2") (h "1c03pzmxa2sc8c5lqx2hp88wlpwqacz1qm5wjifpn75nnv5d7d63")))

(define-public crate-glfw-sys-3.1.3 (c (n "glfw-sys") (v "3.1.3") (h "1xr1csf1wx71j5xb3qsfhhd0w1igprazx0grz2ng6kmq5p1rp20m")))

(define-public crate-glfw-sys-3.2.0 (c (n "glfw-sys") (v "3.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "08qdgw1adymjn09cx4whcv2dcprgq23rxqh9f3qxc8nbg5019zza")))

(define-public crate-glfw-sys-3.2.1 (c (n "glfw-sys") (v "3.2.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1snnfh11rkx25nznpi8jxkfm4l42w8mzmfp9sdjvhy19sld4cc80")))

(define-public crate-glfw-sys-3.2.2 (c (n "glfw-sys") (v "3.2.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0f8b67dpjz09igyn9wx27nr9kkidxxp589324xq57737fribhazp")))

(define-public crate-glfw-sys-3.3.0 (c (n "glfw-sys") (v "3.3.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1lvw4jivh8pfmzfz7irp1ax72v7b5b9mhrxj89hx066xh3qjr3gi") (l "glfw")))

(define-public crate-glfw-sys-3.3.1 (c (n "glfw-sys") (v "3.3.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "08qh847jmmc40fap29y2x3y8sm8f06h02b0h8c2ml8qdmbhd640v") (l "glfw")))

(define-public crate-glfw-sys-3.3.2 (c (n "glfw-sys") (v "3.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0zhm7axnkk0ah1qvg9cz0q5f5zrhcbp5h9ibj7m5iv4knl9pnmhb") (l "glfw")))

(define-public crate-glfw-sys-3.3.4 (c (n "glfw-sys") (v "3.3.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "094axk7c5kz7kyvmr3cpp9rfw29r1qldw71gc9vlwz9mayf1s5pn") (l "glfw")))

(define-public crate-glfw-sys-3.3.5 (c (n "glfw-sys") (v "3.3.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0zznr4i15mqlmzs046jhc6fcs307rzvnn7z92r1lkb6zw89bn32v") (l "glfw")))

(define-public crate-glfw-sys-4.0.0+3.3.5 (c (n "glfw-sys") (v "4.0.0+3.3.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1lby8wdqlxcnfq6xkznd7zlsfk8mmnsdd4xqm9v6c8jhkb9xdgjs") (f (quote (("wayland")))) (l "glfw")))

(define-public crate-glfw-sys-5.0.0+3.3.9 (c (n "glfw-sys") (v "5.0.0+3.3.9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "126gafh6j886hbw1cavlrzlp3mm7cdwr15i6265z73xmbza35z0x") (f (quote (("wayland")))) (l "glfw")))

