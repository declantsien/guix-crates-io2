(define-module (crates-io gl fw glfw-native) #:use-module (crates-io))

(define-public crate-glfw-native-0.1.0 (c (n "glfw-native") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "1d21566lr0sk9gc56x1gqzd9pyxympffv27r7i2jxzgaw297db2v")))

(define-public crate-glfw-native-0.1.1 (c (n "glfw-native") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "1xnj7bgy9pscv5mzg165lxwcmk8pllba0algb1jkzvljfmncyjc9")))

(define-public crate-glfw-native-0.1.2 (c (n "glfw-native") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "0an98mhbpzlmfbabrjr81r0gbvrpf1c3dmx834h2ci0daqagjnqq") (f (quote (("x11") ("wayland") ("default" "x11"))))))

