(define-module (crates-io gl fw glfw-sys-passthrough) #:use-module (crates-io))

(define-public crate-glfw-sys-passthrough-4.0.0+3.3.5 (c (n "glfw-sys-passthrough") (v "4.0.0+3.3.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1wvijmavp7dkv16lxqkz5aab36yvznm70rlqdsbw9gwsns6ahfks") (f (quote (("wayland")))) (l "glfw")))

(define-public crate-glfw-sys-passthrough-4.0.1+3.3.5 (c (n "glfw-sys-passthrough") (v "4.0.1+3.3.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1wmzi7ka5lznglgd4n5zq0y46cs8gy24nv87bj05zscap14gpsnw") (f (quote (("wayland")))) (l "glfw")))

(define-public crate-glfw-sys-passthrough-4.0.3+3.3.5 (c (n "glfw-sys-passthrough") (v "4.0.3+3.3.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1pvdcrg0gzqg8j810gprs6djc1fnypg2sm1s7isbx7hv6r6xpch9") (f (quote (("wayland")))) (l "glfw")))

