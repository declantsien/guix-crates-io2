(define-module (crates-io gl fw glfw-bindgen) #:use-module (crates-io))

(define-public crate-glfw-bindgen-0.1.0 (c (n "glfw-bindgen") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "gl33") (r "^0.2") (f (quote ("global_loader"))) (d #t) (k 2)))) (h "02qg3pmvw1bn4wg06z1a74g7zyfkjvrpfrw7lc2d6k5z9rh8dnig") (f (quote (("wayland"))))))

(define-public crate-glfw-bindgen-0.1.1 (c (n "glfw-bindgen") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "gl33") (r "^0.2") (f (quote ("global_loader"))) (d #t) (k 2)))) (h "00rpjz3y463a5dy2qn97mbljr00qlgdcbqnfwm3jlxhw9lpqz451") (f (quote (("wayland"))))))

