(define-module (crates-io gl az glaze) #:use-module (crates-io))

(define-public crate-glaze-0.0.1 (c (n "glaze") (v "0.0.1") (d (list (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.15.1") (d #t) (k 0)) (d (n "winit") (r "^0.28.3") (d #t) (k 0)))) (h "1z1bc7caygd86icbwpznh0knjh6gzmaglp1fhqslw6b41vd0qihp")))

