(define-module (crates-io gl ul glulxe-sys) #:use-module (crates-io))

(define-public crate-glulxe-sys-0.1.0 (c (n "glulxe-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glk") (r "^0.1") (d #t) (k 0)))) (h "1fv18sgsvpd5gvb54ljy4n7n44v5fw7px0z1yxsg2h50pr4yl49x") (y #t)))

(define-public crate-glulxe-sys-0.1.1 (c (n "glulxe-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glk-sys") (r "^0.1") (d #t) (k 0)))) (h "0fglmjrccs0q3r06rnrpws3wyhi4hlacx4ymi4026lgfs7zm7n2s")))

(define-public crate-glulxe-sys-0.2.0 (c (n "glulxe-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glk-sys") (r "^0.2") (d #t) (k 0)))) (h "0hf508hyrdmshbb9i9a9a2v5lk7qqy3489sg7sxgg2pz4z7akczc")))

