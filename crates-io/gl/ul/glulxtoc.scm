(define-module (crates-io gl ul glulxtoc) #:use-module (crates-io))

(define-public crate-glulxtoc-0.1.0 (c (n "glulxtoc") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "dyn-fmt") (r "^0.3.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "if-decompiler") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "relooper") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0yay8314fj8sjb9qszzsqx1mkfbiwbkvaa76nygqqml2rzvwp9mz")))

