(define-module (crates-io gl tf gltf_kun_vrm) #:use-module (crates-io))

(define-public crate-gltf_kun_vrm-0.0.9 (c (n "gltf_kun_vrm") (v "0.0.9") (d (list (d (n "gltf_kun") (r "^0.0.12") (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_vrm") (r "^0.0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1yl26gvhj4gij0jdygjxs58w89i2dzxnwn5cp0jb358agclfs543")))

(define-public crate-gltf_kun_vrm-0.0.10 (c (n "gltf_kun_vrm") (v "0.0.10") (d (list (d (n "gltf_kun") (r "^0.0.12") (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_vrm") (r "^0.0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0hnz554dy0a60cvkim4q23hiqkq76hggfvywzqfzwx1r2a1i9hph")))

