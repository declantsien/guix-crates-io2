(define-module (crates-io gl tf gltf-derive) #:use-module (crates-io))

(define-public crate-gltf-derive-0.1.0 (c (n "gltf-derive") (v "0.1.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1b78znp7aq4f3jxs08fibnml7pl8qzrcnqjx7riwbjsfjrd8x4zv")))

(define-public crate-gltf-derive-0.2.0 (c (n "gltf-derive") (v "0.2.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1ashdf8rj23frjz1a4li8r8y44m2xapjxyl095lin8mgbgkm5p1z")))

(define-public crate-gltf-derive-0.9.0 (c (n "gltf-derive") (v "0.9.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "11pskx8476c72snqm7wq8ahfcy6c6kf863bshi8zgm8xam2npvy6")))

(define-public crate-gltf-derive-0.9.1 (c (n "gltf-derive") (v "0.9.1") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "16xrbavx6n43frc8075j8mc8xhfhc27l8yh29wicx7jp6lw9va8f")))

(define-public crate-gltf-derive-0.9.2 (c (n "gltf-derive") (v "0.9.2") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0hyv6jbpl2j9sf7xcqz94igmnv0rkqszjaaflylxg26nx9fy1c12")))

(define-public crate-gltf-derive-0.9.3 (c (n "gltf-derive") (v "0.9.3") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0z6agaqlgvj600135s1jh7mavhjsi21vdfnpd1r6h6wzh7mz8a8v")))

(define-public crate-gltf-derive-0.10.0 (c (n "gltf-derive") (v "0.10.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "050ivqn2m0s8hshfrwpqzm86kksh344vj0sks74az9g0flksgg8j")))

(define-public crate-gltf-derive-0.10.1 (c (n "gltf-derive") (v "0.10.1") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1kqznzmj81rkf9phz0q46y9r87drq9dflhh7m2c88hss0xs3q7c0")))

(define-public crate-gltf-derive-0.11.0 (c (n "gltf-derive") (v "0.11.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1wbi3b5m906c88s1sgi58fa1s5x17615w4raycwvfrrri290bjyj")))

(define-public crate-gltf-derive-0.11.1 (c (n "gltf-derive") (v "0.11.1") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "13b7h4wcajvicr03yzfbmcqsx8nvy51dgsm3ajxjb9fkx50jfgq6")))

(define-public crate-gltf-derive-0.11.2 (c (n "gltf-derive") (v "0.11.2") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "14x9ksbm15p7bilw66hcvy49z4mihfcwihcdyckq48068jspg3md")))

(define-public crate-gltf-derive-0.11.3 (c (n "gltf-derive") (v "0.11.3") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1i32r9hfnxb1i7sfb84gsir9saxxwf09xf3dllh1zq6l72n6m5lx")))

(define-public crate-gltf-derive-0.12.0 (c (n "gltf-derive") (v "0.12.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0vvq6a8i8n16iw55k4hp42mwa8gjlm5kn13k265anhibccj9b6hq")))

(define-public crate-gltf-derive-0.13.0 (c (n "gltf-derive") (v "0.13.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "08frnk0760m524ah7vkkrwq56pzwzn4bpjhz9rky2ib35k3b95xk")))

(define-public crate-gltf-derive-0.14.0 (c (n "gltf-derive") (v "0.14.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1d5acvk43s6zdr3snpkmhh50p92k74713gqqm99nc8rj41ikx91h")))

(define-public crate-gltf-derive-0.15.0 (c (n "gltf-derive") (v "0.15.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1v0j205f2f6wwf94plpswhmmvly3b16a391qk9i0vsd141vpa3vv")))

(define-public crate-gltf-derive-0.15.1 (c (n "gltf-derive") (v "0.15.1") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1faalh7xvyhp6xxlg89yl67i11nzwxizm5c0wpdysqb90hmcb988")))

(define-public crate-gltf-derive-0.15.2 (c (n "gltf-derive") (v "0.15.2") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14vjz9yj47vy14rhkpcx29kcc3qqrpcs472gamip68jjpzknsqzn")))

(define-public crate-gltf-derive-0.16.0 (c (n "gltf-derive") (v "0.16.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0w4wln579zv3fl06wbb6xkp62am19zsbq87w9nlvrizrw0rr6ahz")))

(define-public crate-gltf-derive-1.0.0 (c (n "gltf-derive") (v "1.0.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "04yigkrirqz4xafwvmhl16cw2y29k324qvljlq1bzcjb51p3vmdx")))

(define-public crate-gltf-derive-1.1.0 (c (n "gltf-derive") (v "1.1.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0blzkhmgb5pwh264pq1nz3q5vbajgsd1wnif7cg13044b6z3vcv7")))

(define-public crate-gltf-derive-1.2.0 (c (n "gltf-derive") (v "1.2.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1wmi8qmsa46fnbbxjdm0727lgi7vz2pkk0hjms9i31hpy243q8pc")))

(define-public crate-gltf-derive-1.3.0 (c (n "gltf-derive") (v "1.3.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "086yl6zwkpx7gjg4zakiysxh8wnd9ww41w76kr7avrs7vnjwxjzj")))

(define-public crate-gltf-derive-1.4.0 (c (n "gltf-derive") (v "1.4.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0mrawdn8icr28xkk2cghys9nx0af2rmn68xgzq1m9ms0aldgx3s3")))

(define-public crate-gltf-derive-1.4.1 (c (n "gltf-derive") (v "1.4.1") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0lac9yrbbyhdx9fvz8qijaxmskmqpisdnzl0difvmbrq2mqhw1ql")))

