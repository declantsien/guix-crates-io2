(define-module (crates-io gl tf gltf-importer) #:use-module (crates-io))

(define-public crate-gltf-importer-0.1.0 (c (n "gltf-importer") (v "0.1.0") (d (list (d (n "gltf") (r "^0.8") (d #t) (k 0)))) (h "0pxxmhzi6qqsdhmkx666jb92ssi2wpf1vnb2qirmi6pm9bidvi8g") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

(define-public crate-gltf-importer-0.1.1 (c (n "gltf-importer") (v "0.1.1") (d (list (d (n "gltf") (r "^0.8") (d #t) (k 0)))) (h "0ppwzkqyyqf19pi378bvzybj9swswcz2mjww2cg4i3bgyj3cfqz8") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

(define-public crate-gltf-importer-0.8.0 (c (n "gltf-importer") (v "0.8.0") (d (list (d (n "gltf") (r "^0.8") (d #t) (k 0)))) (h "109hspn54z2jx6hlid9ppjmwh3jh95ixnka953ykh0zyyr3d6f2w") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

(define-public crate-gltf-importer-0.9.0 (c (n "gltf-importer") (v "0.9.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "gltf") (r "^0.9") (d #t) (k 0)) (d (n "gltf-utils") (r "^0.1") (d #t) (k 0)))) (h "0sb2zmy968m7jc6897arq5xj397hkikhgj1jbihs4fr3wy39wfh0") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

(define-public crate-gltf-importer-0.9.1 (c (n "gltf-importer") (v "0.9.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "gltf") (r "^0.9.1") (d #t) (k 0)) (d (n "gltf-utils") (r "^0.9.1") (d #t) (k 0)))) (h "164dl68xckdl6flyai5qmgq8dzf7wvq34cbwfyaldnvl5kv05nyh") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

(define-public crate-gltf-importer-0.9.2 (c (n "gltf-importer") (v "0.9.2") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "gltf") (r "^0.9.2") (d #t) (k 0)) (d (n "gltf-utils") (r "^0.9.2") (d #t) (k 0)))) (h "0hyb392q45fxg9lbcwwjpi6la8wg9gqqnxpvbznfbzk3qcxlddiz") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

(define-public crate-gltf-importer-0.9.3 (c (n "gltf-importer") (v "0.9.3") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "gltf") (r "^0.9.3") (d #t) (k 0)) (d (n "gltf-utils") (r "^0.9.3") (d #t) (k 0)))) (h "1s59mb0rsf1710z2icmgj559dzidv45z00hy1f9w6jffy6r0r3ah") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

(define-public crate-gltf-importer-0.10.0 (c (n "gltf-importer") (v "0.10.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "gltf") (r "^0.10.0") (d #t) (k 0)) (d (n "gltf-utils") (r "^0.10.0") (d #t) (k 0)))) (h "19pq9j7qgc1x5783k3xav8gs63x7rhcfzzwcmpx3ym7fzvgrmf5q") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

(define-public crate-gltf-importer-0.10.1 (c (n "gltf-importer") (v "0.10.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "gltf") (r "^0.10.1") (d #t) (k 0)) (d (n "gltf-utils") (r "^0.10.1") (d #t) (k 0)))) (h "0qn4yvz6g2wfvmi9114ckz7pz66frnl6h199rr4v3g30nk5xd8s1") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

