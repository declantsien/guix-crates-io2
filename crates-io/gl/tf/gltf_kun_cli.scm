(define-module (crates-io gl tf gltf_kun_cli) #:use-module (crates-io))

(define-public crate-gltf_kun_cli-0.0.1 (c (n "gltf_kun_cli") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gltf_kun") (r "^0.0.1") (d #t) (k 0)))) (h "1rpmnliw17s5j08zn0wwfzx7ybhd75r7i87phf89p5p816ws2405")))

(define-public crate-gltf_kun_cli-0.0.2 (c (n "gltf_kun_cli") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gltf_kun") (r "^0.0.2") (d #t) (k 0)))) (h "1ga0y68mf3ihwzi6rrfg4n09cwjxiw40rapcqfqag25iy9d7yv4y")))

(define-public crate-gltf_kun_cli-0.0.3 (c (n "gltf_kun_cli") (v "0.0.3") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gltf_kun") (r "^0.0.3") (d #t) (k 0)))) (h "0wsafdk5ry012lnsf6jhjx910f3vqk0wsnhy3897hb2ygvccwkf2")))

