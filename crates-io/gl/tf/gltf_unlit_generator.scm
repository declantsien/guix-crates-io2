(define-module (crates-io gl tf gltf_unlit_generator) #:use-module (crates-io))

(define-public crate-gltf_unlit_generator-0.1.0 (c (n "gltf_unlit_generator") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "gltf") (r "^0.10.1") (f (quote ("extras" "names"))) (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)))) (h "1i0i2vwvwah2pjrsn9pdy4hj3blgq16sl5lvymbm7m3ax1y0amb4")))

(define-public crate-gltf_unlit_generator-0.2.0 (c (n "gltf_unlit_generator") (v "0.2.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "gltf") (r "^0.10.1") (f (quote ("extras" "names"))) (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "10agwdwq5ns9c9wdhbnrs8r98w76c5inw3zvrqpbgmr897cxmw1h")))

(define-public crate-gltf_unlit_generator-0.3.0 (c (n "gltf_unlit_generator") (v "0.3.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "gltf") (r "^0.10.1") (f (quote ("extras" "names"))) (d #t) (k 0)) (d (n "gltf-json") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "1nqxqh84nqx38jcckjm35r83vk5hrh4bimk8cp1q5kqzrcgjv0as")))

(define-public crate-gltf_unlit_generator-0.4.0 (c (n "gltf_unlit_generator") (v "0.4.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "gltf") (r "^0.10.1") (f (quote ("extras" "names"))) (d #t) (k 0)) (d (n "gltf-json") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "1vyawzfi6ya74v459nc5rpi5k6vs62lhvpy35s5yffzrzlrh9m3i")))

