(define-module (crates-io gl tf gltf-utils) #:use-module (crates-io))

(define-public crate-gltf-utils-0.1.0 (c (n "gltf-utils") (v "0.1.0") (d (list (d (n "gltf") (r "^0.9") (d #t) (k 0)))) (h "0w7375iy77r44rid3cbn6mcnw950vpkmq550ghr25x4sp82dlz9d") (f (quote (("names") ("extras") ("default"))))))

(define-public crate-gltf-utils-0.9.1 (c (n "gltf-utils") (v "0.9.1") (d (list (d (n "gltf") (r "^0.9.1") (d #t) (k 0)))) (h "0wlpj42z1plfqj45v6m1lzb7zlf938rphsfss7dc0yc9aabr39fr") (f (quote (("names") ("extras") ("default"))))))

(define-public crate-gltf-utils-0.9.2 (c (n "gltf-utils") (v "0.9.2") (d (list (d (n "gltf") (r "^0.9.2") (d #t) (k 0)))) (h "01nhxrnabnccd9zh0jshc3v8kbiazdfrbsz0knjih6jm6x38y2fk") (f (quote (("names") ("extras") ("default"))))))

(define-public crate-gltf-utils-0.9.3 (c (n "gltf-utils") (v "0.9.3") (d (list (d (n "gltf") (r "^0.9.3") (d #t) (k 0)))) (h "0g9yj1wmnj3zcy22hkkk9gbyv03289rnrqyg9gxxpwmprgzmzghn") (f (quote (("names") ("extras") ("default"))))))

(define-public crate-gltf-utils-0.10.0 (c (n "gltf-utils") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "gltf") (r "^0.10.0") (d #t) (k 0)))) (h "1jrqi2gqjf4h4gcg8ns3lbn5ksigvvrgpln27x54wxxk69paywl0") (f (quote (("names") ("extras") ("default"))))))

(define-public crate-gltf-utils-0.10.1 (c (n "gltf-utils") (v "0.10.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "gltf") (r "^0.10.1") (d #t) (k 0)))) (h "0vwx3zzi046d1hgwbaj1khfvg0dhvpk441rypagypmrpqlcj1k00") (f (quote (("names") ("extras") ("default"))))))

