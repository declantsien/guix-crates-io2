(define-module (crates-io gl pk glpk-sys) #:use-module (crates-io))

(define-public crate-glpk-sys-0.1.0 (c (n "glpk-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0izh4cjahxcr33dm7272mxm2ixqyywjn00vmi5qbgcl45dl3dv7l")))

(define-public crate-glpk-sys-0.2.0 (c (n "glpk-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "02sb258p1ld855dc4il3n1gm7rckcdf80xrh992w7v8dixh8wd2d")))

