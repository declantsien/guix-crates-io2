(define-module (crates-io gl _d gl_dstruct) #:use-module (crates-io))

(define-public crate-gl_dstruct-0.1.0 (c (n "gl_dstruct") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)))) (h "1dinz6kn5w708k9cgq8yrji2vi5qv0b0hda38lkpas6cl0rs626y") (f (quote (("debug"))))))

(define-public crate-gl_dstruct-0.2.0 (c (n "gl_dstruct") (v "0.2.0") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)))) (h "1mddl8wh61w6a3yly2ql3qfzgz9iz30jn9hc787678b9q8bwh3i9") (f (quote (("debug"))))))

