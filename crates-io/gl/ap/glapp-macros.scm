(define-module (crates-io gl ap glapp-macros) #:use-module (crates-io))

(define-public crate-glapp-macros-0.1.0 (c (n "glapp-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "197ag2z74c7rhhglg54xdhj50bvlrjwkaqvv9ibirq6b0fz466hq") (f (quote (("sdl") ("glutin"))))))

(define-public crate-glapp-macros-0.1.1 (c (n "glapp-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0silncgq0b0hwpv6i3gqk8f2k6laym4fnjmc9wdsfihd96p2idk8") (f (quote (("sdl") ("glutin"))))))

(define-public crate-glapp-macros-0.1.3 (c (n "glapp-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16wps50865xfa7if2pvcpcv5h116l5894dvwd7hyaajdnnb6rzj8") (f (quote (("sdl") ("glutin"))))))

