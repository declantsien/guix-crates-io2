(define-module (crates-io gl sm glsmrs) #:use-module (crates-io))

(define-public crate-glsmrs-0.1.0 (c (n "glsmrs") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.37") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.60") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("WebGlBuffer" "WebGlRenderingContext" "WebGlUniformLocation" "WebGlTexture" "WebGlProgram" "WebGlShader" "WebGlFramebuffer"))) (d #t) (k 0)))) (h "1jvd7g0nm7wc586giy79dj4n4cwqzskrmhsjnbqhskxwsnll77v5")))

(define-public crate-glsmrs-0.1.1 (c (n "glsmrs") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.37") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.60") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("WebGlBuffer" "WebGlRenderingContext" "WebGlUniformLocation" "WebGlTexture" "WebGlProgram" "WebGlShader" "WebGlFramebuffer"))) (d #t) (k 0)))) (h "1vbi96jsryi690hdzhbylk970iwkq86cndvcg6srlkinhgbg39sy")))

(define-public crate-glsmrs-0.2.0 (c (n "glsmrs") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3.56") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.56") (f (quote ("WebGlBuffer" "WebGlRenderingContext" "WebGlUniformLocation" "WebGlTexture" "WebGlProgram" "WebGlShader" "WebGlFramebuffer" "HtmlCanvasElement" "Window" "Document"))) (d #t) (k 0)))) (h "1rbkrn2mjim41klx29xd98fwsw13gp7v5ablhqa07w06v7aqylgr")))

