(define-module (crates-io gl -h gl-headless) #:use-module (crates-io))

(define-public crate-gl-headless-0.1.0 (c (n "gl-headless") (v "0.1.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.51") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "172v991a7k0anzp1vcrqw36gljjsw2k4lqfamkkrajq8la9q92m1")))

(define-public crate-gl-headless-0.2.0 (c (n "gl-headless") (v "0.2.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "gl-headless-macros") (r "^0.1") (d #t) (k 0)) (d (n "glfw") (r "^0.51") (d #t) (k 0)))) (h "1hr0midmprs7xcraffcbjb0s35i424ni6fpl9yprhy4xwn5585v8") (y #t)))

(define-public crate-gl-headless-0.2.1 (c (n "gl-headless") (v "0.2.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "gl-headless-macros") (r "^0.1") (d #t) (k 0)) (d (n "glfw") (r "^0.51") (d #t) (k 0)))) (h "1lc9ypfijmm15j2dxmyq31l94yyfjclrv8myymld6576zq7z1dz9")))

(define-public crate-gl-headless-0.2.2 (c (n "gl-headless") (v "0.2.2") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "gl-headless-macros") (r "^0.1") (d #t) (k 0)) (d (n "glfw") (r "^0.51") (d #t) (k 0)))) (h "0c592pgnp6z656ljyhn0cqzqq5i1rr2rj3c4p47cmqvms02md8g2")))

