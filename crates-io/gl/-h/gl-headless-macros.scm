(define-module (crates-io gl -h gl-headless-macros) #:use-module (crates-io))

(define-public crate-gl-headless-macros-0.1.0 (c (n "gl-headless-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1cjfjnfniq0bnkw9w284149nfy41sdkzwr0b1v8v05i1mr2l3znc")))

(define-public crate-gl-headless-macros-0.1.1 (c (n "gl-headless-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03z62w9xxrmdppszdcrvn8s2a0675ngm1lxv8bxjn0626ky62bci")))

