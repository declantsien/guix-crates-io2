(define-module (crates-io gl ic glicol_parser) #:use-module (crates-io))

(define-public crate-glicol_parser-0.12.8 (c (n "glicol_parser") (v "0.12.8") (d (list (d (n "glicol_macros") (r "^0.12.8") (d #t) (k 0)) (d (n "glicol_synth") (r "^0.12.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "09bwpk0fkdyq5zv8hbx6x1f0xqigj76krgyg9i6da1lijqmryvks")))

(define-public crate-glicol_parser-0.13.0 (c (n "glicol_parser") (v "0.13.0") (d (list (d (n "glicol_macros") (r "^0.13.0") (d #t) (k 0)) (d (n "glicol_synth") (r "^0.13.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "01cprrbj3r4xjh81qgns4744mih5fxjky6dv74z3xa6qj0rbldh0")))

(define-public crate-glicol_parser-0.13.1 (c (n "glicol_parser") (v "0.13.1") (d (list (d (n "glicol_macros") (r "^0.13.1") (d #t) (k 0)) (d (n "glicol_synth") (r "^0.13.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1ypfd5lgvr09ih72ih4cg39fz8bmfmsgj8qwpzxwbyl5wi5vs6vd")))

(define-public crate-glicol_parser-0.13.2 (c (n "glicol_parser") (v "0.13.2") (d (list (d (n "fasteval") (r "^0.2.4") (d #t) (k 0)) (d (n "glicol_macros") (r "^0.13.2") (d #t) (k 0)) (d (n "glicol_synth") (r "^0.13.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "16mcxipq40n6073qk26ld0i9fp48qs3vls2vc7ic9w2lcm9kbkc3")))

(define-public crate-glicol_parser-0.13.4 (c (n "glicol_parser") (v "0.13.4") (d (list (d (n "fasteval") (r "^0.2.4") (d #t) (k 0)) (d (n "glicol_macros") (r "^0.13.4") (d #t) (k 0)) (d (n "glicol_synth") (r "^0.13.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1kbzbgjfnk2cicfri00vphzw7rkqzyvmz8nv69j38dky9r89ngpn")))

(define-public crate-glicol_parser-0.13.5 (c (n "glicol_parser") (v "0.13.5") (d (list (d (n "fasteval") (r "^0.2.4") (d #t) (k 0)) (d (n "glicol_macros") (r "^0.13.5") (d #t) (k 0)) (d (n "glicol_synth") (r "^0.13.5") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "0p9f4f3mvl31rrb51l9zaszq0wxd7rfk9m436hg2c7mnf7dxlvgy")))

