(define-module (crates-io gl ic glicol_macros) #:use-module (crates-io))

(define-public crate-glicol_macros-0.12.8 (c (n "glicol_macros") (v "0.12.8") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0g6kjaz5dsvbsg3yjjzcmb2qv6z095yyflm46kildrmzcq7krnj0")))

(define-public crate-glicol_macros-0.13.0 (c (n "glicol_macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1ncqq0492rz93jj1iyi1k56fb4yc2m7fi98gsxcy3ihgb191qcnl")))

(define-public crate-glicol_macros-0.13.1 (c (n "glicol_macros") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0y4nipfjvan4c9jbphkaw7xsigpxya5ak2kqjf7g47zkwdnh0av7")))

(define-public crate-glicol_macros-0.13.2 (c (n "glicol_macros") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1j4fxrhby5asxi09xdfxbdc6x11ffypk3r537jvprwixzdgazqzg")))

(define-public crate-glicol_macros-0.13.4 (c (n "glicol_macros") (v "0.13.4") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "000sb4q5pppa7660avhyqwa7ph79xp2mfh0432mayc87kazxc6cs")))

(define-public crate-glicol_macros-0.13.5 (c (n "glicol_macros") (v "0.13.5") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1s2nhhw71cgq2vvxsj0ibmc17qy6kzn1k7k35hjzx21h87myd0wq")))

