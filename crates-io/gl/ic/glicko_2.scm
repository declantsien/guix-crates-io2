(define-module (crates-io gl ic glicko_2) #:use-module (crates-io))

(define-public crate-glicko_2-0.0.1 (c (n "glicko_2") (v "0.0.1") (h "15wd8z74c1cmmf605am8yrg91m5njpyhqp1kagp9mpbpg0c5r6b0") (y #t)))

(define-public crate-glicko_2-1.0.0 (c (n "glicko_2") (v "1.0.0") (h "12kw2cj9x8pg1vr86xkpkdl5wgb6440j9j0jk4szsawrnh2kxjqv")))

