(define-module (crates-io gl ic glicko2) #:use-module (crates-io))

(define-public crate-glicko2-0.1.0 (c (n "glicko2") (v "0.1.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)))) (h "1wnni244xwjy65vympjrpl3wq4i7kg0aw6wgmxl56ssrn7lmqv4h")))

(define-public crate-glicko2-0.1.1 (c (n "glicko2") (v "0.1.1") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)))) (h "1q336355fwqb0aql5jykhjchm8q3d98jrsr7l1p7f55yx4zvs3in")))

(define-public crate-glicko2-0.1.2 (c (n "glicko2") (v "0.1.2") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)))) (h "0wdxziwsqjhn904iabnbh2z5n50ad6h8vlgs5c91cy6c9ii5rmah")))

(define-public crate-glicko2-0.2.0 (c (n "glicko2") (v "0.2.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)))) (h "1djk1zq607bkqjkjsx2k7ka01bfr2b5i7y3pak3l6yxzl7d38b02")))

(define-public crate-glicko2-0.3.0 (c (n "glicko2") (v "0.3.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)))) (h "1019gcwj94lbwmr4vr8i46dcxzwnk0y6sbgl2in5lqfxl977zigm")))

(define-public crate-glicko2-0.3.1 (c (n "glicko2") (v "0.3.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)))) (h "1h84i9rni6c0gm90ab3656p4n21dwf14h6wv0kb395ax9lzkdis7")))

