(define-module (crates-io gl ru glrun) #:use-module (crates-io))

(define-public crate-glrun-0.1.0 (c (n "glrun") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1zihjh8s8ja2djjx88rk9psfs8cczffwb1r46f4n1clva5nl45i9")))

(define-public crate-glrun-0.1.1 (c (n "glrun") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "02xc52laa43a1ja6hndnhkplr4l7dgvf5jq3gz447kxqajphxwm2")))

(define-public crate-glrun-0.1.2 (c (n "glrun") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0m8jaabgsjfgqv4r3gv0ch41jfr4rngwis3rwryd9a8mcpbphyqf")))

