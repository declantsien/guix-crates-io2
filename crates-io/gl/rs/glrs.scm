(define-module (crates-io gl rs glrs) #:use-module (crates-io))

(define-public crate-glrs-0.1.0 (c (n "glrs") (v "0.1.0") (d (list (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "glsl") (r "^7.0.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-path") (r "^2.0.0") (d #t) (k 0)))) (h "0gm5v7jgk42b1dvims49arx0g9rvl93ifpyrh5yr4khl88h7w4nj") (r "1.75")))

