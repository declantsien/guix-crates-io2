(define-module (crates-io gl m_ glm_color) #:use-module (crates-io))

(define-public crate-glm_color-0.1.0 (c (n "glm_color") (v "0.1.0") (d (list (d (n "glm") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1ivxzscsv64ill793c37vsrnxq8hh2hb9vy28qbvsplcbxdg5c0h")))

(define-public crate-glm_color-0.1.1 (c (n "glm_color") (v "0.1.1") (d (list (d (n "glm") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "12pjc6lngm7mmzz73inmc4c5q46xfg7qmnx4cdk3dfr6kkqgddzd")))

(define-public crate-glm_color-0.1.2 (c (n "glm_color") (v "0.1.2") (d (list (d (n "glm") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1hqj7kv3pslw3bdr3d7rbdc1gz9f4mxy93qdx7xdcrja7bcgzls2")))

