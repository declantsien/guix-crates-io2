(define-module (crates-io gl uc glucose) #:use-module (crates-io))

(define-public crate-glucose-0.1.0 (c (n "glucose") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1jh54daalpc5bl9zxrhxxhh8158ra7lmnniznnv04g83rfjh2j2y")))

(define-public crate-glucose-0.1.1 (c (n "glucose") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)))) (h "1pqz2j7wvf9mn9yr6rcp1hmbisckmihb24x66h9hl4b451icsnvr")))

(define-public crate-glucose-0.1.2 (c (n "glucose") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)))) (h "0f9q9l7pzsszq174id5bz0xdcxga255n7vmxrccfza1bxr0086g2")))

(define-public crate-glucose-0.1.3 (c (n "glucose") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "16w1fppkamzx4ynpsbdpn4fhkzf5aixai81m7j2d7v1rdblz1cj2")))

(define-public crate-glucose-0.1.4 (c (n "glucose") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "0pn4n48li6kn8p4ic4jxa39y38iwr9mcv2sbyj116w9mclsgvagm")))

(define-public crate-glucose-0.1.5 (c (n "glucose") (v "0.1.5") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "10y3nyamqlym7h113pzbqyfc6liiqdgqp495wihfbqz0cl1yjshc")))

(define-public crate-glucose-0.1.6 (c (n "glucose") (v "0.1.6") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "1zpbh8as2526qpjp5zlnc22ahdw73sqd4jifjbhrxkds7gvzm11w")))

(define-public crate-glucose-0.1.7 (c (n "glucose") (v "0.1.7") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "0d6a2f6q2xlp2hd54kyxwrk4k3jw1lagdjkq18748w1h7fsyd0kd")))

(define-public crate-glucose-0.1.8 (c (n "glucose") (v "0.1.8") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "1jc71pz3hlsjfw87sdcg5xa3833hprsnbdv368sacz4yxkg52b30")))

(define-public crate-glucose-0.1.9 (c (n "glucose") (v "0.1.9") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "1ia9s3c0kvmz663xijfdzfvbzwpz2ji1xcbdamqrlb48xbdrl4vm")))

(define-public crate-glucose-0.1.10 (c (n "glucose") (v "0.1.10") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "13ypj109aw5cc9wgkl3ayrpbgp1wnrk2wknsqs2sca4g39x6nd9y")))

(define-public crate-glucose-0.1.11 (c (n "glucose") (v "0.1.11") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "1f91apw6wlq5a63sy0pmxjjkzl34ggvb5ngdbbafwnhvfrw82f1v")))

(define-public crate-glucose-0.1.12 (c (n "glucose") (v "0.1.12") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "0gpry4my0wa43fb7v7rnynplyk56r5mf6zi8i2lf1wyy45s0jww0")))

(define-public crate-glucose-0.1.13 (c (n "glucose") (v "0.1.13") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "1zrf0si22mdxdsvd3r885qjzdr56ad9yfwykmz5nfmz13vg9c2wg")))

(define-public crate-glucose-0.2.0 (c (n "glucose") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "0kdrp5m8wiv209w0xdwag8lfw63a516s50bscj1k4g5kx3s6w58v") (f (quote (("theory") ("specialized") ("linear-full" "linear" "specialized") ("linear") ("full" "linear-full" "theory" "serde" "bytemuck") ("default" "linear"))))))

(define-public crate-glucose-0.2.1 (c (n "glucose") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "1wzafgsn57463pv37dwn0m7s8drx81b3fsv2lqzm3j9hd0jln9yj")))

(define-public crate-glucose-0.2.2 (c (n "glucose") (v "0.2.2") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 0)))) (h "1ppm1bff626rh1bwsigm7qagxaw4gls5jg4mwhm4rby61m49hdm0")))

(define-public crate-glucose-0.2.3 (c (n "glucose") (v "0.2.3") (d (list (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "fructose") (r "^0.3.9") (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1i9p5sgzy8mqy5a672x9kfjj618c54kva3cjrq75y27x927k93wg") (f (quote (("groups") ("full-extras" "full" "bytemuck" "mint") ("full" "algebra" "groups" "analysis") ("default" "full") ("analysis") ("algebra"))))))

