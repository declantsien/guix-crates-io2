(define-module (crates-io gl -l gl-lite) #:use-module (crates-io))

(define-public crate-gl-lite-0.1.0 (c (n "gl-lite") (v "0.1.0") (d (list (d (n "gl") (r "^0.11.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "glutin") (r "^0.20.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "01fn03hf3br44nqdbrfi7h71b22ny7kf0kiajkqmk55fbsj97m9n")))

(define-public crate-gl-lite-0.1.1 (c (n "gl-lite") (v "0.1.1") (d (list (d (n "gl") (r "^0.11.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "glutin") (r "^0.20.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1x22k6w40fsp9n7cqaci20yln2sr9wrazifsrdg6s47hjxgyq618")))

(define-public crate-gl-lite-0.1.2 (c (n "gl-lite") (v "0.1.2") (d (list (d (n "gl") (r "^0.11.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "glutin") (r "^0.20.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1dlyj3ifpcxnll134xi1bcd3i434f73vs2awr74xnbvifn97cz2r")))

