(define-module (crates-io gl iu glium_text_rusttype) #:use-module (crates-io))

(define-public crate-glium_text_rusttype-0.0.1 (c (n "glium_text_rusttype") (v "0.0.1") (d (list (d (n "cgmath") (r "^0.10") (d #t) (k 2)) (d (n "glium") (r "^0.15") (k 0)) (d (n "glium") (r "^0.15") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)))) (h "0qk0zwpxbl6ym7i2pc2564cwks80qlxbbcz1jpgyabc1ipdhiafj")))

(define-public crate-glium_text_rusttype-0.0.2 (c (n "glium_text_rusttype") (v "0.0.2") (d (list (d (n "cgmath") (r "^0.10") (d #t) (k 2)) (d (n "glium") (r "^0.15") (k 0)) (d (n "glium") (r "^0.15") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)))) (h "1f1z1k4w9qfhvqa1dmrxx30bv2z68rig1g8afpxhj6a8lqncaysd")))

(define-public crate-glium_text_rusttype-0.0.3 (c (n "glium_text_rusttype") (v "0.0.3") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 2)) (d (n "glium") (r "^0.16") (k 0)) (d (n "glium") (r "^0.16") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)))) (h "100c1zvwnhnyyci9gp57gi01l064l47nsqfrg0cgrm33109i14jn")))

(define-public crate-glium_text_rusttype-0.0.4 (c (n "glium_text_rusttype") (v "0.0.4") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 2)) (d (n "glium") (r "^0.16") (k 0)) (d (n "glium") (r "^0.16") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)))) (h "0pyx279yvy9l17avxlw60ayx02cs98qcn5qzmy1bxjdywyprjw3h")))

(define-public crate-glium_text_rusttype-0.0.5 (c (n "glium_text_rusttype") (v "0.0.5") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 2)) (d (n "glium") (r "^0.16") (k 0)) (d (n "glium") (r "^0.16") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)))) (h "1b2a54bpccgrihxfs26r0dm0ncpw1wf5akq4nrs03z7pcyx04g3g")))

(define-public crate-glium_text_rusttype-0.1.0 (c (n "glium_text_rusttype") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 2)) (d (n "glium") (r "^0.16") (k 0)) (d (n "glium") (r "^0.16") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)))) (h "0br10jgrz6yi3khv3srx2r6ar6l2kicivz7nfpm1g6777klq1ddw")))

(define-public crate-glium_text_rusttype-0.2.0 (c (n "glium_text_rusttype") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 2)) (d (n "glium") (r "^0.17") (k 0)) (d (n "glium") (r "^0.17") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)))) (h "1bpr53z1y7alalahfb4nmxyc8x5ird237rlngjknmhqc1j37hjzp")))

(define-public crate-glium_text_rusttype-0.3.0 (c (n "glium_text_rusttype") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 2)) (d (n "glium") (r "^0.19") (k 0)) (d (n "glium") (r "^0.19") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "rusttype") (r "^0.3") (d #t) (k 0)))) (h "166z2hrwb4iczcs0dz04dyg43r4lw2vibzm543fwmcrf29gf4ll1")))

(define-public crate-glium_text_rusttype-0.3.1 (c (n "glium_text_rusttype") (v "0.3.1") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 2)) (d (n "glium") (r "^0.20") (k 0)) (d (n "glium") (r "^0.20") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "rusttype") (r "^0.3") (d #t) (k 0)))) (h "1yqvl3amkldw923nmrrc7ya7dd98ww9m9axa3bqva5s8n8ixxfdn")))

(define-public crate-glium_text_rusttype-0.3.2 (c (n "glium_text_rusttype") (v "0.3.2") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 2)) (d (n "glium") (r "^0.22") (k 0)) (d (n "glium") (r "^0.22") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "rusttype") (r "^0.7") (d #t) (k 0)))) (h "1g932a5wsx0chwgzkyikq53ls6w2g1ipl94rk4k2rv5by8gdjdrq")))

(define-public crate-glium_text_rusttype-0.3.3 (c (n "glium_text_rusttype") (v "0.3.3") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 2)) (d (n "glium") (r "^0.26") (k 0)) (d (n "glium") (r "^0.26") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "rusttype") (r "^0.8") (d #t) (k 0)))) (h "01g9xpbpabiwamzxf2y87wh2b7g8g5z8xhnsn97kd2cpnlrssicl")))

