(define-module (crates-io gl iu glium-types) #:use-module (crates-io))

(define-public crate-glium-types-0.1.0 (c (n "glium-types") (v "0.1.0") (d (list (d (n "derive-cmp-ops") (r "^0.1.0") (d #t) (k 0)) (d (n "glium") (r "^0.34.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "0xdsmv5lj4jy6zv6i376jkd92wmkj3pybijs8milfb1qrwzz6lam")))

(define-public crate-glium-types-0.2.0 (c (n "glium-types") (v "0.2.0") (d (list (d (n "derive-cmp-ops") (r "^0.1.0") (d #t) (k 0)) (d (n "glium") (r "^0.34.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "0zy5afl23s2dh5mp8qjdqhbq201cp2wpdan63pw92h1rfq41fb66")))

(define-public crate-glium-types-0.2.1 (c (n "glium-types") (v "0.2.1") (d (list (d (n "derive-cmp-ops") (r "^0.1.0") (d #t) (k 0)) (d (n "glium") (r "^0.34.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "1mgb53vyqq3yy98rkzwy9qlgdppdrzpc9hw1rabm8ndq9pfk91kh")))

(define-public crate-glium-types-0.2.2 (c (n "glium-types") (v "0.2.2") (d (list (d (n "derive-cmp-ops") (r "^0.1.0") (d #t) (k 0)) (d (n "glium") (r "^0.34.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "1prnjgskvghd6qbhaw4zsc440k7d21xm19rp8677l7l093vcl8yg")))

