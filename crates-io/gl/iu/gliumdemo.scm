(define-module (crates-io gl iu gliumdemo) #:use-module (crates-io))

(define-public crate-gliumdemo-0.1.0 (c (n "gliumdemo") (v "0.1.0") (d (list (d (n "glium") (r "^0.16.0") (d #t) (k 0)) (d (n "image") (r "^0.12.1") (d #t) (k 0)))) (h "1lj178s2rvb4famhbas735f3db42i9gh6i01i9yr1b292n22bvb1")))

(define-public crate-gliumdemo-0.2.0 (c (n "gliumdemo") (v "0.2.0") (d (list (d (n "glium") (r "^0.16.0") (d #t) (k 0)) (d (n "image") (r "^0.12.1") (d #t) (k 0)))) (h "1wv6vq3wj320ypmwh5mx55jz953a3jsa6nyh35snm92dp1qg8v50")))

