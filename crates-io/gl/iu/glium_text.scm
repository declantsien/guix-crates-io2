(define-module (crates-io gl iu glium_text) #:use-module (crates-io))

(define-public crate-glium_text-0.0.1 (c (n "glium_text") (v "0.0.1") (d (list (d (n "freetype-sys") (r "*") (d #t) (k 0)) (d (n "glium") (r "*") (f (quote ("nalgebra"))) (k 0)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nalgebra") (r "*") (d #t) (k 0)))) (h "175f9y9gz9wz96k25m0n40zzgzpxmqpikbv9327jv9nii6gr7idr")))

(define-public crate-glium_text-0.0.2 (c (n "glium_text") (v "0.0.2") (d (list (d (n "freetype-sys") (r "*") (d #t) (k 0)) (d (n "glium") (r "*") (f (quote ("nalgebra"))) (k 0)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nalgebra") (r "*") (d #t) (k 0)))) (h "08knip32qvglsir471vadpm6xrrzm51adbh9ivbsakl0sva5bfni")))

(define-public crate-glium_text-0.0.3 (c (n "glium_text") (v "0.0.3") (d (list (d (n "freetype-sys") (r "*") (d #t) (k 0)) (d (n "glium") (r "*") (f (quote ("nalgebra"))) (k 0)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nalgebra") (r "*") (d #t) (k 0)))) (h "1km8dlsdjq8d5fv6376k15bsxr95nza3014mzhrsp2g1f0fz8p6z")))

(define-public crate-glium_text-0.0.4 (c (n "glium_text") (v "0.0.4") (d (list (d (n "freetype-sys") (r "*") (d #t) (k 0)) (d (n "glium") (r "^0.3") (f (quote ("nalgebra"))) (k 0)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "*") (d #t) (k 0)))) (h "13dhaskm4r29fxbmyxb5wjrqb3pijpsdqwmfgd000y22q02wg4r4")))

(define-public crate-glium_text-0.1.0 (c (n "glium_text") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.1") (d #t) (k 2)) (d (n "freetype-sys") (r "*") (d #t) (k 0)) (d (n "glium") (r "^0.3") (f (quote ("cgmath"))) (k 0)) (d (n "glutin") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "03nx0anm0by98vja82lr1mlz4zrqxiv8m6xkjq9x71sgldvjm7mr")))

(define-public crate-glium_text-0.1.1 (c (n "glium_text") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.1") (d #t) (k 2)) (d (n "freetype-sys") (r "*") (d #t) (k 0)) (d (n "glium") (r "^0.3") (f (quote ("cgmath"))) (k 0)) (d (n "glutin") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0895dj5z2hskc7rd31kqm3vf2m1hk63s1rmd5ip2a0lqgxfrpkvf")))

(define-public crate-glium_text-0.1.2 (c (n "glium_text") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 2)) (d (n "freetype-sys") (r "*") (d #t) (k 0)) (d (n "glium") (r "^0.4") (f (quote ("cgmath"))) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "10vlrcjgqhq12n50hlw31jryl0c9clv3f100rlp3c2q9ml3p8ih8")))

(define-public crate-glium_text-0.2.0 (c (n "glium_text") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.5") (f (quote ("cgmath"))) (k 0)) (d (n "glium") (r "^0.5") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0532ldd1736nvaajynw7kamkh5iswj96z7lpbwxa5v3l010bnmmd")))

(define-public crate-glium_text-0.3.0 (c (n "glium_text") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.6") (f (quote ("cgmath"))) (k 0)) (d (n "glium") (r "^0.5") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "14gia7j3scaab3hs2yhyafzdfcq1862rd24648iv7lj2vzz1m3ca")))

(define-public crate-glium_text-0.3.1 (c (n "glium_text") (v "0.3.1") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.6") (f (quote ("cgmath"))) (k 0)) (d (n "glium") (r "^0.5") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0dspsvj56z4h974p0ad1jimi1kdxhbc4n3mwcmmzjdx6p0wn9mpz")))

(define-public crate-glium_text-0.3.2 (c (n "glium_text") (v "0.3.2") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.6") (f (quote ("cgmath"))) (k 0)) (d (n "glium") (r "^0.5") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1g3m7hwjgwwfm5ihvx92gi5ijfc225ajmkshlrr3k41drw57nzzl")))

(define-public crate-glium_text-0.4.0 (c (n "glium_text") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.7") (f (quote ("cgmath"))) (k 0)) (d (n "glium") (r "^0.5") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1521xk6ww290zz073w8lwj34s94wrbkakcbbr0b194zds6vlmppi")))

(define-public crate-glium_text-0.5.0 (c (n "glium_text") (v "0.5.0") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.8") (f (quote ("cgmath"))) (k 0)) (d (n "glium") (r "^0.8") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1ayq1xijw4lxnbq7n97m0lzk1aas8nggw0qjrm8ms5psg5zi63q2")))

(define-public crate-glium_text-0.5.1 (c (n "glium_text") (v "0.5.1") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.8") (f (quote ("cgmath"))) (k 0)) (d (n "glium") (r "^0.8") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0kzxfgymasms8mwy2lrimihmyfgijsp5cgmczkhbbvrsx48cjh0a")))

(define-public crate-glium_text-0.6.0 (c (n "glium_text") (v "0.6.0") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.9") (f (quote ("cgmath"))) (k 0)) (d (n "glium") (r "^0.9") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1yl9g89xyh75cqzk5s0dff8j0l2lkycag4hjfnx5f8a497m0i9ap")))

(define-public crate-glium_text-0.7.0 (c (n "glium_text") (v "0.7.0") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.2") (d #t) (k 0)) (d (n "glium") (r "^0.12") (f (quote ("cgmath"))) (k 0)) (d (n "glium") (r "^0.12") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p1s7nsywy9isn381h60m7vibh1640jcaf31yqb7s79nrwwjn22l")))

(define-public crate-glium_text-0.8.0 (c (n "glium_text") (v "0.8.0") (d (list (d (n "cgmath") (r "^0.7") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.2") (d #t) (k 0)) (d (n "glium") (r "^0.13") (k 0)) (d (n "glium") (r "^0.13") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "017n69p4rwlh2hs52kvllpw28jd7zfdz22map62rm8mdfw3bkyvn")))

(define-public crate-glium_text-0.9.0 (c (n "glium_text") (v "0.9.0") (d (list (d (n "cgmath") (r "^0.8") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.3") (d #t) (k 0)) (d (n "glium") (r "^0.14") (k 0)) (d (n "glium") (r "^0.14") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sccp1cas4dhpnnzndkg1819y1bhis3rr7pl14k3ms8bh4ymynb3")))

(define-public crate-glium_text-0.10.0 (c (n "glium_text") (v "0.10.0") (d (list (d (n "cgmath") (r "^0.10") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.4") (d #t) (k 0)) (d (n "glium") (r "^0.15") (k 0)) (d (n "glium") (r "^0.15") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wmk14d7w0kf4mj81fz65csnsn9crgjy9y281rdhxcqp1jz5qja4")))

(define-public crate-glium_text-0.11.0 (c (n "glium_text") (v "0.11.0") (d (list (d (n "cgmath") (r "^0.12") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.4") (d #t) (k 0)) (d (n "glium") (r "^0.16") (k 0)) (d (n "glium") (r "^0.16") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12xsji6c3wfan62ydmp8hbxi38dp14d3cyxzm55ly6jz0c8giv2y")))

(define-public crate-glium_text-0.12.0 (c (n "glium_text") (v "0.12.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.6") (d #t) (k 0)) (d (n "glium") (r "^0.21") (k 0)) (d (n "glium") (r "^0.21") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06lcsnf7x7ngdrpwanjr5sfggn81rcsckzm7f208sgl376jf0w8g")))

(define-public crate-glium_text-0.13.0 (c (n "glium_text") (v "0.13.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.7") (d #t) (k 0)) (d (n "glium") (r "^0.22") (k 0)) (d (n "glium") (r "^0.22") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "128z5h5hqn9pca2ffy116rk7zjnnzdb6dzadiircl8mf95n1rlmd")))

(define-public crate-glium_text-0.14.0 (c (n "glium_text") (v "0.14.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.7") (d #t) (k 0)) (d (n "glium") (r "^0.23") (k 0)) (d (n "glium") (r "^0.23") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rnmvy01pbj6a9icc36qjv9na8180vsvl54djsg1znpca4sfn8cx")))

