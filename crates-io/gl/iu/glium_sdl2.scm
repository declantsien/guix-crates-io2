(define-module (crates-io gl iu glium_sdl2) #:use-module (crates-io))

(define-public crate-glium_sdl2-0.1.0 (c (n "glium_sdl2") (v "0.1.0") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 2)) (d (n "genmesh") (r "^0.2.1") (d #t) (k 2)) (d (n "glium") (r "^0.4") (f (quote ("image" "nalgebra" "cgmath" "gl_read_buffer" "gl_depth_textures"))) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "obj") (r "^0.2.1") (d #t) (k 2)) (d (n "sdl2") (r "^0.4") (d #t) (k 0)))) (h "003qfsma06lpnjppkfl0a1whb3z98yb7fx3nz4c3k7nrff1qwi9k")))

(define-public crate-glium_sdl2-0.1.1 (c (n "glium_sdl2") (v "0.1.1") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 2)) (d (n "genmesh") (r "^0.2.1") (d #t) (k 2)) (d (n "glium") (r "^0.4") (f (quote ("image" "nalgebra" "cgmath" "gl_read_buffer" "gl_depth_textures"))) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "obj") (r "^0.2.1") (d #t) (k 2)) (d (n "sdl2") (r "^0.4") (d #t) (k 0)))) (h "0bhw4yf032q18l5fb0indvr9ysihj8lyf2h642s27mjad16w2wfn")))

(define-public crate-glium_sdl2-0.2.0 (c (n "glium_sdl2") (v "0.2.0") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 2)) (d (n "genmesh") (r "^0.2.1") (d #t) (k 2)) (d (n "glium") (r "^0.5") (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "obj") (r "^0.2.1") (d #t) (k 2)) (d (n "sdl2") (r "^0.4") (d #t) (k 0)))) (h "1mlk8n2jkzg79jzkagl0xx3kyq1scbx52gnfdhzna129x71l5zgk")))

(define-public crate-glium_sdl2-0.2.1 (c (n "glium_sdl2") (v "0.2.1") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 2)) (d (n "genmesh") (r "^0.2.1") (d #t) (k 2)) (d (n "glium") (r "^0.5") (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "obj") (r "^0.2.1") (d #t) (k 2)) (d (n "sdl2") (r "^0.4") (d #t) (k 0)))) (h "1pvxh7rcxmvwcgzl5wvwv0adcq9nc7wrf6390x8zskd6bxxs6xh7")))

(define-public crate-glium_sdl2-0.3.0 (c (n "glium_sdl2") (v "0.3.0") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 2)) (d (n "genmesh") (r "^0.2.1") (d #t) (k 2)) (d (n "glium") (r "^0.6") (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "obj") (r "^0.2.1") (d #t) (k 2)) (d (n "sdl2") (r "^0.5") (d #t) (k 0)))) (h "10a4mxyd6bhi81wg9vz6flsfqras2r82qfhn134kmqcwnjm5ryvz")))

(define-public crate-glium_sdl2-0.4.0 (c (n "glium_sdl2") (v "0.4.0") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 2)) (d (n "genmesh") (r "^0.2.1") (d #t) (k 2)) (d (n "glium") (r "^0.7") (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "obj") (r "^0.2.1") (d #t) (k 2)) (d (n "sdl2") (r "^0.5") (d #t) (k 0)))) (h "1xiirapv3pzkkdj2cl3c00lhx7xbb5srzww0c4xfmyin2c7hasi3")))

(define-public crate-glium_sdl2-0.5.0 (c (n "glium_sdl2") (v "0.5.0") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 2)) (d (n "genmesh") (r "^0.2.1") (d #t) (k 2)) (d (n "glium") (r "^0.8") (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "obj") (r "^0.2.1") (d #t) (k 2)) (d (n "sdl2") (r "^0.7") (d #t) (k 0)))) (h "1f9kxhw3rzx7zhh7lc551c36dx6za2jybc2bdf0rysaypqzg7q0r")))

(define-public crate-glium_sdl2-0.6.0 (c (n "glium_sdl2") (v "0.6.0") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 2)) (d (n "genmesh") (r "^0.2.1") (d #t) (k 2)) (d (n "glium") (r "^0.8") (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "obj") (r "^0.2.1") (d #t) (k 2)) (d (n "sdl2") (r "^0.8") (d #t) (k 0)))) (h "1q9xx8z5rfbcqgb7dk5hc6jf9k0a310xjyjyxjx80h5lzwzwq38q")))

(define-public crate-glium_sdl2-0.7.0 (c (n "glium_sdl2") (v "0.7.0") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 2)) (d (n "genmesh") (r "^0.2.1") (d #t) (k 2)) (d (n "glium") (r "^0.9") (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "obj") (r "^0.2.1") (d #t) (k 2)) (d (n "sdl2") (r "^0.9") (d #t) (k 0)))) (h "1r6d5j8vd3pqzp668iqq0sa1mxpxx6w0ibzpbajg9v9pak238cwx")))

(define-public crate-glium_sdl2-0.8.0 (c (n "glium_sdl2") (v "0.8.0") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 2)) (d (n "genmesh") (r "^0.2.1") (d #t) (k 2)) (d (n "glium") (r "^0.10") (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "obj") (r "^0.2.1") (d #t) (k 2)) (d (n "sdl2") (r "^0.9") (d #t) (k 0)))) (h "0vi438d7scr35w45gy1bhwp5rflx315y72s90z0wgm9kpbj868ad")))

(define-public crate-glium_sdl2-0.9.0 (c (n "glium_sdl2") (v "0.9.0") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 2)) (d (n "genmesh") (r "^0.2.1") (d #t) (k 2)) (d (n "glium") (r "^0.12") (k 0)) (d (n "obj") (r "^0.2.1") (d #t) (k 2)) (d (n "sdl2") (r "^0.12") (d #t) (k 0)))) (h "1nsisy8p71vwjj7j98wszdiyzg5mrr80gxb3aj2a8wkcdh6ga3lm")))

(define-public crate-glium_sdl2-0.10.0 (c (n "glium_sdl2") (v "0.10.0") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 2)) (d (n "genmesh") (r "^0.4") (d #t) (k 2)) (d (n "glium") (r "^0.13") (k 0)) (d (n "obj") (r "^0.5") (f (quote ("usegenmesh"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.13") (d #t) (k 0)))) (h "1glxnzwf7ah0jp710qlylm1cry9pq2g95y5c2prsl13vly7vm998")))

(define-public crate-glium_sdl2-0.11.0 (c (n "glium_sdl2") (v "0.11.0") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 2)) (d (n "genmesh") (r "^0.4") (d #t) (k 2)) (d (n "glium") (r "^0.14") (k 0)) (d (n "obj") (r "^0.5") (f (quote ("usegenmesh"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.17") (d #t) (k 0)))) (h "1bpjknd3x2isxlk6cgnkiciq25p4dlp4zagm25f9p659fs8nbcj6")))

(define-public crate-glium_sdl2-0.12.0 (c (n "glium_sdl2") (v "0.12.0") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 2)) (d (n "genmesh") (r "^0.4") (d #t) (k 2)) (d (n "glium") (r "^0.14") (k 0)) (d (n "obj") (r "^0.5") (f (quote ("usegenmesh"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.19") (d #t) (k 0)))) (h "1lw6v778bz59s0wzxvjvx83w6q2xai2nbwsck8ay0a04zdsic7ls")))

(define-public crate-glium_sdl2-0.13.0 (c (n "glium_sdl2") (v "0.13.0") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 2)) (d (n "genmesh") (r "^0.4") (d #t) (k 2)) (d (n "glium") (r "^0.15") (k 0)) (d (n "obj") (r "^0.5") (f (quote ("usegenmesh"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.27") (d #t) (k 0)))) (h "1liwzf0nw39gvgxx9zjcrh8h3gx5hgzv7cv97r7plrad0fpgs61d")))

(define-public crate-glium_sdl2-0.14.0 (c (n "glium_sdl2") (v "0.14.0") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 2)) (d (n "genmesh") (r "^0.4") (d #t) (k 2)) (d (n "glium") (r "^0.17") (k 0)) (d (n "obj") (r "^0.5") (f (quote ("usegenmesh"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "0cpxl346w3cx6mkm2i5nar9s1v4fq62x93zzwnnnc79yrj3dj0an")))

(define-public crate-glium_sdl2-0.15.0 (c (n "glium_sdl2") (v "0.15.0") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 2)) (d (n "genmesh") (r "^0.4") (d #t) (k 2)) (d (n "glium") (r "^0.18") (k 0)) (d (n "obj") (r "^0.5") (f (quote ("usegenmesh"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "0cnxrqhq4d7vxr6f2nfg4yl77qfbrkp1z4k4427k8s8w0z9mjwp7")))

