(define-module (crates-io gl iu glium_pib) #:use-module (crates-io))

(define-public crate-glium_pib-0.1.0 (c (n "glium_pib") (v "0.1.0") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "shared_library") (r "^0.1.4") (d #t) (k 0)))) (h "0pgl8gpk5zvl8dqy5i56v7vmj6s377gwh4ycjavlmkh6vkw4vx5z")))

