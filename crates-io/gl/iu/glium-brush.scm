(define-module (crates-io gl iu glium-brush) #:use-module (crates-io))

(define-public crate-glium-brush-0.1.0 (c (n "glium-brush") (v "0.1.0") (d (list (d (n "glium") (r "^0.22") (k 0)) (d (n "glium") (r "^0.22") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.2") (d #t) (k 0)))) (h "0qqx4w2s7548bks9sicwp3ih5428pls6y2g09aq4srnz9yn3nhqw")))

(define-public crate-glium-brush-0.2.0 (c (n "glium-brush") (v "0.2.0") (d (list (d (n "glium") (r "^0.22") (k 0)) (d (n "glium") (r "^0.22") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.2") (d #t) (k 0)))) (h "0lbpbg19935ylpx3w0kmaxkck900gva06zf6i5fyxrw0fld7qlh8")))

(define-public crate-glium-brush-0.2.1 (c (n "glium-brush") (v "0.2.1") (d (list (d (n "glium") (r "^0.22") (k 0)) (d (n "glium") (r "^0.22") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.2") (d #t) (k 0)))) (h "0r2jwi77sw3z7mvg2qplvy9025qghvmfnbcyjv8ll6y7v9m8kvdf")))

