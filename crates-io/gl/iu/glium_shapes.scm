(define-module (crates-io gl iu glium_shapes) #:use-module (crates-io))

(define-public crate-glium_shapes-0.1.0 (c (n "glium_shapes") (v "0.1.0") (d (list (d (n "approx") (r "~0.1.1") (d #t) (k 0)) (d (n "cgmath") (r "~0.12.0") (d #t) (k 0)) (d (n "glium") (r "~0.16.0") (d #t) (k 0)))) (h "1igkxy5bd9vzdqwyyz2ybyvi5dz9f8wqnk8q1bw0rc2r6l9cwazd")))

(define-public crate-glium_shapes-0.1.1 (c (n "glium_shapes") (v "0.1.1") (d (list (d (n "approx") (r "~0.1.1") (d #t) (k 0)) (d (n "cgmath") (r "~0.12.0") (d #t) (k 0)) (d (n "glium") (r "~0.16.0") (d #t) (k 0)))) (h "0z37an0ldjc99mf28skxx91kngrf15m6agsm49f4gyjh97c355px")))

(define-public crate-glium_shapes-0.2.0 (c (n "glium_shapes") (v "0.2.0") (d (list (d (n "approx") (r "~0.3") (d #t) (k 0)) (d (n "cgmath") (r "~0.17") (d #t) (k 0)) (d (n "glium") (r "~0.25") (d #t) (k 0)))) (h "0p5xsx8ph21f4pxsj06lz2rh5lrwd25cmmah1h7dq2n7h9w8cfcn")))

