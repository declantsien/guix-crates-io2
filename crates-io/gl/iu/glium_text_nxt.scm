(define-module (crates-io gl iu glium_text_nxt) #:use-module (crates-io))

(define-public crate-glium_text_nxt-0.15.0 (c (n "glium_text_nxt") (v "0.15.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 2)) (d (n "freetype-sys") (r "^0.7") (d #t) (k 0)) (d (n "glium") (r "^0.27") (k 0)) (d (n "glium") (r "^0.27") (f (quote ("glutin"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08xxifcb0iigk1qd5w98n7wrhl8ijpikx5vq7m1wirc4530afh30")))

