(define-module (crates-io gl iu glium-typed-buffer-any) #:use-module (crates-io))

(define-public crate-glium-typed-buffer-any-0.3.0 (c (n "glium-typed-buffer-any") (v "0.3.0") (d (list (d (n "glium") (r "^0.22") (d #t) (k 0)))) (h "1ni1rx8yrg26l0g68np7v1nwaaph5w3ps88dj2vqwv0s70qbfq5g")))

(define-public crate-glium-typed-buffer-any-0.4.0 (c (n "glium-typed-buffer-any") (v "0.4.0") (d (list (d (n "glium") (r "^0.23") (d #t) (k 0)))) (h "064zy1ydks2j6zjkxib9072ifmw9yigchrp1sqw8z2kkcz0rbm4x")))

(define-public crate-glium-typed-buffer-any-0.5.0 (c (n "glium-typed-buffer-any") (v "0.5.0") (d (list (d (n "glium") (r "^0.25.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)))) (h "1fj35fp3q8pkv9inxcb83v594accbyfw8qalk4zhiw7ai0jlvd3x")))

(define-public crate-glium-typed-buffer-any-0.6.0 (c (n "glium-typed-buffer-any") (v "0.6.0") (d (list (d (n "glium") (r "^0.26") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1i52sxmcynvphzbrymnawzb1fi3ynwd4a418l5nppp286f2bgfqh")))

