(define-module (crates-io gl df gldf_rs_python) #:use-module (crates-io))

(define-public crate-gldf_rs_python-0.2.0 (c (n "gldf_rs_python") (v "0.2.0") (d (list (d (n "gldf-rs") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.57") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (f (quote ("extension-module" "abi3-py37"))) (d #t) (k 0)) (d (n "pyo3-build-config") (r "^0.19.2") (d #t) (k 1)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1flvalvh2zcg0l35i66r39156iafdix95fdxcmlj58api6d17si1") (f (quote (("extension-module" "pyo3/extension-module"))))))

