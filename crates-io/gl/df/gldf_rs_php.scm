(define-module (crates-io gl df gldf_rs_php) #:use-module (crates-io))

(define-public crate-gldf_rs_php-0.2.0 (c (n "gldf_rs_php") (v "0.2.0") (d (list (d (n "gldf-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "phper") (r "^0.12.0") (d #t) (k 0)))) (h "0r2f6gp9xza62zm2asbb4563y7mlfl7y4hyyxrmfmcy3fag5mckv")))

