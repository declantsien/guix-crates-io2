(define-module (crates-io gl df gldf-sign) #:use-module (crates-io))

(define-public crate-gldf-sign-0.1.0 (c (n "gldf-sign") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("std" "cargo" "wrap_help" "string"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "gldf-rs") (r "^0.2.2") (d #t) (k 0)) (d (n "minisign") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "0lpkv9mznx2a6vyqi6iwax5089lrywxy378i6n06ydwjaxrh9n8n")))

