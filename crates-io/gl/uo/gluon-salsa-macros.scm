(define-module (crates-io gl uo gluon-salsa-macros) #:use-module (crates-io))

(define-public crate-gluon-salsa-macros-0.14.1 (c (n "gluon-salsa-macros") (v "0.14.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "127d2khc518l3787riz48wkfv59awm75ffx82mn8aa45gi1a730i")))

(define-public crate-gluon-salsa-macros-0.15.2 (c (n "gluon-salsa-macros") (v "0.15.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03wf9f2dczyk41rm8jy0jpmclw1cciin0sfgg41hh89fm4bjnyl0") (f (quote (("async"))))))

