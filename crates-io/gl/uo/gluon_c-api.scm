(define-module (crates-io gl uo gluon_c-api) #:use-module (crates-io))

(define-public crate-gluon_c-api-0.2.2 (c (n "gluon_c-api") (v "0.2.2") (d (list (d (n "gluon") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "0gf7w78khhx97pqwchn6d6mahhgb8ayd3zx1x1pnp77vjhqy57cy") (f (quote (("test" "gluon/test") ("skeptic" "gluon/skeptic") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.3.0 (c (n "gluon_c-api") (v "0.3.0") (d (list (d (n "gluon") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "1nc22kdh5svx6wm2vs6flx8fw8firl7b8gx984a41rldk3mx72jd") (f (quote (("test" "gluon/test") ("skeptic" "gluon/skeptic") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.4.0 (c (n "gluon_c-api") (v "0.4.0") (d (list (d (n "gluon") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "0vxxjrbx6cp4a59kv58j75pgs7bf4khhd2kwj45hxnwdyvdaxjrg") (f (quote (("test" "gluon/test") ("skeptic" "gluon/skeptic") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.4.2 (c (n "gluon_c-api") (v "0.4.2") (d (list (d (n "gluon") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "1ja84xdqhvpd5mss5sbd33sdz3yf8f9409hxpslsmh0g6yspq7c5") (f (quote (("test" "gluon/test") ("skeptic" "gluon/skeptic") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.5.0 (c (n "gluon_c-api") (v "0.5.0") (d (list (d (n "gluon") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "1ni3l4mhm38x5wckwjm6dhjrh9ilkva4gkfa9j7r5a6k3qm3z9l9") (f (quote (("test" "gluon/test") ("skeptic" "gluon/skeptic") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.6.0 (c (n "gluon_c-api") (v "0.6.0") (d (list (d (n "gluon") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "010fhaqmdh5xwfxjk2g1nqb61jca5v2x6b67x8xc65ycxfv7wf3k") (f (quote (("test" "gluon/test") ("skeptic" "gluon/skeptic") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.6.1 (c (n "gluon_c-api") (v "0.6.1") (d (list (d (n "gluon") (r "^0.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "1j4n8hyry1lsrf28di1jq72ax4f99cb4kg3l63k0wzzwmsrm1rvq") (f (quote (("test" "gluon/test") ("skeptic" "gluon/skeptic") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.6.2 (c (n "gluon_c-api") (v "0.6.2") (d (list (d (n "gluon") (r "^0.6.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "1h6f8i6ywixwl6zn9mc37fppmv4vcwdn4x70cr0z5z3k5aihql1c") (f (quote (("test" "gluon/test") ("skeptic" "gluon/skeptic") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.7.0 (c (n "gluon_c-api") (v "0.7.0") (d (list (d (n "gluon") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "1afbw3n0c9rmfa858k9kbx655jhm934ip92bwwzid395j9kyawfr") (f (quote (("test" "gluon/test") ("skeptic" "gluon/skeptic") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.7.1 (c (n "gluon_c-api") (v "0.7.1") (d (list (d (n "gluon") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0cdv79pqmfr156d0rzkl6m61zhgnd548r8rzimmaayfh6nqhklli") (f (quote (("test" "gluon/test") ("skeptic" "gluon/skeptic") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.8.0 (c (n "gluon_c-api") (v "0.8.0") (d (list (d (n "gluon") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0khc00cias1d987l0fg2x735dddz7s5ifzsrihh5n2pir8aalkv2") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.8.1 (c (n "gluon_c-api") (v "0.8.1") (d (list (d (n "gluon") (r "^0.8.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1c085yrikjjlz56fvi7fhijmv0g049qmhzr732iyps90wq4dgzg9") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.9.0 (c (n "gluon_c-api") (v "0.9.0") (d (list (d (n "gluon") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "01hl0agwflx4hy6a8vxyqjls0dl7nnr05z8jd7yl5sgv96n9a7q8") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.9.1 (c (n "gluon_c-api") (v "0.9.1") (d (list (d (n "gluon") (r "^0.9.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0gh4ghgbck0488f0xhbjmbgal63gav3ha4ia2cggwl0qwwpc6x5h") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.9.2 (c (n "gluon_c-api") (v "0.9.2") (d (list (d (n "gluon") (r "^0.9.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1vkklyz7qir0j7jrbnrswv7284528yy2g3s5c1l1xv9bjamy85a0") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.9.3 (c (n "gluon_c-api") (v "0.9.3") (d (list (d (n "gluon") (r "^0.9.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0bby6vwklgnxjkaaryh8ir7c91z5lwby023fwvcf6s5m10iklql0") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.9.4 (c (n "gluon_c-api") (v "0.9.4") (d (list (d (n "gluon") (r "^0.9.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1kqfxzmy6afmaabmfaf6nva04vgd2l2i1n11ksq3zia1hlpz6d99") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.10.0 (c (n "gluon_c-api") (v "0.10.0") (d (list (d (n "gluon") (r "^0.10.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1v9jqnmlaww18m1kc9raxjqbg5m7sr6wia5yk4p27izd69vz0xzl") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.10.1 (c (n "gluon_c-api") (v "0.10.1") (d (list (d (n "gluon") (r "^0.10.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1srdwyrr8j9qvc5l82wk246ip5zc0y7m5isqy0q39qa8srcymcnc") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.11.0 (c (n "gluon_c-api") (v "0.11.0") (d (list (d (n "gluon") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1c7gs49bb6bqs3vfmc4kx3d1nhwidgyjapjrwmnx4wl8fg93ks2c") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.11.2 (c (n "gluon_c-api") (v "0.11.2") (d (list (d (n "gluon") (r "^0.11.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0rmywlrwwxcs8hsgpgbr6qfjh83fdv3ldbdan4yv2qk65bb2ccs1") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.12.0 (c (n "gluon_c-api") (v "0.12.0") (d (list (d (n "gluon") (r "^0.12.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1ap857ck22am3gjyy0x1pr526p0yh6b5m7zc83zwh701k5akpqki") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.13.0 (c (n "gluon_c-api") (v "0.13.0") (d (list (d (n "gluon") (r "^0.13.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1jqxl3bdbmwki3sxq1qx8x19jh5jrbc884xzv5axn5prv0dj8gx8") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.13.1 (c (n "gluon_c-api") (v "0.13.1") (d (list (d (n "gluon") (r "^0.13.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1hv8qffzqigwj5flbvqsaf3ffh4akyicqvajb5bqy9kc74698ya4") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.14.1 (c (n "gluon_c-api") (v "0.14.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gluon") (r "^0.14.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0zjd9h04f8sxsfhixy01aydpsil2nl0cr69499hwvr9qligsgcsl") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.15.0 (c (n "gluon_c-api") (v "0.15.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gluon") (r "^0.15.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "04zqg5xiqqw24h65fbyrz8l957vhfs1ylx1z3rlkppxwvby6v9sv") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.15.1 (c (n "gluon_c-api") (v "0.15.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gluon") (r "^0.15.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0n7lz020z4p74jq4zvmiadnv61qv53qmpfl32lsb07am6z4nxk50") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.16.1 (c (n "gluon_c-api") (v "0.16.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gluon") (r "^0.16.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0bvhla14jsd8rh3sc1c4kaljzl314ssjmkgpg6yld0kcjk174nka") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.17.0 (c (n "gluon_c-api") (v "0.17.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gluon") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0m2lm6ab3v9s5q3v00pf15ig610vig0w52dms9inkq5j5cb3s613") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.17.1 (c (n "gluon_c-api") (v "0.17.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gluon") (r "^0.17.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "15gczx64mmaa5d5rj09fa3rl5pfh1kxvl46vv37dyn52nm23vahp") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.17.2 (c (n "gluon_c-api") (v "0.17.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gluon") (r "^0.17.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0nhwfv21p7l5n8wqjmcws8vb32vapygv1dh7hb2rd4l4akww5zq7") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.18.0 (c (n "gluon_c-api") (v "0.18.0") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "gluon") (r "^0.18.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.103") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "16g985rwy28mnm2724ms6aybzpv6hdhnm6y3aj9i7q14qbhrz04z") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

(define-public crate-gluon_c-api-0.18.2 (c (n "gluon_c-api") (v "0.18.2") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "gluon") (r "^0.18.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1dk5wfklrs53cdlcrji954047mjvbsqkf4l3ccbz9jm5sf40mrmq") (f (quote (("test" "gluon/test") ("nightly" "gluon/nightly"))))))

