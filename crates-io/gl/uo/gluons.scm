(define-module (crates-io gl uo gluons) #:use-module (crates-io))

(define-public crate-gluons-0.1.0 (c (n "gluons") (v "0.1.0") (h "0951903hnkfdqkl70grn1n1qpwfvab0xwjd7vpcj4vhpy59aa7g1")))

(define-public crate-gluons-0.1.1 (c (n "gluons") (v "0.1.1") (h "0lk5q2l0r9j66wg71lncad3979zgxg7y76ybfl18rhhcixxvyi6y")))

(define-public crate-gluons-0.1.2 (c (n "gluons") (v "0.1.2") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)))) (h "0h1bz7v0yvg9q8vjd6agqqnmmkzbf85cdiraypqq4i5a2y2k9b90")))

