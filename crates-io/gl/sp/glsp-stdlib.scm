(define-module (crates-io gl sp glsp-stdlib) #:use-module (crates-io))

(define-public crate-glsp-stdlib-0.1.0 (c (n "glsp-stdlib") (v "0.1.0") (d (list (d (n "glsp") (r "0.*") (d #t) (k 0) (p "glsp-engine")) (d (n "glsp-proc-macros") (r "0.*") (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("union" "write"))) (d #t) (k 0)))) (h "021cfv4f1s0r8l4sj3kd3ql22099vgdsxcb9021f8cjsv6kg0hf5")))

(define-public crate-glsp-stdlib-0.2.0 (c (n "glsp-stdlib") (v "0.2.0") (d (list (d (n "glsp") (r "^0.2") (d #t) (k 0) (p "glsp-engine")) (d (n "glsp-proc-macros") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("union" "write"))) (d #t) (k 0)))) (h "0k74w0ak5w8kz5q6m7y59443r3rhamxpc45picjk7swwa6b79kbh")))

