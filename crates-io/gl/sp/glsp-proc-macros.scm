(define-module (crates-io gl sp glsp-proc-macros) #:use-module (crates-io))

(define-public crate-glsp-proc-macros-0.1.0 (c (n "glsp-proc-macros") (v "0.1.0") (d (list (d (n "glsp") (r "0.*") (d #t) (k 0) (p "glsp-engine")))) (h "0g8bvpxjkrh6h6zvz6xaawj1shv69spjh8r5j1ygjxx2l2s7q910")))

(define-public crate-glsp-proc-macros-0.2.0 (c (n "glsp-proc-macros") (v "0.2.0") (d (list (d (n "glsp") (r "^0.2") (d #t) (k 0) (p "glsp-engine")))) (h "1l23l43jykrrnh5zy6gy5afy5r2w8f4wv481nk00b6g5jck9zjs3")))

