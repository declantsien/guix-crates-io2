(define-module (crates-io gl sp glsp-proc-macros2) #:use-module (crates-io))

(define-public crate-glsp-proc-macros2-0.1.0 (c (n "glsp-proc-macros2") (v "0.1.0") (d (list (d (n "glsp") (r "0.*") (f (quote ("compiler"))) (d #t) (k 0) (p "glsp-engine")) (d (n "glsp-proc-macros") (r "0.*") (d #t) (k 0)) (d (n "glsp-stdlib") (r "0.*") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0zini5wvmbsmwq1q88pkj8s2drnra927bhs921ilfjqa3k5946x5")))

(define-public crate-glsp-proc-macros2-0.2.0 (c (n "glsp-proc-macros2") (v "0.2.0") (d (list (d (n "glsp") (r "^0.2") (f (quote ("compiler"))) (d #t) (k 0) (p "glsp-engine")) (d (n "glsp-proc-macros") (r "^0.2") (d #t) (k 0)) (d (n "glsp-stdlib") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0a7693vs1n45miax2ghrh8xim30cymznx77dk1amfynqbks8i3qa")))

