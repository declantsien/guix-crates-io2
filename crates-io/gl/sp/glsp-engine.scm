(define-module (crates-io gl sp glsp-engine) #:use-module (crates-io))

(define-public crate-glsp-engine-0.1.0 (c (n "glsp-engine") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("union" "write"))) (d #t) (k 0)))) (h "0wlxycn9zjpr9schfnnna9mihdxaavar7gcwiw5zjysbcsvkb7gf") (f (quote (("unsafe-internals") ("compiler" "serde" "serde/derive" "bincode" "flate2"))))))

(define-public crate-glsp-engine-0.2.0 (c (n "glsp-engine") (v "0.2.0") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("union" "write"))) (d #t) (k 0)))) (h "13l0fqfhxzwkb05bvlbarvym6jnbf5f72q2m57galqz2hmdfw1l8") (f (quote (("unsafe-internals") ("compiler" "serde" "serde/derive" "bincode" "flate2"))))))

