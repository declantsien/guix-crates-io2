(define-module (crates-io gl u- glu-sys) #:use-module (crates-io))

(define-public crate-glu-sys-0.1.0 (c (n "glu-sys") (v "0.1.0") (h "0nn6w3pmqah1dp483nll7zq292plqqmcvs6qlmh4jmla14zf11nz")))

(define-public crate-glu-sys-0.1.1 (c (n "glu-sys") (v "0.1.1") (h "1h3h3dhr966iwlcal8hk006548yxk1rw680mvwqpl945rc24alhf")))

(define-public crate-glu-sys-0.1.2 (c (n "glu-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "06gsc2qb83n64i746h5mrgw9g282ahhzrjpachfyrh8ffvwzvd0f")))

(define-public crate-glu-sys-0.1.3 (c (n "glu-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "074979wgd71v074wkq065mk9nydqkyxjbp36nmh4bxfpymxq9vrs")))

(define-public crate-glu-sys-0.1.4 (c (n "glu-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "1wy3bj82gcqzb4k9mkvwl9hdc98q1wwgn3kcf26y4by2kjmnlvmr")))

