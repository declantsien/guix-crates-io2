(define-module (crates-io gl oo gloo-worker-macros) #:use-module (crates-io))

(define-public crate-gloo-worker-macros-0.1.0 (c (n "gloo-worker-macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1rs0f6b34mkhlmpmhqi747c34000sd5mxma92yacjyw5sicalv4m")))

