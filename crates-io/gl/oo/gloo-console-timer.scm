(define-module (crates-io gl oo gloo-console-timer) #:use-module (crates-io))

(define-public crate-gloo-console-timer-0.1.0 (c (n "gloo-console-timer") (v "0.1.0") (d (list (d (n "gloo-timers") (r "^0.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.2.43") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.17") (f (quote ("console"))) (d #t) (k 0)))) (h "1sj1r71hx43yifvp8zlzs4w6ww9gj4dc7zvd5i007b199da7b1ml")))

