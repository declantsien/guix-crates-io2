(define-module (crates-io gl oo gloo-dialogs) #:use-module (crates-io))

(define-public crate-gloo-dialogs-0.1.0 (c (n "gloo-dialogs") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (d #t) (k 0)))) (h "0h2b8sqjiy48whddawl5zdassvrk78q3shil2czjivd25rx5bysg")))

(define-public crate-gloo-dialogs-0.1.1 (c (n "gloo-dialogs") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (d #t) (k 0)))) (h "1rh2j0l8rbj8pbypxqy99qi2x3hq52sclijs8h47zlkjmij261k7")))

(define-public crate-gloo-dialogs-0.2.0 (c (n "gloo-dialogs") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (d #t) (k 0)))) (h "1pqmg2z3x4c3id25jd0p8rjwy5qjbc4k1x8gflsi9c1207hlhixz") (r "1.64")))

