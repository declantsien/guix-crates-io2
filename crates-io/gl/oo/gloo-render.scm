(define-module (crates-io gl oo gloo-render) #:use-module (crates-io))

(define-public crate-gloo-render-0.1.0 (c (n "gloo-render") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (d #t) (k 0)))) (h "0vkzsrm80x42pdhpv1x990jcqnryj1rkhd3a7i5bpwwx2ipdlk2v")))

(define-public crate-gloo-render-0.1.1 (c (n "gloo-render") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (d #t) (k 0)))) (h "0r3pxj22l489ldakj6521a0f0n1r9v8xrai3k12d9kv7xxm31n9g")))

(define-public crate-gloo-render-0.2.0 (c (n "gloo-render") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (d #t) (k 0)))) (h "0cwqcka7l5p29idq174c6mi5cgal0rywngdck26qwfki8ikqn02n") (r "1.64")))

