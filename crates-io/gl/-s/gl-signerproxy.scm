(define-module (crates-io gl -s gl-signerproxy) #:use-module (crates-io))

(define-public crate-gl-signerproxy-0.1.0 (c (n "gl-signerproxy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.3") (f (quote ("tls" "transport"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.3") (d #t) (k 1)) (d (n "tower") (r "^0.3") (d #t) (k 0)) (d (n "which") (r "^4.0.2") (d #t) (k 0)))) (h "161pn0rnhp51y0rwb7h7zpmra4z2qv16n84d19ggayz9z8bwshk9")))

