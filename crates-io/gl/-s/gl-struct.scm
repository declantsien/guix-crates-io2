(define-module (crates-io gl -s gl-struct) #:use-module (crates-io))

(define-public crate-gl-struct-0.0.1 (c (n "gl-struct") (v "0.0.1") (d (list (d (n "gl") (r "^0.12.0") (d #t) (k 0)) (d (n "glfw") (r "^0.27.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 2)))) (h "1w8f5kjx6rrxwm4j9m23vac36r3gx18nm1h7zf2lps5avrkblrs4") (f (quote (("large_uniform_arrays") ("extra_large_uniform_arrays") ("default"))))))

