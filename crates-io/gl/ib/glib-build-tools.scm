(define-module (crates-io gl ib glib-build-tools) #:use-module (crates-io))

(define-public crate-glib-build-tools-0.0.0 (c (n "glib-build-tools") (v "0.0.0") (h "0wxkrgzk71flkzpnm0r4sndxbfvdmqdg20s7z4x132wnmpr94hgn")))

(define-public crate-glib-build-tools-0.1.0 (c (n "glib-build-tools") (v "0.1.0") (d (list (d (n "gio") (r "^0.16") (o #t) (d #t) (k 0)))) (h "14z15aj9jiyk5ihyxgswirs6d43l13axn9xs7lslykd4qx6cxmw0") (f (quote (("dox" "gio" "gio/dox")))) (r "1.63")))

(define-public crate-glib-build-tools-0.16.0 (c (n "glib-build-tools") (v "0.16.0") (d (list (d (n "gio") (r "^0.16") (o #t) (d #t) (k 0)))) (h "0mdd3nqvgrx63acx8nj9qc2gawajdqqjp11f5s83v7pkf4yaw2mp") (f (quote (("dox" "gio" "gio/dox")))) (r "1.63")))

(define-public crate-glib-build-tools-0.16.3 (c (n "glib-build-tools") (v "0.16.3") (d (list (d (n "gio") (r "^0.16") (o #t) (d #t) (k 0)))) (h "1z73bl10zmxwrv16v4f5wcky1f3z5a2v0hknca54al4k2p5ka695") (f (quote (("dox" "gio" "gio/dox")))) (r "1.63")))

(define-public crate-glib-build-tools-0.17.0 (c (n "glib-build-tools") (v "0.17.0") (d (list (d (n "gio") (r "^0.17") (o #t) (d #t) (k 0)))) (h "184f7llpxny4h0hbds97w6z3ghldgw1ldx5ssnl6mh4wpb4q114g") (f (quote (("dox" "gio" "gio/dox")))) (r "1.64")))

(define-public crate-glib-build-tools-0.17.10 (c (n "glib-build-tools") (v "0.17.10") (d (list (d (n "gio") (r "^0.17") (o #t) (d #t) (k 0)))) (h "05p7ab2vn8962cbchi7a6hndhvw64nqk4w5kpg5z53iizsgdfrbs") (f (quote (("dox" "gio" "gio/dox")))) (r "1.64")))

(define-public crate-glib-build-tools-0.18.0 (c (n "glib-build-tools") (v "0.18.0") (d (list (d (n "gio") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0p5c2ayiam5bkp9wvq9f9ihwp06nqs5j801npjlwnhrl8rpwac9l") (r "1.70")))

(define-public crate-glib-build-tools-0.19.0 (c (n "glib-build-tools") (v "0.19.0") (d (list (d (n "gio") (r "^0.19") (d #t) (k 0)))) (h "0sn83nn2vv9ljczblwfhgn6x3bhkf9p93n3h1m5x3vv0zx7kg3qh") (r "1.70")))

