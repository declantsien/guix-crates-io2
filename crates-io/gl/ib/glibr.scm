(define-module (crates-io gl ib glibr) #:use-module (crates-io))

(define-public crate-glibr-0.1.0 (c (n "glibr") (v "0.1.0") (h "12nxzm32byhbvp2hdxkqfs9mvnqbqf2bxsgamsbhrnbnk9jac34r")))

(define-public crate-glibr-0.1.1 (c (n "glibr") (v "0.1.1") (h "19qca4dl74d2531rwwqm2jqm6d6w9q330mis31wfci11gvazmvgc")))

