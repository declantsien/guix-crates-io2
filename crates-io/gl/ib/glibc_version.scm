(define-module (crates-io gl ib glibc_version) #:use-module (crates-io))

(define-public crate-glibc_version-0.1.0 (c (n "glibc_version") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "078g2gs957bsargpjddgv176rwhhhixvlvblzvfd9bn2w18yjygf")))

(define-public crate-glibc_version-0.1.1 (c (n "glibc_version") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0a4jqhzw9j0g7nvmjg4zl6vcpf10bk1sknxqspm44v877b1f7bs0")))

(define-public crate-glibc_version-0.1.2 (c (n "glibc_version") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gn3y7j2rml9yqq6224dvhc3szciv9h0r2mnck0f5d0sbxizfgw0")))

