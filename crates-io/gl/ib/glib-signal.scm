(define-module (crates-io gl ib glib-signal) #:use-module (crates-io))

(define-public crate-glib-signal-0.1.0 (c (n "glib-signal") (v "0.1.0") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "glib") (r "^0.16") (d #t) (k 0)))) (h "1zf9r4vbficpk6r8fcn85qa2yyvq0894ri0z9644hs2b28dlfqxz") (f (quote (("dox" "glib/dox") ("default")))) (s 2) (e (quote (("futures" "dep:futures-core" "dep:futures-channel")))) (r "1.60")))

(define-public crate-glib-signal-0.2.0 (c (n "glib-signal") (v "0.2.0") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "glib") (r "^0.17") (d #t) (k 0)))) (h "0c8xgnfqspv4b167ij468bbccq5plmqcgz5q0p827bqy1ky9dfbc") (f (quote (("dox" "glib/dox") ("default")))) (s 2) (e (quote (("futures" "dep:futures-core" "dep:futures-channel")))) (r "1.60")))

(define-public crate-glib-signal-0.3.0 (c (n "glib-signal") (v "0.3.0") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "glib") (r "^0.18") (d #t) (k 0)))) (h "030wwiz8ilwdnim9v8279jwyywgf26ziwglklyiccz823vsdjj7h") (f (quote (("default")))) (s 2) (e (quote (("futures" "dep:futures-core" "dep:futures-channel")))) (r "1.70")))

(define-public crate-glib-signal-0.4.0 (c (n "glib-signal") (v "0.4.0") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "glib") (r "^0.19") (d #t) (k 0)))) (h "1cs9612aqpi46c4a6ly1f7ny0z0ppl8slq2wny6cxhc37hv298rh") (f (quote (("default")))) (s 2) (e (quote (("futures" "dep:futures-core" "dep:futures-channel")))) (r "1.70")))

