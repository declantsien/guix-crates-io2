(define-module (crates-io gl ib glib-itc) #:use-module (crates-io))

(define-public crate-glib-itc-0.1.0 (c (n "glib-itc") (v "0.1.0") (d (list (d (n "glib") (r "^0.1.2") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "gtk") (r "^0.1.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1iybwbhs91b2083lfz1msf9xfgj93gphs2nvb0mvjcjk79krwkzv")))

(define-public crate-glib-itc-0.1.1 (c (n "glib-itc") (v "0.1.1") (d (list (d (n "glib") (r "^0.1.2") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "gtk") (r "^0.1.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "14frhlx4d7v6blw4m6ljl5r5nwd0r8xv97jyf185kxl2srwfw4b5")))

(define-public crate-glib-itc-0.1.2 (c (n "glib-itc") (v "0.1.2") (d (list (d (n "glib") (r "^0.1.2") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "gtk") (r "^0.1.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "0h8a5lrkvr2rl40m2q2smj95mzhaqfgwhzkwllg02js6qsijl3iy")))

(define-public crate-glib-itc-0.1.3 (c (n "glib-itc") (v "0.1.3") (d (list (d (n "glib") (r "^0.1.2") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "gtk") (r "^0.1.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "0923h9k0gbrbdadinn685gi7k6pdp2ar7i7s35ca5zsn834c3nbi")))

(define-public crate-glib-itc-0.1.4 (c (n "glib-itc") (v "0.1.4") (d (list (d (n "glib") (r "^0.1.2") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "gtk") (r "^0.1.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "0izhbyvr3wg2g9ry0pd233fj8wabd1m2jrx49369r0mbfv9b11b0")))

(define-public crate-glib-itc-0.2.0 (c (n "glib-itc") (v "0.2.0") (d (list (d (n "glib") (r "^0.3.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk") (r "^0.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "os_pipe") (r "^0.6.0") (d #t) (k 0)))) (h "18xskbj0r5qha23s650kkpdxfcgxfhxh8gzxrc7k3q8661pdfgwv")))

(define-public crate-glib-itc-0.2.1 (c (n "glib-itc") (v "0.2.1") (d (list (d (n "glib") (r "^0.3.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk") (r "^0.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)))) (h "1wvki7w4cvivkmzbbc78b19z933sdw1zq7dlznc67fp2mc9r2h0z")))

(define-public crate-glib-itc-0.3.0 (c (n "glib-itc") (v "0.3.0") (d (list (d (n "glib") (r "^0.4.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)))) (h "1c7x718xbk3hdgjcq1zdh2c1npbqy1kz1acbdxys59q0bbmwxj75")))

(define-public crate-glib-itc-0.4.0 (c (n "glib-itc") (v "0.4.0") (d (list (d (n "glib") (r "^0.4.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)))) (h "1z001xqmd39xxwbahi3xmzv2vvbfz7fw8ka70wa1lz5902538qzn")))

(define-public crate-glib-itc-0.5.0 (c (n "glib-itc") (v "0.5.0") (d (list (d (n "glib") (r "^0.6.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "gtk") (r "^0.5.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)))) (h "0nhaj4n048w1m0cra19z8jnpy62s9cc9g1ihh42g83aylh2lcav5")))

