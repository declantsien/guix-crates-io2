(define-module (crates-io gl ib glib_logger) #:use-module (crates-io))

(define-public crate-glib_logger-0.1.0 (c (n "glib_logger") (v "0.1.0") (d (list (d (n "glib-sys") (r "^0.9.1") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1r00nq6hkpwg6jyw66fgzlbghnhmq1xzzahn6j1zn4z00464x008")))

