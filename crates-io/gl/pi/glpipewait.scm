(define-module (crates-io gl pi glpipewait) #:use-module (crates-io))

(define-public crate-glpipewait-0.1.0 (c (n "glpipewait") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("env"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0nzfprkizklrc44qalwcyf2cjgy7ms6i0lwdz6vz0d3gbbwd43wb")))

