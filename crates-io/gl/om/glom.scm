(define-module (crates-io gl om glom) #:use-module (crates-io))

(define-public crate-glom-0.1.0 (c (n "glom") (v "0.1.0") (d (list (d (n "reqwasm") (r "^0.5.0") (d #t) (k 0)) (d (n "safina-executor") (r "^0.3.3") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window" "Document" "Element" "HtmlDocument" "HtmlElement" "HtmlHeadElement" "Headers" "Request" "RequestInit" "RequestMode" "Response" "Node"))) (d #t) (k 0)))) (h "01r24xq81s4cjxi9z5nk0kib27fsvf850358y55y212qvhvkb5vw")))

