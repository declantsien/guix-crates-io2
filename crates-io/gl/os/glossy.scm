(define-module (crates-io gl os glossy) #:use-module (crates-io))

(define-public crate-glossy-0.1.0 (c (n "glossy") (v "0.1.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "glsl-optimizer-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1s6h26gyqwxhi2vg4v0mjh5jxg7mlszy96il1vvamaanzbpxm1sz") (f (quote (("optimizer" "glsl-optimizer-sys" "libc") ("default" "optimizer"))))))

(define-public crate-glossy-0.2.0 (c (n "glossy") (v "0.2.0") (h "1b9ijbn326w0b5y7c9r8ns9b26b2i375z9id5x6jkdgi1b1rbxyd")))

