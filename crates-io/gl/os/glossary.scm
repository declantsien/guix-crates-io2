(define-module (crates-io gl os glossary) #:use-module (crates-io))

(define-public crate-glossary-0.1.0 (c (n "glossary") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1n59n00z9knxmrj6hlk6a5la9v2yxkz597b7wdik7280v6pfmw8g")))

(define-public crate-glossary-0.1.1 (c (n "glossary") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "0dz6pmyih7n4dihy7qyqs6fg324b0d2k3a72ss9j5rhwmbx9vmsz")))

