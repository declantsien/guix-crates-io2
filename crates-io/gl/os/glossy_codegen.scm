(define-module (crates-io gl os glossy_codegen) #:use-module (crates-io))

(define-public crate-glossy_codegen-0.2.0 (c (n "glossy_codegen") (v "0.2.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "glossy") (r "^0.2") (d #t) (k 0)) (d (n "glsl-optimizer-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0mzidh874hslnp9nz51aagqhgzlgnakrcc8lw7r6imdfmrnkhh0f") (f (quote (("optimizer" "glsl-optimizer-sys" "libc") ("default" "optimizer"))))))

