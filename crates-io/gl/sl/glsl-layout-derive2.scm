(define-module (crates-io gl sl glsl-layout-derive2) #:use-module (crates-io))

(define-public crate-glsl-layout-derive2-0.4.0 (c (n "glsl-layout-derive2") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1vb4z6bcj8vbblsmix7dbxlmr0zqsf8sfy0h9g3sqmwdf4h88dcp")))

