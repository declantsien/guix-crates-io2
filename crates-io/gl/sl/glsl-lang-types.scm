(define-module (crates-io gl sl glsl-lang-types) #:use-module (crates-io))

(define-public crate-glsl-lang-types-0.2.0 (c (n "glsl-lang-types") (v "0.2.0") (d (list (d (n "lang-util") (r "^0.2.0") (d #t) (k 0)) (d (n "rserde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cyaapb747vxd2sm11p26fvzhkjvi0pb50nyjrb3h2z2ys18yw5q") (f (quote (("serde" "rserde" "lang-util/serde") ("default"))))))

(define-public crate-glsl-lang-types-0.2.1 (c (n "glsl-lang-types") (v "0.2.1") (d (list (d (n "lang-util") (r "^0.2.1") (d #t) (k 0)) (d (n "rserde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dyq43dp6n14kxyhdgmjfrhxkkci9ncinbwx3cf0ahnwfr0ngdid") (f (quote (("serde" "rserde" "lang-util/serde") ("default"))))))

(define-public crate-glsl-lang-types-0.3.0 (c (n "glsl-lang-types") (v "0.3.0") (d (list (d (n "lang-util") (r "^0.3.0") (d #t) (k 0)) (d (n "rserde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05p0pyrj9szxr2wn1m32h0g0w4wzyyi9hkw6pd5kn9cp5a4gw8l8") (f (quote (("serde" "rserde" "lang-util/serde") ("default"))))))

(define-public crate-glsl-lang-types-0.3.1 (c (n "glsl-lang-types") (v "0.3.1") (d (list (d (n "lang-util") (r "^0.3.0") (d #t) (k 0)) (d (n "rserde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ygbhs5mrv4mzh5sr13qlw3cwfs3717259jy9c28rxc0cn5h3z28") (f (quote (("serde" "rserde" "lang-util/serde") ("default"))))))

(define-public crate-glsl-lang-types-0.4.0 (c (n "glsl-lang-types") (v "0.4.0") (d (list (d (n "lang-util") (r "^0.4.0") (d #t) (k 0)) (d (n "rserde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nzji05x3nqraiscwikipd3agmkb7caghh95lzy2wngmxssi83rp") (f (quote (("serde" "rserde" "lang-util/serde") ("default"))))))

(define-public crate-glsl-lang-types-0.4.1 (c (n "glsl-lang-types") (v "0.4.1") (d (list (d (n "lang-util") (r "^0.4.0") (d #t) (k 0)) (d (n "rserde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lfgv827cq4rv1xm11h2v7vc24lzp59g9gnifzmkrj2q7ny9rqav") (f (quote (("serde" "rserde" "lang-util/serde") ("default"))))))

(define-public crate-glsl-lang-types-0.5.0 (c (n "glsl-lang-types") (v "0.5.0") (d (list (d (n "lang-util") (r "^0.5.0") (d #t) (k 0)) (d (n "rserde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fcdyr441bxywcfb0ap8azycfh45g1047af5xkv385cf7qbz47i6") (f (quote (("serde" "rserde" "lang-util/serde") ("default"))))))

(define-public crate-glsl-lang-types-0.5.1 (c (n "glsl-lang-types") (v "0.5.1") (d (list (d (n "lang-util") (r "^0.5.0") (d #t) (k 0)) (d (n "rserde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bmrisnd1i9rkfnaq9nbav9fsdkb8gqxbhcblqph1bdxmcpq2sw2") (f (quote (("serde" "rserde" "lang-util/serde") ("default"))))))

(define-public crate-glsl-lang-types-0.5.2 (c (n "glsl-lang-types") (v "0.5.2") (d (list (d (n "lang-util") (r "=0.5.2") (d #t) (k 0)) (d (n "rserde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1265z88d15y2d6m15xb1167kqcmn01si6i8j4bg3gkbhb4k9y4gm") (f (quote (("serde" "rserde" "lang-util/serde") ("default"))))))

