(define-module (crates-io gl sl glsl-layout2) #:use-module (crates-io))

(define-public crate-glsl-layout2-0.5.1 (c (n "glsl-layout2") (v "0.5.1") (d (list (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.23.0") (o #t) (d #t) (k 0)) (d (n "glsl-layout-derive2") (r "^0.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (o #t) (d #t) (k 0)))) (h "180lbx7x01mmim6f6bn7xcvv81bqvcm42y5yhi8i67v2f10wwz18") (f (quote (("bigger-arrays"))))))

