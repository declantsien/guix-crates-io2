(define-module (crates-io gl sl glslopt) #:use-module (crates-io))

(define-public crate-glslopt-0.1.0 (c (n "glslopt") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0xhqfnj3j8hhj04625fc2pwv841gaysbbrplpi7vxpy76zxwfmwc")))

(define-public crate-glslopt-0.1.1 (c (n "glslopt") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1hqrw4cd1xqs2kgfsz1d83slzv80ijsmd36mkdfnb28wgniikk4i")))

(define-public crate-glslopt-0.1.2 (c (n "glslopt") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0yc3kd04hpa1wh4rizpqz7jp02f4irss0fdgd2ic91bgrwzkhazj")))

(define-public crate-glslopt-0.1.3 (c (n "glslopt") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "084cnsgv0vlcs7pdbfcby4jscpzsh8lpm6f39ha94n84apfn8sd1")))

(define-public crate-glslopt-0.1.4 (c (n "glslopt") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0i1fq0axpvll6wpvk39f4jbcnrrrpk82mz94hx11hp6j3aa2wp06")))

(define-public crate-glslopt-0.1.5 (c (n "glslopt") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0xrj2szq57vkfwy5b1a35xl6nqggal6flf9g9clrpm6nqsy5mqxd")))

(define-public crate-glslopt-0.1.6 (c (n "glslopt") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0zsq3fblgz9vm9ych278daf4960id2gjhfb2hanks20jwqmgh4jq")))

(define-public crate-glslopt-0.1.7 (c (n "glslopt") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1whdwicwv3jpzq9vx99zgwnp7px5z0qxbpjpz4fzg6frbnkml8ld")))

(define-public crate-glslopt-0.1.8 (c (n "glslopt") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0lr80514py6bv89smzny4xlcph45qhkj4gznsrvva08wan0gxip6")))

(define-public crate-glslopt-0.1.9 (c (n "glslopt") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1bybm2987ipykccxdjqsvrsgc42idzpfbxh89gdxmpsh8k0gb8vl")))

(define-public crate-glslopt-0.1.10 (c (n "glslopt") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0xqqv3a8iws4larngj8yqkkwv8ha04xjwglg32mpnn1x00lycnzf")))

