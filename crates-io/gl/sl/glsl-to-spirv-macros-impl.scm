(define-module (crates-io gl sl glsl-to-spirv-macros-impl) #:use-module (crates-io))

(define-public crate-glsl-to-spirv-macros-impl-0.1.0 (c (n "glsl-to-spirv-macros-impl") (v "0.1.0") (d (list (d (n "glsl-to-spirv") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0jbnqilafb71i3ibkm2jwy0mfdfjif5600n2byj67f5380nand7n")))

