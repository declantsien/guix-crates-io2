(define-module (crates-io gl sl glsl-tree) #:use-module (crates-io))

(define-public crate-glsl-tree-0.1.0 (c (n "glsl-tree") (v "0.1.0") (d (list (d (n "glsl") (r "^4.1") (d #t) (k 0)))) (h "1v18vw4kzvb1xqnbwrmviamkal4465nv8iqxd4r0yzgcj021zvw1")))

(define-public crate-glsl-tree-0.2.0 (c (n "glsl-tree") (v "0.2.0") (d (list (d (n "glsl") (r "^5") (d #t) (k 0)))) (h "15i2z7fgg24la0n28npbx8qswkf75qx8ap631avjj4xg27pzxbx9")))

(define-public crate-glsl-tree-0.2.1 (c (n "glsl-tree") (v "0.2.1") (d (list (d (n "glsl") (r "^6") (d #t) (k 0)))) (h "04yb6s7nif4hpzjmfvag6y3ncy51rzc241ic8ync7dif6wdhjhjv")))

(define-public crate-glsl-tree-0.3.0 (c (n "glsl-tree") (v "0.3.0") (d (list (d (n "glsl") (r "^7") (d #t) (k 0)))) (h "0wlmjfl53msxr5z4v051aw6k2hq7bgmx8mzdshbrgymag4dalkgi")))

