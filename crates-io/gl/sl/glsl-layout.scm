(define-module (crates-io gl sl glsl-layout) #:use-module (crates-io))

(define-public crate-glsl-layout-0.1.0 (c (n "glsl-layout") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "glsl-layout-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0zlqyajhni1xagf9hrklk5bigldvnv84qly23wb632bc0wws2dni") (f (quote (("gfx" "gfx_core" "glsl-layout-derive/gfx") ("bigger-arrays"))))))

(define-public crate-glsl-layout-0.1.1 (c (n "glsl-layout") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "glsl-layout-derive") (r "^0.1.1") (d #t) (k 0)))) (h "16ba8bxfsgzcfvlplr4h31k44dvlhj51qpqv6wy2vl2jk4jvrw34") (f (quote (("gfx" "gfx_core" "glsl-layout-derive/gfx") ("bigger-arrays"))))))

(define-public crate-glsl-layout-0.2.0 (c (n "glsl-layout") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "glsl-layout-derive") (r "^0.2") (d #t) (k 0)))) (h "0qnk9azh02vf81m36fvqr7vs4iqwwdphs1sdwvc7898vx013rs3j") (f (quote (("bigger-arrays"))))))

(define-public crate-glsl-layout-0.3.0 (c (n "glsl-layout") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "glsl-layout-derive") (r "^0.3") (d #t) (k 0)))) (h "1vqbr18iqlbhszdn0vn5sfb7vh35z9lda545pm79pb0vyb1pix0l") (f (quote (("bigger-arrays"))))))

(define-public crate-glsl-layout-0.3.2 (c (n "glsl-layout") (v "0.3.2") (d (list (d (n "cgmath") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "glsl-layout-derive") (r "^0.3.2") (d #t) (k 0)))) (h "1g1kk7zrfg9763r0fmz256ks593a084xz1l6ww218l36yclgl2ca") (f (quote (("bigger-arrays"))))))

(define-public crate-glsl-layout-0.4.0 (c (n "glsl-layout") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glsl-layout-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (o #t) (d #t) (k 0)))) (h "09vnysv0r0rjgkxw3s84zy0b32195hanxbawmgw514fb1viasa12") (f (quote (("bigger-arrays"))))))

(define-public crate-glsl-layout-0.4.1 (c (n "glsl-layout") (v "0.4.1") (d (list (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "glsl-layout-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (o #t) (d #t) (k 0)))) (h "0ncxk50yiahh306hxm54vj9xqvkpn1xqz40diliy2c13gldz7s0x") (f (quote (("bigger-arrays"))))))

(define-public crate-glsl-layout-0.4.2 (c (n "glsl-layout") (v "0.4.2") (d (list (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "glsl-layout-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (o #t) (d #t) (k 0)))) (h "0sz1hk5fz8wg2sv5k0blagav29a8vs1h93p7kklg50jba1i6rm58") (f (quote (("bigger-arrays"))))))

(define-public crate-glsl-layout-0.5.0 (c (n "glsl-layout") (v "0.5.0") (d (list (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "glsl-layout-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (o #t) (d #t) (k 0)))) (h "15f2kbl6w97w46z5mmjq6i8wbgpfvv7vckbzc0dwl9av83x0chis") (f (quote (("bigger-arrays"))))))

