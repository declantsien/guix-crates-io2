(define-module (crates-io gl sl glslang-sys) #:use-module (crates-io))

(define-public crate-glslang-sys-0.0.1 (c (n "glslang-sys") (v "0.0.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0qy2f2xgdxzzghyyq1k48ll8i10xbkswkj3xxn4accgar69jwhlq")))

(define-public crate-glslang-sys-0.1.0 (c (n "glslang-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0s89fa33xrf35cfkdfrqlxk04g9snrhl7592jmj9r96ssjznldla")))

(define-public crate-glslang-sys-0.2.0 (c (n "glslang-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1kn00jdcqb3g3alnhcpbql2j84wn0p8w7c0pzlg61sq267ja90id") (l "glslang")))

(define-public crate-glslang-sys-0.2.1 (c (n "glslang-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "0znbwmm1h62fns9jvc58mmdvqs278rx06qj16mjzvd3nr3pirj53") (l "glslang")))

(define-public crate-glslang-sys-0.3.0 (c (n "glslang-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "006nsgjbn9dpk2d3hpbd2zyfmkz4f414c1l7cyhkm5lqrkxsvshz") (l "glslang")))

(define-public crate-glslang-sys-0.3.1 (c (n "glslang-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "05mqz2brlj63a7kn3wysaxwyysck792dczp74qna3s6nbbbz4xkd") (l "glslang")))

(define-public crate-glslang-sys-0.3.2 (c (n "glslang-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "1189jd1n3hrd71jdv1jkxfy8jajwcz0h5jhbvx1rpv46y19f0iva") (l "glslang")))

