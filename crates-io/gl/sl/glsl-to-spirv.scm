(define-module (crates-io gl sl glsl-to-spirv) #:use-module (crates-io))

(define-public crate-glsl-to-spirv-0.1.0 (c (n "glsl-to-spirv") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "104r9cs1jzd4hyfv55j25dx96r7mmgfpdj68dl15yrl2mhz905xl")))

(define-public crate-glsl-to-spirv-0.1.1 (c (n "glsl-to-spirv") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "1p0s0v6n64v8638bsn6qisfl2wwswzjwdqx948b73iid55nd3flp")))

(define-public crate-glsl-to-spirv-0.1.2 (c (n "glsl-to-spirv") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0cf54r171wbr6vlfy52nzcvsh16h1rwxb01rizwskrv737ini5n0")))

(define-public crate-glsl-to-spirv-0.1.3 (c (n "glsl-to-spirv") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1.19") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1m84z3l7vpn5cf4b30apl0rrijf5lxa9fy82q2djjgjfwwfgivnb")))

(define-public crate-glsl-to-spirv-0.1.4 (c (n "glsl-to-spirv") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1.27") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1rllp6368i2j7ccqqkf5d382cnbbw4rj85m764qjylxaxm4ddlch")))

(define-public crate-glsl-to-spirv-0.1.5 (c (n "glsl-to-spirv") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1.27") (d #t) (k 1)) (d (n "sha2") (r "^0.7") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0xgj9p8f1bmin9cqpqcgsxnkqlwfqf0krvswbjvdkcw82d5mg862")))

(define-public crate-glsl-to-spirv-0.1.6 (c (n "glsl-to-spirv") (v "0.1.6") (d (list (d (n "cmake") (r "^0.1.27") (d #t) (k 1)) (d (n "sha2") (r "^0.7") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "025bj8zary8srbmmrz46lpsh133bbw7livwlzdpycxhwp9hfcpb0")))

(define-public crate-glsl-to-spirv-0.1.7 (c (n "glsl-to-spirv") (v "0.1.7") (d (list (d (n "cmake") (r "^0.1.27") (d #t) (k 1)) (d (n "sha2") (r "^0.7") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1wdydcmzf7pc7q7k5l5szva42kp0rdnzcg9d79h0gma6hz4ypji8")))

