(define-module (crates-io gl sl glsl-to-spirv-macros) #:use-module (crates-io))

(define-public crate-glsl-to-spirv-macros-0.1.0-pre (c (n "glsl-to-spirv-macros") (v "0.1.0-pre") (h "03km4nyfvz3scg487jbih8shnh0fsraykrznprqgv7gbqkrj1p9v")))

(define-public crate-glsl-to-spirv-macros-0.1.0 (c (n "glsl-to-spirv-macros") (v "0.1.0") (h "131dxkaavhjfyabmcn9a3fh17gqnpagwgmfksnivhns8991kfpvp")))

(define-public crate-glsl-to-spirv-macros-0.1.1 (c (n "glsl-to-spirv-macros") (v "0.1.1") (h "16m8g4g1ic4lqm06j65m6pv7mdcqswx11mjhikzfzwws4xx1iwg8")))

