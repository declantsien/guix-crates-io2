(define-module (crates-io gl sl glsl-optimizer-sys) #:use-module (crates-io))

(define-public crate-glsl-optimizer-sys-0.1.0 (c (n "glsl-optimizer-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fkpb2acncyhd22dg4jl6s77n17d0x57x28dwdawnwgqrhcj9v2x")))

