(define-module (crates-io gl sl glslang) #:use-module (crates-io))

(define-public crate-glslang-0.1.0 (c (n "glslang") (v "0.1.0") (h "1bqlbcyqqvxi7rj487pnn7c5lf9sk2x1lb5a8dwgpid9jggcbrmg") (y #t)))

(define-public crate-glslang-0.1.1 (c (n "glslang") (v "0.1.1") (d (list (d (n "glslang-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rspirv") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jslxhqwrdyikfa6l3sangzhr3z3v24crybbfdkjzlvmfpmn7ybf") (y #t)))

(define-public crate-glslang-0.2.0 (c (n "glslang") (v "0.2.0") (d (list (d (n "glslang-sys") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rspirv") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x1lsczkzyg1wvkg1w4wh52fzz5dcqqbsfxkawhi950jd1vxy4bx") (y #t)))

(define-public crate-glslang-0.2.1 (c (n "glslang") (v "0.2.1") (d (list (d (n "glslang-sys") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rspirv") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s2j2znjjcw79n9wvrjc73zfvwhc01bbykdpgbdmb41m0nbi3h1x") (y #t)))

(define-public crate-glslang-0.2.2 (c (n "glslang") (v "0.2.2") (d (list (d (n "glslang-sys") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rspirv") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12lcl0b6jk1wvkwypz8yqx7cdlqaahs1yglf30y04xpljb0j3p2x") (y #t)))

(define-public crate-glslang-0.2.3 (c (n "glslang") (v "0.2.3") (d (list (d (n "glslang-sys") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rspirv") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m1s9n3ifz1fnzf1xi8slaa976lpdnaf7rr53q370hlvkv2bk8zz") (y #t)))

(define-public crate-glslang-0.2.4 (c (n "glslang") (v "0.2.4") (d (list (d (n "glslang-sys") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rspirv") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1v8w342p9lgm6xzysspm83kwq7p5kgfh6pyiq9i7zjhay82p6q39") (y #t)))

(define-public crate-glslang-0.3.0 (c (n "glslang") (v "0.3.0") (d (list (d (n "glslang-sys") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rspirv") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d9dmk4kcb23r4jmhywl7i7ks2p4j1lkz82c2gmiiy8n5pa8pj7r")))

(define-public crate-glslang-0.3.1 (c (n "glslang") (v "0.3.1") (d (list (d (n "glslang-sys") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rspirv") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j5vz2rpaa8f9zv90y081k57yscdx8djr29v6yk25hnlwn8n9py5")))

(define-public crate-glslang-0.3.2 (c (n "glslang") (v "0.3.2") (d (list (d (n "glslang-sys") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rspirv") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fkvlcwjfbjzbl4z87zrsnf82m69zvnb2csj0n42ygb4f607na66")))

