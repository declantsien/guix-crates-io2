(define-module (crates-io gl sl glslt_cli) #:use-module (crates-io))

(define-public crate-glslt_cli-0.5.0 (c (n "glslt_cli") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glslt") (r "^0.5.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "0jmhzg4m07pvvd7wm2bkgm8a92pn6drzl1p8vlyqaf9460ybbb8i")))

(define-public crate-glslt_cli-0.6.3 (c (n "glslt_cli") (v "0.6.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glslt") (r "^0.6.3") (f (quote ("cli"))) (d #t) (k 0)))) (h "0bgiri2qff2i39i10p1vfn5g4lx7mx4p5zi5vbhpjdlnd5hp28b2")))

(define-public crate-glslt_cli-0.6.4 (c (n "glslt_cli") (v "0.6.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glslt") (r "^0.6.4") (f (quote ("cli"))) (d #t) (k 0)))) (h "1x8wmrx2v0i8xhpzc6k0advr17wz58am7nwqmxp3viq6bcc8lm1g")))

(define-public crate-glslt_cli-0.7.1 (c (n "glslt_cli") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glslt") (r "^0.7.1") (f (quote ("cli"))) (d #t) (k 0)))) (h "0nlb64jffkyd35rfmir12bb11gnim9jhf05q7ivazfhvgdrrx1l4")))

(define-public crate-glslt_cli-0.7.0 (c (n "glslt_cli") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glslt") (r "^0.7.0") (f (quote ("cli"))) (d #t) (k 0)))) (h "09m9dw8d8b0znr45jnw7dr8w1igidpzh50gyxpr7ln8rfqf7z9ww")))

(define-public crate-glslt_cli-0.7.2 (c (n "glslt_cli") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glslt") (r "^0.7.2") (f (quote ("cli"))) (d #t) (k 0)))) (h "1mqznq8fyrmxhbl1ccgna363wdnzr82znhvjh2bwiwzwzirh6x1y")))

