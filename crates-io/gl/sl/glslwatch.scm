(define-module (crates-io gl sl glslwatch) #:use-module (crates-io))

(define-public crate-glslwatch-0.1.0 (c (n "glslwatch") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)))) (h "1cfn859m5g153idj21zq27dnvnmqqv80j87qzmbi8d8aiazcv2wm")))

(define-public crate-glslwatch-0.1.1 (c (n "glslwatch") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)))) (h "1pf4pry6r6w2zg88bf24n7xarj117qm87srcw8zvsh8qsllw8cdp")))

(define-public crate-glslwatch-0.1.2 (c (n "glslwatch") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)))) (h "1aysvlmgaq44b1cx284b6qclr5w4di7hyx61ff22hnx9xizcjb8a")))

(define-public crate-glslwatch-0.1.3 (c (n "glslwatch") (v "0.1.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)))) (h "01gxarb84h7gy3qirjsj2rmkabbr0w680jvc44baf01p8i5b212s")))

(define-public crate-glslwatch-0.1.4 (c (n "glslwatch") (v "0.1.4") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "135kspnf052ncmigl6q3hgqpv2bkganqd4cpd1x9yx74rkfp580m")))

