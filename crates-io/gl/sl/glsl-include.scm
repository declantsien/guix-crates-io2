(define-module (crates-io gl sl glsl-include) #:use-module (crates-io))

(define-public crate-glsl-include-0.1.0 (c (n "glsl-include") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0434i9yzwv9hhzr88lrk9zy7p0vqxjxv6rba3ihr0hw2dgkha52v") (f (quote (("default") ("benches" "criterion"))))))

(define-public crate-glsl-include-0.2.0 (c (n "glsl-include") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1s1z61pk9gz2622n6jsip1cbj4lmd6kir4caw24rhzzir7apdji5")))

(define-public crate-glsl-include-0.2.1 (c (n "glsl-include") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1275i6d1n7q5mdh04skxilgznjafn7d3pn3h3m1v747gxp9bk5ra")))

(define-public crate-glsl-include-0.2.2 (c (n "glsl-include") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0f3r701ck9y2qvz463ibf62jfxcgy0a8n5gkzwwfk379cz6v1wrc")))

(define-public crate-glsl-include-0.2.3 (c (n "glsl-include") (v "0.2.3") (d (list (d (n "criterion") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0v1vsvq05w8n8iiz6y2m2l1ga3h0a5dih3d15mhzs95d0sh0kh9i")))

(define-public crate-glsl-include-0.3.0 (c (n "glsl-include") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0a9h1hhwfriy7cs4rwgmxsvzns3nyb3innxbpif50k0l0333wxdh")))

(define-public crate-glsl-include-0.3.1 (c (n "glsl-include") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1djad0gknm961brhg778zrc9rd4bqqzhxphd7rab8yhycfqsz8ns")))

