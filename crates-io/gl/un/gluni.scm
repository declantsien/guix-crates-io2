(define-module (crates-io gl un gluni) #:use-module (crates-io))

(define-public crate-gluni-0.1.0 (c (n "gluni") (v "0.1.0") (d (list (d (n "posh") (r "^0.1") (d #t) (k 0)))) (h "1yvdi60l0yy05jiamamazdaq41qwcp988i7jamb6ydlyjinh9s1s") (y #t)))

(define-public crate-gluni-0.1.1 (c (n "gluni") (v "0.1.1") (h "1acqb60r0sc8kr8y4l0wp01zz7pbhmyzq36zr5kz2w6my3ijzb61")))

(define-public crate-gluni-0.1.2 (c (n "gluni") (v "0.1.2") (h "1vlqqqi46qd1x1nfii7763m3ahf13yi89bggh4zgaazgang03ky8")))

(define-public crate-gluni-0.1.3 (c (n "gluni") (v "0.1.3") (h "1isk3hbsd0dxfccv3qjw0gm60bqlkgkkra8rgzkagzpmbpjal0qa")))

(define-public crate-gluni-0.1.4 (c (n "gluni") (v "0.1.4") (h "076bb3wwxnhaf3aws3dav2593qs1rfhphbxj187gj2nil1y3y4v5")))

