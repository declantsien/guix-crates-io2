(define-module (crates-io gl ea gleam) #:use-module (crates-io))

(define-public crate-gleam-0.1.0 (c (n "gleam") (v "0.1.0") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1kwi13z0d5mz8cqzb324zc2vqvn2f7v4hvy0bflgxlzxs7lmrfam")))

(define-public crate-gleam-0.1.1 (c (n "gleam") (v "0.1.1") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1z977nmqcfh47rn3wpp312qpmv7mibpkd85924pc9wb0avh8iw4h")))

(define-public crate-gleam-0.1.2 (c (n "gleam") (v "0.1.2") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "06jlq8si9jfdm8dpg3yrndb7qfb5c16g8kpf2z7l6lbbnjmg9kc0")))

(define-public crate-gleam-0.1.3 (c (n "gleam") (v "0.1.3") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "16svd1v6z32yir4mvvsh4s554z6wxf0f7zg05hnxffcmr4v6i19b")))

(define-public crate-gleam-0.1.4 (c (n "gleam") (v "0.1.4") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "16lknrdh727qm7x6yg1a2mzpsf99v6myk17maych1rdffdj2vs0x")))

(define-public crate-gleam-0.1.5 (c (n "gleam") (v "0.1.5") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1svk8xs2nc1a4lai61i4gvd89nkhngpgmb4xmj7g8hmjxgj9562y")))

(define-public crate-gleam-0.1.6 (c (n "gleam") (v "0.1.6") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "10rnv3yamrj2qbnz3g530xn47a0z9h12mbmivc9qcsr71rm5zr27")))

(define-public crate-gleam-0.1.7 (c (n "gleam") (v "0.1.7") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1fznalzq31674f6m4xws4yy8m52bx6zmsfd7vnx41wh2xr264rqg")))

(define-public crate-gleam-0.1.9 (c (n "gleam") (v "0.1.9") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0mq78arc3982da1j6m7mwqj5j7npy1xl1nd9snr827b6m1v8r3kk")))

(define-public crate-gleam-0.1.10 (c (n "gleam") (v "0.1.10") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0bbrr1k49wkgah923x9k6wzdqin6878ipisb1iln2sha2f7xrkc0")))

(define-public crate-gleam-0.1.11 (c (n "gleam") (v "0.1.11") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "05x5zr7kyk8h5bk0ir9j3kjhm110jgy8l5z0gvpvrsnn248bg7ap")))

(define-public crate-gleam-0.1.12 (c (n "gleam") (v "0.1.12") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1ggnkk1033v1vzkr577r8z5zlqkyz5mc2z4225405iynp25k1vv4")))

(define-public crate-gleam-0.1.13 (c (n "gleam") (v "0.1.13") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0r2isp1bqrdh3sp7hj84a0zad8p7mksx0ks6ib0m0rrrg2s6smld")))

(define-public crate-gleam-0.1.15 (c (n "gleam") (v "0.1.15") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "^0.1.0") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.8") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0i6yldp3gc3m191ab3ws40bwlsm18dpw3sy3r4znww5q9f83w5wh")))

(define-public crate-gleam-0.1.16 (c (n "gleam") (v "0.1.16") (d (list (d (n "gl_common") (r "^0.1") (d #t) (k 0)) (d (n "gl_generator") (r "^0.1.0") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.8") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "04swzayff485rr6mnxh2rpaq10zsh4i0r30aj8l157ifds1r6yzk")))

(define-public crate-gleam-0.1.18 (c (n "gleam") (v "0.1.18") (d (list (d (n "gl_common") (r "^0.1") (d #t) (k 0)) (d (n "gl_generator") (r "^0.1.0") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.8") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0v3si2h5nblz87h20msgcrkdfhr9brwwgdhsi6zh04vdrw9libyn")))

(define-public crate-gleam-0.1.19 (c (n "gleam") (v "0.1.19") (d (list (d (n "gl_generator") (r "^0.2.0") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.8") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fs6xfxxvm5hn534ssn4nx8q3fqajjkw6x6skh70v887q3m24nfi")))

(define-public crate-gleam-0.1.20 (c (n "gleam") (v "0.1.20") (d (list (d (n "gl_generator") (r "^0.4.0") (d #t) (k 1)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)))) (h "1dd7hp4346vmb2b9kr7f6ydsvsq7fydjfmh7xdni9lq6hrcj4ci0")))

(define-public crate-gleam-0.2.0 (c (n "gleam") (v "0.2.0") (d (list (d (n "gl_generator") (r "^0.4.0") (d #t) (k 1)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)))) (h "0rbidck8a66awd59x1rfn2ippzv0a9m8srd07k5kkhk33acxlhkp")))

(define-public crate-gleam-0.2.2 (c (n "gleam") (v "0.2.2") (d (list (d (n "gl_generator") (r "^0.4.0") (d #t) (k 1)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)))) (h "1ipyhppvybzzw9xnn3fsnncc5sm8vh9sz67kzysyamwxpk05pws2")))

(define-public crate-gleam-0.2.3 (c (n "gleam") (v "0.2.3") (d (list (d (n "gl_generator") (r "^0.4.0") (d #t) (k 1)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)))) (h "1zh65rp6hlnz2cm5mz5v87jhzxn55a4bc5d1klfcg89hz6rbnxys")))

(define-public crate-gleam-0.2.5 (c (n "gleam") (v "0.2.5") (d (list (d (n "gl_generator") (r "^0.4.0") (d #t) (k 1)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)))) (h "1sfny325md5r5dnvcay3382c7w7a68p0zbxjg74lyj7dlcn4fg5w")))

(define-public crate-gleam-0.2.6 (c (n "gleam") (v "0.2.6") (d (list (d (n "gl_generator") (r "^0.4.0") (d #t) (k 1)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)))) (h "0rnqff2dfmkc6cx6g2gf73p7rfy2zl2xp154mhhil6hrw44im6m0")))

(define-public crate-gleam-0.2.8 (c (n "gleam") (v "0.2.8") (d (list (d (n "gl_generator") (r "^0.4.0") (d #t) (k 1)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)))) (h "0ir7qznf4mbaiin8gqi8s9ihqa3g66v1bzffyiskz4aadgaw49x5")))

(define-public crate-gleam-0.2.10 (c (n "gleam") (v "0.2.10") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "1ly0jzmnijjasv24i1pnkcq9al79wz9jg5s3x3y49npiblnvz339")))

(define-public crate-gleam-0.2.11 (c (n "gleam") (v "0.2.11") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "16xkfq9f8yl6r7vxbvjb9is2x9qmgidhk1ayijvgw0f3j308bb58")))

(define-public crate-gleam-0.2.12 (c (n "gleam") (v "0.2.12") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "1lba31knf2hxpaxk6mbwa8ayvwck95j60c4xjh2nchdjc6dm6k43")))

(define-public crate-gleam-0.2.13 (c (n "gleam") (v "0.2.13") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "085y8g7ls4rvw1jk3vimg8356v6vz0nlivgdw8n4gyrq2jyk8zyb")))

(define-public crate-gleam-0.2.14 (c (n "gleam") (v "0.2.14") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "1zqwjy9igg3mkdc391ikm05rcndq4aywii0l9ggxzj8n92s2awqf")))

(define-public crate-gleam-0.2.15 (c (n "gleam") (v "0.2.15") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "1fphnqx655azvbmxh25wjv59z3qnm1xa62wm597h5pa39468f7si")))

(define-public crate-gleam-0.2.16 (c (n "gleam") (v "0.2.16") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "13k39zfjwm2qgxf9kvjr2r7n5kwwfk9px0zb9kf6k6ncr0ji80n8")))

(define-public crate-gleam-0.2.17 (c (n "gleam") (v "0.2.17") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "0hm6pyb5554701ilpilbahwvdqcpfmnmrw923sp8vaq9x8iz0inx")))

(define-public crate-gleam-0.2.18 (c (n "gleam") (v "0.2.18") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "13r5hg01lh7y4yxph7g0kcpl7ikbhsb759jw2f2b8zng1iqia37v")))

(define-public crate-gleam-0.2.19 (c (n "gleam") (v "0.2.19") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "0v57cxznyccic8igmd2snz5n7qz4hj95pafs7c5ns0daiclhzizw")))

(define-public crate-gleam-0.2.20 (c (n "gleam") (v "0.2.20") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "0nmcrf2r6xza20l5yrg0vvk9hw287i5z3chdac760jr6lnr678qk")))

(define-public crate-gleam-0.2.21 (c (n "gleam") (v "0.2.21") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "0qchh7sj8hzn6w4ylpiq0rpdrznbgdw3gp0gsyb8fsifjyjz6h9c")))

(define-public crate-gleam-0.2.22 (c (n "gleam") (v "0.2.22") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)))) (h "0rymkbfhwm2wp1m67lw29h9795xqybz7mgkwdrf9b6766imzr6g2")))

(define-public crate-gleam-0.2.24 (c (n "gleam") (v "0.2.24") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "16rm407034ir53rwai1ypfqif7cxkj16gvcrzs6y8hgq3n56qkdh")))

(define-public crate-gleam-0.2.25 (c (n "gleam") (v "0.2.25") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "03g0s0fxn6rchqz4bxdnzslxn0xv5p69vawybwldj047d69jyr2r")))

(define-public crate-gleam-0.2.26 (c (n "gleam") (v "0.2.26") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1bilrym5r8kb1sqpx60xwdq4y3dr4hzbcw8ifgxyaxrn6g6czigc")))

(define-public crate-gleam-0.2.27 (c (n "gleam") (v "0.2.27") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "15wff4pjbldqgn9qjpgi1vvsvqi7ihhq22pbm7848mkj40kbgb2k")))

(define-public crate-gleam-0.2.28 (c (n "gleam") (v "0.2.28") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1n126g46p5k75dyni83ajz6js1x9jbjhfq1kci29iz75q22hxcdh")))

(define-public crate-gleam-0.2.29 (c (n "gleam") (v "0.2.29") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "14y2yny047kv6r3qcvz8dsagb7bz06z12a5ry0ixksdx54i410qv")))

(define-public crate-gleam-0.2.30 (c (n "gleam") (v "0.2.30") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "198xwprl3h6x7wj6m6ma34y82bsbmpz080wahvwcqsd9g8827w3a")))

(define-public crate-gleam-0.2.31 (c (n "gleam") (v "0.2.31") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "12kagnw8v4hhaci5kxkg8fqw6flhx3c5sbhxlxkpg6qnvvnj6c3s")))

(define-public crate-gleam-0.2.32 (c (n "gleam") (v "0.2.32") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "009d8rgxygh7sjpzl5kk7jklkqzvgs76gb5bqn0a0a6mg3jy144m")))

(define-public crate-gleam-0.3.0 (c (n "gleam") (v "0.3.0") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0vpzyxjy7lmry3yj2hz62l6af3d4cnss2xwq82vf3hyzycc1w8vy")))

(define-public crate-gleam-0.4.0 (c (n "gleam") (v "0.4.0") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1nwgba0sphw3cxiv8xvn8bni2hn44lyxs8m9slqac8dhsljdmcws")))

(define-public crate-gleam-0.4.1 (c (n "gleam") (v "0.4.1") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0cn30z1r4bv8bpfixxdh0830fa92mvsjj4xk8zkjv39m19m3jn19")))

(define-public crate-gleam-0.4.2 (c (n "gleam") (v "0.4.2") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "01g04idr3kw5gmf6s8fqkb0j0pbbh4n4y0h5iilapiagprfnacij")))

(define-public crate-gleam-0.4.3 (c (n "gleam") (v "0.4.3") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0id34wwckx1mhcj6p410jxfqfdk82z9pfc0byp8vwsif5acgyilw")))

(define-public crate-gleam-0.4.4 (c (n "gleam") (v "0.4.4") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1vlhnhkndcv9jbvnhhnbmwxl0alm0prfxpmc3zhdav35wlp37b33")))

(define-public crate-gleam-0.4.5 (c (n "gleam") (v "0.4.5") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "102kjd1l03bskd9gy9m53mxqmwbiv69034zfz03lbxfpljk48sd8")))

(define-public crate-gleam-0.4.6 (c (n "gleam") (v "0.4.6") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "118av6819r5z2xqqd7554a1hh1vcwn7smlp2b6a2yj410j5cd1fr")))

(define-public crate-gleam-0.4.7 (c (n "gleam") (v "0.4.7") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0qf39rwxmrrw3b4is7prgch2zl3wrkyl9jqjdispgv8lyh2f8zli")))

(define-public crate-gleam-0.4.8 (c (n "gleam") (v "0.4.8") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "07myjn9d6jlcrpgmq3nj047wp9ghfjx3zgvc0ap3xa62y10p325z")))

(define-public crate-gleam-0.4.9 (c (n "gleam") (v "0.4.9") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "05fh5cnnpvfx1559i7pwa7a1bnl4ibj62y4jw7b6dbnqfksljbvq")))

(define-public crate-gleam-0.4.10 (c (n "gleam") (v "0.4.10") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0vszkxbkr2bm5kzcjmajj2n1vyl3hp9hk20gmsd16q54yvwz226q")))

(define-public crate-gleam-0.4.11 (c (n "gleam") (v "0.4.11") (d (list (d (n "gl_generator") (r "^0.6.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "03yh7j2v9sk2nwrf293gg11c7vmzd8rg4fdhxzv47xbk4ffmq7ig")))

(define-public crate-gleam-0.4.12 (c (n "gleam") (v "0.4.12") (d (list (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1m376mav0vf5zh8hdp8x8js94jfn3slk3sg5n51zgh28hg346ff3")))

(define-public crate-gleam-0.4.13 (c (n "gleam") (v "0.4.13") (d (list (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0cgff16hq6hrdpk1c4mwhk6s8fa7dsmqs83w9nw7j8i5pk7bm792")))

(define-public crate-gleam-0.4.14 (c (n "gleam") (v "0.4.14") (d (list (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0nymk25mxkg95ygvzji2345w374g5yl1ds98hws41gy0dcksmmlb")))

(define-public crate-gleam-0.4.15 (c (n "gleam") (v "0.4.15") (d (list (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "16gfi47al8ql5nb0az14iaha2vp02hgh17bmlam2p4rlccri7xnz")))

(define-public crate-gleam-0.4.16 (c (n "gleam") (v "0.4.16") (d (list (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0nhw2b635kfk0qrijsj1f78fw2b75rydj424awlnc8mvi2c09zbg")))

(define-public crate-gleam-0.4.17 (c (n "gleam") (v "0.4.17") (d (list (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1q2yghnqzaws604diy6kmxxfr3axcbi6jdqp8wgn9p2b01492kvg")))

(define-public crate-gleam-0.4.18 (c (n "gleam") (v "0.4.18") (d (list (d (n "gl_generator") (r "^0.8") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1pc80skkhzav4z10wr43wv77pz1knxxisqarw6cv56fr31wdyzs3")))

(define-public crate-gleam-0.4.19 (c (n "gleam") (v "0.4.19") (d (list (d (n "gl_generator") (r "^0.8") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1sdlm1bp5hkzgakabkysq81rii8lmm1asi6c9rjvq8lmhycncxag")))

(define-public crate-gleam-0.4.20 (c (n "gleam") (v "0.4.20") (d (list (d (n "gl_generator") (r "^0.8") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "05w07d69h3sm81n568p19r7xmi3k8v20pkjxnmypp7xykf6q374m")))

(define-public crate-gleam-0.4.21 (c (n "gleam") (v "0.4.21") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1vas8gfr1srlmhw7f05aznj863gdrvp7j0hg8mbvf8cympp9vvrj")))

(define-public crate-gleam-0.4.23 (c (n "gleam") (v "0.4.23") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "1qhbx615zhsskldkiggsz4lpjm6yfrwnvkfhgl566dqq6pssgqvn")))

(define-public crate-gleam-0.4.24 (c (n "gleam") (v "0.4.24") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "1jgfa2jirq8i1ji7r6nj34zxpb1s5rjpi0qa57rw1zicbbk9g6cd")))

(define-public crate-gleam-0.4.25 (c (n "gleam") (v "0.4.25") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "0278akdqrwpy6f06dqrglk3v0p7agy5r4aisb973rmbklzv5p6j4")))

(define-public crate-gleam-0.4.26 (c (n "gleam") (v "0.4.26") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "1iqfjnhhzwgi1cv5sdn9aigdxpa59zx356d0j37zvf8jrxps8aqk")))

(define-public crate-gleam-0.4.28 (c (n "gleam") (v "0.4.28") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "09p61cykgxq7nmqbkwyly7f51x8mak9yl7cvav4kdcw3h852vyqa")))

(define-public crate-gleam-0.4.29 (c (n "gleam") (v "0.4.29") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "10gd6qamniwznnzgin4znw4084wm0sqmilkxzicv80i9vijyykdp")))

(define-public crate-gleam-0.4.30 (c (n "gleam") (v "0.4.30") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "17ycaph39r905wypnadbhhc7pa73ndkkxqn253bms4la6y9lwn4b")))

(define-public crate-gleam-0.4.31 (c (n "gleam") (v "0.4.31") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "1zk0m8pp6vfvy4qdg3k9b8pvd6vj701rd992i3rpah8vy11g3skk")))

(define-public crate-gleam-0.4.32 (c (n "gleam") (v "0.4.32") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "0gd6p4z9mlkczzy5vl6wz7s4p0nxagqvdlwganyjvdrky1wk8dkh")))

(define-public crate-gleam-0.4.33 (c (n "gleam") (v "0.4.33") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "02r9jddhxxa6mfl3ayjdqifij5k3jcxry29fh13f1jwgijbxdw66")))

(define-public crate-gleam-0.4.34 (c (n "gleam") (v "0.4.34") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "18plxn01p1pwfsvi4p4cx7glh2mfisivc23qds4g7kfyrb6y5rdl")))

(define-public crate-gleam-0.5.0 (c (n "gleam") (v "0.5.0") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "101kigdim6b785117y5qvh4xyrnw5j9h49y8sbj6pds7kr2kjyz4")))

(define-public crate-gleam-0.5.1 (c (n "gleam") (v "0.5.1") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "0iafb81am0y4361vlkxva26gi5pbnf524k0q5wvxs8qayky97dqj")))

(define-public crate-gleam-0.6.0 (c (n "gleam") (v "0.6.0") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "1hk6xlcz3h7rkf5bi6mdp2nmrly6qfp9n773va7ri5r5h6nffh8d")))

(define-public crate-gleam-0.6.1 (c (n "gleam") (v "0.6.1") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "0607i4zhydyz7l2v3r7c21zmyvim0c35cf3r1lsbl8nwvnkw5y3h")))

(define-public crate-gleam-0.6.2 (c (n "gleam") (v "0.6.2") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "0bf9pmskq0vr82nv176m0zzdn6q01i4xjxphljygc8wjshwnbfi4")))

(define-public crate-gleam-0.6.3 (c (n "gleam") (v "0.6.3") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "0i3gjh2ynxd7wm9kspsh0bdmvj35bh1imag1hgqscg1rq0n9aq12")))

(define-public crate-gleam-0.6.4 (c (n "gleam") (v "0.6.4") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "0zylvkps9mz03lajndlfcwlimpv4wv6djkr1farr5aald0fr1a6n")))

(define-public crate-gleam-0.6.5 (c (n "gleam") (v "0.6.5") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "1p85nn6247wvn4s6in6qc3bi9js0fx0ah37a95ghykhrab4bwb4n")))

(define-public crate-gleam-0.6.6 (c (n "gleam") (v "0.6.6") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)))) (h "0g7nnwdv9hxgwvj2y1b3c9pr39pnf3kxg70n8v7pb1jlw5wzilpm")))

(define-public crate-gleam-0.6.7 (c (n "gleam") (v "0.6.7") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)))) (h "06hlfia3v8r846gql11pcq1n63p5dsi7dsnslxvmp7v13pii1y5y")))

(define-public crate-gleam-0.6.8 (c (n "gleam") (v "0.6.8") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)))) (h "1nkpk24xf0pq8976zssgpf4wrvncya6bjnl9qxcy7bj2ayqzaisb")))

(define-public crate-gleam-0.6.9 (c (n "gleam") (v "0.6.9") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)))) (h "17vrmf4343yjp6hradzis2wrkf8yn3sbqkvmyl9l13v281nk57in")))

(define-public crate-gleam-0.6.11 (c (n "gleam") (v "0.6.11") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)))) (h "0xy2x7xrk2kbzxzpd15zkm0n0dnrzxm43jf95726gv189431hc5r")))

(define-public crate-gleam-0.6.12 (c (n "gleam") (v "0.6.12") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)))) (h "13n3w9qp5qqhxwzq5f0sjr5aq8dr8j8bgpxympgf2c6j26k9qlgi")))

(define-public crate-gleam-0.6.13 (c (n "gleam") (v "0.6.13") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)))) (h "1zkiqz6bf675im80q96jslcb398npnn1yycmh553ig7v4hb5kmzk")))

(define-public crate-gleam-0.6.14 (c (n "gleam") (v "0.6.14") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)))) (h "0f8kvs80z4zrrv1jx5gssyfy02hgkkg5apa1pyqmvwiwvm1sig0p")))

(define-public crate-gleam-0.6.15 (c (n "gleam") (v "0.6.15") (d (list (d (n "gl_generator") (r "^0.11") (d #t) (k 1)))) (h "1k7y76ynwc0f91kd1153lsb6khgh9qawy98nmiyls0i18cxgmva3")))

(define-public crate-gleam-0.6.16 (c (n "gleam") (v "0.6.16") (d (list (d (n "gl_generator") (r "^0.11") (d #t) (k 1)))) (h "1mbm9yqxbs5nrnnsizsbjlyzbh0v9903iby0gixipq85j14nkfrr")))

(define-public crate-gleam-0.6.17 (c (n "gleam") (v "0.6.17") (d (list (d (n "gl_generator") (r "^0.11") (d #t) (k 1)))) (h "1i169808j5mb6zdv8wkd9y0zg115g9bd33k31nngyhz0fj4gsikz")))

(define-public crate-gleam-0.6.18 (c (n "gleam") (v "0.6.18") (d (list (d (n "gl_generator") (r "^0.11") (d #t) (k 1)))) (h "0wbg2ypc62w42y87yyjwq0izx8dhxf4qmnwzp2p5vlyclfsmb968")))

(define-public crate-gleam-0.6.19 (c (n "gleam") (v "0.6.19") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "1iazvk3kvw3620gm6x8hy2x1lz51k04acl78cr3ppryhk5y0vqfa")))

(define-public crate-gleam-0.6.20 (c (n "gleam") (v "0.6.20") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0ma4n66amyqn4wy73ymnpsxvqf5f1zlp5mqqzj5x5xnpl0wpix6g") (y #t)))

(define-public crate-gleam-0.7.0 (c (n "gleam") (v "0.7.0") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0jf8fimzj9rx0cm7yd96r7skfksg3f0zn0b4a403zbhifjxgk94y")))

(define-public crate-gleam-0.8.0 (c (n "gleam") (v "0.8.0") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0s51jrsfsshxj7w801dg2zw1bb785m2vs1xj9d9abq3r63q8x893")))

(define-public crate-gleam-0.9.2 (c (n "gleam") (v "0.9.2") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "1fdi7zm86f7v5k18c6l2yvn46z9qn99ab14g2w3yv0b1di71yb9k")))

(define-public crate-gleam-0.10.0 (c (n "gleam") (v "0.10.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "11hrlr59ir0h7q92jz1dpwp49ih5p246c09iml5p5zwg8rvgvcbp")))

(define-public crate-gleam-0.11.0 (c (n "gleam") (v "0.11.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "04m1p80wnllcrhq9qslbi70za7cplnxkrd00ak0mgd1an5dkp8fr")))

(define-public crate-gleam-0.12.0 (c (n "gleam") (v "0.12.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "07npa9m3x9h9rvbcf9avwwvykavdahpnz0c21w7rc5hcn2q27l58")))

(define-public crate-gleam-0.12.1 (c (n "gleam") (v "0.12.1") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "0dqdrrym4nif18dkqipjbmr4afgr7ghmbb55h0djcfkdvywzbpiz")))

(define-public crate-gleam-0.12.2 (c (n "gleam") (v "0.12.2") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "0jfbmbblpvgd831r71h16n3j1mlnw70137y5wbpk621zgq51pnm6")))

(define-public crate-gleam-0.13.0 (c (n "gleam") (v "0.13.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "199zf3m4h1b3jx73bzj1aw8x9jbqiya0xb15vx5ll5w0dixp0gg1")))

(define-public crate-gleam-0.13.1 (c (n "gleam") (v "0.13.1") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "0ykj2ppzphm447dma4vmb0ycrm3f0880kippiaam88rqkmc40b4r")))

(define-public crate-gleam-0.13.2 (c (n "gleam") (v "0.13.2") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "014p3hc8y2daldpjixbrwdv686jgd8p58j664sprlz2vcnzlmg9f") (y #t)))

(define-public crate-gleam-0.14.0 (c (n "gleam") (v "0.14.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "0s1zv3lpl7dz05bbas99bz14cn0c7agjby00psrhrfrlrpjffbzm")))

(define-public crate-gleam-0.15.0 (c (n "gleam") (v "0.15.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "03vzi5g3adyd04rhx0q5516xhxiqr232xpl596zhks5n5cglhwq1")))

