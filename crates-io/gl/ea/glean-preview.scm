(define-module (crates-io gl ea glean-preview) #:use-module (crates-io))

(define-public crate-glean-preview-0.0.0 (c (n "glean-preview") (v "0.0.0") (h "1b3a09m4v6kcqlcjpxrj5c6bsrdkirzxx52awghkbrnfrxvhrzz9")))

(define-public crate-glean-preview-0.0.1 (c (n "glean-preview") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.7.1") (f (quote ("termcolor" "atty" "humantime"))) (k 2)) (d (n "ffi-support") (r "^0.3.5") (d #t) (k 0)) (d (n "glean-core") (r "^22.0.0") (d #t) (k 0)) (d (n "glean-ffi") (r "^22.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "00z1npj1s9ds945gj2inycyij18l5a88lnpmp6ai058fkw18frhb")))

(define-public crate-glean-preview-0.0.2 (c (n "glean-preview") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.7.1") (f (quote ("termcolor" "atty" "humantime"))) (k 2)) (d (n "glean-core") (r "^22.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "0d0l69838vmwfvgjxhwkysrx8rlzmxwh44sz9sqq63s27kxwsl6c")))

(define-public crate-glean-preview-0.0.3 (c (n "glean-preview") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.7.1") (f (quote ("termcolor" "atty" "humantime"))) (k 2)) (d (n "glean-core") (r "^22.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "0hk4phl3307gib7c9vqigj1yggr7ql0rvyh3zgz27hc3mlgphx70")))

(define-public crate-glean-preview-0.0.4 (c (n "glean-preview") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.7.1") (f (quote ("termcolor" "atty" "humantime"))) (k 2)) (d (n "glean-core") (r "^22.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "0hi3xm0a7qd58fbxjgh7xb5bx1b3vi93ajg1aq06h1wki15z6fyb")))

(define-public crate-glean-preview-0.0.5 (c (n "glean-preview") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.7.1") (f (quote ("termcolor" "atty" "humantime"))) (k 2)) (d (n "glean-core") (r "^24.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0b2fjq25g8lbk4ijxpaplmm612j5k5336b9plnhnkwwm4gs1h8hq")))

