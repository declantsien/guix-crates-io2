(define-module (crates-io gl ea gleam_finder) #:use-module (crates-io))

(define-public crate-gleam_finder-0.1.0 (c (n "gleam_finder") (v "0.1.0") (d (list (d (n "minreq") (r "^1.4.1") (f (quote ("https"))) (d #t) (k 0)))) (h "0zkykycifcaaxv0m27xdzzhrkiri320nfbl9iqfjair95j2vqdvy") (y #t)))

(define-public crate-gleam_finder-0.1.1 (c (n "gleam_finder") (v "0.1.1") (d (list (d (n "minreq") (r "^1.4.1") (f (quote ("https"))) (d #t) (k 0)))) (h "1a0n0cvqf2a59kf2sg1nd9mpz3gaflpv052cjgpdi1w5y0mbyrlk") (y #t)))

(define-public crate-gleam_finder-0.2.1 (c (n "gleam_finder") (v "0.2.1") (d (list (d (n "minreq") (r "^1.4.1") (f (quote ("https"))) (d #t) (k 0)))) (h "0pr1id9hd0p0zdzgz59vlbdkhn1cwxaqcwf4xx51gj8zvg8f6sdm") (y #t)))

(define-public crate-gleam_finder-0.2.2 (c (n "gleam_finder") (v "0.2.2") (d (list (d (n "minreq") (r "^1.4.1") (f (quote ("https"))) (d #t) (k 0)))) (h "0902km9v50yzbdcdpdmzayx887wwj885iqlgs1f8gm0v459v6a0r") (y #t)))

(define-public crate-gleam_finder-0.2.3 (c (n "gleam_finder") (v "0.2.3") (d (list (d (n "minreq") (r "^1.4.1") (f (quote ("https"))) (d #t) (k 0)))) (h "06j7cj4fy4v6ax7fwf3y7k819h755zbnxn5sj3cgf1pv04bp4m0v") (y #t)))

(define-public crate-gleam_finder-0.2.4 (c (n "gleam_finder") (v "0.2.4") (d (list (d (n "minreq") (r "^1.4.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "176dbsc84q96zyq01nrmvcvfvwrgc4yi4sd5yv9sqaqdkhix16a2") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.2.5 (c (n "gleam_finder") (v "0.2.5") (d (list (d (n "minreq") (r "^1.4.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xg11183z7rn0p0y7wz16y3jc39saj9dd8rjijmvf4fnj4nmqgdc") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.2.6 (c (n "gleam_finder") (v "0.2.6") (d (list (d (n "minreq") (r "^2.0.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n57bnr56ajz05mwrm8zfjpgixy3m4s0xm4w1q3ynv8q2fdi29y0") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.2.7 (c (n "gleam_finder") (v "0.2.7") (d (list (d (n "minreq") (r "^2.0.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "047jzz9vcryhm2xddh0f6hdrymq5md5jhfmdvh5gdvxkj6r4y83s") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.2.8 (c (n "gleam_finder") (v "0.2.8") (d (list (d (n "minreq") (r "^2.0.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zinq8pzblvygr0190p7g10h3k8l119jg8yj9i2fqz50y3cr3d2b") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.2.9 (c (n "gleam_finder") (v "0.2.9") (d (list (d (n "minreq") (r "^2.0.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "024d73zskvd3iaasmcxbk5m2m9mmmq8i5gf1srv3jpfnw6z5jrwq") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.3.0 (c (n "gleam_finder") (v "0.3.0") (d (list (d (n "minreq") (r "^2.1.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ai81adx4qgwqrcdmq4fv0vrnsa3zlz95lk0d61llgd55981bg05") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.3.1 (c (n "gleam_finder") (v "0.3.1") (d (list (d (n "minreq") (r "^2.1.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "115n5c3ivfm7iyajfwrk2iyinlpmpvwcn4yqd0j8vcbsg4x5mvqk") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.3.2 (c (n "gleam_finder") (v "0.3.2") (d (list (d (n "minreq") (r "^2.1.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mkgqfw1fw6lgvnm32y5rks69g2xm2cprbwvamcgyi6jn8malaaz") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.4.0 (c (n "gleam_finder") (v "0.4.0") (d (list (d (n "minreq") (r "^2.1.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dvgxb9mqfdah5g9lx6myh6zb69s8wkk6djpy0ky4847ygnjx71n") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.4.2 (c (n "gleam_finder") (v "0.4.2") (d (list (d (n "minreq") (r "^2.1.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "string-tools") (r "^0.1.0") (d #t) (k 0)))) (h "13gzw0m928263mn28vazr9a25icx7w2sw68i6k3xk8aradgfnyp1") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.5.0 (c (n "gleam_finder") (v "0.5.0") (d (list (d (n "minreq") (r "^2.1.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "string-tools") (r "^0.1.0") (d #t) (k 0)))) (h "0aqpnvgyala57jn7z2ydfgxia446jln2mbr4dbyyqijxzkifp363") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.5.1 (c (n "gleam_finder") (v "0.5.1") (d (list (d (n "minreq") (r "^2.1.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "string-tools") (r "^0.1.0") (d #t) (k 0)))) (h "1pkxl1hnnv4xnq962j71jw18v44m2bjzb7ixczzwygpsx2lbqlf5") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.5.2 (c (n "gleam_finder") (v "0.5.2") (d (list (d (n "minreq") (r "^2.1.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "string-tools") (r "^0.1.0") (d #t) (k 0)))) (h "03ak296xg7k36kxnddfsa9b03jhavvqgqf72mahvss9nw4njpa3z") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.5.3 (c (n "gleam_finder") (v "0.5.3") (d (list (d (n "minreq") (r "^2.1.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "string-tools") (r "^0.1.0") (d #t) (k 0)))) (h "0vii4jpzwqb7kszrgzfwby7xxdc7wd9pm25bpw4gc3qh02f934r8") (f (quote (("serde-support" "serde")))) (y #t)))

(define-public crate-gleam_finder-0.0.0-do-not-use (c (n "gleam_finder") (v "0.0.0-do-not-use") (h "1cbs313cb0zkdj9pnmpgwpfj48dcycazgnbqlr3dwgxk1x5nbdbv")))

