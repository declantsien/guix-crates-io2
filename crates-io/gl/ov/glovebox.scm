(define-module (crates-io gl ov glovebox) #:use-module (crates-io))

(define-public crate-glovebox-0.0.1 (c (n "glovebox") (v "0.0.1") (h "0isvxmvsrsasw8mdnkyz2s2m8s3qh9lwbxhkp8cm7jixhs053vaw")))

(define-public crate-glovebox-0.0.2 (c (n "glovebox") (v "0.0.2") (h "0bq5zhdphy629im12b9bqs59cqg7a4nkclqa4cijg006b9a0nqsh")))

