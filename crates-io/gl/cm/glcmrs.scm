(define-module (crates-io gl cm glcmrs) #:use-module (crates-io))

(define-public crate-glcmrs-0.1.0 (c (n "glcmrs") (v "0.1.0") (d (list (d (n "blas-src") (r "^0.9.0") (f (quote ("openblas"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("blas" "rayon"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "numpy") (r "^0.20.0") (d #t) (k 0)) (d (n "openblas-src") (r "^0.10") (f (quote ("cblas" "system"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (d #t) (k 0)))) (h "1wr020idxr77ahnf44cqrr2f8ig5bm1ip620ajsc44ynzzjgsn2r")))

