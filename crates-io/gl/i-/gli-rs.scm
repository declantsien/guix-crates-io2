(define-module (crates-io gl i- gli-rs) #:use-module (crates-io))

(define-public crate-gli-rs-0.2.0 (c (n "gli-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.48.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.30") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1k167h06r318npmdswhpgscj99v21q3lm95d73a6iqysyp7nk1jc") (f (quote (("bindings" "bindgen"))))))

(define-public crate-gli-rs-0.2.1 (c (n "gli-rs") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.48.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.31") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0mcbpvg1ir2b3v77lvmqc7qhkl0jlkjbiaa0ir9piprrj1dh3gxq") (f (quote (("bindings" "bindgen"))))))

(define-public crate-gli-rs-0.3.0 (c (n "gli-rs") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.49.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.31") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0zyxkg1gjh4ljn2a5jbahig1grlwqgah89agc85dlkgrw8rmsrpn") (f (quote (("bindings" "bindgen"))))))

(define-public crate-gli-rs-0.3.1 (c (n "gli-rs") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.49.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.31") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1liilvcdac04bngcdbpwmhjka6ygc30wfwg2sn0n73zd25xdq11n") (f (quote (("rc_debug") ("bindings" "bindgen"))))))

(define-public crate-gli-rs-0.3.2 (c (n "gli-rs") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.49.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.31") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1rvfw3iang5da4fly2cqpc4cahlksccjgbabx2sjg3v98j10cc76") (f (quote (("rc_debug") ("bindings" "bindgen"))))))

(define-public crate-gli-rs-0.4.0 (c (n "gli-rs") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.53.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "19daz732129vlb0p6w3zywp7pqmqw51q40ndk4m5gpdq93qscbdi") (f (quote (("rc_debug") ("bindings" "bindgen"))))))

