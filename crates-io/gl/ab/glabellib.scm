(define-module (crates-io gl ab glabellib) #:use-module (crates-io))

(define-public crate-glabellib-0.0.1 (c (n "glabellib") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hubcaps") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1ccrwsqipzlwnw997mwl850ks6qzld1vzizbc0qw0iwzqmq2ygn9")))

(define-public crate-glabellib-0.0.3 (c (n "glabellib") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hubcaps") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "196lq9212kvxfkn59nxrdhxx3pw9ji6m1qf8ym012sl2gijslk61")))

(define-public crate-glabellib-0.0.4 (c (n "glabellib") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hubcaps") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0m6vmr9liww6h3xgwwkqyhnzq38l8773l4725h0xml3d452fijl5")))

(define-public crate-glabellib-0.0.5 (c (n "glabellib") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hubcaps") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1282b9559l4ync8qm5zjs9vsg3y8wdll969b1xpp9ahs98h38nxv")))

(define-public crate-glabellib-0.0.6 (c (n "glabellib") (v "0.0.6") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hubcaps") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "09h0fk0l6fb5ckjjkmrivyc8nzm1l71fmygkih74fq19z31riwrx")))

