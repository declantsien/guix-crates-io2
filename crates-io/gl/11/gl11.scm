(define-module (crates-io gl #{11}# gl11) #:use-module (crates-io))

(define-public crate-gl11-0.1.0 (c (n "gl11") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "glfw") (r "^0.51") (d #t) (k 2)))) (h "1072gs9i1mcvq7rxprvzkrfb4js6idvn1469q9zvf7kmgm9h8whm")))

(define-public crate-gl11-0.1.1 (c (n "gl11") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "glfw") (r "^0.51") (d #t) (k 2)))) (h "0dni2innvkwrq13q5n8l9sd8im2g6s2c3ppkhvm05x0947xbmhsi")))

