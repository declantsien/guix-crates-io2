(define-module (crates-io gl -a gl-abstractions) #:use-module (crates-io))

(define-public crate-gl-abstractions-0.1.0 (c (n "gl-abstractions") (v "0.1.0") (d (list (d (n "glad-sys") (r "^0.0.4") (o #t) (d #t) (k 0)) (d (n "glfw-sys") (r "^3.2.2") (o #t) (d #t) (k 0)))) (h "10w1kw2wqr9h4mfk1jl2f7lkdqfgcv0k1a7ph2y14l56g0whg2gf") (f (quote (("glfw" "glfw-sys") ("glad" "glad-sys") ("full" "glad-sys" "glfw-sys") ("default")))) (y #t)))

