(define-module (crates-io gl as glasses) #:use-module (crates-io))

(define-public crate-glasses-0.1.0 (c (n "glasses") (v "0.1.0") (h "0fn85ss8hjpk0gqxxv5s18737g1pxpip2dgzk9sxcc5a8i7rhyax") (f (quote (("default"))))))

(define-public crate-glasses-0.1.1 (c (n "glasses") (v "0.1.1") (h "1kjy664y2h7vwnp8jvbqnrlraigvjk0fa3jlx56kyhayfmsvfm29") (f (quote (("default"))))))

