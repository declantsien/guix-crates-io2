(define-module (crates-io gl as glassy) #:use-module (crates-io))

(define-public crate-glassy-0.0.1 (c (n "glassy") (v "0.0.1") (h "03raqbpqi8zn0lf43hyd880jzb5y7wdhiw865c5m7jkzdgcw00r0") (r "1.70.0")))

(define-public crate-glassy-0.0.2 (c (n "glassy") (v "0.0.2") (h "0krbwg9gya0vnkswsx9dis9w4085flr3pl7i7nzaz38rsrmqd1ar") (r "1.70.0")))

(define-public crate-glassy-0.0.3 (c (n "glassy") (v "0.0.3") (h "0awxcixk0q36lcpxfdybjp381ycac31lnfiq69vm4bdzvl2330wl") (r "1.70.0")))

