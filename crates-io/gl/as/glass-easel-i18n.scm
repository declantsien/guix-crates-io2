(define-module (crates-io gl as glass-easel-i18n) #:use-module (crates-io))

(define-public crate-glass-easel-i18n-0.4.0 (c (n "glass-easel-i18n") (v "0.4.0") (d (list (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "0jxyz6ijpfqcrmcpi1l8pj4jvi7m1xib4bhp3sinrmkdk8jdycml")))

(define-public crate-glass-easel-i18n-0.5.0 (c (n "glass-easel-i18n") (v "0.5.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glass-easel-template-compiler") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "1v9r8lwxpf1g30hcjrsw32ikgk169881v9r4m13mf9sprh28wz0x")))

(define-public crate-glass-easel-i18n-0.5.1 (c (n "glass-easel-i18n") (v "0.5.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glass-easel-template-compiler") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "1ph3n48j8bj9fk4ix5lmrhq1qscvg2c418an4r5izcsj6nf70v5k")))

(define-public crate-glass-easel-i18n-0.5.2 (c (n "glass-easel-i18n") (v "0.5.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glass-easel-template-compiler") (r "^0.5.4") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "100aj3f44rnvds5fxf3r0xhja3fkdvvs5k3hli5mpbsjhr8fm79j")))

