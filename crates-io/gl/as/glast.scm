(define-module (crates-io gl as glast) #:use-module (crates-io))

(define-public crate-glast-0.0.1 (c (n "glast") (v "0.0.1") (h "0wmygijy8gnrfybhp6ba4dvsqipirvilpkfla468kfwwlh9pz1q2") (f (quote (("logging"))))))

(define-public crate-glast-0.1.0 (c (n "glast") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0la3ghn06a4awjq5gfp9dgldfbwxr2aislpcwzsf05c35vcpmyj5")))

