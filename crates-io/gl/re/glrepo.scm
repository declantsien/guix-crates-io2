(define-module (crates-io gl re glrepo) #:use-module (crates-io))

(define-public crate-glrepo-0.4.0 (c (n "glrepo") (v "0.4.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^2") (f (quote ("stderr"))) (d #t) (k 0)))) (h "1rsz0h5mbjfahbxnb9jm8g29v8w2a80zgavhdafw7k3k25vlm7hb")))

(define-public crate-glrepo-0.4.1 (c (n "glrepo") (v "0.4.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^2") (f (quote ("stderr"))) (d #t) (k 0)))) (h "0rkibahgf4c0xncik84wlbd4x61arncqfnjsxbwv3my9yascv3zh")))

