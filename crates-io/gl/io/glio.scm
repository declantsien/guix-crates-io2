(define-module (crates-io gl io glio) #:use-module (crates-io))

(define-public crate-glio-0.0.1 (c (n "glio") (v "0.0.1") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)))) (h "11f876h02njcls2pbxjjrdpmfhfsi07rgh3n2sdhscz79grn681h") (y #t)))

(define-public crate-glio-0.0.2 (c (n "glio") (v "0.0.2") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)))) (h "1kd9yxywvfrb1p9mj7rpnpj66jm62lxc908kngh7vhz6gyl6rqd6") (y #t)))

(define-public crate-glio-0.0.3 (c (n "glio") (v "0.0.3") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)))) (h "0smqawr7s9a17vglgjl72lxjxvmk43nigbiimj8d2wbssdn8mpaz") (y #t)))

