(define-module (crates-io gl is glisp) #:use-module (crates-io))

(define-public crate-glisp-0.0.1 (c (n "glisp") (v "0.0.1") (h "1q5893p8ir9ynn6ha5x451i1xgy79qrzmcb63smpkslb4x7v9x5l")))

(define-public crate-glisp-0.0.2 (c (n "glisp") (v "0.0.2") (h "0g9fmxhqiqddpspjr7afqjgz1sb6pmpijdkxpffp2sqi000bcljd")))

(define-public crate-glisp-0.0.3 (c (n "glisp") (v "0.0.3") (d (list (d (n "cc") (r "^1.0.29") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cpp") (r "^0.5.1") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0k3b88s9v0j82jzh6afi08z5gllfhw6hm99fqb11nnj1vc4gf5mr")))

(define-public crate-glisp-0.0.4 (c (n "glisp") (v "0.0.4") (d (list (d (n "cc") (r "^1.0.30") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cpp") (r "^0.5.1") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)))) (h "1nwg0jn374cxcg4n8xhwlwyb4j6v4nziiw5jjfpr5mm6l0h75m73")))

