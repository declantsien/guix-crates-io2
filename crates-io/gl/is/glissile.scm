(define-module (crates-io gl is glissile) #:use-module (crates-io))

(define-public crate-glissile-0.0.1 (c (n "glissile") (v "0.0.1") (h "0lsgxn40ghsknsbgd7b7mcy2x6wycll0jfxxnbd0bxpfja0cqcg1")))

(define-public crate-glissile-0.0.2 (c (n "glissile") (v "0.0.2") (h "1rfx7r8m80zczmjwyfykyhpwg7vj756znj2w1jm7nmm148fj6my8")))

(define-public crate-glissile-0.0.3 (c (n "glissile") (v "0.0.3") (h "1g6gavamn06an82bb2cwb9kc71y8ycrya1i391jh1gi4py6lqn7k")))

