(define-module (crates-io gl cp glcp) #:use-module (crates-io))

(define-public crate-glcp-0.1.0 (c (n "glcp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)))) (h "0mn6w3z8vaxjg7wy2gxqha2v3qsim6xyjvv2pkn3w575jmy7jpga")))

(define-public crate-glcp-0.1.1 (c (n "glcp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)))) (h "1zjnwsi162j4fvkywa4p6210w19nrqnfkqbxrjxg3xm92q8i1g1s")))

(define-public crate-glcp-0.1.2 (c (n "glcp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)))) (h "1p0a7qzdkhmdbcjf6q40a7kbr24ajn5d9pqqm8i3yzf29q8wp1wh")))

