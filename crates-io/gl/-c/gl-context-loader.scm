(define-module (crates-io gl -c gl-context-loader) #:use-module (crates-io))

(define-public crate-gl-context-loader-0.1.0 (c (n "gl-context-loader") (v "0.1.0") (d (list (d (n "gleam") (r "^0.13.1") (o #t) (k 0)))) (h "11azg7syz1icihi7jxhljfy00vnc1gysam4lfspr124lgzxwypir") (f (quote (("std" "gleam") ("default" "std"))))))

(define-public crate-gl-context-loader-0.1.1 (c (n "gl-context-loader") (v "0.1.1") (d (list (d (n "gleam") (r "^0.13.1") (o #t) (k 0)))) (h "15vlfqi8ph4mwwgdv8w5nva58pfa0bf2mj6xjyx9vssji0c1q99a") (f (quote (("std" "gleam") ("default" "std") ("debug" "std"))))))

(define-public crate-gl-context-loader-0.1.2 (c (n "gl-context-loader") (v "0.1.2") (d (list (d (n "gleam") (r "^0.13.1") (o #t) (k 0)))) (h "1jf5g1pqb02kvvq0nrpbkmgh6fkayjsmcnx90siagb10cr38ghpq") (f (quote (("std" "gleam") ("default" "std") ("debug" "std"))))))

(define-public crate-gl-context-loader-0.1.3 (c (n "gl-context-loader") (v "0.1.3") (d (list (d (n "gleam") (r "^0.13.1") (o #t) (k 0)))) (h "0s4ciz5p1gmp8ygyd4mkv6dlx8kdla9ig9wcgcp4x5sl8c50rbsv") (f (quote (("std" "gleam") ("default" "std") ("debug" "std"))))))

(define-public crate-gl-context-loader-0.1.4 (c (n "gl-context-loader") (v "0.1.4") (d (list (d (n "gleam") (r "^0.13.1") (o #t) (k 0)))) (h "1z6dxy6lmz1zkjb0rf3y9qa4a3yslzz7dx5gdgrbc9mrayhjidsd") (f (quote (("std") ("gleam_trait" "gleam" "std") ("default" "std") ("debug" "std"))))))

(define-public crate-gl-context-loader-0.1.5 (c (n "gl-context-loader") (v "0.1.5") (d (list (d (n "gleam") (r "^0.13.1") (o #t) (k 0)))) (h "189i7n85iqr4vcflyw9fphpa6aizns8pdq8fj5lb12fwb6vi4npf") (f (quote (("std") ("gleam_trait" "gleam" "std") ("error" "debug" "std") ("default" "std") ("debug" "std"))))))

(define-public crate-gl-context-loader-0.1.6 (c (n "gl-context-loader") (v "0.1.6") (d (list (d (n "gleam") (r "^0.13.1") (o #t) (k 0)))) (h "0wfldrgvpydpl4kbiq691zr0mrfpnxjsgn2024m02mrzpjc04c43") (f (quote (("std") ("gleam_trait" "gleam" "std") ("error" "debug" "std") ("default" "std") ("debug" "std"))))))

(define-public crate-gl-context-loader-0.1.7 (c (n "gl-context-loader") (v "0.1.7") (d (list (d (n "gleam") (r "^0.13.1") (o #t) (k 0)))) (h "01mxfrn51x1vf5yg9dk5cdhw1hbwjl6a5rbj94nnnyzd0bds2jn6") (f (quote (("std") ("gleam_trait" "gleam" "std") ("error" "debug" "std") ("default" "std") ("debug" "std"))))))

(define-public crate-gl-context-loader-0.1.8 (c (n "gl-context-loader") (v "0.1.8") (d (list (d (n "gleam") (r "^0.13.1") (o #t) (k 0)))) (h "1xwy2jxvj37v6xppd6hvg75vy1mc3dwr4m8fa2hf092gf6pnbrj1") (f (quote (("std") ("gleam_trait" "gleam" "std") ("error" "debug" "std") ("default" "std") ("debug" "std"))))))

