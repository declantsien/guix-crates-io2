(define-module (crates-io gl -c gl-capture) #:use-module (crates-io))

(define-public crate-gl-capture-0.0.1 (c (n "gl-capture") (v "0.0.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)))) (h "14dm3s4hbb4ihp54hmmn74d8w4ghi14d2svip59sww6877mpg3nl")))

(define-public crate-gl-capture-0.0.2 (c (n "gl-capture") (v "0.0.2") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.51") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)))) (h "0gl0x6jn2qyram36k8br22kn34fhgm4lj9cydhzix3pdq15j46j3")))

