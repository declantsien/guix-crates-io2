(define-module (crates-io gl _g gl_generator_profiling_struct) #:use-module (crates-io))

(define-public crate-gl_generator_profiling_struct-0.1.0 (c (n "gl_generator_profiling_struct") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.9") (f (quote ("unstable_generator_utils"))) (d #t) (k 0)))) (h "0122gdcrs6j5p8q0ccgc8rk18420bwmz3galqhbqa747s5rllciy")))

(define-public crate-gl_generator_profiling_struct-0.1.1 (c (n "gl_generator_profiling_struct") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.9") (f (quote ("unstable_generator_utils"))) (d #t) (k 0)))) (h "0qvdh490ayni3cjrvjiqnlfhgifj9x70br9g9gw6m5v2jwa072vc")))

(define-public crate-gl_generator_profiling_struct-0.1.2 (c (n "gl_generator_profiling_struct") (v "0.1.2") (d (list (d (n "gl_generator") (r "^0.9") (f (quote ("unstable_generator_utils"))) (d #t) (k 0)))) (h "0ry0qqpw9qjkszij21hxansw05rgn6gzxmm2l7fqibkdcsqf34vg")))

