(define-module (crates-io gl ua gluac-rs) #:use-module (crates-io))

(define-public crate-gluac-rs-0.1.0 (c (n "gluac-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1p1bvmdlc0rzjjpl3r7qm7znkffv15drw7z0kf3b8abydrr07pva") (y #t)))

(define-public crate-gluac-rs-0.1.1 (c (n "gluac-rs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1kirlc0mxlysgwnj7z632dyl5z3s1fjch57x1ryjfjm1qfgk10hy")))

(define-public crate-gluac-rs-0.1.3 (c (n "gluac-rs") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1r3aq8zlvy4cprywkzwks3gqjjpz37ykj5fz69ynpy0970wbk09a")))

