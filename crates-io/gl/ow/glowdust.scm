(define-module (crates-io gl ow glowdust) #:use-module (crates-io))

(define-public crate-glowdust-0.0.1 (c (n "glowdust") (v "0.0.1") (d (list (d (n "ctor") (r "^0.2.6") (d #t) (k 2)) (d (n "io-uring") (r "^0.6") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde-json-core") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0s889823qwbnqmbhjk06acqcxk7xdifzh3fdm46v25lhp578l6cx")))

