(define-module (crates-io gl ow glow-effects) #:use-module (crates-io))

(define-public crate-glow-effects-0.5.0 (c (n "glow-effects") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1inga8srfyfh2k417carb8lkza2nxgwndj1vzr9vf57izq27m0f8")))

