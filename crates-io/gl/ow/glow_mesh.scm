(define-module (crates-io gl ow glow_mesh) #:use-module (crates-io))

(define-public crate-glow_mesh-0.1.0 (c (n "glow_mesh") (v "0.1.0") (d (list (d (n "glow") (r "^0.11.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 2)) (d (n "minvect") (r "^0.1.1") (d #t) (k 0)))) (h "1946mlf05pvlbr5xflac65ymrp05mnswxwizczmkr4bz44b86m9x")))

(define-public crate-glow_mesh-0.1.1 (c (n "glow_mesh") (v "0.1.1") (d (list (d (n "glow") (r "^0.11.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 2)) (d (n "minvect") (r "^0.1.1") (d #t) (k 0)))) (h "01618qnj8zii79j4n4471c0hx6xlvl2w5zclqni3sp45bw5714lv")))

(define-public crate-glow_mesh-0.1.2 (c (n "glow_mesh") (v "0.1.2") (d (list (d (n "glow") (r "^0.11.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 2)) (d (n "minimg") (r "^0.1.2") (d #t) (k 0)) (d (n "minvect") (r "^0.1.3") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "00iii9appkgrmjy9p1n53b15c1q5n9znr11479brwhjkkwzzhh1h")))

(define-public crate-glow_mesh-0.1.3 (c (n "glow_mesh") (v "0.1.3") (d (list (d (n "glow") (r "^0.11.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 2)) (d (n "minimg") (r "^0.1.2") (d #t) (k 0)) (d (n "minvect") (r "^0.1.4") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "1kjlwq1ln36qd1wj0mriy7zj86xnvfh9vfgd3gxr8lxr2xkhbqvq")))

(define-public crate-glow_mesh-0.1.4 (c (n "glow_mesh") (v "0.1.4") (d (list (d (n "glow") (r "^0.11.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 2)) (d (n "minimg") (r "^0.1.2") (d #t) (k 0)) (d (n "minvect") (r "^0.1.4") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "1zcny3llfm2hi8xm6cxj1w6kggayf0f8zms1idwc2gykf6w5bff7")))

