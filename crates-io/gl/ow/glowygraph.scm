(define-module (crates-io gl ow glowygraph) #:use-module (crates-io))

(define-public crate-glowygraph-0.2.1 (c (n "glowygraph") (v "0.2.1") (d (list (d (n "glium") (r "^0.13.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "0l4zym26dp4pk9qi9cnakml3dzk9zrfkmxz3fl2kiz2fr277bqwr")))

(define-public crate-glowygraph-0.3.0 (c (n "glowygraph") (v "0.3.0") (d (list (d (n "glium") (r "^0.13.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "1mf60zrc24zspb900y2nraisjs4cpcx7gz7axydjmx9k72aqq336")))

(define-public crate-glowygraph-0.4.0 (c (n "glowygraph") (v "0.4.0") (d (list (d (n "glium") (r "^0.13.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "0vr3q1g1qgfrixms2zg137hlvxgcba0rpvq39shg9v4a07b602lf")))

(define-public crate-glowygraph-0.4.1 (c (n "glowygraph") (v "0.4.1") (d (list (d (n "glium") (r "^0.13.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "1hbabzvhz83sj11gdw9hcrbx8xq4q1f2fl5jlcf4c02myw88jpj2")))

(define-public crate-glowygraph-0.4.2 (c (n "glowygraph") (v "0.4.2") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.32") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.5") (d #t) (k 0)))) (h "0cn0g3c7vc4mc195alir4r41fkb6hx5zk8r343wr3k7a3jgzn5fb")))

(define-public crate-glowygraph-0.5.0 (c (n "glowygraph") (v "0.5.0") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1w93risrqan23y3rp5nrlb6hspn52qgwdzmjpwv2yv531j1nahcr")))

(define-public crate-glowygraph-0.5.1 (c (n "glowygraph") (v "0.5.1") (d (list (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0sdnr1k5h0zsp69d1s87ls93jn6njz6z6j3lmkkhkrsylfvby7wj")))

(define-public crate-glowygraph-0.5.2 (c (n "glowygraph") (v "0.5.2") (d (list (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1wkaxy0qmik8bx54vd31a7l73cf6pjwhxv7mzaa2vjpm348r75mm")))

(define-public crate-glowygraph-0.5.3 (c (n "glowygraph") (v "0.5.3") (d (list (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0c88smxznw53c605z3ava0bmyp25yw2xsizyvky759jn8djm179k")))

(define-public crate-glowygraph-0.5.4 (c (n "glowygraph") (v "0.5.4") (d (list (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "087as28day20dmjgg3081havh7ma7mf1j7hcb8aw3ypk545m1kyv")))

(define-public crate-glowygraph-0.6.0 (c (n "glowygraph") (v "0.6.0") (d (list (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1wr0n0xm15xxska0w8z57540zacganxpdjsw8w4x4sabkvvml4f8")))

(define-public crate-glowygraph-0.7.0 (c (n "glowygraph") (v "0.7.0") (d (list (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0ffpxx4iyjlnj73amwwqsj8jlmplhr1mzn9b2qwnqdgzlbcbb8iy") (y #t)))

(define-public crate-glowygraph-0.7.1 (c (n "glowygraph") (v "0.7.1") (d (list (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1xj3ydcyb1cma5vgfypzxgrbwzvf6skf1mj28m11l92lb3pj2693")))

(define-public crate-glowygraph-0.7.2 (c (n "glowygraph") (v "0.7.2") (d (list (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "07l7ava959bmnjhqhl1q243xh3zzr71mqnjhk48dpfzal0dylcvf")))

(define-public crate-glowygraph-0.7.3 (c (n "glowygraph") (v "0.7.3") (d (list (d (n "glium") (r "^0.15") (d #t) (k 0)) (d (n "nalgebra") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "11hjc1dkkvz1c5n193yi26ssjvh29hvk52l9v0mx4haamgp1p34x")))

(define-public crate-glowygraph-0.8.0 (c (n "glowygraph") (v "0.8.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "glium") (r "^0.20") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1qcx0w8dkbnjng2qaxszjgkyk0mh5rhjnyawsj1a5ssa35jinzif")))

