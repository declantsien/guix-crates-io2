(define-module (crates-io gl ow glow_glyph) #:use-module (crates-io))

(define-public crate-glow_glyph-0.0.0 (c (n "glow_glyph") (v "0.0.0") (h "0xb6amy76srjy86vi0s12zaaynhfzymmkl6zf4vn0b2im4551hva")))

(define-public crate-glow_glyph-0.1.0 (c (n "glow_glyph") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "glow") (r "^0.4") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16pzsmgrpsjf20vi3zp7gnclnybz3kjmyriwyar4lm8xjrp08f64")))

(define-public crate-glow_glyph-0.2.0 (c (n "glow_glyph") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "glow") (r "^0.4") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rxmn28hv2hjibsa0pq90c2zk4r6gq08v79jv6g887kw9rz0mh5h")))

(define-public crate-glow_glyph-0.2.1 (c (n "glow_glyph") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "glow") (r "^0.4") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "079rwx1q4f0s0584pnnib4m3851f152f1spjmnhjr1f0bi05pjfi")))

(define-public crate-glow_glyph-0.3.0 (c (n "glow_glyph") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "glow") (r "^0.5") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01p224rcpq4ysrby7pn6piwcw5nyv0lpilfbfn7jrm7261y785aq")))

(define-public crate-glow_glyph-0.4.0 (c (n "glow_glyph") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "glow") (r "^0.6") (d #t) (k 0)) (d (n "glutin") (r "^0.25") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jxicg5znxkk9pslal55jgpz2w6qpw40pdc46a72ppa6xd808lgh")))

(define-public crate-glow_glyph-0.5.0 (c (n "glow_glyph") (v "0.5.0") (d (list (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "glow") (r "^0.11.1") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "160z0qk0ksq1vbr97wir8h1fyq4c5kanjbz710cx32dz30fzfc9q")))

(define-public crate-glow_glyph-0.5.1 (c (n "glow_glyph") (v "0.5.1") (d (list (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "glow") (r "^0.11.1") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "044mvz368x3kqkzzg3awxw3bbv0rv2dblaqfw97s5fa797364khg")))

(define-public crate-glow_glyph-0.6.0 (c (n "glow_glyph") (v "0.6.0") (d (list (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "glow") (r "^0.12.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mk45vmkva4k89vac27m2imlwm543ialadp10fhcwprlrqi2h8i2")))

(define-public crate-glow_glyph-0.7.0 (c (n "glow_glyph") (v "0.7.0") (d (list (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "glow") (r "^0.13.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1npg9z5hchx87qsjdi3ygj3ykxbfdr7q8p0hd30mxinap88fwhl8")))

