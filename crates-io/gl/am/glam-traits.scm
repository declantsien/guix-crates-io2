(define-module (crates-io gl am glam-traits) #:use-module (crates-io))

(define-public crate-glam-traits-0.1.0 (c (n "glam-traits") (v "0.1.0") (d (list (d (n "glam") (r "^0.25.0") (d #t) (k 0)))) (h "0ygk2dzbr7qqcbyygj78ibkv1zhyc5gk5pnyryrp2qihkgfbirpd")))

(define-public crate-glam-traits-0.1.1 (c (n "glam-traits") (v "0.1.1") (d (list (d (n "glam") (r "^0.25.0") (d #t) (k 0)))) (h "0z9rkrbc55pgrxp2bf5m3gkipkz421chs0c3vz9x9kizxsm4x6gp")))

(define-public crate-glam-traits-0.1.2 (c (n "glam-traits") (v "0.1.2") (d (list (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)))) (h "10wlyqg8gb00dlr214c6b2f6qw6qvsm8p2pnbcmsqp6wkjhcr52s") (s 2) (e (quote (("ext" "dep:num-traits"))))))

(define-public crate-glam-traits-0.1.3 (c (n "glam-traits") (v "0.1.3") (d (list (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)))) (h "1w7155l6ax0yw7rnm47d35gfnjwjsp1wdwlaq0qzg18rhkhdmb1v") (s 2) (e (quote (("ext" "dep:num-traits"))))))

(define-public crate-glam-traits-0.1.4 (c (n "glam-traits") (v "0.1.4") (d (list (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)))) (h "002la7lrrgriw0x5jymxmkh8111fhc00880fyjilbjyip3h96z7p") (s 2) (e (quote (("ext" "dep:num-traits"))))))

