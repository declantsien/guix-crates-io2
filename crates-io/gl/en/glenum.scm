(define-module (crates-io gl en glenum) #:use-module (crates-io))

(define-public crate-glenum-0.1.0 (c (n "glenum") (v "0.1.0") (h "11f9ay0jhcnfk0laxymnn7afcrvvr4a8isflz8f0k2vvz1j3dah0")))

(define-public crate-glenum-0.1.1 (c (n "glenum") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0hfkg0hz83d4g0qpzgjd344j28m8bynxidw684m5ymh6lifkx3fy")))

