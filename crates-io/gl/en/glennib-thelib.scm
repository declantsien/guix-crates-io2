(define-module (crates-io gl en glennib-thelib) #:use-module (crates-io))

(define-public crate-glennib-thelib-0.1.0 (c (n "glennib-thelib") (v "0.1.0") (h "1jnlfzw7gq24470fk63s34rvydir3mq5rr05q952i2p0920pkhnj")))

(define-public crate-glennib-thelib-0.1.1 (c (n "glennib-thelib") (v "0.1.1") (h "0klvlxfm92ivixzzx5s9q9nzvmnfr264r9cl0x35sdbyfcga9wgk")))

(define-public crate-glennib-thelib-0.1.2 (c (n "glennib-thelib") (v "0.1.2") (h "054grin8yx5qha8piv848rbwgfkkdyhwfanalhn6gmy0v9r5dwbl")))

