(define-module (crates-io gl l- gll-pg-macros) #:use-module (crates-io))

(define-public crate-gll-pg-macros-0.1.0 (c (n "gll-pg-macros") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "03s1rqgvxzr9234cn2sdvhy5acn819xr54g3s9hl7iblsj87404n")))

(define-public crate-gll-pg-macros-0.2.0 (c (n "gll-pg-macros") (v "0.2.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0pds53jx5xkgdrx428y69jh333blpclxqxa32asl2wlyg1mbd9r5")))

(define-public crate-gll-pg-macros-0.2.1 (c (n "gll-pg-macros") (v "0.2.1") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "16vfzf6cvfrajrybxv3hndsv09qlnrbirrgv4hg4r86zz31ygia6")))

(define-public crate-gll-pg-macros-0.3.0 (c (n "gll-pg-macros") (v "0.3.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0km82zcgxk9rblqs357jgpl3p7gg7nz4vy76xa5qiavxh4qn6ksa")))

(define-public crate-gll-pg-macros-0.4.0 (c (n "gll-pg-macros") (v "0.4.0") (d (list (d (n "aho-corasick") (r "^0.7.15") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "15r280kfw1kiivrrq63blq3g1am6p3isqp1pv3jbgprpbw3f23v1")))

(define-public crate-gll-pg-macros-0.5.0 (c (n "gll-pg-macros") (v "0.5.0") (d (list (d (n "aho-corasick") (r "^1.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0fi9qk6yw3fqgzxnv0pwrfbnphdwb1lbdgfm534l0z7rk0l3wgw2")))

