(define-module (crates-io gl l- gll-pg-core) #:use-module (crates-io))

(define-public crate-gll-pg-core-0.1.0 (c (n "gll-pg-core") (v "0.1.0") (d (list (d (n "logos") (r "^0.9") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "00pydwblxq0wh591bqvnmsq8kcnyj3i7j7i7p3awainard81jg0i")))

(define-public crate-gll-pg-core-0.2.0 (c (n "gll-pg-core") (v "0.2.0") (d (list (d (n "logos") (r "^0.9") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "08hm2yf29nkllqf2n4s7zsda86crkifcf8vzfpcbh4i0y9s5k4c1")))

(define-public crate-gll-pg-core-0.2.1 (c (n "gll-pg-core") (v "0.2.1") (d (list (d (n "logos") (r "^0.9") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "168zvi24yxmpakk81rb9g93cn83fr9b07dih297zljbgxn3b8wkw")))

(define-public crate-gll-pg-core-0.3.0 (c (n "gll-pg-core") (v "0.3.0") (d (list (d (n "logos") (r "^0.9") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)))) (h "1v2cdi1xpf7l5xh6vakvpj6g97rh8kylmn3f5rkjmvnlnk3l2y9w")))

(define-public crate-gll-pg-core-0.4.0 (c (n "gll-pg-core") (v "0.4.0") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.5") (d #t) (k 0)))) (h "19173s08i22isnrgmsfnrs0d1kwsgr10yp2s1jaadz7mz5yhlkv0")))

(define-public crate-gll-pg-core-0.5.0 (c (n "gll-pg-core") (v "0.5.0") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.5") (d #t) (k 0)))) (h "0dr98lfg3fgnixh3j9ahv3ydbh87y6hdz40w6vl7fkk85590ivxx")))

