(define-module (crates-io gl l- gll-macros) #:use-module (crates-io))

(define-public crate-gll-macros-0.0.1 (c (n "gll-macros") (v "0.0.1") (d (list (d (n "gll") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 2)))) (h "0581ihax7ha559dsi4xijly5nshys33rzji54vrmzkxiz4zdrq6a")))

(define-public crate-gll-macros-0.0.2 (c (n "gll-macros") (v "0.0.2") (d (list (d (n "gll") (r "^0.0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 2)))) (h "1xsny4zrvpj51i3bbkm9xvjyf7fqg7ic7wjzysazfvyhk3a0izds")))

