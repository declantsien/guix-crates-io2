(define-module (crates-io gl #{32}# gl32) #:use-module (crates-io))

(define-public crate-gl32-0.1.0 (c (n "gl32") (v "0.1.0") (d (list (d (n "gl_generator") (r "~0.10.0") (d #t) (k 1)) (d (n "glutin") (r "~0.19.0") (d #t) (k 2)))) (h "1pmba8kkz4jm6a4mzn7j4b441afxb5h825srzkjdxmnp5hl2b7cf")))

(define-public crate-gl32-0.1.1 (c (n "gl32") (v "0.1.1") (d (list (d (n "gl_generator") (r "~0.10.0") (d #t) (k 1)) (d (n "glutin") (r "~0.19.0") (d #t) (k 2)))) (h "1vrp1vv9l5zvbvam7k4azkmbrw68g2gkhj7zp7kqh1zmjnh9l2hd")))

(define-public crate-gl32-0.2.0 (c (n "gl32") (v "0.2.0") (d (list (d (n "gl_generator") (r "~0.10.0") (d #t) (k 1)) (d (n "glutin") (r "~0.19.0") (d #t) (k 2)))) (h "12n83zihxxiva3k2fd8s4kfgfnlc50p7pphj02znpxkpr1rga0a5")))

