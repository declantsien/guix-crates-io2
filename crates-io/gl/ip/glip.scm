(define-module (crates-io gl ip glip) #:use-module (crates-io))

(define-public crate-glip-0.3.0 (c (n "glip") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "libflate") (r "^0.1.19") (d #t) (k 0)) (d (n "maxminddb") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)))) (h "0pc7b9y52dj2sjgmk1jrmj9n5g2gsngms3pbkzjpbf1x3da7jpiy")))

(define-public crate-glip-0.5.0 (c (n "glip") (v "0.5.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "libflate") (r "^0.1.19") (d #t) (k 0)) (d (n "maxminddb") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)))) (h "15048465xmkg7807i0sm069s5xy3fd1bkpjikdhwmr6dcblsy4xv")))

(define-public crate-glip-0.6.0 (c (n "glip") (v "0.6.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "libflate") (r "^0.1.19") (d #t) (k 0)) (d (n "maxminddb") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)))) (h "1w783n3gkd5vhij1axpr864i2gbwj2vbbz8kk3ivbl26169icly5")))

