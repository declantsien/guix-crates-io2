(define-module (crates-io gl us glusterfs-exporter) #:use-module (crates-io))

(define-public crate-glusterfs-exporter-0.1.1 (c (n "glusterfs-exporter") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "prometheus") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_xml") (r "^0.9") (d #t) (k 0)))) (h "16kq553cgv2ycfagvl7d18dv0a9kzqklv4342l6c3f0j3syxppg6")))

