(define-module (crates-io gl ut glutin_gles2_sys) #:use-module (crates-io))

(define-public crate-glutin_gles2_sys-0.1.0 (c (n "glutin_gles2_sys") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "0bxvsl2sms46jf292ybf9mvlxi8maxiqlnxndf0hjzwn53aj787i")))

(define-public crate-glutin_gles2_sys-0.1.1 (c (n "glutin_gles2_sys") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "1ggrx8g12gd30z5xnbi5k2nxra1al55bm4zsqcrkh1hhd80mmf5v")))

(define-public crate-glutin_gles2_sys-0.1.2 (c (n "glutin_gles2_sys") (v "0.1.2") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "1a4nd41s66qmcsgjc49gqh373w8z98cg5pdjxz9nmxxn6lpijgmk")))

(define-public crate-glutin_gles2_sys-0.1.3 (c (n "glutin_gles2_sys") (v "0.1.3") (d (list (d (n "gl_generator") (r "^0.11") (d #t) (k 1)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "1pswvl5zyqmqwzjr674yzslj0al2xbqsp2ai9ggb9qbshlq6r6c9")))

(define-public crate-glutin_gles2_sys-0.1.4 (c (n "glutin_gles2_sys") (v "0.1.4") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "1q6scyshfvsz4i1jakylc1cnz33m14q5q8j5wi9qxjzbdgcm7s07")))

(define-public crate-glutin_gles2_sys-0.1.5 (c (n "glutin_gles2_sys") (v "0.1.5") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "00wisv3a7818bpw5nnqwibmh1bw032izix2l3657q2kkidq4w2g8")))

(define-public crate-glutin_gles2_sys-0.1.6 (c (n "glutin_gles2_sys") (v "0.1.6") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "0x0g0iwfahbrzv0nr0dx6l2nfipx3ifd67xf1aphc16yfif4jn8f") (y #t) (r "1.57.0")))

(define-public crate-glutin_gles2_sys-0.2.0 (c (n "glutin_gles2_sys") (v "0.2.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "1mn2iwr2nj6kg4jlw15v1l43z525d1yp578gsh8rfifd6y3xgd3d") (r "1.57.0")))

(define-public crate-glutin_gles2_sys-0.3.0 (c (n "glutin_gles2_sys") (v "0.3.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "1c57gfdd47d7jpfl30kqj5h1ygg7isfg1rbh7259mx9fx3ydxgkr") (r "1.60.0")))

(define-public crate-glutin_gles2_sys-0.4.0 (c (n "glutin_gles2_sys") (v "0.4.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "0gvk1a20n32caw959iagsssfbc9q2i5si6hc4ha21gbp0cjsb738") (r "1.60.0")))

(define-public crate-glutin_gles2_sys-0.5.0 (c (n "glutin_gles2_sys") (v "0.5.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "1wcihv3si95xj0ydy9hrqnmj98ak09g76bkq6iwwasc1rdnyxmzn") (r "1.65.0")))

