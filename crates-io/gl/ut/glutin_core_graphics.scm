(define-module (crates-io gl ut glutin_core_graphics) #:use-module (crates-io))

(define-public crate-glutin_core_graphics-0.1.0 (c (n "glutin_core_graphics") (v "0.1.0") (d (list (d (n "glutin_core_foundation") (r "*") (d #t) (k 0)))) (h "00f83l7nf53abn0zgz4hhvfp12bgdi17y90dgr5an4ycckswpc2w")))

(define-public crate-glutin_core_graphics-0.1.1 (c (n "glutin_core_graphics") (v "0.1.1") (d (list (d (n "glutin_core_foundation") (r "*") (d #t) (k 0)))) (h "0lip7h599y9cy1w5hg6crdxjs848y90bim66c021rajs4a6pcfd4")))

(define-public crate-glutin_core_graphics-0.1.2 (c (n "glutin_core_graphics") (v "0.1.2") (d (list (d (n "glutin_core_foundation") (r "*") (d #t) (k 0)))) (h "19s3fm2qs8p7ks1w9iswvma53yv2q3zhaqiv6dpxmg0198fp0krf")))

(define-public crate-glutin_core_graphics-0.1.3 (c (n "glutin_core_graphics") (v "0.1.3") (d (list (d (n "glutin_core_foundation") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1hi8vcsa5baapvslf170r667l4hlwxsg6li6br29ih0c808k34sb")))

(define-public crate-glutin_core_graphics-0.1.4 (c (n "glutin_core_graphics") (v "0.1.4") (d (list (d (n "glutin_core_foundation") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1mai2j2zlh7li9lbl3np2zxbw14p71na6anva9l2pkjh8zpdal5k")))

