(define-module (crates-io gl ut glutin_egl_sys) #:use-module (crates-io))

(define-public crate-glutin_egl_sys-0.1.0 (c (n "glutin_egl_sys") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt" "winuser" "wingdi" "libloaderapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1rvxmdjv8mf044wi1r4acvxk56hgmaws1wwv7n7lwrf7fikvfmpi")))

(define-public crate-glutin_egl_sys-0.1.1 (c (n "glutin_egl_sys") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt" "winuser" "wingdi" "libloaderapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1f7bc7rrwsvm4cz4lb4wnqrp57lfpbsaa4g9m4hilxvdwhr007cd")))

(define-public crate-glutin_egl_sys-0.1.2 (c (n "glutin_egl_sys") (v "0.1.2") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt" "winuser" "wingdi" "libloaderapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1jpnp7dqf1x8n6yzqpcshbrj2vrygrjzpgs1qs6kl1lmnia4aaam")))

(define-public crate-glutin_egl_sys-0.1.3 (c (n "glutin_egl_sys") (v "0.1.3") (d (list (d (n "gl_generator") (r "^0.11") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winnt" "winuser" "wingdi" "libloaderapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "09nk7nknjsw2svzqrxmggc53h37xl9a9xd83v4dbdckcmf3qkx13")))

(define-public crate-glutin_egl_sys-0.1.4 (c (n "glutin_egl_sys") (v "0.1.4") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winnt" "winuser" "wingdi" "libloaderapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0k1x1frdp4wp47qkai8zzmgqxzpfcn7780m29qgd92lbnbrxwbkp")))

(define-public crate-glutin_egl_sys-0.1.5 (c (n "glutin_egl_sys") (v "0.1.5") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winnt" "winuser" "wingdi" "libloaderapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "04f2ci9kb8q4dv4kviigvgfy54lr4jmbnmjsvi50qj13anjnmfra")))

(define-public crate-glutin_egl_sys-0.1.6 (c (n "glutin_egl_sys") (v "0.1.6") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winnt" "winuser" "wingdi" "libloaderapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0g81bz7ppvaksvwcw1jg553g8b2shvmnfm9ms6hixwvinj20z438")))

(define-public crate-glutin_egl_sys-0.1.7 (c (n "glutin_egl_sys") (v "0.1.7") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0jbxhwq0f7y65iha2laxd365w7zx62ry4z31x0gsam2nrj1bxsjx") (y #t) (r "1.57.0")))

(define-public crate-glutin_egl_sys-0.2.0 (c (n "glutin_egl_sys") (v "0.2.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "11z7550dwkylyq9cy6v8hzdpmc8ga85n365ybx36fznpwj0mww6m") (r "1.57.0")))

(define-public crate-glutin_egl_sys-0.3.0 (c (n "glutin_egl_sys") (v "0.3.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1hdxk0nchk8isigfijq48l6lfqydnppr95vmfh6anjx5syi9ag1c") (r "1.60.0")))

(define-public crate-glutin_egl_sys-0.3.1 (c (n "glutin_egl_sys") (v "0.3.1") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1pxg00f2azdaka810szlhc6js50fbygzfy0ck47k93p1q3zbinrs") (r "1.60.0")))

(define-public crate-glutin_egl_sys-0.4.0 (c (n "glutin_egl_sys") (v "0.4.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "windows-sys") (r "^0.45") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0z9nm3d6qcgqg7f6qkbnsfs4cy90d8raw09inf2qc564nnmz1ap5") (r "1.60.0")))

(define-public crate-glutin_egl_sys-0.5.0 (c (n "glutin_egl_sys") (v "0.5.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "windows-sys") (r "^0.45") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11sddlsa8kwgwh9mvbyv5il2jkvyhs9mvjkdzivvjwqmqpfwnfqv") (r "1.60.0")))

(define-public crate-glutin_egl_sys-0.5.1 (c (n "glutin_egl_sys") (v "0.5.1") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "windows-sys") (r "^0.45") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1iapzqscy4891crxdddddq4qqqday1sf0s0j762yqs2sdjr4wy5g") (r "1.60.0")))

(define-public crate-glutin_egl_sys-0.6.0 (c (n "glutin_egl_sys") (v "0.6.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1kcv5pdpdsyhzpiahga15kk7yd4m64ia2k6xqcrz97ihylimdk3p") (r "1.65.0")))

