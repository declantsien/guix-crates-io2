(define-module (crates-io gl ut glutin_glx_sys) #:use-module (crates-io))

(define-public crate-glutin_glx_sys-0.1.0 (c (n "glutin_glx_sys") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11-dl") (r "^2.18") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "1bilxqy6bfi1n6skrhh43vn732211613a2inyly43xh5aps3nass")))

(define-public crate-glutin_glx_sys-0.1.1 (c (n "glutin_glx_sys") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11-dl") (r "^2.18") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "1vacm1sirg8gvccfn6lml1i309xlh9ak5jpn7z8scc8am6a60ykc")))

(define-public crate-glutin_glx_sys-0.1.2 (c (n "glutin_glx_sys") (v "0.1.2") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11-dl") (r "^2.18") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "1g0fh75443kyipixlsqkii7lz9blsli8mfd6qfkbb3nwz0cf7qqy")))

(define-public crate-glutin_glx_sys-0.1.3 (c (n "glutin_glx_sys") (v "0.1.3") (d (list (d (n "gl_generator") (r "^0.11") (d #t) (k 1)) (d (n "x11-dl") (r "^2.18") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "1mxn4gc2yb133jg8s7hggd7hiakp4djdpraqyfbfnlq8wymygs98")))

(define-public crate-glutin_glx_sys-0.1.4 (c (n "glutin_glx_sys") (v "0.1.4") (d (list (d (n "gl_generator") (r "^0.11") (d #t) (k 1)) (d (n "x11-dl") (r "^2.18") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "1k0i609xfxfjhrpldpz6lxbi86zg0mlc60bak3lzmmb0z036sfsw")))

(define-public crate-glutin_glx_sys-0.1.5 (c (n "glutin_glx_sys") (v "0.1.5") (d (list (d (n "gl_generator") (r "^0.11") (d #t) (k 1)) (d (n "x11-dl") (r "^2.18.3") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "0mxs3mil68xqqb49466n5rpwpcllj6fwqjgrcrzzmz26bv5ab40j")))

(define-public crate-glutin_glx_sys-0.1.6 (c (n "glutin_glx_sys") (v "0.1.6") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)) (d (n "x11-dl") (r "^2.18.3") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "1hq970xnc6l122rx8pa82pirscdr5xnq526702hmxkynfkg47hh8")))

(define-public crate-glutin_glx_sys-0.1.7 (c (n "glutin_glx_sys") (v "0.1.7") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "x11-dl") (r "^2.18.3") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "0l8kk60kq5v6hl1qr6ym2arzvbsgkh71aa8485cp901bq27kqfby")))

(define-public crate-glutin_glx_sys-0.1.8 (c (n "glutin_glx_sys") (v "0.1.8") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "x11-dl") (r "^2.18.3") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "0s14s3v2dddkx141w2x65s8ik54mrn432hisbc65i62hhrshagfr")))

(define-public crate-glutin_glx_sys-0.1.9 (c (n "glutin_glx_sys") (v "0.1.9") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "x11-dl") (r "^2.18.3") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "1l564f7vsdgf9ahlhi8yczs96pma6z19116jr7wv89lcicw1kfb3") (y #t) (r "1.57.0")))

(define-public crate-glutin_glx_sys-0.2.0 (c (n "glutin_glx_sys") (v "0.2.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "x11-dl") (r "^2.18.3") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "1cjbmrhpiqg4zq2f767rdbrhwpg0p9mq02yx230njp8fl4srqzi4") (r "1.57.0")))

(define-public crate-glutin_glx_sys-0.3.0 (c (n "glutin_glx_sys") (v "0.3.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "x11-dl") (r "^2.18.3") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "03vfz517jza7k8nanyxlwldbhr27czib9hk9g5icj4c2qm84hz4l") (r "1.60.0")))

(define-public crate-glutin_glx_sys-0.4.0 (c (n "glutin_glx_sys") (v "0.4.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "x11-dl") (r "^2.18.3") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "150l397l64p4r46wshh8zdlwifpcqpm93fm3csh4m5k8wmgwnlqv") (r "1.60.0")))

(define-public crate-glutin_glx_sys-0.5.0 (c (n "glutin_glx_sys") (v "0.5.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "x11-dl") (r "^2.18.3") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os=\"dragonfly\", target_os=\"netbsd\", target_os=\"openbsd\"))") (k 0)))) (h "0krv3chf5sy83rsfwq267paczskpwnb5gcw0agac5p0hdilgsrd1") (r "1.65.0")))

