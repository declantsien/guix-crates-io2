(define-module (crates-io gl ut glutin_emscripten_sys) #:use-module (crates-io))

(define-public crate-glutin_emscripten_sys-0.1.0 (c (n "glutin_emscripten_sys") (v "0.1.0") (h "1ix0jmm8p5if4qarzdfl5mz9rbq4hhgqarakb3bzwvyz13dkynr4")))

(define-public crate-glutin_emscripten_sys-0.1.1 (c (n "glutin_emscripten_sys") (v "0.1.1") (h "1wb3qfxg3jh6ibb7bxmlmvf4jcpzck3pn0035g1sds3nvx343pl0")))

