(define-module (crates-io gl ut glutin_core_foundation) #:use-module (crates-io))

(define-public crate-glutin_core_foundation-0.1.0 (c (n "glutin_core_foundation") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "18hzkzpd4jclvz4jj9lkbd5m0dp3gahifz3jlvxvlhxck8sl6ya4")))

(define-public crate-glutin_core_foundation-0.1.1 (c (n "glutin_core_foundation") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ggxpsxv3p65k21vgfbhhjpvy6p0jw21zgli7c9aqp3xb3l0f1gi")))

(define-public crate-glutin_core_foundation-0.1.2 (c (n "glutin_core_foundation") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0vd8xyqhw9nr80z9334ffamhpfczkqa4fv2mgw2jda1xsgr4ia6w")))

