(define-module (crates-io gl ut gluten-keyboard) #:use-module (crates-io))

(define-public crate-gluten-keyboard-0.1.0 (c (n "gluten-keyboard") (v "0.1.0") (h "0gqv3p9gc6sz05y71i8mzvcfzn2m7z12scrxs2nlvrl3kjv328q5") (y #t)))

(define-public crate-gluten-keyboard-0.1.1 (c (n "gluten-keyboard") (v "0.1.1") (h "02ywwy3pngk2x6cdbk77jpjaqd21d40jxfygvh6vvlk5ngi9a86v")))

(define-public crate-gluten-keyboard-0.1.2 (c (n "gluten-keyboard") (v "0.1.2") (h "1pscqmjqm7xpm8y75wgq17mv36mqb6ghklxzy5rixswhsykisbcd")))

