(define-module (crates-io gl ut glutin_cocoa) #:use-module (crates-io))

(define-public crate-glutin_cocoa-0.1.1 (c (n "glutin_cocoa") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0gy45dmgmqlh13ls4h3ykpxz2d0m3h1kzggnbyvxm2zi3k8xvvmq")))

(define-public crate-glutin_cocoa-0.1.2 (c (n "glutin_cocoa") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "115jvriy0d3245ml4949aq17nkdbnp0bqgkwp0liwsd4s9q3902a") (y #t)))

(define-public crate-glutin_cocoa-0.1.3 (c (n "glutin_cocoa") (v "0.1.3") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "objc") (r "^0.1") (d #t) (k 0)))) (h "033na6hrsi10wsldv7gcxbzdyq2121jph3khqx8k60f3zjkhxcx0")))

(define-public crate-glutin_cocoa-0.1.4 (c (n "glutin_cocoa") (v "0.1.4") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "objc") (r "^0.1") (d #t) (k 0)))) (h "1a4cdzg0hs8j1mw11m0v1r80w0lbmnp4xxdqw04j7w0cm5ii3wf1")))

(define-public crate-glutin_cocoa-0.1.5 (c (n "glutin_cocoa") (v "0.1.5") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "objc") (r "^0.1") (d #t) (k 0)))) (h "13dj16hki2s5kb2m9n40h1mgibnk2gjphd3286jwri8fjvxphmgh")))

(define-public crate-glutin_cocoa-0.1.6 (c (n "glutin_cocoa") (v "0.1.6") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "objc") (r "^0.1") (d #t) (k 0)))) (h "0nl7ykhp816np671pj0nszlcrsny56wyg27k1dnmhsrr22c4ix1p")))

