(define-module (crates-io gl ad gladis) #:use-module (crates-io))

(define-public crate-gladis-0.1.0 (c (n "gladis") (v "0.1.0") (h "18b4iwfa0msl7sijnm6b4m077mgfyfmcqv60a53kadpg7f9slsp0")))

(define-public crate-gladis-0.1.1 (c (n "gladis") (v "0.1.1") (h "0azgslj96pcfgj9422qxhfy59bgrghp1z35qh2vgz07r55f5f37h")))

(define-public crate-gladis-0.1.2 (c (n "gladis") (v "0.1.2") (d (list (d (n "gtk") (r "0.9.*") (d #t) (k 0)))) (h "17r08vb5dbzlgfw5z1zgciqf2fl4rc117r95b3mj6787pdzrsy00")))

(define-public crate-gladis-0.2.0 (c (n "gladis") (v "0.2.0") (d (list (d (n "gtk") (r "0.9.*") (d #t) (k 0)))) (h "1bi5h5q1l89y7d0mjmbirs9ksdhpw3jq5njyfsfqzc6cg06afvz1")))

(define-public crate-gladis-0.2.1 (c (n "gladis") (v "0.2.1") (d (list (d (n "gladis_proc_macro") (r "0.2.*") (d #t) (k 2)) (d (n "gtk") (r "0.9.*") (d #t) (k 0)))) (h "09nks3f3sb61mgj2j1hfny3j2l5nzq2knzsw3qydx77ic7d9lcqm")))

(define-public crate-gladis-0.3.0 (c (n "gladis") (v "0.3.0") (d (list (d (n "gladis_proc_macro") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "gladis_proc_macro") (r "0.3.*") (d #t) (k 2)) (d (n "gtk") (r "0.9.*") (d #t) (k 0)))) (h "1ija5wkafiqnpx9ff2gk03q8yfaqmclgyk5l39p1x18wwxkid6g1") (f (quote (("derive" "gladis_proc_macro") ("default" "derive"))))))

(define-public crate-gladis-0.3.1 (c (n "gladis") (v "0.3.1") (d (list (d (n "gladis_proc_macro") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "gladis_proc_macro") (r "0.3.*") (d #t) (k 2)) (d (n "gtk") (r "0.9.*") (d #t) (k 0)))) (h "1pxi8gvv3kbxa5fsj22yyh2izx6s39xwmyyhj0g66zkpzaikpkr1") (f (quote (("derive" "gladis_proc_macro") ("default" "derive"))))))

(define-public crate-gladis-0.4.0 (c (n "gladis") (v "0.4.0") (d (list (d (n "gladis_proc_macro") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "gladis_proc_macro") (r "0.4.*") (d #t) (k 2)) (d (n "gtk") (r "0.9.*") (d #t) (k 0)))) (h "0h1i8fjd7ncahih93mh2b0kl6ypxdbyynv7jcawmq6mkv014pnbi") (f (quote (("derive" "gladis_proc_macro") ("default" "derive"))))))

(define-public crate-gladis-0.4.1 (c (n "gladis") (v "0.4.1") (d (list (d (n "gladis_proc_macro") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "gladis_proc_macro") (r "0.4.*") (d #t) (k 2)) (d (n "gtk") (r "0.9.*") (d #t) (k 0)))) (h "1fiwypdchrkl539ik4rrax42325mg095r0pr2fmg88kz2rmy38sb") (f (quote (("derive" "gladis_proc_macro") ("default" "derive"))))))

(define-public crate-gladis-1.0.0 (c (n "gladis") (v "1.0.0") (d (list (d (n "gio") (r "0.14.*") (d #t) (k 2)) (d (n "gladis_proc_macro") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "gladis_proc_macro") (r "^1.0.0") (d #t) (k 2)) (d (n "gtk") (r "0.14.*") (d #t) (k 0)))) (h "09rk32d7abkp9g5z1ray99srymm1bfpzz8r6pl5kbxa6qkpm1l3f") (f (quote (("derive" "gladis_proc_macro") ("default" "derive"))))))

(define-public crate-gladis-1.0.1 (c (n "gladis") (v "1.0.1") (d (list (d (n "gio") (r "0.14.*") (d #t) (k 2)) (d (n "gladis_proc_macro") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "gladis_proc_macro") (r "^1.0.1") (d #t) (k 2)) (d (n "gtk") (r "0.14.*") (d #t) (k 0)) (d (n "relm") (r "^0.22.0") (d #t) (k 2)) (d (n "relm-derive") (r "^0.22.0") (d #t) (k 2)))) (h "1jgfgqmryklb8b0ga5qni14cxb1kdldb0zb2ll211dvc176cx2dh") (f (quote (("derive" "gladis_proc_macro") ("default" "derive"))))))

(define-public crate-gladis-2.0.0 (c (n "gladis") (v "2.0.0") (d (list (d (n "gio") (r "^0.15") (d #t) (k 2)) (d (n "gladis_proc_macro") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "gladis_proc_macro") (r "^2.0.0") (d #t) (k 2)) (d (n "gtk") (r "^0.15") (d #t) (k 0)) (d (n "relm") (r "^0.23") (d #t) (k 2)) (d (n "relm-derive") (r "^0.23") (d #t) (k 2)))) (h "0h3l31xk98rqmh1ajcyirhmy7hcmyryjc9wzxv2pbhq2kxg4v73z") (f (quote (("derive" "gladis_proc_macro") ("default" "derive"))))))

(define-public crate-gladis-2.1.0 (c (n "gladis") (v "2.1.0") (d (list (d (n "gio") (r "^0.15") (d #t) (k 2)) (d (n "gladis_proc_macro") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "gladis_proc_macro") (r "^2.1.0") (d #t) (k 2)) (d (n "gtk") (r "^0.15") (d #t) (k 0)) (d (n "relm") (r "^0.23") (d #t) (k 2)) (d (n "relm-derive") (r "^0.23") (d #t) (k 2)))) (h "0lk5c1rg2xfz51giz4wqd6r53qhs485m8k2k331dl4apfpi5yfsk") (f (quote (("derive" "gladis_proc_macro") ("default" "derive"))))))

(define-public crate-gladis-2.1.1 (c (n "gladis") (v "2.1.1") (d (list (d (n "gio") (r "^0.15") (d #t) (k 2)) (d (n "gladis_proc_macro") (r "^2.1.1") (o #t) (d #t) (k 0)) (d (n "gladis_proc_macro") (r "^2.1.1") (d #t) (k 2)) (d (n "gtk") (r "^0.15") (d #t) (k 0)) (d (n "relm") (r "^0.23") (d #t) (k 2)) (d (n "relm-derive") (r "^0.23") (d #t) (k 2)))) (h "0r8qfm78gir7hq6hlkf2kmdcnj5q41rx5scb1vxk1d325by00lwj") (f (quote (("derive" "gladis_proc_macro") ("default" "derive"))))))

(define-public crate-gladis-2.1.2 (c (n "gladis") (v "2.1.2") (d (list (d (n "gio") (r "^0.15") (d #t) (k 2)) (d (n "gladis_proc_macro") (r "^2.1.2") (o #t) (d #t) (k 0)) (d (n "gladis_proc_macro") (r "^2.1.2") (d #t) (k 2)) (d (n "gtk") (r "^0.15") (d #t) (k 0)) (d (n "relm") (r "^0.23") (d #t) (k 2)) (d (n "relm-derive") (r "^0.23") (d #t) (k 2)))) (h "09z959rv8f6r5z1sqxssjz1vn9cnja1yqgmyihs30jg1xj7q8asy") (f (quote (("derive" "gladis_proc_macro") ("default" "derive"))))))

