(define-module (crates-io gl ad glade-bindgen-gtk4) #:use-module (crates-io))

(define-public crate-glade-bindgen-gtk4-1.2.1 (c (n "glade-bindgen-gtk4") (v "1.2.1") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1m2qqlls95pcbd1dlhzw8qbvcvqrjsmfrfliy885zavcg283zy8h")))

