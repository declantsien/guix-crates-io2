(define-module (crates-io gl ad glad-sys) #:use-module (crates-io))

(define-public crate-glad-sys-0.0.0 (c (n "glad-sys") (v "0.0.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "0xqr071s1f8brl5na8mk3gr2rq7cikysj0yr82n6xa3waf83fkhn") (y #t)))

(define-public crate-glad-sys-0.0.1 (c (n "glad-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "04gh18aqcg6bnsw1g9ck2vc9nyhwrig2dcqg41bcxz1cmcvbmbrn") (y #t)))

(define-public crate-glad-sys-0.0.2 (c (n "glad-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "14p0lwlr10am6l8rwpxiwas9h8kk0nwz8danhiwibvsx4i5jagwb") (y #t)))

(define-public crate-glad-sys-0.0.3 (c (n "glad-sys") (v "0.0.3") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "0igipk6jmp1517y4cmgpbjnxyd7ci9n96vsqv4l24dkl173314nc") (y #t) (l "glad")))

(define-public crate-glad-sys-0.0.4 (c (n "glad-sys") (v "0.0.4") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "1di2xh9ciclwj3rial9vmajk43s27llxzrdybgsdz7bqf5aracc4") (y #t) (l "glad")))

(define-public crate-glad-sys-0.0.5 (c (n "glad-sys") (v "0.0.5") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2.46") (d #t) (k 0)))) (h "1miz4n8aczjwkrfbhsj1x1izblrd9j70sxqhrlxr61wawqyf7cja") (y #t) (l "glad")))

(define-public crate-glad-sys-0.0.6 (c (n "glad-sys") (v "0.0.6") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "1q3w6dc8w4ml283l1j2q0y34qmhcm6nyqsj9zn6mcw72xfq37zzr") (y #t) (l "glad")))

