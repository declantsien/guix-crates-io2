(define-module (crates-io gl ad glade) #:use-module (crates-io))

(define-public crate-glade-0.1.0-alpha (c (n "glade") (v "0.1.0-alpha") (d (list (d (n "amplify") (r "~0.1.0") (d #t) (k 0)) (d (n "derive_wrapper") (r "~0.1.6") (d #t) (k 0)))) (h "17y7fhvc933dv11gqxs6xzn6sqi9zzcxxkyxvnk7pjr117jsmzxv")))

(define-public crate-glade-0.1.0-alpha.2 (c (n "glade") (v "0.1.0-alpha.2") (d (list (d (n "amplify") (r "~2.3.1") (d #t) (k 0)) (d (n "amplify_derive") (r "~2.3.1") (d #t) (k 0)))) (h "1yv0vwnr68si4npmrv87hh0yir47kranr2cp8nig0kckyk5djfjd")))

(define-public crate-glade-0.1.0-alpha.3 (c (n "glade") (v "0.1.0-alpha.3") (d (list (d (n "amplify") (r "~2.4.0") (d #t) (k 0)) (d (n "amplify_derive") (r "~2.4.2") (d #t) (k 0)))) (h "1qfmg0jpfrh6j64bp2d79avx7471p2ncnlwgs4001qciggdr247w")))

(define-public crate-glade-0.1.0-alpha.4 (c (n "glade") (v "0.1.0-alpha.4") (d (list (d (n "amplify") (r "^3") (d #t) (k 0)) (d (n "amplify_derive") (r "^2") (d #t) (k 0)))) (h "16q13lfkqpxi63ar0zzks91pfp4s6n53kiqxn78wgzmii4xgl1sw")))

(define-public crate-glade-2.2.0 (c (n "glade") (v "2.2.0") (d (list (d (n "gio") (r "^0.16.7") (d #t) (k 2)) (d (n "glade_derive") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "glade_derive") (r "^2.2.0") (d #t) (k 2)) (d (n "gtk") (r "^0.16.2") (d #t) (k 0)) (d (n "relm") (r "^0.24.1") (d #t) (k 2)) (d (n "relm-derive") (r "^0.24.0") (d #t) (k 2)))) (h "0grgd0id9r9prkn6349wgfxk3z889ybss0ang50wfx4xfd98xcf9") (f (quote (("derive" "glade_derive") ("default" "derive"))))))

