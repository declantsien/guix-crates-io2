(define-module (crates-io gl ad glade_derive) #:use-module (crates-io))

(define-public crate-glade_derive-0.1.0-alpha (c (n "glade_derive") (v "0.1.0-alpha") (d (list (d (n "amplify_derive") (r "~0.1.0") (d #t) (k 0)) (d (n "glade") (r "~0.1.0-alpha") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1jn2mh1cimwaiyl93wiycsrhlwyvn9nj564k1gllw6zlcxw1bdr0")))

(define-public crate-glade_derive-2.2.0 (c (n "glade_derive") (v "2.2.0") (d (list (d (n "gtk") (r "^0.16.2") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wc4fvlgjpfk3z17j3xzs85nb469ihjazfqzlyi8i1nk7cvafxkj")))

