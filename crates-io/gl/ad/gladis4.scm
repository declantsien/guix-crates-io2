(define-module (crates-io gl ad gladis4) #:use-module (crates-io))

(define-public crate-gladis4-0.1.0 (c (n "gladis4") (v "0.1.0") (d (list (d (n "gio") (r "^0.15") (d #t) (k 2)) (d (n "gladis_proc_macro") (r "^0.1.0") (o #t) (d #t) (k 0) (p "gladis4_proc_macro")) (d (n "gladis_proc_macro") (r "^0.1.0") (d #t) (k 2) (p "gladis4_proc_macro")) (d (n "gtk") (r "^0.4") (d #t) (k 0) (p "gtk4")) (d (n "relm4") (r "^0.4") (d #t) (k 2)) (d (n "relm4-components") (r "^0.4") (d #t) (k 2)) (d (n "relm4-macros") (r "^0.4") (d #t) (k 2)))) (h "18zpl4da9d0izc6slg5h7zm61x3r9yppkr9vrgz09pyqrpj8qn8w") (f (quote (("derive" "gladis_proc_macro") ("default" "derive"))))))

