(define-module (crates-io gl ad glade-bindgen) #:use-module (crates-io))

(define-public crate-glade-bindgen-0.1.0 (c (n "glade-bindgen") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0v004j9xrkjnn9z32wkm48891d21njlfs59f0svxrl3amvd5wza1")))

(define-public crate-glade-bindgen-0.2.0 (c (n "glade-bindgen") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0f704y137padgypziy4hj71c07widi05ddaz48bbp760pk5fcd9n")))

(define-public crate-glade-bindgen-0.2.1 (c (n "glade-bindgen") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "12vmqlxlh97jx0q67sa436f9gax25l63s041pgl5j7irqh421lxa")))

(define-public crate-glade-bindgen-0.2.2 (c (n "glade-bindgen") (v "0.2.2") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "05jn37kix0i0kq120spl34d30xmbx4ij05plfyd0i1c31ynr2264")))

(define-public crate-glade-bindgen-1.0.0 (c (n "glade-bindgen") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0aij61cmaqbk5284rjaf4psazwvdqi3b2gfcq10dfxab28dn3c8r")))

(define-public crate-glade-bindgen-1.1.0 (c (n "glade-bindgen") (v "1.1.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "157b9r876cjnja9jjynzx8l125f7lk5k11s6g5crhin8lrg4vysi")))

(define-public crate-glade-bindgen-1.2.0 (c (n "glade-bindgen") (v "1.2.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0l4s0w0x95apmrpm1sq536d97vgryg3nc8gq4qmx5hmy1aps21bg")))

(define-public crate-glade-bindgen-1.2.1 (c (n "glade-bindgen") (v "1.2.1") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "13arl5qvr056ph738nijhvwy47z17kwfra00pm9xffq879lgnm24")))

