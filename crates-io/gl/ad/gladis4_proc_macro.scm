(define-module (crates-io gl ad gladis4_proc_macro) #:use-module (crates-io))

(define-public crate-gladis4_proc_macro-0.1.0 (c (n "gladis4_proc_macro") (v "0.1.0") (d (list (d (n "gtk") (r "^0.4") (d #t) (k 2) (p "gtk4")) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07z3h63r196frx7hfz90yx2hp3b5dzvw2p9qj9vw0r1qvwldmb4k")))

