(define-module (crates-io gl es glessite) #:use-module (crates-io))

(define-public crate-glessite-0.2.0 (c (n "glessite") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1i2zq392w7q4slkpxpgx64gq47lwkvybwmylknnj7p2l8bcfvfdl")))

(define-public crate-glessite-0.2.1 (c (n "glessite") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1vx76s6yxrvjqljfkvi8kmzi9g625d9f3n76hjyk55s3054zqrh6")))

