(define-module (crates-io gl es gles31) #:use-module (crates-io))

(define-public crate-gles31-1.0.0 (c (n "gles31") (v "1.0.0") (d (list (d (n "beryllium") (r "^0.0.19") (d #t) (k 2)))) (h "12i6i9gpj86gnhq0m0697mikwrwq3i0bpbp5m5z48i6mjc5n5n00")))

(define-public crate-gles31-1.0.1 (c (n "gles31") (v "1.0.1") (d (list (d (n "beryllium") (r "^0.0.19") (d #t) (k 2)))) (h "0n8m4czmc3fcszjdsz2qa31krs3hjrmy1sm9lbccynspf55606z8")))

