(define-module (crates-io gl id glidertracker-status) #:use-module (crates-io))

(define-public crate-glidertracker-status-0.1.0 (c (n "glidertracker-status") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ws") (r "^0.7") (d #t) (k 0)))) (h "05b5vmxfqhz5vvvff2gvqd1i8d79y14j051833bgyhk3a43wxbc2")))

