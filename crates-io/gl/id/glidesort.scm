(define-module (crates-io gl id glidesort) #:use-module (crates-io))

(define-public crate-glidesort-0.1.0 (c (n "glidesort") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "1k6v175yq4wplxh0s8n5r4jhvw98vkyz01wm9crbkw2qjx1gbw8i") (f (quote (("unstable") ("tracking" "lazy_static"))))))

(define-public crate-glidesort-0.1.1 (c (n "glidesort") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "0c8vswczki59xrygk8p7pz3mxvq76li9jrg5mr0236zv8dmfmfda") (f (quote (("unstable") ("tracking" "lazy_static"))))))

(define-public crate-glidesort-0.1.2 (c (n "glidesort") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "1q79y8qgpf75y8sx51qb1858h5q48vj63hbg305kwkb4xgk05qgj") (f (quote (("unstable") ("tracking" "lazy_static"))))))

