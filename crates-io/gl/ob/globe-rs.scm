(define-module (crates-io gl ob globe-rs) #:use-module (crates-io))

(define-public crate-globe-rs-0.1.0 (c (n "globe-rs") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "0dvnnhr41xd0fw4pl616qp0xzq8nvs3lv01qf33h6pz7illk7lkg")))

(define-public crate-globe-rs-0.1.1 (c (n "globe-rs") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "1g2v2902n6dj2b9sprxczf9ynlkp9v7rkbv6h7v1jy2wyhs9rkfi")))

(define-public crate-globe-rs-0.1.2 (c (n "globe-rs") (v "0.1.2") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "1yi0rvpc7vwqml6xh69zjas9kw45x5kdqg204xqyd4snahrvpzqi")))

(define-public crate-globe-rs-0.1.3 (c (n "globe-rs") (v "0.1.3") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "042bz3vaa4zjj61sdy2xd7f0iddkwlmxdw99ndra626g4pdfrbff")))

(define-public crate-globe-rs-0.1.4 (c (n "globe-rs") (v "0.1.4") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "1bdl76v3sv3wppfcdk3j4sssmsli824fp3dkl3qwssbr05gwidzv")))

(define-public crate-globe-rs-0.1.5 (c (n "globe-rs") (v "0.1.5") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "1sq1qgazx1zzrvvj4plxh4rg42bc7p9f1g70jx0w558hbhjjwyz6")))

(define-public crate-globe-rs-0.1.6 (c (n "globe-rs") (v "0.1.6") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "0lzqmwi60brdx77sczfs9b6y3mgm9xqfiy71j5gnvrgfp7gyr88w")))

(define-public crate-globe-rs-0.1.7 (c (n "globe-rs") (v "0.1.7") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "0jjybmx19s2z9yvy3lm31vmh3qh74s3b6y9hrsdi3z2gaw09kn2y")))

(define-public crate-globe-rs-0.1.8 (c (n "globe-rs") (v "0.1.8") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "0g898vxrd6bnwb8gl22immxgmvkpzwiz68i7m7z23qpmwg66iqfq")))

