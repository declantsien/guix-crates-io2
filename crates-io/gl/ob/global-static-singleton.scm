(define-module (crates-io gl ob global-static-singleton) #:use-module (crates-io))

(define-public crate-global-static-singleton-0.1.0 (c (n "global-static-singleton") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "18l460qmr0n9261s75gb6wmx4znhs51y8d6ixi0vxn701gm9521l")))

(define-public crate-global-static-singleton-0.1.1 (c (n "global-static-singleton") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0m20wm205wwqjs5gy5r3r8c87hbjcj42dhw66mw1adrq9mq939bd")))

(define-public crate-global-static-singleton-0.1.2 (c (n "global-static-singleton") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "10g81vzj7llxpmwwljhzjv82s7j40b1jfmxz056rzaf922ilhij8")))

(define-public crate-global-static-singleton-0.2.0 (c (n "global-static-singleton") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0w5f26kh6rrhbz592rl4b7d0qrbwz3y1whl7m914wgdjwx2aq5m3")))

