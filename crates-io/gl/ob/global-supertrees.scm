(define-module (crates-io gl ob global-supertrees) #:use-module (crates-io))

(define-public crate-global-supertrees-0.1.0 (c (n "global-supertrees") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1im3fh2yf7xj4avc2m4xr933xp0gw5rx4n9kdk4r0iiwkyxg9khj") (f (quote (("singlecore"))))))

