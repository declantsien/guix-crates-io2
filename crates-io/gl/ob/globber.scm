(define-module (crates-io gl ob globber) #:use-module (crates-io))

(define-public crate-globber-0.1.0 (c (n "globber") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0s1w5p0176jwl9pdlsbh5854qg9lh8v2bq2p7fx8a27jl8myinnn")))

(define-public crate-globber-0.1.1 (c (n "globber") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "158hpxs1bm193v2phx28sdpc6l8iwzfxplcw9nip5qgiymnlc4mf")))

(define-public crate-globber-0.1.2 (c (n "globber") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "02h353f25gmlfn9j3k7nky04nxcl2q3yd2rnkjrsdj6kwg4l6fbx")))

(define-public crate-globber-0.1.3 (c (n "globber") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0hm9j8a4flbfalknhr662q8hp0id4n55l2qjlqg5vfk8z5v33fik")))

