(define-module (crates-io gl ob global-secrets-manager) #:use-module (crates-io))

(define-public crate-global-secrets-manager-0.1.1 (c (n "global-secrets-manager") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mnla3147d3dd9npqxsk75z25jkli20jffxnk3g6j24x6xmj8jxl")))

(define-public crate-global-secrets-manager-0.1.2 (c (n "global-secrets-manager") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p53zhjvyw475imgncbjm2pcpxn9b932n8prm6srcq2qx8fvasqb")))

(define-public crate-global-secrets-manager-0.1.3 (c (n "global-secrets-manager") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03hnhjz4a43hzxakb1wn6cmsrmgl9d7hj618hga7cx7cklcp5nkk")))

