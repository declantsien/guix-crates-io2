(define-module (crates-io gl ob global-executor) #:use-module (crates-io))

(define-public crate-global-executor-0.0.0 (c (n "global-executor") (v "0.0.0") (h "0a60263mkik8q4i7bc012gp63gcixxvqnwphsrsjzw4cpqdbdp5f")))

(define-public crate-global-executor-0.1.0 (c (n "global-executor") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.4") (d #t) (k 0)) (d (n "async-global-executor") (r "^1.4") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 2)) (d (n "blocking") (r "^1.0") (d #t) (k 2)) (d (n "executor-trait") (r "^0.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)))) (h "1m6p33s7rshxs7s8kmq6byh1d20ysg17nb0n4xrjb4m8kvxbkx5v")))

