(define-module (crates-io gl ob globenv) #:use-module (crates-io))

(define-public crate-globenv-0.1.0 (c (n "globenv") (v "0.1.0") (d (list (d (n "winreg") (r "^0.51.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0x343x6halg3hky03da35pg7cmjnnkb5mmzlvd8d0ll42n09ykyc")))

(define-public crate-globenv-0.2.0 (c (n "globenv") (v "0.2.0") (d (list (d (n "winreg") (r "^0.51.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0nfpydhd1fwqh20caqdy4r4i2zf2a49y4xvxp26lf3ijnb983ipd")))

(define-public crate-globenv-0.2.1 (c (n "globenv") (v "0.2.1") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1s0lwwippz2j8zil2n0hm11nl6lwz1g2jg1ins907an6zhm9q0d7")))

