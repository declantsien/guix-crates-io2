(define-module (crates-io gl ob globals) #:use-module (crates-io))

(define-public crate-globals-0.0.1 (c (n "globals") (v "0.0.1") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)))) (h "11y2sgzm9w2r61nj6dan6v20vlp6h186320a6bmm3mwkxvwzwx31")))

(define-public crate-globals-0.0.2 (c (n "globals") (v "0.0.2") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)))) (h "09izcym3jnkp5xw7nk2k5pyjc4845shjbkmg2lf0zs7r3fbviqpc")))

(define-public crate-globals-0.0.3 (c (n "globals") (v "0.0.3") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)))) (h "1z70bjy8kmfj52lic5jxqxxz1bv9vvp8fylv29qwyqv72agl8f85")))

(define-public crate-globals-0.0.4 (c (n "globals") (v "0.0.4") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)))) (h "0izxg3mhn1d63ds85ydsdn4jwbrag7d72fqrwgy9pdlisz0c1xba")))

(define-public crate-globals-0.1.0 (c (n "globals") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "103yn5c3xh6w1nqhgajisp2ra7j9i12amq0v37snkyjmygazl3bb")))

(define-public crate-globals-0.1.1 (c (n "globals") (v "0.1.1") (d (list (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "06qhwwwsbhxy1vyj4c9fbzv47vf0aa9787ixzp0yaz7zdwcjbfsj")))

(define-public crate-globals-0.1.2 (c (n "globals") (v "0.1.2") (d (list (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "05iaabzy9cildbcynxmbl3qrfwiclvymz76gz9w85f9j3bfvkwxn")))

(define-public crate-globals-0.1.3 (c (n "globals") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0f7gq8vpv7hq1y81nxvqcdf7qs1rhq6xi56pd1wvmv54zalq58yq")))

(define-public crate-globals-1.0.0 (c (n "globals") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0hcwcyh9pqmfdv39311hgn349zv5b2r92y4080241h9rrxyzg2h5")))

(define-public crate-globals-1.0.1 (c (n "globals") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0jq81nfgkg4k3n2cwigh11gh6d1haf5nsjz7cyzxlig9kbw07i6l")))

(define-public crate-globals-1.0.2 (c (n "globals") (v "1.0.2") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0ppah3krdrwkz4rvi9wv8sk2l7fg607lsgizlpv8mdl0xn05zydd")))

(define-public crate-globals-1.0.3 (c (n "globals") (v "1.0.3") (d (list (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0xdki6vijvbynl189rvy6pbx1f56bpicq9gvl1y7z04vdi9wx81q") (r "1.63")))

(define-public crate-globals-1.0.4 (c (n "globals") (v "1.0.4") (d (list (d (n "spin") (r "^0") (d #t) (k 0)))) (h "0fpwzc616a4wnjvv3hp635m6ykxp4a72gkym3hy9ji8hs9pja42c") (r "1.63")))

(define-public crate-globals-1.0.5 (c (n "globals") (v "1.0.5") (d (list (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "0mggvpvlzy0pjp9h4gg1sa2mh8g0nx6gzyfd9m69cpzkhbsh26z6") (r "1.63")))

