(define-module (crates-io gl ob globe-cli) #:use-module (crates-io))

(define-public crate-globe-cli-0.1.0 (c (n "globe-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("std"))) (k 0)) (d (n "crossterm") (r "^0.17.7") (d #t) (k 0)) (d (n "globe") (r "^0.1.0") (d #t) (k 0)))) (h "1rxhbi2zz7sxs9p5yjh90njahvnky3b6mxfini2apaajrifz4sc3")))

(define-public crate-globe-cli-0.1.1 (c (n "globe-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("std"))) (k 0)) (d (n "crossterm") (r "^0.17.7") (d #t) (k 0)) (d (n "globe") (r "^0.1.1") (d #t) (k 0)))) (h "1hl6y8fk63z6x8ji8b9pzzk8rwq22y5n9lmng1ssab3v1ydvyak2")))

(define-public crate-globe-cli-0.1.2 (c (n "globe-cli") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("std"))) (k 0)) (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "globe") (r "^0.1.2") (d #t) (k 0)))) (h "0yq3aq74m42i8n2bgxfx2z8ghfa6zpsc2j72nr816qxlk61dl194")))

(define-public crate-globe-cli-0.2.0 (c (n "globe-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("std"))) (k 0)) (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "globe") (r "^0.2.0") (d #t) (k 0)))) (h "0cb98hsrbw0xcl28w9nv5zqig65c8iscj27z5qavvw3ja0z7nlfd")))

