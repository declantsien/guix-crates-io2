(define-module (crates-io gl ob globox) #:use-module (crates-io))

(define-public crate-globox-0.1.0 (c (n "globox") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "imagesize") (r "^0.10.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "0qsv8lnza30kj6dgw8y2wi026v4kmvk773z1kwrfbg2k1qb2h2fl")))

