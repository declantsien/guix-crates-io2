(define-module (crates-io gl ob global-duration) #:use-module (crates-io))

(define-public crate-global-duration-0.1.0 (c (n "global-duration") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0yxc1ak3bxaxr4sybjc7bqvkzzk12rg61bbjl902p7fya0ixmh3d")))

(define-public crate-global-duration-0.2.0 (c (n "global-duration") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0y7rrn0pywqarkhy3y7nx9w8c4hzqadcl52wb2g335wl9887jhrf")))

(define-public crate-global-duration-0.2.1 (c (n "global-duration") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ccb41qhkgwz6i573kd1j8nb8l9skrcyzmgyhn0vssmx1jdp5b15")))

