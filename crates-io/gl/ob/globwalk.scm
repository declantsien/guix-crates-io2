(define-module (crates-io gl ob globwalk) #:use-module (crates-io))

(define-public crate-globwalk-0.1.0 (c (n "globwalk") (v "0.1.0") (d (list (d (n "globset") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1h2irpy9kxl06wbbwy0xpk1pcz258rck6m01xk7f1vi7r0iyk3da")))

(define-public crate-globwalk-0.2.0 (c (n "globwalk") (v "0.2.0") (d (list (d (n "globset") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1kzslwx49cgrx8y7mvlckpqw8h6vgki94ypnxj9chqfm2i80h043")))

(define-public crate-globwalk-0.3.0 (c (n "globwalk") (v "0.3.0") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0kh6f4iakz1n9nxw6d8bl9f03zabnmdjfcbj28v98gadqkf0b57h")))

(define-public crate-globwalk-0.3.1 (c (n "globwalk") (v "0.3.1") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1fvvc7hpyhzxjiwn71yd0wnjk1ah5nzjpjdy5ixm0nzkikd08pff")))

(define-public crate-globwalk-0.4.0 (c (n "globwalk") (v "0.4.0") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0sqfgjz3il5g22hbql8fhnfxhyvnhzzy3hn0ycrc23xcknn17nhc")))

(define-public crate-globwalk-0.5.0 (c (n "globwalk") (v "0.5.0") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "09axyql26s09z60sgi3y3lkin9swy2b5km3b0v6mm84xhlljxyl9")))

(define-public crate-globwalk-0.6.0 (c (n "globwalk") (v "0.6.0") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "04kimb8xidy4hprzbxjwcb5rb98qjpg928pvilklxfy9c1r2dq2b")))

(define-public crate-globwalk-0.7.0 (c (n "globwalk") (v "0.7.0") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "06p7pq3v811x0q6gqs5x3n6pcmlk9f097rc1nh0nlxjx4g7f2zjc")))

(define-public crate-globwalk-0.7.1 (c (n "globwalk") (v "0.7.1") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "ignore") (r "^0.4.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0yr6kcm2izsjicisjk5dzy00wh7jmh823zxqlfq9fs2rd01wzjsk")))

(define-public crate-globwalk-0.7.2 (c (n "globwalk") (v "0.7.2") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "ignore") (r "^0.4.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "08anzm0sm64fqdns5almz9d8k63lng0ld8mdmagz86zymd98i6ny")))

(define-public crate-globwalk-0.7.3 (c (n "globwalk") (v "0.7.3") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "ignore") (r "^0.4.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "17f9czz46ww5f2qk9349jax7wc38bdmp46cv469plsc6qnp1gnyr")))

(define-public crate-globwalk-0.8.0 (c (n "globwalk") (v "0.8.0") (d (list (d (n "backtrace") (r "= 0.3.35") (d #t) (k 2)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "ignore") (r "^0.4.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0ihld70ngnri1qd8sd61099yfzcl6iqn17rfa102q1bl6ck710hp")))

(define-public crate-globwalk-0.8.1 (c (n "globwalk") (v "0.8.1") (d (list (d (n "backtrace") (r "=0.3.35") (d #t) (k 2)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "ignore") (r "^0.4.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1k6xwkydr7igvwjn3xkwjywk4213lcs53f576ilqz1h84jaazqwk")))

(define-public crate-globwalk-0.9.0 (c (n "globwalk") (v "0.9.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "ignore") (r "^0.4.11") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "151pi6ir9qpimjr23z5kiz7mkpqq85lsxgcyzxnk21gijcv3gfms")))

(define-public crate-globwalk-0.9.1 (c (n "globwalk") (v "0.9.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "ignore") (r "^0.4.11") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0mz7bsa66p2rrgnz3l94ac4kbklh7mq8j30iizyxjy4qyvmn1xqb")))

