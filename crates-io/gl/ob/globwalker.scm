(define-module (crates-io gl ob globwalker) #:use-module (crates-io))

(define-public crate-globwalker-0.9.0 (c (n "globwalker") (v "0.9.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1flncwyqi9q36ki81zcdvx9k55x7vp28ml7i2g2ga9bx8f68z2dq") (r "1.63")))

