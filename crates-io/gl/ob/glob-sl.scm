(define-module (crates-io gl ob glob-sl) #:use-module (crates-io))

(define-public crate-glob-sl-0.4.0 (c (n "glob-sl") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1qpzbyr87w6fcp7i5c5fzjqgynip6pnp6v12115y8605ypvbqnyb") (y #t)))

(define-public crate-glob-sl-0.4.1 (c (n "glob-sl") (v "0.4.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "02hf8fqklxqzffya8gkg97646k7kb9m5mypgqb7b34zc264qi1jy")))

(define-public crate-glob-sl-0.4.2 (c (n "glob-sl") (v "0.4.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1ri1f26ds20mv54z5y0d6qxd3hwpvlv1x1mq29jm761w52wc8bv3")))

