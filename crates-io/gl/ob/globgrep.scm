(define-module (crates-io gl ob globgrep) #:use-module (crates-io))

(define-public crate-globgrep-0.1.0 (c (n "globgrep") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "148l07kr1lrd9zamkpj28dgra8pq75n2ib6zpbdspc9swpc8md60")))

