(define-module (crates-io gl ob globgroups) #:use-module (crates-io))

(define-public crate-globgroups-0.1.0-beta.1 (c (n "globgroups") (v "0.1.0-beta.1") (d (list (d (n "chumsky") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "smallvec") (r "~2.0.0-alpha.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wxdn20h2f2v5adhqa320kgpya04d4mznwdij9c5x2ihcbasksg9")))

(define-public crate-globgroups-0.1.0-beta.2 (c (n "globgroups") (v "0.1.0-beta.2") (d (list (d (n "chumsky") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "~2.0.0-alpha.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jp6bb5r3whk0i8pxn4n2c7b34cgx5lkzzyzd492daap0kkzn9c4") (s 2) (e (quote (("serde" "dep:serde"))))))

