(define-module (crates-io gl ob glob-find) #:use-module (crates-io))

(define-public crate-glob-find-0.0.0 (c (n "glob-find") (v "0.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1w5ah34i45dpl3hckfbprr64xacmpzfw7v1byvxp1cnm3gb8149v")))

