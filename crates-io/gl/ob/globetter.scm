(define-module (crates-io gl ob globetter) #:use-module (crates-io))

(define-public crate-globetter-0.1.0 (c (n "globetter") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1p5mmpgln65qy47vii6j3vyygd41sc7kmrwypdjfgqk2b0maq7yk")))

(define-public crate-globetter-0.1.1 (c (n "globetter") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0b2k1a4yna1ydfyim8kkaywlazraf579w62j06a3vdi63xb3ppm1")))

(define-public crate-globetter-0.2.0 (c (n "globetter") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1z811rpmil9nng6b61hj6k7sg52l4gv6px8j6xa6cnfg3k2ycmmn")))

