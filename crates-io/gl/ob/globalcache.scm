(define-module (crates-io gl ob globalcache) #:use-module (crates-io))

(define-public crate-globalcache-0.2.0 (c (n "globalcache") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync" "rt" "time"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (o #t) (d #t) (k 0)))) (h "1pvmwjf05f9y4100bs2yimngl1l91rj1zzq80m06fgdk2mfbm7a6") (f (quote (("sync") ("global" "tokio" "tracing" "once_cell") ("async" "tokio"))))))

(define-public crate-globalcache-0.2.1 (c (n "globalcache") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync" "rt" "time"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (o #t) (d #t) (k 0)) (d (n "tuple") (r "^0.5.1") (d #t) (k 0)))) (h "0ipp9s91lljf5v60ryg4blh5qczgqawkmk54l34mqiryq87qxxc2") (f (quote (("sync") ("global" "tokio" "tracing" "once_cell") ("async" "tokio"))))))

(define-public crate-globalcache-0.2.2 (c (n "globalcache") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync" "rt" "time"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (o #t) (d #t) (k 0)) (d (n "tuple") (r "^0.5.1") (d #t) (k 0)))) (h "0a4hdyynkb58lmrvalwnawybwqsm6v5l189n7k52202gbgz0xm6c") (f (quote (("sync") ("global" "tokio" "tracing" "once_cell") ("async" "tokio"))))))

