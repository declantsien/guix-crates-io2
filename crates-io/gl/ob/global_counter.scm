(define-module (crates-io gl ob global_counter) #:use-module (crates-io))

(define-public crate-global_counter-0.1.0 (c (n "global_counter") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "1gjly3v60ms690rh4rsm89vdlzlngfi714whjy89f0yrmazrbfng")))

(define-public crate-global_counter-0.1.1 (c (n "global_counter") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "0xflmkbmfhnszkcilsddnb3qf1iir2fzlibqdlbaxi02rw7rqp3p")))

(define-public crate-global_counter-0.1.2 (c (n "global_counter") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "0ki1vs6f0dl726k4kfmmj7yy8vm91l9w82g2cg8nm892k74v6358")))

(define-public crate-global_counter-0.1.3 (c (n "global_counter") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "1dp3idrzrwpmfpcvwxf8rx290gas4vx840zjkwya1vdb0msrdbgz")))

(define-public crate-global_counter-0.1.4 (c (n "global_counter") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0d2k4p4aspq5jkmxvwlydnrb5w08kxmciml39747pfa6ff6lwrfw") (f (quote (("default" "parking_lot"))))))

(define-public crate-global_counter-0.1.5 (c (n "global_counter") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0c1vkidyzw9760vm2vpk258sk5d1698rnwjd591bm61x8lrd4k0b") (f (quote (("default" "parking_lot")))) (y #t)))

(define-public crate-global_counter-0.2.0 (c (n "global_counter") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1yn6xaddwdcmrh0n1ykhz046vxl4hhqb1zkv4wvbbj3nmp6cxync") (f (quote (("default" "parking_lot")))) (y #t)))

(define-public crate-global_counter-0.2.1 (c (n "global_counter") (v "0.2.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (o #t) (d #t) (k 0)))) (h "0dkaaqq4n92kmllf6xysbm5xf23k09fkkvsfyza0br8bz66d6z03") (f (quote (("default" "parking_lot"))))))

(define-public crate-global_counter-0.2.2 (c (n "global_counter") (v "0.2.2") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (o #t) (d #t) (k 0)))) (h "17shsqqymllbwdwjfw9m0682v0ykbr5bc3iclc5iwqjksfkc0xra") (f (quote (("default" "parking_lot"))))))

