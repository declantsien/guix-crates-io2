(define-module (crates-io gl ob globiter) #:use-module (crates-io))

(define-public crate-globiter-0.0.0 (c (n "globiter") (v "0.0.0") (h "109i6hy3da964rsysxcca03clpmh70pamqb00ffnjb6c9qg9hvgg")))

(define-public crate-globiter-0.1.0 (c (n "globiter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "0d6bqy3r83w2dapnmns9gwlkjfhv7lhrsc2j5xigywfwlqwcsmq7")))

