(define-module (crates-io gl ob global-static) #:use-module (crates-io))

(define-public crate-global-static-0.1.0 (c (n "global-static") (v "0.1.0") (h "0gxq2da4jdhqyzac1q9s80fi32hxgnbivwr3kkfylm5l0kdq7cph")))

(define-public crate-global-static-0.1.1 (c (n "global-static") (v "0.1.1") (h "0sfpbrr28ddhxrypxjk4zk211m1xl18g3y5xkbmxsh8kw7bs1vwf")))

(define-public crate-global-static-0.2.0 (c (n "global-static") (v "0.2.0") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "199hmfif3ch8x44mllxlrhw19xm628n36djd1lnzgdq4f287vwkv")))

(define-public crate-global-static-0.2.1 (c (n "global-static") (v "0.2.1") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0gzd2f7nk3ppi44kq49wynmvrki7faa3wj2bjdg9lwf7401n40wq") (s 2) (e (quote (("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.2.2 (c (n "global-static") (v "0.2.2") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0cxh7n750ldb1ynr3394aylhp166s1hymby1vbz1nzqnfznwjmr4") (s 2) (e (quote (("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.2.3 (c (n "global-static") (v "0.2.3") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0l3b4zqy7ryzz7rah41l7p84vs3xjcr47ssdy7h5fma9z38igb62") (s 2) (e (quote (("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.2.4 (c (n "global-static") (v "0.2.4") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0300hyzlk65laaqvyl1832mxql3pl29kirjj3f11bfbs3481h3zw") (s 2) (e (quote (("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.2.5 (c (n "global-static") (v "0.2.5") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0r8kyh2blc1l889x9a8pnmpa2088v9mj93f7gi96hmd9prahrhry") (s 2) (e (quote (("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.2.6 (c (n "global-static") (v "0.2.6") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0nry4nqikijy9b26blpl43awi7hcdd2vg7lrnrdmirwi06n1nbr4") (s 2) (e (quote (("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.3.0 (c (n "global-static") (v "0.3.0") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0fnamzbwi0d56zx22b86ih8zlv7335sb4bp5nk6j6pk70idycrfj") (s 2) (e (quote (("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.3.1 (c (n "global-static") (v "0.3.1") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "005j3104zhxq9imy08rfq8z0qpr2anz0244j9f62vqsi38avaq6f") (s 2) (e (quote (("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.4.0 (c (n "global-static") (v "0.4.0") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0gwzap477fv20chxjbcc91d5jildwhxhmvl6cbxh0c3nf16vxm1c") (y #t) (s 2) (e (quote (("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.4.1 (c (n "global-static") (v "0.4.1") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0maji7w0dn851isy87iy1gssfsardiawcv2yym01ywqkrpb1lsh5") (y #t) (s 2) (e (quote (("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.4.2 (c (n "global-static") (v "0.4.2") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "singleton") (r "^0.1.0") (o #t) (d #t) (k 0) (p "global-static-singleton")))) (h "0zs39ggg5pkjrs4qx3j28j26yrw1fyl2g5s6cchphhw9jm1hhrzg") (s 2) (e (quote (("singleton" "dep:singleton" "ctor") ("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.4.3 (c (n "global-static") (v "0.4.3") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "singleton") (r "^0.1.1") (o #t) (d #t) (k 0) (p "global-static-singleton")))) (h "0kg14c4xy87b38jb3a8dkbc8ry2h1nxh5w8v711vw248d48xs2k1") (s 2) (e (quote (("singleton" "dep:singleton" "ctor") ("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.4.4 (c (n "global-static") (v "0.4.4") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "singleton") (r "^0.1.1") (o #t) (d #t) (k 0) (p "global-static-singleton")))) (h "123lbb2n2yrcw48lgxhsq1922mlx5dpdr2iwv2m66ri7i1q8wjkr") (s 2) (e (quote (("singleton" "dep:singleton" "ctor") ("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.5.0 (c (n "global-static") (v "0.5.0") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "singleton") (r "^0.1.1") (o #t) (d #t) (k 0) (p "global-static-singleton")))) (h "0ydnab59d1vs7bz2jblgzwwh2a0la0hf1k7k02ajmjnxml9idg25") (s 2) (e (quote (("singleton" "dep:singleton" "ctor") ("ctor" "dep:ctor"))))))

(define-public crate-global-static-0.5.1 (c (n "global-static") (v "0.5.1") (d (list (d (n "ctor") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "singleton") (r "^0.2.0") (o #t) (d #t) (k 0) (p "global-static-singleton")))) (h "04yzd965wxps9s6mdnl09ws32whnnx6pjh6jw3ds04fva4zga0r5") (s 2) (e (quote (("singleton" "dep:singleton" "ctor") ("ctor" "dep:ctor"))))))

