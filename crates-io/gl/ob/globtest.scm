(define-module (crates-io gl ob globtest) #:use-module (crates-io))

(define-public crate-globtest-0.1.0 (c (n "globtest") (v "0.1.0") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)))) (h "01ri49pbjddcii375alkgxcw46c4v31frzwxgilb0xkx9i2yqp7b")))

(define-public crate-globtest-0.1.1 (c (n "globtest") (v "0.1.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)))) (h "17hjggqz8w81z0qp03blj4cinddr8jzw016sgd2nrg423w87gv13")))

