(define-module (crates-io gl ob globreeks) #:use-module (crates-io))

(define-public crate-globreeks-0.1.0 (c (n "globreeks") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "globset") (r "^0.4.13") (d #t) (k 0)))) (h "0qyxmiwna226v19yxbxnixicc8pp7jyzsxfv931rhcjzn4gsljvp")))

(define-public crate-globreeks-0.1.1 (c (n "globreeks") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "globset") (r "^0.4.13") (d #t) (k 0)))) (h "0dh27p5jgfglhdwskjbsixvfwcgiy5wsdbxa1chpz0hz5k5m0qrw")))

