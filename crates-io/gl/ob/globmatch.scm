(define-module (crates-io gl ob globmatch) #:use-module (crates-io))

(define-public crate-globmatch-0.1.0 (c (n "globmatch") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "globset") (r "^0.4.8") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "00ji1ncxy1y1aj3cxw73svyfiaxfw8rsq7bg8f39mzjx5wjn4yg4")))

(define-public crate-globmatch-0.1.1 (c (n "globmatch") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "globset") (r "^0.4.8") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0rcm1wmfpa870w8mgrid2jwkhxvap0y15j2mf1n4hkh0vs1dliz9")))

(define-public crate-globmatch-0.1.2 (c (n "globmatch") (v "0.1.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "globset") (r "^0.4.8") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "14hqmmswraab4s8j4k41r1sn1kwfb31yyi3k11m4015i6ng6fdxb")))

(define-public crate-globmatch-0.1.3 (c (n "globmatch") (v "0.1.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "globset") (r "^0.4.8") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1sya9gfj2dr0a75av0v9nzqv51rw5rhgama634shwzpx8748q5dh")))

(define-public crate-globmatch-0.1.4 (c (n "globmatch") (v "0.1.4") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "globset") (r "^0.4.8") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0xgxjwg0h31k0cwci6lzm91sj6pdb443yah8r83gsjgpfbpnbn1f")))

(define-public crate-globmatch-0.2.0 (c (n "globmatch") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "globset") (r "^0.4.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1q8mpq8qg675jadqp6ws2gbz969g5mifakw6mz12s4cn4jknm6s6")))

(define-public crate-globmatch-0.2.1 (c (n "globmatch") (v "0.2.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "globset") (r "^0.4.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "08i4ib9x4r4fpyaimcdq2sfxas2ax0vqjnb70qhj6jagjr7fbg2q")))

(define-public crate-globmatch-0.2.3 (c (n "globmatch") (v "0.2.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0qzfag84k000fqfdr0l2jsq12anjyzbkrgk14977zlad6lpdyby9")))

(define-public crate-globmatch-0.2.4 (c (n "globmatch") (v "0.2.4") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "15v06zvqas0dchqnnn0kmr9rbvgqzzcacf8sjk1y0v73smvjzs70")))

(define-public crate-globmatch-0.2.5 (c (n "globmatch") (v "0.2.5") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1809rjlpyq8g55fvz7knzcy0klzfw4cffpw5afzjnaycg5ihfm9p")))

(define-public crate-globmatch-0.3.0 (c (n "globmatch") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1n101a48xnyqnmqlyil3p2c36lqipfgrbn03jgkvrqvhdq61bm65")))

