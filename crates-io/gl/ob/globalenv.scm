(define-module (crates-io gl ob globalenv) #:use-module (crates-io))

(define-public crate-globalenv-0.1.0 (c (n "globalenv") (v "0.1.0") (d (list (d (n "winreg") (r "^0.7") (d #t) (k 2)))) (h "0yx78dbyqiwbzi5z1ys16jqi92rwgk7ax6f7wgswxx0cs254jsry")))

(define-public crate-globalenv-0.2.0 (c (n "globalenv") (v "0.2.0") (d (list (d (n "winreg") (r "^0.7") (d #t) (k 0)))) (h "1y0vbbhpaam4r60wrpy6bzrp2zjq0pwmc3yh7ycq6mlvdp8bjnzy")))

(define-public crate-globalenv-0.3.0 (c (n "globalenv") (v "0.3.0") (d (list (d (n "winreg") (r "^0.7") (d #t) (k 0)))) (h "1xai2bp9waszk8m1x18xbafvyz6c0j7vy5w768n9kkx3n9zrnc01")))

(define-public crate-globalenv-0.4.0 (c (n "globalenv") (v "0.4.0") (d (list (d (n "winreg") (r "^0.7") (d #t) (t "cfg(windows)") (k 0)))) (h "1xjrf1dx47211wsm5f5hn87gaslzsz0m4qc5583w7rcfwqhvwd64")))

(define-public crate-globalenv-0.4.1 (c (n "globalenv") (v "0.4.1") (d (list (d (n "winreg") (r "^0.7") (d #t) (t "cfg(windows)") (k 0)))) (h "0x0rfznnfnq4l5gacvgd6iprmaaz6h23r3whq4s4wg45akkl4y22")))

(define-public crate-globalenv-0.4.2 (c (n "globalenv") (v "0.4.2") (d (list (d (n "winreg") (r "^0.7") (d #t) (t "cfg(windows)") (k 0)))) (h "1fbpxqf7fcwvwmvndn8l2k3khps3isb3iyc7ccav77kb2jw9msmb")))

