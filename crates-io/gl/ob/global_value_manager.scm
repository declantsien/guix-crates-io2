(define-module (crates-io gl ob global_value_manager) #:use-module (crates-io))

(define-public crate-global_value_manager-0.1.0 (c (n "global_value_manager") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1q11w84q77cc9vn5fpcb51xpl7rhpg6frjz2x6jmbcn1k11gw6gx")))

(define-public crate-global_value_manager-0.1.1 (c (n "global_value_manager") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0rjl1amvggasjda8g2m09gvhq8ic1k75cg3q8lgbgrpj97021mw2")))

