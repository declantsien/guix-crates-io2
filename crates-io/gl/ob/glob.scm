(define-module (crates-io gl ob glob) #:use-module (crates-io))

(define-public crate-glob-0.0.1 (c (n "glob") (v "0.0.1") (h "1ibz8vkwh9k7dcq1s9vw7shdl2zvzldxf0f72dvkb7sjswawzsf7")))

(define-public crate-glob-0.0.2 (c (n "glob") (v "0.0.2") (h "0f5vjmli06v1icn4n90dw0sal743njhh3zncdj4hljvq2yl635cl")))

(define-public crate-glob-0.0.3 (c (n "glob") (v "0.0.3") (h "1cxg805cv3j73118y33jgp8yxl1y5jbpzlysysc3rnhl782yya7k")))

(define-public crate-glob-0.1.0 (c (n "glob") (v "0.1.0") (h "0am50mqvprz444jiilw3ik7v3hhx719dghdlyvlhhwjcj7ick1qn")))

(define-public crate-glob-0.1.1 (c (n "glob") (v "0.1.1") (h "1kddzznrghk5s9820nfjkic8kaa5zx8m7khad3j69qym9kbsaywi")))

(define-public crate-glob-0.1.2 (c (n "glob") (v "0.1.2") (h "1z0lmwkr0vyvd7b9a8qfjjbrnarr0fkczbva3q78vkiz9abhfq9n")))

(define-public crate-glob-0.1.3 (c (n "glob") (v "0.1.3") (h "1kh2vwrx7j568zd6s1ck910cwcyvxpx4zmr7syyj0vhagvz1lhja")))

(define-public crate-glob-0.1.4 (c (n "glob") (v "0.1.4") (h "191fj6sqyi75ff6cd33rm2n98d97g7cplznh5iayxwfdz059j56z")))

(define-public crate-glob-0.1.5 (c (n "glob") (v "0.1.5") (h "1di79lk3kdwk8fwbmv4pvhshigq6mjspcj88yvrq24cqd8rd6iya")))

(define-public crate-glob-0.1.6 (c (n "glob") (v "0.1.6") (h "0hnskbvxxil98qgndz7pym4y9kgmc2dlz8frzcwz0lbi085942m8")))

(define-public crate-glob-0.1.7 (c (n "glob") (v "0.1.7") (h "09gscyfdqmpglc9iiq4hv18mqva7z8i0iivabj5zx631cpilgm1z")))

(define-public crate-glob-0.1.8 (c (n "glob") (v "0.1.8") (h "1nr0hykyxr6a1gv880xsk61a31nld3bzdambfykmi78v7x7cimhc")))

(define-public crate-glob-0.1.9 (c (n "glob") (v "0.1.9") (h "1jylscmvbyjjixz1s1hyfy2qsyggnc7jdcphlqi09r1b783x1ill")))

(define-public crate-glob-0.2.0 (c (n "glob") (v "0.2.0") (h "0752aivbsg5dg9wlnx1arxx00jzq3a34yclz97xc23q94n1f74nq")))

(define-public crate-glob-0.2.1 (c (n "glob") (v "0.2.1") (h "1bc83ly4xlp1w3483i3a5smfxn3dl8s065linjxx8w0m0bnhka8c")))

(define-public crate-glob-0.2.2 (c (n "glob") (v "0.2.2") (h "1xaniaa1fxbyck4ziwxb9pfczmcmlj8gnwvgnz8iyp7hx49a3z1i")))

(define-public crate-glob-0.2.3 (c (n "glob") (v "0.2.3") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "16bqhzp3smnm52k9xxf7dr1vn2jkvxv3651icay2l5s01qfqn6lc")))

(define-public crate-glob-0.2.4 (c (n "glob") (v "0.2.4") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0im9xa2c3nhycfyb0m8ms3lw7y6rmdl7gc98z3b8jhqn9crpp4fp")))

(define-public crate-glob-0.2.5 (c (n "glob") (v "0.2.5") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0nf8g5gcgcdc3pw99amjphrahlnysh3w7v3am34nqwz8r2r2pprj")))

(define-public crate-glob-0.2.6 (c (n "glob") (v "0.2.6") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0spyl7hgr7mbs46a6y882wn6qjvs963d67v82gv5gs0wq0w7mqq3")))

(define-public crate-glob-0.2.7 (c (n "glob") (v "0.2.7") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "11fndjc1f722n6xjxz0ahx7dwxk0d65alx7ij22vshx490431jv8")))

(define-public crate-glob-0.2.8 (c (n "glob") (v "0.2.8") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1491kff5fgi1f28m9ybzl919hvkfrjm7glw8n60hz58inapalk9y")))

(define-public crate-glob-0.2.9 (c (n "glob") (v "0.2.9") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0l5r4szza49gfx8g1v77bw67n9xymlbplh9yn656yz2nw0ljpww1")))

(define-public crate-glob-0.2.10 (c (n "glob") (v "0.2.10") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "12s0i75kyxqqp7zq41dfzsrkhidmlrmbd0bx9w6618pn5vnlfkgb")))

(define-public crate-glob-0.2.11 (c (n "glob") (v "0.2.11") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1ysvi72slkw784fcsymgj4308c3y03gwjjzqxp80xdjnkbh8vqcb")))

(define-public crate-glob-0.3.0 (c (n "glob") (v "0.3.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0x25wfr7vg3mzxc9x05dcphvd3nwlcmbnxrvwcvrrdwplcrrk4cv")))

(define-public crate-glob-0.3.1 (c (n "glob") (v "0.3.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "16zca52nglanv23q5qrwd5jinw3d3as5ylya6y1pbx47vkxvrynj")))

