(define-module (crates-io gl ob globe) #:use-module (crates-io))

(define-public crate-globe-0.1.0 (c (n "globe") (v "0.1.0") (h "1iwakf7fyg69w4fxai7z87yal72pd1a6v484kihb6qy3fm7xcrfi")))

(define-public crate-globe-0.1.1 (c (n "globe") (v "0.1.1") (h "1j9dcf9wr859hhyw4wb10hm32lc43c96z49hwjay5gzrbx8clyqr")))

(define-public crate-globe-0.1.2 (c (n "globe") (v "0.1.2") (h "0gpf4qsac2p012pqmb223sp75hmcvz1jw1rcwpjd2s3yjsrjakfr")))

(define-public crate-globe-0.2.0 (c (n "globe") (v "0.2.0") (h "0sk633h1q3krsbzygmlc4yy66l2c6qan85385d50lbylx2mmbkpf")))

