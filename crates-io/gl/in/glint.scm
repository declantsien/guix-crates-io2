(define-module (crates-io gl in glint) #:use-module (crates-io))

(define-public crate-glint-0.1.0 (c (n "glint") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.10") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9") (d #t) (k 0)))) (h "0p6mylhcppy1zvv07ap2lslzq4apvkdrlgl3fv16j1r0dj8p200v")))

(define-public crate-glint-0.1.2 (c (n "glint") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.10") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9") (d #t) (k 0)))) (h "1s92s97yrvwx94fi721cp0cb50z8b1jhw9lv5qyjahcwvrxkq12c")))

(define-public crate-glint-0.1.3 (c (n "glint") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.10") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9") (d #t) (k 0)))) (h "1szn01nifj0ib743bjz6sm8fwswrx42m355fn05gbkmvrd3ii0x9")))

(define-public crate-glint-0.1.4 (c (n "glint") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.10") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9") (d #t) (k 0)))) (h "0prjmvqkmq6pdi7x2zjfqmnnkbkx2x2zw9x3v87az9bzxy8wxdca")))

(define-public crate-glint-0.1.5 (c (n "glint") (v "0.1.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.10") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9") (d #t) (k 0)))) (h "0jccmpjly3vr0wb338f21r77nqrkfrhnhbibwp1az3fr8y509dsg")))

(define-public crate-glint-0.1.6 (c (n "glint") (v "0.1.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.10") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9") (d #t) (k 0)))) (h "1zjqy4rlawxl984yvclycp6sy5j6xaqxqz05wp050ynggik0x6qm")))

(define-public crate-glint-0.1.7 (c (n "glint") (v "0.1.7") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.10") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9") (d #t) (k 0)))) (h "0cbpjw7halh9skkcwnf0m52jf1nbr1ciij6sm3rddcvfjckq96fa")))

(define-public crate-glint-0.2.0 (c (n "glint") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.10") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9") (d #t) (k 0)))) (h "08jlm7i0gr9sl9i0qiv41a6rrhgd4s6j94z7vblwj7pc2qzdphaf")))

(define-public crate-glint-0.2.1 (c (n "glint") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.10") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9") (d #t) (k 0)))) (h "0j8s2xczbz0vp90yh948pi3ln3i5qlgv2dnb5dnpz3hpza4h616m")))

(define-public crate-glint-0.4.0 (c (n "glint") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9") (d #t) (k 0)))) (h "197jfrhws5ffc42na94if306cny49qw08p863w03s1v1qx1013zg")))

(define-public crate-glint-0.5.0 (c (n "glint") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.17.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9.0") (d #t) (k 0)))) (h "05qd07d6x2c65ipdy5p86nzaliwvhf7k66cwh335ada06kd9s8z6")))

(define-public crate-glint-0.6.0 (c (n "glint") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.17.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9.0") (d #t) (k 0)))) (h "0k9x7x5xq0zxjyks3mmx4ss595nbqa0rnz4j7bgfryspq5h9mgir")))

(define-public crate-glint-0.6.2 (c (n "glint") (v "0.6.2") (d (list (d (n "crossterm") (r "^0.17.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9.0") (d #t) (k 0)))) (h "0dlqwykcpvdyavc97393sz8a5s8020c5n0ksd98bifyrg50vsm05")))

(define-public crate-glint-6.3.4 (c (n "glint") (v "6.3.4") (d (list (d (n "crossterm") (r "^0.17.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "unic-segment") (r "^0.9.0") (d #t) (k 0)))) (h "1503gq1czqwy9q9sl07lhp622m6vnzhqljnzqphgvsb5pklb4vww")))

