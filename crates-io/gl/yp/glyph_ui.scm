(define-module (crates-io gl yp glyph_ui) #:use-module (crates-io))

(define-public crate-glyph_ui-0.0.0 (c (n "glyph_ui") (v "0.0.0") (d (list (d (n "crossterm") (r "^0.18") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "textwrap") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0h4xj4v36wjhakadqfg02h0qazkb3j7ms9ibwdlawg33mmnrbd6a")))

(define-public crate-glyph_ui-0.1.0 (c (n "glyph_ui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "keyboard-types") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "textwrap") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "06yf017d8xsfib70sjfhilc99w4kvf52391dm2i96v5rgdxlww62")))

