(define-module (crates-io gl yp glyph-bbox) #:use-module (crates-io))

(define-public crate-glyph-bbox-0.1.0 (c (n "glyph-bbox") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "0wjdc1zz0k4n8yjva3d32ri2c9ndabb003am01wi9v8n8h612qp8")))

