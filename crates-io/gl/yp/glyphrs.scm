(define-module (crates-io gl yp glyphrs) #:use-module (crates-io))

(define-public crate-glyphrs-0.1.3 (c (n "glyphrs") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)))) (h "1hyygfh7fjsa1sad9h633dkjwfvkfk5854qxw3j5wxd2a5h0d60f")))

(define-public crate-glyphrs-0.1.4 (c (n "glyphrs") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.13") (d #t) (k 0)))) (h "0f3g46iwh5k1237aknw3cs11g483h7rvl42w3zz35gcbg0cxlzry")))

(define-public crate-glyphrs-0.1.5 (c (n "glyphrs") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.13") (d #t) (k 0)))) (h "031kvywx83fhqzcpc8haa5iy84k227xnhywzkg3jx7hp0hz3g0bg")))

(define-public crate-glyphrs-0.1.6 (c (n "glyphrs") (v "0.1.6") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.13") (d #t) (k 0)))) (h "1gialzi6x6wb0i99r5m0r9fihgd9v4mrszpkkx785vvq1l5y3g7a")))

(define-public crate-glyphrs-0.1.7 (c (n "glyphrs") (v "0.1.7") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.13") (d #t) (k 0)))) (h "0aj43bzvk2iwnpmsn5xvg2gi2vpw333chy00ij341m7cmgw59nci")))

