(define-module (crates-io gl yp glyph-names) #:use-module (crates-io))

(define-public crate-glyph-names-0.1.0 (c (n "glyph-names") (v "0.1.0") (h "1783g5l5bhxci0agmzws56fj4nfp4ybx0l8hfw56j3w8wcdxql40")))

(define-public crate-glyph-names-0.2.0 (c (n "glyph-names") (v "0.2.0") (h "0zw0pkjcpbvp22alk8kwirn4g33v9i05mdaz5alkn6kc5mq1sly3")))

