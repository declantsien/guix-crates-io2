(define-module (crates-io gl yp glyph_brush_layout) #:use-module (crates-io))

(define-public crate-glyph_brush_layout-0.1.0 (c (n "glyph_brush_layout") (v "0.1.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^0.5") (d #t) (k 2)) (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "xi-unicode") (r "^0.1") (d #t) (k 0)))) (h "002rwgw1jmc7ngnl050vs54pq30pn8nyfrw6kfj9l7l76rimysvf")))

(define-public crate-glyph_brush_layout-0.1.1 (c (n "glyph_brush_layout") (v "0.1.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^1") (d #t) (k 2)) (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "xi-unicode") (r "^0.1") (d #t) (k 0)))) (h "0y1n0fyk69pdm24nvrhncmri7jv5808yi5pmwzsylk5ja1ans089")))

(define-public crate-glyph_brush_layout-0.1.2 (c (n "glyph_brush_layout") (v "0.1.2") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^1") (d #t) (k 2)) (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "xi-unicode") (r "^0.1") (d #t) (k 0)))) (h "1qnw7f5wk65a8kgpf2f61c44f953adb1h0yg7rnzy4p1rw20mh71")))

(define-public crate-glyph_brush_layout-0.1.3 (c (n "glyph_brush_layout") (v "0.1.3") (d (list (d (n "approx") (r "^0.3.1") (d #t) (k 2)) (d (n "full_rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0) (p "rusttype")) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^1") (d #t) (k 2)) (d (n "xi-unicode") (r "^0.1") (d #t) (k 0)))) (h "1w0qx6bbq4yj13f8ayk3jnkfaxn52zd6p4pqzxi2lqb0xgswr1ri")))

(define-public crate-glyph_brush_layout-0.1.4 (c (n "glyph_brush_layout") (v "0.1.4") (d (list (d (n "approx") (r "^0.3.1") (d #t) (k 2)) (d (n "full_rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0) (p "rusttype")) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^1") (d #t) (k 2)) (d (n "xi-unicode") (r "^0.1") (d #t) (k 0)))) (h "14db519l4a0kdmd2wd5dha7qmc6wg9pmrryps90nzsyfvn67i8w2")))

(define-public crate-glyph_brush_layout-0.1.5 (c (n "glyph_brush_layout") (v "0.1.5") (d (list (d (n "approx") (r "^0.3.1") (d #t) (k 2)) (d (n "full_rusttype") (r "^0.7.4") (f (quote ("gpu_cache"))) (d #t) (k 0) (p "rusttype")) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^1") (d #t) (k 2)) (d (n "xi-unicode") (r "^0.1") (d #t) (k 0)))) (h "1s3mrvq8q1znb4bj425yma1ff2lqpxgch80g08890zrgiq4yw5nm")))

(define-public crate-glyph_brush_layout-0.1.6 (c (n "glyph_brush_layout") (v "0.1.6") (d (list (d (n "approx") (r "^0.3.1") (d #t) (k 2)) (d (n "full_rusttype") (r "^0.7.4") (f (quote ("gpu_cache"))) (d #t) (k 0) (p "rusttype")) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^1") (d #t) (k 2)) (d (n "xi-unicode") (r "^0.1") (d #t) (k 0)))) (h "103khdwk0d8nfj9nsfzvawxmzci2mvri68lyb181b8s6q0303vq2")))

(define-public crate-glyph_brush_layout-0.1.7 (c (n "glyph_brush_layout") (v "0.1.7") (d (list (d (n "approx") (r "^0.3.1") (d #t) (k 2)) (d (n "full_rusttype") (r "^0.7.4") (f (quote ("gpu_cache"))) (d #t) (k 0) (p "rusttype")) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^1") (d #t) (k 2)) (d (n "xi-unicode") (r "^0.2") (d #t) (k 0)))) (h "0wjzl8dgrjlrkj0hv14jwnd9yxjnxxs1slv8w3awkv9qc1mlj5zl")))

(define-public crate-glyph_brush_layout-0.1.8 (c (n "glyph_brush_layout") (v "0.1.8") (d (list (d (n "approx") (r "^0.3.1") (d #t) (k 2)) (d (n "full_rusttype") (r "^0.8") (d #t) (k 0) (p "rusttype")) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^1") (d #t) (k 2)) (d (n "xi-unicode") (r "^0.2") (d #t) (k 0)))) (h "0qnbwl6g7l8n42fknrziqq7d80ya4hlb1296839s5j1dlrhd9dk2")))

(define-public crate-glyph_brush_layout-0.1.9 (c (n "glyph_brush_layout") (v "0.1.9") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "full_rusttype") (r "^0.8") (d #t) (k 0) (p "rusttype")) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^1") (d #t) (k 2)) (d (n "xi-unicode") (r "^0.2") (d #t) (k 0)))) (h "1ir4xi5b0s0yykxm26d7mvspa8dlqay1q91fnfv73p7if32ssw4b")))

(define-public crate-glyph_brush_layout-0.2.0 (c (n "glyph_brush_layout") (v "0.2.0") (d (list (d (n "ab_glyph") (r "^0.2.1") (d #t) (k 0)) (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^1") (d #t) (k 2)) (d (n "xi-unicode") (r "^0.2") (d #t) (k 0)))) (h "05qpqsv0j0csi6waz16y2jxal19m8yvql71cyilgwyyggnzrm94s")))

(define-public crate-glyph_brush_layout-0.2.1 (c (n "glyph_brush_layout") (v "0.2.1") (d (list (d (n "ab_glyph") (r "^0.2.1") (d #t) (k 0)) (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^2") (d #t) (k 2)) (d (n "xi-unicode") (r "^0.3") (d #t) (k 0)))) (h "08482jyj2gxwn14hqxqrc4zz8brpnyd7j0hv5y8c285z63ahdg0h")))

(define-public crate-glyph_brush_layout-0.2.2 (c (n "glyph_brush_layout") (v "0.2.2") (d (list (d (n "ab_glyph") (r "^0.2.1") (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^2") (d #t) (k 2)) (d (n "xi-unicode") (r "^0.3") (d #t) (k 0)))) (h "15zlcphcjiwpxdvsjygcs477v3yhr5ijnm8lbvq45navk37iikqm")))

(define-public crate-glyph_brush_layout-0.2.3 (c (n "glyph_brush_layout") (v "0.2.3") (d (list (d (n "ab_glyph") (r "^0.2.1") (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^2.5.1") (d #t) (k 2)) (d (n "xi-unicode") (r "^0.3") (d #t) (k 0)))) (h "0f3scdx5kqjndnwvamfjca6dl8fs6np0jl2wd71mmjh09wrw4cnc")))

