(define-module (crates-io gl yp glyphspack) #:use-module (crates-io))

(define-public crate-glyphspack-0.1.0 (c (n "glyphspack") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1baayq6929lds2l21xywdzl3cbc4l4aprb7d7ganwccr82c2j083")))

(define-public crate-glyphspack-1.0.0 (c (n "glyphspack") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("suggestions" "wrap_help"))) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0mf0fg1g8wa0nflrnjqyrqdzf9i1x5iw1skkjfcbv6xj1c7x1rfl")))

(define-public crate-glyphspack-2.0.0 (c (n "glyphspack") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("std" "suggestions" "wrap_help"))) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "0230q2g4ci627pn1i132jhbc1xkn24acc224zm5lpx5bkfff69xh")))

