(define-module (crates-io gl yp glyph_packer) #:use-module (crates-io))

(define-public crate-glyph_packer-0.0.0 (c (n "glyph_packer") (v "0.0.0") (d (list (d (n "image") (r "*") (d #t) (k 0)))) (h "1qk9gfkw1n2fvn3f6kq1a7ds6r4h72w643jxjk5i23xdahk6vq9d")))

(define-public crate-glyph_packer-0.0.1 (c (n "glyph_packer") (v "0.0.1") (d (list (d (n "image") (r "= 0.3.9") (d #t) (k 0)))) (h "0gpp5gkmcjjhsldhvhglk5ch18z5f4sd00bm058g8gkxwf2rkfqr")))

(define-public crate-glyph_packer-0.0.2 (c (n "glyph_packer") (v "0.0.2") (d (list (d (n "image") (r "^0.6") (d #t) (k 0)))) (h "1wdql1ynd6hx5fxcdkagvbs2r0ji934r3n9h01w3zbx4shq9b85r")))

