(define-module (crates-io gl yp glyph-bbox-render) #:use-module (crates-io))

(define-public crate-glyph-bbox-render-0.1.0 (c (n "glyph-bbox-render") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glyph-bbox") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.2.2") (d #t) (k 0)))) (h "0pygnm1l3kp9yrg7ww6b9vb7b8g8z99z6d9vhjbgj2zjmxrvjcm5")))

