(define-module (crates-io gl ue gluesql-derive-proc) #:use-module (crates-io))

(define-public crate-gluesql-derive-proc-0.1.0 (c (n "gluesql-derive-proc") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "02vw6wjhgvdzpvc5z042sjgzrk5hsl75q2g8p0mrqpiz5pb9zgsm")))

(define-public crate-gluesql-derive-proc-0.1.1 (c (n "gluesql-derive-proc") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0iyx5i2kxhkqxlfinlfpqwif7ahyabb613f42slgvl4r5hhxgl2k")))

(define-public crate-gluesql-derive-proc-0.1.3 (c (n "gluesql-derive-proc") (v "0.1.3") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "089pjx1llbx71jxjsb2k4ka5i1ihq57n5zjxyv1z19x4qjy4ygms")))

(define-public crate-gluesql-derive-proc-0.1.4 (c (n "gluesql-derive-proc") (v "0.1.4") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0b6yj00d7l019v1smgsnqwgffjz21cnc9jgmdknkcra71g1zasp5")))

(define-public crate-gluesql-derive-proc-0.1.5 (c (n "gluesql-derive-proc") (v "0.1.5") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ag1bybdy350y5anpg04lyb5x49z119pz4f42d64mcvw12mgd0j4")))

(define-public crate-gluesql-derive-proc-0.1.7 (c (n "gluesql-derive-proc") (v "0.1.7") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0115cx8wflxlsdi9yw4575fvi81n7kfplg8kfvg1dj0hc79nbgd1")))

(define-public crate-gluesql-derive-proc-0.2.0 (c (n "gluesql-derive-proc") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0jb6xf72j1rmzzxqcjjd6arly3w6qkwla1n86gyywx3r627pvb5w")))

(define-public crate-gluesql-derive-proc-0.2.1 (c (n "gluesql-derive-proc") (v "0.2.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1azkllb3k4z0l9d8vyixz31kx9py1hr4i3cc5ain96nmki2xw7cy")))

