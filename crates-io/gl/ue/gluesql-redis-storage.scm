(define-module (crates-io gl ue gluesql-redis-storage) #:use-module (crates-io))

(define-public crate-gluesql-redis-storage-0.15.0 (c (n "gluesql-redis-storage") (v "0.15.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde" "wasmbind"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gluesql-core") (r "^0.15.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "test-suite") (r "^0.15.0") (d #t) (k 2) (p "gluesql-test-suite")) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.6") (d #t) (k 2)))) (h "00y8ifacjca6xn0mvnl5rsjsx6449v5wqhn7qxjfh0867ran1k28")))

