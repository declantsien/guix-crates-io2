(define-module (crates-io gl ue gluesql-utils) #:use-module (crates-io))

(define-public crate-gluesql-utils-0.10.0 (c (n "gluesql-utils") (v "0.10.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)))) (h "0c3gynlfwd67x6mxapfk9j9xmsaycwd4baan2x8m2h53xbhl33ha")))

(define-public crate-gluesql-utils-0.10.1 (c (n "gluesql-utils") (v "0.10.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)))) (h "0xhw34hvhm2rc37nxl8v7yj87s9izjaq56zjxvmma07i1cz5x2rc")))

(define-public crate-gluesql-utils-0.10.2 (c (n "gluesql-utils") (v "0.10.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)))) (h "1m8s40lxn7520kiywmcanxyyq52k2hmrsjjz3rfpqyy255qm395v")))

(define-public crate-gluesql-utils-0.11.0 (c (n "gluesql-utils") (v "0.11.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)))) (h "1jpsdwbc7vj7fp40mf8gghx0ni5a4bdza59zn1aijp8iin415aly")))

(define-public crate-gluesql-utils-0.12.0 (c (n "gluesql-utils") (v "0.12.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)))) (h "1rkpi714l492j5fjkb9ldxj4szkxsi4dr1zsr5l9y4hzz4frdn3f")))

(define-public crate-gluesql-utils-0.13.0 (c (n "gluesql-utils") (v "0.13.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)))) (h "17vxv7wjrqh7380z95zdb026b54nkwb7183fmly0nix338skizl7")))

(define-public crate-gluesql-utils-0.14.0 (c (n "gluesql-utils") (v "0.14.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)))) (h "1mnx8kzy9xpij2ndjary0j7ksyds9m2kmbx1wqsn4gn43jkr03ip")))

(define-public crate-gluesql-utils-0.15.0 (c (n "gluesql-utils") (v "0.15.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)))) (h "0wamlh16bdj3z7f5cngcwzyjf70bnjbcv61q5s5yf3n33s6vx2a6")))

