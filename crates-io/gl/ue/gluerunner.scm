(define-module (crates-io gl ue gluerunner) #:use-module (crates-io))

(define-public crate-gluerunner-0.1.0-beta (c (n "gluerunner") (v "0.1.0-beta") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "gluescript") (r "^0.1.0-beta") (d #t) (k 0)) (d (n "jsonpath-rust") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zmf5mqbryals5s7immffq96zfaz3nsvzcsfb4z9d3rnzyyp0vn8")))

(define-public crate-gluerunner-0.1.0 (c (n "gluerunner") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "gluescript") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonpath-rust") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sl1dp0z3lm59mprx1vcb9l5b2g6g7bp1bly3s27ll618is4824s")))

