(define-module (crates-io gl ue glueshell) #:use-module (crates-io))

(define-public crate-glueshell-0.1.0-beta (c (n "glueshell") (v "0.1.0-beta") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "gluerunner") (r "^0.1.0-beta") (d #t) (k 0)) (d (n "gluescript") (r "^0.1.0-beta") (d #t) (k 0)))) (h "0j5j1n8l1f408zb3pyp5026m2rd8f3lr3qayrbccr8p18mhmw4hz")))

(define-public crate-glueshell-0.1.0 (c (n "glueshell") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "gluerunner") (r "^0.1.0") (d #t) (k 0)) (d (n "gluescript") (r "^0.1.0") (d #t) (k 0)))) (h "09wcn9mwglwric1crg7sncd9v3i6zcjfpziaa76yyhgdxzns4hxx")))

