(define-module (crates-io gl ue gluescript) #:use-module (crates-io))

(define-public crate-gluescript-0.1.0-beta (c (n "gluescript") (v "0.1.0-beta") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)))) (h "1b32x26q5l303j73g6ka45fg9wb4gkap01w9xrisnzs10037820p")))

(define-public crate-gluescript-0.1.0 (c (n "gluescript") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)))) (h "10r92xx0h26ymfnxr08cckrqn74lzvc0pr7830hmgmbgvja4ydny")))

