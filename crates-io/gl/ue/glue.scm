(define-module (crates-io gl ue glue) #:use-module (crates-io))

(define-public crate-glue-0.1.2 (c (n "glue") (v "0.1.2") (h "1pjcvwq4m6rwlzshns787qii8wcgbklya60n8rgrg8az83q794qf") (y #t)))

(define-public crate-glue-0.2.2 (c (n "glue") (v "0.2.2") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "13h0jzzc1glyncgwspmbvm0d60qpmbccnnbndyrsv0g090l04z44") (y #t)))

(define-public crate-glue-0.3.1 (c (n "glue") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0gyyazylv6kfkgfb1hncbbyf3cyjjw43z3ghbbfd52d93y4fapy8") (y #t)))

(define-public crate-glue-0.4.1 (c (n "glue") (v "0.4.1") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0lpv65mqck4a9jbb768iwx2vni9fvp8b466mhxr14lwsp3y4l6s5") (y #t)))

(define-public crate-glue-0.5.3 (c (n "glue") (v "0.5.3") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "11dajmhallpm3jl50zyxxjlwhswpb61bn70kanlgdlzcvryk9g45") (y #t)))

(define-public crate-glue-0.6.1 (c (n "glue") (v "0.6.1") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "tera") (r "^1.0.0-beta.6") (d #t) (k 1)))) (h "1mqs6qxivw6h8qm0xppaxy83crwd3y3p8primxz34pdbf5xz8bry") (y #t)))

(define-public crate-glue-0.7.1 (c (n "glue") (v "0.7.1") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "17nb39dnryyblx67plcra58g9iqhf1bqj5ns3lb2wwg246d7zwhd") (y #t)))

(define-public crate-glue-0.8.4 (c (n "glue") (v "0.8.4") (d (list (d (n "bork") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1ikd10ia0b4g4cms6f6aaqkwh1ipc9l5a9yxhma01kmxl551dp4d") (y #t)))

(define-public crate-glue-0.1.3 (c (n "glue") (v "0.1.3") (h "02m0pdb1l845q99jnywzx8jyfp05cv5zrq83n08pd5vscaaas3mh") (y #t)))

(define-public crate-glue-0.1.4 (c (n "glue") (v "0.1.4") (h "1rgavwfiwjfxsi7rvvvfwmdszrszv88sby0zaa6qr62cfxaadav4")))

(define-public crate-glue-0.2.3 (c (n "glue") (v "0.2.3") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1fy0r4q8z8snd61yq3h70rv8c2w7pjd3hm1n2p5raagp28bc5kvg")))

(define-public crate-glue-0.3.2 (c (n "glue") (v "0.3.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0w0xbz27i09fac48bwr9gadi4f0j97xsa6n2di35pd7rrhclrx4c")))

(define-public crate-glue-0.4.2 (c (n "glue") (v "0.4.2") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0cxsmnwj833c8h16215b3710sj1ai4jfvbgxvjhni5a7nfzf03xx")))

(define-public crate-glue-0.5.4 (c (n "glue") (v "0.5.4") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "01mmv7rb00srg1lhw22lwvwgprw6zhhim43lk59yayv3xbjjnv6g")))

(define-public crate-glue-0.6.2 (c (n "glue") (v "0.6.2") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "tera") (r "^1.0.0-beta.6") (d #t) (k 1)))) (h "0wbxxwx9jp6gyl812234wsipxv7njjxhjah8yh8zql719ykqk733")))

(define-public crate-glue-0.7.2 (c (n "glue") (v "0.7.2") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "072wx1c8j3cdyn4b8hiiiabq95sy7s4jf7aayjvx4hffzy8ssx6d")))

(define-public crate-glue-0.8.5 (c (n "glue") (v "0.8.5") (d (list (d (n "bork") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0gqilyrs9k9r82iqqidhnpnx2xcaz2z8gmdk8kxaxq38ffdn1bwr")))

(define-public crate-glue-0.8.6 (c (n "glue") (v "0.8.6") (d (list (d (n "bork") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1al5gafb3d9w69l76djjlmgysw5si2jgaaw5328ac7idx9bnrj42")))

(define-public crate-glue-0.8.7 (c (n "glue") (v "0.8.7") (d (list (d (n "bork") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0mqb5w3fng2lh3gipsakdafy3q4fs20z20nvsx9yiih19d6iw1d0")))

