(define-module (crates-io gl ot glotaran_converter_cli) #:use-module (crates-io))

(define-public crate-glotaran_converter_cli-0.1.0 (c (n "glotaran_converter_cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glotaran_converter_lib") (r "^0.1.0") (d #t) (k 0)))) (h "1hmj2w6lyfcvwhj9pd18q80z5lzwfw1ng9si8z3xs4ma64h7yxr6")))

(define-public crate-glotaran_converter_cli-0.1.1 (c (n "glotaran_converter_cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glotaran_converter_lib") (r "^0.1.0") (d #t) (k 0)))) (h "16zz3xg5pibmf5d84v5y21lj301x45hgn1vxq3v56js0f09rbncp")))

