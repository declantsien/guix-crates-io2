(define-module (crates-io gl ot glotaran_converter_lib) #:use-module (crates-io))

(define-public crate-glotaran_converter_lib-0.1.0 (c (n "glotaran_converter_lib") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1biiv3pjsgfzd7xjdsbqrl505wyahqzid3kj10m437prm9ya6186")))

(define-public crate-glotaran_converter_lib-0.1.1 (c (n "glotaran_converter_lib") (v "0.1.1") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1a1djvah5fn29d17qk43i2jm3cx0qs2998z9ypwrvi43zxx48jbr")))

(define-public crate-glotaran_converter_lib-0.1.2 (c (n "glotaran_converter_lib") (v "0.1.2") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1z0lzblnv31vx1mszxl2jz0w7rq22djirxmcy3w6gaib0f1yw88f")))

(define-public crate-glotaran_converter_lib-0.1.3 (c (n "glotaran_converter_lib") (v "0.1.3") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0jvjj5nb9nij21g48bmmgnlvpkpmjsnc0jg7hl8ls8v921z9zvv1")))

