(define-module (crates-io gl it glitchcat) #:use-module (crates-io))

(define-public crate-glitchcat-0.1.0 (c (n "glitchcat") (v "0.1.0") (d (list (d (n "console") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "strum") (r "^0.10") (d #t) (k 0)) (d (n "strum_macros") (r "^0.10") (d #t) (k 0)))) (h "0a1779c2grqk0g1lj3w47vjyiqhgvj69rv9qyrjks5vh1y6dq377")))

(define-public crate-glitchcat-0.2.0 (c (n "glitchcat") (v "0.2.0") (d (list (d (n "console") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "strum") (r "^0.10") (d #t) (k 0)) (d (n "strum_macros") (r "^0.10") (d #t) (k 0)))) (h "0r70hlmhsm7a8yjaky6r9a0h7byl4cs25911k9pn8jqpy59xdvkn")))

(define-public crate-glitchcat-0.3.0 (c (n "glitchcat") (v "0.3.0") (d (list (d (n "console") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "strum") (r "^0.10") (d #t) (k 0)) (d (n "strum_macros") (r "^0.10") (d #t) (k 0)))) (h "08hykbcpdwk2frsj95qqqppyn85x2mmbrbpn6v2v4c24vc5xqqnh")))

(define-public crate-glitchcat-0.3.1 (c (n "glitchcat") (v "0.3.1") (d (list (d (n "console") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "strum") (r "^0.10") (d #t) (k 0)) (d (n "strum_macros") (r "^0.10") (d #t) (k 0)))) (h "1nw2nzcvx03s9rqlrzkc6l0l2bacma5ihggzbqnc7d0v9afs1k82")))

(define-public crate-glitchcat-0.3.2 (c (n "glitchcat") (v "0.3.2") (d (list (d (n "console") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "strum") (r "^0.10") (d #t) (k 0)) (d (n "strum_macros") (r "^0.10") (d #t) (k 0)))) (h "01g8lsf2wg8wc1l62f729qlkxnjfmgka95p4fih709azlg9cjghd")))

