(define-module (crates-io gl it glit) #:use-module (crates-io))

(define-public crate-glit-0.1.0 (c (n "glit") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "~2.29") (d #t) (k 0)) (d (n "git2") (r "^0.6.10") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rand_derive") (r "^0.3") (d #t) (k 0)))) (h "08w0by4l9is6skzj9r5sr5lmyp19pj8766rq8924an9sh0zckcd9")))

(define-public crate-glit-0.1.1 (c (n "glit") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "~2.29") (d #t) (k 0)) (d (n "git2") (r "^0.6.10") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rand_derive") (r "^0.3") (d #t) (k 0)))) (h "1wrcw0srlimwladx2y64nk60z4s1i7fmm30hnfp7zrx5yai2n3ai")))

(define-public crate-glit-0.1.2 (c (n "glit") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "~2.29") (d #t) (k 0)) (d (n "git2") (r "^0.6.10") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rand_derive") (r "^0.3") (d #t) (k 0)))) (h "1arlm890yiidx5745s1szr5vy3gnwhcik59dkcnl70pfph5fycbs")))

(define-public crate-glit-0.1.3 (c (n "glit") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "~2.29") (d #t) (k 0)) (d (n "git2") (r "^0.6.10") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rand_derive") (r "^0.3") (d #t) (k 0)))) (h "15w6cdfdgn76fvr6mf2srj15w0hfsz8xkmimv7lwqp42f1x9rdvr")))

(define-public crate-glit-0.2.0 (c (n "glit") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 2)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1h318566f8zmqh405y7v30nf5qgjnn9zpdql9si7k8f353939x42")))

(define-public crate-glit-0.2.1 (c (n "glit") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 2)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (t "cfg(windows)") (k 0)))) (h "13hlg79d5jayc5a4qq194ni7dam7fnx6jdd6q4qb4rk0fqmwk0pv")))

(define-public crate-glit-0.2.2 (c (n "glit") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 2)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (t "cfg(windows)") (k 0)))) (h "1j7prvi8y4zd6pvcmfa649jpvs9h5sqpmjhgz01inpqa15zamxdv")))

(define-public crate-glit-0.3.0 (c (n "glit") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 2)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "04xf5r9hnvvmilsn7hhd3xdvr770wm6w0p77ksp003kl7gyb0pdp")))

