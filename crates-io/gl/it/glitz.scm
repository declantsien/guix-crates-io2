(define-module (crates-io gl it glitz) #:use-module (crates-io))

(define-public crate-glitz-0.1.0 (c (n "glitz") (v "0.1.0") (d (list (d (n "chlorine") (r "^1") (f (quote ("int_extras"))) (d #t) (k 0)))) (h "0zs8d0kh7yfx19para0sxpxcgh3ckv237n40r8njd18rm2afg7ky")))

(define-public crate-glitz-0.2.0 (c (n "glitz") (v "0.2.0") (d (list (d (n "chlorine") (r "^1") (f (quote ("int_extras"))) (d #t) (k 0)) (d (n "zstring") (r "^0.0.3") (d #t) (k 0)))) (h "0wq2alvanz5grvzpjl6jigj84s3jpl6d84jgh3qivzccm8m5zhy7") (f (quote (("std") ("default" "std"))))))

(define-public crate-glitz-0.2.1 (c (n "glitz") (v "0.2.1") (d (list (d (n "chlorine") (r "^1") (f (quote ("int_extras"))) (d #t) (k 0)) (d (n "zstring") (r "^0.0.3") (d #t) (k 0)))) (h "1k8b07imqdrwg864yi9x2wi1w9bfyihh1xfmlnjypbzpdgc7b1h4") (f (quote (("std") ("default" "std"))))))

(define-public crate-glitz-0.3.0 (c (n "glitz") (v "0.3.0") (d (list (d (n "chlorine") (r "^1") (f (quote ("int_extras"))) (d #t) (k 0)) (d (n "zstring") (r "^0.0.3") (d #t) (k 0)))) (h "1f14frrqyl2zwhmwqik9d3fajai8vq1yhza1qza645ahzwj7mfvf") (f (quote (("std") ("default" "std"))))))

(define-public crate-glitz-0.3.1 (c (n "glitz") (v "0.3.1") (d (list (d (n "chlorine") (r "^1") (f (quote ("int_extras"))) (d #t) (k 0)) (d (n "zstring") (r "^0.0.3") (d #t) (k 0)))) (h "03vbjq8nrh2v1lvybmbyy436hgwsqmzfpqgc06p3idpd5b7nv9wn") (f (quote (("std") ("default" "std"))))))

(define-public crate-glitz-0.4.0 (c (n "glitz") (v "0.4.0") (d (list (d (n "chlorine") (r "^1") (f (quote ("int_extras"))) (d #t) (k 0)) (d (n "zstring") (r "^0.1.2") (d #t) (k 0)))) (h "0nxc6zn5lrkv5mdf1x1af9alwjpzc75sarwy02151v6sw882m3c5") (f (quote (("std") ("default" "std"))))))

