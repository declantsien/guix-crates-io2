(define-module (crates-io gl it glitter) #:use-module (crates-io))

(define-public crate-glitter-0.1.0 (c (n "glitter") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.13.0") (d #t) (k 2)))) (h "1hkz5x9pixyvqk8nzj2r3ip913gdlizjfbiwvcl5dnaxaxycw5fl") (f (quote (("default" "cgmath" "image")))) (y #t)))

(define-public crate-glitter-0.1.1 (c (n "glitter") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.10.3") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.13.0") (d #t) (k 2)))) (h "0nzkr7mhw0vl0w4dfkvdb0fkdiifsynypp4qdi3gvp04bv6j3y1c") (f (quote (("default" "cgmath" "image")))) (y #t)))

(define-public crate-glitter-0.1.2 (c (n "glitter") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.10.3") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.13.0") (d #t) (k 2)))) (h "1j28vcv5jghy01r9507y78m81hq1wylayjid8scn92m9r5vy5c1h") (f (quote (("default" "cgmath" "image")))) (y #t)))

