(define-module (crates-io gl it glitchup_derive) #:use-module (crates-io))

(define-public crate-glitchup_derive-0.1.0 (c (n "glitchup_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.41") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1d0m8k7mjf4jpzr33sv007b3gqqpd4ikhr072cn3iyqb010fhaj1")))

(define-public crate-glitchup_derive-0.1.1 (c (n "glitchup_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.41") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1h5s65hwk1n3p1a6aznbxn7r6lkd0dwsfxc7a94vf9d1bm1xg7nl")))

(define-public crate-glitchup_derive-0.1.2 (c (n "glitchup_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.41") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fr38vkl4sk68j6rxgn67zs55lp1s0jsvx54b6wzriqmf37ky5yd")))

(define-public crate-glitchup_derive-0.1.3 (c (n "glitchup_derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.41") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "144i2cpq4hb3y7k57ipx0jyrby5b606nmk8n6ac82j50dn07246m")))

(define-public crate-glitchup_derive-0.2.0 (c (n "glitchup_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.41") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0701fn062dvwvx9l0d3g45lgzzv1jzabizbnhm9d35p11pm85rm6")))

(define-public crate-glitchup_derive-0.3.0 (c (n "glitchup_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.41") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0n40b7lqfrdrrz0z18bms2zmfzlivadsibwz8y5z90zlsmnpq6ml")))

(define-public crate-glitchup_derive-0.4.0 (c (n "glitchup_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.41") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03p0kwrhv9n93h43zrwqk2w99zmfvf3hkv36xwgqmk3w70s1kd2v")))

