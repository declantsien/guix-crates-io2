(define-module (crates-io gl _l gl_loader) #:use-module (crates-io))

(define-public crate-gl_loader-0.0.1 (c (n "gl_loader") (v "0.0.1") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0a7a1hid3akdwdc3y82qd8jpvsmjmpp83wcw42wgc227h1sahn2g")))

(define-public crate-gl_loader-0.0.2 (c (n "gl_loader") (v "0.0.2") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "18vk8pb4i5g9z3vax2w5y3i6nnhrgm0kgbrgr0w4g5k8515w8v97")))

(define-public crate-gl_loader-0.0.4 (c (n "gl_loader") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jjzn03irv29jnr3cldygcs9yky0gabbvkcicxgmlswq96wl6clb")))

(define-public crate-gl_loader-0.1.0 (c (n "gl_loader") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11nhskvw1p6i52s3pa4lkywacs992brxm80yxf0hv070il6vn7wc")))

(define-public crate-gl_loader-0.1.1 (c (n "gl_loader") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1psa8l0sx86wiypai5sd5zpyjpd5wxpldrr6gkjbsmfpabca6k53")))

(define-public crate-gl_loader-0.1.2 (c (n "gl_loader") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lwr1gd7hrb2nk67zw4pc04vl4h868r5a7846zjr0548bzfrcbg3")))

