(define-module (crates-io gl _l gl_lib) #:use-module (crates-io))

(define-public crate-gl_lib-0.0.1 (c (n "gl_lib") (v "0.0.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.25.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (f (quote ("bundled" "static-link"))) (d #t) (k 2)) (d (n "walkdir") (r "^2.1") (d #t) (k 0)))) (h "0zj8szh0bn9f9p31hxwsawr1xa4yy84s338wcnmpdj1z66wwl0r8") (f (quote (("debug"))))))

(define-public crate-gl_lib-0.1.0 (c (n "gl_lib") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.25.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (f (quote ("bundled" "static-link"))) (d #t) (k 2)) (d (n "walkdir") (r "^2.1") (d #t) (k 0)))) (h "15jrrch4d43v287r7b1cvhkwdyqllhvv159p99pdr9w21rkm164p") (f (quote (("debug"))))))

