(define-module (crates-io gl op glopy) #:use-module (crates-io))

(define-public crate-glopy-0.1.0 (c (n "glopy") (v "0.1.0") (d (list (d (n "globset") (r "^0.4.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "039r1y1saj68g02p9zk3mbdyy57pfcvwrkbzg22i77mcrnd0n02w")))

(define-public crate-glopy-0.2.0 (c (n "glopy") (v "0.2.0") (d (list (d (n "globset") (r "^0.4.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "144q0jflimzh14pb93a4jk56psanm9siqg587k6nnh3sq1v2q3g6")))

(define-public crate-glopy-0.3.0 (c (n "glopy") (v "0.3.0") (d (list (d (n "globset") (r "^0.4.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0gvlv0vnxjp9d9l8r6cskwpf27yssd31mrdzqg35rxqswhqdsi6h")))

