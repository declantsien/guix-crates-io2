(define-module (crates-io gl an glang) #:use-module (crates-io))

(define-public crate-glang-0.1.0 (c (n "glang") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "glang_core") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6.0") (d #t) (k 0)))) (h "06r5ylmzys5p8c58fkyf5icx745m1x3yh80ka25finqc1hivvc9a")))

