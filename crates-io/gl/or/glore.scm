(define-module (crates-io gl or glore) #:use-module (crates-io))

(define-public crate-glore-0.1.0 (c (n "glore") (v "0.1.0") (h "10lf6qgml5crxhg3cymskxvph3a4fr36c5c93k9pkwwii5rx8avi")))

(define-public crate-glore-0.1.1 (c (n "glore") (v "0.1.1") (h "0cqc1m4781gwakk6accyjsz3lbxsbz66v5vl4mndbsz65hzsdmxc")))

(define-public crate-glore-0.1.2 (c (n "glore") (v "0.1.2") (h "0j0pwjn3d1fvyznqk9nsavnpjn4lc2a512d0ma5gp899gs9cvjv9")))

(define-public crate-glore-0.1.3 (c (n "glore") (v "0.1.3") (h "12yl5g2wixkn2md26hgg41ka132qa2axp55m9ji128jpsaw878zj")))

(define-public crate-glore-0.1.4 (c (n "glore") (v "0.1.4") (h "1bam2ribgwy6qsk7gxi2fl7y2ivgl2k3z5bvnqjhym1dd9d3xnlj")))

