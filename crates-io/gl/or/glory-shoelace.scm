(define-module (crates-io gl or glory-shoelace) #:use-module (crates-io))

(define-public crate-glory-shoelace-0.0.0 (c (n "glory-shoelace") (v "0.0.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "066g8w2385xm3ca1kld4rpaiv4ls90fa7mjpm1q7rv16pp9f5qdc")))

(define-public crate-glory-shoelace-0.0.2 (c (n "glory-shoelace") (v "0.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "educe") (r "^0.4.23") (d #t) (k 0)) (d (n "glory-core") (r "^0.2.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.65") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.88") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Element" "HtmlElement"))) (d #t) (k 0)))) (h "1bfcq6avi21r9wwdlszrli995c76ix5dy40w2l159wsw147sdci3") (f (quote (("web-ssr") ("default" "web-csr") ("__single_holder")))) (s 2) (e (quote (("web-csr" "__single_holder" "dep:wasm-bindgen")))) (r "1.73")))

(define-public crate-glory-shoelace-0.0.3 (c (n "glory-shoelace") (v "0.0.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "educe") (r "^0.4.23") (d #t) (k 0)) (d (n "glory-core") (r "^0.2.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.65") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.88") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Element" "HtmlElement"))) (d #t) (k 0)))) (h "196dd83h6wlaif913ax0c6gg032mhp7iiq85yyv83m1achx9ynaj") (f (quote (("web-ssr") ("default" "web-csr") ("__single_holder")))) (s 2) (e (quote (("web-csr" "__single_holder" "dep:wasm-bindgen")))) (r "1.73")))

