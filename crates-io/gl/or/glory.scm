(define-module (crates-io gl or glory) #:use-module (crates-io))

(define-public crate-glory-0.0.0 (c (n "glory") (v "0.0.0") (h "0b3r58v8spiqfvxapsyyhmpdh8s5hcfj217v197ikjdg164vzdnw")))

(define-public crate-glory-0.1.0 (c (n "glory") (v "0.1.0") (d (list (d (n "glory-core") (r "^0.1.0") (k 0)) (d (n "glory-routing") (r "^0.1.0") (o #t) (k 0)))) (h "0waggs0ajvph9gnh1px9wychj6mg9c44qfcwav4a2kfs0z7xm0mq") (f (quote (("web-ssr" "glory-core/web-ssr" "glory-routing/web-ssr") ("web-csr" "glory-core/web-csr" "glory-routing/web-csr") ("default" "web-csr")))) (s 2) (e (quote (("salvo" "web-ssr" "glory-core/salvo" "routing" "glory-routing?/salvo") ("routing" "dep:glory-routing"))))))

(define-public crate-glory-0.2.0 (c (n "glory") (v "0.2.0") (d (list (d (n "glory-core") (r "^0.2.0") (k 0)) (d (n "glory-routing") (r "^0.2.0") (o #t) (k 0)))) (h "146r4djnb6lkcv2pqmmfl8dbzfayr03v2y6ymfd4zkzlz09k40g8") (f (quote (("default" "web-csr")))) (s 2) (e (quote (("web-ssr" "glory-core/web-ssr" "glory-routing?/web-ssr") ("web-csr" "glory-core/web-csr" "glory-routing?/web-csr") ("salvo" "web-ssr" "glory-core/salvo" "routing" "glory-routing?/salvo") ("routing" "dep:glory-routing"))))))

(define-public crate-glory-0.3.0 (c (n "glory") (v "0.3.0") (d (list (d (n "glory-core") (r "^0.3.0") (k 0)) (d (n "glory-routing") (r "^0.3.0") (o #t) (k 0)))) (h "0zx95xr3mjqbx178nk2jkib0w5n4y6xaw73kvznjwasnrfb6mjim") (f (quote (("default" "web-csr")))) (s 2) (e (quote (("web-ssr" "glory-core/web-ssr" "glory-routing?/web-ssr") ("web-csr" "glory-core/web-csr" "glory-routing?/web-csr") ("salvo" "web-ssr" "glory-core/salvo" "routing" "glory-routing?/salvo") ("routing" "dep:glory-routing"))))))

(define-public crate-glory-0.3.1 (c (n "glory") (v "0.3.1") (d (list (d (n "glory-core") (r "^0.3.1") (k 0)) (d (n "glory-routing") (r "^0.3.1") (o #t) (k 0)))) (h "0yn2j3kw3l6i0f60rp50hsdw7az4acr228vy2rxhvn0l17gb8p79") (f (quote (("default" "web-csr")))) (s 2) (e (quote (("web-ssr" "glory-core/web-ssr" "glory-routing?/web-ssr") ("web-csr" "glory-core/web-csr" "glory-routing?/web-csr") ("salvo" "web-ssr" "glory-core/salvo" "routing" "glory-routing?/salvo") ("routing" "dep:glory-routing"))))))

