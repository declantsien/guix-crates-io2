(define-module (crates-io gl _s gl_struct_loader) #:use-module (crates-io))

(define-public crate-gl_struct_loader-0.1.0 (c (n "gl_struct_loader") (v "0.1.0") (d (list (d (n "gl_types") (r "^0.1.0") (d #t) (k 0)))) (h "0lmpb4ryh4k6jsi8lchr1pxjvcd660wyapsrg9l63mnvfdcxax6f") (f (quote (("track_caller") ("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-gl_struct_loader-0.1.1 (c (n "gl_struct_loader") (v "0.1.1") (d (list (d (n "gl_types") (r "^0.1.0") (d #t) (k 0)))) (h "0n67daxz13lgjzz1w7py1xrgr8x3v28mjrwzdvp3aw2w6wxhdnsi") (f (quote (("track_caller") ("std" "alloc") ("default" "std") ("alloc"))))))

