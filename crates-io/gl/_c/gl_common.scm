(define-module (crates-io gl _c gl_common) #:use-module (crates-io))

(define-public crate-gl_common-0.0.2 (c (n "gl_common") (v "0.0.2") (h "05x6bbcxbznmqshxza65vrx1igqjimmv3ykjdvllncwl4h5id1qm")))

(define-public crate-gl_common-0.0.3 (c (n "gl_common") (v "0.0.3") (h "09k02zff065f8spf8b9mrsbgk78ypdjz21y6j69sn3rk3sz3770h")))

(define-public crate-gl_common-0.0.4 (c (n "gl_common") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1ha3vrclcna8j61r3m6zf5066bwag7zbxxzag0ssh2sm7qhn191v")))

(define-public crate-gl_common-0.1.0 (c (n "gl_common") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1jmi0qp8jx6699p39m3bqqw31mmxsj1gk07vpjmy87q56w2x6gy8")))

