(define-module (crates-io gl _c gl_constants) #:use-module (crates-io))

(define-public crate-gl_constants-0.1.0 (c (n "gl_constants") (v "0.1.0") (d (list (d (n "gl_types") (r "^0.1.0") (d #t) (k 0)))) (h "1xflaiigd1r1ldy0mb37c90apznif3qgjg2cjfbq9969psmxys9v")))

(define-public crate-gl_constants-0.1.1 (c (n "gl_constants") (v "0.1.1") (d (list (d (n "gl_types") (r "^0.1.0") (d #t) (k 0)))) (h "1l3cqsi96z44jxw8hl3kjk0rffyw7xi9j35jr2dmf5vdm66gmfg1")))

