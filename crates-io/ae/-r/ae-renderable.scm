(define-module (crates-io ae -r ae-renderable) #:use-module (crates-io))

(define-public crate-ae-renderable-0.1.1 (c (n "ae-renderable") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "10cnyxlw247cmlr79xj7j8g4a2g6ijv105mms494ps4vxrn0al3h") (r "1.65")))

