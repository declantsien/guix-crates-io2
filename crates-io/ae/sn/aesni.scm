(define-module (crates-io ae sn aesni) #:use-module (crates-io))

(define-public crate-aesni-0.1.0 (c (n "aesni") (v "0.1.0") (h "19svmcbpy64njy5m93ih30qzckfjbm2p6d01is2y7cyixaj1h71l") (y #t)))

(define-public crate-aesni-0.1.1 (c (n "aesni") (v "0.1.1") (h "1yh25vaanh0n5kca3jh2j02849j7xy7ils4bf22ld2vpz2v3ih50") (y #t)))

(define-public crate-aesni-0.1.2 (c (n "aesni") (v "0.1.2") (h "1scrkczp79i3g4va0kyf9zzj5705mxhzw6070d4rfnarw0p4x1z5") (y #t)))

(define-public crate-aesni-0.1.3 (c (n "aesni") (v "0.1.3") (h "04a725gdxkjw594ylhssk017ifxh7nhskjinbgwwk1c0n7l4l3xc") (y #t)))

(define-public crate-aesni-0.1.4 (c (n "aesni") (v "0.1.4") (h "0smmdr9acvn7lhal2br29h9pm865cqsbgq8nqk056xqwaszb22pg") (y #t)))

(define-public crate-aesni-0.2.0 (c (n "aesni") (v "0.2.0") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "1d5bgqb8mg6k71pmzzfbr9krsvwsvqnfwjfrab2i829k4zxl2kws") (y #t)))

(define-public crate-aesni-0.2.1 (c (n "aesni") (v "0.2.1") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "0qinly43fh3fwfk6v41f1sxyhib96hc52s6hfjk5ixqkgkg4gfmp") (y #t)))

(define-public crate-aesni-0.3.0 (c (n "aesni") (v "0.3.0") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "18shyrwqm750dxndm067qhi8gjnf5qqakg8jbxy0fabqi9n1kcir") (y #t)))

(define-public crate-aesni-0.2.2 (c (n "aesni") (v "0.2.2") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "0537pzrncinx8lccikrg95ai5dip1ryp9k6vgfpn056niilw722y") (y #t)))

(define-public crate-aesni-0.3.1 (c (n "aesni") (v "0.3.1") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "0r41z45q4pzrbj4wvs2xhq6xqhkkdqn9ss44a1xjmarsxj3pl5ig") (f (quote (("ignore_target_feature_check")))) (y #t)))

(define-public crate-aesni-0.3.2 (c (n "aesni") (v "0.3.2") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "11ki7lybrnhv3ikabi66x8gs3yzgsmhc23wla12689md624d6ln7") (y #t)))

(define-public crate-aesni-0.3.3 (c (n "aesni") (v "0.3.3") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "1285ccjq6s0m1ykwb4742cp2kgzyil5mx3jzw473z3w8282hydgv") (y #t)))

(define-public crate-aesni-0.3.4 (c (n "aesni") (v "0.3.4") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "15md5r7kynnawjqjkxna92nli94c8drsggr4ifvz1n2ynip2x2mx") (y #t)))

(define-public crate-aesni-0.3.5 (c (n "aesni") (v "0.3.5") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "0pvv1rr3gsj93jds17061415viv9s3v4app0hp2ksz5lj530gjmi") (y #t)))

(define-public crate-aesni-0.4.0 (c (n "aesni") (v "0.4.0") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.1") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "1xaxk3q3197bvd8hzsns5x9qhm6k8r60a90ax9m0qb5n5la8r0zj") (f (quote (("default" "ctr") ("ctr")))) (y #t)))

(define-public crate-aesni-0.4.1 (c (n "aesni") (v "0.4.1") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.1") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "17ldjn62c44pv2pw6pcm11imna6yil5if75c0mpzgd1bkzxvf438") (f (quote (("nocheck") ("default" "ctr") ("ctr")))) (y #t)))

(define-public crate-aesni-0.5.0 (c (n "aesni") (v "0.5.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "stream-cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)))) (h "0sg3xdphac6nl7jji22wjlgy6xxvmix05v7959yavdmi3h3582b9") (f (quote (("nocheck") ("default" "ctr") ("ctr" "stream-cipher")))) (y #t)))

(define-public crate-aesni-0.5.1 (c (n "aesni") (v "0.5.1") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "stream-cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)))) (h "10igpp778szxicp7lz0a3adj9w0nckyq2qfabwwmp24x4iyfx4za") (f (quote (("nocheck") ("default" "ctr") ("ctr" "stream-cipher")))) (y #t)))

(define-public crate-aesni-0.6.0 (c (n "aesni") (v "0.6.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "007imgcfl82nilfpamj5dik83pkcmkzvbkxp384p7r3iz6sscw1g") (f (quote (("nocheck") ("default" "ctr") ("ctr" "stream-cipher")))) (y #t)))

(define-public crate-aesni-0.7.0 (c (n "aesni") (v "0.7.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.4") (f (quote ("block-cipher"))) (o #t) (d #t) (k 0)) (d (n "stream-cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "0r6j0mjkyqnwvgib01cvrwfw8rlx1biw75234niv723n1fdx6l6h") (f (quote (("nocheck") ("default" "ctr") ("ctr" "stream-cipher")))) (y #t)))

(define-public crate-aesni-0.8.0 (c (n "aesni") (v "0.8.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.6") (f (quote ("block-cipher"))) (o #t) (d #t) (k 0)) (d (n "stream-cipher") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)))) (h "12n6r06calfar3pzb77izj1pdi1y0j07g91f7qwpvc4b604fhvqa") (f (quote (("nocheck") ("default" "ctr") ("ctr" "stream-cipher"))))))

(define-public crate-aesni-0.9.0 (c (n "aesni") (v "0.9.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.7") (f (quote ("block-cipher"))) (o #t) (d #t) (k 0)) (d (n "stream-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)))) (h "0mp6zk485y66m8vvm6vk69bwmf5gdbkxf3xgmk89cfk3mraxd95n") (f (quote (("nocheck") ("default" "ctr") ("ctr" "stream-cipher"))))))

(define-public crate-aesni-0.10.0 (c (n "aesni") (v "0.10.0") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1kmh07fp9hbi1aa8dr2rybbgw8vqz6hjmk34c4w7sbscx7si2bpa") (f (quote (("nocheck") ("default" "ctr") ("ctr"))))))

(define-public crate-aesni-0.99.99 (c (n "aesni") (v "0.99.99") (h "01lvzqqqk8mi55f37py2d4vaxmis4m7cmpphxfrs9ypcq1313x2p")))

