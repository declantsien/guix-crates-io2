(define-module (crates-io ae s_ aes_crypto) #:use-module (crates-io))

(define-public crate-aes_crypto-0.1.0 (c (n "aes_crypto") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0hrz9m6dwbv1wi932hwvy6mdv8fqkaxvas25g3hl0b0kp8w74alk") (f (quote (("vaes")))) (y #t)))

(define-public crate-aes_crypto-0.2.0 (c (n "aes_crypto") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0lf0slzyhifjkjn5wbxbig8cxd99mds4j812avjjrnxjxdhc6195") (f (quote (("vaes"))))))

(define-public crate-aes_crypto-0.2.1 (c (n "aes_crypto") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0fpqw1b45nr18yiznzdrb4fxapmhhkkj81k8lmp5zda1kl4hg7al") (f (quote (("vaes"))))))

(define-public crate-aes_crypto-0.2.2 (c (n "aes_crypto") (v "0.2.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0ja8pxd6ykjks543s8snvdvkz92v5bgm8q30fbfm91xcj2vl3m0c") (f (quote (("vaes"))))))

(define-public crate-aes_crypto-0.2.3 (c (n "aes_crypto") (v "0.2.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0j5yap9ics0xrgfm63yfbp4fa11ixc2sj2afq7f3d0bnh5wsjwrg") (f (quote (("vaes"))))))

(define-public crate-aes_crypto-1.0.0 (c (n "aes_crypto") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "118qh301vkigcx0q9zjsn96n539ycxghyj18smf4nrhm25nnfijq") (f (quote (("nightly")))) (y #t)))

(define-public crate-aes_crypto-1.1.0 (c (n "aes_crypto") (v "1.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0bdf2vgfz4vpnnbr68vgisml44s6vaixvnqqfgdx5w2k0vzcq3zm") (f (quote (("nightly"))))))

(define-public crate-aes_crypto-1.2.0 (c (n "aes_crypto") (v "1.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "09n1s0vvdf7yba9ds0whmfh24j6ik6f65bgq9kf45anaxbp6m3zq") (f (quote (("nightly"))))))

