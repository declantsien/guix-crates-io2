(define-module (crates-io ae s_ aes_frast) #:use-module (crates-io))

(define-public crate-aes_frast-0.1.0 (c (n "aes_frast") (v "0.1.0") (h "0ph5b7s1hkg5wvad03i63az4ldmy47d1mf78lrfgzcd9z13gq9gz")))

(define-public crate-aes_frast-0.1.1 (c (n "aes_frast") (v "0.1.1") (h "18rpnvfkllm2d2zx4yad9qakx0kqirs27ksnaxj3jmwj1bfk0hrm") (y #t)))

(define-public crate-aes_frast-0.1.2 (c (n "aes_frast") (v "0.1.2") (h "049bhrar7qnq3mcdj0qcdma9h86xrr64k042w9j72z5qrvx4hf9p")))

(define-public crate-aes_frast-0.1.4 (c (n "aes_frast") (v "0.1.4") (h "06vzjw9l8dm73cyp2dzphqh6yvvfbcm2lch9c0fl44r3w6iglahb")))

(define-public crate-aes_frast-0.1.5 (c (n "aes_frast") (v "0.1.5") (h "01af9vzdncw443x4513pfdaj22i198hrwl6ffq2jy9x95k0mzb8c")))

(define-public crate-aes_frast-0.2.0 (c (n "aes_frast") (v "0.2.0") (h "0ndgw4130wfkrqns4yxih4qnsbqnh3ywljf6j7ha5bn7w5vb5rh7")))

(define-public crate-aes_frast-0.2.1 (c (n "aes_frast") (v "0.2.1") (h "0182d6w36j8kdg18r57i7b4khwq3fp633x5waa8gq97mhf05ksan")))

(define-public crate-aes_frast-0.2.2 (c (n "aes_frast") (v "0.2.2") (h "0rcj55whfh9ykf8nknkhqfyyrp420ifx88vc8vgrq61pcwchwrwz")))

