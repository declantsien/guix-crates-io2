(define-module (crates-io ae on aeon_derive) #:use-module (crates-io))

(define-public crate-aeon_derive-0.2.0 (c (n "aeon_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "08nbrrglhhvvqlin3hibm34xzn0787a1a006y9jmb3bl8h919nxn")))

