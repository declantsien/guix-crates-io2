(define-module (crates-io ae on aeon) #:use-module (crates-io))

(define-public crate-aeon-0.1.0 (c (n "aeon") (v "0.1.0") (h "1drrm6c35jj00hrng1z4v4xbgn9s4wkbi3avmqqvm8011vsdpknp")))

(define-public crate-aeon-0.2.0 (c (n "aeon") (v "0.2.0") (h "1alxfxfw9ih9gdns7s7gq10wa76kkdv3j76p0hvh2s78sszmr0fm")))

(define-public crate-aeon-0.3.1 (c (n "aeon") (v "0.3.1") (h "0ansp582g8qnarc0nkpi4qrfpzizcbnv59vlv4n068kihjmwwi3n")))

