(define-module (crates-io ae ru aeruginous-io) #:use-module (crates-io))

(define-public crate-aeruginous-io-0.1.0 (c (n "aeruginous-io") (v "0.1.0") (d (list (d (n "sysexits") (r "^0.7.10") (d #t) (k 0)))) (h "006yjqhwv7c5565zbdiqz68ha2h8iiqaqzamv686bbwvpqg5rp3p") (r "1.75.0")))

(define-public crate-aeruginous-io-0.2.0 (c (n "aeruginous-io") (v "0.2.0") (d (list (d (n "sysexits") (r "^0.7.10") (d #t) (k 0)))) (h "074n2n70n45hvycrz8qay3l5qpq6fzp4h60xvjmi41kvmfdzidpm") (r "1.75.0")))

(define-public crate-aeruginous-io-0.3.0 (c (n "aeruginous-io") (v "0.3.0") (d (list (d (n "sysexits") (r "^0.7.10") (d #t) (k 0)))) (h "14s8vmlqga08ahsb93zal6d29zf3wfrj5ghcbvzdp5nqxvmpxsr4") (r "1.75.0")))

(define-public crate-aeruginous-io-0.4.0 (c (n "aeruginous-io") (v "0.4.0") (d (list (d (n "sysexits") (r "^0.7.11") (d #t) (k 0)))) (h "1k4vir8yradri73gpskir1wfppnp1ri0nq620jg5cjl7mn1nqjh5") (r "1.75.0")))

(define-public crate-aeruginous-io-0.5.0 (c (n "aeruginous-io") (v "0.5.0") (d (list (d (n "sysexits") (r "^0.7.11") (d #t) (k 0)))) (h "1h513wpkaj4hswvwqs8b682akjssbgsc5q0l5n5gfm262hmn52jg") (r "1.75.0")))

(define-public crate-aeruginous-io-0.6.0 (c (n "aeruginous-io") (v "0.6.0") (d (list (d (n "sysexits") (r "^0.7.11") (d #t) (k 0)))) (h "1vi7y1bldyh98r4ymfhqbrm27vh2aa851hdmr9qhmb8zhr0kb5xq") (r "1.75.0")))

(define-public crate-aeruginous-io-0.7.0 (c (n "aeruginous-io") (v "0.7.0") (d (list (d (n "sysexits") (r "^0.7.11") (d #t) (k 0)))) (h "0bk5sid7sff221bpw3q32hvyxlx0ljip1k5fz9lpp6mad519sbxw") (r "1.75.0")))

(define-public crate-aeruginous-io-0.8.0 (c (n "aeruginous-io") (v "0.8.0") (d (list (d (n "sysexits") (r "^0.7.11") (d #t) (k 0)))) (h "07fr018q47dm2v4h0rqsqadsal92qw88v022gdkwvx8k7y727b32") (r "1.76.0")))

