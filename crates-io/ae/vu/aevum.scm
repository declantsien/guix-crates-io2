(define-module (crates-io ae vu aevum) #:use-module (crates-io))

(define-public crate-aevum-0.1.0 (c (n "aevum") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "01m145azr22zib3rg4b577qp30nj5748l9lx14rvq5w71gzql410")))

(define-public crate-aevum-0.1.1 (c (n "aevum") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "091h2xis2ii1k38fdlv472ll1liwznyxlm999sz6264l8wi0afzl")))

