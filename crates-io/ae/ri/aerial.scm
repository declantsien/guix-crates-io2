(define-module (crates-io ae ri aerial) #:use-module (crates-io))

(define-public crate-aerial-0.1.0 (c (n "aerial") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.2.11") (d #t) (k 0)) (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.47") (d #t) (k 0)) (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "tendril") (r "^0.1.6") (d #t) (k 0)) (d (n "url") (r "^0.5.2") (d #t) (k 0)))) (h "13mv28i062y2n265bqc0rwnd7y903rz7c53jx729b43xfpszy38a")))

