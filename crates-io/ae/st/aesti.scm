(define-module (crates-io ae st aesti) #:use-module (crates-io))

(define-public crate-aesti-0.1.0 (c (n "aesti") (v "0.1.0") (d (list (d (n "fmt-extra") (r "^0.1") (d #t) (k 2)) (d (n "hex") (r "^0.2") (d #t) (k 2)) (d (n "index-fixed") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0jf3r4ihw4fdcdbvk6w088rrqf7v47605y8jgrbi5g7hd6b5ysmv")))

(define-public crate-aesti-0.2.0 (c (n "aesti") (v "0.2.0") (d (list (d (n "fmt-extra") (r "^0.1") (d #t) (k 2)) (d (n "hex") (r "^0.2") (d #t) (k 2)) (d (n "index-fixed") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0y1f3vdzgdj7whbmac3yipiy66s8mcrp9rv3115jdm6zd95ip7jr")))

(define-public crate-aesti-0.2.1 (c (n "aesti") (v "0.2.1") (d (list (d (n "fmt-extra") (r "^0.1") (d #t) (k 2)) (d (n "hex") (r "^0.2") (d #t) (k 2)) (d (n "index-fixed") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0n44k0xjyy5xjfy4h7w3y4z7cajfd49cdrrslj4jbl8n8sr4rbzn")))

(define-public crate-aesti-0.3.0 (c (n "aesti") (v "0.3.0") (d (list (d (n "fmt-extra") (r "^0.1") (d #t) (k 2)) (d (n "hex") (r "^0.2") (d #t) (k 2)) (d (n "index-fixed") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0vbiaj210i5fb3nf6vvdkkhiyyhvzq5m9z06f8aiaxnx3i4202d0")))

