(define-module (crates-io ae -d ae-direction) #:use-module (crates-io))

(define-public crate-ae-direction-0.1.0 (c (n "ae-direction") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1gjh8gpiadjpk4900bm0mcaj961av4fzqf5wxrzd1phjbqgygjnf") (r "1.65")))

(define-public crate-ae-direction-0.1.1 (c (n "ae-direction") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1g7cknm31h3mfl64c408152a4xav4fja172bd0484cr82307mpd1") (r "1.65")))

(define-public crate-ae-direction-0.1.2 (c (n "ae-direction") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.90") (k 0)) (d (n "typeshare") (r "^1.0.0") (d #t) (k 0)))) (h "11i8dmxxzz6a1pdh6yk4fy2nzbyjky5821v3003f7smhhf7ww6ms") (r "1.65")))

(define-public crate-ae-direction-0.1.3 (c (n "ae-direction") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.90") (k 0)) (d (n "typeshare") (r "^1.0.0") (d #t) (k 0)))) (h "05lnl540f4yvrlnyvmmvx0jqcwpm3yfs92riqzk94dhp83ip95fv") (r "1.65")))

(define-public crate-ae-direction-0.1.4 (c (n "ae-direction") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pfbynx8nghgysf8xwvfhrcff5x92wgshgp9gjx3aan0pxpqw8rw") (r "1.65")))

