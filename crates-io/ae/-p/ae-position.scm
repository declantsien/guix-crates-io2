(define-module (crates-io ae -p ae-position) #:use-module (crates-io))

(define-public crate-ae-position-0.1.1 (c (n "ae-position") (v "0.1.1") (d (list (d (n "ae-direction") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "161yqxc3cp69cplc7h30k1swnq82hkyh2ypi1pca87qd0jwa89bf") (r "1.65")))

(define-public crate-ae-position-0.1.2 (c (n "ae-position") (v "0.1.2") (d (list (d (n "ae-direction") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.90") (k 0)) (d (n "typeshare") (r "^1.0.0") (d #t) (k 0)))) (h "0my9kx4nlw64jix96pb5arg3ig9xi37qww9p59sap0c11rxm89pz") (r "1.65")))

(define-public crate-ae-position-0.1.4 (c (n "ae-position") (v "0.1.4") (d (list (d (n "ae-direction") (r "^0.1.1") (d #t) (k 0)))) (h "08almq1zc2fqna67vxssp7wqfy0njcbla9yfs4qz6nfcdlaf9s39") (r "1.65")))

