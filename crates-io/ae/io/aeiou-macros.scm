(define-module (crates-io ae io aeiou-macros) #:use-module (crates-io))

(define-public crate-aeiou-macros-0.1.0 (c (n "aeiou-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1hn7nh0q3bzxws41qy5jjv4qkz5kqmpw9zah97wvnhxbwp6a8cy4")))

