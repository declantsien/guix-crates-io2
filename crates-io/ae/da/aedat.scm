(define-module (crates-io ae da aedat) #:use-module (crates-io))

(define-public crate-aedat-1.2.3 (c (n "aedat") (v "1.2.3") (d (list (d (n "flatbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1qglsv7qh7nj5zmv5xrllf80cdbkzmdi3qmah58jm2h7rn8h3sp1")))

(define-public crate-aedat-1.2.4 (c (n "aedat") (v "1.2.4") (d (list (d (n "flatbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1x6hrvf30cdd781jyjynpmgqsr6az90s5qdbgfxbv98bqgv64q2d")))

(define-public crate-aedat-1.2.5 (c (n "aedat") (v "1.2.5") (d (list (d (n "flatbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "0f5q2cx2c0kprd2z5nfivca37qcxnjlfm54l8pvb8m6f0gqz2bpp")))

(define-public crate-aedat-1.2.6 (c (n "aedat") (v "1.2.6") (d (list (d (n "flatbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "0w7ssnn4z4d94v9lbipbcs5bmsjbwswa22bx8vyahz13kp7fw2g1")))

(define-public crate-aedat-1.2.7 (c (n "aedat") (v "1.2.7") (d (list (d (n "flatbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1jdcyp9bb3cd068yvqcqkwf2lx4na513s6day2bcnmdr8p3cakvr")))

(define-public crate-aedat-1.3.0 (c (n "aedat") (v "1.3.0") (d (list (d (n "flatbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "10jwk0dj12kpv13xkx9xma95w6nhnmwi0a23jimw37vi7gsjaqdp")))

(define-public crate-aedat-1.3.1 (c (n "aedat") (v "1.3.1") (d (list (d (n "flatbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "13d4flzgvnmh08x2mhq9bpji60m3alychhhgvkci822bbzcivjg6")))

(define-public crate-aedat-1.3.2 (c (n "aedat") (v "1.3.2") (d (list (d (n "flatbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1s7a6xr7pqclvr6zzyrdp40fb5yr1dlac8jxdszk17phzzwanfj8")))

(define-public crate-aedat-1.3.3 (c (n "aedat") (v "1.3.3") (d (list (d (n "flatbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "084prciw9fixwc8h4lv2b91gw46izd5lcsrgg1xz10nhazxs24s6")))

