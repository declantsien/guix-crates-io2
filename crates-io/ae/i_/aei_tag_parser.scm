(define-module (crates-io ae i_ aei_tag_parser) #:use-module (crates-io))

(define-public crate-aei_tag_parser-1.0.0 (c (n "aei_tag_parser") (v "1.0.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mhg3cn6dcd5w77b219b00kia500ga045idbb7c3qsd9hk7svxmw")))

(define-public crate-aei_tag_parser-1.0.1 (c (n "aei_tag_parser") (v "1.0.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "058cpc2sd1vilzzbd5sjjag355g9ajynrx629mznip86yv4l9xlw")))

(define-public crate-aei_tag_parser-1.0.2 (c (n "aei_tag_parser") (v "1.0.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10fpv3gs1vgm53qgpfis292i0k01pmrbai5fqrsx144jvz0fysqg")))

(define-public crate-aei_tag_parser-1.1.0 (c (n "aei_tag_parser") (v "1.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rv95kjlpxnzi5wflc6mvwpkl2354c2cx98fj5y0caf1hswmgq82")))

(define-public crate-aei_tag_parser-1.2.0 (c (n "aei_tag_parser") (v "1.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dvbd3va66hhjlq5sj8w70m2q1i3hxgnyh503j5gl12aq3dy551r")))

(define-public crate-aei_tag_parser-1.2.1 (c (n "aei_tag_parser") (v "1.2.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12jd5cq7ff8lqs4zks66z5hx830fbjiyng8vwnbvfld75jkz5pc4")))

(define-public crate-aei_tag_parser-1.2.2 (c (n "aei_tag_parser") (v "1.2.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sx1afxaz6jr6jk6g11av05ghrh59a6km9f8xb4r25rd1jpkqxv3")))

