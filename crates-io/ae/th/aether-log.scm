(define-module (crates-io ae th aether-log) #:use-module (crates-io))

(define-public crate-aether-log-0.1.0 (c (n "aether-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (o #t) (k 0)))) (h "094h038q6gy2dfi1h47fsxkwpdfy0pv1m5z7c85v1ivbvxlqin95") (f (quote (("default" "archive")))) (s 2) (e (quote (("archive" "dep:zip"))))))

(define-public crate-aether-log-0.1.1 (c (n "aether-log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (o #t) (k 0)))) (h "18fzqk0r5jrlrnlaxhfn5zmxj5npwl84z8kmaq694nccr7hl769m") (f (quote (("default" "archive")))) (s 2) (e (quote (("archive" "dep:zip"))))))

