(define-module (crates-io ae th aetherus) #:use-module (crates-io))

(define-public crate-Aetherus-0.1.0 (c (n "Aetherus") (v "0.1.0") (h "167dla9z2w2hcd8ff9ikrwd32ma7vwxcx4h027dbkx9c6nyazz2k") (y #t)))

(define-public crate-Aetherus-0.1.1 (c (n "Aetherus") (v "0.1.1") (h "0jbl92g2ww4r57yjqmlcjbypz51aa3xb6x2y12dc7r78awm06g51")))

