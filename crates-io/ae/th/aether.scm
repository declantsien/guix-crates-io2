(define-module (crates-io ae th aether) #:use-module (crates-io))

(define-public crate-aether-0.0.1 (c (n "aether") (v "0.0.1") (d (list (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "wasmi") (r "^0.4") (d #t) (k 0)))) (h "0cdzlg4qgncaq2kbkz5wwxfa5p8clxd5y42pr36nhspxmc6qgkbj")))

(define-public crate-aether-0.0.2 (c (n "aether") (v "0.0.2") (d (list (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "wasmi") (r "^0.4") (d #t) (k 0)))) (h "0nin6a11jh1iv9hwh4ab6fsjcban92djr9z38nvz3x9klmy9zk75")))

(define-public crate-aether-0.0.3 (c (n "aether") (v "0.0.3") (d (list (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "wasmi") (r "^0.4") (d #t) (k 0)))) (h "10cnkbii1pj4iaq73m2lndm79if27l5kcfghshrj7zz4isgi28gq")))

