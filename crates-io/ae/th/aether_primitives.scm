(define-module (crates-io ae th aether_primitives) #:use-module (crates-io))

(define-public crate-aether_primitives-0.1.0 (c (n "aether_primitives") (v "0.1.0") (d (list (d (n "chfft") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-complex") (r ">= 0.0.0") (d #t) (k 0)) (d (n "num-traits") (r ">= 0.0.0") (d #t) (k 0)))) (h "1i2y6xhs9ghn8mvxppifddjlpjmnsgzqnca9jnffghlp521rf2zm") (f (quote (("fft_chfft" "chfft" "fft") ("fft") ("default"))))))

