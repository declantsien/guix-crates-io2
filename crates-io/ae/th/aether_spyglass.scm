(define-module (crates-io ae th aether_spyglass) #:use-module (crates-io))

(define-public crate-aether_spyglass-0.1.0 (c (n "aether_spyglass") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_winit" "bevy_render" "bevy_core_pipeline" "bevy_pbr"))) (k 2)) (d (n "bevy_egui") (r "^0.20.2") (d #t) (k 0)))) (h "0h4b1mr5aaab4lrng9k6np5m59dh61crj41iwcr304pyrpp2a2gg")))

(define-public crate-aether_spyglass-0.2.0 (c (n "aether_spyglass") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_winit" "bevy_render" "bevy_core_pipeline" "bevy_pbr"))) (k 2)) (d (n "bevy_egui") (r "^0.21.0") (d #t) (k 0)))) (h "0lpah71m67xdzis8b643131da51lw6zc72jfyninrr3y6w8n5c14")))

