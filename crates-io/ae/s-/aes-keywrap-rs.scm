(define-module (crates-io ae s- aes-keywrap-rs) #:use-module (crates-io))

(define-public crate-aes-keywrap-rs-0.1.0 (c (n "aes-keywrap-rs") (v "0.1.0") (d (list (d (n "crypto2") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0hsb4xlzmpiwq4cn5bsibfcj3amc5gn3wzxcjjxdvk3vwf2yfrcn")))

(define-public crate-aes-keywrap-rs-0.2.0 (c (n "aes-keywrap-rs") (v "0.2.0") (d (list (d (n "crypto2") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1zg22vb8fzxsavvfbd7h260ssia0pibbrjk8k23vf4c175hd4y81")))

