(define-module (crates-io ae s- aes-config) #:use-module (crates-io))

(define-public crate-aes-config-0.1.1 (c (n "aes-config") (v "0.1.1") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "10qa40w6bxq8y0i276l6b5wls6rhbbsv0zk9h17m191p378s6303")))

(define-public crate-aes-config-0.1.2 (c (n "aes-config") (v "0.1.2") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0d9miy20l5bwc5hv4rd9dxs8hl2401z90zk2qsr35bb1avhia27l")))

