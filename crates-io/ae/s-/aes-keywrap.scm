(define-module (crates-io ae s- aes-keywrap) #:use-module (crates-io))

(define-public crate-aes-keywrap-0.1.0 (c (n "aes-keywrap") (v "0.1.0") (d (list (d (n "aes") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "0ig3ybg7vfkrxig7657n3m8x5slhahrsx8dmvjb26kxj572j8hfy")))

(define-public crate-aes-keywrap-0.2.0 (c (n "aes-keywrap") (v "0.2.0") (d (list (d (n "aes") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "1mkajh6yh12x9k1ryrdcsgv1vip8yzc8pnsk77h6l1xrq71z1xwq")))

(define-public crate-aes-keywrap-0.2.1 (c (n "aes-keywrap") (v "0.2.1") (d (list (d (n "aes") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "1bjamndqr0ps0wa1npdxqrhczh760ddawdqarxsyda8lilzbqk6z")))

(define-public crate-aes-keywrap-0.2.2 (c (n "aes-keywrap") (v "0.2.2") (d (list (d (n "aes") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "1wzrdmrbmrx21ynj2yfz88l8sc7da9pg20v860n1bqhn4vhg8gaf")))

(define-public crate-aes-keywrap-0.2.3 (c (n "aes-keywrap") (v "0.2.3") (d (list (d (n "aes") (r "^0.7.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "0v6qjv028yfgq8chp3az1cphdbijpbwy89pnrl7abnjx4jax1dmx")))

(define-public crate-aes-keywrap-0.2.4 (c (n "aes-keywrap") (v "0.2.4") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "09qi2bzy3z97735vfgsh5fjwbrv6iq864cx0339crgfah6p4yif1")))

(define-public crate-aes-keywrap-0.2.5 (c (n "aes-keywrap") (v "0.2.5") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "14ckbji8ywykjj33sa3gz1i80xf8w3yi3p68rxf9nn9bgsmvfw0b")))

