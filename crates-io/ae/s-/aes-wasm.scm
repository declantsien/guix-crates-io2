(define-module (crates-io ae s- aes-wasm) #:use-module (crates-io))

(define-public crate-aes-wasm-0.1.0 (c (n "aes-wasm") (v "0.1.0") (d (list (d (n "aegis") (r "^0.3.0") (d #t) (k 2)) (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.7") (d #t) (k 2)) (d (n "cmac") (r "^0.7.2") (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)))) (h "1qlx5fqvn58h6ks8a67cpd7cx0krbfrr5j9wnhp1wyka4m5md4rn")))

(define-public crate-aes-wasm-0.1.1 (c (n "aes-wasm") (v "0.1.1") (d (list (d (n "aegis") (r "^0.3.0") (d #t) (k 2)) (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.7") (d #t) (k 2)) (d (n "cmac") (r "^0.7.2") (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)))) (h "0zngbw1nzipjk7yh3x7dqrgzy9kinsa0lwmbhilwva41lx56kbkv")))

(define-public crate-aes-wasm-0.1.2 (c (n "aes-wasm") (v "0.1.2") (d (list (d (n "aegis") (r "^0.3.0") (d #t) (k 2)) (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.7") (d #t) (k 2)) (d (n "cmac") (r "^0.7.2") (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)))) (h "1jvmq11a9xss3w9c5rmqd2dylqsnyxq1alchgdrb9mlj2rajcwzi")))

(define-public crate-aes-wasm-0.1.3 (c (n "aes-wasm") (v "0.1.3") (d (list (d (n "aegis") (r "^0.3.0") (d #t) (k 2)) (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.7") (d #t) (k 2)) (d (n "cmac") (r "^0.7.2") (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)))) (h "138r513nxxpr1d7jsdda48dd54r2pqj6dnp42vwf3grb78mazwr9")))

(define-public crate-aes-wasm-0.1.4 (c (n "aes-wasm") (v "0.1.4") (d (list (d (n "aegis") (r "^0.3.0") (d #t) (k 2)) (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.7") (d #t) (k 2)) (d (n "cmac") (r "^0.7.2") (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)))) (h "1mylnr1n9ks1qhlrnkx5zam9kkbpaw9gi9fy805m6swkxnv3ldsb")))

(define-public crate-aes-wasm-0.1.5 (c (n "aes-wasm") (v "0.1.5") (d (list (d (n "aegis") (r "^0.4.3") (d #t) (k 2)) (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "cmac") (r "^0.7.2") (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)))) (h "0ci8w0mzg1whddv9isf9v9p98p2q0gbc4b1kg8jpcw17qaw7ihv1")))

(define-public crate-aes-wasm-0.1.6 (c (n "aes-wasm") (v "0.1.6") (d (list (d (n "aegis") (r "^0.4.3") (d #t) (k 2)) (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "cmac") (r "^0.7.2") (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)))) (h "0a3cqnrlfagdgq5ixra432fqcjhqb1mpajz37q1az3m9jsb56h7y")))

(define-public crate-aes-wasm-0.1.7 (c (n "aes-wasm") (v "0.1.7") (d (list (d (n "aegis") (r "^0.4.4") (d #t) (k 2)) (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "cmac") (r "^0.7.2") (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)))) (h "16a07nw5lilhxayiw0zpn6wr6l9kdjgj01fyqv95bzwlbf2ykvdd")))

(define-public crate-aes-wasm-0.1.8 (c (n "aes-wasm") (v "0.1.8") (d (list (d (n "aegis") (r "^0.6.2") (d #t) (k 2)) (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "cmac") (r "^0.7.2") (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)))) (h "0njc95x9d449znvzim08sm764da43bn7fl30ahg6w1f3s75nx5ia")))

(define-public crate-aes-wasm-0.1.9 (c (n "aes-wasm") (v "0.1.9") (d (list (d (n "aegis") (r "^0.6.2") (d #t) (k 2)) (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.9") (d #t) (k 2)) (d (n "cmac") (r "^0.7.2") (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)))) (h "0niwkb0m4fx86hf108inhx4gagxbngkphcyhr9ihrv1da6ih3dm1")))

(define-public crate-aes-wasm-0.1.10 (c (n "aes-wasm") (v "0.1.10") (d (list (d (n "aegis") (r "^0.6.3") (d #t) (k 2)) (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.9") (d #t) (k 2)) (d (n "cmac") (r "^0.7.2") (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)))) (h "0ijmyf8ryv06fmkyv2wfx6mqsbp1fs3laqf2m8k1dydyw85ffxja")))

