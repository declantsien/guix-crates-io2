(define-module (crates-io ae s- aes-ndlr) #:use-module (crates-io))

(define-public crate-aes-ndlr-0.0.1 (c (n "aes-ndlr") (v "0.0.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "14pfq0y96dn117gp1d6g28nrh51l51mb0gqd1iqcvy151jqr0glk")))

(define-public crate-aes-ndlr-0.0.2 (c (n "aes-ndlr") (v "0.0.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0c9klnydw6fkpmf5fii87a2yxz17qg4zmk8pp2ichn9k4zwpyvy7")))

(define-public crate-aes-ndlr-0.0.3 (c (n "aes-ndlr") (v "0.0.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1c56z22p52v1kwvpn2a6dlnmy8pybp4srxvmzmlcbpa80n4hvs0q")))

