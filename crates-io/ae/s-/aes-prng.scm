(define-module (crates-io ae s- aes-prng) #:use-module (crates-io))

(define-public crate-aes-prng-0.1.0 (c (n "aes-prng") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1yf8b02gwnvr54l58nf2mnlyfvng80qhnhfp3l87l8ba4fdi4jxi")))

(define-public crate-aes-prng-0.2.0 (c (n "aes-prng") (v "0.2.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "00djkwpyv7k6sp0xpfxajvxpql5jn9wrl8l1ri0lzc3m65zc5s58")))

(define-public crate-aes-prng-0.2.1 (c (n "aes-prng") (v "0.2.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1adgvp4lr5zvdrr3pnbrf7qqzcrzpd4kbb5rgppdcd3hrd4wvk29")))

