(define-module (crates-io ae s- aes-sid) #:use-module (crates-io))

(define-public crate-aes-sid-0.0.0 (c (n "aes-sid") (v "0.0.0") (d (list (d (n "aes") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "cmac") (r "^0.2") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.7") (k 0)) (d (n "subtle") (r "^2") (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "0vkjks3p1v68h8h75wnkw7gdc77vrd6ib5qvby5j08lzj6i3z16r") (f (quote (("default" "aes")))) (y #t)))

(define-public crate-aes-sid-0.1.0 (c (n "aes-sid") (v "0.1.0") (d (list (d (n "aes") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "cmac") (r "^0.2") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.7") (k 0)) (d (n "subtle") (r "^2") (k 0)) (d (n "uuid") (r "^0.8") (o #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1pcbligfqwaxmvqflp9yfjbsydsfrs19mjmrw0vp6nai9fam75yk") (f (quote (("default" "aes")))) (y #t)))

(define-public crate-aes-sid-0.1.1 (c (n "aes-sid") (v "0.1.1") (h "1rd5naczqas3lg57vahg7l0j44s4f9wjgclqdxbydgq3ri4sy8j3") (y #t)))

