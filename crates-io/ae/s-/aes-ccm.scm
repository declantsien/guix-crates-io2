(define-module (crates-io ae s- aes-ccm) #:use-module (crates-io))

(define-public crate-aes-ccm-0.1.0 (c (n "aes-ccm") (v "0.1.0") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.0") (d #t) (k 2)))) (h "0494v3vy0m75k1zzslpj4q2wpwdm6i2k13s61rax113bkd4zr848")))

(define-public crate-aes-ccm-0.2.0 (c (n "aes-ccm") (v "0.2.0") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.0") (d #t) (k 2)))) (h "194bg9z9q3y2jhilijq1v4nw9bk57xa304njvmkg6dmlgy48l54a") (y #t)))

(define-public crate-aes-ccm-0.2.1 (c (n "aes-ccm") (v "0.2.1") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.0") (d #t) (k 2)))) (h "1yrxmghd9xsbp673i1wvs0srhqjc22wn4q4lz5bz7g98f46rfzx3")))

(define-public crate-aes-ccm-0.2.2 (c (n "aes-ccm") (v "0.2.2") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.0") (d #t) (k 2)))) (h "0dclzn2yrqzwwm2c3ghvndh9kp4rqij2nk4fbxj24j00z52ahm69")))

(define-public crate-aes-ccm-0.3.0 (c (n "aes-ccm") (v "0.3.0") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.0") (d #t) (k 2)))) (h "0yd6jx5wflzdcs78n5i4m757365yizwgf4k9lla7gk6pbi8rbmja") (f (quote (("std") ("default" "std"))))))

(define-public crate-aes-ccm-0.4.0 (c (n "aes-ccm") (v "0.4.0") (d (list (d (n "aead") (r "^0.2") (k 0)) (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.0") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "1f9518262dx2ayj9x035l47v30j9ly3msck6yc118hdnzvvw9iky") (f (quote (("heapless" "aead/heapless") ("default" "alloc") ("alloc" "aead/alloc"))))))

(define-public crate-aes-ccm-0.4.1 (c (n "aes-ccm") (v "0.4.1") (d (list (d (n "aead") (r "^0.2") (k 0)) (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.0") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "1f67hz8hmkd7rwhi49z3k5hcpw4ynwaxzshvgm6k78kg2czifyc3") (f (quote (("heapless" "aead/heapless") ("default" "alloc") ("alloc" "aead/alloc"))))))

(define-public crate-aes-ccm-0.5.0 (c (n "aes-ccm") (v "0.5.0") (d (list (d (n "aead") (r "^0.3") (k 0)) (d (n "aes") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (k 0)) (d (n "hex-literal") (r "^0.2.0") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "1169bgja8y9d8rbc1rqjn67k96y1pgg27jd3bcs2bqnhq3xp1fpz") (f (quote (("heapless" "aead/heapless") ("default" "aes" "alloc") ("alloc" "aead/alloc"))))))

