(define-module (crates-io ae s- aes-soft) #:use-module (crates-io))

(define-public crate-aes-soft-0.1.0 (c (n "aes-soft") (v "0.1.0") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "1ykwjz3v97xg5wr3g38b8vmbxw4fipwn6dgm04yhl6wc5ms03x4i") (y #t)))

(define-public crate-aes-soft-0.2.0 (c (n "aes-soft") (v "0.2.0") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "1cfch83alzxvq1bbf8xww7ffwz2xj00qm6cn3sq5r84hl2q07k37") (y #t)))

(define-public crate-aes-soft-0.3.0 (c (n "aes-soft") (v "0.3.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0cj1i8fzgs48b094gd1p2vc8nashs228asacblmy2131wn964plr") (y #t)))

(define-public crate-aes-soft-0.3.1 (c (n "aes-soft") (v "0.3.1") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "11p145gqndd76qpwab8pybm18r9v3916kmaq3jwbba74i403cb04") (y #t)))

(define-public crate-aes-soft-0.3.2 (c (n "aes-soft") (v "0.3.2") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1f4whbjhsw7v702z3j5rlrs8yhaqykhqxpyihsxl0s36i73ikp5c") (y #t)))

(define-public crate-aes-soft-0.3.3 (c (n "aes-soft") (v "0.3.3") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "039si7yjp0wcd750sgq52c60sh2ikaxwd7rq7g0ba7ws7ypfgmyg") (y #t)))

(define-public crate-aes-soft-0.4.0 (c (n "aes-soft") (v "0.4.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "19szsg0qqxq42k7bj5p3svb147n8wxy9a20n4g7mcl2fwrz689a9") (y #t)))

(define-public crate-aes-soft-0.5.0 (c (n "aes-soft") (v "0.5.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1djwaygvf4z4ki50vnq3k983rgcx238hyl1vxzbplcj9kj493pb3") (y #t)))

(define-public crate-aes-soft-0.6.0 (c (n "aes-soft") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "15k2a89zmind79h0a86ldyq06g3fc971nnynrd2dy3ff8zxb2nlc")))

(define-public crate-aes-soft-0.6.1 (c (n "aes-soft") (v "0.6.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "16n7r3ngy82fnhhd0jvwrrwhd7cj8wxk98qykgbly8p001bbipbn")))

(define-public crate-aes-soft-0.6.2 (c (n "aes-soft") (v "0.6.2") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1zm7h43hbzms9p52bpcl6vj6jgzg07wqnd1bq4h7xcha1m1696g3")))

(define-public crate-aes-soft-0.6.3 (c (n "aes-soft") (v "0.6.3") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1516iwnbykf82diww8idwzg1k9rhppbnjfvsb77lx1d3gg4xp2sf")))

(define-public crate-aes-soft-0.6.4 (c (n "aes-soft") (v "0.6.4") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0wj0fi2pvmlw09yvb1aqf0mfkzrfxmjsf90finijh255ir4wf55y") (f (quote (("semi_fixslice"))))))

(define-public crate-aes-soft-0.99.99 (c (n "aes-soft") (v "0.99.99") (h "05wvvns7mv9s4jl1najm0x0mw1k7i8p19gykjw5vx55c0rnvbgwq")))

