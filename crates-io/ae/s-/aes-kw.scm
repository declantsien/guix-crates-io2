(define-module (crates-io ae s- aes-kw) #:use-module (crates-io))

(define-public crate-aes-kw-0.0.0 (c (n "aes-kw") (v "0.0.0") (h "1kr61a5incyvlynnj729v5gdlnv66644ri600jqwczzy0d2g1k8f") (y #t)))

(define-public crate-aes-kw-0.1.0 (c (n "aes-kw") (v "0.1.0") (d (list (d (n "aes") (r "^0.7") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1l7fpshn6wdvd8v4ikf0w880i1f6k2fcqaw1ifbw56zraqqzwb6s") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-aes-kw-0.2.0 (c (n "aes-kw") (v "0.2.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0cdfk6b0xxi1p01pxidi6m32q3jijvbqc6cagi14mfw95zacp74x") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-aes-kw-0.2.1 (c (n "aes-kw") (v "0.2.1") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "131xvnah1magbr8q0lwmg3c13lv54vh41f2z79zmzyyf5lsjpyk9") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

