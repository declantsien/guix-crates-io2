(define-module (crates-io ae s- aes-256-gcm) #:use-module (crates-io))

(define-public crate-aes-256-gcm-1.0.0 (c (n "aes-256-gcm") (v "1.0.0") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0z8d74ijgb3kzbqmph8cvxj5zlncxlbmzxwn1si532v943lafh4z") (f (quote (("default"))))))

(define-public crate-aes-256-gcm-1.1.0 (c (n "aes-256-gcm") (v "1.1.0") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0wbmral8s2s5hrpyd40v1128l92g54w4qykyvab366060215jllf") (f (quote (("default")))) (y #t)))

(define-public crate-aes-256-gcm-1.1.1 (c (n "aes-256-gcm") (v "1.1.1") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0wzks51wgd499a7h3hzrk22rhk00ffb7zm4k7wbakfsc1f0wrz4z") (f (quote (("default"))))))

(define-public crate-aes-256-gcm-1.1.2 (c (n "aes-256-gcm") (v "1.1.2") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1hq0i93h5899kxlcfn4w39w5fwip0lxygch7c8cicdxwfrs7rgc1") (f (quote (("default"))))))

