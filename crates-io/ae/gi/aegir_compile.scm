(define-module (crates-io ae gi aegir_compile) #:use-module (crates-io))

(define-public crate-aegir_compile-0.1.0 (c (n "aegir_compile") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full" "extra-traits"))) (d #t) (k 0)))) (h "0m0ffxaq72bmr7x8b5cb5gqvwjz604zrd7sgk5azih831b9rgv3n")))

(define-public crate-aegir_compile-0.2.0 (c (n "aegir_compile") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full" "extra-traits"))) (d #t) (k 0)))) (h "0z6zf5y8w8zwbnz2v41hri5wi6s7b704v7i3l62vg1llc4lxk7rx")))

