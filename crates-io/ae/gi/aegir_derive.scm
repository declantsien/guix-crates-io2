(define-module (crates-io ae gi aegir_derive) #:use-module (crates-io))

(define-public crate-aegir_derive-0.2.0 (c (n "aegir_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wqhk664gdny410kn4y0gz8vs3y3ih5xjc7gn1rd4v7wjknj1v2a")))

(define-public crate-aegir_derive-0.3.0 (c (n "aegir_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g2a42ml6qjd0pn5bs4p8663ywy8ms27f8jlncjgn4fkjalg0ma7")))

