(define-module (crates-io ae ad aead) #:use-module (crates-io))

(define-public crate-aead-0.0.0 (c (n "aead") (v "0.0.0") (h "12yz63abmx8ak1hwm66cx74mxsix1hgrqvblycfl6glf2id0gdlc") (y #t)))

(define-public crate-aead-0.1.0 (c (n "aead") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.12") (k 0)))) (h "185zgbgfdfrvfzx08646ss69vg6mrbw6r57an7xrinwzwjf048y1")))

(define-public crate-aead-0.1.1 (c (n "aead") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.12") (k 0)))) (h "0xwb2qfhj3lha0pmnjprmx0b8jbgi5knwfchs5axjmfsd5vy56jj")))

(define-public crate-aead-0.1.2 (c (n "aead") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.12") (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0yba17wk8prf2lf35433j3f04aknqyvw8mjxzwwjpm39byx9m2qq") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-aead-0.2.0 (c (n "aead") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.12") (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1r3ijikx9h117q0xgkc56yb0501kifjr3gsfp5bvnrz7asdipw2c") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aead-0.3.0 (c (n "aead") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1fkipbsrs90wxnmfalg7wy3ivdll20cf9jal779y3mh0vp7g5ri0") (f (quote (("std" "alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-aead-0.3.1 (c (n "aead") (v "0.3.1") (d (list (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0ysnm6vwkyim61x6sc82nv2cxdc7xpmiddfspjm5dwma286vlwin") (f (quote (("std" "alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-aead-0.3.2 (c (n "aead") (v "0.3.2") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0c8388alvivcj4qkxgh4s4l6fbczn3p8wc0pnar6crlfvcdmvjbz") (f (quote (("std" "alloc") ("dev" "blobby") ("default" "alloc") ("alloc"))))))

(define-public crate-aead-0.4.0-pre (c (n "aead") (v "0.4.0-pre") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0mpjvj7l7bwlp61qkp75xmcrfavmzyirix1ph29nxghdm5hmwwkl") (f (quote (("stream") ("std" "alloc") ("dev" "blobby") ("default" "alloc") ("alloc"))))))

(define-public crate-aead-0.4.0 (c (n "aead") (v "0.4.0") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.6") (o #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)))) (h "06m1mw1hrckgwgm7mh28gj1dqkyj783clalgfxi9fywkzpnfrsla") (f (quote (("stream") ("std" "alloc") ("dev" "blobby") ("alloc")))) (y #t)))

(define-public crate-aead-0.4.1 (c (n "aead") (v "0.4.1") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.7") (o #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)))) (h "08anckdjcr9zchvdrspkw5agnc4d3rh19rd37z8hmz2l5wrk6awj") (f (quote (("stream") ("std" "alloc") ("dev" "blobby") ("alloc"))))))

(define-public crate-aead-0.4.2 (c (n "aead") (v "0.4.2") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.7") (o #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0w1zjxb0bj6g6mfb8c7qyd81hjypyc3bq5a4ymv9f8y8l257jgkf") (f (quote (("stream") ("std" "alloc" "rand_core/std") ("dev" "blobby") ("alloc"))))))

(define-public crate-aead-0.4.3 (c (n "aead") (v "0.4.3") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.7") (o #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0xw8kp9j1whfdxhgmr2qf9xgslkg52zh6gzmhsh13y9w3s73nq8b") (f (quote (("stream") ("std" "alloc" "rand_core/std") ("dev" "blobby") ("alloc"))))))

(define-public crate-aead-0.5.0-pre.1 (c (n "aead") (v "0.5.0-pre.1") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (k 0)) (d (n "crypto-common") (r "^0.1.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.7") (o #t) (k 0)))) (h "05srcfi6c861jvl4smxm58rrgn0fw37kbfx8x0fm32pg74hf4jb8") (f (quote (("stream") ("std" "alloc" "crypto-common/std") ("rand_core" "crypto-common/rand_core") ("getrandom" "crypto-common/getrandom" "rand_core") ("dev" "blobby") ("default" "rand_core") ("alloc"))))))

(define-public crate-aead-0.5.0-pre.2 (c (n "aead") (v "0.5.0-pre.2") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (k 0)) (d (n "crypto-common") (r "^0.1.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.7") (o #t) (k 0)))) (h "181v2lyz0niw0390k6n7xrb922rk8yzlc76w2mvnp5psm6pqbn7b") (f (quote (("stream") ("std" "alloc" "crypto-common/std") ("rand_core" "crypto-common/rand_core") ("getrandom" "crypto-common/getrandom" "rand_core") ("dev" "blobby") ("default" "rand_core") ("alloc")))) (r "1.56")))

(define-public crate-aead-0.5.0 (c (n "aead") (v "0.5.0") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (k 0)) (d (n "crypto-common") (r "^0.1.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.7") (o #t) (k 0)))) (h "1gsa4lb3jvsyh42xdzajq2pn3b1kg8ixvz4shyfvgdjr22kww1mf") (f (quote (("stream") ("std" "alloc" "crypto-common/std") ("rand_core" "crypto-common/rand_core") ("getrandom" "crypto-common/getrandom" "rand_core") ("dev" "blobby") ("default" "rand_core") ("alloc")))) (y #t) (r "1.56")))

(define-public crate-aead-0.5.1 (c (n "aead") (v "0.5.1") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (k 0)) (d (n "crypto-common") (r "^0.1.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.7") (o #t) (k 0)))) (h "1j6pmc8pk4ha64bj9l6xzbhd85s2y1dblna2zsq83h0zy6w2w6aw") (f (quote (("stream") ("std" "alloc" "crypto-common/std") ("rand_core" "crypto-common/rand_core") ("getrandom" "crypto-common/getrandom" "rand_core") ("dev" "blobby") ("default" "rand_core") ("alloc")))) (r "1.56")))

(define-public crate-aead-0.5.2 (c (n "aead") (v "0.5.2") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (k 0)) (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (k 0)) (d (n "crypto-common") (r "^0.1.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.7") (o #t) (k 0)))) (h "1c32aviraqag7926xcb9sybdm36v5vh9gnxpn4pxdwjc50zl28ni") (f (quote (("stream") ("std" "alloc" "crypto-common/std") ("rand_core" "crypto-common/rand_core") ("getrandom" "crypto-common/getrandom" "rand_core") ("dev" "blobby") ("default" "rand_core") ("alloc")))) (r "1.56")))

(define-public crate-aead-0.6.0-pre.0 (c (n "aead") (v "0.6.0-pre.0") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (k 0)) (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (k 0)) (d (n "crypto-common") (r "=0.2.0-pre.5") (d #t) (k 0)) (d (n "heapless") (r "^0.8") (o #t) (k 0)))) (h "1kp0mrriifaxq3psqd7z3nfrdxyxyzz21vjpjgv949kxarkkzr9x") (f (quote (("stream") ("std" "alloc" "crypto-common/std") ("rand_core" "crypto-common/rand_core") ("getrandom" "crypto-common/getrandom") ("dev" "blobby") ("default" "rand_core") ("alloc")))) (r "1.65")))

