(define-module (crates-io ae ad aead-gcm-stream) #:use-module (crates-io))

(define-public crate-aead-gcm-stream-0.1.0 (c (n "aead-gcm-stream") (v "0.1.0") (d (list (d (n "aead") (r "^0.5") (k 0)) (d (n "aead") (r "^0.5") (f (quote ("dev"))) (k 2)) (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "ctr") (r "^0.9") (d #t) (k 0)) (d (n "ghash") (r "^0.5") (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0xw1nh0jm2blryjfrvcr0gk1w5jikdffwxn85j2f4gfm4ssyq2aa")))

(define-public crate-aead-gcm-stream-0.2.0 (c (n "aead-gcm-stream") (v "0.2.0") (d (list (d (n "aead") (r "^0.5") (k 0)) (d (n "aead") (r "^0.5") (f (quote ("dev"))) (k 2)) (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "ctr") (r "^0.9") (d #t) (k 0)) (d (n "ghash") (r "^0.5") (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0h7cpdr0pqdfwysyhv6d8azyhnnmcc220phkm72hrmwpilsxr83g")))

