(define-module (crates-io ae mo aemo-rs) #:use-module (crates-io))

(define-public crate-aemo-rs-0.1.0 (c (n "aemo-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "scraper") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0k8wq0faccxbbq1bq46zrxccrcs46mdchdidvd0byqisip8q9z7r")))

