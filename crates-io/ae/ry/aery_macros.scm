(define-module (crates-io ae ry aery_macros) #:use-module (crates-io))

(define-public crate-aery_macros-0.1.0-dev (c (n "aery_macros") (v "0.1.0-dev") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08v3imykafrg502hpxkrzivdy4z6dbvm66yiwfi09kdaqsil3mg4")))

(define-public crate-aery_macros-0.2.0-dev (c (n "aery_macros") (v "0.2.0-dev") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08dsjmfi75k92qy08hic42s3ipzy4gdydxjpgglzfvhmp2la2bp8")))

(define-public crate-aery_macros-0.3.0-dev (c (n "aery_macros") (v "0.3.0-dev") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0fvnnialmbdyr3alslx0laz1399vnrmmbmfm301v137s6pag4jsm")))

