(define-module (crates-io ae ry aery) #:use-module (crates-io))

(define-public crate-aery-0.1.0 (c (n "aery") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)))) (h "1v5s4vl88j1pkm0rgri9ds8c5alnxfiaqpa01f2fcm46vl6axnfl")))

(define-public crate-aery-0.2.0 (c (n "aery") (v "0.2.0") (d (list (d (n "aery_macros") (r "^0.1.0-dev") (d #t) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)))) (h "00rrdlq06jxlalcff55vnp44bv8wd9gqvcx6hpwy2bbvq88jgrsc")))

(define-public crate-aery-0.3.0 (c (n "aery") (v "0.3.0") (d (list (d (n "aery_macros") (r "^0.2.0-dev") (d #t) (k 0)) (d (n "aquamarine") (r "^0.3.2") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)))) (h "1p7iiqc6141xpfv1nm4yc7hfd6zcgvfinb7471fdj98vyrn5snp4")))

(define-public crate-aery-0.3.1 (c (n "aery") (v "0.3.1") (d (list (d (n "aery_macros") (r "^0.2.0-dev") (d #t) (k 0)) (d (n "aquamarine") (r "^0.3.2") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)))) (h "1qg9grsxx7z09h1ddf7z2k110hizi1398y2yrfvbp5zxm87zvaip")))

(define-public crate-aery-0.4.0 (c (n "aery") (v "0.4.0") (d (list (d (n "aery_macros") (r "^0.3.0-dev") (d #t) (k 0)) (d (n "aquamarine") (r "^0.3.2") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)))) (h "1h292xrksvac3vn1gyf274zmwd3an9wzl4w18aadng7zfzavihsp") (f (quote (("default" "aery") ("aery"))))))

(define-public crate-aery-0.5.0 (c (n "aery") (v "0.5.0") (d (list (d (n "aery_macros") (r "^0.3.0-dev") (d #t) (k 0)) (d (n "aquamarine") (r "^0.3.2") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)))) (h "1hm63nx4adqn5033xkvllz83qbs0gn0z1yga93pkc544gnnrkh99") (f (quote (("default" "aery") ("aery"))))))

(define-public crate-aery-0.5.1 (c (n "aery") (v "0.5.1") (d (list (d (n "aery_macros") (r "^0.3.0-dev") (d #t) (k 0)) (d (n "aquamarine") (r "^0.3.2") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.12") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_log") (r "^0.12") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)))) (h "155gh5xc8ad32cmjkwr40dd06a49y6vznbbid3abbifm2iidqbs7") (f (quote (("default" "aery") ("aery"))))))

(define-public crate-aery-0.5.2 (c (n "aery") (v "0.5.2") (d (list (d (n "aery_macros") (r "^0.3.0-dev") (d #t) (k 0)) (d (n "aquamarine") (r "^0.3.2") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.12") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_log") (r "^0.12") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12") (d #t) (k 0)) (d (n "bevy_vector_shapes") (r "^0.6") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)))) (h "1gp7xq9g73w4vxs63c3f98d47cx7sysy1pzi1191c20n92w9h5dc") (f (quote (("default" "aery") ("aery"))))))

(define-public crate-aery-0.6.0 (c (n "aery") (v "0.6.0") (d (list (d (n "aery_macros") (r "^0.3.0-dev") (d #t) (k 0)) (d (n "aquamarine") (r "^0.3.2") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_app") (r "^0.13") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.13") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_log") (r "^0.13") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.13") (d #t) (k 0)) (d (n "bevy_vector_shapes") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)))) (h "1lfcz20qafkff31yg0j1g1gq3r309f20lppvvgpy2z2xwvrxfn80") (f (quote (("default" "aery") ("aery"))))))

