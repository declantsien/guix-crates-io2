(define-module (crates-io ae t_ aet_authentication) #:use-module (crates-io))

(define-public crate-aet_authentication-0.1.0 (c (n "aet_authentication") (v "0.1.0") (h "1ffps683hq4b22vz1s5qgygrap5hjavi5ss8b2jkfahnf16y8kc1")))

(define-public crate-aet_authentication-0.1.1 (c (n "aet_authentication") (v "0.1.1") (h "0x4pa4skvmj2b38caxia5il4qi7k0y1ps03p2si0cs4cdzpbqh7i")))

