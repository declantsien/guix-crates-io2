(define-module (crates-io ae t_ aet_file_settings) #:use-module (crates-io))

(define-public crate-aet_file_settings-0.1.1 (c (n "aet_file_settings") (v "0.1.1") (d (list (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "0yx6q7a48ni03gynsnvwfqi5jbrh890ydqk3iphzhssivpanfk9k")))

(define-public crate-aet_file_settings-0.1.2 (c (n "aet_file_settings") (v "0.1.2") (d (list (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "00rq5kp5yz27skpr44ln40b2mbv49lq8691gh8y67r0vnnfjxl4y")))

(define-public crate-aet_file_settings-0.1.3 (c (n "aet_file_settings") (v "0.1.3") (d (list (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "1blm9iyb0xcdppby5bf6izj35bjf3k3aak61cd89h8byd9qqb140")))

(define-public crate-aet_file_settings-0.1.4 (c (n "aet_file_settings") (v "0.1.4") (d (list (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "12bdavspj5a01ijf4h9r5gzgqmyh6z061ni2ziwc7q3m9x1pby94")))

