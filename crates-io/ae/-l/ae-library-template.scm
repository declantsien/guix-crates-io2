(define-module (crates-io ae -l ae-library-template) #:use-module (crates-io))

(define-public crate-ae-library-template-0.1.0 (c (n "ae-library-template") (v "0.1.0") (h "1daqbpxbfm4wm5xh6i3hlgk9dyz866vyvm1bwzm5wgv90gcl96fz")))

(define-public crate-ae-library-template-0.1.1 (c (n "ae-library-template") (v "0.1.1") (h "1vmb1k2pdjmbcl4nx5xl57mdwi4fp6pln2msa8mpdmm4gjjic953")))

