(define-module (crates-io ae ro aeronet_wt_core_derive) #:use-module (crates-io))

(define-public crate-aeronet_wt_core_derive-0.2.0 (c (n "aeronet_wt_core_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "07151n6k0mb9h9crx87373lky6kqxsqgkpb96y73gd9yndnlxkgd")))

(define-public crate-aeronet_wt_core_derive-0.3.0 (c (n "aeronet_wt_core_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "016q9zjw8jsnm95zf8pclhg3rd68hivrx40phai2vgzb6fzbxpcc")))

