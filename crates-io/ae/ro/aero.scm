(define-module (crates-io ae ro aero) #:use-module (crates-io))

(define-public crate-aero-0.1.0 (c (n "aero") (v "0.1.0") (h "1ab82vifbv948x6cfp111j22a4nhirdwkincj05jmf8iia9a9qqa")))

(define-public crate-aero-0.1.1 (c (n "aero") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14qi6xqp3w49na04zd11sh7kkcj4fdkmvr70xyh9pwk9466sjf9b")))

(define-public crate-aero-0.1.3 (c (n "aero") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1snbw8kfddmvnh2hgxc3n3ywv60pfw110lgfczx1i065qhp9wc0g")))

(define-public crate-aero-0.1.4 (c (n "aero") (v "0.1.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "049akk9hws70sipmszvq6fq361sr28133yr1qli9bkiiizmxd21d")))

