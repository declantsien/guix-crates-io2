(define-module (crates-io ae ro aeronet_wt_core) #:use-module (crates-io))

(define-public crate-aeronet_wt_core-0.2.0 (c (n "aeronet_wt_core") (v "0.2.0") (d (list (d (n "aeronet") (r "^0.2.0") (d #t) (k 0)) (d (n "aeronet_wt_core_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "00zjzf470dyz715v6qx2amw2waqf654x385d8sb8wn0rpbw8038d")))

(define-public crate-aeronet_wt_core-0.3.0 (c (n "aeronet_wt_core") (v "0.3.0") (d (list (d (n "aeronet") (r "^0.3.0") (d #t) (k 0)) (d (n "aeronet_wt_core_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "088lrybb8n791qrh64cfky5fnix3mbcpxqxga3b5mii6nlg1s39r")))

