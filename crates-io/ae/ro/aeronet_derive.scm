(define-module (crates-io ae ro aeronet_derive) #:use-module (crates-io))

(define-public crate-aeronet_derive-0.4.0 (c (n "aeronet_derive") (v "0.4.0") (d (list (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1smf7fpmzi62gyjblq3d7r6vk84hld1lkdn43cyly50i434qhib1") (r "1.74.0")))

