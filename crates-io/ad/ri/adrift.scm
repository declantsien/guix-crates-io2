(define-module (crates-io ad ri adrift) #:use-module (crates-io))

(define-public crate-adrift-0.0.1 (c (n "adrift") (v "0.0.1") (h "1nzfj96fjrw06ijjcx729am89hczha61amg79ac8g264g4y3glz8")))

(define-public crate-adrift-0.0.2 (c (n "adrift") (v "0.0.2") (d (list (d (n "adrift_core") (r "^0.0.2") (d #t) (k 0)) (d (n "adrift_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "02m98bi8x3j2c78hbjs7bwc5f1vk8spx68fjsgbxm6sgxhklq995")))

(define-public crate-adrift-0.0.3 (c (n "adrift") (v "0.0.3") (d (list (d (n "adrift_core") (r "^0.0.2") (d #t) (k 0)) (d (n "adrift_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ykr8jshkk5xnjj7vqlvzd1sy84l27icgvwjzvd987mrkr91np5w")))

(define-public crate-adrift-0.0.4 (c (n "adrift") (v "0.0.4") (d (list (d (n "adrift_core") (r "^0.0.4") (d #t) (k 0)) (d (n "adrift_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "05w9gjkdwdb18z8bv5inbifrng5vvvq0p7i24ax03b6qqxkrw6kw")))

(define-public crate-adrift-0.0.5 (c (n "adrift") (v "0.0.5") (d (list (d (n "adrift_core") (r "^0.0.5") (d #t) (k 0)) (d (n "adrift_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xhwbsg9n9m35bdpwan40f9gnadxwps73w01ivg5xl4q9mmdsba4")))

(define-public crate-adrift-0.0.6 (c (n "adrift") (v "0.0.6") (d (list (d (n "adrift_core") (r "^0.0.6") (d #t) (k 0)) (d (n "adrift_macros") (r "^0.0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xyaf0lkflzf5i5zlh5f0q9q3cwscmm6vgvaxf3bmwqfn0r9gic0")))

(define-public crate-adrift-0.0.7 (c (n "adrift") (v "0.0.7") (d (list (d (n "adrift_core") (r "^0.0.7") (d #t) (k 0)) (d (n "adrift_macros") (r "^0.0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1j8ql8fh9l57k4gr4cs0h56a6f0dl6w68dhyg2ksj6z03ngssbi9")))

(define-public crate-adrift-0.0.8 (c (n "adrift") (v "0.0.8") (d (list (d (n "adrift_core") (r "^0.0.8") (d #t) (k 0)) (d (n "adrift_macros") (r "^0.0.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yanjracdzzzz8v00prsbc8mqys153r3mxfjf1insw0mwhirsn73")))

(define-public crate-adrift-0.0.9 (c (n "adrift") (v "0.0.9") (d (list (d (n "adrift_core") (r "^0.0.9") (d #t) (k 0)) (d (n "adrift_macros") (r "^0.0.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jbbid6y773vqiv1i29xrfbg54lyiy9ycv58c787qxwmiwx80abq")))

(define-public crate-adrift-0.0.10 (c (n "adrift") (v "0.0.10") (d (list (d (n "adrift_core") (r "^0.0.10") (d #t) (k 0)) (d (n "adrift_macros") (r "^0.0.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zaq2i14h9q1yjnp2wgkgdqrdl8f5ymzv215m2nhmd5c7r0da59w")))

(define-public crate-adrift-0.0.11 (c (n "adrift") (v "0.0.11") (d (list (d (n "adrift_core") (r "^0.0.11") (d #t) (k 0)) (d (n "adrift_macros") (r "^0.0.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b0dl2brylza415s768pssb5i8v21qk9c6w26xna2qfq2140avqj")))

