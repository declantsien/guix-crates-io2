(define-module (crates-io ad s7 ads7924) #:use-module (crates-io))

(define-public crate-ads7924-0.1.0 (c (n "ads7924") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "13hmdvhfgy2znyp7w1xkfxg3cac0h0zk0hcfbxghajpgrmbljpqq")))

