(define-module (crates-io ad s7 ads7828) #:use-module (crates-io))

(define-public crate-ads7828-0.1.0 (c (n "ads7828") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "058ca0igpaidpkzqwhlgakzknw0m2gdphm7j980fv910l9axhxrs")))

(define-public crate-ads7828-0.1.1 (c (n "ads7828") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0fzm2yyfzv29hc58jmxr29yrgvkb23dp15ff95kdr4y3b9fmkadv")))

