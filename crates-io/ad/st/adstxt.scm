(define-module (crates-io ad st adstxt) #:use-module (crates-io))

(define-public crate-adstxt-0.1.0 (c (n "adstxt") (v "0.1.0") (d (list (d (n "hyper") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "macros"))) (o #t) (d #t) (k 0)))) (h "1l1f7052lbqspifzfkpkaykka1lmniv3367l6yh632368jnx8scx") (f (quote (("parser") ("default" "parser") ("crawler" "log" "hyper" "hyper-tls" "tokio"))))))

(define-public crate-adstxt-0.1.1 (c (n "adstxt") (v "0.1.1") (h "19c8kj6hrd1x9f2mz02wqj942jk67983p03f252lyx7r8kkl58xc") (f (quote (("default"))))))

