(define-module (crates-io ad ob adobe-cmap-parser) #:use-module (crates-io))

(define-public crate-adobe-cmap-parser-0.1.0 (c (n "adobe-cmap-parser") (v "0.1.0") (d (list (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "122fagywilyf7vj0jaclglyvipajzrcrpcllc72a0ca81cn9kdc3")))

(define-public crate-adobe-cmap-parser-0.1.1 (c (n "adobe-cmap-parser") (v "0.1.1") (d (list (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "17pm6vzigshqa8sys33n4vawfjrjk8536m4yl4kq1pjg2z3br6ja")))

(define-public crate-adobe-cmap-parser-0.2.0 (c (n "adobe-cmap-parser") (v "0.2.0") (d (list (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "03cn8xhjxahi1p45vsqfz11axmb0dygl1xz0h74x4cg2ifh4b6x9")))

(define-public crate-adobe-cmap-parser-0.3.0 (c (n "adobe-cmap-parser") (v "0.3.0") (d (list (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "0lxzlzs8j6kzl2xzss7l8di0hmqagcp5g7w0aggkc5wwzysk0dwf")))

(define-public crate-adobe-cmap-parser-0.3.1 (c (n "adobe-cmap-parser") (v "0.3.1") (d (list (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "06n9g9p2jzarrxn0629bf4275hjqh2wpbq8c8s62qcfd75ivnq30")))

(define-public crate-adobe-cmap-parser-0.3.2 (c (n "adobe-cmap-parser") (v "0.3.2") (d (list (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "11z9swpr15m1zhgcpw4zfc19z5sx7bvzk5jby2bz7vqqd4izlwph")))

(define-public crate-adobe-cmap-parser-0.3.3 (c (n "adobe-cmap-parser") (v "0.3.3") (d (list (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "1nlxh6j3cbklimmnqg8q7y4d90yyr5msklygasbfrj38dl3gban3")))

(define-public crate-adobe-cmap-parser-0.3.4 (c (n "adobe-cmap-parser") (v "0.3.4") (d (list (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "097bin6kslbnvjmh9kab4inqkf668p65l3in7w7c3d5xsx9igwxs")))

(define-public crate-adobe-cmap-parser-0.3.5 (c (n "adobe-cmap-parser") (v "0.3.5") (d (list (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "1g54570yb12jlhw1whyy9z5mw8pwfbbmpw125h8bi2jh2zbajgbx")))

(define-public crate-adobe-cmap-parser-0.4.0 (c (n "adobe-cmap-parser") (v "0.4.0") (d (list (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "1mfq1f1cryayhnhn289303dnl9fphxcr5v2xc2hp1p3x61x966i6")))

