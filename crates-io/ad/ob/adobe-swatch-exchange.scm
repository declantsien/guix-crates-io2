(define-module (crates-io ad ob adobe-swatch-exchange) #:use-module (crates-io))

(define-public crate-adobe-swatch-exchange-1.0.0 (c (n "adobe-swatch-exchange") (v "1.0.0") (h "062idb7z0w90x42gap28ik9kqjmixyl75w2rf8l1h5k26gpgjfli") (r "1.65")))

(define-public crate-adobe-swatch-exchange-2.0.0 (c (n "adobe-swatch-exchange") (v "2.0.0") (h "18rjpic0rxrjlfqmwx00aq3byg220plhlk48c7k683m579b8dld1") (r "1.65")))

(define-public crate-adobe-swatch-exchange-2.0.1 (c (n "adobe-swatch-exchange") (v "2.0.1") (h "0s3y09qafg3szr6d9ss81ysj6jygk6liy4iaq03gy8y96pffik7p") (r "1.65")))

