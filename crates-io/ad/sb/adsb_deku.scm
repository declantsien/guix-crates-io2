(define-module (crates-io ad sb adsb_deku) #:use-module (crates-io))

(define-public crate-adsb_deku-0.1.0 (c (n "adsb_deku") (v "0.1.0") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "deku") (r "^0.12") (d #t) (k 0)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)))) (h "0xqsr36cgccpj112yy4shhv8rb7f24p1lv971ip2z7knavyvnwif")))

(define-public crate-adsb_deku-0.1.1 (c (n "adsb_deku") (v "0.1.1") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "deku") (r "^0.12") (d #t) (k 0)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)))) (h "1pf2x40x1844dj49k9kjj889xwn5rrydmga3wdj3x71h9vwsdcn2")))

(define-public crate-adsb_deku-0.2.0 (c (n "adsb_deku") (v "0.2.0") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "deku") (r "^0.12") (d #t) (k 0)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)))) (h "1xr4ixlk608jhn37ykxfha3p8wdvfdj1gh8vy9hcslb08xbcblbh")))

(define-public crate-adsb_deku-0.3.0 (c (n "adsb_deku") (v "0.3.0") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "deku") (r "^0.12") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)))) (h "0kl6zi8s8xs3vp5x2059i4i3k4pllvx5vwwrqmqyzb1zvwpfdjzh")))

(define-public crate-adsb_deku-0.4.0 (c (n "adsb_deku") (v "0.4.0") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "deku") (r "^0.12") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)))) (h "0c73xmm3z7p3lzll9drzq3yv032zv7s7r7v4q33sz7vf7la2psid")))

(define-public crate-adsb_deku-0.5.0 (c (n "adsb_deku") (v "0.5.0") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "deku") (r "^0.12") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)))) (h "0703b11yx84ac77x6i2zmv2mr0912lar677ky2869bb241pvpm99") (r "1.58.1")))

(define-public crate-adsb_deku-0.5.1 (c (n "adsb_deku") (v "0.5.1") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "deku") (r "^0.12") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)))) (h "1f2lnmaxqqdlclfwkpg0n1jcpglbxvgm1z8k1i0fvlihkjnlawnd") (r "1.58.1")))

(define-public crate-adsb_deku-0.6.0 (c (n "adsb_deku") (v "0.6.0") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "deku") (r "^0.13") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)))) (h "0i1xr9n46rgyzm2lx2qx34zmbhm5l2q0lhfg60qjlhyc814fgh1q") (f (quote (("std" "deku/std" "alloc") ("default" "std") ("alloc" "deku/alloc")))) (r "1.59.0")))

(define-public crate-adsb_deku-0.6.1 (c (n "adsb_deku") (v "0.6.1") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "deku") (r "^0.13") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)))) (h "10w521i27d0blg3lzvrk9z13268shjvzwnsk6x9ndjw5kmg8w59a") (f (quote (("std" "deku/std" "alloc") ("default" "std") ("alloc" "deku/alloc")))) (r "1.59.0")))

(define-public crate-adsb_deku-0.6.2 (c (n "adsb_deku") (v "0.6.2") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "deku") (r "^0.15") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)))) (h "01wd4bv20hc3928whdma1g6p7wzri6zrx25abmkmc7r2vacpb0rk") (f (quote (("std" "deku/std" "alloc") ("default" "std") ("alloc" "deku/alloc")))) (r "1.64.0")))

(define-public crate-adsb_deku-0.6.3 (c (n "adsb_deku") (v "0.6.3") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "deku") (r "^0.15") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1f4l4brmw49887sw057g8smyqyigl6i291m88lzz4bl5gyi2kiib") (f (quote (("std" "deku/std" "alloc") ("default" "std") ("alloc" "deku/alloc")))) (r "1.64.0")))

(define-public crate-adsb_deku-0.7.0 (c (n "adsb_deku") (v "0.7.0") (d (list (d (n "assert_hex") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "deku") (r "^0.16") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hexlit") (r "^0.5") (d #t) (k 2)) (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0i85gij02mk86nv6v8y91s285wrgflh26qxzjmnpv6g21spir55i") (f (quote (("std" "deku/std" "alloc") ("default" "std") ("alloc" "deku/alloc")))) (r "1.70.0")))

