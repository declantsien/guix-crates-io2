(define-module (crates-io ad sb adsbx_browser) #:use-module (crates-io))

(define-public crate-adsbx_browser-0.1.0 (c (n "adsbx_browser") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0n7bx66jk064pwyrn66icrxsqvhcvm6pw5gjg3flfdcj5dl6h1kd")))

