(define-module (crates-io ad sb adsb) #:use-module (crates-io))

(define-public crate-adsb-0.1.0 (c (n "adsb") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2.2") (d #t) (k 0)))) (h "1swvgv5mm38fzjpbdzp9hj6g4c5vnvibcll1asdcwilcnyrnpky0")))

(define-public crate-adsb-0.2.0 (c (n "adsb") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 2)) (d (n "nom") (r "^4.2.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 2)))) (h "09m8i8k1v991zifa0d6gvh8qh8k1333mnvylk4cvmlj077l5s2yn")))

(define-public crate-adsb-0.2.1 (c (n "adsb") (v "0.2.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 2)))) (h "0hv2zzisrwb94n2ggjm91hi5afwaj5cf71xgyn2i0501dbp15g8q")))

(define-public crate-adsb-0.2.2 (c (n "adsb") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 2)))) (h "19lsdmnz6l4fk5xrr5z1056f4hl9c24cm0h4mrbixi74ivg9bxc7")))

(define-public crate-adsb-0.2.3 (c (n "adsb") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 2)))) (h "0casizgfinrnvw28rdz0d9cfbk1vw6mb6z4avv1yckiyf5sc7z3k")))

(define-public crate-adsb-0.2.4 (c (n "adsb") (v "0.2.4") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 2)))) (h "0f1djz8ii5d5ypgkyvska7v27faipp0m2q2xjp34w7mlc735z55z")))

(define-public crate-adsb-0.3.0 (c (n "adsb") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 2)))) (h "01yczk6d3vgxbhqrchcc62d8xz58vn7zijcx4saijgzn67bfqicj")))

