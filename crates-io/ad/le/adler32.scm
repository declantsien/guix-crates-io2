(define-module (crates-io ad le adler32) #:use-module (crates-io))

(define-public crate-adler32-0.1.0 (c (n "adler32") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "0mvvd0z99rq8rzmv6776wwd96fwxgn6qb512s7asmi7n2qf6z1pv")))

(define-public crate-adler32-0.2.0 (c (n "adler32") (v "0.2.0") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "1y9ay6kpbz05qc7j98i7drhcnj954hm5djkflaym9myvyrcala79")))

(define-public crate-adler32-0.3.0 (c (n "adler32") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1fnm0ghbjwky6nq0iy24a90rrnlwhcjsf04l7alp000hnhz07gjp")))

(define-public crate-adler32-1.0.0 (c (n "adler32") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1kdraxqlvda0ha06ygg36wa98i28xa7cd8pgph2wxnq87bhkzwrz")))

(define-public crate-adler32-1.0.1 (c (n "adler32") (v "1.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "12wsw6s027c0ykv1q74jr4dbj1nrx8krqpnlg6n2qrj26sgg54yf")))

(define-public crate-adler32-1.0.2 (c (n "adler32") (v "1.0.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0icgbzr0vwckdvj06qly5jc71c7vp4sjvxy9mnz74z2qz2d0pgbc")))

(define-public crate-adler32-1.0.3 (c (n "adler32") (v "1.0.3") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0p7fxlnks9l7p7rwfqi7aqgnk2bps5zc0rjiw00mdw19nnbjjlky")))

(define-public crate-adler32-1.0.4 (c (n "adler32") (v "1.0.4") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1hnan4fgmnidgn2k84hh2i67c3wp2c5iwd5hs61yi7gwwx1p6bjx")))

(define-public crate-adler32-1.1.0 (c (n "adler32") (v "1.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0bgks405vz823bphgwhj4l9h6vpfh900s0phfk4qqijyh9xhfysn") (f (quote (("std") ("rustc-dep-of-std" "core" "compiler_builtins") ("default" "std"))))))

(define-public crate-adler32-1.2.0 (c (n "adler32") (v "1.2.0") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 2)) (d (n "humansize") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0d7jq7jsjyhsgbhnfq5fvrlh9j0i9g1fqrl2735ibv5f75yjgqda") (f (quote (("std") ("rustc-dep-of-std" "core" "compiler_builtins") ("default" "std"))))))

