(define-module (crates-io ad le adler32_checksum_rs) #:use-module (crates-io))

(define-public crate-adler32_checksum_rs-0.1.0 (c (n "adler32_checksum_rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "14j6238hmk2rfxmif9y6yfv0nbz1q1l4rmz552v8z6zg9x1as3s2")))

