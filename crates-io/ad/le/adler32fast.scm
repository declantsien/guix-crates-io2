(define-module (crates-io ad le adler32fast) #:use-module (crates-io))

(define-public crate-adler32fast-1.0.0 (c (n "adler32fast") (v "1.0.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.0") (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0kh1qjs2ha3a9qrzs204q8qlflgfzmpbn755ckm5b5wp5q4jpf1z") (f (quote (("std") ("default" "std"))))))

(define-public crate-adler32fast-1.0.1 (c (n "adler32fast") (v "1.0.1") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.0") (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1zvqnjaydwah13ldfx96mcnzv2bkn2x0m01ws7scp6jwbkp6y2l1") (f (quote (("std") ("default" "std"))))))

(define-public crate-adler32fast-1.0.2 (c (n "adler32fast") (v "1.0.2") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.0") (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1vl05ba9chyg32hr7wifyg3xynj4xl5xzln8kibhkxkh0gvam2lh") (f (quote (("std") ("default" "std"))))))

(define-public crate-adler32fast-1.0.3 (c (n "adler32fast") (v "1.0.3") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.0") (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "011rrl1fqsgcsdh83d99dr0v5ninwz0dglcrnkphva00zcd9x4ym") (f (quote (("std") ("default" "std"))))))

(define-public crate-adler32fast-1.0.4 (c (n "adler32fast") (v "1.0.4") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.0") (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0zqpxpn4c6qy3d598xv28s38x4fpzv7dl0szcb8j068bl0ki7x5x") (f (quote (("std") ("default" "std"))))))

(define-public crate-adler32fast-1.1.0 (c (n "adler32fast") (v "1.1.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.0") (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1jcvvz88syxgbwji1iyk7g46dlr4wrl23izzyssdry08k8r8i8xc") (f (quote (("std") ("default" "std"))))))

