(define-module (crates-io ad le adler) #:use-module (crates-io))

(define-public crate-adler-0.1.0 (c (n "adler") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)))) (h "157b3zqbgsga05xjfdnixncg9p5a5wmbp1bpqjkz6ckg19yk1n6w")))

(define-public crate-adler-0.2.0 (c (n "adler") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)))) (h "00y5dgzqkgsazr8ggsfl9z1j75jrq31spb32vqj3zj6c59g4aiwi") (f (quote (("std") ("default" "std"))))))

(define-public crate-adler-0.2.1 (c (n "adler") (v "0.2.1") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)))) (h "0g85x58iq3a544kdksq130cz9r3b0xq3zppq35kj49gmysk018ba") (f (quote (("std") ("rustc-dep-of-std" "core" "compiler_builtins") ("default" "std"))))))

(define-public crate-adler-0.2.2 (c (n "adler") (v "0.2.2") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)))) (h "044bl7flj4sgn7dq6phqrvlbbby42ygsyc23ph9g4scm0vfskjfc") (f (quote (("std") ("rustc-dep-of-std" "core" "compiler_builtins") ("default" "std"))))))

(define-public crate-adler-0.2.3 (c (n "adler") (v "0.2.3") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)))) (h "0zpdsrfq5bd34941gmrlamnzjfbsx0x586afb7b0jqhr8g1lwapf") (f (quote (("std") ("rustc-dep-of-std" "core" "compiler_builtins") ("default" "std"))))))

(define-public crate-adler-1.0.0 (c (n "adler") (v "1.0.0") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)))) (h "0dsd94903arx9n5wc4nwpd89kjidm1s4zw5pbgwq1qpq1c9n5qnb") (f (quote (("std") ("rustc-dep-of-std" "core" "compiler_builtins") ("default" "std"))))))

(define-public crate-adler-1.0.1 (c (n "adler") (v "1.0.1") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)))) (h "14shamgp9fb98syji8glqdf717vzzigmrsvjjgxhymdmqz2qkp5y") (f (quote (("std") ("rustc-dep-of-std" "core" "compiler_builtins") ("default" "std"))))))

(define-public crate-adler-1.0.2 (c (n "adler") (v "1.0.2") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)))) (h "1zim79cvzd5yrkzl3nyfx0avijwgk9fqv3yrscdy1cc79ih02qpj") (f (quote (("std") ("rustc-dep-of-std" "core" "compiler_builtins") ("default" "std"))))))

