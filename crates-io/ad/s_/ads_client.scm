(define-module (crates-io ad s_ ads_client) #:use-module (crates-io))

(define-public crate-ads_client-1.0.0 (c (n "ads_client") (v "1.0.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "0ifdcxl9b247slvik6a3bpil2b3q7k7azcvvnf0fa2whamggqdns")))

(define-public crate-ads_client-1.1.0 (c (n "ads_client") (v "1.1.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "0hywcnm4f1vi1ngljv8mbnhc0q5327qh6vy30l77lvb2qsp4kiqn")))

(define-public crate-ads_client-1.2.0 (c (n "ads_client") (v "1.2.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "0xqc7pdfs7arz8kp0v20fwm6ym591311mnwy5ai5pkick5p2awm5")))

(define-public crate-ads_client-1.2.1 (c (n "ads_client") (v "1.2.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "1kr1racxkrg7vk5wm2h90724qmd4lp65a81wad0dbbpzc7d5l6gg")))

(define-public crate-ads_client-1.3.0 (c (n "ads_client") (v "1.3.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "0c2aszk6wmkvkv3lryplirn7cdsva3lk58j0rhviwvgyy5bjy23q")))

(define-public crate-ads_client-1.4.0 (c (n "ads_client") (v "1.4.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "1fc0i5fi10hfng8bbh1556b1f17nfmwz10islpfn2jnpaiyvjpm8")))

(define-public crate-ads_client-1.4.1 (c (n "ads_client") (v "1.4.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 2)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)))) (h "1c277wpayw6bgmw32h74xs76hv9caw4rvggfvyigmq541qbjxymc")))

