(define-module (crates-io ad do addon_distributor) #:use-module (crates-io))

(define-public crate-addon_distributor-0.1.0 (c (n "addon_distributor") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1pmaf30wspcr6cgzd3w0mk2f5j1hnmcnz75v4z2wcsy026h8d5kj")))

