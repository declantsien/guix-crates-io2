(define-module (crates-io ad mo admob-android) #:use-module (crates-io))

(define-public crate-admob-android-0.1.9 (c (n "admob-android") (v "0.1.9") (d (list (d (n "crossbow-android") (r "^0.1.9") (d #t) (k 0)))) (h "0k1z3qigkspy6p938zg819ah3pyrjr7ars1jkiwmw8m8s3gqzgbw")))

(define-public crate-admob-android-0.2.0 (c (n "admob-android") (v "0.2.0") (d (list (d (n "crossbow-android") (r "^0.2.0") (d #t) (k 0)))) (h "0dkqiz4pnyvmjkfjavk3pndyj4sx0qwqdd9qg5g6gkbp4zx0qsqn")))

(define-public crate-admob-android-0.2.1 (c (n "admob-android") (v "0.2.1") (d (list (d (n "crossbow-android") (r "^0.2.1") (d #t) (k 0)))) (h "1aaqjl9klrknljvdc6x9hlzfysch936zawxmwxdv9aflin5cxcv5")))

(define-public crate-admob-android-0.2.2 (c (n "admob-android") (v "0.2.2") (d (list (d (n "crossbow-android") (r "^0.2.2") (d #t) (k 0)))) (h "04m9rjhqbxb1xs6474bp4615cv5xfh4mhfk9r63sp0ldr6biknhg")))

(define-public crate-admob-android-0.2.3 (c (n "admob-android") (v "0.2.3") (d (list (d (n "crossbow-android") (r "^0.2.3") (d #t) (k 0)))) (h "0qwjcz6bbf2f4lfrpw8c7h3qipxx22izgjqnrid7snimldsilsjy")))

