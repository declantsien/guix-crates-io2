(define-module (crates-io ad de adder_v1) #:use-module (crates-io))

(define-public crate-adder_v1-0.1.0 (c (n "adder_v1") (v "0.1.0") (h "15pvk4d0mhnz25fbycmf5l59vikcib21xcfxlkrx0ssgqjanygji")))

(define-public crate-adder_v1-0.1.1 (c (n "adder_v1") (v "0.1.1") (h "1f7cf9ahlvwnm5dpjyy8i1wa13xakzvshg7vr39pkim0vv62i44c")))

