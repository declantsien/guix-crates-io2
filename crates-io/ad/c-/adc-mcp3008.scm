(define-module (crates-io ad c- adc-mcp3008) #:use-module (crates-io))

(define-public crate-adc-mcp3008-0.1.0 (c (n "adc-mcp3008") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)))) (h "1pvkxrkvla18m94ajn275fw93csiinddyj7g00yw7mgqrf1gny1i")))

(define-public crate-adc-mcp3008-0.1.1 (c (n "adc-mcp3008") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "1qylavlcy9qxiqjinqhwcc40zdf19mbglllcjbddfbc8sad4vd8g")))

