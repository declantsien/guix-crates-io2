(define-module (crates-io ad c- adc-interpolator) #:use-module (crates-io))

(define-public crate-adc-interpolator-0.1.0 (c (n "adc-interpolator") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1imr2qplxwf8zssksp33165rf71gdqgl223yan3b9vqj85xv05x0")))

(define-public crate-adc-interpolator-0.2.0 (c (n "adc-interpolator") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "03wnpqkgw57cr5p70na2s6k1wz7p9j37zh5cshyd0a4j1q496nc5")))

