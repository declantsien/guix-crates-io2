(define-module (crates-io ad fl adflib) #:use-module (crates-io))

(define-public crate-adflib-0.0.1 (c (n "adflib") (v "0.0.1") (h "0q1pjiyb4xgc1dw1iqv8ds2rh9r3k0l0g4hacvnf81m5sb9b5n0j")))

(define-public crate-adflib-0.0.2 (c (n "adflib") (v "0.0.2") (h "0g8l3bldw34fz0di7iw12ahsbx4iwix0sx32hl6iycv2dyl33nhp")))

(define-public crate-adflib-0.0.3 (c (n "adflib") (v "0.0.3") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v85ra8cc40i1z3qwnfwvh0vw4sd0wmsrf29yfh70crn4k6gr659")))

