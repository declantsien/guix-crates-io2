(define-module (crates-io ad kf adkfjakdjk) #:use-module (crates-io))

(define-public crate-adkfjakdjk-0.1.0 (c (n "adkfjakdjk") (v "0.1.0") (h "0vm302290fyphyx0iz4gb72dxiaqbiz5ss2k8fqf2izwhr0xq3kw")))

(define-public crate-adkfjakdjk-0.2.0 (c (n "adkfjakdjk") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12sbnqznlz9yxfghaykfhjjwipcmnqqz7rsqj1r42any9h35kcv9")))

(define-public crate-adkfjakdjk-0.2.1 (c (n "adkfjakdjk") (v "0.2.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xrql28l67z2w304x4m5lk8a4njwz5jpn2wiqz720nvjjwd5gp1k")))

(define-public crate-adkfjakdjk-0.3.0 (c (n "adkfjakdjk") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12q7pv9x2888gda4cwbgf7jzqn8864sa8d3q89mzhh1rlll64k7w")))

