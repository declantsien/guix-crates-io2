(define-module (crates-io ad om adom) #:use-module (crates-io))

(define-public crate-adom-0.0.1 (c (n "adom") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.5") (d #t) (k 0)))) (h "15fsdx328whjhggmsq599xja60ig36l19lbin4jsikaxlbx2di7y")))

(define-public crate-adom-0.0.2 (c (n "adom") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.5") (d #t) (k 0)))) (h "1hbggiwznyvpwml12pq92hgzwx38l3brkag3ay3z74xsswf6564v")))

(define-public crate-adom-0.0.3 (c (n "adom") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.5") (d #t) (k 0)))) (h "046cn41fc2p9b2jml40bgykiv46234lvk94v5sfcaxg61qs4gnjr")))

(define-public crate-adom-0.0.4 (c (n "adom") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.5") (d #t) (k 0)))) (h "0xwhnlnx9b9sj0jasx123j2ssf4kjhq5wyxvxrpx9al5bsh3dpmh")))

(define-public crate-adom-0.0.5 (c (n "adom") (v "0.0.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0") (d #t) (k 0)))) (h "0lx9w9rm10s1gahz5q33acaq5r8ray978qj2l5nnkichi6fc606a")))

(define-public crate-adom-0.0.6 (c (n "adom") (v "0.0.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0") (d #t) (k 0)))) (h "054q4nwwla5pfcmpi76marms6zf0gwzp4gwld8hzzywdp57vyvab")))

