(define-module (crates-io ad vr advreader) #:use-module (crates-io))

(define-public crate-advreader-1.2.0 (c (n "advreader") (v "1.2.0") (d (list (d (n "flume") (r "^0.11") (d #t) (k 0)))) (h "1nxnvlp2rr0pq5cys3q265wqh4kqz3z1qkh9vs2yif3qk6qwknz4")))

(define-public crate-advreader-2.0.0 (c (n "advreader") (v "2.0.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "flume") (r "^0.11") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 2)) (d (n "threadpool") (r "^1.8") (d #t) (k 2)))) (h "0s1zhxv9f7mwi6fk38arsksms3nkjn0928rgc00lc69fv0nw55pm")))

(define-public crate-advreader-2.1.0 (c (n "advreader") (v "2.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "flume") (r "^0.11") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 2)) (d (n "threadpool") (r "^1.8") (d #t) (k 2)))) (h "07xapifd62mr85w2bfg8h9igqbab9d14avql3fl0wrpfs2v821b9")))

(define-public crate-advreader-2.1.1 (c (n "advreader") (v "2.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "flume") (r "^0.11") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 2)) (d (n "threadpool") (r "^1.8") (d #t) (k 2)))) (h "1xi0ym4744wlzqn9w6d0fcgnwd2wf9z69ccw8jrh8q2c2ppnkahy")))

(define-public crate-advreader-2.2.0 (c (n "advreader") (v "2.2.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "flume") (r "^0.11") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 2)) (d (n "threadpool") (r "^1.8") (d #t) (k 2)))) (h "0gd471g31ydjh95n80nc32awxqfw9kisiwivnrawz86sw3ifbkdp")))

