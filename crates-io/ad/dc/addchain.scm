(define-module (crates-io ad dc addchain) #:use-module (crates-io))

(define-public crate-addchain-0.0.0 (c (n "addchain") (v "0.0.0") (h "0xy3s5rril5mwy6k5y3pn1y0p2sh7q3qih4y1pjygfs39irlqyrz")))

(define-public crate-addchain-0.1.0 (c (n "addchain") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1728ndjvxwaz86g1ms6nhph6kn3zgjacv8z908h4kfx7jcn24xqi")))

(define-public crate-addchain-0.2.0 (c (n "addchain") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0w45hpybsx9gzhlxf6x9451kycg8xwj3x8qzjnk8wqm55926jbiv")))

