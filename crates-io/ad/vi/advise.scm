(define-module (crates-io ad vi advise) #:use-module (crates-io))

(define-public crate-advise-0.1.0 (c (n "advise") (v "0.1.0") (d (list (d (n "anstream") (r "^0.6.11") (d #t) (k 0)) (d (n "anstyle") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0k3752nzs5lcp712sj6dzmyp7qz9jxdw4gxvnmqppjf5s04jgz4q")))

