(define-module (crates-io ad vi advisory-lock) #:use-module (crates-io))

(define-public crate-advisory-lock-0.1.0 (c (n "advisory-lock") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "minwinbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13jl25b4fklc08aa7jnnyaadjzzciamr2gbivnr0si7z4yqs89av")))

(define-public crate-advisory-lock-0.2.0 (c (n "advisory-lock") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "minwinbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0j5jxkmpy4cfsq5a3vfhmh5l7hzxazp2rhwjk4dzs35zw3j59mwr")))

(define-public crate-advisory-lock-0.3.0 (c (n "advisory-lock") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "minwinbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "14wg5p8kvb6iw7pa832v1kd6acw4py66ym697ynzjc7r91yyxjm6")))

