(define-module (crates-io ad dt addtwo_macro) #:use-module (crates-io))

(define-public crate-addtwo_macro-0.1.0 (c (n "addtwo_macro") (v "0.1.0") (h "1n8mayr7yr63j968v1har5isgyyajv72naixyv7c0cinwa6zqjpi")))

(define-public crate-addtwo_macro-0.1.1 (c (n "addtwo_macro") (v "0.1.1") (h "01gcpijq6pqipmg1kn9j4zcwp57yi0si84gvkgyinzck6afmkvjh")))

