(define-module (crates-io ad pc adpcm-xq-sys) #:use-module (crates-io))

(define-public crate-adpcm-xq-sys-0.1.1 (c (n "adpcm-xq-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1pgkgzn05xfs7iw8ic104xdayj7b4lnxq5c2vsx7bss1sz5q74b8") (f (quote (("nds") ("default"))))))

(define-public crate-adpcm-xq-sys-0.1.2 (c (n "adpcm-xq-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "12ic20vh0cqg5y33z30yiqs78vwvz8gwb0r8x1a2kdxfcrabw3l6") (f (quote (("nds") ("default")))) (y #t)))

(define-public crate-adpcm-xq-sys-0.1.3 (c (n "adpcm-xq-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1g55a2i8kncr4mqgd017iahsabywzwqppn1kysn8jwrp7hgdi51b") (f (quote (("nds") ("default"))))))

