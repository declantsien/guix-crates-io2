(define-module (crates-io ad iv adivon) #:use-module (crates-io))

(define-public crate-adivon-0.1.0 (c (n "adivon") (v "0.1.0") (h "0hjgas2rgxdhx4pab3761qmravyc8mq353bzwpnk7q2xw8kglba3")))

(define-public crate-adivon-0.2.0 (c (n "adivon") (v "0.2.0") (h "1bln43wk3hcmhxdbg3akfj54kin21a0jkjc1b51lzs4wd9rpdal1")))

(define-public crate-adivon-0.2.1 (c (n "adivon") (v "0.2.1") (h "17xlyxsb38iyibi4xc62aar245vb33n6mm7b804m3inv7pljwahn")))

(define-public crate-adivon-0.2.2 (c (n "adivon") (v "0.2.2") (h "1rj7fd4sr6v7yn3qqz9fa4fdjr51i4llnrjc0lgf2s0hlidcq6qp")))

(define-public crate-adivon-0.2.3 (c (n "adivon") (v "0.2.3") (h "0hax6j3h1z5zncssys8jgr3cvvla60yb0hnaf3fgbwmjqxxk3cxn")))

(define-public crate-adivon-0.2.4 (c (n "adivon") (v "0.2.4") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "vec_map") (r "*") (d #t) (k 0)))) (h "1iwpr02r8fs3ki8pl1largx75a9k48qzrmg6v5gand2mvg5yvfnp")))

(define-public crate-adivon-0.2.5 (c (n "adivon") (v "0.2.5") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.3") (d #t) (k 0)))) (h "1jfi6cvrpb5dycvgm7q4jw5dddlfv5dkhxdkz03qam3a7lc9vnbc")))

(define-public crate-adivon-0.2.6 (c (n "adivon") (v "0.2.6") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.6") (d #t) (k 0)))) (h "1ac2l2n0xql5x0bqalc22cw2lx6rb06if2b4rgrkpc4xsm24m09m") (f (quote (("dev" "clippy") ("default"))))))

