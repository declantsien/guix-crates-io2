(define-module (crates-io ad me admerge) #:use-module (crates-io))

(define-public crate-admerge-0.1.0 (c (n "admerge") (v "0.1.0") (d (list (d (n "byteseeker") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12jipq9y5v33hgpa97x55qqi4bhl07bi7841gqiilfnixbcx4qbc") (y #t)))

(define-public crate-admerge-0.1.1 (c (n "admerge") (v "0.1.1") (d (list (d (n "byteseeker") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dxpjznv85p8zfz1782w6iwqvswczq2b4bybhqrlf9ivnn98cch9")))

(define-public crate-admerge-0.1.2 (c (n "admerge") (v "0.1.2") (d (list (d (n "byteseeker") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06ai051nq26rprs17g7vjxlgmw9psy99l9s8pmkzq56plwhapabz")))

(define-public crate-admerge-0.1.3 (c (n "admerge") (v "0.1.3") (d (list (d (n "byteseeker") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13pr45cl9bwq2852gvzllyz0x32gpj394a8vygk7v6ybllsvpg9x")))

