(define-module (crates-io ad m- adm-cli) #:use-module (crates-io))

(define-public crate-adm-cli-0.1.0 (c (n "adm-cli") (v "0.1.0") (d (list (d (n "actix") (r "^0.7.9") (d #t) (k 0)) (d (n "adm") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "0ilx6c8mnbij3892hjgqjyghbv9w6fclpv4ix6mzmsm52pb24k1f")))

