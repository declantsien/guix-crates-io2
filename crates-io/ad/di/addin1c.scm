(define-module (crates-io ad di addin1c) #:use-module (crates-io))

(define-public crate-addin1c-0.1.0 (c (n "addin1c") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.11") (d #t) (k 0)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 0)))) (h "0nxmxcrjgy7ll20jkql0hl80mir2chfamm1l0fyz1mc3gpsq9nk1")))

(define-public crate-addin1c-0.1.1 (c (n "addin1c") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.11") (d #t) (k 0)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 0)))) (h "1hrif1qv9f76cfsj8hsxdf6rddw0xw5ji6kakzy8133bykxa5r1w")))

(define-public crate-addin1c-0.2.0 (c (n "addin1c") (v "0.2.0") (d (list (d (n "smallvec") (r "^1.11") (d #t) (k 0)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 0)))) (h "1hsd47rzp1llr6nyjbbrknx2zd03cbr5p7jjn7x1r5863918byaq")))

(define-public crate-addin1c-0.3.0 (c (n "addin1c") (v "0.3.0") (d (list (d (n "smallvec") (r "^1.11") (d #t) (k 0)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 0)))) (h "1flpj9xamwnqly41sksdb01f4w4ivy5vcpwqgn4bv5z91p3fm9zd")))

(define-public crate-addin1c-0.4.0 (c (n "addin1c") (v "0.4.0") (d (list (d (n "smallvec") (r "^1.11") (d #t) (k 0)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 0)))) (h "13d1z5ig3mwlcddz6crnv9ss2nw6sn29gcpr7lfyf2yzy43q23ci")))

