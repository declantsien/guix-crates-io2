(define-module (crates-io ad di additional-accounts-request) #:use-module (crates-io))

(define-public crate-additional-accounts-request-0.1.0 (c (n "additional-accounts-request") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("event-cpi"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.8.0") (d #t) (k 0)))) (h "0z81chd1dqawl0m7kz8vn8qzy50xy5s97lwhpci799kswrkjmgil")))

(define-public crate-additional-accounts-request-0.1.2 (c (n "additional-accounts-request") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.30.0") (f (quote ("event-cpi"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.30.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.8.0") (d #t) (k 0)))) (h "14r5fdmysf5bfgraxbdlhqpf62abhj349zkjvmdxdjhgk4ap5zdb")))

