(define-module (crates-io ad it aditus) #:use-module (crates-io))

(define-public crate-aditus-0.0.1 (c (n "aditus") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive" "unstable-doc" "env"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "1s4fwj22nrklggrsz9z5yqfqfxg4hyp9c84qjhwrjmsm2rkjlc8a")))

