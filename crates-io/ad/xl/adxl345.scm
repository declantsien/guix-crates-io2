(define-module (crates-io ad xl adxl345) #:use-module (crates-io))

(define-public crate-adxl345-0.0.3 (c (n "adxl345") (v "0.0.3") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "08169r1j6cyvxjw11rx7a0p9hwnrpbb1n2x1hpdqxfzzjkwqnab9")))

(define-public crate-adxl345-0.0.4 (c (n "adxl345") (v "0.0.4") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1g9wfdzm8vqcnpls5hbhicci8a6f115chqm26jh7s4rr4pxkm06s")))

