(define-module (crates-io ad xl adxl345_driver2) #:use-module (crates-io))

(define-public crate-adxl345_driver2-1.0.0 (c (n "adxl345_driver2") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "c2rust-bitfields") (r "^0.18") (f (quote ("no_std"))) (k 0)) (d (n "ctrlc") (r "^3") (f (quote ("termination"))) (d #t) (k 2)) (d (n "embedded-hal") (r "=1.0.0-rc.1") (d #t) (k 0)))) (h "0kbg6d7wnisaqh8rkdszhi2s31j92b5l4fjkc0kc1hjj0xkyi6zr") (f (quote (("no_std") ("default"))))))

(define-public crate-adxl345_driver2-2.0.0 (c (n "adxl345_driver2") (v "2.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "c2rust-bitfields") (r "^0.18") (f (quote ("no_std"))) (k 0)) (d (n "ctrlc") (r "^3") (f (quote ("termination"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "rppal") (r "^0.17") (f (quote ("hal"))) (d #t) (k 2)))) (h "0v4im5a7m3h3rric3phfqddy7xkn41az32k6586b54mhgbs3qiy0") (f (quote (("no_std") ("default"))))))

(define-public crate-adxl345_driver2-2.0.1 (c (n "adxl345_driver2") (v "2.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "c2rust-bitfields") (r "^0.18") (f (quote ("no_std"))) (k 0)) (d (n "ctrlc") (r "^3") (f (quote ("termination"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "rppal") (r "^0.17") (f (quote ("hal"))) (d #t) (k 2)))) (h "12cc90lv508z14sq0p80n1q4x18i9z8b9nyhlxj6l5i1br26bnvk") (f (quote (("no_std") ("default"))))))

