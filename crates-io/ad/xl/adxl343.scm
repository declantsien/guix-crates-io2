(define-module (crates-io ad xl adxl343) #:use-module (crates-io))

(define-public crate-adxl343-0.1.0 (c (n "adxl343") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0ixz1zmzfdsa32f83k5l046x9r3lv65hj6jdj4v1l0dzfwh1792f")))

(define-public crate-adxl343-0.2.0 (c (n "adxl343") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0qj7vg9vdlciwpn41blpz3wyi568zdkjb36nx7w6bhm98n621b5d")))

(define-public crate-adxl343-0.3.0 (c (n "adxl343") (v "0.3.0") (d (list (d (n "accelerometer") (r "^0.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1r10abpyr82d8kgivpmr96g5h77xn84llb8f66bsafi89p1b82xd")))

(define-public crate-adxl343-0.4.0 (c (n "adxl343") (v "0.4.0") (d (list (d (n "accelerometer") (r "^0.4") (f (quote ("orientation"))) (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0230cdv6hslcnj7jn35z63jxb5x5j12hpdpba3qyc7n8xiyd1mfq") (f (quote (("u16x3") ("i16x3") ("default" "i16x3"))))))

(define-public crate-adxl343-0.4.1 (c (n "adxl343") (v "0.4.1") (d (list (d (n "accelerometer") (r "^0.5.2") (f (quote ("orientation"))) (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "08kir1nvj9njbd436l3r6l9g1lm00yg2i82cfyg6vnwq4p3ywvn3") (f (quote (("u16x3") ("i16x3") ("default" "i16x3"))))))

(define-public crate-adxl343-0.4.2 (c (n "adxl343") (v "0.4.2") (d (list (d (n "accelerometer") (r "^0.6") (f (quote ("orientation"))) (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "05miz2mb45fsilcwc6nrk0lmrldz59c5rp3g2vqsjblk4ava2wig") (f (quote (("u16x3") ("i16x3") ("default" "i16x3"))))))

(define-public crate-adxl343-0.4.3 (c (n "adxl343") (v "0.4.3") (d (list (d (n "accelerometer") (r "^0.7") (f (quote ("orientation"))) (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1l289csk0anjyaa3h7z02fixkjkfmq0r7q2k84klg8q0ycvv6abq") (f (quote (("u16x3") ("i16x3") ("default" "i16x3"))))))

(define-public crate-adxl343-0.5.0 (c (n "adxl343") (v "0.5.0") (d (list (d (n "accelerometer") (r "^0.8") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "11ymmcmf7zm0ywb0zyhchhil8lsragq5hfdckj5yb3n4f9d4g6x6") (f (quote (("u16x3") ("i16x3") ("default" "i16x3"))))))

(define-public crate-adxl343-0.6.0 (c (n "adxl343") (v "0.6.0") (d (list (d (n "accelerometer") (r "^0.9") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1hn3iyjcngzndbvr55c5kispfwbjnvpkvqkqr0vkl8p70815zidl") (f (quote (("u16x3") ("i16x3") ("default" "i16x3"))))))

(define-public crate-adxl343-0.7.0 (c (n "adxl343") (v "0.7.0") (d (list (d (n "accelerometer") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1bfb6h5xsmgvr5hhlahd07zirpkw35lj1ly2gfikzaljlrc59y6k") (f (quote (("u16x3") ("i16x3") ("default" "i16x3"))))))

(define-public crate-adxl343-0.8.0 (c (n "adxl343") (v "0.8.0") (d (list (d (n "accelerometer") (r "^0.11") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1s8v50qmap8fq15pm6zs968dsffaf4wr110m4wd4vs3nnkjjbcsz") (f (quote (("u16x3") ("i16x3") ("default" "i16x3"))))))

