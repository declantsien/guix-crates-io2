(define-module (crates-io ad xl adxl313) #:use-module (crates-io))

(define-public crate-adxl313-0.1.0 (c (n "adxl313") (v "0.1.0") (d (list (d (n "accelerometer") (r "~0.11") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "0kr83nd22gby8qzijfy4mn3qakc584xbls5glwi03hrngjrpg92b")))

(define-public crate-adxl313-0.1.1 (c (n "adxl313") (v "0.1.1") (d (list (d (n "accelerometer") (r "~0.11") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "1a1ffwy64y17n20hm1hddxf7yc7wmjmkj20xylyhs92ipxvsjq1h")))

(define-public crate-adxl313-0.1.2 (c (n "adxl313") (v "0.1.2") (d (list (d (n "accelerometer") (r "~0.11") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "1raillw06csnmp76jj36s9j92idwbzbvssxa5cm4ividcahk6yb4")))

(define-public crate-adxl313-0.2.0 (c (n "adxl313") (v "0.2.0") (d (list (d (n "accelerometer") (r "~0.11") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "022gn35n31sp3gl51m11gbqad1cihwc8sc3c4qh2fqf0kxpb2d85")))

(define-public crate-adxl313-0.2.1 (c (n "adxl313") (v "0.2.1") (d (list (d (n "accelerometer") (r "~0.11") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "0l4ddm42zlz4l60fqmmsgr722glhyirnfsx6980krclcknh66nz3")))

(define-public crate-adxl313-0.2.2 (c (n "adxl313") (v "0.2.2") (d (list (d (n "accelerometer") (r "~0.11") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "09091zm0mzrc8z6qyw8vqmkb9fijnlri5ai2qqrdym2v13drb5w9")))

(define-public crate-adxl313-0.2.3 (c (n "adxl313") (v "0.2.3") (d (list (d (n "accelerometer") (r "~0.11") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "0pjm6iizhk8slxqzpbib60z77y69akf7bvd5rnz8473961a65hyn")))

(define-public crate-adxl313-0.2.4 (c (n "adxl313") (v "0.2.4") (d (list (d (n "accelerometer") (r "~0.11") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "0yd16gbrpii29fj1pmn2m673bkaxd89v8amd7pv35zljs5h3siq9")))

