(define-module (crates-io ad xl adxl345_driver) #:use-module (crates-io))

(define-public crate-adxl345_driver-0.0.6 (c (n "adxl345_driver") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "c2rust-bitfields") (r "^0.3.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (f (quote ("termination"))) (d #t) (k 2)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal" "hal-unproven"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1hgy0xr572njwkr94a546jvq9l1d8ip3vzibxyby4293pwhxi7vf")))

