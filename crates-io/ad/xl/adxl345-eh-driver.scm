(define-module (crates-io ad xl adxl345-eh-driver) #:use-module (crates-io))

(define-public crate-adxl345-eh-driver-0.1.0 (c (n "adxl345-eh-driver") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "12yjhxawyl66iq4fh82vi9qj9hnlznfh31rb66x27awirsisjbl6")))

(define-public crate-adxl345-eh-driver-0.2.0 (c (n "adxl345-eh-driver") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "1gl19yv9pxbmifvxzkzvx8h0qw337d5rl8kymrk5a9ihwph0ww60")))

(define-public crate-adxl345-eh-driver-0.2.1 (c (n "adxl345-eh-driver") (v "0.2.1") (d (list (d (n "bitfield") (r "^0.15.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0s8p3fdk2vj3bgmigfs8czik19601gy6924q1vhapihx8jw1ms6p")))

