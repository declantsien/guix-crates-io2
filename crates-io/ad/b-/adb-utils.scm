(define-module (crates-io ad b- adb-utils) #:use-module (crates-io))

(define-public crate-adb-utils-0.1.0 (c (n "adb-utils") (v "0.1.0") (h "1pk75c3nw7i5p8b0s5cxp9gw9wsccf452zhbydx9gjlr8l7nwm6z")))

(define-public crate-adb-utils-0.2.0 (c (n "adb-utils") (v "0.2.0") (h "0dspgjdxg1yd6slswd4q572rjkizskxj0ssi38cinxs699kvkw0v")))

(define-public crate-adb-utils-0.3.0 (c (n "adb-utils") (v "0.3.0") (h "0r3jy66lii3pvmmachk10yikbvgd7wn74kphwxr4w12yz5ngwizh")))

(define-public crate-adb-utils-0.4.0 (c (n "adb-utils") (v "0.4.0") (h "17kvnxv8zbdjcv9bigvcf0kdhw98z8qlj2m3ylvnw4qrj8v0wp4j")))

(define-public crate-adb-utils-0.5.0 (c (n "adb-utils") (v "0.5.0") (h "0si5xpwyw2dlj0cl5wjzbi81gv8x2yfcpfr093yi79mzrdqszizj")))

(define-public crate-adb-utils-0.6.0 (c (n "adb-utils") (v "0.6.0") (h "1w6nm2sm5j0kckl4c3d2w34bri20klhbzhdy8q0i8ssm3blg6c6s")))

