(define-module (crates-io ad b- adb-rs) #:use-module (crates-io))

(define-public crate-adb-rs-0.1.0 (c (n "adb-rs") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.1.0") (d #t) (k 0)))) (h "0w7524s0qyn2kihzi4y2w3i90sks10hvpwxnlwxzlr7g4rflkccn")))

(define-public crate-adb-rs-0.1.1 (c (n "adb-rs") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.1.0") (d #t) (k 0)))) (h "1zhfvkipwj3vpjkwlzbfhfkk7c3rww15y7pmw6mg5sx5qi436241")))

(define-public crate-adb-rs-0.1.3 (c (n "adb-rs") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.1.0") (d #t) (k 0)))) (h "079b155cm25gl2a4d2qdaxmycirsdyzrgcykqvpm0g890fzm3gnh")))

