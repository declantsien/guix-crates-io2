(define-module (crates-io ad b- adb-rust) #:use-module (crates-io))

(define-public crate-adb-rust-0.1.0 (c (n "adb-rust") (v "0.1.0") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cn9wq8k9f0y1nr3k1zyk8fmdyvyyak7a3v4cx900by028dq45kg")))

(define-public crate-adb-rust-0.1.1 (c (n "adb-rust") (v "0.1.1") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15yhwckmxdm150cj0k1f4rpp0wnh0y623d77gvj14jjwbip1nxdl")))

(define-public crate-adb-rust-0.1.2 (c (n "adb-rust") (v "0.1.2") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "016k9gj65g652h085sy81mj92rdqlpqij5q8zpxc9bf7rx5q4r16")))

(define-public crate-adb-rust-0.1.3 (c (n "adb-rust") (v "0.1.3") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1apx2gzbfwn3vbv4qxcl4dwrk28skk7hywk2ssph7kmp7jcn8jj4")))

(define-public crate-adb-rust-0.1.4 (c (n "adb-rust") (v "0.1.4") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k0xjqkxg8c6j529c39cr40hv3ka0cfxlyjhqzzdc5shxlcy12r0")))

(define-public crate-adb-rust-0.1.5 (c (n "adb-rust") (v "0.1.5") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k9ra701dmpr9ns1i3xvylymjrvqaa35krkk0di5cgbnj0rn5arq")))

(define-public crate-adb-rust-0.1.6 (c (n "adb-rust") (v "0.1.6") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k5mysd51snn05lxa45q4riwlvcd2gjlaphjr9mdf519llyn6bq0")))

(define-public crate-adb-rust-0.2.0 (c (n "adb-rust") (v "0.2.0") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16jb5lagxya9g94sblvc6mx8514ry3jp6cx9qncbm79p77s4qd39")))

(define-public crate-adb-rust-0.2.1 (c (n "adb-rust") (v "0.2.1") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0slfhrqjlk185m3j4rcx8j223fdzj6h06i13n1nhp6p6laxrir79")))

(define-public crate-adb-rust-0.2.2 (c (n "adb-rust") (v "0.2.2") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yabn3sc8bj2ilbcisxcllwmgvkxjg735d968ljg3grn545v58nc")))

(define-public crate-adb-rust-0.2.3 (c (n "adb-rust") (v "0.2.3") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17smsiii5frncir8dgkn9zbhb6z3m0wil03rjfb7ppyhfznzrrfi")))

(define-public crate-adb-rust-0.2.4 (c (n "adb-rust") (v "0.2.4") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pfgv73lm3wwcwjk1xz33pd2rbadjy8g888gvqkbl8qb8p2g98y8") (y #t)))

