(define-module (crates-io ad #{99}# ad9959) #:use-module (crates-io))

(define-public crate-ad9959-0.1.0 (c (n "ad9959") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1isykdbjg13hvvb997hwn3irxi4ac37c03rlss6qrazrivjri782")))

(define-public crate-ad9959-0.2.0 (c (n "ad9959") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.11.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0gh5nsjl26dfavx1wlgl6m15f70h09sfaicqgx23sbmgrajx55cv")))

(define-public crate-ad9959-0.2.1 (c (n "ad9959") (v "0.2.1") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0h2i0kqhxymasha5abdna2glxigks317pps9vr65fd1d69rl3d08")))

