(define-module (crates-io ad or adorn) #:use-module (crates-io))

(define-public crate-adorn-0.1.0 (c (n "adorn") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "1gffw5pgvw7jqgsg0rdz36dl9qzvbxd7vri28j8z88jkgjm64cwc")))

(define-public crate-adorn-0.1.1 (c (n "adorn") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "04i3q95kckrc57pk90zn350a1w8is2ayc24rcdypbnmccskahnbn")))

(define-public crate-adorn-0.1.2 (c (n "adorn") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "0zy6rifn0yapk9ydaclq8sxk04b92sx5jp4pd303f37rn3l12dld")))

(define-public crate-adorn-0.2.0 (c (n "adorn") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.3.5") (d #t) (k 2)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mkqfm4wmk13qckqf484piw6dq8ip3r8mzskbzcik8jsy87yqy4l")))

(define-public crate-adorn-0.2.1 (c (n "adorn") (v "0.2.1") (d (list (d (n "compiletest_rs") (r "^0.3.5") (d #t) (k 2)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vq20djh8hgq9yfnwp0hxb1ixdhn9fi5dg1y9kisfn2mmlr7fvgz")))

(define-public crate-adorn-0.3.0 (c (n "adorn") (v "0.3.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01kamlx8dff4pqgnbi6slbmrhb40bz6x1l26d89d60kgckc6fqi5")))

(define-public crate-adorn-0.4.0 (c (n "adorn") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m90iwm4726zyb8bqdj43gxdnxbadfyxzcr1k8xsivv85sq7gzbp")))

