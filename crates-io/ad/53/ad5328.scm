(define-module (crates-io ad #{53}# ad5328) #:use-module (crates-io))

(define-public crate-ad5328-0.1.0 (c (n "ad5328") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1wal5gpw3g8my9ig7p1abjxspx54pxxal298fgpvi5gsb5nkb36r")))

(define-public crate-ad5328-0.1.1 (c (n "ad5328") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1c1ajmv4p6w7gv46xjfiaj9rkfs54p52i3339dw8l5qj2qahpkxv")))

