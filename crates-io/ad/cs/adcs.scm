(define-module (crates-io ad cs adcs) #:use-module (crates-io))

(define-public crate-adcs-0.1.0 (c (n "adcs") (v "0.1.0") (d (list (d (n "astro") (r "^2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)))) (h "0hl1yjlnvn63vsskz9m41n575l5844gcxpxx8pzdppqdslj6mk57") (y #t)))

