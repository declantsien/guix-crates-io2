(define-module (crates-io ad te adtest) #:use-module (crates-io))

(define-public crate-adtest-0.1.0 (c (n "adtest") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "0ixs5l5sy1y3gfjmnmwmmx48f89w3gswjx9mhid7k4fbypqiwxs0")))

