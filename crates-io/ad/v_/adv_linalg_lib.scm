(define-module (crates-io ad v_ adv_linalg_lib) #:use-module (crates-io))

(define-public crate-adv_linalg_lib-0.1.0 (c (n "adv_linalg_lib") (v "0.1.0") (d (list (d (n "adv_linalg_proc_macro") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1dspc15p3lmpigc8kqhf8klbladnywfk90wizn7w71428csf9arr") (f (quote (("no_std") ("full" "no_std") ("default" "full"))))))

(define-public crate-adv_linalg_lib-0.1.1 (c (n "adv_linalg_lib") (v "0.1.1") (d (list (d (n "adv_linalg_proc_macro") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "03157knkbcn1w6cw5z2japnn1i0fgf2n5cljww0fci4v8n9wg6aw") (f (quote (("no_std") ("full" "no_std") ("default" "full"))))))

