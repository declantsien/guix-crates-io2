(define-module (crates-io ad v_ adv_random) #:use-module (crates-io))

(define-public crate-adv_random-1.0.0 (c (n "adv_random") (v "1.0.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "0z99mcnskkimyx9qsk2aflvwag8j11ls47iiya12w2rcvf4r2v2q")))

(define-public crate-adv_random-1.0.1 (c (n "adv_random") (v "1.0.1") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "0ndcl6fvzq8wxllrsp0lwkkj0rx8w2azmyrqg0b519bk55z6x39j")))

(define-public crate-adv_random-1.1.1 (c (n "adv_random") (v "1.1.1") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "14ahhzdwdscqxypkr4w8yfc655j7jcfd218lbyvksb48v67z90hf")))

(define-public crate-adv_random-2.0.0 (c (n "adv_random") (v "2.0.0") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "rand") (r "^0") (o #t) (d #t) (k 0)))) (h "1n4cn5a9s4iaaqzmk9wz7q3snd60yfy0l4nmcdz8v2yajf8zvii3") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-adv_random-2.0.1 (c (n "adv_random") (v "2.0.1") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "rand") (r "^0") (o #t) (d #t) (k 0)))) (h "11cm3m7nll07k30x30223jxrj8w78falghclrik8lbizdxwz7ns4") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-adv_random-2.0.2 (c (n "adv_random") (v "2.0.2") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "rand") (r "^0") (o #t) (d #t) (k 0)))) (h "0yl9psb2gjvm15wlfs0v4w94wg9lmy7l9ahd40vhs5iawk1z4177") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand"))))))

