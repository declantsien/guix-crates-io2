(define-module (crates-io ad v_ adv_linalg_proc_macro) #:use-module (crates-io))

(define-public crate-adv_linalg_proc_macro-0.1.0 (c (n "adv_linalg_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zbpm3hgvpc6sbd5ki4jickyd7bv80xpy8f0vimqn3bk7ajx3ad9")))

(define-public crate-adv_linalg_proc_macro-0.2.0 (c (n "adv_linalg_proc_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)))) (h "0fps6wlcp9nk5r0d5v6d095463ngff02qn9qgwj3rjfxrys5k0pz")))

