(define-module (crates-io ad bi adbium) #:use-module (crates-io))

(define-public crate-adbium-0.1.0 (c (n "adbium") (v "0.1.0") (h "1zkcbigpkz9bi1dx03l0ifd53fzambqlnbnahnyjg5jwpd8idwiw")))

(define-public crate-adbium-0.1.1 (c (n "adbium") (v "0.1.1") (h "11bajy2xd4b9k69c4g2124awczhnp7ngyr4vyamwwf3r32a7h0y2")))

(define-public crate-adbium-0.1.2 (c (n "adbium") (v "0.1.2") (h "0yp6y69xi9jbh62q7wsb7qyl43vdxs3q9kvbz29mmx06gvvnchkh")))

(define-public crate-adbium-0.1.3 (c (n "adbium") (v "0.1.3") (h "1bgb1b7cllh93hhqic7lp8q6b7rbk1jmydmc8w7ni00lg67qxhpl")))

