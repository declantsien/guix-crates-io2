(define-module (crates-io ad eq adequate) #:use-module (crates-io))

(define-public crate-adequate-0.1.0 (c (n "adequate") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "0mwdf4z2n8sqn400316dg65ihdknrdbl0iy19aadnw5n7g5n8r10")))

(define-public crate-adequate-0.1.1 (c (n "adequate") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "1dzxycy4ha3cgd91zcc73svzbzjmd53fdidkd08dfhqqf1zlsbdp")))

(define-public crate-adequate-0.1.2 (c (n "adequate") (v "0.1.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "1njbwhp2z2d42g0jivrx0c93wxrhrlq13qmvqhkvv0iprsmf6bnq")))

