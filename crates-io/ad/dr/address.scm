(define-module (crates-io ad dr address) #:use-module (crates-io))

(define-public crate-address-0.1.0 (c (n "address") (v "0.1.0") (h "1bx5gwglz0zq8v9n2fv4l7js4syhlplm8m1aa6212j8s0qq9rysn")))

(define-public crate-address-0.1.1 (c (n "address") (v "0.1.1") (h "0js80azn2wlwbn3i689g3ligw8v8px753m7hw2ccbhgbrp1br231")))

(define-public crate-address-0.4.0 (c (n "address") (v "0.4.0") (h "0v88kzaxcy1crrbsncrkfqx9av0ji9z8i4hdc27mxck7avq3iwn3")))

(define-public crate-address-0.5.0 (c (n "address") (v "0.5.0") (h "10d5889fcpp9vnrw5ka0ijp51ws764phzxj16baj8sjrysq987yx")))

(define-public crate-address-0.6.0 (c (n "address") (v "0.6.0") (h "07zikr4f54cil76vwfis19sb25lpzcmjpcfwz96h0b1vicj6l838")))

(define-public crate-address-0.7.0 (c (n "address") (v "0.7.0") (h "1mjjp49n1gl4x1rgbv5b3556rmrb2r3i33547a1hlrl8znph34qr")))

(define-public crate-address-0.8.0 (c (n "address") (v "0.8.0") (h "0hzrmpq6v0diidb0ls7ckzhmcnhkxi3fhwlpy3sn8zc5q7f981k3")))

(define-public crate-address-0.9.0 (c (n "address") (v "0.9.0") (h "1p3nclz0zv6d6qy8mj11s0jhwwjmb7128dk7cppl3q5brv7fxv0n")))

(define-public crate-address-0.10.0 (c (n "address") (v "0.10.0") (h "10cmhlkrdr521kbiw5j65483y5rc1ljv4kg26rq8x2d31dyrqfl9")))

