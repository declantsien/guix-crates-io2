(define-module (crates-io ad dr addr-hal) #:use-module (crates-io))

(define-public crate-addr-hal-0.1.0 (c (n "addr-hal") (v "0.1.0") (h "0z4xid6275h4appmkwgg8578f8yqm1yhhx8pfznw7h4db3djxa3p")))

(define-public crate-addr-hal-0.1.1 (c (n "addr-hal") (v "0.1.1") (h "1h96dys6z30cga4jzvs5qpi9wryc0xmqyk3y60y45n9c5bh82vyq")))

