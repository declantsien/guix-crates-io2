(define-module (crates-io ad dr addr_of_enum) #:use-module (crates-io))

(define-public crate-addr_of_enum-0.1.0 (c (n "addr_of_enum") (v "0.1.0") (d (list (d (n "addr_of_enum_macro") (r "^0.1.0") (d #t) (k 0)))) (h "10cn2711pybkz8j1accab4fnm7fm9g8mc3w43493f5y30pk6bgaf")))

(define-public crate-addr_of_enum-0.1.1 (c (n "addr_of_enum") (v "0.1.1") (d (list (d (n "addr_of_enum_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0hwws5hjdckwrllny79lp1aahrwdgn1wbijpabirmbzyf8wfib65")))

(define-public crate-addr_of_enum-0.1.2 (c (n "addr_of_enum") (v "0.1.2") (d (list (d (n "addr_of_enum_macro") (r "^0.1.2") (d #t) (k 0)))) (h "12myqak48zzqnvsvwzfv8nfxyyqc5zf0i106k8q4k755ybbjwpp0")))

(define-public crate-addr_of_enum-0.1.3 (c (n "addr_of_enum") (v "0.1.3") (d (list (d (n "addr_of_enum_macro") (r "^0.1.3") (d #t) (k 0)))) (h "1yqq3b6m5m0p98icprwz8a5qsfpw9q1pfffi4qa7031xqigqpnvf")))

(define-public crate-addr_of_enum-0.1.4 (c (n "addr_of_enum") (v "0.1.4") (d (list (d (n "addr_of_enum_macro") (r "^0.1.4") (d #t) (k 0)))) (h "0pw4zks1pc7jwpmbd0kr26m5049n86v7gxdrvwn6x01354dvbgk9")))

(define-public crate-addr_of_enum-0.1.5 (c (n "addr_of_enum") (v "0.1.5") (d (list (d (n "addr_of_enum_macro") (r "^0.1.5") (d #t) (k 0)))) (h "0273qkin5209if85vlyl5hv6blpq56av26ndzqhhkhdaibmihp00")))

