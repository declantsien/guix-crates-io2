(define-module (crates-io ad dr address-cmp) #:use-module (crates-io))

(define-public crate-address-cmp-0.1.0 (c (n "address-cmp") (v "0.1.0") (h "1n2lczcdidkdjs2mx92jx02kbv98q6ysc2jqr6r8x401rw55ack8") (y #t)))

(define-public crate-address-cmp-0.2.0 (c (n "address-cmp") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14nbgsmpg1sxngailb5i6dxbddzvvir2aliavkw6v3nvwnj4dblm")))

(define-public crate-address-cmp-0.2.1 (c (n "address-cmp") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sppzihj7s2qfqxxc4mz2j49rzl3nk8dcfirj05ixmqi8krxi06n")))

