(define-module (crates-io ad dr address-literal) #:use-module (crates-io))

(define-public crate-address-literal-1.0.0 (c (n "address-literal") (v "1.0.0") (d (list (d (n "primitive-types") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f1b4mgy4fwfijfmpdkmkz9bn4xcg8xb5q4gm242832z7p52q12f")))

(define-public crate-address-literal-1.1.0 (c (n "address-literal") (v "1.1.0") (d (list (d (n "primitive-types") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vpw6i8kancrl7b7m5vg9ddps4d555v5z58qzb22p57qfbymx7aw")))

(define-public crate-address-literal-1.1.1 (c (n "address-literal") (v "1.1.1") (d (list (d (n "primitive-types") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14sjqawynx8ajwjrf7adzg3hiakg0kx1x9qxwywrrj087njd462f")))

(define-public crate-address-literal-1.2.0 (c (n "address-literal") (v "1.2.0") (d (list (d (n "primitive-types") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p1h5vfhgjf4bk05nswv4m2p4slpxrm5kisn1r2wsyin6gj8pl5s")))

(define-public crate-address-literal-1.2.1 (c (n "address-literal") (v "1.2.1") (d (list (d (n "primitive-types") (r "^0.11") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z47w1yq3q9xjs5k6x6n6qs3r7qpvdhnapp96fz4rvznmilqbk4z")))

(define-public crate-address-literal-1.2.2 (c (n "address-literal") (v "1.2.2") (d (list (d (n "primitive-types") (r "^0.11.1") (f (quote ("rustc-hex"))) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "180p97lmzv14z9zp1c3j1bn5xga893qgf4j2clyc5q24rzdc9a3c")))

(define-public crate-address-literal-1.2.3 (c (n "address-literal") (v "1.2.3") (d (list (d (n "primitive-types") (r "^0.12.1") (f (quote ("rustc-hex"))) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "025z2kiscj8b26dd61b7ijzgrvggswaywml3c0bm2048wjz1b0za")))

(define-public crate-address-literal-1.2.4 (c (n "address-literal") (v "1.2.4") (d (list (d (n "primitive-types") (r "^0.12") (f (quote ("rustc-hex"))) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14wq5i8zj8vhwpabi6ijva4vc5ghxmdawz2pki4zs5psj2zcykbs")))

