(define-module (crates-io ad dr addressable_queue) #:use-module (crates-io))

(define-public crate-addressable_queue-0.1.0 (c (n "addressable_queue") (v "0.1.0") (h "0pr01a5vh377q8i341xqkirycpmmmz7l3zlskpc2dd96hqyf0mbg")))

(define-public crate-addressable_queue-0.2.0 (c (n "addressable_queue") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dpqn77k96nfp7yy705hlv48jwf0ww7gbs1ls02j7h0mc1wmj2yn") (f (quote (("default" "serde"))))))

(define-public crate-addressable_queue-0.2.1 (c (n "addressable_queue") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kll2pz89hin35ws0jnfsx6r3wwlk61rb1smn7x7jb2im505f41j") (f (quote (("default" "serde"))))))

