(define-module (crates-io ad dr addressable-pairing-heap) #:use-module (crates-io))

(define-public crate-addressable-pairing-heap-0.1.0 (c (n "addressable-pairing-heap") (v "0.1.0") (d (list (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "stash") (r "^0.1") (d #t) (k 0)) (d (n "unreachable") (r "^0") (d #t) (k 0)))) (h "0jg7a4a4iyrkdqj6405lyx183159migqacj0iifl0lsbcnpm4dfr") (f (quote (("bench"))))))

(define-public crate-addressable-pairing-heap-0.2.0 (c (n "addressable-pairing-heap") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "stash") (r "^0.1.2") (d #t) (k 0)) (d (n "unreachable") (r "^0.1.1") (d #t) (k 0)))) (h "1ydzkfa5pgc9azf249pffbx3rhk4n003xmk4cb0kmjh53nbmx483") (f (quote (("bench"))))))

