(define-module (crates-io ad s- ads-proto) #:use-module (crates-io))

(define-public crate-ads-proto-0.1.0 (c (n "ads-proto") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "058h4ii6wxngybblqymyhlfcqgry09vkj7jpwbnn30ljsx36r5dd")))

(define-public crate-ads-proto-0.1.1 (c (n "ads-proto") (v "0.1.1") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)))) (h "17wl67kw22ngah4bbymc7n798dvsx7x15l2kf9jnzcp3n86mvyx5")))

