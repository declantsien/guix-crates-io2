(define-module (crates-io ad ir adirector) #:use-module (crates-io))

(define-public crate-adirector-0.1.0 (c (n "adirector") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "time" "rt" "sync"))) (k 0)))) (h "07m3432ajln5b079gd6zlikavsa7xndjh3riw6s97ww8y7zx034y")))

(define-public crate-adirector-0.1.1 (c (n "adirector") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "time" "rt" "sync"))) (k 0)))) (h "1cba4b0jy7hdyqx318rsxnxp2wj3innr1szpcqrfhk7v1wlgsc36")))

