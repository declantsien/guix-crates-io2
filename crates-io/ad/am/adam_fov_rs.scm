(define-module (crates-io ad am adam_fov_rs) #:use-module (crates-io))

(define-public crate-adam_fov_rs-0.1.0 (c (n "adam_fov_rs") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("png" "bevy_winit" "bevy_wgpu"))) (k 2)) (d (n "bevy_ascii_terminal") (r "^0.7.4") (d #t) (k 2)) (d (n "bevy_tiled_camera") (r "^0.2.4") (d #t) (k 2)) (d (n "glam") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1jxixd1rzhw0g9c9yniqq87iiar09llvqhlrir0wivmy20fcgdca")))

(define-public crate-adam_fov_rs-0.1.1 (c (n "adam_fov_rs") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5") (f (quote ("png" "bevy_winit" "bevy_wgpu"))) (k 2)) (d (n "bevy_ascii_terminal") (r "^0.7.4") (d #t) (k 2)) (d (n "bevy_tiled_camera") (r "^0.2.4") (d #t) (k 2)) (d (n "glam") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0c7zs1rb6qd9iivp1lzlzs432l65ci96lf1jxkvjiqc5p5wmgch1")))

(define-public crate-adam_fov_rs-0.1.3 (c (n "adam_fov_rs") (v "0.1.3") (d (list (d (n "bevy") (r "^0.6") (f (quote ("png" "bevy_winit" "render"))) (k 2)) (d (n "bevy_ascii_terminal") (r "^0.9.0") (d #t) (k 2)) (d (n "bevy_tiled_camera") (r "^0.3.0") (d #t) (k 2)) (d (n "glam") (r "^0.20.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1n6jnqhiqkwiyywkq2jxj9336nzgv46qan5g81z8a5v9r5n1yxb2")))

(define-public crate-adam_fov_rs-0.1.4 (c (n "adam_fov_rs") (v "0.1.4") (d (list (d (n "bevy") (r "^0.7") (f (quote ("png" "bevy_winit" "render"))) (k 2)) (d (n "bevy_ascii_terminal") (r "^0.9.1") (d #t) (k 2)) (d (n "bevy_tiled_camera") (r "^0.3.1") (d #t) (k 2)) (d (n "glam") (r "^0.20.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1msdgxymyl348a911cj85f2nbij64f1nmmfz5v3ys7bnaq6pqriy")))

(define-public crate-adam_fov_rs-0.2.0 (c (n "adam_fov_rs") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("png" "bevy_winit" "bevy_render" "bevy_core_pipeline" "bevy_sprite"))) (k 2)) (d (n "bevy_ascii_terminal") (r "^0.11.4") (d #t) (k 2)) (d (n "glam") (r "^0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "sark_grids") (r "^0.5") (d #t) (k 0)))) (h "0hr9nmslxxnhx6fha7w8nm4d6chqlmf60fn2bi3gc6rhc2ldy1v9")))

