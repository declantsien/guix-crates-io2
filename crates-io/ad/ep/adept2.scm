(define-module (crates-io ad ep adept2) #:use-module (crates-io))

(define-public crate-adept2-0.1.0 (c (n "adept2") (v "0.1.0") (d (list (d (n "adept2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0mwx1pjryrpaw1mgl627v36crsf9z0xpl10v03rq97aghnv7m74l")))

