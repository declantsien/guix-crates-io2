(define-module (crates-io ad du addupstream) #:use-module (crates-io))

(define-public crate-addupstream-0.2.4 (c (n "addupstream") (v "0.2.4") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1a6vncfr9kpbi3af6ckdbpasa9zbk3bl5rp511109y7xyi7q8z2b")))

