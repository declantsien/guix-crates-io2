(define-module (crates-io ad s1 ads101x) #:use-module (crates-io))

(define-public crate-ads101x-0.1.0 (c (n "ads101x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 2)))) (h "18x36n4sr34h13w2pryj8wj5s0fh6sj3l0hac8ny0cqvvvwra3a0")))

