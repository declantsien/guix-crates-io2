(define-module (crates-io ad s1 ads1263) #:use-module (crates-io))

(define-public crate-ads1263-0.1.0 (c (n "ads1263") (v "0.1.0") (h "05162z751in5c772pll9wgc9mjylnp52gmknj8f3wjcrvz69j3dy")))

(define-public crate-ads1263-0.1.1 (c (n "ads1263") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1szanrkkykzrmjd8z8j9i2s3dbnwwwc130n1pydl917mdsr8s592")))

(define-public crate-ads1263-0.1.2 (c (n "ads1263") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "10pfalhf0v164dkvyqjzrw9vnwhq2dapgajclcz7gbyc1cvbgwqh")))

(define-public crate-ads1263-0.1.3 (c (n "ads1263") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1vlz55mqf9bpw7qs9mfpi21w63kgjqgy95kiqh5ic4jl3qmslixd")))

(define-public crate-ads1263-0.1.4 (c (n "ads1263") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1h1b7x3hqcbllhgnal956y337acxl434519h985ss7947w75g0f5")))

(define-public crate-ads1263-0.1.5 (c (n "ads1263") (v "0.1.5") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1kgm4larymqywv1w0vv52y3n38gzzan6z1z6bpkaryywr14ag65l")))

(define-public crate-ads1263-0.1.6 (c (n "ads1263") (v "0.1.6") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0z1r1ix9ic9fkz2jv7dxhij7nhj9zp3n6pif439i6h93fbg2h5ys")))

(define-public crate-ads1263-0.1.7 (c (n "ads1263") (v "0.1.7") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0wz0453my6799hwk3fzkq1a5p4q9lm7rryc0h9880vfy87aq49f5")))

