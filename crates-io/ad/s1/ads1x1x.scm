(define-module (crates-io ad s1 ads1x1x) #:use-module (crates-io))

(define-public crate-ads1x1x-0.1.0 (c (n "ads1x1x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "14jbqngyrnazx10b3krlg23bjirsz3lzxxmvla8d9vdlbdb7zk30")))

(define-public crate-ads1x1x-0.2.0 (c (n "ads1x1x") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "0brm7bn4536kn4sy7p5bhcvihj7il7xr3jjvm0qdrrvd2nfjhzgn")))

(define-public crate-ads1x1x-0.2.1 (c (n "ads1x1x") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "00bmj7i22jxy1srfxbhm6964a4q0vyr6ynpw7jl119v13x8cz70c")))

(define-public crate-ads1x1x-0.2.2 (c (n "ads1x1x") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "13j88138xyl370vy88nhv9gz71ha8lgrd26p1kdk6f8r90732i3b")))

