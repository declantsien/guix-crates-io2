(define-module (crates-io ad s1 ads122x04) #:use-module (crates-io))

(define-public crate-ads122x04-0.1.0 (c (n "ads122x04") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "1njf67pk3hzx0cd8p4adky0apfkryzf40r5vslzsl566lcky8h52")))

(define-public crate-ads122x04-0.2.0 (c (n "ads122x04") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "0f8ic33y9saqiz75mhiaisycncjslfc3kmwf6qh42y4qqxii5z7a")))

(define-public crate-ads122x04-0.3.0 (c (n "ads122x04") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "07a5ilq0hxqspw2kc117d6b8fd1srnihrksrpy0p2hzxnjvn4wk0")))

