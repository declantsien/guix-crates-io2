(define-module (crates-io ad s1 ads129xx) #:use-module (crates-io))

(define-public crate-ads129xx-0.1.0 (c (n "ads129xx") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "14cbz4g0v01ix1c1mapsjm2svbaggfbpavl7p1d450ag8z72i16z")))

(define-public crate-ads129xx-0.1.1 (c (n "ads129xx") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0x94kr96nn8fh2pgipiyfkjr5hyizxl045cx5fpzk6lm16afs2cr")))

(define-public crate-ads129xx-0.2.0 (c (n "ads129xx") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1vdm55qxf2jqi7b08fxcb2ajddnkfpgw7k3a1b240h0p3f14q0gj") (y #t)))

(define-public crate-ads129xx-0.2.1 (c (n "ads129xx") (v "0.2.1") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0yji68yb5dykjzbha7hb4bvl33aq324d6a312afvbjc8fwc6jvz6")))

