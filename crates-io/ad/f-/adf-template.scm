(define-module (crates-io ad f- adf-template) #:use-module (crates-io))

(define-public crate-adf-template-0.1.0 (c (n "adf-template") (v "0.1.0") (d (list (d (n "adf-template-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "htmltoadf") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "1k6f6fijfvyw0w1m4hyqm71mlj4igzhd3pg175wgn74la7iwc63z")))

