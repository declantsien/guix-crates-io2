(define-module (crates-io ad f- adf-template-macro) #:use-module (crates-io))

(define-public crate-adf-template-macro-0.1.0 (c (n "adf-template-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "04j26z06n2jaapsb4j3bprz45lxzhrpy6k5qlbi70q65p880rqj8")))

(define-public crate-adf-template-macro-0.1.1 (c (n "adf-template-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1f9dzni13a6vmks9681asf192ccymw9mrmvl4szy77lnw0vpj42z")))

