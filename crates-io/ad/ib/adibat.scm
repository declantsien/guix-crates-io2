(define-module (crates-io ad ib adibat) #:use-module (crates-io))

(define-public crate-adibat-0.1.0 (c (n "adibat") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0hlxyqv19iph51907yczz9dypibs7g8f6vn4giq2i117f2s2k509")))

(define-public crate-adibat-0.2.0 (c (n "adibat") (v "0.2.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0mvgkbbsvxlgpvfrxslqkykjxw1l5iy5573y1d7m96vpyx97psfq")))

