(define-module (crates-io ad i_ adi_gpu_vulkan) #:use-module (crates-io))

(define-public crate-adi_gpu_vulkan-0.1.0 (c (n "adi_gpu_vulkan") (v "0.1.0") (d (list (d (n "adi_gpu_base") (r "^0.1") (d #t) (k 0)) (d (n "afi") (r "^0.3.1") (d #t) (k 0)) (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.3") (d #t) (k 0)) (d (n "awi") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qks4k8r10adw8qsmh8vxw27r62prp24njw82i94ckxx982drqbq")))

(define-public crate-adi_gpu_vulkan-0.2.0 (c (n "adi_gpu_vulkan") (v "0.2.0") (d (list (d (n "adi_gpu_base") (r "^0.2") (d #t) (k 0)) (d (n "afi") (r "^0.3.2") (d #t) (k 0)) (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11zxf9f7gmsfiqvmvzw12m31l7139mra5n8fjkwyrzys6sf7mcjk")))

(define-public crate-adi_gpu_vulkan-0.3.0 (c (n "adi_gpu_vulkan") (v "0.3.0") (d (list (d (n "adi_gpu_base") (r "^0.4") (d #t) (k 0)) (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m2acj2jg5k5ggga1b71glf527j3kc9bszkvhfvbvmxcb09sky5k")))

(define-public crate-adi_gpu_vulkan-0.4.0 (c (n "adi_gpu_vulkan") (v "0.4.0") (d (list (d (n "adi_gpu_base") (r "^0.5") (d #t) (k 0)) (d (n "ami") (r "^0.7") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lfgzki15h5hnccqvnyf516svvi4zxb5shi7zwxpq3x2f0a605ay")))

(define-public crate-adi_gpu_vulkan-0.5.0 (c (n "adi_gpu_vulkan") (v "0.5.0") (d (list (d (n "adi_gpu_base") (r "^0.6") (d #t) (k 0)) (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x2nz21mvzwgrqdxb317ndqk20c9ycfkkccsb4x0k0sphdkp356z")))

(define-public crate-adi_gpu_vulkan-0.5.1 (c (n "adi_gpu_vulkan") (v "0.5.1") (d (list (d (n "adi_gpu_base") (r "^0.6") (d #t) (k 0)) (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0adgqzgmq4bnrmj95b67il62ix5n6j02a2n6rmi6nzfqi8h967mj")))

(define-public crate-adi_gpu_vulkan-0.5.2 (c (n "adi_gpu_vulkan") (v "0.5.2") (d (list (d (n "adi_gpu_base") (r "^0.6") (d #t) (k 0)) (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09vaa9zvz81vj078xlsvqkdig0aivmyidhjbwyq204z3yhmvc15g")))

(define-public crate-adi_gpu_vulkan-0.5.3 (c (n "adi_gpu_vulkan") (v "0.5.3") (d (list (d (n "adi_gpu_base") (r "^0.6") (d #t) (k 0)) (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cg59knjpnia4gqw0yz9ffq4qcqbxjvlndj2xxa1b1ld0z7h0qa9")))

(define-public crate-adi_gpu_vulkan-0.6.0 (c (n "adi_gpu_vulkan") (v "0.6.0") (d (list (d (n "adi_gpu_base") (r "^0.7") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0miljjhhjk82xi6bbhshk4fc62k5rb59nn8340248vrlw9qpsp9f")))

(define-public crate-adi_gpu_vulkan-0.7.0 (c (n "adi_gpu_vulkan") (v "0.7.0") (d (list (d (n "adi_gpu_base") (r "^0.8") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yl8daxjkq9a6kfq7swh3ybqz659pg99dncv432cxrig2i4qcfmj")))

(define-public crate-adi_gpu_vulkan-0.8.0 (c (n "adi_gpu_vulkan") (v "0.8.0") (d (list (d (n "adi_gpu_base") (r "^0.9") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ail1i3ymhm59x3c64v4zajl16r5f533ad8d5ky6m6km4113iq9h")))

(define-public crate-adi_gpu_vulkan-0.11.0 (c (n "adi_gpu_vulkan") (v "0.11.0") (d (list (d (n "adi_gpu_base") (r "^0.11") (d #t) (k 0)) (d (n "asi_vulkan") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l2cqlhbcjng2w1574axn8ss5lfkj0mr5xpj291m238p90zhxiz5")))

