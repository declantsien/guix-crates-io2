(define-module (crates-io ad i_ adi_clock) #:use-module (crates-io))

(define-public crate-adi_clock-0.1.0 (c (n "adi_clock") (v "0.1.0") (h "0gn3lifv03fss7b27810b3mrqc89zf3jhj4wsq5rf5fmcckhhipk")))

(define-public crate-adi_clock-0.1.1 (c (n "adi_clock") (v "0.1.1") (h "1pj57sqqrm675670nfdia5p2s1dlxshw380c47s4j7gnzdkrbxvy")))

(define-public crate-adi_clock-0.2.0 (c (n "adi_clock") (v "0.2.0") (h "16361fzj0d6s36as0h8as333zgs376x0vnkl0y1vx5y9a5rbai2j")))

(define-public crate-adi_clock-0.3.0 (c (n "adi_clock") (v "0.3.0") (h "1kliv5v6y6y4ilsgmqv32153jhydxfxwy7azx4b06x1a2y0rbg6l")))

