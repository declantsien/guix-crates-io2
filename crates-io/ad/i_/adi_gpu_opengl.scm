(define-module (crates-io ad i_ adi_gpu_opengl) #:use-module (crates-io))

(define-public crate-adi_gpu_opengl-0.1.0 (c (n "adi_gpu_opengl") (v "0.1.0") (d (list (d (n "adi_gpu_base") (r "^0.2") (d #t) (k 0)) (d (n "afi") (r "^0.3.2") (d #t) (k 0)) (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "asi_opengl") (r "^0.1") (d #t) (k 0)))) (h "0dgln035nv7pfkzd03r9yflgsnk337vrm2nnigv8xzm7mdzjkpq1")))

(define-public crate-adi_gpu_opengl-0.2.0 (c (n "adi_gpu_opengl") (v "0.2.0") (d (list (d (n "adi_gpu_base") (r "^0.4") (d #t) (k 0)) (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "asi_opengl") (r "^0.1") (d #t) (k 0)))) (h "0s9kp4nvla439vip5fhv11i6zpg0164lhp67r37xjyd823nrlq0d")))

(define-public crate-adi_gpu_opengl-0.3.0 (c (n "adi_gpu_opengl") (v "0.3.0") (d (list (d (n "adi_gpu_base") (r "^0.5") (d #t) (k 0)) (d (n "ami") (r "^0.7") (d #t) (k 0)) (d (n "asi_opengl") (r "^0.2") (d #t) (k 0)))) (h "14wx7fhmr3fxa2z5sx0w4bafq4hy2696i6a4zq2mmy2yhyaqgc9j")))

(define-public crate-adi_gpu_opengl-0.4.0 (c (n "adi_gpu_opengl") (v "0.4.0") (d (list (d (n "adi_gpu_base") (r "^0.6") (d #t) (k 0)) (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "asi_opengl") (r "^0.3") (d #t) (k 0)))) (h "07cv5jispiiihfqryy3ll6wz39zrab6vv0glg1206a4pm6n909vy")))

(define-public crate-adi_gpu_opengl-0.4.1 (c (n "adi_gpu_opengl") (v "0.4.1") (d (list (d (n "adi_gpu_base") (r "^0.6") (d #t) (k 0)) (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "asi_opengl") (r "^0.4") (d #t) (k 0)))) (h "0qcqswr0icdj5pyzi13q381czi3rrr826n77sgy5pk664rmrk0vr")))

(define-public crate-adi_gpu_opengl-0.5.0 (c (n "adi_gpu_opengl") (v "0.5.0") (d (list (d (n "adi_gpu_base") (r "^0.7") (d #t) (k 0)) (d (n "asi_opengl") (r "^0.4") (d #t) (k 0)))) (h "0lspi2dgi7jmwg6d81hyym46yfwy5hzzwx7y2f4wjqv7lis0g3y9")))

(define-public crate-adi_gpu_opengl-0.6.0 (c (n "adi_gpu_opengl") (v "0.6.0") (d (list (d (n "adi_gpu_base") (r "^0.8") (d #t) (k 0)) (d (n "asi_opengl") (r "^0.5") (d #t) (k 0)))) (h "1nq0pzvkf4g41jl3gkxf2rqwdr2lr2crf5qfwighjdjndkmzvh4j")))

(define-public crate-adi_gpu_opengl-0.7.0 (c (n "adi_gpu_opengl") (v "0.7.0") (d (list (d (n "adi_gpu_base") (r "^0.9") (d #t) (k 0)) (d (n "asi_opengl") (r "^0.5") (d #t) (k 0)))) (h "0fbdldma57hpb48dlw06ffs3bypgnv1sxbr9a3dl9x1blbd6n04f")))

(define-public crate-adi_gpu_opengl-0.11.0 (c (n "adi_gpu_opengl") (v "0.11.0") (d (list (d (n "adi_gpu_base") (r "^0.11") (d #t) (k 0)) (d (n "asi_opengl") (r "^0.6") (d #t) (k 0)))) (h "0xmi85mly2l9mya1lqf0f5ga07p6wmsqni7icxpy8mamfzbnxzir")))

