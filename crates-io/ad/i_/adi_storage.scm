(define-module (crates-io ad i_ adi_storage) #:use-module (crates-io))

(define-public crate-adi_storage-0.1.0 (c (n "adi_storage") (v "0.1.0") (h "1cjmy17hkyykys8q5la9fchxr7182mbzjdddrhlqny4p6i3p6ppz")))

(define-public crate-adi_storage-0.2.0 (c (n "adi_storage") (v "0.2.0") (d (list (d (n "whoami") (r "^0.1.1") (d #t) (k 0)))) (h "1mz8wns7nx1cgdg7w0sh95bb5a4asw2jz8nsdx53f25hj4w2ngdx")))

(define-public crate-adi_storage-0.1.1 (c (n "adi_storage") (v "0.1.1") (d (list (d (n "whoami") (r "^0.2.0") (d #t) (k 0)))) (h "1hn6dixq86pcpf454iiyfcvwnipraikizp9yb1lhcxf9fvvxkciq")))

(define-public crate-adi_storage-0.2.1 (c (n "adi_storage") (v "0.2.1") (d (list (d (n "whoami") (r "^0.2.0") (d #t) (k 0)))) (h "0nprrcfnyk0cziswi3kwz28w3ax87sqhzdkwn4zjabmi4yrfsxlx")))

