(define-module (crates-io ad i_ adi_gpu_base) #:use-module (crates-io))

(define-public crate-adi_gpu_base-0.1.0 (c (n "adi_gpu_base") (v "0.1.0") (d (list (d (n "afi") (r "^0.3.1") (d #t) (k 0)) (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "awi") (r "^0.2") (d #t) (k 0)))) (h "1izywjk43amj53ch9vksl4dv1pslmiifhpyh4zymm545dskk5fis")))

(define-public crate-adi_gpu_base-0.2.0 (c (n "adi_gpu_base") (v "0.2.0") (d (list (d (n "afi") (r "^0.3.2") (d #t) (k 0)) (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "awi") (r "^0.3") (d #t) (k 0)))) (h "1d6c84kxr33ggzyrrqvmpgbjdfr6lcgmgz1xwpfx9n4hirv22dxq")))

(define-public crate-adi_gpu_base-0.3.0 (c (n "adi_gpu_base") (v "0.3.0") (d (list (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "awi") (r "^0.4") (d #t) (k 0)))) (h "1rpwik4xx6g2jy858hl6wfrmjdr65vgr51mzlm2fl3dxbp7h26kk")))

(define-public crate-adi_gpu_base-0.4.0 (c (n "adi_gpu_base") (v "0.4.0") (d (list (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "awi") (r "^0.5") (d #t) (k 0)))) (h "06zi1qciig7m2444h261ss1p4pyjdhlvfl8bq5aygplna7sayphf")))

(define-public crate-adi_gpu_base-0.5.0 (c (n "adi_gpu_base") (v "0.5.0") (d (list (d (n "ami") (r "^0.7") (d #t) (k 0)) (d (n "awi") (r "^0.5") (d #t) (k 0)))) (h "0nj9ba2j88sc820s98wi0wzvbk0pd5c0q4gnx4qhnx010wc4y3iv")))

(define-public crate-adi_gpu_base-0.6.0 (c (n "adi_gpu_base") (v "0.6.0") (d (list (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "awi") (r "^0.6") (d #t) (k 0)))) (h "1asflgkysjl8lr7c3xjhcjh78qj55553a58vranzzkwnsyipaai5")))

(define-public crate-adi_gpu_base-0.6.1 (c (n "adi_gpu_base") (v "0.6.1") (d (list (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "awi") (r "^0.7") (d #t) (k 0)))) (h "0857y1wmfcdcvn7kr1x4f915h0mwz31ga2aj8hpj73ika0pwlq1x")))

(define-public crate-adi_gpu_base-0.7.0 (c (n "adi_gpu_base") (v "0.7.0") (d (list (d (n "ami") (r "^0.9") (d #t) (k 0)) (d (n "awi") (r "^0.7") (d #t) (k 0)))) (h "1sw9m2g3yfnffzrrwf8wljd52xcndq2yzlay5rks8afh25654mg6")))

(define-public crate-adi_gpu_base-0.8.0 (c (n "adi_gpu_base") (v "0.8.0") (d (list (d (n "awi") (r "^0.7") (d #t) (k 0)) (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "euler") (r "^0.3") (d #t) (k 0)))) (h "1xjlybi0bbvbziiwljzlvdslf4dhnrm27z1npdmh4hn7fz203d8n")))

(define-public crate-adi_gpu_base-0.9.0 (c (n "adi_gpu_base") (v "0.9.0") (d (list (d (n "awi") (r "^0.7") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "euler") (r "^0.4") (d #t) (k 0)))) (h "0mnj81fqbdghzr3jl09skr21a0g85809j8c81fv1m0a2wxv7zr7b")))

(define-public crate-adi_gpu_base-0.11.0 (c (n "adi_gpu_base") (v "0.11.0") (d (list (d (n "awi") (r "^0.8") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "euler") (r "^0.4") (d #t) (k 0)))) (h "1fv9akr0nvqxyqxry9y2k0c97a1a0cmk7zam4x367nvc2m5qg4y3")))

