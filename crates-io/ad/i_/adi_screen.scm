(define-module (crates-io ad i_ adi_screen) #:use-module (crates-io))

(define-public crate-adi_screen-0.1.0 (c (n "adi_screen") (v "0.1.0") (d (list (d (n "adi_clock") (r "^0.2.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.38") (d #t) (k 1)))) (h "1n5zb6k01sf0iw5ih0x6zp4xzv9p44nvq7vq7ad31nz9nivs84sz") (f (quote (("validation") ("checks"))))))

(define-public crate-adi_screen-0.2.0 (c (n "adi_screen") (v "0.2.0") (d (list (d (n "adi_clock") (r "^0.2.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.38") (d #t) (k 1)))) (h "09lhv3azhssxwivx9niwknx4wg8f42kcvw3v1is7k2pnqyy4gns7") (f (quote (("validation") ("checks"))))))

(define-public crate-adi_screen-0.2.1 (c (n "adi_screen") (v "0.2.1") (d (list (d (n "adi_clock") (r "^0.2.0") (d #t) (k 0)) (d (n "ami") (r "^0.2.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.50") (d #t) (k 1)))) (h "19x4r9z7dwh86s7l7sy04w2xaggjwc3w3iwjq0zwrcr3rh4kdda4") (f (quote (("validation") ("checks")))) (y #t)))

(define-public crate-adi_screen-0.2.2 (c (n "adi_screen") (v "0.2.2") (d (list (d (n "adi_clock") (r "^0.2.0") (d #t) (k 0)) (d (n "ami") (r "^0.2.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.50") (d #t) (k 1)))) (h "13ipz40hw0qjqkl9pm2ymg10k3dri4gvv8wf8ppjljxwa3pk4bxg") (f (quote (("validation") ("checks"))))))

(define-public crate-adi_screen-0.3.0 (c (n "adi_screen") (v "0.3.0") (d (list (d (n "aci_png") (r "^0.5.1") (d #t) (k 0)) (d (n "aci_ppm") (r "^0.4") (d #t) (k 2)) (d (n "adi_clock") (r "^0.3") (d #t) (k 0)) (d (n "adi_gpu") (r "^0.1") (d #t) (k 0)) (d (n "afi") (r "^0.3.1") (d #t) (k 0)) (d (n "ami") (r "^0.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.3.0") (d #t) (k 0)))) (h "0vqp4gi25scbac1ck65d1fr1mvrwpjy87m9hhhpgz6vhzyxw8xwa")))

(define-public crate-adi_screen-0.4.0 (c (n "adi_screen") (v "0.4.0") (d (list (d (n "aci_png") (r "^0.5.1") (d #t) (k 0)) (d (n "aci_ppm") (r "^0.4") (d #t) (k 2)) (d (n "adi_clock") (r "^0.3") (d #t) (k 0)) (d (n "adi_gpu") (r "^0.2") (d #t) (k 0)) (d (n "afi") (r "^0.3.1") (d #t) (k 0)) (d (n "ami") (r "^0.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.3.0") (d #t) (k 0)))) (h "10miki9niyfzqd4vs3vynxy1vhjbm1mm31rksbrmhgnjflwrdrsb")))

(define-public crate-adi_screen-0.5.0 (c (n "adi_screen") (v "0.5.0") (d (list (d (n "aci_png") (r "^0.5.1") (d #t) (k 0)) (d (n "adi_clock") (r "^0.3") (d #t) (k 0)) (d (n "adi_gpu") (r "^0.3") (d #t) (k 0)) (d (n "afi") (r "^0.3.1") (d #t) (k 0)) (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "awi") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.4") (d #t) (k 0)))) (h "0akkf7cjqqmw0xjvlyxjsmz5k0p2axgq3hwp5h62hw6aj5ws098d")))

(define-public crate-adi_screen-0.6.0 (c (n "adi_screen") (v "0.6.0") (d (list (d (n "aci_png") (r "^0.5.2") (d #t) (k 0)) (d (n "adi_clock") (r "^0.3") (d #t) (k 0)) (d (n "adi_gpu") (r "^0.4") (d #t) (k 0)) (d (n "afi") (r "^0.3.2") (d #t) (k 0)) (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "rusttype") (r "^0.4") (d #t) (k 0)))) (h "1k3md2cqlycpj1q74swylb40hmks71ngjkw1jd3kysjwvxhf32r5")))

(define-public crate-adi_screen-0.7.0 (c (n "adi_screen") (v "0.7.0") (d (list (d (n "aci_png") (r "^0.6") (d #t) (k 0)) (d (n "adi_clock") (r "^0.3") (d #t) (k 0)) (d (n "adi_gpu") (r "^0.5") (d #t) (k 0)) (d (n "ami") (r "^0.6") (d #t) (k 0)) (d (n "rusttype") (r "^0.5.2") (d #t) (k 0)))) (h "1mjvz53sccn8cy6pxfydp577aahl0rxbgfs0l5h6zhymskbyw68r")))

(define-public crate-adi_screen-0.8.0 (c (n "adi_screen") (v "0.8.0") (d (list (d (n "aci_png") (r "^0.6") (d #t) (k 0)) (d (n "adi_clock") (r "^0.3") (d #t) (k 0)) (d (n "adi_gpu") (r "^0.7") (d #t) (k 0)) (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "fonterator") (r "^0.1") (d #t) (k 0)))) (h "1fsdzkpclqbwqg4c0qvcx6nvp19yqn7dybwjfb7yvsbdd253psgd")))

(define-public crate-adi_screen-0.9.0 (c (n "adi_screen") (v "0.9.0") (d (list (d (n "aci_png") (r "^0.6") (d #t) (k 0)) (d (n "adi_clock") (r "^0.3") (d #t) (k 0)) (d (n "adi_gpu") (r "^0.7") (d #t) (k 0)) (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "fonterator") (r "^0.1") (d #t) (k 0)))) (h "09563s9zfwilh8m2nakfb08arh2yygswn056qla07znq3iyb22d5")))

(define-public crate-adi_screen-0.10.0 (c (n "adi_screen") (v "0.10.0") (d (list (d (n "aci_png") (r "^0.6") (d #t) (k 0)) (d (n "adi_clock") (r "^0.3") (d #t) (k 0)) (d (n "adi_gpu") (r "^0.8") (d #t) (k 0)) (d (n "ami") (r "^0.9") (d #t) (k 0)) (d (n "fonterator") (r "^0.1") (d #t) (k 0)))) (h "0dh20qkz17h0nk9gcnd07712hqd7whkpcf8va0qrfjcv03z6zw4c")))

(define-public crate-adi_screen-0.11.0 (c (n "adi_screen") (v "0.11.0") (d (list (d (n "aci_png") (r "^0.6") (d #t) (k 0)) (d (n "adi_clock") (r "^0.3") (d #t) (k 0)) (d (n "adi_gpu") (r "^0.9") (d #t) (k 0)) (d (n "fonterator") (r "^0.1") (d #t) (k 0)))) (h "0vc52036vl7q9jdymjgp7chxpagv7c099nr9wxnlvzffd1vyiwvi")))

(define-public crate-adi_screen-0.11.1 (c (n "adi_screen") (v "0.11.1") (d (list (d (n "aci_png") (r "^0.6") (d #t) (k 0)) (d (n "adi_clock") (r "^0.3") (d #t) (k 0)) (d (n "adi_gpu") (r "^0.10") (d #t) (k 0)) (d (n "fonterator") (r "^0.1") (d #t) (k 0)))) (h "0lksbfqzzg2g7f7xx9j6a7a8i3qgl2b3lfdy5y8mmyjw9nw7gnjw")))

(define-public crate-adi_screen-0.12.0 (c (n "adi_screen") (v "0.12.0") (d (list (d (n "aci_png") (r "^0.7") (d #t) (k 0)) (d (n "adi_clock") (r "^0.3") (d #t) (k 0)) (d (n "adi_gpu") (r "^0.11") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fonterator") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.2") (d #t) (k 0)))) (h "17aj2hyzfixk7qczqlccw8xgnay7i7m1kr3r2wb66bbgs7kdnz5m")))

(define-public crate-adi_screen-0.13.0 (c (n "adi_screen") (v "0.13.0") (h "1ffmcc4fr2ajsxjm7ank24wjqcbgz6wnnhn4w4g800zqzmm1833m")))

