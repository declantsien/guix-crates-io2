(define-module (crates-io ad an adana-std-process) #:use-module (crates-io))

(define-public crate-adana-std-process-0.0.6 (c (n "adana-std-process") (v "0.0.6") (d (list (d (n "adana-script-core") (r "^0.15.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)))) (h "1ilsgc6rkdv9mnya45lmfpym95iczky6mxq7hcx6zc7mqdppjwfi") (r "1.73")))

(define-public crate-adana-std-process-0.16.0 (c (n "adana-std-process") (v "0.16.0") (d (list (d (n "adana-script-core") (r "^0.16.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0agzrmcizcb8bbvl8hrxsykw2vc695ab2q3xzffz4ydq4fvn0fm7") (r "1.75")))

(define-public crate-adana-std-process-0.17.0 (c (n "adana-std-process") (v "0.17.0") (d (list (d (n "adana-script-core") (r "^0.17.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "116d7krz15k70zx9sp33nyqkcpd17pxjla8x5yskwggv0ya6kp51") (r "1.76")))

