(define-module (crates-io ad an adana-std-date) #:use-module (crates-io))

(define-public crate-adana-std-date-0.0.6 (c (n "adana-std-date") (v "0.0.6") (d (list (d (n "adana-script-core") (r "^0.15.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0pch9klk2mj7i99s5fmmbixmf357wp9vvxa1c9l8j52lw04l1l0k") (r "1.73")))

(define-public crate-adana-std-date-0.16.0 (c (n "adana-std-date") (v "0.16.0") (d (list (d (n "adana-script-core") (r "^0.16.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1ddyhvx34avmgrx0smvqri0xrs35il7h7g90wm77c3qbd6nyr78l") (r "1.75")))

(define-public crate-adana-std-date-0.17.0 (c (n "adana-std-date") (v "0.17.0") (d (list (d (n "adana-script-core") (r "^0.17.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0dmyyaccnjmxxbicrqbc2yzb8zc427chrdvs2a1awd3vx8708wk6") (r "1.76")))

