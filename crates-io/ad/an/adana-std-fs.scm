(define-module (crates-io ad an adana-std-fs) #:use-module (crates-io))

(define-public crate-adana-std-fs-0.0.6 (c (n "adana-std-fs") (v "0.0.6") (d (list (d (n "adana-script-core") (r "^0.15.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)))) (h "1brhz132j7hf1mv9pckiwd817fvjj9i2z8h986p8m4m55lw5s16y") (r "1.73")))

(define-public crate-adana-std-fs-0.16.0 (c (n "adana-std-fs") (v "0.16.0") (d (list (d (n "adana-script-core") (r "^0.16.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "1c2w7c2ij7l42dqfpnfmgwppy84rnq304azhb3l193cgkvi4jnln") (r "1.75")))

(define-public crate-adana-std-fs-0.17.0 (c (n "adana-std-fs") (v "0.17.0") (d (list (d (n "adana-script-core") (r "^0.17.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0vb12lhr60zz6xgz5g6k6wkycw08lxqbzjxxr7gjhp5gyslnipz8") (r "1.76")))

