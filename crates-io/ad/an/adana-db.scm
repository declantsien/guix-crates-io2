(define-module (crates-io ad an adana-db) #:use-module (crates-io))

(define-public crate-adana-db-0.16.5-rc.1 (c (n "adana-db") (v "0.16.5-rc.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)))) (h "10xdpw2w2b56h4ljkdsay8bdmqyimw8mkxr379qx11vqzxryfag3") (r "1.75")))

(define-public crate-adana-db-0.17.1 (c (n "adana-db") (v "0.17.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)))) (h "0250lvfmrhkc4bh8sgbga575m99gwf1mzbppq6dhmf69647b7z52") (r "1.75")))

(define-public crate-adana-db-0.17.2 (c (n "adana-db") (v "0.17.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)))) (h "1qyjl61w2vb98vlw0mvm18q24hfzbv19kwrn2r13x1qx8zdssg7m") (r "1.75")))

(define-public crate-adana-db-0.17.3 (c (n "adana-db") (v "0.17.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)))) (h "0dni640qk4aws2yl6dl3pga683wn9gzwdv6zyi22klldkl0knv9s") (r "1.76")))

(define-public crate-adana-db-0.17.4 (c (n "adana-db") (v "0.17.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)))) (h "08kfd6hzfkzvx78cvh7v61wab8il1cyp4c1zblp7l935v56lq5n6") (r "1.76")))

(define-public crate-adana-db-0.17.5 (c (n "adana-db") (v "0.17.5") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)))) (h "0wlnv0cby16kyil32nk10gnm6r6621hhpljp3my51psh3gcvr3zd") (r "1.76")))

(define-public crate-adana-db-0.17.6 (c (n "adana-db") (v "0.17.6") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)))) (h "11j6x3p8x7iizi6n1p734whhxr30b7y43sprffdvi2jaw9g3xmrn") (r "1.76")))

(define-public crate-adana-db-0.17.7 (c (n "adana-db") (v "0.17.7") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)))) (h "18mkgrqrs41cm8dmbll6j3m6jp74xvgwgp9m673r8gzljhk51jch") (r "1.76")))

(define-public crate-adana-db-0.17.8 (c (n "adana-db") (v "0.17.8") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)))) (h "02kyjw6pzd2wb6hrib54g8riks31hnqmqglcmqmq24l4vh64arqw") (r "1.77")))

(define-public crate-adana-db-0.17.9 (c (n "adana-db") (v "0.17.9") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("serde_derive" "rc"))) (d #t) (k 0)))) (h "1n09qs4x9f6dvlj7liafdm77n8xw2c6w1qc2k0r7lqdfzsdr42lq") (r "1.78")))

