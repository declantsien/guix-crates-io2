(define-module (crates-io ad an adana-std-io) #:use-module (crates-io))

(define-public crate-adana-std-io-0.0.6 (c (n "adana-std-io") (v "0.0.6") (d (list (d (n "adana-script-core") (r "^0.15.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)))) (h "1yj9xlkghanvr53lpgfag65bpwa43b2zvg137q14m566qpdl9xcw") (r "1.73")))

(define-public crate-adana-std-io-0.16.0 (c (n "adana-std-io") (v "0.16.0") (d (list (d (n "adana-script-core") (r "^0.16.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "081gngs6qw8d6fy5qj3k3qrv19id3bz73wxvd90whdi9p5w42qp1") (r "1.75")))

(define-public crate-adana-std-io-0.17.0 (c (n "adana-std-io") (v "0.17.0") (d (list (d (n "adana-script-core") (r "^0.17.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "01shjlpabyvm0x9fvch0ba1cdz8w3k6jp8kck1bv4vmf1f3zq5cz") (r "1.76")))

