(define-module (crates-io ad e7 ade791x) #:use-module (crates-io))

(define-public crate-ade791x-0.3.1 (c (n "ade791x") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)))) (h "04cdrxw653cmwg3bsd2w394bgapngjfr1s5pmlwqkpganc0l3kl9")))

(define-public crate-ade791x-0.3.2 (c (n "ade791x") (v "0.3.2") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)))) (h "157rhm686pmlxxmkpv46p60xhrwhw3815kc5i23fd9cbndkr312p")))

(define-public crate-ade791x-0.3.3 (c (n "ade791x") (v "0.3.3") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)))) (h "0jmcm3f6pjssg2zlqmx0x5rd37qc7rqhgl3vq5vz7bg2h4qka9vk")))

(define-public crate-ade791x-0.3.4 (c (n "ade791x") (v "0.3.4") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)))) (h "0469lsms93rya38r9yk8fnr1g0aw9l9bc1awc8xx2ip0s67pzx53")))

