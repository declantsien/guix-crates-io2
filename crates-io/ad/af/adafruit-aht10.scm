(define-module (crates-io ad af adafruit-aht10) #:use-module (crates-io))

(define-public crate-adafruit-aht10-0.1.0 (c (n "adafruit-aht10") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1snrdhqjvyqlm40ml7wx182ffaccs35ywh2y2s6fa7kig2nygr0z")))

