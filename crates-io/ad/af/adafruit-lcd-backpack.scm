(define-module (crates-io ad af adafruit-lcd-backpack) #:use-module (crates-io))

(define-public crate-adafruit-lcd-backpack-0.1.1 (c (n "adafruit-lcd-backpack") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "mcp230xx") (r "^1.0.0") (d #t) (k 0)))) (h "0g2qi8w4q6ir5xjbzwk1hnblxjyajzlmxdqv3p2hah3y5kxl896b") (s 2) (e (quote (("defmt" "dep:defmt"))))))

