(define-module (crates-io ad af adafruit-alphanum4) #:use-module (crates-io))

(define-public crate-adafruit-alphanum4-0.1.0 (c (n "adafruit-alphanum4") (v "0.1.0") (d (list (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "ht16k33") (r "^0.4.0") (k 0)))) (h "0vbia9087bvyyxykpv714fznqc8rsnwm72s0n2m3n90avga254j8")))

(define-public crate-adafruit-alphanum4-0.1.1 (c (n "adafruit-alphanum4") (v "0.1.1") (d (list (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "ht16k33") (r "^0.4.0") (k 0)))) (h "1m33n2fq9z1ikivqy4rry009c1g2n8iqfzflvbns8yd7b3c8k5jf")))

(define-public crate-adafruit-alphanum4-0.1.2 (c (n "adafruit-alphanum4") (v "0.1.2") (d (list (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "ht16k33") (r "^0.4.0") (k 0)))) (h "123m6bw8lz59xvxg8fnsawkkk1avm82ldcwj7dyv1cnpm7nf7a6b")))

