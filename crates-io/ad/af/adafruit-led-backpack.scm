(define-module (crates-io ad af adafruit-led-backpack) #:use-module (crates-io))

(define-public crate-adafruit-led-backpack-0.1.0 (c (n "adafruit-led-backpack") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "ht16k33") (r "^0.4") (d #t) (k 0)) (d (n "rppal") (r "^0.11") (f (quote ("hal"))) (d #t) (k 2)))) (h "16valw1nndby9k26wabciz96ghb9ynnwydmn2wwmn5nib6jy686z")))

