(define-module (crates-io ad af adafruit_motorkit) #:use-module (crates-io))

(define-public crate-adafruit_motorkit-0.1.0 (c (n "adafruit_motorkit") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 0)) (d (n "pwm-pca9685") (r "^0.2") (d #t) (k 0)) (d (n "rppal") (r "^0.11") (f (quote ("hal"))) (d #t) (k 0)))) (h "0k1riq6x0asrypwlxqrx65jxzgd15mxz73lfwpabj3ppw932wpvk")))

(define-public crate-adafruit_motorkit-0.1.1 (c (n "adafruit_motorkit") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 0)) (d (n "pwm-pca9685") (r "^0.2") (d #t) (k 0)) (d (n "rppal") (r "^0.11") (f (quote ("hal"))) (d #t) (k 0)))) (h "1y99vn7zz8cqh3k0khbssmc1c5zfpq5i7i4b4czswb5xlng68g3p")))

