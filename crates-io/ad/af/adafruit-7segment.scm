(define-module (crates-io ad af adafruit-7segment) #:use-module (crates-io))

(define-public crate-adafruit-7segment-0.1.0 (c (n "adafruit-7segment") (v "0.1.0") (d (list (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "ht16k33") (r "^0.4.0") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0wx5l4m3pyzw51w8b7s13p0s5n77p202lddwzjmfhaq1pddm9s7r") (f (quote (("std") ("default" "std"))))))

