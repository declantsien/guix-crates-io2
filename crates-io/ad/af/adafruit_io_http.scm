(define-module (crates-io ad af adafruit_io_http) #:use-module (crates-io))

(define-public crate-adafruit_io_http-0.0.1 (c (n "adafruit_io_http") (v "0.0.1") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "042w7cah6lxn614lrb45g2whd9bks0f6zs8krv05yf8g7mdl5k5f")))

(define-public crate-adafruit_io_http-0.0.2 (c (n "adafruit_io_http") (v "0.0.2") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "0n916pjn84maryb7n8dzf9x7yi55jnl6g8jyhh3qrw02h7si3yc8")))

(define-public crate-adafruit_io_http-0.0.3 (c (n "adafruit_io_http") (v "0.0.3") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "08y09iyzyyipk36c4zcf4gk8lqwdijgbd8nxfglldc6i84m5hklb")))

(define-public crate-adafruit_io_http-0.0.31 (c (n "adafruit_io_http") (v "0.0.31") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "0d3vgrwx1n2a0kyajp63w8h79j4zy0njr3x85nhqhkar5kzginkb")))

(define-public crate-adafruit_io_http-0.0.32 (c (n "adafruit_io_http") (v "0.0.32") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "07nz2b4g3w5bhjgi05i9ayr5d3jmhfp2zkj9q7myv1g94fzhmckk")))

(define-public crate-adafruit_io_http-0.1.0 (c (n "adafruit_io_http") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.3.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0547rvnwj3kmyxsk7rfpxbg1jbyy358z8gs28j5r1h55msks080b")))

(define-public crate-adafruit_io_http-0.1.1 (c (n "adafruit_io_http") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.3.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0g9l0b997hcn0pgh3axgqhd7anjnw1fzammc6r9ffkx2552ha0rp")))

