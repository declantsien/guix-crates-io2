(define-module (crates-io ad af adafruit_gps) #:use-module (crates-io))

(define-public crate-adafruit_gps-0.1.0 (c (n "adafruit_gps") (v "0.1.0") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "0a6iry8b7xrhxzgp8pqvi00n45rpy24n6qpirqkczh84bzbb3j2z")))

(define-public crate-adafruit_gps-0.1.1 (c (n "adafruit_gps") (v "0.1.1") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "08v7w2njml5ya8yr3awchv67jgvnz062h753gb0i8m4q64b6g98q")))

(define-public crate-adafruit_gps-0.1.2 (c (n "adafruit_gps") (v "0.1.2") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "1sh3aqj1xwybskxxlbshrjhpml3rn8nkl3jql5hw4gpmdypkq0sb")))

(define-public crate-adafruit_gps-0.2.0 (c (n "adafruit_gps") (v "0.2.0") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "0kd1ncfigdhnawr57zbgnhvhyczcd43bdc7fwrjhg6izd87578r0")))

(define-public crate-adafruit_gps-0.2.1 (c (n "adafruit_gps") (v "0.2.1") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "0r0d8cw4phcf8ici8asmkznxjprks3za8xi8wkzkwqxca0xya255")))

(define-public crate-adafruit_gps-0.2.2 (c (n "adafruit_gps") (v "0.2.2") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "1v6sllvh176sigzwnrn44dmw2vbjhc07c5m85vipyl3wjrzhyjqx")))

(define-public crate-adafruit_gps-0.3.0 (c (n "adafruit_gps") (v "0.3.0") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "1cs6dmdm1k3ykl17g0ld7n5ib3lqcjylm5ksjcj16xyp6x0lykrc") (y #t)))

(define-public crate-adafruit_gps-0.3.1 (c (n "adafruit_gps") (v "0.3.1") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "13v3v85dqk6gpl356b49ci0rfr4hfqx6rb2c8d8r956mipngkymz")))

(define-public crate-adafruit_gps-0.3.2 (c (n "adafruit_gps") (v "0.3.2") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "02cg608grlmqr628z24w569s8n59bjd8a370hmymzm8ym8ilzv40")))

(define-public crate-adafruit_gps-0.3.3 (c (n "adafruit_gps") (v "0.3.3") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "09zmpfxgak7ir2dn1xadqcn8gkyszy1mc1y3j0gq1l729my2i13j")))

(define-public crate-adafruit_gps-0.3.4 (c (n "adafruit_gps") (v "0.3.4") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "0wz9n656zh4p7s0cnbbxxb5iac5ic93fkqvldmqcp37rk7262vil")))

(define-public crate-adafruit_gps-0.3.5 (c (n "adafruit_gps") (v "0.3.5") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "1d8830z0byb5iqf29nnwb34ryarcdyhw59n3r4cyhxjkna0w0nvi")))

(define-public crate-adafruit_gps-0.3.6 (c (n "adafruit_gps") (v "0.3.6") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "0ybalfrxikyjr2dk1brgbymqgbj0vjwa7qr7yvbv1mils8j533wk")))

(define-public crate-adafruit_gps-0.4.1 (c (n "adafruit_gps") (v "0.4.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "0g23gv4zvqv8wddlj8h0albwshki6av7bhf0n1shkip4ss887icl") (f (quote (("bench"))))))

