(define-module (crates-io ad af adafruit-nrf52840-express) #:use-module (crates-io))

(define-public crate-adafruit-nrf52840-express-0.1.0 (c (n "adafruit-nrf52840-express") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.14") (d #t) (k 0)) (d (n "shared-bus") (r "^0.2.2") (d #t) (k 0)))) (h "1axam1n3bmv7k5hkwm13dxg0df9lqb8wrjpfjn1w0di14h3zfgn0")))

(define-public crate-adafruit-nrf52840-express-0.2.0 (c (n "adafruit-nrf52840-express") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.14") (d #t) (k 0)) (d (n "shared-bus") (r "^0.2.2") (d #t) (k 0)))) (h "0fksxzwwc0mkb6as1k1ixqy8i8nzlp59nyzm82s9pa0qv4v6rf8q")))

