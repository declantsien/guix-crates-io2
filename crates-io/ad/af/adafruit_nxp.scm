(define-module (crates-io ad af adafruit_nxp) #:use-module (crates-io))

(define-public crate-adafruit_nxp-0.1.0 (c (n "adafruit_nxp") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (f (quote ("libm-force"))) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 2)))) (h "038rp7fxa17i3mlk35d8g0ns00raql96vc7ldma6r8gzjs8bwpww")))

(define-public crate-adafruit_nxp-0.1.1 (c (n "adafruit_nxp") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (f (quote ("libm-force"))) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 2)))) (h "1sbla8ad7y9appk93a7iiva7d5mpx3fd3fjbjs672fpdmjwqccsn")))

(define-public crate-adafruit_nxp-0.1.2 (c (n "adafruit_nxp") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (f (quote ("libm-force"))) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 2)))) (h "1457qf9kz7nla6zd93hr7awmr7p6mndmbcbsgr01ysjw2qk8s6fw")))

(define-public crate-adafruit_nxp-0.1.3 (c (n "adafruit_nxp") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (f (quote ("libm-force"))) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 2)))) (h "0hlyjan2h91r41jvn3c62gpfm2yb99l6c1v2ac8fwd9gkn1sh4sk")))

