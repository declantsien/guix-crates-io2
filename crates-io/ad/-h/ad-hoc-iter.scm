(define-module (crates-io ad -h ad-hoc-iter) #:use-module (crates-io))

(define-public crate-ad-hoc-iter-0.1.0 (c (n "ad-hoc-iter") (v "0.1.0") (h "0zfrx3c3vy7sl69wx9zdjxg50bn7cnqd5085akcw5nvzdpxmkvij") (f (quote (("maybe-many") ("default" "maybe-many"))))))

(define-public crate-ad-hoc-iter-0.1.1 (c (n "ad-hoc-iter") (v "0.1.1") (h "1qx6ic2f69jj6kf1qjvyghcz85q6xhdyp02b7y7jidw1id4i76pi") (f (quote (("maybe-many") ("default" "maybe-many"))))))

(define-public crate-ad-hoc-iter-0.2.0 (c (n "ad-hoc-iter") (v "0.2.0") (h "163rg7070lxm28lrsg6s7mfb1bahm2jwavc3050c7ci3niciqg81") (f (quote (("maybe-many"))))))

(define-public crate-ad-hoc-iter-0.2.1 (c (n "ad-hoc-iter") (v "0.2.1") (h "0qnr855c83di3y7h4lfhsiqrcyvzx0wfa9483jvwxfh0d5m9kswr") (f (quote (("maybe-many"))))))

(define-public crate-ad-hoc-iter-0.2.2 (c (n "ad-hoc-iter") (v "0.2.2") (h "1s001883zh88zvcda4plh8xgrqpsg8qivqi11yad2cv08wls8s2h") (f (quote (("maybe-many"))))))

(define-public crate-ad-hoc-iter-0.2.3 (c (n "ad-hoc-iter") (v "0.2.3") (h "1j09hgdrlblky063ahbmqcjlx3dgpby308i6hwv33dffprvdva4h") (f (quote (("maybe-many"))))))

