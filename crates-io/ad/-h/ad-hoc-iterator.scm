(define-module (crates-io ad -h ad-hoc-iterator) #:use-module (crates-io))

(define-public crate-ad-hoc-iterator-0.2.1 (c (n "ad-hoc-iterator") (v "0.2.1") (h "1k5fr0dap87sr95wk2pdqpwp9l726zkmgb2xyl490zj188ddsd12")))

(define-public crate-ad-hoc-iterator-0.2.2 (c (n "ad-hoc-iterator") (v "0.2.2") (h "03smn0p060rnrl8nvrzmb4ycmjz0xscm31y9lb47gqglmnb966j2")))

(define-public crate-ad-hoc-iterator-0.2.3 (c (n "ad-hoc-iterator") (v "0.2.3") (h "0v70s0i26pma2k978wj46k301ac9264xfl6py111s75sfbvwpsi4")))

