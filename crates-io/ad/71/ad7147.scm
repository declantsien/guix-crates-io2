(define-module (crates-io ad #{71}# ad7147) #:use-module (crates-io))

(define-public crate-ad7147-0.1.0 (c (n "ad7147") (v "0.1.0") (d (list (d (n "byte-slice-cast") (r "^1.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-1") (r "=1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)))) (h "15cqs9bh9yjkmvl4wy62nkdkdb82c0mjjmnws2hiq8qwikcq50pn")))

