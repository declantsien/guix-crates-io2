(define-module (crates-io ad zu adzuna) #:use-module (crates-io))

(define-public crate-adzuna-1.0.0 (c (n "adzuna") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0myzx07kpbrngls489xniqiyp8dndhp1xqrwqq7k7bcspp686a4k")))

(define-public crate-adzuna-1.0.1 (c (n "adzuna") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gqmg151ixyj20lwbs5z9gdq3sg5q2jz27jjakf3w5x4c9daq6hd")))

(define-public crate-adzuna-1.0.2 (c (n "adzuna") (v "1.0.2") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03f1r70y86x299xz9qv003cppyk94cfmn3hymzjlcp68wssxd19v")))

