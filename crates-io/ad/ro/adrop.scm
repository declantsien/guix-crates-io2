(define-module (crates-io ad ro adrop) #:use-module (crates-io))

(define-public crate-adrop-0.1.0 (c (n "adrop") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1ysmha6awkmmf9fb3l38gjn2z4drqz3kyr6lrswk720q2hc2zr0n")))

(define-public crate-adrop-0.2.0 (c (n "adrop") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1mi1h7dlyh44s6slrckv7hlqisbmzzgxgaabgy58pwcyaz5lvffd")))

(define-public crate-adrop-0.2.1 (c (n "adrop") (v "0.2.1") (h "0a36hs4xlkg2djf5zsg3hlggwwd8mk37cp70kdniw3q0m3ryl01s")))

