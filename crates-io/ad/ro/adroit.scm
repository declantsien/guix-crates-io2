(define-module (crates-io ad ro adroit) #:use-module (crates-io))

(define-public crate-adroit-0.0.0 (c (n "adroit") (v "0.0.0") (h "0l3hx8wv5y13lybcbmfs5ai40azyvmjvdsy09hzmpl1ppwkj045c")))

(define-public crate-adroit-0.1.0 (c (n "adroit") (v "0.1.0") (d (list (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "logos") (r "^0.14") (d #t) (k 0)))) (h "09bwi9y7wkl9a27jj28ydszhgw4f7by5k8211as4a8kkn0dyq5h4")))

