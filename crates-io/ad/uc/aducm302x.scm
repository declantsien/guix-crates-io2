(define-module (crates-io ad uc aducm302x) #:use-module (crates-io))

(define-public crate-ADuCM302x-0.1.0 (c (n "ADuCM302x") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0l4fsyqhiz1cwizs6da7s7xl19gfqhgyqfl1wic37k4s1x7m1r4i") (f (quote (("rt" "cortex-m-rt/device"))))))

