(define-module (crates-io ad #{98}# ad983x) #:use-module (crates-io))

(define-public crate-ad983x-0.1.0 (c (n "ad983x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1yn28mrzz5qrm06x8h65s1nfyw9shksvc9iad9jbczj1603bas3q")))

(define-public crate-ad983x-0.1.1 (c (n "ad983x") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "14x6240lbf607p1acnpxxr7dgk32dxw4i1k6a9hfrfjhqr9ww189")))

(define-public crate-ad983x-0.2.0 (c (n "ad983x") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0ymydb4sn134bva4b6mfsrg3fn9nsxs29y1pm53y37s1z9d9q302")))

(define-public crate-ad983x-0.3.0 (c (n "ad983x") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "081ynawngkgifhmci98qp4kvyd0m42y7nh37pnbjibjmgqaahr7k")))

(define-public crate-ad983x-1.0.0 (c (n "ad983x") (v "1.0.0") (d (list (d (n "dummy-pin") (r "^1.0.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-bus") (r "^0.1") (d #t) (k 2)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (k 2)))) (h "1xl8dg7vh9rscsxx1c9kd01lgbnbhzxrpnp2xydfrilqb4p4bzgj")))

