(define-module (crates-io ad #{98}# ad9850) #:use-module (crates-io))

(define-public crate-ad9850-0.1.0 (c (n "ad9850") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "01f8vavf7iiw0kidfcfabvwgv7wfy88b3wx1q98jsy7921kdckwm")))

(define-public crate-ad9850-0.1.1 (c (n "ad9850") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1kqv0hf0cfji3ijax6k0q4jmn64mxri3z44ggajdmgjh9lsl8jp0")))

(define-public crate-ad9850-0.1.2 (c (n "ad9850") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0yz911fmp3x7b4csp88v9pgvqi3g6jh4haq570261250rbrlngy7")))

