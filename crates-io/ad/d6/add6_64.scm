(define-module (crates-io ad d6 add6_64) #:use-module (crates-io))

(define-public crate-add6_64-0.1.0 (c (n "add6_64") (v "0.1.0") (d (list (d (n "add3") (r "^2.0.0") (d #t) (k 0)))) (h "0s8j9lj8ck4vjsb4f3f01ym3bbhzkmnj2ry0qyqin3kw9cdg3gjf")))

(define-public crate-add6_64-0.2.0 (c (n "add6_64") (v "0.2.0") (d (list (d (n "add3") (r "^3.0.0") (d #t) (k 0)))) (h "03nrajmbp7pkbmcdyga6fxlnmn2zwwi7q8p1qhv1v9sxzm0053wk")))

