(define-module (crates-io ad vo advocat) #:use-module (crates-io))

(define-public crate-advocat-3.0.0 (c (n "advocat") (v "3.0.0") (d (list (d (n "configparser") (r "^3.0") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "1g680ga1npw0ggdxschsk03xk0gg3cmv31yad457wj6m4548hc9j")))

