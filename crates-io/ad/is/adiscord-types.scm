(define-module (crates-io ad is adiscord-types) #:use-module (crates-io))

(define-public crate-adiscord-types-0.0.1 (c (n "adiscord-types") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)))) (h "09w8nz2acw69avvg0rqlizlprjjzg1zf10nkpiknxs3n1zlr12hq") (f (quote (("gateway"))))))

(define-public crate-adiscord-types-0.0.2 (c (n "adiscord-types") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)))) (h "1x4k7k144z7hbvl1bff1bfvdqx6jshjs59d047ffzi8h64qccys1") (f (quote (("gateway"))))))

(define-public crate-adiscord-types-0.0.3 (c (n "adiscord-types") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0387p1b7vx7zxs1iaylp6q9135ccdzs4amrql4lqim84gk4995y8") (f (quote (("gateway"))))))

(define-public crate-adiscord-types-0.0.4 (c (n "adiscord-types") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)))) (h "15kkqwzcclns7c3sswmhppyda1d8h3km1pa595fyhrsd5dkii42q") (f (quote (("gateway"))))))

(define-public crate-adiscord-types-0.0.5 (c (n "adiscord-types") (v "0.0.5") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.14") (d #t) (k 0)))) (h "0caigx266hjzxbyzjv6ac9m0yxlqmz0kkfn5dvwaa5l8msmjav2n") (f (quote (("gateway"))))))

(define-public crate-adiscord-types-0.0.6 (c (n "adiscord-types") (v "0.0.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "11q5xgjcz0a6jvwbf80570021jwp72xvv93n4mhgxg26rx6wc2w4") (f (quote (("gateway"))))))

