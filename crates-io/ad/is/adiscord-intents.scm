(define-module (crates-io ad is adiscord-intents) #:use-module (crates-io))

(define-public crate-adiscord-intents-0.0.1 (c (n "adiscord-intents") (v "0.0.1") (h "0sm2ajm22z92if4cil159qb04a4qzbr7ayvhh2cp4rvkqi3vif2r")))

(define-public crate-adiscord-intents-0.0.2 (c (n "adiscord-intents") (v "0.0.2") (h "09ai7gknwdwns7bjx3lmza8f2zzayyv3fvz5c2fm9xi0li00x3zm")))

(define-public crate-adiscord-intents-0.0.3 (c (n "adiscord-intents") (v "0.0.3") (h "0wfb0x4bvj3rik80bazfg86q9xcmmlfi21fiz7j5whxx2nrjg1b8")))

(define-public crate-adiscord-intents-0.0.4 (c (n "adiscord-intents") (v "0.0.4") (h "19q9ra3x3l3v097aq84d1hfaiprash4n3hldc6vfc3kzxsyzvjdn")))

