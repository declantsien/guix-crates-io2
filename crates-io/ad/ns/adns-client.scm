(define-module (crates-io ad ns adns-client) #:use-module (crates-io))

(define-public crate-adns-client-0.1.0 (c (n "adns-client") (v "0.1.0") (d (list (d (n "adns-proto") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h4fc79pgiv26fxsh3456g74cm6b1s604rgchz35afv69wxs1v49")))

(define-public crate-adns-client-0.1.1 (c (n "adns-client") (v "0.1.1") (d (list (d (n "adns-proto") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17g0nsavvmc00yypc66fs8xvzqhg98nkm747vrv2c0rha2z4bj82")))

