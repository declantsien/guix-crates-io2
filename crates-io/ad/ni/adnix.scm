(define-module (crates-io ad ni adnix) #:use-module (crates-io))

(define-public crate-adnix-0.1.0 (c (n "adnix") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 1)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)))) (h "0l743i6xk025xq11mbd168ri4iak3bmbcdslflyhvq95dbvsgsjj")))

(define-public crate-adnix-0.1.1 (c (n "adnix") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 1)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)))) (h "0h0w25y4mpnp2ajpy75b1d7yjq9yfhky1xhmjlk1nch9vwdblbxr")))

(define-public crate-adnix-0.2.0 (c (n "adnix") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 1)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)))) (h "0w5crpvccks772mivm2bv9qj2s6r4hwq18ry9y80cx5yiy4jpnlj")))

(define-public crate-adnix-0.3.0 (c (n "adnix") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 1)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "ureq") (r "^0.11.2") (d #t) (k 0)))) (h "112rdxy8icw1j3ikfballvwvi5ykffx6f46m330fhks2n1h3m51i")))

(define-public crate-adnix-0.4.0 (c (n "adnix") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 1)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "ureq") (r "^0.12.1") (d #t) (k 0)))) (h "049ccnkbb2kc2cd31901vx421nj2gihn401zpsvizjz5fc5cf6x3")))

(define-public crate-adnix-0.4.1 (c (n "adnix") (v "0.4.1") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (d #t) (k 1)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "ureq") (r "^1.2.0") (d #t) (k 0)))) (h "140h72fhldd79c9x3y2dkv521a33ziwlmsrdj6pbajqbn7aqb4yw")))

(define-public crate-adnix-0.4.2 (c (n "adnix") (v "0.4.2") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.1") (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0dz50zncv53dambw2smxakkk1lp1mimp3c78x6k1c2xzv0019ifi")))

(define-public crate-adnix-0.4.3 (c (n "adnix") (v "0.4.3") (d (list (d (n "clap") (r "^4.2.0") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "0rf0zhrh3ygxksvivsvpgv2ac2aasdf1xblz85krn9kj2d9ybl4q")))

(define-public crate-adnix-0.4.4 (c (n "adnix") (v "0.4.4") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "0hm8vn0jfa0dq85ac85dk8s8inb0nrlvxwzddc90zfkb7w4dx3p4")))

(define-public crate-adnix-0.4.5 (c (n "adnix") (v "0.4.5") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "1r0r816vk2k7jcza2v2cs22xk9j2y8sw04ayg8gd1y1zclarpavs")))

(define-public crate-adnix-0.4.6 (c (n "adnix") (v "0.4.6") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "1xbapgbl89jq7qw7h4qxf50sfxpisiyf8qn9mfd4xj54qf5awvzb")))

(define-public crate-adnix-0.4.7 (c (n "adnix") (v "0.4.7") (d (list (d (n "clap") (r "^4.3.24") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "15d69l49mngi3snqyw3ghnkg2pf930gbwy1m5cg1xnkrn2cx2clp")))

(define-public crate-adnix-0.4.8 (c (n "adnix") (v "0.4.8") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "regex") (r "^1.10.1") (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)))) (h "0sz2v7p05nnkzklvnskha6wb2i7g08y00il381k6a00pv29i6m6z")))

(define-public crate-adnix-0.4.9 (c (n "adnix") (v "0.4.9") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)))) (h "1nc1jjp9lbypq5g0hmjnnmnl892hh1yd5x3yymjxsjid1byf1572")))

(define-public crate-adnix-0.4.10 (c (n "adnix") (v "0.4.10") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "080n1kylca7a2mfjqkgvggn24xzmfw2yj15jn7g7a5f0aklmdhh4")))

