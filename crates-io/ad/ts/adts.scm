(define-module (crates-io ad ts adts) #:use-module (crates-io))

(define-public crate-adts-0.2.0 (c (n "adts") (v "0.2.0") (d (list (d (n "genindex") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1942hnf6bznl1yk1ylambnmwfxxbk16xb15bahpag2za184m2zw6") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde" "genindex/serde"))))))

