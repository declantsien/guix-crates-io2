(define-module (crates-io ad ts adts-reader) #:use-module (crates-io))

(define-public crate-adts-reader-0.1.0 (c (n "adts-reader") (v "0.1.0") (d (list (d (n "bitstream-io") (r "^0.6.3") (d #t) (k 2)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 2)))) (h "07dfvrgk9na8as52jrs4r5fjvpdsqc45x514abvdh29168h8w222")))

(define-public crate-adts-reader-0.2.0 (c (n "adts-reader") (v "0.2.0") (d (list (d (n "bitstream-io") (r "^0.6.3") (d #t) (k 2)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 2)))) (h "1ycfhs9kj4d0963w6yrn5lbplrgdxnfdpl1zhvn78v0lbq4hkd7r")))

(define-public crate-adts-reader-0.3.0 (c (n "adts-reader") (v "0.3.0") (d (list (d (n "bitstream-io") (r "^0.6.3") (d #t) (k 2)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 2)))) (h "1drfbizdbnsg9x02gz67afhxjx5qlk49z50gn81ckhfya2cknzq4")))

