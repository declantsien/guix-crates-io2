(define-module (crates-io ad he adhesion) #:use-module (crates-io))

(define-public crate-adhesion-0.1.0 (c (n "adhesion") (v "0.1.0") (h "1dm1wqyqpzrzigqp3pb59wm4q65675208dab7iy516cl0fh8s4vd")))

(define-public crate-adhesion-0.2.0 (c (n "adhesion") (v "0.2.0") (d (list (d (n "galvanic-assert") (r "^0.8.3") (d #t) (k 2)) (d (n "scan-rules") (r "^0.2.0") (d #t) (k 2)) (d (n "skeptic") (r "^0.10.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.10.0") (d #t) (k 2)))) (h "0lnjqvm52c1x7l8p9s0m43iz8gb5msb30aqga6fvlmvscz0ymlhv")))

(define-public crate-adhesion-0.3.0 (c (n "adhesion") (v "0.3.0") (d (list (d (n "galvanic-assert") (r "^0.8.3") (d #t) (k 2)) (d (n "scan-rules") (r "^0.2.0") (d #t) (k 2)) (d (n "skeptic") (r "^0.10.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.10.0") (d #t) (k 2)))) (h "1w1i8rydrzw4c98w5wjjrck6pw3lq5wcvsns4whhh6abw4sx2qz8")))

(define-public crate-adhesion-0.4.0 (c (n "adhesion") (v "0.4.0") (d (list (d (n "galvanic-assert") (r "^0.8.3") (d #t) (k 2)) (d (n "scan-rules") (r "^0.2.0") (d #t) (k 2)) (d (n "skeptic") (r "^0.10.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.10.0") (d #t) (k 2)))) (h "02wq9j7b1c43waky2idmv0j0smffh3mxyn6rsi24dv0hg93dnvxi")))

(define-public crate-adhesion-0.5.0 (c (n "adhesion") (v "0.5.0") (d (list (d (n "galvanic-assert") (r "^0.8.3") (d #t) (k 2)) (d (n "scan-rules") (r "^0.2.0") (d #t) (k 2)) (d (n "skeptic") (r "^0.10.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.10.0") (d #t) (k 2)))) (h "0y8581imir8yv1v69qlyvwcazgsxm10904zail94aqjxpxv8wqr4")))

