(define-module (crates-io ad ss adss) #:use-module (crates-io))

(define-public crate-adss-0.2.1 (c (n "adss") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "star-sharks") (r "^0.6.0") (f (quote ("zeroize_memory"))) (k 0)) (d (n "strobe-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.5") (d #t) (k 0)))) (h "0w6vv09m73yajgh3gprgm394n5m350dg3kiknf124qgz4yn6rg6q")))

(define-public crate-adss-0.2.2 (c (n "adss") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "star-sharks") (r "^0.6.0") (f (quote ("zeroize_memory"))) (k 0)) (d (n "strobe-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.5") (d #t) (k 0)))) (h "1pxr4y4vgwp8i0bp9acm505q07nnf16vxvhr03j0nnbyhfw6hskn")))

