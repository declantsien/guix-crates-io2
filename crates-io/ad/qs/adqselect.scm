(define-module (crates-io ad qs adqselect) #:use-module (crates-io))

(define-public crate-adqselect-0.1.0 (c (n "adqselect") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "floydrivest") (r "^0.2.2") (d #t) (k 2)) (d (n "kth") (r "^0.1.0") (d #t) (k 2)) (d (n "order-stat") (r "^0.1.3") (d #t) (k 2)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "09vg6y2h4rvk84bgpgyw7iwbnqdc6q5csb9s3j8lk2hw8wm224b9")))

(define-public crate-adqselect-0.1.1 (c (n "adqselect") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "floydrivest") (r "^0.2.2") (d #t) (k 2)) (d (n "kth") (r "^0.1.0") (d #t) (k 2)) (d (n "order-stat") (r "^0.1.3") (d #t) (k 2)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0pvyy1qghbdjc33l606m9rfgnk2c21n58xq22cadzfxzshfyxz52")))

(define-public crate-adqselect-0.1.2 (c (n "adqselect") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "floydrivest") (r "^0.2.2") (d #t) (k 2)) (d (n "kth") (r "^0.1.0") (d #t) (k 2)) (d (n "order-stat") (r "^0.1.3") (d #t) (k 2)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1vbc9k0hb0w1ncgbmfd42ip27kg7wn8cq98d10sv4vn9lcdia9cx")))

(define-public crate-adqselect-0.1.3 (c (n "adqselect") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "floydrivest") (r "^0.2.2") (d #t) (k 2)) (d (n "kth") (r "^0.1.0") (d #t) (k 2)) (d (n "order-stat") (r "^0.1.3") (d #t) (k 2)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1w7ymzb2nv757b0pa2j3zc5qs1cjszzanlpa5j7w9njycbd2vvi4")))

