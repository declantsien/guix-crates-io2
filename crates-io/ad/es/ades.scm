(define-module (crates-io ad es ades) #:use-module (crates-io))

(define-public crate-ades-0.1.0 (c (n "ades") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "0c6rc3mv35niivnr16hmg6fg3wwwjsasl66rxpx78vy85m6sf0d4")))

(define-public crate-ades-0.1.1 (c (n "ades") (v "0.1.1") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "1zyxxrrcq5bnvjna3n3x7mxg0q26npcfwmn563w771zlxhixjlk9")))

(define-public crate-ades-0.1.2 (c (n "ades") (v "0.1.2") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "0knn39zrpbx2f0drlarwk8pxhzmshgm6cvdrmfzwhg9nm07v95dk")))

(define-public crate-ades-0.1.3 (c (n "ades") (v "0.1.3") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "05b1610akh61r3ill9b5vixabyvzsan2vxgm149lk9davkx78f0d")))

(define-public crate-ades-0.1.4 (c (n "ades") (v "0.1.4") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "0hdvi8r1b8wvaks2qnwfdx9wp2j9lrdjjdsv3iccb2vhpd99x9nh")))

(define-public crate-ades-0.1.5 (c (n "ades") (v "0.1.5") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "~3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "07m7j0g6vgrg7y37h3ank2b5vq32f8ba9mmaw05vgxz1ajd0zgfd")))

(define-public crate-ades-0.1.6 (c (n "ades") (v "0.1.6") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "~3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "15z2p59g6pprhq4mn8jvg39p16hm95w7q3kq7nnjqcjy5bjfhibz")))

(define-public crate-ades-0.1.7 (c (n "ades") (v "0.1.7") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "0cw1cm3nqkg8q7rglqf3bbfyzglyg8dqk7iry844nqdj0ii8b79r")))

(define-public crate-ades-0.1.8 (c (n "ades") (v "0.1.8") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "0y03skaysryvj9xlc3hq5z862756mq9a2jrv51biv8p6swy0wzbz")))

(define-public crate-ades-0.1.9 (c (n "ades") (v "0.1.9") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "1naa1blq9ln4v0pc8ypx0fsfzg2j0pd0vj6wrhbv2rmf57g75kgj") (y #t)))

(define-public crate-ades-0.1.10 (c (n "ades") (v "0.1.10") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "1wmh372c64i5zayxgxqwk2qmh7jqfp3dimjddckk62rzinmck92l") (y #t)))

