(define-module (crates-io ad ja adjacency-list) #:use-module (crates-io))

(define-public crate-adjacency-list-0.0.0 (c (n "adjacency-list") (v "0.0.0") (d (list (d (n "graph-types") (r "0.0.*") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0w909ari86sbwi1nr1qidqc4h7p1wzcqihjzs06c984gx3h81rz9") (f (quote (("wolfram" "graph-types/wolfram_wxf") ("default"))))))

(define-public crate-adjacency-list-0.0.1 (c (n "adjacency-list") (v "0.0.1") (d (list (d (n "graph-types") (r "0.0.*") (d #t) (k 0)))) (h "0icjs9dx2h0jyfs20wv80qrcfwa3ajkk99qsp50y8m0p1ybr9bf6") (f (quote (("wolfram" "graph-types/wolfram_wxf") ("default"))))))

(define-public crate-adjacency-list-0.0.2 (c (n "adjacency-list") (v "0.0.2") (d (list (d (n "graph-types") (r "^0.0.12") (d #t) (k 0)))) (h "09jb0dvz55izvdpc1aqwfmml3bbkxxrga15vja9l45kcyj9vkajm") (f (quote (("wolfram" "graph-types/wolfram_wxf") ("default"))))))

