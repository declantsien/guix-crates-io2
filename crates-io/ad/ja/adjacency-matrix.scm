(define-module (crates-io ad ja adjacency-matrix) #:use-module (crates-io))

(define-public crate-adjacency-matrix-0.0.0 (c (n "adjacency-matrix") (v "0.0.0") (d (list (d (n "graph-types") (r "0.0.*") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0973vff90a1mfz5xvz4qpp9wsjv59xmfiqcyicmasf3a19qzgxg5") (f (quote (("default"))))))

(define-public crate-adjacency-matrix-0.0.1 (c (n "adjacency-matrix") (v "0.0.1") (d (list (d (n "graph-types") (r "^0.0.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "09w4kljaa8arcyqkamp75vkn09wbyzpgc5jdxwbwkpsc7nfw1wls") (f (quote (("wolfram" "graph-types/wolfram_wxf") ("default"))))))

