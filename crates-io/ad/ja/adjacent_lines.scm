(define-module (crates-io ad ja adjacent_lines) #:use-module (crates-io))

(define-public crate-adjacent_lines-0.1.0 (c (n "adjacent_lines") (v "0.1.0") (d (list (d (n "iterslide") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.6.1") (d #t) (k 0)))) (h "17kbq42q58c30fkjhcadsdp8w9ry5p84gd230p9s1ij95k0kb17w")))

