(define-module (crates-io ad ja adjacent-pair-iterator) #:use-module (crates-io))

(define-public crate-adjacent-pair-iterator-0.1.0 (c (n "adjacent-pair-iterator") (v "0.1.0") (h "0xi48krajymcn0p71ymrswqk8cvyikc5aasn67ciwvcfszsdk5ak")))

(define-public crate-adjacent-pair-iterator-0.1.1 (c (n "adjacent-pair-iterator") (v "0.1.1") (h "1pi24zlrphzn321ahhvyf3zv2f9ik4yclfyzrrcs6qhv779jgymz")))

(define-public crate-adjacent-pair-iterator-0.1.2 (c (n "adjacent-pair-iterator") (v "0.1.2") (h "10pd54phnddlkp72903djlvs716mhwmrg60zvs8jdrhm1l7ar6f1")))

(define-public crate-adjacent-pair-iterator-1.0.0 (c (n "adjacent-pair-iterator") (v "1.0.0") (h "10r8fm2kr4pr116kfcsaym37r3z7mlvzs1x6a8lv68s8dwccv4r0") (r "1.31")))

