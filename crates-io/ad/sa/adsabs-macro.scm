(define-module (crates-io ad sa adsabs-macro) #:use-module (crates-io))

(define-public crate-adsabs-macro-0.1.0 (c (n "adsabs-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02zjr5jcwr4zj37vczn6mh9yymxl7ing8v8gf6pzbp437gglkzm8")))

(define-public crate-adsabs-macro-0.1.1 (c (n "adsabs-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02j9j3fxn3z5clx9xycvdb7nrs345fjzzf1hbvw2ay3r92428rjf")))

