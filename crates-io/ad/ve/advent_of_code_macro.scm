(define-module (crates-io ad ve advent_of_code_macro) #:use-module (crates-io))

(define-public crate-advent_of_code_macro-0.1.0 (c (n "advent_of_code_macro") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "10ay3bg77sk2kvl5367hp7vdag4hia2msbal10wm0rv3bgy7g0b6")))

(define-public crate-advent_of_code_macro-0.1.1 (c (n "advent_of_code_macro") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "0l5l5c2ryfhkzi8lrxq57fcgyg7b52fzgbag6km2ylznl3qw5ym5")))

(define-public crate-advent_of_code_macro-0.1.2 (c (n "advent_of_code_macro") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "005b7s6ai7vmczxmraqd7zpii2s91zlsv5zbmpb8qhbz9v9pxf2j")))

