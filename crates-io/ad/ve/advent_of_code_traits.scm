(define-module (crates-io ad ve advent_of_code_traits) #:use-module (crates-io))

(define-public crate-advent_of_code_traits-0.1.0 (c (n "advent_of_code_traits") (v "0.1.0") (h "17val99cc4ka0k8sqblbm7zgi77335vm6nixlrb5pvs0fz8k0y52")))

(define-public crate-advent_of_code_traits-0.1.1 (c (n "advent_of_code_traits") (v "0.1.1") (h "0yg2m36rzr9md65ik2wqs0j00kpszzxgbb5q91idayi079jsswwr")))

(define-public crate-advent_of_code_traits-0.2.0 (c (n "advent_of_code_traits") (v "0.2.0") (h "1lqzyip4r79fhqp0rjfxiyp8bsh39z89kzf6frwr2265jbnx8dpy")))

