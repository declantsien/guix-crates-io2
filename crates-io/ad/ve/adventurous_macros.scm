(define-module (crates-io ad ve adventurous_macros) #:use-module (crates-io))

(define-public crate-adventurous_macros-0.3.0 (c (n "adventurous_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1rkd2p85gkfvawr1myb0q71isr07cpfl94qfiw9kgsdx1md0lyss")))

