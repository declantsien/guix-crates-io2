(define-module (crates-io ad ve adversarial_search) #:use-module (crates-io))

(define-public crate-adversarial_search-0.1.0 (c (n "adversarial_search") (v "0.1.0") (h "1s4l91lrcx490qmkghs8h592h9hq0n9zm4hb37mm4ph0pl61m40i")))

(define-public crate-adversarial_search-0.1.1 (c (n "adversarial_search") (v "0.1.1") (h "002jdkvcrxyjr1f80nvgchds94chr0d14g64b98j96v1xmf25drz")))

