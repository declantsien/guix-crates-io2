(define-module (crates-io ad ve adventurous) #:use-module (crates-io))

(define-public crate-adventurous-0.0.1 (c (n "adventurous") (v "0.0.1") (h "0bn1x5b3amgg0cfbf13scgg4x7l6x5aqxr8cnxq43s2syr4q7n6d")))

(define-public crate-adventurous-0.0.2 (c (n "adventurous") (v "0.0.2") (h "0l9n5ww6ryg0q7n8r4y2kb5nw5jlvlafk08nd342wlj2fq9lqfhm")))

(define-public crate-adventurous-0.0.3 (c (n "adventurous") (v "0.0.3") (h "1g6n448n1810rrfpbvnvj6kwm4i5pl7kwlwnwj3vn294lqwaxzd7")))

(define-public crate-adventurous-0.1.0 (c (n "adventurous") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1zc8xi0xyc3ykxgm7c2s3irqj23gzv094761kz9417s9v3hl0x5g")))

(define-public crate-adventurous-0.2.0 (c (n "adventurous") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "0babkajiki9n2zfygfyg18b70bq39jpijyip5bzsh43bp3rqln9a")))

(define-public crate-adventurous-0.3.0 (c (n "adventurous") (v "0.3.0") (d (list (d (n "adventurous_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "0srkac3ffb2wbszln03cafqh0bshdyif9yjff6iiamc5w5p8666q")))

