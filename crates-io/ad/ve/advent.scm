(define-module (crates-io ad ve advent) #:use-module (crates-io))

(define-public crate-advent-0.1.0 (c (n "advent") (v "0.1.0") (h "172psqsvhi0iz17hi322cqw35dd11izgnxzvxnca1kmzql4n7hnl")))

(define-public crate-advent-0.1.1 (c (n "advent") (v "0.1.1") (h "0r1k4lmvdi5pzwg2pn0ixhh9g04pzgrpbfykyh3mxlm122zzmz82")))

(define-public crate-advent-0.2.0 (c (n "advent") (v "0.2.0") (h "1w26532phj8f0vsczv6iqv8mb3mhpjw9f7pd9b0k1lhd61wx2p20")))

(define-public crate-advent-0.2.2 (c (n "advent") (v "0.2.2") (h "0r7m4ya07499iyq54fwn3y87l75jck8z92yas7xryv532afv84a1")))

