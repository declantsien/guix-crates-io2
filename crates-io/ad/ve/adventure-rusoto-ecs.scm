(define-module (crates-io ad ve adventure-rusoto-ecs) #:use-module (crates-io))

(define-public crate-adventure-rusoto-ecs-0.1.0 (c (n "adventure-rusoto-ecs") (v "0.1.0") (d (list (d (n "adventure") (r "^0.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.37") (d #t) (k 0)) (d (n "rusoto_ecs") (r "^0.37") (d #t) (k 0)))) (h "0wllj6ral0h8znw01j4751plj5qjpyni3zj8jwkr9n2rg26gnb7f")))

(define-public crate-adventure-rusoto-ecs-0.2.0 (c (n "adventure-rusoto-ecs") (v "0.2.0") (d (list (d (n "adventure") (r "^0.3.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.37") (d #t) (k 0)) (d (n "rusoto_ecs") (r "^0.37") (d #t) (k 0)))) (h "1i4ac33488cwp8262z1bcmmf0lxqk3ngrhh8dv5fnvjxklcg5nap")))

(define-public crate-adventure-rusoto-ecs-0.3.0 (c (n "adventure-rusoto-ecs") (v "0.3.0") (d (list (d (n "adventure") (r "^0.4.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.40") (k 0)) (d (n "rusoto_ecs") (r "^0.40") (k 0)))) (h "0xf349wgxr1d4ss2fcz717hpb7j3fz4937ydkvv6hh0km97xrfyn") (f (quote (("rustls" "rusoto_core/rustls" "rusoto_ecs/rustls") ("native-tls" "rusoto_core/native-tls" "rusoto_ecs/native-tls") ("default" "native-tls"))))))

(define-public crate-adventure-rusoto-ecs-0.4.0 (c (n "adventure-rusoto-ecs") (v "0.4.0") (d (list (d (n "adventure") (r "^0.5.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.42.0") (k 0)) (d (n "rusoto_ecs") (r "^0.42.0") (k 0)))) (h "0ap41gjnqgzwzbivgv0mzcaf898hyqsy38gpgs7npvk0k2ab7mx6") (f (quote (("rustls" "rusoto_core/rustls" "rusoto_ecs/rustls") ("native-tls" "rusoto_core/native-tls" "rusoto_ecs/native-tls") ("default" "native-tls"))))))

