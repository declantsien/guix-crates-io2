(define-module (crates-io ad ve adventage-macros) #:use-module (crates-io))

(define-public crate-adventage-macros-0.1.0 (c (n "adventage-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1lhmm7mnaavh04c46zvrq8h8m5n807947pdc0jxhcvq1f33hqxq6")))

(define-public crate-adventage-macros-0.2.0 (c (n "adventage-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0xwwnvkyg45x966nhsb8jb9sb8q30wyp1gw8j2998pz0jxg19bcq")))

(define-public crate-adventage-macros-0.2.1 (c (n "adventage-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "17zrdy7bkcazw2cxkp5ssngl8zrpy1q0w8mjvdl9xn6hz7asns0x")))

