(define-module (crates-io ad ve adventage) #:use-module (crates-io))

(define-public crate-adventage-0.1.0 (c (n "adventage") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "07vjwr0hwkp8hv92lwfsaff2ajg2bp8sw3qcnpxp0xbc512jgjw6")))

(define-public crate-adventage-0.2.0 (c (n "adventage") (v "0.2.0") (d (list (d (n "adventage-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0067kxcjznkb3764q7d5g6wffjw2wgf1cpd7kpx4ikihzn91kfs4")))

(define-public crate-adventage-0.2.1 (c (n "adventage") (v "0.2.1") (d (list (d (n "adventage-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0fgdf29mhfbdifncadwn97dl2i4sjv3i8yzlpdfrlk9g3sh25x10")))

