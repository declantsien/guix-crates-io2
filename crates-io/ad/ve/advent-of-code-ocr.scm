(define-module (crates-io ad ve advent-of-code-ocr) #:use-module (crates-io))

(define-public crate-advent-of-code-ocr-0.1.0 (c (n "advent-of-code-ocr") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (k 0)))) (h "1k0dlv7lxl3zb52r9nr6h50drgx48xxv39q606fqmg7aijh2qa5p")))

(define-public crate-advent-of-code-ocr-0.1.1 (c (n "advent-of-code-ocr") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (k 0)))) (h "0miwxll8nnnpas2w4n5a95l6nai0vhl2v0g2dyhdd2iz48ycy8dl")))

