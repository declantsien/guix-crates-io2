(define-module (crates-io ad ve adventure-rusoto-sns) #:use-module (crates-io))

(define-public crate-adventure-rusoto-sns-0.3.0 (c (n "adventure-rusoto-sns") (v "0.3.0") (d (list (d (n "adventure") (r "^0.4.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.40") (k 0)) (d (n "rusoto_sns") (r "^0.40") (k 0)))) (h "0qh7vc9f2ql6klg6gx021369apva8rp0nm3v1y7imnm43ir4n4q4") (f (quote (("rustls" "rusoto_core/rustls" "rusoto_sns/rustls") ("native-tls" "rusoto_core/native-tls" "rusoto_sns/native-tls") ("default" "native-tls"))))))

(define-public crate-adventure-rusoto-sns-0.4.0 (c (n "adventure-rusoto-sns") (v "0.4.0") (d (list (d (n "adventure") (r "^0.5.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.42.0") (k 0)) (d (n "rusoto_sns") (r "^0.42.0") (k 0)))) (h "0knjnqk67yh0j8nyv9da8x06l7av0z96in55n7d1fhhkjkp5vzli") (f (quote (("rustls" "rusoto_core/rustls" "rusoto_sns/rustls") ("native-tls" "rusoto_core/native-tls" "rusoto_sns/native-tls") ("default" "native-tls"))))))

