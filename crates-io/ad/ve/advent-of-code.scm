(define-module (crates-io ad ve advent-of-code) #:use-module (crates-io))

(define-public crate-advent-of-code-2019.12.44 (c (n "advent-of-code") (v "2019.12.44") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "03194y87k228bzkdbwj9cbpg9wgsqqbvll1zli9vfi5mmbm0aib3")))

(define-public crate-advent-of-code-2019.12.46 (c (n "advent-of-code") (v "2019.12.46") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "13zsdkx3lp0346sipibfn3y4nibn1589mbp9y8kg3shkbk8jcqk4")))

(define-public crate-advent-of-code-2019.12.47 (c (n "advent-of-code") (v "2019.12.47") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1bi2y8mpbz7pmwp4dn2rs0mlqf3ajsdlnq41vq6vy6344d736ql6")))

(define-public crate-advent-of-code-2019.12.48 (c (n "advent-of-code") (v "2019.12.48") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1pva8rzxb791jvsiijal7nckkl7rch9wixyhxpd1imalph488sgr")))

(define-public crate-advent-of-code-2019.12.49 (c (n "advent-of-code") (v "2019.12.49") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1vcfksn20fyxvw6kry9z6x55m3nn7im9pqd2dwq3lcsbczwfblx7")))

(define-public crate-advent-of-code-2019.12.50 (c (n "advent-of-code") (v "2019.12.50") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0qyalzl5mklq2gvlzv2v8h8frcnj8hncdlxa4lsvhhhj205bp95f")))

(define-public crate-advent-of-code-2019.12.51 (c (n "advent-of-code") (v "2019.12.51") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0nlgl8xibh49pi326kblirmm5sam9h8j4dgvc5skzsksmlwhyl7c")))

(define-public crate-advent-of-code-2019.12.52 (c (n "advent-of-code") (v "2019.12.52") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1rjdd9sgc339pfgbdn1lmz1sr08mhjqrvris00bsavnrpcl1dnkz")))

(define-public crate-advent-of-code-2019.12.53 (c (n "advent-of-code") (v "2019.12.53") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1w7b4mp74imn72nzhaja4yshvk4dcfg5vfs0yrncxrg5r0p5l1v6")))

(define-public crate-advent-of-code-2019.12.54 (c (n "advent-of-code") (v "2019.12.54") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1i6mfvmrj5qskinfvf8wj34b5gd0n55k9m6dhgnj42kvjfcg925a")))

(define-public crate-advent-of-code-2019.12.55 (c (n "advent-of-code") (v "2019.12.55") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "12v902ljc0qs2wgyx1f3a9jplv5q7xcvwiiwl3f6n14jg1xhw112")))

(define-public crate-advent-of-code-2019.12.56 (c (n "advent-of-code") (v "2019.12.56") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "00wnivdivv9il3m9xx07nwhqyfsrsxrnh7y9lcprslkz2b40ki1p")))

(define-public crate-advent-of-code-2019.12.57 (c (n "advent-of-code") (v "2019.12.57") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0vdks6a27j5ms475qhai95i69c3fl6d57wxkykz18xn61z120jrv")))

(define-public crate-advent-of-code-2019.12.58 (c (n "advent-of-code") (v "2019.12.58") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1i4a1w4fmmh4d6cbvvm4agbax1pv62v9k23ckm1hpah3b4f7dild")))

(define-public crate-advent-of-code-2019.12.59 (c (n "advent-of-code") (v "2019.12.59") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "01gkqnjbpl6nlmhf11w8c8n36dg21v6f3qnqg56z7vvjs0gvzq4m")))

(define-public crate-advent-of-code-2019.12.60 (c (n "advent-of-code") (v "2019.12.60") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0g447skg2h6bpfcdxxj4k3f63469d1fqb6plvhm3h34qv4y51mdf")))

(define-public crate-advent-of-code-2019.12.61 (c (n "advent-of-code") (v "2019.12.61") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "19hby2fvms7n2p4hs0p4a5yvwjbwmd1rq1ar1bd1m8hrnl6k0f3z")))

(define-public crate-advent-of-code-2019.12.62 (c (n "advent-of-code") (v "2019.12.62") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "04n9nq7h89sm26a4mh9y7pmsjq6sx5nsb77mv54y9bs1w50ba7v2")))

(define-public crate-advent-of-code-2019.12.63 (c (n "advent-of-code") (v "2019.12.63") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1c182d6ji3p7y047rnnbzbzd06fy8p6682nhzwzk8l216x0igz0w")))

(define-public crate-advent-of-code-2019.12.64 (c (n "advent-of-code") (v "2019.12.64") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0c8gvidgy3p4zb8d7l79m1vpfh295nhczr4kbipmh5i9bgx9cnzp")))

(define-public crate-advent-of-code-2019.12.65 (c (n "advent-of-code") (v "2019.12.65") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "039xq1j574b8fxhg232g0bb8nxq44shsg6p5ab0sq6rgk86dygv8")))

(define-public crate-advent-of-code-2019.12.66 (c (n "advent-of-code") (v "2019.12.66") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "16pdn4kgsg4bivwfj9cxfkn5fap1zdgdh3baxngj1gh1qq1wsa2f")))

(define-public crate-advent-of-code-2019.12.67 (c (n "advent-of-code") (v "2019.12.67") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0vw8qgbln169bs2lqhr0acyd169r6mmzhq5j23wh9iixlj3aipxn")))

(define-public crate-advent-of-code-2019.12.68 (c (n "advent-of-code") (v "2019.12.68") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1hrz883w6a8a2yw16sdnr663njal7yy635b861g1sx3pzi7c4vbq")))

(define-public crate-advent-of-code-2019.12.69 (c (n "advent-of-code") (v "2019.12.69") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0x2iksa896rcyj6im2b266ij2ihwh8f4frpn2php1m4mbgaxzp7i")))

(define-public crate-advent-of-code-2019.12.70 (c (n "advent-of-code") (v "2019.12.70") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1rcizwshsvbzmqj8v6x5kazf4h550il74yvx6jwpnkw02x1m5dqv")))

(define-public crate-advent-of-code-2019.12.71 (c (n "advent-of-code") (v "2019.12.71") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "00fwrpw5fgl8r4y3g8qkg2m480mjnyc0i5fmdkrrl1an3swhnrbb")))

(define-public crate-advent-of-code-2019.12.72 (c (n "advent-of-code") (v "2019.12.72") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1rg7pz5hiakahyvr7v8b8q79v6dqxgffgz2ca8slp387iyrpby34")))

(define-public crate-advent-of-code-2019.12.73 (c (n "advent-of-code") (v "2019.12.73") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1cw1mmcaq40pli5jfvbzyqlgadv9kjgrya8k2z4j1s5vw6kqy7ya")))

(define-public crate-advent-of-code-2019.12.74 (c (n "advent-of-code") (v "2019.12.74") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1iqs2ckg5a06ajipq46z3na6wr9iibjfnrz5qy49g9ja14r9pxi3")))

(define-public crate-advent-of-code-2019.12.75 (c (n "advent-of-code") (v "2019.12.75") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "05kq3pyywmcz464crv19w8krcx61mxh6ca5bn0mvi15s69zcakf7")))

(define-public crate-advent-of-code-2019.12.76 (c (n "advent-of-code") (v "2019.12.76") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1kf523bg3gq63wkvvvad83qp6p0sfhk6935wkcpxsf31h4df99ix")))

(define-public crate-advent-of-code-2019.12.77 (c (n "advent-of-code") (v "2019.12.77") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1fa0ya1pcyz23zswapyapq88fjm5f9j9c66fq8v68q3i507h2zq2")))

(define-public crate-advent-of-code-2019.12.78 (c (n "advent-of-code") (v "2019.12.78") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0hlmqhmp5js16qwggqpqn6yj3llbrh8hziwhbv8z891g5xbl103y")))

(define-public crate-advent-of-code-2019.12.79 (c (n "advent-of-code") (v "2019.12.79") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1q0msdba8y7k0g7b8as4cnd7yw7f4r82pdmlspqjmv6v7g6r6iq1")))

(define-public crate-advent-of-code-2019.12.80 (c (n "advent-of-code") (v "2019.12.80") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "066h0iwwxk4yf0yyfz5hsgk9awi8gdywc85jp317xyrz21khw4h7")))

(define-public crate-advent-of-code-2019.12.81 (c (n "advent-of-code") (v "2019.12.81") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0lqbxyc07z4k101diwf9rlr7xzwa8jssx8xds9z22sbwmfmw0hry")))

(define-public crate-advent-of-code-2019.12.82 (c (n "advent-of-code") (v "2019.12.82") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "16ilgc68wr7anjjv0ab0jmh6nxxjasvm7kdcz54ws8430p6qhmmv")))

(define-public crate-advent-of-code-2019.12.83 (c (n "advent-of-code") (v "2019.12.83") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1v1bjbcchl8mlbyf5y25bvw2ycpap6x5j4fckvl6r8y57mqcfnr2")))

(define-public crate-advent-of-code-2019.12.84 (c (n "advent-of-code") (v "2019.12.84") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "112f56gsn6xd9glg0qrb41mmz6ampm0n0raiqrdchf232q3c7ni5")))

(define-public crate-advent-of-code-2019.12.85 (c (n "advent-of-code") (v "2019.12.85") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "122kmcx5qxn4zfrrb8hamiw5rdmla1240b2ls2jw73nmg0jyahfm")))

(define-public crate-advent-of-code-2019.12.86 (c (n "advent-of-code") (v "2019.12.86") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0ddqzskfx2627ds2nfmvdmd3p392il26mv76i95gj6vhr21nw5wc")))

(define-public crate-advent-of-code-2019.12.87 (c (n "advent-of-code") (v "2019.12.87") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1vx8kilh0i791z2wc70a1d8wqi0lym1rihyvkrq11hcpa5i65zka")))

(define-public crate-advent-of-code-2019.12.88 (c (n "advent-of-code") (v "2019.12.88") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0zwrkk0wln37gamzx7sih54i9nx3cn1v4jqf578kpg7gvmxkkzcj")))

(define-public crate-advent-of-code-2019.12.89 (c (n "advent-of-code") (v "2019.12.89") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1qms83rdbq246gwhn3d61ww8c5qh97q5i2mbv532ga61zl8cpqp6")))

(define-public crate-advent-of-code-2019.12.91 (c (n "advent-of-code") (v "2019.12.91") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "07qjq3zc5bdmc2yh7i1iv1iax337ch697jlg2ambkr2s72jk84ma")))

(define-public crate-advent-of-code-2019.12.92 (c (n "advent-of-code") (v "2019.12.92") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1pjw9fiaq48a3p5aafhbaajrvg1003zlzndf6r70r01y7j8ycv85")))

(define-public crate-advent-of-code-2019.12.93 (c (n "advent-of-code") (v "2019.12.93") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "16y18ic3an0q4cvr8dnlhz8arcjrb4ib1f1hi24z9z6rl73hmzgc")))

(define-public crate-advent-of-code-2019.12.94 (c (n "advent-of-code") (v "2019.12.94") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "0d9qjb6fibay5c924cq2ci5h165l1pmgzyxr555grg6bcw3la292")))

(define-public crate-advent-of-code-2019.12.95 (c (n "advent-of-code") (v "2019.12.95") (d (list (d (n "bytecount") (r "0.*") (d #t) (k 0)) (d (n "mod_exp") (r "1.*") (d #t) (k 0)))) (h "1zm9n7vrdwqfaij2yrxaqli8qxypq48zg3v19pnbcwg3f6rrslxc")))

(define-public crate-advent-of-code-2019.12.96 (c (n "advent-of-code") (v "2019.12.96") (h "0smlkr6ynx29wc5ibzjglfmy486z4cqz3d5pgxqfy7ysvs4hjrhs")))

(define-public crate-advent-of-code-2019.12.97 (c (n "advent-of-code") (v "2019.12.97") (h "0li1zxviiwz6g0ncfshmc07g368grv7lcfdsc5j9qjzdd16wdlh3")))

(define-public crate-advent-of-code-2019.12.98 (c (n "advent-of-code") (v "2019.12.98") (h "05ij0fp4g62kw1b1y4b9hbd7b7scds5bszq68khdpfwbbkvh11z7")))

(define-public crate-advent-of-code-2019.12.99 (c (n "advent-of-code") (v "2019.12.99") (h "0jwmmx1jmyn5lyfd93vncr1h3j4y3wrps8ksn7fxjipjrf24ini2")))

(define-public crate-advent-of-code-2019.12.100 (c (n "advent-of-code") (v "2019.12.100") (h "13lczpi3wzaj2nr9f93y55j4rwlxq2lnmpgjkg99012kfagrbxhm")))

(define-public crate-advent-of-code-2019.12.101 (c (n "advent-of-code") (v "2019.12.101") (h "1ydz9y1c7hq94zipjp8gbzyx7w0ag835bqmrgwx8xymarqxy2xwl")))

(define-public crate-advent-of-code-2019.12.102 (c (n "advent-of-code") (v "2019.12.102") (h "0plih356p4zfyyld58cdwh2sivj5nf158plf0bdrbjyka8hadrzd")))

(define-public crate-advent-of-code-2019.12.103 (c (n "advent-of-code") (v "2019.12.103") (h "0q834xdy6yd8s2xnqgj1h9x38igc6c71fyvlbph59bpz0h8lgnbw")))

(define-public crate-advent-of-code-2019.12.104 (c (n "advent-of-code") (v "2019.12.104") (h "0a712lx7djnm7y6vwdfvf137wivr0b166q81gbv5g5fh3vr6lb94")))

(define-public crate-advent-of-code-2019.12.105 (c (n "advent-of-code") (v "2019.12.105") (h "0s2ckqxxhrprx1p6agvnkz30bl41pxlqp4ps7lxxv1qgw76icg79")))

(define-public crate-advent-of-code-2019.12.106 (c (n "advent-of-code") (v "2019.12.106") (h "1kckr5fq6wwzsa89xfnimbb60krysk1j0l38pv8y3l5q25wwsbcs")))

(define-public crate-advent-of-code-2019.12.107 (c (n "advent-of-code") (v "2019.12.107") (h "14ija5hbhp07cxi74xm388mrdvwxxnms3pzr9hw7p32dq5qp3br4")))

(define-public crate-advent-of-code-2019.12.108 (c (n "advent-of-code") (v "2019.12.108") (h "18ba5liczb11qd33461src68vw9s5cygpnkmspd0rpxngy14i9r5")))

(define-public crate-advent-of-code-2019.12.109 (c (n "advent-of-code") (v "2019.12.109") (h "196hiv19kjn1wddhvj0061as4f8ncv843na69mdfg683i18db9gl")))

(define-public crate-advent-of-code-2019.12.110 (c (n "advent-of-code") (v "2019.12.110") (h "0p4q06nf6410zb1fj7zjv15vg7cahnmw1159n1dill1mqw2j30x5")))

(define-public crate-advent-of-code-2019.12.111 (c (n "advent-of-code") (v "2019.12.111") (h "0jnkqcih1jywas0h0kdjhga6qi7mzbblhrzif99vw0r0n6fvmcgz") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.112 (c (n "advent-of-code") (v "2019.12.112") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "17r9xih0i73wpwh1m41yzfvphs1yjg7rjyfl3yz8w5glv623kpin") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.113 (c (n "advent-of-code") (v "2019.12.113") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "17982cgzrdnml665b6ibdv1c6h4fh4y5id7ywsp4raggk0brkgj9") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.114 (c (n "advent-of-code") (v "2019.12.114") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1f3s94vsvfq1bdwqdzkaxrz13x80rdgyd7l67g4g5yah55bm8lic") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.115 (c (n "advent-of-code") (v "2019.12.115") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0lpbvhry9n4gsqvc5yqscw31vsxj97zgqnyii86yhmdn5fgaqhjj") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.116 (c (n "advent-of-code") (v "2019.12.116") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1ak88ffbgr8ysmij220bvbw7774j627lsgz4rnpslnmmbha3xs5p") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.117 (c (n "advent-of-code") (v "2019.12.117") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1rp7q8kii15bxl7rvm5x1ng033mzqgm2bih5a4q53b4a8p62cj9v") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.118 (c (n "advent-of-code") (v "2019.12.118") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "085fm6xczy3hg5jss8l5jjc124ydfm4wiqchzw6w4kn1hzhzpp3y") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.119 (c (n "advent-of-code") (v "2019.12.119") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0y85m9i58c4fi50ng3x59gq49ps2j7r3jrp3hcj6v6rdyy8340mv") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.120 (c (n "advent-of-code") (v "2019.12.120") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1v75dw939kb7azhlv124r7rbxyvcsqpwhgn4xm73zbw7p023x7gp") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.121 (c (n "advent-of-code") (v "2019.12.121") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "04xl7dbnz34fjsjfhdl2hxcp8619jplysjrzxfxky3nvrynyxc3k") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.122 (c (n "advent-of-code") (v "2019.12.122") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "18k2rpv0pzlcii393fv5b79q92a0kd59p4x0xnr8400yndhsrw8r") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.123 (c (n "advent-of-code") (v "2019.12.123") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "17vml21ip6c94g37k9abpkmi6309n869cqgs0a78h0m3jx0ipijj") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.124 (c (n "advent-of-code") (v "2019.12.124") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0zmyg8s437rsl6ws6gas52y4k9xxd0y4mxnbggxx0bczkm30jdjq") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.125 (c (n "advent-of-code") (v "2019.12.125") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0xx67bpbnvb996mdd5qh12ynfla098vncbiik6cycciyfaf99jc4") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.126 (c (n "advent-of-code") (v "2019.12.126") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0108gsn386ca2d15dgpa1713h3b7c38hpb1jfqhrpr645wc4nwx3") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.127 (c (n "advent-of-code") (v "2019.12.127") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0r4pkxhjv54ypypv1kyp0j3vs701pq3s6ap09mdssnd24h1jnab3") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.128 (c (n "advent-of-code") (v "2019.12.128") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1iwxwvyp03cy1mpfqqvwz9pxxzhn6ha2qbfcpdy0njf9nf2wskpw") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.129 (c (n "advent-of-code") (v "2019.12.129") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1chkm9xs80rkqfxixdy01yg2yha6wjsswydff1ih9lx2hy5b3mjs") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.130 (c (n "advent-of-code") (v "2019.12.130") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0ikalj92bk7jbfxi49wz810xxj9zsgvlvg047n4qaw164gq1lzzf") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.131 (c (n "advent-of-code") (v "2019.12.131") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "099553a0aqwgqc9lgvag7sixpz0qw5rl70yhjlnh61ipgwlvlhby") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.132 (c (n "advent-of-code") (v "2019.12.132") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1pmivckzrfh7q0rz2g2dqvv1lwr827kbzw3jx8lmqdvf3m3lckbn") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.133 (c (n "advent-of-code") (v "2019.12.133") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0g21cb2ny8r5ba2kcwdsq3spr6zyamdmfg69qwqlhbv7a390qnmn") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.134 (c (n "advent-of-code") (v "2019.12.134") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0s0brcm5560bs791lx28l4ib1bxvpyb42ppsgrdhr4qr3pag7jmm") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.135 (c (n "advent-of-code") (v "2019.12.135") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0lrkwpnygsyvyjfrhmyl0kp39yinp7pvvh3q66y2gj35yjdckj19") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.136 (c (n "advent-of-code") (v "2019.12.136") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "03s0iz0aj39x6n2c4dz4niaps797ily2xsi7pjk8qlvh92cdvx0r") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.137 (c (n "advent-of-code") (v "2019.12.137") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0h6h6nhpfyyahkr22rbllyhs3m3hcj9qaxzmrv98rzp4w2dlp8bw") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.138 (c (n "advent-of-code") (v "2019.12.138") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1shq0p3dqh9js4yqv5hkzj3ndg1b90mv2sjb6qyp8ldyg6z066zi") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.139 (c (n "advent-of-code") (v "2019.12.139") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1qa9pd452m0b5g51h8400cga3qrr8wf1s08yja07cnhhncyz170s") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.140 (c (n "advent-of-code") (v "2019.12.140") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1xjkgldc65aj2gvmcyiam39rvj2dhhj6w39dmdkq0x9p5bx2q4q1") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.141 (c (n "advent-of-code") (v "2019.12.141") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0jr5fpm6khb2ig3yc24w69fh694bs6hadliwxvzgxnphpakl83lg") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.142 (c (n "advent-of-code") (v "2019.12.142") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0m7jqgjgd2vx5lljdaw6yjsf42gsnm92rwk33i7fhzwy5s7zrljl") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.143 (c (n "advent-of-code") (v "2019.12.143") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0h06j2b85hj39h338rizawqakh365xy1jh5sfks034vs8w431yal") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.144 (c (n "advent-of-code") (v "2019.12.144") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0nddpx9q1j9vxwc0drzchqnmsr9b15blsbk9jaayh79gfbqp5002") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.145 (c (n "advent-of-code") (v "2019.12.145") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0vv2d94wyrdp5h9m0qrzfazhiyz83g0lp6m47dxvqcksz7d7hdrs") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.146 (c (n "advent-of-code") (v "2019.12.146") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0a7v6x7101xnfhzdqap5rzghclm73rw8gqbqc872i0ykfr7brq7c") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.147 (c (n "advent-of-code") (v "2019.12.147") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1fpviqwkxjnxk3m9ysnafl4y1hw98sbmwl13rzwhq9wrknigsk7l") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.148 (c (n "advent-of-code") (v "2019.12.148") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0h9b6dcmybhyxr988szjbgmcwh7gkdab1621y0sf2gs10yshsxhk") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.149 (c (n "advent-of-code") (v "2019.12.149") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1g4gn8yz9jjq25327hbf4nvj6gmrpk6bma6622rmzrky4bpvl2n5") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.150 (c (n "advent-of-code") (v "2019.12.150") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0k9ygqywv8ag8fgjxp9xaz445x41kcbb7whb546kgd0cya255vjb") (f (quote (("debug-output"))))))

(define-public crate-advent-of-code-2019.12.155 (c (n "advent-of-code") (v "2019.12.155") (d (list (d (n "paste") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "1x22hx8mmkbc4nwnjgq7bc3zxxykr4hsfi3vynb1jaba2k18h2fq") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.156 (c (n "advent-of-code") (v "2019.12.156") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1k0049nrb7bcyhiih4xds08hz30rwlb7ac8d8wm1dyl0q0xwfhal") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.157 (c (n "advent-of-code") (v "2019.12.157") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1hsrlpcw71lzchfjwb669mdn0w7q8hm1rvqrjv6l4jpp614xi6nm") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.158 (c (n "advent-of-code") (v "2019.12.158") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0fkk4adlrn5qc4yhb0bbyyh8rqn8kprq3wmp9vr3d97xvz1lbd70") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.159 (c (n "advent-of-code") (v "2019.12.159") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "11hqddjalxi3sw7rn5dma33c14fg1cjpxv9f8jr3fzm6l0vsrxgi") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.160 (c (n "advent-of-code") (v "2019.12.160") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1gavqq6585f2rxcrwwvk92inrwy10lclqf8v9r3qrmpwhsm44q2g") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.161 (c (n "advent-of-code") (v "2019.12.161") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "03gbpx8gmc6a89lkgkxcnnlia9jbaza0k9dnx258lyq9rkdylikz") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.162 (c (n "advent-of-code") (v "2019.12.162") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0zj8i4yxf7n2339b1lfpqcxw7s43sr92dh4p9ljvbc17zvb3rwdp") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.163 (c (n "advent-of-code") (v "2019.12.163") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0x90gi6vgwmmx0wb72n152w9wp9ga4n3c5mxcxxw1cx3b35lwlpy") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.164 (c (n "advent-of-code") (v "2019.12.164") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1pw049qv2x9vrh6viidq0pw991jzfw2hf8ym9f4a1sjgbxcwp71g") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.165 (c (n "advent-of-code") (v "2019.12.165") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1fg42q99dycmjkhjq0ljy1zsi33wikrgd15xbmsjkilksxcy3lfy") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.166 (c (n "advent-of-code") (v "2019.12.166") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0qbr3xi5h73z031s23bi7n6m8sv273gp1xrzkl8j8xai2jrp3z4a") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.167 (c (n "advent-of-code") (v "2019.12.167") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0rs7h8skzpw5yjqsagcg2p7k90rk7asz6wphqkwsqrxhi17gw2lv") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.168 (c (n "advent-of-code") (v "2019.12.168") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "16v1crdcn5bxy1l2nizk7xnh6bbmdcmzj5srchjsb6qcyyfpffbj") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.169 (c (n "advent-of-code") (v "2019.12.169") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "10qag7jpvwpw7h8jrd0334pxps60yh5qpc01cz3k86dkq3czm0vj") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.170 (c (n "advent-of-code") (v "2019.12.170") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "08gz4dcsjd1pljs0pla7nk5mw9xkk76kwkqjpdwjbgchdpxsrz66") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.171 (c (n "advent-of-code") (v "2019.12.171") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0kpqfw28gk2zvi72bwlgadi8yn0cqpmjq9sp80pzihb09k7r09vm") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.172 (c (n "advent-of-code") (v "2019.12.172") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0msq5lpx9zjy1n8z48vxqnq903kfmd786wak72aya34s9i324a3m") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.173 (c (n "advent-of-code") (v "2019.12.173") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "01zma0lsnfyiw1n4swakrcy73i1y28cnsmb22hy65j0a7cpqk9a2") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.174 (c (n "advent-of-code") (v "2019.12.174") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0sq0p0l07pxfar0sf9l5y01dfi43ycinarsdlq7fnz87vlab1nh0") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.175 (c (n "advent-of-code") (v "2019.12.175") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0gdwzp5l1mqhs74pam9q7f5iz2aq4zjfbymp9z70ij351bc5chkx") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.176 (c (n "advent-of-code") (v "2019.12.176") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "09fcbgxbm62isxn4p4y4vabig426157pkynrg183wj0bfsmj6ray") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.177 (c (n "advent-of-code") (v "2019.12.177") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1l6c0p4mxj3hh2ixs70n8aakq685s5z6zvxj6d9a45gjp6fvryrw") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.178 (c (n "advent-of-code") (v "2019.12.178") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0saj8igddf7diwszfhjh094lsx5cbkhzif7fj4rzl1nqg0k7glnx") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.179 (c (n "advent-of-code") (v "2019.12.179") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "01iw758krsyd00rpdckna7wymij6pr5svghm4334pr75qq70nln9") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.180 (c (n "advent-of-code") (v "2019.12.180") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "082jfbmqlxk1yhviiahwwbnf7i1dhd700g0smzmv6m3z5i61738i") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.181 (c (n "advent-of-code") (v "2019.12.181") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0wxb2p295g54m5frdwlarikfcb53n7lf6pwma06g1wjs93dxb2k7") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.182 (c (n "advent-of-code") (v "2019.12.182") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "17jn1fmxjp9ihz2cbw8ddy39idp468ncm7p3gyhpp0lx46d1f7ha") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.183 (c (n "advent-of-code") (v "2019.12.183") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0c3xzmsqjcvsma6baq4z9a06l462yvkhjixadbwrg1i40dw3v69y") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.184 (c (n "advent-of-code") (v "2019.12.184") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "00jpfjg7j2bpszi49h81ckssidnascrksx14al91ha6g3y48sxmy") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.185 (c (n "advent-of-code") (v "2019.12.185") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1b7xggnhgz9brqzi6lj0g44lbnl2pz7bpgm195kg6xdbppzkzzgd") (f (quote (("visualization") ("debug-output"))))))

(define-public crate-advent-of-code-2019.12.186 (c (n "advent-of-code") (v "2019.12.186") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0dj64l254r2ql65cxp8jihmj15r5y6xd3vcbprhi9qshkmid880s") (f (quote (("visualization") ("debug-output") ("count-allocations" "backtrace"))))))

(define-public crate-advent-of-code-2019.12.187 (c (n "advent-of-code") (v "2019.12.187") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "12c7p2g33bhmsss7n59klq5c22lvps8c0iribz039an0c3rpbd1y") (f (quote (("visualization") ("debug-output") ("count-allocations" "backtrace"))))))

(define-public crate-advent-of-code-2019.12.188 (c (n "advent-of-code") (v "2019.12.188") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "04lh7ldp797dxpmsla0lk2gjwiwvni7cj6m4igydyani06p31jsn") (f (quote (("visualization") ("debug-output") ("count-allocations" "backtrace"))))))

(define-public crate-advent-of-code-2019.12.189 (c (n "advent-of-code") (v "2019.12.189") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0npp7x7hmp7d2b8y6zwvv6gq802gpi7dirc0ivnmqkmhgrxkkqnb") (f (quote (("visualization") ("debug-output") ("count-allocations" "backtrace"))))))

(define-public crate-advent-of-code-2019.12.190 (c (n "advent-of-code") (v "2019.12.190") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0jz5544qa8y2vw5h3y338fn53gccwbwq778cakdbxvjyj1x96sd5") (f (quote (("visualization") ("debug-output") ("count-allocations" "backtrace"))))))

(define-public crate-advent-of-code-2019.12.191 (c (n "advent-of-code") (v "2019.12.191") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "071ayyqmkvrnlbb2va2af2a4rc8n2gmdzbr9kxi0814pmskgvac1") (f (quote (("visualization") ("debug-output") ("count-allocations" "backtrace"))))))

(define-public crate-advent-of-code-2019.12.192 (c (n "advent-of-code") (v "2019.12.192") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0s85rdc9xd741vbpgg93jgyvyfnj1zjyqyldljvjy92d5n5mcl4w") (f (quote (("visualization") ("debug-output") ("count-allocations" "backtrace"))))))

(define-public crate-advent-of-code-2019.12.193 (c (n "advent-of-code") (v "2019.12.193") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1sh9l50idqav9wsa11iklrf35hb9w1nq6hsjvb6a7iracqf3ccvn") (f (quote (("visualization") ("debug-output") ("count-allocations" "backtrace"))))))

(define-public crate-advent-of-code-2019.12.194 (c (n "advent-of-code") (v "2019.12.194") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1rhn3i1qx9sh7k4rvcs5wd52vw0szvw6zp3ga9976rdaiw8y2lam") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.195 (c (n "advent-of-code") (v "2019.12.195") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0fwkl331022hb3zkw20lkbxrqcn3vzgpcyg7ivql4kdv5dvnrxbm") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.196 (c (n "advent-of-code") (v "2019.12.196") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1629bvfhwi60i47lzbpavqm122w2dcklvr82a9ccj14yzla4b7g2") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.197 (c (n "advent-of-code") (v "2019.12.197") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0f6j0hy61nfipp5k98syysk8s0a0n02g71b9xys1xms5nn0nnbdz") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.198 (c (n "advent-of-code") (v "2019.12.198") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0y3l4gpma62gfq2fl0jjx9gz44dy70wxgw9fjqipcdpd4r31vdx7") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.199 (c (n "advent-of-code") (v "2019.12.199") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1yqp8m94gjcvij74skw28hfmamqgypqx8ccb10ny7rrby48h4c1p") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.200 (c (n "advent-of-code") (v "2019.12.200") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0lpjnadn4ks9pflv6i8z25jns3h8aw4bim1swd65xw3kab7pmxja") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.201 (c (n "advent-of-code") (v "2019.12.201") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0vxj96mcbv0ibxhd4y96zclfggfrjp2gi1s3iysyqmdlmrgl6n5w") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.202 (c (n "advent-of-code") (v "2019.12.202") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0vh3ii8rfvf6b8qi5slxc3rfxn94dzhr3g61zdrphlnq7myrzngk") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.203 (c (n "advent-of-code") (v "2019.12.203") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0qfl50d75nca18siqx1jm6ndwmm0dqdywv43a3h3j87jdjd72i8f") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.204 (c (n "advent-of-code") (v "2019.12.204") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0l6pk6zxiwfhkhhn3dbjvc3pgl183973mcbg4az68j0pq45ypbzh") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.205 (c (n "advent-of-code") (v "2019.12.205") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1wmrw0larr9ckbcf754np15sz3gr0abr5p6yng0anvq85zd7rkj3") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.206 (c (n "advent-of-code") (v "2019.12.206") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0gljdb9f5p135lx1i0zarpv4fip8xswh9198dvy1dmaa6350d6aw") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.207 (c (n "advent-of-code") (v "2019.12.207") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "09drxa4c8pq87gbsx3algip0cmnmn142cgqcwzy8zgg4vp5rcdy0") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.208 (c (n "advent-of-code") (v "2019.12.208") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0hjpkv49ggisgpcv0pv9isd0820z58kgbypc3nnsfy7nnby1v2g6") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.209 (c (n "advent-of-code") (v "2019.12.209") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1aa355k0a2sra61ydi00yykpl2lliw9b3nzi10ryrpma8287z2nx") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.210 (c (n "advent-of-code") (v "2019.12.210") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1gcjxn1c63a2pmw39wcdbibg29maqlypz2560qc50qx5dm99xfvp") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.211 (c (n "advent-of-code") (v "2019.12.211") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "05qqdip06d3vzylckm8afvvjkp7x6vl5xfv77n2gravfxk879mq7") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.212 (c (n "advent-of-code") (v "2019.12.212") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1mlqdfkbv7hdcrm5hniciddllnm65czx5raqicpxs2lbmq7bdzj1") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.213 (c (n "advent-of-code") (v "2019.12.213") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0xc7y0kwlclfca7cvj0j0k9ppq6sgmgjg2mmjmsfnn26973zhjqj") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.214 (c (n "advent-of-code") (v "2019.12.214") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1cqj4hw0305hccf6chkcjr4xjzjvr1rk1pizl8lylijnxg334wi8") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.215 (c (n "advent-of-code") (v "2019.12.215") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "07axgp1bl8wckqahja10xbdylpg11bg24sm8l79i6xwfw6xqiyd1") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.216 (c (n "advent-of-code") (v "2019.12.216") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0y2fp9kbqj61142rvhdxr2mi423arc5749kazvi3x7zgji02aqf8") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.217 (c (n "advent-of-code") (v "2019.12.217") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1njvhw8aqp52392ac2rszzid6ii2k9vz9x43p6da6zb7kh7rkgk6") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.218 (c (n "advent-of-code") (v "2019.12.218") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1r9g562faq24i44l9rpl63nl08b67zdcz0l5vx99zy4zl664wcf1") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.219 (c (n "advent-of-code") (v "2019.12.219") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "036v7hh5zwr1hjvwvrj3xww25yi3rryj24mh8zxw91ykk9iagf16") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.220 (c (n "advent-of-code") (v "2019.12.220") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "16mlpa7bphgcbnympynvpg8gb28yzkrc00vw2k7wxb9kcaq2gfld") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.221 (c (n "advent-of-code") (v "2019.12.221") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "12jq7z3gg5s1blvrqpgh10xp90b0h6kx00jfakckns84fhm4gq7f") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.222 (c (n "advent-of-code") (v "2019.12.222") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "193r9jfhmx4lsc8qz64aaqnad3hphlkl2c4viy1736v4flnnzcdd") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.223 (c (n "advent-of-code") (v "2019.12.223") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1icjnxf2ixik6mxfsk6gf91gqsnp4qzil6g58q2nxrdmivmi1gmr") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.224 (c (n "advent-of-code") (v "2019.12.224") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0kw6sis1dyq1s2px8j4q9x07zydxchv7h5qkah2vc38nr212aicf") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.225 (c (n "advent-of-code") (v "2019.12.225") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0xf6xkjkai25ipim6pm14qd1cdnr7srr6hzvjy66p53insmk2yc8") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.226 (c (n "advent-of-code") (v "2019.12.226") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ga8sfy25wyhd2qvfhmwzk9hmqhmwfnzj2hj739k9ii3ylw1yd21") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.227 (c (n "advent-of-code") (v "2019.12.227") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0sylxxlxrgvmyzd68a9w053r2s0wq6kd9naqiq5ls5m6pw1f5926") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.228 (c (n "advent-of-code") (v "2019.12.228") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1lp4l34x60490kbgx041n7zn25qvjn4908mrgr92w5wdh58fqv2y") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.229 (c (n "advent-of-code") (v "2019.12.229") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "15lhisrnkimycxnng8qkv3phdhx5mampf61g48j8warcld16pfnm") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.230 (c (n "advent-of-code") (v "2019.12.230") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "17b38k1l9ipkc0dmfqq5kjfq74ci1cng4jw93wkibmmd047qcm4r") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.231 (c (n "advent-of-code") (v "2019.12.231") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1nas0bz9qfz2f8cmfk1sk5488adx5aj0cdqj3lw1kxjh35fsdv6k") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.232 (c (n "advent-of-code") (v "2019.12.232") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0jbr36zfzdks4rfyvi69p4hnvfn713lrzhvmv9pw6dcslhv855q0") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.233 (c (n "advent-of-code") (v "2019.12.233") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1dix69rg6zrbs7vr3qd6sb6bi0wn4i1wmd258iyabsnryq8nl5wf") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.234 (c (n "advent-of-code") (v "2019.12.234") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "07vkjyirngy3s2gdypr05yrlj8sz1rr428v307icsf3dfrymjqfd") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.235 (c (n "advent-of-code") (v "2019.12.235") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1vws9f404cfgpm5w3lzz4z2sfpzp3qhaq5gmnjih1nkkw4ld6mhz") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.236 (c (n "advent-of-code") (v "2019.12.236") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1kpmzkkz7j5ll1l8mj67136sw4ccff898mhq38z7x25hqrsg69m0") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.237 (c (n "advent-of-code") (v "2019.12.237") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "02pbd91i5144x1mkiw89sysl7rxclhbvp2wqhj6xvcn9zij5lsfj") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.238 (c (n "advent-of-code") (v "2019.12.238") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1bqxghhia0c2ya43l2x5kwwgxldzs9n5ji47xpadydf1mkdkq0b1") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.239 (c (n "advent-of-code") (v "2019.12.239") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1ix3d7cx05adc9bxz6lalvrlqr5027yfgbs4drv11gs2ijg2pyna") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.240 (c (n "advent-of-code") (v "2019.12.240") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1cps8l56nkdwlqk8ji6ma46x8pfnsq76si4m3driqswszhv2cql0") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.241 (c (n "advent-of-code") (v "2019.12.241") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "08a4ljvj9k3mncgsfanffji17a9ik69s96vwla4nwshxblzjsw9q") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.242 (c (n "advent-of-code") (v "2019.12.242") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1igyy1dbz0i1mhv5rs17fbvnkq9m7w95qrc5lmw7pgf6xlfcfsy0") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.243 (c (n "advent-of-code") (v "2019.12.243") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1yn3jxz5mp8may5yz3kpcqpp16ayd2s6jm7r6hswbm8nlqg3qdim") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.244 (c (n "advent-of-code") (v "2019.12.244") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1gj6fznidx96m3wgv651ka41jll5lqh4888yr4cag3mfpphn1dds") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.245 (c (n "advent-of-code") (v "2019.12.245") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1rhvq6w756ckh8dh7zzynx6y4ckkk5dsdrazzxai7cxgmfrhf5pz") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.246 (c (n "advent-of-code") (v "2019.12.246") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "151ymaz27g3p7y4048sq5ga0rf0wp10j9y4bs7z0wg4jffnr01rq") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.247 (c (n "advent-of-code") (v "2019.12.247") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "11k5lj73fp6ygp9ypdgwk9v8wmzsnrhwryw03c1zlipqllvxr0n7") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.248 (c (n "advent-of-code") (v "2019.12.248") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0m0l96a3z8p4vhg1hwiq6pafa8yq1xsj2iqhgi83x8sajr5xaf5i") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.249 (c (n "advent-of-code") (v "2019.12.249") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "03j9zc0239cwbny1n1qnvdqqnsai3n1dlq5fciy9pi05kyjzd3y4") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.250 (c (n "advent-of-code") (v "2019.12.250") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "02l119psp1l0jhp07l7f9sa6msgwciql97qbmn51bcljinm96d5h") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.251 (c (n "advent-of-code") (v "2019.12.251") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "04fkr624c7yq4yn9dkg25957wrbjx5gwsmc898zdj0pg3471djsb") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.252 (c (n "advent-of-code") (v "2019.12.252") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1d1bhz0jhdxky6z63msjc89akbk335arxabk20x6nv1n961b6pp0") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.253 (c (n "advent-of-code") (v "2019.12.253") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "09xzs96gaqn6w2nizbia3kykr2a8w92r9mv5qjpkdmb1r6jqsz9c") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.254 (c (n "advent-of-code") (v "2019.12.254") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ripxfh4h2ihprw67npzf7cx7lgllagxyqs30ng6vbfmvr2a66hs") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.255 (c (n "advent-of-code") (v "2019.12.255") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "03vxl3spra3ypy9idrnm6abrmhrjbr7sfkddc4rbpf2nb0x9izy5") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.256 (c (n "advent-of-code") (v "2019.12.256") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0fn52a99lqbjf2sksb0g8839khzaf8vingn89i8ddfib54z7n12n") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.257 (c (n "advent-of-code") (v "2019.12.257") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "011pwv5bna048c4ybn7pwd19bw362wif5ra23sqkaw4zlhsdnhmi") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.258 (c (n "advent-of-code") (v "2019.12.258") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0mdgc6dij6nlrqskcwb1ixqc7v6384h6pqpgvdm7b0d9wb4x37xk") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.259 (c (n "advent-of-code") (v "2019.12.259") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1bmvnmviz8cs8d9wxbg7408lmag7z1qxd9gr0y9k1q9mkdakzzii") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.260 (c (n "advent-of-code") (v "2019.12.260") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1x957v2wvp2x069xxkiwvz8p5a85s8drinffp2ahpcyqhq5px80x") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.261 (c (n "advent-of-code") (v "2019.12.261") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1hs32n1jynrnac74yrr3gzx1d3by88p50s0sfzr0xfczbzriprpn") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.262 (c (n "advent-of-code") (v "2019.12.262") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "10c69p7hb6p7xbql2y4wi8hzw4diij8af465sbn10b4qxjbh4879") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.263 (c (n "advent-of-code") (v "2019.12.263") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "16y24qmr7nzb1s9x7237synsspg2brx3jvkhljzsj2sgk2z20a8l") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.264 (c (n "advent-of-code") (v "2019.12.264") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "035igbkazgrcxfdnjxwlxzmwm4npdzk4ljp0niq8z4cgqaxvsj60") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.265 (c (n "advent-of-code") (v "2019.12.265") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1bz3v4vpw5ljsr77hx946ff6sh7avif8af49gr60afp1mjmn056b") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.266 (c (n "advent-of-code") (v "2019.12.266") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1l6fd4vk6q9d7lcrmzgl8kr65y428046vb853d0l9g8bf4sr9cpf") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.267 (c (n "advent-of-code") (v "2019.12.267") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "18yncd2gb4hzk92rjdvhijv9sjsiv0lbi5qir5b72k0f055303w3") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.268 (c (n "advent-of-code") (v "2019.12.268") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1cl66a0v6vsjy86s01yzx2jid9wb1cn4l9v74yxmfkkksy5r67h1") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.269 (c (n "advent-of-code") (v "2019.12.269") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0nm15rxh0dfzxz3kxlwwcwh3190d82hbiw6jbmjq2382a9h4iv6y") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.270 (c (n "advent-of-code") (v "2019.12.270") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1wqb4qsls1jddqnmbdpyvqy0dagp42xs0i6vh24hlqq2sig7lhxw") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.271 (c (n "advent-of-code") (v "2019.12.271") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1k8hyhkqlip3svhqp99qf1pn19wadf4imgc4vd2z890ivcch468b") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.272 (c (n "advent-of-code") (v "2019.12.272") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0n5gjv7hwhnzgys5qy9g7xigy1s41cxhkn27n4hj6dl6bvlr4y9v") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.273 (c (n "advent-of-code") (v "2019.12.273") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "10skvknxjyabw4cqbg53741js5vmycba409hi4qdxxa6g59aaz81") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.274 (c (n "advent-of-code") (v "2019.12.274") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1bbhq73w2hls4iwmxdpylp598zxablgx9d4lafkyhy2z19cv4d67") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.275 (c (n "advent-of-code") (v "2019.12.275") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0p00yfa1zv1liaf06qfm4mbi99big8nvpnb16famwq6mkq6hna7a") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.276 (c (n "advent-of-code") (v "2019.12.276") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0l111ir37fym9258y2dccq5qc5yiixiwz55w2b1bcmqmvl8l3dxg") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.277 (c (n "advent-of-code") (v "2019.12.277") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ivxxbkrgm1cihl4pgg2dw5z8ijqwcfjwz0cqbfsf1jmy5bx0jvv") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.278 (c (n "advent-of-code") (v "2019.12.278") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1r6f22ra9sx613pipj3g5f2aqag19lmliy1383lk95lhjs5v1ji9") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.279 (c (n "advent-of-code") (v "2019.12.279") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "11qbad4zdvcrayhqqm60lr0phg6k9cgq0y3349g45h5mrk46i6ky") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.280 (c (n "advent-of-code") (v "2019.12.280") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0gd4m0vqm9g8xm4chg7m9i1c9hwirl74m0mjpyylkyg8nj6j5d06") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.281 (c (n "advent-of-code") (v "2019.12.281") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0r0sqqw4qpayxwrky6c8121jm5p5a1q4d9pjammnh2yzbnalix7k") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.282 (c (n "advent-of-code") (v "2019.12.282") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "14m4wy7qw2fl0m8qa0z45yv4i5lcq5070jzkc9kjjpyjq1gsp48j") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.283 (c (n "advent-of-code") (v "2019.12.283") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0344l43hr811wjdvp7l8z511v7k0h0jclikc877dgbjhfpzi92gm") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.284 (c (n "advent-of-code") (v "2019.12.284") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1ga3m80xn6g6msc2xrcpl85diszcp2xvybf780d3i1bq4qiakg02") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.285 (c (n "advent-of-code") (v "2019.12.285") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1vx0x90g9r67fzvkml6fl520d4xh7579y9i3yd90g7kb0sr7a08m") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.286 (c (n "advent-of-code") (v "2019.12.286") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "02q4jbms3fwn6n90qca892qn8hy8ny8sj2903s2g4s7kiwkjybrj") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.287 (c (n "advent-of-code") (v "2019.12.287") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1m1glxgdhw8w3m165b1z680ckm9hgs5kysdfp3q367jv6hmk1ql2") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.288 (c (n "advent-of-code") (v "2019.12.288") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "06vy08dk7jzk0wd1hgg2pl00gmqi4df64m5havvrjkfy56wsjmb3") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.289 (c (n "advent-of-code") (v "2019.12.289") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1v699xkar8jg8lwiizjpmrg0j650xc9xmbqsqj4jhi2x1fxnyjmf") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.290 (c (n "advent-of-code") (v "2019.12.290") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0p1hqwm46r9lz3l38g732bccmms6xfdxjs08avgd9hll5mmfsrfb") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.291 (c (n "advent-of-code") (v "2019.12.291") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0fd7h34ss0kvz3nxssa0b19nnk02xhjgabvvzhbky2lq9psg7agg") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.292 (c (n "advent-of-code") (v "2019.12.292") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0mcyjdphds45430zdnfc5i2y1cgvsgs2x8972czlh4pjvqmkzm50") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.293 (c (n "advent-of-code") (v "2019.12.293") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "01pqfiz5zxa04yzfbr62bzvfz1qrmhirdy3b0wdb6kyyjyy530ha") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.294 (c (n "advent-of-code") (v "2019.12.294") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1365pwbk76z2mcy29ajja53wrxl2harmwap8iv3786q7icfdgp7a") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.295 (c (n "advent-of-code") (v "2019.12.295") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0psw5hcx77h7dkm150amqwf2wdvzxihhal7kq56hpifln2dxk9d9") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.296 (c (n "advent-of-code") (v "2019.12.296") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0ihjnrm2ac2j4dp8r8qlg2afwwixq8n5m04npr4darxrnriipqsq") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.297 (c (n "advent-of-code") (v "2019.12.297") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0q2dbdxbkn951dimxxywmpcja22cfzksy91v5j09ziv000rdm717") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.298 (c (n "advent-of-code") (v "2019.12.298") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1b9i2vmmhhh05a7xjr46hbdqa7nxdfq5lcabfkxc3a3vaq2kg0h6") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.299 (c (n "advent-of-code") (v "2019.12.299") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1c87y0vrsisi5y98aq3swh46mw647wdxywfxm2lh6qp12npfipnz") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.300 (c (n "advent-of-code") (v "2019.12.300") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0hpb5gsjzdz5aicdbkkf2vhcbdax3jnwb6wq4slkcvlzk9ias4v9") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.301 (c (n "advent-of-code") (v "2019.12.301") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0wbb4f1q3kladmrc1akdlnxi42vzakxq5q62awh1cihyisc59g13") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.302 (c (n "advent-of-code") (v "2019.12.302") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1iimjdqi7hv2dmrvbkkr84rpr080w9lxml7i3xwfl0izf9p6kahs") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.303 (c (n "advent-of-code") (v "2019.12.303") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1dhkfx654d3vgmh6apgx11zhnswbphnc8w594p9n220cnzff3m5d") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.304 (c (n "advent-of-code") (v "2019.12.304") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1b4rwpkzx3mpsc39rnaiiv3xcjk1cqrgws1qmm0mbaqip5synz98") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.305 (c (n "advent-of-code") (v "2019.12.305") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0wxng2ldy3xhp3al1kgyfld14x0k6h1n54f5pfrs8cyzcprc66wx") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.306 (c (n "advent-of-code") (v "2019.12.306") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0cnlmz640z6ai63l32m6f3kckasvlrk2c0sdqn4nwcb0p4m88brp") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.307 (c (n "advent-of-code") (v "2019.12.307") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1iid3nd21illvfkp5q8driqfb2mksps5knip20pm87ksrj6jbdv8") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.308 (c (n "advent-of-code") (v "2019.12.308") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0myw4vjg7qbb7b3cd0qdx6ikwx6kxqf5wcb9bihivpv9k4qvllzg") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.309 (c (n "advent-of-code") (v "2019.12.309") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1x69yd0zig9rdmvnz2ls2659sx2285pg1qa3j1m102l0mnkjbajj") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.310 (c (n "advent-of-code") (v "2019.12.310") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0s7w45k6xx4rq8n447lqlk53msrhqd3ad5kp50681v71rwpfzq7l") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.311 (c (n "advent-of-code") (v "2019.12.311") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0fmmwbxgj3539frmh9d6835rsmh9mg91xz30k2ladjliqnyahp4n") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.312 (c (n "advent-of-code") (v "2019.12.312") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1pp19gl500rafsscm214im4hfw1pxhqm1f44yl0rnwn8xrba4fwl") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.313 (c (n "advent-of-code") (v "2019.12.313") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0q3zq4v09h7d7dm63h1x8njf4lca4v3njk1scq97pii6v45mz1jb") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.314 (c (n "advent-of-code") (v "2019.12.314") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "18f2psfjg9015mihsqap2m88hz87w84x22vpknniybij3iabkq65") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.315 (c (n "advent-of-code") (v "2019.12.315") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1554xphlz06ilx5hdjysmc615pg3vy34mxjjpgk56ggw2xq26vpw") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.316 (c (n "advent-of-code") (v "2019.12.316") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0xqq7dnly032xxdk93pqw7qzc97ci9n1jvyhlcqaz3axjgif5sz6") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.317 (c (n "advent-of-code") (v "2019.12.317") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1xgsi19j2j1ihf23nrgks4y91jnxbj1kv5g558hfdzn812pwv0rw") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.318 (c (n "advent-of-code") (v "2019.12.318") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1k2cv2bk2zrw3rfc5fpwdvg23kzjlrb4dypzsi9p1l7i42qjz2yk") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.319 (c (n "advent-of-code") (v "2019.12.319") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "144w97i0s5r5q9dvfpv7i89anxbw19ig1vs07qg4d0wr34f3sqxl") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.320 (c (n "advent-of-code") (v "2019.12.320") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1gky48ilcnr7306xb36xlq79vjxa5ffgzvhbk5qmddf01w5469h3") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.321 (c (n "advent-of-code") (v "2019.12.321") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0n0anbd4fxgxkwp3ysgysd4n1cxjxy579fmx187b88yhxibagzck") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.322 (c (n "advent-of-code") (v "2019.12.322") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1mh62h3dyli8lldzpcpm3nqy4gprlqqgj20n7csdcl1b874i71bp") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.323 (c (n "advent-of-code") (v "2019.12.323") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0ngz6j0dv1qrarxamszawa8z9c7jb0i5i8q353kshjymhwmy33iy") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.324 (c (n "advent-of-code") (v "2019.12.324") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "14iijba0n5ys76fp5xcnj8hcmx4r4p54mw2k1awbfy4pmwpbd2b8") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.325 (c (n "advent-of-code") (v "2019.12.325") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0gzinr17clhvfmqhhr8k7xdjf4qjh9hlzc6xb3y0gq797clpbnyy") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.328 (c (n "advent-of-code") (v "2019.12.328") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "1ii7l2s8g95g6pa11gv5wa8mbnnqqdjqg089s1zdx6agk7ndiaxl") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.329 (c (n "advent-of-code") (v "2019.12.329") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "0xc2irkadjb0rmc7a1bd1sb3nzqa272wb5cbkcqfqpm6qda00rk0") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.330 (c (n "advent-of-code") (v "2019.12.330") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 0)))) (h "05qhcbj2n5xhbk7ngc881jz3zhy7jnqvb5gaz18bvaicyi7ns8qa") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.331 (c (n "advent-of-code") (v "2019.12.331") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0j8s4sjyrw1301sgg1g4x9qi5mwc1wnhk4qm22cnsr952qbbrk56") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.332 (c (n "advent-of-code") (v "2019.12.332") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0mpkjgmpd3my1xkqgjv52ippbfwlky2wqhkdjvgpbsr978fy7v0i") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.333 (c (n "advent-of-code") (v "2019.12.333") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1cxg506zxdbp2h4l06r2qzc9fcfjzlcacdz5vxb8maj101zb2z42") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.334 (c (n "advent-of-code") (v "2019.12.334") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1n0wjw7jkicpnv3dybsamfiz2wkvpk92pyfcaqf7vis8zb5lgllf") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.335 (c (n "advent-of-code") (v "2019.12.335") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1r5v5xpgp4zg6nsvff6frkc5vmc5rngpchkymmdzjag9h1y4571a") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.336 (c (n "advent-of-code") (v "2019.12.336") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0hk2vjarnwdb5b3sxzw16rpq2dm4k622lr902hfb7xz1pksyhlzj") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.337 (c (n "advent-of-code") (v "2019.12.337") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1f78ki47w2vj7xplb59ign16c28fk9pwdml96szvvifxhzbl6f37") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.338 (c (n "advent-of-code") (v "2019.12.338") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1zjmbb5p12wz25j2nc21ic3y5lh930hsfglagx6sgmcfib5gpmzk") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.339 (c (n "advent-of-code") (v "2019.12.339") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ik9zw2y36g37qn63mkh4a93sbcwdkm02w6s3vwhpsa3gzyaaai9") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.340 (c (n "advent-of-code") (v "2019.12.340") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "09319ri8lyxvca5fn2ys9qvq7q0gplvxsw4jr6rd2cvd6fh5w9rd") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.341 (c (n "advent-of-code") (v "2019.12.341") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1vcv1a28zg0cirmm94r61nz96ymax3cg6b0s1d6hqpkf6a6x61ak") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.342 (c (n "advent-of-code") (v "2019.12.342") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1q8mf3s3hfg63vlv219z49m1s35akjdp52cd9y4d149c49gjyzbm") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.343 (c (n "advent-of-code") (v "2019.12.343") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0zr7025p53hvalv4p4vhhmls3wxawws2na71an23rqxggyq371yk") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.344 (c (n "advent-of-code") (v "2019.12.344") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0i3a5crkkbnlvng63rqky57l4isic6a72qm3s5ipq7lz3a7599az") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.345 (c (n "advent-of-code") (v "2019.12.345") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1pbyhp1jz5mvj4jnclrais832jswrl0v9wj2qfadprpack0fndqh") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.346 (c (n "advent-of-code") (v "2019.12.346") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ism7hr7yd9acq2mzajjffq8a9ydcgh4v950sh8gilrkmsk1bz2m") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.347 (c (n "advent-of-code") (v "2019.12.347") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1z4n4jkw6lmdhliwqjdwbcmx1l8mgb7caq1shb6km0vssfi2hdif") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.348 (c (n "advent-of-code") (v "2019.12.348") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "09bk63029p9ra8wvdsfhpl5qq2dbhmvfj06yhmd9jnhgiizvyh53") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.349 (c (n "advent-of-code") (v "2019.12.349") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1x0d600gz9b992fhp4s7v2g4na19wzbsw8p49gd2rpgw6badny49") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.350 (c (n "advent-of-code") (v "2019.12.350") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0c61vx038h4flklmzhml8wqqq602zpjz14rgda0s0vhpdmcaa8jn") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.351 (c (n "advent-of-code") (v "2019.12.351") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1vxx93b89bgyxbl02m4fl7y63lvs4acjhvvf2b78di9pz3ffbxd8") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.352 (c (n "advent-of-code") (v "2019.12.352") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0xkvgjlh689chgiw9b8rl9l4jyshmkq1d0nch9839s26kxyabgq8") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.353 (c (n "advent-of-code") (v "2019.12.353") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1whwmjnnz5dps1rzi6kq0m6xnmp5n9cjgwf7w6fbb7fl452iwxsg") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.354 (c (n "advent-of-code") (v "2019.12.354") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "04bk8nb4g082rbpw46valw8awzgcvl3qr686r49x4qjpyq0qb0f7") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.355 (c (n "advent-of-code") (v "2019.12.355") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1vmfy9zs9jkr7nbzgyx2q7zm7gwh5haaxwwggw3hqp8b62i31p5n") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.356 (c (n "advent-of-code") (v "2019.12.356") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1r0pnd1xh96myk49i8dad91yvw5ymij2f7c7jc7c2kx91kmv6rqr") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.357 (c (n "advent-of-code") (v "2019.12.357") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1d98ib4mphv9xn0isy4hx8c0s0gnpjpyp5z3570rpc04pyayyvcw") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.358 (c (n "advent-of-code") (v "2019.12.358") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "12g0h00mnn7134jma6pnls3d3i9and34xv6y3mcwrndhjlbhl79n") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.359 (c (n "advent-of-code") (v "2019.12.359") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "05zv3ndxpzqmakrwvbwb5irrj5k3dvjpcxqir897dnvq6dyjjhas") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.360 (c (n "advent-of-code") (v "2019.12.360") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "05hl660l9688415pa277a4cjsmcn9h5ri9vi3q4qrin92bs7dflg") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.361 (c (n "advent-of-code") (v "2019.12.361") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1fwsyki7fxnyrsvkd1shgha1xgkpfjaj0vhj5ivbkn40z2aqf7y6") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.362 (c (n "advent-of-code") (v "2019.12.362") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0gg0r8iagyfz3dy7inf5drlj2jwm94sbw7h4c0v8wkrh9izyiwkh") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.363 (c (n "advent-of-code") (v "2019.12.363") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1mz2xfx4nsw95pvw3p3rhs4njndbxh1qkbssd95iqlc3cykppx5f") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.364 (c (n "advent-of-code") (v "2019.12.364") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "10whl58smg5wlmmi5qmd54j6crw5m4qrlr2vjkap23mrg1xi5gda") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.365 (c (n "advent-of-code") (v "2019.12.365") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "13sjsw3fdnpa5dw5q3va7zpndblxza1akwnfmsls2r8xcfm22n1z") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.366 (c (n "advent-of-code") (v "2019.12.366") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1sg73zcbd7d6wk4s4ck6cmn43vb0h1g89mq1h3j26bqf4vy53dv3") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.367 (c (n "advent-of-code") (v "2019.12.367") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "03ra2g9sp3gfccjvazwglc5iw6fmchrvcakprynpy1cgalddnm30") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.368 (c (n "advent-of-code") (v "2019.12.368") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "068kmypzvy8db9qi42p89ivb49dcsjbhc35ws6913c29d0yni1hy") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.369 (c (n "advent-of-code") (v "2019.12.369") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1j62vyn8bm4f67wxs2amlj2131993by37hsx89dax0cfdx9n8w7b") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.370 (c (n "advent-of-code") (v "2019.12.370") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "19yk9vynjyggx00ynr2fviazjl5apixwmky598m1nz25hjlvkzb7") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.371 (c (n "advent-of-code") (v "2019.12.371") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1hfgs6rwhlqlylv70hslry0c4igf5a007r1vphz8fka0qcwvcf9j") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.372 (c (n "advent-of-code") (v "2019.12.372") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "148n1l09v847cgjh71apr5nafhhs96c9in87rwi1c9zzb4qwqh8j") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.373 (c (n "advent-of-code") (v "2019.12.373") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0gz75k8wkxr4b4f03c8yx9rk0kq622w49clazgd98s2n7x0lcvyh") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.374 (c (n "advent-of-code") (v "2019.12.374") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "04c68jamgk104jd5zv86l867phz208i0m2zhyf8n97ldfxvcvcdw") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.375 (c (n "advent-of-code") (v "2019.12.375") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1yyb36mlcivzmjrg0d1q8r3kmmkqwjhkfc7gcrp8lydar2gd618j") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.376 (c (n "advent-of-code") (v "2019.12.376") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0lwxdlk454qqd2hivag93qvazbq4qm1g1c16pbx6gqfqkzzzcybi") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.377 (c (n "advent-of-code") (v "2019.12.377") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "017x8134nasf1jw657b6q7z2y862i50ydzp49vgmmlibxbr1281w") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.378 (c (n "advent-of-code") (v "2019.12.378") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0qsrv3h9il6l39d8lj6zhz56di7zb0xh6rk5l6yvxck4608mnl68") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.379 (c (n "advent-of-code") (v "2019.12.379") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1s0pv6y7svb33r8wjcn7r6pfyihqnbbiz1bb06a5j4gi84fq113j") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.380 (c (n "advent-of-code") (v "2019.12.380") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "11kgg5jlzm75r61jcqqrslkizdpbfsn4hijzyydh6d6srj476q75") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.381 (c (n "advent-of-code") (v "2019.12.381") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "15ki96nvdcq2sar90qfkwq4ml6i20zy1ar9wgpkpznjdlxy73ccn") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.382 (c (n "advent-of-code") (v "2019.12.382") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1753aks1s2sw5mfij5h0vd1kgrg9zdmlb03drrign6fs6wzgqk4m") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.383 (c (n "advent-of-code") (v "2019.12.383") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1hr5aymmkax2lw40n3b5w5dgyfk157lsblbbcvax7yjmwdbdi3yp") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.384 (c (n "advent-of-code") (v "2019.12.384") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1jg2vyfzlc69jlx090k0wpyk5a5lcb6d6inpvb33mgdrw323zy8n") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.385 (c (n "advent-of-code") (v "2019.12.385") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "19crcqvya6ics6xzz5g876w2g9v6ynwm8pq9dy7gq89qlr591xf2") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.386 (c (n "advent-of-code") (v "2019.12.386") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0gyx6nsc4a5qb2azbcb5nihfwx6vwxf1zhbkd517539aqkkjp2bc") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.387 (c (n "advent-of-code") (v "2019.12.387") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0pdksbz92ifqprw0mv978kzayjpxkxv5prdkar2nx59rk76m21b5") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.388 (c (n "advent-of-code") (v "2019.12.388") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ligcyycyy9w75jg71lmad6fh1sxivyyl7cs7im99xlf9axi1fng") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.389 (c (n "advent-of-code") (v "2019.12.389") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0dkxfh563ycfa6lpanaf4dz9xkfixb9w6aykl88vbpr4almyr8cb") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.390 (c (n "advent-of-code") (v "2019.12.390") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1gwmjd1i92ciaqqd5grazdsfcch4wh9y6l8bwsbpnx4w0av2kg5v") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.391 (c (n "advent-of-code") (v "2019.12.391") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "12nsfwq0jjavid7x13x183crj54df1s4c27qw48z9gwgfvkjam66") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.392 (c (n "advent-of-code") (v "2019.12.392") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0j9ikgyivipsv294d447mf35qj69mlx4ws868w64bq54iz3xnlyz") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.393 (c (n "advent-of-code") (v "2019.12.393") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1nc7pi46gpfxzac10pd1rf0g5kqg69ysgvcfnjvlzya6wiphr5bf") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.394 (c (n "advent-of-code") (v "2019.12.394") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "12lj1hw7ssn1xnghcfhlmhfmb8dx7cg7wi7njzwj6lm203cajan8") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.395 (c (n "advent-of-code") (v "2019.12.395") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "183m6h3a19z74qdz7d9fg4rsn0qlh226vzx9hwg1rdykcf52c3qi") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.396 (c (n "advent-of-code") (v "2019.12.396") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "11mj3jy4s45r94np6bz8f9gjshpxfz40n1qhmxlg252g7lpc696y") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.397 (c (n "advent-of-code") (v "2019.12.397") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "171yhwdw60bqfyvnsdgb8aiiyxfx9y69fpsvsx2kp0mbgxmis2y3") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.398 (c (n "advent-of-code") (v "2019.12.398") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1v0sf646z9ndi848l39az54ndm6ww6gif69c81qy1bas5bsi4kjy") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.399 (c (n "advent-of-code") (v "2019.12.399") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "13w45jjkaczxr2cfqy3ariqmldqjzh9a25aw16pqig3c83cs0nw1") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.400 (c (n "advent-of-code") (v "2019.12.400") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "1nq4rh4khgc7qam8a0k2gidmlnfj3xy1rwkvvgchpa31jymn9h4g") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.401 (c (n "advent-of-code") (v "2019.12.401") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "0yggdqqlliykfiz41wz0709gnqzdpdh9ciz4bdfzdwg8wri3hy0a") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.402 (c (n "advent-of-code") (v "2019.12.402") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "0kxr889iajlfpkb3iahlhplll8hnhmkgav03vkvm2yxj6kl3kbyz") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.403 (c (n "advent-of-code") (v "2019.12.403") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "07sxcvq4n6caij71k3d72rx2qnid67x5bkgdxqfa6cxm9g7nm49s") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.404 (c (n "advent-of-code") (v "2019.12.404") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "16pnd4dlwwfnxp8s5a01z3ay1a3ykdqiy3x8rn0gnk3wqhi51f0s") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.405 (c (n "advent-of-code") (v "2019.12.405") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "1wy56612mlywzz4likq6v7hbrk0saww1qgsbbjdxzhm7s5ac3yqd") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.406 (c (n "advent-of-code") (v "2019.12.406") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "1lqpd380hyzb1v1iylg2rhqamfzf52jhf2rc4dacidicmkjz297r") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.407 (c (n "advent-of-code") (v "2019.12.407") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "1s5lk13pcpmv5xa9iwlqqpzrm6lylib741kgxn9lf549r9aiv9kh") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.408 (c (n "advent-of-code") (v "2019.12.408") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "1rppq023jaw6nihxk5lbm8w297irh7vwq2fwgz8ryafsjyww76lh") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.409 (c (n "advent-of-code") (v "2019.12.409") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "07bma27nggdx66p8ci6g6i7svimzmisxppcd81g9i26isq3c2kva") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.410 (c (n "advent-of-code") (v "2019.12.410") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "03qaf2zfmkkdi8mg9xdkd7glj3b1a8h1lc3j3h3i84xl3z2lr06w") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.411 (c (n "advent-of-code") (v "2019.12.411") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "1aw4aagf1y6icxkpbx518sd6zf3vffdx9cpa4rc746q14g394qll") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.412 (c (n "advent-of-code") (v "2019.12.412") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 0)))) (h "1ckq1gg7iwj18k1pix3xj48przgg06pakspz4g67h0wrah2853i7") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.413 (c (n "advent-of-code") (v "2019.12.413") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "065nr8b1swacl18mnvri251axw6a18ii9asminsm1phsm39ikid5") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.414 (c (n "advent-of-code") (v "2019.12.414") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1bi4n7qhhanmvf4ld7fq274gpnzr90a73z04q2bba0hvpcqw5gsk") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.415 (c (n "advent-of-code") (v "2019.12.415") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1hykhffzwqb3d4y74kx5a9l0kadbq0734nassaqhwmicd41h7h4q") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.416 (c (n "advent-of-code") (v "2019.12.416") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1rsijpshyd7nbygphkvv73mz9vcaykg21rszqbqi036m3k6cjdcd") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.417 (c (n "advent-of-code") (v "2019.12.417") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0667arl84yix8j9hfwiq53w3ck2qqpkdcz6ijlv2qv76zhgkcsy6") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.418 (c (n "advent-of-code") (v "2019.12.418") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1gvj3d9g8ilzhyfl13igl1zy9aklpk0fjq7fc8b9600n03xcdk2b") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.419 (c (n "advent-of-code") (v "2019.12.419") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1p44c54jmi65k30g9d4vzzxvf7nsh8nakjk027pja86h57j8k17b") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.420 (c (n "advent-of-code") (v "2019.12.420") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "04bh3f5a6rk49ygx1d99pskpp20r1k81q3karqcswdn2ycs9d88l") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.421 (c (n "advent-of-code") (v "2019.12.421") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1f34fxh94gdndfnci6hkmvim89aiv81m81f7bil8rn6clm6s465a") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.422 (c (n "advent-of-code") (v "2019.12.422") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0c9y2ahljrss8cy7282yk4sz2pfvqng8q9jg8kf3k59rrkzls8qf") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.423 (c (n "advent-of-code") (v "2019.12.423") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0551vfxmndgbb5lqgjzj0achr834r18hjfwpcnnjqdnj1apy97i0") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.424 (c (n "advent-of-code") (v "2019.12.424") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0z0c4x1bjqxjaw0al0gnbzdsg2w495xl0fn8x50783fl1p0rgrkw") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.425 (c (n "advent-of-code") (v "2019.12.425") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1wqrdd3f4p2iyx2ndp41lf883v0l2fmwz4rzqn273x0lpm85xwdv") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.426 (c (n "advent-of-code") (v "2019.12.426") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0arj592qh4avn02wvlw6a3xxhrliqv0jinwvzmspcigi8f87pzjz") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter"))))))

(define-public crate-advent-of-code-2019.12.427 (c (n "advent-of-code") (v "2019.12.427") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "054lsf52zidv2lfjgfnyv13b6h76jv5zncppnslhaqb1r7d5pdyw") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.57")))

(define-public crate-advent-of-code-2019.12.428 (c (n "advent-of-code") (v "2019.12.428") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0zqfvdqlc32flib2b9bw3yc76y008dg9qz2n74w65lmj4l4rl36x") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.57")))

(define-public crate-advent-of-code-2019.12.429 (c (n "advent-of-code") (v "2019.12.429") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1jv1nx2n8qjl1b8x14a2rvsny9r4p9ppcfwzqpvq6gn23bgr665a") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.57")))

(define-public crate-advent-of-code-2019.12.430 (c (n "advent-of-code") (v "2019.12.430") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "11axb5b629v98jzl9svvxv0vf3ypxgiqpkp9jn40y9q5rc476v7x") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.57")))

(define-public crate-advent-of-code-2019.12.431 (c (n "advent-of-code") (v "2019.12.431") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1rvzcqcwyijkyc07xycpv88zw3bhzcy0srhkycb977k0p39q6cpg") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.57")))

(define-public crate-advent-of-code-2019.12.432 (c (n "advent-of-code") (v "2019.12.432") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0n65s8sdwc6k93azkqpwan1ym0fisddk9qywbrs6f3nc88ajpdzv") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.57")))

(define-public crate-advent-of-code-2019.12.433 (c (n "advent-of-code") (v "2019.12.433") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1cwxk0yjbfd9jizgh36pqsic0xrxz4kh6hxmbyyk3krmlpk3gvi3") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.57")))

(define-public crate-advent-of-code-2019.12.434 (c (n "advent-of-code") (v "2019.12.434") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0xiyq4xr6kmk1y8z216a13xwr55s5nscykw6qy46z4bbx4illd7h") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.57")))

(define-public crate-advent-of-code-2019.12.435 (c (n "advent-of-code") (v "2019.12.435") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1xs1r3fpjc0rmy4xx2gbaxf175rhrwn8bmvpk1b1zcz3j6v79xpw") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.58")))

(define-public crate-advent-of-code-2019.12.436 (c (n "advent-of-code") (v "2019.12.436") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1qfga2aicnq89pa0k93yv2490gjllmayjn0qn7p16jxb5r4hcsw6") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.58")))

(define-public crate-advent-of-code-2019.12.437 (c (n "advent-of-code") (v "2019.12.437") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0pvnxfhm46ck89q69rh580m47kakg7bxyijsgk4143xp1df3xrln") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.58")))

(define-public crate-advent-of-code-2019.12.438 (c (n "advent-of-code") (v "2019.12.438") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "05dhp5v2pr2jzzhg22m0ldwmq4a0qc1hr7z70g46y3zzmzr08vww") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.58")))

(define-public crate-advent-of-code-2019.12.439 (c (n "advent-of-code") (v "2019.12.439") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "16ic13pwdf4llb41sfaicw777ca96cy2x9a6hk8arc5r7paggv0l") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.58")))

(define-public crate-advent-of-code-2019.12.440 (c (n "advent-of-code") (v "2019.12.440") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1615d2m51b5qqwgfqh8spykgxgwhilk623v52nmdjgxa75xcay79") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.58")))

(define-public crate-advent-of-code-2019.12.441 (c (n "advent-of-code") (v "2019.12.441") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0brwvr6d8qi1vkf2q5nbivgrkp8mj4h84qi1qn54ifmwfg5aspl4") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.58")))

(define-public crate-advent-of-code-2019.12.442 (c (n "advent-of-code") (v "2019.12.442") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0im736dbfmwvlabh9r4ga61x0vddgdvw09w6ip9hgxbqhw08myfc") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.58")))

(define-public crate-advent-of-code-2019.12.443 (c (n "advent-of-code") (v "2019.12.443") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1ma9dimj3ba6wd9w4xminl6vpi1dwcikrnvkww1px9j8aajsw6zr") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.58")))

(define-public crate-advent-of-code-2019.12.444 (c (n "advent-of-code") (v "2019.12.444") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1bq4fzs9488mr919igw4irn6v64akfm345s6xh6001x8zpi7bz6m") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.58")))

(define-public crate-advent-of-code-2019.12.445 (c (n "advent-of-code") (v "2019.12.445") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0sky248wyxd37qksj6d2nrf3l52hxibm6wxvplgci7mn2ld4pknf") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.58")))

(define-public crate-advent-of-code-2019.12.446 (c (n "advent-of-code") (v "2019.12.446") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1viyf13hxw9jdy5hfsqrsbab68y1bz8q4dbhd00nrdj0dfvw5z2i") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.62")))

(define-public crate-advent-of-code-2019.12.447 (c (n "advent-of-code") (v "2019.12.447") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0nk5mg1jbvz3v6qmybmswgks74lzpihlv2vgx2zx630scbbziliz") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.62")))

(define-public crate-advent-of-code-2019.12.448 (c (n "advent-of-code") (v "2019.12.448") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0900ywabrq8gzyyw5hk5n33305zkl9xi42h02k35kmxfwjdydqan") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.62")))

(define-public crate-advent-of-code-2019.12.449 (c (n "advent-of-code") (v "2019.12.449") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1jp50dhm1r4g8wv0giv9whq0ai2q2yi78ynybxh01zcfnjkkx26r") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.62")))

(define-public crate-advent-of-code-2019.12.450 (c (n "advent-of-code") (v "2019.12.450") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "15yfmncsv725jclx290kaq95n7v3cm243kfawahfbf6a623wjngy") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.62")))

(define-public crate-advent-of-code-2019.12.451 (c (n "advent-of-code") (v "2019.12.451") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0dknddfp836rjvrqi75bzsrzz4j6110fai1g0r070pg363352fds") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.62")))

(define-public crate-advent-of-code-2019.12.452 (c (n "advent-of-code") (v "2019.12.452") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1fj6d8kidfr9zh41c3dilm5kvlxj7z4k4x7fa36903kdc2g5r7xg") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.62")))

(define-public crate-advent-of-code-2019.12.453 (c (n "advent-of-code") (v "2019.12.453") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "11a2alk9cmgpqid46brrg9zajr3idh7dkagvfn921cshjwwgm7l9") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.62")))

(define-public crate-advent-of-code-2019.12.454 (c (n "advent-of-code") (v "2019.12.454") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "028q2if6kjm721jg3f3f038yyw3h31knh7v563ld4rrqnidppm4z") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.62")))

(define-public crate-advent-of-code-2019.12.455 (c (n "advent-of-code") (v "2019.12.455") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "10d2ihld51gmki5apyaff811h4vbfpk50y4ir4bhi64k1sb0k228") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.62")))

(define-public crate-advent-of-code-2019.12.458 (c (n "advent-of-code") (v "2019.12.458") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1g5ky8j7nimjm50rpdyl6alcpiv58209isjslrv7qvi3zy0g2gii") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.64")))

(define-public crate-advent-of-code-2019.12.459 (c (n "advent-of-code") (v "2019.12.459") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1lp0m6cjgwax7ivfpxfzsg407ihmmrik45apn97d59gh05qrgmlh") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.64")))

(define-public crate-advent-of-code-2019.12.460 (c (n "advent-of-code") (v "2019.12.460") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0bm8wav7bl24dpi0cmh44lsw8k9abskwxcl5a4lc6hs5f3j7in04") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.64")))

(define-public crate-advent-of-code-2019.12.461 (c (n "advent-of-code") (v "2019.12.461") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0h0dsw80ghz28gm081g7gllfz4sfb1h06s44mqz2bvm9w6yn795v") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.64")))

(define-public crate-advent-of-code-2019.12.462 (c (n "advent-of-code") (v "2019.12.462") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1s394lkq3bfvvvn8c3x3fdidbzlhlwzm49b7fqzadin7mgharyvb") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.64")))

(define-public crate-advent-of-code-2019.12.463 (c (n "advent-of-code") (v "2019.12.463") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1r9aza8z5a9bqjpbdmyil4jfm5zvxi6nidfd6fd2hpv14psv3mmi") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.64")))

(define-public crate-advent-of-code-2019.12.464 (c (n "advent-of-code") (v "2019.12.464") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0245ayffsbk3dcradfd42ml5pqwc4xwjm9gkrfhr4d4mx5yjksln") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.64")))

(define-public crate-advent-of-code-2019.12.466 (c (n "advent-of-code") (v "2019.12.466") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "10nmmnssm20mmdkr577qfaa5gqh34vpswshyh92yznm6wh19mcnn") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.64")))

(define-public crate-advent-of-code-2019.12.467 (c (n "advent-of-code") (v "2019.12.467") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0dsz23g8rncsy8r0aa660dm0xa6cf4yzb7aipwng40wlnclbvi15") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.468 (c (n "advent-of-code") (v "2019.12.468") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "17ih5ajkzzhidpdwcxp4p44hczrmxsaj2y3mpxni71glw157zn7n") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.469 (c (n "advent-of-code") (v "2019.12.469") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "095syj5qk5hadmnvss12wl2m2fhmjnmnjq2bm57n4svw7j01v5l0") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.470 (c (n "advent-of-code") (v "2019.12.470") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0zxisgkknl5pj0p4cwg7lj286kb5frsipdqm4n0kik7304bbiz1z") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.471 (c (n "advent-of-code") (v "2019.12.471") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0chn15h23lzyyk10cpih7b73ia96kmwfjpji6wwswm6pwjdlcinr") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.472 (c (n "advent-of-code") (v "2019.12.472") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1ibyq74b8jgqnkw84xlq32cfr8k2d73xs3a5ajry6lv0drxxwmrm") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.473 (c (n "advent-of-code") (v "2019.12.473") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1hxiyvv4znpg0bsqgivw4sn6g1q0fr7k9n4bayk9fprpa4mjbz58") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.474 (c (n "advent-of-code") (v "2019.12.474") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0xmh5syfjlbw8a5cfqh1sy8yxicxmd0w1fbdnid1j6r7cmglyn9j") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.475 (c (n "advent-of-code") (v "2019.12.475") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "02bs4kzj39j3bcn2xblwsmdcf5xxp6qqj8w1mp1bxj2frsn7q8xw") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.476 (c (n "advent-of-code") (v "2019.12.476") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "195cl31nrzd4lsaikv8j5kkdkayskfm906bmx7mfnjcwj504gxcd") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.477 (c (n "advent-of-code") (v "2019.12.477") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1k84dvh0l05xipxg0zizy1lsk3p0gg5hvnlirir920dfhh1s0brj") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.478 (c (n "advent-of-code") (v "2019.12.478") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1gj57xka03x1rp74s35bk5sr6f83wfx4yxpb0w5bislgh2rnj05s") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.479 (c (n "advent-of-code") (v "2019.12.479") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1a677zypiywpjnfz2fflwapdq9x2ysrg8kmgcx2kd1rbxqam4cyr") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2019.12.480 (c (n "advent-of-code") (v "2019.12.480") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "17kc97whl5bx91zd2bqzyxycpq8qch9whsmigpm0f7ddfbb0rchx") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.1 (c (n "advent-of-code") (v "2022.0.1") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "01ii1k4i8izrnlf1jzdbhgir0aibwrcy7hkaka2k4anrpvhpbal4") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.2 (c (n "advent-of-code") (v "2022.0.2") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0cdnawawdhqp097hszvfxfdy3b29jh871px2b309x6jg6gmlgx3d") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.3 (c (n "advent-of-code") (v "2022.0.3") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1m96709gpdn99kd0lrv36wylwpqwz19kyn5g4w6wnhrgqgh349yb") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.4 (c (n "advent-of-code") (v "2022.0.4") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1p2qqlsv5lbzzijaxbysgs7ssyhk71gagmjfl1wxbbr30d6w8hx8") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.5 (c (n "advent-of-code") (v "2022.0.5") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0jz6q8p76j5pgk62mr8sg38a7rml2zw2s9fqrn8v5hrc1hq9mbna") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.6 (c (n "advent-of-code") (v "2022.0.6") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "089v4q1fwd4j3pcfjbkn8bwqagfcbzagb157fxkr467aaaw48bbj") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.7 (c (n "advent-of-code") (v "2022.0.7") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1ra8i0ibjfs95lsxfb7h56b54czi442i5a9plz6pd9hfbg3zbciv") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.8 (c (n "advent-of-code") (v "2022.0.8") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0f9xc9h1cpycmijvd99lpaq6wfzh1nyn5089rvzccnhbkikqwm1f") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.9 (c (n "advent-of-code") (v "2022.0.9") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1qi4m64qjkpzfb8vnpfrrf5z85ka9ssd44j4rfj9sps4v047ffha") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.10 (c (n "advent-of-code") (v "2022.0.10") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0gf104cxi5xb0rwzihqx9pil890m5qnbyxak6jmgb3y3mzds4a4w") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.11 (c (n "advent-of-code") (v "2022.0.11") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0gh777v729bfjdh86mkzds9bq6g1s4725xymni9kbcg05w9c9r5b") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.12 (c (n "advent-of-code") (v "2022.0.12") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1a3a7spzp6jssywxcvbfq2mjf6afbia1j2pkcbhq4bma0671za16") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.13 (c (n "advent-of-code") (v "2022.0.13") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0s0rb780jld10gjmj301sjba75dphrn2sv17b77nh16whn38n059") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.14 (c (n "advent-of-code") (v "2022.0.14") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1mfjayxxj1y7yl314swpwjc4yybhs7qsdk2hmy537k7kr2y2zjpa") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.15 (c (n "advent-of-code") (v "2022.0.15") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1xvk6xll007wk12fcd7xip50kqj8ggxmwdmrby1jzgddjsmxc3gq") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.16 (c (n "advent-of-code") (v "2022.0.16") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "191pajiifjargxmyl9h3k1ipdgqgm87w9r87jkvihfmfn43790c4") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.17 (c (n "advent-of-code") (v "2022.0.17") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0dxirad019jn504640myl8xil4805q1f6zvigfi8p5ch9dcj42zc") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.18 (c (n "advent-of-code") (v "2022.0.18") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "05f9z0w7chqsv6yiiy7nyg6s9qch4y7vwvm5bra7wjba3hq8arn1") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.19 (c (n "advent-of-code") (v "2022.0.19") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1n0slpar55cgnj33bjx1dxb17rm04495v4dyzb8g2yjhwg1s6g7h") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.20 (c (n "advent-of-code") (v "2022.0.20") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1m1jq1iq3s90mjmvi0xizydiykhc8v75222bpgk06ff3si5nz166") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.21 (c (n "advent-of-code") (v "2022.0.21") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "02dhb8x1di8xrp14d5p8m46a92jwhr0817vbigrgjqz9154qxp5s") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.22 (c (n "advent-of-code") (v "2022.0.22") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "08d2v216x3sbabh95i2cgvlb6vs6fi38d5aip00jd15mwqd1s2f5") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.23 (c (n "advent-of-code") (v "2022.0.23") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0054i1mk90d28w6r5d120i696qlf4cnz52yqmp8x5xj6ap3llh37") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.24 (c (n "advent-of-code") (v "2022.0.24") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0nn785bdn7dfvjy3mqvvl32f7691v2liappcbp4wddw4bjdw1dw3") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.65")))

(define-public crate-advent-of-code-2022.0.25 (c (n "advent-of-code") (v "2022.0.25") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "07rcy38jsc4c47czjipckd6qblaylf26drrfn5ic320injjg0qiy") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.26 (c (n "advent-of-code") (v "2022.0.26") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "1nsa6w7qsvq19r2smrlwxqm6a9cr5jw16fkc5w422sd8g2q4nypp") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.27 (c (n "advent-of-code") (v "2022.0.27") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0vj481zilpv4bhhxbfsizfm5cjbnaji0vz68jcw5czk89ms7imbx") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.28 (c (n "advent-of-code") (v "2022.0.28") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "103m0zjh4f8m98awh7z5xnakqyncrz1g6qk3657vmi6c58a5cd77") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.29 (c (n "advent-of-code") (v "2022.0.29") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "005sjgj95522nfhqcz3jr8x348s8l1yvjcm472p7yw76k8r49227") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.30 (c (n "advent-of-code") (v "2022.0.30") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "11jy4ki8jxsi322ddvlf3jxwx33wcvnpj92n3198wradmf7g7pnc") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.31 (c (n "advent-of-code") (v "2022.0.31") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "146gz62icy4ivh5ayhbqbdrf5rdi0ckmljvcf8bfznylbvc054rc") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.32 (c (n "advent-of-code") (v "2022.0.32") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "0frgz661qx2y9adkgnk6gd8bbvb948svy2zpy6vl1swj2i5lszfa") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.33 (c (n "advent-of-code") (v "2022.0.33") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)))) (h "12s6nwnwrvp3v9j77gyb2mx859z03lnps8ns8xmvb0gxs9rld88g") (f (quote (("visualization") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.43 (c (n "advent-of-code") (v "2022.0.43") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)) (d (n "svgplot") (r "^2022.0.43") (o #t) (d #t) (k 0)))) (h "1ixq75l3cw22n0325qxalkzw3gj5cbqzwl4v8l1laixhhn1mvw6c") (f (quote (("visualization" "svgplot") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.44 (c (n "advent-of-code") (v "2022.0.44") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)) (d (n "svgplot") (r "^2022.0.44") (o #t) (d #t) (k 0)))) (h "1jgxx067wb8hy2zhsrvlnpmz9fyzngrykzjgfgcklidss1j40gfq") (f (quote (("visualization" "svgplot") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.45 (c (n "advent-of-code") (v "2022.0.45") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)) (d (n "svgplot") (r "^2022.0.45") (o #t) (d #t) (k 0)))) (h "053bqv07n7jn5z20c9wh691bwgnqxq9ryfry00b9hbfx06zvmn5g") (f (quote (("visualization" "svgplot") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.46 (c (n "advent-of-code") (v "2022.0.46") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)) (d (n "svgplot") (r "^2022.0.46") (o #t) (d #t) (k 0)))) (h "1b0i2xgr078rxyxn8qgli4rra998csdpzp19i17zf2a6kqlqpq0d") (f (quote (("visualization" "svgplot") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.49 (c (n "advent-of-code") (v "2022.0.49") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)) (d (n "svgplot") (r "^2022.0.49") (o #t) (d #t) (k 0)))) (h "09frwl8in2b3f6vbvkimc5b3zj36w47vm0fdyy45xzzx4sdpqqii") (f (quote (("visualization" "svgplot") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.50 (c (n "advent-of-code") (v "2022.0.50") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)) (d (n "svgplot") (r "^2022.0.50") (o #t) (d #t) (k 0)))) (h "144i1s8s6g1bd0siqqzsld9lzdkiv1cd4ysxafrbbqxdy409vs7m") (f (quote (("visualization" "svgplot") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.52 (c (n "advent-of-code") (v "2022.0.52") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)) (d (n "svgplot") (r "^2022.0.52") (o #t) (d #t) (k 0)))) (h "0qffvfgblldsddajqwzir6zrraq13j6rmain4b3awrig6a7q3hz8") (f (quote (("visualization" "svgplot") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.53 (c (n "advent-of-code") (v "2022.0.53") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "iai") (r "^0") (d #t) (k 2)) (d (n "svgplot") (r "^2022.0.53") (o #t) (d #t) (k 0)))) (h "0xd4zv0k7m5yffbxarc76x8pbxcnamr42ar0lasyfciibl2f807r") (f (quote (("visualization" "svgplot") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.54 (c (n "advent-of-code") (v "2022.0.54") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "svgplot") (r "^2022.0.54") (o #t) (d #t) (k 0)))) (h "1h0j0rp4mnky4lx0kl5dwm675gwvb06407600pkn7n3887k5zl8i") (f (quote (("visualization" "svgplot") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.55 (c (n "advent-of-code") (v "2022.0.55") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "svgplot") (r "^2022.0.55") (o #t) (d #t) (k 0)))) (h "1d3xqnirvkhgid1hl0swxfhwxs4p1bmk5lbsczh42kyj8mkjqxxn") (f (quote (("visualization" "svgplot") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.56 (c (n "advent-of-code") (v "2022.0.56") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "svgplot") (r "^2022.0.56") (o #t) (d #t) (k 0)))) (h "1ic3mrfzabgchfxh3wnkbgn7648bs9yrvlc1xg3s5r22jaj9jiil") (f (quote (("visualization" "svgplot") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.57 (c (n "advent-of-code") (v "2022.0.57") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "svgplot") (r "^2022.0.57") (o #t) (d #t) (k 0)))) (h "0a0xrkyv3mfwx9yrg74zpa7g88p81vlcalw4yak3wb1grx35ccv4") (f (quote (("visualization" "svgplot") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.58 (c (n "advent-of-code") (v "2022.0.58") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "pollster") (r "^0") (o #t) (d #t) (k 0)) (d (n "svgplot") (r "^2022.0.58") (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0") (o #t) (d #t) (k 0)))) (h "11ca65j0mshdlgynp2xqdk287h5cakz425xdmpz0dp3vjb03h374") (f (quote (("webgpu-compute" "bytemuck" "pollster" "wgpu") ("visualization" "svgplot") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.66")))

(define-public crate-advent-of-code-2022.0.59 (c (n "advent-of-code") (v "2022.0.59") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "pollster") (r "^0") (o #t) (d #t) (k 0)) (d (n "svgplot") (r "^2022.0.59") (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0") (o #t) (d #t) (k 0)))) (h "0dpk4mwi8s134qh36aw6x1l3w60br0y377pbz56bm63knpjlsv6f") (f (quote (("webgpu-compute" "bytemuck" "pollster" "wgpu") ("visualization" "svgplot") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.74")))

(define-public crate-advent-of-code-2022.0.60 (c (n "advent-of-code") (v "2022.0.60") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "pollster") (r "^0") (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0") (o #t) (d #t) (k 0)))) (h "1g5s8khyvp461zniarz8ybfspb4if6ngr0658zghk94c2jbhiyb0") (f (quote (("webgpu-compute" "bytemuck" "pollster" "wgpu") ("visualization") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.74")))

(define-public crate-advent-of-code-2022.0.61 (c (n "advent-of-code") (v "2022.0.61") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "pollster") (r "^0") (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0") (o #t) (d #t) (k 0)))) (h "01wj51gz2g165jrpxlzbpm1r0pmlbrxyl6grh9zakmyv0h9cxrmc") (f (quote (("webgpu-compute" "bytemuck" "pollster" "wgpu") ("visualization") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.74")))

(define-public crate-advent-of-code-2022.0.62 (c (n "advent-of-code") (v "2022.0.62") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "pollster") (r "^0") (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0") (o #t) (d #t) (k 0)))) (h "10ljqig81flfsrpv8867z3cl55z6mdv4s7ignmd4r9dgafvlv4ln") (f (quote (("webgpu-compute" "bytemuck" "pollster" "wgpu") ("visualization") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.74")))

(define-public crate-advent-of-code-2022.0.63 (c (n "advent-of-code") (v "2022.0.63") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "pollster") (r "^0") (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0") (o #t) (d #t) (k 0)))) (h "0m8c5wd7qbv7yprfyk0kn110ckal454cx4ni7hsk3byd1kwzm8pm") (f (quote (("webgpu-compute" "bytemuck" "pollster" "wgpu") ("visualization") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.74")))

(define-public crate-advent-of-code-2022.0.64 (c (n "advent-of-code") (v "2022.0.64") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "pollster") (r "^0") (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0") (o #t) (d #t) (k 0)))) (h "160gb3hkg7ifjiryvmbbhi2pha00d7sphl1a7vjy64gwyvihvsmb") (f (quote (("webgpu-compute" "bytemuck" "pollster" "wgpu") ("visualization") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.75")))

(define-public crate-advent-of-code-2022.0.65 (c (n "advent-of-code") (v "2022.0.65") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "pollster") (r "^0") (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0") (o #t) (d #t) (k 0)))) (h "18zb6sy4ky12wgkwa4k9whjxj2c6z5kpx009f30xkmymfk1mqhjj") (f (quote (("webgpu-compute" "bytemuck" "pollster" "wgpu") ("visualization") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.76")))

(define-public crate-advent-of-code-2022.0.66 (c (n "advent-of-code") (v "2022.0.66") (d (list (d (n "allocation-counter") (r "^0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0") (k 2)) (d (n "pollster") (r "^0") (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0") (o #t) (d #t) (k 0)))) (h "1waal68y8x5z8rwkvlss7dgk5ma529kbrw1v4fjj0rwjdwwnz6zs") (f (quote (("webgpu-compute" "bytemuck" "pollster" "wgpu") ("visualization") ("simd") ("debug-output") ("count-allocations" "allocation-counter")))) (r "1.76")))

