(define-module (crates-io ad ve adventjson) #:use-module (crates-io))

(define-public crate-adventjson-0.1.0 (c (n "adventjson") (v "0.1.0") (h "0nzwjqpapfqmnhqmbi95bh9jfmn1a4zd6fp5x58vmkpg1z8byymn")))

(define-public crate-adventjson-0.1.1 (c (n "adventjson") (v "0.1.1") (h "128hr9wlhc8alhfz4a3v3rj23jbavcd1kqgj35qxc31c5j5b6b7n")))

