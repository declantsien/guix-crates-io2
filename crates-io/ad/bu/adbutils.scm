(define-module (crates-io ad bu adbutils) #:use-module (crates-io))

(define-public crate-adbutils-0.1.0 (c (n "adbutils") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)))) (h "08dix5nw3hylvi189kxz5zn91niak331z20bwasqjkjk261sbcv6")))

(define-public crate-adbutils-0.1.1 (c (n "adbutils") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)))) (h "1wninj43r09qkbq6cqcbh6d2d12j70gpp299i43gqsblps570vwg")))

