(define-module (crates-io ad va advancedresearch-higher_order_core) #:use-module (crates-io))

(define-public crate-advancedresearch-higher_order_core-0.1.0 (c (n "advancedresearch-higher_order_core") (v "0.1.0") (h "0n7xap5acq7317ksladyc7hrjmbbbq8dpg970l1nyvqq7sr40yd8")))

(define-public crate-advancedresearch-higher_order_core-0.1.1 (c (n "advancedresearch-higher_order_core") (v "0.1.1") (h "18h0z7g3c44qlclcm9cx8j1nv5pd5w7g5r6142qdvpp59dpkq2cq")))

(define-public crate-advancedresearch-higher_order_core-0.2.0 (c (n "advancedresearch-higher_order_core") (v "0.2.0") (h "180a3w4k36psbhhch05aayx6xpgidsm2hij0imiydrh7mq8p2rl3")))

(define-public crate-advancedresearch-higher_order_core-0.3.0 (c (n "advancedresearch-higher_order_core") (v "0.3.0") (h "1rjhxlqfy40bfcfn2dic219pfrsmh7hc0rn70gs0djdq7sa012mv")))

