(define-module (crates-io ad va advapi32-sys) #:use-module (crates-io))

(define-public crate-advapi32-sys-0.0.1 (c (n "advapi32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1226ahypf1i1f4g6c8mhkqz2n7zdl98081ng27hc8mvigcim0gr1")))

(define-public crate-advapi32-sys-0.0.2 (c (n "advapi32-sys") (v "0.0.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "154pprycib7b0llypk2s9l1fy4c5z290vgil7llh3f1pcav8s89a")))

(define-public crate-advapi32-sys-0.0.3 (c (n "advapi32-sys") (v "0.0.3") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "023gplmy0r90jf62s2lf5mg2kg8wsiicp3l1dy8yqhgs6qsv0cnm")))

(define-public crate-advapi32-sys-0.0.4 (c (n "advapi32-sys") (v "0.0.4") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "18zhq6yysv83469yabs4d7wjrnvxk8h70wyf5gsn3d7mhl1c8qr9")))

(define-public crate-advapi32-sys-0.1.0 (c (n "advapi32-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "16j0x6hqahl3vkh42062hapy89kar01nd4f8w6iflfblpzcpcrbf")))

(define-public crate-advapi32-sys-0.1.1 (c (n "advapi32-sys") (v "0.1.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "05i966nsh5fawysmlzdfk125di3lqh1q779l99m0ibfba19jn9xl")))

(define-public crate-advapi32-sys-0.1.2 (c (n "advapi32-sys") (v "0.1.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "1mvbqq7v9ibbn2hxizhyf4qq9ghbi6fyw8h21ir8drb750rr4z1h")))

(define-public crate-advapi32-sys-0.2.0 (c (n "advapi32-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "16largvlrd1800vvdchml0ngnszjlnpqm01rcz5hm7di1h48hrg0")))

