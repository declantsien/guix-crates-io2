(define-module (crates-io ad va advanedresearch-trinoise) #:use-module (crates-io))

(define-public crate-advanedresearch-trinoise-0.1.0 (c (n "advanedresearch-trinoise") (v "0.1.0") (h "127bx6f2zlnbibcaxvbw8akbz12minnl7xcbagh578sy1whwxckn")))

(define-public crate-advanedresearch-trinoise-0.1.1 (c (n "advanedresearch-trinoise") (v "0.1.1") (h "15sn2m5yvpddx1h4sbkzxhvcsnf10r2zbcd2r4pqlrydi5klprld")))

