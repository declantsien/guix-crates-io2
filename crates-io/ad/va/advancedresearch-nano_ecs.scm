(define-module (crates-io ad va advancedresearch-nano_ecs) #:use-module (crates-io))

(define-public crate-advancedresearch-nano_ecs-0.1.0 (c (n "advancedresearch-nano_ecs") (v "0.1.0") (h "1jmjz9xqlfzsgw8sa4crk2cc7i0chlv31mbszr7vvyiillsc70b5")))

(define-public crate-advancedresearch-nano_ecs-0.2.0 (c (n "advancedresearch-nano_ecs") (v "0.2.0") (h "1ngkwwjzza5d8pfphwzh45rc5z4x8ig7w4hfklgb9v898zh7indf")))

(define-public crate-advancedresearch-nano_ecs-0.3.0 (c (n "advancedresearch-nano_ecs") (v "0.3.0") (h "0cizg2fcaj6jv56agz46a3s31fb04g6p40fxwapvpg3pihd4fclk")))

(define-public crate-advancedresearch-nano_ecs-0.4.0 (c (n "advancedresearch-nano_ecs") (v "0.4.0") (h "0c7gdz4p54kzfixwm1n5mzw49bmq5vrpb2jas3cxfmxqlvvqbxd7")))

(define-public crate-advancedresearch-nano_ecs-0.4.1 (c (n "advancedresearch-nano_ecs") (v "0.4.1") (h "0pfsrq81d5qsyhsmm5551mzkwsvfkmaz02qc7w2dbdwz28pl6ibb")))

(define-public crate-advancedresearch-nano_ecs-0.5.0 (c (n "advancedresearch-nano_ecs") (v "0.5.0") (h "0slrngm82ay8kazziq310k5hcpbr8d88qfp9qarcr2jqpngc1fc6")))

(define-public crate-advancedresearch-nano_ecs-0.5.1 (c (n "advancedresearch-nano_ecs") (v "0.5.1") (h "0by864ncy1sdxjscp2hcb89xns8gbyrbnlchr70m64v6arkl3gsk")))

(define-public crate-advancedresearch-nano_ecs-0.5.2 (c (n "advancedresearch-nano_ecs") (v "0.5.2") (h "15xx65h93lv5jxy5qqq51a19cxl6ifil4xblx0na6vngnq0sba56")))

(define-public crate-advancedresearch-nano_ecs-0.6.0 (c (n "advancedresearch-nano_ecs") (v "0.6.0") (h "08q4cfphn2ad6sd10w94gqclqbgs90xgi9i9z1w59m1wg8ncaa28")))

(define-public crate-advancedresearch-nano_ecs-0.7.0 (c (n "advancedresearch-nano_ecs") (v "0.7.0") (h "1rz6rvk31h0d56f1q28n09pj5v2nywll4gnf4vyg2clcl5l949d8")))

(define-public crate-advancedresearch-nano_ecs-0.8.0 (c (n "advancedresearch-nano_ecs") (v "0.8.0") (h "0klmq2ggg3bv73cgpkc22hhc9xg4wg5kww18ddnf1jxclm61za4p")))

(define-public crate-advancedresearch-nano_ecs-0.9.0 (c (n "advancedresearch-nano_ecs") (v "0.9.0") (h "1sainhva8jfb546731s25fx855394vvb8f2x9nc82ifznaz2m5yr")))

