(define-module (crates-io ad va advanced-pid) #:use-module (crates-io))

(define-public crate-advanced-pid-0.1.0 (c (n "advanced-pid") (v "0.1.0") (h "0wnj6x9hggzqp34kk1dlqbnxjcbs2gj0mzvqa9n1bi20kxzhnpg4") (f (quote (("std") ("f64") ("f32") ("default" "std" "f32"))))))

(define-public crate-advanced-pid-0.1.1 (c (n "advanced-pid") (v "0.1.1") (h "0j0yraj05w67bmrz7x3zr9aak4mfbkasl0r2xn08n8kdi9hp6x0z") (f (quote (("std") ("f64") ("f32") ("default" "std" "f32"))))))

(define-public crate-advanced-pid-0.1.2 (c (n "advanced-pid") (v "0.1.2") (h "032f8dhvsg8831n0p3wk955h2bxzx1nxm3zl4ji81849r34mmj87") (f (quote (("std") ("f64") ("f32") ("default" "std" "f32"))))))

(define-public crate-advanced-pid-0.2.0 (c (n "advanced-pid") (v "0.2.0") (h "1d2rwhivy547vlf8szg5jjk8d641s3ygfddqq99f4x1kc96qixv9") (f (quote (("std") ("f64") ("default" "std"))))))

(define-public crate-advanced-pid-0.2.1 (c (n "advanced-pid") (v "0.2.1") (h "1783q8c5mj5srvv8v426143v42k38n5f8xp0pvnrdqq35hkwhxgg") (f (quote (("std") ("f64") ("default" "std"))))))

(define-public crate-advanced-pid-0.2.2 (c (n "advanced-pid") (v "0.2.2") (h "0nvcsmas36lkjhzhbdpbc8qk2p33l15a5gjdndxd42fk31x2ldhz") (f (quote (("std") ("f64") ("default" "std"))))))

