(define-module (crates-io ad va advancedresearch-hypo) #:use-module (crates-io))

(define-public crate-advancedresearch-hypo-0.1.0 (c (n "advancedresearch-hypo") (v "0.1.0") (h "0klpkjpjlrx4anxbqj43cqrph3vb8kzmjjsxnnqc8hspd786gyd2")))

(define-public crate-advancedresearch-hypo-0.1.1 (c (n "advancedresearch-hypo") (v "0.1.1") (h "10v9llp27dcvjadvzhxnkk4jvc5n7aky2g01crzkdj3dj42g3p2l")))

(define-public crate-advancedresearch-hypo-0.2.0 (c (n "advancedresearch-hypo") (v "0.2.0") (h "0nbxb0xvsd4dkh0x77slq1f1dliyy2rvci03cryv2fwpbvb8bz8c")))

