(define-module (crates-io ad va advantage) #:use-module (crates-io))

(define-public crate-advantage-0.0.0 (c (n "advantage") (v "0.0.0") (h "18alw96l8qnpql3cwdsk2sikc5vjcjrx2alrnq61svfyl41rb0zf")))

(define-public crate-advantage-0.1.0 (c (n "advantage") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.9") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0zdrq37kaw8rd8gwkic2ydmcpszbr8060sg7g2xvfwvx714zkyn3") (f (quote (("ffi" "cbindgen") ("default"))))))

