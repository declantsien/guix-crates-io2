(define-module (crates-io ad va advancedresearch-asi_core0) #:use-module (crates-io))

(define-public crate-advancedresearch-asi_core0-0.1.0 (c (n "advancedresearch-asi_core0") (v "0.1.0") (h "17vrh07r825084v15z3dq2mmfbcdrz1bcrl58p63wqcrzzpfs43w")))

(define-public crate-advancedresearch-asi_core0-0.2.0 (c (n "advancedresearch-asi_core0") (v "0.2.0") (h "12aj3w2r3w9xidddqcj79n8nyns6z77jdf1fya4p6wgsfy8vfqw9")))

