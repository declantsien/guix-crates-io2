(define-module (crates-io ad va advancedresearch-utility_programming) #:use-module (crates-io))

(define-public crate-advancedresearch-utility_programming-0.1.0 (c (n "advancedresearch-utility_programming") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "0ppyl2bdjscg1r64wp7l77vdnsbj5040ii5y2dijrdfz8qprqdnl")))

(define-public crate-advancedresearch-utility_programming-0.2.0 (c (n "advancedresearch-utility_programming") (v "0.2.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "1znz50rpf57glra87bl130jvshr2pa9yq4k99x9f1lff6vp547rs")))

(define-public crate-advancedresearch-utility_programming-0.3.0 (c (n "advancedresearch-utility_programming") (v "0.3.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "15dlwh390n18mllhhp00wdbgbyv151v4wg3l6z4mmghphb3svh6n")))

