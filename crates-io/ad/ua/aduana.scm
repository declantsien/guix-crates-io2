(define-module (crates-io ad ua aduana) #:use-module (crates-io))

(define-public crate-aduana-0.1.0 (c (n "aduana") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "084a2qkvv2n3sl33mqa5fi621baqb36j1dx074mlkfmr7824dvdy")))

