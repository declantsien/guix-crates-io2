(define-module (crates-io ad i- adi-adxl355) #:use-module (crates-io))

(define-public crate-adi-adxl355-0.1.0 (c (n "adi-adxl355") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.10") (d #t) (k 0)) (d (n "micromath") (r "^2.0") (f (quote ("vector"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17kd0i0bz3yv9yjbbdrad82cl1lmg5l13abads3hyp6hmiyv7vsk") (f (quote (("default")))) (s 2) (e (quote (("std" "dep:thiserror") ("defmt" "dep:defmt")))) (r "1.68")))

(define-public crate-adi-adxl355-0.2.0 (c (n "adi-adxl355") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "micromath") (r "^2.0") (f (quote ("vector"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "161d6z92rrd494vg518a2vnvi1pjjlnk8a4ccfxbx6inyrkwaz9i") (f (quote (("default")))) (s 2) (e (quote (("std" "dep:thiserror") ("defmt" "dep:defmt")))) (r "1.68")))

(define-public crate-adi-adxl355-1.0.0 (c (n "adi-adxl355") (v "1.0.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "micromath") (r "^2.0") (f (quote ("vector"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0jaz850isgr0v9kmm8jsrpylx7hij2nb4gwbhcfxrwwa3xa2dhbv") (f (quote (("default")))) (s 2) (e (quote (("std" "dep:thiserror") ("defmt" "dep:defmt")))) (r "1.68")))

