(define-module (crates-io ad bl adblock-rust-server) #:use-module (crates-io))

(define-public crate-adblock-rust-server-0.1.0 (c (n "adblock-rust-server") (v "0.1.0") (d (list (d (n "adblock") (r "^0.3.6") (f (quote ("embedded-domain-resolver"))) (k 0)))) (h "1r8m89yhf7zr22hm9xj0y13ggc1g6zandbiz5i0a9psvrv9szq1x")))

(define-public crate-adblock-rust-server-0.2.0 (c (n "adblock-rust-server") (v "0.2.0") (d (list (d (n "adblock") (r "^0.3.6") (f (quote ("embedded-domain-resolver"))) (k 0)) (d (n "attohttpc") (r "^0.16.3") (d #t) (k 0)))) (h "09dbssnb7v9hx3y7dpmjbc1rcf6nilhza0x8gawi94jk62wg4z8k")))

(define-public crate-adblock-rust-server-0.3.0 (c (n "adblock-rust-server") (v "0.3.0") (d (list (d (n "adblock") (r "^0.3.10") (f (quote ("embedded-domain-resolver" "full-regex-handling"))) (k 0)) (d (n "attohttpc") (r "^0.16.3") (d #t) (k 0)))) (h "1as3x5ygzmsi6y1p9ncfnfw8b5p45gi3rphzs56szjglk3asz5iz")))

(define-public crate-adblock-rust-server-0.3.1 (c (n "adblock-rust-server") (v "0.3.1") (d (list (d (n "adblock") (r "^0.3.10") (f (quote ("embedded-domain-resolver" "full-regex-handling"))) (k 0)) (d (n "attohttpc") (r "^0.16.3") (d #t) (k 0)))) (h "04s0c4xg4gj416fybrmxyq1bczkf3r51xfy3cqnw613zywmigj9q")))

