(define-module (crates-io ad d- add-one-boring) #:use-module (crates-io))

(define-public crate-add-one-boring-0.1.0 (c (n "add-one-boring") (v "0.1.0") (h "0zl3in78bs1pbbc0kxkasvk5ba7r9ggivkfrv6kyzi9lxj44zv7y")))

(define-public crate-add-one-boring-0.2.0 (c (n "add-one-boring") (v "0.2.0") (h "0hs0z5dqcgm6j926xq1kr6q0kn0z1i0ixi1szk0p13divpdxgmic")))

(define-public crate-add-one-boring-0.3.0 (c (n "add-one-boring") (v "0.3.0") (h "0cl4nlg2wbhlpzqhzm0mn81yf0p3835gb9419mn08lnk54nh6cf7")))

