(define-module (crates-io ad d- add-header) #:use-module (crates-io))

(define-public crate-add-header-0.1.0 (c (n "add-header") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "13lrrv58adhq63h29ncbmav42msb484cvhc1m7zycanbylm55ph6") (r "1.65")))

(define-public crate-add-header-0.1.1 (c (n "add-header") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0ls2yvjn721g38vv57vz4dd1ilx86044hqf075g8ick9laz9qn4p") (r "1.65")))

(define-public crate-add-header-0.1.2 (c (n "add-header") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "13s3zmnx4shc7p0van4ys0zxgs9s1r8601k6vdi03cybfv2ii9lm") (r "1.65")))

(define-public crate-add-header-0.2.0 (c (n "add-header") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0qdd3snq7hgnvddv8sva3sckiy72v4pxcwnb61xm0206vv8wxzrc") (r "1.65")))

(define-public crate-add-header-0.2.1 (c (n "add-header") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0gc9pz5f4qpvfy3dn74lfam73h6wid4qhyrbvw0qyal3dc5k1rpp") (r "1.65")))

(define-public crate-add-header-0.3.1 (c (n "add-header") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0d94vl2mnx7cxlbh2i916v7r4qxqb42d7cjp1x0i3mk0lm5a6hw5") (r "1.65")))

(define-public crate-add-header-0.4.0 (c (n "add-header") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0qnq49669ia2y5wdzcfvpi116l68nmga538b0pzbgdiwd1hmdv8v") (r "1.65")))

(define-public crate-add-header-0.4.1 (c (n "add-header") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "08h6hxc6f3fbgk81isydcqfrvmcac7kcmhrk2pp1b0z0f5r6swy3") (r "1.65")))

(define-public crate-add-header-0.4.2 (c (n "add-header") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0ij19dwz76wx6ci6rjxiwisbrqmxa1j39aly9k0n7q4sa118gr5l") (r "1.65")))

(define-public crate-add-header-0.4.3 (c (n "add-header") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0msa9w4b1ywv6gmpqzi413h3xp251kg5pb5rn1jbm6z4zp5nvjar") (r "1.65")))

(define-public crate-add-header-0.4.4 (c (n "add-header") (v "0.4.4") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1frhbc7awq9njwlq5pnl73w2bi91rwr4mg6xg8pprfv2l4xfcdx5") (r "1.65")))

(define-public crate-add-header-0.4.5 (c (n "add-header") (v "0.4.5") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "05v9kl9j2a21a4qg5bcfiimasv5law1qwaixfnyxzjmxkhf3wa55") (r "1.65")))

(define-public crate-add-header-0.4.6 (c (n "add-header") (v "0.4.6") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "10h1xwpvmsc7sd5wzgrm8crz2qn17qbkvzrga4kjw7g00003v880") (r "1.74.1")))

(define-public crate-add-header-0.4.7 (c (n "add-header") (v "0.4.7") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1h1f6dqy6ww311qr381skznxj0s4a31nyjy9sgl1n2qawim0m4h1") (r "1.74.1")))

(define-public crate-add-header-0.4.25 (c (n "add-header") (v "0.4.25") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0cf65i6bbm2hscy3chglna0gzfaba1iqdy98zanh18xj06p3lpzp") (r "1.74.1")))

(define-public crate-add-header-0.4.26 (c (n "add-header") (v "0.4.26") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1yh5rpd6vc0bvifa9b98z5z42pmj8qm8483zz9r4wv633ffyfc3s") (r "1.74.1")))

(define-public crate-add-header-0.4.30 (c (n "add-header") (v "0.4.30") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0gynmy9fcp4lx3mxx5w3w3ff730pn691h00p50fjhxq28li70fx2") (r "1.74.1")))

(define-public crate-add-header-0.4.40 (c (n "add-header") (v "0.4.40") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "19qzysjavgvfaghkdmy0lgjnpa8i8dmmbg3czhw00kx6s3gv8abf") (r "1.74.1")))

(define-public crate-add-header-0.5.0 (c (n "add-header") (v "0.5.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0mdjqzbg3rp0ijp9vmx00wga2rszrq80dazn4h97nd3njrbrzqjj") (r "1.74.1")))

(define-public crate-add-header-0.5.1 (c (n "add-header") (v "0.5.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "10ssxjb1niwv8kfga4wrgi694bb3nkdn3mlcvbjwpdd807dz0hbm") (r "1.74.1")))

(define-public crate-add-header-0.5.2 (c (n "add-header") (v "0.5.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0kxm27cjlbw4zgmv1xg5jfdd9mbh8ac4pngx5vb4c17j447z5qif") (r "1.74.1")))

(define-public crate-add-header-0.5.3 (c (n "add-header") (v "0.5.3") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0hgvd76cfrfillzhpxcg905i5qnx44dynr162p91ss8iqa7w7hg1") (r "1.74.1")))

(define-public crate-add-header-0.5.5 (c (n "add-header") (v "0.5.5") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "16qw0blrkxklnk1wn2n4d41rqsbswxy87h0zhk9xi73arx2h0liy") (r "1.74.1")))

(define-public crate-add-header-0.5.10 (c (n "add-header") (v "0.5.10") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0h0r1j3xqn0v79yw25ax83qysnfzl657qalsah6r3n8yrhnp6nv5") (r "1.74.1")))

(define-public crate-add-header-0.6.0 (c (n "add-header") (v "0.6.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "19xnf4f99mj9d66xi1dj34g0w3r8cw0gzrjf3n24pdl7w7rx77qn") (r "1.74.1")))

(define-public crate-add-header-0.6.2 (c (n "add-header") (v "0.6.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1i9in27v59a4kp8bcr9ax91i9iykr7g6qk4jf0zz06hk1p8xqs8n") (r "1.74.1")))

