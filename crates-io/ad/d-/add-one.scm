(define-module (crates-io ad d- add-one) #:use-module (crates-io))

(define-public crate-add-one-0.1.0 (c (n "add-one") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "01zcqp0njp3pvx41q214fbxdd317g4b4jb0sbk01fmrq0jshfb91")))

(define-public crate-add-one-0.1.1 (c (n "add-one") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "03c1bw0v56xyqag9fxx254f81dnjvcyqnqzp5yvr1gg1ny0xyyjw")))

(define-public crate-add-one-0.1.2 (c (n "add-one") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "11pziz457jvzpgakq59cxk65l8gvixd8i5wwr6nni9ibrlsvl0b0")))

(define-public crate-add-one-0.2.0 (c (n "add-one") (v "0.2.0") (h "13zrqwl6jbsn0kralidwa05w3mv79998x8xw3cyxwrjipd0xifni")))

(define-public crate-add-one-0.2.1 (c (n "add-one") (v "0.2.1") (h "14kzjjkvsnh3y64jajcknvdmvn0d433fzf2891hq1yr3jgypj312")))

(define-public crate-add-one-0.2.2 (c (n "add-one") (v "0.2.2") (h "1bj943r0f2mfsywcz6vms1qla17y9pgls57qjsh06y197qi6ybrh")))

(define-public crate-add-one-0.2.3 (c (n "add-one") (v "0.2.3") (h "03jgj36yyyr4w2v4549x5czdw8kcb918q1vwmxqcx72hz2hd45bj")))

(define-public crate-add-one-0.2.4 (c (n "add-one") (v "0.2.4") (h "1c5i6svr35l9v68n72d189x596g2wqc1s758qg2s2gm8sxkiwxnq")))

(define-public crate-add-one-1.0.0 (c (n "add-one") (v "1.0.0") (h "1amirslidzvqamd6z5v56a7cp6d2nnwvfkx5qclhpdk2f880knz1")))

