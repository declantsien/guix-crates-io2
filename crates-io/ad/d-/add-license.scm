(define-module (crates-io ad d- add-license) #:use-module (crates-io))

(define-public crate-add-license-0.1.0 (c (n "add-license") (v "0.1.0") (h "06mn7y5sp9pl2izpa9w5xvrknpb7m1plnv214p8c08dpd82vvs24") (y #t)))

(define-public crate-add-license-0.1.1 (c (n "add-license") (v "0.1.1") (h "1b8rwfx3yslp472gpgzws9hbwfi7f3m42jqrwynw0xp0iqnhfmf3") (y #t)))

(define-public crate-add-license-0.1.2 (c (n "add-license") (v "0.1.2") (h "1mq9n13pin9l764yv08l5p3x165qhgf2g8ia0nyk4gkxylr7ma49") (y #t)))

(define-public crate-add-license-0.1.3 (c (n "add-license") (v "0.1.3") (h "1kz8c0k4x1amjx3wkqaq06lgxpqnvhlpyg9iccm5y4i317f7mprm") (y #t)))

(define-public crate-add-license-0.1.4 (c (n "add-license") (v "0.1.4") (h "1r88ssggbx1a5fl9g4fc1p0dnl7n2wis9wm2rhznxjs4wv6pnp6f") (y #t)))

(define-public crate-add-license-0.2.0 (c (n "add-license") (v "0.2.0") (d (list (d (n "toml") (r "^0.8.13") (d #t) (k 0)))) (h "0qci00cdy8rqmw265f236hd9428k0wb96by76jr8p4mbc9r2h932") (y #t)))

