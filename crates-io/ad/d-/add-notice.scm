(define-module (crates-io ad d- add-notice) #:use-module (crates-io))

(define-public crate-add-notice-0.1.0 (c (n "add-notice") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "03kdzzzp1zspmw5x1v2ni17j5dkv9zrd85fipvgc8c193v7l63rj") (r "1.74.1")))

(define-public crate-add-notice-0.2.0 (c (n "add-notice") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0cwm04pkc478ig58s54p5sm6abpkk3977q9x1k8n829csnwl16qq") (r "1.74.1")))

(define-public crate-add-notice-0.2.1 (c (n "add-notice") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1vjy3df17w1l3rhdlnfmczwxqkg18amqb85hvljpli53285bc4dw") (r "1.74.1")))

