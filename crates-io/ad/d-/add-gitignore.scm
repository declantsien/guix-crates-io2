(define-module (crates-io ad d- add-gitignore) #:use-module (crates-io))

(define-public crate-add-gitignore-0.2.0 (c (n "add-gitignore") (v "0.2.0") (d (list (d (n "console") (r "^0.8.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)))) (h "15ip51d845y9xag9gdf6pbgpyp2mckn8ya510f624fwz64r5gc5h")))

