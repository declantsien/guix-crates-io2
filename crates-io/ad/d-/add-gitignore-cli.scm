(define-module (crates-io ad d- add-gitignore-cli) #:use-module (crates-io))

(define-public crate-add-gitignore-cli-1.0.0 (c (n "add-gitignore-cli") (v "1.0.0") (d (list (d (n "inquire") (r "^0.7.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l7c0nmp4rl7wd1knlrj1idkk59glx9gy08x65prz2bz1qlcbqv8")))

(define-public crate-add-gitignore-cli-1.0.1 (c (n "add-gitignore-cli") (v "1.0.1") (d (list (d (n "inquire") (r "^0.7.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1viwgxf8g3f01616wj9bfdxfzjyi4d5gad3fkcp11kr4rj4a9pv2")))

