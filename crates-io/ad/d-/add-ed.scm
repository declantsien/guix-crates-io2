(define-module (crates-io ad d- add-ed) #:use-module (crates-io))

(define-public crate-add-ed-0.5.0 (c (n "add-ed") (v "0.5.0") (d (list (d (n "regex") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1xjvphmpm1sf95v4hcnxm0p6rbqg00fqimjd03gj311j6isq7llb") (f (quote (("vecbuffer" "regex"))))))

(define-public crate-add-ed-0.5.1 (c (n "add-ed") (v "0.5.1") (d (list (d (n "regex") (r "^1.4") (o #t) (d #t) (k 0)))) (h "11a68q8s0cprkk9142ivrg4agcrn07gvgmmnxq8vai7n5lb783l8") (f (quote (("vecbuffer" "regex"))))))

(define-public crate-add-ed-0.6.0 (c (n "add-ed") (v "0.6.0") (d (list (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1jl0z582m8n0611cilimw0k5msk54w787dklxkz15f5jrxamdqfz") (f (quote (("vecbuffer" "regex"))))))

(define-public crate-add-ed-0.6.1 (c (n "add-ed") (v "0.6.1") (d (list (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1y78qzvly48fi6g9dkhp7a56gnsq73zblc0qqqzd29v1bcyykhnq") (f (quote (("vecbuffer" "regex"))))))

(define-public crate-add-ed-0.7.0 (c (n "add-ed") (v "0.7.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0f18q5slnqnazmlnnpf6hdpbwapx805km1467r0g1smka4cxn4z4") (f (quote (("vecbuffer" "regex"))))))

(define-public crate-add-ed-0.7.1 (c (n "add-ed") (v "0.7.1") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "08vsfanazdsb6wchbczi37pwjv00zaz8bspci4d7mys844qsv7mz") (f (quote (("vecbuffer" "regex")))) (y #t)))

(define-public crate-add-ed-0.7.2 (c (n "add-ed") (v "0.7.2") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0m1l1qp3a1xlz659mga0cr2pzji7i28khzs2249d5f9ajr3mszyd") (f (quote (("vecbuffer" "regex"))))))

(define-public crate-add-ed-0.7.3 (c (n "add-ed") (v "0.7.3") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "11nb561r4n3j48p54n7p9s6rizillxffljgffmm2aa86iky90niw") (f (quote (("vecbuffer" "regex"))))))

(define-public crate-add-ed-0.7.4 (c (n "add-ed") (v "0.7.4") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "06z69ll9x8dml4gzw1kij162qc7vq189x7vdp98gq2vxacx4687p") (f (quote (("vecbuffer" "regex"))))))

(define-public crate-add-ed-0.7.5 (c (n "add-ed") (v "0.7.5") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0c9szmjdswpmb2rh89rb0r8m79nvv838m8y4mv6iss1wfsgxfbxd") (f (quote (("vecbuffer" "regex"))))))

(define-public crate-add-ed-0.7.6 (c (n "add-ed") (v "0.7.6") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0hbch8zjk283ivn3i61h12n04vafybqd9gywf89awa32wxkbc5g2") (f (quote (("vecbuffer" "regex"))))))

(define-public crate-add-ed-0.7.7 (c (n "add-ed") (v "0.7.7") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0dwx5sydwbicnngj9jgnvpxm7q29c145gfwslvlgr8kyk336w84h") (f (quote (("vecbuffer" "regex"))))))

(define-public crate-add-ed-0.8.0 (c (n "add-ed") (v "0.8.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "09xq8mwlqjiivml2czk3d7m7jsmdglf80im9dkc25w57j5q602fq") (f (quote (("vecbuffer" "regex") ("initial_input_data"))))))

(define-public crate-add-ed-0.9.0-alpha (c (n "add-ed") (v "0.9.0-alpha") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "125jcnajzr623mnpha6ndz2bwd0rbjcr9mxdl17mswwp8ba7llkl") (f (quote (("vecbuffer" "regex") ("initial_input_data") ("default" "vecbuffer"))))))

(define-public crate-add-ed-0.9.0-alpha2 (c (n "add-ed") (v "0.9.0-alpha2") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "00x77dywjkx24qck6ilx9g4imx2nqk76px4ycfc58ymxj351clvp") (f (quote (("vecbuffer" "regex") ("initial_input_data") ("default" "vecbuffer"))))))

(define-public crate-add-ed-0.9.0-alpha3 (c (n "add-ed") (v "0.9.0-alpha3") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0dx3nxcwly0w1rp671zkl2l1ymsw9j79ys2vzzj6grx99c1rdjv4") (f (quote (("vecbuffer" "regex") ("initial_input_data") ("default" "vecbuffer"))))))

(define-public crate-add-ed-0.9.0-alpha4 (c (n "add-ed") (v "0.9.0-alpha4") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "1rcmssfp6bl0557005q3zk4z637j4cynn65snsjd517y5ml35hg3") (f (quote (("vecbuffer" "regex") ("initial_input_data") ("default" "vecbuffer"))))))

(define-public crate-add-ed-0.9.0-alpha5 (c (n "add-ed") (v "0.9.0-alpha5") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "16ma89ji1723yaspmv2widpfp4dh6cs8k4vzkcfgwmgwcjcdlwml") (f (quote (("vecbuffer" "regex") ("initial_input_data") ("default" "vecbuffer"))))))

(define-public crate-add-ed-0.9.0-alpha6 (c (n "add-ed") (v "0.9.0-alpha6") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0997g17v0ji9bh74lzw171bsym602xjm550l9r3yrlb1abmq769p") (f (quote (("vecbuffer" "regex") ("initial_input_data") ("default" "vecbuffer"))))))

(define-public crate-add-ed-0.9.0-alpha7 (c (n "add-ed") (v "0.9.0-alpha7") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0xyzhmf8zg5cg49abl6afyny7cy1a7lsj1vnpfjk5hj3hqdb75nm") (f (quote (("vecbuffer" "regex") ("initial_input_data") ("default" "vecbuffer"))))))

(define-public crate-add-ed-0.9.0-alpha8 (c (n "add-ed") (v "0.9.0-alpha8") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0s5nlha2pqrqxkmg6wrxdln1f869wjnvg67mp26akvq1c86kymyb") (f (quote (("vecbuffer" "regex") ("initial_input_data") ("default" "vecbuffer"))))))

(define-public crate-add-ed-0.10.0 (c (n "add-ed") (v "0.10.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1a2cdgyk5pjgl4ks6sl54clx2z1zvnnrm24x15kzbyix8ymdj2sc") (f (quote (("test_local_io" "local_io") ("local_io") ("initial_input_data") ("default" "local_io"))))))

(define-public crate-add-ed-0.11.0-alpha (c (n "add-ed") (v "0.11.0-alpha") (d (list (d (n "as-any") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gqjwy25m1wn7banvh4haf8jssr1b461s6kswikzwpm23zbdk5gk") (f (quote (("test_local_io" "local_io") ("local_io") ("initial_input_data") ("default" "local_io") ("bin_deps" "clap" "local_io"))))))

(define-public crate-add-ed-0.11.0 (c (n "add-ed") (v "0.11.0") (d (list (d (n "as-any") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01hrqh8qkdhc8rifsrhvp5553hqs8nigg519gnknrxhhk1mf423k") (f (quote (("test_local_io" "local_io") ("local_io") ("initial_input_data") ("default" "local_io") ("bin_deps" "clap" "local_io"))))))

(define-public crate-add-ed-0.11.1 (c (n "add-ed") (v "0.11.1") (d (list (d (n "as-any") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09i5dwlmjn82f1d9352a0lq35pryrf1sc90apsr4viv6f78vs67v") (f (quote (("test_local_io" "local_io") ("local_io") ("initial_input_data") ("default" "local_io") ("bin_deps" "clap" "local_io"))))))

(define-public crate-add-ed-0.11.2 (c (n "add-ed") (v "0.11.2") (d (list (d (n "as-any") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xlqn87wb75f4yz5vkjay2kbkn4c4xy60s4p4s09d37918hl56g5") (f (quote (("test_local_io" "local_io") ("local_io") ("initial_input_data") ("default" "local_io") ("bin_deps" "clap" "local_io"))))))

(define-public crate-add-ed-0.12.0 (c (n "add-ed") (v "0.12.0") (d (list (d (n "as-any") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04524hpahr6pzxxby9p9wdxhhvxj42hlravy3jys7n54pdbq3658") (f (quote (("test_local_io" "local_io") ("local_io") ("initial_input_data") ("default" "local_io")))) (s 2) (e (quote (("serde" "dep:serde") ("bin_deps" "dep:clap" "local_io"))))))

(define-public crate-add-ed-0.13.0 (c (n "add-ed") (v "0.13.0") (d (list (d (n "as-any") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10ykxn207jyjvzj5pqb8kjxd402qp1i825bsqf6pirb8d6phlqxv") (f (quote (("test_local_io" "local_io") ("local_io") ("initial_input_data") ("default" "local_io")))) (s 2) (e (quote (("serde" "dep:serde") ("bin_deps" "dep:clap" "local_io"))))))

