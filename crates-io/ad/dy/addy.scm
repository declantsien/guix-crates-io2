(define-module (crates-io ad dy addy) #:use-module (crates-io))

(define-public crate-addy-0.0.1 (c (n "addy") (v "0.0.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "0l8af57w4k445np5y46dw3w4lnxbji9fkx6zmh2g9kgvvhbvdckh")))

(define-public crate-addy-0.1.0 (c (n "addy") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "0v6lnwhjjl676dfxxbnlkp3xfb75z5avk3q176jq4czf8by1kgcl")))

(define-public crate-addy-0.1.1 (c (n "addy") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "0303d8bj6wy0wp8l1ax9mzjz6qn785igvpmmvg7zwpnp0y30arxr")))

