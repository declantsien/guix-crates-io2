(define-module (crates-io ad rg adrg) #:use-module (crates-io))

(define-public crate-adrg-0.1.0 (c (n "adrg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1d47pd0jawlrrnmjfh0wmjssx8n2pkwlsly2xp98i83rjryk3n7v") (y #t)))

