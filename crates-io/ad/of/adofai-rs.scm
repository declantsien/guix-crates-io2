(define-module (crates-io ad of adofai-rs) #:use-module (crates-io))

(define-public crate-adofai-rs-0.1.0 (c (n "adofai-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_jsonrc") (r "^0.1.0") (d #t) (k 0)))) (h "0pmkzbf1d5255rh2vyd2gabvigdi11j792484nsyh8cq93gacj1y")))

