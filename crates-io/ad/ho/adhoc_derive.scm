(define-module (crates-io ad ho adhoc_derive) #:use-module (crates-io))

(define-public crate-adhoc_derive-0.1.0 (c (n "adhoc_derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0cdv6wm24q2sql0byv9rr5fn7m9dwqwcjbyv2gz1mbz0iv32dy91")))

(define-public crate-adhoc_derive-0.1.1 (c (n "adhoc_derive") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "04vcxqcsw1sm49pb2m5y8l6rc59f6csm7yd9b4gq48nck7pi99zm")))

(define-public crate-adhoc_derive-0.1.2 (c (n "adhoc_derive") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "0q8cjd2nbw2cwmsm7w34wrf4f5a03h71s3f52a66r4gfg305kym7")))

