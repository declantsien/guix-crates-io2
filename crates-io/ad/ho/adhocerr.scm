(define-module (crates-io ad ho adhocerr) #:use-module (crates-io))

(define-public crate-adhocerr-0.1.0 (c (n "adhocerr") (v "0.1.0") (h "12q0sy5f4hv699a74ry1x4ik0wa2l4yjwp34xyiw7jw243z2892v")))

(define-public crate-adhocerr-0.1.1 (c (n "adhocerr") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)))) (h "0fj4vj0sn7b8nfk5mmknm6p5kn1i48ny74dbc4wgk8l5jjbmzp5i")))

(define-public crate-adhocerr-0.1.2 (c (n "adhocerr") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)))) (h "1bq4mnfddjjwl0p9xrp0q8g1s6b9xvfg4cajda3rl7wnajz8bpga")))

