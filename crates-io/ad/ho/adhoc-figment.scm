(define-module (crates-io ad ho adhoc-figment) #:use-module (crates-io))

(define-public crate-adhoc-figment-0.1.0 (c (n "adhoc-figment") (v "0.1.0") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)))) (h "1kqjyx2lqq5ygr1ylzg8v8wxr7pskakgs2k0fijgl5ha5h3bj0wa")))

(define-public crate-adhoc-figment-0.1.1-dev (c (n "adhoc-figment") (v "0.1.1-dev") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "figment") (r "^0.10") (f (quote ("toml"))) (d #t) (k 2)))) (h "1g0gbjzc6g4ng4aj8hhmwnzbk9m46mr9d0pn850qxkal0dsx0hjm") (y #t)))

(define-public crate-adhoc-figment-0.1.1 (c (n "adhoc-figment") (v "0.1.1") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "figment") (r "^0.10") (f (quote ("toml"))) (d #t) (k 2)))) (h "1iwwxdryqkzz8lq7givjywzlwylj2qqvy3j5qch9vlc0vsiis7yi")))

