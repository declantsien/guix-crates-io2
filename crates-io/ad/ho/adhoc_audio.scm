(define-module (crates-io ad ho adhoc_audio) #:use-module (crates-io))

(define-public crate-adhoc_audio-0.1.0 (c (n "adhoc_audio") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sc8i2br6jhjb6kj61l1qdc01bs0x7sgf86s5biblcr71c0a7ldd") (f (quote (("default") ("cli" "clap" "rand"))))))

(define-public crate-adhoc_audio-0.1.1 (c (n "adhoc_audio") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q2vz66q5sa5rhr3g5qgknw48ybg7bpdil2vfzbvm6xwj8ml9qvn") (f (quote (("default") ("cli" "clap" "rand"))))))

(define-public crate-adhoc_audio-0.1.2 (c (n "adhoc_audio") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "077yaa8zraznjicms385d1f8m2ppjqz8msvslcnwyscm3v2z2wmg") (f (quote (("default") ("cli" "clap" "rand"))))))

(define-public crate-adhoc_audio-0.1.3 (c (n "adhoc_audio") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lgh1cjwi89ski2mpm76n9mci5vz5d2vrcjyvy9yrs9i3h9b9r34") (f (quote (("default") ("cli" "clap" "rand"))))))

(define-public crate-adhoc_audio-0.1.4 (c (n "adhoc_audio") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cg4phwlkvrf86zzdwc32aasdhskyjqdd2yjc3pcndazh1gjx48h") (f (quote (("default" "cli") ("cli" "clap" "rand" "rayon"))))))

