(define-module (crates-io ad f4 adf4351) #:use-module (crates-io))

(define-public crate-adf4351-0.1.0 (c (n "adf4351") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.3") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f4") (r "^0.12.1") (f (quote ("stm32f407" "rt"))) (d #t) (k 2)) (d (n "stm32f4xx-hal") (r "^0.8.3") (f (quote ("stm32f407" "rt"))) (d #t) (k 2)))) (h "0ivhkxqsymhr94mvl2ljpm1inwx8yq9a2xpf2mlh7bnx8mg15mh5")))

