(define-module (crates-io ad ap adaptive_backoff) #:use-module (crates-io))

(define-public crate-adaptive_backoff-0.1.0 (c (n "adaptive_backoff") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11cjn8352pc7q37gi2nkmcdjm6sbrcn1yy1n75bdnijbk7jhy7ip")))

(define-public crate-adaptive_backoff-0.2.0 (c (n "adaptive_backoff") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qbpff5393cz04ki1060xs6xyahnv5xndj8lic9f7qpzcld2xh01")))

(define-public crate-adaptive_backoff-0.2.1 (c (n "adaptive_backoff") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "016zzv0229a3gf52a4wclaih9y9z3432in96bj1hpi5n1m5gw4vc")))

