(define-module (crates-io ad ap adapton-lab) #:use-module (crates-io))

(define-public crate-adapton-lab-0.1.0 (c (n "adapton-lab") (v "0.1.0") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ap2r919p3md2gxdl64ymggynpd7n68xflx8iw0bpk7iwgfnlv6m")))

