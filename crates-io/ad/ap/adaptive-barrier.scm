(define-module (crates-io ad ap adaptive-barrier) #:use-module (crates-io))

(define-public crate-adaptive-barrier-0.1.0 (c (n "adaptive-barrier") (v "0.1.0") (h "003ygsiqsd85v0p846q1ym23dbp4iagn89p7k6yrvbg9di1mbjqc")))

(define-public crate-adaptive-barrier-1.0.0 (c (n "adaptive-barrier") (v "1.0.0") (h "1004swrxg9g755h0sk0y1kclk4y9hzk6dzl8772df4l4j44gqz8w")))

