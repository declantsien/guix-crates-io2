(define-module (crates-io ad ap adapton) #:use-module (crates-io))

(define-public crate-adapton-0.0.1 (c (n "adapton") (v "0.0.1") (h "0ppdnmcclzs72hn3zpx7vnrn717d82rbvhbdkfvxdycbx2ffb9g8")))

(define-public crate-adapton-0.1.1 (c (n "adapton") (v "0.1.1") (h "1lln0414wlbda5ga93ch7c7sg4zf55yh9bgszq2wvy023i4z29wz")))

(define-public crate-adapton-0.2.1 (c (n "adapton") (v "0.2.1") (d (list (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.24") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1hj2jxp3fspxzmby2h7jy4djx3vfi7bjv0l1vkqskjqaxcdr9dlp")))

(define-public crate-adapton-0.2.2 (c (n "adapton") (v "0.2.2") (d (list (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.24") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0mlcp75z56yjn9xbsga1vjkxp33fpicmdzh72k7p6s1jzvy75yq1")))

(define-public crate-adapton-0.2.3 (c (n "adapton") (v "0.2.3") (d (list (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.24") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0h0ypc2m0giwqqp8i1r6aqf49xrphkr976aicnmav9k8y1sklqg4")))

(define-public crate-adapton-0.2.4 (c (n "adapton") (v "0.2.4") (d (list (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "166kxgpggq2wn20vwavbk3j1i1m6zm61514s2gyplgvba7cpgqkj")))

(define-public crate-adapton-0.2.5 (c (n "adapton") (v "0.2.5") (h "17ma88f1p0193nldmn5zvs59ij6l8nh33c5glp4q56p1cy7sdyvl")))

(define-public crate-adapton-0.2.6 (c (n "adapton") (v "0.2.6") (h "0s4r7f0c15dghwzgqad823msb8v3rvfr3smrzvxzy4ca37h9008l")))

(define-public crate-adapton-0.2.7 (c (n "adapton") (v "0.2.7") (h "1lhja89mn5sb82gvys0h94y4441q9qsffkwyk74vm10jd69lrm4y")))

(define-public crate-adapton-0.3.0 (c (n "adapton") (v "0.3.0") (h "1bvr3rm2n5hgf8hpvac4y2s8ywjr7dl7afvv1z4a16ck386hxd05")))

(define-public crate-adapton-0.3.1 (c (n "adapton") (v "0.3.1") (h "1whm5x3xkly1dhy95abcgajljkbjldrykn249qc6sgh3fla6159p")))

(define-public crate-adapton-0.3.2 (c (n "adapton") (v "0.3.2") (h "0sv0xbdzqb3fhwiinnsr94qzzx2p0p0pxp60hv9fbfbmfxx4z7sm")))

(define-public crate-adapton-0.3.3 (c (n "adapton") (v "0.3.3") (h "1lpkp7hs86w0x9nz44dqhjm7m76pwmmwq7v02zalnsf49hpfg84a")))

(define-public crate-adapton-0.3.4 (c (n "adapton") (v "0.3.4") (h "1kdddsm2b2c26dvq2la61bjzq124pyclhkzz0qw23zchs7znxqq7")))

(define-public crate-adapton-0.3.5 (c (n "adapton") (v "0.3.5") (h "084vr18lj7rlfw41ksgxmivgb2garb02w24328bfyhlj3xsps5qs")))

(define-public crate-adapton-0.3.6 (c (n "adapton") (v "0.3.6") (h "08vliqr13a56i235ilrf9nrkn86c009kcs38vwwqy3x8516i6gl3")))

(define-public crate-adapton-0.3.7 (c (n "adapton") (v "0.3.7") (h "16y1khi1ks92laqbazblq8i2va5cf59zvdz2hy8cawqv0q5fyjq3")))

(define-public crate-adapton-0.3.8 (c (n "adapton") (v "0.3.8") (h "16zxfy5l33qdgawi24frrbjgg3fj5rh2qyxppyayzcqj5w1sbmyq")))

(define-public crate-adapton-0.3.9 (c (n "adapton") (v "0.3.9") (h "1f0mli0zlnaxk88v2pry802vpnad5i75lpjrnbk9gffl29ppmssa")))

(define-public crate-adapton-0.3.10 (c (n "adapton") (v "0.3.10") (h "122pn2sqjadknrvh2ak3zls8h0wrdnmn762z7jk4wq7109ks36ih")))

(define-public crate-adapton-0.3.11 (c (n "adapton") (v "0.3.11") (h "16i5grmzinl6lkmyd8qh0fm28cqq0z1c695h35qjzrvzjkc0v5s6")))

(define-public crate-adapton-0.3.12 (c (n "adapton") (v "0.3.12") (h "1x7lm4x4vivgb9xv2k526w5mz0dnmqghiddd9ms23zypg62l6xqz")))

(define-public crate-adapton-0.3.13 (c (n "adapton") (v "0.3.13") (h "1nch8apw7h16rdwdc0hwp3zvdp820ibxk628rxzlhsrzvz1gsawr")))

(define-public crate-adapton-0.3.14 (c (n "adapton") (v "0.3.14") (h "1s3sk1l8rkhlpz315b8pb8ap0hsczriji9jgbz1gwi19axh9pm5w")))

(define-public crate-adapton-0.3.15 (c (n "adapton") (v "0.3.15") (h "0gcyg671vnrjshmyqp901x5f9v4cimxyf2l0qarz8spc8jg6lkdk")))

(define-public crate-adapton-0.3.16 (c (n "adapton") (v "0.3.16") (h "0cmhv9cfvhjj6gshfialdkkjny5ncd9x1p6fk8y3bknqz9dihcnc")))

(define-public crate-adapton-0.3.17 (c (n "adapton") (v "0.3.17") (h "0bs4sw5kbq1hhs2zkw0g6j57373gy4vv30gjalpsif36f3jqmkqd")))

(define-public crate-adapton-0.3.18 (c (n "adapton") (v "0.3.18") (h "0dw21kmagarchnjn239x3x669nld2jrq4f35qrhmwqr7d23fsnrc")))

(define-public crate-adapton-0.3.19 (c (n "adapton") (v "0.3.19") (h "05rpxy31dmm6pa81qvx6zdrg65d26zv35vg3dbzgvsn7wrc1gjrh")))

(define-public crate-adapton-0.3.20 (c (n "adapton") (v "0.3.20") (h "1sa7720rfny7pr58mzxgj1vs98sk3hh0fbf72bqjg7aa6x0amyic")))

(define-public crate-adapton-0.3.21 (c (n "adapton") (v "0.3.21") (h "13a2xniwrnbck6b2ds4c3jl9yi1sjbmn79y3s62wn8sx662xvl1p")))

(define-public crate-adapton-0.3.22 (c (n "adapton") (v "0.3.22") (h "0c3asn5b3wbrzc0wsscs6v3a2lyfkz4qw4rdfrh71jc0bm6krhhg")))

(define-public crate-adapton-0.3.23 (c (n "adapton") (v "0.3.23") (h "1q01qhqpprrqwnxr3fnkj7b0m19c3l0q2y1x6g1sl38cy5jzar9b")))

(define-public crate-adapton-0.3.24 (c (n "adapton") (v "0.3.24") (h "182lx74j1mlfc3yryp2cv20ap3vki9k9iwar2s5kwwzvjpihm0y7")))

(define-public crate-adapton-0.3.25 (c (n "adapton") (v "0.3.25") (h "1s3gkrvf74rmmvdb84mkyqfggdib92dsx5h05x08cdxm3zirqj7f")))

(define-public crate-adapton-0.3.26 (c (n "adapton") (v "0.3.26") (h "05qkw7j5acbja97dxsky5pjcq3ay7qq63zr7bsdzv8q515c0r2b8")))

(define-public crate-adapton-0.3.27 (c (n "adapton") (v "0.3.27") (h "1hxi1ld097lbmy6f912fj8i3jglzqb4m3h7pk0z28y8l79p8kchl")))

(define-public crate-adapton-0.3.28 (c (n "adapton") (v "0.3.28") (h "108nqqqmmrnl5rsdvz3774r3q498vfpjsfknswvhzayh6hgi10h7")))

(define-public crate-adapton-0.3.29 (c (n "adapton") (v "0.3.29") (h "0nqd6q3g7cvgbyr8adj109hzgcr6z72ic3xffxdqm11ijfaibhgm")))

(define-public crate-adapton-0.3.30 (c (n "adapton") (v "0.3.30") (h "089c66ah26mxng10s27akn1qpi8ahyh88l7jc8rnw397bnfgsq7n")))

(define-public crate-adapton-0.3.31 (c (n "adapton") (v "0.3.31") (h "0j4s1i7v7nd0gm0491nfi1yh708pk0mh5y8v80p8m5gl17y8h74q")))

