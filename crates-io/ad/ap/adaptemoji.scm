(define-module (crates-io ad ap adaptemoji) #:use-module (crates-io))

(define-public crate-adaptemoji-0.1.0 (c (n "adaptemoji") (v "0.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.25") (f (quote ("default-formats"))) (k 0)))) (h "1h44d4addld38zpsgkc8dfpr5615610ijbhz8l0bn48r2as4ya9k") (f (quote (("rayon" "image/rayon") ("full" "rayon" "cli") ("default" "full")))) (s 2) (e (quote (("cli" "dep:clap")))) (r "1.74")))

(define-public crate-adaptemoji-0.1.1 (c (n "adaptemoji") (v "0.1.1") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.25") (f (quote ("default-formats"))) (k 0)))) (h "0ff8mgh6n2qw20zbghk7gkasxy2k8h72vmv141v5al5aqi053s92") (f (quote (("rayon" "image/rayon") ("full" "rayon" "cli") ("default" "full")))) (s 2) (e (quote (("cli" "dep:clap")))) (r "1.74")))

