(define-module (crates-io ad er aderyn_driver) #:use-module (crates-io))

(define-public crate-aderyn_driver-0.0.8 (c (n "aderyn_driver") (v "0.0.8") (d (list (d (n "aderyn_core") (r "^0.0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "1k0xby7i9xybxvppsmniri9j8hfc0bn4n9p6l81yx4jz66qg03rn")))

(define-public crate-aderyn_driver-0.0.9 (c (n "aderyn_driver") (v "0.0.9") (d (list (d (n "aderyn_core") (r "^0.0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "0wixfn90ca3x3g1q33lypcs2h5bffid0yxcrv4gl6d98s4r964v0")))

(define-public crate-aderyn_driver-0.0.10 (c (n "aderyn_driver") (v "0.0.10") (d (list (d (n "aderyn_core") (r "^0.0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0bc6j2f67gqhifgxgfxxz37mwp988xlw7cvw3n00idsbh563mfib")))

(define-public crate-aderyn_driver-0.0.11 (c (n "aderyn_driver") (v "0.0.11") (d (list (d (n "aderyn_core") (r "^0.0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1fi6y900yfv8y0dg51nsvwmi2fzs1whqadvhrmjwmwq639dy72wf")))

(define-public crate-aderyn_driver-0.0.12 (c (n "aderyn_driver") (v "0.0.12") (d (list (d (n "aderyn_core") (r "^0.0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0359gqd1822axvy1m6a5xmd8vvh3z9j6ygwivvp4898xwjmlwi15")))

(define-public crate-aderyn_driver-0.0.13 (c (n "aderyn_driver") (v "0.0.13") (d (list (d (n "aderyn_core") (r "^0.0.13") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "01v149gndm2aj9yfyc63il4x3h5kbb77axpc103apq6mi29p612r")))

(define-public crate-aderyn_driver-0.0.14 (c (n "aderyn_driver") (v "0.0.14") (d (list (d (n "aderyn_core") (r "^0.0.14") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "11pf7mwkydj71m8jqnlqcydv4ladsfizjj80rrq7vnjf5i2wgw1d")))

(define-public crate-aderyn_driver-0.0.15 (c (n "aderyn_driver") (v "0.0.15") (d (list (d (n "aderyn_core") (r "^0.0.15") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1k996nh4ic83r8ljacp9wwphszzij144mkj60crn26hlxzq5dcyj")))

(define-public crate-aderyn_driver-0.0.16 (c (n "aderyn_driver") (v "0.0.16") (d (list (d (n "aderyn_core") (r "^0.0.16") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0wp7j381s4rvqd2gpzq546px4d2jidy4yq1psfb91n35pz7bvhss")))

(define-public crate-aderyn_driver-0.0.17 (c (n "aderyn_driver") (v "0.0.17") (d (list (d (n "aderyn_core") (r "^0.0.17") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0zyzbp7ij50zfmzbli66f0gvxn7w4d96y9yqplswhb1b6zcwca1v")))

(define-public crate-aderyn_driver-0.0.18 (c (n "aderyn_driver") (v "0.0.18") (d (list (d (n "aderyn_core") (r "^0.0.18") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "075a273xis0qzdldbcxiff8lf28yh39jnawclmv9py9divdhqxjs")))

(define-public crate-aderyn_driver-0.0.19 (c (n "aderyn_driver") (v "0.0.19") (d (list (d (n "aderyn_core") (r "^0.0.19") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1kpmm7irrm2zyx99a3f175svi4p1wzd7xpism7pl9nf83p6l34ak")))

(define-public crate-aderyn_driver-0.0.20 (c (n "aderyn_driver") (v "0.0.20") (d (list (d (n "aderyn_core") (r "^0.0.20") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0xg5sn6k8420f71dri73ybxr4cfnwqsmj9mjwghs11ldvnjfc4qa")))

(define-public crate-aderyn_driver-0.0.21 (c (n "aderyn_driver") (v "0.0.21") (d (list (d (n "aderyn_core") (r "^0.0.21") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0msxpqy6c51khzlv6drkw4a3y8lbcp1n9fcdlchr9cp8si1gkl5f")))

(define-public crate-aderyn_driver-0.0.22 (c (n "aderyn_driver") (v "0.0.22") (d (list (d (n "aderyn_core") (r "^0.0.22") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0in0y2qla316yi44pm08jdyb5ypxfxaq51c7772bxzj5368raycp")))

(define-public crate-aderyn_driver-0.0.23 (c (n "aderyn_driver") (v "0.0.23") (d (list (d (n "aderyn_core") (r "^0.0.23") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1ci4iqavfc9rzz01a8xfsx5nsbwsid564h669slsnd4aqkp4lw5r")))

(define-public crate-aderyn_driver-0.0.24 (c (n "aderyn_driver") (v "0.0.24") (d (list (d (n "aderyn_core") (r "^0.0.24") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "17m3ddd1zmx95591i58czr9bbayz363m8d0cl3ssg0bgxz6bsxpg")))

(define-public crate-aderyn_driver-0.0.25 (c (n "aderyn_driver") (v "0.0.25") (d (list (d (n "aderyn_core") (r "^0.0.25") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1r71nb19h2crbsxi10j1763d194bdgg0r86jsgda54d2ih7z834s")))

(define-public crate-aderyn_driver-0.0.26 (c (n "aderyn_driver") (v "0.0.26") (d (list (d (n "aderyn_core") (r "^0.0.26") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "17qppqjwmlnsn8ci0zs8dzkcggayr5w8k26zwrcx4p56z0cpfbav")))

(define-public crate-aderyn_driver-0.0.27 (c (n "aderyn_driver") (v "0.0.27") (d (list (d (n "aderyn_core") (r "^0.0.27") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "cyfrin-foundry-compilers") (r "^0.3.20-aderyn") (f (quote ("svm-solc"))) (d #t) (k 0)) (d (n "foundry-config") (r "^0.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)))) (h "1fsbbxn3mriz5lp8amcj9f2jswp78z4kdrgbgkw89j8lrcidw9vg")))

(define-public crate-aderyn_driver-0.0.28 (c (n "aderyn_driver") (v "0.0.28") (d (list (d (n "aderyn_core") (r "^0.0.28") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "cyfrin-foundry-compilers") (r "^0.3.20-aderyn") (f (quote ("svm-solc"))) (d #t) (k 0)) (d (n "foundry-config") (r "^0.2.0") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "toml") (r "^0.8.13") (d #t) (k 0)))) (h "1j6bn8kw97rzb79bcbd79iac33v9x7vysg8m34sq4v09kdc74mzi")))

