(define-module (crates-io ad er aderyn_py) #:use-module (crates-io))

(define-public crate-aderyn_py-0.0.12 (c (n "aderyn_py") (v "0.0.12") (d (list (d (n "aderyn_driver") (r "^0.0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "1zmnxkdlpqwzb9li0256j6ciwbd8g7q2ws3k65v8yvm4gwyhimgd")))

(define-public crate-aderyn_py-0.0.13 (c (n "aderyn_py") (v "0.0.13") (d (list (d (n "aderyn_driver") (r "^0.0.13") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "08yv39fg35pb9bddzg4cp7w5nfv3n09w3p7rsl3z3zisr6m1hwgh")))

(define-public crate-aderyn_py-0.0.20 (c (n "aderyn_py") (v "0.0.20") (d (list (d (n "aderyn_driver") (r "^0.0.20") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "18dpzbhh1czd6z14l1gz6gkri1w4dpdz1zk523bzpix3v1cvm51z")))

(define-public crate-aderyn_py-0.0.21 (c (n "aderyn_py") (v "0.0.21") (d (list (d (n "aderyn_driver") (r "^0.0.21") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "0y1lza172dg2ng0fwl5cr25yfzsc2bzx73mvx0s7n2i8ygbiafjg")))

(define-public crate-aderyn_py-0.0.22 (c (n "aderyn_py") (v "0.0.22") (d (list (d (n "aderyn_driver") (r "^0.0.22") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "1qdfhkjf14kwfiyqh4pnanp68cjk48jsnf54qmfa6p5bnrjdln99")))

(define-public crate-aderyn_py-0.0.23 (c (n "aderyn_py") (v "0.0.23") (d (list (d (n "aderyn_driver") (r "^0.0.23") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "050gz9xv546qbq9zsd5h4v6wjl2s8r4vagca3l6awrmc9f93rwz9")))

(define-public crate-aderyn_py-0.0.24 (c (n "aderyn_py") (v "0.0.24") (d (list (d (n "aderyn_driver") (r "^0.0.24") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "1dip1bjf161jnnijg1hg3vkzli2pxijkxjh88gsrjdj8yds5ch9a")))

(define-public crate-aderyn_py-0.0.25 (c (n "aderyn_py") (v "0.0.25") (d (list (d (n "aderyn_driver") (r "^0.0.25") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "1x2qf518ywa0sl8pj3a3238nzxsrci3w3pyylldfjsi3n7l49xdq")))

(define-public crate-aderyn_py-0.0.26 (c (n "aderyn_py") (v "0.0.26") (d (list (d (n "aderyn_driver") (r "^0.0.26") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "09hmrgmvsf3a7rzf7v8dalrvjcrx05zrf74l62gay3m962ddbx9v")))

(define-public crate-aderyn_py-0.0.27 (c (n "aderyn_py") (v "0.0.27") (d (list (d (n "aderyn_driver") (r "^0.0.27") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "10631kmrbcmpaly27jn4x24f87x5l98br1vs3yndj29pzs8p859q")))

(define-public crate-aderyn_py-0.0.28 (c (n "aderyn_py") (v "0.0.28") (d (list (d (n "aderyn_driver") (r "^0.0.28") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "07wafcy6wqq12m73bmicxwrxxzdycx13w6hly2b2031a0w86z7w2")))

