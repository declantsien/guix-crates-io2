(define-module (crates-io ad d_ add_macro) #:use-module (crates-io))

(define-public crate-add_macro-0.1.0 (c (n "add_macro") (v "0.1.0") (d (list (d (n "display_derive") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0v92i1awsgzbzjyr6pqqckig0ybj7k9rq08kx012vig2jd1kqpfn") (y #t)))

(define-public crate-add_macro-0.1.1 (c (n "add_macro") (v "0.1.1") (d (list (d (n "display_derive") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "13ps7r5ziyngyp8s8ql24ajrxj75rzn63z9s87yvlxzmyc7i0ckv") (y #t)))

(define-public crate-add_macro-0.1.3 (c (n "add_macro") (v "0.1.3") (d (list (d (n "display_derive") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0fy5ly84fiw9q4fg6xg6sk9zgifn3c7s2srcpclvcwwix7ckdw2v") (y #t)))

(define-public crate-add_macro-0.1.4 (c (n "add_macro") (v "0.1.4") (d (list (d (n "display_derive") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0d56ih061n4ncnacgw8fv4ihwdvywzg01lial8zypzmjwdz9z6b5") (y #t)))

(define-public crate-add_macro-0.1.5 (c (n "add_macro") (v "0.1.5") (d (list (d (n "display_derive") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "056rvdk285whnkay42fhm2sh3x7ifwpb8rc1bw91iqbj23yp2f9x") (y #t)))

(define-public crate-add_macro-0.1.6 (c (n "add_macro") (v "0.1.6") (d (list (d (n "display_derive") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "15y12pjs09mwig59xksncy5j5qdwwpqhyrf2r6lhrl7r6iqv0bz7") (y #t)))

(define-public crate-add_macro-0.1.7 (c (n "add_macro") (v "0.1.7") (d (list (d (n "add_macro-display_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0nlrhgn2bvdy77y6dak94nhwfpld7vkwc9d9613nms6pcw3m66pc")))

(define-public crate-add_macro-0.1.8 (c (n "add_macro") (v "0.1.8") (d (list (d (n "add_macro-display_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "add_macro-parse_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1mzz2z6bx9swvy55jiaxpqjx4pn5sp60pbr9kzbwgy2zrbwr22b0")))

(define-public crate-add_macro-0.1.9 (c (n "add_macro") (v "0.1.9") (d (list (d (n "add_macro-display_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "add_macro-parse_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1pz8amfl081bj0ic8i1w7qfgadq3bf5zvnw2nipbzz2nbcb1cg4s")))

(define-public crate-add_macro-0.2.0 (c (n "add_macro") (v "0.2.0") (d (list (d (n "add_macro-display_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "add_macro-parse_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "15bwdbzk3c48pv984pkdl97z81sl6q3c3m2qa07nwf1b63bzr2y3")))

(define-public crate-add_macro-0.2.1 (c (n "add_macro") (v "0.2.1") (d (list (d (n "add_macro-display_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "add_macro-parse_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0gyjds8b9crvynwzx508750y5dlma198234issmwkcwm860a3434")))

(define-public crate-add_macro-0.2.2 (c (n "add_macro") (v "0.2.2") (d (list (d (n "add_macro-display_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "add_macro-parse_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "03y3m1ph135hlq6nkdn9bx7hw3gf03chj3gb1cn81a3pffrp3jq5")))

