(define-module (crates-io ad d_ add_macro-parse_impl) #:use-module (crates-io))

(define-public crate-add_macro-parse_impl-0.1.0 (c (n "add_macro-parse_impl") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vzn1xpfz9fbdpn7wv2xjp40qjp9m3kzld88bmaan4m9sjj4qqhf")))

(define-public crate-add_macro-parse_impl-0.1.1 (c (n "add_macro-parse_impl") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rz9difz14bixyvjj8y0ihwm8xpqpl81p9a7n9lzm1zfd3lylz7d")))

