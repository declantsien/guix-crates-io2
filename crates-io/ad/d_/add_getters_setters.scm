(define-module (crates-io ad d_ add_getters_setters) #:use-module (crates-io))

(define-public crate-add_getters_setters-0.1.0 (c (n "add_getters_setters") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1sgs5sjii6r8sax910ndp6hgpzqm0qsfajswk8aabjgdgimvhmkq")))

(define-public crate-add_getters_setters-0.1.1 (c (n "add_getters_setters") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0iqjd9s6960j3s2q8cx306cgiq6qykynqh42526piknl7s44qf5m")))

(define-public crate-add_getters_setters-0.1.2 (c (n "add_getters_setters") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0d7jh2bq7ns945yl8m2v90py23c7k3hcgl2kwf0hzjqfgx9ks62q")))

(define-public crate-add_getters_setters-0.1.3 (c (n "add_getters_setters") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "00lmnh2b5cm9iygjr8f6vgj4x22ippjpi0j6lix0mq3wcpcdm1q8")))

(define-public crate-add_getters_setters-0.1.4 (c (n "add_getters_setters") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0kw1ksg4rx4w4c7qwayxn0fzwf6fmc3lhhz0z5qn7d0n5fsyaf8i")))

(define-public crate-add_getters_setters-0.1.5 (c (n "add_getters_setters") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "015qqx8rc3fli81y3w72dj10nmql282hbwzfk071gbpl5909l5mm")))

(define-public crate-add_getters_setters-0.1.6 (c (n "add_getters_setters") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mvvvxvzbmvsdfsfs97y6pyzf34nvpphn2sfrzfn6v6ld9blc5kx")))

(define-public crate-add_getters_setters-1.0.0 (c (n "add_getters_setters") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1a8hr3sd99n42rsvwcxy4h3jxs3jb4cqbix6g05dg8j25i6k598b")))

(define-public crate-add_getters_setters-1.0.1 (c (n "add_getters_setters") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mx4pj6z0vri8vhjm866yi5y71xax8z6nxyqp27a0i8bsgwq3zvl")))

(define-public crate-add_getters_setters-1.1.0 (c (n "add_getters_setters") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "13zjv2f2gqhjhj7r82mwn4d00ssg555lkljrg7c5551qfxy12i8f")))

(define-public crate-add_getters_setters-1.1.1 (c (n "add_getters_setters") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0fs1frz2gpb4jmvnsbz3fzp45y69f0324hadn6f8234kqhkggjn7")))

(define-public crate-add_getters_setters-1.1.2 (c (n "add_getters_setters") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02ga0hlnhk5b1ihfyharb2z9f11gafy4ilf44rh2njvln5x6gpzg")))

