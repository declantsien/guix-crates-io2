(define-module (crates-io ad d_ add_macro-display_impl) #:use-module (crates-io))

(define-public crate-add_macro-display_impl-0.1.0 (c (n "add_macro-display_impl") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gjc4bm83sjprkn7d7n8b59swfrccjg6my2b58pj6x4c49ibi00x")))

(define-public crate-add_macro-display_impl-0.1.1 (c (n "add_macro-display_impl") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1q35r025gf23fp37ixfp7d4isjcabxbdn73flg6f7n2xf3ri1wlg")))

