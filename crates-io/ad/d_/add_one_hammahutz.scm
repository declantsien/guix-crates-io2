(define-module (crates-io ad d_ add_one_hammahutz) #:use-module (crates-io))

(define-public crate-add_one_hammahutz-0.1.0 (c (n "add_one_hammahutz") (v "0.1.0") (h "0la1ir74szc4yrazgz2afd3sgzg0j4s653s24n0xnh7wn498lclp")))

(define-public crate-add_one_hammahutz-0.1.1 (c (n "add_one_hammahutz") (v "0.1.1") (h "01s36c81rdrf3frzmfg9r8wfgaj5y8ha2yby5bnp2yijnrkavd13")))

