(define-module (crates-io ad d_ add_one_john) #:use-module (crates-io))

(define-public crate-add_one_john-0.1.0 (c (n "add_one_john") (v "0.1.0") (h "01xg53afrzzk8s9l1d5kqkz008zbjam81c9b0m2ig764n2f5sv4s")))

(define-public crate-add_one_john-1.0.0 (c (n "add_one_john") (v "1.0.0") (h "1jihq2cyzjinfhlqypyz4gvqq1pfw3ia3kxca6bqrvjplfiky627") (y #t)))

