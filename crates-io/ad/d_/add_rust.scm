(define-module (crates-io ad d_ add_rust) #:use-module (crates-io))

(define-public crate-add_rust-0.1.0 (c (n "add_rust") (v "0.1.0") (h "1impkjvzx99cpd4s76cbb9ahdfgxpgkm5z48ravznhnrrcxcypiw")))

(define-public crate-add_rust-0.1.1 (c (n "add_rust") (v "0.1.1") (h "1d7r2d1azh23p0mr1mmfgg1ary004ysmw1vqd52bcq8hvn0a7yn9")))

