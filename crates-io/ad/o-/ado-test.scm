(define-module (crates-io ad o- ado-test) #:use-module (crates-io))

(define-public crate-ado-test-0.1.0 (c (n "ado-test") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.149") (d #t) (k 0)))) (h "0wjhgd60033rn9mxyshylia9j58v308jkfd713wmq0im7d523w8q")))

(define-public crate-ado-test-0.2.0 (c (n "ado-test") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.149") (d #t) (k 0)))) (h "1wkcgkn3irvw31ild53l5hnn7yh7ns3j8d55m8cf32y8g4d810yd")))

