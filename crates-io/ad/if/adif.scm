(define-module (crates-io ad if adif) #:use-module (crates-io))

(define-public crate-adif-0.1.0 (c (n "adif") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "lexical") (r "^5.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cp18ikcvzyymnh7mvdwvkajpky3g7yzvhq2raygp2rpkyggsk5z")))

(define-public crate-adif-0.1.1 (c (n "adif") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "lexical") (r "^5.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xkpg71hbkx6mwnmhgy6lmvcl4nnq6hl9k4ab0i8m8qf2crgbi0w")))

(define-public crate-adif-0.1.2 (c (n "adif") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "lexical") (r "^5.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0skdz2zwq6sjm8c3fjrz9s88ng4wx4k08x7hzwjf08qfinss7qkd")))

(define-public crate-adif-0.1.3 (c (n "adif") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "lexical") (r "^5.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08598ql74zj616i0an2li9hj35r6hhd58ph059ifi7kal2fw33sa")))

