(define-module (crates-io ad vt advtools) #:use-module (crates-io))

(define-public crate-advtools-0.2.0 (c (n "advtools") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "odds") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "0nib82s27i5fza51r6n4ri5bsmgqmpm7f76c1an1j8jvrf6vkcf1")))

(define-public crate-advtools-0.2.1 (c (n "advtools") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "odds") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1xrn7k8a5k9r44pxyhlgzh420kjglps38vw8ri6sry87jm15bick")))

(define-public crate-advtools-0.2.2 (c (n "advtools") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "odds") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "08bx8kfbbqr2bn700gvrd8pkv47w0jnbskp1fbqqr8klzc4g0jmg")))

(define-public crate-advtools-0.2.3 (c (n "advtools") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.4.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)) (d (n "odds") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0vifkhjig616gk14yc3zqrih8kq6gzc3bn3c8h407248yl8z8sy2")))

(define-public crate-advtools-0.3.0 (c (n "advtools") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.4.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)) (d (n "odds") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "160d1x12abdsfgp3y84ca1psyi1prq3dp54rwqvv18130695726k")))

(define-public crate-advtools-0.3.1 (c (n "advtools") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.4.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)) (d (n "odds") (r "^0.3.1") (f (quote ("std-string" "std-vec"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1ypl9dziswpmp3n8dnkam6f986809l4argizjv3gjapcrmqks8jv")))

(define-public crate-advtools-0.4.0 (c (n "advtools") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.4.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)) (d (n "odds") (r "^0.3.1") (f (quote ("std-string" "std-vec"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "17nfdnvmfkcdj18hvrmzh4ijwgsadf8xg9li358cp4kjz0phss23")))

(define-public crate-advtools-0.5.0 (c (n "advtools") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6.3") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "odds") (r "^0.3.1") (f (quote ("std-string" "std-vec"))) (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "120121a92n7nwvl9ycbck6va67iw944rhc1plv98cccs0dyyrmlm")))

(define-public crate-advtools-0.6.0 (c (n "advtools") (v "0.6.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "odds") (r "^0.4.0") (f (quote ("std-string" "std-vec"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1kl3gicjpq9imcbqh3sfh8jq7a3n757l4pgl4ihn96qiqizymn4r")))

(define-public crate-advtools-0.7.0 (c (n "advtools") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "odds") (r "^0.4.0") (f (quote ("std-string" "std-vec"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1r2rjfsqczdr2lsn7rp8n69d59pyp1v8brmazmv9ckrwvcqq9w7k")))

(define-public crate-advtools-0.8.0 (c (n "advtools") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "odds") (r "^0.4.0") (f (quote ("std-string" "std-vec"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1hn14hxj77q4dw6y83s341niicvd4n2z6443hx1sfj369v2afb7j")))

