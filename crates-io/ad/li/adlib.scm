(define-module (crates-io ad li adlib) #:use-module (crates-io))

(define-public crate-adlib-0.1.0 (c (n "adlib") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "1likh7sn496560ihr81k1amgwm1pr1pcm72f67bkisyyx3ddcp5r") (y #t)))

(define-public crate-adlib-0.1.1 (c (n "adlib") (v "0.1.1") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)))) (h "05m6iffa3zx1q78gm79vcnjlyl49ass56af11ghh8bc4mkfgrvn7") (y #t)))

