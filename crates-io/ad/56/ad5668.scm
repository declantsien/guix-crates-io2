(define-module (crates-io ad #{56}# ad5668) #:use-module (crates-io))

(define-public crate-ad5668-0.1.0 (c (n "ad5668") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)))) (h "1m9nxm7lh2047rnfiv1kqix72jm18ig60dhr533cgs5y7fl8a440")))

(define-public crate-ad5668-0.1.1 (c (n "ad5668") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)))) (h "12ypmpm449fbl6bxp42fwhy2jab3ai0rzm16agk6vig9p9h33zri")))

(define-public crate-ad5668-0.1.2 (c (n "ad5668") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)))) (h "0f8867fdi0jnq62kslmi6v3qqkfzzq5gjnrws9yxpq1d3hg9yw65")))

(define-public crate-ad5668-0.1.3 (c (n "ad5668") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "03p46330np0p4k146b81ic78mzj6q8ryxnf1b21lcdkvhby7k3xm")))

