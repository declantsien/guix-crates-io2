(define-module (crates-io ad mi admin-app) #:use-module (crates-io))

(define-public crate-admin-app-0.1.0 (c (n "admin-app") (v "0.1.0") (d (list (d (n "apdu-dispatch") (r "^0.1") (d #t) (k 0)) (d (n "ctaphid-dispatch") (r "^0.1") (d #t) (k 0)) (d (n "delog") (r "^0.1") (d #t) (k 0)) (d (n "iso7816") (r "^0.1") (d #t) (k 0)) (d (n "trussed") (r "^0.1") (d #t) (k 0)))) (h "0v1n52pralzk99gr7yxg91ahh3gcrdqjp4h8syakzb102y1z9nb7") (f (quote (("log-warn") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all"))))))

