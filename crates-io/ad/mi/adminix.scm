(define-module (crates-io ad mi adminix) #:use-module (crates-io))

(define-public crate-adminix-0.0.1 (c (n "adminix") (v "0.0.1") (d (list (d (n "adminix_macro") (r "^0.0.1") (d #t) (k 0) (p "adminix_macro")))) (h "10bk05ky80zj5prv5xagb69sc7p50fmi7fy5a3n331k9xj053fm9")))

(define-public crate-adminix-0.0.2 (c (n "adminix") (v "0.0.2") (d (list (d (n "adminix_macro") (r "^0.0.1") (d #t) (k 0) (p "adminix_macro")))) (h "144lcshca9l35wv236y6ac66fzdzg8d2ihdw6cqjmvfhpylqfpqi")))

