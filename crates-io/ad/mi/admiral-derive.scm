(define-module (crates-io ad mi admiral-derive) #:use-module (crates-io))

(define-public crate-admiral-derive-0.1.0 (c (n "admiral-derive") (v "0.1.0") (d (list (d (n "admiral-types") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1ix66dz53by7hbrhn9w6jw5zkl6kcgl2x38ixhxpwcv4v8yb6y0v")))

