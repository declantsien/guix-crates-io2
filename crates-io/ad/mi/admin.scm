(define-module (crates-io ad mi admin) #:use-module (crates-io))

(define-public crate-admin-0.1.1 (c (n "admin") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.5") (d #t) (k 0)) (d (n "rocket") (r "^0.4.3") (d #t) (k 0)))) (h "05q9fyhfb26nhzh420cpn8cg9ix1fm4w3946z6na4dx1rji121yv")))

