(define-module (crates-io ad mi admin_bot) #:use-module (crates-io))

(define-public crate-admin_bot-0.1.0 (c (n "admin_bot") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "telebot") (r "^0.2.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0lbivf6c4drn9pkjmsqbh879a3alfk8255w04q4jzxbxx6sb3v8c")))

