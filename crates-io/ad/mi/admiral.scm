(define-module (crates-io ad mi admiral) #:use-module (crates-io))

(define-public crate-admiral-0.1.0 (c (n "admiral") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 0)) (d (n "admiral-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "admiral-types") (r "=0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("macros" "rt"))) (d #t) (k 0)))) (h "1jvx3p5cr424xja6bxblw7kxsyxxd7xkbygg6v0vv3b6ay30v4pz")))

