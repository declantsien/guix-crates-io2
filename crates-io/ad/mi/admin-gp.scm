(define-module (crates-io ad mi admin-gp) #:use-module (crates-io))

(define-public crate-admin-gp-1.0.0-beta (c (n "admin-gp") (v "1.0.0-beta") (d (list (d (n "actix-web") (r "^3.3.3") (d #t) (k 0)) (d (n "green-barrel") (r "^1.0.8-beta") (d #t) (k 0)) (d (n "mongodb") (r "^1.2.5") (f (quote ("sync"))) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0037wq0rz1a4976pq4dj5c3106lagyb0m7a2h3x0yv79873xb72m") (y #t) (r "1.57")))

(define-public crate-admin-gp-1.0.1-beta (c (n "admin-gp") (v "1.0.1-beta") (d (list (d (n "actix-web") (r "^3.3.3") (d #t) (k 0)) (d (n "green-barrel") (r "^1.0.8-beta") (d #t) (k 0)) (d (n "mongodb") (r "^1.2.5") (f (quote ("sync"))) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1d674jwzifkbymgz30ydkyz3dqrcy0s1ai676wb83r1hii0jkghg") (y #t) (r "1.57")))

(define-public crate-admin-gp-1.0.2-beta (c (n "admin-gp") (v "1.0.2-beta") (d (list (d (n "actix-web") (r "^3.3.3") (d #t) (k 0)) (d (n "green-barrel") (r "^1.0.9-beta") (d #t) (k 0)) (d (n "mongodb") (r "^1.2.5") (f (quote ("sync"))) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0519w6zikjlm41hv1zb455i8r4ql389fl4c7vxza6v6llw1drvrm") (y #t) (r "1.57")))

(define-public crate-admin-gp-1.0.3-beta (c (n "admin-gp") (v "1.0.3-beta") (d (list (d (n "actix-web") (r "^3.3.3") (d #t) (k 0)) (d (n "green-barrel") (r "^1.0.9-beta") (d #t) (k 0)) (d (n "mongodb") (r "^1.2.5") (f (quote ("sync"))) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1yvqwya2n80prwccg36hxj3g8xmw2vdivw7pxx36rhq4b0ysghnb") (y #t) (r "1.57")))

(define-public crate-admin-gp-1.0.4-beta (c (n "admin-gp") (v "1.0.4-beta") (d (list (d (n "actix-web") (r "^3.3.3") (d #t) (k 0)) (d (n "green-barrel") (r "^1.0.12-beta") (d #t) (k 0)) (d (n "mongodb") (r "^1.2.5") (f (quote ("sync"))) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "14wdg28lrxgxfa27f7azqyyva16351rvx329aarfbprx0wiwshan") (y #t) (r "1.57")))

