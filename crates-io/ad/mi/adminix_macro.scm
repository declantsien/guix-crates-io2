(define-module (crates-io ad mi adminix_macro) #:use-module (crates-io))

(define-public crate-adminix_macro-0.0.1 (c (n "adminix_macro") (v "0.0.1") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "maud") (r "^0.23") (f (quote ("actix-web"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.4") (f (quote ("sqlite" "runtime-actix-rustls"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f8v7bhc9njp90k81fk22326yapp9f1z9jbiiphaw3k3p31wgafp")))

(define-public crate-adminix_macro-0.0.2 (c (n "adminix_macro") (v "0.0.2") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "maud") (r "^0.23") (f (quote ("actix-web"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.4") (f (quote ("sqlite" "runtime-actix-rustls"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j664bxjblc3gva0iwg1k66bwg4yxhh9g1mbxwsybd41lmvg9ag1")))

