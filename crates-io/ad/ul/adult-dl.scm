(define-module (crates-io ad ul adult-dl) #:use-module (crates-io))

(define-public crate-adult-dl-0.0.1 (c (n "adult-dl") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.37") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "stream"))) (d #t) (k 0)) (d (n "seahorse") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1nxz4np2k68ipq849p8sc0b14h6npj48x3p3v4f7awqv8in3nfah")))

(define-public crate-adult-dl-0.0.2 (c (n "adult-dl") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.37") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "stream"))) (d #t) (k 0)) (d (n "seahorse") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "110jmj393q5h1xb5jfsgzlx2mhn99vvcynvkvdd6gagjnmnsjv1l")))

