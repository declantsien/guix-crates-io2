(define-module (crates-io ad to adtobs) #:use-module (crates-io))

(define-public crate-adtobs-0.1.0 (c (n "adtobs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)))) (h "120s675m54wk6m7dnv6caif4y0zcq49ksv5jpn8z32g87anzysq2")))

(define-public crate-adtobs-0.1.1 (c (n "adtobs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)))) (h "1z7zxi8il1flji14vvzg6dv223jybim8a2qw3gzyaqfzi7sdk1jr")))

(define-public crate-adtobs-0.1.2 (c (n "adtobs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)))) (h "12c2vdr652mrx8cq91jcmma9inybalq5xxq4brs2b4n2whd90qq4")))

