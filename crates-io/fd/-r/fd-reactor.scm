(define-module (crates-io fd -r fd-reactor) #:use-module (crates-io))

(define-public crate-fd-reactor-0.1.0 (c (n "fd-reactor") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1y28n292h329qk91x62hwspcllm5zrg3d3z1crxrf3s3phsh40ws")))

