(define-module (crates-io fd -o fd-oxigen) #:use-module (crates-io))

(define-public crate-fd-oxigen-1.0.0 (c (n "fd-oxigen") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "oxigen") (r "^2.1") (f (quote ("global_cache"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0wv3jqz3m9c4x9iwladrc3fyd41i857wi3p8al79kwasplbn1k9b")))

