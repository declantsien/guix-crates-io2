(define-module (crates-io fd _b fd_bs58) #:use-module (crates-io))

(define-public crate-fd_bs58-0.1.0 (c (n "fd_bs58") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0q41iqn7d1w9qljwq4vk5l3g4yxcj47rkbkr59bf8pgr1yrw3apx")))

