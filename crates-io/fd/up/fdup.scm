(define-module (crates-io fd up fdup) #:use-module (crates-io))

(define-public crate-fdup-0.1.0 (c (n "fdup") (v "0.1.0") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0r0bpbrxmhjy9b3cvynvnzw1i4yf6dn5xbq54b7qd5pqyv6hxbv0")))

(define-public crate-fdup-0.1.1 (c (n "fdup") (v "0.1.1") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0y8ypd261vx4v2apl163kd9likipv1vgjjvgbpicrqp5d0g4q7sn")))

(define-public crate-fdup-2.0.0 (c (n "fdup") (v "2.0.0") (d (list (d (n "colmac") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1wgbmmksvwvhlldc15jgd66af5f0bw4rrifpzvkvl3llflgbm5i0")))

(define-public crate-fdup-2.0.1 (c (n "fdup") (v "2.0.1") (d (list (d (n "colmac") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1pggpjdl4812hj0bbah0zzh0l8mbhxdlyviill5ly342gyk97dkq")))

(define-public crate-fdup-2.0.2 (c (n "fdup") (v "2.0.2") (d (list (d (n "colmac") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "08rfbfa7w624347wr2slghfhc6vql5x8fmdk05m078xw79rawrwf")))

