(define-module (crates-io fd b- fdb-gen) #:use-module (crates-io))

(define-public crate-fdb-gen-0.2.0 (c (n "fdb-gen") (v "0.2.0") (d (list (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "08w6d0a5x4qwhbm069yssz55bkf8bx958ir96i7zm07ddbffblqf") (f (quote (("fdb-6_3") ("default"))))))

(define-public crate-fdb-gen-0.2.1 (c (n "fdb-gen") (v "0.2.1") (d (list (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "132dann7izgj8gg60928ipb9bhiis2iylaxdpy95qdxa5sybx97l") (f (quote (("fdb-6_3") ("default")))) (r "1.49")))

(define-public crate-fdb-gen-0.2.2 (c (n "fdb-gen") (v "0.2.2") (d (list (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1hj01rkpv6sq8jvhp6ck65jdl3lz3l2mk7n4f82qpzbpc75y4qwx") (f (quote (("fdb-6_3") ("default")))) (r "1.49")))

(define-public crate-fdb-gen-0.3.0 (c (n "fdb-gen") (v "0.3.0") (d (list (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1b88305m3nam83ni5ai3h74a4442g86fylxvspsmpqd5i4gshsbm") (f (quote (("fdb-7_1") ("fdb-6_3") ("default")))) (r "1.49")))

(define-public crate-fdb-gen-0.3.1 (c (n "fdb-gen") (v "0.3.1") (d (list (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0xlzdp2ixi5zfch7jzqd9bg8z8105icpx849j6hgm9kyp2518pd0") (f (quote (("fdb-7_1") ("fdb-6_3") ("default")))) (r "1.49")))

