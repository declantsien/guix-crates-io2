(define-module (crates-io fd b- fdb-sys) #:use-module (crates-io))

(define-public crate-fdb-sys-0.2.0 (c (n "fdb-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)))) (h "17g4c1fw7r6hsps0xm0hd75rz0s2xqcwj2zhsy4ggzqzaxc1s4bm") (f (quote (("fdb-6_3") ("default"))))))

(define-public crate-fdb-sys-0.2.1 (c (n "fdb-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)))) (h "1cbbdh1f9asg4gq82qsgm0xlsl8rhjpy3i7q9r0xz00gd0wpdd2a") (f (quote (("fdb-6_3") ("default")))) (r "1.49")))

(define-public crate-fdb-sys-0.2.2 (c (n "fdb-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0kkskf1j83fns6ggdcvnsgvgr8ig2bv6k7l7sjgr5fn5x6p1ij1y") (f (quote (("fdb-6_3") ("default")))) (r "1.49")))

(define-public crate-fdb-sys-0.3.0 (c (n "fdb-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0y6xny8j1vkmyz6sl26wacfpdln79s48bj1hrk5f6ny98a85x5yc") (f (quote (("fdb-7_1") ("fdb-6_3") ("default")))) (r "1.49")))

(define-public crate-fdb-sys-0.3.1 (c (n "fdb-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0pw1hhmq0sbz29k2dnfia6qh2cqx9ppcvzz5h83f02wicsrl4nsv") (f (quote (("fdb-7_1") ("fdb-6_3") ("default")))) (r "1.49")))

