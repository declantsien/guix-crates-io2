(define-module (crates-io fd ca fdcanusb) #:use-module (crates-io))

(define-public crate-fdcanusb-0.1.0 (c (n "fdcanusb") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serial2") (r "^0.2.19") (d #t) (k 0)))) (h "1v8kbkijc0z3azb6r6ayl7ay0axc8mf419p46gbscw9cpmw8w6h3")))

(define-public crate-fdcanusb-0.2.0 (c (n "fdcanusb") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serial2") (r "^0.2.20") (o #t) (d #t) (k 0)))) (h "07bvkwhv455kwxzvpan9wi5vy72813p3pcz4bl8n6gziw5v36gqg") (f (quote (("default" "serial2")))) (s 2) (e (quote (("serial2" "dep:serial2"))))))

