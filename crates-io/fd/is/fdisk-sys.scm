(define-module (crates-io fd is fdisk-sys) #:use-module (crates-io))

(define-public crate-fdisk-sys-0.1.0 (c (n "fdisk-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1qygk2j8b69bpgyjzfkghgcl5lci1kavvbhiz5mq6dm2ncl2207q")))

(define-public crate-fdisk-sys-0.2.0 (c (n "fdisk-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.141") (d #t) (k 0)))) (h "012k5cgbs5gi9b27qvhasrdxs7an883kw5mk9ry7x94l8h5h7c2s")))

