(define-module (crates-io fd is fdisk) #:use-module (crates-io))

(define-public crate-fdisk-0.1.0 (c (n "fdisk") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12") (k 0)) (d (n "fdisk-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "0i29gplfisk0bg6yp7zd7q8apcq4chkcnpw6dxy7js3rh2ihad0j")))

(define-public crate-fdisk-0.1.1 (c (n "fdisk") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.12") (k 0)) (d (n "fdisk-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "1kk8qsqb5cxgb5m94s2cm01xlby7lx8c32x6768y8psmqbh1qxv2")))

(define-public crate-fdisk-0.1.2 (c (n "fdisk") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.12") (k 0)) (d (n "fdisk-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "05w4zrnaas4fcmi6gprgywl45rpwa2sdy2pfszrkng9pa87a2hk5")))

(define-public crate-fdisk-0.2.0 (c (n "fdisk") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "fdisk-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)))) (h "1xkyp6mq521d1zxy8cdvn4i9m9kznvi21r3skn4xfpnih0p3j41b")))

