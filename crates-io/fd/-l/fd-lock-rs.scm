(define-module (crates-io fd -l fd-lock-rs) #:use-module (crates-io))

(define-public crate-fd-lock-rs-0.1.0 (c (n "fd-lock-rs") (v "0.1.0") (d (list (d (n "nix") (r "^0.19.1") (d #t) (k 0)))) (h "0pw3h8p98gp5ngifs8lx4afsjb05g506rzicxfjvr2y8agnhgnf1")))

(define-public crate-fd-lock-rs-0.1.1 (c (n "fd-lock-rs") (v "0.1.1") (d (list (d (n "nix") (r "^0.19.1") (d #t) (k 0)))) (h "0kjqdym3wbnsg52lav1rcwnd424xjjc09agk7jd0hmdrcqrdm895")))

(define-public crate-fd-lock-rs-0.1.2 (c (n "fd-lock-rs") (v "0.1.2") (d (list (d (n "nix") (r "^0.19.1") (d #t) (k 0)))) (h "0ax2xgllfamnm1z34mqmymwyn5kqsjnjjjyv30rsymrlv0ww26i7")))

(define-public crate-fd-lock-rs-0.1.3 (c (n "fd-lock-rs") (v "0.1.3") (d (list (d (n "nix") (r "^0.19.1") (d #t) (k 0)))) (h "11bnga2xhnpz6jdxn3ch7fjaxw862d8j45mq3k6vynla8n44x8ij")))

(define-public crate-fd-lock-rs-0.1.4 (c (n "fd-lock-rs") (v "0.1.4") (d (list (d (n "nix") (r "^0.24.2") (d #t) (k 0)))) (h "1csdydwqkhvhgfwm0rjax9gxsb9bv2kd79rf9xk5iq3r3mz583zg")))

