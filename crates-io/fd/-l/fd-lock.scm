(define-module (crates-io fd -l fd-lock) #:use-module (crates-io))

(define-public crate-fd-lock-1.0.0 (c (n "fd-lock") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "winapi") (r "^0.3.7") (d #t) (k 0)))) (h "0cmzh7yysjzy8kh931d29n41a46wshs7x3syhdb0szynyji3scfa")))

(define-public crate-fd-lock-1.1.0 (c (n "fd-lock") (v "1.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "winapi") (r "^0.3.7") (d #t) (k 0)))) (h "0ry5b1l2i4274jbzbbqk9kbm6qmydixa9anpc0jj30n50pjqddjc")))

(define-public crate-fd-lock-1.1.1 (c (n "fd-lock") (v "1.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "winapi") (r "^0.3.7") (d #t) (k 0)))) (h "0m24g1h5sg8whajd80b6gxgh7yw371ndqjq2wdg9zm24a9wyqnx1")))

(define-public crate-fd-lock-2.0.0 (c (n "fd-lock") (v "2.0.0") (d (list (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "winapi") (r "^0.3.7") (d #t) (t "cfg(windows)") (k 0)))) (h "01kzrikg3a60lxmr0k8bbm4nggh6693f1pf530ip136qzwpg0400")))

(define-public crate-fd-lock-3.0.0 (c (n "fd-lock") (v "3.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "winapi") (r "^0.3.7") (d #t) (t "cfg(windows)") (k 0)))) (h "0dif8wk9xrqkjyfgqqy3zfg4ckmkpyzzk5p5m01s99q63bcnv05q")))

(define-public crate-fd-lock-3.0.1 (c (n "fd-lock") (v "3.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.28.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1i8l7y18wwgjycdsqz079pvrpzmg83gk5n7f52j4czbja3z11hfg")))

(define-public crate-fd-lock-3.0.2 (c (n "fd-lock") (v "3.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.28.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0gdh9k7a2fsvd24wakr2jfqzswd5207lxc1j67al7208hpk10sd1")))

(define-public crate-fd-lock-3.0.3 (c (n "fd-lock") (v "3.0.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.32.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.30.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fff33bj0xn9ssw12xxlhskjf11kcw2cyn9pwxfdpwwwx9npbvzw")))

(define-public crate-fd-lock-3.0.4 (c (n "fd-lock") (v "3.0.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.33.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.30.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13yzdavjpg75sqlp738ws05jk5prd2l7ykyias4nyng012cavv02")))

(define-public crate-fd-lock-3.0.5 (c (n "fd-lock") (v "3.0.5") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.34.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.30.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jl6yi4qnx6sc504lsrgjkqp97p60wn17jsnbi0wcc7cr3s4bqj6")))

(define-public crate-fd-lock-3.0.6 (c (n "fd-lock") (v "3.0.6") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.35.6") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05zmsng1fcgx88sxm4h788yaii0z1g1zbimlkadwia3r9mzcq7g1")))

(define-public crate-fd-lock-3.0.7 (c (n "fd-lock") (v "3.0.7") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.35.6") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04b7n7gmnxcw3am687b1wq9idy429z8alxb82z27v5cd0n0sb4qc")))

(define-public crate-fd-lock-3.0.8 (c (n "fd-lock") (v "3.0.8") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "09qa9gmc83fy0a0li701hhfbml2x2ivlp7h4q7diappakydwc8dv")))

(define-public crate-fd-lock-3.0.9 (c (n "fd-lock") (v "3.0.9") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0hpx9rmcgmixf9a21bmqn07qh4ma5zwzj32dvnzjhfxxy07ikh18")))

(define-public crate-fd-lock-3.0.10 (c (n "fd-lock") (v "3.0.10") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fgd5kcqw7mwbvc6i0rkmw5xgmmw5pfgs6pllj8sdhqmwh5a7wcf")))

(define-public crate-fd-lock-3.0.11 (c (n "fd-lock") (v "3.0.11") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.37.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bfbsm9mlvr9v6pbqyzjj7ci7aqqgka1s2v18z602jif9bxsx6cp")))

(define-public crate-fd-lock-3.0.12 (c (n "fd-lock") (v "3.0.12") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.37.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0hlnn1302p37qlc9xl2k5y0vw8q8id5kg59an6riy89hjlynpbir")))

(define-public crate-fd-lock-3.0.13 (c (n "fd-lock") (v "3.0.13") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.38.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1df1jdncda67g65hrnmd2zsl7q5hdn8cm84chdalxndsx7akw0zg")))

(define-public crate-fd-lock-4.0.0 (c (n "fd-lock") (v "4.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.38.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yb8ihxff0f8dmr90vy6j0mlvdjadskcfysh308vszf7xpqpf0qb")))

(define-public crate-fd-lock-4.0.1 (c (n "fd-lock") (v "4.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.38.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1k5vmgglx744zz3j06if0q7b1c5g0pnm61pqk21zd68wnw6plgxr")))

(define-public crate-fd-lock-4.0.2 (c (n "fd-lock") (v "4.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.38.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ixrsd19k2cpl773p9hd0yk3hac684d9aphbxy0jq9q64bd6hmvy")))

