(define-module (crates-io fd -p fd-passing) #:use-module (crates-io))

(define-public crate-fd-passing-0.0.1 (c (n "fd-passing") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1zckhshqf47ik9kv69lpivr3j2s3v8nx1pbgzf44b91xk96a1lsp")))

