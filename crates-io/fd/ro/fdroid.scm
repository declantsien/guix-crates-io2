(define-module (crates-io fd ro fdroid) #:use-module (crates-io))

(define-public crate-fdroid-0.1.0 (c (n "fdroid") (v "0.1.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0mfals15xbxsnazf7ldjjva8nm0mbdzj7w2n7vjzp7rjc80zw6km")))

(define-public crate-fdroid-0.1.1 (c (n "fdroid") (v "0.1.1") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bpbil6jpydx9paplyhxqicj60yd6vkd9631211k8gdgpxil7g5d")))

