(define-module (crates-io fd pa fdpass) #:use-module (crates-io))

(define-public crate-fdpass-0.1.0 (c (n "fdpass") (v "0.1.0") (d (list (d (n "fd") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5") (d #t) (k 0)))) (h "0n30h0q5w07skkj3im8cfkm1d6gna2z3jf9mvfgm2psq5j7ljzr2")))

