(define-module (crates-io fd td fdtdump) #:use-module (crates-io))

(define-public crate-fdtdump-0.1.0 (c (n "fdtdump") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fdt-rs") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "unsafe_unwrap") (r "^0.1.0") (d #t) (k 0)))) (h "0vyg0h7gpbbwp36k7yymcdxwy46klkrnnzyfiasp7d6d293bf1ch")))

