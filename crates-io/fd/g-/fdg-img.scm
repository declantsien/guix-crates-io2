(define-module (crates-io fd g- fdg-img) #:use-module (crates-io))

(define-public crate-fdg-img-0.1.0 (c (n "fdg-img") (v "0.1.0") (d (list (d (n "fdg-sim") (r "^0.3.1") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.4.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3.2") (d #t) (k 0)))) (h "1k7a3iwl235mh9gd57p1iz1snccrzwsjnqxhggb2wvhjbr1r9n8l")))

(define-public crate-fdg-img-0.1.1 (c (n "fdg-img") (v "0.1.1") (d (list (d (n "fdg-sim") (r "^0.4.0") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.4.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3.2") (d #t) (k 0)))) (h "0nxr0bzf30ciqxzjkpm9vjhbjsi86blp9n9ds4c87q0mycf41n7c")))

(define-public crate-fdg-img-0.1.2 (c (n "fdg-img") (v "0.1.2") (d (list (d (n "fdg-sim") (r "^0.4") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.4") (f (quote ("json"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3.2") (d #t) (k 0)))) (h "0qmqkqp02s5xirmfv4s8j30rljw4plmzga1d71145liyr1ryfyaz")))

(define-public crate-fdg-img-0.1.3 (c (n "fdg-img") (v "0.1.3") (d (list (d (n "fdg-sim") (r "^0.6") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.6") (f (quote ("json"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "0dl46v2sbm9s0bvmyqn348mrn833lqdkzs4kj4hysrxq8anghpjp")))

(define-public crate-fdg-img-0.1.4 (c (n "fdg-img") (v "0.1.4") (d (list (d (n "fdg-sim") (r "^0.7") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.7") (f (quote ("json"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "1yxqv123bvzajyxhyf6qwswgfybg2xg56zv2hx9hkr4hphhlrqcl")))

(define-public crate-fdg-img-0.2.0 (c (n "fdg-img") (v "0.2.0") (d (list (d (n "fdg-sim") (r "^0.7") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.7") (f (quote ("json"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "0psq0v5h5gwwyfrywi2p6cirlagwh9zyn8jygw7ag9d2p3hyvc5p")))

(define-public crate-fdg-img-0.3.0 (c (n "fdg-img") (v "0.3.0") (d (list (d (n "fdg-sim") (r "^0.7") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.7") (f (quote ("json"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "09cqrq2rhlwrsirhykxnz7qbr0v3b3krk615qyk35jkx3kf3hr6n")))

(define-public crate-fdg-img-0.4.0 (c (n "fdg-img") (v "0.4.0") (d (list (d (n "fdg-sim") (r "^0.8") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.8") (f (quote ("json"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "17lyh017zhnw8lb1i9a83r81s1qnm1wd006xmpsmgaxyj103zg6n")))

(define-public crate-fdg-img-0.4.1 (c (n "fdg-img") (v "0.4.1") (d (list (d (n "fdg-sim") (r "^0.8") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.8") (f (quote ("json"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "1zb4nlc3f7131a9sz54qszgfw9p2k1w3acinlzk9knn0iswsx984") (f (quote (("wasm" "wasm-bindgen" "serde_json" "serde" "fdg-sim/json"))))))

(define-public crate-fdg-img-0.4.2 (c (n "fdg-img") (v "0.4.2") (d (list (d (n "fdg-sim") (r "^0.9.1") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.9") (f (quote ("json"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "04py3s5cxl65hj3bb93lzinr8zrr4h6pngqznv94g896h20g8mys") (f (quote (("wasm" "wasm-bindgen" "serde_json" "serde" "fdg-sim/json"))))))

