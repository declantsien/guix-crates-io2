(define-module (crates-io fd ef fdeflate) #:use-module (crates-io))

(define-public crate-fdeflate-0.1.0 (c (n "fdeflate") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.12.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.6.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (d #t) (k 0)))) (h "03s4w944pqk505jghdd52dpvdsd36g2jv9qfrl9rsf4p2vdy6rn5") (r "1.61.0")))

(define-public crate-fdeflate-0.1.1 (c (n "fdeflate") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.12.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.6.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (d #t) (k 0)))) (h "1aknvbk22gwfgnwbrwrgadmgj42ivf8wzi5299cmnlwzwajxsl83") (r "1.57.0")))

(define-public crate-fdeflate-0.2.0 (c (n "fdeflate") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.12.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.6.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (d #t) (k 0)))) (h "0vscx5f91dqd5d63awhhkf5k09mw3smd7n6f0q1wzz5mv7mnpgv2") (r "1.57.0")))

(define-public crate-fdeflate-0.2.1 (c (n "fdeflate") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.6.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (d #t) (k 0)))) (h "0sirh9kd64qyhdnygmcdbr7bk0npcib00g4dyrq00n6aq9235niv") (r "1.57.0")))

(define-public crate-fdeflate-0.3.0 (c (n "fdeflate") (v "0.3.0") (d (list (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (d #t) (k 0)))) (h "045fyccqx0wbfrh36sgcf79za5qg95vpihmbklj0dvhlqpmbsafk") (r "1.57.0")))

(define-public crate-fdeflate-0.3.1 (c (n "fdeflate") (v "0.3.1") (d (list (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (d #t) (k 0)))) (h "0s5885wdsih2hqx3hsl7l8cl3666fgsgiwvglifzy229hpydmmk4") (r "1.57.0")))

(define-public crate-fdeflate-0.3.2 (c (n "fdeflate") (v "0.3.2") (d (list (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (d #t) (k 0)))) (h "0cm95k7hzfsdm9yqcw88psapazvd3yyzjzqs068bmphs4n341bvw") (r "1.57.0")))

(define-public crate-fdeflate-0.3.3 (c (n "fdeflate") (v "0.3.3") (d (list (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (d #t) (k 0)))) (h "1g9ibjazj6wklff2q8gx9nm3wcmcaf3fkw0ic6m4ai7wdpfri410") (r "1.57.0")))

(define-public crate-fdeflate-0.3.4 (c (n "fdeflate") (v "0.3.4") (d (list (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (d #t) (k 0)))) (h "0ig65nz4wcqaa3y109sh7yv155ldfyph6bs2ifmz1vad1vizx6sg") (r "1.57.0")))

