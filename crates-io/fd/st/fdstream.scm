(define-module (crates-io fd st fdstream) #:use-module (crates-io))

(define-public crate-fdstream-0.0.1 (c (n "fdstream") (v "0.0.1") (d (list (d (n "libc") (r "^0.1.3") (d #t) (k 0)))) (h "19fxdipxnmp13fm4dpci9ag4xfbj2zhkmawgcfs7ql69dwslkc9g")))

(define-public crate-fdstream-0.0.2 (c (n "fdstream") (v "0.0.2") (d (list (d (n "libc") (r "^0.1.3") (d #t) (k 0)))) (h "1bqsb9f9fh7qn7mypdjil4gssyj09rw1w72g27x2lnzq3xa8ib4q")))

(define-public crate-fdstream-0.0.3 (c (n "fdstream") (v "0.0.3") (d (list (d (n "libc") (r "^0.1.4") (d #t) (k 0)))) (h "13f1d4w10z12yf9imlhmdcjga8igzx2qfrfnjpkfcn8cvh88wqy0")))

(define-public crate-fdstream-0.0.4 (c (n "fdstream") (v "0.0.4") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)))) (h "11qf8q28wxp4dbsmicq7vnvvribg2q3dzx3cl3fqhqm6hbsmpr1k")))

(define-public crate-fdstream-0.0.5 (c (n "fdstream") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "1qi22qvmk89j4lkkn89dm0kpvpq3067lssmis20dcip0iq2m7kca")))

