(define-module (crates-io fd li fdlibm-rs) #:use-module (crates-io))

(define-public crate-fdlibm-rs-0.1.0 (c (n "fdlibm-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "00yafl0kn8g3hq8736ghqnn3kpnp4kk8jd888n5r38g2rfnmlb3d")))

