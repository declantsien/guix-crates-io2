(define-module (crates-io fd li fdlimit) #:use-module (crates-io))

(define-public crate-fdlimit-0.1.0 (c (n "fdlimit") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vrdljpggwqsrslxshahvrk0psj63pwyx51dpwr394jzf7qi0635")))

(define-public crate-fdlimit-0.1.1 (c (n "fdlimit") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1alvrp7p7a9cmq1g63q8bcj3nwgad1q1axr8f6rq0m8f0nkibvmi")))

(define-public crate-fdlimit-0.1.2 (c (n "fdlimit") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l9mk9xbmqyc9qyfhjg6hvpdkrqg6a4dnxl96ab19yvfnxdwb14h")))

(define-public crate-fdlimit-0.1.3 (c (n "fdlimit") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vambf8fhkcinmlmgnrka7z1wdfalbjnvwzi04hpx86dgi7ydfwa")))

(define-public crate-fdlimit-0.1.4 (c (n "fdlimit") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g30d6gqkrwy8ylwdy7pqm443iq0p5dmnpz4ks41pirl7dclm98d")))

(define-public crate-fdlimit-0.2.0 (c (n "fdlimit") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l3qi8yjqsf215zjmvffndv27192dp8s31fb1ayv4jc35ci6xg27")))

(define-public crate-fdlimit-0.2.1 (c (n "fdlimit") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06rzmk7f60ifwc549hq6d3dzj7q3k1mx4rsvrbj3nnizci1rwk1c")))

(define-public crate-fdlimit-0.3.0 (c (n "fdlimit") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1rbz4wpf6a2csjfwxlv2hg22jiq8xaxmy71mczpxjwzgqbdzg0p1")))

