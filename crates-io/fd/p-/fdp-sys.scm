(define-module (crates-io fd p- fdp-sys) #:use-module (crates-io))

(define-public crate-fdp-sys-0.1.0 (c (n "fdp-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1imcjbyrb0pjglfyn5qa98flcxva796shjz2bqq6pl5pa38zgxz8") (l "FDP")))

(define-public crate-fdp-sys-0.1.1 (c (n "fdp-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1hzwsyyvmfldhrrlgicb9r9v1n8ak1m7jjdcij8blczinr7f7p5v") (l "FDP")))

