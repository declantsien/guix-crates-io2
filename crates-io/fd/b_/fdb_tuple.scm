(define-module (crates-io fd b_ fdb_tuple) #:use-module (crates-io))

(define-public crate-fdb_tuple-0.1.0 (c (n "fdb_tuple") (v "0.1.0") (d (list (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (o #t) (d #t) (k 0)))) (h "1i9vvshldp0m88yxkm8c3mn1ynmjyp1fa0y2b2pym53xlx87larr") (f (quote (("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-fdb_tuple-0.2.0 (c (n "fdb_tuple") (v "0.2.0") (d (list (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (o #t) (d #t) (k 0)))) (h "15c3r8pkw0mcw6wv4sl2d37a7fk8a34xcwqxb3vxhbjb4n9y90j1") (f (quote (("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-fdb_tuple-0.2.1 (c (n "fdb_tuple") (v "0.2.1") (d (list (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (o #t) (d #t) (k 0)))) (h "0zdxaqbh8l9flmgca0han2s63h21fnfx8v22bh9laagpadj5pgk1") (f (quote (("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-fdb_tuple-0.3.0 (c (n "fdb_tuple") (v "0.3.0") (d (list (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (o #t) (d #t) (k 0)))) (h "0294zcx3v9dawrz8c55ssvprc44x1li5dzcx69ya3svizqs662by") (f (quote (("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-fdb_tuple-0.4.0 (c (n "fdb_tuple") (v "0.4.0") (d (list (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (o #t) (d #t) (k 0)))) (h "004nkmkvww6269vm6gby0m3pazydh3473f7j7vgc7vq7lj0hcyp2") (f (quote (("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

