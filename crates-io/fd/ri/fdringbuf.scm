(define-module (crates-io fd ri fdringbuf) #:use-module (crates-io))

(define-public crate-fdringbuf-0.0.1 (c (n "fdringbuf") (v "0.0.1") (d (list (d (n "nix") (r "*") (d #t) (k 2)))) (h "0i5gn00ckqwyjgszlvqfzqrmv5pjjl69qsq5m715kbdavgfs616c")))

(define-public crate-fdringbuf-0.0.2 (c (n "fdringbuf") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 2)))) (h "0h4cn7h7xz9l0q7x75hsaq0gd9yg7hrb8nmic4qxbrinmnss7x1k")))

