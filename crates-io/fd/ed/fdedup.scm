(define-module (crates-io fd ed fdedup) #:use-module (crates-io))

(define-public crate-fdedup-1.0.0 (c (n "fdedup") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zvmh18917vcp94ycc53cghb2dvp42fa355i44scn950wzyjh6ar")))

