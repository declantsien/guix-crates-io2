(define-module (crates-io fd ns fdns-gamenetworkingsockets-sys) #:use-module (crates-io))

(define-public crate-fdns-gamenetworkingsockets-sys-0.1.0 (c (n "fdns-gamenetworkingsockets-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "1jvaw0mwfbbd7kiszgksd6fqj8kxnc4yhq6arj7cwqdxn8nzy70p") (l "gamenetworkingsockets")))

(define-public crate-fdns-gamenetworkingsockets-sys-0.2.0 (c (n "fdns-gamenetworkingsockets-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "1iasqk1sb7s9v8gq1s5hagqph05zhzz46436pfcv8d6saxdzxpzd") (l "gamenetworkingsockets")))

(define-public crate-fdns-gamenetworkingsockets-sys-0.3.0 (c (n "fdns-gamenetworkingsockets-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "0mryy9hj4v202mm6r037286pl22s12izv304ikbq30qgsgvrkm02") (l "gamenetworkingsockets")))

