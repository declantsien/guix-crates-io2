(define-module (crates-io fd ir fdir) #:use-module (crates-io))

(define-public crate-fdir-0.1.0 (c (n "fdir") (v "0.1.0") (h "0r82jpc0l6d7swk4kbj2cm844wzb5w8k3pr4wpw78awndpkjxhdl") (y #t)))

(define-public crate-fdir-0.1.2 (c (n "fdir") (v "0.1.2") (h "0m30x0xpwnsr36xpjcc85d1bkiyzjiifc3qv39kf4skkl0qkvj12") (y #t)))

(define-public crate-fdir-0.1.3 (c (n "fdir") (v "0.1.3") (d (list (d (n "axum") (r "^0.6.20") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "1d18ji7sja474k040sfzhifg489d855hq9krpxzn6j31dgx253n1") (y #t)))

(define-public crate-fdir-0.1.4 (c (n "fdir") (v "0.1.4") (d (list (d (n "hyper") (r "^0.14.27") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "05vxvaa682sppnx0dwg16w52affrbb2s5jj542c87qf99hnvjsr7") (y #t)))

(define-public crate-fdir-0.1.5 (c (n "fdir") (v "0.1.5") (d (list (d (n "hyper") (r "^0.14.27") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "15g5iaa13vwq94riqywm637bmkn4lsqciak4rb1gpjz62crjsx5c") (y #t)))

(define-public crate-fdir-0.1.1 (c (n "fdir") (v "0.1.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "0xalvgks0rly55scpxsav4ivf281sn04g3dkyyf0b20ys3lkfrkg") (y #t)))

(define-public crate-fdir-0.1.6 (c (n "fdir") (v "0.1.6") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "1b81jrx9ymy34xwgs46i9zfgazh014nscbsxwdfj3wjx8z6h2xzc") (y #t)))

(define-public crate-fdir-0.1.7 (c (n "fdir") (v "0.1.7") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "0vwg1q94k373nfbl5sk44smvpz7p0qhmxwqdbf9zdf0alw6s0lai") (y #t)))

(define-public crate-fdir-0.1.8 (c (n "fdir") (v "0.1.8") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "1b9805simhz1sxridslx8i3kpxl9x4k4kl3c92vdrl0d5x4j0gdb") (y #t)))

(define-public crate-fdir-0.1.9 (c (n "fdir") (v "0.1.9") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1q05jgy4wiqq5fjjiz9yy07wpis04rig35qzyh1n83ysfy8wih6r") (y #t)))

(define-public crate-fdir-0.1.10 (c (n "fdir") (v "0.1.10") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "0hpc2jfm66h5158aiqc510bq3c5sf5wfi89khqji5zndk7jyfixp")))

(define-public crate-fdir-0.1.12 (c (n "fdir") (v "0.1.12") (h "0msqhgl9vh6sav8ja325ysdi2p36ziwyyzjb4jsjqn1b8gs3kfrl")))

