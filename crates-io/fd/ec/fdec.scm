(define-module (crates-io fd ec fdec) #:use-module (crates-io))

(define-public crate-fdec-0.1.0 (c (n "fdec") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1qmj4786fx5xqr2xmknrzlv6w95skss3003slldz17vvbvplnfcy")))

(define-public crate-fdec-0.2.0 (c (n "fdec") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1fj04am7v6qzq4rnw68awb351p9gj21lw6af777g7s6z82sz6l06")))

(define-public crate-fdec-0.2.1 (c (n "fdec") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "07mhzlcs4dbbcizwim7f9apskfil2za7nnrlraa1lk5awjg2zifl")))

(define-public crate-fdec-0.3.0 (c (n "fdec") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1qchzlzynb6s1cvd2d4x54nmjw9539wwbmygnnb4jlnpy9x56bnz")))

(define-public crate-fdec-0.3.1 (c (n "fdec") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0npl8p7mg0w0w27rf5rjnbysrbmb8agapi6k6yx60fj16n9hkz5r")))

