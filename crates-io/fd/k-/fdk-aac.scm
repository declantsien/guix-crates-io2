(define-module (crates-io fd k- fdk-aac) #:use-module (crates-io))

(define-public crate-fdk-aac-0.1.0 (c (n "fdk-aac") (v "0.1.0") (d (list (d (n "fdk-aac-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1hcg8q5n661k40rbb80sd2xm28bbhzvb05b8spvrmmwikd0dllz6")))

(define-public crate-fdk-aac-0.2.0 (c (n "fdk-aac") (v "0.2.0") (d (list (d (n "fdk-aac-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1k0a4kbjjfvain9cbkfrziz0z56nkz15vmcrgs6wizzkjhghmkrn")))

(define-public crate-fdk-aac-0.3.0 (c (n "fdk-aac") (v "0.3.0") (d (list (d (n "fdk-aac-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1x460wf4pdyvidiqph1pxrrwkmsd2603cjygzr60lcls7gqn9dvy")))

(define-public crate-fdk-aac-0.4.0 (c (n "fdk-aac") (v "0.4.0") (d (list (d (n "fdk-aac-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1nxhfv9d9zjnz9a57hj8df3sx0nv7rpy225brsyv16lpg9hrbwm8")))

(define-public crate-fdk-aac-0.5.0 (c (n "fdk-aac") (v "0.5.0") (d (list (d (n "fdk-aac-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0v3i7gkzcs3iq46wjipr0525zprsdppn8yxbvclaf521za4gkwyk")))

(define-public crate-fdk-aac-0.6.0 (c (n "fdk-aac") (v "0.6.0") (d (list (d (n "fdk-aac-sys") (r "^0.5") (d #t) (k 0)))) (h "0jf6q9ph8f92vkl46cryj6mapr2bbglvdlxaqlk87kiwarv3hy42")))

