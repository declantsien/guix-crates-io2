(define-module (crates-io fd k- fdk-aac-sys) #:use-module (crates-io))

(define-public crate-fdk-aac-sys-0.1.0 (c (n "fdk-aac-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0qmlqmprd91yd3ys9qmc6zr5gz2f1znwbkqwnryrhsc1g2fbca9b")))

(define-public crate-fdk-aac-sys-0.2.0 (c (n "fdk-aac-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0zimnkin1ky30cmaqmzahfhi3bf3v8rbkh6xq7fk6i1r59756yhr")))

(define-public crate-fdk-aac-sys-0.3.0 (c (n "fdk-aac-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1c8syly1iypsi1nndd9vlcy64a06mqi4dq50kxvjzgamzdbnia1x")))

(define-public crate-fdk-aac-sys-0.4.0 (c (n "fdk-aac-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1z0piq2dxs57bi5qk7rby7qiavim8zcg0bmn6wpsi2kslygfzk8c")))

(define-public crate-fdk-aac-sys-0.5.0 (c (n "fdk-aac-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "10yaa4nb59ikzkk1773bf3i9zbvbbz3ssm9mhfqmqvah24k6sl94")))

