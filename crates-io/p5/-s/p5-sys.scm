(define-module (crates-io p5 -s p5-sys) #:use-module (crates-io))

(define-public crate-p5-sys-0.1.0 (c (n "p5-sys") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.44") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.67") (d #t) (k 0)))) (h "11v1hha5s5sq52wa0vklcih1x0xvmwdql0rrhjfyf7my37cvsfmm")))

(define-public crate-p5-sys-0.1.1 (c (n "p5-sys") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.44") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.67") (d #t) (k 0)))) (h "17fil56y25x1zx0mgiac640v4h6bwcvf7276jq7k5gg3r4n72mcs")))

