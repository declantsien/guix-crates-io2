(define-module (crates-io p5 #{0x}# p50x) #:use-module (crates-io))

(define-public crate-p50x-0.1.0 (c (n "p50x") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (o #t) (d #t) (k 0)) (d (n "safe-transmute") (r "^0.10.1") (d #t) (k 0)) (d (n "serial-unit-testing") (r "^0.2.3") (k 0)))) (h "0aqsrlv1lri28j0mppg99h9i4fg9kcz9qpn3snkc4nqwysqyfcmq") (f (quote (("default" "binary") ("binary" "clap"))))))

