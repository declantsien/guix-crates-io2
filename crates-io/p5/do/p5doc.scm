(define-module (crates-io p5 do p5doc) #:use-module (crates-io))

(define-public crate-p5doc-0.1.0 (c (n "p5doc") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mfpimb5wcfj8l7ssy2xnfky6y8d2qs9rvkh1wjf7yk2l8hdr48x")))

