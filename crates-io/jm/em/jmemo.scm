(define-module (crates-io jm em jmemo) #:use-module (crates-io))

(define-public crate-jmemo-0.1.0 (c (n "jmemo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo" "help"))) (d #t) (k 0)) (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "jlogger-tracing") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "1flq64v1mbwgkhy4a6zbdqbnpj1liiy9vbslkxir1a96n0kijfd9")))

(define-public crate-jmemo-0.1.1 (c (n "jmemo") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo" "help"))) (d #t) (k 0)) (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "jlogger-tracing") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "19hn768wyp7vlzsw6n7jjwy0sl0l7wvv8kjnfc6nv1brnilmqbl5")))

(define-public crate-jmemo-0.1.2 (c (n "jmemo") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo" "help"))) (d #t) (k 0)) (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "jlogger-tracing") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "1qjha7vxyskxwjlg7rliqj3akj6xf3l0zfrl6smm7r307pkakr5f")))

