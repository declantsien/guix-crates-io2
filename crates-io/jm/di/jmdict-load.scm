(define-module (crates-io jm di jmdict-load) #:use-module (crates-io))

(define-public crate-jmdict-load-0.1.0 (c (n "jmdict-load") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)))) (h "1v8878q9sm66hw5j6jdbkz0csrsw0hdcr3nrhf41kf5vpaqcpk5j")))

