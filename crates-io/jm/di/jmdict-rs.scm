(define-module (crates-io jm di jmdict-rs) #:use-module (crates-io))

(define-public crate-jmdict-rs-0.1.0 (c (n "jmdict-rs") (v "0.1.0") (d (list (d (n "jmdict-load") (r "^0.1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bcslw360f36r96rydmpl3qlhj5pkixbzsc7n5k1lb6kba4wb4v4")))

(define-public crate-jmdict-rs-0.1.1 (c (n "jmdict-rs") (v "0.1.1") (d (list (d (n "jmdict-load") (r "^0.1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zrp8cmaa934451ynhcxk3lddlfzfq5sqxv63v6anb2rj3vg014p") (f (quote (("no-data"))))))

(define-public crate-jmdict-rs-0.1.2 (c (n "jmdict-rs") (v "0.1.2") (d (list (d (n "jmdict-load") (r "^0.1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11nv6ryz049ajkzkrmr4hrizxm5wy1wxr33zq30pbl3b5s8dc1wy") (f (quote (("no-data"))))))

