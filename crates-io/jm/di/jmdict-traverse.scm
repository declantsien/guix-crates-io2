(define-module (crates-io jm di jmdict-traverse) #:use-module (crates-io))

(define-public crate-jmdict-traverse-0.99.1 (c (n "jmdict-traverse") (v "0.99.1") (d (list (d (n "directories") (r "^3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 0)) (d (n "jmdict-enums") (r "^0.99.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "libflate") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0ny7b6c6yl5c0agnpjd5yh6band6fk9pj2wzr17c1qq8wsmhqzhy")))

(define-public crate-jmdict-traverse-1.0.0 (c (n "jmdict-traverse") (v "1.0.0") (d (list (d (n "directories") (r "^3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 0)) (d (n "jmdict-enums") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "libflate") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "166kyvdkkmhrdq0g8wh43rwickhz0m91vdbrpjlwaygv8cmzzzdf")))

(define-public crate-jmdict-traverse-2.0.0 (c (n "jmdict-traverse") (v "2.0.0") (d (list (d (n "directories") (r "^3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 0)) (d (n "jmdict-enums") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "libflate") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0d13pz7ydjspzjpljsxq7wp1js6vk82f3p0vv1r88yxqglhk0d2j")))

