(define-module (crates-io jm di jmdict-enums) #:use-module (crates-io))

(define-public crate-jmdict-enums-0.99.1 (c (n "jmdict-enums") (v "0.99.1") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 1)))) (h "1p8wm4796sf6i7j5y08w34kl9wwczyvpi34mfzr4g0zzjkwawmsi") (f (quote (("translations-swe") ("translations-spa") ("translations-slv") ("translations-rus") ("translations-hun") ("translations-ger") ("translations-fre") ("translations-eng") ("translations-dut") ("scope-archaic"))))))

(define-public crate-jmdict-enums-1.0.0 (c (n "jmdict-enums") (v "1.0.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 1)))) (h "09qrzmsrfyw3csrfn0akicb0dy4kynlp6ir7anj3aqi5n9gphw48") (f (quote (("translations-swe") ("translations-spa") ("translations-slv") ("translations-rus") ("translations-hun") ("translations-ger") ("translations-fre") ("translations-eng") ("translations-dut") ("scope-archaic"))))))

(define-public crate-jmdict-enums-2.0.0 (c (n "jmdict-enums") (v "2.0.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 1)))) (h "0vwnxn6c09b043prd6c6sjzjaiir27l1cfdqsryx7mdkcyqfixjs") (f (quote (("translations-swe") ("translations-spa") ("translations-slv") ("translations-rus") ("translations-hun") ("translations-ger") ("translations-fre") ("translations-eng") ("translations-dut") ("scope-archaic"))))))

