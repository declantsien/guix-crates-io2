(define-module (crates-io jm ap jmap) #:use-module (crates-io))

(define-public crate-jmap-0.0.1 (c (n "jmap") (v "0.0.1") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "066mdyx2fh9w6gia982g9ca9a49p1fygkyj53c2a9wwyvcr54hqb")))

(define-public crate-jmap-0.0.2 (c (n "jmap") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "07q4nikngfcc8illwfbskdp9v0j4821fwpz2lm0813s2908dr3sf")))

(define-public crate-jmap-0.0.3 (c (n "jmap") (v "0.0.3") (d (list (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1dw659vw7c192g6c1gwcpi1jgz1pn305fr75s8xhnpz90ss3ix1h")))

(define-public crate-jmap-0.0.4 (c (n "jmap") (v "0.0.4") (d (list (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1zz900xmyd28bqpmxw29alpj6116lqd3swsdb4j9hp1xbpcm8sk8")))

(define-public crate-jmap-0.0.5 (c (n "jmap") (v "0.0.5") (d (list (d (n "chrono") (r "0.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.*") (d #t) (k 0)) (d (n "uuid") (r "0.*") (d #t) (k 0)))) (h "1z163lrdgv6isqsqphif02k6c8dza0mlrlgj0y6r21kdmap84sv7")))

