(define-module (crates-io jm es jmespatch) #:use-module (crates-io))

(define-public crate-jmespatch-0.3.0 (c (n "jmespatch") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "slug") (r "^0.1") (d #t) (k 1)))) (h "1w55qrm5v129q5vhvlfky0a3z0sil009bvidva74vqxd6akr3kvs") (f (quote (("sync") ("specialized"))))))

