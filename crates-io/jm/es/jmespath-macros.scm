(define-module (crates-io jm es jmespath-macros) #:use-module (crates-io))

(define-public crate-jmespath-macros-0.1.0 (c (n "jmespath-macros") (v "0.1.0") (d (list (d (n "jmespath") (r "^0.1.0") (d #t) (k 0)))) (h "1p2m0wnwkzaky6ps13nl5ig4lhz15jmxc57cq445k9wrznalgwd5")))

(define-public crate-jmespath-macros-0.1.1 (c (n "jmespath-macros") (v "0.1.1") (d (list (d (n "jmespath") (r "^0.2.0") (d #t) (k 0)))) (h "1l03riln0wq0f5p8hddcck131kazmv2ypk1y16znsphk5gq2s93w")))

