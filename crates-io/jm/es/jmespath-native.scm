(define-module (crates-io jm es jmespath-native) #:use-module (crates-io))

(define-public crate-jmespath-native-0.1.0 (c (n "jmespath-native") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "slyce") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ih7yag67iii0hpd5n3cgin0xma5vv1mdzcnhngm29z5ls35bzgr")))

