(define-module (crates-io jm c- jmc-tools) #:use-module (crates-io))

(define-public crate-jmc-tools-0.0.1 (c (n "jmc-tools") (v "0.0.1") (h "1w410qy9h74n7xfdm5piyfys4048naz6jj3c715d5jx61km8ppg0") (y #t)))

(define-public crate-jmc-tools-0.0.2 (c (n "jmc-tools") (v "0.0.2") (h "0h26aihmd3glz919r72yb2g4rlc3cjcr4n3cbpwrwlfazl83cair") (y #t)))

(define-public crate-jmc-tools-1.0.0 (c (n "jmc-tools") (v "1.0.0") (h "1b1yv7kshqg37ni0jxncfv7l1ixl63gav7jdx1xm9hdri8kyxqql")))

