(define-module (crates-io jm sp jmspack-rust) #:use-module (crates-io))

(define-public crate-jmspack-rust-0.1.0 (c (n "jmspack-rust") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0g3rxs52qf3hly6hjqvsa4s87mybsn6ic2mjjw0g4f8i9yx7yp88") (r "1.56.0")))

