(define-module (crates-io it ex itex2mml) #:use-module (crates-io))

(define-public crate-itex2mml-0.1.0 (c (n "itex2mml") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0abdgsqsbjinaxf2pxrld5icsgdcxq9gdhsgsdxp4918v49p9j5a")))

(define-public crate-itex2mml-0.1.1 (c (n "itex2mml") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19lnkw52wkkasp25zmhx6sfrzxngsmp45gf2d3646jxg0ni2aqj6")))

(define-public crate-itex2mml-0.1.2 (c (n "itex2mml") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0npszr3zxr8f4cvd31h5wv9gzmad2k009r9k4a3hr9djmx3pn7rl")))

(define-public crate-itex2mml-0.2.0 (c (n "itex2mml") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "053hc59hccrlzlry5h7n2qg0spvjq3l87al44mkd2h51qzqqnpsd")))

