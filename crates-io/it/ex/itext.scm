(define-module (crates-io it ex itext) #:use-module (crates-io))

(define-public crate-itext-0.1.0 (c (n "itext") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (o #t) (d #t) (k 1)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "093m3h31nxcvfz9fc5mrvzwqq8hf9ab91i1zq5jzyvcj2lmbzz9f") (f (quote (("bundled" "color-eyre"))))))

(define-public crate-itext-0.2.0 (c (n "itext") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6.2") (o #t) (d #t) (k 1)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "10w2dg5jky352i2hd47ar8v3k1zvydkh167iqfbi2jarwx4l2d6j") (f (quote (("bundled" "color-eyre"))))))

(define-public crate-itext-0.2.2 (c (n "itext") (v "0.2.2") (d (list (d (n "color-eyre") (r "^0.6.2") (o #t) (d #t) (k 1)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1n4q29a3gm7i4pbfnm0na4krcha0qgk9mi9vg5csp9syllhrn3xl") (f (quote (("bundled" "color-eyre"))))))

