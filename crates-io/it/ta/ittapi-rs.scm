(define-module (crates-io it ta ittapi-rs) #:use-module (crates-io))

(define-public crate-ittapi-rs-0.1.0 (c (n "ittapi-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0vgnqmql4fhjz3l22mz3a871bsnz1yfrs3jm0584hr7kbsadrv4k") (f (quote (("force_32")))) (y #t)))

(define-public crate-ittapi-rs-0.1.1 (c (n "ittapi-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0mvjy4yz32x61xyp7immg3wbzhf432ar327vyh9zi5001g748ls6") (f (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.1.2 (c (n "ittapi-rs") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1w86x09akim87kxgx4qvcxqn3xhsv7fs0awz2d27kpj6qpy2pfaw") (f (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.1.3 (c (n "ittapi-rs") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 2)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "1175028j2xway7qc8ksgvp6fjw32l57aa02zkbhapb218ysq0aik") (f (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.1.4 (c (n "ittapi-rs") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 2)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "177jvh1ifbq83kw67dmb11mbn8g6xs19ql025vjhmahhhd0dq47a") (f (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.1.5 (c (n "ittapi-rs") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 2)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "1c7qy7i897kdxcjvq9q1my3ckn34xcxf6cv78p2fa6b323vxl5ps") (f (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.1.6 (c (n "ittapi-rs") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 2)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "0n1nfacd1bmf0lgcxngmc6mzf6mgnxmgd8cppw19ngmmsmfd4pi6") (f (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.2.0 (c (n "ittapi-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 2)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "1c3j67fpha5sxymwnc6gz0ig144d3qrjqxy2mnvvybyp3a5684pp")))

(define-public crate-ittapi-rs-0.3.0 (c (n "ittapi-rs") (v "0.3.0") (h "12czvcrz25jsgf65vd22l1ylw8g4yypca938nlrbx7009p4vqd6w")))

