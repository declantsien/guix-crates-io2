(define-module (crates-io it ta ittapi-sys) #:use-module (crates-io))

(define-public crate-ittapi-sys-0.3.0 (c (n "ittapi-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 2)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "18ghrw1vjbg81sww8appyj9cw3z6fvxqckbcyxjbwfj3ip1qa3wd")))

(define-public crate-ittapi-sys-0.3.1 (c (n "ittapi-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 2)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "00q787ciwlg5wbn3fw02a7djhkiigx98lixbflfwff1z32vi26g2")))

(define-public crate-ittapi-sys-0.3.2 (c (n "ittapi-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 2)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "1njfwvlbqzx1gly8a0jm0vrfvah4sr6v7gp3p8cg918lw367iq47")))

(define-public crate-ittapi-sys-0.3.3 (c (n "ittapi-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 2)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "19f11lg8dgd51w6mqmr5irchhskb5k0z6m6yilblh9gp4d6jmcx9")))

(define-public crate-ittapi-sys-0.3.4 (c (n "ittapi-sys") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 2)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "0z572hb4qgqsja2n3bmf78qy6csyic6rkhnc1mm6skp5jqy7dy7j")))

(define-public crate-ittapi-sys-0.3.5 (c (n "ittapi-sys") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 2)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "1n972w6wcna94rir1r3k1j0imksqywkx3vk0lqv0a1k56x3mwyyb")))

(define-public crate-ittapi-sys-0.4.0 (c (n "ittapi-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 2)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)))) (h "1z7lgc7gwlhcvkdk6bg9sf1ww4w0b41blp90hv4a4kq6ji9kixaj")))

