(define-module (crates-io it ta ittapi) #:use-module (crates-io))

(define-public crate-ittapi-0.3.0 (c (n "ittapi") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "ittapi-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "scoped-env") (r "^2.1") (d #t) (k 2)))) (h "1n5i2kddfcd7ra1g87a091g5anpqv8mcr8q34zhrj2vxvalsdfx4")))

(define-public crate-ittapi-0.3.1 (c (n "ittapi") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "ittapi-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "scoped-env") (r "^2.1") (d #t) (k 2)))) (h "18dilrgc0kdcv508220axmlwiq9yrnln864qkvsiy1vh01ay0gv6")))

(define-public crate-ittapi-0.3.2 (c (n "ittapi") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "ittapi-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "scoped-env") (r "^2.1") (d #t) (k 2)))) (h "1x227sj52zmq0vmilwi9m4b3fzhwa2qm0ldcil2fg70n0vzzdi78")))

(define-public crate-ittapi-0.3.3 (c (n "ittapi") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "ittapi-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "scoped-env") (r "^2.1") (d #t) (k 2)))) (h "19v29bx28ddl2slmb93gh9a201smhw31m8awqcx7vkkjf51qqr1f")))

(define-public crate-ittapi-0.3.4 (c (n "ittapi") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "ittapi-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "scoped-env") (r "^2.1") (d #t) (k 2)))) (h "0b2qh33qgdvbn6wgblcq9j5nnahi00sdw3mqx2kr4gdmnfvx1q21")))

(define-public crate-ittapi-0.3.5 (c (n "ittapi") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "ittapi-sys") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "scoped-env") (r "^2.1") (d #t) (k 2)))) (h "0wbjdpq28dvm5iigi4jmscvz7nf5cmjhgsi2c9wss730jfww1995")))

(define-public crate-ittapi-0.4.0 (c (n "ittapi") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "ittapi-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "scoped-env") (r "^2.1") (d #t) (k 2)))) (h "1cb41dapbximlma0vnar144m2j2km44g8g6zmv6ra4y42kk6z6bb")))

