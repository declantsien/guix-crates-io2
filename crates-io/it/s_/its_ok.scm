(define-module (crates-io it s_ its_ok) #:use-module (crates-io))

(define-public crate-its_ok-0.1.0 (c (n "its_ok") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 2)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("fold" "full" "parsing"))) (d #t) (k 0)))) (h "1k6x4r5n5sza097439j2qyb7g0hlf72dq9rwc55j9z316jrdiw8m")))

