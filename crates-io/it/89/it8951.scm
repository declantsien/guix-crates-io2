(define-module (crates-io it #{89}# it8951) #:use-module (crates-io))

(define-public crate-it8951-0.1.0 (c (n "it8951") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "1liagdrwsxhii337zd527052mwknzzqwi4rgza387f0hlyv2ia70")))

(define-public crate-it8951-0.2.0 (c (n "it8951") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)))) (h "1px01dl6xglpxnx1ayh7byma0b4xwadp0wqyvm2xmkca5xqfmv41")))

