(define-module (crates-io it el itelem) #:use-module (crates-io))

(define-public crate-itelem-0.1.0 (c (n "itelem") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1x5niyr18pwz168gpbq4yjq6mdpgf4bj9w61g24491kl17qqjwli")))

(define-public crate-itelem-0.1.1 (c (n "itelem") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1vly2wgyllrv8iwk3avf5xcrs18lpl16x8p7hjcbpx0czl2bhdx1")))

(define-public crate-itelem-0.1.2 (c (n "itelem") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1n1bzqhaxva302ds4b0jf97gzpplmjb880z8s18zww0vsrvhpfc7")))

(define-public crate-itelem-0.1.3 (c (n "itelem") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0ma3n4vbdx049yvkdkgh9vmvv78r7qy79vgabdxyss9na53wxcj5")))

(define-public crate-itelem-0.1.4 (c (n "itelem") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1i37qpxbbbrdac5i2r7rmln192wr7vpl44pi60j7yhhdjhrhmsn6")))

(define-public crate-itelem-0.1.5 (c (n "itelem") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "yore") (r "^1.0.2") (d #t) (k 0)))) (h "03hk0i1gk0sjp98bfld1pwapnwvlafw8877nbd4gmdwgpbbpqqr9")))

