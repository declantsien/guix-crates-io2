(define-module (crates-io it em item) #:use-module (crates-io))

(define-public crate-item-0.1.0 (c (n "item") (v "0.1.0") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "1rwy7f0byrxphqh44xc5rkcjwwahbw5gqiwqfxhfpc3lss6bz7ch") (y #t)))

(define-public crate-item-0.2.0 (c (n "item") (v "0.2.0") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "19ds09gpy8506a0185165k607w5hz0kv3l0l2pq0kj9djfmr61hd") (y #t)))

(define-public crate-item-0.3.0 (c (n "item") (v "0.3.0") (d (list (d (n "nom") (r "^1.2.4") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1qjzcrzvn57qgwnl1zdk2gfvwy7ysmyf1kzvgwr55zi731v18x6n") (f (quote (("printing" "quote") ("parsing" "nom") ("default" "parsing" "printing")))) (y #t)))

(define-public crate-item-0.3.1 (c (n "item") (v "0.3.1") (d (list (d (n "nom") (r "^1.2.4") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0rlljxnbin8b4mmhacc9sf2caa4b7xigqpbvhzd7ji1gdp76ik8j") (f (quote (("printing" "quote") ("parsing" "nom") ("default" "parsing" "printing")))) (y #t)))

(define-public crate-item-0.3.2 (c (n "item") (v "0.3.2") (d (list (d (n "nom") (r "^1.2.4") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1rl5ral0a7ny60jlssivfbly4q4qhkyczh4p7s23nxrksj8lzkyn") (f (quote (("printing" "quote") ("parsing" "nom") ("default" "parsing" "printing")))) (y #t)))

