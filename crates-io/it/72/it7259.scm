(define-module (crates-io it #{72}# it7259) #:use-module (crates-io))

(define-public crate-it7259-0.0.1 (c (n "it7259") (v "0.0.1") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "^1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")))) (h "0wzvr1za3702jfypkgx3bhiabyf4hgqzpwiiyzd047czcb6igwsr")))

