(define-module (crates-io it co itconfig-macro) #:use-module (crates-io))

(define-public crate-itconfig-macro-1.0.0 (c (n "itconfig-macro") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0i7fjm8bb8g2hpd91gaggvjif27mg738b4qjzg7idw77danaa237")))

(define-public crate-itconfig-macro-1.0.1 (c (n "itconfig-macro") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "12nzzqafw8wpf5mcyh8190mwfbjga8jdl74xmr21kakkxijrv7gj")))

(define-public crate-itconfig-macro-1.0.2 (c (n "itconfig-macro") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "08dahjfhkvawb8b5rj54rd2gwklvcrkld5y6p0snca4fb958qvyw")))

(define-public crate-itconfig-macro-1.1.0 (c (n "itconfig-macro") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "0bbx85603v49274ihx30rngpnvbfm2143chmw7k0p3gcl8fi6jhq")))

(define-public crate-itconfig-macro-1.1.1 (c (n "itconfig-macro") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "0kkic4ccfhxfh9w20a7dy4h5vfjp69k8vfzf5vr7ssbdb3a7ad9d")))

