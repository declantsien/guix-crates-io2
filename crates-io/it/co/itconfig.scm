(define-module (crates-io it co itconfig) #:use-module (crates-io))

(define-public crate-itconfig-0.1.0 (c (n "itconfig") (v "0.1.0") (h "1x3g57awh98q6xa5xppvj09j71wj1ajm4a1ns0rkjl9xbl55z6fw")))

(define-public crate-itconfig-0.1.1 (c (n "itconfig") (v "0.1.1") (h "1dwj099l4q190w1nbpbmab00l3a7057qganbjcfym9hzy77dl8ln")))

(define-public crate-itconfig-0.2.0 (c (n "itconfig") (v "0.2.0") (h "1p7p3kqqhw1d8x9qf695p5251z8wjka59rifnmqbgrb67j6g0yik")))

(define-public crate-itconfig-0.2.1 (c (n "itconfig") (v "0.2.1") (h "1f299f0y0a4yphk4qmh3flzchxff0b367j4ij0iids0yms6dfiig")))

(define-public crate-itconfig-0.2.2 (c (n "itconfig") (v "0.2.2") (h "1wc2iyq1c6pckpcgj7i0mmav4j4qrc3sx8agkwbw44qq01jf3yyi")))

(define-public crate-itconfig-0.2.3 (c (n "itconfig") (v "0.2.3") (h "1byi8hlg5lxss8gbjcxbdfmp7xbwa1yvi7h3761vc6i81c4q2ram")))

(define-public crate-itconfig-0.3.0 (c (n "itconfig") (v "0.3.0") (h "0hs2ilfrpj0b3cyf01y2fy305axz0pxdvp7wzwg3j3lilhpprzh5")))

(define-public crate-itconfig-0.4.0 (c (n "itconfig") (v "0.4.0") (h "04wlp1x69mrd1bmlm5qa2fc5xvgy980hdy2v3wi6n8rnh48wbs6s")))

(define-public crate-itconfig-0.5.0 (c (n "itconfig") (v "0.5.0") (h "0x0i3hn19xrb1s5ab4xzz8cdzaskhhbi84ah32dclz47f282716v")))

(define-public crate-itconfig-0.5.1 (c (n "itconfig") (v "0.5.1") (h "0d80ni4afsk531mcjglsyfm48cp9hpfdfl0nxyvb3r78fq4xmbhr")))

(define-public crate-itconfig-0.6.0 (c (n "itconfig") (v "0.6.0") (h "060mrkcav2q1if35c27bax9n1rji5r2kr3b46a6811rxmp2rg7wr")))

(define-public crate-itconfig-0.7.0 (c (n "itconfig") (v "0.7.0") (h "1kyra4har82r6vpfsnsy142hrpmwxpnl1jqv5ba7ddf1vvmrgbjx")))

(define-public crate-itconfig-0.7.1 (c (n "itconfig") (v "0.7.1") (h "1sf2klbn3raw1lfxnzfgfhvg1qar8nnkgdxv36zihdzp26x5mccw")))

(define-public crate-itconfig-0.8.0 (c (n "itconfig") (v "0.8.0") (h "1xxlnkw36bmy3f2b0hkiw2iiz2pjcamnbr15pqj8z40sn0nm8zrf")))

(define-public crate-itconfig-0.9.0 (c (n "itconfig") (v "0.9.0") (h "06rxzkb461s2i6yvdi0jvlhr5iy32riwdlragfxv3ypdzvg34mdr") (f (quote (("macro") ("default" "macro"))))))

(define-public crate-itconfig-0.10.0 (c (n "itconfig") (v "0.10.0") (d (list (d (n "failure") (r "^0.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kcqxf3wjlgskjf1bqa5f2glcc1y3j9v0ppmp8pb8pc8jcvq6fix") (f (quote (("macro") ("default" "macro"))))))

(define-public crate-itconfig-0.10.1 (c (n "itconfig") (v "0.10.1") (d (list (d (n "failure") (r "^0.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jhx1psmwk5lmzjnzw00wmc75rd8jm79iayrmsnzvbag9djsql26") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("numbers" "int" "uint" "float") ("macro") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "macro" "numbers" "bool") ("bool"))))))

(define-public crate-itconfig-0.10.2 (c (n "itconfig") (v "0.10.2") (d (list (d (n "failure") (r "^0.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (o #t) (d #t) (k 0)))) (h "1c6gp0pdwd1iw08zpcfk4pkmq0yqzyfnmr5qgrlqk8zd9gygfg53") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "macro" "primitives") ("bool") ("array" "serde_json"))))))

(define-public crate-itconfig-0.11.0 (c (n "itconfig") (v "0.11.0") (d (list (d (n "failure") (r "^0.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (o #t) (d #t) (k 0)))) (h "0lwnczzv86lp4l6ymy1mp2damn4xnaqz27ybczip2rkv2vdcaak1") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("static" "lazy_static") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "macro" "primitives") ("bool") ("array" "serde_json")))) (y #t)))

(define-public crate-itconfig-0.11.1 (c (n "itconfig") (v "0.11.1") (d (list (d (n "failure") (r "^0.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (o #t) (d #t) (k 0)))) (h "10qr529iyj278q440nyx848j3iqlslbjaj7myrlwl1qlj2idvrsd") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("static" "lazy_static") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "macro" "primitives" "static") ("bool") ("array" "serde_json"))))))

(define-public crate-itconfig-0.11.2 (c (n "itconfig") (v "0.11.2") (d (list (d (n "failure") (r "^0.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (o #t) (d #t) (k 0)))) (h "0jbr3iivzljksamrwi12c3zwspqaqxnmjwfra8l7jhhrjk0jd5ah") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("static" "lazy_static") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "macro" "primitives" "static") ("bool") ("array" "serde_json"))))))

(define-public crate-itconfig-1.0.0 (c (n "itconfig") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itconfig-macro") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.44") (o #t) (d #t) (k 0)))) (h "0gg6idcdxzawjg6qxgh56pqhbgca57pghy4a2m4h8z0qsa9wjkz9") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro" "itconfig-macro") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "primitives") ("bool") ("array" "serde_json"))))))

(define-public crate-itconfig-1.0.1 (c (n "itconfig") (v "1.0.1") (d (list (d (n "failure") (r "^0.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itconfig-macro") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.44") (o #t) (d #t) (k 0)))) (h "0d917hbibmyfjzi6ndksdrnlycybswma67ff8ri35h0hxkdiabar") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro" "itconfig-macro") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "primitives") ("bool") ("array" "serde_json"))))))

(define-public crate-itconfig-1.0.2 (c (n "itconfig") (v "1.0.2") (d (list (d (n "failure") (r "^0.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itconfig-macro") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.44") (o #t) (d #t) (k 0)))) (h "02wlwdciilk2lwa4z7dcmsqlcpclv8z1n1l8k2skas1nzp7aq66p") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro" "itconfig-macro") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "primitives") ("bool") ("array" "serde_json"))))))

(define-public crate-itconfig-1.0.3 (c (n "itconfig") (v "1.0.3") (d (list (d (n "itconfig-macro") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0gm35azbxkh92ixys1zmqzvm9vlax5cxwp2fr4j7ahf10fcmll38") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro" "itconfig-macro") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "primitives") ("bool") ("array" "serde_json"))))))

(define-public crate-itconfig-1.0.4 (c (n "itconfig") (v "1.0.4") (d (list (d (n "itconfig-macro") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "02nrw8wagk2i1cjxb98vh01ba8s1xv0bkfjchrla50w7ihksjmsw") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro" "itconfig-macro") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "primitives") ("bool") ("array" "serde_json"))))))

(define-public crate-itconfig-1.0.5 (c (n "itconfig") (v "1.0.5") (d (list (d (n "itconfig-macro") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0zapb2s7yhaqg3bc61kbsw06ynpl95bb5wfifavk3ns5q7cgarc1") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro" "itconfig-macro") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "primitives") ("bool") ("array" "serde_json"))))))

(define-public crate-itconfig-1.1.0 (c (n "itconfig") (v "1.1.0") (d (list (d (n "itconfig-macro") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1s6khh0ba9nmdpm5yb5r4mszln8gjjqid01hdmbk81vkssh4cv4m") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro" "itconfig-macro") ("json_array" "serde_json") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "primitives") ("bool"))))))

(define-public crate-itconfig-1.1.1 (c (n "itconfig") (v "1.1.1") (d (list (d (n "itconfig-macro") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0831kpj814mrab628rlgz7d7ci3ccs1f21si7707ql8wrak0j13w") (f (quote (("usize") ("uint" "u8" "u16" "u32" "u64" "u128" "usize") ("u8") ("u64") ("u32") ("u16") ("u128") ("primitives" "numbers" "bool") ("numbers" "int" "uint" "float") ("macro" "itconfig-macro") ("json_array" "serde_json") ("isize") ("int" "i8" "i16" "i32" "i64" "i128" "isize") ("i8") ("i64") ("i32") ("i16") ("i128") ("float" "f32" "f64") ("f64") ("f32") ("default" "primitives") ("bool"))))))

