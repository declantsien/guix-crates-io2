(define-module (crates-io it rs itrs) #:use-module (crates-io))

(define-public crate-itrs-0.1.0 (c (n "itrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.9") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1szg97yhad8ccrymn4sw78h0xic2paky1s60dhnp77sfs8f56r0k") (r "1.70")))

(define-public crate-itrs-0.1.1 (c (n "itrs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.9") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0jbyrhkzm907d1yf2cap4qjk09anwfmax1l2dq5wwmwsg84dqvrb") (r "1.70")))

(define-public crate-itrs-0.1.2 (c (n "itrs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.9") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "12wmgr78p0lswrs4c8p9lgxxda4xnfi6mklhnv9pm27fwc797w1a") (r "1.70")))

(define-public crate-itrs-0.1.3 (c (n "itrs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.9") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "120cf5y6qbq5zlsdyf2bh12ndaycrn6gyfvqyf9bj7xmqk9fvq91") (r "1.70")))

