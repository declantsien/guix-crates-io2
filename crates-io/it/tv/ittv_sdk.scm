(define-module (crates-io it tv ittv_sdk) #:use-module (crates-io))

(define-public crate-ittv_sdk-0.1.0 (c (n "ittv_sdk") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.26") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j1mgbm1qjj69xz7j3mw3az61fknaj1mnpmnvi013pkl213ga6n4")))

(define-public crate-ittv_sdk-0.1.1 (c (n "ittv_sdk") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.26") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p7sqjdqpi9iaxldmy7fgjvcdc083hqdx3vwfx1avjkychwfpbbn")))

(define-public crate-ittv_sdk-0.1.2 (c (n "ittv_sdk") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.26") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q1hqnpyq388bc57nh252pmaqzcqq9j5zr7d46hg6z5j93fyaj1b")))

(define-public crate-ittv_sdk-0.1.3 (c (n "ittv_sdk") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.26") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fdq6yskm0zzyb6yiq6x5p3s459526pqaimszckrzn5lwszsq30g")))

(define-public crate-ittv_sdk-0.1.4 (c (n "ittv_sdk") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.26") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "17abyr4hlmv2wrn1k05hzsfddmh1yfbjs581jijjr94dd75rcbyn")))

