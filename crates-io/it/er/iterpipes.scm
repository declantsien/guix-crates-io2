(define-module (crates-io it er iterpipes) #:use-module (crates-io))

(define-public crate-iterpipes-0.1.0 (c (n "iterpipes") (v "0.1.0") (d (list (d (n "time") (r "^0.2.9") (d #t) (k 2)))) (h "1j19nrdbmirm987412w47c56qd3bs0xppi3mc7dy5v44lhscwvrz")))

(define-public crate-iterpipes-0.1.1 (c (n "iterpipes") (v "0.1.1") (d (list (d (n "time") (r "^0.2.9") (d #t) (k 2)))) (h "0sghi61kmllbmmxf5kbgl7rk6sq4bjl9wjp4w5wa6d3aq58n8ylv")))

(define-public crate-iterpipes-0.1.2 (c (n "iterpipes") (v "0.1.2") (d (list (d (n "time") (r "^0.2.9") (d #t) (k 2)))) (h "0sa4r8lbwslhpp5j436i5dp30sglmy5ma2nif1wac2y7i385mklv")))

(define-public crate-iterpipes-0.1.3 (c (n "iterpipes") (v "0.1.3") (d (list (d (n "time") (r "^0.2.9") (d #t) (k 2)))) (h "12h4f6dwj1gqgj0nl40g8hk5wdmg8pq4wljb75gglah9mfp1smbm")))

(define-public crate-iterpipes-0.1.4 (c (n "iterpipes") (v "0.1.4") (d (list (d (n "time") (r "^0.2.9") (d #t) (k 2)))) (h "0s9qg93mqggs2k1r3q8pyqjfqryq0gya37rgjzvhj6qb0z82yy1b")))

(define-public crate-iterpipes-0.2.0 (c (n "iterpipes") (v "0.2.0") (d (list (d (n "time") (r "^0.2.9") (d #t) (k 2)))) (h "02fjyr4jbmg3vgw2slrjwsl1mg61kfd1rqg0i9nmls3685ywb0jh")))

