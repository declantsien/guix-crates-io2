(define-module (crates-io it er itertools-num) #:use-module (crates-io))

(define-public crate-itertools-num-0.1.0 (c (n "itertools-num") (v "0.1.0") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0yjr572kfkgq7gq50ap80i369w8yc9hm2wxcpcvw380ji50ryi97")))

(define-public crate-itertools-num-0.1.1 (c (n "itertools-num") (v "0.1.1") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "04vqf6k4zc8i8wxvjgi0sxw6hp8xk6n65y9np84fdrl3hdhgly2d")))

(define-public crate-itertools-num-0.1.2 (c (n "itertools-num") (v "0.1.2") (d (list (d (n "itertools") (r "^0.7.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "1z42m1ywrk9ivp5dwjj6hbxk9zhwxnhngwy2qr5y7wiqp1q7pjl3")))

(define-public crate-itertools-num-0.1.3 (c (n "itertools-num") (v "0.1.3") (d (list (d (n "itertools") (r "^0.7.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (k 2)))) (h "1rr7ig9nkpampcas23s91x7yac6qdnwssq3nap522xbgkqps4wm8")))

