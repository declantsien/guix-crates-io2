(define-module (crates-io it er iterate-text) #:use-module (crates-io))

(define-public crate-iterate-text-0.0.1 (c (n "iterate-text") (v "0.0.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0mbhm964gf8fmfr9rg2fvlpvxn5zwj2ijkfidgl18qvw44r88fl5")))

