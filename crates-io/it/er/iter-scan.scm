(define-module (crates-io it er iter-scan) #:use-module (crates-io))

(define-public crate-iter-scan-0.0.0 (c (n "iter-scan") (v "0.0.0") (h "1syd3g5ccsixm0g27mxjk60vdcfds0nqa3fdd0vkg0fpy8bp899s")))

(define-public crate-iter-scan-0.1.0 (c (n "iter-scan") (v "0.1.0") (h "0qr72pscfbr4h8nyddsdm3pg7lxiyak36z87dfnk5cnxsdaxdg5n")))

(define-public crate-iter-scan-0.2.0 (c (n "iter-scan") (v "0.2.0") (d (list (d (n "replace_with") (r "^0.1.7") (k 0)))) (h "0g32s69dznwxnh5kcz91b71ni6ashhw1a8crj61k187ci9fxjgr6") (f (quote (("std" "replace_with/std") ("default" "std"))))))

