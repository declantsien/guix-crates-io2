(define-module (crates-io it er iter-trait) #:use-module (crates-io))

(define-public crate-iter-trait-0.1.0 (c (n "iter-trait") (v "0.1.0") (d (list (d (n "bit-set") (r "^0") (o #t) (d #t) (k 0)) (d (n "bit-vec") (r "^0") (o #t) (d #t) (k 0)) (d (n "blist") (r "^0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "enum-set") (r "^0") (o #t) (d #t) (k 0)) (d (n "interval-heap") (r "^0") (o #t) (d #t) (k 0)) (d (n "linear-map") (r "^0") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0") (o #t) (d #t) (k 0)) (d (n "lru-cache") (r "^0") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0") (o #t) (d #t) (k 0)))) (h "1sldp74im38v9a4hqsma18s0zsa4qmv8zn60jh5vnxislnn9rdz0") (f (quote (("std" "collections") ("nightly") ("default" "std" "nightly") ("contain-rs" "std" "bit-set" "bit-vec" "blist" "enum-set" "interval-heap" "linear-map" "linked-hash-map" "lru-cache" "vec_map") ("collections"))))))

(define-public crate-iter-trait-0.2.0 (c (n "iter-trait") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "11p9sgm9gdjylij6mb4cy3d7qqhjlv2pyl4kma0pry277k3wkl6r") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-iter-trait-0.3.0 (c (n "iter-trait") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "102a2q10abpmzcmdd9v3dzvfmg4bfn8r7a3r0mdialmfwp0fvqa8") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-iter-trait-0.3.1 (c (n "iter-trait") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)))) (h "1gqa2blpk6icdjf7wzp5jgy6a3kspxkil5i9qdjwgg6kyy91b4pw") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

