(define-module (crates-io it er iter-python) #:use-module (crates-io))

(define-public crate-iter-python-0.9.0 (c (n "iter-python") (v "0.9.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 2)))) (h "0nmznrp7v2mziczmzv26yyb1qib6p15l6naiymiax0b370xz0vw9") (f (quote (("nightly") ("default"))))))

(define-public crate-iter-python-0.9.1 (c (n "iter-python") (v "0.9.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 2)))) (h "1gmdxvn3h0kn74alfjqpizbjvmmkffq747y09dhqdjmpqmwifc6f") (f (quote (("nightly") ("default"))))))

(define-public crate-iter-python-0.9.2 (c (n "iter-python") (v "0.9.2") (d (list (d (n "candy") (r "^0.1.3") (d #t) (k 0)) (d (n "join-lazy-fmt") (r "^0.9.2") (d #t) (k 0)))) (h "14n4fkfl4v5qxigcky8f6pjyv4fhqsgqhxw7b5z52hndw9sxf4gz") (f (quote (("nightly") ("default"))))))

(define-public crate-iter-python-0.10.0-rc1 (c (n "iter-python") (v "0.10.0-rc1") (d (list (d (n "join-lazy-fmt") (r "^0.9.2") (d #t) (k 0)))) (h "0giny5y25pv6wjan2xf119kds6bnyygcv0585vyah7f8c87spinz") (f (quote (("std") ("default" "std") ("better-docs"))))))

(define-public crate-iter-python-0.10.0 (c (n "iter-python") (v "0.10.0") (d (list (d (n "join-lazy-fmt") (r "^0.9.2") (d #t) (k 0)))) (h "0lpc86pb516k2cwq373b5kwvwzc95bkcz1l470hajpjl50b5nd0w") (f (quote (("std") ("default" "std") ("better-docs"))))))

(define-public crate-iter-python-0.10.1 (c (n "iter-python") (v "0.10.1") (d (list (d (n "join-lazy-fmt") (r "^0.9.2") (d #t) (k 0)))) (h "0lfkd3ymwrylmpk4yw31q9c2m0irr0pxifllpklz7rm18g8ry6sq") (f (quote (("std") ("default" "std") ("better-docs"))))))

