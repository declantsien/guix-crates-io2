(define-module (crates-io it er iter_fixed) #:use-module (crates-io))

(define-public crate-iter_fixed-0.1.0 (c (n "iter_fixed") (v "0.1.0") (h "0vbpd1pjsfa4848wzn04kmb1ca2yyf8xpjqr29207mb9wygmpsjx")))

(define-public crate-iter_fixed-0.1.1 (c (n "iter_fixed") (v "0.1.1") (h "0sqv0yybwwdiki4ayz5zb8scjwdvdm67gi26lc8klv0qi1h756ha")))

(define-public crate-iter_fixed-0.1.2 (c (n "iter_fixed") (v "0.1.2") (h "12bqr9v6iwd1snz75wfq7dyywh1fhh5b0d48annpqg3ajn82hcbk")))

(define-public crate-iter_fixed-0.1.3 (c (n "iter_fixed") (v "0.1.3") (h "19309fpmgjw8f9bkhmrzp0nlwiw9cq2fi4psk800cnhvad8nhbr2") (f (quote (("nightly_features") ("default" "nightly_features"))))))

(define-public crate-iter_fixed-0.2.0 (c (n "iter_fixed") (v "0.2.0") (h "1ddkqrix5ym3y3jw1p6vlz75qjkv1g7d8cghx8ib5zwi642f47b1") (f (quote (("nightly_features") ("default" "nightly_features"))))))

(define-public crate-iter_fixed-0.2.1 (c (n "iter_fixed") (v "0.2.1") (h "171nb9dcijf9005rcljz9g183h1b59qv7qjvyrv57w5cvafykns1") (f (quote (("nightly_features") ("default" "nightly_features"))))))

(define-public crate-iter_fixed-0.2.2 (c (n "iter_fixed") (v "0.2.2") (h "1msmfr8l4618wn8v0pk0s1l1i90mzlgp0nfglnixfsyspizn6vap") (f (quote (("nightly_features") ("default" "nightly_features"))))))

(define-public crate-iter_fixed-0.3.0 (c (n "iter_fixed") (v "0.3.0") (h "0wl9a24r6sb52zzy9rz56y4avj898nllsa49qf08ifln00482q8f") (f (quote (("nightly_features") ("default"))))))

(define-public crate-iter_fixed-0.3.1 (c (n "iter_fixed") (v "0.3.1") (h "0z3kc0q7a7h6myrfxpmnvfc9wv01w38lb13ajhnz4kzh1s0i67ad") (f (quote (("nightly_features") ("default"))))))

