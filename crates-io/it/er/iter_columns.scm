(define-module (crates-io it er iter_columns) #:use-module (crates-io))

(define-public crate-iter_columns-0.1.0 (c (n "iter_columns") (v "0.1.0") (d (list (d (n "iter_columns_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0s2ha64kj6irmppfiqn5v231yh4cqdha6y9ml8lvjj4szgd78rh4")))

(define-public crate-iter_columns-0.2.0 (c (n "iter_columns") (v "0.2.0") (d (list (d (n "iter_columns_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1wfm4yl6ibi220yzz6ilw0d7rbgplidr8kbh0zb1jzxl9p19cjil") (f (quote (("no_array"))))))

(define-public crate-iter_columns-0.2.1 (c (n "iter_columns") (v "0.2.1") (d (list (d (n "iter_columns_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1w9kp5531piccfc97v2b78xm84bvnw0p54w2xs1p2r2hprrsn1cr") (f (quote (("no_array"))))))

(define-public crate-iter_columns-0.3.0 (c (n "iter_columns") (v "0.3.0") (h "1amjzmjprb2myjdbsnijsbql02ll71a2hyhkqly7f7snli16kz6n") (f (quote (("array_into_iter"))))))

