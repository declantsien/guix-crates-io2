(define-module (crates-io it er iterstream) #:use-module (crates-io))

(define-public crate-iterstream-0.1.0 (c (n "iterstream") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.6") (f (quote ("thread-pool"))) (d #t) (k 0)))) (h "1mvk8vzaxi5ii16216fzl1iwwdqn6s8j0pcszgws6ys36f5fx14d")))

(define-public crate-iterstream-0.1.1 (c (n "iterstream") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.6") (f (quote ("thread-pool"))) (d #t) (k 0)))) (h "1jqwkb37a71bl5j64lanniymgl3pyzijnk33kc142h6cxd14jjif")))

(define-public crate-iterstream-0.1.2 (c (n "iterstream") (v "0.1.2") (d (list (d (n "futures") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 0)))) (h "0d0pj629xwyhvalmpkylqa4j6bj3qaijics3sv7zn1y9s12di9fl")))

