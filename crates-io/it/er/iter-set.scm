(define-module (crates-io it er iter-set) #:use-module (crates-io))

(define-public crate-iter-set-0.1.0 (c (n "iter-set") (v "0.1.0") (h "0lwjldnzvsdf70bl4sbnq8z9cm1zd1mklvfbmbiw5nfcsz0cv7is")))

(define-public crate-iter-set-0.1.1 (c (n "iter-set") (v "0.1.1") (h "0pn6miffxszcmna7k16hxf2salx9a6q102l4qbnmzcrp088qswhz")))

(define-public crate-iter-set-0.2.0 (c (n "iter-set") (v "0.2.0") (h "0gl2m03x9kyqida9745ssvgqpyjqsw0jz3rbzgwqdxqsz34b6sxl")))

(define-public crate-iter-set-1.0.0 (c (n "iter-set") (v "1.0.0") (h "1j2mbjybp27j2q3av3hs8vlqlbryj2q1i0kp3qiqja1iw5pkqmv4")))

(define-public crate-iter-set-1.0.1 (c (n "iter-set") (v "1.0.1") (h "080bfyhl6cjnf5d0zss6jxwgdncn2whzh2r2lawf91f6xmycn07f")))

(define-public crate-iter-set-1.0.2 (c (n "iter-set") (v "1.0.2") (h "1ka660h99wbbdjzpb3ibmnmf0jc1vcwq8nms1w10wipyncywgcxw")))

(define-public crate-iter-set-1.0.3 (c (n "iter-set") (v "1.0.3") (h "07sh9q0453wmrx9li3dx9hnww4xwdhxsk3rmjvfqw7j5wwrp6lqm")))

(define-public crate-iter-set-2.0.0 (c (n "iter-set") (v "2.0.0") (h "03z129rlqg360zjddvsi1gnyhssvfv1y51hlzf6cc1d771n7rjxy")))

(define-public crate-iter-set-2.0.1 (c (n "iter-set") (v "2.0.1") (h "04v8m78002cj3gml0fs7ba2l9jm52sh8m4grczxq5h8rp2nknnqg")))

(define-public crate-iter-set-2.0.2 (c (n "iter-set") (v "2.0.2") (h "16xdfdfrfhygmbvfnlx7g1wkav6vf0h68ycfhlfk8ib9n1zdx0yk")))

