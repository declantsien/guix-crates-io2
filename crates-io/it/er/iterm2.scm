(define-module (crates-io it er iterm2) #:use-module (crates-io))

(define-public crate-iterm2-0.1.0 (c (n "iterm2") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)))) (h "1njr60h0drgw18qjm7m3km150b2y2jw442kiwrikvi3chl145zw8")))

(define-public crate-iterm2-0.2.0 (c (n "iterm2") (v "0.2.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)))) (h "0qhirggdw2h9rybfigh16m8s0z1wnnyyb53m28pvn87rq2hw23nj")))

(define-public crate-iterm2-0.2.1 (c (n "iterm2") (v "0.2.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)))) (h "1ric9ypwj2ylalg9zjmacmzj5fs3r9xkai753h2gysk7rkfggxzv")))

