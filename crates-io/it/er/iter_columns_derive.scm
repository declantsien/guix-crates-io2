(define-module (crates-io it er iter_columns_derive) #:use-module (crates-io))

(define-public crate-iter_columns_derive-0.1.0 (c (n "iter_columns_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "01x86z0ifasqyfvyb8x607h6swvkavsh9q9nl7kahjc8rwzyx3w6") (y #t)))

(define-public crate-iter_columns_derive-0.1.1 (c (n "iter_columns_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0jqw3w55xc1d2vcr83l4lxw5965vw9w4d4wiqbpnjdlk4kl2kkqi") (y #t)))

