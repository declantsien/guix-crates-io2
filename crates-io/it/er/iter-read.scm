(define-module (crates-io it er iter-read) #:use-module (crates-io))

(define-public crate-iter-read-0.1.0 (c (n "iter-read") (v "0.1.0") (h "01gnzmr9sgr534chd4rnbhxynbfi5cn4kxbmjfad3xbp5bs8kdi9") (f (quote (("unstable"))))))

(define-public crate-iter-read-0.2.0 (c (n "iter-read") (v "0.2.0") (h "1gp2dw4jr00z1ffybkpkzirpz1aij78av68a6d1jynh5w4jzw8gx") (f (quote (("unstable"))))))

(define-public crate-iter-read-0.2.1 (c (n "iter-read") (v "0.2.1") (h "00xk55m2yjd7cjc9yyhxhr73bmg5p96n2r21b46zga5bvrwn6gnh") (f (quote (("unstable"))))))

(define-public crate-iter-read-0.3.0 (c (n "iter-read") (v "0.3.0") (h "0pczfmfpzpq5w6i5ik6i3lf8pky24h8ax284pd4j83r97cblr8sf") (f (quote (("unstable"))))))

(define-public crate-iter-read-0.3.1 (c (n "iter-read") (v "0.3.1") (h "1i4kiz7y1aj3a7ynj76afsikc4kpnhlfl7s5xk20kmasl0zcm5y3") (f (quote (("unstable"))))))

(define-public crate-iter-read-1.0.0 (c (n "iter-read") (v "1.0.0") (h "0kyhi9f45bf4lf0ndk1psfk76w8qx85l3259wk39pkjyg0k8r57s") (f (quote (("unstable"))))))

(define-public crate-iter-read-1.0.1 (c (n "iter-read") (v "1.0.1") (h "12q45qrvyx88kmdzs7shnsdjmg5najr6hy2ivbmmcd4fmsmw3655") (f (quote (("unstable"))))))

