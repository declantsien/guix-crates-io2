(define-module (crates-io it er itermap) #:use-module (crates-io))

(define-public crate-itermap-0.1.0 (c (n "itermap") (v "0.1.0") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "0acvrghz7zj1954lqlnls43jp6y06nv2s5lbdwccw66jwysc6p3r")))

(define-public crate-itermap-0.2.0 (c (n "itermap") (v "0.2.0") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "05ak951440rzg09x3cha2a5dis87p5znzw12vpd7m6wamv48rbw0")))

(define-public crate-itermap-0.2.1 (c (n "itermap") (v "0.2.1") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "0zfa9q7flw8i8xdj2j2ghjdrpf6xa9dks3ralbyph4dhq40b9wwk")))

(define-public crate-itermap-0.2.2 (c (n "itermap") (v "0.2.2") (d (list (d (n "maplit") (r "^1") (d #t) (k 2)))) (h "16p9p8cvvbc28p2r0mmhiafz3aij2yv3py2q4v9hy2swrpg3ykqn")))

(define-public crate-itermap-0.3.0 (c (n "itermap") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0zvl8pj827hp0lgyidni6xv1i9d2984i6bh6x81rmmn8lw7k0l8r") (y #t)))

(define-public crate-itermap-0.4.0 (c (n "itermap") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0g2ir3v1ch7m15d5ahn5lf20xxc4bs0q4s4h8h09923b19r1kmwk")))

