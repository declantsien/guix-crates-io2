(define-module (crates-io it er iter_num_tools) #:use-module (crates-io))

(define-public crate-iter_num_tools-0.1.0 (c (n "iter_num_tools") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "15m4cf5bmcnynpyy1s2a61s2hlq2hi1w4vjnhy3i8vd2479v878n")))

(define-public crate-iter_num_tools-0.1.1 (c (n "iter_num_tools") (v "0.1.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1waqd3d1jhjxa5dkdz7wq5ymrcxg8k1mcapr8z0dl0000z8vqgax")))

(define-public crate-iter_num_tools-0.1.2 (c (n "iter_num_tools") (v "0.1.2") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0amm14mjzh2p3s3gkwk7f67vqd9mp81zkd3lmmpbfnfwmxmj3znf")))

(define-public crate-iter_num_tools-0.1.3 (c (n "iter_num_tools") (v "0.1.3") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0jd511wmr1aqv11xd4j5414f6hn8mbmm247bdrcnzri6dicvmxpg")))

(define-public crate-iter_num_tools-0.1.4 (c (n "iter_num_tools") (v "0.1.4") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1hcq7r7hl9zffv9kci25iqnwfhvkwraricvggz7sdjzlmrmwlrqi")))

(define-public crate-iter_num_tools-0.1.5 (c (n "iter_num_tools") (v "0.1.5") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0pzjg8b2riywd1vl5r7yjqb496mx483qmqapbjdq73pyg9g98xf7")))

(define-public crate-iter_num_tools-0.1.6 (c (n "iter_num_tools") (v "0.1.6") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "175bihllvc8s8571gq9snmv5dmzw90f05sb2zwa562hgfsdlcwns")))

(define-public crate-iter_num_tools-0.1.7 (c (n "iter_num_tools") (v "0.1.7") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "029xi14p6b1qwkyhqfzlmfb6j416339zcxzclqg863kcrir81ixl")))

(define-public crate-iter_num_tools-0.1.8 (c (n "iter_num_tools") (v "0.1.8") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1sbrrx74221vva0x8qb1yf6yp6hng5v37y3sc5ggvci3dcwhykhd")))

(define-public crate-iter_num_tools-0.1.9 (c (n "iter_num_tools") (v "0.1.9") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0h4s2rs6xcwldhwwypjrc87r5hsahr9f6jfrvd4zg3fsvmkpdk4n")))

(define-public crate-iter_num_tools-0.1.10 (c (n "iter_num_tools") (v "0.1.10") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "01qh1k6wishpxqzkwgqafic0ibfr9dnf1sliw1ry5k16lzp92n72")))

(define-public crate-iter_num_tools-0.1.11 (c (n "iter_num_tools") (v "0.1.11") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0bwxq3r1s12xqv2f5c175gr4p93p3lxnp6m51izlca85sjxsr2mb")))

(define-public crate-iter_num_tools-0.1.12 (c (n "iter_num_tools") (v "0.1.12") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "16mwx7r8biwckh4hw910ydd90q8izmf3qy7c2vfq2sfl7hm9896h")))

(define-public crate-iter_num_tools-0.1.13 (c (n "iter_num_tools") (v "0.1.13") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0158r0kpvcxcy2379mmb3x5x7hbz3wv6mxz9a1id61xw5h1djnnh")))

(define-public crate-iter_num_tools-0.1.14 (c (n "iter_num_tools") (v "0.1.14") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1c4g638idfbvk09j2rjbbriyl8cpqxngkiv3z7wam51xsqzgzrmh")))

(define-public crate-iter_num_tools-0.1.15 (c (n "iter_num_tools") (v "0.1.15") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0zl3v43wfr8djs5a2nsjl1gn7iy2j88bd7kdcpf0d71gipdbs50b")))

(define-public crate-iter_num_tools-0.1.16 (c (n "iter_num_tools") (v "0.1.16") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0zh8hafd6sg987b5yychkikap188znf1h3qkkg6dliyiq477rqq7")))

(define-public crate-iter_num_tools-0.1.17 (c (n "iter_num_tools") (v "0.1.17") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1cymx4ci2lrz0r6b6dbk1w3wp780vsh8z6s5q6l5595m1fl0gwk6")))

(define-public crate-iter_num_tools-0.2.0 (c (n "iter_num_tools") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "075jscjmy5praiqzy2cvy1rq6jl7gk3z78dw9v8yd14z1p02ag0c")))

(define-public crate-iter_num_tools-0.4.0 (c (n "iter_num_tools") (v "0.4.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0xwish1bv6pya7fb860j2dgig0gyygqd3qpkzgdv2md4hpyli5y1") (y #t)))

(define-public crate-iter_num_tools-0.4.1 (c (n "iter_num_tools") (v "0.4.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1xqlymhh2178yjgsrmg4yrkxc024yx09ysmhqz3x3gihbb8vcgnc")))

(define-public crate-iter_num_tools-0.4.2 (c (n "iter_num_tools") (v "0.4.2") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "03gzyh4lh3lbncis83j6wy97f0zknrp6hh7pg7lw82vda5q6q0z2")))

(define-public crate-iter_num_tools-0.4.3 (c (n "iter_num_tools") (v "0.4.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1ryrvpl7dc06zcglifsdwcbkdpgd62695lhgsgd2visnwnzfh1dg")))

(define-public crate-iter_num_tools-0.4.4 (c (n "iter_num_tools") (v "0.4.4") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0dqx940hzs6jd837mlnyxjbc3lvfm1ddak9i0vhs23qbmapqg2gp")))

(define-public crate-iter_num_tools-0.4.5 (c (n "iter_num_tools") (v "0.4.5") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0yrmblh720kvmd7k50zcfy4vx6s7y42w8kc1vwyn9i4xszyj4x19")))

(define-public crate-iter_num_tools-0.5.0 (c (n "iter_num_tools") (v "0.5.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0y094q8b605in3fn9j5b7zvb009f4hz2dh8c9gmfwzflxgij79yf") (f (quote (("trusted_len"))))))

(define-public crate-iter_num_tools-0.6.0 (c (n "iter_num_tools") (v "0.6.0") (d (list (d (n "array_iter_tools") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0gb8n8idyj82wp5fgwvwfzzd8x7m8mbc7dsbpifibpr964igyz34") (f (quote (("trusted_len") ("iter_advance_by"))))))

(define-public crate-iter_num_tools-0.6.1 (c (n "iter_num_tools") (v "0.6.1") (d (list (d (n "array_iter_tools") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0sd7h97y35al0a759i9g7xbddxw7jq1vy7iy3md5nfiiyhbrxgxi") (f (quote (("trusted_len") ("iter_advance_by"))))))

(define-public crate-iter_num_tools-0.6.2 (c (n "iter_num_tools") (v "0.6.2") (d (list (d (n "array_iter_tools") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0wriqf0lqm6df79wdhdsra6ym8n6xp4y0n1van3hccn6mgpwc4bp") (f (quote (("trusted_len") ("iter_advance_by"))))))

(define-public crate-iter_num_tools-0.7.0 (c (n "iter_num_tools") (v "0.7.0") (d (list (d (n "array-bin-ops") (r "^0.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1nhzcxmay0w7aixiy8nd1499k1lr6wi05ri8c8dladh5q49vid86") (f (quote (("trusted_len") ("iter_advance_by")))) (y #t)))

(define-public crate-iter_num_tools-0.7.1 (c (n "iter_num_tools") (v "0.7.1") (d (list (d (n "array-bin-ops") (r "^0.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "197l67h55n9lzlw1xnslgkmas9c1d2kjgbddliib1d22ik0rc6a1") (f (quote (("trusted_len") ("iter_advance_by"))))))

