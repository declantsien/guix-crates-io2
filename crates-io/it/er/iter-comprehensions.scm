(define-module (crates-io it er iter-comprehensions) #:use-module (crates-io))

(define-public crate-iter-comprehensions-0.1.0 (c (n "iter-comprehensions") (v "0.1.0") (h "071hj6k9vp8zmr3f74y88s6hcg7hv6cy688skm3h3gb7f9vpc4bj")))

(define-public crate-iter-comprehensions-0.2.0 (c (n "iter-comprehensions") (v "0.2.0") (h "1m5zvs3cszwp192f88h9xvdjqbsh8iwhr3m0cb4xn0r1j94hki5b")))

(define-public crate-iter-comprehensions-0.3.0 (c (n "iter-comprehensions") (v "0.3.0") (h "142nf3n747yi2yfbzrx6vmnw3lw0gs4wawi8drbgk7scr4fniziq")))

(define-public crate-iter-comprehensions-0.4.0 (c (n "iter-comprehensions") (v "0.4.0") (h "0v54srm41dq550pnvxhi3y4xzpbqms8l122z6x4n30zd5byzzj7c")))

(define-public crate-iter-comprehensions-0.4.1 (c (n "iter-comprehensions") (v "0.4.1") (h "00102ylkfwwc788hc984v6nm1fj3vg1xdz1s6xy8l32l5qbcpmhj")))

(define-public crate-iter-comprehensions-0.4.2 (c (n "iter-comprehensions") (v "0.4.2") (h "103g3f9wr2x9r21ifn47wy1g8yd5l7h74gg4xpbv954whqx8aizw")))

(define-public crate-iter-comprehensions-0.5.0 (c (n "iter-comprehensions") (v "0.5.0") (h "1j9z0i8wixk56ymwhp2bnbblnfg24gjzbwyi7mp8gyv3shvpafra")))

(define-public crate-iter-comprehensions-0.5.1 (c (n "iter-comprehensions") (v "0.5.1") (h "1zif1bf6p9wrl59zq72ljykzfl8gsrm60d5wk0w6zx5xjh78l12k")))

(define-public crate-iter-comprehensions-0.5.2 (c (n "iter-comprehensions") (v "0.5.2") (h "1r7ibzl6adbjncj9206qgvar30p6izpk8gp0xzhd4r5iykxn10kd")))

(define-public crate-iter-comprehensions-1.0.0 (c (n "iter-comprehensions") (v "1.0.0") (h "19pzsldzr9avrv6s3xbkfdnqnds20qqlb0fc8wpzaxb9xlzzmf4j")))

