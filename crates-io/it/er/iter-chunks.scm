(define-module (crates-io it er iter-chunks) #:use-module (crates-io))

(define-public crate-iter-chunks-0.1.0 (c (n "iter-chunks") (v "0.1.0") (h "0bbchbhmhc32grgkrcbb7cz9fwkdr80f6i2j800yks36imccb7b2")))

(define-public crate-iter-chunks-0.1.1 (c (n "iter-chunks") (v "0.1.1") (h "1xa84wfn4b4yrc2wk7gz9x9iz15b7389jbb0by3kr288231vqvxq")))

(define-public crate-iter-chunks-0.1.2 (c (n "iter-chunks") (v "0.1.2") (h "18vsn2qpqs5r0sia80g3866rrb4364zvs41xkgsdjmm6whrzb7qi")))

(define-public crate-iter-chunks-0.1.3 (c (n "iter-chunks") (v "0.1.3") (h "14vmdksj4ndfwp229w12x5q18knvzr0v6h9fjbrkig0r9vydvaxp")))

(define-public crate-iter-chunks-0.2.0 (c (n "iter-chunks") (v "0.2.0") (h "1b73jzliwax30h2ixg7b9nrz04hddh2ipr9khpzfi24wfq93xw6a")))

(define-public crate-iter-chunks-0.2.1 (c (n "iter-chunks") (v "0.2.1") (h "11azjw34f5fahq6lyizc4yi64dy4g8q1b762fz27h7r36ic8yaf4")))

(define-public crate-iter-chunks-0.2.2 (c (n "iter-chunks") (v "0.2.2") (h "01x49f0fkhyncfdzam1438s0v69l37cg1rnv016x2vxnbwwkfwld")))

