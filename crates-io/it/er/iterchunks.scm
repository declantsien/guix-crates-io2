(define-module (crates-io it er iterchunks) #:use-module (crates-io))

(define-public crate-iterchunks-0.1.0 (c (n "iterchunks") (v "0.1.0") (h "1i8zw8r0lp99kiyj10ff5xgzj2zcp4xmflxxwg79k9qs5nr9yis7")))

(define-public crate-iterchunks-0.1.1 (c (n "iterchunks") (v "0.1.1") (h "0kxiwj8819h66wg69jns1wrlpf97n5zxrngrmpzjm4jyj185s7sb") (r "1.56")))

(define-public crate-iterchunks-0.1.2 (c (n "iterchunks") (v "0.1.2") (d (list (d (n "arrays") (r "^0.1.0") (d #t) (k 0)))) (h "1ybac2n4gmaz4c6vqjm48gmk7i1r1gpd7vw77dmfimm2a6mskvz0") (r "1.56")))

(define-public crate-iterchunks-0.2.0 (c (n "iterchunks") (v "0.2.0") (d (list (d (n "arrays") (r "^0.1.0") (d #t) (k 0)))) (h "0xaikgbaaa2h62r4a5vd2g5arkgv1s2l2qlwfwm8rj4s4wfhn9qg") (r "1.56")))

(define-public crate-iterchunks-0.3.0 (c (n "iterchunks") (v "0.3.0") (d (list (d (n "arrays") (r "^0.1.0") (d #t) (k 0)))) (h "1037h1nbasgrp75mmg1322vfbmj8n5mvyh1m52z79j1v6ij19dnf") (r "1.56")))

(define-public crate-iterchunks-0.4.0 (c (n "iterchunks") (v "0.4.0") (d (list (d (n "itermore") (r "^0.5.0") (f (quote ("array_chunks"))) (k 0)))) (h "0wwap136md1gmcwj9qmnx5r2kw6icin4w62il4yjgl21a864xsj2") (r "1.56")))

(define-public crate-iterchunks-0.5.0 (c (n "iterchunks") (v "0.5.0") (d (list (d (n "itermore") (r "^0.6.0") (f (quote ("array_chunks"))) (k 0)))) (h "1vrwl5j5agm8bsxz8srs57hxmf8ki757nqydp7cwzxg6rfi8jxzm") (r "1.56")))

