(define-module (crates-io it er iterator-sorted) #:use-module (crates-io))

(define-public crate-iterator-sorted-0.1.0 (c (n "iterator-sorted") (v "0.1.0") (h "1llbpv5p150ph1xqvs51p4wd238csyg9pwlbq559zyf85dfpf0fi")))

(define-public crate-iterator-sorted-0.2.0 (c (n "iterator-sorted") (v "0.2.0") (h "1l9m9jw9h4r345i4bkh3x1b4374xlslw37cqkd1ndhhz35k1sg7d")))

