(define-module (crates-io it er iterlower) #:use-module (crates-io))

(define-public crate-iterlower-1.0.0 (c (n "iterlower") (v "1.0.0") (d (list (d (n "unic-ucd") (r "^0.9.0") (d #t) (k 0)))) (h "0fx7w5lyfbmd2s6xnjax8sgkvmx89dzfl0nibvdzga7m5appplad")))

(define-public crate-iterlower-1.0.1 (c (n "iterlower") (v "1.0.1") (d (list (d (n "unic-ucd") (r "^0.9.0") (d #t) (k 0)))) (h "1mvmc5w99cb6ppfiqxdisy06zxwmwmhgwrvbwhrqclbwr3l1430l")))

