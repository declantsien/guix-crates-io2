(define-module (crates-io it er iter_all_proc) #:use-module (crates-io))

(define-public crate-iter_all_proc-0.1.0 (c (n "iter_all_proc") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lbnq03703ajwb3j4zw21hsj2wbv9ksszi5r65ra7dd9443n619k")))

(define-public crate-iter_all_proc-0.1.1 (c (n "iter_all_proc") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13zrxy8rliggmh7syggrj8plisyzicxl435ldmy8syl9hgs61q7j")))

(define-public crate-iter_all_proc-0.1.2 (c (n "iter_all_proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17chggc1h2s4ka85j90qihwrshav1y8m8yssqg74jgf9izk22fc5")))

(define-public crate-iter_all_proc-0.1.4 (c (n "iter_all_proc") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03mjqs8rm026xranz4j8jl08mzb30jc6dgffbvz5nzvvsapbff43")))

(define-public crate-iter_all_proc-0.1.5 (c (n "iter_all_proc") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05mqka30nxy8q9cjq34mcxp9ig38xwybs4dlh7lnj89zd7wn6zbc")))

(define-public crate-iter_all_proc-0.2.0 (c (n "iter_all_proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04ph0x0f4967qnj3r9dkjbvq511q5v0vb8zk5k7pgb2gy1cqgglb")))

(define-public crate-iter_all_proc-0.2.1 (c (n "iter_all_proc") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r974m1xrspj67insqkx5v4ccq3wspm62pk89s86pydzxnkj9h1p")))

(define-public crate-iter_all_proc-0.2.2 (c (n "iter_all_proc") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06p798fi6whbkhka7lpqqxflpfx1liqzxnqspzc0iprck5ksidv8")))

