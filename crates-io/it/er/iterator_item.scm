(define-module (crates-io it er iterator_item) #:use-module (crates-io))

(define-public crate-iterator_item-0.1.0 (c (n "iterator_item") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "iterator_item_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "05fwdfx76al35jqly3436hwx020jksbmgccily5hr880wy3dx085") (f (quote (("std_async_iter"))))))

(define-public crate-iterator_item-0.2.0 (c (n "iterator_item") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "iterator_item_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0rp679ri7ywxn691kx9c03slix8aw65mq1cvw1izkgkvwrp3xm1y") (f (quote (("std_async_iter"))))))

