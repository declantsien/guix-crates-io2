(define-module (crates-io it er iter-set-ops) #:use-module (crates-io))

(define-public crate-iter-set-ops-0.1.0 (c (n "iter-set-ops") (v "0.1.0") (d (list (d (n "binary-heap-plus") (r "^0.5.0") (d #t) (k 0)) (d (n "compare") (r "^0.1.0") (d #t) (k 0)))) (h "1jqyga8jxqvqz5887h8abi1y7ck3p0wb7yd5767f13kh66wabjk4")))

(define-public crate-iter-set-ops-0.1.1 (c (n "iter-set-ops") (v "0.1.1") (d (list (d (n "binary-heap-plus") (r "^0.5.0") (d #t) (k 0)) (d (n "compare") (r "^0.1.0") (d #t) (k 0)))) (h "0bxlmzp3sv8i8xrgnhmf9kyis4zdiiw3w8bvm6j8shda9s73ai0d")))

(define-public crate-iter-set-ops-0.2.0 (c (n "iter-set-ops") (v "0.2.0") (d (list (d (n "binary-heap-plus") (r "^0.5") (d #t) (k 0)) (d (n "compare") (r "^0.1") (d #t) (k 0)))) (h "1h4bwbka098a3qhv91403fv4v5jmarsqa3ffn4bijdyjnbj55q4r")))

(define-public crate-iter-set-ops-0.2.1 (c (n "iter-set-ops") (v "0.2.1") (d (list (d (n "binary-heap-plus") (r "^0.5") (d #t) (k 0)) (d (n "compare") (r "^0.1") (d #t) (k 0)))) (h "02ffsnd3g07lyhgv55mqvbzq22dvkcwy5768yihgj6x9r7xpnggn")))

(define-public crate-iter-set-ops-0.2.2 (c (n "iter-set-ops") (v "0.2.2") (d (list (d (n "binary-heap-plus") (r "^0.5") (d #t) (k 0)) (d (n "compare") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ab581ykylh68i84h2bsiwg2sagjfgacvynyyr0yqlrz4q9acg7i")))

(define-public crate-iter-set-ops-0.2.3 (c (n "iter-set-ops") (v "0.2.3") (d (list (d (n "binary-heap-plus") (r "^0.5") (d #t) (k 0)) (d (n "compare") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0f2xa3d7pa8krl8ghsm2bkxszfa0lagc87px7ads019l5sxbbagq")))

