(define-module (crates-io it er iterator-ext) #:use-module (crates-io))

(define-public crate-iterator-ext-0.1.0 (c (n "iterator-ext") (v "0.1.0") (h "0nr2pv2ygh36ax7m9ng7gbhhfcq7yzkkaak6ys29izy7sic4glxf")))

(define-public crate-iterator-ext-0.2.0 (c (n "iterator-ext") (v "0.2.0") (h "0dw0ji4kgcarcnaak7qzpi8agpa20r1s1d5il62js9gkxzzcfjhn")))

(define-public crate-iterator-ext-0.2.1 (c (n "iterator-ext") (v "0.2.1") (h "1k2k6a4rsjpkki8qjhph04byiqxh77m5kdrkgpsl25w0zpjddj32")))

