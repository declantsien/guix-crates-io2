(define-module (crates-io it er iterm2mintty) #:use-module (crates-io))

(define-public crate-iterm2mintty-0.1.0 (c (n "iterm2mintty") (v "0.1.0") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "plist") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_ini") (r "^0.1.3") (d #t) (k 0)))) (h "0b3zal09aqzy7b7i5pf8wz1dafpy6fy6mj7k3k9rgy7xqhysqzli")))

(define-public crate-iterm2mintty-0.1.1 (c (n "iterm2mintty") (v "0.1.1") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "plist") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_ini") (r "^0.1.3") (d #t) (k 0)))) (h "06h0x67vsb5hbhs2d9xh7a7v1bn0gznvhazli523zbz8hw8bj2fi")))

(define-public crate-iterm2mintty-0.1.2 (c (n "iterm2mintty") (v "0.1.2") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "plist") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_ini") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "19byn9s470jag03r6q1vwlhgf7j1k6k6n96n7n8ycyh1k2dbyp73")))

(define-public crate-iterm2mintty-0.2.0 (c (n "iterm2mintty") (v "0.2.0") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "plist") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_ini") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "1qlfr6xngi64gr1132nkv4mpzc2wpdydpq2m0cdhr5sal8zxxxc1")))

