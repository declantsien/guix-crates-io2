(define-module (crates-io it er iterator_item_macros) #:use-module (crates-io))

(define-public crate-iterator_item_macros-0.1.0 (c (n "iterator_item_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut" "parsing" "fold"))) (d #t) (k 0)))) (h "17qldnzi19hfkp57rcslixal3gg9nl68yn20whfrqqqrwhhbxzfm")))

