(define-module (crates-io it er iterator-endiate) #:use-module (crates-io))

(define-public crate-iterator-endiate-0.1.0 (c (n "iterator-endiate") (v "0.1.0") (h "1k243bk11s47jc18gnmy61xgcbcnk9dfgrlp4wqs0m7d54wyzzjw")))

(define-public crate-iterator-endiate-0.2.0 (c (n "iterator-endiate") (v "0.2.0") (h "10vzanv71cyknzd7i6hpkkvgyps1yxi1fhrcxiwasrdq8mf2gs58")))

(define-public crate-iterator-endiate-0.2.1 (c (n "iterator-endiate") (v "0.2.1") (h "04ki6nx2pjdwk1z5rpcpxpjkvzdc7fms1wifk1zv630a381lgffa")))

