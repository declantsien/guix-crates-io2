(define-module (crates-io it er iterextd) #:use-module (crates-io))

(define-public crate-iterextd-0.1.0 (c (n "iterextd") (v "0.1.0") (d (list (d (n "trybuild") (r "^1.0.83") (d #t) (k 2)))) (h "017hfh8dq5zwnggl64mj2ngv89812l79ww8dw6rllpxdx9smxill")))

(define-public crate-iterextd-0.2.0 (c (n "iterextd") (v "0.2.0") (d (list (d (n "trybuild") (r "^1.0.83") (d #t) (k 2)))) (h "07i5bprq7265h0h0jlqhlcx9mz47gr4swk8amp6gh8wd3wkz69js") (f (quote (("itern"))))))

(define-public crate-iterextd-0.3.0 (c (n "iterextd") (v "0.3.0") (d (list (d (n "trybuild") (r "^1.0.83") (d #t) (k 2)))) (h "0s67nkkmyrrmjh38jizjkr865rxlfvsfh85r9g454f618gnn00y2") (f (quote (("itern"))))))

(define-public crate-iterextd-0.4.0 (c (n "iterextd") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.5.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "num_convert") (r "^0.7.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.83") (d #t) (k 2)))) (h "1vd1cxwl1i1cimiq5inc2ynyqvcq4g5f3am0zklrdcp6yl1zpkfb") (f (quote (("itern"))))))

(define-public crate-iterextd-0.4.1 (c (n "iterextd") (v "0.4.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.5.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "num_convert") (r "^0.7.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.83") (d #t) (k 2)))) (h "05v72qrj470jr220hpdyl7a5ihlpmm98igjxllcf5hknxzpfqrdk") (f (quote (("itern")))) (r "1.75.0")))

