(define-module (crates-io it er iter_from_fn) #:use-module (crates-io))

(define-public crate-iter_from_fn-0.1.0 (c (n "iter_from_fn") (v "0.1.0") (h "1kz26ykqd98rbwkjw550l5z9q0qpss2rwx0q583vz51j1yzf3cyw")))

(define-public crate-iter_from_fn-1.0.0 (c (n "iter_from_fn") (v "1.0.0") (h "09my1rwqv7r1k22qwjqsxmx341j2mj61sp3xj797vv0y5ipz47hg")))

