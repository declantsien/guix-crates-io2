(define-module (crates-io it er iterify) #:use-module (crates-io))

(define-public crate-iterify-0.1.0 (c (n "iterify") (v "0.1.0") (h "1pmq5hclf1xhzr799nwrzzr6a8cb43bz8pp6vdgpp8cgrgrbzizv")))

(define-public crate-iterify-0.1.1 (c (n "iterify") (v "0.1.1") (h "0a2gq4vw4bq09b5zg2j58fnhhag4pg0ywdlylcq2h33drf836cci")))

(define-public crate-iterify-0.1.2 (c (n "iterify") (v "0.1.2") (h "1bmixp3ifxcypqrbmf0vmcq9606f86p4jj1x5wkgw2wsrl1j4fl9")))

