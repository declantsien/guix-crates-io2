(define-module (crates-io it er itertools) #:use-module (crates-io))

(define-public crate-itertools-0.0.2 (c (n "itertools") (v "0.0.2") (h "0mfpyc1kq7bzjhnb4540cdkpfs5vs5wh9p6xmc7m7nz9ha26paca")))

(define-public crate-itertools-0.0.3 (c (n "itertools") (v "0.0.3") (h "0mqjp9lc2ncrf7wljjwqvfsk6qrpyb2zykcl5kk6bjir2fr99h8f")))

(define-public crate-itertools-0.0.4 (c (n "itertools") (v "0.0.4") (h "1977y6ciy5b7b5d06v0zx16s2376qhir1qwwykgzfdzndkpgsp9s")))

(define-public crate-itertools-0.0.5 (c (n "itertools") (v "0.0.5") (h "1x07w2j95vxgrwdgq9m11cdhlj6p4rnkn4xlq4hf8nx3zsdjbfkb")))

(define-public crate-itertools-0.0.6 (c (n "itertools") (v "0.0.6") (h "0wk6mzmm05ijg5a95qc8y99fdpp1336yc19rhibix81xkr1x0mxv")))

(define-public crate-itertools-0.0.7 (c (n "itertools") (v "0.0.7") (h "118mijgi7871km2a5049h5m9lninl3lmg7224n076y2vlqyfwjv1")))

(define-public crate-itertools-0.0.8 (c (n "itertools") (v "0.0.8") (h "0n3bfywhx1bvz280bwa1l85fjjxia1mzqwd6q4nsj917qbkbngy8")))

(define-public crate-itertools-0.0.9 (c (n "itertools") (v "0.0.9") (h "0nfsqb4lkjlnzpidwri6689lglbmp71wbfcaw9k5im4bpq9zqzl2")))

(define-public crate-itertools-0.0.10 (c (n "itertools") (v "0.0.10") (h "0n6397fwpwgaf45kin5ssgj85p345y8005b7dlkvdc9ph4cgb7mh")))

(define-public crate-itertools-0.0.11 (c (n "itertools") (v "0.0.11") (h "1zj1ibgzgs41klhypqlmiqlqja11mqpmiw1lvj35rdqmxdxljrds")))

(define-public crate-itertools-0.0.12 (c (n "itertools") (v "0.0.12") (h "1xzdysixinnv8lg2yc01cczlsid3lm4gd2iprhwhrdqm8kpj0pdk")))

(define-public crate-itertools-0.0.13 (c (n "itertools") (v "0.0.13") (h "0vzw5i9b71rymk4qlarxpxg11w4b8bbj89awh4k9aiay6cl93897")))

(define-public crate-itertools-0.0.14 (c (n "itertools") (v "0.0.14") (h "0briwilr30gpp0d1iam8w99p7rx6hm7pxbch5a6n633cnm985029")))

(define-public crate-itertools-0.0.16 (c (n "itertools") (v "0.0.16") (h "1fw9b8plpscdfax4i34vc3q1k89d88g2da0gqk4ckx74rr5rr79b")))

(define-public crate-itertools-0.0.17 (c (n "itertools") (v "0.0.17") (h "0pv2a82yzspqvjnc429b0kyyy33m1ddjb56r0xirhl765lmrmyg8")))

(define-public crate-itertools-0.0.18 (c (n "itertools") (v "0.0.18") (h "1pjsabkcvfwaisixb7bg03hwfhszn0jrrp670g2q0135a675cijx")))

(define-public crate-itertools-0.0.19 (c (n "itertools") (v "0.0.19") (h "1g9gpz3klps6iajjapspjpr8dy798acm69m9y8i0x75j7xh4njxb")))

(define-public crate-itertools-0.0.20 (c (n "itertools") (v "0.0.20") (h "0l2swq0vspns0igyakfmgw7jlpb8l34fyhxa9hwnl5k0fxrmz2dz")))

(define-public crate-itertools-0.1.0 (c (n "itertools") (v "0.1.0") (h "044xz3xvr71njzv0rxp3i89vjphwg8bjj72wczkqpcqn4dj4jr1x")))

(define-public crate-itertools-0.1.1 (c (n "itertools") (v "0.1.1") (h "15qnc6zqxd32gfmvlzlikzcwgwblrh0n2x6k3lw4vy5v107rgnf7")))

(define-public crate-itertools-0.1.2 (c (n "itertools") (v "0.1.2") (h "1a8fxgl9csh6w83isx430j80variphah6ydv4m2m2z0jk25i1kgp")))

(define-public crate-itertools-0.1.3 (c (n "itertools") (v "0.1.3") (h "1yj9j1gcm97v5wad52qwfk7nwh8wypv3rjlazrj9jd0cfn0vvy42")))

(define-public crate-itertools-0.1.4 (c (n "itertools") (v "0.1.4") (h "0zbnx0jcazlapp9h16lrs8d3kcwz6jwq8nv28kr3ncqcbwg95yd7")))

(define-public crate-itertools-0.1.5 (c (n "itertools") (v "0.1.5") (h "08bz5mv4618kprjdn6bkwqylxjqpqdblvy46pjg2fkswwjwdvvrp")))

(define-public crate-itertools-0.1.6 (c (n "itertools") (v "0.1.6") (h "1n5xfla6370d9bdk7cx3qpmfy439d23f1bsd532cs0rpgj29nn6x")))

(define-public crate-itertools-0.1.7 (c (n "itertools") (v "0.1.7") (h "0rwy946gd4lpkjy187kxvx8ywdrdlldqhjijk6yhlh3vrj0g66gr")))

(define-public crate-itertools-0.1.8 (c (n "itertools") (v "0.1.8") (h "03gmmy5zj9g6g64f0g7ss6m7cb404m0z82w3j31qdsx29makvwsw")))

(define-public crate-itertools-0.1.9 (c (n "itertools") (v "0.1.9") (h "1zgjm8sinwb818gna3v7d656ndzpwqimx1yz6y0cb4ddcz5l88wv")))

(define-public crate-itertools-0.1.10 (c (n "itertools") (v "0.1.10") (h "0xxkz3lgn1dv8zxxxhxssxf3pwvin0zrgbkkfhfic2pa3px6vpiv")))

(define-public crate-itertools-0.2.0 (c (n "itertools") (v "0.2.0") (h "1sbq7h2bs17fgmssb1295zmklj33kkr3g2pj58margwwv5nascji") (f (quote (("unstable"))))))

(define-public crate-itertools-0.2.1 (c (n "itertools") (v "0.2.1") (h "0z1i5krqc1bqbmfxhg94hkfg1phdni8fa9c5g992w7yndi19z9fw") (f (quote (("unstable"))))))

(define-public crate-itertools-0.2.2 (c (n "itertools") (v "0.2.2") (h "1568gg4s5sr2s5ny85h5gn5zb9r4i10bpqsrjawai4pnvvlh7ckg") (f (quote (("unstable"))))))

(define-public crate-itertools-0.2.3 (c (n "itertools") (v "0.2.3") (h "0c7kzlvnvb13995sgvbx7wsaplf85nv8ccqz96kvh20hfnj2nx8n") (f (quote (("unstable"))))))

(define-public crate-itertools-0.2.4 (c (n "itertools") (v "0.2.4") (h "18blzml1fls69c3jagwk27vzmbnlmcx3gasyny8fmflin0gx3yxf") (f (quote (("unstable"))))))

(define-public crate-itertools-0.2.5 (c (n "itertools") (v "0.2.5") (h "19xgh12q54rgkzzdi0glbd6kancwf2la7qznallj1g6idy7snbn3") (f (quote (("unstable"))))))

(define-public crate-itertools-0.2.6 (c (n "itertools") (v "0.2.6") (h "1k0fx9hk1wbzksdwyvz0xxmy936592f5yrb30c76qc0yy41a1w59") (f (quote (("unstable"))))))

(define-public crate-itertools-0.2.7 (c (n "itertools") (v "0.2.7") (h "0ry75zg4a7ys3nz6aqdy8hcl7nhzrp1hnxmp225ah9bzvvsjd707") (f (quote (("unstable"))))))

(define-public crate-itertools-0.2.8 (c (n "itertools") (v "0.2.8") (h "1hcjqyc838lj9pbrz5dwsxy4whz2sf5ln0xsx231vf9ixk56myjy") (f (quote (("unstable"))))))

(define-public crate-itertools-0.2.9 (c (n "itertools") (v "0.2.9") (h "030b5s37hkylw7yc1ri7hgn09095yz6zdk4pxddy6p9dmjk5z7xa") (f (quote (("unstable"))))))

(define-public crate-itertools-0.3.0 (c (n "itertools") (v "0.3.0") (h "1hrvz6scf5npv9wxapds2zg195dh76agnlbsadbhycmj7zg2sfss") (f (quote (("unstable"))))))

(define-public crate-itertools-0.3.1 (c (n "itertools") (v "0.3.1") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "1mk92h6mx8v5m92yb9lbb3dy22rvlx7bfsl14ixs2c313m9b2prb") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.2 (c (n "itertools") (v "0.3.2") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0ybxgr6yndip67j9ds9b0648laz4h4z11s196calp0mdn3kjg4kw") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.3 (c (n "itertools") (v "0.3.3") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0d37kpl89yiacl3yn11vz4p3npqyvi2d344ri380pb8wbh9x5748") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.4 (c (n "itertools") (v "0.3.4") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "1a6g9s10p9ynbrvwp24fzb6nhayxna5c1ymyn7l6vnazklb7kgh8") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.5 (c (n "itertools") (v "0.3.5") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0v362xm0hxpy75qfalxwhlc9h86ya3lg0yrm9pj36g0jyppr4b8l") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.6 (c (n "itertools") (v "0.3.6") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "09kngnb559mh5jz7x7w0k7s9sy5ffvpyi95hfy27f8rgin5wg8j4") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.7 (c (n "itertools") (v "0.3.7") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0pkawjyzrsa1311xkpdn89hv2cx9r7vg76qjyd5m6g8n7xd3p2z0") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.8 (c (n "itertools") (v "0.3.8") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0jhxjc90v9rmp8k963qn2y9j7ljm5qjn8msi9h1skz2y360rzpkz") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.9 (c (n "itertools") (v "0.3.9") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "1k5pr9iyx3ihi36473vfvm2nivv5pzmiz22ymccf9yh30kgsw9av") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.10 (c (n "itertools") (v "0.3.10") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "1gas6z9wmfsvp0x9fk2dfrya55l996v2qnq09ni4rm18xz9j5bai") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.11 (c (n "itertools") (v "0.3.11") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0rxq1x3xzd8lb02wp0qybl5zwh21d61z2lim7q6gpm08m3vv6yrl") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.12 (c (n "itertools") (v "0.3.12") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0xmmzslz6s92j5x363ycriy1ahv4z203rcxv9n8dc6xr1srx7v2y") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.13 (c (n "itertools") (v "0.3.13") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "1lvzjn6dzz0h4rw7a53c5zzdck469wkppgz95wzwj060aim3vlq5") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.14 (c (n "itertools") (v "0.3.14") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "1md5bm5akxyzfxzsj6pchfadianc296inzfgc0spsdgddzf20li6") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.15 (c (n "itertools") (v "0.3.15") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0prhflwr3xc252ssjsay7lac3dsvp9xr2ix5kdn90x568cpbir3s") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.16 (c (n "itertools") (v "0.3.16") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "08x9324cbppybb4nyg77xgmx2nz96z4d7qzhz506gzylaymap1gj") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.17 (c (n "itertools") (v "0.3.17") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0rxb3wc9xh4p53nbbh5j0wz8m2w8nqp1k9dhn05rq1p92kvmmrra") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.18 (c (n "itertools") (v "0.3.18") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "19c36n5z0hcmw5kz6aj0yizz8y2g4msz4b6sj49zc7xbmgxyl14i") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.19 (c (n "itertools") (v "0.3.19") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0p7vxli0kmrj7az5rdqhrj8nx4jaw1pqbx78z6cn0l5mn05dx52s") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.20 (c (n "itertools") (v "0.3.20") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "1rvsr89vk2x8j1693ynijrpr58w2d66azzn3hixnh94jz7gpfmd1") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.21 (c (n "itertools") (v "0.3.21") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "10cn6076mfapbd4y0wylczjkii78lfdz3h3si5wrm0n0d23hwi2f") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.22 (c (n "itertools") (v "0.3.22") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0qvgvp2yc88xdaw828s7cblx6gh0h2nv3fx5684djhqsxqsjsib2") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.23 (c (n "itertools") (v "0.3.23") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (o #t) (d #t) (k 0)))) (h "1gl7blnqqrbji6jfcglqm4j7w81lp231qs0p21arpawbwlikv7pk") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.24 (c (n "itertools") (v "0.3.24") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "14r3ndm9jl6v08y64m5xg2ri9wli3d8jhqxagf83ga8bzdqkqfsx") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3.25 (c (n "itertools") (v "0.3.25") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "036ss6ckcb2wsiihh35k8jcx130qws3yv62nszw8zzawd0f3zdqn") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.4.0 (c (n "itertools") (v "0.4.0") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "16810zyjy57qsgrhy9x3w91q8zz7k59k9ghiwayqyrncy7jdyaqa") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.4.1 (c (n "itertools") (v "0.4.1") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "0phlv5w6nfr6azqfql1cnvyypvhyicqxw6wp07872lxhl46smwrd") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.4.2 (c (n "itertools") (v "0.4.2") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "0swbvh34cxnid21q311861zchba5pc58bagh3rnwmsw7fb26anya") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.4.3 (c (n "itertools") (v "0.4.3") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "1wqs0iyc4i2a7snb0fahbim6dz8m1g1mlp5xpqhpxrwscr0pww10") (f (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.4.4 (c (n "itertools") (v "0.4.4") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "13nw859k0v49n4nxlb386idxhbjs81vkldbxbh4nwyc0acfs4f67") (f (quote (("unstable") ("qc" "quickcheck"))))))

(define-public crate-itertools-0.4.5 (c (n "itertools") (v "0.4.5") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "0rlnvwdw4zn1l5bjsqb3ab76k0nd66yfhy5ara2gqpngwkl57x70") (f (quote (("unstable") ("qc" "quickcheck"))))))

(define-public crate-itertools-0.4.6 (c (n "itertools") (v "0.4.6") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "0znrwqgic5rp6f2b6w206l1ryl0czbi5h77v22453cka1k2a1jky") (f (quote (("unstable") ("qc" "quickcheck"))))))

(define-public crate-itertools-0.4.7 (c (n "itertools") (v "0.4.7") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "0id9wx9rjw592jwpb0z7qj00h8161sb3f1dym6qfbca3q3w88wd9") (f (quote (("unstable") ("qc" "quickcheck"))))))

(define-public crate-itertools-0.4.8 (c (n "itertools") (v "0.4.8") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "09cg6gx00j6ykcfcxvps828xn9m6zi0jwb71lwjhdc7qwm3asi8d") (f (quote (("unstable") ("qc" "quickcheck"))))))

(define-public crate-itertools-0.4.9 (c (n "itertools") (v "0.4.9") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "1xdnx2dy72pn6wwk93m4s81a363fnvi1pnm9d65hgxp4b0470fq5") (f (quote (("unstable"))))))

(define-public crate-itertools-0.4.10 (c (n "itertools") (v "0.4.10") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "1q49zg66pizcm14j9qmzz2pywwnx049sd02pfz3srq5vs4xw7v06") (f (quote (("unstable"))))))

(define-public crate-itertools-0.4.11 (c (n "itertools") (v "0.4.11") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "1r8cs5gj0c6ixnanw66im6zhxyxfwcfcimj688j5jnmpw438nlph") (f (quote (("unstable"))))))

(define-public crate-itertools-0.4.12 (c (n "itertools") (v "0.4.12") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "06521kzfsj59sllvr95szql46idh33c53c8gp0qfb4d7w4rdppfz") (f (quote (("unstable"))))))

(define-public crate-itertools-0.4.13 (c (n "itertools") (v "0.4.13") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "0xjd33fpqjkr5brpxj64spw9jm8iwg3j0fpgzlf0p128zsjiyvh8") (f (quote (("unstable"))))))

(define-public crate-itertools-0.4.14 (c (n "itertools") (v "0.4.14") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "0xm6d5lmv4acdwrjdpbv3gcbzpwl4yvibn8jb5ng8nv33200bhzj") (f (quote (("unstable"))))))

(define-public crate-itertools-0.4.15 (c (n "itertools") (v "0.4.15") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "0kq6smsnyrpw89593lrkhph96ws145yd6v0h0kdz95mxbvnizwl8") (f (quote (("unstable"))))))

(define-public crate-itertools-0.4.16 (c (n "itertools") (v "0.4.16") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "0gq37lavj22imj3nnh2zzvclaj3r078lnqag9k7yy46przkmcvmc") (f (quote (("unstable"))))))

(define-public crate-itertools-0.4.17 (c (n "itertools") (v "0.4.17") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "03v1dhlfqrrq1bc7qpq74bx4x2wlfy887rm4zc5863bkx7h87nvx") (f (quote (("unstable"))))))

(define-public crate-itertools-0.4.18 (c (n "itertools") (v "0.4.18") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "1hybgjw80fadyjhba48xz5r284rznf0vj7cmlvvnxm7pwyadc5bg") (f (quote (("unstable"))))))

(define-public crate-itertools-0.4.19 (c (n "itertools") (v "0.4.19") (d (list (d (n "permutohedron") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "0gxwcmxyq7fmccdqclfzyg4wnb2b445g8n3fqqyz8n30nmpbbaf4") (f (quote (("unstable"))))))

(define-public crate-itertools-0.5.0-alpha.0 (c (n "itertools") (v "0.5.0-alpha.0") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "0q72lfh8pm0kb94crlawa895l4wpwranfy2ky8rqfd1rqn4xp7w2")))

(define-public crate-itertools-0.5.0-alpha.1 (c (n "itertools") (v "0.5.0-alpha.1") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "00w6jwmr0ary4002a3zi57wyqa5qj2ysd9pik1g1yxvqr3lsp3xn")))

(define-public crate-itertools-0.5.0 (c (n "itertools") (v "0.5.0") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "14vhj45azp8lfa4ymfrmcnl0a6van70lhnbjzy126x4cjgsyilzn")))

(define-public crate-itertools-0.5.1 (c (n "itertools") (v "0.5.1") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "1jl7iksdgk92ryksiknmj32kx8lq4z2vh3hqds6r8v7lkb14mph0")))

(define-public crate-itertools-0.5.2 (c (n "itertools") (v "0.5.2") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "1xdhshggv144z3mvdh7znlnr53wyvm0dkpvsbsprgd2m9jvsg1qn")))

(define-public crate-itertools-0.5.3 (c (n "itertools") (v "0.5.3") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "07x97rvcwapkadf9bsa8sbsc6zi2q6b2n38n7r0l8w0bji3snncz")))

(define-public crate-itertools-0.5.4 (c (n "itertools") (v "0.5.4") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "1lz62zwnxg630g1g7nxk36qhhxan7145sqapiiqgnfb8ngjbb8nx")))

(define-public crate-itertools-0.5.5 (c (n "itertools") (v "0.5.5") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (o #t) (d #t) (k 0)))) (h "092lkxnxvwa7fp69f4dc6j5yi7njfxrairpbsg7hh64ybahv10gg")))

(define-public crate-itertools-0.5.6 (c (n "itertools") (v "0.5.6") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "1541342408ga2p2dxalhcid5xx1cm5pvcm002vxlni6lc684g02f")))

(define-public crate-itertools-0.5.7 (c (n "itertools") (v "0.5.7") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "0yh8592l85rwwix14bsapk727n06qwbjfhbm62nxd7xf01z6z3cf")))

(define-public crate-itertools-0.5.8 (c (n "itertools") (v "0.5.8") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "083hib5kajnpn61fb4jrk0kfxi3wb270w1a9q2cbvg6vfaj6v566")))

(define-public crate-itertools-0.5.9 (c (n "itertools") (v "0.5.9") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "02kkmxqa6mzm02jh2sbaj9dbgwakdklklfrc1xxkfwbbpbkmfmfr")))

(define-public crate-itertools-0.5.10 (c (n "itertools") (v "0.5.10") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "1z4lyrakgynvhylya72qb3vizmxmd62whjmg4r8k01d4inbxccs8")))

(define-public crate-itertools-0.6.0 (c (n "itertools") (v "0.6.0") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "16bvnjf24jr2qhjgsdlbmcki67w7ynfp5952b5yicikjm4l0jakp")))

(define-public crate-itertools-0.6.1 (c (n "itertools") (v "0.6.1") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "1fq5hdisxj64n3yj3g1maz74jq3j1vng34lii1cpydr08x0mk0z5")))

(define-public crate-itertools-0.6.2 (c (n "itertools") (v "0.6.2") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "180mwh16i9qd0vnzwrkrnmqgvg6yab58jhc98hr43krr07b8bhi2")))

(define-public crate-itertools-0.6.3 (c (n "itertools") (v "0.6.3") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "1w62gv1a7js20yqqvbvpyabccfxji2aj66bhj5n2gvri7hknlkdb")))

(define-public crate-itertools-0.6.4 (c (n "itertools") (v "0.6.4") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "0qmv9jf7p0wkyxxqgb2vnvgq376vrq18k4j8afd6r5zb06gfgkr6")))

(define-public crate-itertools-0.6.5 (c (n "itertools") (v "0.6.5") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "08fx60wxmjakssqn3w9045dasqvaf1gmgzg5kag062k9l56vxwnk")))

(define-public crate-itertools-0.7.0 (c (n "itertools") (v "0.7.0") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "1558zmj02z1h07y2s7qs0sqgbsxzjh3kaa8y7ai2wpzm1jw4gziy") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7.1 (c (n "itertools") (v "0.7.1") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "0v3iwbz30h2ifz6qdcng6dkpg1yhwj9rkv44w32rq9prxhp37h3w") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7.2 (c (n "itertools") (v "0.7.2") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "06kmcb7r490qs32avs45hn016cn4bqp0zs8fd9whbdfk7wfhalic") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7.3 (c (n "itertools") (v "0.7.3") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "1pqg3h34yw0bq25kvzja4v3wcmabd3lfy486vgpag5gigifzgwwr") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7.4 (c (n "itertools") (v "0.7.4") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "1igqdvcc9x32l3zqhspa4yyb706nlanmsv4klgpr4xs1pfws0yll") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7.5 (c (n "itertools") (v "0.7.5") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (k 2)))) (h "1hjr3n2lz5b0jacr5gbllng25syaxg1r8ximfdyp938bqv4rbwd3") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7.6 (c (n "itertools") (v "0.7.6") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (k 2)))) (h "15035jjc8hwc8091z3jn6lajjrda013qqzmnrqdhbdak74i34wxh") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7.7 (c (n "itertools") (v "0.7.7") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (k 2)))) (h "1lccfc9jxp7bg0ak5kcy0jp40yxgrarcbj5rqd280cwlfd63pm93") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7.8 (c (n "itertools") (v "0.7.8") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (k 2)))) (h "0l7lfiadcg04qw0rnz8fyhcmgcigl0bpc4rkapcysvvpdfbmd27m") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7.9 (c (n "itertools") (v "0.7.9") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (k 2)))) (h "07dga7w8w4w0sgklzrl01bf2kmv99k2f79vpxjwxnnk7axwm5vvw") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7.10 (c (n "itertools") (v "0.7.10") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (k 2)))) (h "18vwsdn38sh0zdg3462c4c3agaqk355rrk2hda26lkh3fpz59m2v") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7.11 (c (n "itertools") (v "0.7.11") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (k 2)))) (h "03cpsj26xmyamcalclqzr1i700vwx8hnbgxbpjvs354f8mnr8iqd") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.8.0 (c (n "itertools") (v "0.8.0") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0n2k13b6w4x2x6np2lykh9bj3b3z4hwh2r4cn3z2dgnfq7cng12v") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.8.1 (c (n "itertools") (v "0.8.1") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0vhdvb2ysn24h3wpdp2wkccyljzxhfxsnjcc2gippc57vv4pbyl7") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.8.2 (c (n "itertools") (v "0.8.2") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1154j48aw913v5jnyhpxialxhdn2sfpl4d7bwididyb1r05jsspm") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.9.0 (c (n "itertools") (v "0.9.0") (d (list (d (n "criterion") (r "= 0.3.0") (d #t) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0jyml7ygr7kijkcjdl3fk5f34y5h5jsavclim7l13zjiavw1hkr8") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.10.0 (c (n "itertools") (v "0.10.0") (d (list (d (n "criterion") (r "=0") (d #t) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "06dkghwi1a6ah2103gibxnr2ys762m5x4rp75x0q43imis8p5m9p") (f (quote (("use_std" "use_alloc") ("use_alloc") ("default" "use_std"))))))

(define-public crate-itertools-0.10.1 (c (n "itertools") (v "0.10.1") (d (list (d (n "criterion") (r "=0") (d #t) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1bsyxnm20x05rwc5qskrqy4cfswrcadzlwc26dkqml6hz64vipb9") (f (quote (("use_std" "use_alloc") ("use_alloc") ("default" "use_std"))))))

(define-public crate-itertools-0.10.3 (c (n "itertools") (v "0.10.3") (d (list (d (n "criterion") (r "=0") (d #t) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1qy55fqbaisr9qgbn7cvdvqlfqbh1f4ddf99zwan56z7l6gx3ad9") (f (quote (("use_std" "use_alloc") ("use_alloc") ("default" "use_std"))))))

(define-public crate-itertools-0.10.4 (c (n "itertools") (v "0.10.4") (d (list (d (n "criterion") (r "=0") (d #t) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1l3qmfysx7salxbci99i2crd3ky73bkla2vrlj190yp6g5vj9gyq") (f (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std"))))))

(define-public crate-itertools-0.10.5 (c (n "itertools") (v "0.10.5") (d (list (d (n "criterion") (r "= 0") (d #t) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0ww45h7nxx5kj6z2y6chlskxd1igvs4j507anr6dzg99x1h25zdh") (f (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std"))))))

(define-public crate-itertools-0.11.0 (c (n "itertools") (v "0.11.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0mzyqcc59azx9g5cg6fs8k529gvh4463smmka6jvzs3cd2jp7hdi") (f (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std")))) (r "1.36.0")))

(define-public crate-itertools-0.12.0 (c (n "itertools") (v "0.12.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1c07gzdlc6a1c8p8jrvvw3gs52bss3y58cs2s21d9i978l36pnr5") (f (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std")))) (r "1.43.1")))

(define-public crate-itertools-0.12.1 (c (n "itertools") (v "0.12.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0s95jbb3ndj1lvfxyq5wanc0fm0r6hg6q4ngb92qlfdxvci10ads") (f (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std")))) (r "1.43.1")))

(define-public crate-itertools-0.13.0 (c (n "itertools") (v "0.13.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "11hiy3qzl643zcigknclh446qb9zlg4dpdzfkjaa9q9fqpgyfgj1") (f (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std")))) (r "1.43.1")))

