(define-module (crates-io it er iterx) #:use-module (crates-io))

(define-public crate-iterx-0.0.1 (c (n "iterx") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0vvfcq3mir1yf1ssf53k667lrxy7bxwi5c06ymh2a021dy9vc35s") (y #t)))

(define-public crate-iterx-0.0.2 (c (n "iterx") (v "0.0.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "18zp4wlb8paqj5chlynmqb06z469ifld817d89mg4ng97rw8lp1h") (y #t)))

(define-public crate-iterx-0.0.3 (c (n "iterx") (v "0.0.3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0r92aad44z7566k7y1yaxk313ny6jqs0p850c88cj3zjb2wksmd3") (y #t)))

(define-public crate-iterx-0.0.4 (c (n "iterx") (v "0.0.4") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1w8v5i051rsncl5hmy7m6hq654bkz7kjrl4cmqzkixk3msgwibdn") (y #t)))

(define-public crate-iterx-0.0.5 (c (n "iterx") (v "0.0.5") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0va8grmb67gri2v9hbdpiqd2j0ql3x2r9pbd473z50w61p8m7p33") (y #t)))

(define-public crate-iterx-0.0.6 (c (n "iterx") (v "0.0.6") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0vya65k81fbh1g3l2jwr8nxy3q9vn79d8hs88akw310h1jx0rb9d") (y #t)))

(define-public crate-iterx-0.0.7 (c (n "iterx") (v "0.0.7") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0d4p8a7si51l8zz214g3cg8ig4l769l684j33i5hy9k55njc3xyh") (y #t)))

(define-public crate-iterx-0.0.8 (c (n "iterx") (v "0.0.8") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0xfbnr95lia4c70nir1d5xmsr1nz40f0nfiwpkcr419bgbcb393m") (y #t)))

(define-public crate-iterx-0.0.9 (c (n "iterx") (v "0.0.9") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1z28gkalzr61l75fcr5j9wfvgmnqxz3yipr412ih6383if78jad7") (y #t)))

(define-public crate-iterx-0.0.10 (c (n "iterx") (v "0.0.10") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0mixb5p069f469gbmawxacv2ypm4pv1ydzzwhmllqv1yph3nkihj")))

