(define-module (crates-io it er iterator-enum) #:use-module (crates-io))

(define-public crate-iterator-enum-0.1.0 (c (n "iterator-enum") (v "0.1.0") (d (list (d (n "derive_utils") (r "^0.6.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1y6rx5p0w6vra7czydwbvzl4wf9slwpsngznhpxw0m9svim7miq2") (f (quote (("try_trait") ("trusted_len") ("std") ("exact_size_is_empty") ("default" "std"))))))

(define-public crate-iterator-enum-0.1.1 (c (n "iterator-enum") (v "0.1.1") (d (list (d (n "derive_utils") (r "^0.6.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0sgc6frrwrby0q63b85wlaa9v23rn7xmqxrjalfwazgiz62h7nk4") (f (quote (("try_trait") ("trusted_len") ("std") ("exact_size_is_empty") ("default" "std"))))))

(define-public crate-iterator-enum-0.1.2 (c (n "iterator-enum") (v "0.1.2") (d (list (d (n "derive_utils") (r "^0.6.3") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0qpdg33n83qsmlqy1r7jv0xfl5b9xrcn7in5b3v12ix7ws4bky96") (f (quote (("try_trait") ("trusted_len") ("std") ("exact_size_is_empty") ("default" "std"))))))

(define-public crate-iterator-enum-0.2.0 (c (n "iterator-enum") (v "0.2.0") (d (list (d (n "derive_utils") (r "^0.7.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full"))) (d #t) (k 0)))) (h "00jhhb9lajiymk5ls4am0q8vpafhrzpfl09pfkcc40bjlq7qj7by") (f (quote (("try_trait") ("trusted_len") ("exact_size_is_empty") ("default"))))))

(define-public crate-iterator-enum-0.2.1 (c (n "iterator-enum") (v "0.2.1") (d (list (d (n "derive_utils") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon_crate") (r "^1.1") (d #t) (k 2) (p "rayon")) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fna1qbc9q7jr1d815f9f1rird4jyxjiqcbb931x3db56jpff3yw") (f (quote (("try_trait") ("trusted_len") ("rayon") ("exact_size_is_empty"))))))

(define-public crate-iterator-enum-0.2.2 (c (n "iterator-enum") (v "0.2.2") (d (list (d (n "derive_utils") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon_crate") (r "^1.2") (d #t) (k 2) (p "rayon")) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lss5la54f061plw9hcgslvjhqjq5im9rms14jllzq9zcfsy8dbd") (f (quote (("trusted_len") ("rayon"))))))

(define-public crate-iterator-enum-0.2.3 (c (n "iterator-enum") (v "0.2.3") (d (list (d (n "iter-enum") (r "^0.2") (d #t) (k 0)) (d (n "rayon_crate") (r "^1.2") (d #t) (k 2) (p "rayon")))) (h "037zxpq6qxkkdyh068f29215iyxjqzv7s6l4bvri827smg6ryl7f") (f (quote (("trusted_len" "iter-enum/trusted_len") ("rayon" "iter-enum/rayon"))))))

