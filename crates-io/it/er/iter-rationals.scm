(define-module (crates-io it er iter-rationals) #:use-module (crates-io))

(define-public crate-iter-rationals-0.1.0 (c (n "iter-rationals") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-rational") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0c1i4f8hrxvcc76i43lwiyj6drrvffkizajg2lvg7x05z4nry8vs")))

(define-public crate-iter-rationals-0.2.0 (c (n "iter-rationals") (v "0.2.0") (d (list (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-rational") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "105g1gdx3lra6wm49766d98lxkccsv4ih3cyar8fn1db2gmvblbw")))

(define-public crate-iter-rationals-0.2.1 (c (n "iter-rationals") (v "0.2.1") (d (list (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-rational") (r "^0.4.1") (d #t) (k 0)))) (h "1w8m5gnibdi05y93hqwkvb0qqv8mqbx6hmn5bv3dgsfvwvjsnfl8")))

