(define-module (crates-io it er iter_fallback) #:use-module (crates-io))

(define-public crate-iter_fallback-0.1.0 (c (n "iter_fallback") (v "0.1.0") (h "09kd47dv54v30qqifs79waad77cihsg5wzr8sdrd8x81b86py3vz")))

(define-public crate-iter_fallback-0.1.1 (c (n "iter_fallback") (v "0.1.1") (h "1z5s8qzrkik4q72mx05n0glva15m9il17c0fafi3mxb5bq3yyi33")))

(define-public crate-iter_fallback-0.1.2 (c (n "iter_fallback") (v "0.1.2") (h "1n1js14fj7jfh4415x03s5gp9fhbxjkgi1073n0q869lsx620h9v")))

