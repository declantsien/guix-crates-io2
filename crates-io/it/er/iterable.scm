(define-module (crates-io it er iterable) #:use-module (crates-io))

(define-public crate-iterable-0.1.0 (c (n "iterable") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "1yamw5bkm5b81fbflhn2rnkd3sl11vkqd4mn6kz8bwwlnf6fyj76")))

(define-public crate-iterable-0.2.0 (c (n "iterable") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "1pj8j89pszwjl68p9l9ybm088x7xjnimynqhx56d2np26fylr3v3")))

(define-public crate-iterable-0.3.0 (c (n "iterable") (v "0.3.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "1kpfhwxpbycpz445b6h3g3p3rjyxbpnd10lvl8kg963z3bifwkrv")))

(define-public crate-iterable-0.4.0 (c (n "iterable") (v "0.4.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "14fwkkh19w3bsvmign2fpnr9344045b7v7a2dinbfv0mpmns6m0h")))

(define-public crate-iterable-0.4.1 (c (n "iterable") (v "0.4.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "1pdlxdx561c58mfbfv1y3jgiiswpxasmzy7al0x35szkl4avq5zn")))

(define-public crate-iterable-0.5.0 (c (n "iterable") (v "0.5.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "1nyz0g1k1rqdak6jzq13rjjbxz6xjd2w6x19v3c601mj7wk5kndp")))

(define-public crate-iterable-0.6.0 (c (n "iterable") (v "0.6.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "0px6axq6vlm68hj1wp78xilp9w46n8dh90kxasjmrzvxmgbdylf1")))

