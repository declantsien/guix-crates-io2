(define-module (crates-io it er iterslide) #:use-module (crates-io))

(define-public crate-iterslide-0.0.1 (c (n "iterslide") (v "0.0.1") (h "02xd99vh98k4p6bdn1dkgh2b70cfslxvwk2ysq5c4ixyj9vji5aj")))

(define-public crate-iterslide-0.0.2 (c (n "iterslide") (v "0.0.2") (h "03jn7r5n1246v8lwqd6qpih6dcjacw6prkl3062rsw3r9w579183")))

(define-public crate-iterslide-0.0.3 (c (n "iterslide") (v "0.0.3") (h "09lvwh3c5dnpljw236fwih37yfwks2vr6cibp8s927v66y18d2i4")))

(define-public crate-iterslide-0.0.4 (c (n "iterslide") (v "0.0.4") (h "1pak8fw8d1f5b2zdfdyyfsw96xqw9gyhnivw2hhvqdq3sggmyzq2")))

(define-public crate-iterslide-0.0.5 (c (n "iterslide") (v "0.0.5") (h "1i467kzn6qg9wfy086vpzqp8mc418dbpfmzmklx0ndd5302m42gz")))

(define-public crate-iterslide-1.0.0 (c (n "iterslide") (v "1.0.0") (h "0q07aa94aiiblvbyzjcknhji8chvvzzaj6ddw7fl5sl0yi9gl816")))

(define-public crate-iterslide-1.0.1 (c (n "iterslide") (v "1.0.1") (h "1qb2frl4yikd3nivqnfwcwid1812jw133qnpw2ryr0i5mxyip0dv")))

