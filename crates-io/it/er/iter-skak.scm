(define-module (crates-io it er iter-skak) #:use-module (crates-io))

(define-public crate-iter-skak-0.1.0 (c (n "iter-skak") (v "0.1.0") (h "0snl4vx9i3hc43kbc40hj5w581qjwh3b5z64w0vzzqsl42kn7qnz")))

(define-public crate-iter-skak-0.2.0 (c (n "iter-skak") (v "0.2.0") (h "141m9l4hc51ybqgfpq35zicgpgmaazm1c7sp2i6vcwchs2y3ysng")))

