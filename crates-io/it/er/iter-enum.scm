(define-module (crates-io it er iter-enum) #:use-module (crates-io))

(define-public crate-iter-enum-0.2.3 (c (n "iter-enum") (v "0.2.3") (d (list (d (n "derive_utils") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon_crate") (r "^1.2") (d #t) (k 2) (p "rayon")) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "023h1sgspca5rg77xaxn2k13sqb736435q4qkyyafk4p7y1pn42q") (f (quote (("trusted_len") ("rayon"))))))

(define-public crate-iter-enum-0.2.4 (c (n "iter-enum") (v "0.2.4") (d (list (d (n "derive_utils") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon_crate") (r "^1.2") (d #t) (k 2) (p "rayon")) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fz4xqs0amigfz16p926hj2zfyj7rm7mlrkc7y4raggc3dvskpic") (f (quote (("trusted_len") ("rayon"))))))

(define-public crate-iter-enum-0.2.5 (c (n "iter-enum") (v "0.2.5") (d (list (d (n "derive_utils") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (d #t) (k 2) (p "rayon")) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1fajdl5dprcr4hakdn478mxym9cphvmjkvmc0nvq9gsk5b0lpac6") (f (quote (("trusted_len") ("rayon"))))))

(define-public crate-iter-enum-0.2.6 (c (n "iter-enum") (v "0.2.6") (d (list (d (n "derive_utils") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (d #t) (k 2) (p "rayon")) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0nha62q9cfb40c7v14iyajb1g4y9ljza80kxkir2l4lfsgalj0f3") (f (quote (("trusted_len") ("rayon"))))))

(define-public crate-iter-enum-0.2.7 (c (n "iter-enum") (v "0.2.7") (d (list (d (n "derive_utils") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (d #t) (k 2) (p "rayon")) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0wf8i6wcvs3gx3c07r8kq6f1sz8vrvsjvbrqzzyyz35lscj4zlya") (f (quote (("trusted_len") ("rayon"))))))

(define-public crate-iter-enum-1.0.0 (c (n "iter-enum") (v "1.0.0") (d (list (d (n "derive_utils") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (d #t) (k 2) (p "rayon")) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0zfv5m1ls00pq531pl6caidagyyqa3c3zl928axvqlvydkns77i0") (f (quote (("rayon"))))))

(define-public crate-iter-enum-1.0.1 (c (n "iter-enum") (v "1.0.1") (d (list (d (n "derive_utils") (r "^0.11") (d #t) (k 0)) (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (d #t) (k 2) (p "rayon")) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01dhvfjdv53rm2sxk3xxksxma53lhc50r58ayr6rrrppkl6pz52g") (f (quote (("rayon"))))))

(define-public crate-iter-enum-1.0.2 (c (n "iter-enum") (v "1.0.2") (d (list (d (n "derive_utils") (r "^0.12") (d #t) (k 0)) (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (d #t) (k 2) (p "rayon")) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "18r1jg38b8ax7bpw617yw7kc4b9k3f5vgyhc2x41rsh1p659n7gh") (f (quote (("rayon")))) (r "1.31")))

(define-public crate-iter-enum-1.1.0 (c (n "iter-enum") (v "1.1.0") (d (list (d (n "derive_utils") (r "^0.13") (d #t) (k 0)) (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (d #t) (k 2) (p "rayon")) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "12z95r0573anzv0ac0871qay3a4llrxzzsbj2p6qpsd20dw9nw8y") (f (quote (("rayon")))) (r "1.56")))

(define-public crate-iter-enum-1.1.1 (c (n "iter-enum") (v "1.1.1") (d (list (d (n "derive_utils") (r "^0.13.1") (d #t) (k 0)) (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (d #t) (k 2) (p "rayon")) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1kb50wiiyicrhlbdkxfc9wy7xd2ddbagdivjf44cpjb1ad7graw3") (f (quote (("rayon")))) (r "1.56")))

(define-public crate-iter-enum-1.1.2 (c (n "iter-enum") (v "1.1.2") (d (list (d (n "derive_utils") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (d #t) (k 2) (p "rayon")) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "11isk3pv4n1c3mk86cgw4ydxq1dwhn5y79204df3g65g7way6jpf") (f (quote (("rayon")))) (r "1.56")))

(define-public crate-iter-enum-1.1.3 (c (n "iter-enum") (v "1.1.3") (d (list (d (n "derive_utils") (r "^0.14.1") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (d #t) (k 2) (p "rayon")) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "0bq7vvhng8mf4l7lchckwxdz8pi3kaiy5jpyvwqif2l95jq8xzls") (f (quote (("rayon")))) (r "1.56")))

