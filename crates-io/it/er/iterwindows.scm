(define-module (crates-io it er iterwindows) #:use-module (crates-io))

(define-public crate-iterwindows-0.1.0 (c (n "iterwindows") (v "0.1.0") (h "0hb4ydf9bx0q0n8mi6b9a0prdq7pw3bd3yx05g0wafgmxrh4xch8")))

(define-public crate-iterwindows-0.1.1 (c (n "iterwindows") (v "0.1.1") (h "0ncrl2f7c0ck94p60jzxaz150i3ppajgbsqaq8g22dwcgp83ln93") (r "1.56")))

(define-public crate-iterwindows-0.1.2 (c (n "iterwindows") (v "0.1.2") (d (list (d (n "arrays") (r "^0.1.0") (d #t) (k 0)))) (h "0jyhyaq5idbix15w61yvvny22d9xdzvlsv7a169jds46gfabq0yj") (r "1.56")))

(define-public crate-iterwindows-0.2.0 (c (n "iterwindows") (v "0.2.0") (d (list (d (n "arrays") (r "^0.1.0") (d #t) (k 0)))) (h "1kcwxpdf8bv0l620jf7kbg4vr2pximz550dhk2zqa2d11xi9r0j0") (r "1.56")))

(define-public crate-iterwindows-0.3.0 (c (n "iterwindows") (v "0.3.0") (d (list (d (n "arrays") (r "^0.1.0") (d #t) (k 0)))) (h "1c1kcfz7cxgjc0z8jgf5cpbwizwpyxyg8xm9prynda8rq243mgrw") (r "1.56")))

(define-public crate-iterwindows-0.4.0 (c (n "iterwindows") (v "0.4.0") (d (list (d (n "itermore") (r "^0.5.0") (f (quote ("array_windows"))) (k 0)))) (h "0fj8p8qgiww5p1p80w51n2kr439h61k0dj9sa1izay6igfypqhw8") (r "1.56")))

(define-public crate-iterwindows-0.5.0 (c (n "iterwindows") (v "0.5.0") (d (list (d (n "itermore") (r "^0.6.0") (f (quote ("array_windows"))) (k 0)))) (h "1rn4adhkwi5d7i112sxiy7rxp7x4inrky4j2gyz997y878pxkads") (r "1.56")))

