(define-module (crates-io it er iter-merge-sort) #:use-module (crates-io))

(define-public crate-iter-merge-sort-0.1.0 (c (n "iter-merge-sort") (v "0.1.0") (h "15wmq0swd5rylj10y7sikm4kybg9dlafwpvxqa9phqmdffzq6ndq")))

(define-public crate-iter-merge-sort-0.1.1 (c (n "iter-merge-sort") (v "0.1.1") (h "1kiqg4xd3dbrhia4g1517i3zsq4azbvs39k533j9658k98vxmn37")))

(define-public crate-iter-merge-sort-0.1.2 (c (n "iter-merge-sort") (v "0.1.2") (h "1wi1y9c6hjj594c26c07qyq3mrajr36lamar3sd8nr5psn9dyhnj")))

