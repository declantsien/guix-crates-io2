(define-module (crates-io it er iter-diff) #:use-module (crates-io))

(define-public crate-iter-diff-0.1.0 (c (n "iter-diff") (v "0.1.0") (h "0lvx2vq5da029anaqzkaw078hg9hmwk8klaash7hw0cacyndqwz0") (y #t)))

(define-public crate-iter-diff-0.1.1 (c (n "iter-diff") (v "0.1.1") (h "0kvyav6s1c8dbqx9zy5gqr1kj4d4p45c17mps16n572gb54vbnyf")))

