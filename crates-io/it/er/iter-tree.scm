(define-module (crates-io it er iter-tree) #:use-module (crates-io))

(define-public crate-iter-tree-0.1.0 (c (n "iter-tree") (v "0.1.0") (h "1ismmsw2r020vcc2fwiwjs9n9xndp05ayckaq72dd1qf7zzmdbwl") (f (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1.1 (c (n "iter-tree") (v "0.1.1") (h "1kw2ap2qdcl9mwmyn9xggc5k1i2hhy3vyg775xiky436v65wnfhb") (f (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1.2 (c (n "iter-tree") (v "0.1.2") (h "0xi5pljd7hx72rq9r0c9gxnzvcjw4v2mld7pfrdz8gg23gw7xb8k") (f (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1.3 (c (n "iter-tree") (v "0.1.3") (h "110k602rsxn876z55sb26a9q6kzvi7qp3mqv2c3lmx5akxzlld0b") (f (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1.4 (c (n "iter-tree") (v "0.1.4") (h "1h8h36ldcy0zxr6abm3lf8a7mrazxf087i863vhcmh8f19a8n4lw") (f (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1.5 (c (n "iter-tree") (v "0.1.5") (h "11cwvyi96hwr5q9ri51b666l85636kjzfqyx2hx1a176brz8lzn9") (f (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1.6 (c (n "iter-tree") (v "0.1.6") (h "0f5ld65rczvq9mgic9sfg01h76mmw5fl88x51by7a472kfrihn7p") (f (quote (("vec") ("deque") ("default" "vec"))))))

