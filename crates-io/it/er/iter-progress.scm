(define-module (crates-io it er iter-progress) #:use-module (crates-io))

(define-public crate-iter-progress-0.3.0 (c (n "iter-progress") (v "0.3.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0s6xnf2r83qd2zj55pn9n05pbbdwi8xysp0qj83py19rwvchba8l")))

(define-public crate-iter-progress-0.4.0 (c (n "iter-progress") (v "0.4.0") (h "1c4333zj7sswhvfxicdivx99fm3jlhrw4zrrxz1xpbrhkcwbgz30")))

(define-public crate-iter-progress-0.5.0 (c (n "iter-progress") (v "0.5.0") (h "08d8yyf2fz8f0r23zkix4rlb64vbdsrb2a414acldcdvbkr9zmi7")))

(define-public crate-iter-progress-0.6.0 (c (n "iter-progress") (v "0.6.0") (h "01iw6hb4687qsfscgl9qg3qdijx0nbrwbnlhhz96w5j87jm9fr0j")))

(define-public crate-iter-progress-0.7.0 (c (n "iter-progress") (v "0.7.0") (h "1579zci8y2ynqly1b4b7h9kzdq9fwzgddna9dxx712k5jb4qbzq5")))

(define-public crate-iter-progress-0.8.0 (c (n "iter-progress") (v "0.8.0") (h "1m2w1h94zy6p7yvkjgwq16ks6p8x1kjhnv3gd4b8wfjfvmj9s1cp")))

(define-public crate-iter-progress-0.8.1-rc1 (c (n "iter-progress") (v "0.8.1-rc1") (h "075g21yl686hgyvzr8k0s5s1f9fyj58m98wwllc9cn82kbbql5mb")))

