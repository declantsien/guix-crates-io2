(define-module (crates-io it er iter-group) #:use-module (crates-io))

(define-public crate-iter-group-0.1.0 (c (n "iter-group") (v "0.1.0") (h "1x2xdw2i7qim3720dvqlvwhk9xq0v5w6r71dr9cm083gw69ssskc")))

(define-public crate-iter-group-0.2.0 (c (n "iter-group") (v "0.2.0") (h "16w8nr3v3nkdsnd44djdmx89l208znq6x370hv14bjwgigjfjz1g")))

