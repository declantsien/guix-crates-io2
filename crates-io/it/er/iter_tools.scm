(define-module (crates-io it er iter_tools) #:use-module (crates-io))

(define-public crate-iter_tools-0.1.0 (c (n "iter_tools") (v "0.1.0") (d (list (d (n "itertools") (r "~0.10.3") (d #t) (k 0)) (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "09f336fcfdnvybm2kdrph2gjcn6m3fbszk6m6qpzk1cy3r8n5jbg")))

(define-public crate-iter_tools-0.1.1 (c (n "iter_tools") (v "0.1.1") (d (list (d (n "itertools") (r "~0.10.3") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1zc0kv2b478622x4iqp9gw2ghxhwy079qxma9mxqx5wpnpxsq11h")))

(define-public crate-iter_tools-0.1.2 (c (n "iter_tools") (v "0.1.2") (d (list (d (n "itertools") (r "~0.10.3") (o #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0klqs6sgfjbfmkv2p1d679zcm63smr458rmka8m0qx14gpncxv1f") (f (quote (("use_std" "itertools/use_std") ("use_alloc" "itertools/use_alloc") ("default" "itertools" "use_std") ("all" "itertools" "use_std"))))))

(define-public crate-iter_tools-0.1.3 (c (n "iter_tools") (v "0.1.3") (d (list (d (n "itertools") (r "~0.10.3") (o #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0lbiw93n3a97w8c8pl6km2s3fjx7758nzf3lkpwqipfszdzziqay") (f (quote (("use_std" "itertools/use_std") ("use_alloc" "itertools/use_alloc") ("default" "itertools" "use_std") ("all" "itertools" "use_std"))))))

(define-public crate-iter_tools-0.1.4 (c (n "iter_tools") (v "0.1.4") (d (list (d (n "itertools") (r "~0.10.3") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "15s8hn1p8bhv5nqh16f1xwar9can3pk20migncmjbcxkk7fay72k") (f (quote (("use_std" "itertools/use_std") ("use_alloc" "itertools/use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-iter_tools-0.2.0 (c (n "iter_tools") (v "0.2.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "^0.1.5") (d #t) (k 2)))) (h "1qgl6bmbwx4yn0d83i21bhfzf3cmqyrs0qfwg7wfgl5wd1l0qhd1") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.4.0 (c (n "iter_tools") (v "0.4.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.4.0") (d #t) (k 2)))) (h "0yghvp4p9bvvy5nh87l5m96hdxkpnzf5arnl8kzsnaa2bhp1c8d6") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.5.0 (c (n "iter_tools") (v "0.5.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "0c4mhk4yk1ga9hvfl5i4g13ca2vnw181xcgbzjjchxpvr0jbfan0") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.6.0 (c (n "iter_tools") (v "0.6.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0kq2ivicidq6bwvincaqsnfixlysgxaignrldxlvf9ggd9syflv6") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.7.0 (c (n "iter_tools") (v "0.7.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1q14cp750m7z1vvcwc0mply6dq3h3cabfwjjk9s84ycj222gh94j") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.8.0 (c (n "iter_tools") (v "0.8.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1kjq64xqddr07wxhmrwjzb76x6w88l0p21miwzmjhk7y79lx18hb") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.9.0 (c (n "iter_tools") (v "0.9.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0r6fqc7gnc6r5zj0q2vqmmz53ia6l2f91lb5z06bgrhqjrp63xjr") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.10.0 (c (n "iter_tools") (v "0.10.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0j7bh051f1h94sh34vpgalqh1zljn27f7r9jkv839mjkyxjq58h7") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.11.0 (c (n "iter_tools") (v "0.11.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "19v5ya4hfn06c963f5w9rzh55yadplfdka7zbcn6xr0zizix7yp8") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.12.0 (c (n "iter_tools") (v "0.12.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "0ca1gh2m6y2lwj4i60q4zrxyca0qrzn6b6xp2b7yjnywk53l7qwn") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.13.0 (c (n "iter_tools") (v "0.13.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1ai7jd22xa9a8z7dlcad5zmllz16p202dzvgflyxrpax7hmjc73x") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.14.0 (c (n "iter_tools") (v "0.14.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "0cwmx8v2yqg00nv8hl9xhl53nvhjhsxr9qrl11kh66lpbwgpzv7r") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.15.0 (c (n "iter_tools") (v "0.15.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "1lgzdxjlvz4nlh79q8qjanm9m3w5428ic64n7r7zjf0yg3ajz2hb") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.16.0 (c (n "iter_tools") (v "0.16.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "18pw6q36lxncwkx5q3fnbx4b8gdd2c4q7jp7fscfza0l0zpyk8yx") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.17.0 (c (n "iter_tools") (v "0.17.0") (d (list (d (n "itertools") (r "~0.11.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.9.0") (d #t) (k 2)))) (h "1w7kngvw0xq6i3fif8wq73hb6j37g280fhq1a9ykd8h86c5z9yam") (f (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

