(define-module (crates-io it er itertools-wild) #:use-module (crates-io))

(define-public crate-itertools-wild-0.1.0 (c (n "itertools-wild") (v "0.1.0") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "itertools") (r "^0.6.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "0pnm8ysww74rh3wm42s3xa2yz2yn3szm7irl52b69w7499pp0y0m")))

(define-public crate-itertools-wild-0.1.1 (c (n "itertools-wild") (v "0.1.1") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "itertools") (r "^0.6.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "1m72ib0c2nmvzpqbqdlnrk6020fbaq64n5912z8gkrsjxv4j9kxb")))

