(define-module (crates-io it er iterm) #:use-module (crates-io))

(define-public crate-iterm-0.1.0 (c (n "iterm") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)))) (h "1bhpvgvwwlrw57s7435jzjh0yksv4dpj9b4w2nna8sp3zfc8x358")))

(define-public crate-iterm-0.2.0 (c (n "iterm") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)))) (h "1s7hwmvrhmx7a2pals25zb700yrl63wjim6iqsv3hmn4xhpzkx6a")))

(define-public crate-iterm-0.3.0 (c (n "iterm") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)))) (h "1rln4slps82g6xnc7z66zd868j2gw3564w3cbsa8ma95ab8dp7bd")))

(define-public crate-iterm-0.4.0 (c (n "iterm") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)))) (h "1zjkhzax1a7bbn0hcfjfx0jak77hgvsvhb0i65d2d9w0qaf72jjc")))

(define-public crate-iterm-0.5.0 (c (n "iterm") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)))) (h "1gba4011bg4vpdaykcqrmj3ww9kpyjx7vjzz68xvnr9dmba1q2zw")))

(define-public crate-iterm-0.6.0 (c (n "iterm") (v "0.6.0") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)))) (h "0s0riqxny9yml8dhwbj3w81cc6lnrirk8vh2pwrj84mq1j4mrsr9")))

