(define-module (crates-io it er iter_traits) #:use-module (crates-io))

(define-public crate-iter_traits-0.1.0 (c (n "iter_traits") (v "0.1.0") (h "0qr6q15sc3qaxd7rcdp80mzkk8nsxmksjaz6kdi7c8rcv34jv536") (y #t)))

(define-public crate-iter_traits-0.1.1 (c (n "iter_traits") (v "0.1.1") (h "13npkrbn32zp2mg5r681p0n18xvzn491ygqg5gl65qpxivl719x7") (y #t)))

