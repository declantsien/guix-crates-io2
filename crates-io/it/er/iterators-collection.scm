(define-module (crates-io it er iterators-collection) #:use-module (crates-io))

(define-public crate-iterators-collection-0.3.0 (c (n "iterators-collection") (v "0.3.0") (h "04ql3fsjl6cjz5gvcsdmnjmkgb9m2nh4l9jzw73zljaanhjqagak")))

(define-public crate-iterators-collection-0.3.1 (c (n "iterators-collection") (v "0.3.1") (h "0a1jz6adl98c2p3af0xh1al07mc3w67ci79xdpq2vadnqp6mdrwh")))

(define-public crate-iterators-collection-0.3.2 (c (n "iterators-collection") (v "0.3.2") (h "0yhagmdzpc50vpz4rn5q14jrbfaxxan2s55vm295i2rnf7n9bmkp")))

(define-public crate-iterators-collection-0.3.3 (c (n "iterators-collection") (v "0.3.3") (h "0886gw47ygjzwmnf30pzmm0y64bqwg4dy2wj1xdxm1m1y586c8dy")))

