(define-module (crates-io it ch itch-logview) #:use-module (crates-io))

(define-public crate-itch-logview-0.1.0 (c (n "itch-logview") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.2.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1f5l7y9bwgi25pr5a4043qrsf1czf1k1jzaw0avk2z9p6klxw1x3")))

