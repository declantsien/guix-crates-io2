(define-module (crates-io it ch itchy) #:use-module (crates-io))

(define-public crate-itchy-0.1.0 (c (n "itchy") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.0") (d #t) (k 0)) (d (n "decimal") (r "^1.0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 0)) (d (n "nom") (r "^3.2.0") (d #t) (k 0)))) (h "0qw8jw37r6gn6mljqv5v2bd6cb39xz2yljxhsxg5qacl2mwfpg1n")))

(define-public crate-itchy-0.1.1 (c (n "itchy") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4.1") (d #t) (k 0)) (d (n "decimal") (r "^1.0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.20") (d #t) (k 0)) (d (n "nom") (r "^3.2.0") (d #t) (k 0)))) (h "18dww31zmlq5j6k5pif29rhdz5p6jvb5abyf4cni7f4qpqzy29fy")))

(define-public crate-itchy-0.2.0 (c (n "itchy") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "decimal") (r "^2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)))) (h "0w9ql4kpbxzxrrmv5b4hznnmgs04vyvy1bwfy7dn0kjfaqzhxhfs")))

(define-public crate-itchy-0.2.1 (c (n "itchy") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "decimal") (r "^2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)))) (h "1fx9acar9nmziiyjm8dm5wr6nskh1pklij4gzx8g06l4jnc9mnpj")))

(define-public crate-itchy-0.2.2 (c (n "itchy") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "decimal") (r "^2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)))) (h "1w1r0b0c21wmj0hw5hwgn3zj415y6754zmajhbr95lh169c68n88")))

(define-public crate-itchy-0.2.3 (c (n "itchy") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "decimal") (r "^2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mrxnk1fw27viyh6dj8scz883m8bj2akysaj3man13l7ks8k6y9x") (s 2) (e (quote (("serde" "dep:serde" "arrayvec/serde"))))))

