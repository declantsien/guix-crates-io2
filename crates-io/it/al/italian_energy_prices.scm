(define-module (crates-io it al italian_energy_prices) #:use-module (crates-io))

(define-public crate-italian_energy_prices-0.1.0 (c (n "italian_energy_prices") (v "0.1.0") (d (list (d (n "calamine") (r "^0.18") (d #t) (k 0)))) (h "03bm90y3ib6sqf6l8bysmyibw7gl9m37qdfsipfxfkdr045vhjkv")))

(define-public crate-italian_energy_prices-0.1.1 (c (n "italian_energy_prices") (v "0.1.1") (d (list (d (n "calamine") (r "^0.18") (d #t) (k 0)))) (h "0f84xwfr1qs4c4yyij6j0i7ndwpn6b4bb3sivcisgvq2v9h8hphq")))

(define-public crate-italian_energy_prices-0.1.2 (c (n "italian_energy_prices") (v "0.1.2") (d (list (d (n "calamine") (r "^0.19") (d #t) (k 0)))) (h "15fyj5ng2c0mrzf63b15mm2d63n5jz1pxp1pb8nqhxf65i5y6bmc")))

(define-public crate-italian_energy_prices-0.1.3 (c (n "italian_energy_prices") (v "0.1.3") (d (list (d (n "calamine") (r "^0.20") (d #t) (k 0)))) (h "0vdrxi1mvshj20fad99yxqskzc5p7skxjrsnqsg3w4d41klbhp8c")))

(define-public crate-italian_energy_prices-0.1.4 (c (n "italian_energy_prices") (v "0.1.4") (d (list (d (n "calamine") (r "^0.22") (d #t) (k 0)))) (h "1m65b3lvjk0m87kmzw6n64m24ipn1gjyp5q58wp74zcavgvnycxs")))

(define-public crate-italian_energy_prices-0.1.5 (c (n "italian_energy_prices") (v "0.1.5") (d (list (d (n "calamine") (r "^0.24") (d #t) (k 0)))) (h "1cq5bm9d8154mh1xzgy6qdxaj3k6bwnmij5m4cnrg5vxicfrsx52")))

