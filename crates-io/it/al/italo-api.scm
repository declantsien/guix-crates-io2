(define-module (crates-io it al italo-api) #:use-module (crates-io))

(define-public crate-italo-api-0.0.1 (c (n "italo-api") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g8cb3cnppb2ix6rkrw455z1w0n9626s61nh68rd5j0vv7ddyh45")))

(define-public crate-italo-api-0.0.2 (c (n "italo-api") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0drja5jsrv9z8f9qzmlkv4cbrcqf4vjabxnf54chg4fw2f5radlb")))

