(define-module (crates-io it ty ittyr) #:use-module (crates-io))

(define-public crate-ittyr-0.1.0 (c (n "ittyr") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0jbxp6awkkzbvbf16f3ybijdyhhdkfpjjii7sd7if1mr7217xz39")))

(define-public crate-ittyr-0.1.1 (c (n "ittyr") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0yrzppfjs16yrzisxc3rca90snizfsm241bq1d1pchadb0j0i5qf")))

