(define-module (crates-io it oa itoap) #:use-module (crates-io))

(define-public crate-itoap-0.1.0 (c (n "itoap") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "itoa") (r "^0.4.6") (f (quote ("i128"))) (d #t) (k 2)))) (h "1sdh7n4h6y7ga5vygxanf1va77dywfkii2crfx1z8xdlb5blm04f") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itoap-0.1.1 (c (n "itoap") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "itoa") (r "^0.4.6") (f (quote ("i128"))) (d #t) (k 2)))) (h "1i5byqiir7mar2fajah3wzkpwnxragc4z8bds52i5l4csy8bzgql") (f (quote (("std" "alloc") ("perf-inline") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-itoap-0.1.2 (c (n "itoap") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "itoa") (r "^0.4.6") (f (quote ("i128"))) (d #t) (k 2)))) (h "1rvay6lg6baj4h3grf2h6qsxdag31l3abwfy416l1x0sy7iwminx") (f (quote (("std" "alloc") ("simd") ("default" "simd" "std") ("alloc"))))))

(define-public crate-itoap-0.1.3 (c (n "itoap") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "itoa") (r "^0.4.6") (f (quote ("i128"))) (d #t) (k 2)))) (h "00lshczfzrcnpyamlqjqzcr07jka0hwwvbc7jsq44g51ianb7icb") (f (quote (("std" "alloc") ("simd") ("default" "simd" "std") ("alloc"))))))

(define-public crate-itoap-0.1.4 (c (n "itoap") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.2") (f (quote ("small_rng"))) (k 2)))) (h "05xy9vn7q2znn214yyr0m82s44y7qz08kp1kzm1vffqypadyb71s") (f (quote (("std" "alloc") ("simd") ("default" "simd" "std") ("alloc"))))))

(define-public crate-itoap-0.1.5 (c (n "itoap") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.2") (f (quote ("small_rng"))) (k 2)))) (h "0xspww8y6dwcgszl3lcn1f7n7ghxsa9zmxyv7m2c3g46wjx1dzr2") (f (quote (("std" "alloc") ("simd") ("default" "simd" "std") ("alloc"))))))

(define-public crate-itoap-1.0.0 (c (n "itoap") (v "1.0.0") (d (list (d (n "itoa") (r "^0.4.6") (f (quote ("i128"))) (k 2)) (d (n "rand") (r "^0.8.2") (f (quote ("small_rng"))) (k 2)))) (h "1hxbqj3fqgqffw0db3qhka4hk736jiyn9y2hw7sc40clv7ssmiip") (f (quote (("std" "alloc" "itoa/std") ("simd") ("default" "simd" "std") ("alloc")))) (y #t)))

(define-public crate-itoap-1.0.1 (c (n "itoap") (v "1.0.1") (d (list (d (n "itoa") (r "^0.4.6") (f (quote ("i128"))) (k 2)) (d (n "rand") (r "^0.8.2") (f (quote ("small_rng"))) (k 2)))) (h "1f48gsd18kbvskwbnwszhqjpk1l4rdmahh7kaz86b432cj9g8a4h") (f (quote (("std" "alloc") ("simd") ("default" "simd" "std") ("alloc"))))))

