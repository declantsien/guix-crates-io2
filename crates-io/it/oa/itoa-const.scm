(define-module (crates-io it oa itoa-const) #:use-module (crates-io))

(define-public crate-itoa-const-1.0.0 (c (n "itoa-const") (v "1.0.0") (h "03h1ly4ka22mdpn0aznf9b5wyr3vbma9g6y49mifhxc6sza43rq5") (y #t) (r "1.36")))

(define-public crate-itoa-const-1.0.1 (c (n "itoa-const") (v "1.0.1") (h "1v7f4f3r3qa6wvpv9869xaqhi2rphfgps9yfrmdpjzngnj96vryr") (y #t) (r "1.36")))

(define-public crate-itoa-const-1.0.5 (c (n "itoa-const") (v "1.0.5") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0n46nlkaxsgwbnsxyv3ifa912fkkq5i3mhl3l3nvzklwz9cpzxn6") (y #t) (r "1.36")))

