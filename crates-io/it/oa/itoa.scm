(define-module (crates-io it oa itoa) #:use-module (crates-io))

(define-public crate-itoa-0.1.0 (c (n "itoa") (v "0.1.0") (h "0avhjk310kpl5rf0ff2yvg5qcg26hf666ynhb8bipbfkd8l91qwg")))

(define-public crate-itoa-0.1.1 (c (n "itoa") (v "0.1.1") (h "18g7p2hrb3dk84z3frfgmszfc9hjb4ps9vp99qlb1kmf9gm8hc5f")))

(define-public crate-itoa-0.2.0 (c (n "itoa") (v "0.2.0") (h "0bq2b0x919b080yxfp8ff65gwqmdncg1ix0nh3pz4xf7adv9x3zz")))

(define-public crate-itoa-0.2.1 (c (n "itoa") (v "0.2.1") (h "0dl6fw90jdckx3faik2ziq76ksyskfc1xl6smlk1b9gaxp6sqdsm")))

(define-public crate-itoa-0.3.0 (c (n "itoa") (v "0.3.0") (h "1mdqbfhwfx54l0rap357jgcc661qbqssvi7y2zl7s1l7qp19vzci")))

(define-public crate-itoa-0.3.1 (c (n "itoa") (v "0.3.1") (h "0715dhfb2pscd2467as4isnv5v770n2j96792fn9mzb6pi7l0bzb")))

(define-public crate-itoa-0.3.2 (c (n "itoa") (v "0.3.2") (h "147vllb74mqrnar5psdc5jjaab6czjvqlydrlab89hnv3g5gck7p")))

(define-public crate-itoa-0.3.3 (c (n "itoa") (v "0.3.3") (h "1xp188lcf1dcq04axwp8s3vwgc2q7kw5qmgxr7dpgvf289s2a5xc")))

(define-public crate-itoa-0.3.4 (c (n "itoa") (v "0.3.4") (h "136vwi6l2k1vrlvfx49lhficj813pk88xrcx1q3axqh1mwms6943") (f (quote (("i128"))))))

(define-public crate-itoa-0.4.0 (c (n "itoa") (v "0.4.0") (h "11wmaj7rfg47lnc9s1cbm8dw8w941a6swy37ywvrqy4gfxhdzacj") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4.1 (c (n "itoa") (v "0.4.1") (h "10n6y9nrmzya7c0h1dqn7mf79zvk8zz5vrb6c5cmmjp1c7nbnsf0") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4.2 (c (n "itoa") (v "0.4.2") (h "01i6wrldqr0gnq287ddp4ip4h8zf4qr5zl8bbxmph7fdimaminss") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4.3 (c (n "itoa") (v "0.4.3") (h "02z6pfmppv4n5zcnz2aydqijvmgvg4fd6wr3s4q0xwsi953g61hk") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4.4 (c (n "itoa") (v "0.4.4") (h "0zvg2d9qv3avhf3d8ggglh6fdyw8kkwqg3r4622ly5yhxnvnc4jh") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4.5 (c (n "itoa") (v "0.4.5") (h "13nxqrfnh83a7x5rw4wq2ilp8nxvwy74dxzysdg59dbxqk0agdxq") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4.6 (c (n "itoa") (v "0.4.6") (h "1rnpb7rr8df76gnlk07b9086cn7fc0dxxy1ghh00q6nip7bklvyw") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4.7 (c (n "itoa") (v "0.4.7") (h "0di7fggbknwfjcw8cgzm1dnm3ik32l2m1f7nmyh8ipmh45h069fx") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4.8 (c (n "itoa") (v "0.4.8") (h "1m1dairwyx8kfxi7ab3b5jc71z1vigh9w4shnhiajji9avzr26dp") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-1.0.0 (c (n "itoa") (v "1.0.0") (h "0y53m7zlkyk8m5niwjbdwqsz05fg6vyg1pg6r4q7ns47yqffdwyj") (r "1.36")))

(define-public crate-itoa-1.0.1 (c (n "itoa") (v "1.0.1") (h "0d8wr2qf5b25a04xf10rz9r0pdbjdgb0zaw3xvf8k2sqcz1qzaqs") (r "1.36")))

(define-public crate-itoa-1.0.2 (c (n "itoa") (v "1.0.2") (h "13ap85z7slvma9c36bzi7h5j66dm5sxm4a2g7wiwxbsh826nfb0i") (r "1.36")))

(define-public crate-itoa-1.0.3 (c (n "itoa") (v "1.0.3") (h "0m7773c6lb61c20gp81a0pac8sh8w473m4rck0x247zyfi3gi2kc") (r "1.36")))

(define-public crate-itoa-1.0.4 (c (n "itoa") (v "1.0.4") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1k2y06z87wjxda4v751ybf7n9q4kwl9ly9jffa78vpxs3qsas5s2") (r "1.36")))

(define-public crate-itoa-1.0.5 (c (n "itoa") (v "1.0.5") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0h343sak699ci49anaa7l3p94b9kcc4ypaqwcam6qsz8p7s85mgs") (r "1.36")))

(define-public crate-itoa-1.0.6 (c (n "itoa") (v "1.0.6") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "19jc2sa3wvdc29zhgbwf3bayikq4rq18n20dbyg9ahd4hbsxjfj5") (r "1.36")))

(define-public crate-itoa-1.0.7 (c (n "itoa") (v "1.0.7") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1n4z37s9h8fd3n46gcvk1l2y3x48hjp84h59r94qlgc9nbx4ian0") (r "1.36")))

(define-public crate-itoa-1.0.8 (c (n "itoa") (v "1.0.8") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0jig0fmn7bdqpb1jz3ibnlw6gdm31wyn510x0k9mninch59jmc32") (r "1.36")))

(define-public crate-itoa-1.0.9 (c (n "itoa") (v "1.0.9") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0f6cpb4yqzhkrhhg6kqsw3wnmmhdnnffi6r2xzy248gzi2v0l5dg") (r "1.36")))

(define-public crate-itoa-1.0.10 (c (n "itoa") (v "1.0.10") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0k7xjfki7mnv6yzjrbnbnjllg86acmbnk4izz2jmm1hx2wd6v95i") (r "1.36")))

(define-public crate-itoa-1.0.11 (c (n "itoa") (v "1.0.11") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0nv9cqjwzr3q58qz84dcz63ggc54yhf1yqar1m858m1kfd4g3wa9") (r "1.36")))

