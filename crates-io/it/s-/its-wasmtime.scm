(define-module (crates-io it s- its-wasmtime) #:use-module (crates-io))

(define-public crate-its-wasmtime-0.1.0 (c (n "its-wasmtime") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wasmtime") (r "^20.0.2") (f (quote ("runtime" "component-model"))) (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^20.0.2") (d #t) (k 0)) (d (n "wit-component") (r "^0.207.0") (d #t) (k 0)))) (h "0vsskisas0gycvpzdbs0yb1bz946afxs07wgzcpldr7m2bwpphyq")))

(define-public crate-its-wasmtime-0.1.1 (c (n "its-wasmtime") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wasmtime") (r "^20.0.2") (f (quote ("runtime" "component-model"))) (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^20.0.2") (d #t) (k 0)) (d (n "wit-component") (r "^0.207.0") (d #t) (k 0)))) (h "0km7ypqik53ylwb9gfrh23x9il5rlihkpr3azjbb6bmbndpvlrlw")))

