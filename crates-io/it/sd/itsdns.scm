(define-module (crates-io it sd itsdns) #:use-module (crates-io))

(define-public crate-itsdns-0.1.0 (c (n "itsdns") (v "0.1.0") (d (list (d (n "embedded-nal-async") (r "^0.4.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "embedded-io") (r "^0.4.0") (f (quote ("async" "std"))) (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 2)))) (h "1jzx110wgx7kdvfsr1qdfapjlw29pyniighd121xi25lgcmz71hh")))

