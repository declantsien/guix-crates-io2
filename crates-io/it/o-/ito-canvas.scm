(define-module (crates-io it o- ito-canvas) #:use-module (crates-io))

(define-public crate-ito-canvas-0.1.0 (c (n "ito-canvas") (v "0.1.0") (d (list (d (n "dot-canvas") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-canvas") (r "^0.1.0") (d #t) (k 0)))) (h "1qlhmxcxiphq201gx08d1arjrvyv2y7rhhmwi451fpimv93qpzi3")))

(define-public crate-ito-canvas-0.1.1 (c (n "ito-canvas") (v "0.1.1") (d (list (d (n "dot-canvas") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-canvas") (r "^0.1.0") (d #t) (k 0)))) (h "0dv0mvzcrb29ph24gjk820x5264h5kw698n0yrw8dpkxvqsb6hwn")))

