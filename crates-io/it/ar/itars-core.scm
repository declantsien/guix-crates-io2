(define-module (crates-io it ar itars-core) #:use-module (crates-io))

(define-public crate-itars-core-0.1.0 (c (n "itars-core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "03ilc2gfzpbv45j69gycblvs2c511sychn2rm2bdii8iih2cb7bc")))

