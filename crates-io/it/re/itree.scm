(define-module (crates-io it re itree) #:use-module (crates-io))

(define-public crate-itree-0.2.2 (c (n "itree") (v "0.2.2") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "indextree") (r "^1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0s8vy6k4yihnvy87pms0r4jp01lydhnnj4cqmd1gsawas8sj02rc")))

(define-public crate-itree-0.3.0 (c (n "itree") (v "0.3.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "indextree") (r "^1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "181pcih7g96hc6ha6iwf01rlz222q95dclxd89bs8gb2ad7h0vd4")))

(define-public crate-itree-0.3.1 (c (n "itree") (v "0.3.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "indextree") (r "^1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "16wj2i3q8lgnvi3fyrs6vrqlvd6xgrqzzr6d2yfd67g9jfl3a0jg")))

