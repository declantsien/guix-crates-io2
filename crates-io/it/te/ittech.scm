(define-module (crates-io it te ittech) #:use-module (crates-io))

(define-public crate-ittech-0.1.1 (c (n "ittech") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nom") (r "^6.1") (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "155xbxhzzxi4261gp23pwjinb4whzhwn7jkqf9srq02wr5304siw")))

(define-public crate-ittech-0.2.0 (c (n "ittech") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nom") (r "^6.1") (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1s0377d5hlc988g56dyxlry8m00izwbz7q91i4nvwqfh0iygz2ns")))

(define-public crate-ittech-0.3.0 (c (n "ittech") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (o #t) (k 0)))) (h "065w205wd8lcjww7d5mxzyxs4mni1jklg9zaq6a1bp4q86p474lz") (f (quote (("log" "tracing/log"))))))

