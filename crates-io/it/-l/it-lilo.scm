(define-module (crates-io it -l it-lilo) #:use-module (crates-io))

(define-public crate-it-lilo-0.1.0 (c (n "it-lilo") (v "0.1.0") (d (list (d (n "fluence-it-types") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1ysjjvf0g3831wj7icc0vvfj3dxdwfrczqxgzdzz9xh83q7g9k4r")))

(define-public crate-it-lilo-0.2.0 (c (n "it-lilo") (v "0.2.0") (d (list (d (n "fluence-it-types") (r "^0.3.0") (d #t) (k 0)) (d (n "it-memory-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1zacjj08gknppypvdd85blybb7m5cqii29izfja35w496adysya7")))

(define-public crate-it-lilo-0.3.0 (c (n "it-lilo") (v "0.3.0") (d (list (d (n "fluence-it-types") (r "^0.3.0") (d #t) (k 0)) (d (n "it-memory-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0arwz8as5dhbm97pw096sm1z3fqfw4q5f1rj4q8ziybmqan8i07a")))

(define-public crate-it-lilo-0.4.0 (c (n "it-lilo") (v "0.4.0") (d (list (d (n "fluence-it-types") (r "^0.3.0") (d #t) (k 0)) (d (n "it-memory-traits") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1j149rnm62wp7f53vvd9xpr7r9symjn09zmh7563p1qhqvydyfba")))

(define-public crate-it-lilo-0.4.1 (c (n "it-lilo") (v "0.4.1") (d (list (d (n "fluence-it-types") (r "^0.3.0") (d #t) (k 0)) (d (n "it-memory-traits") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1j5yd676yhlspx00xvj2cvr99jvr70842wxnbg8rmpx2dvg6swjr")))

(define-public crate-it-lilo-0.5.0 (c (n "it-lilo") (v "0.5.0") (d (list (d (n "fluence-it-types") (r "^0.4.0") (d #t) (k 0)) (d (n "it-memory-traits") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1h8s97cx1s15pwwwfa4ki5ywaz7zfvlz7kj9p8wsih3jyx31z0c9")))

(define-public crate-it-lilo-0.5.1 (c (n "it-lilo") (v "0.5.1") (d (list (d (n "fluence-it-types") (r "^0.4.1") (d #t) (k 0)) (d (n "it-memory-traits") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "03phkw92ssvbzx13yqmna5wvd3ny765lzi8bkcs4csv9qg196ipi")))

(define-public crate-it-lilo-0.6.0 (c (n "it-lilo") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fluence-it-types") (r "^0.4.1") (d #t) (k 0)) (d (n "it-memory-traits") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1y28lw776zifz810fbshjmr25sgjw1l3hk7m37q1xsqyw5vqw410")))

(define-public crate-it-lilo-0.7.0 (c (n "it-lilo") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "fluence-it-types") (r "^0.4.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "it-memory-traits") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1zlwwf6ijlcsrw5zfdfkryrzlxa6hbams2rx3kqvdf78dh7339qz")))

