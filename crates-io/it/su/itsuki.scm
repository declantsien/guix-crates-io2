(define-module (crates-io it su itsuki) #:use-module (crates-io))

(define-public crate-itsuki-0.1.0 (c (n "itsuki") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0x52x4jplwlqjra36sdim77dz8wdci9mn7z6170vsbb1pdyb0jy2")))

(define-public crate-itsuki-0.1.1 (c (n "itsuki") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "18by7bh41jfc2y8ri0h4casc63kw607qmafphjsab12na3fh9qb2")))

(define-public crate-itsuki-0.1.2 (c (n "itsuki") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "19chslvds5li2fxhclbpgl9sa2vnhfgssmx1cjk1ygsj288n6i93")))

(define-public crate-itsuki-0.2.0 (c (n "itsuki") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "147bwr92skzbvga7qnlkpby3bb4hjj3i02vd5y2cvnqq6v12akk9")))

