(define-module (crates-io it sc itscoho_minigrep) #:use-module (crates-io))

(define-public crate-itscoho_minigrep-0.1.0 (c (n "itscoho_minigrep") (v "0.1.0") (h "02zs4zmrpakvx9mz2kgrqaa1pnz7g26w8mcxcbn8qpgik0zkpwph") (y #t)))

(define-public crate-itscoho_minigrep-0.1.1 (c (n "itscoho_minigrep") (v "0.1.1") (h "0rwl2jk7h2kfbvdcdp3qdj72r6ayi5dbj37p7s4dr9daybc9x7x6")))

