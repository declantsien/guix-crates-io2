(define-module (crates-io it sy itsy) #:use-module (crates-io))

(define-public crate-itsy-0.1.0 (c (n "itsy") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "000rsy07jjj2i376i2fr6hm69yx87vdw178hsn5mzgcx86sy69cm")))

(define-public crate-itsy-0.2.0 (c (n "itsy") (v "0.2.0") (d (list (d (n "nom") (r "^6.1") (f (quote ("alloc"))) (k 0)))) (h "0xnl0rk8yz7bq12hlvlqbn0l6zxrzw1z2xwlrgmc34hw9mqa1gak")))

(define-public crate-itsy-0.3.0 (c (n "itsy") (v "0.3.0") (d (list (d (n "nom") (r "^7.0.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0566f80p4v7kh16rm7jgmzgc3zal2g9qdabf4f3pn8bqs1dn75ks") (f (quote (("default" "compiler") ("debugging") ("compiler" "nom"))))))

(define-public crate-itsy-0.4.0 (c (n "itsy") (v "0.4.0") (d (list (d (n "nom") (r "^7.1.1") (f (quote ("alloc"))) (o #t) (k 0)))) (h "16lv44xh93cwkp1133d3v58kqqzdv7iy8hizsjf5wzdc88fvhxq9") (f (quote (("default" "compiler") ("debugging") ("compiler" "nom"))))))

