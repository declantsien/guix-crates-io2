(define-module (crates-io it yb itybity) #:use-module (crates-io))

(define-public crate-itybity-0.1.0 (c (n "itybity") (v "0.1.0") (d (list (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "15rx97aa4kfidr6n8lydbv9q5xxkgw1crvpsn11lk0lszgrcqfi7") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1.1 (c (n "itybity") (v "0.1.1") (d (list (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "1iw7dpr6ljnkwya7l4mf9cm63kvxp76737zn5plk7gi2f9c87fr3") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1.2 (c (n "itybity") (v "0.1.2") (d (list (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "1ki908lxw3r24q8xzg0fq33c1asfsvxdwfg9r0wbpfshxhj7b8x4") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1.3 (c (n "itybity") (v "0.1.3") (d (list (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "1nmm1vks22icxr23q760n9pzgb612wgg14ysp7pz9bv3j973sii2") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1.4 (c (n "itybity") (v "0.1.4") (d (list (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "0phkavg3q10nsxxh6iyckrangkmj9vi5mw3s8qjfsrzcbv8k0n4m") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1.5 (c (n "itybity") (v "0.1.5") (d (list (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "1qj8zzn0gl2k0gl6kx8namp8r8fvfa4c22pzh20jfq5nikw6376c") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1.6 (c (n "itybity") (v "0.1.6") (d (list (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "1x112mfajljwylzxi7jf1snygbvlnvx7hnn0z1sq4vky0f6wap14") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.2.0 (c (n "itybity") (v "0.2.0") (d (list (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "02891sxladwhrk83yi3whnvrcqqmc9hnil9c4awmk63sgrlsn9gz") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("rayon" "std" "dep:rayon"))))))

(define-public crate-itybity-0.2.1 (c (n "itybity") (v "0.2.1") (d (list (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "0yay2sw8q0nhxc8vkhdl6smmgs29mc1idbqypjcyrm563265ahpj") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("rayon" "std" "dep:rayon"))))))

