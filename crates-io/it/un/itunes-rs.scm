(define-module (crates-io it un itunes-rs) #:use-module (crates-io))

(define-public crate-itunes-rs-0.1.0 (c (n "itunes-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("interface" "implement" "Win32_System_Com" "Win32_System_Ole" "Win32_Foundation"))) (d #t) (k 0)))) (h "0h68d68km4abg37v1xw4vvx5zgnj63dpy63pvrn540c0db61xqwg")))

