(define-module (crates-io l1 #{0n}# l10n_impl) #:use-module (crates-io))

(define-public crate-l10n_impl-0.0.0 (c (n "l10n_impl") (v "0.0.0") (h "0gizg6gdi66853l64v6kkvq81lz7cfj3yq3y1s4lakcjilsvhpzw") (r "1.56")))

(define-public crate-l10n_impl-0.1.0-beta.1 (c (n "l10n_impl") (v "0.1.0-beta.1") (d (list (d (n "l10n_core") (r "=0.1.0-beta.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "066acavkcypnnr1lglndzg11bi6g3hfbvr07vny710pbv09lzn2x") (f (quote (("allow-incomplete")))) (r "1.56")))

(define-public crate-l10n_impl-0.1.0 (c (n "l10n_impl") (v "0.1.0") (d (list (d (n "l10n_core") (r "=0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0zh8ipyhj7bcv4018db39w2jx08rz7p7zbvr6y68f52l0ra6hd0j") (f (quote (("allow-incomplete")))) (y #t) (r "1.61")))

(define-public crate-l10n_impl-0.1.1 (c (n "l10n_impl") (v "0.1.1") (d (list (d (n "l10n_core") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1kmghlw4mdx1w69008lb0v6aljvc5wip9irkgr80snsya1zr75j8") (f (quote (("allow-incomplete")))) (r "1.61")))

