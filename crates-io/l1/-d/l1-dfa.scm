(define-module (crates-io l1 -d l1-dfa) #:use-module (crates-io))

(define-public crate-l1-dfa-0.0.1 (c (n "l1-dfa") (v "0.0.1") (h "0w16n0fpkik4vi6dhmkv7kchnj0qg06vvsfwl2prjzpj18gp39hx")))

(define-public crate-l1-dfa-0.0.2 (c (n "l1-dfa") (v "0.0.2") (h "0s6ghm69cnkvqg5n0vnx8bm28wjpkn9fsb3xiyrj2zp394vyvfvz")))

(define-public crate-l1-dfa-0.0.3 (c (n "l1-dfa") (v "0.0.3") (h "1dzfa1mp18xfz3pxsjb37kafpp0wd7xnp74scd8dbxz1ajrwpdld")))

(define-public crate-l1-dfa-0.0.4 (c (n "l1-dfa") (v "0.0.4") (h "0zkhh1f0rm0inlfpw1h082fg9djyz6sajsxz6mxffid5ww1jda44")))

(define-public crate-l1-dfa-0.0.5 (c (n "l1-dfa") (v "0.0.5") (h "11l83pssr5f07walbgcm9i67jmr1180i7cvbaaxz8sh5y89k5dvc")))

(define-public crate-l1-dfa-0.0.6 (c (n "l1-dfa") (v "0.0.6") (h "1hx7w2g9j4nlcbpchzzcd7yq7qc5k5l72yg91rdq9z1wfdk1fzjz")))

(define-public crate-l1-dfa-0.0.7 (c (n "l1-dfa") (v "0.0.7") (h "06i92h735ggbqid8hy0b5b9p84dwmc4m1jicyf9fz84p8yahilwc")))

(define-public crate-l1-dfa-0.0.8 (c (n "l1-dfa") (v "0.0.8") (h "01g697jnjxf73avr67igzx8rn5fqb3linhfx894qplfy4s2cy5mr")))

(define-public crate-l1-dfa-0.0.9 (c (n "l1-dfa") (v "0.0.9") (h "1pxx2qmj42mrh4f1156q85s1b6wp6dfa2wsc853y1da2lar3djdz")))

(define-public crate-l1-dfa-0.0.10 (c (n "l1-dfa") (v "0.0.10") (d (list (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "05fsn7lnndnzcmfx3m6zhnrhpnly5avvvwmi949wh50aas72qyzz")))

(define-public crate-l1-dfa-0.1.0 (c (n "l1-dfa") (v "0.1.0") (d (list (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "0pr0nqix48ah3r2c7dr5q9p0hcan4k4f0fmkhhzhnfgkac5l90ka")))

(define-public crate-l1-dfa-0.1.1 (c (n "l1-dfa") (v "0.1.1") (d (list (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "03phm89ij651qfr90xifck564sxjh4ricvndyiqh7sl64pp4v436")))

