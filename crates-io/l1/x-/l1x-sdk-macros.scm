(define-module (crates-io l1 x- l1x-sdk-macros) #:use-module (crates-io))

(define-public crate-l1x-sdk-macros-0.2.1 (c (n "l1x-sdk-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "08vx4lrpjm8c2i12i54sh6ir16r963yzfq3j2380gs2xvlq55lpl")))

