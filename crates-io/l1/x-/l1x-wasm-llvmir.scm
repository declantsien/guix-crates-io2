(define-module (crates-io l1 x- l1x-wasm-llvmir) #:use-module (crates-io))

(define-public crate-l1x-wasm-llvmir-0.2.1 (c (n "l1x-wasm-llvmir") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cranelift-wasm") (r "^0.94.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "inkwell") (r "^0.2.0") (f (quote ("llvm15-0"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rccell") (r "^0.1.3") (d #t) (k 0)) (d (n "wasmparser") (r "^0.102.0") (d #t) (k 0)) (d (n "wasmprinter") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmtime") (r "^0.37.0") (d #t) (k 0)) (d (n "wat") (r "^1.0.61") (d #t) (k 0)))) (h "0qbj59say4gywhq8cm16qylwyrc8f5x15f83zmk7018sd3d7w2jv")))

