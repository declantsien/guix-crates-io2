(define-module (crates-io l1 x- l1x-sdk) #:use-module (crates-io))

(define-public crate-l1x-sdk-0.3.1 (c (n "l1x-sdk") (v "0.3.1") (d (list (d (n "borsh") (r "^0.9") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "l1x-sdk-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "l1x-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "macropol") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uint") (r "^0.9.5") (d #t) (k 0)))) (h "06j1s93y0nm0k6lndiim5yrsd18ap4dx21z4330waf9bn66hw1gz")))

