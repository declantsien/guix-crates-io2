(define-module (crates-io kn ea kneasle_ringing_utils) #:use-module (crates-io))

(define-public crate-kneasle_ringing_utils-0.1.0 (c (n "kneasle_ringing_utils") (v "0.1.0") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0bmqc4y1qxjiwdzgmh46kv3mbahz786pbl4f21dqlvvpkrr94ll3")))

(define-public crate-kneasle_ringing_utils-0.1.1 (c (n "kneasle_ringing_utils") (v "0.1.1") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0rsav13j5qh8gkmn0i3zmn9mk7mdfl708zwlhhk3xnpl5p4hjkjj")))

(define-public crate-kneasle_ringing_utils-0.1.2 (c (n "kneasle_ringing_utils") (v "0.1.2") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "1jy3sa78lh3rh75p8qr88f8hbg2r3xzq7k91x4r2rqwdfh3fb94v")))

(define-public crate-kneasle_ringing_utils-0.1.3 (c (n "kneasle_ringing_utils") (v "0.1.3") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "1jpv16y78lbjjbgvswas9s2qykdx8kf7mbqil5ykjvnxyvr9dy46")))

(define-public crate-kneasle_ringing_utils-0.1.4 (c (n "kneasle_ringing_utils") (v "0.1.4") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "16kyympx71k5hr17179nrjc7x1if5mf8a7vavz6i4dn76piyrzgw")))

(define-public crate-kneasle_ringing_utils-0.1.5 (c (n "kneasle_ringing_utils") (v "0.1.5") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0nr318h3swb1948z5n4c7l9pdjp2zrcd1zc36lybjrmhd8rk9y3d")))

(define-public crate-kneasle_ringing_utils-0.1.6 (c (n "kneasle_ringing_utils") (v "0.1.6") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0nr5fnn8n3w5c0nss1dbq2zqmznr7krzxjpzc104yfvx1s2m012v")))

(define-public crate-kneasle_ringing_utils-0.1.7 (c (n "kneasle_ringing_utils") (v "0.1.7") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "067cp2fgpzqsn34lapbwbz5ra8q801drygmnzh7aik3fw67skbnw")))

(define-public crate-kneasle_ringing_utils-0.1.8 (c (n "kneasle_ringing_utils") (v "0.1.8") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0a0lj1cvj4ydf2y2vhjr8pjp3ylzcf2mfx1hc7mvkrfmnaz2sydx")))

(define-public crate-kneasle_ringing_utils-0.1.9 (c (n "kneasle_ringing_utils") (v "0.1.9") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "1f2hisn9zvw67xjpmllms7pfdlrwfvhfrc81vb3yr60ag3x1c602")))

(define-public crate-kneasle_ringing_utils-0.1.10 (c (n "kneasle_ringing_utils") (v "0.1.10") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0nfm3jj37j2ssp84jss2facp3nhra1rwdfw9hmg1kgnxx3yp0mwl")))

(define-public crate-kneasle_ringing_utils-0.1.11 (c (n "kneasle_ringing_utils") (v "0.1.11") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0j82v4fb2dkjgsd9yhlqps2b1kfn8sl92f452qq8p96b4sxaad3z")))

(define-public crate-kneasle_ringing_utils-0.1.12 (c (n "kneasle_ringing_utils") (v "0.1.12") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0hglgzp9ax2hgbcj5q814r8hdi9rl7vyg37vfb281b8py5zdicp4")))

(define-public crate-kneasle_ringing_utils-0.1.13 (c (n "kneasle_ringing_utils") (v "0.1.13") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "1h4f5zzb20mr85bq6b3vv0vqzs43w14b9xdzsmafihvj4sjmignc")))

(define-public crate-kneasle_ringing_utils-0.1.14 (c (n "kneasle_ringing_utils") (v "0.1.14") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "1ff4fdjj85nlwc3rhg9p8y3bg7rhcwnwfbdmrv5x233dc8zicrh1")))

(define-public crate-kneasle_ringing_utils-0.1.15 (c (n "kneasle_ringing_utils") (v "0.1.15") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0xmh8av4di0gzqpz6zrz3y0whzpzimyjk2hydp247fxxwkjpasn6")))

(define-public crate-kneasle_ringing_utils-0.1.17 (c (n "kneasle_ringing_utils") (v "0.1.17") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "1l9adqsjrngffn0z26bxr7cbhw44ciyf39rz5hrs0hgvkgs0cjjz")))

(define-public crate-kneasle_ringing_utils-0.1.18 (c (n "kneasle_ringing_utils") (v "0.1.18") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0ypn1igfnmw4424gxxksrk7r9h4r10kni5yxbvzb5jg9r2jmbiij")))

(define-public crate-kneasle_ringing_utils-0.1.19 (c (n "kneasle_ringing_utils") (v "0.1.19") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0mfa12imdpv3qvf7nypai551hgrr5cg7gqdrg8b0vp21nhxzgq6z")))

(define-public crate-kneasle_ringing_utils-0.1.20 (c (n "kneasle_ringing_utils") (v "0.1.20") (d (list (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0cpfz78x5gadwis7adb64dvzaywyw0zwvgfxy6k2zgima3fpgiim")))

