(define-module (crates-io kn ow known-folders) #:use-module (crates-io))

(define-public crate-known-folders-1.0.0 (c (n "known-folders") (v "1.0.0") (d (list (d (n "version-sync") (r "^0.9.4") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1nmx3djnfns2273iwrz8s23llapsf0lwi5xc7v7skarc6ig6qcz2") (r "1.58.0")))

(define-public crate-known-folders-1.0.1 (c (n "known-folders") (v "1.0.1") (d (list (d (n "version-sync") (r "^0.9.4") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (d #t) (t "cfg(windows)") (k 0)))) (h "110n9kvq5993m0m7csi4nvdkpx1krag4sk23hz71qfy4v4ki8vrb") (r "1.58.0")))

(define-public crate-known-folders-1.1.0 (c (n "known-folders") (v "1.1.0") (d (list (d (n "version-sync") (r "^0.9.4") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1n71r93n1vwnn057pm5jgnqx9a360xp32fvhrg7j77bhya4wg5s3") (r "1.58.0")))

