(define-module (crates-io kn ow know_yaml) #:use-module (crates-io))

(define-public crate-know_yaml-0.0.7 (c (n "know_yaml") (v "0.0.7") (d (list (d (n "know") (r "^0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1044xnhxhv7iwsvz89pz8zf41aiy1kd2ziabpdqx2mbbfvs0c463") (f (quote (("default")))) (r "1.70")))

(define-public crate-know_yaml-0.0.8 (c (n "know_yaml") (v "0.0.8") (d (list (d (n "know") (r "^0.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0lclcsk2fz21w5vlaxi92mszlx95nfrg7k1laqdd0fqppgsjwv2l") (f (quote (("default")))) (r "1.70")))

