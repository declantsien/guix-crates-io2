(define-module (crates-io kn ow know-thy-shell) #:use-module (crates-io))

(define-public crate-know-thy-shell-0.1.0 (c (n "know-thy-shell") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1.0") (d #t) (k 0)))) (h "1n0rkp5x6ja6ycmk5bs6chcxxsn1hmify8vxn8wvz77559j996s5")))

(define-public crate-know-thy-shell-0.1.1 (c (n "know-thy-shell") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1.1") (d #t) (k 0)))) (h "1b20i5ka2vhwprjjwlf660c5zj3gdd0wd1cmaah6qq6yrniscn0r")))

(define-public crate-know-thy-shell-0.1.2 (c (n "know-thy-shell") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1.2") (d #t) (k 0)))) (h "0zjqc13ypf0n9zzq9xd6xq24zhhlzswayrrzvviz31w9rphv856q")))

(define-public crate-know-thy-shell-0.1.3 (c (n "know-thy-shell") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1.3") (d #t) (k 0)))) (h "1i3fjnr2dcawyxsgzgfmw8iaamsn5f67pcn1ymzvj6difmg8pqv9")))

(define-public crate-know-thy-shell-0.1.4 (c (n "know-thy-shell") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1.4") (d #t) (k 0)))) (h "1pdj0qg20gig3z6qblnr4xf55d1q552k5amk3xn0mkvnrdl0zpcm")))

(define-public crate-know-thy-shell-0.1.5 (c (n "know-thy-shell") (v "0.1.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1") (d #t) (k 0)))) (h "07ak3hydm65kirnr70hxrgm6a4r9gzslll42h0j7pcy2l77dpbdr")))

(define-public crate-know-thy-shell-0.1.6 (c (n "know-thy-shell") (v "0.1.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1") (d #t) (k 0)))) (h "1snf89r9720ily7nj9iw6bn3a4z05nhyxcjx3wdxvasx65fzw9mn")))

(define-public crate-know-thy-shell-0.1.7 (c (n "know-thy-shell") (v "0.1.7") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1") (d #t) (k 0)))) (h "0k1kgbglmyxghl902sjr7f3nma112vpvl66r3c86j77fqvi0ksvw")))

(define-public crate-know-thy-shell-0.1.8 (c (n "know-thy-shell") (v "0.1.8") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1") (d #t) (k 0)))) (h "18lzx3vxkimfmsdk7wkc4vzmd7jivnb3di7mshbyy89c2i8w9bpx")))

(define-public crate-know-thy-shell-0.1.9 (c (n "know-thy-shell") (v "0.1.9") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1") (d #t) (k 0)))) (h "15ld9kcjaxhb707ckgwv45m64x8hqi7r48hcjy0jgzg03b3hh3gm")))

(define-public crate-know-thy-shell-0.1.10 (c (n "know-thy-shell") (v "0.1.10") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1") (d #t) (k 0)))) (h "1q3kgf4m6bkdpgindihdq8bzwfr8a2383m1addmmy39fjgw11dsf")))

(define-public crate-know-thy-shell-0.1.12 (c (n "know-thy-shell") (v "0.1.12") (d (list (d (n "arrow") (r "^0.14") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "datafusion") (r "^0.14") (k 0)) (d (n "kts-analyze") (r "^0.1") (d #t) (k 0)) (d (n "kts-etl") (r "^0.1") (d #t) (k 0)))) (h "1f4ynpdpgx9qgmynhkb3qjpg2663794khm34sfmxnzadsajp8gan")))

