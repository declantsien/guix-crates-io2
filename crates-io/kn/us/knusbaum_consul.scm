(define-module (crates-io kn us knusbaum_consul) #:use-module (crates-io))

(define-public crate-knusbaum_consul-0.2.0 (c (n "knusbaum_consul") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1i73rm1avd6mkyp9gxrdbbgsk9ng710c3j49lsvl34ggcz769phf")))

