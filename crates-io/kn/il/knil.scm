(define-module (crates-io kn il knil) #:use-module (crates-io))

(define-public crate-knil-0.1.0 (c (n "knil") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1813v00x926iph4y4yr5b5l42vzvlkjb59bjg09s58axydf67lgx") (f (quote (("timestamp" "chrono") ("file-log") ("envload" "dotenv") ("default" "timestamp" "colors") ("colors" "colored"))))))

(define-public crate-knil-0.1.1 (c (n "knil") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1l939qcai4hwkll9fr1p4sf3jgc281zn25hkd627i1ik6y909df1") (f (quote (("stamps" "chrono") ("default" "colors" "stamps") ("colors" "colored"))))))

(define-public crate-knil-0.1.2 (c (n "knil") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0psnvvnwhwix52l8945pi616vxdjww0ay9v3v1vg23bf4jr0kc4q") (f (quote (("stamps" "chrono") ("default" "colors" "stamps") ("colors" "colored"))))))

(define-public crate-knil-0.1.3 (c (n "knil") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "15vzjsmbfph2ihikvbb3kc9mwnsm3wi4897dmxm6b7fhyaq0cfqz") (f (quote (("stamps" "chrono") ("loadenv") ("default" "colors" "stamps") ("colors" "colored"))))))

