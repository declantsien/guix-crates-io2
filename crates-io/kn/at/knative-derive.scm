(define-module (crates-io kn at knative-derive) #:use-module (crates-io))

(define-public crate-knative-derive-0.1.0 (c (n "knative-derive") (v "0.1.0") (d (list (d (n "knative-conditions") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02y8id4h7h9lvglx34h9hb8lq2cycz5gpy97bxvl6c7vsh5ksn37")))

