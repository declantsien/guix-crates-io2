(define-module (crates-io kn at knative-eventing) #:use-module (crates-io))

(define-public crate-knative-eventing-0.1.0 (c (n "knative-eventing") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.14.0") (f (quote ("v1_22" "schemars"))) (d #t) (k 0)) (d (n "knative") (r "^0.1.0") (d #t) (k 0)) (d (n "kube") (r "^0.70.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (f (quote ("chrono" "url"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0sgkircbijgk3mlqaq4r7d670bxribjdn1wcal89jdqhd70lwl69")))

