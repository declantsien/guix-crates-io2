(define-module (crates-io kn at knativity) #:use-module (crates-io))

(define-public crate-knativity-0.1.0 (c (n "knativity") (v "0.1.0") (h "06bawadq08j63f89nw0snbxcp126164z5fzbd0mkhryarih6k3wn") (y #t)))

(define-public crate-knativity-0.0.1 (c (n "knativity") (v "0.0.1") (h "149v5n9jrgrr3n8vig9cz26mnw0v8w55fpzb39rm4njs0880js7i")))

(define-public crate-knativity-0.0.2 (c (n "knativity") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 2)) (d (n "cloudevents-sdk") (r "^0.7") (f (quote ("http-binding" "hyper" "http-body" "reqwest"))) (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "full"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0h7bzq53b0kgp4g4a01xd1hxp5msdxf4kv4q2a3nmlwfa865zvry")))

(define-public crate-knativity-0.0.3 (c (n "knativity") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "backoff") (r "^0.4") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 2)) (d (n "cloudevents-sdk") (r "^0.7") (f (quote ("http-binding" "hyper" "http-body" "reqwest"))) (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1zd5cmpbg0jqpvclda8lslpzzvp9h3riyq29rf6ygbfnl7ar3n5a")))

