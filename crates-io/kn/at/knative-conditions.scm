(define-module (crates-io kn at knative-conditions) #:use-module (crates-io))

(define-public crate-knative-conditions-0.1.0 (c (n "knative-conditions") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "05hahrsyy5qyrhd0xxxsz6ajm7s7zjddkrgkkmzwmz88niiw2xbz")))

