(define-module (crates-io kn ig knights-tour) #:use-module (crates-io))

(define-public crate-knights-tour-0.0.0 (c (n "knights-tour") (v "0.0.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "svg") (r "^0.13.0") (d #t) (k 0)))) (h "0rzkcrsdbm6dip31cxbz3z4q4yd2r0v1ck0nyh5wdhrf9vlyrmfp") (f (quote (("default"))))))

