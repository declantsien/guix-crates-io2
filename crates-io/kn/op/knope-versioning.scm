(define-module (crates-io kn op knope-versioning) #:use-module (crates-io))

(define-public crate-knope-versioning-0.0.1 (c (n "knope-versioning") (v "0.0.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "miette") (r "^7.2.0") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "relative-path") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "04g5zw2b3hjhjah8bmh75aj92yainbh9kjkkn8qnsabx3a32qs04") (r "1.71.1")))

