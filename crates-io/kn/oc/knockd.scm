(define-module (crates-io kn oc knockd) #:use-module (crates-io))

(define-public crate-knockd-0.1.0 (c (n "knockd") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)))) (h "0s8c5k7d3fwb62hcjv30ph491dwh2radm064q3y8rp5gvcw77q3n")))

