(define-module (crates-io kn oc knock) #:use-module (crates-io))

(define-public crate-knock-0.1.0 (c (n "knock") (v "0.1.0") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "0fkkvsxijj5ikfgwg1njq4dndih8dzcqmi2pvv87kmd0cpd6hyyj")))

(define-public crate-knock-0.1.1 (c (n "knock") (v "0.1.1") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "1i9xk3gk5brz3ydlw15q8wm9bj4r467nbd0wvynhihwnc2f5ckz9")))

(define-public crate-knock-0.1.2 (c (n "knock") (v "0.1.2") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "1fkrc8s1v30ykdhqnllcm7lfhfc9ki7ydav9m7wiszy9i91zlxg0")))

(define-public crate-knock-0.1.3 (c (n "knock") (v "0.1.3") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "087p8scrgbd3ggc1kyx3k956kh2xgzh8gm9ccbg9npzfzxqgrkf8")))

(define-public crate-knock-0.1.4 (c (n "knock") (v "0.1.4") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "03kq4dfl0x189xwn1aq7xpbk1wihiwyyjjd6w8jnaa0v0wv1dkdn")))

(define-public crate-knock-0.1.5 (c (n "knock") (v "0.1.5") (d (list (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "03j3qzzbjikznihn9n3sd7vm0zchljyw4ph9l3zg70yyyhpadj9x") (f (quote (("default"))))))

(define-public crate-knock-0.1.6 (c (n "knock") (v "0.1.6") (d (list (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "10g3pxk0wp99973r2f6qsjf3md3b6vxm8bj3wbk6kawflr29l1i1") (f (quote (("default"))))))

(define-public crate-knock-0.1.7 (c (n "knock") (v "0.1.7") (d (list (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "13i4fih018mhpa88wd2ixvbq2f3slss3l7sj1m1qi7fav4r09xh5") (f (quote (("default" "native-tls"))))))

(define-public crate-knock-0.1.8 (c (n "knock") (v "0.1.8") (d (list (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "0cwwid7jw8q90bmv7w2kiz2q2nk9myc2mlr79llpkw7chchcv3vs") (f (quote (("default" "native-tls"))))))

