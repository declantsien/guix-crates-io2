(define-module (crates-io kn oc knock-knock) #:use-module (crates-io))

(define-public crate-knock-knock-0.1.1 (c (n "knock-knock") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("suggestions"))) (k 0)) (d (n "whois-rust") (r "^1.2.4") (d #t) (k 0)) (d (n "whoisthere") (r "^0.1.2") (d #t) (k 0)))) (h "036qcc7y12arvxik9dgcnkd76gihap17rb7wl0ml6q40sl1g3b3i")))

(define-public crate-knock-knock-0.2.0 (c (n "knock-knock") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("std" "suggestions"))) (k 0)) (d (n "whois-rust") (r "^1.5.0") (d #t) (k 0)) (d (n "whoisthere") (r "^0.1.3") (d #t) (k 0)))) (h "0cn7c741i5l3hkihf5azag3v14sqg296zps8a47gb0a9418nbxwq")))

(define-public crate-knock-knock-0.3.0 (c (n "knock-knock") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("std" "suggestions"))) (k 0)) (d (n "rdap_client") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h9zk5cm5q3snvjkxlh1l0c103gd1mab36f5fdhg3683nq9glgbd")))

