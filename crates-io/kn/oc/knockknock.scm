(define-module (crates-io kn oc knockknock) #:use-module (crates-io))

(define-public crate-knockknock-1.0.0 (c (n "knockknock") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "zpinger") (r "^0.1.0") (d #t) (k 0)))) (h "01qi37r9zqxh0hpyghhq7pr3h9i2sr8j37m225l1gg9k3j61fzcf") (y #t)))

(define-public crate-knockknock-1.0.1 (c (n "knockknock") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "zpinger") (r "^0.1.0") (d #t) (k 0)))) (h "1c2bgxdxsyw4qia3645rrhwk98rmhr30s0lhxzir2fr6k24ml8qa")))

(define-public crate-knockknock-1.0.2 (c (n "knockknock") (v "1.0.2") (d (list (d (n "clap") (r "=3.0.0-beta.5") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "zpinger") (r "^0.1.0") (d #t) (k 0)))) (h "0rnrfx8jhkkdk9qqd39w7hfaa83f3pfdw9m5b62l3anhqgmn8jrc")))

