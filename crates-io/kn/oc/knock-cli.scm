(define-module (crates-io kn oc knock-cli) #:use-module (crates-io))

(define-public crate-knock-cli-0.1.0 (c (n "knock-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)))) (h "17yhqsvgascpigk93786b8nxplm79rjw20ircbfvsg16yfl5cv2q")))

