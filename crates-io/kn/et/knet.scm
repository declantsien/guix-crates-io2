(define-module (crates-io kn et knet) #:use-module (crates-io))

(define-public crate-knet-0.1.0 (c (n "knet") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "derive-knet") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1lrf8lj1lilhc33w5qyb0sn7vav7233padmhncas6q4vlz22i8db")))

(define-public crate-knet-0.2.0 (c (n "knet") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "derive-knet") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "14rrcmbgqckg6rglmcmjwj748w1r4kkjjakmq25qx1fqvvq5hd35")))

(define-public crate-knet-0.3.0 (c (n "knet") (v "0.3.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "derive-knet") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "07lwc5yyxmmbfn7j9v36mrmippc655cn8pm36nmdrqlxxzyylbad")))

(define-public crate-knet-0.3.1 (c (n "knet") (v "0.3.1") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "derive-knet") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0v9snv8ia5593jbxj83nwxh9xl3sgx6zybxxc23js60q72l4j1l8")))

