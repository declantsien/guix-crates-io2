(define-module (crates-io kn et knetrs) #:use-module (crates-io))

(define-public crate-knetrs-0.1.1 (c (n "knetrs") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "ffi-opaque") (r "^2.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vzpigf60d2qnivkj0hc7fmbnqia2x54bv1p53jzhixls76wry79")))

