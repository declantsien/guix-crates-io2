(define-module (crates-io kn et knet-bindings) #:use-module (crates-io))

(define-public crate-knet-bindings-2.0.0-alpha1+1.90.632-b035 (c (n "knet-bindings") (v "2.0.0-alpha1+1.90.632-b035") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2.0") (d #t) (k 0)))) (h "1k07lybfbdps0g9x89z4rm6yyi5h9c3z3ghpdf6mz584any54gfw")))

(define-public crate-knet-bindings-2.0.0-alpha1+1.90.647-0fc83 (c (n "knet-bindings") (v "2.0.0-alpha1+1.90.647-0fc83") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2.0") (d #t) (k 0)))) (h "1330l43skvvabs1m1d08z5qcb4qfcaqgd9lcrkwf46jw5zpd1ncs")))

