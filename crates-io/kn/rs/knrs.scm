(define-module (crates-io kn rs knrs) #:use-module (crates-io))

(define-public crate-knrs-0.1.0 (c (n "knrs") (v "0.1.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)))) (h "13zkmpsdsywmkba3hww4mjgmadh9h9s3rh2ijrz73pai69hjsx46")))

(define-public crate-knrs-0.1.1 (c (n "knrs") (v "0.1.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)))) (h "0p3wvvwdias2aiy0a34wgaxbpjsdcycmyjnx5brl2sbzmlaa8hbv")))

(define-public crate-knrs-0.2.0 (c (n "knrs") (v "0.2.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)))) (h "0k62yamz5r9yrk1imndbvkjcsfbz8sdnjm2gg7cyaxl5rxqjzjhl")))

(define-public crate-knrs-0.2.1 (c (n "knrs") (v "0.2.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)))) (h "0hdv8fwzzwnqclis9vdjbspwjkszq7x6j2j3s32pljkdi23jbpd4")))

