(define-module (crates-io kn ys knyst_macro) #:use-module (crates-io))

(define-public crate-knyst_macro-0.5.0 (c (n "knyst_macro") (v "0.5.0") (d (list (d (n "ident_case") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits" "parsing" "full"))) (d #t) (k 0)))) (h "0yhf2xim2byrnx5gm2riw31pndz4gdwz5xhgi0hsm2zx6mgbpn23")))

