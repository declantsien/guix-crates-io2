(define-module (crates-io kn -c kn-cuda-sys) #:use-module (crates-io))

(define-public crate-kn-cuda-sys-0.2.0 (c (n "kn-cuda-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0sc3fjl7drg5yvhkl7lbwwy243isssb8bip1h6w5dlnmc8w194m4")))

(define-public crate-kn-cuda-sys-0.2.1 (c (n "kn-cuda-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0d8nm1jf77c9cjx99zdkcy3lirxbk6b7ylpi4n23gbw0dxr7ag0l")))

(define-public crate-kn-cuda-sys-0.3.1 (c (n "kn-cuda-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0aahcs718y4gixd2cxb8nbc4nzzrp0agqhc27p5qw8sdwjj5cv4c")))

(define-public crate-kn-cuda-sys-0.4.0 (c (n "kn-cuda-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1vw3glcqq70l3m2ydj6i6filwf51rfibm2wcnyvp8yg68qcrwdc6")))

(define-public crate-kn-cuda-sys-0.4.1 (c (n "kn-cuda-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0p9bqxp9a1xrcd0g680bb1ax382isjckirmki1xaqbxdlxk89184") (f (quote (("docsrs"))))))

(define-public crate-kn-cuda-sys-0.4.2 (c (n "kn-cuda-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1l9sxgk8416m94rfh8r9wcm0dm0mn17ixrnjmbcc3ivlcwp72kn2")))

(define-public crate-kn-cuda-sys-0.4.3 (c (n "kn-cuda-sys") (v "0.4.3") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "05pviyp0sw7pzidsna9574hfrxba7ldxbn5i7w48pqnaz1xk713l")))

(define-public crate-kn-cuda-sys-0.4.4 (c (n "kn-cuda-sys") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "051v98rk2191kzvrd5am13q8lakawzl7p0sdp3xac2x9556blx9m")))

(define-public crate-kn-cuda-sys-0.5.0 (c (n "kn-cuda-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1gv30sxacgy2sn8hqxx1rr19y0n501whbg8gnymcbnxqlg4gyzah")))

(define-public crate-kn-cuda-sys-0.6.0 (c (n "kn-cuda-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "0s95jdv161dcnxhb1wkcyfj7d4m2gzcxb8ak567pvcmv5wmkanv6")))

(define-public crate-kn-cuda-sys-0.6.1 (c (n "kn-cuda-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "16bfdqf5j827p6rgl8rm1003gkffiv9hnlwvjm268y15ymghd4hp")))

(define-public crate-kn-cuda-sys-0.7.0 (c (n "kn-cuda-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "19520mqfmzll2zyh2bv6xrxd30z0ja9sqfnzvg7k4nzb3b3jmrkf")))

(define-public crate-kn-cuda-sys-0.7.1 (c (n "kn-cuda-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "1qiiz28gq6v10qbs8jrg6h4vxv3kgicjmzq82vvi6pl81alpvz18")))

(define-public crate-kn-cuda-sys-0.7.2 (c (n "kn-cuda-sys") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "0b96dv4vdrr59n8frc1fcxcbza7h1pvvcysj7dl1pvwxygpgw93n")))

