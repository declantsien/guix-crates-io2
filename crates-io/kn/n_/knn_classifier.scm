(define-module (crates-io kn n_ knn_classifier) #:use-module (crates-io))

(define-public crate-knn_classifier-0.1.0 (c (n "knn_classifier") (v "0.1.0") (h "0wbzvz5cszcmjjav0dankvvivbqqxsdbkb1hkxwk4h3vi4apbqg1")))

(define-public crate-knn_classifier-0.1.1 (c (n "knn_classifier") (v "0.1.1") (h "1dylfj2bfqr1bfvwx19920frq2cm2qk10bpzmc8vlf2n5mxxfkh1")))

(define-public crate-knn_classifier-0.1.2 (c (n "knn_classifier") (v "0.1.2") (h "1wwa62mc42sy7i2x664ylz8c26w5z83gfl0g51sf7cxkjikaj43r")))

