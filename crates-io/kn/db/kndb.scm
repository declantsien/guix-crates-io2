(define-module (crates-io kn db kndb) #:use-module (crates-io))

(define-public crate-kndb-0.0.0 (c (n "kndb") (v "0.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rustyline") (r "^8.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1yk3lcr26r7n9afick6dipkvx5fnr40gbxlx5pvq7qn8rak88j5j")))

