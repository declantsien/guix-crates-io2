(define-module (crates-io kn i- kni-rs) #:use-module (crates-io))

(define-public crate-kni-rs-0.1.0 (c (n "kni-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "hyper") (r "^0.14.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0x77l88w01hq5x2i8cqbjf3shfhhdcc3va8d2mr5lna7ylmd3vcg")))

