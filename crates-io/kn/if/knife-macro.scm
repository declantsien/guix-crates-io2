(define-module (crates-io kn if knife-macro) #:use-module (crates-io))

(define-public crate-knife-macro-0.1.2 (c (n "knife-macro") (v "0.1.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0c8i2ygdsngpmil2apj4nddx91blir5203a9m3xnil9g992sg6c6") (f (quote (("default")))) (y #t)))

(define-public crate-knife-macro-0.1.3 (c (n "knife-macro") (v "0.1.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0xmn83ijm3b950yn7skq336y5lmbmvx9dncqi1yzp3bb1kwxyyhq") (f (quote (("default")))) (y #t)))

(define-public crate-knife-macro-0.1.4 (c (n "knife-macro") (v "0.1.4") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0f9lglkz37cdxhapjpai7gl4sqyavirycxxym5n01fws5h2c42ff") (f (quote (("default")))) (y #t)))

(define-public crate-knife-macro-0.1.5 (c (n "knife-macro") (v "0.1.5") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "19cjwz06yi6z30w98imqcz6nw2lw4n2c6zd3q2i4rdz4vs6gpl2s") (f (quote (("default")))) (y #t)))

(define-public crate-knife-macro-0.1.6 (c (n "knife-macro") (v "0.1.6") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0kp8cphjcwhphyvwbgd5aha18y0f6678jyrcdcyilvh2cb525g3g") (f (quote (("default")))) (y #t)))

(define-public crate-knife-macro-0.1.7 (c (n "knife-macro") (v "0.1.7") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0fqxnp3dzav5cj998895hmgk0i7sbypqj4p6fvphgajcvphcfr2a") (f (quote (("default")))) (y #t)))

