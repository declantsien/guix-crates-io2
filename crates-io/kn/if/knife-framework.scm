(define-module (crates-io kn if knife-framework) #:use-module (crates-io))

(define-public crate-knife-framework-0.1.2 (c (n "knife-framework") (v "0.1.2") (d (list (d (n "knife-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "05l8is4fqzz9010zi57zsqx4baf62gzl3d8sd8261rpi4w98cfmw") (f (quote (("default")))) (y #t)))

(define-public crate-knife-framework-0.1.3 (c (n "knife-framework") (v "0.1.3") (d (list (d (n "knife-macro") (r "^0.1.3") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1wd7dnh99kb3b7abl6m6jdsi6ch5ldw4jgw1qbmdy81zkk4sp93a") (f (quote (("default")))) (y #t)))

(define-public crate-knife-framework-0.1.4 (c (n "knife-framework") (v "0.1.4") (d (list (d (n "knife-macro") (r "^0.1.4") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "13j90pnjp0zh7f31w47jjlfzyarfn2w3a4mbbmblgiz49hqmkkzj") (f (quote (("default")))) (y #t)))

(define-public crate-knife-framework-0.1.5 (c (n "knife-framework") (v "0.1.5") (d (list (d (n "knife-macro") (r "^0.1.5") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0b5lmi6z9dlky3wyfa9qdynb934gscjjrvj1706znjsv7qaxiq5g") (f (quote (("default")))) (y #t)))

(define-public crate-knife-framework-0.1.6 (c (n "knife-framework") (v "0.1.6") (d (list (d (n "knife-macro") (r "^0.1.6") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0pkgmz3022ghn3jijz40ijbqrzmafp8lba0nyj1zll9zn2bjr2yv") (f (quote (("default")))) (y #t)))

(define-public crate-knife-framework-0.1.7 (c (n "knife-framework") (v "0.1.7") (d (list (d (n "knife-macro") (r "^0.1.7") (d #t) (k 0)) (d (n "knife-util") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0cs2sxncsrqr8rk450ai2k17klh054p8k8ard0sh8r7a5in5sfs0") (f (quote (("default")))) (y #t)))

