(define-module (crates-io kn if knife) #:use-module (crates-io))

(define-public crate-knife-0.0.0 (c (n "knife") (v "0.0.0") (h "13ch3kq7jrc7pmvrhgkdikzgs5sj0lbj2agckqld5w1qlp4wpr63") (y #t)))

(define-public crate-knife-0.0.1 (c (n "knife") (v "0.0.1") (h "089zh6n4ds9lk14p3g0p1nlpfjq2f1gwbg527al2666vagah0qni") (y #t)))

(define-public crate-knife-0.2.0 (c (n "knife") (v "0.2.0") (h "1995pam5wmh5xazq10rd1cwgkjfnlkpr58a003a760l788af960g") (y #t)))

(define-public crate-knife-0.3.0 (c (n "knife") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0nqw0h0y4cwx5gzz9mi71i0sgsyiq0hrwqkyr6svh86jizdkx3f6")))

