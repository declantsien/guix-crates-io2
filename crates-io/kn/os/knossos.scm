(define-module (crates-io kn os knossos) #:use-module (crates-io))

(define-public crate-knossos-0.1.0 (c (n "knossos") (v "0.1.0") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0xmh9651bk81qas8a8i1gc24ayrry67k58apa4mnc30h59xw098d") (y #t)))

(define-public crate-knossos-0.1.1 (c (n "knossos") (v "0.1.1") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0sl938brwdv6vww7n394rsk1z2dh1xjggzgjl8nq3fy6ib0sl1dg") (y #t)))

(define-public crate-knossos-0.1.2 (c (n "knossos") (v "0.1.2") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1a458js2cs55kx40xrqpv8pps6xlr9jk1dnasinvq0kwzffn0x40")))

(define-public crate-knossos-0.2.0 (c (n "knossos") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1zhgnq6f0kp3z09i4fvi4zpvxhq1vvxjjypqird5w2x6pj75mvfw")))

(define-public crate-knossos-0.3.0 (c (n "knossos") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1k9h5gg8qqgf19xc2xlx17b77lbfd5ljh957v7xgidnr0aaij5sr")))

(define-public crate-knossos-0.4.0 (c (n "knossos") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0f8nizhslhqqv0l7z7rhypkf2bbhvyfvlpm4xbpz63086izvl8fj")))

