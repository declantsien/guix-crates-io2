(define-module (crates-io kn x_ knx_examples_rs) #:use-module (crates-io))

(define-public crate-knx_examples_rs-0.0.1 (c (n "knx_examples_rs") (v "0.0.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "knx_ets_rs") (r "^0.0.1") (d #t) (k 0)) (d (n "knx_rs") (r "^0.0.1") (d #t) (k 0)) (d (n "log4rs") (r "^0.4.1") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "0bl9gzvg5ackjrgrcm7pya217drw4bh0c434y874gngzbsqwq64f")))

(define-public crate-knx_examples_rs-0.0.2 (c (n "knx_examples_rs") (v "0.0.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "knx_ets_rs") (r "^0.0.2") (d #t) (k 0)) (d (n "knx_rs") (r "^0.0.2") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "0x72g7wrsb76vhqff8kr4mxcr1qpssx4yddpva2ili29hxpk5zx6")))

(define-public crate-knx_examples_rs-0.0.3 (c (n "knx_examples_rs") (v "0.0.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "knx_ets_rs") (r "^0.0.2") (d #t) (k 0)) (d (n "knx_rs") (r "^0.0.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.1") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "18dv9xv95sqc9ywjspf8s0yh15lgxhgpiq3x9rlbzl2qc28k3vhq")))

(define-public crate-knx_examples_rs-0.0.4 (c (n "knx_examples_rs") (v "0.0.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "knx_ets_rs") (r "^0.0.4") (d #t) (k 0)) (d (n "knx_rs") (r "^0.0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.1") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "0zw8y0vnhhv5yl0kfm79181b19yxyn57kaqmjp2cv7gwg2j1z82f")))

(define-public crate-knx_examples_rs-0.0.5 (c (n "knx_examples_rs") (v "0.0.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "knx_ets_rs") (r "^0.0.5") (d #t) (k 0)) (d (n "knx_rs") (r "^0.0.5") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.1") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "0d4mgq8c08c1q1irf56zbzj9lh0hlkm5q0l15b4hrbfmaj46q3k3")))

(define-public crate-knx_examples_rs-0.0.6 (c (n "knx_examples_rs") (v "0.0.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "knx_ets_rs") (r "^0.0.6") (d #t) (k 0)) (d (n "knx_rs") (r "^0.0.6") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "05shyx0ypmy98i02hnghg2sbn1xfgj1pw91qkkjsw2dmrbqby61v")))

