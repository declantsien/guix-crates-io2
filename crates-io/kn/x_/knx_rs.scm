(define-module (crates-io kn x_ knx_rs) #:use-module (crates-io))

(define-public crate-knx_rs-0.0.1 (c (n "knx_rs") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.2.2") (k 0)) (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.43") (k 0)))) (h "057sym1xxfxf9aywpz8cn5kb5p6d6yy6md377s0ccwhqpjkd958n")))

(define-public crate-knx_rs-0.0.2 (c (n "knx_rs") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.2.2") (k 0)) (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (k 0)))) (h "1i3x77k6azzbxzp65ycirlczjc8lfx5l2ffsx11wnpdlhcps8xin")))

(define-public crate-knx_rs-0.0.3 (c (n "knx_rs") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.2.7") (k 0)) (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (k 0)) (d (n "num-traits") (r "^0.2.6") (k 0)))) (h "1hmn99r8dd1kdbl6yqa90an6d6vg2hf83fdvk6kk6pr0qhsjaml6")))

(define-public crate-knx_rs-0.0.4 (c (n "knx_rs") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.2.7") (k 0)) (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (k 0)) (d (n "num-traits") (r "^0.2.6") (k 0)))) (h "06hl3s2c3n7hvj6schn3d3x3gl2w5fpfxqac2pzm8bnvg3skq6qa")))

(define-public crate-knx_rs-0.0.5 (c (n "knx_rs") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^4.2.0") (k 0)) (d (n "num-traits") (r "^0.2.6") (k 0)))) (h "1xy0s3qzsivm3zdmy2zrldcf74wl2hyi5wnrdrhh3a7p47y08zcn")))

(define-public crate-knx_rs-0.0.6 (c (n "knx_rs") (v "0.0.6") (d (list (d (n "byteorder") (r "^1.3.2") (k 0)) (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (k 0)) (d (n "num-traits") (r "^0.2.8") (k 0)))) (h "1gg88f986r5h5g6ngfp1kf3vkiy3spfp68dmrdcnfvc4433c25bz")))

