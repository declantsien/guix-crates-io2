(define-module (crates-io kn x- knx-rust) #:use-module (crates-io))

(define-public crate-knx-rust-0.0.1 (c (n "knx-rust") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.3.2") (k 0)) (d (n "mio") (r "^0.8.11") (f (quote ("net" "os-poll" "os-ext"))) (d #t) (k 2)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "net" "rt" "macros" "sync"))) (d #t) (k 2)))) (h "0563bz0rvbngnz14lfdi2fx489kvpg85s1142i8dfmbk2ib0d2xa")))

