(define-module (crates-io kn it knitting_chart) #:use-module (crates-io))

(define-public crate-knitting_chart-0.1.0 (c (n "knitting_chart") (v "0.1.0") (d (list (d (n "knitting_parse") (r "^0.2.1") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 0)))) (h "0xk2wv84mr3qgcw5z6x07w70x1dcx79jsxii60799kxpps0h3wig")))

(define-public crate-knitting_chart-0.2.0 (c (n "knitting_chart") (v "0.2.0") (d (list (d (n "knitting_parse") (r "^0.3.0") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)))) (h "08wad1mvb7fq15xzl4mm13jm6pf8nykh0n0hhm79f12v4srn44r6")))

(define-public crate-knitting_chart-0.2.1 (c (n "knitting_chart") (v "0.2.1") (d (list (d (n "knitting_parse") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)))) (h "0sk4zvd55l5agpcx2irjx5cvycvd9qmi2lxg3fwx241nfgysdhyr")))

(define-public crate-knitting_chart-0.2.2 (c (n "knitting_chart") (v "0.2.2") (d (list (d (n "knitting_parse") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "1crqw50n4rbdkcqr0v1zdmhmqs22r30sh7gzzh2hvsdvildp9n4j")))

