(define-module (crates-io kn it knit-text) #:use-module (crates-io))

(define-public crate-knit-text-0.1.0 (c (n "knit-text") (v "0.1.0") (h "014bjswj2yh0hpiai8s3vl6jhiciira6sij1j5243w4lyn7w54i4") (y #t)))

(define-public crate-knit-text-0.1.1 (c (n "knit-text") (v "0.1.1") (h "1751gg50wa096dhb4i32kf4wac7846hy3ny5n4zhbcarhyqzjm98")))

