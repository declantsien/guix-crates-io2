(define-module (crates-io kn it knit) #:use-module (crates-io))

(define-public crate-knit-0.1.0 (c (n "knit") (v "0.1.0") (h "00r8bb7fkl4n7il29f2q1c4v8dar941w00fr9hgwz7abkpjqmjy6") (y #t)))

(define-public crate-knit-0.1.1 (c (n "knit") (v "0.1.1") (h "0h8lqyipr3vad6ibixcbi37h4lib0jqm74kv7255k1aq568zbxpb")))

