(define-module (crates-io kn it knitro_rs) #:use-module (crates-io))

(define-public crate-knitro_rs-0.1.0 (c (n "knitro_rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)))) (h "1rp2lc03w7x7w6wnpngcl4qnld2i8drylsgv88lblzsvjg61250d") (f (quote (("knitro_13") ("knitro_12"))))))

(define-public crate-knitro_rs-0.2.0 (c (n "knitro_rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "1mcnfamvvfwcqhrvzry47imdkhjv2m46kb5mzmdvyllqnh7mhflv") (f (quote (("knitro_13") ("knitro_12"))))))

