(define-module (crates-io kn it knitting_parse) #:use-module (crates-io))

(define-public crate-knitting_parse-0.1.2 (c (n "knitting_parse") (v "0.1.2") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "046yddiw9b4q8iv08znhnrbb2pc3lp6hz9h0x634gqdd3q5mfr9j")))

(define-public crate-knitting_parse-0.1.3 (c (n "knitting_parse") (v "0.1.3") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "05d9sak9knr3zbchwbgf62b8x7mv9l6dvjma42kgjjba5dqnn8d3")))

(define-public crate-knitting_parse-0.1.4 (c (n "knitting_parse") (v "0.1.4") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "006ibz1pgxx404y6d419vw3jm7x6k03yp64c40dhw6b2mj9w9sih")))

(define-public crate-knitting_parse-0.2.0 (c (n "knitting_parse") (v "0.2.0") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)))) (h "05200yk6pgchmaxm84ynca64nqyihnqbz6yli4c4g6d2fr3xlzl0")))

(define-public crate-knitting_parse-0.2.1 (c (n "knitting_parse") (v "0.2.1") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)))) (h "13avypln6zbqddxg2z3g7x1v7sdyr1fxyzkqnw0nn2xaj68qcvxs")))

(define-public crate-knitting_parse-0.3.0 (c (n "knitting_parse") (v "0.3.0") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)))) (h "1vaiq148vj8ypfllkqbsgk373zz08rdw915yvk4sz5d25c3dmhfk")))

(define-public crate-knitting_parse-0.3.1 (c (n "knitting_parse") (v "0.3.1") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "0biaj9l4r1vhhwfv4g8ba0i5g3bgls54vkpw1i2pbbsmzdk0k8b6")))

(define-public crate-knitting_parse-0.3.2 (c (n "knitting_parse") (v "0.3.2") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1d2b9nv0dyig1ay2mrayrq53g3ahwcvvixbfzs9bzi3jgr2hip61")))

