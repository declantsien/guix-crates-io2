(define-module (crates-io kn ap knaptime) #:use-module (crates-io))

(define-public crate-knaptime-0.0.1 (c (n "knaptime") (v "0.0.1") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10r43ia7645xc4218vgf5x46dyvn8vzqlgfg1lzfn8bwzvq0azyk")))

(define-public crate-knaptime-0.0.2 (c (n "knaptime") (v "0.0.2") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03hrhljg4vxg4f83hpx6yiwvwiylg5bs5j9rhbnv838jrpl60wk8")))

(define-public crate-knaptime-0.0.3 (c (n "knaptime") (v "0.0.3") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12c8ip3c5sa8a4z9v0xj7zili10wihgkywm2r5nsar2xqqwm9yi9")))

(define-public crate-knaptime-0.0.4 (c (n "knaptime") (v "0.0.4") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0c4pbdnwa557hvvs09kbva9mlw68qcqmdhv4cj78vmghslbyldzw")))

