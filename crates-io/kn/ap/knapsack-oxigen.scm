(define-module (crates-io kn ap knapsack-oxigen) #:use-module (crates-io))

(define-public crate-knapsack-oxigen-1.0.0 (c (n "knapsack-oxigen") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "oxigen") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "08ci2hs44jrspdhb46li6z05v944r4nvds44p24x27w2q2h4lz3d")))

