(define-module (crates-io kn ub knube) #:use-module (crates-io))

(define-public crate-knube-0.1.0 (c (n "knube") (v "0.1.0") (h "11856avqmd6w9p3ajqdm6r7q8lysxih6pcxq09f5gagpwmn4f1m9")))

(define-public crate-knube-0.2.0 (c (n "knube") (v "0.2.0") (h "1kh36xmvzl74z3h04w46xkzzm8mxrqcpmllz2ghb8pf1lal4lpqw")))

