(define-module (crates-io kn ar knarkzel) #:use-module (crates-io))

(define-public crate-knarkzel-0.1.0 (c (n "knarkzel") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.4") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (d #t) (k 0)))) (h "08p7hnhydj4hzgv1589g9gksz6xm5kjjzn0rvcnf5s9d2kxnk154")))

(define-public crate-knarkzel-0.2.0 (c (n "knarkzel") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2.2") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.4") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (f (quote ("tls"))) (k 0)))) (h "0a4laqnifjnfgh3msr0kzpq6dxx2n7qkgpsrzidaf5dvg099knds")))

(define-public crate-knarkzel-0.3.0 (c (n "knarkzel") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.4") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (f (quote ("tls"))) (k 0)))) (h "02qyfw28rmz08xf7hj6gssqvsg3sh0g1072yqhfrq6c0rz4c9hz9")))

(define-public crate-knarkzel-0.4.0 (c (n "knarkzel") (v "0.4.0") (d (list (d (n "getrandom") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.4") (d #t) (k 0)))) (h "0rvphhb08qgyn087bghb3svc0xdbl683wjdq9yi5m2gf3sjr5c5h")))

(define-public crate-knarkzel-0.4.1 (c (n "knarkzel") (v "0.4.1") (d (list (d (n "getrandom") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.4") (d #t) (k 0)))) (h "1iy46ly8pf4354w1lwsrkyjwdj94d77027cqdqizdrqj4m0y5nl8")))

(define-public crate-knarkzel-0.5.0 (c (n "knarkzel") (v "0.5.0") (d (list (d (n "getrandom") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.4") (d #t) (k 0)))) (h "07d33jkp8isdjkids990fsx0k47p6k3831ckl7wq6j63sccs7qxy")))

(define-public crate-knarkzel-0.6.0 (c (n "knarkzel") (v "0.6.0") (d (list (d (n "getrandom") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.4") (d #t) (k 0)))) (h "1ysb52r09ai3y5k0wsipghsla88s4zgj1jxx252id8kzxgdc1s7h")))

