(define-module (crates-io cl up cluproccmdline) #:use-module (crates-io))

(define-public crate-cluproccmdline-0.1.1 (c (n "cluproccmdline") (v "0.1.1") (h "0q75qzzzfm4hhn7ggsinhyzvl17cdz289vfacj681sfi1pazh5sb")))

(define-public crate-cluproccmdline-0.1.2 (c (n "cluproccmdline") (v "0.1.2") (h "12m3z4dbvhh5l83kg81dmzcaxfl499378kanrv402b9kf6gz13hp")))

(define-public crate-cluproccmdline-0.1.3 (c (n "cluproccmdline") (v "0.1.3") (h "17nxjm9kkcdkqx89adi9mylpa6gxqcvjqyi7521h1xg37c37vp8f")))

(define-public crate-cluproccmdline-0.1.4 (c (n "cluproccmdline") (v "0.1.4") (h "15v0rsc0h1zy6kjfvnnkw6z86mqcbxpw7gk8v12q7h2r40wrna16")))

(define-public crate-cluproccmdline-0.1.5 (c (n "cluproccmdline") (v "0.1.5") (h "1gl935anmh2mkild2a6603rbvxjiv65nz9al9fl84k54g7j24m3g")))

