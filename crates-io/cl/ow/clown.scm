(define-module (crates-io cl ow clown) #:use-module (crates-io))

(define-public crate-clown-0.1.0 (c (n "clown") (v "0.1.0") (h "03shrzbj4sc68xwq3fqz61wkrvhkyg57hli660d5gh2pqzmf6rxv")))

(define-public crate-clown-1.0.0 (c (n "clown") (v "1.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("visit-mut" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0jdwig3p3g8ipdzq6dmap6kq6qd1n7fh98rjrf5fxl4ib5x52wz6")))

(define-public crate-clown-1.0.1 (c (n "clown") (v "1.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("visit-mut" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1fazg992mysf19p1zr8fif6d8rdcbij4qcs4gpk91jhjq998i4sq")))

(define-public crate-clown-1.0.2 (c (n "clown") (v "1.0.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("visit-mut" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0aixwg44hw0r7gqybwdgljds2b0rjy77xh41n2515x6y3w59cqvx")))

(define-public crate-clown-1.0.3 (c (n "clown") (v "1.0.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("visit-mut" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1bwlwqkqpyxmp5p38baxa1av3n4clq84dp65b50saq70i8crqj1h")))

(define-public crate-clown-1.1.0 (c (n "clown") (v "1.1.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("visit-mut" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "08wq2b445wkh2askvynwcbnvv4kr5jwamb79rpwl02b1s435mdri")))

