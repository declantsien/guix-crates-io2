(define-module (crates-io cl un cluna) #:use-module (crates-io))

(define-public crate-cluna-1.1.0 (c (n "cluna") (v "1.1.0") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "065lj6d2x34j6l5zdcj640z0r69bn453rc657d71j17vxqccbdx1")))

