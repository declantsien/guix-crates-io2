(define-module (crates-io cl ia clia-local-offset) #:use-module (crates-io))

(define-public crate-clia-local-offset-0.1.0 (c (n "clia-local-offset") (v "0.1.0") (d (list (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "tz-rs") (r "^0.6") (d #t) (k 0)))) (h "13c06njnp9jw5wqsjrb5d1zk4ims22x720qa4s0jx72af0ayp5w7")))

