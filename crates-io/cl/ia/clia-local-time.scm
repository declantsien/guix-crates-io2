(define-module (crates-io cl ia clia-local-time) #:use-module (crates-io))

(define-public crate-clia-local-time-0.1.0 (c (n "clia-local-time") (v "0.1.0") (d (list (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt" "std" "time"))) (d #t) (k 0)))) (h "0vg1kj87m16jkgl88qajirr55njarhhbafrhjkqlnardp4air5qd")))

(define-public crate-clia-local-time-0.2.0 (c (n "clia-local-time") (v "0.2.0") (d (list (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt" "std" "time"))) (d #t) (k 0)))) (h "1jq8khjphfmmyr0c4knlralyb43fcpwb8rk04dm09q6a5np1bcs1")))

(define-public crate-clia-local-time-0.2.1 (c (n "clia-local-time") (v "0.2.1") (d (list (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt" "std" "time"))) (d #t) (k 0)))) (h "0x87b61fmgi9hd95ghdkrpbrj9pafsqvb1byc8pr3z6lv8pfyi7l")))

