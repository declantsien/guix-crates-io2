(define-module (crates-io cl ia clia) #:use-module (crates-io))

(define-public crate-clia-0.1.0 (c (n "clia") (v "0.1.0") (h "1a9fr6c8crpb9ln6dlvwlkqmny9y30hhsaybpx7qdhri58l55z00") (y #t)))

(define-public crate-clia-0.1.1 (c (n "clia") (v "0.1.1") (h "0b651h0azzvfqfgg1ddf6fjqh61g0fx18xwr3fhar6ca0sxcxclp") (y #t)))

(define-public crate-clia-0.1.2 (c (n "clia") (v "0.1.2") (h "1ibjhqanhv6h03zwqphm701c8rx23hqp82slhyd4vididwlwvnyf")))

(define-public crate-clia-0.1.3 (c (n "clia") (v "0.1.3") (h "1rkhhfbhk9qqw12wrqxl8rlibsn1gpklxlhqa7zmjln6w672pj6j")))

