(define-module (crates-io cl ia clia-rustorm-codegen) #:use-module (crates-io))

(define-public crate-clia-rustorm-codegen-0.18.0 (c (n "clia-rustorm-codegen") (v "0.18.0") (d (list (d (n "clia-rustorm-dao") (r "^0.18.0") (d #t) (k 0)) (d (n "find-crate") (r "^0.6.3") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "1k7wr4kdhl9a6szc3nahbknd24aph05ippyfqbjs847nigd3j2lc")))

