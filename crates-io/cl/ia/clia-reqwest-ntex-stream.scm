(define-module (crates-io cl ia clia-reqwest-ntex-stream) #:use-module (crates-io))

(define-public crate-clia-reqwest-ntex-stream-1.0.0 (c (n "clia-reqwest-ntex-stream") (v "1.0.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "reqwest") (r "^0") (d #t) (k 0)))) (h "1byw1hcbd29jp59g8m9bia05z8809alf0j4wi84b0xs6xms6w701") (y #t)))

(define-public crate-clia-reqwest-ntex-stream-1.0.1 (c (n "clia-reqwest-ntex-stream") (v "1.0.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "reqwest") (r "^0") (d #t) (k 0)))) (h "180givpm6dxmd0lwp65gz7pldmzhc1i7ll8fh1mlq1g9kgdiy00y")))

