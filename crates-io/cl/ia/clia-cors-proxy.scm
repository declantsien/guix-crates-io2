(define-module (crates-io cl ia clia-cors-proxy) #:use-module (crates-io))

(define-public crate-clia-cors-proxy-0.1.0 (c (n "clia-cors-proxy") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0.6.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1myy9msgyw93ma3sfbr4rxgax1f2pyh1w4s4wjwfhzh7yl0fyh6g")))

(define-public crate-clia-cors-proxy-0.1.1 (c (n "clia-cors-proxy") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clia-tracing-config") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0.6") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0xsisjj8w9bgrw14vhbpx75yv7hbanvrsgjn5c4c8mng43l1x45z")))

(define-public crate-clia-cors-proxy-0.2.0 (c (n "clia-cors-proxy") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clia-tracing-config") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0.6") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "189acswimybg0ljbwgir98pnji5w7q5yyk6gd0yax01ra8s8rsy1")))

