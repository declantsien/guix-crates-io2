(define-module (crates-io cl ia clia-url-qs) #:use-module (crates-io))

(define-public crate-clia-url-qs-0.1.0 (c (n "clia-url-qs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)))) (h "16w3dnnz20227jly8l9a268rnvbx5mdv2jbr4z3qrk019k1wmqxc")))

(define-public crate-clia-url-qs-0.1.1 (c (n "clia-url-qs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)))) (h "09aw6m8qczlmp2ydr1j3r86647ysms77ram76wdl0qlsi3wmia3i")))

