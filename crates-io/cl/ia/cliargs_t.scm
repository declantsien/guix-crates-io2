(define-module (crates-io cl ia cliargs_t) #:use-module (crates-io))

(define-public crate-cliargs_t-0.1.0 (c (n "cliargs_t") (v "0.1.0") (d (list (d (n "contracts") (r "^0.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1sw21mcxkhcvxv569zbl7dysvikm52gn00dyrr35v5v4b0c0sz5h")))

