(define-module (crates-io cl ia clia-nacos-api) #:use-module (crates-io))

(define-public crate-clia-nacos-api-0.2.2 (c (n "clia-nacos-api") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nacos-api_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p9rclhldj1jschzrq0ivq67vibj44m9lrbr58q5sqdz519f8l3q")))

