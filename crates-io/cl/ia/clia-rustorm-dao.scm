(define-module (crates-io cl ia clia-rustorm-dao) #:use-module (crates-io))

(define-public crate-clia-rustorm-dao-0.18.0 (c (n "clia-rustorm-dao") (v "0.18.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bigdecimal") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "geo-types") (r "^0.7.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.3") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "18x805y4kgpq9n1da3jq1g69w3m3qqi2mq2229wnhyvyavynzdvz")))

