(define-module (crates-io cl ia clia-ntex-cors-mod) #:use-module (crates-io))

(define-public crate-clia-ntex-cors-mod-0.2.0 (c (n "clia-ntex-cors-mod") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0.5.14") (d #t) (k 0)) (d (n "ntex") (r "^0.5") (f (quote ("tokio"))) (d #t) (k 2)))) (h "0ag01rxdwbjqqpfn65pcnjlwn98bn2mgc5sh9awwhv9jvv1jknky")))

(define-public crate-clia-ntex-cors-mod-0.3.0 (c (n "clia-ntex-cors-mod") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0.6.5") (d #t) (k 0)) (d (n "ntex") (r "^0.6.5") (f (quote ("tokio"))) (d #t) (k 2)))) (h "09zjj00a5q2djb2zdd3ngh4824crxi106cl3yr2i5r3yk05x1njp")))

(define-public crate-clia-ntex-cors-mod-0.4.0 (c (n "clia-ntex-cors-mod") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0.7.0") (d #t) (k 0)) (d (n "ntex") (r "^0.7.0") (f (quote ("tokio"))) (d #t) (k 2)))) (h "09i3aa44ax7iakrvk7lj9xq7r6ywlrgcbkgm1ry9mx4xr3ws3yhx")))

