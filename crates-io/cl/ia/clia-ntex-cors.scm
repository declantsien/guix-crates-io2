(define-module (crates-io cl ia clia-ntex-cors) #:use-module (crates-io))

(define-public crate-clia-ntex-cors-0.2.0 (c (n "clia-ntex-cors") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0.5.14") (d #t) (k 0)) (d (n "ntex") (r "^0.5") (f (quote ("tokio"))) (d #t) (k 2)))) (h "1l5zsfzwz5213smfhrrg78qgkk3yv8zc0sh3fb6b82alymadfbwd")))

(define-public crate-clia-ntex-cors-0.3.0 (c (n "clia-ntex-cors") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0.6.0-beta.0") (d #t) (k 0)) (d (n "ntex") (r "^0.6.0-beta.0") (f (quote ("tokio"))) (d #t) (k 2)))) (h "1552ak945lbzxwsmgq8m83fm127hf8qya1w3myjq6154v18rv66c")))

(define-public crate-clia-ntex-cors-0.4.0 (c (n "clia-ntex-cors") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ntex") (r "^0.7.0") (d #t) (k 0)) (d (n "ntex") (r "^0.7.0") (f (quote ("tokio"))) (d #t) (k 2)))) (h "0m889w9539xrbfcrvmlawh2bg36f9jv78jv92asbv5fhy0bqh728")))

