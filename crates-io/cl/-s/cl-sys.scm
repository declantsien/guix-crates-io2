(define-module (crates-io cl -s cl-sys) #:use-module (crates-io))

(define-public crate-cl-sys-0.1.0 (c (n "cl-sys") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0iam1a3cwv8ql0hv2fn89vyz3pdfn6myxshpq9wmdn6f36abwpmp")))

(define-public crate-cl-sys-0.2.0 (c (n "cl-sys") (v "0.2.0") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kdnryxs8z25kzw5p2xwa39d2k7m3yn27iwircrsqq6jagr3ngcs")))

(define-public crate-cl-sys-0.2.1 (c (n "cl-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ahlgbm430xy22m8dgr8rvnmvj9aya0fnc289lgqq09csvi8fzyy")))

(define-public crate-cl-sys-0.2.2 (c (n "cl-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zmjw625wdp8hxb7v31bvjwmrnr6x6mjc3c2hcrvlfyv0r8kf1d0") (f (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("default" "opencl_version_1_1" "opencl_version_1_2")))) (y #t)))

(define-public crate-cl-sys-0.3.0 (c (n "cl-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05gsbx6szikmmq7l87plsllk1v7jxwlfd50dm8kmqbldi35zmbdm") (f (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.3.1 (c (n "cl-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0galjxbjah5id8vkk8kxp3v6k81d4mvxm4sa7gjgf2vjfl2f088w") (f (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.3.2 (c (n "cl-sys") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fx5xigykbwjr97zv5hbfynkqbdzydyc08vab6bgmj4ssf5nw40n") (f (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.4.0 (c (n "cl-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19am2d96zd9phvay3h1jz1mhz82zp3qyfzc7gg6kq0vhbs4wc15v") (f (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("opencl_vendor_mesa") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.4.1 (c (n "cl-sys") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "198bi84biqzkd77smy6llr4q5rpcjwnrgj0bk1c20vyrv877abh5") (f (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("opencl_vendor_mesa") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.4.2 (c (n "cl-sys") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i4fwg6351l40xiwmilndcvq4zi7ain2j4z1x14nrkcazyikymz8") (f (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("opencl_vendor_mesa") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.4.3 (c (n "cl-sys") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15b57q51zsczqlykyxs06ziwwnzd5cmzgyw0c438qqspm4jdissg") (f (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("opencl_vendor_mesa") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

