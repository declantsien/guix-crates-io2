(define-module (crates-io cl ou cloudflared) #:use-module (crates-io))

(define-public crate-cloudflared-0.0.1 (c (n "cloudflared") (v "0.0.1") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 0)))) (h "097w2nzq80jadp6yxkwgbsdz6hinh71w1ghzm0lk24wag19lin6j")))

(define-public crate-cloudflared-0.0.2 (c (n "cloudflared") (v "0.0.2") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 0)))) (h "08lw9vv8n78q801iqcsjdrv9z2ax3nmj5whzv6j6l26w5z7672r3")))

(define-public crate-cloudflared-0.0.3 (c (n "cloudflared") (v "0.0.3") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 0)))) (h "04yq5ffx1d6k3i1vmdvv3adc3z3pphgvas9clmnsqmg4gdk9iwph")))

