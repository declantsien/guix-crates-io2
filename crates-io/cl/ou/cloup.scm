(define-module (crates-io cl ou cloup) #:use-module (crates-io))

(define-public crate-cloup-0.1.1 (c (n "cloup") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "0jkwgc1jqwy0y7cf58c3sc508mqw35r55fjvk0na89h13h4qxmvk")))

(define-public crate-cloup-0.2.0 (c (n "cloup") (v "0.2.0") (h "04waln9qhwjq6ds9gn1x8vvpffc4hx422bbqa7cxv6vm1lrxj5xc")))

