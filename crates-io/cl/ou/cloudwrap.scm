(define-module (crates-io cl ou cloudwrap) #:use-module (crates-io))

(define-public crate-cloudwrap-0.0.1 (c (n "cloudwrap") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.32") (d #t) (k 0)) (d (n "rusoto_ssm") (r "^0.32") (d #t) (k 0)))) (h "0npj7jn51vp5h1ikvaa5hwfvjqh8b7dwsxml99qd0yygx0fi5y3n")))

