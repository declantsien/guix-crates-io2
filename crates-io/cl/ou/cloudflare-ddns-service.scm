(define-module (crates-io cl ou cloudflare-ddns-service) #:use-module (crates-io))

(define-public crate-cloudflare-ddns-service-0.4.0 (c (n "cloudflare-ddns-service") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cloudflare") (r "^0.8.2") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("time" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0w7diyrqb5cxvjjj67cln6q4b2m7c2269y9gb1kgv4ajsfvcw238")))

