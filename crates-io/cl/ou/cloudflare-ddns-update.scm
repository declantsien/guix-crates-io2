(define-module (crates-io cl ou cloudflare-ddns-update) #:use-module (crates-io))

(define-public crate-cloudflare-ddns-update-0.1.0 (c (n "cloudflare-ddns-update") (v "0.1.0") (d (list (d (n "cloudflare") (r "^0.5.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "055cmp8vv17xzfcwak8nv3djmjanhjl6h692l6rviwp6bds0dh6a") (y #t)))

(define-public crate-cloudflare-ddns-update-0.1.1 (c (n "cloudflare-ddns-update") (v "0.1.1") (d (list (d (n "cloudflare") (r "^0.5.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "009d3cvfb2pzdmxk0q2jgwh750fpwwsmnzm7nff97h269idp3jzf") (y #t)))

