(define-module (crates-io cl ou cloudflare-r2-rs) #:use-module (crates-io))

(define-public crate-cloudflare-r2-rs-0.1.0 (c (n "cloudflare-r2-rs") (v "0.1.0") (d (list (d (n "aws-config") (r "^1.1.7") (f (quote ("behavior-version-latest"))) (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^1.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1fh3p3r573ccjqa2jz4mv7bf7jwcf22g03vqiih2hz94lkw729b8")))

(define-public crate-cloudflare-r2-rs-0.2.0 (c (n "cloudflare-r2-rs") (v "0.2.0") (d (list (d (n "aws-config") (r "^1.5.0") (f (quote ("behavior-version-latest"))) (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^1.31.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "0g7489b7mn1mj8s3v6w6l49bsnlm6fndp5mc8c98yq8v2x12qpwk")))

(define-public crate-cloudflare-r2-rs-0.3.0 (c (n "cloudflare-r2-rs") (v "0.3.0") (d (list (d (n "aws-config") (r "^1.5.0") (f (quote ("behavior-version-latest"))) (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^1.31.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "05q3iwf4qamzz6hjs4g91d4ha47rv40yay42jm3jaqd0s5vxwq0w")))

