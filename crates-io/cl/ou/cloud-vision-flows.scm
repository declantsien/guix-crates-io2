(define-module (crates-io cl ou cloud-vision-flows) #:use-module (crates-io))

(define-public crate-cloud-vision-flows-0.1.0 (c (n "cloud-vision-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0g77zm68xklpbslxwq1adh8lia8f2sn0vagfwmw3656203l2bajg")))

(define-public crate-cloud-vision-flows-0.1.1 (c (n "cloud-vision-flows") (v "0.1.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "07yf42yw56wq1wm7xzlaw7vvxc3vzhxbq2imh69c7baddqb2r6l2")))

