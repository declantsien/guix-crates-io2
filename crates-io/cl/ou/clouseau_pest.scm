(define-module (crates-io cl ou clouseau_pest) #:use-module (crates-io))

(define-public crate-clouseau_pest-0.1.0 (c (n "clouseau_pest") (v "0.1.0") (d (list (d (n "clouseau_core") (r "^0.1.0") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "reflection") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 0)))) (h "0gbfjidzc6dy90rld579i7fsv4zlsf9p1anqy8r3j06ijsgymdzk")))

(define-public crate-clouseau_pest-0.2.0 (c (n "clouseau_pest") (v "0.2.0") (d (list (d (n "clouseau_core") (r "^0.2.0") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.2.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "1kb96s2jrn1xm4iaxf2pd7j06v405g4gvj672wvvrq27r83930fq")))

(define-public crate-clouseau_pest-0.2.1 (c (n "clouseau_pest") (v "0.2.1") (d (list (d (n "clouseau_core") (r "^0.2.1") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "0273s528vp7kwch10vqj7bhh2j54ns97xyv3f3b3fzwr3n7ff8s2")))

(define-public crate-clouseau_pest-0.2.2 (c (n "clouseau_pest") (v "0.2.2") (d (list (d (n "clouseau_core") (r "^0.2.2") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.2.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "02ikyqgirm640n4a41i34g0bjccnhxib1fjhad9zb5zpkmwjbx4n")))

(define-public crate-clouseau_pest-0.2.3 (c (n "clouseau_pest") (v "0.2.3") (d (list (d (n "clouseau_core") (r "^0.2.3") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.2.3") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "1j55ml6ab73m3py9jraihvmjhgj2bhh96lkc7bish7gqs6452jnf")))

(define-public crate-clouseau_pest-0.2.4 (c (n "clouseau_pest") (v "0.2.4") (d (list (d (n "clouseau_core") (r "^0.2.4") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.2.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "00m0mvg2b76njgmb8fs9xpkxbndgjcl8g7m2l5124m8cbikgrmfs")))

(define-public crate-clouseau_pest-0.2.5 (c (n "clouseau_pest") (v "0.2.5") (d (list (d (n "clouseau_core") (r "^0.2.5") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.2.5") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "17wya36rsn8dsr2p6c31dlz50wbmia02xh5n735hc2jnnl90qn49")))

(define-public crate-clouseau_pest-0.3.0 (c (n "clouseau_pest") (v "0.3.0") (d (list (d (n "clouseau_core") (r "^0.3.0") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "03lrwks2n9wgsb7zhaj5lxb1w3j1c88k6lbpy680hq6gjcr7irzm")))

(define-public crate-clouseau_pest-0.3.1 (c (n "clouseau_pest") (v "0.3.1") (d (list (d (n "clouseau_core") (r "^0.3.1") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.3.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "0j1v3bj8mp2zjhrffrd51ixw2v5sywzqysjlxcc4vgsnq1dfx04x")))

(define-public crate-clouseau_pest-0.3.2 (c (n "clouseau_pest") (v "0.3.2") (d (list (d (n "clouseau_core") (r "^0.3.2") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.3.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "0sahaa27chsqi6g11q8z7m260y48nprj6m4vwan5jabkbzy1ni2c")))

(define-public crate-clouseau_pest-0.3.3 (c (n "clouseau_pest") (v "0.3.3") (d (list (d (n "clouseau_core") (r "^0.3.3") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.3.3") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "10npa642c0hpiaa10hrd6i3x72gfvv0y2gdgdgnfyjnx8nf5zqjr")))

(define-public crate-clouseau_pest-0.3.4 (c (n "clouseau_pest") (v "0.3.4") (d (list (d (n "clouseau_core") (r "^0.3.4") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.3.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "1slxf727q41lv23lwi1r3ijjzcw5wkmrxc4qgvrx2iqwqgw52zz1")))

(define-public crate-clouseau_pest-0.3.5 (c (n "clouseau_pest") (v "0.3.5") (d (list (d (n "clouseau_core") (r "^0.3.5") (d #t) (k 0)) (d (n "clouseau_query") (r "^0.3.5") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "velcro") (r "^0.5") (d #t) (k 2)))) (h "0516wc0rbaf9vd54zw33mqscb856yk0fnvjjb5grl4kghsv3c186") (y #t)))

