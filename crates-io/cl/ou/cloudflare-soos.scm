(define-module (crates-io cl ou cloudflare-soos) #:use-module (crates-io))

(define-public crate-cloudflare-soos-1.18.0 (c (n "cloudflare-soos") (v "1.18.0") (d (list (d (n "wasm-bindgen") (r "^0.2.51") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0jj3z3iw5n7y5xr45ibqw3mfr7icr9pjw0h35wygnvviv9nig25b")))

(define-public crate-cloudflare-soos-2.0.0 (c (n "cloudflare-soos") (v "2.0.0") (d (list (d (n "wasm-bindgen") (r "^0.2.51") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "04s97d27s3xfih23crj0dv5442xhr63816p5f4p0pf83bjhzx27p")))

(define-public crate-cloudflare-soos-2.1.0 (c (n "cloudflare-soos") (v "2.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.51") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "weezl") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0673acbk58z750svnzdymqk09b2mzdp1bvjlgff20l29y97y5k04") (f (quote (("gif" "weezl") ("default" "gif"))))))

(define-public crate-cloudflare-soos-2.2.0 (c (n "cloudflare-soos") (v "2.2.0") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.51") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "weezl") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0733lvnwzcacpidasnwp4vmmbz1cnnrl5rg5ma3z5w2lf4vky4f0") (f (quote (("default" "gif")))) (s 2) (e (quote (("gif" "dep:weezl"))))))

(define-public crate-cloudflare-soos-2.3.0 (c (n "cloudflare-soos") (v "2.3.0") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.51") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "weezl") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1m20q1ip510hvhmbh529m2rdab305jphrgq0y5hm32hlg7jq11sh") (f (quote (("default" "gif")))) (s 2) (e (quote (("gif" "dep:weezl"))))))

(define-public crate-cloudflare-soos-2.3.1 (c (n "cloudflare-soos") (v "2.3.1") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.51") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1wraxgnjk8jd58r7z9yqi9knwws37hpkldzbzw8ra8rd21hn8jws") (f (quote (("gif") ("default" "gif"))))))

