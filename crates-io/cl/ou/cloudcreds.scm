(define-module (crates-io cl ou cloudcreds) #:use-module (crates-io))

(define-public crate-cloudcreds-0.1.0 (c (n "cloudcreds") (v "0.1.0") (h "1c5ms75ijv8j825h85hicr5wmgay7xykhkx5lgqiy72q1hkklcr4")))

(define-public crate-cloudcreds-0.1.1 (c (n "cloudcreds") (v "0.1.1") (h "1m0ycf51mqqnwdj2f6rx1d80xmzmp90wq9rfkgazn22bpxjjjkhi")))

