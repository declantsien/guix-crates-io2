(define-module (crates-io cl ou cloudflare-zlib-sys) #:use-module (crates-io))

(define-public crate-cloudflare-zlib-sys-0.1.0 (c (n "cloudflare-zlib-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.15") (d #t) (k 1)))) (h "07c9gjyj2nicihal7pprrfngm8jrcxf9w6n01wa8awxiyszpi2wg") (y #t) (l "z")))

(define-public crate-cloudflare-zlib-sys-0.1.1 (c (n "cloudflare-zlib-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.15") (d #t) (k 1)))) (h "0qkjwz4zvhh3ydv3zdp2j1cl5wlj2pcik5zybq7in93x8y7wm65s") (f (quote (("default" "asm") ("asm")))) (y #t) (l "z")))

(define-public crate-cloudflare-zlib-sys-0.1.2 (c (n "cloudflare-zlib-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.15") (d #t) (k 1)))) (h "18jym1mg94cri03m0gh115zh3a9z1pvzaif4ccqh14zrpd75f9qs") (f (quote (("default" "asm") ("asm")))) (y #t) (l "z")))

(define-public crate-cloudflare-zlib-sys-0.2.0 (c (n "cloudflare-zlib-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "01lwfd15ijw4d8jsqp87yv4wpmzcp84qm0qqwy3yxmm0fjr5q6by") (f (quote (("default" "asm") ("asm"))))))

(define-public crate-cloudflare-zlib-sys-0.2.2 (c (n "cloudflare-zlib-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "03b50snwgzyfr57qmj486mapav088rj8dhx5a1387d77nbr2qa9g") (f (quote (("default" "asm") ("asm")))) (y #t)))

(define-public crate-cloudflare-zlib-sys-0.2.3-beta.1 (c (n "cloudflare-zlib-sys") (v "0.2.3-beta.1") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "0irfwh5ams59vn3pc8g2x7la5p65m916kgz9csmah5qyqqpqj0k5") (f (quote (("default" "asm") ("asm")))) (y #t)))

(define-public crate-cloudflare-zlib-sys-0.2.3 (c (n "cloudflare-zlib-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "0r6nyzfv4rc46fvmpm60p4maihbr7kdxrjxvvn6vhins0ds8j1y9") (f (quote (("default" "asm") ("asm")))) (y #t)))

(define-public crate-cloudflare-zlib-sys-0.2.4 (c (n "cloudflare-zlib-sys") (v "0.2.4") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "1h7mqx2va8gkdm1bnqwdamsq6zbfiz9sw6psp6k4l0pn1wgwvnsx") (f (quote (("default" "asm") ("asm")))) (y #t)))

(define-public crate-cloudflare-zlib-sys-0.3.0 (c (n "cloudflare-zlib-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "0vyd0l0vprvh9hc1ikllybrk8xc0lz9f509d2xgxgrpyxp8vch10") (f (quote (("default" "asm") ("asm"))))))

(define-public crate-cloudflare-zlib-sys-0.3.1 (c (n "cloudflare-zlib-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "1pwxb28j3g8hgqnr83430wwqsk1yvr677rmxc678mw2a5v73dx4n") (f (quote (("default" "asm") ("asm"))))))

(define-public crate-cloudflare-zlib-sys-0.3.2 (c (n "cloudflare-zlib-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "12fscp2pk4dj4xb6jwin8ma46rk8617q5pihyligxli7mpm22cn0") (f (quote (("asm")))) (y #t) (l "cfzlib")))

(define-public crate-cloudflare-zlib-sys-0.3.3 (c (n "cloudflare-zlib-sys") (v "0.3.3") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "141wz02xs4dn3hrawg1r91yrjjqrir0rzj2sdlsanlwwqvw5y663") (f (quote (("asm")))) (l "cfzlib")))

