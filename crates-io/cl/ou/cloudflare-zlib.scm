(define-module (crates-io cl ou cloudflare-zlib) #:use-module (crates-io))

(define-public crate-cloudflare-zlib-0.2.0 (c (n "cloudflare-zlib") (v "0.2.0") (d (list (d (n "cloudflare-zlib-sys") (r "^0.2") (k 0)))) (h "1bidggmjplsjq5mybz4zlr3h25gddzl0ykq7md9amkih48kxdi9y") (f (quote (("default" "asm") ("asm" "cloudflare-zlib-sys/asm")))) (y #t)))

(define-public crate-cloudflare-zlib-0.2.1 (c (n "cloudflare-zlib") (v "0.2.1") (d (list (d (n "cloudflare-zlib-sys") (r "^0.2") (k 0)))) (h "0v3scni0rgnla1xrpgsgfsy3a0dx0fc3iraqk3431wii8immv3rr") (f (quote (("default" "asm") ("asm" "cloudflare-zlib-sys/asm")))) (y #t)))

(define-public crate-cloudflare-zlib-0.2.2 (c (n "cloudflare-zlib") (v "0.2.2") (d (list (d (n "cloudflare-zlib-sys") (r "^0.2") (k 0)))) (h "1b4d0zpd7p47r9nzz03ixb13sfccwj4zbkhkwm6i7vvpfwmsg5yd") (f (quote (("default" "asm") ("asm" "cloudflare-zlib-sys/asm") ("arm-conditional") ("arm-always")))) (y #t)))

(define-public crate-cloudflare-zlib-0.2.3 (c (n "cloudflare-zlib") (v "0.2.3") (d (list (d (n "cloudflare-zlib-sys") (r "^0.2") (k 0)))) (h "0w1y5ffhpw32a5d7sjf1nzxx25v2my0s2205jjr7vayd1h0q3id1") (f (quote (("default" "asm") ("asm" "cloudflare-zlib-sys/asm") ("arm-conditional") ("arm-always")))) (y #t)))

(define-public crate-cloudflare-zlib-0.2.5 (c (n "cloudflare-zlib") (v "0.2.5") (d (list (d (n "cloudflare-zlib-sys") (r "^0.2") (k 0)))) (h "1fg4bwjq9yjmvcxkkzvz860n5ca1pybcpjxdbk8sqnym36h67vgm") (f (quote (("default" "asm") ("asm" "cloudflare-zlib-sys/asm") ("arm-conditional") ("arm-always")))) (y #t)))

(define-public crate-cloudflare-zlib-0.2.6 (c (n "cloudflare-zlib") (v "0.2.6") (d (list (d (n "cloudflare-zlib-sys") (r "^0.2") (k 0)))) (h "1ig9lv5kqmh1nyj4k0yvwlvs8qq6n87idl1522qxwl1kdd5za6ry") (f (quote (("default" "asm") ("asm" "cloudflare-zlib-sys/asm") ("arm-conditional") ("arm-always"))))))

(define-public crate-cloudflare-zlib-0.2.7 (c (n "cloudflare-zlib") (v "0.2.7") (d (list (d (n "cloudflare-zlib-sys") (r "^0.2") (k 0)))) (h "09gmav35mmkz9vcjky4qmgpwfvph6isf1ncw0280gksmdd1dfhnj") (f (quote (("default" "asm") ("asm" "cloudflare-zlib-sys/asm") ("arm-conditional") ("arm-always"))))))

(define-public crate-cloudflare-zlib-0.2.8 (c (n "cloudflare-zlib") (v "0.2.8") (d (list (d (n "cloudflare-zlib-sys") (r "^0.3") (k 0)))) (h "19yblnbv94rqvqlsacj2ah9vh040sdanyir3p5xkf8pl6k9f9mnk") (f (quote (("default" "asm" "arm-conditional") ("asm" "cloudflare-zlib-sys/asm") ("arm-conditional") ("arm-always"))))))

(define-public crate-cloudflare-zlib-0.2.9 (c (n "cloudflare-zlib") (v "0.2.9") (d (list (d (n "cloudflare-zlib-sys") (r "^0.3") (k 0)))) (h "0kgrc383jx8z26fgz7k0pfw7czgb6nhl4qvm2pmldw87vysyzz1c") (f (quote (("default" "asm") ("asm" "cloudflare-zlib-sys/asm") ("arm-conditional") ("arm-always"))))))

(define-public crate-cloudflare-zlib-0.2.10 (c (n "cloudflare-zlib") (v "0.2.10") (d (list (d (n "cloudflare-zlib-sys") (r "^0.3.2") (d #t) (k 0)))) (h "14fbdq45ja4h9zhq46b5nkrr4addc514gmmh25qn18khhq51dyj0") (f (quote (("asm") ("arm-conditional") ("arm-always")))) (r "1.60")))

