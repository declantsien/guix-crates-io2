(define-module (crates-io cl ou cloudabi) #:use-module (crates-io))

(define-public crate-cloudabi-0.0.1 (c (n "cloudabi") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1jv3k4rx4ksyr0giqv5jwfwha243yj1mw5vjqxdr3r5sz9bkv0wi")))

(define-public crate-cloudabi-0.0.2 (c (n "cloudabi") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1dasn0p18c7yndwgwk77g0qwayv56mf81zhxnhv3l58c0jrjljy3") (f (quote (("default" "bitflags"))))))

(define-public crate-cloudabi-0.0.3 (c (n "cloudabi") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0kxcg83jlihy0phnd2g8c2c303px3l2p3pkjz357ll6llnd5pz6x") (f (quote (("default" "bitflags"))))))

(define-public crate-cloudabi-0.1.0 (c (n "cloudabi") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (o #t) (d #t) (k 0)))) (h "0rv4yf5jlldfkynzrw687s00f4x12ypw7axv71vawhy6h4i52i23") (f (quote (("default" "bitflags"))))))

