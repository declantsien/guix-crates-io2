(define-module (crates-io cl ou cloudflare-dns-updater_rs) #:use-module (crates-io))

(define-public crate-cloudflare-dns-updater_rs-0.1.0 (c (n "cloudflare-dns-updater_rs") (v "0.1.0") (d (list (d (n "ajson") (r "^0.2") (d #t) (k 0)) (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1ppn83j0sb7p09x58v895v0hf3lmcsbh8ip4ddxpgqdd621hqacg")))

