(define-module (crates-io cl ou cloudfront-policy-signer) #:use-module (crates-io))

(define-public crate-cloudfront-policy-signer-0.1.0 (c (n "cloudfront-policy-signer") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)))) (h "0467n5cbksqn04vx56qjldjvf951569207mqpgahn4isa6b0mrq0")))

(define-public crate-cloudfront-policy-signer-0.1.1 (c (n "cloudfront-policy-signer") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)))) (h "1dqhvj5lpvqn20ri9ldx8h6zh4a37v7gdi2r2b4shdvjdv0asa3s")))

(define-public crate-cloudfront-policy-signer-0.1.2 (c (n "cloudfront-policy-signer") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)))) (h "0n7hdf3qxp5s3pp1jflz03ny4laqwjq6fa9gg8qjfkvdcrpc0mfb")))

(define-public crate-cloudfront-policy-signer-0.1.3 (c (n "cloudfront-policy-signer") (v "0.1.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)))) (h "1qvw0j8ngp5fcjnpxwrpg3b6p2mkwf0qw4xwb7zy3q37l0ya5fji")))

