(define-module (crates-io cl ou cloud-hypervisor-command-builder) #:use-module (crates-io))

(define-public crate-cloud-hypervisor-command-builder-0.38.0-beta.1 (c (n "cloud-hypervisor-command-builder") (v "0.38.0-beta.1") (d (list (d (n "bytesize") (r "^1.3.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "12zydzddh449gzb7pk10mzy42i623saqmiq88ykqrn6jdwqqh95c") (r "1.76")))

(define-public crate-cloud-hypervisor-command-builder-0.38.0-beta.2 (c (n "cloud-hypervisor-command-builder") (v "0.38.0-beta.2") (d (list (d (n "bytesize") (r "^1.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)))) (h "13jgsb5cah9a6ppa875rfxxphnardqdfjzhbqjh5y8kzp4bkqs7p") (r "1.76")))

