(define-module (crates-io cl ou clouseau_macros) #:use-module (crates-io))

(define-public crate-clouseau_macros-0.1.0 (c (n "clouseau_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "0x2b0mlkzl5s1pv32wf01izn7kjidzri67yd6n8hdw04622h2m53")))

(define-public crate-clouseau_macros-0.2.0 (c (n "clouseau_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "1j3fmmvnkn7lvwng5m8208ckcj0dmm6s9xg6l4824dcim330grix")))

(define-public crate-clouseau_macros-0.2.1 (c (n "clouseau_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "10zsaw79lr0k9dq5pkdp7g3rzx4k3y8lf5bvzrrsy8g1lzd4nnpj")))

(define-public crate-clouseau_macros-0.2.2 (c (n "clouseau_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "1rvbl4vy4p2jw82w9cr04i8bbvqm4hc93ck2pdy4g563654rgr9v")))

(define-public crate-clouseau_macros-0.2.3 (c (n "clouseau_macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "10d8362wzwv440n1cfn20blzplx6fg5gcz208qdgw6rlcrwkrvmn")))

(define-public crate-clouseau_macros-0.2.4 (c (n "clouseau_macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "1nsxffys2047fkk9p0vsvi9695r19xd47n26a664z80hi2hnpdyd")))

(define-public crate-clouseau_macros-0.2.5 (c (n "clouseau_macros") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "0dq3wa90c4ziynr6wx456s29y6aspk58lnh2p3f0g8yjawfzsm6b")))

(define-public crate-clouseau_macros-0.3.0 (c (n "clouseau_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing" "parsing"))) (d #t) (k 0)))) (h "11g3jn9k4pi8ygxrc31c1c8x4g3gq7sf9ji3hig35phps1m9p8ij")))

(define-public crate-clouseau_macros-0.3.1 (c (n "clouseau_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing" "parsing"))) (d #t) (k 0)))) (h "1vfwdkiirsqglqx2x8950xhb21qri78f7dhj5ybszr2czjpd3f8a")))

(define-public crate-clouseau_macros-0.3.2 (c (n "clouseau_macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing" "parsing"))) (d #t) (k 0)))) (h "0yzxip9bcc7k5bcdvk8h9iypgwvrkf9z105kvr1h3h5gsq65dsqn")))

(define-public crate-clouseau_macros-0.3.3 (c (n "clouseau_macros") (v "0.3.3") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing" "parsing"))) (d #t) (k 0)))) (h "1kvkaq1m4pcrq59qci1sprpgv04m7rysq8rcicj49gixh1gyw0yj")))

(define-public crate-clouseau_macros-0.3.4 (c (n "clouseau_macros") (v "0.3.4") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing" "parsing"))) (d #t) (k 0)))) (h "1jcghr88g0sjf3pnapx74rzf005l61irvfkdy01avpmjvsbzkf4i")))

(define-public crate-clouseau_macros-0.3.5 (c (n "clouseau_macros") (v "0.3.5") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "printing" "parsing"))) (d #t) (k 0)))) (h "1524lliz79vc3xs489wfw22hd333kcs1hf9dd7zhn4l1sd96svlw") (y #t)))

