(define-module (crates-io cl ou cloud-translate) #:use-module (crates-io))

(define-public crate-cloud-translate-0.1.0 (c (n "cloud-translate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "google-cloud-auth") (r "^0.8.0") (d #t) (k 0)) (d (n "polib") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sa5az3lfjy50nxa6pd1rs1yf1x5v2rhkjvs2nx263lwc6n3vpag")))

