(define-module (crates-io cl ou cloud-macro) #:use-module (crates-io))

(define-public crate-cloud-macro-0.1.0 (c (n "cloud-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "018fb8cyy59lpqjgrhkyk6mdwn3b14rqqbmn9dj2bxkjqij0daiw")))

