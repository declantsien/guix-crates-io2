(define-module (crates-io cl ou cloudllm) #:use-module (crates-io))

(define-public crate-cloudllm-0.1.0 (c (n "cloudllm") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "openai-rust") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19z5w2xbl5rf4zsk8qrlk03lwrbxjd7021yx2i5yvq4v54m41bc1")))

(define-public crate-cloudllm-0.1.1 (c (n "cloudllm") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "openai-rust") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d1qaz0ib2pqv5kb2wfxcn24rxx38g44w840gxnkxxijmic5iipq")))

(define-public crate-cloudllm-0.1.2 (c (n "cloudllm") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "openai-rust") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cc19a2wb6wda5bl970jsis3imdsnsxfii7czkxpdg7yri2bssg8")))

(define-public crate-cloudllm-0.1.3 (c (n "cloudllm") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "openai-rust") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fi13kz5716v1h9h4v7kdp16ip50hdnxdkzz4cmdly19jlhfyksp")))

