(define-module (crates-io cl ou cloudsync) #:use-module (crates-io))

(define-public crate-cloudsync-0.1.0 (c (n "cloudsync") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "firestore") (r "^0.11") (d #t) (k 0)) (d (n "gcloud-sdk") (r "^0.19") (f (quote ("google-firestore-v1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "11l1mhlmxy38bz50l553ykzvw0fpfjf0712nd6vsaffilsh6x8ic")))

