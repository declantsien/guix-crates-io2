(define-module (crates-io cl ou clouseau_query) #:use-module (crates-io))

(define-public crate-clouseau_query-0.1.0 (c (n "clouseau_query") (v "0.1.0") (d (list (d (n "clouseau_core") (r "^0.1.0") (d #t) (k 0)))) (h "0h0dv7dvdrr2cbzpwnib7cn9ia02863h97g76s52sq5ram16awkd")))

(define-public crate-clouseau_query-0.2.0 (c (n "clouseau_query") (v "0.2.0") (d (list (d (n "clouseau_core") (r "^0.2.0") (d #t) (k 0)))) (h "1mgj4012bz5igj2lhnkrzhd1s1xwm62fwj1abrrfkmga7lar9hcg")))

(define-public crate-clouseau_query-0.2.1 (c (n "clouseau_query") (v "0.2.1") (d (list (d (n "clouseau_core") (r "^0.2.1") (d #t) (k 0)))) (h "0xhl1853cfw0hdhv6w62q8ydmq1fnyxld0dfwxmdvad67sbk4aml")))

(define-public crate-clouseau_query-0.2.2 (c (n "clouseau_query") (v "0.2.2") (d (list (d (n "clouseau_core") (r "^0.2.2") (d #t) (k 0)))) (h "04y1nldhw8bh8d3zavxakfqnqc48z78yjs15a31f7bpx5pamri3n")))

(define-public crate-clouseau_query-0.2.3 (c (n "clouseau_query") (v "0.2.3") (d (list (d (n "clouseau_core") (r "^0.2.3") (d #t) (k 0)))) (h "0vmljhfzs93qdacw8x33ckzq70kj44qxmjllrhzq9ixbx8qgsjxn")))

(define-public crate-clouseau_query-0.2.4 (c (n "clouseau_query") (v "0.2.4") (d (list (d (n "clouseau_core") (r "^0.2.4") (d #t) (k 0)))) (h "08jmm472ins4yk0dxgp0gkca9v59dvbl5xs5x4az338ycr1abn4x")))

(define-public crate-clouseau_query-0.2.5 (c (n "clouseau_query") (v "0.2.5") (d (list (d (n "clouseau_core") (r "^0.2.5") (d #t) (k 0)))) (h "1m9hwg4hqy8636azzrx9f7h73axa6qdw0gzxbhsh7wz3zpgn5n8h")))

(define-public crate-clouseau_query-0.3.0 (c (n "clouseau_query") (v "0.3.0") (d (list (d (n "clouseau_core") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1hfn47rk02jl1xab1hrmmwb01bi1cwgivx09m309rizg52n47kqq")))

(define-public crate-clouseau_query-0.3.1 (c (n "clouseau_query") (v "0.3.1") (d (list (d (n "clouseau_core") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0kswilxw86rg3lbchnqldpmid6ywq3ajy472zg2ngriq2gz4a76x")))

(define-public crate-clouseau_query-0.3.2 (c (n "clouseau_query") (v "0.3.2") (d (list (d (n "clouseau_core") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0hdbdkmx8r2pgbglkv6y8aznq17xyafb2hbakb5awiz6mj6cyhyi")))

(define-public crate-clouseau_query-0.3.3 (c (n "clouseau_query") (v "0.3.3") (d (list (d (n "clouseau_core") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "162nwz741qaj133275vaaqijm9rzsswpdsxdxi4gwrip8k8hf741")))

(define-public crate-clouseau_query-0.3.4 (c (n "clouseau_query") (v "0.3.4") (d (list (d (n "clouseau_core") (r "^0.3.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0cmfs63gcmpnvg7h2g920zzhssi31knirczpm4wf1i98cfc8w6h4")))

(define-public crate-clouseau_query-0.3.5 (c (n "clouseau_query") (v "0.3.5") (d (list (d (n "clouseau_core") (r "^0.3.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1vmqsg9rgp3xd94ph73fc35q9brzcmrh03rdkblzrndr0lcbx701") (y #t)))

