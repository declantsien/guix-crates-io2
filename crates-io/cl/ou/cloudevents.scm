(define-module (crates-io cl ou cloudevents) #:use-module (crates-io))

(define-public crate-cloudevents-0.1.0 (c (n "cloudevents") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1jamj6j6fxbiw2qb5yqs0qw8c1nq19sf18zbjagy0xg05b1pvj7l") (f (quote (("v02") ("default" "v02"))))))

(define-public crate-cloudevents-0.1.1 (c (n "cloudevents") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1ly2h2f6nrlpn0vpglad5c3wkx83m8c8dlcbhg0z04vl7sf90yrm") (f (quote (("v02") ("default" "v02"))))))

