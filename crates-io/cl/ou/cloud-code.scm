(define-module (crates-io cl ou cloud-code) #:use-module (crates-io))

(define-public crate-cloud-code-0.1.0 (c (n "cloud-code") (v "0.1.0") (d (list (d (n "cita_cloud_proto") (r "^6.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1whgkmd8b62i4ik2sj76d40zcrfvvjxfk3h9xyljmgba7z6k9m3q") (y #t)))

(define-public crate-cloud-code-0.1.1 (c (n "cloud-code") (v "0.1.1") (d (list (d (n "cita_cloud_proto") (r "=6.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1g5vcf8x44xc6nsdy1n642fi47jg1pfq867khbw1kaxsx60vq4sg")))

(define-public crate-cloud-code-0.1.2 (c (n "cloud-code") (v "0.1.2") (d (list (d (n "cita_cloud_proto") (r "=6.3.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "05i8gsl1lz7cxhd06yryzhi46kalg08iambxnizknddx9q63sa2m")))

(define-public crate-cloud-code-0.1.3 (c (n "cloud-code") (v "0.1.3") (d (list (d (n "cita_cloud_proto") (r "=6.3.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0971j829wj4zvb2155n4w2lfqljk3jl6q09li2gkr46anmd5sq61")))

(define-public crate-cloud-code-0.2.0 (c (n "cloud-code") (v "0.2.0") (d (list (d (n "cita_cloud_proto") (r "=6.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0j7fhclp1ndx9xi9n7pzi3smfhg8b5i9x32ayn9gigga1809rk6s") (y #t)))

(define-public crate-cloud-code-0.2.1 (c (n "cloud-code") (v "0.2.1") (d (list (d (n "cita_cloud_proto") (r "=6.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1np1291fjkda7xky7rcpws6mwakiy7glphgj8f0d39bcwgapb2s1")))

(define-public crate-cloud-code-0.3.0 (c (n "cloud-code") (v "0.3.0") (d (list (d (n "cita_cloud_proto") (r "=6.5.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "08r19vmwx1k6fyd1csby7x5fg6rzhajksmz3qswh0sk56dabdnv8")))

(define-public crate-cloud-code-0.4.0 (c (n "cloud-code") (v "0.4.0") (d (list (d (n "cita_cloud_proto") (r "=6.6.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0rqw8gscw52p2zrij7w396djlyyi9c36d2rn10cncqgdyd9rgm65")))

(define-public crate-cloud-code-0.5.0 (c (n "cloud-code") (v "0.5.0") (d (list (d (n "cita_cloud_proto") (r "^6.6.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0gxi9v2ny7bhvxpjim3blpia6zzqs2smx66schndykqy5l64m4n3") (y #t)))

(define-public crate-cloud-code-0.5.1 (c (n "cloud-code") (v "0.5.1") (d (list (d (n "cita_cloud_proto") (r "^6.6.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "11bc5ixwaslqa5dv0dhgqimfkzw0xphidm8mw2a5dlqjyxlys297")))

(define-public crate-cloud-code-0.5.2 (c (n "cloud-code") (v "0.5.2") (d (list (d (n "cita_cloud_proto") (r "=6.6.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "074999lyl10pxwjgb4wyb6yhrhpgcb9wc4dvsl52jxc2fms9kfwx") (y #t)))

