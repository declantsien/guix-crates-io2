(define-module (crates-io cl ou cloud-detect) #:use-module (crates-io))

(define-public crate-cloud-detect-0.1.0 (c (n "cloud-detect") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ywzjxcn6m0a7ll8p5cwigx4gvy6wryh04qlgfx0brzwhspvic82")))

(define-public crate-cloud-detect-0.2.0 (c (n "cloud-detect") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "150020kpqr6sazg6m4qrh45qnncjmml69l682d2m52vnib9c2g0d")))

(define-public crate-cloud-detect-0.3.0 (c (n "cloud-detect") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0718p8897zgz6jd2yma2wk7kqsx2kh1lbg023sckjlfr1n7w5mwm")))

(define-public crate-cloud-detect-0.4.0 (c (n "cloud-detect") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ww83fixsln5sfir3k8vi3wspknanmq4s993pa837cxc493xvvyv") (f (quote (("blocking"))))))

(define-public crate-cloud-detect-0.5.0 (c (n "cloud-detect") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "10x4c52qxq782aspq2mvkwxbqq7ajgrxachhi8nhqb3il4gsm84q") (f (quote (("default" "reqwest") ("blocking" "reqwest/blocking"))))))

(define-public crate-cloud-detect-1.0.0 (c (n "cloud-detect") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "10if3q2jacvq9lngx6a556i2z017gdl5c1hjl7wd3ghxcgfxcjm1")))

(define-public crate-cloud-detect-1.1.0 (c (n "cloud-detect") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "127zq7kpaq0avxcfz47fv8qciy4xmkrg48bg0vbmgrind18m6mgi")))

