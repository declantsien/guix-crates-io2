(define-module (crates-io cl ou cloud-meta) #:use-module (crates-io))

(define-public crate-cloud-meta-0.1.0 (c (n "cloud-meta") (v "0.1.0") (h "1l3nh09xz2m0g8390ysr5snvg2z499fj2f1w5ihvqgng4k3iwwxa")))

(define-public crate-cloud-meta-0.2.0 (c (n "cloud-meta") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "http") (r "^0.2.6") (d #t) (k 0)) (d (n "hyper") (r "^0.14.16") (f (quote ("client" "http1" "tcp"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1aks7wcqqay9j67b0hv66azh80n6cwrzfcpsr78zys7g5w6pkpph")))

