(define-module (crates-io cl ou cloud-discovery-kubernetes) #:use-module (crates-io))

(define-public crate-cloud-discovery-kubernetes-0.1.0 (c (n "cloud-discovery-kubernetes") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.11") (f (quote ("v1_18"))) (d #t) (k 0)) (d (n "kube") (r "^0.51") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rust-cloud-discovery") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0y86fzq1ik8kvim85wvv3hx3qwrsl3syji1jhkn3nbw8kkxsk8w9")))

(define-public crate-cloud-discovery-kubernetes-0.2.0 (c (n "cloud-discovery-kubernetes") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.11") (f (quote ("v1_18"))) (d #t) (k 0)) (d (n "kube") (r "^0.51") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rust-cloud-discovery") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1jh20kp1gi42nsf0z205pgkfyn9n0y6qs5f766l9djjpqz674lfh")))

