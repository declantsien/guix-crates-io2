(define-module (crates-io cl ou cloudfront_sign) #:use-module (crates-io))

(define-public crate-cloudfront_sign-0.1.1 (c (n "cloudfront_sign") (v "0.1.1") (d (list (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0yz02gibidk9k88nh5m5w2hs2p03idvsawryi8afj82qad7gxid6")))

(define-public crate-cloudfront_sign-0.1.2 (c (n "cloudfront_sign") (v "0.1.2") (d (list (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1axs7khka0kkbbp2klhqivrra9d4g92hv27m6grk97xs5awy6fix")))

(define-public crate-cloudfront_sign-0.2.0 (c (n "cloudfront_sign") (v "0.2.0") (d (list (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1s5i6qp5k1mh874kf4jhrhvimfgkspxnhcdh2z4maqs3hhk0kcgv")))

(define-public crate-cloudfront_sign-0.2.1 (c (n "cloudfront_sign") (v "0.2.1") (d (list (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0cp865541xvzfhrvpkgvd81kcf2g089nrhl5nw5hilfjj3fwwplm")))

(define-public crate-cloudfront_sign-0.3.0 (c (n "cloudfront_sign") (v "0.3.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "rsa") (r "^0.9") (f (quote ("sha1"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0qj15jq824330d0nywqbqjdc220j6qim1yc4h189hdyj0zvjs18j")))

