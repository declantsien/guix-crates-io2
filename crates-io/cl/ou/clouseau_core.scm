(define-module (crates-io cl ou clouseau_core) #:use-module (crates-io))

(define-public crate-clouseau_core-0.1.0 (c (n "clouseau_core") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1f8nh8493r0p2y2mmx7aldly626gy88m9ghr1w7bb9w006sf7sar")))

(define-public crate-clouseau_core-0.2.0 (c (n "clouseau_core") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1b3f7pwvaw32r67i0ngx7hjm2ky159xdp3wwsmglp7dpjlh1x3d7")))

(define-public crate-clouseau_core-0.2.1 (c (n "clouseau_core") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1lsw0jhm4q5rmag521vyhrqykfzdp9qkmc3x40hifdkqammcmkw8")))

(define-public crate-clouseau_core-0.2.2 (c (n "clouseau_core") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1l6vza9pzqwb79dlc9dq1ik3xmcd94xq3992h3lwks4s7xq23hqi")))

(define-public crate-clouseau_core-0.2.3 (c (n "clouseau_core") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0y90rs6c167fyx1fzi9idnyznr45l3jpb7qrz9yr7gwl6g4xsqbn")))

(define-public crate-clouseau_core-0.2.4 (c (n "clouseau_core") (v "0.2.4") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0d4f5bl63magwyly2wdsgc3b3l60vkjiqv44dlnv2z3vcd6x8g4i")))

(define-public crate-clouseau_core-0.2.5 (c (n "clouseau_core") (v "0.2.5") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0zvbg7g9h341625a8fdndd14wfgbcrpinlmnzy6k2mp5mdj8ndlq")))

(define-public crate-clouseau_core-0.3.0 (c (n "clouseau_core") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0f7g2b9qnrm9pnkn9q956zn3k7xv1wxbra708mmk48wri0d6v48x")))

(define-public crate-clouseau_core-0.3.1 (c (n "clouseau_core") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1shiggrg5y06c2qrn02p33r3x0qqmlha0l9az9iyr4v5qf0lran8")))

(define-public crate-clouseau_core-0.3.2 (c (n "clouseau_core") (v "0.3.2") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1lii3zd4rv29sgp5vkxz5simjpj9zd247ss0d4b5ifx26cwj4kd0")))

(define-public crate-clouseau_core-0.3.3 (c (n "clouseau_core") (v "0.3.3") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "08x08icnz0rsjzwhcj31519h63v2k26hfpclndakz19vwh6c5sr8")))

(define-public crate-clouseau_core-0.3.4 (c (n "clouseau_core") (v "0.3.4") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "063wskx0vmhls1fm1p0gkn8460sas4p43diq7f0kwkgyzfbrj49w")))

(define-public crate-clouseau_core-0.3.5 (c (n "clouseau_core") (v "0.3.5") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1zf4mjqr7c5f55dww8szpr6079a3xihjvvq3snhp6mlnvrjyk78l") (y #t)))

