(define-module (crates-io cl ou clout) #:use-module (crates-io))

(define-public crate-clout-0.0.1 (c (n "clout") (v "0.0.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0vvkl86gn379cjaly66dh850qyc8m7c06i8lzg54lvxl0fnq4rpw")))

(define-public crate-clout-0.1.0 (c (n "clout") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1jm2znlb9rywrypggifx3gcv51q5qs39qv94pz4km150jdicgg6j")))

(define-public crate-clout-0.1.1 (c (n "clout") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1ga449gj73qaqizi3nliq99d5wz53pmwn9b89djsnj54183xnvl5")))

(define-public crate-clout-0.1.2 (c (n "clout") (v "0.1.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1ah3g58y9c2zqs5dqlrvlk265mz3bjbmwfbikk8qr0mh0m5xfsh8")))

(define-public crate-clout-0.2.0 (c (n "clout") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0y462kqmfy8z6x04ivmsm59lm0i2l3fl9lpixr4fc9ik76ava4m1")))

