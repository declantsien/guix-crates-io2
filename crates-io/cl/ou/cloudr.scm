(define-module (crates-io cl ou cloudr) #:use-module (crates-io))

(define-public crate-cloudr-1.2.1 (c (n "cloudr") (v "1.2.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "1sfr3lrai4zaih6xf0qgk4di5zlxcrdw9yiqhg5ggjk72g44yhx8")))

(define-public crate-cloudr-1.2.2 (c (n "cloudr") (v "1.2.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "1yrwc4z4cgkqn9kmkkl0n2n458fhfysqynknqwmy7sihmia90jnl")))

(define-public crate-cloudr-1.2.3 (c (n "cloudr") (v "1.2.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "125nv67vny6nqdmsbzr5abdl9nw2mvfnz3g1w1mx96smkkqbav1r")))

(define-public crate-cloudr-1.2.4 (c (n "cloudr") (v "1.2.4") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "1kbj04gypl29l553r4nc3bnd85vj46b3f4xiiwq331avc5msmwlv")))

(define-public crate-cloudr-1.3.0 (c (n "cloudr") (v "1.3.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "1zkjr5qqv5m4fad4vp3yaw6z19y9lrxsp806h0zyhv17hrdnyk95")))

