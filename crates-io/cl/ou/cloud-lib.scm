(define-module (crates-io cl ou cloud-lib) #:use-module (crates-io))

(define-public crate-cloud-lib-0.1.0 (c (n "cloud-lib") (v "0.1.0") (d (list (d (n "cloud-macro") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0iwdj6lm5hxw4j92yg0bfyj4h7sz0n64y2zknpv5lzaja4a4mkxl") (f (quote (("json" "serde_json"))))))

