(define-module (crates-io cl ou cloudflare-calls-rs) #:use-module (crates-io))

(define-public crate-cloudflare-calls-rs-0.1.0 (c (n "cloudflare-calls-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "037f9637fln3djq9hnnyx61mkjr1w65cs3f6i295l71bhj7pk8gz")))

(define-public crate-cloudflare-calls-rs-1.0.0 (c (n "cloudflare-calls-rs") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1d0wmpji8w6wd938dv1ihrjn86y803dq5b3v6s2b7hpjf5clzziy")))

(define-public crate-cloudflare-calls-rs-1.0.1 (c (n "cloudflare-calls-rs") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0hp8r5qh8x3wjgy1q7ycddmkgcmn8rg5dwm7miy165grk3ivw6nl")))

