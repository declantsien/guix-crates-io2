(define-module (crates-io cl ou cloudwatch_logger) #:use-module (crates-io))

(define-public crate-cloudwatch_logger-0.1.0 (c (n "cloudwatch_logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rusoto_core") (r "^0.34.0") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.34.0") (d #t) (k 0)))) (h "0sf2g8kb5qg0l8x3j33z60wwa8wn8wpwrdnm47pnl1knmv15fij5") (y #t)))

(define-public crate-cloudwatch_logger-0.1.1 (c (n "cloudwatch_logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rusoto_core") (r "^0.34.0") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.34.0") (d #t) (k 0)))) (h "05hk50vhj51jyqjq0pr3sl2943iiiml9cacmi406qnwkkxhbpl6d") (y #t)))

(define-public crate-cloudwatch_logger-0.1.2 (c (n "cloudwatch_logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rusoto_core") (r "^0.34.0") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.34.0") (d #t) (k 0)))) (h "12fg91knkml30gl96l4bkpgw6zhzi0m2yak5f59zlxjqsqs0pdla") (y #t)))

