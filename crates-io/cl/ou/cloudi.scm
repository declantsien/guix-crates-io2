(define-module (crates-io cl ou cloudi) #:use-module (crates-io))

(define-public crate-cloudi-0.1.0 (c (n "cloudi") (v "0.1.0") (d (list (d (n "erlang_rs") (r "^0.2.0") (d #t) (k 0)))) (h "1993k3zdn9lb50cvqw24m0xs4sasg8ixazvhg8a5rii0iwx062ig") (r "1.66.1")))

(define-public crate-cloudi-0.2.0 (c (n "cloudi") (v "0.2.0") (d (list (d (n "erlang_rs") (r "^0.2.0") (d #t) (k 0)))) (h "1ipdv3cj6sbxz0wl0cp4rmz0zibyl4vqpbxp8ibjzvwy50ya8p3h") (r "1.66.1")))

(define-public crate-cloudi-2.0.6 (c (n "cloudi") (v "2.0.6") (d (list (d (n "erlang_rs") (r "^2.0.6") (d #t) (k 0)))) (h "14icpwj23whka1h50k0iwpr3vssri7bq96cvjmn3pj2mimxqvmz5") (r "1.66.1")))

(define-public crate-cloudi-2.0.7 (c (n "cloudi") (v "2.0.7") (d (list (d (n "erlang_rs") (r "^2.0.7") (d #t) (k 0)))) (h "02sbf8ljhw6scyhnp2shqba2hdx81661xg69xig3mzpzm384zx0l") (r "1.66.1")))

