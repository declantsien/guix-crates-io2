(define-module (crates-io cl ic click_captcha) #:use-module (crates-io))

(define-public crate-click_captcha-0.1.0 (c (n "click_captcha") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg2webp") (r "^0.1.3") (d #t) (k 0)))) (h "0ykx4nk33sq3ss05crw5hscv20hlmjwwp13i0c2k2ka42dvz7zyl")))

(define-public crate-click_captcha-0.1.1 (c (n "click_captcha") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg2webp") (r "^0.1.3") (d #t) (k 0)))) (h "132325vqmzfpidkidhdw8mr4qr73xkf1z7d76bj1y9qjjnydwpdg")))

(define-public crate-click_captcha-0.1.2 (c (n "click_captcha") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg2webp") (r "^0.1.3") (d #t) (k 0)))) (h "1jf6bm4agr9c0ijf5w3sx6mn34yhkjxs889024qmiqvfcjlhz9yz")))

(define-public crate-click_captcha-0.1.3 (c (n "click_captcha") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg2webp") (r "^0.1.3") (d #t) (k 0)))) (h "03hl4lix5g3ipqh7r11xqvl1pmpw9gw7490jlv42znjk689m5axi")))

(define-public crate-click_captcha-0.1.6 (c (n "click_captcha") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg2webp") (r "^0.1.3") (d #t) (k 0)))) (h "03kp2si6bfzmdyzmjsngd41q9awhhjm0rd129srlmamwnl3gj0q8")))

(define-public crate-click_captcha-0.1.7 (c (n "click_captcha") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg2webp") (r "^0.1.3") (d #t) (k 0)))) (h "0vz58311f8lc7w21pknx8swhknc2lyr297y02fq8kjaa8mzz5wc5")))

(define-public crate-click_captcha-0.1.8 (c (n "click_captcha") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg2webp") (r "^0.1.3") (d #t) (k 0)))) (h "1jvj005pgzzhsbr6y9j8r48rc4myxqk9n8w4s1hx4bsn73j72345")))

(define-public crate-click_captcha-0.1.10 (c (n "click_captcha") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg2webp") (r "^0.1.3") (d #t) (k 0)))) (h "08ssblq8gngbw1qg49glqngz8jzdmidf6gan5mgldj2ljba93b6h")))

(define-public crate-click_captcha-0.1.11 (c (n "click_captcha") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg2webp") (r "^0.1.3") (d #t) (k 0)))) (h "1wkk6nbk6d7fq4j5ks0xjwnksqyl5wc4q2z3pmb5ak6wrgx8gyyf")))

(define-public crate-click_captcha-0.1.15 (c (n "click_captcha") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg2webp") (r "^0.1.3") (d #t) (k 0)))) (h "1yiaqrkjirlva4zhklqm7wwz4v5igphfm7ng35vwjdy289lvxlnz")))

