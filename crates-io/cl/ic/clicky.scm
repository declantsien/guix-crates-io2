(define-module (crates-io cl ic clicky) #:use-module (crates-io))

(define-public crate-clicky-0.1.0 (c (n "clicky") (v "0.1.0") (d (list (d (n "evdev") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0yfwsdcd29xkx4mc61bvvb67gbcjbyxsij9cz49j62y97q9k9vzs")))

