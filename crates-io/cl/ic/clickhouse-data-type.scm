(define-module (crates-io cl ic clickhouse-data-type) #:use-module (crates-io))

(define-public crate-clickhouse-data-type-0.0.0 (c (n "clickhouse-data-type") (v "0.0.0") (h "02x9303cw4jm9r5axd87fj821njxiv4pg6f896w4bz55l1mpc94z")))

(define-public crate-clickhouse-data-type-0.1.0 (c (n "clickhouse-data-type") (v "0.1.0") (d (list (d (n "chrono-tz") (r "^0.5") (k 0)) (d (n "pest") (r "^2.1") (k 0)) (d (n "pest_derive") (r "^2.1") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1k28a12z6ilk2x63brmviw2aaa6isykxxs658c7v1psrnk3f4grh")))

(define-public crate-clickhouse-data-type-0.2.0 (c (n "clickhouse-data-type") (v "0.2.0") (d (list (d (n "chrono-tz") (r "^0.8") (k 0)) (d (n "pest") (r "^2.5") (f (quote ("std"))) (k 0)) (d (n "pest_derive") (r "^2.5") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0048pz3v6y7ihjpd1yjbzds01vwv0bc480zvdg2qqs1pbwirjh2q")))

