(define-module (crates-io cl ic cliclr) #:use-module (crates-io))

(define-public crate-cliclr-0.1.0 (c (n "cliclr") (v "0.1.0") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0784n24gwmhvyviyr78akif6n4nr7facyvdhr001ac153yqkz5jq")))

(define-public crate-cliclr-0.1.1 (c (n "cliclr") (v "0.1.1") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "1c32r67wg3yfj7y3rn21np26m5c6lc5lxhkdswkjfzphla5yrpgh")))

