(define-module (crates-io cl ic click7) #:use-module (crates-io))

(define-public crate-click7-0.1.0 (c (n "click7") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "08xk51nh6q4xi105yzd12djlcnzmhmf64fxzdbgijxx09zghd495")))

(define-public crate-click7-0.2.0 (c (n "click7") (v "0.2.0") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "redb") (r "^2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19c3cxg3h319jx5zzz0sh7y26vfgad4k6scppp8dlcwl91h3v75j")))

