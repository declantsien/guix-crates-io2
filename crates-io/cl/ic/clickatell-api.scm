(define-module (crates-io cl ic clickatell-api) #:use-module (crates-io))

(define-public crate-clickatell-api-0.2.0 (c (n "clickatell-api") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "13nazkvbms4b3r3k9xbf62vap9shln9b26swy2l99j3ax72z16v6")))

(define-public crate-clickatell-api-0.2.1 (c (n "clickatell-api") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "079mn2570fk6alk6f2z617llsqkzw6nd7k3yhf6db6m372jaf0nb")))

(define-public crate-clickatell-api-0.3.0 (c (n "clickatell-api") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "15gwbaqfppy60m452hj5v5kp4lz35q0ym4if10lmy1knr40g8adk")))

