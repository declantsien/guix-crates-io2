(define-module (crates-io cl ic clicker-xdo) #:use-module (crates-io))

(define-public crate-clicker-xdo-0.1.0 (c (n "clicker-xdo") (v "0.1.0") (d (list (d (n "libxdo") (r "~0.6.0") (d #t) (k 0)))) (h "15y7js1hcjj43g6fk33gl174jiww8yrp1s5f18b0b6kip6vgy2hl")))

(define-public crate-clicker-xdo-0.1.1 (c (n "clicker-xdo") (v "0.1.1") (d (list (d (n "libxdo") (r "~0.6.0") (d #t) (k 0)))) (h "1l5dp01a96193hznp09579hq2jbwq82ipv80qbhawc5rb7n4sj4y")))

(define-public crate-clicker-xdo-0.1.2 (c (n "clicker-xdo") (v "0.1.2") (d (list (d (n "libxdo") (r "~0.6.0") (d #t) (k 0)))) (h "15aydkbgwn04ai105c4560zsqmbcy7rgfdras4f9bl6y32jhkxla")))

(define-public crate-clicker-xdo-0.1.3 (c (n "clicker-xdo") (v "0.1.3") (d (list (d (n "libxdo") (r "~0.6.0") (d #t) (k 0)))) (h "13j2v46vb1aaxfx7gzahfqzxr1sx1kjdnpya1awp2c4q7f0xybrx")))

(define-public crate-clicker-xdo-0.2.0 (c (n "clicker-xdo") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libxdo") (r "~0.6.0") (d #t) (k 0)))) (h "1flykfk87zbmsp0gr83005biiihsz528ikpxhl32qf573ijamr7f")))

(define-public crate-clicker-xdo-0.2.1 (c (n "clicker-xdo") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libxdo") (r "~0.6.0") (d #t) (k 0)))) (h "0n651xhvxnz3nlx5lga33ymmnqpm2n93plp6dda5b3gdn07a03a4")))

(define-public crate-clicker-xdo-0.2.2 (c (n "clicker-xdo") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libxdo") (r "~0.6.0") (d #t) (k 0)))) (h "0ya3kl5441kdsk3qmniyfl6bf73vixi1r7f365xh8568nlkj96s8")))

