(define-module (crates-io cl ic clickhouse-derive) #:use-module (crates-io))

(define-public crate-clickhouse-derive-0.1.0 (c (n "clickhouse-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nv8a7abfi8vb4gmf6lcirx7ycy05fhid1l5bg4kmilrpyrzka7r")))

(define-public crate-clickhouse-derive-0.1.1 (c (n "clickhouse-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "198y523j71vq78l8dc7w2r1fr32x9pmps3y7xq3wan28hljm9bqq")))

