(define-module (crates-io cl ic clickrs) #:use-module (crates-io))

(define-public crate-clickrs-0.1.0 (c (n "clickrs") (v "0.1.0") (d (list (d (n "clickrs_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1v7drk8b4m6swgv35csj7jlyd07z85pdfx07ba0xjracxynma0rx")))

(define-public crate-clickrs-0.1.1 (c (n "clickrs") (v "0.1.1") (d (list (d (n "clickrs_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "045mhlamilvwc75zp8hrscrsgmd3v84ay114d7j4dawqj62p6c6j")))

(define-public crate-clickrs-0.1.2 (c (n "clickrs") (v "0.1.2") (d (list (d (n "clickrs_proc_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0z07n21jp6ja081xi6r9gcxm49iqpfgzxy9009bh0xg5vzdilf8l")))

(define-public crate-clickrs-0.1.3 (c (n "clickrs") (v "0.1.3") (d (list (d (n "clickrs_proc_macro") (r "^0.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0lfcsns259lyh60b2zgr7blj8h87bd57pf2kjvaffdzvdkb2wgd7")))

(define-public crate-clickrs-0.1.4 (c (n "clickrs") (v "0.1.4") (d (list (d (n "clickrs_proc_macro") (r "^0.1.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.4") (d #t) (k 0)))) (h "00pl8j6mflnf8q692py2qcwm5mvzx36fyavxw5c4xvf5dnrrckpy")))

(define-public crate-clickrs-0.1.5 (c (n "clickrs") (v "0.1.5") (d (list (d (n "clickrs_proc_macro") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.4") (d #t) (k 0)))) (h "1ci212fsqlm9xrsxrgvhiydpyix3lsglrl65zx48v174gmqkdlz5")))

