(define-module (crates-io cl ic clickrs_proc_macro) #:use-module (crates-io))

(define-public crate-clickrs_proc_macro-0.1.0 (c (n "clickrs_proc_macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wigw99jx3r7c6x8jiaqzncz98kskifssnqblvfw1qykk6q7vpn7")))

(define-public crate-clickrs_proc_macro-0.1.1 (c (n "clickrs_proc_macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qzipby2r54gq8ichj85rv9v5fkjari4iwdrx4vrc79hvnzc83pj")))

(define-public crate-clickrs_proc_macro-0.1.2 (c (n "clickrs_proc_macro") (v "0.1.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mpv5q97j6d16x1j72b7y1p731976fyvn3c1bkx2m5y9ckkl1zb4")))

(define-public crate-clickrs_proc_macro-0.1.3 (c (n "clickrs_proc_macro") (v "0.1.3") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sgil8x3n0h3428h9hq7zzwq99955kmlkph87x0m8kly2z8q4slp")))

(define-public crate-clickrs_proc_macro-0.1.4 (c (n "clickrs_proc_macro") (v "0.1.4") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wiwvlldx960h0r1gnjzp2z18jipzqf9313abcf9zw62pbqczfg1")))

(define-public crate-clickrs_proc_macro-0.1.5 (c (n "clickrs_proc_macro") (v "0.1.5") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05n6fqwva3asmjpn85x6ldinr2lz1wqvyahjzysglc2zzvls4i79")))

