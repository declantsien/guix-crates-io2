(define-module (crates-io cl ic clickhouse-driver-cth) #:use-module (crates-io))

(define-public crate-clickhouse-driver-cth-0.1.0 (c (n "clickhouse-driver-cth") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ipin7wggqh09mzh2a90blv7cm6fivijxlzn4646pddslii20qim") (f (quote (("int128") ("default"))))))

