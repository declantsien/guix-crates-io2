(define-module (crates-io cl ic clickhouse-sql-parser) #:use-module (crates-io))

(define-public crate-clickhouse-sql-parser-0.1.0 (c (n "clickhouse-sql-parser") (v "0.1.0") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)))) (h "0v8k1mx1539v7agnq2s35d52zk1gjl4im00czj02xvrv5v90y6k9")))

(define-public crate-clickhouse-sql-parser-0.1.1 (c (n "clickhouse-sql-parser") (v "0.1.1") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)))) (h "05z6lhsvryc2ckbd1plg334rf0jh4y2bvrrmy9mpfx4zbpdkysxb")))

(define-public crate-clickhouse-sql-parser-0.1.2 (c (n "clickhouse-sql-parser") (v "0.1.2") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)))) (h "157zj8jqnbj01dfdqsqhh66h5vx9wmdgligsca0p0b6dz7cxmygz")))

