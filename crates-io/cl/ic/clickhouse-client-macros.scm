(define-module (crates-io cl ic clickhouse-client-macros) #:use-module (crates-io))

(define-public crate-clickhouse-client-macros-0.1.0 (c (n "clickhouse-client-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1zsm3aab500qkd4rlqvdq7m60rs2nxcr3mzibfhi3cx35wy1afbj")))

(define-public crate-clickhouse-client-macros-0.2.0 (c (n "clickhouse-client-macros") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0baifd5hxsginmh1wffb5vxr93k2pp886s60j5gqvmk6dgyjccvg")))

(define-public crate-clickhouse-client-macros-0.3.0 (c (n "clickhouse-client-macros") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0hzgw5m321hkhizhffmcnr53i3rr9bqfz1cjianidvy4k2cmsxr8")))

(define-public crate-clickhouse-client-macros-0.4.0 (c (n "clickhouse-client-macros") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0b5m10f41g49x39qvmxgi9zyd8k4pw262kbi83m364dy48m0ha87")))

(define-public crate-clickhouse-client-macros-0.5.0 (c (n "clickhouse-client-macros") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1gqgfsrb0qkam991kg8d75892gxzfrzxzmbxnzmvjwxam1z1iycb")))

(define-public crate-clickhouse-client-macros-0.6.0 (c (n "clickhouse-client-macros") (v "0.6.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0mydpqrh2gv90lszqzacbfnas82i02daflqrb4zkslqj53ggafwc")))

(define-public crate-clickhouse-client-macros-0.7.0 (c (n "clickhouse-client-macros") (v "0.7.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1rgk4l250wiivyc4ql0rmw9zdms1vf2ws9jhy1dl3k5zr20s96ms")))

(define-public crate-clickhouse-client-macros-0.8.0 (c (n "clickhouse-client-macros") (v "0.8.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0kk5pvi42zn2a3qbjdxr134fc94bphd6by3qbl5cmzzvwbiklfvg")))

(define-public crate-clickhouse-client-macros-0.11.0 (c (n "clickhouse-client-macros") (v "0.11.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0wb44lryn1h7g3zc8d3iyc52kwrkggay50ynyfw2v4yk75ldxdxf")))

(define-public crate-clickhouse-client-macros-0.12.0 (c (n "clickhouse-client-macros") (v "0.12.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0yb7z43mi54w1wy9j05s20l2hhyx1ahspp9sz5pjxwhlq2jh74zk")))

(define-public crate-clickhouse-client-macros-0.13.0 (c (n "clickhouse-client-macros") (v "0.13.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1zbz65xi4hmr3h7rl23r0yqa3crd6mnilmkinmqap65dwlrmb6n8")))

(define-public crate-clickhouse-client-macros-0.14.0 (c (n "clickhouse-client-macros") (v "0.14.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0ldzjw0mnq90jcdvxisphphg91z94xflzxx61fxyis45gm7sf3d8")))

(define-public crate-clickhouse-client-macros-0.15.0 (c (n "clickhouse-client-macros") (v "0.15.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0i22qgjpms4as046cwbi8jg6cp07w44jz73ha2sqz69fckbp3h6r")))

(define-public crate-clickhouse-client-macros-0.16.0 (c (n "clickhouse-client-macros") (v "0.16.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "13cslmfajfv9yipd8h336fs9hhfkjqr284ysbixrm37ggrq8j4hh") (y #t)))

(define-public crate-clickhouse-client-macros-0.17.0 (c (n "clickhouse-client-macros") (v "0.17.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "00n2b067y3cm3hbi28k3v61w3jpfxk4dagwrljmdjzpas7wkz1q7")))

