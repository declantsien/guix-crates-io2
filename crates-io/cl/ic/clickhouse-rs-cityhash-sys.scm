(define-module (crates-io cl ic clickhouse-rs-cityhash-sys) #:use-module (crates-io))

(define-public crate-clickhouse-rs-cityhash-sys-0.1.0 (c (n "clickhouse-rs-cityhash-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "01dv7vh6r0rm0f7swdr4q66s6c41zh2hz7qbnl2q1kahx8p28yrn") (l "clickhouse-rs")))

(define-public crate-clickhouse-rs-cityhash-sys-0.1.1 (c (n "clickhouse-rs-cityhash-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "1xyqhz2y1zzk1831m3kgk1k7pp58lv376fgxsl7jzn0k575nf6jm") (l "clickhouse-rs")))

(define-public crate-clickhouse-rs-cityhash-sys-0.1.2 (c (n "clickhouse-rs-cityhash-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "1fcjsnzpw0k3md76pxzi8m98laa35fpdczp102v6r3d2013rvbsb") (l "clickhouse-rs")))

