(define-module (crates-io cl ic clickhouse-driver-cthrs) #:use-module (crates-io))

(define-public crate-clickhouse-driver-cthrs-0.1.1 (c (n "clickhouse-driver-cthrs") (v "0.1.1") (d (list (d (n "clickhouse-driver-cth") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1cdz9i7l11w9ifsfspglp6ql1hh86i63h1y8420snlssyf8z25sm") (f (quote (("simd") ("default"))))))

