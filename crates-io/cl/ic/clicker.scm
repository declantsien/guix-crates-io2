(define-module (crates-io cl ic clicker) #:use-module (crates-io))

(define-public crate-clicker-0.1.0 (c (n "clicker") (v "0.1.0") (d (list (d (n "device_query") (r "^0.1.3") (d #t) (k 0)) (d (n "enigo") (r "^0.0.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0hf57imkkfzxp68a6rln7q7qy59gm24zcnp1mbfcw3k76g5pyy3l")))

(define-public crate-clicker-0.2.0 (c (n "clicker") (v "0.2.0") (d (list (d (n "device_query") (r "^0.1.3") (d #t) (k 0)) (d (n "enigo") (r "^0.0.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1w06rz87lzx0x4ns85jyrkn470q0hzqcsa3843vnagwvc3dsm7j2")))

(define-public crate-clicker-0.2.1 (c (n "clicker") (v "0.2.1") (d (list (d (n "device_query") (r "^0.1.3") (d #t) (k 0)) (d (n "enigo") (r "^0.0.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1x6jm71scn12zld2rns8a2j9dbqii3l77xnya94vhbmma7bflidz")))

(define-public crate-clicker-0.2.2 (c (n "clicker") (v "0.2.2") (d (list (d (n "device_query") (r "^0.1.3") (d #t) (k 0)) (d (n "enigo") (r "^0.0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0zmp22xsaihf0b1cnk9k6b2iy5vla6hnaj3kvd4270qkk7fll4p6")))

(define-public crate-clicker-0.3.0 (c (n "clicker") (v "0.3.0") (d (list (d (n "device_query") (r "^0.1.3") (d #t) (k 0)) (d (n "enigo") (r "^0.0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1bn3s1wxm6rg2la0sm43crlyhrw3r5k103gblrx4yh1d6g2a35ic")))

(define-public crate-clicker-0.3.1 (c (n "clicker") (v "0.3.1") (d (list (d (n "device_query") (r "^0.1.3") (d #t) (k 0)) (d (n "enigo") (r "^0.0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0c0gnqqznylrcjdii9x01rvirvp0xa0gvgyjjnnqxj94sch2gq54")))

(define-public crate-clicker-0.4.0 (c (n "clicker") (v "0.4.0") (d (list (d (n "device_query") (r "^0.1.3") (d #t) (k 0)) (d (n "enigo") (r "^0.0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1k0zk6myhqfp0h0zg619f57gjv22s0hc3psw9n84149x8fvphnc2")))

(define-public crate-clicker-0.4.1 (c (n "clicker") (v "0.4.1") (d (list (d (n "device_query") (r "^0.1.3") (d #t) (k 0)) (d (n "enigo") (r "^0.0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0sgx6a8kvx6jy3dl26pqaxapyqihg2cdr3f7fag5g51rmjsvlgp5")))

(define-public crate-clicker-0.5.0 (c (n "clicker") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "inputbot") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1mnsa8fza5yriabfinzr4nk74pc9nk88bbfchzl9qmi53w2cnx1n")))

