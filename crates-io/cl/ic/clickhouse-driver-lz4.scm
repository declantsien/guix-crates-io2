(define-module (crates-io cl ic clickhouse-driver-lz4) #:use-module (crates-io))

(define-public crate-clickhouse-driver-lz4-0.1.0 (c (n "clickhouse-driver-lz4") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "19il4hdg26iknyqvjbl6xnsifydvzwl47d5g7mc6hxlvsqv8a3jq") (l "clickhouse-driver")))

