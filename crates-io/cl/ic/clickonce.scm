(define-module (crates-io cl ic clickonce) #:use-module (crates-io))

(define-public crate-clickonce-0.0.1 (c (n "clickonce") (v "0.0.1") (h "095hjypapsf3jjyfnw03qwxs22hxrxxx5kdvqjh5s4ams6x44hyf") (r "1.56")))

(define-public crate-clickonce-0.0.2 (c (n "clickonce") (v "0.0.2") (h "1p4zpb9yhc7m4ygndsj8d832bii47fs6wy83qbbhd50crsb64v7g") (r "1.56")))

(define-public crate-clickonce-0.0.3 (c (n "clickonce") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)))) (h "17inxay6pnqy86dgkxxqrrvaf6vxmrjjmpgzh037hqqmyni5cyc0") (r "1.56")))

