(define-module (crates-io cl ic clicsv) #:use-module (crates-io))

(define-public crate-clicsv-0.1.4 (c (n "clicsv") (v "0.1.4") (d (list (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "01ibmdvb38vzbnpwc8app2hp4px5d7abrqpkmvx5l5kxr2n9kd4f")))

