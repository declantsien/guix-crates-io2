(define-module (crates-io cl ic clickhouse-data-value) #:use-module (crates-io))

(define-public crate-clickhouse-data-value-0.0.0 (c (n "clickhouse-data-value") (v "0.0.0") (h "0qcz7kcyailmlx0q0f65d1v9vrxb88nz563mssb32863s6jh9nap")))

(define-public crate-clickhouse-data-value-0.1.0 (c (n "clickhouse-data-value") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "pest") (r "^2.1") (k 0)) (d (n "pest_derive") (r "^2.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "0r1vp9i59br6wqk369hcm7dw696m3ss77p4kjsnrfipvxys7b7lk") (f (quote (("with-datetime64" "chrono") ("with-datetime" "chrono") ("with-date" "chrono") ("with-all" "with-date" "with-datetime" "with-datetime64") ("default" "with-all"))))))

(define-public crate-clickhouse-data-value-0.1.1 (c (n "clickhouse-data-value") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.1") (k 0)) (d (n "pest_derive") (r "^2.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "17d4bk1sxi98jlbsdgyb7wigsri49pd7yvx989cvxm6v6dryibsf")))

(define-public crate-clickhouse-data-value-0.2.0 (c (n "clickhouse-data-value") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.1") (k 0)) (d (n "pest_derive") (r "^2.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "0ibxnmm1lqqvv5zpxpxd28pj8idgh7i3165pci90dr0xld3gac6f")))

(define-public crate-clickhouse-data-value-0.3.0 (c (n "clickhouse-data-value") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.5") (f (quote ("std"))) (k 0)) (d (n "pest_derive") (r "^2.5") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1c1n1r5akp7lvw12scqgdlgrwnmgfn4gy632h43y1y8cdsjmg395")))

(define-public crate-clickhouse-data-value-0.3.1 (c (n "clickhouse-data-value") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "pest") (r "^2.5") (f (quote ("std"))) (k 0)) (d (n "pest_derive") (r "^2.5") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1") (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0axmcl1qybr70ip5i145wvk3g2d5id2dg1553p9pzf4169mfvqlc")))

