(define-module (crates-io cl au clausen) #:use-module (crates-io))

(define-public crate-clausen-1.0.0 (c (n "clausen") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polylog") (r "^2.5.3") (d #t) (k 0)))) (h "1c9z5kzi7mfy9r4f30qa1wfmrisf1pyzbj5hm4g611k6bv1q6k3v")))

(define-public crate-clausen-1.0.1 (c (n "clausen") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polylog") (r "^2.5.3") (d #t) (k 0)))) (h "01n4vvffmpp226dx2vaxnqmgnkp50fxwfniawaxff8zr7qn5kmzg")))

