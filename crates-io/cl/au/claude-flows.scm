(define-module (crates-io cl au claude-flows) #:use-module (crates-io))

(define-public crate-claude-flows-0.1.0 (c (n "claude-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0fafgv7l0iv97h8h24jbi8mryx4xhh4l3r7smny9w97nvi1jws3q")))

(define-public crate-claude-flows-0.1.1 (c (n "claude-flows") (v "0.1.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "08f9mlpa2nyhichadshb8wcm904vd1vc5fgdw9v6cs7xzlcc6kmj")))

(define-public crate-claude-flows-0.1.2 (c (n "claude-flows") (v "0.1.2") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "19v5300rmyan65grkh45d5fr1p9hxwfw29rgs5d8z75xdcsfvz74")))

(define-public crate-claude-flows-0.1.3 (c (n "claude-flows") (v "0.1.3") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0mghzw5spkh1h25xykvqw453l7y03qvdz9aa2x2krri48cvy79jv")))

(define-public crate-claude-flows-0.1.4 (c (n "claude-flows") (v "0.1.4") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0wmvhbcdijlrgn99v598mvn7g195mfzaq2bcvfyy8q9ihz0sd93l")))

(define-public crate-claude-flows-0.1.5 (c (n "claude-flows") (v "0.1.5") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1fb7i4fiagy4ayrl34iaxxpdgc83fjkb080jl5f0niida2n9724c")))

(define-public crate-claude-flows-0.1.6 (c (n "claude-flows") (v "0.1.6") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "17bkihlz8hab274bln0a6vjcwxn8vcqs0lbmjfq4jvdq2b985c1i")))

(define-public crate-claude-flows-0.2.0 (c (n "claude-flows") (v "0.2.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0llpyi44yjhpc8nc3jxzdn481pjhgxmwkcbjbzys5ibjja8nx0v3")))

(define-public crate-claude-flows-0.2.1 (c (n "claude-flows") (v "0.2.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1gjh7hih144vdaai0rha8zwdlpkx570qk7vjq0q83l65ysjqsg5c")))

