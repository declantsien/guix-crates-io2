(define-module (crates-io cl au claude) #:use-module (crates-io))

(define-public crate-claude-0.1.0 (c (n "claude") (v "0.1.0") (d (list (d (n "regex") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0qjpmcayiz7a482lbfh2h22ym7spw3ak3id3jk9x9zd5125m94m2") (f (quote (("parsing" "regex"))))))

(define-public crate-claude-0.2.0 (c (n "claude") (v "0.2.0") (d (list (d (n "regex") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "15a8fffrj2zxgwyy1rf8842d2rnzp5fcqhh37bh7y422iq4r29q6") (f (quote (("serialization" "serde" "serde_json" "serde_derive") ("parsing" "regex") ("default"))))))

(define-public crate-claude-0.3.0 (c (n "claude") (v "0.3.0") (d (list (d (n "regex") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "198klbnm60ra2slqccfdwly5sgihpqbcq7fiv3b83bpgvdrrxh9j") (f (quote (("serialization" "serde" "serde_json" "serde_derive") ("parsing" "regex") ("default"))))))

(define-public crate-claude-0.3.1 (c (n "claude") (v "0.3.1") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0pqzs1cpqk2ijlrxagaipkvdsyqryn0nvfz8g0n2z86pgspqh4ml") (f (quote (("serialization" "serde" "serde_json" "serde_derive") ("parsing" "regex") ("default"))))))

