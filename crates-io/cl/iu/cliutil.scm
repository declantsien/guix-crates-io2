(define-module (crates-io cl iu cliutil) #:use-module (crates-io))

(define-public crate-cliutil-0.0.1-alpha (c (n "cliutil") (v "0.0.1-alpha") (h "1l8zfxl1l5vpj1wnzcblmswfbz2y19xgrpm7qr88z7xfx8afz2gk") (y #t)))

(define-public crate-cliutil-0.0.2 (c (n "cliutil") (v "0.0.2") (h "0pz0kgcnzmn7nk9mwz1ipfsr9vm5aziix4mdszr1njzwiqi8gyl8") (y #t)))

(define-public crate-cliutil-0.1.0-alpha.0 (c (n "cliutil") (v "0.1.0-alpha.0") (h "01ja34827x801hlgrhxfra0n59xj0m5y1wy36nq7rzf0h0i377wg")))

