(define-module (crates-io cl gi clgit) #:use-module (crates-io))

(define-public crate-clgit-0.1.0 (c (n "clgit") (v "0.1.0") (h "090vcphd3fn7qskg825nj1md6h8vfj7cqgx85a1my5hxc912awj1")))

(define-public crate-clgit-0.1.1 (c (n "clgit") (v "0.1.1") (h "06bslfx2ppyw5sn6zffjx7nqc7bnbdg2vjsx4irhdr5h14kp2b2b")))

