(define-module (crates-io cl ot cloth_bumpmap) #:use-module (crates-io))

(define-public crate-cloth_bumpmap-0.1.0 (c (n "cloth_bumpmap") (v "0.1.0") (d (list (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1h6a7xgv4g73r8p19fzji4ij8cyfz1wj052h0qsws3i0hhrdgvzy")))

(define-public crate-cloth_bumpmap-0.1.1 (c (n "cloth_bumpmap") (v "0.1.1") (d (list (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1xp1fs5gxb5zbyc29qk64izc6idhqkc8pycky4lf83hm9idkm9sb")))

(define-public crate-cloth_bumpmap-0.1.2 (c (n "cloth_bumpmap") (v "0.1.2") (d (list (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.11") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)))) (h "01b09pdwiy9nb2dlr9bxrayzmasikvcllnbaccfg4hp9zil2c90i")))

(define-public crate-cloth_bumpmap-0.1.3 (c (n "cloth_bumpmap") (v "0.1.3") (d (list (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)))) (h "1phqy7hswafh2rgksrkkacy65kwhxg10c2yl9sh12i6cgnb370ir")))

(define-public crate-cloth_bumpmap-0.1.4 (c (n "cloth_bumpmap") (v "0.1.4") (d (list (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)))) (h "045yvfhlfsjnpb1rbmv7rfs71lm7rwhnjn089k3c5kppnjvfz32m")))

(define-public crate-cloth_bumpmap-0.1.6 (c (n "cloth_bumpmap") (v "0.1.6") (d (list (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "05jnvl8zzylvmzfds00zz2nj6ykzhsfy3l2z9yn5hrd3afzjzany")))

