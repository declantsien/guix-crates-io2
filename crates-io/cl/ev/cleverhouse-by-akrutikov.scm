(define-module (crates-io cl ev cleverhouse-by-akrutikov) #:use-module (crates-io))

(define-public crate-cleverhouse-by-akrutikov-0.1.0 (c (n "cleverhouse-by-akrutikov") (v "0.1.0") (h "08acmi1glgvjdfc02vnnjxkj4iwjkblxyryf2dp2sdxmw0d91qm1")))

(define-public crate-cleverhouse-by-akrutikov-0.1.1 (c (n "cleverhouse-by-akrutikov") (v "0.1.1") (h "1xcz9q4xlvwlih181j16r13i8302mn3l2rfzmm2yc9f8rfcv9598")))

(define-public crate-cleverhouse-by-akrutikov-0.1.2 (c (n "cleverhouse-by-akrutikov") (v "0.1.2") (h "0xq71mj3axvxqcv56gvgbqkr5m7b1r3h8s8a8493ahirdijnw0pw")))

(define-public crate-cleverhouse-by-akrutikov-0.1.3 (c (n "cleverhouse-by-akrutikov") (v "0.1.3") (h "0kkdxhzb6qbxhx55nqjpa19k3zrhrs9hn4g6qg3rwsp40ygklqa0")))

(define-public crate-cleverhouse-by-akrutikov-0.1.4 (c (n "cleverhouse-by-akrutikov") (v "0.1.4") (h "19j4i02jwlpyhfdlnvqrqrj67s00birrdg5zg26jjr2xzawh5qjx")))

(define-public crate-cleverhouse-by-akrutikov-0.1.5 (c (n "cleverhouse-by-akrutikov") (v "0.1.5") (h "04caikwnlfrmk3gzgxczwhv12npr6daf98amzz75cqpv8xprrj9c")))

(define-public crate-cleverhouse-by-akrutikov-0.1.6 (c (n "cleverhouse-by-akrutikov") (v "0.1.6") (h "0hsv8kxwfps4xlp9awsvxyidzl8z6pp3h13l5fgiad9f3pxadwjq")))

(define-public crate-cleverhouse-by-akrutikov-0.1.7 (c (n "cleverhouse-by-akrutikov") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "167x6wsi5xx9i2am8y85vp1h3wisyf3s9vj6ry0ld89678zm4y8z")))

(define-public crate-cleverhouse-by-akrutikov-0.1.8 (c (n "cleverhouse-by-akrutikov") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("net" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "10m5if653s90dxw67nrp86yial7x30vjn0yvg2pmsj43dy53d543")))

