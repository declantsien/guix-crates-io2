(define-module (crates-io cl ev clevert) #:use-module (crates-io))

(define-public crate-clevert-0.8.1 (c (n "clevert") (v "0.8.1") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shared_child") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0kgcvwzna3d22yagfypavja20jv6nk42mv8pi2pll50b26qafmcc")))

