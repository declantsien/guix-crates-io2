(define-module (crates-io cl ev clever-graphics) #:use-module (crates-io))

(define-public crate-clever-graphics-0.1.0 (c (n "clever-graphics") (v "0.1.0") (d (list (d (n "core-graphics") (r "^0.22") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "objc_exception") (r "^0.1") (d #t) (k 0)) (d (n "orphan-crippler") (r "^0.1.2") (d #t) (k 0)))) (h "0p6h2nqasp2bmh4bmi7v9i53s2k743508sq5np7rdr24x7spdmgw") (f (quote (("parking_lot" "orphan-crippler/parking_lot"))))))

(define-public crate-clever-graphics-0.1.1 (c (n "clever-graphics") (v "0.1.1") (d (list (d (n "core-graphics") (r "^0.22") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "objc_exception") (r "^0.1") (d #t) (k 0)) (d (n "orphan-crippler") (r "^0.1.2") (d #t) (k 0)))) (h "0xnqw621p65njmvksvrl4g2klmcw44v65h579kqnjczcx7d6aw5f") (f (quote (("parking_lot" "orphan-crippler/parking_lot"))))))

