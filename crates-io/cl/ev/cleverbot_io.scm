(define-module (crates-io cl ev cleverbot_io) #:use-module (crates-io))

(define-public crate-cleverbot_io-1.0.0 (c (n "cleverbot_io") (v "1.0.0") (d (list (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "url") (r "^0.5.5") (d #t) (k 0)))) (h "0iry2b617kmv49qcssxas4yy2dg4d8lh43458zzdhyaz7p1a7q08")))

(define-public crate-cleverbot_io-1.0.1 (c (n "cleverbot_io") (v "1.0.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "1qcv7zhqliz37ldl76kabxqdd875i41b8wi8sdjpxchkf33g1273")))

