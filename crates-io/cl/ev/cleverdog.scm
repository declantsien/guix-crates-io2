(define-module (crates-io cl ev cleverdog) #:use-module (crates-io))

(define-public crate-cleverdog-0.1.0 (c (n "cleverdog") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rmpv") (r "^0.4") (d #t) (k 0)) (d (n "rustls") (r "^0.15") (d #t) (k 0)) (d (n "webpki") (r "^0.19") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (d #t) (k 0)))) (h "18qr419kw0qrmd627wdi58gj73vl1f4gi1jyq7zj0vs7x24k65h8")))

(define-public crate-cleverdog-0.1.1 (c (n "cleverdog") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rmpv") (r "^0.4") (d #t) (k 2)) (d (n "rustls") (r "^0.15") (d #t) (k 2)) (d (n "webpki") (r "^0.19") (d #t) (k 2)) (d (n "webpki-roots") (r "^0.16") (d #t) (k 2)))) (h "0q2kmqdcfndmwg8h2mida3av0knpk00pq8yg6drai6vl3m5ybb56")))

