(define-module (crates-io cl us clust-rs) #:use-module (crates-io))

(define-public crate-clust-rs-0.1.0 (c (n "clust-rs") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "kdtree") (r "^0.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0n315fsbj2pl62ilny0qgb63f2ahsrk1v37pxmx0l9qavqai20yj") (y #t)))

