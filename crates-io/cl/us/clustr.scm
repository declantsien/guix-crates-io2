(define-module (crates-io cl us clustr) #:use-module (crates-io))

(define-public crate-clustr-0.1.0 (c (n "clustr") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "edit-distance") (r "^2.1.0") (d #t) (k 0)) (d (n "fast-math") (r "^0.1.1") (d #t) (k 0)))) (h "1njaphca7cfa54acjiyxhr23m7rmp6qlivbp8zpn09fqhvgkgp4v") (y #t)))

(define-public crate-clustr-0.1.1 (c (n "clustr") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "edit-distance") (r "^2.1.0") (d #t) (k 0)) (d (n "fast-math") (r "^0.1.1") (d #t) (k 0)))) (h "0mh0i2irq92qfvj4vhq6kr7kd6jhyhmh1q52y9vdfk2lvjnim61m")))

(define-public crate-clustr-0.1.2 (c (n "clustr") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "edit-distance") (r "^2.1.0") (d #t) (k 0)) (d (n "fast-math") (r "^0.1.1") (d #t) (k 0)))) (h "0cgj22bdmpgxrswza7kgfdzcj0flf9b7bjv76h9jils9b9dmb0cj")))

