(define-module (crates-io cl us clust_macros) #:use-module (crates-io))

(define-public crate-clust_macros-0.7.0 (c (n "clust_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "10swgm2m995aycx6lkbdvd9zfh240jbscbwby5npx686zpc11y5z") (r "1.76")))

(define-public crate-clust_macros-0.8.0 (c (n "clust_macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0y6qmv4l850v1hcb608az0z6xk3f9s9fx5kdz1xwjbzn8wds8ijb") (r "1.76")))

