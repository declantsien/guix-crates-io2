(define-module (crates-io cl us clustering) #:use-module (crates-io))

(define-public crate-clustering-0.1.0 (c (n "clustering") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1spwbha7lwgk6xc1by5g2brqpkmnrp65drlx5jspvxb3h0sb291q")))

(define-public crate-clustering-0.2.0 (c (n "clustering") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (o #t) (d #t) (k 0)))) (h "14205v40m9mjj6pdlnw2fraxpsgd3q51jdvxp8xxjhvbjf4n0zgf") (s 2) (e (quote (("parallel" "dep:rayon") ("logging" "dep:log" "dep:env_logger" "dep:lazy_static"))))))

(define-public crate-clustering-0.2.1 (c (n "clustering") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (o #t) (d #t) (k 0)))) (h "17bwac4q8ifq4icxfcn4isdx982dgfda2bmklb0d3wlw0x90gzk3") (s 2) (e (quote (("parallel" "dep:rayon") ("logging" "dep:log" "dep:env_logger" "dep:lazy_static"))))))

