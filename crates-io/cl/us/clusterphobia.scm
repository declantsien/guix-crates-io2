(define-module (crates-io cl us clusterphobia) #:use-module (crates-io))

(define-public crate-clusterphobia-0.1.0 (c (n "clusterphobia") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "hilbert") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 0)))) (h "0xx0wsin13awv6ripnna2097cb2x1n41r8qng14s3r3dqf2cz04x")))

