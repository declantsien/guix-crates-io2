(define-module (crates-io cl is clisearch) #:use-module (crates-io))

(define-public crate-clisearch-0.1.0 (c (n "clisearch") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)))) (h "1lryg0zb5j1hfxn0m7b1hgscr0xa1bmi12pq4qjag8w702q519vi")))

(define-public crate-clisearch-0.2.0 (c (n "clisearch") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)))) (h "0vb2sh2j066w83mya7nj0wx329ryibk9zxb8f4xb9amw5fpzli3g")))

