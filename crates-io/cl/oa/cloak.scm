(define-module (crates-io cl oa cloak) #:use-module (crates-io))

(define-public crate-cloak-0.1.0 (c (n "cloak") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "open") (r "^1.2.1") (d #t) (k 0)) (d (n "ring") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1cc7hfpl56lkwdf0zkszc58h9xxr2dc287p82qch17swxmilw9mg")))

(define-public crate-cloak-0.2.0 (c (n "cloak") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "open") (r "^1.2.1") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0k50nwb0q3ciqnpv3fjvrjms5lx7y5vvibbgdrfclnsrq9xba7gs")))

