(define-module (crates-io cl if cliff) #:use-module (crates-io))

(define-public crate-cliff-0.1.0 (c (n "cliff") (v "0.1.0") (h "045gn0zl3zr57pn6nwf9lrplq4zhv2yvm3iv8cc48afh3z6hn5w8")))

(define-public crate-cliff-0.2.0 (c (n "cliff") (v "0.2.0") (h "0d67ycdwwn4k7azxys1xfwwkx4r3mml8yxsa8p7v6p5bmqan2vzq")))

(define-public crate-cliff-0.3.0 (c (n "cliff") (v "0.3.0") (h "10hflgy47wd20fgyifix35762cpr6vhhmhvj35jw1s5c167wf6g7")))

(define-public crate-cliff-0.3.1 (c (n "cliff") (v "0.3.1") (h "1qknjarsadmfmiyvrxc1syvwz4sv9gjzmbbrzlvz5fdpnjhjg2n4")))

(define-public crate-cliff-0.3.2 (c (n "cliff") (v "0.3.2") (h "0xhnnfk30xgy6dlr6sqx794vbx40w0xb5snpyh2spwrw1vrsbxcy")))

