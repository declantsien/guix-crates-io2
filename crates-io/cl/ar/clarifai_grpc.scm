(define-module (crates-io cl ar clarifai_grpc) #:use-module (crates-io))

(define-public crate-clarifai_grpc-0.0.1 (c (n "clarifai_grpc") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "06fyswgsk61cy27pjrl1zw2sfmiznmaa5wqkcf0vaqisp41pm4zc")))

(define-public crate-clarifai_grpc-0.0.2 (c (n "clarifai_grpc") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "0za38w1yfqxw0iw70sgxpypa3sfcmdm5km1ljsvl25zj7xj0czkn")))

(define-public crate-clarifai_grpc-7.2.0 (c (n "clarifai_grpc") (v "7.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "0x335dgrz5qxxqk6inii53f44ss6zk9m6vcqv9hb6wbk2y9sz5yd")))

(define-public crate-clarifai_grpc-7.4.0 (c (n "clarifai_grpc") (v "7.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "1cxigmy43y0x2jj1d22jccd4rlb7h0qxbwk93hvs8jq12n8s1l55")))

(define-public crate-clarifai_grpc-7.6.0 (c (n "clarifai_grpc") (v "7.6.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "0g4absijc5hlkf1xm80f2rg9k3x2bl0jc03pn848nynb6pdxpd66")))

(define-public crate-clarifai_grpc-7.7.0 (c (n "clarifai_grpc") (v "7.7.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "01xjkqyhlkd0jb83ziz6g65c0qkcc84qp2valz96kd2pslqm2dhc")))

(define-public crate-clarifai_grpc-7.8.0 (c (n "clarifai_grpc") (v "7.8.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "04xpll2g8amcfiax6ylxcakmbsp2srsq096dm7kyrjsjs17z9av2")))

(define-public crate-clarifai_grpc-7.9.0 (c (n "clarifai_grpc") (v "7.9.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "1wk0znhvmzpj1d15r4jxz9q4v4l2z8zsci5bwz27lh00hqlyp0jv")))

(define-public crate-clarifai_grpc-7.10.0 (c (n "clarifai_grpc") (v "7.10.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "103fa5wh35kp6xqzzyjaibxpd097phwcy49r3mdgw08incswlafs")))

(define-public crate-clarifai_grpc-7.11.0 (c (n "clarifai_grpc") (v "7.11.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "00cvldgb5rhw68xq47hpi9yc6cbm6p08c8whwzdpqvr1akvq04gi")))

(define-public crate-clarifai_grpc-8.0.0 (c (n "clarifai_grpc") (v "8.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "0y6x5hnshy91yvvilkpm3cy1v92gs83qnwb7c6argq4ji7kigqd2")))

