(define-module (crates-io cl ar clara-api) #:use-module (crates-io))

(define-public crate-clara-api-0.0.0-reserved (c (n "clara-api") (v "0.0.0-reserved") (d (list (d (n "jsonrpsee") (r "^0.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z115l2w93khi625bqpc72gmyx1fbv0c6d19d0z760qz4nbmgmgj") (r "1.75.0")))

