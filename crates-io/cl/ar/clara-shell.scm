(define-module (crates-io cl ar clara-shell) #:use-module (crates-io))

(define-public crate-clara-shell-0.1.0 (c (n "clara-shell") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "subst") (r "^0.1.2") (d #t) (k 0)))) (h "09847005sia7qn2hzhy4pg8yyp10awl1zfyrwh0cfv3ps2mzaypv")))

(define-public crate-clara-shell-0.1.1 (c (n "clara-shell") (v "0.1.1") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.8.0") (d #t) (k 0)) (d (n "subst") (r "^0.1.2") (d #t) (k 0)))) (h "0rgyy5far1brngr9cwypgfz1paxqxk634whlqjj2hz0slyk2zb8i")))

