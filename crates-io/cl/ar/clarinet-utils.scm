(define-module (crates-io cl ar clarinet-utils) #:use-module (crates-io))

(define-public crate-clarinet-utils-1.0.0 (c (n "clarinet-utils") (v "1.0.0") (d (list (d (n "hmac") (r "^0.12.0") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.11.0") (f (quote ("simple"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.0") (d #t) (k 0)))) (h "0g6gxsv1qh3vj1mhvlxgfqq6wjy5mq4snljh2zxp7zissm0376zi")))

