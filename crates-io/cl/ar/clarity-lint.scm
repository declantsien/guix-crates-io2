(define-module (crates-io cl ar clarity-lint) #:use-module (crates-io))

(define-public crate-clarity-lint-0.1.0 (c (n "clarity-lint") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clarity-repl") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "16cgarfzlgniw5w8lz3zxl5fbfyj1jlr109sg8xirf4xp6c0c0zm")))

(define-public crate-clarity-lint-0.1.1 (c (n "clarity-lint") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clarity-repl") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "06zib1zvqvj8m3zxyb1lajvmsyd3m8hmw5045rizck6j14k4xr1k")))

