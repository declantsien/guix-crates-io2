(define-module (crates-io cl ar clarion) #:use-module (crates-io))

(define-public crate-clarion-0.1.0 (c (n "clarion") (v "0.1.0") (d (list (d (n "time") (r "^0.3.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "1ddawzaqvk97jzk9d9jhbfhaayjhb1nzb6rwsvymf8hgqibcld47")))

(define-public crate-clarion-0.1.1 (c (n "clarion") (v "0.1.1") (d (list (d (n "time") (r "^0.3.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "1y6n64zw271a489hpdz7g0j68vcn7mp4a50bz8s64hg1s2ghzxq4")))

(define-public crate-clarion-0.1.2 (c (n "clarion") (v "0.1.2") (d (list (d (n "time") (r "^0.3.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "1llxdkdxwwpk01la9s1xk4m24gx9f97dmm7mgxj7hjbdi0qa89w8")))

