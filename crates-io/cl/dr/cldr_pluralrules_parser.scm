(define-module (crates-io cl dr cldr_pluralrules_parser) #:use-module (crates-io))

(define-public crate-cldr_pluralrules_parser-0.1.0 (c (n "cldr_pluralrules_parser") (v "0.1.0") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "1y4jm57zivx1sf1dlrjg7pg4rfj78mlbgchmwvjpdrycb0p04y2m")))

(define-public crate-cldr_pluralrules_parser-1.0.0 (c (n "cldr_pluralrules_parser") (v "1.0.0") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "1gb2w9xb9lczr5yq6xmrsk63hxqabmpl1irwbixwk7fzwp0gf1sf")))

(define-public crate-cldr_pluralrules_parser-1.0.1 (c (n "cldr_pluralrules_parser") (v "1.0.1") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "13k0fyyixznrxd7caxlrmv79sys3n44k2l1yac9l21bqnnyy9jbk")))

(define-public crate-cldr_pluralrules_parser-2.0.0 (c (n "cldr_pluralrules_parser") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "13vm7lq1lzpwpi4rggfbrf9ci059168w68kx486srh3dfwaxlqxc")))

