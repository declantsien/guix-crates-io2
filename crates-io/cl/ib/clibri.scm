(define-module (crates-io cl ib clibri) #:use-module (crates-io))

(define-public crate-clibri-0.1.0 (c (n "clibri") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1hsl7c8gzrbj7dg7j1kr39zqwvcqilrxs7p5dn87vn5fkasrnr19")))

(define-public crate-clibri-0.1.1 (c (n "clibri") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "11sdviqc03l287dpjjzd8b87p1wplfb001y5j7mkp8bx9p082wp0")))

(define-public crate-clibri-0.1.2 (c (n "clibri") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "00a4klrzsqxma53wk2mhdcmg1v6w4lzgzlmz2g93mgwwrf33mdjh")))

(define-public crate-clibri-0.1.3 (c (n "clibri") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "08fp9p5snqn5505lys0kah65i0iwdpxvvh4gd1yd0zz3d7inmk9w")))

(define-public crate-clibri-0.1.4 (c (n "clibri") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0z4fa1dlsf9pwsr5rpb8wk15qwy3lqmnnvk9bghdj8wzxdjlrad0")))

(define-public crate-clibri-0.1.5 (c (n "clibri") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0s768dbb2xbdnm4k8zizpnhzbm1sn8bpxb88kq4q55rs0fwalams")))

(define-public crate-clibri-0.1.6 (c (n "clibri") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1dqz08i63ani1rchk9ypvvcy0albkqc7dvfsnjgnwkfcnlwsycb2")))

(define-public crate-clibri-0.1.7 (c (n "clibri") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0yw0gzfd0rjlazmr17slkjvzgv5lckwm7xninnd3ca1z4sydp7l9")))

