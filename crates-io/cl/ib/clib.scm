(define-module (crates-io cl ib clib) #:use-module (crates-io))

(define-public crate-clib-0.0.1 (c (n "clib") (v "0.0.1") (h "013g6g6phi27mfrijv4w2hfdyy56rxcj3p6rsvlxvvyj2plla241")))

(define-public crate-clib-0.1.0 (c (n "clib") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0") (d #t) (k 1)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "0qi16mn7xh7x7b77q03v6fvlvm63x3cysy8scvcxmnai205kflpd") (f (quote (("zlib") ("x11") ("tk86" "tcl86") ("tcl86") ("sqlite3") ("liblzma") ("libcurl"))))))

(define-public crate-clib-0.2.0 (c (n "clib") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "inwelling") (r "^0.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1kqrhihyj8ia9x9z971mwa9gyzxh01k4cfwnc40939rzyjpi71rp")))

(define-public crate-clib-0.2.1 (c (n "clib") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1dgbcqnab7srgpvphzng4nzm0iq06wivbp80c2l3cdxbi5f74bh5")))

(define-public crate-clib-0.2.2 (c (n "clib") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0jk742ds109akgqp4ilphjs7av7vsnz7jj2hbllc1gmsq8izapfb")))

(define-public crate-clib-0.2.3 (c (n "clib") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "inwelling") (r "^0.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "toml") (r "^0.7.3") (d #t) (k 1)))) (h "0qbk4i5kxivn6dg1zakjcgzf99rz58gymajy1qgyhvn7i00z6il2")))

(define-public crate-clib-0.2.4 (c (n "clib") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "inwelling") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "toml") (r "^0.7.3") (d #t) (k 1)))) (h "0mz96j8yd1f6xib56yffmjdpy7j4rjqvzzg179fha7rlrncad8gx")))

