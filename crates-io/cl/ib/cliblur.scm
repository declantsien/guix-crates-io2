(define-module (crates-io cl ib cliblur) #:use-module (crates-io))

(define-public crate-cliblur-0.1.2 (c (n "cliblur") (v "0.1.2") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "mtpng") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qzy16psc0yy34i6qyah12cqnr5w21126h2hb9rq24wjab78cqb7")))

(define-public crate-cliblur-0.1.3 (c (n "cliblur") (v "0.1.3") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "mtpng") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n8q0fw2h71pbp7jkhl9bv2cw8x8ajsc0bc22ir96l53rnsgsrb4")))

(define-public crate-cliblur-0.2.0 (c (n "cliblur") (v "0.2.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mtpng") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 0)))) (h "1g3zzrm3b0d056fina3180vpapd1jymkia38bj92kh9cqas6rqay")))

(define-public crate-cliblur-0.3.0 (c (n "cliblur") (v "0.3.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 0)))) (h "0fk4m879k1xwawvpssgc7wz5cijndr8lsq7dw6ny3rjl0ah4l271")))

(define-public crate-cliblur-0.3.1 (c (n "cliblur") (v "0.3.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 0)))) (h "1y9hqm0qwcjlvzyizvccl2n02hmd62hgim1168ka8w2z2ssrk16r")))

(define-public crate-cliblur-0.4.0 (c (n "cliblur") (v "0.4.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 0)))) (h "1728gjr5m6whn7lrl4zvr307ik6ph8iwix216p54896p28zayv6w")))

