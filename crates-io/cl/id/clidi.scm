(define-module (crates-io cl id clidi) #:use-module (crates-io))

(define-public crate-clidi-1.0.1 (c (n "clidi") (v "1.0.1") (h "0mr5075n98qwak2918alb3vw9kadqz8ai0sfiy7z2p6cjch1lp09") (y #t)))

(define-public crate-clidi-1.0.2 (c (n "clidi") (v "1.0.2") (h "0zmj4wcv3iqqrylf7yka495am1fkjsmybw282n722rrhzjw4fi3a") (y #t)))

(define-public crate-clidi-1.0.3 (c (n "clidi") (v "1.0.3") (h "13zjnrgla3ngmk386rwfmpmy1p7h97d2hnx8qkayjd0vipcj30pp") (y #t)))

(define-public crate-clidi-1.1.0 (c (n "clidi") (v "1.1.0") (h "16izjpdf2dvncdyrpxb0lv1h49xgm42wcf9mx9avpdflwila9982") (y #t)))

(define-public crate-clidi-1.1.1 (c (n "clidi") (v "1.1.1") (h "1yjn1s5c4ks9ybrvmpv7mq18n1ff9dnfsmrzch9m9nz4vbld7hpv") (y #t)))

(define-public crate-clidi-1.2.1 (c (n "clidi") (v "1.2.1") (h "1zrxjc5w5p0s4wm8qmvpmfndh6zg1aryb2nsr23xvjs4phjv5c9s") (y #t)))

(define-public crate-clidi-1.3.0 (c (n "clidi") (v "1.3.0") (h "0sl9fqpy69ya350kky07i3p9hc87vag71v3h8i7v2l1qbd3k5xcj") (y #t)))

(define-public crate-clidi-1.3.1 (c (n "clidi") (v "1.3.1") (h "03xw04mlxjsr8qwhq43xs39jb0j912k4x884bif8k6g4li32f8vs")))

