(define-module (crates-io cl op clopy) #:use-module (crates-io))

(define-public crate-clopy-0.1.0 (c (n "clopy") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15cyvjqlx8albc0p64vycz67n1145rawy3x7fqh9dwbq9nj78gmj")))

(define-public crate-clopy-0.1.1 (c (n "clopy") (v "0.1.1") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1w0rigypq4y03d05qwnf8fcmbd3pc6y7alwz0ih7qzcac6qbpq34")))

(define-public crate-clopy-0.1.2 (c (n "clopy") (v "0.1.2") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13klmm9ahi3vw0748m4nqnfm7j6gam6vcvlm8wpmcvg338ap0k26")))

