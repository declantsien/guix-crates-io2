(define-module (crates-io cl ex clex) #:use-module (crates-io))

(define-public crate-clex-0.1.0 (c (n "clex") (v "0.1.0") (d (list (d (n "ethnum") (r "^1") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)))) (h "00lvxlhqynw9ing8vvmzc31hj4aaswahdpg6jxmapaq3cgafdfa5") (f (quote (("llvm-intrinsics" "ethnum/llvm-intrinsics") ("default" "ethnum"))))))

