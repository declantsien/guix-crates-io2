(define-module (crates-io cl d2 cld2-sys) #:use-module (crates-io))

(define-public crate-cld2-sys-0.0.0 (c (n "cld2-sys") (v "0.0.0") (h "1zsmlv46pfi9ayz4qshlwjh1kmbzvpp0vcdkqv46jbijpryf7w3b") (y #t)))

(define-public crate-cld2-sys-0.0.2 (c (n "cld2-sys") (v "0.0.2") (d (list (d (n "gcc") (r "~0.0.1") (d #t) (k 0)))) (h "1wdqnf9p92wl9rpjy2y9hdcp0x9pf0ibv648hmh01br929y8yi7c")))

(define-public crate-cld2-sys-0.0.3 (c (n "cld2-sys") (v "0.0.3") (d (list (d (n "gcc") (r "~0.0.1") (d #t) (k 1)) (d (n "toml") (r "~0.1.2") (d #t) (k 1)))) (h "1fsjj28lfd3wkghaccz5l1dvlxrg9szqf542zyq209i0g38c5cax")))

(define-public crate-cld2-sys-0.0.5 (c (n "cld2-sys") (v "0.0.5") (d (list (d (n "gcc") (r "~0.0.1") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)) (d (n "toml") (r "~0.1.2") (d #t) (k 1)))) (h "0yd9gavi4a8gqxncplj7xr8d761dpwi1hlkyjhjw52vhnxjpzg7c")))

(define-public crate-cld2-sys-0.0.6 (c (n "cld2-sys") (v "0.0.6") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)) (d (n "toml") (r "*") (d #t) (k 1)))) (h "175pib16z111kl8qn4c826mki0ih6w93lpmszh7x6zcwvbambr0b")))

(define-public crate-cld2-sys-0.0.7 (c (n "cld2-sys") (v "0.0.7") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)) (d (n "toml") (r "*") (d #t) (k 1)))) (h "0x89wn40symx87q7zqkpqlqv85ak6hx0yca3jl3zaskbgknvm368")))

(define-public crate-cld2-sys-0.0.9 (c (n "cld2-sys") (v "0.0.9") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)) (d (n "toml") (r "*") (d #t) (k 1)))) (h "1339qj1gm3sxm3dwngpk13z5il8sz5kzjspqqnlf1s6fzsxrcs53")))

(define-public crate-cld2-sys-0.1.0 (c (n "cld2-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.19") (d #t) (k 1)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 1)) (d (n "toml") (r "^0.1.23") (d #t) (k 1)))) (h "0qqcli36jg0qh770hn8zl0809kg31hy8l8asdi1hc8yall2py30n")))

(define-public crate-cld2-sys-1.0.0 (c (n "cld2-sys") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3.38") (d #t) (k 1)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 1)) (d (n "toml") (r "^0.1.23") (d #t) (k 1)))) (h "1lly8ddl2a0vgjq9sjl4vf9ygps7bh6qvkdr8mhqg0k37hkmh4ws")))

(define-public crate-cld2-sys-1.0.1 (c (n "cld2-sys") (v "1.0.1") (d (list (d (n "gcc") (r "^0.3.38") (d #t) (k 1)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 1)) (d (n "toml") (r "^0.1.23") (d #t) (k 1)))) (h "0f8k6ns77hsx11yzjfvykyhdyj5nnpq3vi3x8yzqp464qhliq201")))

(define-public crate-cld2-sys-1.0.2 (c (n "cld2-sys") (v "1.0.2") (d (list (d (n "gcc") (r "^0.3.38") (d #t) (k 1)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 1)) (d (n "toml") (r "^0.1.23") (d #t) (k 1)))) (h "13w01dy4ccz219hdqrbiisbjhkqisxmnbsddfiw2ii524pj8bfxz")))

