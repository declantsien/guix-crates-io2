(define-module (crates-io cl d2 cld2) #:use-module (crates-io))

(define-public crate-cld2-0.0.0 (c (n "cld2") (v "0.0.0") (h "13h1qb5p3z2ycvxlhjh837hjzvj1jqlhcaaxn4d5pkywv0w62qrs") (y #t)))

(define-public crate-cld2-0.0.2 (c (n "cld2") (v "0.0.2") (d (list (d (n "cld2-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1c5db692gs78mkrxzyqyp1x7b3jg7kjax4d9j54y5ba0q3w0930h")))

(define-public crate-cld2-0.0.3 (c (n "cld2") (v "0.0.3") (d (list (d (n "cld2-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0ajymh13y9wm0kdnp915dwwrj4y75ds5lv6i543wvid7cbprjpb9")))

(define-public crate-cld2-0.0.4 (c (n "cld2") (v "0.0.4") (d (list (d (n "cld2-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0gi0d1fl55gvpz2006k28wm3nxcxmz3kmrdk0n1rqhyqvzlkvkh6")))

(define-public crate-cld2-0.0.5 (c (n "cld2") (v "0.0.5") (d (list (d (n "cld2-sys") (r "^0.0.5") (d #t) (k 0)))) (h "15j2bgrbbn7npx8jdj4x7b92vravlr9xnjla6yjshbhpd6nq7mdw")))

(define-public crate-cld2-0.0.6 (c (n "cld2") (v "0.0.6") (d (list (d (n "cld2-sys") (r "*") (d #t) (k 0)))) (h "10pz00ys4isxsm6win5s69mbggs7dvgg1y971yq4gxyri44mjw4g")))

(define-public crate-cld2-0.0.9 (c (n "cld2") (v "0.0.9") (d (list (d (n "cld2-sys") (r "*") (d #t) (k 0)))) (h "0pvxmxfyr3rwmcnpl866gs9s2li28fdl63grhh0hvilpgpsnc85w")))

(define-public crate-cld2-0.1.0 (c (n "cld2") (v "0.1.0") (d (list (d (n "cld2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "0fij8k09xpa9vjlpwf9alnlcs2w5q75arbvrmjwnyirxkyhcw1jq") (f (quote (("unstable"))))))

(define-public crate-cld2-1.0.0 (c (n "cld2") (v "1.0.0") (d (list (d (n "cld2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "0iradl74ynxilg1dnx990r1zdq0s7kyl5mg9zyk4b901f4wls1hn") (f (quote (("unstable"))))))

(define-public crate-cld2-1.0.1 (c (n "cld2") (v "1.0.1") (d (list (d (n "cld2-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "1rbyaqk80ipp454r38b2bnlg1zrlknr4shffrkcm74mpzhyp082q") (f (quote (("unstable"))))))

(define-public crate-cld2-1.0.2 (c (n "cld2") (v "1.0.2") (d (list (d (n "cld2-sys") (r "^1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "1vazwn4fyfvrjic8ngvkbvz7m8yab0fqxvmjg609j0ak5di8p4c0") (f (quote (("unstable"))))))

