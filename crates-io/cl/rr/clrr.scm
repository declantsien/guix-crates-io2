(define-module (crates-io cl rr clrr) #:use-module (crates-io))

(define-public crate-clrr-1.0.0 (c (n "clrr") (v "1.0.0") (d (list (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "onig") (r "^6.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1cd2r4l868gwwafca97s7rwwklrfsyh7grpkz65npry9lanc70nh") (y #t)))

