(define-module (crates-io cl -t cl-to-cd) #:use-module (crates-io))

(define-public crate-cl-to-cd-0.0.1-alpha7 (c (n "cl-to-cd") (v "0.0.1-alpha7") (d (list (d (n "json") (r "^0.11.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)))) (h "1199r2bz1vkgqp28y13n1c9shw5bd13sdb81yskl1j6bpi8k0ac8")))

(define-public crate-cl-to-cd-0.0.1-alpha10 (c (n "cl-to-cd") (v "0.0.1-alpha10") (d (list (d (n "json") (r "^0.11.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)))) (h "0b18sq1pccni4fwd5f0ws3vw0xdynsbmwzk83n4l38m0jka98ghd")))

(define-public crate-cl-to-cd-0.0.1 (c (n "cl-to-cd") (v "0.0.1") (d (list (d (n "json") (r "^0.11.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)))) (h "06ivrdh1vni68rf635grvgbdsgx8abvz78g190yvc5l9f963ifvm")))

