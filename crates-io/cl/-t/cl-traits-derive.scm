(define-module (crates-io cl -t cl-traits-derive) #:use-module (crates-io))

(define-public crate-cl-traits-derive-0.0.1 (c (n "cl-traits-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1jxv8ax8gb1ib60nchrl3mq08c6x1dw7jj3bl4kgxh4fnffrb6zg")))

(define-public crate-cl-traits-derive-0.3.0 (c (n "cl-traits-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "19r3ncb8n5qckbszdbmbraryizcrhmxicd7b46dvd20bglnvjz8r")))

(define-public crate-cl-traits-derive-0.4.0 (c (n "cl-traits-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1g4ldkz4pa8i20w8kmlzfmwcjim755mh0zgkcsz1r5q371midm9p")))

(define-public crate-cl-traits-derive-0.4.1 (c (n "cl-traits-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0n55286ihfm1kmyip460sj1i19266fzl1zwz47y16kkgs1aid9bk")))

(define-public crate-cl-traits-derive-1.0.0 (c (n "cl-traits-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0k5swf65fka1jh8kfcizwiikrhxw9l09vp1kk20y9skkbiq077p8")))

