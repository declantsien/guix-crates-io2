(define-module (crates-io cl -g cl-generic-read-buf) #:use-module (crates-io))

(define-public crate-cl-generic-read-buf-0.1.0 (c (n "cl-generic-read-buf") (v "0.1.0") (d (list (d (n "cl-generic-vec") (r "^0.4.0") (d #t) (k 0)))) (h "0djhsg3n0gqwadfh1kyfwli91ppncsqwaba59cc34gm9wcwx8251")))

(define-public crate-cl-generic-read-buf-0.1.1 (c (n "cl-generic-read-buf") (v "0.1.1") (d (list (d (n "cl-generic-vec") (r "^0.4.0") (d #t) (k 0)))) (h "0j4mvy2dvsp41lv2qyv4sqgyawdgchrcbpi5n4i3l187qpvndjaz")))

(define-public crate-cl-generic-read-buf-0.1.2 (c (n "cl-generic-read-buf") (v "0.1.2") (d (list (d (n "cl-generic-vec") (r "^0.4.0") (d #t) (k 0)))) (h "11h5cq345hl9fpxwab3cny956pkwwb5j09y626x4nq2m8n5nchqq")))

(define-public crate-cl-generic-read-buf-0.1.3 (c (n "cl-generic-read-buf") (v "0.1.3") (d (list (d (n "cl-generic-vec") (r "^0.4.0") (d #t) (k 0)))) (h "1zarjnsvm68yjnn7z8l35hc6qnjx8xmraschpz2d641a2ajflfvy")))

