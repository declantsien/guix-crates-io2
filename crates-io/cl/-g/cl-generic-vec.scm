(define-module (crates-io cl -g cl-generic-vec) #:use-module (crates-io))

(define-public crate-cl-generic-vec-0.1.4 (c (n "cl-generic-vec") (v "0.1.4") (d (list (d (n "mockalloc") (r "^0.1") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "06v7vyi3vrjs4a8grj9ib3pa8g381xf1298s07l6cbnswch1b2jq") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.2.0 (c (n "cl-generic-vec") (v "0.2.0") (d (list (d (n "mockalloc") (r "^0.1") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "02d023vw99yrzm3pq9k6bg14kqxrlvfrzlcq7i4zwwjg2vsxfcvy") (f (quote (("std" "alloc") ("nightly") ("default" "std" "nightly") ("alloc"))))))

(define-public crate-cl-generic-vec-0.2.1 (c (n "cl-generic-vec") (v "0.2.1") (d (list (d (n "mockalloc") (r "^0.1") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "022gq1v71ffbxzfz2sl3j5bv9q6zk1i45z2qkp8gz102b2jzc168") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.3.0 (c (n "cl-generic-vec") (v "0.3.0") (d (list (d (n "mockalloc") (r "^0.1") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "1265fh58xg1lqpa3x7q5zbbc9rqnkq0s7bic6wp0mcfz1n9g1076") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.3.1 (c (n "cl-generic-vec") (v "0.3.1") (d (list (d (n "mockalloc") (r "^0.1") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "06saxq2gfy5rwl1v638i3z7x2ar0q4ffyn6c0dd2vjqsvj6crhwg") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.3.2 (c (n "cl-generic-vec") (v "0.3.2") (d (list (d (n "mockalloc") (r "^0.1") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "1n463h4igl46yj88v9zbq006qlr16wrl1h48pffdhc0n3cmz4h86") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.3.3 (c (n "cl-generic-vec") (v "0.3.3") (d (list (d (n "mockalloc") (r "^0.1") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "1ff7v4q67d3vqi4209nvchd3wxflxf719w75cai1pljw931bm2wj") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.3.4 (c (n "cl-generic-vec") (v "0.3.4") (d (list (d (n "mockalloc") (r "^0.1.2") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "15rf69xy30s0pwfjinvcfiik7lzpgza2ym33579ya0gdrlym7vas") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.4.0 (c (n "cl-generic-vec") (v "0.4.0") (d (list (d (n "mockalloc") (r "^0.1.2") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "1rxhgrczzn6jr185pyhava6yrjgg7g58hnxgw4hx1jp350jmwz9j") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.1.0 (c (n "cl-generic-vec") (v "0.1.0") (d (list (d (n "cl-generic-vec") (r "^0.4.0") (d #t) (k 0)))) (h "16bm1vika820fzz1n10jhv9x22kxwb3p649b9yk10f13v5vsbgss") (y #t)))

