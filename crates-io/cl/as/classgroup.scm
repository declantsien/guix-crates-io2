(define-module (crates-io cl as classgroup) #:use-module (crates-io))

(define-public crate-classgroup-0.1.0 (c (n "classgroup") (v "0.1.0") (d (list (d (n "criterion") (r ">= 0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "04za6sw8vv1clalyvbi8hwk3zq2d838bmfnvn15ali2pwh8850lf")))

