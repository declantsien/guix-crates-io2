(define-module (crates-io cl as classic_crypto) #:use-module (crates-io))

(define-public crate-classic_crypto-0.1.0 (c (n "classic_crypto") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l5p3j7dcp4qsj162vsiybr0f06b98md95z1ykmn108xynjbrl2p")))

(define-public crate-classic_crypto-0.2.0 (c (n "classic_crypto") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p4nx6625ghwa4pahgga1khmqzhcxl4iwp2x095ky8nwkijqi3kq")))

