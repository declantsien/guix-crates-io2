(define-module (crates-io cl as classic-core-test-tube) #:use-module (crates-io))

(define-public crate-classic-core-test-tube-0.2.0 (c (n "classic-core-test-tube") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "classic-rust") (r "^0.2.0") (d #t) (k 0)) (d (n "cosmrs") (r "^0.9.0") (f (quote ("cosmwasm" "rpc"))) (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "cw1-whitelist") (r "^0.15.0") (d #t) (k 2)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1k5hv6pvsb680bvhvlh9vgd58x1cnyyia2zyzi5hr32v7pkc09d5") (f (quote (("default"))))))

