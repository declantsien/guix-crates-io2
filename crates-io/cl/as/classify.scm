(define-module (crates-io cl as classify) #:use-module (crates-io))

(define-public crate-classify-0.1.0 (c (n "classify") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gjlidhr5jd0qkdkpk5kp73isd05g4fscf0wn9dxi200ywa5bvdp") (r "1.61")))

(define-public crate-classify-0.1.1 (c (n "classify") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16v5swkrb0xqiadabxlj1nwh7hfzqjfwy1j54p0spqqqd65h815m") (r "1.61")))

(define-public crate-classify-0.1.2 (c (n "classify") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0id53vks1d1s7y150hagssx834y8s7f83y9wdyyqnjmrvadlf1yv") (r "1.61")))

(define-public crate-classify-0.1.3 (c (n "classify") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1d18jb25gpvin5ddbca0ad90i74d68jbf8z0n8m6ll9jm958zq17") (r "1.61")))

(define-public crate-classify-0.1.4 (c (n "classify") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1b1hizadniq3bi0m7hshric272rsqa11kk0x757a8is2iys6jic2") (r "1.61")))

(define-public crate-classify-0.1.5 (c (n "classify") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mvgd8skszx6kg22z3qxq0k026fr42ig1j157484am2k1fypyg3b") (r "1.61")))

(define-public crate-classify-0.2.0 (c (n "classify") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1cfj33q6mkmc7rgbbgkhhrs325afiqs2lhjbgv4ds622zxjrsd3j") (r "1.61")))

(define-public crate-classify-0.2.1 (c (n "classify") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1z081579vq9nsycxyfkywqxwfc59s26rn19wwq6pw0zq3vdygci7") (r "1.61")))

(define-public crate-classify-0.2.2 (c (n "classify") (v "0.2.2") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("js"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0w77s1ll2rvq5ij5rmjp81f85168v6vh5v6ygpckyg5pgh2djh26") (f (quote (("js" "getrandom/js")))) (r "1.61")))

