(define-module (crates-io cl as classnames) #:use-module (crates-io))

(define-public crate-classnames-1.0.0 (c (n "classnames") (v "1.0.0") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "04931453fwghqhjnzixrslc1vyvk0lmv76ymc275k1czgxwijjx0")))

(define-public crate-classnames-1.0.1 (c (n "classnames") (v "1.0.1") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "0lc880yy4japw2a6yx44cpgb3k5pqppdml48mz9x0af7svh2jjjs")))

(define-public crate-classnames-1.0.2 (c (n "classnames") (v "1.0.2") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "10wqd2gkchknzfnmrad68rr0m8wpws2mf02gkbh6nd2ckgkx8gni")))

(define-public crate-classnames-2.0.0 (c (n "classnames") (v "2.0.0") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "02w9997vnrx6fqb63zjyc6m4cplhrj1ngm8bk944aqxkvwgd2vxn")))

(define-public crate-classnames-2.0.1 (c (n "classnames") (v "2.0.1") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "1xc31ckfpa2q1ak7fnfvi3v2ywsxp3kbgzsfrkaqbpcjjkw6i4p7")))

(define-public crate-classnames-2.0.2 (c (n "classnames") (v "2.0.2") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "1rgr2pvxidy4vvl6kpp563f8ryyhwpmr6aibpnd53zbcprx44mjf")))

(define-public crate-classnames-2.1.0 (c (n "classnames") (v "2.1.0") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "19a0gviic096i6bgd65qz1drd0xmw07x02hw2r2j3mjibw3vcqpx")))

(define-public crate-classnames-2.1.1 (c (n "classnames") (v "2.1.1") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "01q20j7alnpm5779j037sis1pc3ilbyw1k227p9rj74k2bzwhslm")))

(define-public crate-classnames-2.1.2 (c (n "classnames") (v "2.1.2") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "1h5j8m44fdaklck46ab7v60vsldlka6h9dfrzaczzy1asna0xnii")))

(define-public crate-classnames-2.1.3 (c (n "classnames") (v "2.1.3") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "02dnivlk3akykw2xv8fdgpbfh24m8fl5fi0k6vsb89y1b9hl6b18")))

(define-public crate-classnames-2.1.4 (c (n "classnames") (v "2.1.4") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "1vp6i5lzl7qvhjmd0x0sgbphj5jmwzqiab4fi5jblgdlwizbj5ij")))

(define-public crate-classnames-2.1.5 (c (n "classnames") (v "2.1.5") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "154kd2d880i112hdpf6c95j2qvm7rz5ix3782v85v0rq1nzi5m96")))

(define-public crate-classnames-2.1.6 (c (n "classnames") (v "2.1.6") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 2)))) (h "0zwx4q3zzhvgh7wdc6xyxwnq0i4f6zrj08il7qkv2a2aypg6qlgf")))

