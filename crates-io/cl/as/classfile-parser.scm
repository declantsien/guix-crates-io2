(define-module (crates-io cl as classfile-parser) #:use-module (crates-io))

(define-public crate-classfile-parser-0.2.0 (c (n "classfile-parser") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "0rxaca1v2hwhy7q3ayszxx6zx8j1jvjcrsc6a6mr37k6xcgjnv25")))

(define-public crate-classfile-parser-0.2.1 (c (n "classfile-parser") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "1hlpdnmf0asc9r157v4barwy0vrq01j4am41d3kh550qa5g43s9v")))

(define-public crate-classfile-parser-0.2.2 (c (n "classfile-parser") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0iybmi07r7j8akwq20759nrxq2z4pmkap9npzpls215w6qh2gfag")))

(define-public crate-classfile-parser-0.3.0 (c (n "classfile-parser") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0fllwrsapzdhk165rnqgsd5b3vyj64x14bd5bsvfyvh80bhlnhzi")))

(define-public crate-classfile-parser-0.3.1 (c (n "classfile-parser") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1d98xz13xwmipsx71lyqn8gmknmkjza4vilnl1vdzqqpqklf08ar")))

(define-public crate-classfile-parser-0.3.2 (c (n "classfile-parser") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "0p80mywd1zyfpi2b9kj5kymi3s20hdgh6i34awhpbfpsb4hvs6ba")))

(define-public crate-classfile-parser-0.3.3 (c (n "classfile-parser") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "0v8hkyfl4y8xkrw7nv6c2rgi0qigcvdpkadq4g0knngd3ahs7lmq")))

(define-public crate-classfile-parser-0.3.4 (c (n "classfile-parser") (v "0.3.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "1vmkyzngd5y8ksypfs971l5nyivrmiwmwz1ig3iqc2m047yr21f3")))

(define-public crate-classfile-parser-0.3.5 (c (n "classfile-parser") (v "0.3.5") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "06713z7q5d9sk4ld5pl2s0nsa90g6xchavpph9dbbdwjvnwg9w0h")))

(define-public crate-classfile-parser-0.3.6 (c (n "classfile-parser") (v "0.3.6") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.3") (d #t) (k 0)))) (h "1fcrwj7l468y8g6cp7x45zqiqb01pyjcyjiz7x2fs7iai6cyc7qr")))

(define-public crate-classfile-parser-0.3.7 (c (n "classfile-parser") (v "0.3.7") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.3") (d #t) (k 0)))) (h "0rppl4ywqm219j0w43mqbaax7zr64akrc1nazw1nfi0njky1605d")))

