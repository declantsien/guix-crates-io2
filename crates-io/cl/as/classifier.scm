(define-module (crates-io cl as classifier) #:use-module (crates-io))

(define-public crate-classifier-0.0.1 (c (n "classifier") (v "0.0.1") (h "01nq6bwmaz0g0wm4qbn0q8qbm8ndphn82la19wpfpg1k8c106lhk")))

(define-public crate-classifier-0.0.2 (c (n "classifier") (v "0.0.2") (d (list (d (n "regex") (r "^0.1.27") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)))) (h "0fjvwr3sgvny91qyfl0ibn4qyv0crclkaq0z8d4h4162pxdf53z4")))

(define-public crate-classifier-0.0.3 (c (n "classifier") (v "0.0.3") (d (list (d (n "regex") (r "^0.1.27") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)))) (h "1fsw8s0l209xz74ifp3pr4xdibc2m0piqnl22im1kqk1axhwlym6")))

