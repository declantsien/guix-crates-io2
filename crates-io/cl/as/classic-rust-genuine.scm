(define-module (crates-io cl as classic-rust-genuine) #:use-module (crates-io))

(define-public crate-classic-rust-genuine-0.3.0 (c (n "classic-rust-genuine") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.22") (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "osmosis-std-derive") (r "^0.20.1") (d #t) (k 0)) (d (n "prost") (r "^0.11") (f (quote ("prost-derive"))) (k 0)) (d (n "prost-types") (r "^0.11") (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-cw-value") (r "^0.7.0") (d #t) (k 0)))) (h "0irwwhyp41v2qjmwycgssrks5a4ii5hrf6g4rrr3j4h8qmfsbxv0")))

