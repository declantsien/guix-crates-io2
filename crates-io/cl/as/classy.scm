(define-module (crates-io cl as classy) #:use-module (crates-io))

(define-public crate-classy-0.1.0 (c (n "classy") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "zip") (r "^0.6.5") (d #t) (k 0)))) (h "0shmr5nd69l638hkmb9xa51z7p6wklqn27y0z70g28n14csx76vx")))

(define-public crate-classy-0.1.1 (c (n "classy") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "zip") (r "^0.6.5") (d #t) (k 0)))) (h "04nz05ddgkmih48538nbh8ccrf390950q6c0vi9cav75m6h6bhqi")))

(define-public crate-classy-0.1.2 (c (n "classy") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "zip") (r "^0.6.5") (d #t) (k 0)))) (h "04ld00vjx1qgqbpiqk4h7xdjx7qs547h0hb4mr67kx2vj8nvhgfk")))

(define-public crate-classy-0.2.0 (c (n "classy") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "zip") (r "^0.6.5") (d #t) (k 0)))) (h "0zgznlx2p6wzq6lz1qf2988l5y4d6j15ky296jnc83vc2wlxldla")))

(define-public crate-classy-0.3.0 (c (n "classy") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "zip") (r "^0.6.5") (d #t) (k 0)))) (h "0vyvh3xyyfinahvwza553vff2fpnqy6k38vyjkv30xhn100liijq")))

