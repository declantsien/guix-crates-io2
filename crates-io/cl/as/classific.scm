(define-module (crates-io cl as classific) #:use-module (crates-io))

(define-public crate-classific-0.1.1 (c (n "classific") (v "0.1.1") (h "0j4znj0wp9pb373ajw8qhk74sm3hisqnrdabxgigpvya0img2ixl")))

(define-public crate-classific-0.1.2 (c (n "classific") (v "0.1.2") (h "18vpm3w5i3vza04fzxwag7zjx8w6kqp539qll7x7z362yh14a7lf")))

(define-public crate-classific-0.1.3 (c (n "classific") (v "0.1.3") (h "0s3kspjx8z1ab7419awph968yxvgac259lhswzbimdqiqsly8a5n")))

(define-public crate-classific-0.1.4 (c (n "classific") (v "0.1.4") (h "0d1fv1zh5200vp6ah4cvmbmk24rvy7py38mrs5mymi8sqq6jcv3n")))

