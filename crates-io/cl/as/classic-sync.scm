(define-module (crates-io cl as classic-sync) #:use-module (crates-io))

(define-public crate-classic-sync-0.4.2 (c (n "classic-sync") (v "0.4.2") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "01rbzbjk01cxsjqj1rwxai69ccvzcyhn4cy1c09rh34ayhzj3hn3")))

(define-public crate-classic-sync-0.4.3 (c (n "classic-sync") (v "0.4.3") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0ki0l5vz047dmmb9d58lnar7mwjfnkcv3fp28adj92gh3ld8qlaj")))

(define-public crate-classic-sync-0.4.4 (c (n "classic-sync") (v "0.4.4") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1h2pivaa8f3zyfs0hp5rjljcf71m4pshg4p30iafvzggzq5ryai3")))

