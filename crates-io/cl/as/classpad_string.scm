(define-module (crates-io cl as classpad_string) #:use-module (crates-io))

(define-public crate-classpad_string-0.1.0 (c (n "classpad_string") (v "0.1.0") (d (list (d (n "bidir-map") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ng94bs0m6ibx247flsi7431a0g036fhrjx6pgmmm0ks1qgqw1yv")))

(define-public crate-classpad_string-0.1.1 (c (n "classpad_string") (v "0.1.1") (d (list (d (n "bidir-map") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1hzpidr9w5p98gmm5ih04b21rivb1n0w2bvfydwzd8f3lzfccnl7")))

(define-public crate-classpad_string-0.2.1 (c (n "classpad_string") (v "0.2.1") (d (list (d (n "bidir-map") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1hgvi7cil9fmyj0gnqr8sq4x6ana2aa73i1wg8svzncz4p0fr43c")))

