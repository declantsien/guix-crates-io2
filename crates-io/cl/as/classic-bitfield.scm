(define-module (crates-io cl as classic-bitfield) #:use-module (crates-io))

(define-public crate-classic-bitfield-0.1.0 (c (n "classic-bitfield") (v "0.1.0") (d (list (d (n "derive_deref") (r "^1.1.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "087k0bm2zp4bfvr8i8afzmmcz1d7mbwbhvq3i5c3zdhrc34gaar1") (f (quote (("serde"))))))

(define-public crate-classic-bitfield-0.2.0 (c (n "classic-bitfield") (v "0.2.0") (d (list (d (n "derive_deref") (r "^1.1.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0p2g9hggivbr2c6dqrpdg3gkc5nij7qw2by27q5yrg58fphbmr7g") (f (quote (("serde"))))))

(define-public crate-classic-bitfield-0.2.2 (c (n "classic-bitfield") (v "0.2.2") (d (list (d (n "derive_deref") (r "^1.1.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0x4xj6hcy47ffrm71cd59apw2kczv0ql8pigc484qck8qdn5bwwl") (f (quote (("serde"))))))

(define-public crate-classic-bitfield-0.2.3 (c (n "classic-bitfield") (v "0.2.3") (d (list (d (n "derive_deref") (r "^1.1.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1k7dna6p8gfx1cln526j2sk4sn1fl0w02s8lng3635g2y7smjvm0") (f (quote (("serde"))))))

