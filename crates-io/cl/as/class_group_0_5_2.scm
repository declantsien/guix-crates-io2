(define-module (crates-io cl as class_group_0_5_2) #:use-module (crates-io))

(define-public crate-class_group_0_5_2-0.5.2 (c (n "class_group_0_5_2") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "criterion") (r ">=0.2") (d #t) (k 2)) (d (n "curv") (r "^0.7") (d #t) (k 0) (p "curv-kzen")) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "ring-algorithm") (r "^0.2.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)))) (h "1frh0r6h67kapi3hk1mnf495vmgyqnwdzyffqzagq5inppksf8jk") (l "libpari")))

