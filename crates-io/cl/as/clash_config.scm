(define-module (crates-io cl as clash_config) #:use-module (crates-io))

(define-public crate-clash_config-0.1.0 (c (n "clash_config") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1ys9j6jlw4iibzlnx7iv31rcswyfbidm21hgmgdxvkn7lg6k9gnf")))

