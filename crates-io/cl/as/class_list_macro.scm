(define-module (crates-io cl as class_list_macro) #:use-module (crates-io))

(define-public crate-class_list_macro-0.1.0 (c (n "class_list_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x56dqaf31d0pmavlkkb6y421ra3jmccrszg6w0jv2w51jjir31w") (y #t)))

(define-public crate-class_list_macro-0.1.1 (c (n "class_list_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01cnbkgcaq9khlzbya5d3y0w47m8dbmxyqq4hxdh7n7gywh472hx")))

(define-public crate-class_list_macro-0.1.2 (c (n "class_list_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0aaxc2q8l4r7f3ljvqax2jn6cjihfxkqzifbnmxyz68zwn4644sk")))

(define-public crate-class_list_macro-0.1.3 (c (n "class_list_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1645zvl712d3fis7zx6v1vbc0wgmpwpis1mkw2v5i71q5pmi2l91")))

(define-public crate-class_list_macro-0.1.4 (c (n "class_list_macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y4v19xnwj95k1vfs49iz8ljxidvd8nnii9zdd0b83jv26a6fqi2")))

(define-public crate-class_list_macro-0.1.5 (c (n "class_list_macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pfpl15lzajgw1sjajrmp335d4vbxviv6qb9ll9dzpsmkivzkrfx")))

(define-public crate-class_list_macro-0.1.6 (c (n "class_list_macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12hifjc0qm70z8qbbbrsl3mhfzqjnrf4j5x8w82lzdwdvsg85n97")))

(define-public crate-class_list_macro-0.1.7 (c (n "class_list_macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0girhjsah744dbq3ax9aqj6ascrnizlvxiiv4vjxfbgd155z2w9r")))

