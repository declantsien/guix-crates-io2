(define-module (crates-io cl as classifier-measures) #:use-module (crates-io))

(define-public crate-classifier-measures-0.3.0 (c (n "classifier-measures") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0yws29dg9kha3zpdk9kiq2dl92y3gcl3g6srikbhblvy0slqnsr8")))

(define-public crate-classifier-measures-0.3.1 (c (n "classifier-measures") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "04hha6z2lr06r217radg1gwzxxa8i6mv8gb3xvw1d1ilnaj4g1pi")))

(define-public crate-classifier-measures-0.3.2 (c (n "classifier-measures") (v "0.3.2") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1a3784crah2xkg90gz32a4adq0wc16jn0w4a4zicdakb9r0gcmzg")))

(define-public crate-classifier-measures-0.4.0 (c (n "classifier-measures") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1a37z65likw4v9bkw316fzzl8mx0fb2gbajclzrjp9dq4yrzpxzi")))

(define-public crate-classifier-measures-0.4.1 (c (n "classifier-measures") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1yvp6akcm6sny5sn0yd00l8rl8dsvfr5a2j7f5bp7j5nsbi2sy3k")))

(define-public crate-classifier-measures-0.4.2 (c (n "classifier-measures") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "13yy2pp39gdrqc06a0hm230p5q1hi3ilw846n204xha8p3zs0zvf")))

(define-public crate-classifier-measures-0.4.3 (c (n "classifier-measures") (v "0.4.3") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1d1zlmxj6hgyxia4whsf08n9q5drkaqvcrr8b4lzg1wb7d6zphhh")))

