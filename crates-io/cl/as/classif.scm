(define-module (crates-io cl as classif) #:use-module (crates-io))

(define-public crate-classif-0.0.1 (c (n "classif") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1lqc942pvpya1fpjs9hy5pfiialqbhyp4578afi8i74s6za4d6mc")))

(define-public crate-classif-0.0.2 (c (n "classif") (v "0.0.2") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "13rr8v3ga74vir6yls038faf4zh025yym21qggqw4mmzbpwj6gw6")))

