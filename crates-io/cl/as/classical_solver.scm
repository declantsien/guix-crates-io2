(define-module (crates-io cl as classical_solver) #:use-module (crates-io))

(define-public crate-classical_solver-0.1.0 (c (n "classical_solver") (v "0.1.0") (d (list (d (n "annealers") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "19mlknxpsycsc04pdg2l42rniw6rmx19gqd3rk6365ilp3krv988")))

