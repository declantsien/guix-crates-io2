(define-module (crates-io cl as classics-ranking-bot) #:use-module (crates-io))

(define-public crate-classics-ranking-bot-1.0.0 (c (n "classics-ranking-bot") (v "1.0.0") (d (list (d (n "roboat") (r "^0.20.1") (d #t) (k 0)) (d (n "safelog") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a8iqv05352wmp205dnjg5v9g1m23dyf5r5fr6mwygsdia92kfqx")))

