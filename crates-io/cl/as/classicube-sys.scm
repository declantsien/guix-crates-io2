(define-module (crates-io cl as classicube-sys) #:use-module (crates-io))

(define-public crate-classicube-sys-3.0.1 (c (n "classicube-sys") (v "3.0.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1hxxi0xbm5qic0yx585sqqzlhqxg0ny3w8nj7zinm31017lxhn0l") (f (quote (("no_std" "cstr_core" "libc" "libm") ("default"))))))

(define-public crate-classicube-sys-3.0.2+classicube.1.3.6 (c (n "classicube-sys") (v "3.0.2+classicube.1.3.6") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "18fh46nss715v654q7r4vgz415127lwsllml4a9pivijphhhbjiv") (f (quote (("no_std" "cstr_core" "libc" "libm") ("default"))))))

