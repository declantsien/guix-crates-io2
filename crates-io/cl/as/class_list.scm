(define-module (crates-io cl as class_list) #:use-module (crates-io))

(define-public crate-class_list-0.1.0 (c (n "class_list") (v "0.1.0") (d (list (d (n "class_list_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1kbg265hkkjizl1md2wwa6nz86g78dn9ry8j76iayh0fmj3js414") (y #t)))

(define-public crate-class_list-0.1.1 (c (n "class_list") (v "0.1.1") (d (list (d (n "class_list_macro") (r "^0.1.1") (d #t) (k 0)))) (h "11wncffri5kn7vp3q09a4fb1vwyiqx0609kqai90mg38272chihp")))

(define-public crate-class_list-0.1.2 (c (n "class_list") (v "0.1.2") (d (list (d (n "class_list_macro") (r "^0.1.2") (d #t) (k 0)))) (h "1f6kcnyqayjz6r950z1a6rk00s7qjr5xdsxqch650q2h0fav6wps")))

(define-public crate-class_list-0.1.3 (c (n "class_list") (v "0.1.3") (d (list (d (n "class_list_macro") (r "^0.1.3") (d #t) (k 0)))) (h "018gnj1yypyg4ji3qm5iy6q1srj7mq60g92zxiscsfsk1cv8w1wf")))

(define-public crate-class_list-0.1.4 (c (n "class_list") (v "0.1.4") (d (list (d (n "class_list_macro") (r "^0.1.4") (d #t) (k 0)))) (h "1sqww8b1mrnm69a0gsjxgv6cbw7ca2dqzdw1lfxa60brbsbvsrsg")))

(define-public crate-class_list-0.1.5 (c (n "class_list") (v "0.1.5") (d (list (d (n "class_list_macro") (r "^0.1.5") (d #t) (k 0)))) (h "1yb7ww9rvz1jbqihny48kk4hrn7l87cmqbw0h3fsn5xw8n4bj88p")))

(define-public crate-class_list-0.1.6 (c (n "class_list") (v "0.1.6") (d (list (d (n "class_list_macro") (r "^0.1.6") (d #t) (k 0)))) (h "1a4mqjrlssw6ncdcqg8m1jfdzknjjnhjmnjz82f6nc09z9z5w3f1")))

(define-public crate-class_list-0.1.7 (c (n "class_list") (v "0.1.7") (d (list (d (n "class_list_macro") (r "^0.1.7") (d #t) (k 0)))) (h "0ci4dmbmz2grlscivll55v352kxc4v8wm9rgdkn1wxbcsi9v03ps")))

