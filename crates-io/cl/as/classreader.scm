(define-module (crates-io cl as classreader) #:use-module (crates-io))

(define-public crate-classreader-0.1.0 (c (n "classreader") (v "0.1.0") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "04br9smxyb4cn0nrb35scw11ig7qcmm13f2gyh9a78dgkwyqww3f")))

(define-public crate-classreader-0.2.0 (c (n "classreader") (v "0.2.0") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "0r58c079g72r6bzxkl1pwxx8fzfchd1h9v4q62vbfdx6zj26nhjm")))

(define-public crate-classreader-0.2.1 (c (n "classreader") (v "0.2.1") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "1vpdxk3sm6w5f6js8yxrkvzzlwahl2g20r29nvjm6ijjzs81g8li")))

