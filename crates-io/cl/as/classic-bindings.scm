(define-module (crates-io cl as classic-bindings) #:use-module (crates-io))

(define-public crate-classic-bindings-0.1.0 (c (n "classic-bindings") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "02j7bjnbr24qgqkznx512fln3ikhj352c4gqkl0137b974pj9rz3")))

(define-public crate-classic-bindings-0.1.1 (c (n "classic-bindings") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "013kp7b564zfi1ada1i7nh58gi6bqhp54nbm4fwvgs76m9szgxcz")))

(define-public crate-classic-bindings-0.2.0 (c (n "classic-bindings") (v "0.2.0") (d (list (d (n "classic-rust") (r "^0.1.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1ni86m3zv4q9pmf48d5i9x5d61r7wfry4076pq8q2529cc57x2nb") (f (quote (("stargate"))))))

(define-public crate-classic-bindings-0.2.1 (c (n "classic-bindings") (v "0.2.1") (d (list (d (n "classic-rust") (r "^0.2.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "02j6p961yahw0ciwlj948h82fjsdfnd7wzwi86sib37nvxic15sr") (f (quote (("stargate"))))))

