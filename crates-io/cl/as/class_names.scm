(define-module (crates-io cl as class_names) #:use-module (crates-io))

(define-public crate-class_names-0.0.0 (c (n "class_names") (v "0.0.0") (h "1jrn28rk0vbjppcv7yfd404lnzvsvg702l219j8mszz0mqh10pmh")))

(define-public crate-class_names-0.0.1 (c (n "class_names") (v "0.0.1") (h "16rcxk7abmlkdichfxjpd6n0lmmi1dxqlm1f9qhhvkr2cx0ldii2")))

