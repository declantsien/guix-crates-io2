(define-module (crates-io cl as class2json) #:use-module (crates-io))

(define-public crate-class2json-0.1.0 (c (n "class2json") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0155kpq7igky877xp7ys3wic91wa8qg52adz32bsd82l0v5jv6xm")))

