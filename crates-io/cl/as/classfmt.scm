(define-module (crates-io cl as classfmt) #:use-module (crates-io))

(define-public crate-classfmt-0.1.0 (c (n "classfmt") (v "0.1.0") (h "0dvz7j5sw13sxj2hwwp2lp7c1r5gkgdj6cgydkldrx84nfbcd6lv")))

(define-public crate-classfmt-0.2.0 (c (n "classfmt") (v "0.2.0") (h "11dczqhdg7hs9xw9pggpv1b63na5ch7l57s2h3q9bcl960zzqlql")))

(define-public crate-classfmt-0.3.0 (c (n "classfmt") (v "0.3.0") (h "1w0nq1w4cs453xj5i6vjxyszbzdcqzdhszfgqx3h1d5m7yri2ais")))

(define-public crate-classfmt-0.3.1 (c (n "classfmt") (v "0.3.1") (h "153xd0v32xidf1z130zvq19359993qas93066ivxqvq1ii1cc6dw")))

(define-public crate-classfmt-0.4.0 (c (n "classfmt") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0b1y59xh3ry52cvmyjgrd6vj876xyvqg63sk413w95p6h360dqms")))

(define-public crate-classfmt-0.4.1 (c (n "classfmt") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1xyf6srll96i5karvm3zvnl4qk48xhz0krwh2visl0fzy0nmhv8w")))

(define-public crate-classfmt-0.5.0 (c (n "classfmt") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "002v9jafx97si1p0f3ka5s6mpnlzwcfm7zhr31zzyawcqsla8lqa")))

(define-public crate-classfmt-0.5.1 (c (n "classfmt") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0pv6x0cn29zyyrv6v8vspc43l9l6ih1cfnr2v8fqvr6x97almfbx")))

