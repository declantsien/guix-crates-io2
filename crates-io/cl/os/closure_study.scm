(define-module (crates-io cl os closure_study) #:use-module (crates-io))

(define-public crate-closure_study-0.1.0 (c (n "closure_study") (v "0.1.0") (h "1vms19rvi4frgblk7cm7jamr62jbnqra87jaihbfr7k0ds0r7rqr") (y #t)))

(define-public crate-closure_study-0.1.1 (c (n "closure_study") (v "0.1.1") (h "0ji09wwjfdi78dgxik6zajq63j2vdi9qlq3w9i60dqdas9lx84jh") (y #t)))

(define-public crate-closure_study-0.1.2 (c (n "closure_study") (v "0.1.2") (h "1s5wx1kf647rnzch51rzlag4vqnxka12hkjlxjf07frfv5dvxzlv")))

