(define-module (crates-io cl os close-file) #:use-module (crates-io))

(define-public crate-close-file-0.1.0 (c (n "close-file") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1p5indsgviqr9hvlsybl15yj8l8433bllf6v8abid53i76lca5xn")))

