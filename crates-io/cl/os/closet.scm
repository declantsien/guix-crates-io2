(define-module (crates-io cl os closet) #:use-module (crates-io))

(define-public crate-closet-0.1.0 (c (n "closet") (v "0.1.0") (h "1iy6mkn5r3337bcmg2pibl250pxxb6nmr7lm438yl1k8akxr1n77") (y #t)))

(define-public crate-closet-0.1.1 (c (n "closet") (v "0.1.1") (h "148awnk33kfqby2blhxbi179hjrvlhxj2jl2sp58c5wbqnymj6nm") (y #t)))

(define-public crate-closet-0.1.2 (c (n "closet") (v "0.1.2") (h "08vp3vbiz86inhc0ij0r0dgddvj3i64fmh81f0bdlrlsddrdyr2l")))

(define-public crate-closet-0.1.3 (c (n "closet") (v "0.1.3") (h "08bk5j3fq2yya4ql1wnn0gp8d7612y4kbx5x7xblfrn2zkyq6vmy")))

(define-public crate-closet-0.2.0 (c (n "closet") (v "0.2.0") (h "0ylzrwiqw5p5zrpbdh8whpm4c3vsriqhcri3gq78rka4nxkpsvjn")))

(define-public crate-closet-0.2.1 (c (n "closet") (v "0.2.1") (h "1l1v9ljipnq0kxr6qlzp5f7476lnj4jlfbrh8qm6y2mki247ibjh")))

(define-public crate-closet-0.2.2 (c (n "closet") (v "0.2.2") (h "1iqiwkrbyjc2mb4ih9sziraxqrld2a2yn0gbig2668yp8922nws9")))

