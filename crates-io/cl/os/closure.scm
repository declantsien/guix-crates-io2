(define-module (crates-io cl os closure) #:use-module (crates-io))

(define-public crate-closure-0.1.0 (c (n "closure") (v "0.1.0") (h "12wrgyjis1rn6dsvm5h7payxricly4z990zd5n4vb34k71bxmns1")))

(define-public crate-closure-0.1.1 (c (n "closure") (v "0.1.1") (h "1mxvyyidp78pr1522f403c1r438wvw2lyaqa7gj40qvlp6a1x0vv")))

(define-public crate-closure-0.1.2 (c (n "closure") (v "0.1.2") (h "10b74mjwnijrvdci7h9s7zpwkpkghs0nk78y52frqcsn7h37h8y6")))

(define-public crate-closure-0.2.0 (c (n "closure") (v "0.2.0") (h "0rm06rahlmin3rwyflimflvs7faksw88gv8vnwyjzfli7ixj2a87")))

(define-public crate-closure-0.3.0 (c (n "closure") (v "0.3.0") (h "10mp4hxbjz710s4sr7dbqi0p8qkm0xibgmvdaskia3b13gb3y5yn")))

