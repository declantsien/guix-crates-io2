(define-module (crates-io cl os close-to) #:use-module (crates-io))

(define-public crate-close-to-0.1.0 (c (n "close-to") (v "0.1.0") (d (list (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "07909xlw9nzjxb5283chzjmm7jdfvvvg4nkav7l5cdlrjyvxizk5")))

(define-public crate-close-to-0.2.0 (c (n "close-to") (v "0.2.0") (d (list (d (n "num") (r "^0.4.3") (d #t) (k 0)))) (h "1ibm20xx6cn9kx57ya9hccpd58s8qvpwpmh4h5ly5nbc8wx3kmjf")))

