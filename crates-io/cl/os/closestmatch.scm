(define-module (crates-io cl os closestmatch) #:use-module (crates-io))

(define-public crate-closestmatch-0.1.0 (c (n "closestmatch") (v "0.1.0") (d (list (d (n "rayon") (r "^0.7.0") (d #t) (k 0)))) (h "1ayyykyznhv2v16r3s578lzg19zvafpipq9g5h93nvnp5b43b8zh")))

(define-public crate-closestmatch-0.1.1 (c (n "closestmatch") (v "0.1.1") (d (list (d (n "rayon") (r "^0.7.0") (d #t) (k 0)))) (h "06zpiny9blc5qy5yk3pg29b419gi7ksdirpps2l2f3ry5cdpyi1g")))

(define-public crate-closestmatch-0.1.2 (c (n "closestmatch") (v "0.1.2") (d (list (d (n "rayon") (r "^0.7.0") (d #t) (k 0)))) (h "04qzdz500svag5dk9dkmd86xaxgmr3iw0bww4ls4na4k5wq7kl3a")))

