(define-module (crates-io cl os closur) #:use-module (crates-io))

(define-public crate-closur-0.1.0 (c (n "closur") (v "0.1.0") (h "1wl09hdkggsfpkq2fna7c71a99qrihyfdlxc173ril4a1fmr0d4w") (y #t)))

(define-public crate-closur-0.1.1 (c (n "closur") (v "0.1.1") (h "1yd76i99a25ksaz6dpk72a7zvbq77wrq7h164kcb5hir56sgh4n3")))

