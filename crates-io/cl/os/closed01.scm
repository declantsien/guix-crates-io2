(define-module (crates-io cl os closed01) #:use-module (crates-io))

(define-public crate-closed01-0.0.1 (c (n "closed01") (v "0.0.1") (h "165csggkmlrc0djqdvxkkv8qjlzbx1s1sm6d1cds59956n0vzz3q")))

(define-public crate-closed01-0.1.0 (c (n "closed01") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0fknya0vl9ah7s5y2bgrwmsca78m0rarnk2lisr7xm17658ya9nx")))

(define-public crate-closed01-0.1.1 (c (n "closed01") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0vkiinsvmgqyghjf8ql1cpz3vp1wvb102qzr8gy3ll2yypp2igkd")))

(define-public crate-closed01-0.2.0 (c (n "closed01") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1dvign0nyrw6491y2c2bj54x5zp1m7dipi6ncnih62g6a8k7kmmj")))

(define-public crate-closed01-0.3.0 (c (n "closed01") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1rbfsp7sask780400s8sbrrfjzj9zvzgyi82vddzl27nl8qx7yv1")))

(define-public crate-closed01-0.4.0 (c (n "closed01") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "09v8vfx25vl2gjp4z0md1xrcnr5bm3n9hc4h2qzk5gdpnzkwwimz")))

(define-public crate-closed01-0.5.0 (c (n "closed01") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0db2i1zf7gvadm96gvcs6xrblbgx55rw1ffqbv3ffbz7r45wzxpb")))

