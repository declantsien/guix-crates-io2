(define-module (crates-io cl os closure-pass) #:use-module (crates-io))

(define-public crate-closure-pass-0.1.0 (c (n "closure-pass") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "186gv0748z019z4gw504vn1qwcwrc2s334kivsjphzr5aqnh9s66")))

