(define-module (crates-io cl os close_fds) #:use-module (crates-io))

(define-public crate-close_fds-0.1.0 (c (n "close_fds") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19v8w40mkr5k1nwh6y1ws7v14j2209qpy10f7qf71xpn3xf4qxhk")))

(define-public crate-close_fds-0.2.0 (c (n "close_fds") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "192n44ll70d3s875l6zpz3nvmx9v0z0b4h981f9b3zlrnbamgw8b")))

(define-public crate-close_fds-0.2.1 (c (n "close_fds") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r53kqp5dh75mr1lcmkb0fm1p0f0s6dqif1bp38pyw7kjnykphi2")))

(define-public crate-close_fds-0.2.2 (c (n "close_fds") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x1dbhczrv2hxnyhpyy3i74c30sk7ad1par3wrlvf0bx0m3qq09c")))

(define-public crate-close_fds-0.3.0 (c (n "close_fds") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cm4v5v55xd1xr9hqh21izg3m2sdymdqx4xd828v6k122ss9k058")))

(define-public crate-close_fds-0.3.1 (c (n "close_fds") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1iidn5smcgyfln2kd3sk3njcnzzpls3sz7qgccyb1xr9sf6khh9i")))

(define-public crate-close_fds-0.3.2 (c (n "close_fds") (v "0.3.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vd2i1gkvfcgdlzgrkgivhx3ky0zs98g8q3mwmwrxmg97pridi1v")))

