(define-module (crates-io cl os closer) #:use-module (crates-io))

(define-public crate-closer-0.1.0 (c (n "closer") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0hv0yg9c1z2lrqalzs5mg7wc8hvr06vjchxw8n433xr1xm6hp93i")))

(define-public crate-closer-0.1.1 (c (n "closer") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0s6igib4s99sd8vws4wp0g8gmnyv65pdy3qylk5ijn3czg6viisc")))

(define-public crate-closer-0.1.2 (c (n "closer") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "00mdqawkpjbi5p9w7wvgzy1ylz8sa5zzpbhh491b3xd38yrx3icc")))

