(define-module (crates-io cl os closure_attr_derive) #:use-module (crates-io))

(define-public crate-closure_attr_derive-0.2.0 (c (n "closure_attr_derive") (v "0.2.0") (d (list (d (n "closure_attr_core") (r "^0.2.0") (d #t) (k 0)))) (h "0p4ifzjap1nhr04yl26ah87m7jcfl6piydgj4jgdbigy4hrbs2kh")))

(define-public crate-closure_attr_derive-0.3.0 (c (n "closure_attr_derive") (v "0.3.0") (d (list (d (n "closure_attr_core") (r "^0.3.0") (d #t) (k 0)))) (h "1272dnkaridbsgbcwzv5rbkrxzg2w4qnf8d18g2am685gragkif9")))

(define-public crate-closure_attr_derive-0.4.0 (c (n "closure_attr_derive") (v "0.4.0") (d (list (d (n "closure_attr_core") (r "^0.4.0") (d #t) (k 0)))) (h "11qsml7ngxz7jswdc9ykgrz8mprk1vyyxhxhkj9hfcz0arg36dgp")))

