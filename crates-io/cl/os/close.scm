(define-module (crates-io cl os close) #:use-module (crates-io))

(define-public crate-close-0.1.0 (c (n "close") (v "0.1.0") (h "1d5d4a10a77zfymhfik98dip3d83df2ypngdpml010603hr6g2ha")))

(define-public crate-close-0.1.1 (c (n "close") (v "0.1.1") (h "03n2s0kxf76qzjjv4pyam1pxc9acwlkp8asm67bkdzaas7z0h1x6")))

(define-public crate-close-0.1.2 (c (n "close") (v "0.1.2") (h "114fn308dyh1isv62rf2z4jfm0i1wsj9qjkkwd4q74mmk9kyryb1")))

(define-public crate-close-0.2.0 (c (n "close") (v "0.2.0") (h "12zkyib4d9acb1ksn0fg6g0c4g03da57xc3gjvv24c5w6agg4mfk")))

