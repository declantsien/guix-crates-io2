(define-module (crates-io cl os close_enough) #:use-module (crates-io))

(define-public crate-close_enough-0.2.0 (c (n "close_enough") (v "0.2.0") (d (list (d (n "clap") (r "^2.26") (f (quote ("color" "wrap_help"))) (o #t) (k 0)))) (h "1h6vz1s20g98hda3yniiw9wvrz15n8s4l29c9wvd7g6nxj6j2pyn") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-close_enough-0.2.1 (c (n "close_enough") (v "0.2.1") (d (list (d (n "clap") (r "~2.26.2") (f (quote ("color" "wrap_help"))) (o #t) (k 0)))) (h "12qi2lz2kpkq82hfyk61p9f3nd3l1krpjf18148y30hjh4z1wb2d") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-close_enough-0.3.0 (c (n "close_enough") (v "0.3.0") (d (list (d (n "clap") (r "~2.33.0") (f (quote ("color" "wrap_help"))) (o #t) (k 0)))) (h "16b9d2q0x6x9xnjrkjg1wz8xwrrvnv96kbxbqw6jzmvg0d157x5f") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-close_enough-0.4.0 (c (n "close_enough") (v "0.4.0") (d (list (d (n "home") (r "^0.5.3") (o #t) (d #t) (k 0)) (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "structopt") (r "~0.3.21") (o #t) (d #t) (k 0)))) (h "1rj339qhbqyj320fgi2l1fy6m88ixdhhhyh2kr5rgbh0jqc2x5p4") (f (quote (("default" "cli") ("cli" "home" "structopt"))))))

(define-public crate-close_enough-0.5.0 (c (n "close_enough") (v "0.5.0") (d (list (d (n "directories") (r "~4.0.1") (o #t) (d #t) (k 0)) (d (n "ignore") (r "~0.4.18") (d #t) (k 0)) (d (n "structopt") (r "~0.3.26") (o #t) (d #t) (k 0)))) (h "0pg64q4zy369g1klilxna573ia9h3vpiz843f5i9jck07aafpqm9") (f (quote (("default" "cli") ("cli" "directories" "structopt"))))))

