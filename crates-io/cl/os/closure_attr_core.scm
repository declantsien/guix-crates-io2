(define-module (crates-io cl os closure_attr_core) #:use-module (crates-io))

(define-public crate-closure_attr_core-0.1.0 (c (n "closure_attr_core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1a0ydq3894lzbn3407rcxqswy93rwwjjqi1l6nl4qg1vczxzvr6n")))

(define-public crate-closure_attr_core-0.2.0 (c (n "closure_attr_core") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1fs7hi6sgxkyrj94p6rf4lw0bx4410mz3k1332bbs1m8lrnkg4j2")))

(define-public crate-closure_attr_core-0.3.0 (c (n "closure_attr_core") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0iin3lhc1kza53f18gqimajhf0zfcvm5fa05xq0m3af9rnwm7w95")))

(define-public crate-closure_attr_core-0.4.0 (c (n "closure_attr_core") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0q0flmw5am6z5d1l5lwmpbkmy1d0vxaw73zci0bnjkadwv734i87")))

