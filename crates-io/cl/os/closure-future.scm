(define-module (crates-io cl os closure-future) #:use-module (crates-io))

(define-public crate-closure-future-0.1.0 (c (n "closure-future") (v "0.1.0") (d (list (d (n "async-oneshot") (r "^0.5.0") (d #t) (k 0)) (d (n "pollster") (r "^0.2.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1yiblzbhji7zfizcyi9xyvk5d8wk0crf59db53h17nsimh5lamsv") (f (quote (("nightly-docs"))))))

