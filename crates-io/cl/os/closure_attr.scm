(define-module (crates-io cl os closure_attr) #:use-module (crates-io))

(define-public crate-closure_attr-0.1.0 (c (n "closure_attr") (v "0.1.0") (d (list (d (n "closure_attr_core") (r "^0.1.0") (d #t) (k 0)))) (h "1fy3njgp3qrzqafq1fwvsx172d18mrpd5crf889l0izmrm9sf1wk")))

(define-public crate-closure_attr-0.2.0 (c (n "closure_attr") (v "0.2.0") (d (list (d (n "closure_attr_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1q1rhwkhaa8qmwqpfnh66xj3p8ghw9xqzn1h5cma0m8ybcy7plh3")))

(define-public crate-closure_attr-0.3.0 (c (n "closure_attr") (v "0.3.0") (d (list (d (n "closure_attr_derive") (r "^0.3.0") (d #t) (k 0)))) (h "0qx5677pzqf3ys68fvi0p58wl4514s5k5651a7xnc4cpyy7gq7cz")))

(define-public crate-closure_attr-0.4.0 (c (n "closure_attr") (v "0.4.0") (d (list (d (n "closure_attr_derive") (r "^0.4.0") (d #t) (k 0)))) (h "1ilgz8c12v9p6qr0nxf6aw7p9mzd1kk3vwccnw0c7k6y4kfig5db")))

