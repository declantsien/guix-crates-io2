(define-module (crates-io cl os closest-sum-pair) #:use-module (crates-io))

(define-public crate-closest-sum-pair-0.1.0 (c (n "closest-sum-pair") (v "0.1.0") (h "0rc3rsjzaczf2f9dyjkln3fpcn0k74rk7j1zfq2rvndjm1670f48") (y #t)))

(define-public crate-closest-sum-pair-0.1.1 (c (n "closest-sum-pair") (v "0.1.1") (h "0bvs7s4xgcfi13156y5w0ml844a5ljbd41chqjvkasgc1mrks6mb") (y #t)))

(define-public crate-closest-sum-pair-0.1.2 (c (n "closest-sum-pair") (v "0.1.2") (h "10rqafasz9d6sq43jy7i77w4w3myl8z1cd6ahr7599vzmyvbprz4") (y #t)))

(define-public crate-closest-sum-pair-0.1.3 (c (n "closest-sum-pair") (v "0.1.3") (h "106pyn3j3wqcwv5c7mv0lygc1q920amg38qjnprfa0lqd4xyjxk6") (y #t)))

(define-public crate-closest-sum-pair-0.1.4 (c (n "closest-sum-pair") (v "0.1.4") (h "031pbg6j8b4cpgjwpw5sim05dlri2j7djdhk0b0ihzwv5h75f3gf") (y #t)))

(define-public crate-closest-sum-pair-0.1.5 (c (n "closest-sum-pair") (v "0.1.5") (h "1m220fk4ia2fnbfb5af54smlw2zyssr5d5v8rs567hqhfv2j0zmj") (y #t)))

(define-public crate-closest-sum-pair-0.1.6 (c (n "closest-sum-pair") (v "0.1.6") (h "19b0nzwh81949411qh4zcmi0qyx7r6103fdriz7pw0k3llncg5aa") (y #t)))

(define-public crate-closest-sum-pair-0.1.7 (c (n "closest-sum-pair") (v "0.1.7") (h "1pbkyhc3dpcrc5pja0ssxd5j2ld7fdgj26q41fhzazrkwihr8qc6") (y #t)))

(define-public crate-closest-sum-pair-0.1.8 (c (n "closest-sum-pair") (v "0.1.8") (h "07m2h9dg3wd88s8ic3fwlbi0r9b86b7jc3q9dzfn2wjbjq8a0mr4") (y #t)))

(define-public crate-closest-sum-pair-0.1.9 (c (n "closest-sum-pair") (v "0.1.9") (h "1ld9yd8gc6c1dzlxkbs5aj4mg6cp6brskmixmhh33jvmbcpj2zb5") (y #t)))

(define-public crate-closest-sum-pair-0.2.0 (c (n "closest-sum-pair") (v "0.2.0") (h "1zpp6wz0wkxvc1jvcdfcc35gbczq4c81scmld3fybfsspcw8359m") (y #t)))

(define-public crate-closest-sum-pair-0.3.0 (c (n "closest-sum-pair") (v "0.3.0") (h "12adgz7435ka1y8hgkzlky0mvr9bayq8a336racvz9f4p83h29zm") (y #t)))

(define-public crate-closest-sum-pair-0.4.0 (c (n "closest-sum-pair") (v "0.4.0") (h "0bfrmi4cyb62knf9yj9a7dnrq2m97365kg5skb8718qdkakzwajz") (y #t)))

(define-public crate-closest-sum-pair-0.4.1 (c (n "closest-sum-pair") (v "0.4.1") (h "0jkwbm2i9vr6cw8ni6fsnrpbj7h428gan2bvp16bbg3xkmrwdg6r") (y #t)))

(define-public crate-closest-sum-pair-0.5.0 (c (n "closest-sum-pair") (v "0.5.0") (h "0bb7n4rg8ay55z5qcqdpmd3x1j3lqvfnrf76h7bm1y8v7wgsj2c8") (y #t)))

(define-public crate-closest-sum-pair-1.0.0 (c (n "closest-sum-pair") (v "1.0.0") (h "1hqa991si9qv3w6vvxywnkfmydhs984wwar4d125zpwacwpagf9d")))

