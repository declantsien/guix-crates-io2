(define-module (crates-io cl os close-err) #:use-module (crates-io))

(define-public crate-close-err-1.0.0 (c (n "close-err") (v "1.0.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0ma3c1n929ca51p1nb4qlgkbd65618c2sl9f689xplmcr0yxh3ag")))

(define-public crate-close-err-1.0.1 (c (n "close-err") (v "1.0.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "18pfhb3gqrlc8p1q4hb3c79g6ywgjj2kpyq4pycikl2jr8h1pf3k")))

(define-public crate-close-err-1.0.2 (c (n "close-err") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "1zmvkksg47vszgk5lzzdslyjdf1rjnka40f7mxfczcr44v8dvp5h")))

