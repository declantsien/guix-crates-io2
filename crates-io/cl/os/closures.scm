(define-module (crates-io cl os closures) #:use-module (crates-io))

(define-public crate-closures-0.1.0 (c (n "closures") (v "0.1.0") (h "0wkbi6ln04f3fwaqs8wgiwbkssx1pjcild338910gsny0zmksbql")))

(define-public crate-closures-0.1.1 (c (n "closures") (v "0.1.1") (h "18inph7hyl5y2q9jhdcpladv6w8ciwz5lpahc2b5y45hcxfgsrh0")))

(define-public crate-closures-0.1.2 (c (n "closures") (v "0.1.2") (h "0gchfpxggw9vwcgchjakxasdg0zjh7k50vkd5xmy9lhg35zl42rp")))

