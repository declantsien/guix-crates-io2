(define-module (crates-io cl os closure_cacher) #:use-module (crates-io))

(define-public crate-closure_cacher-0.1.0 (c (n "closure_cacher") (v "0.1.0") (h "0n4gi9mv4g00f59ny7cylpc7sr6dh8vzna2ynsclrpjmfk9dnwa5") (y #t)))

(define-public crate-closure_cacher-0.1.1 (c (n "closure_cacher") (v "0.1.1") (h "1b9dvyrya3fhbmvvj9mxcb4hgakwhf9c4ddrzvj9rmvgawd6x2pp") (y #t)))

(define-public crate-closure_cacher-0.1.2 (c (n "closure_cacher") (v "0.1.2") (h "1mxf31z13k3agd0k99417nhwhi7zkivzy0146r3ahwpi1bqh58n2") (y #t)))

(define-public crate-closure_cacher-0.2.0 (c (n "closure_cacher") (v "0.2.0") (h "0qkaapkwmgpx6fcwh9zax08cihgkgf966axin71s5i6wq5kmk46h") (y #t)))

(define-public crate-closure_cacher-0.2.1 (c (n "closure_cacher") (v "0.2.1") (h "0nwsg26v1sbdafzk8qksvx4j88z69pwlwwn5ji1q8h56945rjvl4")))

