(define-module (crates-io cl yt clytia) #:use-module (crates-io))

(define-public crate-clytia-0.1.0 (c (n "clytia") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0vzlr4jczqsmp704p4i8b0q1i6zjsysmy944v6jmmz1a4paqxb6m")))

(define-public crate-clytia-0.1.1 (c (n "clytia") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ph8mh4djzqbbplhan9rv6idh8nxvp8fw2qrh18z9jphgw5bmbjr")))

(define-public crate-clytia-0.2.0 (c (n "clytia") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "19w2bzmyl658hayx11snzz5b9qk0wr7lxhsmx2nz6gckp963xbdv")))

(define-public crate-clytia-0.2.1 (c (n "clytia") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1jrjg5748w3494r3pgyf0gvwq4by23ngm145g9y4p991adz1k30i")))

