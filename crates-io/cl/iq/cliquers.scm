(define-module (crates-io cl iq cliquers) #:use-module (crates-io))

(define-public crate-cliquers-0.0.1 (c (n "cliquers") (v "0.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "0ad0b9ixiba2cmvr0sarl274gp5x3hbz9rbxjgjg5ip1mgssxp4x")))

(define-public crate-cliquers-0.1.0 (c (n "cliquers") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "1hv4a9vlpx9ly15hmjdj6lswwsk4p0z37pkgi5xavqqnbq4lviz2")))

(define-public crate-cliquers-0.2.0 (c (n "cliquers") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "10irvwliw9ffqlrwjb11djjig5md7wvsq9zyp5yybmg6wghnx5bd")))

(define-public crate-cliquers-0.3.0 (c (n "cliquers") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "1js1113vys5f6c34i3l3riz9zh0507qlbnvpfk64xavrx7rbaq7y")))

