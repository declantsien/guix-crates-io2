(define-module (crates-io cl uc clucstr) #:use-module (crates-io))

(define-public crate-clucstr-0.1.0 (c (n "clucstr") (v "0.1.0") (h "1g7gmdpz9hlf2lggk1ljb5lf6dckl5jmj74jjpwxyi3awrk9khlm")))

(define-public crate-clucstr-0.1.1 (c (n "clucstr") (v "0.1.1") (h "0lapcvdfsxargc92d0ps6hfqrpz8gylbkagkzq0vp1gw32lfj5cc")))

(define-public crate-clucstr-0.1.2 (c (n "clucstr") (v "0.1.2") (h "1j57m23rmxanm4r9rx9ds3f31b0blb89i1vajn54kkigsf40avda")))

(define-public crate-clucstr-0.1.3 (c (n "clucstr") (v "0.1.3") (h "0l9xwlkmvj36z8lp11mbx86i1ya7x5xzbyrdl20l3zzf0q0dgb89")))

(define-public crate-clucstr-0.1.4 (c (n "clucstr") (v "0.1.4") (h "1wyk4ax1fxrlf85r4wd9m54z4bcxb11wld3bp8glkypgxsiabw65")))

(define-public crate-clucstr-0.1.6 (c (n "clucstr") (v "0.1.6") (h "0r8is2id9spfn5nnc0zygkfqx2ribk16491dq40kvppn89ip3djs")))

(define-public crate-clucstr-0.1.7 (c (n "clucstr") (v "0.1.7") (h "1h74pqi6d6icb28k6avk398nzpbvpmmagq1nnndrh1vimyfpl9gv")))

(define-public crate-clucstr-1.1.9 (c (n "clucstr") (v "1.1.9") (h "0hrz7fjjaj8jwnp6znnjbljls24c734mrgwhc1jqcmfvhbv2q8sn")))

(define-public crate-clucstr-1.1.91 (c (n "clucstr") (v "1.1.91") (h "08bzn5pmhz4c82sazmc8j3cyp5hcdygna5a30y679px506vibzyr")))

(define-public crate-clucstr-1.2.0 (c (n "clucstr") (v "1.2.0") (d (list (d (n "memchr") (r "^2.7.2") (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)))) (h "0d3z0qfapvhf5vvlnwrf16m009xnz7y7ah93c6n5nxhdqvw62gab")))

