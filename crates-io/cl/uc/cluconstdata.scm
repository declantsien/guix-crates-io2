(define-module (crates-io cl uc cluconstdata) #:use-module (crates-io))

(define-public crate-cluConstData-1.2.3 (c (n "cluConstData") (v "1.2.3") (h "0fv9kdkp4n27pcy9iccd2jhmckszsc46cwgyj01xqb6cdsjbrsv6")))

(define-public crate-cluConstData-1.2.4 (c (n "cluConstData") (v "1.2.4") (h "12mfbk48aknl1c29qrb5r1mjj8ghyfhs3sj9kismjrf5wcxirrr4")))

(define-public crate-cluConstData-1.2.6 (c (n "cluConstData") (v "1.2.6") (d (list (d (n "cluFullTransmute") (r "^1.0.1") (d #t) (k 0)))) (h "0096ks9a2ccj8vgqqybg42b9m7jsjjisw8jz7kn6m3h0b1ynx0gd") (y #t)))

(define-public crate-cluConstData-1.2.7 (c (n "cluConstData") (v "1.2.7") (d (list (d (n "cluFullTransmute") (r "^1.0.1") (d #t) (k 0)))) (h "0br9nyyx0rrpckbm4q7p0pq5wmksxw5cyjgrl9w8z6xh379xxpc7")))

(define-public crate-cluConstData-1.2.8 (c (n "cluConstData") (v "1.2.8") (d (list (d (n "cluFullTransmute") (r "^1.0.2") (d #t) (k 0)))) (h "00rijw4mqig3cdp005pr59090w129m0hgw9dnxpp05xj9rjf4inx")))

(define-public crate-cluConstData-1.2.9 (c (n "cluConstData") (v "1.2.9") (d (list (d (n "cluFullTransmute") (r "^1.0.2") (d #t) (k 0)))) (h "0vim6lw64b9ss1rjbv3pf5nmmqrkzikrs1gp6vifkz8isr6zf7kk")))

(define-public crate-cluConstData-1.3.0 (c (n "cluConstData") (v "1.3.0") (d (list (d (n "cluFullTransmute") (r "^1.0.2") (d #t) (k 0)))) (h "0kdag2j1sq9fs5j4kf165xpf5xxg32vljqgz9wqgb2xjzpwg6sbj")))

