(define-module (crates-io cl uc clucolor) #:use-module (crates-io))

(define-public crate-cluColor-0.1.0 (c (n "cluColor") (v "0.1.0") (h "1b6lh676vgrakri155zg8qwswllmqj02p32llpz2d7ywv8aa47l4")))

(define-public crate-cluColor-0.1.1 (c (n "cluColor") (v "0.1.1") (h "00n143cl6rfl7qgkhbzlr0zkc69kkpan5gdx6mzyfjhqnm4clw98")))

(define-public crate-cluColor-0.1.2 (c (n "cluColor") (v "0.1.2") (h "0ybl4d7vsdh5r38wpglkzmryqrsgfmksfgifba20mk1zs2nb8bpd")))

(define-public crate-cluColor-0.1.3 (c (n "cluColor") (v "0.1.3") (h "1mgm70r8qi9dhhlm5w8l1vqq8ch5h3c749jspd4kaic6za7h3yci")))

(define-public crate-cluColor-0.1.4 (c (n "cluColor") (v "0.1.4") (h "089rz6sh8bnxpazmqsg6aza7wvssiijimb51hs2pqk9l55cl4w7d")))

(define-public crate-cluColor-0.1.5 (c (n "cluColor") (v "0.1.5") (h "08275lrigfz50hzywpdh0ndaj9pj8avfmlxqicqhfk232338bn4x")))

