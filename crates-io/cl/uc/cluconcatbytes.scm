(define-module (crates-io cl uc cluconcatbytes) #:use-module (crates-io))

(define-public crate-cluConcatBytes-0.1.4 (c (n "cluConcatBytes") (v "0.1.4") (h "1jsqfj074c690i6lg6z9l5gzshz4lq03zb0h4lp788f1xmx4gb34")))

(define-public crate-cluConcatBytes-0.1.5 (c (n "cluConcatBytes") (v "0.1.5") (h "1mkmkjg2nw2crdpcm0ncy0gxjf9dh8ykqr4q4a1y6cv5zm6jm3yk")))

