(define-module (crates-io cl eu cleu-orm-derive) #:use-module (crates-io))

(define-public crate-cleu-orm-derive-0.1.0 (c (n "cleu-orm-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "trybuild") (r "^1.0") (k 2)))) (h "1czwc8h1yqy846nzhmn2sb1943a3na7kxs0d9ksp1ckiisk3zp4w") (f (quote (("default"))))))

