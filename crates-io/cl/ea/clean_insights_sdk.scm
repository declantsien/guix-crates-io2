(define-module (crates-io cl ea clean_insights_sdk) #:use-module (crates-io))

(define-public crate-clean_insights_sdk-2.0.0 (c (n "clean_insights_sdk") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (f (quote ("charset"))) (d #t) (k 0)))) (h "0pdlpy8azfmpcgz2g0wdf6fp6j9svbdgd8q119c0hlz7nynxm0ry")))

(define-public crate-clean_insights_sdk-2.0.1 (c (n "clean_insights_sdk") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (f (quote ("charset"))) (d #t) (k 0)))) (h "1z16ng7mw1irx1pklb27znkvxi2i5w3849wp1i126y3hwzna4gjb")))

(define-public crate-clean_insights_sdk-2.1.0 (c (n "clean_insights_sdk") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (f (quote ("charset"))) (d #t) (k 0)))) (h "015hpflqv7xa242x7inyfqfgn1130204n2664plyqf5mhyslx416")))

(define-public crate-clean_insights_sdk-2.1.1 (c (n "clean_insights_sdk") (v "2.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (f (quote ("charset"))) (d #t) (k 0)))) (h "15abb69s379pgd15fx2girp1rzy06kh46zp19x5p7xl7qkp9z9sc")))

(define-public crate-clean_insights_sdk-2.2.0 (c (n "clean_insights_sdk") (v "2.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (f (quote ("charset"))) (d #t) (k 0)))) (h "0q5fz6cgkz1d6ylmg2qjbqrha4b93pn8fy2zc9zyrq40jkx5s1zs")))

(define-public crate-clean_insights_sdk-2.3.0 (c (n "clean_insights_sdk") (v "2.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (f (quote ("charset"))) (d #t) (k 0)))) (h "06r4fyvh2hdc1fgzcdg0jgcj09g8w5a3rq1ny37paxfik2rin5pc")))

