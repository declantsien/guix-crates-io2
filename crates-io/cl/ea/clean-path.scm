(define-module (crates-io cl ea clean-path) #:use-module (crates-io))

(define-public crate-clean-path-0.2.0 (c (n "clean-path") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0hbnb4v199iz7nbi6zk5r6immhz1bx738dk9sysg6jbq5aknpvk9")))

(define-public crate-clean-path-0.2.1 (c (n "clean-path") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "19dna3ln8rbzhapijwjdxh54i9a15jvhjz3bpzlkgmx5cfrb99ma")))

