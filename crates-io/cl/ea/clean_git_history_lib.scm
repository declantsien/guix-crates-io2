(define-module (crates-io cl ea clean_git_history_lib) #:use-module (crates-io))

(define-public crate-clean_git_history_lib-0.1.0 (c (n "clean_git_history_lib") (v "0.1.0") (d (list (d (n "git2") (r "^0.13.22") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0icn2hdqvknm81szgval42gxp79fvg3djxwcn686sis2yzb3ixiq")))

(define-public crate-clean_git_history_lib-0.1.1 (c (n "clean_git_history_lib") (v "0.1.1") (d (list (d (n "git2") (r "^0.13.23") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1a5lraiy5w946q5xy8cdfjh8156sz3jz6zbk4sxn2pz4bclvadwh")))

(define-public crate-clean_git_history_lib-0.2.0 (c (n "clean_git_history_lib") (v "0.2.0") (d (list (d (n "git2") (r "^0.13.23") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "130yfbgzalwa5s14qs249rhw2gchyyrqrh52pc02p22pbcihhdlx")))

