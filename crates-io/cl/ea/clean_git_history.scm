(define-module (crates-io cl ea clean_git_history) #:use-module (crates-io))

(define-public crate-clean_git_history-0.1.0 (c (n "clean_git_history") (v "0.1.0") (d (list (d (n "git2") (r "^0.13.20") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1g6lgy7s26fxhaglizp9f0rgqgf3lmk1ls652075f3x13zcyaqv6")))

(define-public crate-clean_git_history-0.1.1 (c (n "clean_git_history") (v "0.1.1") (d (list (d (n "git2") (r "^0.13.21") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0vhvby91dhzskyjyw2kwpx6j3h36qh0hh9zavsz9pgk53gk089q9")))

(define-public crate-clean_git_history-0.1.2 (c (n "clean_git_history") (v "0.1.2") (d (list (d (n "clean_git_history_lib") (r "^0.1.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.23") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "06w5y78k9acf079c248axxkv502whhr3n9sqqxd7cp4ipwc6mqxc")))

