(define-module (crates-io cl ea cleanse) #:use-module (crates-io))

(define-public crate-cleanse-0.3.0 (c (n "cleanse") (v "0.3.0") (d (list (d (n "bstr") (r "^0.2.16") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.20") (d #t) (k 0)))) (h "0aq4982f0qypz7c0rjk5nxczj2kd7gl0z0apglka09g0fshhyqjv")))

(define-public crate-cleanse-0.4.0 (c (n "cleanse") (v "0.4.0") (d (list (d (n "bstr") (r "^0.2.16") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.20") (d #t) (k 0)))) (h "04mdgx632y26pywhj1bfmmisxn9wvh239ak0ylvg0c5ry5hcggsc")))

(define-public crate-cleanse-0.5.0 (c (n "cleanse") (v "0.5.0") (d (list (d (n "bstr") (r "^0.2.16") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.20") (d #t) (k 0)))) (h "18nxjxdqw0libsv5mxg9qhfnj5p89m3463zz4kzb1rwjcflmr9bi")))

(define-public crate-cleanse-0.6.0 (c (n "cleanse") (v "0.6.0") (d (list (d (n "bstr") (r "^0.2.16") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.20") (d #t) (k 0)))) (h "150c8fldw0p96jawmhllp8sr63c05kykg87mn2xf808sysxbmazf")))

