(define-module (crates-io cl ea clearcheck) #:use-module (crates-io))

(define-public crate-clearcheck-0.0.1 (c (n "clearcheck") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "0ngc5ciskdkskfb8s26iq7wid6a5z2qms07givfy08r5i1z5ykv2") (s 2) (e (quote (("regex" "dep:regex") ("num" "dep:num") ("file" "dep:walkdir") ("date" "dep:chrono"))))))

(define-public crate-clearcheck-0.0.2 (c (n "clearcheck") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "17fgsfz1vz6d038wbnp1sc4wjzpzy8r3rmrhjmcs3n71lpprdfgi") (s 2) (e (quote (("regex" "dep:regex") ("num" "dep:num") ("file" "dep:walkdir") ("date" "dep:chrono"))))))

