(define-module (crates-io cl ea clean_dynamodb_store) #:use-module (crates-io))

(define-public crate-clean_dynamodb_store-0.0.1 (c (n "clean_dynamodb_store") (v "0.0.1") (d (list (d (n "aws-config") (r "^1.1.9") (f (quote ("behavior-version-latest"))) (d #t) (k 0)) (d (n "aws-sdk-dynamodb") (r "^1.20.0") (f (quote ("behavior-version-latest"))) (d #t) (k 0)))) (h "00gjvv6kqr52sd6nl2s4hc1yvnba1l1ba9yzwni3zg6d5nhmhliq")))

(define-public crate-clean_dynamodb_store-0.0.2 (c (n "clean_dynamodb_store") (v "0.0.2") (d (list (d (n "aws-config") (r "^1.1.9") (f (quote ("behavior-version-latest"))) (d #t) (k 0)) (d (n "aws-sdk-dynamodb") (r "^1.20.0") (f (quote ("behavior-version-latest"))) (d #t) (k 0)))) (h "1q152syjck9kj7ajid3nbaz3dpi4wadd89s4c93fbi32fn3a6g1i")))

