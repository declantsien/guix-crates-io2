(define-module (crates-io cl ea clear-ml) #:use-module (crates-io))

(define-public crate-clear-ml-0.1.0 (c (n "clear-ml") (v "0.1.0") (h "0ava8zvzva0sm8rvmqhcs4jch2fp5dgf98iymdzl6yz504v9lmfg")))

(define-public crate-clear-ml-0.1.1 (c (n "clear-ml") (v "0.1.1") (h "1g292cil5zh6w3azi2ix9f4d141p942mrl3a5kcr86kd5ch9wr3g")))

