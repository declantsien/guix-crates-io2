(define-module (crates-io cl ea clear-coat) #:use-module (crates-io))

(define-public crate-clear-coat-0.0.3 (c (n "clear-coat") (v "0.0.3") (d (list (d (n "iup-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.1.6") (d #t) (k 0)))) (h "0vb6c53vxgr0v1w0sz09a10hsc97cslswfm7jv8xayvgl58krnmy")))

