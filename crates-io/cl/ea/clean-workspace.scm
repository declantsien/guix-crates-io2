(define-module (crates-io cl ea clean-workspace) #:use-module (crates-io))

(define-public crate-clean-workspace-1.0.0 (c (n "clean-workspace") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "12s7aivcdv99av05xc4wxjq69lpc0lk7zd3kifa68s5dv34ljsqd")))

(define-public crate-clean-workspace-1.0.1 (c (n "clean-workspace") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "01gjsbzq0i69hr9gyamfpxrpddl8h97sw2kv5qz19dhh4j3qfqp7")))

(define-public crate-clean-workspace-1.1.0 (c (n "clean-workspace") (v "1.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s8b5wbzll5zlywdz7yyws4wh87ra9zkmcwdjk5xi1b1xzwyrbjx")))

