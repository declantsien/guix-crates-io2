(define-module (crates-io cl ea clean-base) #:use-module (crates-io))

(define-public crate-clean-base-0.1.0 (c (n "clean-base") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^2") (f (quote ("uuid"))) (d #t) (k 0)))) (h "187z9sj9985a7kfq1s1ckrs7lw0wcfaa60lgnzsp4mlgjr651qgw")))

(define-public crate-clean-base-0.1.1 (c (n "clean-base") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^2") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1k4hdi2f0xpxvkyr3fjjyzq30ga7h6f5x7gkfzpw0klpc42chkfr")))

(define-public crate-clean-base-0.2.0 (c (n "clean-base") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^2") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1kykba8nvifm6a3lfw53pdzqrcsf5745zh6n85vdrsc2qd8smibv")))

(define-public crate-clean-base-0.2.1 (c (n "clean-base") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^2") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1i4sl2i7m1fak86bb92x3h5xy6w8bdw4s1h32vasxqigvimgp1dx")))

(define-public crate-clean-base-0.2.2-pre-release (c (n "clean-base") (v "0.2.2-pre-release") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^2") (f (quote ("uuid"))) (d #t) (k 0)))) (h "0lj4gq2y40x23imx64i1ac5rdfw7jgbxil6x60wsbqvqy0igw8vy")))

(define-public crate-clean-base-0.2.2 (c (n "clean-base") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^2") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1pcxwgr6k34bkkdrdhrjb7l0jjk9ybgxfcqlkq30rvhr8p96iwh0")))

(define-public crate-clean-base-0.3.0 (c (n "clean-base") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "01v8dfmy34zi0fphn9y7wpwkpi21rzr03incr7sng42j771lmsg1")))

(define-public crate-clean-base-0.4.0 (c (n "clean-base") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "0dbc9rlg357rwmvkla4gw6xmf6b5psbi69xanr3app01clxn4ciw")))

(define-public crate-clean-base-0.4.1 (c (n "clean-base") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "10gj90vsagyzxshs0524hhczir9lr5cryfx0yzchr93r9q9nzskq")))

(define-public crate-clean-base-0.5.0 (c (n "clean-base") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1y0sny9i15cfddq2p6w0l7yzb3cvrg85am0il4zymwjm5bicdb58")))

(define-public crate-clean-base-0.5.1 (c (n "clean-base") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "0rxv4dkl4p79rfz85lcsqqabl28ikgcmxxhikznc4rgbzniyx23h")))

(define-public crate-clean-base-0.5.2 (c (n "clean-base") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "0vlmx274flmm06zrksmsby9h3jbsf2hzlp9wzm7ijr9lcmswphhr")))

(define-public crate-clean-base-0.5.3 (c (n "clean-base") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "0k5igqgr6p1l0x0qdvsf1w7hhg2928s9l4r09pc44ryxfr9n721b")))

(define-public crate-clean-base-0.5.4 (c (n "clean-base") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1cv5898zdkykpf936blchb5y4lzgrgc8s7s58zpg45b856j7kyhx")))

(define-public crate-clean-base-0.6.1 (c (n "clean-base") (v "0.6.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1g60yc3wd39zpn7ddw2qz8n95ysb8yzijrk2wbg3sq17rqg14qdq")))

(define-public crate-clean-base-0.6.2 (c (n "clean-base") (v "0.6.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1bl4bmqrnq377nsp2x3yqv84l2dxyl6z56lhi1nfkj77zmckwdpa")))

(define-public crate-clean-base-0.6.3 (c (n "clean-base") (v "0.6.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "158ng74av2jyaiswar17winjf11a5lkchp0nd6k1cb1s8zzzzmcz")))

(define-public crate-clean-base-0.6.4 (c (n "clean-base") (v "0.6.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1xkalrs1hbnb3kwa50ddy48iix8a9wz9vzqcbhd14zsm4fxddixx")))

(define-public crate-clean-base-0.6.5 (c (n "clean-base") (v "0.6.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "184lcw07ig30nyiwicyv8vqhm8bbqpczbmlhap1srp4275hh0iqa")))

(define-public crate-clean-base-0.6.6 (c (n "clean-base") (v "0.6.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1blp7jz6azq80nka1nmsxgyhngzacrfnlqlc6in7s6j91njp94mx")))

(define-public crate-clean-base-0.6.7 (c (n "clean-base") (v "0.6.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "0pv7jiq30lxgb2ayc977096qmw41gjh21fp3zw2gqah2gkqwd22f")))

(define-public crate-clean-base-0.6.8 (c (n "clean-base") (v "0.6.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "16jigvjnvzwj3j2cfhv02k15r3d3lrpkgd7kj53hr8mal3h36i4h")))

(define-public crate-clean-base-0.6.9 (c (n "clean-base") (v "0.6.9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "0xr8qvjhamp7ndyabqcm5zzmr8aidhihcnpgw1bg06a8xvbw45mv")))

(define-public crate-clean-base-1.0.1 (c (n "clean-base") (v "1.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1wyrkjnsw9y516i7z33m159by90bjz0parj2mkn3zh2is9fc2s5p")))

(define-public crate-clean-base-1.1.0 (c (n "clean-base") (v "1.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("uuid"))) (d #t) (k 0)))) (h "01jl6cf31iichbs0fvc6nq5r8fwi9i2vqglamrim89h277pswayf")))

(define-public crate-clean-base-1.1.1 (c (n "clean-base") (v "1.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^4") (f (quote ("uuid"))) (d #t) (k 0)))) (h "0q5mac7mgfqxn3xklnfvba8ay8f2mbbfilrm0c10mqr6zlhlz23n")))

(define-public crate-clean-base-1.2.0 (c (n "clean-base") (v "1.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "utoipa") (r "^4") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1bawgkxg0g06ch7zfm7a0wy38j42irmwa6677vc8hnlhnmxami28")))

