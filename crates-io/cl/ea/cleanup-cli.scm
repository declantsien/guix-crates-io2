(define-module (crates-io cl ea cleanup-cli) #:use-module (crates-io))

(define-public crate-cleanup-cli-0.1.0 (c (n "cleanup-cli") (v "0.1.0") (d (list (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1fxj01b2psgbdx1vkvqj7inas2cbvyzl2yrb0azakpx8k4w1y4p5")))

(define-public crate-cleanup-cli-0.1.1 (c (n "cleanup-cli") (v "0.1.1") (d (list (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0766gxr0dz07i6hkk6kvv7awbpa9dydkv9k5cxcfhs8yzwcjwdd1")))

(define-public crate-cleanup-cli-0.1.2 (c (n "cleanup-cli") (v "0.1.2") (d (list (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "014gx6r3y1a88bp3qm05k9b7ysdhw99dk3vxdynpam0b7d0k35xr")))

