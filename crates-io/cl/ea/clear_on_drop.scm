(define-module (crates-io cl ea clear_on_drop) #:use-module (crates-io))

(define-public crate-clear_on_drop-0.1.0 (c (n "clear_on_drop") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "09v7443gx5rh1ix1jqi1dz2xl5vlw6hgqiphcrabdqvk9li2r7pj") (f (quote (("no_cc") ("nightly" "no_cc"))))))

(define-public crate-clear_on_drop-0.2.0 (c (n "clear_on_drop") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "148fq31mcsfv8ahxjfqmgscvj0n0zsqhfykcx9p0bwbhsvyqib0y") (f (quote (("no_cc") ("nightly" "no_cc"))))))

(define-public crate-clear_on_drop-0.2.1 (c (n "clear_on_drop") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1zg5vrgzgnm58b6xyn6qd84zjlj81hp9icz9z1fszmmi9yb3amm0") (f (quote (("no_cc") ("nightly" "no_cc"))))))

(define-public crate-clear_on_drop-0.2.2 (c (n "clear_on_drop") (v "0.2.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "19nkyrbpkbkn1a471vldgliyljyqmp1fdkn8n7ayjzqfv26l1z17") (f (quote (("no_cc") ("nightly" "no_cc"))))))

(define-public crate-clear_on_drop-0.2.3 (c (n "clear_on_drop") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05qxifm7pkms9gnvr50j67x59m3br5fg68yfcrmv9zr7w40nh9wp") (f (quote (("no_cc") ("nightly" "no_cc"))))))

(define-public crate-clear_on_drop-0.2.4 (c (n "clear_on_drop") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "023v3g9b9y6gkhpwbmkdzw5qbk8p607vnnvdk3xc755jcns5vk69") (f (quote (("no_cc") ("nightly" "no_cc"))))))

(define-public crate-clear_on_drop-0.2.5 (c (n "clear_on_drop") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (f (quote ("cargo_bench_support" "html_reports"))) (d #t) (k 2)))) (h "08vl9ykz9fsiy6iwc3px66mmv3nlvgx6d6dwzr4017wpyiiqll1q") (f (quote (("no_cc") ("nightly" "no_cc"))))))

