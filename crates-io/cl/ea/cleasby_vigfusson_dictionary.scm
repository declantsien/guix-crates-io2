(define-module (crates-io cl ea cleasby_vigfusson_dictionary) #:use-module (crates-io))

(define-public crate-cleasby_vigfusson_dictionary-1.0.0 (c (n "cleasby_vigfusson_dictionary") (v "1.0.0") (d (list (d (n "insta") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y8mb2cfz0akq6rflhf7gwaljpiy9cp7hvmqq2nk54vw21ykcxgq")))

(define-public crate-cleasby_vigfusson_dictionary-1.1.0 (c (n "cleasby_vigfusson_dictionary") (v "1.1.0") (d (list (d (n "insta") (r "^1.21") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "095ki2mvhpmm5dw5xi2v7jncb4npxyq5f5xb32y375pdbsinqc41")))

