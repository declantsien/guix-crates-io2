(define-module (crates-io cl ea clean-macro-docs) #:use-module (crates-io))

(define-public crate-clean-macro-docs-1.0.0 (c (n "clean-macro-docs") (v "1.0.0") (d (list (d (n "if_chain") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bvgygk898pvsv95vvywxy25xkwy01zxhdx11g7wfnamj6k137ia")))

(define-public crate-clean-macro-docs-1.0.1 (c (n "clean-macro-docs") (v "1.0.1") (d (list (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vdivvk6v0ilb4pk1fhi1ld2vz59ksggjf7m2vgrd54rq9vrzmjz")))

(define-public crate-clean-macro-docs-1.0.2 (c (n "clean-macro-docs") (v "1.0.2") (d (list (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g7q1zdp3vyyw3wlg0a6qhmxz83cf82yn1cy7rvjddka13pvmkz3")))

