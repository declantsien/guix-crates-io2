(define-module (crates-io cl ea cleanup-history) #:use-module (crates-io))

(define-public crate-cleanup-history-0.1.0 (c (n "cleanup-history") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "162sysv1kqj2kbm2gcw8y91n9136ikgyh8ad04jgcvclfasx330c")))

(define-public crate-cleanup-history-0.1.1 (c (n "cleanup-history") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "04cl6axz20pi79nfqqv6v5rinky8pa5b0a3q17c7fz58xllxn879")))

(define-public crate-cleanup-history-0.2.0 (c (n "cleanup-history") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0b027i12rigfly7xhapysk24xrcrqx62yn7iklq7r0fh8lv4bw1x")))

