(define-module (crates-io cl ea cleanroom) #:use-module (crates-io))

(define-public crate-cleanroom-0.0.0 (c (n "cleanroom") (v "0.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xdg") (r "^2") (d #t) (k 0)))) (h "0gwy0397x946klaz0jibvz37910abragnsrw12kpj068318axa8p")))

(define-public crate-cleanroom-0.0.1 (c (n "cleanroom") (v "0.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)) (d (n "xdg") (r "^2") (d #t) (k 0)))) (h "1d18760wyiyhzkfc2y0690rnwplcms9syyd9pjq7h1hyk41j7a5w")))

(define-public crate-cleanroom-0.0.2 (c (n "cleanroom") (v "0.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)) (d (n "xdg") (r "^2") (d #t) (k 0)))) (h "1i5rhwpwcrb3ccnvh94lphb1hmj7d0n5fzh3ig0s6s8ss9w667j7")))

(define-public crate-cleanroom-0.0.3 (c (n "cleanroom") (v "0.0.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)) (d (n "xdg") (r "^2") (d #t) (k 0)))) (h "17xsgwpg4hfqjc1z221v7s4if9mm879vdm0mfvi188ll94zaj6j2")))

