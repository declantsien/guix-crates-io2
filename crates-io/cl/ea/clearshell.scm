(define-module (crates-io cl ea clearshell) #:use-module (crates-io))

(define-public crate-clearshell-0.0.1-alpha-1 (c (n "clearshell") (v "0.0.1-alpha-1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.8") (d #t) (k 0)) (d (n "dirs-2") (r "^3.0.1") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "proctitle") (r "^0.1.1") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.7") (d #t) (k 0)))) (h "0fj11sc4553c2dpycd7bi8c9d1fwc21xaib3hbn5fvl1vxmj3vc9")))

(define-public crate-clearshell-0.0.1-alpha-2 (c (n "clearshell") (v "0.0.1-alpha-2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.8") (d #t) (k 0)) (d (n "dirs-2") (r "^3.0.1") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proctitle") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.123") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.7") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0lzk6yl656gcr8s5xrzdlfcq8d3g0pzzaywwfrz5bb87hxjirc6d")))

