(define-module (crates-io cl ea cleartor) #:use-module (crates-io))

(define-public crate-cleartor-0.1.0 (c (n "cleartor") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "config") (r "^0.9") (f (quote ("toml"))) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-socks2") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)))) (h "111rw9szk27xp1kj1mbah5k2s17bq8b61c7zgid6mg54v4wn39f9")))

(define-public crate-cleartor-0.1.1 (c (n "cleartor") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "config") (r "^0.9") (f (quote ("toml"))) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-socks2") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)))) (h "1h1zni9yq4jviqljkyrk0qvh3hq877l7w022rbcyrpw0hqx8pbb1")))

(define-public crate-cleartor-0.1.2 (c (n "cleartor") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "config") (r "^0.9") (f (quote ("toml"))) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-socks2") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)))) (h "0d6si7f5vbj09jqq1q2rxaqaa109i0bixrjlp8s83n55vg35ij0r")))

(define-public crate-cleartor-0.1.3 (c (n "cleartor") (v "0.1.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "config") (r "^0.9") (f (quote ("toml"))) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-socks2") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)))) (h "13lipv3fp35qz5wq8jmr9pl7i7w4virhhpads2h1gmqn63h1k0xf")))

(define-public crate-cleartor-0.1.4 (c (n "cleartor") (v "0.1.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "config") (r "^0.9") (f (quote ("toml"))) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-socks2") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)))) (h "1ljqib53pcfqbi4g8135ilr4rnmr9l53b1qbs4mk9ab0kzgrpdh1")))

(define-public crate-cleartor-0.1.5 (c (n "cleartor") (v "0.1.5") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "config") (r "^0.9") (f (quote ("toml"))) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-socks2") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)))) (h "0cxcdalaq9g2ya5pznyj30h8gwci8b5s93c0gld7mr2rl359dhgc")))

(define-public crate-cleartor-0.1.6 (c (n "cleartor") (v "0.1.6") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-socks2") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "11nc3bkqam8km368h6cwx8v8qnaihcqxn036mw3y3kfmggsz1m2s")))

