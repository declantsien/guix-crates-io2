(define-module (crates-io cl ea cleanup) #:use-module (crates-io))

(define-public crate-cleanup-0.1.0 (c (n "cleanup") (v "0.1.0") (h "04zz4jis5dc0mzl8kj2vjprycragac3p7zs7km73zwyqsc9x2706")))

(define-public crate-cleanup-0.1.1 (c (n "cleanup") (v "0.1.1") (h "0dp5hfvvix21pdqcr56w1jjygi9n0lqd181fjwv2pwdldddc0k1x")))

