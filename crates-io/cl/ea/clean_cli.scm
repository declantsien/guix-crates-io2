(define-module (crates-io cl ea clean_cli) #:use-module (crates-io))

(define-public crate-clean_cli-0.1.0 (c (n "clean_cli") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "15sb278vawjs09qh29n93zv39ki41hacfwwr0av2gy7ggh4ldg3g")))

(define-public crate-clean_cli-0.1.1 (c (n "clean_cli") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "17j23r4kmc3jhimmn8rzf00fhzp3cwz9xf5zhjlv6v8lyazs4szp")))

(define-public crate-clean_cli-0.1.2 (c (n "clean_cli") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1dy4ijg72h4b4kzfb6n8yaxq4plh6v46y0sicdci3ak3h0h7ji0k")))

