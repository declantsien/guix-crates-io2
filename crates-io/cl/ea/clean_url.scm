(define-module (crates-io cl ea clean_url) #:use-module (crates-io))

(define-public crate-clean_url-0.1.0 (c (n "clean_url") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0dzaznn3a935q6bjz9qfrgqfby02ldm3m26djjadc5bdvsf86i5p")))

(define-public crate-clean_url-0.1.1 (c (n "clean_url") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1352lgkkg0nsqwaps6m7541x07yipis8w8wl8sfyqkfi6m5bsz31")))

(define-public crate-clean_url-0.1.2 (c (n "clean_url") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "184l48sm7yjbziifi9qriyx48hv7na6yvf5lvnmz3nz36ah8ldf9")))

(define-public crate-clean_url-0.1.3 (c (n "clean_url") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0i4abphxd9k2nvj1h2jbc1js4yh2yv3acs90cqlccxhn854kg652")))

(define-public crate-clean_url-0.1.4 (c (n "clean_url") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "174gnhwmq3lbpm3fkjn9yan0x6vd6hc6js92amw7pjf0a7kag21v")))

(define-public crate-clean_url-0.1.5 (c (n "clean_url") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "01lwbnb25yb3yrzcshwps1ny1ll50apw6mvvgdff2k0zfy2gx7ic")))

(define-public crate-clean_url-0.1.6 (c (n "clean_url") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1wnrk9dw6sasg53m7n357vk8mlicq5r4vpkgd51al66spwcfja16")))

(define-public crate-clean_url-0.1.7 (c (n "clean_url") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1pswjic7b5axf5cgp21whjjgvwfyyly9pq6k6l9z843azviizdml")))

(define-public crate-clean_url-0.1.8 (c (n "clean_url") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0x6q863is5mbzr38159w3lwb40iaxfspc97a88fm9v881c9c020v")))

(define-public crate-clean_url-0.1.9 (c (n "clean_url") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1x3g5bl0bqpsz9zpqkg4yfcka55q2cnbixbxh3n25dbklhd3397y")))

