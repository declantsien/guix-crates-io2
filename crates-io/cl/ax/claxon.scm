(define-module (crates-io cl ax claxon) #:use-module (crates-io))

(define-public crate-claxon-0.0.1 (c (n "claxon") (v "0.0.1") (h "16lp5cd8qxv2cdlhz84cfsbrk87vjb4ibqbv09xiir47arynj086")))

(define-public crate-claxon-0.1.0 (c (n "claxon") (v "0.1.0") (d (list (d (n "hound") (r "^0.4.0") (d #t) (k 0)))) (h "1h4cvhb9cdnwysa5w03wan6b39irkrr2j7ycv11px0nzd4qlsq67")))

(define-public crate-claxon-0.2.0 (c (n "claxon") (v "0.2.0") (d (list (d (n "hound") (r "^1.1.0") (d #t) (k 2)))) (h "1zaf7lf92cb1x9lr3p8sqi5qss5hzr3f8xgdx01zx63jy61ynv19")))

(define-public crate-claxon-0.2.1 (c (n "claxon") (v "0.2.1") (d (list (d (n "hound") (r "^2.0.0") (d #t) (k 2)))) (h "03gp83v4b864w2jfypm54dz2rh7k1s90b32yvvgla0m3js9n7hh0")))

(define-public crate-claxon-0.3.0 (c (n "claxon") (v "0.3.0") (d (list (d (n "hound") (r "^3.0.0") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 2)))) (h "122z9vb5l1r3fic6ikpbqwvsnhgiaghripagm0yqqgrvjk4rz76r")))

(define-public crate-claxon-0.3.1 (c (n "claxon") (v "0.3.1") (d (list (d (n "hound") (r "^3.0") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 2)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "1iw4kq6551zv4y9sphbiwz5237jas2r1a01sacwxm2jri5rj7lc4")))

(define-public crate-claxon-0.4.0 (c (n "claxon") (v "0.4.0") (d (list (d (n "hound") (r "^3.0") (d #t) (k 2)) (d (n "mp4parse") (r "^0.8") (d #t) (k 2)) (d (n "ogg") (r "^0.5.1") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "1awkp1llz99wk6hdbwvbl4graza3zwmm4x1missap73jm13d0kpa")))

(define-public crate-claxon-0.3.2 (c (n "claxon") (v "0.3.2") (d (list (d (n "hound") (r "^3.0") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 2)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "1b53ysazizyrjjmvgdkrs2lcmjbins74qxz6r90jg34mrrf6zyzd")))

(define-public crate-claxon-0.4.1 (c (n "claxon") (v "0.4.1") (d (list (d (n "hound") (r "^3.0") (d #t) (k 2)) (d (n "mp4parse") (r "^0.8") (d #t) (k 2)) (d (n "ogg") (r "^0.5.1") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "0gyalrfka16cg39il657j70l3rpp2zgx8ycl9dmjxvfj9k7vmv9p")))

(define-public crate-claxon-0.3.3 (c (n "claxon") (v "0.3.3") (d (list (d (n "hound") (r "^3.0") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 2)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "1j4p47y8hs88ysh4z8k9sa2bda3ni2lbfrjv2dg90v44zybka69m")))

(define-public crate-claxon-0.4.2 (c (n "claxon") (v "0.4.2") (d (list (d (n "hound") (r "^3.0") (d #t) (k 2)) (d (n "mp4parse") (r "^0.8") (d #t) (k 2)) (d (n "ogg") (r "^0.5.1") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "001vqidga7984s79lgbrvna934qsxvyvl2dgmfkvv5d44wkrav7q")))

(define-public crate-claxon-0.4.3 (c (n "claxon") (v "0.4.3") (d (list (d (n "hound") (r "^3.0") (d #t) (k 2)) (d (n "mp4parse") (r "^0.8") (d #t) (k 2)) (d (n "ogg") (r "^0.5.1") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "1206mxvw833ysg10029apcsjjwly8zmsvksgza5cm7ma4ikzbysb")))

