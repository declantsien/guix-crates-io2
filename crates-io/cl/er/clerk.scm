(define-module (crates-io cl er clerk) #:use-module (crates-io))

(define-public crate-clerk-0.1.0 (c (n "clerk") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5") (d #t) (k 2)))) (h "092qvwyji3f3mcjyrvn26w5p9d2x2xavd42xldkzc60qf68mxb62")))

(define-public crate-clerk-0.2.0 (c (n "clerk") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5") (d #t) (k 2)))) (h "1mabzv4z3fw4c0rmsq326zd4l8293b178dsmv1sl79ribsrymz6f")))

(define-public crate-clerk-0.3.0 (c (n "clerk") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5") (d #t) (k 2)))) (h "11qc8i5123b4xcc66cs92gai0b0ax8ix2a43a7zwz3ly9kzwld38")))

(define-public crate-clerk-0.4.0 (c (n "clerk") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.1") (d #t) (k 2)))) (h "1h6sqzjmzxb76plyxvfp6lwbppp354w3bypsbwyv5w0k1mhyxa4x")))

