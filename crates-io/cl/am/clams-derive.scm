(define-module (crates-io cl am clams-derive) #:use-module (crates-io))

(define-public crate-clams-derive-0.0.1 (c (n "clams-derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1w132qdvzccpz5mzb36a4y5m9zn8s89xk27paj24pjng5drz8h10")))

(define-public crate-clams-derive-0.0.2 (c (n "clams-derive") (v "0.0.2") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "13060sciqpngmzmvb4yl1sgkanxkvwcjivzx6c3b9ks8r3hiw64m")))

(define-public crate-clams-derive-0.0.3 (c (n "clams-derive") (v "0.0.3") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1rjll0gcykradxp9gamrri50rgbnaqr2a37vlycjfhwl7g4dj4pb")))

(define-public crate-clams-derive-0.0.4 (c (n "clams-derive") (v "0.0.4") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1pi9jw1wxhdsinm9lhxa1gj3fri50i4ag0iv4bp976c933dd1gyd")))

(define-public crate-clams-derive-0.0.5 (c (n "clams-derive") (v "0.0.5") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "17z12d1cbxvxr05wdqgj7zs0gjfgwwxhgppdk6n21k0491ni5h0l")))

(define-public crate-clams-derive-0.0.6 (c (n "clams-derive") (v "0.0.6") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "197ysny03clsqrafzf4b268h3q0zdp8sp7jc4lyv3b4zz324gv1b")))

