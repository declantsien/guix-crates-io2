(define-module (crates-io cl am clamped) #:use-module (crates-io))

(define-public crate-clamped-1.0.0 (c (n "clamped") (v "1.0.0") (h "110sx22vkfdibh8wmyvpxh097z1mdvl2qi3xxzb096c6bkdd5c1p") (f (quote (("inline_always") ("default" "clamp_trait") ("clamp_trait") ("clamp_macro") ("clamp_fn"))))))

