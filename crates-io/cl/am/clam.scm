(define-module (crates-io cl am clam) #:use-module (crates-io))

(define-public crate-clam-0.1.0 (c (n "clam") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1zmvwlpxx0brgipv7yh90ccy3skmjqamqry7h3m86vj4qxgpmnzb")))

(define-public crate-clam-0.1.1 (c (n "clam") (v "0.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "13ccjaiimhvav3izbvdqlpifjr7jha8lj3rs5a8maxnr6hdrizxi")))

(define-public crate-clam-0.1.2 (c (n "clam") (v "0.1.2") (d (list (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1a1v6xwnlam9205wnirn38yrzrh334ma081yx2vknbd8i86hp9hm")))

(define-public crate-clam-0.1.3 (c (n "clam") (v "0.1.3") (d (list (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0zcydkaj2srmrl2qv8hmxfm63can0jixp694r1frcwwigjh6q5bd")))

(define-public crate-clam-0.1.4 (c (n "clam") (v "0.1.4") (d (list (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "mime") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0szbk09ffzfbpvlllm5rrwchsvzr2g0dqf2hn2307qa7s7s8gk4w")))

(define-public crate-clam-0.1.5 (c (n "clam") (v "0.1.5") (d (list (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "mime") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "17jnxl9midfiwkpkcj664bgimi6jdr9d29gl3yw7nlp6c8xjzir9")))

(define-public crate-clam-0.1.6 (c (n "clam") (v "0.1.6") (d (list (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "mime") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "155b07bl30qmww1a3fyfnk5fy6n9dw4mygd6l8a7w2hnw4a14v0y")))

