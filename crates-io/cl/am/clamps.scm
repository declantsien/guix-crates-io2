(define-module (crates-io cl am clamps) #:use-module (crates-io))

(define-public crate-clamps-0.1.0 (c (n "clamps") (v "0.1.0") (h "0rwkz9rqrssvscrim9z8nbfjs88qwln6rnws7c9gl39priw1szpq")))

(define-public crate-clamps-0.1.1 (c (n "clamps") (v "0.1.1") (h "0mch33ynv9cgpph4hyjx0c2blxhvn45b15ay9hjh68afxm4djyc6")))

(define-public crate-clamps-0.1.2 (c (n "clamps") (v "0.1.2") (h "0zivbgjfy6r7shq7garwyjnq7cxapjmvlvlxylkks6jrn3npqzqf")))

