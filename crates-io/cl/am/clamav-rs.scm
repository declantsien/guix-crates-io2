(define-module (crates-io cl am clamav-rs) #:use-module (crates-io))

(define-public crate-clamav-rs-0.5.0 (c (n "clamav-rs") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clamav-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1h5dilpdl6jr3yzkq0v7dh4mspjl3b35cw6k4yf3mx2zs1frbcbk")))

(define-public crate-clamav-rs-0.5.2 (c (n "clamav-rs") (v "0.5.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clamav-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1l91gykd46bhm91ha3ff8ajyca1j80d4f90bkas8h63i0llzyx4p")))

(define-public crate-clamav-rs-0.5.3 (c (n "clamav-rs") (v "0.5.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clamav-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1vbzp0q0cdc7y768fnmlgd23zxzvc4kbajyzbw2ck0n8swd0jsk4")))

(define-public crate-clamav-rs-0.5.5 (c (n "clamav-rs") (v "0.5.5") (d (list (d (n "bindings") (r "^0.5.5") (d #t) (t "cfg(windows)") (k 0) (p "clamav-rs-bindings")) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clamav-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0c14q9s03jhqf0ryrbgd23mcih5fw1ahn93brslfmslr7w1ahw12")))

