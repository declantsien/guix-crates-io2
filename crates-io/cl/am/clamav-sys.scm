(define-module (crates-io cl am clamav-sys) #:use-module (crates-io))

(define-public crate-clamav-sys-0.0.1 (c (n "clamav-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dd74k3xmpcmzbhqyjpjzgr6z36z6dq4c9p926qlflmjfr9xl2sq")))

(define-public crate-clamav-sys-0.0.2 (c (n "clamav-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.10") (d #t) (k 1)))) (h "09cl33b53lh9x1b9ygq15xvl8fhav1i42prg7kimjiw1n0795c0i")))

(define-public crate-clamav-sys-0.0.3 (c (n "clamav-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (t "cfg(unix)") (k 1)) (d (n "vcpkg") (r "^0.2.10") (d #t) (t "cfg(windows)") (k 1)))) (h "1j9pqcd14i4528zh6pb5xy2vy54ph92hmll19g5vyw1zdrd6sas0")))

(define-public crate-clamav-sys-0.0.4 (c (n "clamav-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (t "cfg(unix)") (k 1)) (d (n "vcpkg") (r "^0.2.10") (d #t) (t "cfg(windows)") (k 1)))) (h "1vq9a5fi64im59kp588vdimswkgfx2qs99fdbr5rjhdbk91w671x")))

(define-public crate-clamav-sys-0.0.5 (c (n "clamav-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (t "cfg(unix)") (k 1)) (d (n "vcpkg") (r "^0.2.10") (d #t) (t "cfg(windows)") (k 1)))) (h "1jgwhhp00b8s4w4c72pf9gkx28m12b9bn7im8c3r4zis6pxrl7p2")))

(define-public crate-clamav-sys-1.0.0 (c (n "clamav-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (t "cfg(unix)") (k 1)) (d (n "vcpkg") (r "^0.2.10") (d #t) (t "cfg(windows)") (k 1)))) (h "1rzwrv206wja5cmcws1h3jjlbcklxvrpvivw4zfy3yabvp9ja9ci")))

