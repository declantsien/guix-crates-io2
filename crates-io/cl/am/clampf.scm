(define-module (crates-io cl am clampf) #:use-module (crates-io))

(define-public crate-clampf-0.1.0 (c (n "clampf") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0zhacf19li05jqf45l4bqvhy3vdgpdyai8dq1j0hwdp868h7s7sb") (f (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-clampf-0.1.1 (c (n "clampf") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "05pzyffn97mijan39nkpj1510xh0nzc29pxwb0vfz5dcpcd9pi0n") (f (quote (("std" "num-traits/std") ("default" "std"))))))

