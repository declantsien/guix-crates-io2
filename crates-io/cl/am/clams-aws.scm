(define-module (crates-io cl am clams-aws) #:use-module (crates-io))

(define-public crate-clams-aws-0.0.1 (c (n "clams-aws") (v "0.0.1") (d (list (d (n "clams") (r "^0.0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rusoto_core") (r "^0.32.0") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.32.0") (d #t) (k 0)) (d (n "rusoto_sts") (r "^0.32.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0xgi459p58ifqwhxzdsj6kzlv2ir0dz0v3grkxl43phnvdj06c2k")))

