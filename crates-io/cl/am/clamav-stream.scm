(define-module (crates-io cl am clamav-stream) #:use-module (crates-io))

(define-public crate-clamav-stream-0.1.0 (c (n "clamav-stream") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("io"))) (d #t) (k 2)))) (h "1251b4xvmr5zl9na6cgbgy5bp51mzbva1ywf54m04s3h4460s2vs")))

