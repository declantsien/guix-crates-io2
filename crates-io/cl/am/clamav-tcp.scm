(define-module (crates-io cl am clamav-tcp) #:use-module (crates-io))

(define-public crate-clamav-tcp-0.1.0 (c (n "clamav-tcp") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0yhf242haxqw2g20sjaswigmgqi0rb4n1v20zd3h0ldwzmi3qypn")))

(define-public crate-clamav-tcp-0.1.1 (c (n "clamav-tcp") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "17xvc528bszxq945l3fyj1azwxwxia4n3asgz3nf7zv688jvswlw")))

(define-public crate-clamav-tcp-0.1.2 (c (n "clamav-tcp") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "13b6j98hs5qhysdsij7zighjqjlqdsgyipa3awg4mwjld087k3zl")))

(define-public crate-clamav-tcp-0.1.3 (c (n "clamav-tcp") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "16h7fp94zyiyf34ws8lj99x15xnnm8sfsi052cpadqjj3jcqwgsd")))

(define-public crate-clamav-tcp-0.2.0 (c (n "clamav-tcp") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "128088an6j30062m88wckijapdkail7ya5ajzm89lph02sikx7vv")))

(define-public crate-clamav-tcp-0.2.1 (c (n "clamav-tcp") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "18pm9j759z5slq2vh2sdwl3y6vj2x1gwsz3g13clkmv94lmg5v34")))

