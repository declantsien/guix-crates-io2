(define-module (crates-io cl am clamav) #:use-module (crates-io))

(define-public crate-clamav-0.1.0 (c (n "clamav") (v "0.1.0") (d (list (d (n "libc") (r "~0.2.42") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "075qi2wg4dsd8yydfn0ipqikjjyzl7274a5ayrzgp0viah4aaplz")))

(define-public crate-clamav-0.2.0 (c (n "clamav") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0kwy6sb112s6i5sz4sg6c26zq0vp8bymhlc2a8m5niqn56ickmi6")))

(define-public crate-clamav-0.3.0 (c (n "clamav") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1f4ic0n1jcwf691iyf1wbgm4hckmp2lfq925vv6sn80v7q02q78r")))

(define-public crate-clamav-0.4.0 (c (n "clamav") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0g5n2z4nlfm1lm6mf6q6d62mblaird4gqr7x9ajrvypkg3zxqxi4")))

