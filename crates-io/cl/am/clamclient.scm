(define-module (crates-io cl am clamclient) #:use-module (crates-io))

(define-public crate-clamclient-0.1.0 (c (n "clamclient") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1lkqfsn2nyx2nk6vjx5lrbjgadvlf7jfpzfayijfl8jj1j8r64a2")))

(define-public crate-clamclient-0.1.1 (c (n "clamclient") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1s3ij4g23q8lsi5kbb6iqcsypqrxqr0pnn6nk0wfi63zsfq932cx")))

