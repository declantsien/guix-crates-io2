(define-module (crates-io cl am clam-client) #:use-module (crates-io))

(define-public crate-clam-client-0.1.0 (c (n "clam-client") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "1066irdk8ynra0c896zpg07f4wfr8d0q09sp50snn6zvpg750jf8")))

(define-public crate-clam-client-0.1.1 (c (n "clam-client") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02pzddrvhx5cp7vzpsdkciq23iz5v0plrjj3sky6c4ilhc2x27z6") (f (quote (("serde-rs" "serde"))))))

