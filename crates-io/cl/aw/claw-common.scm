(define-module (crates-io cl aw claw-common) #:use-module (crates-io))

(define-public crate-claw-common-0.2.0 (c (n "claw-common") (v "0.2.0") (d (list (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0z2z12x26pgwdkw90s2w47968l4x2n9xh4477pcjhy87dni5amj2")))

(define-public crate-claw-common-0.2.1 (c (n "claw-common") (v "0.2.1") (d (list (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0qcvwqh6g92ks0dkhfq33ymgkfcsn21grgx6adha9vlix8zjkc81")))

(define-public crate-claw-common-0.2.2 (c (n "claw-common") (v "0.2.2") (d (list (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1xszkh77w047iqa2p5s0jp39j5knwdbh9f8vvmbyx9i4wv94ffh0")))

(define-public crate-claw-common-0.2.3 (c (n "claw-common") (v "0.2.3") (d (list (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0fvqx6hcknyi33a1cf71fc6c2bc8n8kf57y1zvqdmss744bmhqbs")))

(define-public crate-claw-common-0.2.4 (c (n "claw-common") (v "0.2.4") (d (list (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "08raray20bwpkv5j43ib8da8jpzvvq0smvfnrbqbmq4g2h8gwk61")))

(define-public crate-claw-common-0.2.5 (c (n "claw-common") (v "0.2.5") (d (list (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ay04wbnk6fhhcw2fbgs36847hml5385712cnzybilccc3zgmszc")))

(define-public crate-claw-common-0.2.6 (c (n "claw-common") (v "0.2.6") (d (list (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1vm492nz24n8gzwz0dr2vf9r5yywx887b44i0ry3qzgnc5y6k5j1")))

