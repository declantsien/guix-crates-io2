(define-module (crates-io cl aw claw) #:use-module (crates-io))

(define-public crate-claw-0.1.0-alpha1 (c (n "claw") (v "0.1.0-alpha1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0.2") (d #t) (k 0)))) (h "05npjaz9pclmzh16m7s9d1lc70qj57dw8znqkg5h5y1vb8mvmazd")))

(define-public crate-claw-0.1.0-alpha2 (c (n "claw") (v "0.1.0-alpha2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0.2") (d #t) (k 0)))) (h "030cvqqkl1kfdyw6p9n9mbnqkpj6hr0qdzy9fy85d1rgzwz7vdh0")))

