(define-module (crates-io cl aw claw-resolver) #:use-module (crates-io))

(define-public crate-claw-resolver-0.2.0 (c (n "claw-resolver") (v "0.2.0") (d (list (d (n "claw-ast") (r "^0.2.0") (d #t) (k 0)) (d (n "claw-common") (r "^0.2.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.104.0") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1lrahdl04jspbl72pzlv4scywri52ps2wj15fpjrkq7vnyk68xj3")))

(define-public crate-claw-resolver-0.2.1 (c (n "claw-resolver") (v "0.2.1") (d (list (d (n "claw-ast") (r "^0.2.1") (d #t) (k 0)) (d (n "claw-common") (r "^0.2.1") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.104.0") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13s5km9c4l5d6mjqk6x142j7639whsw0jcj46997x2d7iqyqxymn")))

(define-public crate-claw-resolver-0.2.2 (c (n "claw-resolver") (v "0.2.2") (d (list (d (n "claw-ast") (r "^0.2.1") (d #t) (k 0)) (d (n "claw-common") (r "^0.2.1") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.104.0") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wit-parser") (r "^0.200.0") (d #t) (k 0)))) (h "1vhg41d1n8lqfjfsa53mhb3rmr2pr731z4xizwhm5h191l8dbyf4")))

(define-public crate-claw-resolver-0.2.3 (c (n "claw-resolver") (v "0.2.3") (d (list (d (n "claw-ast") (r "^0.2.1") (d #t) (k 0)) (d (n "claw-common") (r "^0.2.1") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.105.3") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "1byk05d20fcpfkfighg7z469s4fngbbm7j7p4h4085zi74bclkyg")))

(define-public crate-claw-resolver-0.2.4 (c (n "claw-resolver") (v "0.2.4") (d (list (d (n "claw-ast") (r "^0.2.4") (d #t) (k 0)) (d (n "claw-common") (r "^0.2.4") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.105.3") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "01xbzkk1bw0hikajf3spc8g8vgjxbhx1zx9cd5vm1vrs6gg2f64l")))

(define-public crate-claw-resolver-0.2.5 (c (n "claw-resolver") (v "0.2.5") (d (list (d (n "claw-ast") (r "^0.2.5") (d #t) (k 0)) (d (n "claw-common") (r "^0.2.5") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.105.3") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "1nz6yn2cgvh0dfd1sjr11j3i7f5afyr2ay8d5iww58f6yqqxgw40")))

(define-public crate-claw-resolver-0.2.6 (c (n "claw-resolver") (v "0.2.6") (d (list (d (n "claw-ast") (r "^0.2.6") (d #t) (k 0)) (d (n "claw-common") (r "^0.2.6") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.105.3") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wit-parser") (r "^0.207") (d #t) (k 0)))) (h "152qr9r20zfaqpzi4qil8w58w3f0x634i3kjjpa3f3kx01crzbrx")))

