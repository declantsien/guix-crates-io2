(define-module (crates-io cl aw claw-ast) #:use-module (crates-io))

(define-public crate-claw-ast-0.2.0 (c (n "claw-ast") (v "0.2.0") (d (list (d (n "claw-common") (r "^0.2.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.104.0") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)))) (h "0q2q4afsbw6rcqqq49z7dg96al1l9mv5086dzdp79df5whq621b6")))

(define-public crate-claw-ast-0.2.1 (c (n "claw-ast") (v "0.2.1") (d (list (d (n "claw-common") (r "^0.2.1") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.104.0") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)))) (h "0i83m66nrap6fvw5prnnkkhd7q5z1xfxwldjw71cgil7lbv04305")))

(define-public crate-claw-ast-0.2.2 (c (n "claw-ast") (v "0.2.2") (d (list (d (n "claw-common") (r "^0.2.1") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.104.0") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "wit-parser") (r "^0.200.0") (d #t) (k 0)))) (h "1sfhls0r571hrqhnxgl6bg8ckxp3c7hz2143d98a5z539n5jsy01")))

(define-public crate-claw-ast-0.2.3 (c (n "claw-ast") (v "0.2.3") (d (list (d (n "claw-common") (r "^0.2.1") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.105.3") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "1n9r8x4hszvrkpw08r4bdkiqk5l23gkql0bpkd3irv4lwxgai735")))

(define-public crate-claw-ast-0.2.4 (c (n "claw-ast") (v "0.2.4") (d (list (d (n "claw-common") (r "^0.2.4") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.105.3") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "1965dzqc751x56m6f8iwpfwf0w69smwq0f6lr0bw9cpwlqzl89sj")))

(define-public crate-claw-ast-0.2.5 (c (n "claw-ast") (v "0.2.5") (d (list (d (n "claw-common") (r "^0.2.5") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.105.3") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "03qsxksay12ylby4b3bs54c6bkz1y7pc1gp715p7nif6dl38g7f4")))

(define-public crate-claw-ast-0.2.6 (c (n "claw-ast") (v "0.2.6") (d (list (d (n "claw-common") (r "^0.2.6") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.105.3") (d #t) (k 0)) (d (n "miette") (r "^7.1.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "wit-parser") (r "^0.207") (d #t) (k 0)))) (h "1422p8gvcw0s7s0jsyp0yip25pl8ziw8hz8xn6qp9yaf2r0wsbhc")))

