(define-module (crates-io cl aw claws) #:use-module (crates-io))

(define-public crate-claws-0.1.0 (c (n "claws") (v "0.1.0") (d (list (d (n "rusoto_core") (r "^0.42") (d #t) (k 0)) (d (n "rusoto_ec2") (r "^0.42") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.42") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1w6ny337vlpsvnbvfhijg4fn81c1s3ppww3gnpv53pbmfb3jph4k")))

(define-public crate-claws-0.2.0 (c (n "claws") (v "0.2.0") (d (list (d (n "rusoto_core") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_ec2") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_s3") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "00bkxifw5k7apdi8bpi238kwgqh6y80vxz3gyzlaa73aa58xgmgn")))

(define-public crate-claws-0.3.0 (c (n "claws") (v "0.3.0") (d (list (d (n "rusoto_core") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_ec2") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_s3") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1q56byggahy7j7hvm1vbis0gq0mdsdphqp24iy704zl1llyjiw31")))

(define-public crate-claws-0.4.0 (c (n "claws") (v "0.4.0") (d (list (d (n "rusoto_core") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_ec2") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_s3") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "057rl06iqsw8jzrm7j6wv3r73jzdggz8z48b4mf3sm1l8xkrpw75")))

(define-public crate-claws-0.5.0 (c (n "claws") (v "0.5.0") (d (list (d (n "rusoto_core") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_ec2") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_s3") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1gkk3czdv9gzjhfk00jnpzjjf3ny1nffgm1q5f7imjgn1gdff738")))

(define-public crate-claws-0.6.0 (c (n "claws") (v "0.6.0") (d (list (d (n "rusoto_core") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_ec2") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_s3") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1s2s3adjaay3zc3272f95wlz5a6l6lynglxpj1dghwly3qxj13g6")))

(define-public crate-claws-0.7.0 (c (n "claws") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_ec2") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_s3") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0pbggzac00qdv77vlv8vi0v6ly1c107928aawb2cisbvbkfsr694")))

(define-public crate-claws-0.8.0 (c (n "claws") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_ec2") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_logs") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_s3") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0bzvhfs5331x20g3adn2w0xyp5i5mp86amcc8glw76j9bvlgkcgq")))

(define-public crate-claws-1.0.0 (c (n "claws") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "fehler") (r "^1.0") (k 0)) (d (n "rusoto_core") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_ec2") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_logs") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_s3") (r "^0.42") (f (quote ("rustls"))) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "0a8qs4jzrg3mj34114sbs2346r9l020mcdb3ami3ypnlwii5szbs")))

(define-public crate-claws-1.0.1 (c (n "claws") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.45") (f (quote ("std"))) (k 0)) (d (n "fehler") (r "^1.0.0") (k 0)) (d (n "rusoto_core") (r "^0.42.0") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_ec2") (r "^0.42.0") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_logs") (r "^0.42.0") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_s3") (r "^0.42.0") (f (quote ("rustls"))) (k 0)) (d (n "structopt") (r "^0.3.25") (k 0)))) (h "1758q6hdv177xddvk4k2ikcy41n5p13f2ify23lmm91wfwlr1fmh")))

(define-public crate-claws-1.0.2 (c (n "claws") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.57") (f (quote ("std"))) (k 0)) (d (n "fehler") (r "^1.0.0") (k 0)) (d (n "rusoto_core") (r "^0.42.0") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_ec2") (r "^0.42.0") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_logs") (r "^0.42.0") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_s3") (r "^0.42.0") (f (quote ("rustls"))) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)))) (h "0fdwl2pf8qdzhhidnq3xbl71id57x94fc9xzn6sry4szgchglc8i")))

