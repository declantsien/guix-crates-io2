(define-module (crates-io cl ip clipper2-sys) #:use-module (crates-io))

(define-public crate-clipper2-sys-0.1.0 (c (n "clipper2-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w1vd6drb03d9mvkqx5gif0gjgmx2wb3qpm7gx8gfcihkvmg4nqg") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen"))))))

(define-public crate-clipper2-sys-0.2.0 (c (n "clipper2-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02i4c3w4llir07psimddzp5mwc3k6w0x9pbz0v97jqyf9zzgq0xh") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen"))))))

(define-public crate-clipper2-sys-0.3.0 (c (n "clipper2-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ma339j9x7s7w8f9qgagchgxdkh2hlciygrbi3xw68f93aky6ic9") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen"))))))

