(define-module (crates-io cl ip clipboard_wayland) #:use-module (crates-io))

(define-public crate-clipboard_wayland-0.1.0 (c (n "clipboard_wayland") (v "0.1.0") (d (list (d (n "smithay-clipboard") (r "^0.3.4") (d #t) (k 0)))) (h "08klq48aga6zbh9krwcxzgl6jwnn75r2wway2p23p8s3kr21pyl8")))

(define-public crate-clipboard_wayland-0.1.1 (c (n "clipboard_wayland") (v "0.1.1") (d (list (d (n "smithay-clipboard") (r "^0.5.1") (d #t) (k 0)))) (h "1yrp15hzd2hwdiag88vkr5yyd772a7334xcb7wbqiz50vhm8fvcj")))

(define-public crate-clipboard_wayland-0.1.2 (c (n "clipboard_wayland") (v "0.1.2") (d (list (d (n "smithay-clipboard") (r "^0.6") (d #t) (k 0)))) (h "1pka7q0fm0nsgzjfcikffgpna5mxx1ngcylvgf0dwzrqw36vig31")))

(define-public crate-clipboard_wayland-0.2.0 (c (n "clipboard_wayland") (v "0.2.0") (d (list (d (n "smithay-clipboard") (r "^0.6") (d #t) (k 0)))) (h "0ir7j7glad3p1mf5xb66jqj6ys3bgwfam650l70jlvx6yyln8qvg")))

(define-public crate-clipboard_wayland-0.2.1 (c (n "clipboard_wayland") (v "0.2.1") (d (list (d (n "smithay-clipboard") (r "^0.7") (d #t) (k 0)))) (h "1h1fdzm63rfhw859b467migk3nsz4lnc9awvq8yawivws0xicd41")))

(define-public crate-clipboard_wayland-0.2.2 (c (n "clipboard_wayland") (v "0.2.2") (d (list (d (n "smithay-clipboard") (r "^0.7") (d #t) (k 0)))) (h "1f1prf3qhww5qqbbqbf27ygq103z9r1b678cs4lpg672qimqhgq0")))

