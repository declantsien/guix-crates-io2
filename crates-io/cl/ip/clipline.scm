(define-module (crates-io cl ip clipline) #:use-module (crates-io))

(define-public crate-clipline-0.1.0 (c (n "clipline") (v "0.1.0") (h "0i6392ip4xw5yrsdawbi2vzcf3nshmcfxj7zhzbpb4g3c39wh796")))

(define-public crate-clipline-0.1.1 (c (n "clipline") (v "0.1.1") (h "0y5nkl98kiqrnd17xzv5yscv1fz3sckqs8an4ppvw4vlgd5mq648") (f (quote (("iter") ("func") ("default" "func" "iter"))))))

(define-public crate-clipline-0.1.2 (c (n "clipline") (v "0.1.2") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "line_drawing") (r "^1.0.0") (d #t) (k 2)))) (h "0qb5lz4ls34197kfg1jp7s3zxa3bhq9gphjh6v29kdsm5whk7h01") (f (quote (("iter") ("func") ("default" "func" "iter"))))))

(define-public crate-clipline-0.1.3 (c (n "clipline") (v "0.1.3") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "line_drawing") (r "^1.0.0") (d #t) (k 2)))) (h "1pslvxisb3hr1fywczx4nhqg8gvqzr4gja0cklz6ysjvbwpxmwal") (f (quote (("iter") ("func") ("default" "func" "iter")))) (y #t)))

(define-public crate-clipline-0.2.0 (c (n "clipline") (v "0.2.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "line_drawing") (r "^1.0.0") (d #t) (k 2)))) (h "0cfwnjprrfrqnji76wxii3yw1l0v39l4pxhlj0wdvmypz6rjm84a") (f (quote (("iter") ("func") ("default" "func" "iter"))))))

