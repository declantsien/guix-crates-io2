(define-module (crates-io cl ip clipboard_x11) #:use-module (crates-io))

(define-public crate-clipboard_x11-0.1.0 (c (n "clipboard_x11") (v "0.1.0") (d (list (d (n "xcb") (r "^0.9") (f (quote ("thread"))) (d #t) (k 0)))) (h "02i9hvhxafy6qai9wi4hvhfc86ka9mdfw33i7vbah9r3qihbsz0k")))

(define-public crate-clipboard_x11-0.2.0 (c (n "clipboard_x11") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11rb") (r "^0.8") (d #t) (k 0)))) (h "1w9zlraif2414h71xqrsgvs2pacamj629i1dsc1x6p0f4ajklh20")))

(define-public crate-clipboard_x11-0.3.0 (c (n "clipboard_x11") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11rb") (r "^0.8") (d #t) (k 0)))) (h "1zsr0vwhkxkrr669x2gi98mjssra5qll24pgv3ah0r27693vlx24")))

(define-public crate-clipboard_x11-0.3.1 (c (n "clipboard_x11") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11rb") (r "^0.8") (d #t) (k 0)))) (h "0yhbpz04hp7z5pm41m0gwcjbvj75fyfszz3vcgjqfgc8y5ihs934")))

(define-public crate-clipboard_x11-0.4.0 (c (n "clipboard_x11") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11rb") (r "^0.9") (d #t) (k 0)))) (h "08g94h0n0jmjvhmphzhpf8ax5ibfnl67mln6wbfh9kbfhc870flq")))

(define-public crate-clipboard_x11-0.4.1 (c (n "clipboard_x11") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11rb") (r "^0.13") (d #t) (k 0)))) (h "0v7mmqdzvkiz4x1jchjfqsiywzm88sdn709rdm6fdzilcr1mpx2w")))

(define-public crate-clipboard_x11-0.4.2 (c (n "clipboard_x11") (v "0.4.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11rb") (r "^0.13") (d #t) (k 0)))) (h "0703a1naak6iwcy98v2pl04f9521w4ik8qx20jghygh1bs0ylx22")))

