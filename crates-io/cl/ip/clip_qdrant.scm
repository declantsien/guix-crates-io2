(define-module (crates-io cl ip clip_qdrant) #:use-module (crates-io))

(define-public crate-clip_qdrant-0.1.0 (c (n "clip_qdrant") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)))) (h "14wv6b187rivb3k6mfj1innwqvc3bzl1dbp9zcv72v5ff49l4qjl")))

(define-public crate-clip_qdrant-0.1.1 (c (n "clip_qdrant") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)))) (h "0yh8cg41nv4jk0kwvqmd0v14pijlrbnfq4klxnhdi3myd0k7h725")))

(define-public crate-clip_qdrant-0.1.2 (c (n "clip_qdrant") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-lazy") (r "^0.1.0") (f (quote ("parking_lot" "nightly"))) (d #t) (k 0)) (d (n "ctor") (r "^0.2.4") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "1y8srkiavnkwka2sqzlm90jkvyp7ws2id8y7skbqaxkdgzhylgnh")))

