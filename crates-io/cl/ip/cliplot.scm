(define-module (crates-io cl ip cliplot) #:use-module (crates-io))

(define-public crate-cliplot-0.1.0 (c (n "cliplot") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "iced") (r "^0.5") (f (quote ("canvas" "tokio" "image_rs"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "plotters-iced") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("io-std" "io-util"))) (d #t) (k 0)))) (h "0bixh9qsaybq9wcddq9zs9v43qsp6wr8r0qaskvf1dc1br14x7qq")))

