(define-module (crates-io cl ip cliply) #:use-module (crates-io))

(define-public crate-cliply-0.1.0 (c (n "cliply") (v "0.1.0") (d (list (d (n "coutils") (r "^1.4.0") (d #t) (k 0)))) (h "0cjpr7xy33b5hsn8ic1ijpbvzal45i2sfrfa1lspb1gz3nwrvgbk")))

(define-public crate-cliply-0.2.0 (c (n "cliply") (v "0.2.0") (d (list (d (n "coutils") (r "^1.5.0") (d #t) (k 0)))) (h "0f5n6wmz022p6bhxfh1kwpr0wfzd6p24r3d2j2whc5b5dbzd09j0")))

