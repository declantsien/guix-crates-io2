(define-module (crates-io cl ip clippy-sarif) #:use-module (crates-io))

(define-public crate-clippy-sarif-0.1.0 (c (n "clippy-sarif") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "16l1mk9klxcnvf005v0an105f7dh3hx596szwhg3yk2lb2pshh7i")))

(define-public crate-clippy-sarif-0.1.1 (c (n "clippy-sarif") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ynsajx1zv0d22nms9a8ky4rrlwbqagfx1pjnpgfaddjiyf8wfd4")))

(define-public crate-clippy-sarif-0.1.2 (c (n "clippy-sarif") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0jj79gf141vh6k3ia6fh2584hgngy7mzq8kngl23whnbr1zvrxq2")))

(define-public crate-clippy-sarif-0.2.4 (c (n "clippy-sarif") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.4") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1qpwfiwa2fpd67wq9lyggjqhpc2pdcy9j8lw3khhbagd2pmyvm8l")))

(define-public crate-clippy-sarif-0.2.5 (c (n "clippy-sarif") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.5") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0yil3nwwn018wdwvdqp495fszcfj96hl76x9v5s2q501br3gr9ip")))

(define-public crate-clippy-sarif-0.2.6 (c (n "clippy-sarif") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.6") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0wqnzb539y8xa5i5c6mbc98kn5ldb8hbhcb3g5wa48ylvx9174bd")))

(define-public crate-clippy-sarif-0.2.7 (c (n "clippy-sarif") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.7") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0233rs6gy5xl30ap6bj96zx784lp8c8bln0izy9f0hs8cfy5hgxk")))

(define-public crate-clippy-sarif-0.2.8 (c (n "clippy-sarif") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.8") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0mif1mbyjlahhipz6f76c8pk8y9vk4fxvb4k5wvnsajhaycfkzag")))

(define-public crate-clippy-sarif-0.2.9 (c (n "clippy-sarif") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.9") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1w6mjz0jz8vic485m0qyf8vryjir5wzrbmyb7180ma7qr223kcpz")))

(define-public crate-clippy-sarif-0.2.10 (c (n "clippy-sarif") (v "0.2.10") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.10") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1awvwndrcqavs2d50xxa6ifh6zcfcnzyhkw42z7j2d4bx3xpcg5h")))

(define-public crate-clippy-sarif-0.2.11 (c (n "clippy-sarif") (v "0.2.11") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.11") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "144c7k3v7dw76zax38pkmvzx330789hznvll07a6jxv18v6l8m8b")))

(define-public crate-clippy-sarif-0.2.12 (c (n "clippy-sarif") (v "0.2.12") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.12") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1skyvg871v2xa25929qfff80bmf4zj54amky69npl905yps16a08")))

(define-public crate-clippy-sarif-0.2.14 (c (n "clippy-sarif") (v "0.2.14") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.14") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1drqvbhlz3f2aacin7y077np84ipkc4xka59mlisxfzawnads9ys")))

(define-public crate-clippy-sarif-0.2.15 (c (n "clippy-sarif") (v "0.2.15") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.15") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1rr3304s247w2r2jxv9jmj73kskis61v4krf8cpjiy9lyfdafdiv")))

(define-public crate-clippy-sarif-0.2.17 (c (n "clippy-sarif") (v "0.2.17") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.17") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1jk8jj41w68jd39drlnhqxkf7i07zfrz4zi3a2hai453bg6br5sh")))

(define-public crate-clippy-sarif-0.2.19 (c (n "clippy-sarif") (v "0.2.19") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.19") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ga80ddnmkzwf93wlkzrjmlw6gv46h5l6wvay20gg4b6ssbswcay")))

(define-public crate-clippy-sarif-0.2.20 (c (n "clippy-sarif") (v "0.2.20") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.20") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "107f191x3rj2zrjzj56nn7ih6jz1jjrcn3jiijgg2l1lzdrdh1wc")))

(define-public crate-clippy-sarif-0.2.21 (c (n "clippy-sarif") (v "0.2.21") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.7") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.21") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ryhy9729n87c1a9w6mllvd2dbin29n42xmlyvf7razabvx5yfnf")))

(define-public crate-clippy-sarif-0.2.22 (c (n "clippy-sarif") (v "0.2.22") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.22") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0m6dykffh2qi2rzimiv2jj322w9cswh6lzax87vzz4wg1lffngih")))

(define-public crate-clippy-sarif-0.2.24 (c (n "clippy-sarif") (v "0.2.24") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.24") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "17avj5y8w0xzfywbynqk1qnq1pzynbdwmq3zd9zcpm3kixxjz49m")))

(define-public crate-clippy-sarif-0.2.25 (c (n "clippy-sarif") (v "0.2.25") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.25") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0qqk54rhf4l77m34flv8cwz0q5dzpl5i7dswcdlli5cjr2jwq8hh")))

(define-public crate-clippy-sarif-0.3.0 (c (n "clippy-sarif") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.10") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.0") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "12ksh7bd3j0gg3b920kdmbvn4c6hry33db83lmxj5rq6ad4dd361")))

(define-public crate-clippy-sarif-0.3.1 (c (n "clippy-sarif") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.1") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "10gcajj18f0ph4s49rsxrb6rcwl16bn3ssacm7zswxy9dv8kbhxw")))

(define-public crate-clippy-sarif-0.3.2 (c (n "clippy-sarif") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.2") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0bxip5r9ybn0j04pkka6ald8i592yxf0i928ra658nmgdfsz4pnh")))

(define-public crate-clippy-sarif-0.3.3 (c (n "clippy-sarif") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.3") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1q9s1lcvpxyp89jyqwi9618as8qyffndkhqj7ifl93zkmm1j4wdz")))

(define-public crate-clippy-sarif-0.3.4 (c (n "clippy-sarif") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.4") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0wnmqhprc0593np0z4mnjz1hd24970rh0cgym7z1mgj5gq6wv5m9")))

(define-public crate-clippy-sarif-0.3.5 (c (n "clippy-sarif") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.5") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ssrhacsjl6yy1fdjgr5626w7psz3pkyqwqqzdv7yazgwr267gm7")))

(define-public crate-clippy-sarif-0.3.6 (c (n "clippy-sarif") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.6") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0rs0cmplgaq5fi4app41wib73ny8k399z3bp94p6pmb32p17b584")))

(define-public crate-clippy-sarif-0.3.7 (c (n "clippy-sarif") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.7") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1if911pdcwbnfnivijcv6f8gwipyyxmaisqlmn45l48nzwq4p37v")))

(define-public crate-clippy-sarif-0.4.0 (c (n "clippy-sarif") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.0") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0bah4qgbj1c3hjda4inpkah0w23by1pzcklxgszf3j9nljy98dz2")))

(define-public crate-clippy-sarif-0.4.1 (c (n "clippy-sarif") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.1") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0fvnw9lgymvpdqab5dzmhbkf6xjzmf5xjsjig8s8la5cqvl5kp15")))

(define-public crate-clippy-sarif-0.4.2 (c (n "clippy-sarif") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.2") (f (quote ("clippy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ihfl28p44ylmszkjcv6ra63j24h1k4pwkycz5cjj2a7m7fh8fmk")))

