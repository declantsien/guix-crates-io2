(define-module (crates-io cl ip clipars) #:use-module (crates-io))

(define-public crate-clipars-0.1.0 (c (n "clipars") (v "0.1.0") (h "1ps197y3xb4gxbb9rwqf7f8b77csw9gfkywciyx1pws72a2pxiy3") (y #t)))

(define-public crate-clipars-0.2.0 (c (n "clipars") (v "0.2.0") (h "11gxb04w350ysrnw27ch8vlimj8kxvyf2r6mabgild2dxgdq530q") (y #t)))

(define-public crate-clipars-0.2.1 (c (n "clipars") (v "0.2.1") (h "0mgzwcbqv58wslpsaqbms6vwn4b2gi6fvihn7nxryhd2rnwwpqkp") (y #t)))

(define-public crate-clipars-0.2.2 (c (n "clipars") (v "0.2.2") (h "1115wrqbzwj3xgrvz91hgb30070w87k77alw5qwf0zqg7nsg6kg5") (y #t)))

(define-public crate-clipars-1.0.0 (c (n "clipars") (v "1.0.0") (h "0x1v0hrssca3sfh2dfkrxwaka67kvwi7kgabq1bb4nqz7g2a5aw7") (y #t)))

(define-public crate-clipars-1.0.1 (c (n "clipars") (v "1.0.1") (h "0mfmrbv60292m9nlpirl2mm4rl3gykj289pml6x6wnmcq7698imd") (y #t)))

(define-public crate-clipars-1.1.0 (c (n "clipars") (v "1.1.0") (h "1marh5z3bhyj8qn5lw1pxbhrdd53cibpbl8w66ghc5xnlxkjzr1d") (y #t)))

(define-public crate-clipars-1.1.2 (c (n "clipars") (v "1.1.2") (h "1m4y683275qnr7b34j2maq5hkjc9g3axpx56inx0lp0cbqmi2rj7") (y #t)))

(define-public crate-clipars-1.1.3 (c (n "clipars") (v "1.1.3") (h "16vk6rkvqhj1zx8r1y2dzm91n1jn385r140m7k21fy8fsk0pgk99") (y #t)))

(define-public crate-clipars-1.1.4 (c (n "clipars") (v "1.1.4") (h "1358qviy3acgixfl723csbq2v24qkxykfzm25srlxnqrgsdp1sx1") (y #t)))

(define-public crate-clipars-1.2.0 (c (n "clipars") (v "1.2.0") (h "08dzskppj58ckngllahs0vj98s8wpm1h84bayjl25bwsfr8dn08r") (y #t)))

(define-public crate-clipars-1.2.1 (c (n "clipars") (v "1.2.1") (h "01lla2c1vk7rc910lrr150ajl8zqjlk2b65q2qxbyxby89ci4hi6") (y #t)))

(define-public crate-clipars-1.2.2 (c (n "clipars") (v "1.2.2") (h "13646fy85a0n3rbjcvfc3lv1yikj6pshfpg2jnq1xy0xf60awsxr") (y #t)))

(define-public crate-clipars-1.2.3 (c (n "clipars") (v "1.2.3") (h "1yg80kyq48fqpjfhixwylllk8h6jf6167gz29faz3r143pqxkhpg") (y #t)))

