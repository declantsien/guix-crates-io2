(define-module (crates-io cl ip clippy-mini-macro-test) #:use-module (crates-io))

(define-public crate-clippy-mini-macro-test-0.1.0 (c (n "clippy-mini-macro-test") (v "0.1.0") (h "1f1q09az4xk4aqnp8g1m6kf4hif75gr7rwzhh4yw7g1gqk94jsgq")))

(define-public crate-clippy-mini-macro-test-0.2.0 (c (n "clippy-mini-macro-test") (v "0.2.0") (h "01175ynzasmyymx7w4rgh0dzcp9mqac9y4fgz9xa9xb56cgjz9x2")))

