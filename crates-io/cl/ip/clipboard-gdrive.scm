(define-module (crates-io cl ip clipboard-gdrive) #:use-module (crates-io))

(define-public crate-clipboard-gdrive-0.1.0 (c (n "clipboard-gdrive") (v "0.1.0") (d (list (d (n "arboard") (r "^3.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)))) (h "04mnqgyqngdm69wwakjr2sxg7dzxbm7kr7iv76an0m775flc8zbh")))

