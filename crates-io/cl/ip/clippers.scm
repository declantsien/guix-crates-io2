(define-module (crates-io cl ip clippers) #:use-module (crates-io))

(define-public crate-clippers-0.1.0 (c (n "clippers") (v "0.1.0") (d (list (d (n "build_cfg") (r "^1") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "image") (r "^0.24") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "122c9r09hqisb9hpx5rcwkznsnfzlsijwkmpn1na3vgjyxigcijw") (y #t)))

(define-public crate-clippers-0.1.1 (c (n "clippers") (v "0.1.1") (d (list (d (n "build_cfg") (r "^1") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "image") (r "^0.24") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09af2amnhapkdw7xfifklh4a0l7wrqmmzws249jyr5irj5gm27y2") (y #t)))

(define-public crate-clippers-0.1.2 (c (n "clippers") (v "0.1.2") (d (list (d (n "build_cfg") (r "^1") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "image") (r "^0.24") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10wyshrlhf6krlbzqchv0c14qciwrvdrsidhyx3sxck2rvghkwk9")))

