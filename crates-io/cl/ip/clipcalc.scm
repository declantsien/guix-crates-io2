(define-module (crates-io cl ip clipcalc) #:use-module (crates-io))

(define-public crate-clipcalc-1.0.0 (c (n "clipcalc") (v "1.0.0") (d (list (d (n "clipboard-win") (r "^5.0.0") (d #t) (k 0)) (d (n "evalexpr") (r "^11.3.0") (d #t) (k 0)))) (h "05aqs8mjq065jk4dkylp4qz8i02705siw44bb5z4hb55c92xyv96")))

