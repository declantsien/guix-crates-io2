(define-module (crates-io cl ip clipsaver) #:use-module (crates-io))

(define-public crate-clipsaver-0.1.0 (c (n "clipsaver") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 0)))) (h "04549ajacnxzs8w1m4d4q3ncg2d13jgqcffh4728mz8dcdy2mlv0")))

