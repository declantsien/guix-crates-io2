(define-module (crates-io cl ip clippy_utils) #:use-module (crates-io))

(define-public crate-clippy_utils-0.1.73 (c (n "clippy_utils") (v "0.1.73") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rustc-semver") (r "^1.1") (d #t) (k 0)))) (h "1v718zapbgr3pvr9kz56cx88cyx7mr59zsq1ln2vl3h0a02qn0xc") (f (quote (("internal") ("deny-warnings")))) (y #t)))

(define-public crate-clippy_utils-0.0.1 (c (n "clippy_utils") (v "0.0.1") (d (list (d (n "term") (r "^0.7") (d #t) (k 1)))) (h "1sh25b40pcrwxj806wbl1l2n6f6m7wd9g6zgsnhj7ghs9lpl7kbq")))

