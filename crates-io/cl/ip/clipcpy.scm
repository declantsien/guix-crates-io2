(define-module (crates-io cl ip clipcpy) #:use-module (crates-io))

(define-public crate-clipcpy-0.1.0 (c (n "clipcpy") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "0sa0wy0cbp8w2kc4aaldqjyfwarwgdh7f1qq92k79jz834v4d1ba") (y #t) (r "1.60")))

(define-public crate-clipcpy-0.2.0 (c (n "clipcpy") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "copypasta-ext") (r "^0.3.7") (d #t) (k 0)))) (h "0b6ny664g8h3jpa6i193z0gjvczhvh8x3rg2cg5hyqw35zgk360r") (y #t) (r "1.60")))

