(define-module (crates-io cl ip clippet) #:use-module (crates-io))

(define-public crate-clippet-0.1.0 (c (n "clippet") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "12byrp3iks5rr6hyf0vnl19wym4wsmh93hcircwmbxfa30yzg0vn")))

(define-public crate-clippet-0.1.1 (c (n "clippet") (v "0.1.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "1kli8qyifs3w0qggg5srj5lk2qlcnyfy1sa5fdscqrnavgdxkidq")))

(define-public crate-clippet-0.1.2 (c (n "clippet") (v "0.1.2") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "1pjqx5pdgs5hbkmjpxfwf5dj37rnwk7sfbi6r1r0ndgbl6ckwjpk")))

(define-public crate-clippet-0.1.3 (c (n "clippet") (v "0.1.3") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "1mmiw6alfk0zd8z16iljxj08ygf70i0jzdzngv4s6j37993f3vln")))

(define-public crate-clippet-0.1.4 (c (n "clippet") (v "0.1.4") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "1hlsxszpsxsg3vimpgh0gx9fzwgjy6k59s5nhjvl7h2izbrv3py2")))

(define-public crate-clippet-0.1.5 (c (n "clippet") (v "0.1.5") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "11l1x169jgg2816viqfhdhch0z1cwbdwvm23n2mg94hzhmi7jzax")))

(define-public crate-clippet-0.1.6 (c (n "clippet") (v "0.1.6") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "1fqj8vllr3nmaq4c0riqafv242p8sfa3vrpfhs70dwb62vy69g2j")))

(define-public crate-clippet-0.1.7 (c (n "clippet") (v "0.1.7") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "1n1n2mqk3mqdmmsmqsc2yg6z8vnjqii0nm2xrvcji200sqimi92m")))

(define-public crate-clippet-0.1.8 (c (n "clippet") (v "0.1.8") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "03pzn23dvib91zxcm71blvydss4wjc248bariv7h9xbqssp0p66f")))

