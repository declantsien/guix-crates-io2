(define-module (crates-io cl ip cliproc) #:use-module (crates-io))

(define-public crate-cliproc-0.1.0 (c (n "cliproc") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0ibh0kvb7336836dm1hk7pydcqikyhvzngw7p221zfgmb07l061k")))

(define-public crate-cliproc-1.0.0 (c (n "cliproc") (v "1.0.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "17kva88wvjzk7kwb2pk99wk20add20lfciaxsc4827ddw2f4knhl")))

