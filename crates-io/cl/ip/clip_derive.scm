(define-module (crates-io cl ip clip_derive) #:use-module (crates-io))

(define-public crate-clip_derive-0.1.0 (c (n "clip_derive") (v "0.1.0") (d (list (d (n "clip_core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "17k7jzqqs4gw3rrfgb0zyycp9zaakk83sn7h6n8k8fslj4pf2jvi")))

