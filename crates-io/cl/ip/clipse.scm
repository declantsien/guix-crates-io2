(define-module (crates-io cl ip clipse) #:use-module (crates-io))

(define-public crate-clipse-0.1.0 (c (n "clipse") (v "0.1.0") (h "1wa83wg1x8vns4dmp2lbq1461prld6qhg5dfkfvq4b31rgak146l")))

(define-public crate-clipse-1.0.0 (c (n "clipse") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "04glj7pqlgsb0j6hp82iqk73yimcy5jlpc8ihdrpb60bmj1wg8bd")))

(define-public crate-clipse-1.1.0 (c (n "clipse") (v "1.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "14pg60wmqsvhy1yd0b3bdivy2y5d74wdj6cga9mvwxr0v2rcw677") (f (quote (("xclip"))))))

(define-public crate-clipse-1.1.1 (c (n "clipse") (v "1.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zg6kz4glisf0r3ri2sl6dn6l47k21yjxgkwaj9k8qm8mqc0vv9v") (f (quote (("xclip"))))))

(define-public crate-clipse-1.1.2 (c (n "clipse") (v "1.1.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ayvip2508k4m2zab3yngk5ll45z723a0kgrx5g1mlrnd76vb1w9") (f (quote (("xclip"))))))

(define-public crate-clipse-1.2.2 (c (n "clipse") (v "1.2.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "081ksx52qs66aw0sp5cih0lrv8qykyp4yvlj3wm1wq55gz8rj78y") (f (quote (("xclip"))))))

