(define-module (crates-io cl ip clip_txt) #:use-module (crates-io))

(define-public crate-clip_txt-0.1.0 (c (n "clip_txt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "tokenizers") (r "^0.13.3") (d #t) (k 0)))) (h "0rdkhsb0z2pbzlgy5cwaqg8k31hls3qjc0vna2w4h58rfv97ydy6")))

