(define-module (crates-io cl ip clipv) #:use-module (crates-io))

(define-public crate-clipv-0.1.0 (c (n "clipv") (v "0.1.0") (d (list (d (n "clip_core") (r "^0.1.0") (d #t) (k 0)) (d (n "clip_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "038jics1z5h53sir8lrgy98xcxckj9rhg7ix6s4mvf1v4874p845") (s 2) (e (quote (("derive" "dep:clip_derive"))))))

