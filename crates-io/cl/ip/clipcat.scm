(define-module (crates-io cl ip clipcat) #:use-module (crates-io))

(define-public crate-clipcat-0.1.0 (c (n "clipcat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "copypasta") (r "^0.10.1") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.5.9") (d #t) (k 0)))) (h "1dsn1wpzijw9h14ca1fnzbk8g1gpifnk8q4i574k3i93k5wclif6")))

(define-public crate-clipcat-0.1.1 (c (n "clipcat") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "copypasta") (r "^0.10.1") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.5.9") (d #t) (k 0)))) (h "1xplj9g672z5msa5dhihaflr5xqp9dyv9846spga83ygqnglsl1p")))

