(define-module (crates-io cl ip clipp) #:use-module (crates-io))

(define-public crate-clipp-0.1.0 (c (n "clipp") (v "0.1.0") (d (list (d (n "clipboard-win") (r "^4.5.0") (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "1x44s7y7d9s4352hw7cs4x4l40ikr2hka5hhd15v5g6zbb0f2qww")))

