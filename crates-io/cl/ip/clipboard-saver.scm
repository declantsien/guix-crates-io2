(define-module (crates-io cl ip clipboard-saver) #:use-module (crates-io))

(define-public crate-clipboard-saver-0.1.0 (c (n "clipboard-saver") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "cocoa") (r "^0.24.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1qr706rqkldpiigb3gxjg20id6h8f2j0hwqz7q0dr52qrf2rdps4")))

