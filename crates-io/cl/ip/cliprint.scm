(define-module (crates-io cl ip cliprint) #:use-module (crates-io))

(define-public crate-cliprint-0.1.0 (c (n "cliprint") (v "0.1.0") (d (list (d (n "nu-ansi-term") (r "^0.48.0") (o #t) (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.48.0") (d #t) (k 2)) (d (n "strfmt") (r "^0.2.4") (d #t) (k 0)))) (h "02hb10cfip609r83wzb6r6071r9a7z1q9a5jnlnnr95ncynzi95d") (f (quote (("color" "nu-ansi-term"))))))

