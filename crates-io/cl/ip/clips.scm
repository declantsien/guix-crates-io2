(define-module (crates-io cl ip clips) #:use-module (crates-io))

(define-public crate-clips-0.1.0 (c (n "clips") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "clips-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "0gpsrffcvmv9z4p2iwssypg1mr1r9bqlmgc6zidssaj1cz8i52yb")))

(define-public crate-clips-0.2.0 (c (n "clips") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "clips-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "0f9z21b9pf9jyrmjwxy8jddf9pm0m2ba1jzq80cyjrxvzwczqg92")))

(define-public crate-clips-0.3.0 (c (n "clips") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "clips-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "1p1s756zfw17h9y6gwnzhjz11d2fgiyqivppnasq6yb3732xpl44")))

(define-public crate-clips-0.4.0 (c (n "clips") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clips-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)))) (h "0cg503pp3laj3m0wk85bqkkn1vwvy77l5kx207lfq80r4ak90hx8")))

