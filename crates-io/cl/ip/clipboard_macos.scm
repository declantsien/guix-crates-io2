(define-module (crates-io cl ip clipboard_macos) #:use-module (crates-io))

(define-public crate-clipboard_macos-0.1.0 (c (n "clipboard_macos") (v "0.1.0") (d (list (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1") (d #t) (k 0)))) (h "15cab2bi6jr7y5bqymsvxaf3hpa0arj1cbg3lp03nic9kfg7ynhl")))

