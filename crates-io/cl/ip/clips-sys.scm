(define-module (crates-io cl ip clips-sys) #:use-module (crates-io))

(define-public crate-clips-sys-0.1.0 (c (n "clips-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)))) (h "0prwnww5d8nz6crxkx9zbj40hlyn73sscik3l9qfvmhcxzi07m9n") (l "clips")))

(define-public crate-clips-sys-0.2.0 (c (n "clips-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "1glg9cz8xrg9jxqpbr305dzxa6m176hf7qzq9hy31d293390r986") (f (quote (("developer")))) (l "clips")))

(define-public crate-clips-sys-0.3.0 (c (n "clips-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "11dgn9rvikgh5nzvr6l1frx31ykwy4g999ymxr6j5232d0qchxgz") (f (quote (("developer")))) (l "clips")))

(define-public crate-clips-sys-0.4.0 (c (n "clips-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "0gwh5x2yw8rbmn27prfpdgnhv0l4zcd95h2fr7cs6aj387w15l53") (f (quote (("developer")))) (l "clips")))

