(define-module (crates-io cl ip clipper2) #:use-module (crates-io))

(define-public crate-clipper2-0.1.0 (c (n "clipper2") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glam") (r "^0.25") (o #t) (d #t) (k 0)))) (h "1mq07aasdkfw5cbmi49nd66ljpjnrlryrvl1j6r3my8rssr3ync0") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen") ("default")))) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-clipper2-0.1.1 (c (n "clipper2") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glam") (r "^0.25") (o #t) (d #t) (k 0)))) (h "0cbic11p4bza6j1hkzhrvz2ihflfs82yiwg41wbczajs9nr30z5b") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen") ("default")))) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-clipper2-0.1.2 (c (n "clipper2") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glam") (r "^0.25") (o #t) (d #t) (k 0)))) (h "0mw16vyp94afidskvsdzzbqqvimx8z911dzcgjkhdx907s57wl1f") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen") ("default")))) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-clipper2-0.2.0 (c (n "clipper2") (v "0.2.0") (d (list (d (n "clipper2c-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "macroquad") (r "^0.4.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0mv9v1fgqlp8xjrgbw2gk5smbcwfsvz90lqppl1gwj2imq4lmvrf") (f (quote (("doc-images") ("default" "doc-images"))))))

(define-public crate-clipper2-0.2.1 (c (n "clipper2") (v "0.2.1") (d (list (d (n "clipper2c-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "macroquad") (r "^0.4.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0dffwy6jqlarmnq18sbqh2686dind6s4wr8diq6g4069ai0w0ksk") (f (quote (("doc-images") ("default" "doc-images"))))))

(define-public crate-clipper2-0.2.2 (c (n "clipper2") (v "0.2.2") (d (list (d (n "clipper2c-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "macroquad") (r "^0.4.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "064k3b66sagxf8jyfnh3i0xfybmh6zvpy2r3aqhh8aidmrc9l4n7") (f (quote (("doc-images") ("default" "doc-images"))))))

(define-public crate-clipper2-0.2.3 (c (n "clipper2") (v "0.2.3") (d (list (d (n "clipper2c-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "macroquad") (r "^0.4.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "13gqjrp60l7ixrd7ik99v903cjw4kb068zvjvy3smg74cqpry634") (f (quote (("doc-images") ("default" "doc-images"))))))

