(define-module (crates-io cl ip cliparser) #:use-module (crates-io))

(define-public crate-cliparser-0.1.0 (c (n "cliparser") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0fzbb1fnm1qp7l65n960ndmam6r1igwvkjljmc4q9qf97vny23ii")))

(define-public crate-cliparser-0.1.1 (c (n "cliparser") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0nxirscxr3zrlwpg3nrh31y6jr1fc4xa085bkbcrqpm7q2lkn38q")))

(define-public crate-cliparser-0.1.2 (c (n "cliparser") (v "0.1.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "18nmzay5ch8dz4701p6irrd4841hms678lc95fmn0qm4f5xp1gbl")))

