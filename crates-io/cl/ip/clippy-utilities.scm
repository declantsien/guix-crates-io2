(define-module (crates-io cl ip clippy-utilities) #:use-module (crates-io))

(define-public crate-clippy-utilities-0.1.0 (c (n "clippy-utilities") (v "0.1.0") (h "1nmh74b1avala0xp7r5wlp13s7j0ck76sc0y9jr1xqvychsjanyw")))

(define-public crate-clippy-utilities-0.2.0 (c (n "clippy-utilities") (v "0.2.0") (d (list (d (n "numeric_cast") (r "^0.1.2") (d #t) (k 0)))) (h "0q479xr9wbx5xi5s4f6phjns5081mwykfzihmcqj8k29kg4hp533")))

