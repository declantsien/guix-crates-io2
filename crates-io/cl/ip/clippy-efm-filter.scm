(define-module (crates-io cl ip clippy-efm-filter) #:use-module (crates-io))

(define-public crate-clippy-efm-filter-0.1.0 (c (n "clippy-efm-filter") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-rc.9") (f (quote ("derive"))) (d #t) (k 0)))) (h "03db88r0mhl02x4x2f5k1bb41h4rhw6l1ra9hb0lv5mgsfnndyj7")))

