(define-module (crates-io cl ip clipass) #:use-module (crates-io))

(define-public crate-clipass-0.1.0 (c (n "clipass") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (d #t) (k 0)) (d (n "hashy") (r "^0.1.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)) (d (n "touch") (r "^0.0.1") (d #t) (k 0)))) (h "1zma1gqvirrzq1c4va3l9gs1p5glajh0dips8i1p9vji8f2nfhjr")))

