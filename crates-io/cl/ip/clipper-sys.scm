(define-module (crates-io cl ip clipper-sys) #:use-module (crates-io))

(define-public crate-clipper-sys-0.1.0 (c (n "clipper-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.36.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.41") (d #t) (k 1)))) (h "03cpdccr7h62iqvxbbgqf1x0x4djkhx8w754nzadrvzq32kggw70")))

(define-public crate-clipper-sys-0.2.0 (c (n "clipper-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "06znaj3rlf77z2qpl36644fxjxqcn5xx8xdscj06y09d2pvzyhz2")))

(define-public crate-clipper-sys-0.3.0 (c (n "clipper-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0plh7z3hpw5g2kwbz6ilai95mkmiabl4nfp04cf9ma0kw8gn9y44")))

(define-public crate-clipper-sys-0.3.1 (c (n "clipper-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (d #t) (k 1)))) (h "1sl19r92g4ms21dgj77r8rdz3a8s3az9w525w3vv9y82qjz0zz4p")))

(define-public crate-clipper-sys-0.3.2 (c (n "clipper-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (d #t) (k 1)))) (h "05jiv2y17j1b9xq8rlx6j19mg50b5h8h540g04nh9d17p2k29cwm")))

(define-public crate-clipper-sys-0.4.0 (c (n "clipper-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (d #t) (k 1)))) (h "1hbg1x7bwlzpf887nxnvv3skvsml2kfr2dqaa37aqszljlif6as5")))

(define-public crate-clipper-sys-0.5.0 (c (n "clipper-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.58.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (d #t) (k 1)))) (h "1idqgpr9fcajwi6k8pqdhg1znj0272yx5swzdznql7yl16926n40") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen"))))))

(define-public crate-clipper-sys-0.6.0 (c (n "clipper-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.59.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (d #t) (k 1)))) (h "0vk81lv9bykaa0ms78h215b4sdf3iwjrpk4m4q1nqid0w7hfaa2r") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen"))))))

(define-public crate-clipper-sys-0.7.0 (c (n "clipper-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.59.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (d #t) (k 1)))) (h "13a37d1xn465fyidfvfgqzikiw549ibw7zdqmzjh2zp9w1fxz291") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen"))))))

(define-public crate-clipper-sys-0.7.1 (c (n "clipper-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.59.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (d #t) (k 1)))) (h "1przp4xk79fzir1ypmxqkark1dcq93f1284n4hm877j11dlnabrl") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen"))))))

(define-public crate-clipper-sys-0.7.2 (c (n "clipper-sys") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.59.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (d #t) (k 1)))) (h "1rfj6dany2914128bsrz2q0kmzbldbkbw0d6yw53jz6la2swqsyw") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen"))))))

