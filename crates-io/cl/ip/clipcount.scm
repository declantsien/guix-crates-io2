(define-module (crates-io cl ip clipcount) #:use-module (crates-io))

(define-public crate-clipcount-0.1.0 (c (n "clipcount") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "1mlslxkhk00czcmnkpgj6jxnqr91p5dm30pnmynqaxixkd3a8g8y")))

(define-public crate-clipcount-1.0.0 (c (n "clipcount") (v "1.0.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "13cvblv0vxx3lwbd98xn3g946ccb0xb220hq75kl7031qks31ky9")))

(define-public crate-clipcount-1.0.1 (c (n "clipcount") (v "1.0.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "0hqax0bc455cr77jq2vgd3jfaaqjagzwyfahdwpk1gwixj3pcgag")))

(define-public crate-clipcount-1.0.2 (c (n "clipcount") (v "1.0.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "0yfjr8y0mcpxb34cvpqwij0hlqznayvw9v8k3n9jj909sw030dys")))

(define-public crate-clipcount-1.0.4 (c (n "clipcount") (v "1.0.4") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "0wyi6pcwqdcdwbxhlb15hjnw9z8nk4wshrp9hlh9x89ady0c4z02")))

(define-public crate-clipcount-1.0.5 (c (n "clipcount") (v "1.0.5") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "110wzy4bdcdpypdmrswy5bf03gpknhw34x4ac0dwkwbq4y52cjjx")))

(define-public crate-clipcount-1.0.6 (c (n "clipcount") (v "1.0.6") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "13yawf594za1q1bmpcv2lwg1v4ddg8xyz0djpkrlnlm4xd40zc0r")))

(define-public crate-clipcount-1.0.7 (c (n "clipcount") (v "1.0.7") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "1mhfg3s95a300l531d3hqymsfm6wvvclskrqs3s642dqrl3fydh8")))

