(define-module (crates-io cl ip clippy-output) #:use-module (crates-io))

(define-public crate-clippy-output-0.1.0 (c (n "clippy-output") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0fv25nbij8f2wrgj1dsahkrdvd881g6c891fdldgg77qngc84fqc")))

(define-public crate-clippy-output-0.1.1 (c (n "clippy-output") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "01067125psiy8rgw8945xs9kvip5vsazqx5jnd83nh8ik9rwyk3z")))

(define-public crate-clippy-output-0.2.0 (c (n "clippy-output") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)))) (h "04230kq46iqhx1dahynqw5qihglfa1pbs4xq1hd5f01998yc2g1x")))

