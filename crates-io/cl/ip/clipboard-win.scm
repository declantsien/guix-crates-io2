(define-module (crates-io cl ip clipboard-win) #:use-module (crates-io))

(define-public crate-clipboard-win-0.2.0 (c (n "clipboard-win") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1si9kan8f4pivi6ymwmbpj82xnmg5976fm1i7rx461xm8p31jz72")))

(define-public crate-clipboard-win-0.3.1 (c (n "clipboard-win") (v "0.3.1") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "11692ijihrm4p8a4iin41h77g46v1jp0c457zacq9a3wbj2nkgwf")))

(define-public crate-clipboard-win-0.3.2 (c (n "clipboard-win") (v "0.3.2") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0ppl8mbkq2jyvhw1qajw7mrflqnm1zrc489ax2mlgndflyd4x6ni")))

(define-public crate-clipboard-win-0.3.3 (c (n "clipboard-win") (v "0.3.3") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "05bjbq19cspkdw2ljw28sv0n7hq682h98239dgrp7sfvp0sq1gg4")))

(define-public crate-clipboard-win-0.4.0 (c (n "clipboard-win") (v "0.4.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0sp9zcbdgsa1plw3ym9jwzli9ph3g9kyf8hxv7r5rj43kq6hk8ci")))

(define-public crate-clipboard-win-0.5.0 (c (n "clipboard-win") (v "0.5.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1vd8y0ay111k8sbzniivh91bxq4lqwc09lzh7cfy1fvwy7ksv2l9")))

(define-public crate-clipboard-win-0.5.1 (c (n "clipboard-win") (v "0.5.1") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "03w65hfs7vym1z9lq08b8pxmhxsds7qnb0kb0iffdvf810p737vq")))

(define-public crate-clipboard-win-0.5.2 (c (n "clipboard-win") (v "0.5.2") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "191pq2x1blj3qvfnkxwanaxbx4m4wkaf63r78r8566xwa91cjglv")))

(define-public crate-clipboard-win-0.6.0 (c (n "clipboard-win") (v "0.6.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "12v8wych3mgljvz0ww1gyx1871siaslcac1dc2a0z0gm93k5ihsi")))

(define-public crate-clipboard-win-0.7.0 (c (n "clipboard-win") (v "0.7.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "18yrba52c6v1ixc2hf0qdj1fb9z4jvyg8jl3m4yxi366mk17kak3")))

(define-public crate-clipboard-win-0.8.0 (c (n "clipboard-win") (v "0.8.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0r0nymp8r29ywq70k2k3sbbqc45gb5fzjj3ri25ic5l0idf9asdp")))

(define-public crate-clipboard-win-0.9.0 (c (n "clipboard-win") (v "0.9.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0adsygzsj90d5sdmpkgvp007fgmp78yn6dzi3ajd6lrsj92rhizn")))

(define-public crate-clipboard-win-0.9.1 (c (n "clipboard-win") (v "0.9.1") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "19s0i8kyy9d707kpvwgs5pmbydrlx4swgi8i48s0jj92npyg3f39")))

(define-public crate-clipboard-win-0.9.2 (c (n "clipboard-win") (v "0.9.2") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1jdma56d6y1lvmpbf8dzhcjzy6q2gyh9nfdhz644min6b562ihfh")))

(define-public crate-clipboard-win-0.9.3 (c (n "clipboard-win") (v "0.9.3") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "08m5kqf6ncrfnrlfzj8dfwwziq4dnn4hw6ivckxy666wia89cwmw")))

(define-public crate-clipboard-win-1.0.0 (c (n "clipboard-win") (v "1.0.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0chpwfpm9w118wcf3wp1kq9pkv8cr2j7ncwva47vjbf7p18s02c3")))

(define-public crate-clipboard-win-1.0.1 (c (n "clipboard-win") (v "1.0.1") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "085rqrkf05q1zgyxjknqrp6j5ldcrp75rbl4lbdnb7fz5841q5cr")))

(define-public crate-clipboard-win-1.1.0 (c (n "clipboard-win") (v "1.1.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0rqnp1q030acvfzy5yh69qfiqjlxi3igq0wvalhk8rqpb7p4lbym")))

(define-public crate-clipboard-win-1.2.0 (c (n "clipboard-win") (v "1.2.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "159k2miyvhf5rwh317nv0g8ir4zmfxj23jny326g03cax4k1mdyi")))

(define-public crate-clipboard-win-1.3.0 (c (n "clipboard-win") (v "1.3.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "09mqw8rmz14zszqz4f14wax0r6jp9kjimd1h4qxz1vqlsm9165jw")))

(define-public crate-clipboard-win-1.4.0 (c (n "clipboard-win") (v "1.4.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0v25qcapbjqdqwzyp0n3bajmm40z330xk1wjhyjr01909m19n9qn")))

(define-public crate-clipboard-win-1.4.1 (c (n "clipboard-win") (v "1.4.1") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1aw4jqc61n89kr02svwdakh15lmvipz8zak81z4s849a8ryqdqh7")))

(define-public crate-clipboard-win-1.4.2 (c (n "clipboard-win") (v "1.4.2") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1ssw5gincbx5z6j8dzym12j7z2nrdgcnhhax4hm022lb0m8sng9n")))

(define-public crate-clipboard-win-1.5.0 (c (n "clipboard-win") (v "1.5.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0s5yp1y49hhbbrj08r8q1fbjcfr3pfvgvgfqg8kaihambdd19pyi")))

(define-public crate-clipboard-win-1.5.1 (c (n "clipboard-win") (v "1.5.1") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "164x4r4hs1xjmy628ipyvqdxq0kav62pfsnjl5qngyx47khdm9dj")))

(define-public crate-clipboard-win-1.5.2 (c (n "clipboard-win") (v "1.5.2") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1xb5dygayn54aassfh4da1s39719zxjzh0a157bpjginp51sibcl")))

(define-public crate-clipboard-win-1.6.0 (c (n "clipboard-win") (v "1.6.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0k8xfxp67r7npllc8jsna04vjx59h3r1lkdgkah6g21as313d6s3")))

(define-public crate-clipboard-win-1.6.1 (c (n "clipboard-win") (v "1.6.1") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "19l0cx44rr6sw5bvh7kdak5aw49pl3i4shpjzy2nlikalx8sh0fa")))

(define-public crate-clipboard-win-1.6.2 (c (n "clipboard-win") (v "1.6.2") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "17wm28q4wx9jni7wbb5x6xrbyqqk3kls8ihifd6qz91zr258y8ha")))

(define-public crate-clipboard-win-1.7.0 (c (n "clipboard-win") (v "1.7.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "04x1q98392fg7vck7srr0mzybabgwiwfq059lbya1dc28kg4jvml")))

(define-public crate-clipboard-win-1.7.1 (c (n "clipboard-win") (v "1.7.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "0hky69rhqa0h653ck2dvkari26km7wvq7g96380igxi63g2qkl9x")))

(define-public crate-clipboard-win-1.8.0 (c (n "clipboard-win") (v "1.8.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (k 0)))) (h "08bv4xkb9dlg6ijc20idqni4abr2p7vfvmn1rch8csikhfyk1213")))

(define-public crate-clipboard-win-1.8.1 (c (n "clipboard-win") (v "1.8.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0b7d4anllwdglz1nsr0r1pphwwigqppwjqp9rkm8am4yi8kifl7h")))

(define-public crate-clipboard-win-2.0.0-pre.1 (c (n "clipboard-win") (v "2.0.0-pre.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(windows)") (k 0)))) (h "103qsnffjvps4nrrb7g49ivvrzy5ib47x23d1vdmig2x2cyla9mg")))

(define-public crate-clipboard-win-2.0.0 (c (n "clipboard-win") (v "2.0.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(windows)") (k 0)))) (h "0ng9p94qvlrbpmzfn2ijcvdkrc5ijhaqvdyvvy15610lqn014fv9")))

(define-public crate-clipboard-win-2.0.1 (c (n "clipboard-win") (v "2.0.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(windows)") (k 0)))) (h "0zkw62ri18iyn7z9biz222r5gmfdy6hfw4niazrvrmg6p1pbnari")))

(define-public crate-clipboard-win-2.1.0 (c (n "clipboard-win") (v "2.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "winbase" "winuser"))) (t "cfg(windows)") (k 0)))) (h "0hd554c7v2qsjn6c32wv316ymr7gq7an8s39j8w936wlq6l8ss84")))

(define-public crate-clipboard-win-2.1.1 (c (n "clipboard-win") (v "2.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "winbase" "winuser"))) (t "cfg(windows)") (k 0)))) (h "18s1frd0hmcnc676ka6qlh981gppb950vyfmj1jbj9jr0xn3xk0l")))

(define-public crate-clipboard-win-2.1.2 (c (n "clipboard-win") (v "2.1.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "winbase" "winuser"))) (t "cfg(windows)") (k 0)))) (h "0431sg4jhabrqf2dliiwhbx9hinb9z4qfcm6914lm5mb17ya5798") (f (quote (("utf16error"))))))

(define-public crate-clipboard-win-2.2.0 (c (n "clipboard-win") (v "2.2.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "basetsd" "shellapi" "winbase" "winuser"))) (t "cfg(windows)") (k 0)))) (h "0svqk0lrw66abaxd6h7l4k4g2s5vd1dcipy34kzfan6mzvb97873") (f (quote (("utf16error"))))))

(define-public crate-clipboard-win-3.0.0-alpha.1 (c (n "clipboard-win") (v "3.0.0-alpha.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "basetsd" "shellapi" "winbase" "winuser" "stringapiset"))) (t "cfg(windows)") (k 0)))) (h "08aisgx0xvr39cg53clmpgsx8k5k0xda2p44nibq84ma47sghmhd")))

(define-public crate-clipboard-win-3.0.0 (c (n "clipboard-win") (v "3.0.0") (d (list (d (n "lazy-bytes-cast") (r "^3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "basetsd" "shellapi" "winbase" "winuser" "stringapiset"))) (t "cfg(windows)") (k 0)))) (h "14j62kfcmplsxr7rdncxrhycpdkfklx39yjqingbzfmgpjzcr497")))

(define-public crate-clipboard-win-3.0.1 (c (n "clipboard-win") (v "3.0.1") (d (list (d (n "lazy-bytes-cast") (r "^4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "basetsd" "shellapi" "winbase" "winuser" "stringapiset"))) (t "cfg(windows)") (k 0)))) (h "16dynban4afcxmc1lq8xqn8cflv4b7qsqv69h2k3c9qa2pql30gm")))

(define-public crate-clipboard-win-3.0.2 (c (n "clipboard-win") (v "3.0.2") (d (list (d (n "lazy-bytes-cast") (r "^5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "basetsd" "shellapi" "winbase" "winuser" "stringapiset"))) (t "cfg(windows)") (k 0)))) (h "19r5hcv8g7cn1jh4gxjwb5x3gpz49ndaw3lrvn04qc53lk876grj")))

(define-public crate-clipboard-win-3.1.0 (c (n "clipboard-win") (v "3.1.0") (d (list (d (n "lazy-bytes-cast") (r "^5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "basetsd" "shellapi" "winbase" "winuser" "stringapiset"))) (t "cfg(windows)") (k 0)))) (h "0piavshc8n0gmi9v59cl15pgi2a93jdahsln3w7kzmgji7c44nlp")))

(define-public crate-clipboard-win-3.1.1 (c (n "clipboard-win") (v "3.1.1") (d (list (d (n "lazy-bytes-cast") (r "^5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "basetsd" "shellapi" "winbase" "winuser" "stringapiset"))) (t "cfg(windows)") (k 0)))) (h "0hh3npqfa1lfn62fwvkmjlpfnizq343a994b898ffsvb100mxpwz")))

(define-public crate-clipboard-win-4.0.0-beta.1 (c (n "clipboard-win") (v "4.0.0-beta.1") (d (list (d (n "error-code") (r "^2") (d #t) (k 0)) (d (n "str-buf") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi"))) (t "cfg(windows)") (k 0)))) (h "02g5xv44nn182jncgb28wvqyjn1apvbm0drsippwlqklmsf2wmma")))

(define-public crate-clipboard-win-4.0.0-beta.2 (c (n "clipboard-win") (v "4.0.0-beta.2") (d (list (d (n "error-code") (r "^2") (d #t) (k 0)) (d (n "str-buf") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi"))) (t "cfg(windows)") (k 0)))) (h "1ka7257hm0gih8g8fn78fbj4cl91g34yw48fd5cpmkdm8k44b96l") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.0.0 (c (n "clipboard-win") (v "4.0.0") (d (list (d (n "error-code") (r "^2") (d #t) (k 0)) (d (n "str-buf") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi"))) (t "cfg(windows)") (k 0)))) (h "0i9hrwzr6z4fqp42zlhafcf2cwsg3rhl2pmxb3frsbllj4898yvh") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.0.1 (c (n "clipboard-win") (v "4.0.1") (d (list (d (n "error-code") (r "^2") (d #t) (k 0)) (d (n "str-buf") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi"))) (t "cfg(windows)") (k 0)))) (h "0pv3pp8pzjp624ypx4y1cl2pknpx2hgjwh4k76zqrpmdj0a28k12") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.0.2 (c (n "clipboard-win") (v "4.0.2") (d (list (d (n "error-code") (r "^2") (d #t) (k 0)) (d (n "str-buf") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi"))) (t "cfg(windows)") (k 0)))) (h "0cwzl95jdfzrijy1r1fvkqz5dxprvgwxdwagjbdan3d6mqyrvhiz") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.0.3 (c (n "clipboard-win") (v "4.0.3") (d (list (d (n "error-code") (r "^2") (d #t) (k 0)) (d (n "str-buf") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi" "synchapi"))) (t "cfg(windows)") (k 0)))) (h "0bd9gw35zjdh50n2gy6qq2gwpnqfafzwkliqkvm9z046fawwc8si") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.1.0 (c (n "clipboard-win") (v "4.1.0") (d (list (d (n "error-code") (r "^2.1") (d #t) (k 0)) (d (n "str-buf") (r "^2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi" "synchapi"))) (t "cfg(windows)") (k 0)))) (h "1vb0s44l14ajxdk0xi48hwnk9ldfwqkqpvksrc80zr5dqgm3ihcx") (f (quote (("std" "error-code/std")))) (y #t)))

(define-public crate-clipboard-win-4.2.0 (c (n "clipboard-win") (v "4.2.0") (d (list (d (n "error-code") (r "^2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "str-buf") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi" "synchapi"))) (t "cfg(windows)") (k 0)))) (h "0963npidg1chf8fff1f8is1sjz9zlz1ka46cin2r1rclmxrl0spx") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.2.1 (c (n "clipboard-win") (v "4.2.1") (d (list (d (n "error-code") (r "^2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "str-buf") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi" "synchapi"))) (t "cfg(windows)") (k 0)))) (h "1a1cpp4yyizz41bkij5x85p220xxrlja6l6wwj9wkvwj364a2kjf") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.2.2 (c (n "clipboard-win") (v "4.2.2") (d (list (d (n "error-code") (r "^2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "str-buf") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi" "synchapi"))) (t "cfg(windows)") (k 0)))) (h "1vd0xvf8lcrswldn4qq8gsdjjf68k0xm8rhi8m1wp2njhc039f1x") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.3.0 (c (n "clipboard-win") (v "4.3.0") (d (list (d (n "error-code") (r "^2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "str-buf") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi" "synchapi"))) (t "cfg(windows)") (k 0)))) (h "1anp6v6i1yh7ys1jjj1h4i7wrj9fmbj1glmlnhcfx8k3l25gnl8r") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.4.0 (c (n "clipboard-win") (v "4.4.0") (d (list (d (n "error-code") (r "^2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "str-buf") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi" "synchapi"))) (t "cfg(windows)") (k 0)))) (h "0x7rd0ky9iqzy9d4778jardbnsml97vi4ppn8s2ngy8vvqcd3lxl") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.4.1 (c (n "clipboard-win") (v "4.4.1") (d (list (d (n "error-code") (r "^2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "str-buf") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi" "synchapi"))) (t "cfg(windows)") (k 0)))) (h "1nq58r8znx92k2yl9qhylp1z8pjfw6n9vfqw3q41zh1d2cw14gig") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.4.2 (c (n "clipboard-win") (v "4.4.2") (d (list (d (n "error-code") (r "^2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "str-buf") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi" "synchapi"))) (t "cfg(windows)") (k 0)))) (h "06921d19sw5smq8wnlk4a703f014cclr8lsv17ffw143g691pay4") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-4.5.0 (c (n "clipboard-win") (v "4.5.0") (d (list (d (n "error-code") (r "^2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "str-buf") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "shellapi" "winbase" "winuser" "winerror" "stringapiset" "errhandlingapi" "synchapi"))) (t "cfg(windows)") (k 0)))) (h "0qh3rypkf1lazniq4nr04hxsck0d55rigb5sjvpvgnap4dyc54bi") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-5.0.0-beta.1 (c (n "clipboard-win") (v "5.0.0-beta.1") (d (list (d (n "error-code") (r "^3") (d #t) (t "cfg(windows)") (k 0)))) (h "1sqml7cprn2iaak89y5i8rkqzbiin9qb7c4r9s77xbmi9d4rwafz") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-5.0.0 (c (n "clipboard-win") (v "5.0.0") (d (list (d (n "error-code") (r "^3") (d #t) (t "cfg(windows)") (k 0)))) (h "1k3jcc7mykdi6ws4wgl9v08d7sy9v9s3dqv7z4g7qxxyv6jh4w65") (f (quote (("std" "error-code/std"))))))

(define-public crate-clipboard-win-5.1.0 (c (n "clipboard-win") (v "5.1.0") (d (list (d (n "error-code") (r "^3") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-win") (r "^3") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "109f5p0288gyqnpnwk0x3wjrr7728lchsnml2f9wzf7g5ybk5j1y") (f (quote (("std" "error-code/std") ("monitor" "windows-win"))))))

(define-public crate-clipboard-win-5.2.0 (c (n "clipboard-win") (v "5.2.0") (d (list (d (n "error-code") (r "^3") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-win") (r "^3") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "15x28184cw6r8hx30farqvcir0kz151dfbfms4avl9q11rqa1y8j") (f (quote (("std" "error-code/std") ("monitor" "windows-win"))))))

(define-public crate-clipboard-win-5.3.0 (c (n "clipboard-win") (v "5.3.0") (d (list (d (n "error-code") (r "^3") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-win") (r "^3") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "1vm978fg2av0h48bz1r9im1afkd0r3qi0sjmsc8v3nw4c6wd85ym") (f (quote (("std" "error-code/std") ("monitor" "windows-win"))))))

(define-public crate-clipboard-win-5.3.1 (c (n "clipboard-win") (v "5.3.1") (d (list (d (n "error-code") (r "^3") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-win") (r "^3") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "1ba48760mbzv6jsfxbqyhf3zdp86ix3p4adgrsd0vqj4a4zlgx3r") (f (quote (("std" "error-code/std") ("monitor" "windows-win"))))))

