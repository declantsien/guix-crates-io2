(define-module (crates-io cl ip clipboard-cli) #:use-module (crates-io))

(define-public crate-clipboard-cli-0.0.1 (c (n "clipboard-cli") (v "0.0.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)))) (h "0bp0dj9n4j2iczs3mzx23n77ky2wkbayiarchcl1f762bda0bxyd")))

