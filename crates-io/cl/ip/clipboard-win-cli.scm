(define-module (crates-io cl ip clipboard-win-cli) #:use-module (crates-io))

(define-public crate-clipboard-win-cli-0.1.0 (c (n "clipboard-win-cli") (v "0.1.0") (d (list (d (n "clipboard-win") (r "^4.5.0") (d #t) (k 0)))) (h "14d09yqyqz0x9zsav2s13qnv14f1l12xljba1z8a73af8hjcmpx3")))

(define-public crate-clipboard-win-cli-0.1.1 (c (n "clipboard-win-cli") (v "0.1.1") (d (list (d (n "clipboard-win") (r "^4.5.0") (d #t) (k 0)))) (h "1qdnwywh0x9dxh9dgvhz49hsxyxpm4fs8n5j0752nnl48zz2s7jw")))

