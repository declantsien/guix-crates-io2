(define-module (crates-io cl ip clipboard2) #:use-module (crates-io))

(define-public crate-clipboard2-0.1.0 (c (n "clipboard2") (v "0.1.0") (d (list (d (n "clipboard-win") (r "^2.0.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc-foundation") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc_id") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "x11-clipboard") (r "^0.3.0-alpha.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0shrlwbmqi1lzc3y4lhi8sgv4yg58hrch6j0ig47fapnnfsah1jb")))

(define-public crate-clipboard2-0.1.1 (c (n "clipboard2") (v "0.1.1") (d (list (d (n "clipboard-win") (r "^2.0.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc-foundation") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc_id") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "x11-clipboard") (r "^0.3.0-alpha.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1rc0c895gg4d92n39nzvcfrw41947r0552pp8xyjlc7v2zr0s3r4")))

