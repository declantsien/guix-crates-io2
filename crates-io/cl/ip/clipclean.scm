(define-module (crates-io cl ip clipclean) #:use-module (crates-io))

(define-public crate-clipclean-0.1.0 (c (n "clipclean") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0czccvgs4rbnh64hy2n3s8pzv66zan6wz9dklfasp2dd4w7d5qzq")))

(define-public crate-clipclean-0.1.1 (c (n "clipclean") (v "0.1.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0x8rj8bvq8rjmakvrxxg6vjmml7fagri55qs8awjj3zi58lgcjhx")))

(define-public crate-clipclean-0.1.2 (c (n "clipclean") (v "0.1.2") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1fgs4bh3m1fscnlqnpw2lzsq4myrdzmwdiv8c72l5z1p8cfrfiw7")))

(define-public crate-clipclean-0.1.3 (c (n "clipclean") (v "0.1.3") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0b9rpfm3dxb8ggj9kr74mh9rb6ah84dj4b0fanlzg2960188whh5")))

(define-public crate-clipclean-0.1.4 (c (n "clipclean") (v "0.1.4") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0j85klkn44cvsczshg47lijw1dsybqq91wai5nq96xhzy7qvglbx")))

(define-public crate-clipclean-0.1.5 (c (n "clipclean") (v "0.1.5") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1gyikrcg60d22lmkwyy4r3k2hjwi5jlyhgcsn0dn4sv0byiymm8v")))

(define-public crate-clipclean-0.1.6 (c (n "clipclean") (v "0.1.6") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1sn9fw7ffqcc523gbvqpg2p4xn14rlbrmzcssy60145dksk8yz53")))

(define-public crate-clipclean-0.2.0 (c (n "clipclean") (v "0.2.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1wr9bx1gymxih3jygvc24740xr4ihc6qaxscvf9zy65yg8hy1fjg") (f (quote (("desktop-notifications" "notify-rust") ("default" "desktop-notifications"))))))

(define-public crate-clipclean-0.2.1 (c (n "clipclean") (v "0.2.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1l6h1dslcb59hn5nw7mqccs78lchdpn5h4ivyqsfn5srqhwd3pjy") (f (quote (("desktop-notifications" "notify-rust") ("default" "desktop-notifications"))))))

(define-public crate-clipclean-0.2.2 (c (n "clipclean") (v "0.2.2") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1z2vm2dgyhnfljhhpcwh8z5bcfa3sdd69f00gzsxdynrh6n1z637") (f (quote (("desktop-notifications" "notify-rust") ("default" "desktop-notifications"))))))

(define-public crate-clipclean-0.2.3 (c (n "clipclean") (v "0.2.3") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "08cprg52kgyh65clg68l7bsxsz0xqyr6884rxvsvrpfak7d37gg8") (f (quote (("desktop-notifications" "notify-rust") ("default" "desktop-notifications"))))))

(define-public crate-clipclean-0.2.4 (c (n "clipclean") (v "0.2.4") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0i8czgzav5mvgy39sndicr7k058bmhplfi83519gbpcxsnyai1j8") (f (quote (("desktop-notifications" "notify-rust") ("default" "desktop-notifications"))))))

