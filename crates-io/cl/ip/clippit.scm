(define-module (crates-io cl ip clippit) #:use-module (crates-io))

(define-public crate-clippit-0.1.0 (c (n "clippit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clippy-output") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1czzjjclxpcz02y9q4h91m46kz5rs5x4sr5gd6lbm06ldkc0f73g")))

(define-public crate-clippit-0.1.1 (c (n "clippit") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clippy-output") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0kw1l013q2nbq5qn651b7m67a5wax10v0k1ki8s5agy3pgyqgplw")))

(define-public crate-clippit-0.2.0 (c (n "clippit") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clippy-output") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1sav0z397i5xzfk2d18vhrxhcwdx6a9scpchpgr42977vd4g0gvb")))

(define-public crate-clippit-0.3.0 (c (n "clippit") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "terminal_size") (r "^0.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "18k07dr4p33hh3ffmx58l6n3ql2gp3kdvfkfxklvgyxs82fwqvnd")))

