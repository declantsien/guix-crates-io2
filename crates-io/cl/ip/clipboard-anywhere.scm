(define-module (crates-io cl ip clipboard-anywhere) #:use-module (crates-io))

(define-public crate-clipboard-anywhere-0.1.0 (c (n "clipboard-anywhere") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "wsl") (r "^0.1.0") (d #t) (k 0)))) (h "0xw3angl5cn2b6fijhac01nmblxg55k5f6rl1sf5vsxjcn0m4fhh")))

(define-public crate-clipboard-anywhere-0.1.1 (c (n "clipboard-anywhere") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "arboard") (r "^2.1.1") (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "wsl") (r "^0.1.0") (d #t) (k 0)))) (h "1ch0wgikvixxmn9fj2wi8v97mcyb01rg0x3xcinm6d5dkv4xpkl6")))

(define-public crate-clipboard-anywhere-0.2.0 (c (n "clipboard-anywhere") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "arboard") (r "^2.1.1") (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 0)) (d (n "wsl") (r "^0.1.0") (d #t) (k 0)))) (h "158p99n53z114s89qvjm2zdifacqfglrhbb67bh1gr6d1nk8h96l")))

(define-public crate-clipboard-anywhere-0.2.1 (c (n "clipboard-anywhere") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "arboard") (r "^2.1.1") (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 0)) (d (n "wsl") (r "^0.1.0") (d #t) (k 0)))) (h "1xv4hql5vmyc5p1lzym5pk59m68m3vpbs1niam4a4n56zpr9njpd")))

(define-public crate-clipboard-anywhere-0.2.2 (c (n "clipboard-anywhere") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "arboard") (r "^3.0.0") (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 0)) (d (n "is-wsl") (r "^0.4.0") (d #t) (k 0)))) (h "0x16hd12hp4mwrsvlgr1fvvvpvmh7lqcvrp9fmczx54i936va853")))

(define-public crate-clipboard-anywhere-0.2.3 (c (n "clipboard-anywhere") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "arboard") (r "^3.0.0") (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 0)) (d (n "is-wsl") (r "^0.4.0") (d #t) (k 0)))) (h "0axix896wx73a5rpk53iwrskm0v74ci8kp2y5r0gzcp2yllilshh")))

