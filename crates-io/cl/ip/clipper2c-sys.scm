(define-module (crates-io cl ip clipper2c-sys) #:use-module (crates-io))

(define-public crate-clipper2c-sys-0.1.0 (c (n "clipper2c-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a328va3fdmxr8v4wg112ih8hqafz13cq5lw02dxa6q9fm0921yi") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen"))))))

(define-public crate-clipper2c-sys-0.1.1 (c (n "clipper2c-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v1k67ck93dyl7ywywkk32rsi2rrw0x90v3701llwrmw3z6q6qzh") (f (quote (("update-bindings" "generate-bindings") ("generate-bindings" "bindgen")))) (s 2) (e (quote (("serde" "dep:serde"))))))

