(define-module (crates-io cl ip clipping) #:use-module (crates-io))

(define-public crate-clipping-0.1.0 (c (n "clipping") (v "0.1.0") (d (list (d (n "piston_window") (r "^0.75.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0iiwn86j7wh3djnf8n09mr8y6klnqmkp43f2nryg0y98r43i61pc")))

(define-public crate-clipping-0.1.1 (c (n "clipping") (v "0.1.1") (d (list (d (n "piston_window") (r "^0.75.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1q62hhvs63mi9ibx9lpgmfsp23yd79sdn9kgwry6z25wqirfxh16")))

