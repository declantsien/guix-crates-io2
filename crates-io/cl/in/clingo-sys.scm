(define-module (crates-io cl in clingo-sys) #:use-module (crates-io))

(define-public crate-clingo-sys-0.1.0 (c (n "clingo-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "02ww52s69zag2gyf7i0ylgkbg9azs1k1qd9k0n2i57zx9yxxcswh")))

(define-public crate-clingo-sys-0.1.1 (c (n "clingo-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0c908r42fq2i6hhr5dranbs280g9xiajiqz85w484x6nqr7mpzvq")))

(define-public crate-clingo-sys-0.1.2 (c (n "clingo-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0gvlk6598ifkgi9p8il16ixs4yxy59dafb8jjfxhg13qlv7ap1bv")))

(define-public crate-clingo-sys-0.1.3 (c (n "clingo-sys") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "130899451l9zc4sdp9x3zrjw6wnbslhm15krsjlhns6dzh4ry4hz")))

(define-public crate-clingo-sys-0.2.0 (c (n "clingo-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0ihjr2j31r96lxmmbdmy9n387k94cjrmbb1dzhp6l4ai32c3zlqz")))

(define-public crate-clingo-sys-0.3.0 (c (n "clingo-sys") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0q6rnajsz318lwshw07v50gvc5lyah9qqbn1bd6wpbavbb15vsqq")))

(define-public crate-clingo-sys-0.4.0 (c (n "clingo-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f0g5lx15x4h0vwk9khgk1v579gqc3a9sjdi3lws5kxmbdx059rq") (l "clingo")))

(define-public crate-clingo-sys-0.4.1 (c (n "clingo-sys") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1svz50zm8m8nc2ar6w6l8xzhywb3h8zkzx21liakdwwb7yh912al") (l "clingo")))

(define-public crate-clingo-sys-0.4.2 (c (n "clingo-sys") (v "0.4.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n0iqjp5nfr3jahp38nikr7vv2j5s21d9gqavf5gna8nwmgx4zmy") (f (quote (("dynamic_linking")))) (l "clingo")))

(define-public crate-clingo-sys-0.5.0 (c (n "clingo-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0yhkdbbkdrqhc0zvgmik5kzz8szzcj9ply93hnv41hnzwq9d9jr4") (f (quote (("dynamic_linking")))) (l "clingo")))

(define-public crate-clingo-sys-0.5.1 (c (n "clingo-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ljdcl6wyx07pyvbl2k20dg7zv3ivsq4asb3bd8yiy8m52mrxhps") (f (quote (("dynamic_linking")))) (l "clingo")))

(define-public crate-clingo-sys-0.6.0 (c (n "clingo-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0i7mmczhpkspz4isdg80r5nhvk0mk0ajff5afcr0qn7va2625n72") (f (quote (("dynamic_linking")))) (l "clingo")))

(define-public crate-clingo-sys-0.7.0 (c (n "clingo-sys") (v "0.7.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nahawv892sjrl7sg0zli064ya9ambwy8inzxsv88nkdx5wb7h24") (f (quote (("static-linking")))) (l "clingo")))

(define-public crate-clingo-sys-0.7.1 (c (n "clingo-sys") (v "0.7.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kbk5lrd6qgp8bpi89l3z4l79f2cp40kfjqdpcrrqj77kw6qxdm2") (f (quote (("static-linking")))) (l "clingo")))

(define-public crate-clingo-sys-0.7.2 (c (n "clingo-sys") (v "0.7.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1czah91lyd1yrcb88pgclaphx864ij97x8fq37hybc54ki66x719") (f (quote (("static-linking")))) (l "clingo")))

