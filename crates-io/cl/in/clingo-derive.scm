(define-module (crates-io cl in clingo-derive) #:use-module (crates-io))

(define-public crate-clingo-derive-0.1.0 (c (n "clingo-derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "clingo") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (d #t) (k 0)))) (h "161h6m2pqdycf4kih880lmkglnmnv54cs5hn52c43yc0ws7mw8ci")))

(define-public crate-clingo-derive-0.1.1 (c (n "clingo-derive") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "clingo") (r "^0.4.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (d #t) (k 0)))) (h "1yflg2ipfxvcc51q8wcg5jziw85k39zcq2vwn6firg3zkjhsy04k")))

(define-public crate-clingo-derive-0.1.2 (c (n "clingo-derive") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "clingo") (r "^0.5") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "04fsxp80g19a857mp8jw8q98pdsicz4nfkwcbvg0l1yqdv5g07r2")))

(define-public crate-clingo-derive-0.2.0 (c (n "clingo-derive") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0fc1wgb4cgdjmjmg5y6xdiznx1c14ijnjxljjrcfjdwi61bzz7gn")))

