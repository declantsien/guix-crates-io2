(define-module (crates-io cl in clinput) #:use-module (crates-io))

(define-public crate-clinput-0.1.0 (c (n "clinput") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15fp8f399wd1bdf6zmp3dg2xhy0s7q6gwzcnfjbg12cr9gx98a28") (y #t)))

(define-public crate-clinput-0.1.1 (c (n "clinput") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sd82g2k6z96xzg2gkljgxpvnp063rcwjmhhg8j8jvy7y2r2ywn0") (y #t)))

(define-public crate-clinput-0.2.0 (c (n "clinput") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hp9qvwb8j0vlz2n2hn7wx111fm6asndy5zbahays5j2dwfxcr72")))

