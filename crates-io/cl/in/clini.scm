(define-module (crates-io cl in clini) #:use-module (crates-io))

(define-public crate-clini-0.1.0 (c (n "clini") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.16") (d #t) (k 0)))) (h "0d8kssvzrh5dh1sxk8ca98r5valv2fg7xv5wkri8hid55zvl3d01")))

