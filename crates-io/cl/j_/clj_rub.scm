(define-module (crates-io cl j_ clj_rub) #:use-module (crates-io))

(define-public crate-clj_rub-0.0.1 (c (n "clj_rub") (v "0.0.1") (d (list (d (n "buildable") (r "= 0.0.1") (d #t) (k 0)) (d (n "docopt") (r "= 0.6.14") (d #t) (k 0)))) (h "19ang6ppv0j0hh757cqp12qp502l7pvh3l3aa7bwmn1jhgn8hbz8")))

(define-public crate-clj_rub-0.0.2 (c (n "clj_rub") (v "0.0.2") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "1hr2qd97m4y6s99pvc647bh1x1c0sx5c6qda0g3xssb8c97nziv8")))

(define-public crate-clj_rub-0.0.3 (c (n "clj_rub") (v "0.0.3") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "1gf8489fxgh98n3l2cir9yx1z4i33sllrl9v4xqa0bb0d5vv8sma")))

(define-public crate-clj_rub-0.0.4 (c (n "clj_rub") (v "0.0.4") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "07ydyjl9nn4aw38byx6mpx3fl8brz41cwbcp5xwa1hcwsa9xbc12")))

