(define-module (crates-io cl pc clpcpi) #:use-module (crates-io))

(define-public crate-clpcpi-0.1.0 (c (n "clpcpi") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bwlznsnyjlypk0n27kh1driwklf413xxssg22wpdj8hnljbrsnf") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("localnet") ("devnet-deploy") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clpcpi-0.1.1 (c (n "clpcpi") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bms48d2szy726ax83ij6x76xrnz026822vm1861xph56lh7iybi") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("localnet") ("devnet-deploy") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clpcpi-0.1.2 (c (n "clpcpi") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bqwm9j2wmwm7qvlvj1zhhfv46x88lwvk6m0l83ivjnljrrfjmka") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("localnet") ("devnet-deploy") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clpcpi-0.1.4 (c (n "clpcpi") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lgw4idhqqlsnpmx0p4b2xmmcnbd1hrf4hagz1j1an04h11zy2pq") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("localnet") ("devnet-deploy") ("default") ("cpi" "no-entrypoint"))))))

