(define-module (crates-io cl f- clf-parser) #:use-module (crates-io))

(define-public crate-clf-parser-0.1.0 (c (n "clf-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "bytecheck") (r "^0.7") (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_64" "validation"))) (o #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (k 0)) (d (n "serde-xml-rs") (r "^0.6") (k 0)))) (h "18sh94w5vzjb6hm0a8zbvg4p8v7qjmsckkv2igl4b9qq4nz6mf17")))

(define-public crate-clf-parser-0.2.0 (c (n "clf-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "rkyv") (r "^0.7.40") (f (quote ("size_64" "validation"))) (o #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "serde-xml-rs") (r "^0.6") (k 0)))) (h "1v8l0bhdg93nb560hckafx4n6sjpdzjln6py0dxdxf5z8lxa24pj")))

