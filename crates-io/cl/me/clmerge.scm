(define-module (crates-io cl me clmerge) #:use-module (crates-io))

(define-public crate-clmerge-0.1.0 (c (n "clmerge") (v "0.1.0") (d (list (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0a613snygwr675x2c26hki5hfjhycsf1i5jilb6gj2xvzr0043ym")))

(define-public crate-clmerge-0.1.1 (c (n "clmerge") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1hzr0gcfbx489457q9dyl6a1yd2jydirmyai1qalxbh998c189f1")))

(define-public crate-clmerge-0.1.2 (c (n "clmerge") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "0v07k4ryrarckgxh4mjd1gvi0m6mc56cikakfv23jcl9njylpsp3")))

