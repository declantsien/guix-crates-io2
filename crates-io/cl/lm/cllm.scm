(define-module (crates-io cl lm cllm) #:use-module (crates-io))

(define-public crate-cllm-2.0.0 (c (n "cllm") (v "2.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1hiz80pss7d315x14irr5b8a5mqmzar1qrb4w184g5xjqp13rwjb") (y #t)))

(define-public crate-cllm-2.0.1 (c (n "cllm") (v "2.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0np072dwpfhx007y2v2rjhdl7w91z325afw5x9bbqlq60xcp7b0m") (y #t)))

