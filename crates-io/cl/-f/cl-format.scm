(define-module (crates-io cl -f cl-format) #:use-module (crates-io))

(define-public crate-cl-format-0.1.0 (c (n "cl-format") (v "0.1.0") (d (list (d (n "cl-format-macros") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11bivpri2zfr3yqxyvdgp9gm8vvf08xy8kkbl0v5pmnn25sfkpjr")))

(define-public crate-cl-format-0.1.1 (c (n "cl-format") (v "0.1.1") (d (list (d (n "cl-format-macros") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yrnsqmcm47grrsa7spa5yi0cfaqs2bcc50q6h1bz0sv2rrl96wx")))

(define-public crate-cl-format-0.1.2 (c (n "cl-format") (v "0.1.2") (d (list (d (n "cl-format-macros") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z2y5q7qyy8q9205ifq8pnwxg85g0qmnvhhs2nnhpfkpb7kxz4yx")))

(define-public crate-cl-format-0.1.3 (c (n "cl-format") (v "0.1.3") (d (list (d (n "cl-format-macros") (r "^0.1") (d #t) (k 0)))) (h "06fgm5agrxkwhcz26696h74xmg4ys6l82jfm3pq84zyqsl7ybpy6")))

(define-public crate-cl-format-0.1.4 (c (n "cl-format") (v "0.1.4") (d (list (d (n "cl-format-macros") (r "^0.1") (d #t) (k 0)))) (h "1vx9d582ikhcnxi0a06s300a0a8v1d15l0x73x71g6q6kkv4adxp")))

(define-public crate-cl-format-0.1.5 (c (n "cl-format") (v "0.1.5") (d (list (d (n "cl-format-macros") (r "^0.1") (d #t) (k 0)))) (h "07mcysx1rgb6dvfbkq7gymmg2fn9nnyzj6p2s30n8lrq06xcq7h5")))

(define-public crate-cl-format-0.1.6 (c (n "cl-format") (v "0.1.6") (d (list (d (n "cl-format-macros") (r "^0.1.3") (d #t) (k 0)))) (h "1qnbdpah2v0bja98dakglpan298misn4fy4m7pkzjqsyq1bpwcs0")))

(define-public crate-cl-format-0.1.7 (c (n "cl-format") (v "0.1.7") (d (list (d (n "cl-format-macros") (r "^0.1") (d #t) (k 0)))) (h "0nvfqrzg59xjiyqp0vqyg70g5cvm0g8ib5sln72xds4gxrk006mi")))

(define-public crate-cl-format-0.2.0 (c (n "cl-format") (v "0.2.0") (d (list (d (n "cl-format-macros") (r "^0.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "12sla1hvhvqrh0z6f3gpw84vrjf2k5q2lgb6m27xiqfnrcdpli7q")))

(define-public crate-cl-format-0.2.1 (c (n "cl-format") (v "0.2.1") (d (list (d (n "cl-format-macros") (r "^0.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0w8skrlrhwp8sldx8i76ggwpx2mlbax4mywflwwlhx9q26frpp7k")))

(define-public crate-cl-format-0.2.2 (c (n "cl-format") (v "0.2.2") (d (list (d (n "cl-format-macros") (r "^0.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "17ycbhpgdg9xwd0bgpk1q0ga3bzsk5ra6ymd2p38plhq11p8gvr6")))

(define-public crate-cl-format-0.2.3 (c (n "cl-format") (v "0.2.3") (d (list (d (n "cl-format-macros") (r "^0.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0a8mnc4yhabjg6dqj3vcwxj6yxwrk9ym88n4yjlvn1b7i54kkghm")))

