(define-module (crates-io cl -f cl-format-macros) #:use-module (crates-io))

(define-public crate-cl-format-macros-0.1.0 (c (n "cl-format-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11ck6bn4v3f7h699bccyg03ldrw46rqs1518zyicc2ws5cxmmimb")))

(define-public crate-cl-format-macros-0.1.1 (c (n "cl-format-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jf3mmc5i9b2ly7lfwxrdf9avir60yxxygbw2wcf3ymm4izaf5zf")))

(define-public crate-cl-format-macros-0.1.2 (c (n "cl-format-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cwwl0gfnlp5rca07y5a84kd1x55bkhwx8blpw48yzj42skic86b")))

(define-public crate-cl-format-macros-0.1.3 (c (n "cl-format-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "167zks27zg0znnxhp78wrxjm02ch080sw008jdir6j1846w2j43y")))

(define-public crate-cl-format-macros-0.1.4 (c (n "cl-format-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w7bj74ig2kil3dzasmfdy20ms9fwqp7bki2fsvwxz5l14nhgrmg")))

(define-public crate-cl-format-macros-0.1.5 (c (n "cl-format-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "163l7h1gkkqaqvaxcaf90bfhikicki11pmwq7mszq7bqv14m2jn3")))

