(define-module (crates-io cl ap clap-vergen-macro) #:use-module (crates-io))

(define-public crate-clap-vergen-macro-0.2.0 (c (n "clap-vergen-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12z4k38yq22lamyasqcja1qwbgsr792h2riz9ii7adfyl5z8sa3m")))

