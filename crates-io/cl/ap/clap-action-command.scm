(define-module (crates-io cl ap clap-action-command) #:use-module (crates-io))

(define-public crate-clap-action-command-0.1.0 (c (n "clap-action-command") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("std"))) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "vec1") (r "^1") (d #t) (k 0)))) (h "11d0z0567k6qad84h8w9cc8hv77i8nc2ig5pcy9gcxlx0dwvjp4l")))

