(define-module (crates-io cl ap clap_complete_command) #:use-module (crates-io))

(define-public crate-clap_complete_command-0.1.0 (c (n "clap_complete_command") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^3") (d #t) (k 0)))) (h "19qq44if6zgrb9p807zk5i9rnif5dcqjd8223by9a93c5mnwhwmr")))

(define-public crate-clap_complete_command-0.2.0 (c (n "clap_complete_command") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^3") (d #t) (k 0)))) (h "0lf2j36ghcdfq1vrcq6s71swsqzm7h2dxmz9j9wqv0xsbg43yyzf")))

(define-public crate-clap_complete_command-0.2.1 (c (n "clap_complete_command") (v "0.2.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^3") (d #t) (k 0)))) (h "0kwbhjbkysm9l04kbij76szsfsn4a0cc5hqpr9wi4qwxj28cssl8")))

(define-public crate-clap_complete_command-0.2.2 (c (n "clap_complete_command") (v "0.2.2") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap_complete") (r "^3") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^3") (d #t) (k 0)))) (h "11fp9w5lp1fmx2cdrnlb09jhkankparljpcm1bwaai2glna8kay4")))

(define-public crate-clap_complete_command-0.3.0 (c (n "clap_complete_command") (v "0.3.0") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap_complete") (r "^3") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^3") (d #t) (k 0)))) (h "01sx1bsvhfsdyi2v4885fjjhm2w9mnb75j1nwgg6rylm95590bkf")))

(define-public crate-clap_complete_command-0.3.1 (c (n "clap_complete_command") (v "0.3.1") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap_complete") (r "^3") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^3") (d #t) (k 0)))) (h "1sa0dhh8qclq3nzic5fqxjcv4n73if8lsc70nn1iakla76y6f1az")))

(define-public crate-clap_complete_command-0.3.2 (c (n "clap_complete_command") (v "0.3.2") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap_complete") (r "^3") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^3") (d #t) (k 0)))) (h "1pnin6ap1gk0gisw8v8r7r7g714rv9xyrbggjf65nnpwd3mwkr8m")))

(define-public crate-clap_complete_command-0.3.3 (c (n "clap_complete_command") (v "0.3.3") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap_complete") (r "^3") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^3") (d #t) (k 0)))) (h "0cnq4xvkjgcrmi169ng05ca9lsknwr5pbww899chq22c4ybbkanm")))

(define-public crate-clap_complete_command-0.3.4 (c (n "clap_complete_command") (v "0.3.4") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap_complete") (r "^3") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^3") (d #t) (k 0)))) (h "09qjsnf7phbgqziq2qjyln490zcs0rrdb8djidiyx5wv72rmjwfp")))

(define-public crate-clap_complete_command-0.4.0 (c (n "clap_complete_command") (v "0.4.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap_complete") (r "^4") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^4") (d #t) (k 0)))) (h "0ccfljj8aa8mbpss1xi25bccj77gwq4prlmscvbqpx9fyyjb8q21")))

(define-public crate-clap_complete_command-0.5.0 (c (n "clap_complete_command") (v "0.5.0") (d (list (d (n "carapace_spec_clap") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "clap_complete") (r "^4") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^4") (o #t) (d #t) (k 0)) (d (n "clap_complete_nushell") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)))) (h "1127blx0sc9hs3w1fldlql192q4033s34qw96qmfrdf7f56qkbbp") (f (quote (("default" "fig" "nushell")))) (s 2) (e (quote (("nushell" "dep:clap_complete_nushell") ("fig" "dep:clap_complete_fig") ("carapace" "dep:carapace_spec_clap"))))))

(define-public crate-clap_complete_command-0.5.1 (c (n "clap_complete_command") (v "0.5.1") (d (list (d (n "carapace_spec_clap") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "clap_complete") (r "^4") (d #t) (k 0)) (d (n "clap_complete_fig") (r "^4") (o #t) (d #t) (k 0)) (d (n "clap_complete_nushell") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)))) (h "0gcsj6ls8y0jpjp5172gdqwx5zj6gm4wdgrqysglr3d73qvrad0q") (f (quote (("default" "fig" "nushell")))) (s 2) (e (quote (("nushell" "dep:clap_complete_nushell") ("fig" "dep:clap_complete_fig") ("carapace" "dep:carapace_spec_clap"))))))

