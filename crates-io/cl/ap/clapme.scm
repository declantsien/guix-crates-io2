(define-module (crates-io cl ap clapme) #:use-module (crates-io))

(define-public crate-clapme-0.0.1 (c (n "clapme") (v "0.0.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.0.1") (d #t) (k 0)))) (h "0nz26rwsbxdazwmcap0zfsq9gkbpf8jm63r55zc1bj7n4zffr04p")))

(define-public crate-clapme-0.0.2 (c (n "clapme") (v "0.0.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.0.2") (d #t) (k 0)))) (h "0glfcgcaka738zkc86jwhhkpyfv9h2pg0lx8zpilpp0rir2i3dhb")))

(define-public crate-clapme-0.1.0 (c (n "clapme") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1nzjqx14n1jx6z2hh3ix0xvlv6bax7cxmwjm34vjcl280pg35phb")))

(define-public crate-clapme-0.1.1 (c (n "clapme") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1229z42cn8fjgfibmdr0z7qawkmdiszy55pk19gap1bwkx067s8k")))

(define-public crate-clapme-0.1.2 (c (n "clapme") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.2") (d #t) (k 0)))) (h "11rjb4qhh0l2q0vj6kp3aar44c2c89z9qrl2wav4zwda7impji3f")))

(define-public crate-clapme-0.1.3 (c (n "clapme") (v "0.1.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.3") (d #t) (k 0)))) (h "0a87klkbqs6dfgj2r8aqx5ygapapq53w4fsr3x2q3czbvzl59kq8")))

(define-public crate-clapme-0.1.4 (c (n "clapme") (v "0.1.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.3") (d #t) (k 0)))) (h "02rdb20d7gfwgvvm65r574p3qajckqc9ciacmij23vwskx3kcsw3")))

(define-public crate-clapme-0.1.5 (c (n "clapme") (v "0.1.5") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.3") (d #t) (k 0)))) (h "1pxp5y6610l6jh1czrbbzsfi06nfj1pgbwd58nlszqc0hn7amkx0")))

(define-public crate-clapme-0.1.6 (c (n "clapme") (v "0.1.6") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.6") (d #t) (k 0)))) (h "1xji444jdaw4dhsqizqbyfdv0g8l38bf6ixdwlrcjibgfi6ivdrj")))

(define-public crate-clapme-0.1.7 (c (n "clapme") (v "0.1.7") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.7") (d #t) (k 0)))) (h "0sk8j9b19kv4aqsf2g2q6zbajxyx11hcixvvaihcvy0a7miyv9xf")))

(define-public crate-clapme-0.1.8 (c (n "clapme") (v "0.1.8") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.8") (d #t) (k 0)))) (h "1r06k6rqaf0ra3h8sp47qjj06kc619rgfqw9383yzy7mhxgw7l0l")))

(define-public crate-clapme-0.1.9 (c (n "clapme") (v "0.1.9") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.9") (d #t) (k 0)))) (h "0qkpnarj0gbjgmx3709c6vrvbix2n0g3pfyjlflbx43bzkwsaihl")))

(define-public crate-clapme-0.1.10 (c (n "clapme") (v "0.1.10") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.10") (d #t) (k 0)))) (h "0b39w79qvsk6rqxmd9vph4d6gpfxbyqq95a66bg74dfa6l4ks3x4")))

(define-public crate-clapme-0.1.11 (c (n "clapme") (v "0.1.11") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.10") (d #t) (k 0)))) (h "0gvsk657vq8x89lvn01yfi7qhzvfwi6m8nxaczwjqrj903has9an")))

(define-public crate-clapme-0.1.12 (c (n "clapme") (v "0.1.12") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.10") (d #t) (k 0)) (d (n "rustyard") (r "^0.6.0") (d #t) (k 0)))) (h "1jl9vvw0nc8833xfvy5k3djql1mr9d1bavczi71v84anscxrl60i")))

(define-public crate-clapme-0.1.13 (c (n "clapme") (v "0.1.13") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.10") (d #t) (k 0)) (d (n "meval") (r "^0.1.0") (d #t) (k 0)))) (h "1af507xckdz4vss7qixyssciaz46zm0zxkw2rhx670nq3gag1lnw")))

(define-public crate-clapme-0.1.14 (c (n "clapme") (v "0.1.14") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.10") (d #t) (k 0)) (d (n "meval") (r "^0.1.0") (d #t) (k 0)))) (h "1l2dw5mqdjdyzkkv6sj1nw2k1fq9gfj2hn00sx3qlx1ag6b0i38j")))

(define-public crate-clapme-0.1.15 (c (n "clapme") (v "0.1.15") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.10") (d #t) (k 0)) (d (n "meval") (r "^0.1.0") (d #t) (k 0)))) (h "1svr0ii609s4inga7na9jsn940g7nlr7v0siw6cy07wr8i52bwqs")))

(define-public crate-clapme-0.1.16 (c (n "clapme") (v "0.1.16") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "clapme_derive") (r "^0.1.10") (d #t) (k 0)) (d (n "meval") (r "^0.1.0") (d #t) (k 0)))) (h "1nhix69na4l8x070yrvk2bk2mx45c92k8ngb7jbpylz3abq13k76")))

