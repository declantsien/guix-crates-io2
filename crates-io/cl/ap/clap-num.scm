(define-module (crates-io cl ap clap-num) #:use-module (crates-io))

(define-public crate-clap-num-0.1.0 (c (n "clap-num") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0qmrvbzhzfbfbjgdmv8ffqgyrhgk4ij67fhk671q4wgqx15fbb5v")))

(define-public crate-clap-num-0.1.1 (c (n "clap-num") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1lpdnnkdf2xpjgq27wnqpxjqjdfxc134lgjcw9d7nnhf5srmpibr")))

(define-public crate-clap-num-0.1.2 (c (n "clap-num") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0339sa427bllkpfp7ji6mav47xszv0497ydndmccx05r3bwszr6b")))

(define-public crate-clap-num-0.1.3 (c (n "clap-num") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0j62dkn14v4v53djs12qvr7923vjdwfsf1pgx0zj51cq0zjw4a2g")))

(define-public crate-clap-num-0.2.0 (c (n "clap-num") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "03fxzlc6b1pzcf67pifg5b8z5xhxmgqgv53g881v26qz65d8ww2n")))

(define-public crate-clap-num-1.0.0 (c (n "clap-num") (v "1.0.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0mphz0p2my2zi62dwv903nan84760537h9k1zigwig39hs1rsn7a") (r "1.56")))

(define-public crate-clap-num-1.0.1 (c (n "clap-num") (v "1.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12xxfp2lpshqr7vnchnzqixmfxdm93mk6ds9qq5y13240zcna308")))

(define-public crate-clap-num-1.0.2 (c (n "clap-num") (v "1.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0zhlv9ps8xa5yfsbqjrs46ccbq09m0iqn9pflbd4w5r8fplmg1a8")))

(define-public crate-clap-num-1.1.0 (c (n "clap-num") (v "1.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0l2jwx6bi9bpv5k70lzp3mw84q595cjk5gy8cyjnn5p0dxza79bb")))

(define-public crate-clap-num-1.1.1 (c (n "clap-num") (v "1.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1dwa3vsrgh3nn4a4nr1k8z6l09qjgkdwwr5h9z2rv1b46ck3s1hf")))

