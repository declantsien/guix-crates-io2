(define-module (crates-io cl ap clap-serde-derive) #:use-module (crates-io))

(define-public crate-clap-serde-derive-0.1.0 (c (n "clap-serde-derive") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-serde-proc") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 2)))) (h "03qfy45g3bx5xzgpwmwjxh32awp0y3kz7fpqh3v2wq207k8wvy5j")))

(define-public crate-clap-serde-derive-0.2.0 (c (n "clap-serde-derive") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-serde-proc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 2)))) (h "1a55m1m50v8valbmiavsbgp5ldsh5qhbfff0v4my9iwnclgn25xr")))

(define-public crate-clap-serde-derive-0.2.1 (c (n "clap-serde-derive") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-serde-proc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 2)))) (h "09f1jkjzhzfjbkdl84jcwi5vq0i0md29i2yvnnq4wfnwrd1xddy4")))

