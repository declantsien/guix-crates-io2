(define-module (crates-io cl ap clapcomp) #:use-module (crates-io))

(define-public crate-clapcomp-0.1.0 (c (n "clapcomp") (v "0.1.0") (d (list (d (n "clap") (r "^2.9.1") (f (quote ("yaml"))) (d #t) (k 0)))) (h "19v8dmbnj0crp75s0yc1s4hs7qhs08qcr5xwy2wsskqbcf6j4z03")))

(define-public crate-clapcomp-0.1.1 (c (n "clapcomp") (v "0.1.1") (d (list (d (n "clap") (r "^2.10.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "16iam1cnacw074h72pv4vxd4skh2m5jllzh3jk4xg411hs3xd2d0")))

(define-public crate-clapcomp-0.1.3 (c (n "clapcomp") (v "0.1.3") (d (list (d (n "clap") (r "^2.16.2") (f (quote ("yaml"))) (d #t) (k 0)))) (h "103qcj3c94ldm3ippdlwlcm6hpc90gxv8wn16p9mlksvc6ij13dp")))

(define-public crate-clapcomp-0.1.4 (c (n "clapcomp") (v "0.1.4") (d (list (d (n "clap") (r "^2.19.2") (f (quote ("yaml"))) (d #t) (k 0)))) (h "035l3rhza1y2ikn5zjmpnal2ri62h8m5n3sqhvispjc7a4npnrz3")))

(define-public crate-clapcomp-0.1.5 (c (n "clapcomp") (v "0.1.5") (d (list (d (n "clap") (r "^2.20.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1i0kfl0z6bvfwpnywy5gj9ff74m1hr91whfb83zkdmg79930sdwc")))

