(define-module (crates-io cl ap clap-and-async-test) #:use-module (crates-io))

(define-public crate-clap-and-async-test-0.1.0 (c (n "clap-and-async-test") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "1d2zyra60wza15hm7jfgyvkflmgclw6vgnnwqdkm2n66ag2r7jfq")))

(define-public crate-clap-and-async-test-0.1.1 (c (n "clap-and-async-test") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "10pir7wxhz345w0dwzdwpdglrqc6gdrq4dig3n8qcq4amyga6baw")))

(define-public crate-clap-and-async-test-0.1.2 (c (n "clap-and-async-test") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.0") (d #t) (k 0)))) (h "0i41g1gvrq8b9xl3anfd4q4gpy66dpca907rg2kjrjzaqkf4h3bn")))

