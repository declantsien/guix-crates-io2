(define-module (crates-io cl ap clappers) #:use-module (crates-io))

(define-public crate-clappers-0.0.1 (c (n "clappers") (v "0.0.1") (h "1y8n1jpq96ks7978nmjh2qnx8crc6706d7a17ip69ass2g9gh822")))

(define-public crate-clappers-0.0.2 (c (n "clappers") (v "0.0.2") (h "1m8igsz3akg0kjlj7v1qclma6l3zv3rjsfp9j7f8f2ji3473k1ar")))

(define-public crate-clappers-0.0.3 (c (n "clappers") (v "0.0.3") (h "0bgmav1nm7xhcx10hkxiw10ikccf5x85y3qiymv6sz5h165mx8zd")))

(define-public crate-clappers-0.1.4 (c (n "clappers") (v "0.1.4") (h "0ml7psraimxrxxqr5jmgjz6g3sa7l00grnsb8q3pnqvnwwcsvf4f")))

(define-public crate-clappers-0.1.5 (c (n "clappers") (v "0.1.5") (h "1av4di4ym1k8in2s9njksyf917r16rh5l623fv5crg1hy1wjd2yl")))

(define-public crate-clappers-1.2.6 (c (n "clappers") (v "1.2.6") (h "00y6v0azw6lsdg4j3nrcg30kdrqp2k78iidgchm5zkwn9gwbsrkd")))

(define-public crate-clappers-1.2.7 (c (n "clappers") (v "1.2.7") (h "0s0s5kyhr10rbq4l4z2l4dpv064jjaiiccbl2y8pasy7cb1amsar")))

(define-public crate-clappers-1.2.8 (c (n "clappers") (v "1.2.8") (h "1r7fssbsq8k415llg3xx8gqd9mpa4c747yhcnnrahw6zm4fc0qcf")))

(define-public crate-clappers-1.2.9 (c (n "clappers") (v "1.2.9") (h "1aryl797kcp9ax7w4d2sig9hqpm8m6j4r31328k310nfz18q1y12")))

(define-public crate-clappers-2.0.0 (c (n "clappers") (v "2.0.0") (h "1zrac5y0nr4nmdc0xhmyyqjwjk7m21xqijzdy6f9ks2xydj0izp1")))

(define-public crate-clappers-2.0.1 (c (n "clappers") (v "2.0.1") (h "15414wvcx3kc0v4jxai70ibs01i3apclvhsqxi9c471ibfzm6b81")))

(define-public crate-clappers-2.0.2 (c (n "clappers") (v "2.0.2") (h "05rv2z65h9snyp5ad0jmswy4mb9alzghv2d386acr6vkj8ik3bzg")))

(define-public crate-clappers-2.0.3 (c (n "clappers") (v "2.0.3") (h "0hy7vcjd4d5a8vajmh7zvygclbsiy8kf5gcy0cfi2rzf4m3ycs21")))

(define-public crate-clappers-3.1.2 (c (n "clappers") (v "3.1.2") (h "0al1ynknjnqxh3d56f4dmzm0jczax901809zijwfv33vxmi1k89k")))

(define-public crate-clappers-3.1.3 (c (n "clappers") (v "3.1.3") (h "1a6kwpbb7hrllfsf27489fbmvc78kzrh65yary79p3cii7x6cg03")))

