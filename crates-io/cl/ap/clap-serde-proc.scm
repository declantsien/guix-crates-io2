(define-module (crates-io cl ap clap-serde-proc) #:use-module (crates-io))

(define-public crate-clap-serde-proc-0.1.0 (c (n "clap-serde-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x045fywzq828yhbsdm80yk3dna6gp73jlzdxdxvrvw98gsgw2kk")))

(define-public crate-clap-serde-proc-0.2.0 (c (n "clap-serde-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ppm7s15dfgqf6ws4p8vz6ikcfqk4sja5aacmp6miw86z7y5qwpn")))

