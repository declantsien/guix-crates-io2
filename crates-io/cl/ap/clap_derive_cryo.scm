(define-module (crates-io cl ap clap_derive_cryo) #:use-module (crates-io))

(define-public crate-clap_derive_cryo-4.3.12 (c (n "clap_derive_cryo") (v "4.3.12") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "124rsf1nsdp42riy4bbqak16npdnla156x6v5qc9iv5ipp7d477x") (f (quote (("unstable-v5" "deprecated") ("raw-deprecated" "deprecated") ("deprecated") ("default") ("debug")))) (r "1.64.0")))

