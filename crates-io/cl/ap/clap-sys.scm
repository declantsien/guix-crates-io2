(define-module (crates-io cl ap clap-sys) #:use-module (crates-io))

(define-public crate-clap-sys-0.1.0 (c (n "clap-sys") (v "0.1.0") (h "04dwacrvy22vhsx9wg2cnbaqply643plx27pbqdl8hkaxwaak2ja")))

(define-public crate-clap-sys-0.2.0 (c (n "clap-sys") (v "0.2.0") (h "1s81k71pzas3n360sdvr30lwrfxqjyryc7f7nij8kfqk5pvnsc5c")))

(define-public crate-clap-sys-0.3.0 (c (n "clap-sys") (v "0.3.0") (h "1qq907flcl8h9szn40qplj1r8dcw6ppmq5di9lcmsgfkaxkf1x17") (r "1.59")))

(define-public crate-clap-sys-0.4.0 (c (n "clap-sys") (v "0.4.0") (h "15r10idni687cwsfcwkrx1fv2f7d3lvcj1v06yzmkl0wk1a47c31") (r "1.59")))

