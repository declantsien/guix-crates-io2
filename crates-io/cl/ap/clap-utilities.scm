(define-module (crates-io cl ap clap-utilities) #:use-module (crates-io))

(define-public crate-clap-utilities-0.0.0 (c (n "clap-utilities") (v "0.0.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1h5r4xgmgvzz33rpqn914fvfzjahg6ib08bq6854y6j6w8sai2vz")))

(define-public crate-clap-utilities-0.1.0 (c (n "clap-utilities") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.2.3") (d #t) (k 0)) (d (n "pipe-trait") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1dbrpv4ijw7l3hyjghcv9kxlmqqrd5z0v3z7gbf9l00m5vlry517")))

(define-public crate-clap-utilities-0.2.0 (c (n "clap-utilities") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.2") (d #t) (k 0)) (d (n "pipe-trait") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "163p99zc3wp02khfrqvn176c5bbprv03l8jrbrh16lgngs0gzg0m")))

