(define-module (crates-io cl ap clap-vergen) #:use-module (crates-io))

(define-public crate-clap-vergen-0.1.0 (c (n "clap-vergen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.59") (d #t) (k 1)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "vergen") (r "^7.3.2") (d #t) (k 1)))) (h "1ffm6vy56g9k35mdjg30z48n10m7g49hphmfb09xmhb67bfjnnrd")))

(define-public crate-clap-vergen-0.2.0 (c (n "clap-vergen") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-vergen-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "1csvw6r8nswn37nf9awp1ayl44gf575gpgmd1wi21aansi80h57g")))

