(define-module (crates-io cl ap clap_main) #:use-module (crates-io))

(define-public crate-clap_main-0.1.0 (c (n "clap_main") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "01z17f875jawh3knnnkpmfvipgg3dnd3d54psix0g7g21l8r0igg")))

(define-public crate-clap_main-0.1.1 (c (n "clap_main") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1fqh098bpg3m0fg0x0w9wpryilaw6hnl79q1gc2b9r5rbj8sq3zc")))

(define-public crate-clap_main-0.1.2 (c (n "clap_main") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "08ixchdmlf8mx6dnsfspj5avgvxpd6sw9fxj9bhf0fhx43qd8spg")))

(define-public crate-clap_main-0.1.3 (c (n "clap_main") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0pbrik56zgq2zggihg5h55wdfcg9grlcfw4nhd8sf46108s9srqm")))

(define-public crate-clap_main-0.2.0 (c (n "clap_main") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0h92f7v0jg9sm5ph86b0wmj8n69mzhbwiy8i0lprblbvkclamyam")))

(define-public crate-clap_main-0.2.1 (c (n "clap_main") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "029r5qbb2ybqqnq31f6l69ban9l7aspdlv016zizc6fjn10vc2hk")))

(define-public crate-clap_main-0.2.2 (c (n "clap_main") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1hqm06f1cmil0dynys4gkrx9r0jwyahfqpg10qs2sw44f6iagv5v")))

(define-public crate-clap_main-0.2.3 (c (n "clap_main") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1cxqvyshgqbdip5qfyig2gmw5w2pvswnmgkwq2rkc8glb3k22g56")))

(define-public crate-clap_main-0.2.4 (c (n "clap_main") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "05ys159pc3pasbk3zphmvz2bsxbb7d6df88pc5xln46y3gki6262")))

(define-public crate-clap_main-0.2.5 (c (n "clap_main") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "18vxy7jrsl0y2d4krgckb82hdlrg2lb1sfgykskpmgh4sfan7cs3")))

(define-public crate-clap_main-0.2.6 (c (n "clap_main") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1fs5zz19xdcql0wfnpqcg2ryf24xsh9y3s19ja67v78ksnsm6krj")))

(define-public crate-clap_main-0.2.7 (c (n "clap_main") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0cpkim5d4rv7zjvia882g3f7s4wsc1xlzwfr8c88n3d5pni80792")))

(define-public crate-clap_main-0.2.8 (c (n "clap_main") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "083b1ygxvdr3siqdpsrj4an4f3kgrg90mfmkwja1ax7zf3jcwqdx")))

(define-public crate-clap_main-0.2.9 (c (n "clap_main") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1la8z0ax739vls6yjh8zxvfjl874lgqswj04bi0j2w7smm04zdzm")))

