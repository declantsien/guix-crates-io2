(define-module (crates-io cl ap clap_generate_fig) #:use-module (crates-io))

(define-public crate-clap_generate_fig-3.0.0-beta.5 (c (n "clap_generate_fig") (v "3.0.0-beta.5") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 0)) (d (n "clap_generate") (r "=3.0.0-beta.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "0vgjkgmvg7gabzbk5i6f2da4x0cv1dmziyrzai660xvay0v2hj5k")))

(define-public crate-clap_generate_fig-3.0.0-rc.1 (c (n "clap_generate_fig") (v "3.0.0-rc.1") (d (list (d (n "clap") (r "=3.0.0-rc.1") (f (quote ("std"))) (k 0)) (d (n "clap_generate") (r "=3.0.0-rc.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "161ryfq35fc5cn7p6bz8aai36c4dwxmksa8a8v25i9cpy55yq3a1")))

(define-public crate-clap_generate_fig-3.0.0-rc.3 (c (n "clap_generate_fig") (v "3.0.0-rc.3") (d (list (d (n "clap") (r "=3.0.0-rc.3") (f (quote ("std"))) (k 0)) (d (n "clap_generate") (r "=3.0.0-rc.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1djzyd5190kdw8x3ih0mxlrp1qjmmflrdb10vbcv8r1hijb4ba5c")))

(define-public crate-clap_generate_fig-3.0.0-rc.4 (c (n "clap_generate_fig") (v "3.0.0-rc.4") (d (list (d (n "clap") (r "=3.0.0-rc.4") (f (quote ("std"))) (k 0)) (d (n "clap_generate") (r "=3.0.0-rc.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "018k3s109lzcm6hfxipf82ijr8mmb2sd81276kayp0nxp4d3q7m9")))

(define-public crate-clap_generate_fig-3.0.0-rc.5 (c (n "clap_generate_fig") (v "3.0.0-rc.5") (d (list (d (n "clap") (r "=3.0.0-rc.5") (f (quote ("std"))) (k 0)) (d (n "clap_generate") (r "=3.0.0-rc.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0bc25z4llsjg4iiaz9nq2k88mhnmvmkmraa93yb9bfwr76mj8hgy")))

(define-public crate-clap_generate_fig-3.0.0-rc.6 (c (n "clap_generate_fig") (v "3.0.0-rc.6") (d (list (d (n "clap") (r "=3.0.0-rc.6") (f (quote ("std"))) (k 0)) (d (n "clap_generate") (r "=3.0.0-rc.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1k3n5ihf55bqhzv499yj77bjzx9vk7bs6nb4craakj7i9ahcdlpy")))

(define-public crate-clap_generate_fig-3.0.0-rc.7 (c (n "clap_generate_fig") (v "3.0.0-rc.7") (d (list (d (n "clap") (r "=3.0.0-rc.7") (f (quote ("std"))) (k 0)) (d (n "clap_generate") (r "=3.0.0-rc.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1mymr6q1m0vgpa03i7ri8l3r4bkxa3379r28cs78qm8f5jk3gdgl")))

(define-public crate-clap_generate_fig-3.0.0-rc.8 (c (n "clap_generate_fig") (v "3.0.0-rc.8") (d (list (d (n "clap") (r "=3.0.0-rc.8") (f (quote ("std"))) (k 0)) (d (n "clap_generate") (r "=3.0.0-rc.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1qhm00knwfqgimy1b3602glxy1qk7i006w3qppycqs34nqk8xcfv")))

(define-public crate-clap_generate_fig-3.0.0-rc.9 (c (n "clap_generate_fig") (v "3.0.0-rc.9") (d (list (d (n "clap") (r "=3.0.0-rc.9") (f (quote ("std"))) (k 0)) (d (n "clap_generate") (r "=3.0.0-rc.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1v9kaw0n9wrfi5lwy4hw58g8a9gk6mxfx6cqr395diwcd9wkcd7i")))

(define-public crate-clap_generate_fig-3.0.0-rc.10 (c (n "clap_generate_fig") (v "3.0.0-rc.10") (d (list (d (n "clap") (r "=3.0.0-rc.10") (f (quote ("std"))) (k 0)) (d (n "clap_generate") (r "=3.0.0-rc.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1ymkgi66kjhwzr7x7cyjdh4rz0imr2gnxllcw6svb87qx6gi30dp")))

(define-public crate-clap_generate_fig-3.0.0-rc.11 (c (n "clap_generate_fig") (v "3.0.0-rc.11") (d (list (d (n "clap") (r "=3.0.0-rc.11") (f (quote ("std"))) (k 0)) (d (n "clap_generate") (r "=3.0.0-rc.11") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "176d58wjhw27qli5x9wrhxavsgnvbvjyyhfpapk2j5vyaqrw42gn")))

(define-public crate-clap_generate_fig-3.0.0-rc.12 (c (n "clap_generate_fig") (v "3.0.0-rc.12") (d (list (d (n "clap_complete_fig") (r "=3.0.0-rc.12") (d #t) (k 0)))) (h "0vnyijz7pc41smmvmhy65p2api9ja7zz1avk1ifgh8h549654fk1")))

(define-public crate-clap_generate_fig-3.0.0-rc.13 (c (n "clap_generate_fig") (v "3.0.0-rc.13") (d (list (d (n "clap_complete_fig") (r "=3.0.0-rc.13") (d #t) (k 0)))) (h "0g5pkh3bcg1jkjbwrjawr4wps8jrlwpzix5inm7r3zfi9xrd3p7w")))

(define-public crate-clap_generate_fig-3.0.0 (c (n "clap_generate_fig") (v "3.0.0") (d (list (d (n "clap_complete_fig") (r "^3.0.0") (d #t) (k 0)))) (h "0jjwpaqqvimipz71c43nk6f47yp0m0w44hpwzh0ngkxjmrvwqncn")))

(define-public crate-clap_generate_fig-3.0.1 (c (n "clap_generate_fig") (v "3.0.1") (d (list (d (n "clap_complete_fig") (r "^3.0.0") (d #t) (k 0)))) (h "1mqmwsgzz5qdyx9mhy0s32iz73av2w7r6nvf1cbfb51ybbybgy0r")))

