(define-module (crates-io cl ap clap_logger) #:use-module (crates-io))

(define-public crate-clap_logger-0.3.0 (c (n "clap_logger") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "1nhlirv9y5ay25y0icqard75ys3d9ina5nq5bpyfwlicvqzzddha") (f (quote (("env"))))))

(define-public crate-clap_logger-0.3.1 (c (n "clap_logger") (v "0.3.1") (d (list (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "06qazhs6na9a2lmi6rj1srjqlm7a8xa5386ywwchp7gh42dsic82") (f (quote (("env"))))))

(define-public crate-clap_logger-0.3.2 (c (n "clap_logger") (v "0.3.2") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v395qrj242h3hcv6yakr2lp3b09dnmjr9ak8aqjdni280j8aq6n") (f (quote (("env"))))))

(define-public crate-clap_logger-0.3.3 (c (n "clap_logger") (v "0.3.3") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0b2fxh424yf8n13jn7n75iyhndrns7fjz9szqhb1f8f90rb1da65") (f (quote (("env"))))))

