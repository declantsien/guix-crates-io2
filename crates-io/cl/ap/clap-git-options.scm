(define-module (crates-io cl ap clap-git-options) #:use-module (crates-io))

(define-public crate-clap-git-options-0.1.0 (c (n "clap-git-options") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("std" "derive"))) (k 0)) (d (n "git-wrapper") (r "0.6.*") (o #t) (d #t) (k 0)))) (h "0wckhazbnwfb17m8wkn4rr4qvzvqiam3xd11nllanfr5b68f59jz")))

(define-public crate-clap-git-options-0.2.0 (c (n "clap-git-options") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("std" "derive"))) (k 0)) (d (n "git-wrapper") (r "0.6.*") (o #t) (d #t) (k 0)))) (h "0nmm3dyh1i7zvspyfy8ynckrc2adsl9xw0vdmg7pwq268czc08q8")))

