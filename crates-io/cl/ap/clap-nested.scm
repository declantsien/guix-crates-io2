(define-module (crates-io cl ap clap-nested) #:use-module (crates-io))

(define-public crate-clap-nested-0.1.0 (c (n "clap-nested") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0hp92i7mxriysjckps6s3rlqdqbn95d0vrfa4li3jrci6ilk87ma") (y #t)))

(define-public crate-clap-nested-0.1.1 (c (n "clap-nested") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0l5m9irh8dyanmv2h3a5az23d8viss74bamm86fd0a3k5sh8azh6") (y #t)))

(define-public crate-clap-nested-0.2.0 (c (n "clap-nested") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1nbg9503h61nk4m6nr85967k24ygyrwzs8g4ny9j23qcxzf4zhri") (y #t)))

(define-public crate-clap-nested-0.2.1 (c (n "clap-nested") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1wmwbpw8b1926gxymkmscw4s742a1xy75r7lh09a9gajr3smzwbr") (y #t)))

(define-public crate-clap-nested-0.3.0 (c (n "clap-nested") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 2)))) (h "1vkx5801lwqiycwal2sjwwk37r5lk3799yr53zvfvzfgc4yxb10h") (y #t)))

(define-public crate-clap-nested-0.3.1 (c (n "clap-nested") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 2)))) (h "109xcr9ncq0wvwwjizmvm6rxan84zrnpv3z26rg9q5j8yx6q5ax1")))

(define-public crate-clap-nested-0.4.0 (c (n "clap-nested") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 2)))) (h "1d43qg8qd31a8mql6n1cbydz12ps9khi9xbnvgvx8qx07dgqd9gs")))

