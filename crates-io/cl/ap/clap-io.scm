(define-module (crates-io cl ap clap-io) #:use-module (crates-io))

(define-public crate-clap-io-0.1.0 (c (n "clap-io") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("std" "derive"))) (k 0)))) (h "1jaw4fv5xr6vh3xckpsrw3w21m4p79ls0ip50ca4j1w2kqw3ir39")))

