(define-module (crates-io cl ap clapi_macros) #:use-module (crates-io))

(define-public crate-clapi_macros-0.1.0 (c (n "clapi_macros") (v "0.1.0") (d (list (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits" "parsing" "visit"))) (d #t) (k 0)))) (h "1dhp9wpfj8yk0lvycm4874brs634xhb9zqv4npk412jc4cpbw13p")))

(define-public crate-clapi_macros-0.1.1 (c (n "clapi_macros") (v "0.1.1") (d (list (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits" "parsing" "visit"))) (d #t) (k 0)))) (h "04ibndwkf79p57hiryzmsdhvrjr7scpcl016djzsj4l8dvr67k7a")))

