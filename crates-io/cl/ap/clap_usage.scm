(define-module (crates-io cl ap clap_usage) #:use-module (crates-io))

(define-public crate-clap_usage-0.1.0 (c (n "clap_usage") (v "0.1.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "clap_complete") (r "^4.3.2") (d #t) (k 0)) (d (n "snapbox") (r "^0.4") (f (quote ("diff" "path" "examples"))) (d #t) (k 2)))) (h "0ax24vaps9s1r2gfvjjkpd0axf1654r99sbsiyqm2ira1jl71v9w")))

