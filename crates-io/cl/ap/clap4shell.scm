(define-module (crates-io cl ap clap4shell) #:use-module (crates-io))

(define-public crate-clap4shell-0.1.3 (c (n "clap4shell") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "clap_complete") (r "^3.2.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0pjy2mlwmzq74c18lkiamj0p3n6sljms0154qwv8nqgd80xy91wh")))

(define-public crate-clap4shell-0.2.0 (c (n "clap4shell") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "clap-serde") (r "^0.5.0") (d #t) (k 0)) (d (n "clap_complete") (r "^3.2.4") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "08yp99rc9bs5sbxkiy0hyzcxjwnmqx76dqh809ynmpsqg0jj388p")))

(define-public crate-clap4shell-0.3.0 (c (n "clap4shell") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "clap-serde") (r "^0.5.0") (d #t) (k 0)) (d (n "clap_complete") (r "^3.2.4") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "11wvhxd0xn3shdz0lr8sr4sw0jcjnxkrmhrvw7i5c23fzrdbhkvj")))

(define-public crate-clap4shell-0.3.1 (c (n "clap4shell") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "clap-serde") (r "^0.5.0") (d #t) (k 0)) (d (n "clap_complete") (r "^3.2.4") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0liqmgn8f7jx7wjdfgm3vpq1j5r6ma8qbbkwyp9dshnwvg14625a")))

(define-public crate-clap4shell-0.3.2 (c (n "clap4shell") (v "0.3.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("env"))) (d #t) (k 0)) (d (n "clap-serde") (r "^0.5.1") (f (quote ("env"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.2.4") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1mfl2cz78iik29w3n77kcawrk0pj7v2sp5psakl4p23625pxzs4f")))

