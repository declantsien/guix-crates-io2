(define-module (crates-io cl ap clap-duration) #:use-module (crates-io))

(define-public crate-clap-duration-0.1.4 (c (n "clap-duration") (v "0.1.4") (d (list (d (n "duration-human") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1714awk4q2dv9xqyk1zan6dhvkn6wm8459yvb85sfniw0xa1acrp")))

(define-public crate-clap-duration-0.1.5 (c (n "clap-duration") (v "0.1.5") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "duration-human") (r "^0.1.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01sc60qzflxj3clhzvcz2v28asssj10qg9qbc45nsyzckag277dl")))

(define-public crate-clap-duration-0.1.6 (c (n "clap-duration") (v "0.1.6") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "duration-human") (r "^0.1.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1za0rqln6p6bnryhawbi4k4l8rk8wfmaf193d57zmq740kd6chl0")))

(define-public crate-clap-duration-0.1.7 (c (n "clap-duration") (v "0.1.7") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "duration-human") (r "^0.1.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "022vnbfp7xgsw409rjmwh8b7nq0zmdvwq1mppprgad9v67rckw0k")))

(define-public crate-clap-duration-0.1.8 (c (n "clap-duration") (v "0.1.8") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "duration-human") (r "^0.1.8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "053d559lzx4gpcfzmspiyq67p72dmdj1v9wds3frs1hpzw851a8v")))

(define-public crate-clap-duration-0.1.9 (c (n "clap-duration") (v "0.1.9") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "duration-human") (r "^0.1.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12yc5p1p44qs0ss8zxqymmjacab7dga0hmpfm71xwlxa3vqhg5nv")))

(define-public crate-clap-duration-0.1.10 (c (n "clap-duration") (v "0.1.10") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "duration-human") (r "^0.1.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ki83bvgbj9vpd0f92ngr0flhw0nliqzh198day2s9h0xxwkn395")))

(define-public crate-clap-duration-0.1.11 (c (n "clap-duration") (v "0.1.11") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "duration-human") (r "^0.1.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v3haklmvxja2x2wz21y218326mnb6ds6g1n05hr6gh5swspvh21")))

