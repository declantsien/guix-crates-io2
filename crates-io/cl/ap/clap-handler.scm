(define-module (crates-io cl ap clap-handler) #:use-module (crates-io))

(define-public crate-clap-handler-0.1.0 (c (n "clap-handler") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "clap-handler-derive") (r "=0.1.0") (d #t) (k 0)))) (h "1bnysady1g7xizk02v9f6pdp2r9hcan51xs3xz7k3dl7f8z4m2fh") (f (quote (("default") ("async" "async-trait" "clap-handler-derive/async"))))))

(define-public crate-clap-handler-0.1.1 (c (n "clap-handler") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "clap-handler-derive") (r "=0.1.0") (d #t) (k 0)))) (h "04g9yf7876ig8sm55c2bc142j20r1czglm6283dkvccx5wlzznsv") (f (quote (("default") ("async" "async-trait" "clap-handler-derive/async"))))))

