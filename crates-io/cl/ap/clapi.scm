(define-module (crates-io cl ap clapi) #:use-module (crates-io))

(define-public crate-clapi-0.1.0 (c (n "clapi") (v "0.1.0") (d (list (d (n "clapi_internal") (r "^0.1.0") (d #t) (k 0)) (d (n "clapi_macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.118") (d #t) (k 2)))) (h "0ic2yxzn29k56zhv1jb02j0v3jx0aprhr1xh7vgyr39ckpn2gngq") (f (quote (("typing") ("macros" "clapi_macros"))))))

(define-public crate-clapi-0.1.1 (c (n "clapi") (v "0.1.1") (d (list (d (n "clapi_internal") (r "^0.1.0") (d #t) (k 0)) (d (n "clapi_macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.118") (d #t) (k 2)))) (h "0zz6vahqrvh1m3j01xg67qhyba3c8bfm44akfvkh2vgvahqf0jri") (f (quote (("typing") ("macros" "clapi_macros"))))))

(define-public crate-clapi-0.1.2 (c (n "clapi") (v "0.1.2") (d (list (d (n "clapi_internal") (r "^0.1.0") (d #t) (k 0)) (d (n "clapi_macros") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.118") (d #t) (k 2)))) (h "0v1bhgw0ll27hl5vcm6zhbydhmfzwi397mycchklg0pczqw5jqhh") (f (quote (("typing") ("macros" "clapi_macros"))))))

