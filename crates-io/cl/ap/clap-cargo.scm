(define-module (crates-io cl ap clap-cargo) #:use-module (crates-io))

(define-public crate-clap-cargo-0.1.1 (c (n "clap-cargo") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1y3yj20nbp8n68kgjhhfby0wcczp0wzwimn4qwvmkgknfls9fds4")))

(define-public crate-clap-cargo-0.1.2 (c (n "clap-cargo") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0kcx1diix8nscvrfx2wc59s5hb0qasbank2d0gj6vm7vz68y96nx")))

(define-public crate-clap-cargo-0.1.4 (c (n "clap-cargo") (v "0.1.4") (d (list (d (n "cargo_metadata") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1m5s96m9g8b0qdx2pwlqn5709j4jrj6rdfvvgs6gns9zg1kkpbvc")))

(define-public crate-clap-cargo-0.2.0 (c (n "clap-cargo") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "1kvxhvg5xh8cajj9fch59psggnm360bxps5kag2myrk9zy5l6bfv")))

(define-public crate-clap-cargo-0.3.0 (c (n "clap-cargo") (v "0.3.0") (d (list (d (n "cargo_metadata") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "0iqk2xnqjfsdh6y1c7znix4223651kg53185x0bypyv97wzn7gwv")))

(define-public crate-clap-cargo-0.3.1 (c (n "clap-cargo") (v "0.3.1") (d (list (d (n "cargo_metadata") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "1gc0xb5pqnvlwqsp4m8vqsf3d57xfhczy6fzckr5slw8mi8izh82")))

(define-public crate-clap-cargo-0.4.0 (c (n "clap-cargo") (v "0.4.0") (d (list (d (n "cargo_metadata") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "19qz5c5k2glqd5jyvll0vm7f4bal0lpr2k8h9bxp9b5r76vyxxx7")))

(define-public crate-clap-cargo-0.4.1 (c (n "clap-cargo") (v "0.4.1") (d (list (d (n "cargo_metadata") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "09fwk993af3nxjyk1i46rk5336042kznixv0frhr858mhfn6dlpb")))

(define-public crate-clap-cargo-0.5.0 (c (n "clap-cargo") (v "0.5.0") (d (list (d (n "cargo_metadata") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "091an3p7vhwcigrh1s337rgpdhmxqchfzricaybra9pv9l0jzh8n")))

(define-public crate-clap-cargo-0.6.0 (c (n "clap-cargo") (v "0.6.0") (d (list (d (n "cargo_metadata") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "0w91hilgmzlj8a0r2rck4zw7ngmi3vcd24shwiqahbf0lpa5mzn2")))

(define-public crate-clap-cargo-0.6.1 (c (n "clap-cargo") (v "0.6.1") (d (list (d (n "cargo_metadata") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "0dc1srhq58g0xg6nyi0fz13593758lgdzmwf39x3v3mjkhgx6xy5")))

(define-public crate-clap-cargo-0.7.0 (c (n "clap-cargo") (v "0.7.0") (d (list (d (n "cargo_metadata") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "1z0b205lcx6yxvv9qa5gvfvyv7ahn4b8g5i5yysmlz3ky4flkw7c")))

(define-public crate-clap-cargo-0.8.0 (c (n "clap-cargo") (v "0.8.0") (d (list (d (n "cargo_metadata") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("std" "derive"))) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "1m18zjv2maf1vwllpfd2y1q4giqiphb21sn4kgi11lnf6jjnl6sm")))

(define-public crate-clap-cargo-0.9.0 (c (n "clap-cargo") (v "0.9.0") (d (list (d (n "cargo_metadata") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("std" "derive"))) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "1lxg7lwjqh4kf3x6xcbwzqkshgxlm7paahj67pwai9kcfrm8pdy6") (r "1.56.1")))

(define-public crate-clap-cargo-0.9.1 (c (n "clap-cargo") (v "0.9.1") (d (list (d (n "cargo_metadata") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("std" "derive"))) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "024vbvb0hfi22n7gbg88i4w1qxlvfzxz854hrn03prnsdk11f6w4") (r "1.56.1")))

(define-public crate-clap-cargo-0.10.0 (c (n "clap-cargo") (v "0.10.0") (d (list (d (n "cargo_metadata") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "derive"))) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "0b3yrzrk9f2qna7y1aa6s5s7nvww3nms15fvc45mcl3k19jm7agc") (r "1.60.0")))

(define-public crate-clap-cargo-0.11.0 (c (n "clap-cargo") (v "0.11.0") (d (list (d (n "cargo_metadata") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "derive"))) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "1g9f11mzyqy3qrzc63cpd5ll4q01kzyqlqr6iibm6pxdxfk2q4i5") (r "1.66.0")))

(define-public crate-clap-cargo-0.12.0 (c (n "clap-cargo") (v "0.12.0") (d (list (d (n "anstyle") (r "^1.0.3") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("std" "derive"))) (o #t) (k 0)))) (h "1r2f4ad1vpaljrfbyfsv986qiwmll0iask4sdvwllka658s22grq") (f (quote (("default" "clap")))) (s 2) (e (quote (("clap" "dep:clap")))) (r "1.70.0")))

(define-public crate-clap-cargo-0.13.0 (c (n "clap-cargo") (v "0.13.0") (d (list (d (n "anstyle") (r "^1.0.3") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("std" "derive"))) (o #t) (k 0)))) (h "1lswnh87n07h4kyr8kwnbr9bxkxs9qdl3j4rd2knirwmarhmbbiq") (f (quote (("default" "clap")))) (s 2) (e (quote (("clap" "dep:clap")))) (r "1.70.0")))

(define-public crate-clap-cargo-0.14.0 (c (n "clap-cargo") (v "0.14.0") (d (list (d (n "anstyle") (r "^1.0.3") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("std" "derive"))) (o #t) (k 0)))) (h "1233cpy2zplca0w07h5dizgf34wxxdhnma39bwwwrizqr0hgvqpn") (f (quote (("default" "clap")))) (s 2) (e (quote (("clap" "dep:clap")))) (r "1.72")))

