(define-module (crates-io cl ap clap-verbosity) #:use-module (crates-io))

(define-public crate-clap-verbosity-2.1.0 (c (n "clap-verbosity") (v "2.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("std" "derive"))) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-log") (r "^0.2.0") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "0qscghkz4ly8ccm71iy1n0gy4lm3p8cpk3kypmcaa1q4irdgfyrx") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.70.0")))

