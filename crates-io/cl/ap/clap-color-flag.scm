(define-module (crates-io cl ap clap-color-flag) #:use-module (crates-io))

(define-public crate-clap-color-flag-1.0.0 (c (n "clap-color-flag") (v "1.0.0") (d (list (d (n "anstream") (r "^0.3.0") (d #t) (k 2)) (d (n "clap") (r "^4.2.0") (f (quote ("std" "derive" "color"))) (k 0)) (d (n "clap") (r "^4.2.0") (d #t) (k 2)) (d (n "colorchoice") (r "^1.0.0") (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 2)))) (h "0q7mcv9i0n34flzj3dvp9nqy9bra2zq57i34yfy8cn3mylmmpgzs") (r "1.65.0")))

