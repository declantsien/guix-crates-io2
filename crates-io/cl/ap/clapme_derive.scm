(define-module (crates-io cl ap clapme_derive) #:use-module (crates-io))

(define-public crate-clapme_derive-0.0.1 (c (n "clapme_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "028brps2rqlmw50cryi6f3vxfigjl42rq2nq6rkqa4fvqvri2xd9")))

(define-public crate-clapme_derive-0.0.2 (c (n "clapme_derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1bx9cplnd0ziwcy2lfqjcwx0iqvlirs1ra9a0l7akjnilcs2k0c9")))

(define-public crate-clapme_derive-0.1.0 (c (n "clapme_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1345m1bg811j115zdb47fxvnvrjdw5v7av9isslfprgjsy92jvwx")))

(define-public crate-clapme_derive-0.1.1 (c (n "clapme_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0xhjdh2zgkzxyrhnxld6jz1a2xvzydlw8hkdl308q5w20x1dzb2z")))

(define-public crate-clapme_derive-0.1.2 (c (n "clapme_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0f82vcqahz18kq1qd9mf5kdc26dcqah3m0zs2n8hid7qsx11a40q")))

(define-public crate-clapme_derive-0.1.3 (c (n "clapme_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1rhl9252q94hiw1bbb9760qcv82b07mnmf1law2kqfa5fg719gwh")))

(define-public crate-clapme_derive-0.1.4 (c (n "clapme_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0j80jvx4yq083bc412hnwhxbsd5znp2bil0lxrh5x0zfbslipnyp")))

(define-public crate-clapme_derive-0.1.5 (c (n "clapme_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "00hc318627j8wfl96kh3pqkb2yhpzxv3x53yvkacwi2b4g16d1w3")))

(define-public crate-clapme_derive-0.1.6 (c (n "clapme_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1z7xmff3xvl661yqxdgmbasm0s46rim5y9rp8vjdvi5izrd0b6d6")))

(define-public crate-clapme_derive-0.1.7 (c (n "clapme_derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1cscrmzwv2jggryrx4sh470kbr8ks0l6hd3q5xw3vfkaxi38aqb2")))

(define-public crate-clapme_derive-0.1.8 (c (n "clapme_derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1w7i27fysqynk4m1m5hnk8v39vvc1zxh8jxwzhhn6sdnkgw2h4hd")))

(define-public crate-clapme_derive-0.1.9 (c (n "clapme_derive") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "09h0hgwy0vr82jrplhwp1p8mbixh2vs5g9a3lbc6x5hbqr5yp0lm")))

(define-public crate-clapme_derive-0.1.10 (c (n "clapme_derive") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0y8s0jrva1h8fap697dz0ajksn5jakhgb0w5361dx12ncjhma5qf")))

