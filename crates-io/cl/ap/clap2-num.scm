(define-module (crates-io cl ap clap2-num) #:use-module (crates-io))

(define-public crate-clap2-num-1.0.2 (c (n "clap2-num") (v "1.0.2") (d (list (d (n "clap2") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ipi40mv69l0flzi03336lphpa0v1jk0akilbidy16a39fqbn2a3") (y #t)))

