(define-module (crates-io cl ap clap-handler-derive) #:use-module (crates-io))

(define-public crate-clap-handler-derive-0.1.0 (c (n "clap-handler-derive") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bl3nzqk7ziklj4afl4mi3sz3g0ag3hnysdvdsg8snsgn1n4vj3b") (f (quote (("default") ("async"))))))

