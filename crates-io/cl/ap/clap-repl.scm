(define-module (crates-io cl ap clap-repl) #:use-module (crates-io))

(define-public crate-clap-repl-0.1.0 (c (n "clap-repl") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 2)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)))) (h "1hlfj2x14sa1i32cl2rzfbljy3g6b8zqx3x3w1n05bjlf2aq58cr")))

(define-public crate-clap-repl-0.1.1 (c (n "clap-repl") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 2)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "0v5i8rlzaxbbqz081pxs87jk5p81k12s9majvsj41266xlzshbgi")))

(define-public crate-clap-repl-0.1.2 (c (n "clap-repl") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 2)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "0j7fv0lm5224na4l7r39603ihmjng32ns80lzlfph1w4rifpr14f")))

(define-public crate-clap-repl-0.2.0 (c (n "clap-repl") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.2") (f (quote ("unstable-dynamic"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.50.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 2)) (d (n "reedline") (r "^0.32.0") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "18y6z16h4v16z97xcscff2a7kk7cd8n6x3m8q4yhxvpf8wz0h0hg")))

