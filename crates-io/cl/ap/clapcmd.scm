(define-module (crates-io cl ap clapcmd) #:use-module (crates-io))

(define-public crate-clapcmd-0.1.0 (c (n "clapcmd") (v "0.1.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "rustyline") (r "^10") (d #t) (k 0)))) (h "1s5d74pwab8kfzl8xgrw29mqa41mnglwaf2cjkfpb0br07fi6vc8")))

(define-public crate-clapcmd-0.1.1 (c (n "clapcmd") (v "0.1.1") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "rustyline") (r "^10") (d #t) (k 0)))) (h "0wsaics8gfqnkjgqwfim0q5b7i8j7ri1y7wqyg287r1qkqgmgwmv")))

(define-public crate-clapcmd-0.1.1-beta.0 (c (n "clapcmd") (v "0.1.1-beta.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "rustyline") (r "^10") (d #t) (k 0)))) (h "0ql2mz1w3h2s1s9a3p79x62g5bm6nbpc8xqwahcg5iglz5k3lzf8")))

(define-public crate-clapcmd-0.2.0 (c (n "clapcmd") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 2)) (d (n "rustyline") (r "^10") (d #t) (k 0)))) (h "13j6c26z4x3cg3cv54vjmvxi5n6p8gm8fwd6apb2zniddkcrnbnc")))

(define-public crate-clapcmd-0.2.1 (c (n "clapcmd") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "081dcqaaz20xz9y900ki1ng49lwcd9k6dg9692rwnvbbx106yk4l") (f (quote (("test-runner") ("default"))))))

(define-public crate-clapcmd-0.3.0 (c (n "clapcmd") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "1aq4xsj65jy6w2kml2gym7lnyhr50splzvanavkv7k0ix0qa64fb") (f (quote (("test-runner") ("default"))))))

(define-public crate-clapcmd-0.3.1 (c (n "clapcmd") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "0nh1zbqbrzi45lxgrl4dpcfd9x4by6ch4lsri2w0f2yy7kykff7h") (f (quote (("test-runner") ("default"))))))

(define-public crate-clapcmd-0.3.2 (c (n "clapcmd") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "0qisw2mc4b4hzb48if8z29ba3nmysva0q2g2cmblfnfrc6wsyzb1") (f (quote (("test-runner") ("default"))))))

(define-public crate-clapcmd-0.3.3 (c (n "clapcmd") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "1d9z6qc1rj0ar5mmshq8zpz5ckb2rcc5h85i8kdh83h59qrsi9aa") (f (quote (("test-runner") ("default"))))))

