(define-module (crates-io cl ap clap_conf) #:use-module (crates-io))

(define-public crate-clap_conf-0.1.0 (c (n "clap_conf") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0a1x892sz8a6337af42ldhw075yrsnlcqq9711nrvqircm53rhnr")))

(define-public crate-clap_conf-0.1.2 (c (n "clap_conf") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1cqzk98rlqcwa4fk4ab44lr1jd8cnqz299wqr8cijbxcdfdj56a5")))

(define-public crate-clap_conf-0.1.3 (c (n "clap_conf") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1f0w22sm0jwvphqa7a7g7kgi0cxs8cypdjzbinnn0kwhcphy7m8b")))

(define-public crate-clap_conf-0.1.4 (c (n "clap_conf") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0kbnv69rd2say87c2xvcqpphxg9na22nca9zn7c5mna1pfljs8mr")))

(define-public crate-clap_conf-0.1.5 (c (n "clap_conf") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1n29wr6ns660hi63mc30zvs7dhidaycw35am9spzknsal3nrs0sn")))

(define-public crate-clap_conf-0.1.6 (c (n "clap_conf") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1w98xss3pghxj1xy0a5mhidbhn0zq9aq9r86xxcd4waanp932v0a") (y #t)))

(define-public crate-clap_conf-0.2.0 (c (n "clap_conf") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0rwy1vpkcn512npai5iq4l9l8ji5b27qvsw1irijvrmzm8mq8lgl")))

