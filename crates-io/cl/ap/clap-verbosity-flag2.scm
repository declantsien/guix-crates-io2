(define-module (crates-io cl ap clap-verbosity-flag2) #:use-module (crates-io))

(define-public crate-clap-verbosity-flag2-2.1.1 (c (n "clap-verbosity-flag2") (v "2.1.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std" "derive"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("help" "usage"))) (k 2)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1fxlrlb1k225qkansy3jp62nba3pfd63v1b1icnnp0zdbdai0w0y") (r "1.70")))

