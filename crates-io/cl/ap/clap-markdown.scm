(define-module (crates-io cl ap clap-markdown) #:use-module (crates-io))

(define-public crate-clap-markdown-0.0.1 (c (n "clap-markdown") (v "0.0.1") (d (list (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "clap") (r "4.*") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1xim7cbllncmi3xan8yh53mzm8shmv3gm254axhyfk58jgz9dj0k")))

(define-public crate-clap-markdown-0.1.0 (c (n "clap-markdown") (v "0.1.0") (d (list (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "clap") (r "4.*") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1a2l5kdj2hcdjpx1h48vy1mkzpx4ivrj3rz96vksi69hk03b3ia5")))

(define-public crate-clap-markdown-0.1.1 (c (n "clap-markdown") (v "0.1.1") (d (list (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "clap") (r "4.*") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1nypmafa6ajgczfzj4xd5lkcihs05fn3d7bw86xs6z0ldqczy4jy")))

(define-public crate-clap-markdown-0.1.2 (c (n "clap-markdown") (v "0.1.2") (d (list (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "clap") (r "4.*") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1lw4kbc6yzgfrx0b0x0fq5xcggnlc4p5i6kx1myjw5xzdh8qr1hx")))

(define-public crate-clap-markdown-0.1.3 (c (n "clap-markdown") (v "0.1.3") (d (list (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "clap") (r "4.*") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0yg5jx3ifybjsbnfz3i4hd2873bpwqnxdwnrns21g4kniwi50prj")))

