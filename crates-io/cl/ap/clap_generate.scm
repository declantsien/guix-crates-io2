(define-module (crates-io cl ap clap_generate) #:use-module (crates-io))

(define-public crate-clap_generate-3.0.0-beta.1 (c (n "clap_generate") (v "3.0.0-beta.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0rzgrn6ra1j4v20kwwhyy8dwgnsx2v77xwcwaifm28m9gw9qp38q") (f (quote (("unstable" "clap/unstable") ("doc") ("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-beta.2 (c (n "clap_generate") (v "3.0.0-beta.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "13ynl0bqfkllcv5zpqrj0hbplhqccqxd9kswj792idl7nvw21x5d") (f (quote (("unstable" "clap/unstable") ("doc") ("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-beta.3 (c (n "clap_generate") (v "3.0.0-beta.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0md31dzwn8wjxch2lzxbw0iyyflh2q4ya6y0fgsmqgpac8ss62al") (f (quote (("default") ("debug" "clap/debug")))) (y #t)))

(define-public crate-clap_generate-3.0.0-beta.4 (c (n "clap_generate") (v "3.0.0-beta.4") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0hrn3hmm36nwnjav138mnmxpdmlj1953rzrfjn8g4s9mz6z1m6rd") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-beta.5 (c (n "clap_generate") (v "3.0.0-beta.5") (d (list (d (n "clap") (r "=3.0.0-beta.5") (f (quote ("std"))) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "16d7zbyb3nzxkrgmk7g07i6i3hzn77fwhmydf0i485rl3kdvayh9") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.0 (c (n "clap_generate") (v "3.0.0-rc.0") (d (list (d (n "clap") (r "=3.0.0-rc.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=3.0.0-rc.0") (f (quote ("std" "derive"))) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0ka66v9z242i214hz6lsbai6l0563kij77x7yw1vb20rxhxi27ww") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.1 (c (n "clap_generate") (v "3.0.0-rc.1") (d (list (d (n "clap") (r "=3.0.0-rc.1") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=3.0.0-rc.1") (f (quote ("std" "derive"))) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0h5aywvcx9ihxh5702qszbigxivfbf08kxzy0h9b9k4grmrg0kz8") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.3 (c (n "clap_generate") (v "3.0.0-rc.3") (d (list (d (n "clap") (r "=3.0.0-rc.3") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=3.0.0-rc.3") (f (quote ("std" "derive"))) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0zgb47q6x1ai7w40q8vpixl9gy72qsv7v9r54x75b9xhji6qld76") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.4 (c (n "clap_generate") (v "3.0.0-rc.4") (d (list (d (n "clap") (r "=3.0.0-rc.4") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=3.0.0-rc.4") (f (quote ("std" "derive"))) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1gdrc3h3f3dccxrgjy7swvl4wbcsxf2czl46nszybq4wvr3yxwxx") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.5 (c (n "clap_generate") (v "3.0.0-rc.5") (d (list (d (n "clap") (r "=3.0.0-rc.5") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=3.0.0-rc.5") (f (quote ("std" "derive"))) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1apdlbkff55g2qkd9b89bs22gvbf9br0sxl6lf5vc444s3kr27rv") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.6 (c (n "clap_generate") (v "3.0.0-rc.6") (d (list (d (n "clap") (r "=3.0.0-rc.6") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=3.0.0-rc.6") (f (quote ("std" "derive"))) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0p8mjhc248qyfpqbngpka9gns5qldzjrsihll7vqi6xnyf4931xc") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.7 (c (n "clap_generate") (v "3.0.0-rc.7") (d (list (d (n "clap") (r "=3.0.0-rc.7") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=3.0.0-rc.7") (f (quote ("std" "derive"))) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1q3vx9z82gglx3x5z14az2clk0cp39k6iv1apl04nxi0pjy708hz") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.8 (c (n "clap_generate") (v "3.0.0-rc.8") (d (list (d (n "clap") (r "=3.0.0-rc.8") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=3.0.0-rc.8") (f (quote ("std" "derive"))) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1akjzpkbz927y6yhhm4hrc8njx2j9jpdyxgvzb7g747gk7lgkva6") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.9 (c (n "clap_generate") (v "3.0.0-rc.9") (d (list (d (n "clap") (r "=3.0.0-rc.9") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=3.0.0-rc.9") (f (quote ("std" "derive"))) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1xd8vp601ggdfw3b5f8fifmkjdhs2kmyk1mbhnr7r4zbir536pf8") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.10 (c (n "clap_generate") (v "3.0.0-rc.10") (d (list (d (n "clap") (r "=3.0.0-rc.10") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=3.0.0-rc.10") (f (quote ("std" "derive"))) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "053lkq2jm0hv57rw1i2r88w7ck3c64pdzbzn0m1fm150n75ajxqz") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.11 (c (n "clap_generate") (v "3.0.0-rc.11") (d (list (d (n "clap") (r "=3.0.0-rc.11") (f (quote ("std"))) (k 0)) (d (n "clap") (r "=3.0.0-rc.11") (f (quote ("std" "derive"))) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "10ynmki850ia9pj3m89h8a13rr8p86r8k0qrf1xv463mw0j1ipy2") (f (quote (("default") ("debug" "clap/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.12 (c (n "clap_generate") (v "3.0.0-rc.12") (d (list (d (n "clap") (r "=3.0.0-rc.12") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "=3.0.0-rc.12") (d #t) (k 0)))) (h "1klris9qbfrj313zj0m9k3kjh0yi2v0q4489pspxz8z6hvqrh77f") (f (quote (("default") ("debug" "clap_complete/debug"))))))

(define-public crate-clap_generate-3.0.0-rc.13 (c (n "clap_generate") (v "3.0.0-rc.13") (d (list (d (n "clap") (r "=3.0.0-rc.13") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "=3.0.0-rc.13") (d #t) (k 0)))) (h "1mzdni82a3sxy6cdwh3vxnm6ih1yf5gxi6jrm14wm4x8hx5svcc1") (f (quote (("default") ("debug" "clap_complete/debug"))))))

(define-public crate-clap_generate-3.0.0 (c (n "clap_generate") (v "3.0.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.0.0") (d #t) (k 0)))) (h "1kykmni46b1m40h1p3dxhy0h4i1fx9vj2dd3ari6yp5qrnksfq38") (f (quote (("default") ("debug" "clap_complete/debug"))))))

(define-public crate-clap_generate-3.0.1 (c (n "clap_generate") (v "3.0.1") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.0.0") (d #t) (k 0)))) (h "1fm20p2y1vjn7x6adn9cj9grs0xkxapyx1aqa53m3wgjrq9s9wq9") (f (quote (("default") ("debug" "clap_complete/debug"))))))

(define-public crate-clap_generate-3.0.2 (c (n "clap_generate") (v "3.0.2") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.0.0") (d #t) (k 0)))) (h "08djjx76wxh5dyjp3shmkfjd9l4h9z1kg06zihyjinq3kjgji8kr") (f (quote (("default") ("debug" "clap_complete/debug"))))))

(define-public crate-clap_generate-3.0.3 (c (n "clap_generate") (v "3.0.3") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.0.0") (d #t) (k 0)))) (h "1695nh1lri5inq6l3wqsqr6sgbiaqv56gljgc0l3db02m322h6wf") (f (quote (("default") ("debug" "clap_complete/debug"))))))

