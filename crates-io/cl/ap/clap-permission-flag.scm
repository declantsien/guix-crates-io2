(define-module (crates-io cl ap clap-permission-flag) #:use-module (crates-io))

(define-public crate-clap-permission-flag-0.1.0 (c (n "clap-permission-flag") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "privdrop") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0qnfssxm8mx1xqkbagxs3gwyv147n4nsajr7mb5a5jv83anh4f04")))

(define-public crate-clap-permission-flag-0.2.0 (c (n "clap-permission-flag") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "privdrop") (r "^0.3.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1pccd5vr57i33avp3ymjmlb2jjxc49d5qfh9bps4vl82c41wfxcs") (f (quote (("chroot"))))))

(define-public crate-clap-permission-flag-0.3.0 (c (n "clap-permission-flag") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "privdrop") (r "^0.5.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0py0bp3mms7h08dsfc5k7184wa7jlpdp29biids9zjn3w95x7gxv") (f (quote (("chroot"))))))

