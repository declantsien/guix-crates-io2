(define-module (crates-io cl ap clappos) #:use-module (crates-io))

(define-public crate-clappos-0.1.0 (c (n "clappos") (v "0.1.0") (h "07il4nbr8rxgq5h2pcjgj77kgirblyj8vjr3qa7zikqqrybghhv3")))

(define-public crate-clappos-0.2.0 (c (n "clappos") (v "0.2.0") (h "03j0jvd58m1i4q44nsgl2v48y996sr10wf7r9ji26cv220n6b7j2")))

(define-public crate-clappos-0.3.0 (c (n "clappos") (v "0.3.0") (h "0w0hy0v3sxm4s85wb59xjdcl9dyvnvfabi4azgj4zxca0nv16g73")))

(define-public crate-clappos-0.3.1 (c (n "clappos") (v "0.3.1") (h "1h3czd1jhjsr8vi9ayv5mcafsnnk8msqzy5xdk5k2l49j84zg9fm")))

(define-public crate-clappos-0.3.2 (c (n "clappos") (v "0.3.2") (h "1glyg878br73731bab61pdqrs1qzznqbfrqrxl88xjsiay1n670z")))

(define-public crate-clappos-0.3.3 (c (n "clappos") (v "0.3.3") (h "10qrq2d5dhyi26s7dvg24j67f4yy7l38is5mb7w8c1dq0kp9pp15")))

(define-public crate-clappos-0.3.4 (c (n "clappos") (v "0.3.4") (h "1y5qfa00ag5fn99mgcbqjlfwxpp2ghbpbznmfqkhfw953qyld80w")))

(define-public crate-clappos-0.3.5 (c (n "clappos") (v "0.3.5") (h "14qmgzdjsgp7l4hmiddcb1q2cn48xd6la7zbiqkxzj7m0zkzvc66")))

(define-public crate-clappos-0.3.7 (c (n "clappos") (v "0.3.7") (h "041n2fl9xivzckcgs6nm4c34zh356cik980196xbgwfr6m3by0b9")))

(define-public crate-clappos-0.3.8 (c (n "clappos") (v "0.3.8") (h "0khqw19qhvp76xvdxvc54jalnsz8ighw72gdir71ppcz47szybm8")))

(define-public crate-clappos-0.3.9 (c (n "clappos") (v "0.3.9") (h "14vk8bl09rcvrnpvn32hf6rw51gp83968rmkzfd0knlphyns9qph")))

(define-public crate-clappos-0.3.10 (c (n "clappos") (v "0.3.10") (h "0kmw4zf34ndmfly95hqwzksmi8f6808i9dncaq59k1vgl3hyczpn")))

