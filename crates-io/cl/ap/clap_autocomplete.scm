(define-module (crates-io cl ap clap_autocomplete) #:use-module (crates-io))

(define-public crate-clap_autocomplete-0.1.0 (c (n "clap_autocomplete") (v "0.1.0") (d (list (d (n "clap") (r ">=3.1, <4") (d #t) (k 0)) (d (n "clap_complete") (r ">=3.1, <4") (d #t) (k 0)) (d (n "get-shell") (r "^0.1") (d #t) (k 0)) (d (n "xdg") (r "^2.4") (d #t) (k 0)))) (h "168jjfji6nym9h8vdy6yhzqav3dxn57ssqy1i71lknnxmly18694")))

(define-public crate-clap_autocomplete-0.1.1 (c (n "clap_autocomplete") (v "0.1.1") (d (list (d (n "clap") (r ">=3.1, <4") (d #t) (k 0)) (d (n "clap") (r ">=3.1, <4") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "clap_complete") (r ">=3.1, <4") (d #t) (k 0)) (d (n "get-shell") (r "^0.1") (d #t) (k 0)) (d (n "xdg") (r "^2.4") (d #t) (k 0)))) (h "157hg2lmhfmb98k5vgvnbqril4m9h4dd5k1mj5czzjy36bk0yc2k")))

(define-public crate-clap_autocomplete-0.2.0 (c (n "clap_autocomplete") (v "0.2.0") (d (list (d (n "clap") (r ">=3.1, <4") (k 0)) (d (n "clap") (r ">=3.1, <4") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "clap_complete") (r ">=3.1, <4") (d #t) (k 0)) (d (n "query-shell") (r "^0.1") (d #t) (k 0)) (d (n "xdg") (r "^2.4") (d #t) (k 0)))) (h "0xwzln9fx72dxw2za9kchvkdd50z9pfypy0ncqb4sirn66c1a2nl")))

(define-public crate-clap_autocomplete-0.2.1 (c (n "clap_autocomplete") (v "0.2.1") (d (list (d (n "clap") (r ">=3.1, <4") (k 0)) (d (n "clap") (r ">=3.1, <4") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "clap_complete") (r ">=3.1, <4") (d #t) (k 0)) (d (n "query-shell") (r "^0.2") (d #t) (k 0)) (d (n "xdg") (r "^2.4") (d #t) (k 0)))) (h "0k1j8zqiafl9p5srpwsjw06c0lj4v3ssn5b9d9bb3lxlsb6p7x7s")))

(define-public crate-clap_autocomplete-0.3.0 (c (n "clap_autocomplete") (v "0.3.0") (d (list (d (n "clap") (r "^4") (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "clap_complete") (r "^4") (d #t) (k 0)) (d (n "query-shell") (r "^0.2") (d #t) (k 0)) (d (n "xdg") (r "^2.4") (d #t) (k 0)))) (h "1xzc6wmg2czwx95vj6jggfyl42dz8b2rv8y6fqzhcrbznjwy7dgi")))

(define-public crate-clap_autocomplete-0.4.0 (c (n "clap_autocomplete") (v "0.4.0") (d (list (d (n "clap") (r "^4") (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "clap_complete") (r "^4") (d #t) (k 0)) (d (n "query-shell") (r "^0.2") (d #t) (k 0)) (d (n "xdg") (r "^2.4") (d #t) (k 0)))) (h "15nph7c7p9gkspggfvp65lz6ih7js08dzfshwv85n303hp9micaf")))

(define-public crate-clap_autocomplete-0.4.1 (c (n "clap_autocomplete") (v "0.4.1") (d (list (d (n "clap") (r "^4") (k 0)) (d (n "clap_complete") (r "^4") (d #t) (k 0)) (d (n "query-shell") (r "^0.3") (d #t) (k 0)) (d (n "xdg") (r "^2.4") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 2)))) (h "1dq81qrhinzllhqgydkpkrrshjhkzsnf8zg04ijbabl231zpdfx8")))

