(define-module (crates-io cl ap clap_complete_fig) #:use-module (crates-io))

(define-public crate-clap_complete_fig-3.0.0-rc.12 (c (n "clap_complete_fig") (v "3.0.0-rc.12") (d (list (d (n "clap") (r "=3.0.0-rc.12") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "=3.0.0-rc.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0bfxrrlamr3s83clzgk8y7y764894812zdsmd696r08dwsdv88xp")))

(define-public crate-clap_complete_fig-3.0.0-rc.13 (c (n "clap_complete_fig") (v "3.0.0-rc.13") (d (list (d (n "clap") (r "=3.0.0-rc.13") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "=3.0.0-rc.13") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1cr1cxxd6w4xzln399zzfg2kchxxyzi2gzqfzazpsfgcnq84b2wn")))

(define-public crate-clap_complete_fig-3.0.0 (c (n "clap_complete_fig") (v "3.0.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1kdfypsmpb3w8bm9pnch68x50sn8550id8pri86gqq1snx71j6x1")))

(define-public crate-clap_complete_fig-3.0.1 (c (n "clap_complete_fig") (v "3.0.1") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0i96l5p2ljp2wilhwwwnm2f2fzlpyqf2aidjn33r8f5vkf6isn9j")))

(define-public crate-clap_complete_fig-3.0.2 (c (n "clap_complete_fig") (v "3.0.2") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "182h1pc4b962b9ckjwybm5s3vlv6isalmaplf80d2w27h8yh1k19")))

(define-public crate-clap_complete_fig-3.1.0 (c (n "clap_complete_fig") (v "3.1.0") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "03lg145ddvcnwyxzn93jr66sr24whybm4ymm3giax97y9bsgc4aw")))

(define-public crate-clap_complete_fig-3.1.1 (c (n "clap_complete_fig") (v "3.1.1") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.1.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.2") (f (quote ("diff"))) (d #t) (k 2)))) (h "1q8yh5n2ay8javkdlba31irz2dlvgmn8619mijrml9v01iv67ymd")))

(define-public crate-clap_complete_fig-3.1.2 (c (n "clap_complete_fig") (v "3.1.2") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.1.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.2") (f (quote ("diff"))) (d #t) (k 2)))) (h "0839ywkh1aw3br7qmfg1bv0n3kk4i3waq2n8ziwyhkr5m2d514fd")))

(define-public crate-clap_complete_fig-3.1.3 (c (n "clap_complete_fig") (v "3.1.3") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.1.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.2") (f (quote ("diff"))) (d #t) (k 2)))) (h "1dc14lw28s73d5b0ykwj5zv4i8hf85fgqmi6b2n049v4jcxc4qs5")))

(define-public crate-clap_complete_fig-3.1.4 (c (n "clap_complete_fig") (v "3.1.4") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.1.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.2") (f (quote ("diff"))) (d #t) (k 2)))) (h "17ihfspn2qd8s3k8ccfs2gmhqzmgjnpvwa1h9akg33d9nymva3k9")))

(define-public crate-clap_complete_fig-3.1.5 (c (n "clap_complete_fig") (v "3.1.5") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.1.2") (d #t) (k 0)) (d (n "snapbox") (r "^0.2") (f (quote ("diff"))) (d #t) (k 2)))) (h "01a57qjvvf9n14af0xb6ym23s49fmlmlpp52amhandrw4c7fs61r")))

(define-public crate-clap_complete_fig-3.2.0 (c (n "clap_complete_fig") (v "3.2.0") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.1.2") (d #t) (k 0)) (d (n "snapbox") (r "^0.2") (f (quote ("diff"))) (d #t) (k 2)))) (h "1jb9czl2pk8waf4qzarcxbd7x0nv0agk6knzbzq04hc8l7lk3mji") (r "1.56.0")))

(define-public crate-clap_complete_fig-3.2.1 (c (n "clap_complete_fig") (v "3.2.1") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.2.1") (d #t) (k 0)) (d (n "snapbox") (r "^0.2") (f (quote ("diff"))) (d #t) (k 2)))) (h "1jbh33vvjx0vh8h0pspja8j7yyfxf3k347kf2bvl1zqpcfn9ws8v") (r "1.56.0")))

(define-public crate-clap_complete_fig-3.2.2 (c (n "clap_complete_fig") (v "3.2.2") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.2.1") (d #t) (k 0)) (d (n "snapbox") (r "^0.2") (f (quote ("diff"))) (d #t) (k 2)))) (h "09jpa9mqxi647fp83zrgfyxxi2p2ldxscc3wgjbknynppsadzzdl") (r "1.56.1")))

(define-public crate-clap_complete_fig-3.2.3 (c (n "clap_complete_fig") (v "3.2.3") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.2.1") (d #t) (k 0)) (d (n "snapbox") (r "^0.2") (f (quote ("diff"))) (d #t) (k 2)))) (h "00igffrg69dfz68pw23rf3q20yxzql2kx22snh0l7m2g7kxmarfm") (r "1.56.1")))

(define-public crate-clap_complete_fig-3.2.4 (c (n "clap_complete_fig") (v "3.2.4") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("std"))) (k 0)) (d (n "clap_complete") (r "^3.2.1") (d #t) (k 0)) (d (n "snapbox") (r "^0.2") (f (quote ("diff"))) (d #t) (k 2)))) (h "1fb4965w8wyrcwq35ywgx4mzfsv2cqba73mdlvmp6ii1q70b8dzd") (r "1.56.1")))

(define-public crate-clap_complete_fig-4.0.0-rc.1 (c (n "clap_complete_fig") (v "4.0.0-rc.1") (d (list (d (n "clap") (r "^4.0.0-alpha.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0-alpha.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0-alpha.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.3") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ip2xdr5n6hk3zksiw1a7fzihd4mg4fjxdixx3q41wyhqd4rxdpl") (r "1.60.0")))

(define-public crate-clap_complete_fig-4.0.0 (c (n "clap_complete_fig") (v "4.0.0") (d (list (d (n "clap") (r "^4.0.0-alpha.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0-alpha.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0-alpha.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.3") (f (quote ("diff"))) (d #t) (k 2)))) (h "0p1kfnx59v8y4n0qlkm8233iklgln8ihki4nkg2z7yl5lmdc2byg") (r "1.60.0")))

(define-public crate-clap_complete_fig-4.0.1 (c (n "clap_complete_fig") (v "4.0.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4") (f (quote ("diff"))) (d #t) (k 2)))) (h "0i3dyjkd0zllz6679jhzwxr6rdg8hgkyd2gmr7pkg9w4f6y1lvdk") (r "1.60.0")))

(define-public crate-clap_complete_fig-4.0.2 (c (n "clap_complete_fig") (v "4.0.2") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4") (f (quote ("diff"))) (d #t) (k 2)))) (h "1hynxz08qfnl91dxd858s847qszzdki3nkq0wlhx17361q0hxcs6") (r "1.60.0")))

(define-public crate-clap_complete_fig-4.1.0 (c (n "clap_complete_fig") (v "4.1.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4") (f (quote ("diff"))) (d #t) (k 2)))) (h "0p5vx4wbcv5rvgnnvkf117bgvrs6hb511kfg5h8a30ppzkc7c36g") (r "1.64.0")))

(define-public crate-clap_complete_fig-4.1.1 (c (n "clap_complete_fig") (v "4.1.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4") (f (quote ("diff"))) (d #t) (k 2)))) (h "1l62qm626hpq80qfwg4z6yx009njkx0b91s8hs7hifrdlxc63833") (r "1.64.0")))

(define-public crate-clap_complete_fig-4.1.2 (c (n "clap_complete_fig") (v "4.1.2") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.10") (f (quote ("diff"))) (d #t) (k 2)))) (h "0m0sz48cw507a0p245dp6bc5p6s4fjah7zrb880ihymd89ibqw91") (r "1.64.0")))

(define-public crate-clap_complete_fig-4.2.0 (c (n "clap_complete_fig") (v "4.2.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.10") (f (quote ("diff"))) (d #t) (k 2)))) (h "1yj1ib7j4bi9cwdy4bf68pa1adw53d3x7m4f8am9p61hcfajibzk") (r "1.64.0")))

(define-public crate-clap_complete_fig-4.3.0 (c (n "clap_complete_fig") (v "4.3.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.10") (f (quote ("diff"))) (d #t) (k 2)))) (h "0jrkajsqy4fzqvbx0zi30ps0ygwlfgrkq0lxqpn9idn3xsdz2wqc") (r "1.64.0")))

(define-public crate-clap_complete_fig-4.3.1 (c (n "clap_complete_fig") (v "4.3.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.11") (f (quote ("diff"))) (d #t) (k 2)))) (h "17yfrdxy555fv56y0n69r4kgd2iwpq4mgz6k5rn5lc2i1b9y3zlr") (r "1.64.0")))

(define-public crate-clap_complete_fig-4.4.0 (c (n "clap_complete_fig") (v "4.4.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.11") (f (quote ("diff"))) (d #t) (k 2)))) (h "0rdrlszyzfkinaqpjjdsdq8l41dajh88nk05sdx43sznnchsx6wy") (r "1.70.0")))

(define-public crate-clap_complete_fig-4.4.1 (c (n "clap_complete_fig") (v "4.4.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.12") (f (quote ("diff"))) (d #t) (k 2)))) (h "1wfjwzj4jhdwfcad97b6qy4db88n8imcisnb7zw2idk3l8hvxg99") (r "1.70.0")))

(define-public crate-clap_complete_fig-4.4.2 (c (n "clap_complete_fig") (v "4.4.2") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.13") (f (quote ("diff"))) (d #t) (k 2)))) (h "0z3kw5fzr57hddj6flcj1lla4dc06341flqw9v9r3v121vbp3rc7") (r "1.70.0")))

(define-public crate-clap_complete_fig-4.5.0 (c (n "clap_complete_fig") (v "4.5.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("std" "help"))) (k 2)) (d (n "clap_complete") (r "^4.0.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.16") (f (quote ("diff"))) (d #t) (k 2)))) (h "040ilrkbik3d01f36307pnm4v2wkbnfx6mrxmhyd5ggsj5gydcsl") (r "1.74")))

