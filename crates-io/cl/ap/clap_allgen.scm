(define-module (crates-io cl ap clap_allgen) #:use-module (crates-io))

(define-public crate-clap_allgen-0.1.0 (c (n "clap_allgen") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("string"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap_complete") (r "^4.4.6") (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.16") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)) (d (n "testresult") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0rb60xzja6wwknf5prj5afwznja817i8xlfnv797gc3zf9vqpcy5")))

(define-public crate-clap_allgen-0.1.1 (c (n "clap_allgen") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("string"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap_complete") (r "^4.4.6") (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.16") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)) (d (n "testresult") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1cx6bng3ydbkq5dwzcqxrl613x6g4ms04fhkmglzmnf8py19j6ja")))

