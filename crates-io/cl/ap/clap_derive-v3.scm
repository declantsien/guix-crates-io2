(define-module (crates-io cl ap clap_derive-v3) #:use-module (crates-io))

(define-public crate-clap_derive-v3-3.0.0-beta.1 (c (n "clap_derive-v3") (v "3.0.0-beta.1") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1490gqq8l6aiik89785h0dsvryl5kq93sn6jgdw3agpbcxangpf6") (f (quote (("unstable") ("nightly") ("doc") ("default") ("debug"))))))

