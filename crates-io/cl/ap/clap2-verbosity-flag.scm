(define-module (crates-io cl ap clap2-verbosity-flag) #:use-module (crates-io))

(define-public crate-clap2-verbosity-flag-2.0.0 (c (n "clap2-verbosity-flag") (v "2.0.0") (d (list (d (n "clap2") (r "^4.0.0") (f (quote ("std" "derive"))) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "006nb0r56xb4mcrjy1qm9qray8n35zf5ckyx2sp352plrfkwx1wg") (y #t) (r "1.60.0")))

