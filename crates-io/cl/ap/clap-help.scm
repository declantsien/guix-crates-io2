(define-module (crates-io cl ap clap-help) #:use-module (crates-io))

(define-public crate-clap-help-0.1.0 (c (n "clap-help") (v "0.1.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.23.1") (d #t) (k 0)) (d (n "terminal-light") (r "^1.1.1") (d #t) (k 0)))) (h "0qbpdbqnf7zhgc93xna69419cnqabi1qrlpd5yi25286cj7riv24") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-0.2.0 (c (n "clap-help") (v "0.2.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.23.1") (d #t) (k 0)) (d (n "terminal-light") (r "^1.1.1") (d #t) (k 0)))) (h "0x6ibpy2lk64ia6qx5zgywx1a2wvgbyasa2yw7ghbn579r4s6qhg") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-0.3.0 (c (n "clap-help") (v "0.3.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.23.1") (d #t) (k 0)) (d (n "terminal-light") (r "^1.1.1") (d #t) (k 0)))) (h "04fczacrvza1db998fg2441bwmbcq0fb1zf3gcwn6yiimfiypczl") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-0.4.0 (c (n "clap-help") (v "0.4.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.23.1") (d #t) (k 0)) (d (n "terminal-light") (r "^1.1.1") (d #t) (k 0)))) (h "0kymqmcpdcy9yp1cda1kvir1zng0ia99si1021xp6h6psh8hs2bl") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-0.5.0 (c (n "clap-help") (v "0.5.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.23.1") (d #t) (k 0)) (d (n "terminal-light") (r "^1.1.1") (d #t) (k 0)))) (h "1dp0mmdr584ymc3cl5w31255d0q39pg8gg3bw19049jfvn8fxp3m") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-0.5.1 (c (n "clap-help") (v "0.5.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.23.1") (d #t) (k 0)) (d (n "terminal-light") (r "^1.1.1") (d #t) (k 0)))) (h "05d41y0y3gv3hf4x3626vm30hkj7lahf7h9c4a8dxs83lz3i4cn9") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-0.6.0 (c (n "clap-help") (v "0.6.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.23.2") (d #t) (k 0)) (d (n "terminal-light") (r "^1.1.1") (d #t) (k 0)))) (h "02nibrq9m96rd2hhsl8xxdm9ix6ngybxqdbzk8cw87rfjxsqlwd0") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-0.6.1 (c (n "clap-help") (v "0.6.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.23.2") (d #t) (k 0)) (d (n "terminal-light") (r "^1.1.1") (d #t) (k 0)))) (h "06dz9pdr3k16a2p6zss1prs7a5r0n3lypr2szh8cfm2y7f6lpqyx") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-0.6.2 (c (n "clap-help") (v "0.6.2") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.23.2") (d #t) (k 0)) (d (n "terminal-light") (r "^1.1.1") (d #t) (k 0)))) (h "134h1pzwvzrqfdh5ld19yyydsdrprr4aqy3l6gw58xxhghlbs1jg") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-0.7.0 (c (n "clap-help") (v "0.7.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.25.0") (d #t) (k 0)) (d (n "terminal-light") (r "^1.1.1") (d #t) (k 0)))) (h "1cx9p3xhzym9g8lxwbiy0q23ly2rz0d1fyf74hlq8mnd476k282l") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-1.0.0 (c (n "clap-help") (v "1.0.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.25.1") (d #t) (k 0)) (d (n "terminal-light") (r "^1.1.1") (d #t) (k 0)))) (h "1k0q2kh70h3bybjyyaqnskmcyn2jdj3krvrp6yjlpaaw0pz0hfjb") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-1.1.0 (c (n "clap-help") (v "1.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.28") (d #t) (k 0)) (d (n "terminal-light") (r "^1.2") (d #t) (k 0)))) (h "0palwflfl7ajjpa4v5zybnrbp07zqqrzsfq256bgfy2lqdgnijmm") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-1.1.1 (c (n "clap-help") (v "1.1.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.29") (d #t) (k 0)) (d (n "terminal-light") (r "^1.2") (d #t) (k 0)))) (h "0zf7x34wrlavrmad6hkkjlj36j5lfb6g90d1zglwf5bs086zgzk7") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-1.1.2 (c (n "clap-help") (v "1.1.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.29") (d #t) (k 0)) (d (n "terminal-light") (r "^1.2") (d #t) (k 0)))) (h "10d8q7ngibvn6rycr30a77w2ph3ps5fp7bagjzfi321pnmimp47r") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-1.1.3 (c (n "clap-help") (v "1.1.3") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.29") (d #t) (k 0)) (d (n "terminal-light") (r "^1.2") (d #t) (k 0)))) (h "1wahabdqadbmnjbhj671yp67wl1riav1x2s9xq5xxd7r3hcqsg0q") (f (quote (("default")))) (r "1.65")))

(define-public crate-clap-help-1.2.0 (c (n "clap-help") (v "1.2.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "termimad") (r "^0.29") (d #t) (k 0)) (d (n "terminal-light") (r "^1.2") (d #t) (k 0)))) (h "0g939jxldiq8cz3drpi3540bmzd1avxam14598s0ab64dzh0h4ss") (f (quote (("default")))) (r "1.65")))

