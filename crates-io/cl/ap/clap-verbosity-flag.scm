(define-module (crates-io cl ap clap-verbosity-flag) #:use-module (crates-io))

(define-public crate-clap-verbosity-flag-0.1.0 (c (n "clap-verbosity-flag") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)))) (h "1ixsync93iz5lwgkz8bsrypk1vgxlr4ds3k8k5fb1xi19a5impi6")))

(define-public crate-clap-verbosity-flag-0.2.0 (c (n "clap-verbosity-flag") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)))) (h "1p3nsjdf758nl5bjjz5wi107i43pmywbgi8857slgdxj4d9lz8dx")))

(define-public crate-clap-verbosity-flag-0.3.0-alpha.0 (c (n "clap-verbosity-flag") (v "0.3.0-alpha.0") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1rhl0vfdzymgb7ykx7171qniq5hsb6jdpvqmkj1mw733xak6fhql")))

(define-public crate-clap-verbosity-flag-0.3.0 (c (n "clap-verbosity-flag") (v "0.3.0") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0qvz7p5wxi5z73w0hs9cakxvsadnmay6fz84g481dpnrmll9lzhp")))

(define-public crate-clap-verbosity-flag-0.3.1 (c (n "clap-verbosity-flag") (v "0.3.1") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0z84jzhg3kxmb1q5p00gbyf4vjxvcazfmzwkhir7r4l9jgw7mmx7")))

(define-public crate-clap-verbosity-flag-0.3.2 (c (n "clap-verbosity-flag") (v "0.3.2") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1hsls0qhj8nk1b16bvj6ial19v5bgyjm2sry0nzimkpv97bls8w2")))

(define-public crate-clap-verbosity-flag-0.4.0 (c (n "clap-verbosity-flag") (v "0.4.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("std" "derive"))) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "1cr9ivgkmzwqdgzb7wn5w1y9b0k2yk0npkspb90hzg3s3amq24pv")))

(define-public crate-clap-verbosity-flag-0.4.1 (c (n "clap-verbosity-flag") (v "0.4.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("std" "derive"))) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "054syvi5i6xqg3yily45v7vvljhfz3rfx0lh9y2f8w1kxx2l8iy2")))

(define-public crate-clap-verbosity-flag-1.0.0 (c (n "clap-verbosity-flag") (v "1.0.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("std" "derive"))) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "1m2igihxv7fs01l1b6pg4sh76y52784n7rrxmwyx1k931wjkgr0p")))

(define-public crate-clap-verbosity-flag-1.0.1 (c (n "clap-verbosity-flag") (v "1.0.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("std" "derive"))) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "0b6402ng8fcc5f0ir61xif1c58f1rvw0b8sm2lb8wbq8830gjdh6") (r "1.56.0")))

(define-public crate-clap-verbosity-flag-2.0.0 (c (n "clap-verbosity-flag") (v "2.0.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std" "derive"))) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0cgj5arb37i68bkhjb7dndhgbqhlvaab41df92gjjwyvvk1vdqi3") (r "1.60.0")))

(define-public crate-clap-verbosity-flag-2.0.1 (c (n "clap-verbosity-flag") (v "2.0.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std" "derive"))) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1pp7zrchy9cgn0jz03v09jhp7n7sww9lcfqwxgr3sl89j1v0bvqy") (r "1.60.0")))

(define-public crate-clap-verbosity-flag-2.1.0 (c (n "clap-verbosity-flag") (v "2.1.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std" "derive"))) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "06f0myl6chqvyf9dpv3ydblqp8sjrkwwm0nai8vzn33rbl0vpzg5") (r "1.70")))

(define-public crate-clap-verbosity-flag-2.1.1 (c (n "clap-verbosity-flag") (v "2.1.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std" "derive"))) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("help" "usage"))) (k 2)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0rwp3aacz3s4zlqrkiwhx83fvy66n1scfdvgz8sars6lbdgfk41w") (r "1.70")))

(define-public crate-clap-verbosity-flag-2.1.2 (c (n "clap-verbosity-flag") (v "2.1.2") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std" "derive"))) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("help" "usage"))) (k 2)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "15f419hg1xskf7pwzk5n27cpn05n0ccbd6sbji906ymi47576zxm") (r "1.70")))

(define-public crate-clap-verbosity-flag-2.2.0 (c (n "clap-verbosity-flag") (v "2.2.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std" "derive"))) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("help" "usage"))) (k 2)) (d (n "env_logger") (r "^0.11.1") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0y54kb14nr2vddl5j5h1s4217hbnxfxh7ln8j7lw5r2qvp0216xv") (r "1.73")))

