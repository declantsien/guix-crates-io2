(define-module (crates-io cl ap clap_lex) #:use-module (crates-io))

(define-public crate-clap_lex-0.1.0 (c (n "clap_lex") (v "0.1.0") (d (list (d (n "os_str_bytes") (r "^6.0") (d #t) (k 0)))) (h "1y1dhjp5g79qmxmy1hkvkvdz7ybln82xbrp63nrb28k2qavys5np")))

(define-public crate-clap_lex-0.1.1 (c (n "clap_lex") (v "0.1.1") (d (list (d (n "os_str_bytes") (r "^6.0") (f (quote ("raw_os_str"))) (k 0)))) (h "0sb6jq722h33vrnyfa4iv6q3gaa22wvm8q38wwshp9rjblxxv78q")))

(define-public crate-clap_lex-0.2.0 (c (n "clap_lex") (v "0.2.0") (d (list (d (n "os_str_bytes") (r "^6.0") (f (quote ("raw_os_str"))) (k 0)))) (h "04wjgd1d3rxsng70rczfzhc7lj87hmwzznhs1dp5xb9d27qkaz53")))

(define-public crate-clap_lex-0.2.1 (c (n "clap_lex") (v "0.2.1") (d (list (d (n "os_str_bytes") (r "^6.0") (f (quote ("raw_os_str"))) (k 0)))) (h "0l724szbk5fqi5c517wamg0nbkbqx5l0ll4w6glkaqlr0spqqmxi") (r "1.56.0")))

(define-public crate-clap_lex-0.2.2 (c (n "clap_lex") (v "0.2.2") (d (list (d (n "os_str_bytes") (r "^6.0") (f (quote ("raw_os_str"))) (k 0)))) (h "04rn568mpbc4smk1wlsh9ky4z12b53rgiv6g6i1fpssh0ikcsf2m") (r "1.56.0")))

(define-public crate-clap_lex-0.2.3 (c (n "clap_lex") (v "0.2.3") (d (list (d (n "os_str_bytes") (r "^6.0") (f (quote ("raw_os_str"))) (k 0)))) (h "1r29wvfs992z7kk1x1jq7v6mhiqgdm0wg7v5dizz2bplqz4a7sw7") (r "1.56.1")))

(define-public crate-clap_lex-0.2.4 (c (n "clap_lex") (v "0.2.4") (d (list (d (n "os_str_bytes") (r "^6.0") (f (quote ("raw_os_str"))) (k 0)))) (h "1ib1a9v55ybnaws11l63az0jgz5xiy24jkdgsmyl7grcm3sz4l18") (r "1.56.1")))

(define-public crate-clap_lex-0.3.0 (c (n "clap_lex") (v "0.3.0") (d (list (d (n "os_str_bytes") (r "^6.0") (f (quote ("raw_os_str"))) (k 0)))) (h "1a4dzbnlxiamfsn0pnkhn7n9bdfjh66j9fxm6mmr7d227vvrhh8d") (r "1.60.0")))

(define-public crate-clap_lex-0.3.1 (c (n "clap_lex") (v "0.3.1") (d (list (d (n "os_str_bytes") (r "^6.0") (f (quote ("raw_os_str"))) (k 0)))) (h "1plaapz3l4lm1clwf2sycb94qbb8g5nv40b2yn84z87wmlrf4gvq") (r "1.64.0")))

(define-public crate-clap_lex-0.3.2 (c (n "clap_lex") (v "0.3.2") (d (list (d (n "os_str_bytes") (r "^6.0") (f (quote ("raw_os_str"))) (k 0)))) (h "028df49gsx92anhdjwcxpvgspvjivhm9p7i2k5rrby9i2zrrq2rm") (r "1.64.0")))

(define-public crate-clap_lex-0.3.3 (c (n "clap_lex") (v "0.3.3") (d (list (d (n "os_str_bytes") (r "^6.0.0") (f (quote ("raw_os_str"))) (k 0)))) (h "0ip688faib67iqqy96i0qss77virr42sib1afj63a7yb99x6ngq3") (r "1.64.0")))

(define-public crate-clap_lex-0.4.0 (c (n "clap_lex") (v "0.4.0") (h "1xqxkkgifn6y1dgypf3xdarcihaqk3n4y0gcwkrq6k34dzxhf22g") (r "1.64.0")))

(define-public crate-clap_lex-0.4.1 (c (n "clap_lex") (v "0.4.1") (h "18dyxyc0g5xrazj8k6mdjd8v3fvka8z3b9k8yl13avlczskdabca") (r "1.64.0")))

(define-public crate-clap_lex-0.5.0 (c (n "clap_lex") (v "0.5.0") (h "06vcvpvp65qggc5agbirzqk2di00gxg6vazzc3qlwzkw70qxm9id") (r "1.64.0")))

(define-public crate-clap_lex-0.5.1 (c (n "clap_lex") (v "0.5.1") (h "0qgrlq509vr49wq91jh50f9pm5f8lxmv1rcbklxnsg4nprxcaz6d") (r "1.70.0")))

(define-public crate-clap_lex-0.6.0 (c (n "clap_lex") (v "0.6.0") (h "1l8bragdvim7mva9flvd159dskn2bdkpl0jqrr41wnjfn8pcfbvh") (r "1.70.0")))

(define-public crate-clap_lex-0.7.0 (c (n "clap_lex") (v "0.7.0") (h "1kh1sckgq71kay2rrr149pl9gbsrvyccsq6xm5xpnq0cxnyqzk4q") (r "1.74")))

