(define-module (crates-io cl ap clap-nested-commands) #:use-module (crates-io))

(define-public crate-clap-nested-commands-0.0.1 (c (n "clap-nested-commands") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hmg19pdc5qka6fpwdiq6dz8qan448mdjfcpm8qqp65avcj58vc4")))

