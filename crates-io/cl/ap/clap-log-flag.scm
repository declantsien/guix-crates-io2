(define-module (crates-io cl ap clap-log-flag) #:use-module (crates-io))

(define-public crate-clap-log-flag-0.1.0 (c (n "clap-log-flag") (v "0.1.0") (d (list (d (n "clap-verbosity-flag") (r "^0.2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "19cjb0w5nim62p0sanj3hsciir7a57l52g3p43wc1nda70qj21cv")))

(define-public crate-clap-log-flag-0.2.0 (c (n "clap-log-flag") (v "0.2.0") (d (list (d (n "clap-verbosity-flag") (r "^0.2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "19v8jl0rkrf39ravnxndijwkrhnpbfrsn4blvg0rr4g3npsymx13")))

(define-public crate-clap-log-flag-0.2.1 (c (n "clap-log-flag") (v "0.2.1") (d (list (d (n "clap-verbosity-flag") (r "^0.2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1fngi1qymyxmyp1zjz63h0rpp1f1j6crya7c5226g159xabk8plk")))

