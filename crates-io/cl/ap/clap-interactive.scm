(define-module (crates-io cl ap clap-interactive) #:use-module (crates-io))

(define-public crate-clap-interactive-0.1.0 (c (n "clap-interactive") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.17") (d #t) (k 0)) (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.4.0") (d #t) (k 0)))) (h "1hazmw5a92kqrqmwkq1izw4i53qzi2b4xfy9529rz1ng0zx3vsg1")))

(define-public crate-clap-interactive-0.1.1 (c (n "clap-interactive") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.4.0") (d #t) (k 0)))) (h "1gaj0qxsjjni2cxmq8vy0byj2k7zsbb89cm5n7fp11i94fkabbjz")))

(define-public crate-clap-interactive-0.1.2 (c (n "clap-interactive") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.4.0") (d #t) (k 0)))) (h "0i4535rdpq4dbsm7sr3hnh9hhmfhcq98bkxpz7kz9vf5kgkzcfws")))

(define-public crate-clap-interactive-0.1.3 (c (n "clap-interactive") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.4.0") (d #t) (k 0)))) (h "10lr4c6w999kl0gs6hncir90iqm2q3w9ldjf894cmsn1x1m6vc24")))

(define-public crate-clap-interactive-0.1.4 (c (n "clap-interactive") (v "0.1.4") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.4.0") (d #t) (k 0)))) (h "0czkxbvvy91aziv2bsr522q70zqws2a2cs1rvj8gi11ki3r5x6kd")))

(define-public crate-clap-interactive-0.1.5 (c (n "clap-interactive") (v "0.1.5") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.4.0") (d #t) (k 0)))) (h "1634zwl5ma6vs5jn7p3m9bh35w03i9fdp9wglxaw7cmrksns8dqg")))

(define-public crate-clap-interactive-0.1.6 (c (n "clap-interactive") (v "0.1.6") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.4.0") (d #t) (k 0)))) (h "0maxsd2s8ffnf6vgw23b15v083rm3svyx35j2rng1i2jhan6b5ca")))

(define-public crate-clap-interactive-0.1.7 (c (n "clap-interactive") (v "0.1.7") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.4.0") (d #t) (k 0)))) (h "0zwz0903mkh7lhlx4lza79fkj9iq55pykwza221zwvdv9agghb5m")))

(define-public crate-clap-interactive-0.1.8 (c (n "clap-interactive") (v "0.1.8") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.4.0") (d #t) (k 0)))) (h "09v4nkw0jccd9gq3aqbkq9w0h5g25sja2xfq3y3z1zaznyn627bd")))

(define-public crate-clap-interactive-0.1.9 (c (n "clap-interactive") (v "0.1.9") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.5.1") (d #t) (k 0)))) (h "1q5l0vmkya4ll691y4c82zgvppfbs0ccjkq0z7qarim7cz87k4zc")))

(define-public crate-clap-interactive-0.2.0 (c (n "clap-interactive") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03mkbxm21daa28w92k06qxpvp73xspfrk0s1r1s2p5gpwz9h3xa2")))

(define-public crate-clap-interactive-0.2.1 (c (n "clap-interactive") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "inquire") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0q5j32isyja14264716895kg2x6b5bz746i23lc6m1q199mkjqcm")))

