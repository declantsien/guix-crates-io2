(define-module (crates-io cl ap clap-test) #:use-module (crates-io))

(define-public crate-clap-test-0.1.0 (c (n "clap-test") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "~0.1.69") (d #t) (k 0)))) (h "0chglvcbg2fk40sjvnw1ba8ans82r78xaz4jn96qk5mim2dydxfv") (y #t)))

(define-public crate-clap-test-0.1.1 (c (n "clap-test") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "~0.1.69") (d #t) (k 0)))) (h "1mnjf0v9xhj59049s8fz89gyjv4v1yhjczsp70d2ysb5d3yl4g7d") (y #t)))

