(define-module (crates-io cl ap clap-port-flag) #:use-module (crates-io))

(define-public crate-clap-port-flag-0.1.0 (c (n "clap-port-flag") (v "0.1.0") (d (list (d (n "structopt") (r "^0.2.8") (d #t) (k 0)))) (h "1axn4bplmfwvncznc2wf3wkv8r8vb6111zdy5vaz59ykax01c1bi")))

(define-public crate-clap-port-flag-0.1.1 (c (n "clap-port-flag") (v "0.1.1") (d (list (d (n "structopt") (r "^0.2.8") (d #t) (k 0)))) (h "0bmls4c8m4s9qb1x3zvcnzf9ngvi335z5hcw0dvn9x0md93m4vay")))

(define-public crate-clap-port-flag-0.2.0 (c (n "clap-port-flag") (v "0.2.0") (d (list (d (n "structopt") (r "^0.2.8") (d #t) (k 0)))) (h "1aj9zfzk7dc1ka9fzfvr2wfywxbkvhrj8dy4dpgkqhi818nrf9yd")))

(define-public crate-clap-port-flag-0.3.0 (c (n "clap-port-flag") (v "0.3.0") (d (list (d (n "structopt") (r "^0.2.8") (d #t) (k 0)))) (h "09kvjdx6z3rbablhrq7jylhzf6s7b3m0riz406ajj3w2mxjyc184")))

(define-public crate-clap-port-flag-0.4.0 (c (n "clap-port-flag") (v "0.4.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 2)) (d (n "hyper") (r "^0.14.20") (f (quote ("server" "http2"))) (d #t) (k 2)) (d (n "tokio") (r "^1.21.0") (f (quote ("macros" "net" "rt-multi-thread"))) (d #t) (k 2)))) (h "0bhmnlprhlja0ihv3vsxdcism610m8lsi1nnqy0p5sqjs7bpm3fn") (f (quote (("fd") ("default" "fd") ("addr_with_port"))))))

