(define-module (crates-io cl ap clapi_internal) #:use-module (crates-io))

(define-public crate-clapi_internal-0.1.0 (c (n "clapi_internal") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1ljysc9xr4z7bbc7kqh09nmga97b3yhmrk872dskgrhfl1gbyx4c")))

