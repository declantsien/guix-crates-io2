(define-module (crates-io cl c- clc-engine) #:use-module (crates-io))

(define-public crate-clc-engine-0.1.0 (c (n "clc-engine") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0862wjbcgq43krpfyhvsgcdznxiv8jzawxb1j5vmifnzdy3m375f")))

(define-public crate-clc-engine-0.1.1 (c (n "clc-engine") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "122w6zcfn1p1g1cql7k1g8maswl2856sykhjhjfjdkx3s15c1vam")))

(define-public crate-clc-engine-0.1.2 (c (n "clc-engine") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "05kqvggachlxx3i2s366qj64bzkpvz3ah4mgr6r66zh6g044wddy")))

(define-public crate-clc-engine-0.1.3 (c (n "clc-engine") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0s7gh1ygdi8dvqxqcs0yxx9wirc5bcza59nv6ifzmyvxr71a4inq")))

(define-public crate-clc-engine-0.1.4 (c (n "clc-engine") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "01q9r73d2ams76xrfxwfkkrw18yf3vyqa12gr4rh9b79fv6b2skr")))

