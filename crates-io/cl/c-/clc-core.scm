(define-module (crates-io cl c- clc-core) #:use-module (crates-io))

(define-public crate-clc-core-0.1.0 (c (n "clc-core") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0s49sknlr8jgllhi45avhqr2n69kgsv9a7pgmi8h8b30m5bs8981")))

