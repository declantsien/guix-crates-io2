(define-module (crates-io cl ay claymore) #:use-module (crates-io))

(define-public crate-claymore-0.0.1 (c (n "claymore") (v "0.0.1") (h "0qiwsrkgsabqzz8mavha0x4x15lj9kd6zgk8gz35h71qxjp3ch80") (y #t)))

(define-public crate-claymore-0.1.1 (c (n "claymore") (v "0.1.1") (h "1x8hmbmkjm4zahr8xnjs1gz4w803z0liyx8ldfqf6swy7ndzxa76") (y #t)))

(define-public crate-claymore-0.1.3 (c (n "claymore") (v "0.1.3") (h "05shzkjfqi57qgcmwnlhg2p8gdrani5cd0hyrwam56z7nzd80ag7") (y #t)))

(define-public crate-claymore-0.1.4 (c (n "claymore") (v "0.1.4") (h "0jzb4dpxphp1jn34ms7ig8kblcc3cgy07knw12qn10prrr9f82cg") (y #t)))

(define-public crate-claymore-0.1.77 (c (n "claymore") (v "0.1.77") (h "1gpw5yw9xj1sj0s16fh3xbvxzniyfnr9dllc4f7bks4nlpmzqs8y") (y #t)))

(define-public crate-claymore-0.77.0 (c (n "claymore") (v "0.77.0") (h "1hll297xdvkfc4crp2rxd7148bbxsb8zmsrjd868y2y332n355py") (y #t)))

(define-public crate-claymore-7.7.18 (c (n "claymore") (v "7.7.18") (h "1fyrdpg5ybb79a2xwkfrdqm8m9h7bccydrink5ghdp982dz1i0c3") (y #t)))

(define-public crate-claymore-2018.7.7 (c (n "claymore") (v "2018.7.7") (h "1kb0agx5vdx8sc7kqjpjbjqzh18p55gyaf67wmiwyzyvhc9vb1zm") (y #t)))

(define-public crate-claymore-2019.12.13 (c (n "claymore") (v "2019.12.13") (h "0jg2i67j0n2c3g4nppcyymdhv3b7fq58n4070a8hlhb79pb0b1rq") (y #t)))

(define-public crate-claymore-9999.999.99 (c (n "claymore") (v "9999.999.99") (h "0ynihncyric0krp7aimkan2hpkvsqwl57rf73c295cyc0w0bfqb6") (y #t)))

(define-public crate-claymore-9.9.9 (c (n "claymore") (v "9.9.9") (h "02ndma727fsdy11hdga4kcj0z7zhmk9jqbsy6samf4qfw9kxp4hs") (y #t)))

(define-public crate-claymore-99999.99999.99999 (c (n "claymore") (v "99999.99999.99999") (h "1cjg36rshqj7m0w4gypsd1cyrkcm7k6gvivjc0qmgfykwjckmx7m") (y #t)))

(define-public crate-claymore-9999999.9999999.9999999 (c (n "claymore") (v "9999999.9999999.9999999") (h "1lngvrf8iwvgwng7mklxcsl4dybrn3hjh5924b99bsi3x4gzw49x") (y #t)))

(define-public crate-claymore-999999999.999999999.999999999 (c (n "claymore") (v "999999999.999999999.999999999") (h "14rbpx1xl159ilr9a1b4ycb0mhcgr5p26fqhny5ngx3nx8h0w4p5") (y #t)))

(define-public crate-claymore-999999999.999999999.9999999991 (c (n "claymore") (v "999999999.999999999.9999999991") (h "0sh5gnks9lig4c55swqgpqyqc91xwfdn97sf7403hvwdnsd32q9b")))

