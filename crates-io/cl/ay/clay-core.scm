(define-module (crates-io cl ay clay-core) #:use-module (crates-io))

(define-public crate-clay-core-0.0.1 (c (n "clay-core") (v "0.0.1") (h "1752f0jw945n1gb74vrhavl82l7vq882x1vvgjf3h0f9wn1rb30m")))

(define-public crate-clay-core-0.1.0 (c (n "clay-core") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (d #t) (k 0)) (d (n "ocl-include") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0lx3h1zz8an2cvc3lri0wiyp08kgwqb34xvazdznynj25v6dmkpg")))

(define-public crate-clay-core-0.1.1 (c (n "clay-core") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (d #t) (k 0)) (d (n "ocl-include") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1p67w9gxqp594lr8j4wh0y2mlkpf72vpspzmqcwdd49969xd61f6")))

(define-public crate-clay-core-0.1.2 (c (n "clay-core") (v "0.1.2") (d (list (d (n "image") (r "^0.22.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (d #t) (k 0)) (d (n "ocl-include") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0ckbwbwj83lm9s63sz7khqib570sj6cfmhgwjv232hikr3cd6zgr")))

(define-public crate-clay-core-0.1.3 (c (n "clay-core") (v "0.1.3") (d (list (d (n "image") (r "^0.22.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (d #t) (k 0)) (d (n "ocl-include") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1blfbidccxmj076r88afqpz1daqkc0afr18h1w9928ahza2ayd9k")))

