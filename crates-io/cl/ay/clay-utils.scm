(define-module (crates-io cl ay clay-utils) #:use-module (crates-io))

(define-public crate-clay-utils-0.1.0 (c (n "clay-utils") (v "0.1.0") (d (list (d (n "clay-core") (r "^0.1") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (d #t) (k 0)))) (h "16f3pm745k0qv8cfg0sblpwgm2vrawjwbbfijxyrb497qp3l9ybg")))

(define-public crate-clay-utils-0.1.1 (c (n "clay-utils") (v "0.1.1") (d (list (d (n "clay-core") (r "^0.1.2") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (d #t) (k 0)))) (h "0yrz7l39lxcf44l515lfv015byfwhamk32d22hdpysmpi1g1f8a1")))

