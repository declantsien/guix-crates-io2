(define-module (crates-io cl ay claym) #:use-module (crates-io))

(define-public crate-claym-0.2.0 (c (n "claym") (v "0.2.0") (h "14hm8gv724xk9xc9v4x99h1z9p166zik1m894zz86s723hmrxabi") (r "1.56")))

(define-public crate-claym-0.3.0 (c (n "claym") (v "0.3.0") (h "1zz5cjyjf2xcgddn2s26rvi4aa7mrrpcy07g0i4xdc3h3i3jps94") (r "1.56")))

(define-public crate-claym-0.4.0 (c (n "claym") (v "0.4.0") (h "0hmazvws9r9mjgxm9k155mdyxnwzfjh62n8jfcss6fqkhq143cm2") (r "1.56")))

(define-public crate-claym-0.5.0 (c (n "claym") (v "0.5.0") (h "0w96ixv21mv6m8pmmf2apf162kh8dqwslvy7ihfkb22c94d8ds9f") (r "1.56")))

(define-public crate-claym-0.5.1 (c (n "claym") (v "0.5.1") (h "0hlqgxx5n927bb8r2wqlwvk752k0fblhxxawj18aya3znddfml2p") (r "1.56")))

(define-public crate-claym-0.6.0 (c (n "claym") (v "0.6.0") (h "0x81cbdqkiwf1a16w15dbx8mssamnfmi5i24svmsjs7c38qdq619") (r "1.56")))

