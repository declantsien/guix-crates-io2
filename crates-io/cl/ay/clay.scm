(define-module (crates-io cl ay clay) #:use-module (crates-io))

(define-public crate-clay-0.0.1 (c (n "clay") (v "0.0.1") (h "00gsg17p2sa8abs7lc912z28l86hw6rcvkhr1d3j06q3vk5y7g7y")))

(define-public crate-clay-0.1.1 (c (n "clay") (v "0.1.1") (d (list (d (n "clay-core") (r "^0.1.0") (d #t) (k 0)) (d (n "clay-utils") (r "^0.1.0") (d #t) (k 2)) (d (n "clay-viewer") (r "^0.1.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (d #t) (k 0)) (d (n "ocl-include") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0z5fvqg99brbb079hb5mhm7paqmb405rhbn0njr270j5pfi08xgk")))

(define-public crate-clay-0.1.2 (c (n "clay") (v "0.1.2") (d (list (d (n "clay-core") (r "^0.1.3") (d #t) (k 0)) (d (n "clay-utils") (r "^0.1.1") (d #t) (k 2)) (d (n "clay-viewer") (r "^0.1.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (d #t) (k 0)) (d (n "ocl-include") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0axln1gm5l3265anp3wz18z82zg64wiqsc6jdj6lndzrc7qk579b")))

