(define-module (crates-io cl ay clay-viewer) #:use-module (crates-io))

(define-public crate-clay-viewer-0.1.1 (c (n "clay-viewer") (v "0.1.1") (d (list (d (n "clay-core") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.22.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "rental") (r "^0.5.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "0qajsahdp9f7sjjhcfmz5897z19yq8asna66i0ayyrhnims05qyd")))

(define-public crate-clay-viewer-0.1.2 (c (n "clay-viewer") (v "0.1.2") (d (list (d (n "clay-core") (r "^0.1.2") (d #t) (k 0)) (d (n "clay-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "rental") (r "^0.5.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "0m8mccviw62isqj8qc88xryx2f4bd7az5dcvaq0hd47lgga1gfj7")))

