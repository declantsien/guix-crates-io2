(define-module (crates-io cl vm clvmr_chinilla) #:use-module (crates-io))

(define-public crate-clvmr_chinilla-0.2.0 (c (n "clvmr_chinilla") (v "0.2.0") (d (list (d (n "bls12_381") (r "=0.7.0") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "=0.4.3") (d #t) (k 0)) (d (n "num-integer") (r "=0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "=0.2.15") (d #t) (k 0)) (d (n "openssl") (r "=0.10.40") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "=0.10.2") (d #t) (k 0)))) (h "10vzyjn0adhw8kpad90fxgn5cj6vh6kfga4pdhwl2jg2ir4vmxw0")))

(define-public crate-clvmr_chinilla-0.1.24 (c (n "clvmr_chinilla") (v "0.1.24") (d (list (d (n "bls12_381") (r "=0.7.0") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "=0.4.3") (d #t) (k 0)) (d (n "num-integer") (r "=0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "=0.2.15") (d #t) (k 0)) (d (n "openssl") (r "=0.10.40") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "=0.10.2") (d #t) (k 0)))) (h "03jkg6p4hxpnzp0b39yxmba05v8lhq49rwzb31vz349b9sdvp53b") (y #t)))

