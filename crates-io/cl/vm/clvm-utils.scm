(define-module (crates-io cl vm clvm-utils) #:use-module (crates-io))

(define-public crate-clvm-utils-0.1.14 (c (n "clvm-utils") (v "0.1.14") (d (list (d (n "clvmr") (r "=0.1.24") (d #t) (k 0)))) (h "0k2nj5jp3czq71h5cmddb5qanq0xl5778kyhn78cl4my6ax2ayq7")))

(define-public crate-clvm-utils-0.1.15 (c (n "clvm-utils") (v "0.1.15") (d (list (d (n "clvmr") (r "=0.1.24") (d #t) (k 0)))) (h "1yajhmc7bvk9f6bw7adiwhm2q16f2j4drqmsn3fjqhcl3wy5050b")))

(define-public crate-clvm-utils-0.2.0 (c (n "clvm-utils") (v "0.2.0") (d (list (d (n "clvmr") (r "=0.2.2") (d #t) (k 0)))) (h "1xh5qs5qmvrgd4h2pf8ibc0iy8p021zlaxgw6dk0h4k4ykfiqsf7")))

(define-public crate-clvm-utils-0.2.2 (c (n "clvm-utils") (v "0.2.2") (d (list (d (n "clvmr") (r "=0.2.3") (d #t) (k 0)))) (h "0q14ab0c4kq6zxksnfh221911h3yx2dxv4kikh2wsp9hxidcsiqd")))

(define-public crate-clvm-utils-0.2.3 (c (n "clvm-utils") (v "0.2.3") (d (list (d (n "clvmr") (r "=0.2.3") (d #t) (k 0)))) (h "1lhm4r39lg3n23kcwgbhczd0nhd89s59ql44ci1ll92xi4w5riw3")))

(define-public crate-clvm-utils-0.2.4 (c (n "clvm-utils") (v "0.2.4") (d (list (d (n "clvmr") (r "=0.2.3") (d #t) (k 0)))) (h "1nglgbsf6z3bdyl091qscrc6b9i1fbmq7djisaj51msm1xnrf6xq")))

(define-public crate-clvm-utils-0.2.5 (c (n "clvm-utils") (v "0.2.5") (d (list (d (n "clvmr") (r "=0.2.3") (d #t) (k 0)))) (h "0d8lby9klj9dzfijsbxn8k2qhfmi684q6d0rhb75w0q7d4wa0yij")))

(define-public crate-clvm-utils-0.2.6 (c (n "clvm-utils") (v "0.2.6") (d (list (d (n "clvmr") (r "=0.2.4") (d #t) (k 0)))) (h "0nzxg8s96f3qqk2625vqhwcsh20dyma5v7lg9ljf2i19pzaxiwpx")))

(define-public crate-clvm-utils-0.2.7 (c (n "clvm-utils") (v "0.2.7") (d (list (d (n "clvmr") (r "=0.2.4") (d #t) (k 0)))) (h "00ayif3zj0178vrayyxj77dmbh44a46r6riaa7ir5xqpzr9sr9hh")))

(define-public crate-clvm-utils-0.2.12 (c (n "clvm-utils") (v "0.2.12") (d (list (d (n "clvm-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "clvmr") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0f9br448w12a4nyrjlixb59kfvfihrv8y7pf6b0f1x859abs2aa6")))

(define-public crate-clvm-utils-0.2.14 (c (n "clvm-utils") (v "0.2.14") (d (list (d (n "clvm-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "clvmr") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0rqajilhdfj8qbgglc0qf1ch0lncri1749mw7sr9pn185j5mwkvs")))

(define-public crate-clvm-utils-0.3.0 (c (n "clvm-utils") (v "0.3.0") (d (list (d (n "clvm-traits") (r "=0.2.14") (d #t) (k 0)) (d (n "clvmr") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1y7i0a8f53l3vbrsday5ji6mjx9k1kszmxzmi7v1ivd2zm2hjb8g")))

(define-public crate-clvm-utils-0.4.0 (c (n "clvm-utils") (v "0.4.0") (d (list (d (n "clvm-traits") (r "^0.3.3") (d #t) (k 0)) (d (n "clvmr") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "025bkfrz92y03cxd1d2lcyx5kh2fdxh1fz2gsjq3gyd29ibcb8q2")))

(define-public crate-clvm-utils-0.5.1 (c (n "clvm-utils") (v "0.5.1") (d (list (d (n "clvm-traits") (r "^0.5.1") (d #t) (k 0)) (d (n "clvmr") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0s4gp6qxywszvv99z3d11mgkrplg0z1fk3q7q3i52bnniahy4zpc")))

(define-public crate-clvm-utils-0.6.0 (c (n "clvm-utils") (v "0.6.0") (d (list (d (n "clvm-traits") (r "^0.6.0") (d #t) (k 0)) (d (n "clvmr") (r "^0.6.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "0gp43iqnzadkgy65av1hqpsi1w8s86agvm1xywgpbypc4i51qmlq")))

(define-public crate-clvm-utils-0.7.0 (c (n "clvm-utils") (v "0.7.0") (d (list (d (n "clvm-traits") (r "^0.7.0") (d #t) (k 0)) (d (n "clvmr") (r "^0.6.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "1kny157s9f58zfp6rhbclqicgkfy04vpwxnm98di1j3b7w2bmwza")))

(define-public crate-clvm-utils-0.8.0 (c (n "clvm-utils") (v "0.8.0") (d (list (d (n "clvm-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "clvm-traits") (r "^0.8.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clvmr") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "0ijp96v1cg4aibq8simk5q4yq8y2y7mjdwzx66ri1c80pdg2gfjr")))

(define-public crate-clvm-utils-0.9.0 (c (n "clvm-utils") (v "0.9.0") (d (list (d (n "clvm-traits") (r "^0.9.0") (d #t) (k 0)) (d (n "clvm-traits") (r "^0.9.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clvmr") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "1jx4nim41dd0990znqs4wz9w0rjzg9s2jjmfqjkm8j1frkwbxm1z")))

