(define-module (crates-io cl vm clvm-derive) #:use-module (crates-io))

(define-public crate-clvm-derive-0.2.12 (c (n "clvm-derive") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0c8sy6js0bmj6i6px40gbsd4xqkl7p12245wfdyplaih061ia87a")))

(define-public crate-clvm-derive-0.2.14 (c (n "clvm-derive") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1njk2lzwsz9pk7vaqfpg1814k08szv3q4qj3w0i4klx4z0wfc44i")))

(define-public crate-clvm-derive-0.5.2 (c (n "clvm-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0pv78bw05lna02z33z3d907xhw4jams6blz66v6qcw3hhaxb3c96")))

(define-public crate-clvm-derive-0.6.0 (c (n "clvm-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0s4cq90z5q8mfapix7mcj9483xrkbc72164bpyqvg1p3vnay1kn4")))

(define-public crate-clvm-derive-0.9.0 (c (n "clvm-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "17n38mh5wdkzbm4lml42injla39lvk8zgv0g099955bh8kidba0l")))

