(define-module (crates-io cl aa claap-test-dont-use) #:use-module (crates-io))

(define-public crate-claap-test-dont-use-0.1.0 (c (n "claap-test-dont-use") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "maud") (r "^0.22.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "1gzjydypx4j3f67gxy2zvlcjf4r1z334igvpk9z852d6h8ggr1c1")))

