(define-module (crates-io cl i- cli-logger) #:use-module (crates-io))

(define-public crate-cli-logger-0.1.0 (c (n "cli-logger") (v "0.1.0") (d (list (d (n "cli-panics") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "1m9si1v4pr9wbsnsxapzcllsswz9nzk4syzjify5g0kllql34dqc")))

(define-public crate-cli-logger-0.1.1 (c (n "cli-logger") (v "0.1.1") (d (list (d (n "cli-panics") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1v6caym3kwrgwx7flpr7gb8qr8qypfndacb0zicgbq0zh1hwsjhc")))

