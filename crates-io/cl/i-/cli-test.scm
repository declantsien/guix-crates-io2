(define-module (crates-io cl i- cli-test) #:use-module (crates-io))

(define-public crate-cli-test-0.1.0 (c (n "cli-test") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.10") (f (quote ("std" "derive" "unicode"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0b5920bpsjvfhp9adq86pj5bzrswgz6v19029hp91l0fm481syic")))

