(define-module (crates-io cl i- cli-rs-command-gen) #:use-module (crates-io))

(define-public crate-cli-rs-command-gen-0.1.8 (c (n "cli-rs-command-gen") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ncafssbjlc7zlk16kj76lg54fi1ndiyzjxfivmf8lfx9dg9m1df")))

(define-public crate-cli-rs-command-gen-0.1.10 (c (n "cli-rs-command-gen") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1kk4drsrxv3fx17ns45favrwqbfr6ypf5lv7z865a5ylna5fkmc6")))

(define-public crate-cli-rs-command-gen-0.1.11 (c (n "cli-rs-command-gen") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0zz4q95iv55brx93vfg0vkvgbdib3gmg30gg01di1587mnxailgc")))

(define-public crate-cli-rs-command-gen-0.1.12 (c (n "cli-rs-command-gen") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1plz6v89f9g22iy934qk99158wys58d495rhwa94qay2a1ynz9pi")))

