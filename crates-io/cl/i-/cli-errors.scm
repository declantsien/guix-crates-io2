(define-module (crates-io cl i- cli-errors) #:use-module (crates-io))

(define-public crate-cli-errors-0.1.0 (c (n "cli-errors") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "109a9wimgla8cd64vdlpyiw6i02nnns9yp8j0hcwqjkdj2g0dmqh") (y #t)))

(define-public crate-cli-errors-0.2.0 (c (n "cli-errors") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "cli-errors-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1lcnhcsv11d8d1zs9kvwibxwdb5hnd1vbdnkmb5mfwshiiypfbnn") (y #t)))

(define-public crate-cli-errors-0.3.0 (c (n "cli-errors") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "cli-errors-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "0ginxrdv7mg538wf08b28q55hbl94qr03i5gb6knm1v8j38rbjvp") (y #t)))

(define-public crate-cli-errors-0.3.1 (c (n "cli-errors") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cli-errors-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1rzvvlw6i4qvm5m7a9arll2i6ad92j973lfrzip8r32wb9n6xr27") (y #t)))

