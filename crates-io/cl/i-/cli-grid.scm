(define-module (crates-io cl i- cli-grid) #:use-module (crates-io))

(define-public crate-cli-grid-0.1.0 (c (n "cli-grid") (v "0.1.0") (h "0vfzg5n0nnc5c6jcgdkmj8qhiimxs6dlhw9jaad44735ayf83pjr")))

(define-public crate-cli-grid-0.1.1 (c (n "cli-grid") (v "0.1.1") (h "1pmqbzrq33p70z3p41rcdiz9l4rcsapn298n3c6cbk2j81wmp21z")))

(define-public crate-cli-grid-0.1.2 (c (n "cli-grid") (v "0.1.2") (h "19n72vv0q6kjc0sk95s1dy7k95gh2fmls82xds66c7cbcsylbibr")))

