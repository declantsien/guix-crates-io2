(define-module (crates-io cl i- cli-tables) #:use-module (crates-io))

(define-public crate-cli-tables-0.1.0 (c (n "cli-tables") (v "0.1.0") (h "1dr50ihaa5rr9xjm1hwhg1p73q1jjxx5g04cijnypc5v1ij5vhsv")))

(define-public crate-cli-tables-0.2.0 (c (n "cli-tables") (v "0.2.0") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "168fkwzra8cwlmzikbharics7vgk6y1y7w5kql1zq0sl6w0gv0xs")))

(define-public crate-cli-tables-0.2.1 (c (n "cli-tables") (v "0.2.1") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0mr3vy6mpwnpw6j7sgjgn91an58acpzd7rcj5c6im17dp43kf0jh")))

