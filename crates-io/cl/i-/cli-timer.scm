(define-module (crates-io cl i- cli-timer) #:use-module (crates-io))

(define-public crate-cli-timer-0.3.0 (c (n "cli-timer") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1v6dpkxgrpcwsi96vzkax253kj8rshapk7f9bg51khwn0rw8jl53")))

(define-public crate-cli-timer-0.3.1 (c (n "cli-timer") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1syjp9n1ja89f9jx57yw8dj3xpjv7dx6npi7sw4p9d1ri32n0vik")))

(define-public crate-cli-timer-0.3.2 (c (n "cli-timer") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0wfjz9bg7j2i1wq7n1pdq7c1f4blqr5v4xsczss92n2mk7rf4pyn")))

(define-public crate-cli-timer-0.3.31 (c (n "cli-timer") (v "0.3.31") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "16ghdc0ljdzh2xcab65pd1xbwb61z1vq4gm9ivi9r5xb9carlbzg")))

(define-public crate-cli-timer-0.3.32 (c (n "cli-timer") (v "0.3.32") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0y9dfw0fkhwfv8i214fmsqxzvy55bid53whh5w7imd71151sqmwd")))

(define-public crate-cli-timer-0.3.33 (c (n "cli-timer") (v "0.3.33") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0gpnkswd0mv4i7jsnqg5a76qxl7fy543868372xq3yhh8m3aw1q1")))

(define-public crate-cli-timer-0.3.34 (c (n "cli-timer") (v "0.3.34") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0j9jp9ba1vcd9n749pzcarh8q3qf3bilhix3k80qqm3fnkph0zmz")))

(define-public crate-cli-timer-0.3.35 (c (n "cli-timer") (v "0.3.35") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0gv001vkifh8qrccl76gkygg4a5cijqs2qcq7rzlrz97nlm00ph4")))

(define-public crate-cli-timer-0.3.36 (c (n "cli-timer") (v "0.3.36") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0hz2sy9wlx4plb8xa0s05qvk2ml9bq7hlyzncgaxjw4ffh6vvk3j")))

(define-public crate-cli-timer-0.3.37 (c (n "cli-timer") (v "0.3.37") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "01n5m8n3iq8kilvv37h64y1j8xm1hji6wl7f8jx4kchhg60d3390")))

(define-public crate-cli-timer-0.3.4 (c (n "cli-timer") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1r5gcgf88f7r2f0k08lri40bv1zsc21hwfb8i9gjcgcbr77ckdnx")))

(define-public crate-cli-timer-0.3.42 (c (n "cli-timer") (v "0.3.42") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1ccp0i7jlqnrl9djis0gwvx6sa4y73n3589pvvhpmk2ww5vbgx9n")))

(define-public crate-cli-timer-0.3.43 (c (n "cli-timer") (v "0.3.43") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1hab0cy0813kvll4n5xbgnmnfd99p2sqivnm334hg9fiw1mpqw1q")))

(define-public crate-cli-timer-0.3.5 (c (n "cli-timer") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0579n72d4wyzdk18aqyzbvh66w85b9k2zv4zn2gkkvl3fksrf30c")))

(define-public crate-cli-timer-0.3.51 (c (n "cli-timer") (v "0.3.51") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0ilixs03x9vhk0ikywnq2p14ab6gq85lr226kf2r7k17csyvsqrc")))

(define-public crate-cli-timer-0.3.53 (c (n "cli-timer") (v "0.3.53") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1nmfjprndp1dggzwqmj8c04f1cdqq0zn317kbds0lwh7nzi3b14g")))

(define-public crate-cli-timer-0.3.62 (c (n "cli-timer") (v "0.3.62") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "1ak22g272g67wivwbl3qkymvk9lqn7r3l50xwjl2pb339z8s4ai6")))

(define-public crate-cli-timer-0.3.70 (c (n "cli-timer") (v "0.3.70") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "19rhb62l21kgb3w0abnlgyq5asp2m3bzdyxz51377fhgyk1zfvfq")))

(define-public crate-cli-timer-0.3.71 (c (n "cli-timer") (v "0.3.71") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "1fbg34b48i9b17g1w6bqza1s8sgc2wcxsndlfq6wp8wj6n86x0i8")))

(define-public crate-cli-timer-0.3.72 (c (n "cli-timer") (v "0.3.72") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "0wqqbj3260hg87vjbjl1ha0lbfx1lzigky0pqgxk0fh4jhmd7qpc")))

(define-public crate-cli-timer-0.3.73 (c (n "cli-timer") (v "0.3.73") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "17b1hpabq7g7p36i28w9v1jq57p6bfdm7riq87fc8yrlic7axbjq")))

(define-public crate-cli-timer-0.3.74 (c (n "cli-timer") (v "0.3.74") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "0vsqr4ijgw1dkja7dvh4g6gnxw4md91kj58q4h1728jybd3lkdw6")))

(define-public crate-cli-timer-0.3.75 (c (n "cli-timer") (v "0.3.75") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "091g4qcbdpk6fs4qw6a4xjb1z3b4l0x8di9qm6l3ji3qshpn66vl")))

(define-public crate-cli-timer-0.3.76 (c (n "cli-timer") (v "0.3.76") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "10rrglwv97i6iq1yzf51yblqzvfjghq3d2x57k9in98slrzy0zz9")))

(define-public crate-cli-timer-0.3.77 (c (n "cli-timer") (v "0.3.77") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "1m68qi8j8480nvbwdilwqjb7l0acqpsxq12vbxddxl9dsk31flrz")))

(define-public crate-cli-timer-0.3.78 (c (n "cli-timer") (v "0.3.78") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "0wjdn8l3sda1agiw1fpd9hhhsimk9rsy5jphjimp102j4myf7jix")))

(define-public crate-cli-timer-0.3.79 (c (n "cli-timer") (v "0.3.79") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.1.3") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "0hsr45c5g75rr2n4gh71767a4bxniy4y0h9iz8f1siqd5bna1wn1")))

(define-public crate-cli-timer-0.3.80 (c (n "cli-timer") (v "0.3.80") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.1.3") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.8.1") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "0dc5pchlcr4bvy1vq17cri4gim678x0ya7jl0ssijm5505mj37nq")))

(define-public crate-cli-timer-0.3.81 (c (n "cli-timer") (v "0.3.81") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.1.3") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1ddp137m3ddg4k0mdidfsdjpsfp5czzm1iclbjc6gkgm0m4w1n2q")))

(define-public crate-cli-timer-0.3.83 (c (n "cli-timer") (v "0.3.83") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1fq4h7sw304n3n6ydhpqj01dyhjq3sx4mkg54zq4xc1kwl7n7ahc")))

(define-public crate-cli-timer-0.3.84 (c (n "cli-timer") (v "0.3.84") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.2.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "0r402dw4vrv8sxkm5a1cf0v35wf7517p46w1b778jwnm5hhkbv0i")))

