(define-module (crates-io cl i- cli-completion) #:use-module (crates-io))

(define-public crate-cli-completion-0.1.0 (c (n "cli-completion") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "074wiv8bw46mlv5hdcd0mvnd9wqk7a743pjbfwa0pxy1qzx1cjx3")))

(define-public crate-cli-completion-0.2.0 (c (n "cli-completion") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0imakd94gqm8q0rk7kfnhpj8x8d97x701x45p59iryix5mgq83ak")))

(define-public crate-cli-completion-0.3.0 (c (n "cli-completion") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0g0dz2lfpqdyycbnh05l9pxfpqp4hrfn44zrak6p3zjixynrx259")))

(define-public crate-cli-completion-0.4.0 (c (n "cli-completion") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1x7cv9zwhh6aslq3si7dy9zhiqwi42p5gcp9dhjs9vvc2rid5m69")))

