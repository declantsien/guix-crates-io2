(define-module (crates-io cl i- cli-ferrilab) #:use-module (crates-io))

(define-public crate-cli-ferrilab-0.1.0 (c (n "cli-ferrilab") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1jyaf5p33855xdwhmrp2b2mkhc6m9shzry2fl8qcyfcpwj5kfc5r")))

