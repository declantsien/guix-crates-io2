(define-module (crates-io cl i- cli-clap-example) #:use-module (crates-io))

(define-public crate-cli-clap-example-0.1.0 (c (n "cli-clap-example") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uj_tcs_rust_23_18") (r "^0.1.0") (d #t) (k 0)))) (h "10w0pmwdfxqgi6ldad9gmkiqbylkp9rf9p8hnkc1wkbcgp6vvr2s")))

