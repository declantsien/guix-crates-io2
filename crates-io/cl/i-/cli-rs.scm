(define-module (crates-io cl i- cli-rs) #:use-module (crates-io))

(define-public crate-cli-rs-0.1.0 (c (n "cli-rs") (v "0.1.0") (h "1h04qv625adb8lzirdp06gg5hamhngafbw11ybxhz3a86pp4p4i1")))

(define-public crate-cli-rs-0.1.1 (c (n "cli-rs") (v "0.1.1") (h "1zgdb8vs7mzi7hfavff1x222hih69lb3kdvfbz3grwv1nhhqkajk")))

(define-public crate-cli-rs-0.1.2 (c (n "cli-rs") (v "0.1.2") (h "0qdbpz101cbfrcyx3g34jkfljdyz70c2xr4nhyxbsqryfkas0sv1")))

(define-public crate-cli-rs-0.1.3 (c (n "cli-rs") (v "0.1.3") (h "0951gyn5jd1qf1ia079qcsw618s78bxwc2n3jk6chjr0fy6v69rn")))

(define-public crate-cli-rs-0.1.4 (c (n "cli-rs") (v "0.1.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0w784293kqpfs4crbg728sqczcvjfvl4lmyg0nhb8k9bga8ryzzk")))

(define-public crate-cli-rs-0.1.5 (c (n "cli-rs") (v "0.1.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "047h7644alchdc1yw9iv8q8haj5z9jwmgbv1xbclc9zczvs0qmlb")))

(define-public crate-cli-rs-0.1.6 (c (n "cli-rs") (v "0.1.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0c8nc9s08h9jq9kdxsgkrxylkwdiy4xm1q1cnrzijg2zypv3slvx")))

(define-public crate-cli-rs-0.1.7 (c (n "cli-rs") (v "0.1.7") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "16amnspi0zi53f22wrazd752h56ky6s292y3kmh1z9hrjbpipig3")))

(define-public crate-cli-rs-0.1.8 (c (n "cli-rs") (v "0.1.8") (d (list (d (n "cli-rs-command-gen") (r "^0.1.8") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0anx2amwy7l0vsnk8h5rdbny22h7s9b8z19y6a6kryvikw79sdn5")))

(define-public crate-cli-rs-0.1.10 (c (n "cli-rs") (v "0.1.10") (d (list (d (n "cli-rs-command-gen") (r "^0.1.10") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "12my18dpngn4p6pwd7ihg1bldw629j1y6m5jqvsbyvg9g8scfyjb")))

(define-public crate-cli-rs-0.1.11 (c (n "cli-rs") (v "0.1.11") (d (list (d (n "cli-rs-command-gen") (r "^0.1.10") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0qbdc2lrqd7lsa46bp9r1lhdimxzvxrfhwa3hx4q3ydx3ps6n210")))

(define-public crate-cli-rs-0.1.12 (c (n "cli-rs") (v "0.1.12") (d (list (d (n "cli-rs-command-gen") (r "^0.1.12") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "031b101imbpk6cr6jvnjvjhyipvf0xmzb750a2qknlgpqjw4s69l")))

(define-public crate-cli-rs-0.1.13 (c (n "cli-rs") (v "0.1.13") (d (list (d (n "cli-rs-command-gen") (r "^0.1.12") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0xp9c3n5rx3gbqxp656m46lqqhzkfrr6ar2k03kzwd35gdhibl4w")))

