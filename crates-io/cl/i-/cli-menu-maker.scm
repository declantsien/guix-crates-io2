(define-module (crates-io cl i- cli-menu-maker) #:use-module (crates-io))

(define-public crate-cli-menu-maker-0.1.0 (c (n "cli-menu-maker") (v "0.1.0") (d (list (d (n "console-menu") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "1nc8ba5ib524wav5c4szaagdr7gqd5ylm97dfhzp4jynmxk5ghn6")))

(define-public crate-cli-menu-maker-0.1.1 (c (n "cli-menu-maker") (v "0.1.1") (d (list (d (n "console-menu") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "155j37x03rnpvgas8q1ganwxsc8rprdpr2d16dg5dycd5g4gs1vn")))

