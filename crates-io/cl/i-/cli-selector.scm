(define-module (crates-io cl i- cli-selector) #:use-module (crates-io))

(define-public crate-cli-selector-0.1.0 (c (n "cli-selector") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)))) (h "1qnkd6zjsyx3xi6zh4ybmwxc6hda4nx5py49rf7d2jhdb9hdirv4")))

(define-public crate-cli-selector-0.2.0 (c (n "cli-selector") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)))) (h "1wd6qgagxw329lc1fn3ky98rzq8rljfkc03gj8b3ds8qz6kwz8qp")))

