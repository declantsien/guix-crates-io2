(define-module (crates-io cl i- cli-helpers) #:use-module (crates-io))

(define-public crate-cli-helpers-0.1.0 (c (n "cli-helpers") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pjzsa4hqj54ka2hdafvb9k67l31cs6la9zbk4vfhklx9flghw22")))

