(define-module (crates-io cl i- cli-es) #:use-module (crates-io))

(define-public crate-cli-es-0.1.0 (c (n "cli-es") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "console") (r "^0.9.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.4") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)))) (h "0pqrjsmm4gajzag9jz61mn1p9qkq2w52x2hr6xgxwabyc5226ksx")))

(define-public crate-cli-es-0.1.1 (c (n "cli-es") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "console") (r "^0.9.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)))) (h "03ry0wh6adj6g3xx70rdirb8sb231lgrkn82zcib78v14imfpf53")))

