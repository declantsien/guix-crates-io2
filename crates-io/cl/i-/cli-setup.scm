(define-module (crates-io cl i- cli-setup) #:use-module (crates-io))

(define-public crate-cli-setup-0.1.0 (c (n "cli-setup") (v "0.1.0") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)))) (h "0fdv1fjsabamqykvx2x6xczq1gxdhh5zp71zcqxnhwryir8h93qv")))

(define-public crate-cli-setup-0.1.1 (c (n "cli-setup") (v "0.1.1") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)))) (h "1rhbrndpc4wndi309nsbw7hnv969fgbmhacbx8h0q3nvypd6w3mi")))

(define-public crate-cli-setup-0.1.2 (c (n "cli-setup") (v "0.1.2") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)))) (h "0zim67bxxhg6y48c15krynii1k89x3bh1axcsvsyr3q47glcrv26")))

(define-public crate-cli-setup-0.1.4 (c (n "cli-setup") (v "0.1.4") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)))) (h "0z96q5q1xys6smgjrbxzmidm25mfjr07c76vypb7arsnf9vqv9nx")))

(define-public crate-cli-setup-0.2.0 (c (n "cli-setup") (v "0.2.0") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)))) (h "13m992gcggf1izph9f2gi0fs08kmncvgy9ysqs6krgx0jpmif8lz")))

(define-public crate-cli-setup-0.2.2 (c (n "cli-setup") (v "0.2.2") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "0ymq43270xa6ljq2rf0q7xlr8m8d1cnk6bnca27wk1k8nghj0nmc")))

(define-public crate-cli-setup-0.2.3 (c (n "cli-setup") (v "0.2.3") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "11d5gwif18dhan3bh2qw3lvqhdx2ki05abbnyrqyvlz8009f47a4")))

(define-public crate-cli-setup-0.2.4 (c (n "cli-setup") (v "0.2.4") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)))) (h "01rig11nnc8knjwy2x193k7hndskd2xk0z71n3sfdkw4l06v12hr")))

(define-public crate-cli-setup-0.2.5 (c (n "cli-setup") (v "0.2.5") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)))) (h "1vxyr785rbvz8r0gy2zxvv2h1b559q4gx4cswskm68wbfzpv7ap2")))

(define-public crate-cli-setup-0.2.6 (c (n "cli-setup") (v "0.2.6") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)))) (h "001bf50gm9cmaa9n4wa0g5v9qqr3dhvd09jzhrmkjj1x33qhbkb2")))

(define-public crate-cli-setup-0.2.7 (c (n "cli-setup") (v "0.2.7") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)))) (h "1z0691a2625rl3dplsyyb0p38k36v0kbf514l1xhr03jh4ph36rj")))

