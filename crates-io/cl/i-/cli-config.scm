(define-module (crates-io cl i- cli-config) #:use-module (crates-io))

(define-public crate-cli-config-0.1.0 (c (n "cli-config") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (o #t) (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "0gy9y0flc020faj3lmjphpzrd1dzdrkyxdcsvshirgvfmvalmd2j") (f (quote (("yaml" "serde_yaml") ("json" "serde_json")))) (s 2) (e (quote (("toml" "dep:toml"))))))

