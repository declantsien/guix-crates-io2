(define-module (crates-io cl i- cli-menu) #:use-module (crates-io))

(define-public crate-cli-menu-0.0.0 (c (n "cli-menu") (v "0.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "cucumber") (r "^0.11") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0xbqs81n9c3iyv3hgdxcdgi01sshn9jgmkya5adw13rabbjny37f") (y #t)))

