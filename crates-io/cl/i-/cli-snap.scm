(define-module (crates-io cl i- cli-snap) #:use-module (crates-io))

(define-public crate-cli-snap-0.1.0 (c (n "cli-snap") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.198") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0rnxsyimm5sga0as0zkfi07kg5imsysdnkgbivcdsrpzmgxb3rg7")))

