(define-module (crates-io cl i- cli-editor) #:use-module (crates-io))

(define-public crate-cli-editor-0.0.0 (c (n "cli-editor") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "028bnc1jahvgg9ar40jjgpz6jxpz4cdsd1kdh6njiqi4wkmpx83i")))

