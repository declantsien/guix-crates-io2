(define-module (crates-io cl i- cli-log) #:use-module (crates-io))

(define-public crate-cli-log-0.1.0 (c (n "cli-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1kz6ilkymjn4bvsjrryfsykr3bwj1i0rjd4q7qmqjx90k7r76xpl")))

(define-public crate-cli-log-1.0.0 (c (n "cli-log") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "04q5cqa8mw6l88l635a9w6d3rpx4p4mhlsk6vlsy9qq79n24m1ss")))

(define-public crate-cli-log-1.1.0 (c (n "cli-log") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "08d3gvaj70k3m5scqfxzsjm1l1b7i5wb12gzgrbv61if1kgfyydv")))

(define-public crate-cli-log-1.2.0 (c (n "cli-log") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file-size") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "proc-status") (r "^0.1") (o #t) (d #t) (k 0)))) (h "02hswvm2wrb8a7hp7fqcdrrg6igwyq1cm1ldspz41d0l99ar80b9") (f (quote (("mem" "proc-status" "file-size") ("default" "mem"))))))

(define-public crate-cli-log-2.0.0 (c (n "cli-log") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "file-size") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "proc-status") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1k559h2am9qk65d72m9p6nzymzqirjp8bh2sya5c4bn8qh6v0aix") (f (quote (("mem" "proc-status" "file-size") ("default" "mem"))))))

