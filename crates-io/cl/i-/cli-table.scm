(define-module (crates-io cl i- cli-table) #:use-module (crates-io))

(define-public crate-cli-table-0.1.0 (c (n "cli-table") (v "0.1.0") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "15zb0yzjdcf583mp3xdj8wqzsl0czz4xy4kf12hgsrb51457cl2m")))

(define-public crate-cli-table-0.2.0 (c (n "cli-table") (v "0.2.0") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1q30iws0m1cxibsxj2fw4jsfvyss5bhmvdzmw25a129cxblvvrvg")))

(define-public crate-cli-table-0.3.0 (c (n "cli-table") (v "0.3.0") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1rxbqbny4b42b213f2rjnddx4y4bg44j60pfv98q2q277pyzwcvc")))

(define-public crate-cli-table-0.3.1 (c (n "cli-table") (v "0.3.1") (d (list (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "16d62iafvp3vxjwilhgl2r4ss2grdx4h0b2gz7c8wik2vazjqy5x")))

(define-public crate-cli-table-0.3.2 (c (n "cli-table") (v "0.3.2") (d (list (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "18knhk0g2dbwyakzpryshx8hfxkizivmniymxmwlqhwfrh0wsrqj")))

(define-public crate-cli-table-0.4.0 (c (n "cli-table") (v "0.4.0") (d (list (d (n "cli-table-derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1lg0wlq4m95faacfzazzv4f7v90a8dcqjd5csp10vd5nswj1rrs4") (f (quote (("title") ("doc") ("derive" "cli-table-derive" "title") ("default" "derive"))))))

(define-public crate-cli-table-0.4.1 (c (n "cli-table") (v "0.4.1") (d (list (d (n "cli-table-derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1fb8xa22abad0g914lsqmyffrdpijdpmqrn2pzyg37inl8nkhs65") (f (quote (("title") ("doc") ("derive" "cli-table-derive" "title") ("default" "derive"))))))

(define-public crate-cli-table-0.4.2 (c (n "cli-table") (v "0.4.2") (d (list (d (n "cli-table-derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "18gdkz0wxr3vmvp6mzd0w8b1cg0bfzw6vlaghjzh3qhmbgpbvv5b") (f (quote (("title") ("doc") ("derive" "cli-table-derive" "title") ("default" "derive"))))))

(define-public crate-cli-table-0.4.3 (c (n "cli-table") (v "0.4.3") (d (list (d (n "cli-table-derive") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0scn7c3km0pgr2wx4qjmqgp1bhnalyq23c2b4vk2844qpsc1gkm5") (f (quote (("title") ("doc") ("derive" "cli-table-derive" "title") ("default" "derive"))))))

(define-public crate-cli-table-0.4.4 (c (n "cli-table") (v "0.4.4") (d (list (d (n "cli-table-derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "144y6fsrsbm43wdvvj77dgpy28nvjmbhfhwdmkypfrdglh49zdd4") (f (quote (("title") ("doc") ("derive" "cli-table-derive" "title") ("default" "derive"))))))

(define-public crate-cli-table-0.4.5 (c (n "cli-table") (v "0.4.5") (d (list (d (n "cli-table-derive") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1sad9zz5aa44lvc93r1n1g0b0qz7w3y593p7nhl6cfssk6gwqyim") (f (quote (("title") ("doc") ("derive" "cli-table-derive" "title") ("default" "csv" "derive"))))))

(define-public crate-cli-table-0.4.6 (c (n "cli-table") (v "0.4.6") (d (list (d (n "cli-table-derive") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1ly9zsi1k4dyl15mgxly7kvni3k6ww5nazwcaldha0rhi198dvc4") (f (quote (("title") ("doc") ("derive" "cli-table-derive" "title") ("default" "csv" "derive"))))))

(define-public crate-cli-table-0.4.7 (c (n "cli-table") (v "0.4.7") (d (list (d (n "cli-table-derive") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "179pvik96qavn84rd74n3v0i4msnxq5hq39n25qbxi72v4bb3yxd") (f (quote (("title") ("doc") ("derive" "cli-table-derive" "title") ("default" "csv" "derive"))))))

