(define-module (crates-io cl i- cli-rpx) #:use-module (crates-io))

(define-public crate-cli-rpx-1.0.0 (c (n "cli-rpx") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "12ih9qvgip4pc9gfk4jhxymp7vqq7l162s4aqfdfgd1qdq6gfi43")))

(define-public crate-cli-rpx-1.0.1 (c (n "cli-rpx") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0qxawpc1qy65x7rqymlabyn8w44kz7r55rfdyalkaglq4api41r9")))

(define-public crate-cli-rpx-1.0.2 (c (n "cli-rpx") (v "1.0.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "19b14cc5v3jpcvmx01440ll2fzsbhjmvhd1sp8fdr9ha678nj28y")))

(define-public crate-cli-rpx-1.1.0 (c (n "cli-rpx") (v "1.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0r3kdcslvw44mlqf66liw0d2di2h5jr9p77mkry0d991cfk93j9z")))

(define-public crate-cli-rpx-1.1.1 (c (n "cli-rpx") (v "1.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0xfz9jywa5jx8ss14qq9kshlp8sdb9gh9ggzjisi8ppw5zbnr7j2")))

