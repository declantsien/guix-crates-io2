(define-module (crates-io cl i- cli-agenda) #:use-module (crates-io))

(define-public crate-cli-agenda-0.1.0 (c (n "cli-agenda") (v "0.1.0") (d (list (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "15wlyzcz4igmdi9i1awi4f8snw40jb0wz7r7kf869dyyd8dxs7wn")))

(define-public crate-cli-agenda-0.1.1 (c (n "cli-agenda") (v "0.1.1") (d (list (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "0zhfd04c67a37gs196j8jsbppbvzxd1lrdqrkrzgn0fnaj032jcz")))

(define-public crate-cli-agenda-0.1.2 (c (n "cli-agenda") (v "0.1.2") (d (list (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "15l96vs3n6ag9i9k1ay7hlbir2k8m6w9xzm3pyjmdlf7ll0hq0y4")))

