(define-module (crates-io cl i- cli-rng) #:use-module (crates-io))

(define-public crate-cli-rng-0.1.1 (c (n "cli-rng") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0blsmzzrsrq8xgjrjfpi241y4i1kkp45sbkqg5c0sdw4vqmhwir6")))

(define-public crate-cli-rng-0.1.2 (c (n "cli-rng") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1794fpdqpj1pcvf9nsb27vdan9yy2d1nk98161r6f4vcz32902mb")))

(define-public crate-cli-rng-0.1.3 (c (n "cli-rng") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1a4hbhsb7gx0hz90xl8ribc9zxwrpjby2vll6j5iy6cgd3z33syi")))

(define-public crate-cli-rng-0.1.4 (c (n "cli-rng") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1lyj92xv98f3rzzf0l8hs72hgiz61ys9cnqgkv30ddx77lkx3vfk")))

(define-public crate-cli-rng-0.1.5 (c (n "cli-rng") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11swg7l1dldrxfg4qzsz1dg9mrh539cs4kc3vlls8ainkk7apbqa")))

(define-public crate-cli-rng-0.1.6 (c (n "cli-rng") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ghx0h2c566pfss31brr7h26zb5bsqbfpm41a7z6z13srwxxs2lf")))

(define-public crate-cli-rng-0.1.7 (c (n "cli-rng") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qhfg1rzj953lvq1rzqv1d297hvzkp16463q1h54a09k26150zj2")))

(define-public crate-cli-rng-0.1.8 (c (n "cli-rng") (v "0.1.8") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "184f02w715iqg1hqnmfqng8zbpzwhh9358xw9wr7lbkm3ri7wg98")))

(define-public crate-cli-rng-0.1.9 (c (n "cli-rng") (v "0.1.9") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ljiqa6p242p2gsln27hy56xwvrxaf4gq2b52c7ck3g56f4mymf2")))

(define-public crate-cli-rng-0.1.10 (c (n "cli-rng") (v "0.1.10") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01c2draqyriph7bizvd9b2pzhpkqdrn75bpbvjp5lf2i96ngm348")))

