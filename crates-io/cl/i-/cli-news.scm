(define-module (crates-io cl i- cli-news) #:use-module (crates-io))

(define-public crate-cli-news-0.1.0 (c (n "cli-news") (v "0.1.0") (d (list (d (n "colour") (r "^0.6.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (d #t) (k 0)))) (h "14rdnd2awnilzpw51dsyawpzsn1b7kxkf3aw29yhcjp9whcs1qk5") (y #t) (r "1.56")))

