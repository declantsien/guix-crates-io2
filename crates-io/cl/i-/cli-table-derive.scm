(define-module (crates-io cl i- cli-table-derive) #:use-module (crates-io))

(define-public crate-cli-table-derive-0.4.0 (c (n "cli-table-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16w19yilvbp3brdln57062j9pap7fd2vzs02966z730dj9gpkqsy")))

(define-public crate-cli-table-derive-0.4.1 (c (n "cli-table-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ib5zlnnyq8wn4k7510p2fh5v23hgrh9fip5k4i23i6kqbg7hp65")))

(define-public crate-cli-table-derive-0.4.2 (c (n "cli-table-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yxksjsnhx33ax9r69zga0gjpbxmdxqplz0p8p00zli93g1xl8bm")))

(define-public crate-cli-table-derive-0.4.3 (c (n "cli-table-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "164k1pb02x90rgcxc7gsf3676s8l68yv2g5ghddikpbabggdbyr4")))

(define-public crate-cli-table-derive-0.4.4 (c (n "cli-table-derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06hla0c5dmaxmx7mn6xjpgz5aic8ha0lxvxw3jcib3h65r8l5sgg")))

(define-public crate-cli-table-derive-0.4.5 (c (n "cli-table-derive") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1m4sh8z0b8q8bhxljdfl9rvk654jcdwzn93n8rn0lyv2vawvzwra")))

