(define-module (crates-io cl p- clp-lib) #:use-module (crates-io))

(define-public crate-clp-lib-0.1.0 (c (n "clp-lib") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1h7wx929ns426709386ba5qzabw1gf0nm5sf4xhb108zxj3v2rsx")))

(define-public crate-clp-lib-0.1.1 (c (n "clp-lib") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "16xz1dy29sl338yarggd0c14b0pd85c2sjj9xqpif8g63f1rjinq")))

