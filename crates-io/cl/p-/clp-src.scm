(define-module (crates-io cl p- clp-src) #:use-module (crates-io))

(define-public crate-clp-src-0.2.0+1.17.7 (c (n "clp-src") (v "0.2.0+1.17.7") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)) (d (n "osi-src") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1nmngglc7106shhkk8d8hd64hp15cb8dlyf179ja0r5kfhb58w19") (f (quote (("osiclp" "osi-src") ("default" "osiclp" "clpsolver") ("clpsolver")))) (l "Clp")))

(define-public crate-clp-src-0.2.1+1.17.7 (c (n "clp-src") (v "0.2.1+1.17.7") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)) (d (n "osi-src") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0x8hr1s1yrya2nyij3ih1qzl9xqpygbga2d6vmx7k1zb8z7hdrv0") (f (quote (("osiclp" "osi-src") ("default" "osiclp" "clpsolver") ("clpsolver")))) (l "Clp")))

(define-public crate-clp-src-0.2.2+1.17.7 (c (n "clp-src") (v "0.2.2+1.17.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)) (d (n "osi-src") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1waax5dj6nw7m9lz7q5wn7hqpq2ig36wv83dlx8g13rdz44xr4wr") (f (quote (("osiclp" "osi-src") ("default" "osiclp") ("clpsolver")))) (l "Clp")))

(define-public crate-clp-src-0.2.4+1.17.8 (c (n "clp-src") (v "0.2.4+1.17.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)) (d (n "osi-src") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1sq02dvdjkx6rnhasp7d14gblm2g4pj9ing1i2rplyyk0wc991qx") (f (quote (("osiclp" "osi-src") ("default" "osiclp") ("clpsolver")))) (l "Clp")))

(define-public crate-clp-src-0.2.5+1.17.9 (c (n "clp-src") (v "0.2.5+1.17.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)) (d (n "osi-src") (r "^0.2") (o #t) (d #t) (k 0)))) (h "033ms66678z2q5bbjpf07229qizpn7a60a4frm6p6iynnwiaqrbl") (f (quote (("osiclp" "osi-src") ("default" "osiclp") ("clpsolver")))) (l "Clp")))

(define-public crate-clp-src-0.2.6+1.17.9 (c (n "clp-src") (v "0.2.6+1.17.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)) (d (n "osi-src") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0rysmrik8bmmg9gx90r1csm0jrfjyj7z6qdics3hrqyd01b4axra") (f (quote (("osiclp" "osi-src") ("default" "osiclp") ("clpsolver")))) (l "Clp")))

(define-public crate-clp-src-0.2.7+1.17.9 (c (n "clp-src") (v "0.2.7+1.17.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)) (d (n "osi-src") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1mrlfgwpb19d20njrlfs3g3x36kjqhrg6l3rqga3zxwmjgx3wf0p") (f (quote (("osiclp" "osi-src") ("default" "osiclp") ("clpsolver")))) (l "Clp")))

