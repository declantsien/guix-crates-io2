(define-module (crates-io cl ut clutch) #:use-module (crates-io))

(define-public crate-clutch-0.2.0 (c (n "clutch") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "blstrs") (r "^0.6.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (d #t) (k 0)) (d (n "fcomm") (r "^0.2.0") (d #t) (k 0)) (d (n "lurk") (r "^0.2.0") (d #t) (k 0)) (d (n "pasta_curves") (r "^0.5.2") (f (quote ("repr-c" "serde"))) (d #t) (k 0) (p "fil_pasta_curves")) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)))) (h "12ra05apd0bfmcc3x8ap4pljhqz8qlr1fv2vf927wwsg4syj2nly")))

