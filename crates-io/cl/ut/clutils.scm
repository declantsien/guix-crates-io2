(define-module (crates-io cl ut clutils) #:use-module (crates-io))

(define-public crate-clutils-0.1.0 (c (n "clutils") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "16n897xv4106sihnwf5wn38dg45hs705p41hz77ay5x7zm2sqfzl") (y #t)))

(define-public crate-clutils-0.0.1 (c (n "clutils") (v "0.0.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "12mr4hq8g93kx7ivdwnn8arq4q9jws5r5bvr0m38kcj7kkb6d0yn")))

(define-public crate-clutils-0.0.2 (c (n "clutils") (v "0.0.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "112q15jgzckrr3qhx8jfj5qv5y2v4m9bicqhz744zm04bsw3dfaj")))

(define-public crate-clutils-0.0.3 (c (n "clutils") (v "0.0.3") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "00vb8pnqgmws8glg2zdn00hkhw2m5mk4mpifpbw62nzv40k73izj")))

(define-public crate-clutils-0.0.4 (c (n "clutils") (v "0.0.4") (h "11l5cs4c8rfpf9w7mrqif383ipjlddck1jghwcp0nyccd7s0csbs")))

(define-public crate-clutils-0.0.5 (c (n "clutils") (v "0.0.5") (h "099n3657djxiy1p9dcp88psb48l8d8fk80030v8bm11sn9wbqwvs")))

(define-public crate-clutils-0.0.6 (c (n "clutils") (v "0.0.6") (h "1s49kyaf8fmmkkj8kjnh32amc5mmk9pipin91bfypmdn18bc4za4")))

(define-public crate-clutils-0.0.7 (c (n "clutils") (v "0.0.7") (h "03yb4707lzkfdcf4pa7i4qh5yi53izrxv3ss2myzyabavz85rkrs")))

(define-public crate-clutils-0.0.8 (c (n "clutils") (v "0.0.8") (h "0f8hqqxbygq3dl5d36plfmc8py3la006rnvnxjc5vvy7yzivwjyc")))

(define-public crate-clutils-0.0.9 (c (n "clutils") (v "0.0.9") (h "135hlgv13sk2jf7h7cxvv0cr1s9r8823ic1wkybkvzi8apmmaky6")))

(define-public crate-clutils-0.0.10 (c (n "clutils") (v "0.0.10") (h "0bcm561mma62hkcpgj2yaxlh7kqzzrcz258b0l43awj2c60v45ix")))

(define-public crate-clutils-0.0.11 (c (n "clutils") (v "0.0.11") (h "07ixnxnpzjj5hc7jm1ara4fj05xmhj45i2ksk9ycn2a8pmwlx1c8")))

