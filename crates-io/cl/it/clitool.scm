(define-module (crates-io cl it clitool) #:use-module (crates-io))

(define-public crate-clitool-0.1.4 (c (n "clitool") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1mz0b8ggfhyp4gic9hw21c70712d8fh215bvmgczhnxkkzry8www")))

(define-public crate-clitool-0.1.5 (c (n "clitool") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0785n8f5gb6iywrs18i1daaqn89jfg33r3r125ilksy9wif8kxm8")))

(define-public crate-clitool-0.2.0 (c (n "clitool") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0a9zvf2bfd8ijymaq8b5k85m21lqxbm7jwscv0xzqcjk6vqpirv3")))

(define-public crate-clitool-0.2.1 (c (n "clitool") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1gr7zl464961fnswmvsf0bhispvxy1swjxjkhv4xd17dbf64cgdz")))

(define-public crate-clitool-0.2.2 (c (n "clitool") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1kdag7gbjwzs8d4f6j1apbmk175d9g4xfi38jl7fmxipqmw37yq6")))

(define-public crate-clitool-0.2.3 (c (n "clitool") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1jc72crm3jc2y43lwq8by5ny6wbi671jwhx5wxh6440ka0r6d4p4")))

