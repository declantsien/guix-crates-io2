(define-module (crates-io cl il clilog) #:use-module (crates-io))

(define-public crate-clilog-0.1.0 (c (n "clilog") (v "0.1.0") (h "0rvr68lmzx1czlxsfc4wq40wvddmcj0gbmw0ajsrvykbgxxd64v4")))

(define-public crate-clilog-0.2.0 (c (n "clilog") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "logging_timer") (r "^1.1.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0zrbf7hfazqaxsarmr2ysmnxia4z6adzsn8qgc2liwabslds3w3v")))

(define-public crate-clilog-0.2.1 (c (n "clilog") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "logging_timer") (r "^1.1.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0zfxn0vvg7gzdzbjw71dlwsr4fkgamlw3q7s8g1jhnn942i7p5in")))

(define-public crate-clilog-0.2.2 (c (n "clilog") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "01mb5gnjg6xqy2sa8lwql3dlbdsa3v43bg9ivx7jribwyv8z2bfs")))

(define-public crate-clilog-0.2.3 (c (n "clilog") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "1z528mq2m5dvxsva0ljr4c2i2wq47sn5rns9asj1w92dd162n771")))

