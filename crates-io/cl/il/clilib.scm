(define-module (crates-io cl il clilib) #:use-module (crates-io))

(define-public crate-clilib-0.1.0 (c (n "clilib") (v "0.1.0") (h "1zijgrn117pmhc64p060xyf715dgb2da6pj0psr2kp597p7mi5wg")))

(define-public crate-clilib-0.1.1 (c (n "clilib") (v "0.1.1") (h "0lgwq166qidjvha95j2rdp1xhl4rh0xpk5ikkyvfb7fwxmf9c322")))

(define-public crate-clilib-0.1.2 (c (n "clilib") (v "0.1.2") (h "0ahg933fmk7d1c1iis46xkzgj1mrqcsqq24676cx16n8rk1akvi3")))

(define-public crate-clilib-0.1.3 (c (n "clilib") (v "0.1.3") (h "0zz57hw6fv2fi9hvyz6impjavqpvbj8mvjanpq7jwywj5nxckys9")))

