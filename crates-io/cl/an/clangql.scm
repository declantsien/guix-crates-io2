(define-module (crates-io cl an clangql) #:use-module (crates-io))

(define-public crate-clangql-0.1.0 (c (n "clangql") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clang-sys") (r "^1.7.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.15.0") (d #t) (k 0)) (d (n "gitql-cli") (r "^0.17.0") (d #t) (k 0)) (d (n "gitql-engine") (r "^0.17.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.16.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0wwmdbs430rwk0bigjy2jmfimm13f45zn851ky6226vxysfzwncl")))

(define-public crate-clangql-0.2.0 (c (n "clangql") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clang-sys") (r "^1.7.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.16.0") (d #t) (k 0)) (d (n "gitql-cli") (r "^0.18.0") (d #t) (k 0)) (d (n "gitql-engine") (r "^0.18.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.17.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "002n7ga00czp0ss68xcj3wfzdhcp27lvjqh5f1kqpqrm81xcwq63")))

(define-public crate-clangql-0.3.0 (c (n "clangql") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clang-sys") (r "^1.7.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.16.0") (d #t) (k 0)) (d (n "gitql-cli") (r "^0.18.0") (d #t) (k 0)) (d (n "gitql-engine") (r "^0.18.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.17.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "16lyz4d8s2m909xa249442bdv8d8b7fk9p5r0dwawc6r3axll52v")))

