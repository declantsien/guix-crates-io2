(define-module (crates-io cl an clang-ast) #:use-module (crates-io))

(define-public crate-clang-ast-0.1.0 (c (n "clang-ast") (v "0.1.0") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "157k3lgx4v49qrw20m3iyry57bq2q8535mwqc5m1m250501z5886")))

(define-public crate-clang-ast-0.1.1 (c (n "clang-ast") (v "0.1.1") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0g0mrzq0hml9skch3l3pk0bx2y9s6xn650dm17wh4sja9z4whwjk")))

(define-public crate-clang-ast-0.1.2 (c (n "clang-ast") (v "0.1.2") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0s07rfq2arfv44g9018rc016z7p1h2525p4nmp7nlk09sgfl4hr9")))

(define-public crate-clang-ast-0.1.3 (c (n "clang-ast") (v "0.1.3") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qjfc9g4c8q6z2492khd41n9gh4nbxxc4y212kiky0rlsz69prkb")))

(define-public crate-clang-ast-0.1.4 (c (n "clang-ast") (v "0.1.4") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fw0jnf6bd1p2w83w9wr3aq7ms06h14k4xkidy2cp7nk8015w8bw")))

(define-public crate-clang-ast-0.1.5 (c (n "clang-ast") (v "0.1.5") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vnh7k250kqan9scpw5x71qa2pkd874wbpr9syhb1wb025n9gr05")))

(define-public crate-clang-ast-0.1.6 (c (n "clang-ast") (v "0.1.6") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1sfqyxszas78s78nga88fl0i5qlr87qsj22vlxarhvx96q86impf")))

(define-public crate-clang-ast-0.1.7 (c (n "clang-ast") (v "0.1.7") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0c78fwpazfxazdbphhq9hbqgarzmfpmhj6y9vw3p0cs8vvh81hyr")))

(define-public crate-clang-ast-0.1.8 (c (n "clang-ast") (v "0.1.8") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "07ynsc8zlbk06nccr6ry731c3wn5c8sh6rlzmh93c2fjx7ix5kx4")))

(define-public crate-clang-ast-0.1.9 (c (n "clang-ast") (v "0.1.9") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ynhg6nhjkz1nfrsa6drqzml1n790nv6ic80cn9pk87ks9ymdv7j")))

(define-public crate-clang-ast-0.1.10 (c (n "clang-ast") (v "0.1.10") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0l74mgfzi1jwalw9fh86qprkyvx3hcha0fhs9qphy7jhxcnpdqzz")))

(define-public crate-clang-ast-0.1.11 (c (n "clang-ast") (v "0.1.11") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ln683pq7idabknmrz3hhfn7am5imx2awkgs1495frmwm8d7qfgy")))

(define-public crate-clang-ast-0.1.12 (c (n "clang-ast") (v "0.1.12") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1hvfcqyvkcy4ppcvqzn39106kzz8yrz2sf6xa0v322dsg2hkkvfy")))

(define-public crate-clang-ast-0.1.13 (c (n "clang-ast") (v "0.1.13") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "070gbkaqza40as9kcgyl0y6syabpvrnf8y3wg9imq5zkvavfv9jj")))

(define-public crate-clang-ast-0.1.14 (c (n "clang-ast") (v "0.1.14") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1frac959h65bw2ah39y1xagpbpjfbz69c5rnca767sx2kb0nns62")))

(define-public crate-clang-ast-0.1.15 (c (n "clang-ast") (v "0.1.15") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "15fg3ijy3pjqv3c0sbfspziqq03swjyfwbsi1dl2mmp0vbw2xkrk")))

(define-public crate-clang-ast-0.1.16 (c (n "clang-ast") (v "0.1.16") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zlqbk30ik07nfm9zrwz57rarnp7hc11p4y1dingv3v5phiqh6ph")))

(define-public crate-clang-ast-0.1.17 (c (n "clang-ast") (v "0.1.17") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0r5r7w7894fvk7z64r1mz01y5sp2p7wwc58zd7kjz36ddih3miq2")))

(define-public crate-clang-ast-0.1.18 (c (n "clang-ast") (v "0.1.18") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1xvyc53mp83vh0bgrw75rlyriz4bs6hdd560d15n6zjyww81xc5c")))

(define-public crate-clang-ast-0.1.19 (c (n "clang-ast") (v "0.1.19") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "0dm3hd4j60f7yqld1x5xs75kl0awvlgj92g9whw6fm5x1qmpyz57")))

(define-public crate-clang-ast-0.1.20 (c (n "clang-ast") (v "0.1.20") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "04qf9kpc2r3ca7c5x8bjp1daj42k69q12q8mz7bhajbm9rpafb8m")))

(define-public crate-clang-ast-0.1.21 (c (n "clang-ast") (v "0.1.21") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "00j70gfcv6bngzx4whs1vdjn8sfz7llqjbp61vh2gb7jf67r5z8m")))

(define-public crate-clang-ast-0.1.22 (c (n "clang-ast") (v "0.1.22") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0.194") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.194") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1rfh6v0dm9z2cz3fh8gqrz69amcnkmwzw7cjc2zxjycf5vgj1q6b")))

(define-public crate-clang-ast-0.1.23 (c (n "clang-ast") (v "0.1.23") (d (list (d (n "clang-ast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0.194") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.194") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1c22z6752whf8mx4pij51dgsa0a3hplviks5vfnd84zky8svq63y")))

