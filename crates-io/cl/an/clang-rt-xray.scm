(define-module (crates-io cl an clang-rt-xray) #:use-module (crates-io))

(define-public crate-clang-rt-xray-0.0.1 (c (n "clang-rt-xray") (v "0.0.1") (d (list (d (n "clang-rt-xray-sys") (r "^0.0.1") (k 0)))) (h "134ggi0790nd4jz1j20dws5sfl5zigpxl4g6s2fx884i4g59rh7l") (f (quote (("std") ("fdr" "clang-rt-xray-sys/fdr") ("default" "std" "basic" "fdr") ("basic" "clang-rt-xray-sys/basic")))) (r "1.69.0")))

