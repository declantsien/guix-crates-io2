(define-module (crates-io cl an clang-typecheck) #:use-module (crates-io))

(define-public crate-clang-typecheck-0.1.0 (c (n "clang-typecheck") (v "0.1.0") (d (list (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.1") (d #t) (k 0)))) (h "0fwq3bvdp4q66k3bpgdkqi57r32b94niyixxywxjvib97vj8filw") (y #t)))

