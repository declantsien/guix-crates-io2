(define-module (crates-io cl an clang-rt-xray-sys) #:use-module (crates-io))

(define-public crate-clang-rt-xray-sys-0.0.1 (c (n "clang-rt-xray-sys") (v "0.0.1") (h "0zim5g3yhi776rw3n4cv6kihwpr35b24b8mww27zi1k7aady1fak") (f (quote (("fdr") ("default" "basic" "fdr") ("basic")))) (l "clang_rt.xray") (r "1.69.0")))

