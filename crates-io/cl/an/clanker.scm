(define-module (crates-io cl an clanker) #:use-module (crates-io))

(define-public crate-clanker-0.1.0 (c (n "clanker") (v "0.1.0") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.7.11") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "whoami") (r "^0.4.1") (d #t) (k 0)))) (h "1dn9jr5j5y33cz5gxn3g55i6zysp5xx35nsg9wkx23kqx2qd8301")))

(define-public crate-clanker-0.1.1 (c (n "clanker") (v "0.1.1") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.7.11") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "whoami") (r "^0.4.1") (d #t) (k 0)))) (h "0ks921jj91l8xb1y25mhfpkv1wfk8qfj2dcv0jb2y3477ky1rk4h")))

(define-public crate-clanker-0.1.2 (c (n "clanker") (v "0.1.2") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.7.11") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "whoami") (r "^0.4.1") (d #t) (k 0)))) (h "19cb3npadp4jbccimrq8vrh9mq2ph1ah0q015dkr85h6d5siqwr6")))

(define-public crate-clanker-0.2.0 (c (n "clanker") (v "0.2.0") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.7.11") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "whoami") (r "^0.4.1") (d #t) (k 0)))) (h "18ck8z82bpca9nx7zcclavrp2s1gvcydpgfq0857la1yvd72410v")))

(define-public crate-clanker-0.2.1 (c (n "clanker") (v "0.2.1") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.7.11") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "whoami") (r "^0.4.1") (d #t) (k 0)))) (h "09gkbzadmwjznxp75znralr7a1vidnbpmp0ssd9avfkrznk30b1p")))

(define-public crate-clanker-0.3.0 (c (n "clanker") (v "0.3.0") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "whoami") (r "^0.4.1") (d #t) (k 0)))) (h "11yh2si1qz1jly6b52y225m27am9swz2g1fdygcv2c5a96plnwnl")))

(define-public crate-clanker-0.3.1 (c (n "clanker") (v "0.3.1") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "whoami") (r "^0.4.1") (d #t) (k 0)))) (h "083xn3lmxqp2jkndknb0kjjndvnkiwczzlbywc6aplcnpsm18417")))

(define-public crate-clanker-0.4.0 (c (n "clanker") (v "0.4.0") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "whoami") (r "^0.4.1") (d #t) (k 0)))) (h "1v6q8hgpwz6yd1glryhzs5v8yfcpvhp08jl0jr4awmpk1drvmpv0")))

(define-public crate-clanker-0.4.1 (c (n "clanker") (v "0.4.1") (d (list (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0mwf71wkal6hbfaizvmqd2xw4ghzfig2bppv88ys96gljkvcs2bi")))

(define-public crate-clanker-0.4.2 (c (n "clanker") (v "0.4.2") (d (list (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.7.11") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0pyqh0k9jd33mhcg85mj3sjhrxwf9s05s7f8wvghjii70nkqbg4j")))

(define-public crate-clanker-0.4.3 (c (n "clanker") (v "0.4.3") (d (list (d (n "libc") (r "^0.2.49") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.7.11") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0kzf0rb6r1n5hwrx2d0brh4fry8jkpchkhxyb3q963xwg5pkg8a2")))

(define-public crate-clanker-0.5.0 (c (n "clanker") (v "0.5.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 0)))) (h "1bqdj8jkaa9ka8wnpsy739jyq98h9fwb4yiv49jl9az17g6h38ap")))

(define-public crate-clanker-0.5.1 (c (n "clanker") (v "0.5.1") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 0)))) (h "1hmmjzsmn0ry3r12smazgkflqinbzd8sfg55k5ym4gkxdzy6igly")))

(define-public crate-clanker-0.5.2 (c (n "clanker") (v "0.5.2") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 0)))) (h "0mgnh8ib1qyxcax0p62c9vhp6zx5mnyk5xksxm6mjzcy4ni9jghg")))

(define-public crate-clanker-0.5.3 (c (n "clanker") (v "0.5.3") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 0)))) (h "0kcx0j47nv6hah8d55flgqvnw41i7salpp63a81xwdwk08r1yqs0")))

(define-public crate-clanker-0.6.0 (c (n "clanker") (v "0.6.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.12.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "08r5kpp2qiagqy1a54g5wskjinq6apymdgn4ixm85z53xn82p9ma")))

(define-public crate-clanker-0.7.0 (c (n "clanker") (v "0.7.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "libgit2-sys") (r "^0.12.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1a02nh68bckcqq4xqg0pgn7xcp0av30v4h562lzz8h7nhdck1nf5")))

