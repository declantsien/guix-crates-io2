(define-module (crates-io cl an clangd-parser) #:use-module (crates-io))

(define-public crate-clangd-parser-0.1.0 (c (n "clangd-parser") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "griff") (r "^0.1.0") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "00jj3zf77bjyz2ppgwjk58h8yl30m4qvihq45fqhnjz56j56xrlh")))

(define-public crate-clangd-parser-0.1.1 (c (n "clangd-parser") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "griff") (r "^0.1.0") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "0hc54rvix6lbmpm6i691w09k624g6xfcknrf2dd8wbz5lmr7kf9p")))

(define-public crate-clangd-parser-0.1.2 (c (n "clangd-parser") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "griff") (r "^0.1.0") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "09vjfqp7bwyazsm8kjdwa9sp5y6gi4fzq9drngjfzmadkqm1f7nh")))

(define-public crate-clangd-parser-0.1.3 (c (n "clangd-parser") (v "0.1.3") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "griff") (r "^0.1.0") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "0w1yiyfahqg0fj2vhzwgrks9wpv602h1rbziykhnbxgm9n825agr") (f (quote (("post-process"))))))

