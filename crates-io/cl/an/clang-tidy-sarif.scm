(define-module (crates-io cl an clang-tidy-sarif) #:use-module (crates-io))

(define-public crate-clang-tidy-sarif-0.2.19 (c (n "clang-tidy-sarif") (v "0.2.19") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.19") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1al850amd7qwy1b173yv81z71zvpjhkhkpw082j9pm6ckgyhhhwg")))

(define-public crate-clang-tidy-sarif-0.2.20 (c (n "clang-tidy-sarif") (v "0.2.20") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.20") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1xdpalv306x1qn6sslb8jcfky63as9w14r542rjiz63s76xf91av")))

(define-public crate-clang-tidy-sarif-0.2.21 (c (n "clang-tidy-sarif") (v "0.2.21") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.7") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.21") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "06yagi8bkmjyi536ayjrq1mrfipwnaq2qizh8kc7b9ywihsczy7a")))

(define-public crate-clang-tidy-sarif-0.2.22 (c (n "clang-tidy-sarif") (v "0.2.22") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.22") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "165sp14brvlhlpiqn8yvjsh86crvrrz869nif75bakxhbd1j102l")))

(define-public crate-clang-tidy-sarif-0.2.24 (c (n "clang-tidy-sarif") (v "0.2.24") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.24") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0d6rvnvfwc20x1krj66g14n7fg79xrnhxafxnfkvlszqhv29my51")))

(define-public crate-clang-tidy-sarif-0.2.25 (c (n "clang-tidy-sarif") (v "0.2.25") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.25") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1a6bncqfdjwrz1fkl03slrric3p7yv0imhglxzis5hs0lz05yfj2")))

(define-public crate-clang-tidy-sarif-0.3.0 (c (n "clang-tidy-sarif") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.10") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.0") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "00gv65rmcl8rxm2lczlyyadxz2a81d37q9y7xka9mxy6f2chkm37")))

(define-public crate-clang-tidy-sarif-0.3.1 (c (n "clang-tidy-sarif") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.1") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1xrvghww6jvm4wcwis76bzc03j5llw4mz0y7isj8kmhzvcgc69x3")))

(define-public crate-clang-tidy-sarif-0.3.2 (c (n "clang-tidy-sarif") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.2") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "18wvz60qlppvy5r02raypzjdzyi5i91ix73c42s7qs6ba06a747c")))

(define-public crate-clang-tidy-sarif-0.3.3 (c (n "clang-tidy-sarif") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.3") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0g56k1bd37pr474f1r91y7ygpnhylj5f8880g6zljalrksc236yb")))

(define-public crate-clang-tidy-sarif-0.3.4 (c (n "clang-tidy-sarif") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.4") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1gfx21lh2720gjniq6551wscz4yk2rdnas9aqgz7xb51zg05mkgy")))

(define-public crate-clang-tidy-sarif-0.3.5 (c (n "clang-tidy-sarif") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.5") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "00nx7hpcc284acq9vcgdip3pknjljd2kcma5yb3p457lriz9d4gf")))

(define-public crate-clang-tidy-sarif-0.3.6 (c (n "clang-tidy-sarif") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.6") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0cj4v2kbkyvlfc7frnmsm8894117fvvawmyffrk5xdh7hsm9hzqm")))

(define-public crate-clang-tidy-sarif-0.3.7 (c (n "clang-tidy-sarif") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.7") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1am4c3jyysfmfsrjxk4lxr6h3bpa99flwswzjfz04q927n8qm34z")))

(define-public crate-clang-tidy-sarif-0.4.0 (c (n "clang-tidy-sarif") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.0") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0wj3alsg06fm3h18j46ahsfxc0sj4aylrdhyyyvqbhq4n9fs8mw7")))

(define-public crate-clang-tidy-sarif-0.4.1 (c (n "clang-tidy-sarif") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.1") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0qpniam1sfd7d1989hxvd9i88zg39d9fbndl519dld2cymhhn8ny")))

(define-public crate-clang-tidy-sarif-0.4.2 (c (n "clang-tidy-sarif") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.2") (f (quote ("clang-tidy-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0vq9fixk9cx9ksdvvb2mv38hk6ljj92q2fl0v1rrdzzrqa40hp5j")))

