(define-module (crates-io cl an clang-format) #:use-module (crates-io))

(define-public crate-clang-format-0.1.0 (c (n "clang-format") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)))) (h "0q5xzcms37ndj2n6yj2541dm7l04wjkzm7pqiw0kmi5a95clcm14")))

(define-public crate-clang-format-0.1.1 (c (n "clang-format") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)))) (h "1blk5kxk96xrk77axrfvkc6jla9c7zkijji1grlr1665lnrrcsvw")))

(define-public crate-clang-format-0.1.2 (c (n "clang-format") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)))) (h "0ghj7byh23am0rnkjmxd39qr4vn23drn92dwrz93miap9pcb6sj8")))

(define-public crate-clang-format-0.1.3 (c (n "clang-format") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)))) (h "0jlzdg6q0gpinm93n0w5vcglnhc8fyrmwfc0sxqsrr5fh6m4jsiw")))

(define-public crate-clang-format-0.2.0 (c (n "clang-format") (v "0.2.0") (h "0x9ibs3z1jkrrxg9fdbk3d23c4mp1nvqz9pwmfbfcrp0fpmybpw8")))

(define-public crate-clang-format-0.3.0 (c (n "clang-format") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hwr869fr1956x47rd6y5sb1c7ajyvjr4jv1xq4d4f8s1ss86qk9")))

