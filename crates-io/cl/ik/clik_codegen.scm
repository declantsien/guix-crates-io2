(define-module (crates-io cl ik clik_codegen) #:use-module (crates-io))

(define-public crate-clik_codegen-0.1.0 (c (n "clik_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r9qqkb5xpj7snv5lm1jscis17k4hbn662j6z92czq4j4j98fwz6")))

(define-public crate-clik_codegen-0.1.1 (c (n "clik_codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jycpglzjm5ba6qcm1wg3g9nvslmgg3sr497glxx3aml6mi7wqvz")))

