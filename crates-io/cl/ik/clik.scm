(define-module (crates-io cl ik clik) #:use-module (crates-io))

(define-public crate-clik-0.0.1 (c (n "clik") (v "0.0.1") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 2)))) (h "1gj8xm9nxibm9knbyx25xv8nfr6rb5mlnk4sx66335wrwk9xa49b")))

(define-public crate-clik-0.1.0 (c (n "clik") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 2)))) (h "03ngr9377hyhy6ari2hxf1wb9i0myvkzba0ryq6zbhnmnv3bi8cj") (y #t) (s 2) (e (quote (("async" "dep:async-recursion"))))))

(define-public crate-clik-0.1.1 (c (n "clik") (v "0.1.1") (d (list (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 2)))) (h "1azkyzcbf5jjkd3cifkj07lawi0dndgj48bmxgwijp559fbb7vd4") (s 2) (e (quote (("async" "dep:async-recursion"))))))

(define-public crate-clik-0.1.2 (c (n "clik") (v "0.1.2") (d (list (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "18770ai2rpv7rcjbsrzhr0m05s0j97bqiwzz4jps389pzk3ami0i") (s 2) (e (quote (("async" "dep:async-recursion"))))))

(define-public crate-clik-0.1.3 (c (n "clik") (v "0.1.3") (d (list (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "04aia94vgy9nj83c804kqxpv5yprjjpdw88xxzbjnn1kjhzd92vr") (s 2) (e (quote (("async" "dep:async-recursion"))))))

(define-public crate-clik-0.2.0 (c (n "clik") (v "0.2.0") (d (list (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "clik_codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1wak5xi8clxn22f1fgip5s4bv7fzl04a3q3dnfpswlwj29jjbpjq") (s 2) (e (quote (("async" "dep:async-recursion"))))))

(define-public crate-clik-0.2.1 (c (n "clik") (v "0.2.1") (d (list (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "clik_codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1j16f5hny80qd88brav3xyz3592v5i0gxl1a5p6x2p72c2sv08jz") (s 2) (e (quote (("async" "dep:async-recursion"))))))

