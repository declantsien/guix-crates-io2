(define-module (crates-io cl oc clockwork-anchor-gen) #:use-module (crates-io))

(define-public crate-clockwork-anchor-gen-0.3.1 (c (n "clockwork-anchor-gen") (v "0.3.1") (d (list (d (n "clockwork-anchor-generate-cpi-crate") (r "^0.3.0") (d #t) (k 0)) (d (n "clockwork-anchor-generate-cpi-interface") (r "^0.3.0") (d #t) (k 0)))) (h "0bi9vyi3sj4h7ywak236grwlbk6h21pzcqpxa0xqwscbkyjqwj9n") (f (quote (("compat-program-result" "clockwork-anchor-generate-cpi-crate/compat-program-result" "clockwork-anchor-generate-cpi-interface/compat-program-result"))))))

(define-public crate-clockwork-anchor-gen-0.3.2 (c (n "clockwork-anchor-gen") (v "0.3.2") (d (list (d (n "clockwork-anchor-generate-cpi-crate") (r "^0.3.2") (d #t) (k 0)) (d (n "clockwork-anchor-generate-cpi-interface") (r "^0.3.2") (d #t) (k 0)))) (h "16mw9rgr280awfbh9blsw2s2w87j42z1g41dwdai89hs230vayxz") (f (quote (("compat-program-result" "clockwork-anchor-generate-cpi-crate/compat-program-result" "clockwork-anchor-generate-cpi-interface/compat-program-result"))))))

