(define-module (crates-io cl oc clock_source) #:use-module (crates-io))

(define-public crate-clock_source-0.1.0 (c (n "clock_source") (v "0.1.0") (h "0ys0z0kh7ab5r8ibyvh55hcfnkipchv36z5wmksidrp7mf9w63rm") (f (quote (("std") ("default" "std"))))))

(define-public crate-clock_source-0.1.1 (c (n "clock_source") (v "0.1.1") (h "0chgmcg1q765gn7c0jjhm0prr4bw7a47gd94jdxv8khzxsva771c") (f (quote (("std") ("default" "std"))))))

(define-public crate-clock_source-0.2.0 (c (n "clock_source") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0v57kl4fg719i3xvw4sb9ina6cnqr9i1m49cw8dqgn3hlx3hidks") (f (quote (("std") ("default" "std") ("custom"))))))

(define-public crate-clock_source-0.2.1 (c (n "clock_source") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0lxjnsirr5y6sxf0qr5kzz8ixm9mkkf2mbbq7gkvyjyih1spcf82") (f (quote (("std") ("default" "std") ("custom"))))))

(define-public crate-clock_source-0.2.2 (c (n "clock_source") (v "0.2.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0wbv3hm81pn0wc6k4kn564aqbpm8zizjxvqvjk0kvrn464r9z2dy") (f (quote (("std") ("default" "std") ("custom"))))))

(define-public crate-clock_source-0.2.3 (c (n "clock_source") (v "0.2.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "time-clock") (r "^0.1") (d #t) (k 0)))) (h "192rnab4p613kg8zyh719sxk9frld7fd3kdvsy0432bq28cz10fp") (f (quote (("std") ("default" "std") ("custom"))))))

(define-public crate-clock_source-0.2.4 (c (n "clock_source") (v "0.2.4") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "time-clock") (r "^0.1") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)))) (h "00yw6d80r6vk47s9wzmn9s081z60kpd17yaal96aagqs15j6hisn") (f (quote (("default") ("custom"))))))

