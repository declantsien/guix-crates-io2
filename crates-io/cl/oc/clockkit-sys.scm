(define-module (crates-io cl oc clockkit-sys) #:use-module (crates-io))

(define-public crate-clockkit-sys-0.1.0 (c (n "clockkit-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "16yvvbqz6yqciy62x5mbwy92hka8jbikn5d1nqk4vri24wla1fr5") (f (quote (("update_bindings" "make_bindings") ("make_bindings" "bindgen") ("default") ("build_server"))))))

(define-public crate-clockkit-sys-0.1.1 (c (n "clockkit-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "01y7mr7jqrvdhr6a1xzs7kp03cxaq400zgfg3fnladm2ly1q54dk") (f (quote (("update_bindings" "make_bindings") ("make_bindings" "bindgen") ("default") ("build_server"))))))

