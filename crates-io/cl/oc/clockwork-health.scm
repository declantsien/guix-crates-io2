(define-module (crates-io cl oc clockwork-health) #:use-module (crates-io))

(define-public crate-clockwork-health-1.0.0 (c (n "clockwork-health") (v "1.0.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "198lhbm0yhp9ga1qn9db3zqs8ymj5g2w36q7sqjv12l3m93f2c8p") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-health-1.0.1 (c (n "clockwork-health") (v "1.0.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0jq9355k3d7nz6ra3cx74950n7nw1p32fd21kzqrahsj9sjn7px2") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-health-1.0.2 (c (n "clockwork-health") (v "1.0.2") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0zn6mcgj1avqi9kk0pkgv5sk3yg17wc56iv0xg8niv6irdp515z0") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-health-1.0.3 (c (n "clockwork-health") (v "1.0.3") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1zr6sf8fvm6r1yxaaj3cjrxsb123651chndf5h8jr5i10p6336j0") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

