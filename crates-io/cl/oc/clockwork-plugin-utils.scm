(define-module (crates-io cl oc clockwork-plugin-utils) #:use-module (crates-io))

(define-public crate-clockwork-plugin-utils-2.0.16 (c (n "clockwork-plugin-utils") (v "2.0.16") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "=1.14.16") (d #t) (k 0)))) (h "1mviha50r50vxaks69mccgb30kmpq505gfpq0287iayn0ii7apiq")))

(define-public crate-clockwork-plugin-utils-2.0.17 (c (n "clockwork-plugin-utils") (v "2.0.17") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "=1.14.16") (d #t) (k 0)))) (h "1qlxgrl5lw183dhhqxg16yfqbvp725hpfdj6l56i9r2zgygpb5s5")))

(define-public crate-clockwork-plugin-utils-2.0.18 (c (n "clockwork-plugin-utils") (v "2.0.18") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "=1.14.16") (d #t) (k 0)))) (h "0n1vxr29w3y06hz14r679mn41vc0x5jw80jmdy41c75pxw7yib9q")))

(define-public crate-clockwork-plugin-utils-2.0.19 (c (n "clockwork-plugin-utils") (v "2.0.19") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "^1.16") (d #t) (k 0)))) (h "1548rd4cwqf8qd0br6liqzsl8j2sz3lx0rrylxw5zf7g1xify54i")))

