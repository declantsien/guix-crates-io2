(define-module (crates-io cl oc clock-bound-ffi) #:use-module (crates-io))

(define-public crate-clock-bound-ffi-1.0.0 (c (n "clock-bound-ffi") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "clock-bound-shm") (r "^1.0.0") (d #t) (k 0)) (d (n "errno") (r "=0.3.0") (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "08hm8z6f9waqwfy9rpm2v1ih1b0vb28s4gbsbr2fn2i3zggwm78n")))

