(define-module (crates-io cl oc clocked-dispatch) #:use-module (crates-io))

(define-public crate-clocked-dispatch-0.1.0 (c (n "clocked-dispatch") (v "0.1.0") (h "02cyparj5sqbm37wcpfjc1kl1rlas7km33l6r2wwkja5zcjggz72")))

(define-public crate-clocked-dispatch-1.0.0 (c (n "clocked-dispatch") (v "1.0.0") (h "0m49dx72hjxvhmqzrnjyhg43vw1i5jx64a8lxx2r53d5a9pmy39h")))

(define-public crate-clocked-dispatch-1.2.0 (c (n "clocked-dispatch") (v "1.2.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "01xkmw2q27znzvbr2fgahxmi8b8ily9rmgybh8h9hwvns55a0wb6")))

(define-public crate-clocked-dispatch-1.3.0 (c (n "clocked-dispatch") (v "1.3.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "06jcifzqy57c5ly7k8xp51dvmbdwzrdvv33phkzqmcs31rydvg46")))

(define-public crate-clocked-dispatch-1.3.1 (c (n "clocked-dispatch") (v "1.3.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0dz4dpmjwia3m7qpgp7djlrbm19wgaw8wj4z6mfri6j0i4mrwqm0")))

(define-public crate-clocked-dispatch-2.0.0 (c (n "clocked-dispatch") (v "2.0.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0b8pgv0gbrlkwn6ci0qy77yy8yknaf0lwp3lsikqlx97csi532k5")))

(define-public crate-clocked-dispatch-2.0.1 (c (n "clocked-dispatch") (v "2.0.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "00q7fmsxm4bsiqbnzn9inaiw8qmg8rabpfskhayj26qax9ldhlch")))

(define-public crate-clocked-dispatch-2.0.2 (c (n "clocked-dispatch") (v "2.0.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0d28055s9yivb5hpr62gwzs3zifbiykp1fxkj8s20jvsw8hjak6w")))

(define-public crate-clocked-dispatch-2.0.3 (c (n "clocked-dispatch") (v "2.0.3") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "12xj48iwqxp2mv95sa3x55gjwa2j7hqxaaa21aviw20vxcv33d1w")))

(define-public crate-clocked-dispatch-2.1.0 (c (n "clocked-dispatch") (v "2.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "09kvx7gqwib2fsd89y94jv6mp4lg3904mrmww123bdlh6m8fp23z")))

(define-public crate-clocked-dispatch-2.1.1 (c (n "clocked-dispatch") (v "2.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1m4igrnm0v9ax5rxk6zywk3mmn50pmlxgi2sk1v0rm0y89ql42b7")))

(define-public crate-clocked-dispatch-2.1.2 (c (n "clocked-dispatch") (v "2.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0cb5fwxshbhj1gcbvvzxw19qrka60137w8w2cii4rs1vr46whjg9")))

(define-public crate-clocked-dispatch-2.1.3 (c (n "clocked-dispatch") (v "2.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "146q9vw17is5kjs067gfampcyprjb7xbzx2hsi11sin29jn85kci")))

(define-public crate-clocked-dispatch-3.0.0 (c (n "clocked-dispatch") (v "3.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "19hjihqp8iy2qd1qmsprw05f7h4pzcld9361hzfipbkrf3a7y3z8")))

(define-public crate-clocked-dispatch-3.0.1 (c (n "clocked-dispatch") (v "3.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1gjz6z1i0ippiy8wx6g9djr8vqc0kaw3s15rn20vpgxss8k0pzxb")))

(define-public crate-clocked-dispatch-4.0.0 (c (n "clocked-dispatch") (v "4.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0sysmmziysipnkbmk9hb248h9lszhcizix0jkwmh0jkjr3f7zjd2")))

(define-public crate-clocked-dispatch-4.0.1 (c (n "clocked-dispatch") (v "4.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "141h04qsnyaf5lqb72ync98glhmzw2v0jgyjiipk23gq69sd34sg")))

(define-public crate-clocked-dispatch-4.0.2 (c (n "clocked-dispatch") (v "4.0.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0m1nii373jv4kag9azxvlbk1h7mqkarqwh1531mv9i92zd0zzcs9")))

