(define-module (crates-io cl oc clockwork-utils) #:use-module (crates-io))

(define-public crate-clockwork-utils-1.2.2 (c (n "clockwork-utils") (v "1.2.2") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "03f3anm81xzpq8wx21km99aks8fdj0pl2jm6bxaki8kijrgrmg8b")))

(define-public crate-clockwork-utils-1.2.4 (c (n "clockwork-utils") (v "1.2.4") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1ajzrq2jfsjdkbh8zc9l7k02hkxfav2mc52li76wpcbn3pjg9zfj")))

(define-public crate-clockwork-utils-1.2.5 (c (n "clockwork-utils") (v "1.2.5") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1vrsk7vkbhi9krq6jb26iy319vx8xa29dz80nb51iain0dsghv8a")))

(define-public crate-clockwork-utils-1.2.9 (c (n "clockwork-utils") (v "1.2.9") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1shqml2kb3yr3lawcy8hj3dnmimgldgy11zvvlkjbpzr6pzd9hs3")))

(define-public crate-clockwork-utils-1.2.11 (c (n "clockwork-utils") (v "1.2.11") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "04dnis987p03ljbp65s8idldq0c2vbfx7bhb73kdrs3irk409z7f")))

(define-public crate-clockwork-utils-1.2.12 (c (n "clockwork-utils") (v "1.2.12") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "10h4ajl1xr2pjwch324wcvpilkrfrpci4gipqi6hjcpz42ag2wxk")))

(define-public crate-clockwork-utils-1.2.13 (c (n "clockwork-utils") (v "1.2.13") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1zffs70yjqmlfx4py4b1r23qjgr3cp9zvrz7qq5v1z02j9lv4pw9")))

(define-public crate-clockwork-utils-1.2.14 (c (n "clockwork-utils") (v "1.2.14") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "15c4jq74bmaa5rxwbr8s4nrwajr5zd6lwdccm6bcc578lvfwmcdc")))

(define-public crate-clockwork-utils-1.2.15 (c (n "clockwork-utils") (v "1.2.15") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "132777knxhw0if51vsc1rfd85lm28isrg7ymrkn9vs7cbh4ps1jf")))

(define-public crate-clockwork-utils-1.3.0 (c (n "clockwork-utils") (v "1.3.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "16c1y0zk72p7z0gz49wnr9xprs5ivzds98lvcfjhs1i9nr5kampz")))

(define-public crate-clockwork-utils-1.3.1 (c (n "clockwork-utils") (v "1.3.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0m3cgs1axpg55wpqy8sv9gl7r6z8dq3c27dnavf8rsgwl18195ki")))

(define-public crate-clockwork-utils-1.3.5 (c (n "clockwork-utils") (v "1.3.5") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1bxr3mlh19n5nfk3jg1b6zxwkrs0fbf7ybjxz1dk4sggchy5d1x4")))

(define-public crate-clockwork-utils-1.3.6 (c (n "clockwork-utils") (v "1.3.6") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1aa3dzna25x3kd0xvff8i18dchpg5zhgx0bwfnw974f7hv4kvlw2")))

(define-public crate-clockwork-utils-1.3.9 (c (n "clockwork-utils") (v "1.3.9") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0x817wnz1lyj2v90r2yv1y2sys7yllr272w5dlm3n0rpnpkjfvhp")))

(define-public crate-clockwork-utils-1.3.12 (c (n "clockwork-utils") (v "1.3.12") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "08p8q6glz6lkynfgrlvsr4r2gvys1jp9y7mr3vi4jpc7l51w0dxj")))

(define-public crate-clockwork-utils-1.3.16 (c (n "clockwork-utils") (v "1.3.16") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "141fjlckv4nxm48ikprl9gzbmcqdhm2mdya6zw271z214j4mv1gx")))

(define-public crate-clockwork-utils-1.4.0 (c (n "clockwork-utils") (v "1.4.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0f6m6jhf322ns2j5zcd880znpk2nxfs54f5dlg1rc1xrnzqq98w0")))

(define-public crate-clockwork-utils-1.4.2 (c (n "clockwork-utils") (v "1.4.2") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0dyxlvhmsprndzchgr3p5qay9vvx4c0bwl9r6s0pl07k5a5id9i6")))

(define-public crate-clockwork-utils-2.0.0-gamma (c (n "clockwork-utils") (v "2.0.0-gamma") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0m2m1vljz9yg7lmy78334gi9xk7l7vqnzc9qpwga70fzmb9a9gw1")))

(define-public crate-clockwork-utils-2.0.1 (c (n "clockwork-utils") (v "2.0.1") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "03nf8w12brx9ydz3d4xc8flnhi5dkbgzrsj32aqyna2j32bpx055")))

(define-public crate-clockwork-utils-2.0.2 (c (n "clockwork-utils") (v "2.0.2") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0bdi1f6mz8pzcbkgx30lvbdwf37i98k5y78x1vv70zw98mlqbk0d")))

(define-public crate-clockwork-utils-2.0.5 (c (n "clockwork-utils") (v "2.0.5") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0w2wd2rf2ymrjgfk39ngwv3abqi9qx9rjjwp0izyyp08z8hi35lf") (y #t)))

(define-public crate-clockwork-utils-2.0.7 (c (n "clockwork-utils") (v "2.0.7") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1vh10kyr0078w9iv7ap608h3ajdvmfvd6nhj26z6ws8cj4a8man8") (y #t)))

(define-public crate-clockwork-utils-2.0.9 (c (n "clockwork-utils") (v "2.0.9") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "11pgq97g9sbw0yafi4wj5z0j5ddxszx95p8j373crkahp2zp6hrp") (y #t)))

(define-public crate-clockwork-utils-2.0.10 (c (n "clockwork-utils") (v "2.0.10") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "03bfaa1w0s354x5qkv265b32m49f7gn5whrs6lhk5dgid2m67p3b") (y #t)))

(define-public crate-clockwork-utils-2.0.11 (c (n "clockwork-utils") (v "2.0.11") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "06hzyqrfjdq8bd6hld3c91ca8nsa3fni7kw1g7n06hkxh5qmz1l7") (y #t)))

(define-public crate-clockwork-utils-2.0.13 (c (n "clockwork-utils") (v "2.0.13") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "112cfbs3ivb7sfzkla19yxzsk9rdz7m94fz4wm7gwiph7gm19g1h") (y #t)))

(define-public crate-clockwork-utils-2.0.14 (c (n "clockwork-utils") (v "2.0.14") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1hyh98k0xnparv6p2zb3lk3nizl51im207z4yx5q9ln0m6g7jhvy") (y #t)))

(define-public crate-clockwork-utils-2.0.15 (c (n "clockwork-utils") (v "2.0.15") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0i307h599q833xvayr9zy9dm9w21w4v7ivm24vx19b9m4kjm2hvs")))

(define-public crate-clockwork-utils-2.0.16 (c (n "clockwork-utils") (v "2.0.16") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0rhassfa11k4gxwkz329lirfbwy7jm2ypdnk05hg6mbyii24kjzj")))

(define-public crate-clockwork-utils-2.0.17 (c (n "clockwork-utils") (v "2.0.17") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1ymy6m9amhlvayz1jsqmjig10xdsrwgfzxyxd40j6yzx1sh28nk1")))

(define-public crate-clockwork-utils-2.0.18 (c (n "clockwork-utils") (v "2.0.18") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0jq2mynm3hhskmp9f4jzarh2hs1vhqk07wajxkrf7wbxnq7ky4mw")))

(define-public crate-clockwork-utils-2.0.19 (c (n "clockwork-utils") (v "2.0.19") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1pa9gcngmvw9x1sx319imjd87a795vzq8hnikm3b5q3pqfa7kkk0")))

