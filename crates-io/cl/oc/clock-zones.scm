(define-module (crates-io cl oc clock-zones) #:use-module (crates-io))

(define-public crate-clock-zones-0.1.0 (c (n "clock-zones") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "num-rational") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "0akcp4kyy0i9c156ir9lz6269alpa8jqmpm714vxp703bcz15lig") (f (quote (("rational" "num-traits" "num-rational") ("default" "bigint" "rational") ("bigint" "num-traits" "num-bigint"))))))

(define-public crate-clock-zones-0.1.1 (c (n "clock-zones") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "num-rational") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "0106s81zkx305230b0yk6yla9xzfaz9c17hm9nya15rqscml6sp6") (f (quote (("rational" "num-traits" "num-rational") ("default" "bigint" "rational") ("bigint" "num-traits" "num-bigint"))))))

(define-public crate-clock-zones-0.1.2 (c (n "clock-zones") (v "0.1.2") (d (list (d (n "ordered-float") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "1ywcvy0pz9a28lm9lhcplp26cg21x4d8g3pjx17dzblq7nzg2lmi") (f (quote (("float" "ordered-float") ("default" "float"))))))

(define-public crate-clock-zones-0.1.3 (c (n "clock-zones") (v "0.1.3") (d (list (d (n "ordered-float") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "03c3nd9r1qsdxxxj309qn1j0lxnadg29mdjsk5nkxz826mi0fi50") (f (quote (("float" "ordered-float") ("default" "float"))))))

(define-public crate-clock-zones-0.1.4 (c (n "clock-zones") (v "0.1.4") (d (list (d (n "ordered-float") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "1iqaalx69227aj35f15snmbmyynwyc5fm8kr3wsk5yv24x4qwb18") (f (quote (("float" "ordered-float") ("default" "float"))))))

(define-public crate-clock-zones-0.2.0 (c (n "clock-zones") (v "0.2.0") (d (list (d (n "ordered-float") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "0ncshrdikr89yazjm9d5vqp4np3jjmwvqm79a74yld5qln9jf6gj") (f (quote (("float" "ordered-float") ("default" "float"))))))

(define-public crate-clock-zones-0.2.1 (c (n "clock-zones") (v "0.2.1") (d (list (d (n "ordered-float") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "01x80zs17rn4saq4hxi6gsjy8afq5ywsyasw3x7r69vq34mxcm2v") (f (quote (("float" "ordered-float") ("default" "float"))))))

(define-public crate-clock-zones-0.3.0 (c (n "clock-zones") (v "0.3.0") (d (list (d (n "ordered-float") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "0li57lvj2m5jw4488654yi8h217v35x0pmyaiplzg341mv53b78h") (f (quote (("float" "ordered-float") ("default" "float"))))))

(define-public crate-clock-zones-0.3.1 (c (n "clock-zones") (v "0.3.1") (d (list (d (n "ordered-float") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "008x6gs4530cvwm5yzr6vk8nk7p7cb9kxf8qssrp0vq2wwpgw5rs") (f (quote (("float" "ordered-float") ("default" "float"))))))

(define-public crate-clock-zones-0.4.0 (c (n "clock-zones") (v "0.4.0") (d (list (d (n "ordered-float") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "1hvxwz4qc933y8879ia0gwvrfzv0a4grw1r4p5xbrh8lgzmp7jcv") (f (quote (("float" "ordered-float") ("default" "float"))))))

(define-public crate-clock-zones-0.4.1 (c (n "clock-zones") (v "0.4.1") (d (list (d (n "ordered-float") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "0wqghq5vdr5qsdj97ivqr0a25imxmbpbjv3ggy8vnsdxvhmzizqa") (f (quote (("float" "ordered-float") ("default" "float"))))))

