(define-module (crates-io cl oc cloc) #:use-module (crates-io))

(define-public crate-cloc-0.1.0 (c (n "cloc") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "11yp0ixgz4qagxyap0ig5ka1mg1wi8iajfyzg2gmgx46cq3v1bhk")))

(define-public crate-cloc-0.1.1 (c (n "cloc") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1a0ij8vx60ij1i8mfq9zg7dg2p0nsgq378l9pi1h08d8nd6lh6yr")))

(define-public crate-cloc-0.1.2 (c (n "cloc") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0a8zsy266mnjl5dgkgfxzi00nmrxaxphm7ih0nrdjbczs13n1ijg")))

(define-public crate-cloc-0.1.3 (c (n "cloc") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1vyrl0rby80w61hix7b7v5zbwfjs31z48hdf5xwglbnn7zrcwqhz")))

(define-public crate-cloc-0.1.4 (c (n "cloc") (v "0.1.4") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "08x81xjzbmi4j9hnsj8h7h3mjxgwlcv6pflhs2fqx4h1mqp1cq4w")))

(define-public crate-cloc-0.2.0 (c (n "cloc") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kly9m5ijn2p23pvjf7j6x8l0vzs79pyf1fsl25lccd3wjyby88s")))

(define-public crate-cloc-0.3.0 (c (n "cloc") (v "0.3.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qyn2ddpjn4s2k89m3m9i3x7ns01qcnyz49cw1gvkxpgcks145v2")))

(define-public crate-cloc-0.4.0 (c (n "cloc") (v "0.4.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h78k57dyarmp3gnvjsmj77djnvbm98ywkhp2d2c2zfjfvbqslb1")))

(define-public crate-cloc-0.5.0 (c (n "cloc") (v "0.5.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "03lymqja67nffc1a4a486afx9477wnxpag1pg25x1ss3wycdzp0q")))

(define-public crate-cloc-0.6.0 (c (n "cloc") (v "0.6.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1kqffq51n2h879gzs0m3v175mn92vliqx5sdp76740wixbmlp8dy")))

(define-public crate-cloc-0.6.1 (c (n "cloc") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1qh17hnsr91lbr5wp44zwvxhck8jl8d22vr1y71wakwyg9nnivy7")))

(define-public crate-cloc-0.6.2 (c (n "cloc") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0gjdbffv5mx8ykmcy7hgcji3805g5v9h5sx0ls0vah58g7bn8zr7")))

