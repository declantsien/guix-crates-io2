(define-module (crates-io cl oc clockwork-anchor-generate-cpi-interface) #:use-module (crates-io))

(define-public crate-clockwork-anchor-generate-cpi-interface-0.3.1 (c (n "clockwork-anchor-generate-cpi-interface") (v "0.3.1") (d (list (d (n "clockwork-anchor-idl") (r "^0.3.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 2)))) (h "1r2jjz1dfb8irjh1ij78xjailrb6536jnnpc3k98vr53xs4m53ii") (f (quote (("compat-program-result" "clockwork-anchor-idl/compat-program-result"))))))

(define-public crate-clockwork-anchor-generate-cpi-interface-0.3.2 (c (n "clockwork-anchor-generate-cpi-interface") (v "0.3.2") (d (list (d (n "clockwork-anchor-idl") (r "^0.3.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 2)))) (h "1s0ph79zr07vwc3lmy3r2k7ix0g5zh0bk5i9a3c7hbkg1d5inq1y") (f (quote (("compat-program-result" "clockwork-anchor-idl/compat-program-result"))))))

