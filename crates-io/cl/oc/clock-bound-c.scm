(define-module (crates-io cl oc clock-bound-c) #:use-module (crates-io))

(define-public crate-clock-bound-c-0.1.0 (c (n "clock-bound-c") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vbr8hrdprkzpijchsf85kyvb8wl3yhijwp725bfq4h6hdzv179b")))

(define-public crate-clock-bound-c-0.1.1 (c (n "clock-bound-c") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qpqv7s5gzy3bdf5galxvc5i3jdgbj0rpnxngn2jnjs2spqmi7p5")))

