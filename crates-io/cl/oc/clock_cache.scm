(define-module (crates-io cl oc clock_cache) #:use-module (crates-io))

(define-public crate-clock_cache-0.1.0 (c (n "clock_cache") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)))) (h "1abfbblmvp71cccnwhm174xgb0amn4bnr13kyrn6g9ynn2fgj7a2") (f (quote (("nightly"))))))

