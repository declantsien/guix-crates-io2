(define-module (crates-io cl oc clockkit) #:use-module (crates-io))

(define-public crate-clockkit-0.1.0 (c (n "clockkit") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clockkit-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "10f80zlpalxvb1vlbc8jz3dyv8wk3xfi5rdd95lrxipbjr0yi7bh") (f (quote (("t_server_manual"))))))

(define-public crate-clockkit-0.2.0 (c (n "clockkit") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 2)) (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cxx") (r "~1.0") (d #t) (k 0)) (d (n "cxx-build") (r "~1.0") (d #t) (k 1)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "tracing") (r "~0.1") (o #t) (d #t) (k 0)))) (h "1sf4xb838w7yw46bpq2rbm8yzj8ihlmsa4mg80d6y17h0z02r6cc") (f (quote (("default") ("build_server")))) (y #t)))

(define-public crate-clockkit-0.2.1 (c (n "clockkit") (v "0.2.1") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 2)) (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cxx") (r "~1.0") (d #t) (k 0)) (d (n "cxx-build") (r "~1.0") (d #t) (k 1)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "tracing") (r "~0.1") (o #t) (d #t) (k 0)))) (h "0b2gpclcwjfbszydqpjzmynsl3wb72jw6qf64w50skvgid1imbh8") (f (quote (("default")))) (s 2) (e (quote (("tracing" "dep:tracing"))))))

