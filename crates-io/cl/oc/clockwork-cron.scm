(define-module (crates-io cl oc clockwork-cron) #:use-module (crates-io))

(define-public crate-clockwork-cron-1.0.0 (c (n "clockwork-cron") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0kvkjmjyyvhjgkpwiaphl73b4zpd7rv8jr4klnva5cgmk6954vnd") (y #t)))

(define-public crate-clockwork-cron-1.0.1 (c (n "clockwork-cron") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "10p02s1xl0d1wkkp37msfbi29agrrpmcpjw4p5rmi1gzqg043vsx") (y #t)))

(define-public crate-clockwork-cron-1.0.2 (c (n "clockwork-cron") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0rhy88nrd3w1wn6rrj7jijlh246fc696xjwk58mq6nr32iqvk6z2") (y #t)))

(define-public crate-clockwork-cron-1.0.3 (c (n "clockwork-cron") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1cyiq4bais92yyypdhklng61ny2ig9f9zvx88qsg6sxqzlj9ryis") (y #t)))

(define-public crate-clockwork-cron-1.0.4 (c (n "clockwork-cron") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "119ghv4w3gjq5xyyjral9f9xab6ra59ylmcjxpb0bc4irgq51sxb") (y #t)))

(define-public crate-clockwork-cron-1.0.5 (c (n "clockwork-cron") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1srmpy3bpw4f4hzvjyk8139yx3p04zx1vx1dmd26hdnb1s57sijg") (y #t)))

(define-public crate-clockwork-cron-1.0.6 (c (n "clockwork-cron") (v "1.0.6") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1rmij26018lwani99sasmig6ls90l8ddvps2v6jn2nkk09l33xp5") (y #t)))

(define-public crate-clockwork-cron-1.1.2 (c (n "clockwork-cron") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1xfzxf7q42m031j5anhzzd4jkavaa294h7lhv8lvjr0b7k041yv6") (y #t)))

(define-public crate-clockwork-cron-1.1.3 (c (n "clockwork-cron") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1ih6clz87nrwvxigqpkfvkpl2k4zamzbyg933f8ypvqs3f7s64p5") (y #t)))

(define-public crate-clockwork-cron-1.1.4 (c (n "clockwork-cron") (v "1.1.4") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "00h4abnxy4zj8gq69rvpilx2zys15qv5ncr0lm9jhvzh9frmzn90")))

(define-public crate-clockwork-cron-1.2.2 (c (n "clockwork-cron") (v "1.2.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1rb15px6yfycsh8n0s9crawcswhr7nchkhv8cgdgayqks2232f7g")))

(define-public crate-clockwork-cron-1.2.4 (c (n "clockwork-cron") (v "1.2.4") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "09qb3rq06857h2dsyy3ywghcvjxmlfcf6mfjlhwm9l54r8dj5hs4")))

(define-public crate-clockwork-cron-1.2.5 (c (n "clockwork-cron") (v "1.2.5") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1v0va9il7zycvpcn232isw7b2amw420mvm2886nz5ld9l097vhjd")))

(define-public crate-clockwork-cron-1.2.9 (c (n "clockwork-cron") (v "1.2.9") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1dapkj0kp59xvz9q5ms1clim30gpfcl8aj20nsyjn3fcaydwp8qc")))

(define-public crate-clockwork-cron-1.2.11 (c (n "clockwork-cron") (v "1.2.11") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0qhasisvvkkf3mvinl9x1mrgvd33gb4yywdxi1zcilrgyw5rf5sd")))

(define-public crate-clockwork-cron-1.2.12 (c (n "clockwork-cron") (v "1.2.12") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1icx7hxznal6824yhb3vzp5kwqm3zai36da79gcxwjrvb4343pai")))

(define-public crate-clockwork-cron-1.2.13 (c (n "clockwork-cron") (v "1.2.13") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "131yjy5l6b1sbmpk9vzx6bw5ldvq9p000viilxvi5y4xdjs65dkl")))

(define-public crate-clockwork-cron-1.2.14 (c (n "clockwork-cron") (v "1.2.14") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1spc53nvfyvbh4izrzpxna1li023b0x8n0gm4az899wd1qli1rzq")))

(define-public crate-clockwork-cron-1.2.15 (c (n "clockwork-cron") (v "1.2.15") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1rfgkn29zp1nwsqv8czjff6yr2iwmk3varnp75vbm7ims9ypf3hp")))

(define-public crate-clockwork-cron-1.3.0 (c (n "clockwork-cron") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1z3q3zi3fv3mzgaqj2hhymv9cl1j6k3ppsmby75m5wschm3ky302")))

(define-public crate-clockwork-cron-1.3.1 (c (n "clockwork-cron") (v "1.3.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0adkd00hdfjbwrm59gig6f63xdlv7aqg7isn47ls4742anw96589")))

(define-public crate-clockwork-cron-1.3.5 (c (n "clockwork-cron") (v "1.3.5") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0z9c4kxkhh2yhabn3di345d6np2liq46002ayq7r13hnbyp60c39")))

(define-public crate-clockwork-cron-1.3.6 (c (n "clockwork-cron") (v "1.3.6") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0a7r6cavza9hkpsfi8z2xrn1kz1iw5a3ha6jsb7s8nyj85fvcwg5")))

(define-public crate-clockwork-cron-1.3.9 (c (n "clockwork-cron") (v "1.3.9") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "15b0fs3yrrcjc4wda5zv5677wi8vjslw8418d18irw9qdx0yks8f")))

(define-public crate-clockwork-cron-1.3.11 (c (n "clockwork-cron") (v "1.3.11") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1ha7cbwin904mk4s315zx9if74xhgar6kjcmwrins26n4w7blk4s")))

(define-public crate-clockwork-cron-1.3.12 (c (n "clockwork-cron") (v "1.3.12") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1zx6kkqww1j3k5b2c8zwjyfinq2ahvhlq1rllnk8pcrp00v8z6ry")))

(define-public crate-clockwork-cron-1.3.16 (c (n "clockwork-cron") (v "1.3.16") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "020asxkvw0kh1bk72ll3i9ni9knpgq6g8g19swfqwq6vpngivbi2")))

(define-public crate-clockwork-cron-1.4.0 (c (n "clockwork-cron") (v "1.4.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "158fnrgmqyrj6af659aq06q4pr79zjfvm398xy352vn0vlfqihyv")))

(define-public crate-clockwork-cron-1.4.2 (c (n "clockwork-cron") (v "1.4.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1klc096ik88l3p9yjpbnxwhrar9vy96x9gxwyw0m5i547g0zl07c")))

(define-public crate-clockwork-cron-2.0.0-beta (c (n "clockwork-cron") (v "2.0.0-beta") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1164smybvm1rkirqfscl114hvs22z9j4frrsajhraxj6b20ffg56")))

(define-public crate-clockwork-cron-2.0.0-gamma (c (n "clockwork-cron") (v "2.0.0-gamma") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0696vysn14wgcmmc5d15vq5fg825lfmmz1iqh1cmw9238xn4ic9r")))

(define-public crate-clockwork-cron-2.0.1 (c (n "clockwork-cron") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1faljqj1m855lvlr3jnrgkm9m3ilfcim38nqjmq7dmi4419f751j")))

(define-public crate-clockwork-cron-2.0.2 (c (n "clockwork-cron") (v "2.0.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "17qlkv6jrg5g70m02mjsc0arcqqyk53l5x0v0zdn2k0cqjlaqi6p")))

(define-public crate-clockwork-cron-2.0.5 (c (n "clockwork-cron") (v "2.0.5") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "022g8rn9ishndyli6j3y6948mb47gjpbj3w4f66scxpifr79z66w") (y #t)))

(define-public crate-clockwork-cron-2.0.7 (c (n "clockwork-cron") (v "2.0.7") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "05n9738j9xx3m32ac1mmj6h79z2pgmz1y4c8lwgr320cwmf5z8k4") (y #t)))

(define-public crate-clockwork-cron-2.0.9 (c (n "clockwork-cron") (v "2.0.9") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "16aak057c4i0g4jy586wbiwyhd31y984iis0lyqsxfha1jmn54z6") (y #t)))

(define-public crate-clockwork-cron-2.0.10 (c (n "clockwork-cron") (v "2.0.10") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "09iw6b3k8hg90y9p1rxvh90dgqn336v4jrfhn6rz7hi7nzzmz8fi") (y #t)))

(define-public crate-clockwork-cron-2.0.11 (c (n "clockwork-cron") (v "2.0.11") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0hayv209iqjq7zlphdpam169mahgx6zkgzf31m32kygndy69pv61") (y #t)))

(define-public crate-clockwork-cron-2.0.13 (c (n "clockwork-cron") (v "2.0.13") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0nl8v9lcgsn6zp6f59xxf1374m2jlckbywyv61a573ajq02zf4yb") (y #t)))

(define-public crate-clockwork-cron-2.0.14 (c (n "clockwork-cron") (v "2.0.14") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "05hahhcs9nn0fla3c5kv6z70bsx5wwcj8hls9ffls58lr6zhd4lj") (y #t)))

(define-public crate-clockwork-cron-2.0.15 (c (n "clockwork-cron") (v "2.0.15") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1f5zzf5jm2iccavd3s3qj1mjj2vval0hd7c9xw9plndihnb5g0sh")))

(define-public crate-clockwork-cron-2.0.16 (c (n "clockwork-cron") (v "2.0.16") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0vrv14bhhh8wz1n567ph996k4b6xgzqwrial1nlykb30mv7wa2lq")))

(define-public crate-clockwork-cron-2.0.17 (c (n "clockwork-cron") (v "2.0.17") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "13dgxfw1x5r8kl12kwnjiy8r055gh4ygf0i9q50a3lxh4y53i9ai")))

(define-public crate-clockwork-cron-2.0.18 (c (n "clockwork-cron") (v "2.0.18") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0bz8lnlp4nnyz5nbqlamijg3mss79a2y9p195bral6n5p8r7nw67")))

(define-public crate-clockwork-cron-2.0.19 (c (n "clockwork-cron") (v "2.0.19") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "08lxk6vx3fmgk0jj7ny1zjj6y5p1nhf8pasab8fcwamps1w2dqaw")))

