(define-module (crates-io cl oc clockwork-anchor-generate-cpi-crate) #:use-module (crates-io))

(define-public crate-clockwork-anchor-generate-cpi-crate-0.3.1 (c (n "clockwork-anchor-generate-cpi-crate") (v "0.3.1") (d (list (d (n "clockwork-anchor-idl") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 2)))) (h "001jb58dh96addqy8jwyl8ibvk1i8lq2j5sfw9silj9z3fzqsv0m") (f (quote (("compat-program-result" "clockwork-anchor-idl/compat-program-result"))))))

(define-public crate-clockwork-anchor-generate-cpi-crate-0.3.2 (c (n "clockwork-anchor-generate-cpi-crate") (v "0.3.2") (d (list (d (n "clockwork-anchor-idl") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 2)))) (h "0nyrgq6lc50d2qj68qmf5xllgbv9fpjypib2w5dyl3q933gshbps") (f (quote (("compat-program-result" "clockwork-anchor-idl/compat-program-result"))))))

