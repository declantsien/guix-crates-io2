(define-module (crates-io cl oc clock-core) #:use-module (crates-io))

(define-public crate-clock-core-0.0.1 (c (n "clock-core") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)))) (h "1qyvhpz2z8llmql54x4zymbybh12lxz5b6pjcp6nrr12lvs5m5b7")))

(define-public crate-clock-core-0.0.2 (c (n "clock-core") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)))) (h "0d4z8q9n4sy018nhm8rjzn8z5vgy1qyczawf64nwg2ckvr82bd2s")))

(define-public crate-clock-core-0.0.3 (c (n "clock-core") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)))) (h "090isjz223bvlizplxz8fp8ayyz6yfbhcipizr2h5f5k0z9q9b34")))

(define-public crate-clock-core-0.0.4 (c (n "clock-core") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)))) (h "1zakgb3xyrrad7s752pp6zlylfs3dd493qyxpw7zdcwi4cbd5dp0")))

(define-public crate-clock-core-0.0.5 (c (n "clock-core") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)))) (h "1zia5rhi1arlpy4mlsi35a580wms5kbcld0503s0zifi0704skii")))

(define-public crate-clock-core-0.0.6 (c (n "clock-core") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)))) (h "1nrw7wjnlmhhcg8a0pzcs5vai65nij89cqkbqmx6zzysxd46fc27")))

(define-public crate-clock-core-0.0.7 (c (n "clock-core") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)))) (h "15k48n641zz5v21d8ilqrvmapzk7ajhml9idzzwrpmqsi1lv04si")))

