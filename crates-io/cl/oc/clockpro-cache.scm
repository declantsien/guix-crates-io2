(define-module (crates-io cl oc clockpro-cache) #:use-module (crates-io))

(define-public crate-clockpro-cache-0.1.0 (c (n "clockpro-cache") (v "0.1.0") (d (list (d (n "bitflags") (r "~0.7.0") (d #t) (k 0)) (d (n "slab") (r "~0.3.0") (d #t) (k 0)))) (h "1jwhwr27lqfv1g0nc8flg86p1kk163q9ghlqrappiignz5xxi9g6")))

(define-public crate-clockpro-cache-0.1.1 (c (n "clockpro-cache") (v "0.1.1") (d (list (d (n "bitflags") (r "~0.7.0") (d #t) (k 0)) (d (n "slab") (r "~0.3.0") (d #t) (k 0)))) (h "1s1q9nvn8jdfz9hk3bhfcfkzm714qx92rhsp6xzzkvy0x2g5mlk7")))

(define-public crate-clockpro-cache-0.1.2 (c (n "clockpro-cache") (v "0.1.2") (d (list (d (n "bitflags") (r "~0.7.0") (d #t) (k 0)) (d (n "slab") (r "~0.3.0") (d #t) (k 0)))) (h "10hvy11jw7xyamvpjair3b5w7108f9va2wzsldzivsk8gf4mx1ng")))

(define-public crate-clockpro-cache-0.1.3 (c (n "clockpro-cache") (v "0.1.3") (d (list (d (n "bitflags") (r "~0.7.0") (d #t) (k 0)) (d (n "slab") (r "~0.3.0") (d #t) (k 0)))) (h "0jd1wrircvbdapx8gm1pxkxj050pirpsb676826mhmqz62ba7h9y")))

(define-public crate-clockpro-cache-0.1.4 (c (n "clockpro-cache") (v "0.1.4") (d (list (d (n "bitflags") (r "~0.7.0") (d #t) (k 0)) (d (n "slab") (r "~0.3.0") (d #t) (k 0)))) (h "127r6w4kl5zyhsy016b62klzlksiqh4h0b8qxniw7f2jq8ffiyvh")))

(define-public crate-clockpro-cache-0.1.5 (c (n "clockpro-cache") (v "0.1.5") (d (list (d (n "bitflags") (r "~0.7.0") (d #t) (k 0)) (d (n "slab") (r "~0.3.0") (d #t) (k 0)))) (h "0zrdwzn8mvpq05jvnh4lqp7qhac3zaimc8v01yy2nf1hh7drlxw3")))

(define-public crate-clockpro-cache-0.1.6 (c (n "clockpro-cache") (v "0.1.6") (d (list (d (n "bitflags") (r "~1") (d #t) (k 0)) (d (n "clippy") (r "~0") (o #t) (d #t) (k 0)) (d (n "slab") (r "~0.4") (d #t) (k 0)))) (h "0m1r7lazg5ndcwp5mzarx6vla1fdqmw304gvn0ga30jsn54vsnzi")))

(define-public crate-clockpro-cache-0.1.7 (c (n "clockpro-cache") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "unsafe_unwrap") (r "^0.1") (d #t) (k 0)))) (h "0pzj6w7biq291kz4dz0r8629rxmx0ga68p2vrcn0grcbhyrrv6h0")))

(define-public crate-clockpro-cache-0.1.8 (c (n "clockpro-cache") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "unsafe_unwrap") (r "^0.1") (d #t) (k 0)))) (h "1g5vldbjyi1kgjw810sbxpvvvsjshzpl7g9mx4j5k84fxfmdgnm2")))

(define-public crate-clockpro-cache-0.1.9 (c (n "clockpro-cache") (v "0.1.9") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "unsafe_unwrap") (r "^0.1") (d #t) (k 0)))) (h "18hzp71c12zsh99ny0s26c3daq28351ijdci200wdgnpnxxw9xy3")))

(define-public crate-clockpro-cache-0.1.10 (c (n "clockpro-cache") (v "0.1.10") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "unsafe_unwrap") (r "^0.1") (d #t) (k 0)))) (h "1laikkzmv47c0gjsbsf2091dppy4cyqjd8m0c8yjlpxyrcsf26vp")))

(define-public crate-clockpro-cache-0.1.11 (c (n "clockpro-cache") (v "0.1.11") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "unsafe_unwrap") (r "^0.1") (d #t) (k 0)))) (h "0p5gs35v48dgx756cdxxalzy040bddrwqd2agxn2i4hz554yv2hb")))

(define-public crate-clockpro-cache-0.1.12 (c (n "clockpro-cache") (v "0.1.12") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "slabigator") (r "^0.9.1") (f (quote ("slot_usize" "releasefast"))) (d #t) (k 0)))) (h "012fki6v75c0l5p60nkcjb0whyn7j2im2kn8593h6v4d8i3z6290")))

