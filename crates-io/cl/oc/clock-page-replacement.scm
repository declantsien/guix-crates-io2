(define-module (crates-io cl oc clock-page-replacement) #:use-module (crates-io))

(define-public crate-clock-page-replacement-0.1.0 (c (n "clock-page-replacement") (v "0.1.0") (h "0gdyjdw0ymvgixszwfkga0pw7l1b151n1sx213m70niy9x6ns7f4")))

(define-public crate-clock-page-replacement-0.1.1 (c (n "clock-page-replacement") (v "0.1.1") (h "0nvnp4ycic1calb15jdlmjx9sngbkz3dx89mjdmknzvpjg3a5h8y")))

