(define-module (crates-io cl oc clockwork-relayer-api) #:use-module (crates-io))

(define-public crate-clockwork-relayer-api-2.0.1 (c (n "clockwork-relayer-api") (v "2.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.15") (d #t) (k 0)))) (h "1niak1wb2k3ln9r24wiw3ldiydhsfmldq17wag8vfg0y7f2swxpd")))

(define-public crate-clockwork-relayer-api-2.0.10 (c (n "clockwork-relayer-api") (v "2.0.10") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.16") (d #t) (k 0)))) (h "0yw7277g086b2fg30qwrss4mswidg15vpkyp83qrp56hry6s9jnv") (y #t)))

(define-public crate-clockwork-relayer-api-2.0.11 (c (n "clockwork-relayer-api") (v "2.0.11") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.16") (d #t) (k 0)))) (h "13sh69vzvgs295l3vi2k080hbarpzz3a0r0xrgxwlvc5a5fjy22g") (y #t)))

(define-public crate-clockwork-relayer-api-2.0.13 (c (n "clockwork-relayer-api") (v "2.0.13") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.16") (d #t) (k 0)))) (h "1jvllnn50ja8aq31ijrm85q1xfanf3ziipj56wvkwv1zv2lyax3x") (y #t)))

(define-public crate-clockwork-relayer-api-2.0.14 (c (n "clockwork-relayer-api") (v "2.0.14") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.16") (d #t) (k 0)))) (h "1zcsbfzy6cpyxnzvn0zzcz4ahlsjxk1pfzaja03b16gm3mrcb0dj") (y #t)))

(define-public crate-clockwork-relayer-api-2.0.15 (c (n "clockwork-relayer-api") (v "2.0.15") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.16") (d #t) (k 0)))) (h "05vk40gs6r01zgyr3rxh2iz61kp6inr75p66dprqnd0q3vmkbqas")))

(define-public crate-clockwork-relayer-api-2.0.16 (c (n "clockwork-relayer-api") (v "2.0.16") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "1m280xm8l331xckaipns5w11ijay7brcdr7sy3rfc3fv3wcd1k15")))

(define-public crate-clockwork-relayer-api-2.0.17 (c (n "clockwork-relayer-api") (v "2.0.17") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "1fw2s4chqpvfnim1gqzsg6dccvrl6nfaj8w1p1vfqyh3d5j7zy77")))

(define-public crate-clockwork-relayer-api-2.0.18 (c (n "clockwork-relayer-api") (v "2.0.18") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "12i9s6jh808sdq4qaq3vlv1rd5x4sfd6cadcz96s1jhjhz7gndxn")))

(define-public crate-clockwork-relayer-api-2.0.19 (c (n "clockwork-relayer-api") (v "2.0.19") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.16") (d #t) (k 0)))) (h "0jh7a21cxiab0l7j3lh1bdwsvx1yrgy8iiz8a7r9i5qz878nlaac")))

