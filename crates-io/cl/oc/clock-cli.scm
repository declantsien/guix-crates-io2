(define-module (crates-io cl oc clock-cli) #:use-module (crates-io))

(define-public crate-clock-cli-0.0.1 (c (n "clock-cli") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "cursive") (r "^0.15.0") (d #t) (k 0)))) (h "03m57ibanm5klyrwfvqviv7frv1ww6s653ljqd0s87vh3p8hqkhh")))

(define-public crate-clock-cli-0.0.2 (c (n "clock-cli") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "cursive") (r "^0.15.0") (d #t) (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)))) (h "0sw8ykxffwk039l3zvg7sy3pgi14nzh0b9gxiz47hgimbj05kkhn")))

(define-public crate-clock-cli-0.0.3 (c (n "clock-cli") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "cursive") (r "^0.15.0") (d #t) (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)))) (h "1s9afpg05901i0cwc1sbk2pxvbzxzyj10wmshp54b0h54yycxg8v")))

(define-public crate-clock-cli-0.0.4 (c (n "clock-cli") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "cursive") (r "^0.15.0") (d #t) (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)))) (h "0wbzx9a9q56cj97hcv0l8yf72kbnjz7xxdcvizp56jrm6cqd61l9")))

(define-public crate-clock-cli-0.0.6 (c (n "clock-cli") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clock-core") (r "^0.0.6") (d #t) (k 0)) (d (n "cursive") (r "^0.15.0") (d #t) (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "1cc2hwgl6clw6xmai8jfa3xiz0vkiisf5n9isj40nlbwdv8mndjd")))

(define-public crate-clock-cli-0.0.7 (c (n "clock-cli") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clock-core") (r "^0.0.6") (d #t) (k 0)) (d (n "cursive") (r "^0.15.0") (d #t) (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "0df2wmyl2bbppljcxr2slyd199qlxdzcyh8z9vrw6vcrkvlq167y")))

(define-public crate-clock-cli-0.1.0 (c (n "clock-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clock-core") (r "^0.0.6") (d #t) (k 0)) (d (n "cursive") (r "^0.15") (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "0g4w5v75pvlqyz0wxdsvfq7p4i108ad9gchmzcf1gjq89z4ccyjy") (f (quote (("default" "cursive/crossterm-backend"))))))

(define-public crate-clock-cli-0.1.1 (c (n "clock-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clock-core") (r "^0.0.6") (d #t) (k 0)) (d (n "cursive") (r "^0.15") (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "0a5k6j1amb5f2kwcj502yyz6xnpgb6d89ianp8rimykgs43madl1") (f (quote (("default" "cursive/crossterm-backend"))))))

