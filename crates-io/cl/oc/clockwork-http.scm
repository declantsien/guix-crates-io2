(define-module (crates-io cl oc clockwork-http) #:use-module (crates-io))

(define-public crate-clockwork-http-1.0.2 (c (n "clockwork-http") (v "1.0.2") (d (list (d (n "anchor-lang") (r "^0.25.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "clockwork-pool") (r "^1.0.2") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1icxqm1ymnd3ws20gh49fv9286q0xmvi7132gpa12hzn5mrq0vdw") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-http-1.0.3 (c (n "clockwork-http") (v "1.0.3") (d (list (d (n "anchor-lang") (r "^0.25.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "clockwork-pool") (r "^1.0.3") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1gqysf1xrlz8gp2g3akb8vlk4kcw6vf2wsc0k0wndzmbhk57yjrs") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-http-1.0.4 (c (n "clockwork-http") (v "1.0.4") (d (list (d (n "anchor-lang") (r "^0.25.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "clockwork-pool") (r "^1.0.4") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1rch381a2jhbivz5aj3g3bx3ih153n053iifvysz6zpd1dkjm40a") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-http-1.0.5 (c (n "clockwork-http") (v "1.0.5") (d (list (d (n "anchor-lang") (r "^0.25.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "clockwork-pool") (r "^1.0.5") (f (quote ("cpi"))) (d #t) (k 0)))) (h "03jpnrxnhbrg47py801sssqwwax3rlswkpdy08zwifs9c0gd4byd") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-http-1.0.6 (c (n "clockwork-http") (v "1.0.6") (d (list (d (n "anchor-lang") (r "^0.25.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "clockwork-pool") (r "^1.0.6") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0wjb3hn0zzlrb280d8dp70rsdi1jhafm0v9hf9gbqlbm2qn41zav") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

