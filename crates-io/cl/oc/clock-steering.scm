(define-module (crates-io cl oc clock-steering) #:use-module (crates-io))

(define-public crate-clock-steering-0.1.0 (c (n "clock-steering") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)))) (h "1qg906jkim4h2i8mjqa5yxcsq3b8m66lckx00j8s2vrsrpdlib40") (f (quote (("std") ("default" "std")))) (r "1.66")))

(define-public crate-clock-steering-0.2.0 (c (n "clock-steering") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.145") (d #t) (k 0)))) (h "00icq4l6glscg5fhdv740iwaqk79kspnjbncr6jfygzglsa8j1z8") (r "1.66")))

