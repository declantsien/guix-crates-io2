(define-module (crates-io cl oc clockwork-macros) #:use-module (crates-io))

(define-public crate-clockwork-macros-1.4.0 (c (n "clockwork-macros") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0489iab83bbyc7igipwhscm35ircbn8vp7gswz6c6gcwlwfk77ps")))

