(define-module (crates-io cl oc clock_ticks) #:use-module (crates-io))

(define-public crate-clock_ticks-0.0.1 (c (n "clock_ticks") (v "0.0.1") (h "1qlsjfaysqs3hgqa2m617w7v237y6qvps8zm7ryj47rmvkqkxxbc")))

(define-public crate-clock_ticks-0.0.2 (c (n "clock_ticks") (v "0.0.2") (h "0zj8ail8s8zpxadw1fkmln02v586q49jldlfdxiqlkk3galrraza")))

(define-public crate-clock_ticks-0.0.3 (c (n "clock_ticks") (v "0.0.3") (h "1nh4p6br5vh6pmkzggks2bb8dvlmp7p4jy42fr24n1iam55gsjb3")))

(define-public crate-clock_ticks-0.0.4 (c (n "clock_ticks") (v "0.0.4") (h "0wm63sffw7051kz8s0z8qi1rv3hs98vxs08hxjzq8qi6bj94x2rw")))

(define-public crate-clock_ticks-0.0.5 (c (n "clock_ticks") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "17jv8hysa1hrylz5fsgfiafhq3x478cmhv7hfpdzgsv9wr26c71w")))

(define-public crate-clock_ticks-0.0.6 (c (n "clock_ticks") (v "0.0.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1ndirmbqbkl19d88pv6457p489qlhy7cpkl6a0bh3jmkdz2h0amg")))

(define-public crate-clock_ticks-0.1.0 (c (n "clock_ticks") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "03c0hx3srjpxq548x2kvvcs3qdyrcv9qdb8fz2ih86fgxy7xk6ys")))

(define-public crate-clock_ticks-0.1.1 (c (n "clock_ticks") (v "0.1.1") (h "0qrd0hb6k19ckhmvryimvdv0jpzb3j34khhaxm0mzb3kivsr16n4")))

