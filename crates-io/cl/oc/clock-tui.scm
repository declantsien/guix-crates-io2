(define-module (crates-io cl oc clock-tui) #:use-module (crates-io))

(define-public crate-clock-tui-0.1.0 (c (n "clock-tui") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "1wzx5hiwrxdfbajjh484qhvakxl3pdmqgwhm9bx5kjcj09k96asq")))

(define-public crate-clock-tui-0.2.0 (c (n "clock-tui") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "1vl5pday582d1n7325iyk6pjzp4pq8pxb9rfr43i7v4n10bgb2ly")))

(define-public crate-clock-tui-0.2.1 (c (n "clock-tui") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "07gzf2flvhklr7l97ajh4w31wmw6j9h7qbx58cklmg7j0hmzvxcp")))

(define-public crate-clock-tui-0.3.0 (c (n "clock-tui") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "0hns5dal1smkkg52sj0r67jy3wh905imm96hcskk1sizchr3l0d0")))

(define-public crate-clock-tui-0.3.1 (c (n "clock-tui") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "196v371brdv75b1rr07p1fxc19grgzrcg0c1xy25l6sr9z6kiwps")))

(define-public crate-clock-tui-0.3.2 (c (n "clock-tui") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "1431gfjrymnfm22hifaw61c7d39w1szghd89sfrm8l3m1xs96h7g")))

(define-public crate-clock-tui-0.4.0 (c (n "clock-tui") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "0xi74ydnjgsyg2yvfahf6g2wy1q1m4wp69ixvsrqxwqv0339rsxq")))

(define-public crate-clock-tui-0.5.0 (c (n "clock-tui") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "0qyxlncp8mssm7n07lrdmllhn7vq58pyvmrm3vpvnga49jwy2p1b")))

