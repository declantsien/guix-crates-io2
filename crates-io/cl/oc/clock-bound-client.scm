(define-module (crates-io cl oc clock-bound-client) #:use-module (crates-io))

(define-public crate-clock-bound-client-1.0.0 (c (n "clock-bound-client") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "clock-bound-shm") (r "^1.0.0") (d #t) (k 0)) (d (n "errno") (r "=0.3.0") (k 0)) (d (n "nix") (r "^0.26") (f (quote ("feature" "time"))) (d #t) (k 0)))) (h "15cysn0hbm3cd5jaf7ch69myh86ny1sn2qgdpa3bam1wjwa4v7d2")))

