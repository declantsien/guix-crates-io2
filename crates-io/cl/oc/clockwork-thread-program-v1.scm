(define-module (crates-io cl oc clockwork-thread-program-v1) #:use-module (crates-io))

(define-public crate-clockwork-thread-program-v1-2.0.2 (c (n "clockwork-thread-program-v1") (v "2.0.2") (d (list (d (n "anchor-gen") (r "^0.3.1") (f (quote ("compat-program-result"))) (d #t) (k 0)) (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)))) (h "07was2jkl6282ms2zcibcxamhzzb1x0iwlr2kkfgr459kgh77zzy") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-clockwork-thread-program-v1-1.4.3 (c (n "clockwork-thread-program-v1") (v "1.4.3") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "clockwork-anchor-gen") (r "^0.3.2") (f (quote ("compat-program-result"))) (d #t) (k 0)))) (h "1anx5xds443sv6r8wnnd1fhr9gz0d40y1ysby6qlcsby4h00z1li") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-thread-program-v1-1.4.4 (c (n "clockwork-thread-program-v1") (v "1.4.4") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "clockwork-anchor-gen") (r "^0.3.2") (f (quote ("compat-program-result"))) (d #t) (k 0)))) (h "15pilzjd0rc5hih3khwqg25cs1ksisw8vn832jljag63pyfrzgy5") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

