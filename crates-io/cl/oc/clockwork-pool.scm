(define-module (crates-io cl oc clockwork-pool) #:use-module (crates-io))

(define-public crate-clockwork-pool-1.0.0 (c (n "clockwork-pool") (v "1.0.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1j6jzkdj5gnxdl7sfwikdpy276nzzrzb68q30wn8smlbr827hkwn") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-pool-1.0.1 (c (n "clockwork-pool") (v "1.0.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0vzmyf6sbay3scnrfp9ik162cj52nzwn2xihg069yybvx51dbx6i") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-pool-1.0.2 (c (n "clockwork-pool") (v "1.0.2") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0q3ilr2391ykwrxgfvkand9kp6l4ry7ragdc880daqzhaacm9bwr") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-pool-1.0.3 (c (n "clockwork-pool") (v "1.0.3") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0dmvim2vxa1lsgx1lfix19axr98v4i219z10bfb2l3qxfj5g1dzh") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-pool-1.0.4 (c (n "clockwork-pool") (v "1.0.4") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0wc339cf7rmz0bvvnyhhsmssyn9nlvqvrnkqdm9bclqj7fa33hh8") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-pool-1.0.5 (c (n "clockwork-pool") (v "1.0.5") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0nvvdal183jlxccfw6m1i8rdgd254p455vbr6ymrvvddkhki3jwj") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-clockwork-pool-1.0.6 (c (n "clockwork-pool") (v "1.0.6") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "12jwg6zij9j31sxhf20scc9ga8pfzgbsfxzdzhrbc0vbcsjh28kp") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

