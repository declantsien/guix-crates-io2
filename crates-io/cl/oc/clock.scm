(define-module (crates-io cl oc clock) #:use-module (crates-io))

(define-public crate-clock-0.0.2 (c (n "clock") (v "0.0.2") (h "04b031q1435q7vrzll5qdgfd8ghzh0vzvqrvbxkwn2fql7li97mh")))

(define-public crate-clock-0.1.0 (c (n "clock") (v "0.1.0") (h "1bvq95s7p5psqdcr3gvd708djlb41ppd2gxjslwyjhg5lf483ka9")))

(define-public crate-clock-0.2.0 (c (n "clock") (v "0.2.0") (h "1d1xz686bjsk509as6gryh5p56yjqg2y0vch3n3xl9qwnjk5v7ci")))

(define-public crate-clock-0.2.1 (c (n "clock") (v "0.2.1") (h "0kqfwh14jzkn39jikz5q478i5r9q2n80hf101qfqxxdqbx4kdwww")))

(define-public crate-clock-0.2.2 (c (n "clock") (v "0.2.2") (h "00zv2156kmg6dxs0c6wz0x3f50qs1kqf94jg30bgs0wd645zcv2y")))

(define-public crate-clock-0.3.0 (c (n "clock") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.29") (d #t) (k 2)))) (h "05ibw5m39wdcyjc2gjlcciykx94q94m1ygij2jwa11s8l609ax4v")))

(define-public crate-clock-0.3.1 (c (n "clock") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.29") (d #t) (k 2)))) (h "1nh9g14igzspi77s72rds4bvvp2f8zn1qcdq13jnxsn3r7saa7lf")))

(define-public crate-clock-0.3.2 (c (n "clock") (v "0.3.2") (d (list (d (n "thiserror") (r "^1.0.29") (d #t) (k 2)))) (h "07qi8abfacmajq48i0g6rpvqqvgk6ckyg9zrr7vygcwidgb3akph")))

(define-public crate-clock-0.3.3 (c (n "clock") (v "0.3.3") (d (list (d (n "thiserror") (r "^1.0.29") (d #t) (k 2)))) (h "1w42dd8s4r782dyzrxh0k5gicvq8i3bjax14inxclamjzdjicgrz")))

