(define-module (crates-io cl oc clockwork-pool-program) #:use-module (crates-io))

(define-public crate-clockwork-pool-program-1.1.2 (c (n "clockwork-pool-program") (v "1.1.2") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1csgq9y0mbxlamhjb9vgvmrlzkzs6xlb1wawack7j8wpypbpbrnc") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-clockwork-pool-program-1.1.3 (c (n "clockwork-pool-program") (v "1.1.3") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1cf2i503dqx4x1r66isqrpjyl9qivmz21ddj2xkzjfz7dj2sx944") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-clockwork-pool-program-1.1.4 (c (n "clockwork-pool-program") (v "1.1.4") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0vd224whl156ax1zh6q81bsm2pk0xgwvkkmq8vn57yhmwrb7w3g2") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

