(define-module (crates-io cl oc clock-bound-shm) #:use-module (crates-io))

(define-public crate-clock-bound-shm-1.0.0 (c (n "clock-bound-shm") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "errno") (r "=0.3.0") (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("feature" "time"))) (d #t) (k 0)))) (h "12k60qdfnfcba7q21y49m9fsykcxcbsg66zb8yq07ks19xjnwf3s") (f (quote (("writer"))))))

