(define-module (crates-io cl oc clocks) #:use-module (crates-io))

(define-public crate-clocks-0.0.1 (c (n "clocks") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "priq") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "1z65v5613f24r0h6inniqshknbh7zbvmb8j9jjagrcf1w4csbm6r")))

