(define-module (crates-io cl uf cluflock) #:use-module (crates-io))

(define-public crate-cluFlock-0.1.2 (c (n "cluFlock") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "0wciwb3lm5j3nddcnfdigy7jcmcz8hvhpck6c4rahnz5i125ff15")))

(define-public crate-cluFlock-0.1.5 (c (n "cluFlock") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "1fb3ahhzhxdxbpx6401h5kyhpw8c8hbrw1vfinnygpzqi4dj8sbx")))

(define-public crate-cluFlock-0.1.8 (c (n "cluFlock") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "0d7f3hz2krp2n13gkjg1yi1fqpzaa90laxrcccxgigkwk865f5i1")))

(define-public crate-cluFlock-0.2.2 (c (n "cluFlock") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "04dllv45d1yiji7d0hc7w70v7vak19blxnjx6spaz88pb9kgdz5l")))

(define-public crate-cluFlock-1.2.4 (c (n "cluFlock") (v "1.2.4") (d (list (d (n "cluFullTransmute") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "13f15nxy9jm2syzslwfcqvkgw56d7162k5mcp497273mv6ydz330") (f (quote (("nightly" "cluFullTransmute") ("default"))))))

(define-public crate-cluFlock-1.2.5 (c (n "cluFlock") (v "1.2.5") (d (list (d (n "cluFullTransmute") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("fileapi" "minwinbase" "winnt" "ntdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1gil2076kch93x2r5xjcp0mk8lhavqip0wjlc47syzd3drpmi6nw") (f (quote (("nightly" "cluFullTransmute") ("default"))))))

(define-public crate-cluFlock-1.2.7 (c (n "cluFlock") (v "1.2.7") (d (list (d (n "libc") (r "^0.2.107") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("fileapi" "minwinbase" "winnt" "ntdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "121g8q945lk5hqvxf571dqsjylg5lhaaacxn2x608f34v2xzmh55") (f (quote (("win_fix_woudblock_in_errresult") ("default" "win_fix_woudblock_in_errresult"))))))

