(define-module (crates-io cl ff clfft) #:use-module (crates-io))

(define-public crate-clfft-0.1.0 (c (n "clfft") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 2)) (d (n "ocl") (r "^0.12.0") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.0") (d #t) (k 2)))) (h "0xyvk9dd8skgffy0rxmbr8vijvk08zmjz7gplbvw1dijbmn528zb") (f (quote (("build_all" "cmake"))))))

(define-public crate-clfft-0.2.0 (c (n "clfft") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 2)) (d (n "ocl") (r "^0.12.0") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.0") (d #t) (k 2)))) (h "0flx7kwvyhlicfw2v0p67vdj3iz4wpnxr6kznd872mdnn9x0wv24") (f (quote (("build_all" "cmake"))))))

(define-public crate-clfft-0.3.0 (c (n "clfft") (v "0.3.0") (d (list (d (n "cl-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 2)) (d (n "ocl") (r "^0.14.1") (d #t) (k 0)) (d (n "ocl-core") (r "^0.5.1") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.0") (d #t) (k 2)))) (h "17k0dcr83vs9v5iqp3mw03i3967fzsql1n11bc0vl9z4x6vsyby9") (f (quote (("build_all" "cmake"))))))

(define-public crate-clfft-0.3.1 (c (n "clfft") (v "0.3.1") (d (list (d (n "cl-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 2)) (d (n "ocl") (r "^0.15.0") (d #t) (k 0)) (d (n "ocl-core") (r "^0.6.0") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.0") (d #t) (k 2)))) (h "1rzn41dm0vk7fm35yn0agr64wimm2dbwl53v06mplgpczrndi6pg") (f (quote (("build_all" "cmake"))))))

(define-public crate-clfft-0.3.2 (c (n "clfft") (v "0.3.2") (d (list (d (n "cl-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "ocl") (r "^0.18.0") (d #t) (k 0)) (d (n "ocl-core") (r "^0.9.0") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.0") (d #t) (k 2)))) (h "0z865395hh0v3ml5arhjcfccy8wirm59kdgslb8iwkbfyf4dhhsp") (f (quote (("build_all" "cmake"))))))

(define-public crate-clfft-0.3.3 (c (n "clfft") (v "0.3.3") (d (list (d (n "cl-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "ocl") (r "^0.19.0") (d #t) (k 0)) (d (n "ocl-core") (r "^0.10.0") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.0") (d #t) (k 2)))) (h "0g3c7d81ny7i292q2r50f9979fqgn2xgkrla7limwpn6y0ppgwvl") (f (quote (("build_all" "cmake"))))))

