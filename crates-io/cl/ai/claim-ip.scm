(define-module (crates-io cl ai claim-ip) #:use-module (crates-io))

(define-public crate-claim-ip-1.0.0 (c (n "claim-ip") (v "1.0.0") (d (list (d (n "nix") (r "^0.19.0") (d #t) (k 0)))) (h "1g00b1c1idbfi65nranhn3hvm4d1sidjhn0k82vx11krfpva2gbx")))

(define-public crate-claim-ip-1.0.7 (c (n "claim-ip") (v "1.0.7") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "eui48") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "19kbpdihk4abq79z27d9cddyjh5hh0gfgsphpk4r8ns3qw3zvfb7")))

(define-public crate-claim-ip-1.0.8 (c (n "claim-ip") (v "1.0.8") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "eui48") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "04n4s8cdpk8cq4ypvy3qs984jyji5h8hl75547pjz336pngkm663")))

(define-public crate-claim-ip-1.1.0 (c (n "claim-ip") (v "1.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "eui48") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1dnj3ydd81flj0913pkxwc7vmlrcd6iyppc96hz347dfcr8dyp67")))

(define-public crate-claim-ip-1.1.1 (c (n "claim-ip") (v "1.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "eui48") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "182nj6a42v5mqdrjz84hs1g7z4m27ll3y8g996jcrwprv904rsli")))

