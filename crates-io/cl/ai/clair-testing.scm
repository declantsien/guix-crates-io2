(define-module (crates-io cl ai clair-testing) #:use-module (crates-io))

(define-public crate-CLAiR-testing-0.1.0 (c (n "CLAiR-testing") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)))) (h "0wnb3lwyahznifbrqgml8lzarc2sz80ghcizj7xamadnwacncb9v")))

