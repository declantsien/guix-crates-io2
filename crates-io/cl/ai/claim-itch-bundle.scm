(define-module (crates-io cl ai claim-itch-bundle) #:use-module (crates-io))

(define-public crate-claim-itch-bundle-0.1.0 (c (n "claim-itch-bundle") (v "0.1.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fantoccini") (r "^0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1bs3cgdjbxnwbvxy7jbjrf9cq15aa6wpw5z31lmrbfng9y8i6gzy")))

