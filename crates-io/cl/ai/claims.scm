(define-module (crates-io cl ai claims) #:use-module (crates-io))

(define-public crate-claims-0.6.0 (c (n "claims") (v "0.6.0") (d (list (d (n "autocfg") (r "^1.0") (d #t) (k 1)))) (h "1vwqmbqqf0ml4vkjzys3cqmais0m35h6m5xvfids43hvhckphhbh")))

(define-public crate-claims-0.7.1 (c (n "claims") (v "0.7.1") (d (list (d (n "autocfg") (r "^1.0") (d #t) (k 1)))) (h "1da6z2r4zz4fw4a69286s54jzr7g7sz3dspq0xiw6mk432z5p6dn")))

