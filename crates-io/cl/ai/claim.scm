(define-module (crates-io cl ai claim) #:use-module (crates-io))

(define-public crate-claim-0.0.0 (c (n "claim") (v "0.0.0") (h "0hqps1i2hnwiqxrarxfa2xck8bawllvs9d5k243wh49igr9yrzdb")))

(define-public crate-claim-0.1.0 (c (n "claim") (v "0.1.0") (d (list (d (n "autocfg") (r "~1.0") (d #t) (k 1)))) (h "18m1dlrc732yngx81aqw0dmzfh2pxq415bykq74wy3145rmdz3vx")))

(define-public crate-claim-0.1.1 (c (n "claim") (v "0.1.1") (d (list (d (n "autocfg") (r "~1.0") (d #t) (k 1)))) (h "0g2ps4f4jv6x91cmz8wjmd350bxzj6z6fl3nccs5f5ix6jiq1wjb")))

(define-public crate-claim-0.2.0 (c (n "claim") (v "0.2.0") (d (list (d (n "autocfg") (r "~1.0") (d #t) (k 1)))) (h "0rdzdi3kv0n99v5arj52fknvsi8f84qiw201kkq6zfqmz2pbfb7d")))

(define-public crate-claim-0.3.0 (c (n "claim") (v "0.3.0") (d (list (d (n "autocfg") (r "~1.0") (d #t) (k 1)))) (h "1anhhcdqy1kbgjszamhhgsh2z365y737za3qq2lcyz219wwajrz5")))

(define-public crate-claim-0.3.1 (c (n "claim") (v "0.3.1") (d (list (d (n "autocfg") (r "~1.0") (d #t) (k 1)))) (h "192vi55kav92259qmv04liywpjbv95rfmkkw8mqjgwcbwqz8jbib")))

(define-public crate-claim-0.4.0 (c (n "claim") (v "0.4.0") (d (list (d (n "autocfg") (r "~1.0") (d #t) (k 1)))) (h "1fcla6qz6c2i350q8gr09ji7ds1ginb6i4whi1q9mcjminakgbb8")))

(define-public crate-claim-0.5.0 (c (n "claim") (v "0.5.0") (d (list (d (n "autocfg") (r "~1.0") (d #t) (k 1)))) (h "1s1wmdqa1937x3dhczxv284nfsk69ci4f8xva1nxzqbjpgb9j47q")))

