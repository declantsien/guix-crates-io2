(define-module (crates-io cl n- cln-ntfy) #:use-module (crates-io))

(define-public crate-cln-ntfy-0.1.0 (c (n "cln-ntfy") (v "0.1.0") (d (list (d (n "cln-plugin") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntfy") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0fpa4jq3s4j2rbpqs76yrp6dlav5vvbsb7n6qzf8a5a00hid6pvj")))

(define-public crate-cln-ntfy-0.1.1 (c (n "cln-ntfy") (v "0.1.1") (d (list (d (n "cln-plugin") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntfy") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("macros" "rt-multi-thread"))) (k 0)))) (h "1l6iyz3a6qai9w4qamjwnv8xnhdp03lkyd1y9l9wbhrg9aija9br")))

