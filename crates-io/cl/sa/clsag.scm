(define-module (crates-io cl sa clsag) #:use-module (crates-io))

(define-public crate-clsag-0.1.0 (c (n "clsag") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "merlin") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0zd8sakgjlhm9r8n26jc790kq18ns2cn13y4i0r378kqfc9xd32b")))

(define-public crate-clsag-0.1.1 (c (n "clsag") (v "0.1.1") (d (list (d (n "curve25519-dalek") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "merlin") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0p80i52x7v1bgb65jg6si9svvqcsx4ip0yn5w3j40xl470x34sd5")))

(define-public crate-clsag-0.1.2 (c (n "clsag") (v "0.1.2") (d (list (d (n "curve25519-dalek") (r "^1.2.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "merlin") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "17276kjby38bblzlgc46gnns3n6dgfc595d11kiazr083ggpmsl8")))

(define-public crate-clsag-0.2.0 (c (n "clsag") (v "0.2.0") (d (list (d (n "curve25519-dalek") (r "^1.2.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "merlin") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1yqm09mfryg5w2vi86l62pkhbdw6yph1g2jpamsryx6va01lb2k9")))

(define-public crate-clsag-0.3.0 (c (n "clsag") (v "0.3.0") (d (list (d (n "curve25519-dalek") (r "^1.2.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "merlin") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0r2y703wsq0d36q5lha0czzv860kc9p2chz25dv7wphdpihn9sk6")))

