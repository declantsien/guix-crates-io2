(define-module (crates-io cl on clone-fields-derive) #:use-module (crates-io))

(define-public crate-clone-fields-derive-0.1.0 (c (n "clone-fields-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0hwwc36ajjwky6axpafsv9kf97kb2l7a5m1wxmzbf00776sc6vwd")))

(define-public crate-clone-fields-derive-0.4.0 (c (n "clone-fields-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "133phsig5pfnbx26i2jvvz2zb3xz2fzinlqym3jwnfpa9m66v14g")))

(define-public crate-clone-fields-derive-0.4.1 (c (n "clone-fields-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "1xl15m35jclpm01igyk58z955wksj4ccxkhnix6ykymjx8ri4znq")))

