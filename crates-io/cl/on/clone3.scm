(define-module (crates-io cl on clone3) #:use-module (crates-io))

(define-public crate-clone3-0.1.0 (c (n "clone3") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (k 0)) (d (n "uapi") (r "^0.2") (k 0)))) (h "10y7sd6qnqn3fah6n95141izka10w31j8h90zmk3i0azl28hcj3s")))

(define-public crate-clone3-0.2.0 (c (n "clone3") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (k 0)) (d (n "uapi") (r "^0.2") (k 0)))) (h "1zpx3wk19q3931q3dfrbhj9asf4yd4qzn0qm4irs4jgxakf43x2d") (f (quote (("linux_5-7" "linux_5-5") ("linux_5-5") ("default" "linux_5-7"))))))

(define-public crate-clone3-0.2.1 (c (n "clone3") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (k 0)) (d (n "uapi") (r "^0.2") (k 0)))) (h "0ji1i5ck1klnlsj227cnh6wn5lsbzpa7jxy650q5l4vz53az88pq") (f (quote (("linux_5-7" "linux_5-5") ("linux_5-5") ("default" "linux_5-7"))))))

(define-public crate-clone3-0.2.2 (c (n "clone3") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.0") (k 0)) (d (n "uapi") (r "^0.2") (k 0)))) (h "0d3d6s495135y2mg9b8dr5zr4bkmymsgaq14y3961n4fm9gxq7ji") (f (quote (("linux_5-7" "linux_5-5") ("linux_5-5") ("default" "linux_5-7"))))))

(define-public crate-clone3-0.2.3 (c (n "clone3") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.0") (k 0)) (d (n "uapi") (r "^0.2") (k 0)))) (h "03znv6afh9lb7r5j9kh8kfk40f4mydw3hrh9ra8h501hx9hy1r2y") (f (quote (("linux_5-7" "linux_5-5") ("linux_5-5") ("default" "linux_5-7"))))))

