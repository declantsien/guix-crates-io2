(define-module (crates-io cl on cloning) #:use-module (crates-io))

(define-public crate-cloning-0.0.1 (c (n "cloning") (v "0.0.1") (d (list (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "125ih5kldsafbqr8vdsny0xlbryhjgvshj7111f11blzkj0n6wfc")))

(define-public crate-cloning-0.0.2 (c (n "cloning") (v "0.0.2") (d (list (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "02x96hirbxk38a7qz7ffcy88l4vi3aaq22d8i1m8ding8gx0lfj8")))

