(define-module (crates-io cl on clone-replace) #:use-module (crates-io))

(define-public crate-clone-replace-0.1.0 (c (n "clone-replace") (v "0.1.0") (d (list (d (n "arc-swap") (r "^1") (d #t) (k 0)))) (h "0y1lrpg2mkz45lmn4ndwy6fxx5v1qmhpp8yqlfl1c1ypgyn7ay2x")))

(define-public crate-clone-replace-0.1.1 (c (n "clone-replace") (v "0.1.1") (d (list (d (n "arc-swap") (r "^1") (d #t) (k 0)))) (h "1d33pk0cipny8h74p8z877yqngjw7dp57pqhag8lkjqbhhw174j2")))

