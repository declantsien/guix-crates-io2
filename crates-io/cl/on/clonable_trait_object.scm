(define-module (crates-io cl on clonable_trait_object) #:use-module (crates-io))

(define-public crate-clonable_trait_object-0.1.0 (c (n "clonable_trait_object") (v "0.1.0") (h "1d7m7zg5ls3c9chisglkk89rsk07lzackj6hpk5z3zxgvqz0bksm")))

(define-public crate-clonable_trait_object-0.1.1 (c (n "clonable_trait_object") (v "0.1.1") (h "1m5s10rn8vyqxg1wkx4bj3zkzs3amhj9ysjws40li50hkicbqs8p")))

