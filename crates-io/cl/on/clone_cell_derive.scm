(define-module (crates-io cl on clone_cell_derive) #:use-module (crates-io))

(define-public crate-clone_cell_derive-0.1.0 (c (n "clone_cell_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0054nbqvlnk62f6ykfmnx18f380q997dfpvlignp4g2xk0s5v03s")))

(define-public crate-clone_cell_derive-0.1.1 (c (n "clone_cell_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0s8kmxpd802zrs2qq9289ka4q46x6xhwdd9gd68ba7k1pblnbj38")))

(define-public crate-clone_cell_derive-0.2.0 (c (n "clone_cell_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1a3d8781ad1d9rplc01ik053h6p8a214qgfr7vsdinhih9l1xpbf")))

