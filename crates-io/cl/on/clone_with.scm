(define-module (crates-io cl on clone_with) #:use-module (crates-io))

(define-public crate-clone_with-0.1.0 (c (n "clone_with") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0iqjbcvqzd98lg092q9y4xb51k9byx0chs5pd2gkk28i7ikddmqg")))

