(define-module (crates-io cl on clone_dyn_meta) #:use-module (crates-io))

(define-public crate-clone_dyn_meta-0.1.0 (c (n "clone_dyn_meta") (v "0.1.0") (d (list (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "11ywxg84xyf4bvi2j95zq3mcwj4sb40nb69dcnma84cg5w6pqr75") (f (quote (("use_std") ("use_alloc") ("full") ("default"))))))

(define-public crate-clone_dyn_meta-0.1.1 (c (n "clone_dyn_meta") (v "0.1.1") (d (list (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0bn4492y1mn0276rcbibrpf6zbw9dv4mk5pgwd9z2rmainhf72bn") (f (quote (("use_std") ("use_alloc") ("full") ("default"))))))

(define-public crate-clone_dyn_meta-0.2.0 (c (n "clone_dyn_meta") (v "0.2.0") (d (list (d (n "macro_tools") (r "^0.2.0") (k 0)) (d (n "test_tools") (r "^0.1.5") (k 2)))) (h "1im06df62w3jpwyqzcb023br2ps49cwz8d2rb72mfig0nhv143jl") (f (quote (("full") ("enabled") ("default"))))))

(define-public crate-clone_dyn_meta-0.3.0 (c (n "clone_dyn_meta") (v "0.3.0") (d (list (d (n "macro_tools") (r "^0.2.0") (k 0)) (d (n "test_tools") (r "^0.1.5") (k 2)))) (h "19h1flpciiip5l5j13n67m5r8gppmpj08ayq48gcg89s8iynw2s2") (f (quote (("full") ("enabled") ("default"))))))

(define-public crate-clone_dyn_meta-0.4.0 (c (n "clone_dyn_meta") (v "0.4.0") (d (list (d (n "macro_tools") (r "^0.2.0") (k 0)) (d (n "test_tools") (r "^0.1.5") (k 2)))) (h "0bj3524430jnhf8s1mmnyy9f9xf764y7ix9fnz847rb5kmicigaa") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.5.0 (c (n "clone_dyn_meta") (v "0.5.0") (d (list (d (n "macro_tools") (r "~0.4.0") (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "15l3c0a0rgj0csbhsh3dkx54zipbsq2vn7fsb1jshxm216j2z0n6") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.6.0 (c (n "clone_dyn_meta") (v "0.6.0") (d (list (d (n "macro_tools") (r "~0.5.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "110ja5h0gbxfmdx6md1jd7s5wfb77lkdkfrvryf2rwv7g4b1p5hh") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.7.0 (c (n "clone_dyn_meta") (v "0.7.0") (d (list (d (n "macro_tools") (r "~0.6.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1i2gw5kj0gs2jyyh89lsd1vjfbas1n6ij9zcz01dmr875x8157k7") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.8.0 (c (n "clone_dyn_meta") (v "0.8.0") (d (list (d (n "macro_tools") (r "~0.8.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0bbklwpg4csr0ip9lbynisl0j4ndmxv4lphpcmss46qni72ybcxx") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.9.0 (c (n "clone_dyn_meta") (v "0.9.0") (d (list (d (n "macro_tools") (r "~0.15.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0jxlvsfi9i5j71555dbnxrhnphgzr02n8dk6v66c17x8mmcizs9l") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.10.0 (c (n "clone_dyn_meta") (v "0.10.0") (d (list (d (n "macro_tools") (r "~0.16.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1vv2szms4b5z7paqrz4f61pdirch17pr8ikjz78fis3w3vbqda3n") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.11.0 (c (n "clone_dyn_meta") (v "0.11.0") (d (list (d (n "macro_tools") (r "~0.18.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1k6z804m5lsrzh16nr8hghzk0w0d1bixdgjhjmkp7s9sljg9qc4c") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.12.0 (c (n "clone_dyn_meta") (v "0.12.0") (d (list (d (n "macro_tools") (r "~0.19.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0fqykyw5yl7dqz9lh7ldmlz0xd57qk7vy23r4wwqcvd4m2i1hnql") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.13.0 (c (n "clone_dyn_meta") (v "0.13.0") (d (list (d (n "macro_tools") (r "~0.20.0") (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "0cz4lpm4alw0anf8y1vsg3j0czfxci5yf8pp70jy8xasd7gcm58f") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.14.0 (c (n "clone_dyn_meta") (v "0.14.0") (d (list (d (n "macro_tools") (r "~0.21.0") (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "0r7xnk7xzbca9k8c46w0zymgn1fg2mnb1iy965qdbkmkdyh7y195") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.15.0 (c (n "clone_dyn_meta") (v "0.15.0") (d (list (d (n "macro_tools") (r "~0.23.0") (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "06d7sggby9mg7ab0sy176bj8vqpb9020whkvgk3n55xkw4ggxarg") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn_meta-0.16.0 (c (n "clone_dyn_meta") (v "0.16.0") (d (list (d (n "macro_tools") (r "~0.24.0") (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "06l0h6ph1vgwz6ihp1acccfck6agb77hlsprpyznfmcpgjhbjnan") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

