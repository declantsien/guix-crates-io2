(define-module (crates-io cl on clone_dyn) #:use-module (crates-io))

(define-public crate-clone_dyn-0.1.0 (c (n "clone_dyn") (v "0.1.0") (d (list (d (n "clone_dyn_meta") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0ygiap0d4k8q38pqgkzsjngmxb00bab4ac9cs248lhlxmdh26afw") (f (quote (("use_std") ("use_alloc") ("full" "use_std") ("default" "use_std"))))))

(define-public crate-clone_dyn-0.4.0 (c (n "clone_dyn") (v "0.4.0") (d (list (d (n "clone_dyn_meta") (r "^0.4.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "^0.1.5") (k 2)))) (h "1fpkbfcfszr053h8f9d2i6mavcnvrda6zz2napcg3lwwmixjch49") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.5.0 (c (n "clone_dyn") (v "0.5.0") (d (list (d (n "clone_dyn_meta") (r "~0.5.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "11ylp8jhws5r05adl3gjr1k9i04mfc7pmza2cw85aqk7pivlc3p4") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.6.0 (c (n "clone_dyn") (v "0.6.0") (d (list (d (n "clone_dyn_meta") (r "~0.6.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "04qkr1198z8rx9gi5kdxmwy6p7zxpkmq5kfhm8ld64gqd76ic54b") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.7.0 (c (n "clone_dyn") (v "0.7.0") (d (list (d (n "clone_dyn_meta") (r "~0.7.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1nxq0nivz0dw48c2svya9ggxsly0d9nhmv0qwdyblxgz590144vw") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.8.0 (c (n "clone_dyn") (v "0.8.0") (d (list (d (n "clone_dyn_meta") (r "~0.8.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0q7mcjisvzdq0jp3cnr50r72k8nqqyqg1dn00l9fj2g7wwmj7ng9") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.9.0 (c (n "clone_dyn") (v "0.9.0") (d (list (d (n "clone_dyn_meta") (r "~0.9.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1fnkv6liprg8s6zr7p1k2lm7nghnk85xn6yf3jm9ip8217gbiijg") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "clone_dyn_meta/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.10.0 (c (n "clone_dyn") (v "0.10.0") (d (list (d (n "clone_dyn_meta") (r "~0.10.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1si5n4s3dwf44vng946h093gi434wc0g5ja2fc98zjxvah96nj5a") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "clone_dyn_meta/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.11.0 (c (n "clone_dyn") (v "0.11.0") (d (list (d (n "clone_dyn_meta") (r "~0.11.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0mw6gbmnhwfwhgy5vvfbasqyl9xvyb8f1xcj21mfzrlfyvq1hrz1") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "clone_dyn_meta/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.12.0 (c (n "clone_dyn") (v "0.12.0") (d (list (d (n "clone_dyn_meta") (r "~0.12.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1p22sq9czg9kv2ak3i2kax1zzy8b5dggsqjpb7xd3r8gnfg44dx8") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "clone_dyn_meta/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.13.0 (c (n "clone_dyn") (v "0.13.0") (d (list (d (n "clone_dyn_meta") (r "~0.13.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1l361ppdcjgsp8x1dyijqadilhwcf70y53h1ry9hcyg9i1n4m74x") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "clone_dyn_meta/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.14.0 (c (n "clone_dyn") (v "0.14.0") (d (list (d (n "clone_dyn_meta") (r "~0.14.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "0qp3za0aj3ph40nvsk0vi9bylw6nvmcp6rk353s86q93z15c6jyg") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "clone_dyn_meta/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.15.0 (c (n "clone_dyn") (v "0.15.0") (d (list (d (n "clone_dyn_meta") (r "~0.15.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "1p8s6ld9b2vk1qzmzyidq60s3zh852lnpyzlkw7ym706jk7h1yif") (f (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled" "clone_dyn_meta/enabled") ("default" "enabled"))))))

(define-public crate-clone_dyn-0.16.0 (c (n "clone_dyn") (v "0.16.0") (d (list (d (n "clone_dyn_meta") (r "~0.16.0") (f (quote ("enabled"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "12ich2g6kwfngdifk9njg53nqhvhjwgvnqy4fp8ifghfas427gf7") (f (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled" "clone_dyn_meta/enabled") ("default" "enabled"))))))

