(define-module (crates-io cl on clonesure) #:use-module (crates-io))

(define-public crate-clonesure-0.1.0 (c (n "clonesure") (v "0.1.0") (h "0rk7cv7w4mi4sawc25qr0jy79zfv1m3hr4nxc6dsd9mjw5cnrxgw")))

(define-public crate-clonesure-0.2.0 (c (n "clonesure") (v "0.2.0") (h "0jizxc7q0yk76lkdhqpi1dpr8qz3i1hnc9z2234iv3zzw185zfxl")))

(define-public crate-clonesure-0.3.0 (c (n "clonesure") (v "0.3.0") (h "1n6b8b4nsx8sans3s2j33dms55iz06bkj5jqqha6fvdj2r4fq175")))

