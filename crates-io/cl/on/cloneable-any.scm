(define-module (crates-io cl on cloneable-any) #:use-module (crates-io))

(define-public crate-cloneable-any-0.0.0 (c (n "cloneable-any") (v "0.0.0") (h "18m0miqbv4dwgs6bsc0p2vl05b05prybr0pg5idilqxw3nx239rq") (y #t)))

(define-public crate-cloneable-any-0.1.0 (c (n "cloneable-any") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.2") (k 0)) (d (n "dyn-clone") (r "^1.0") (k 0)))) (h "1g0i8gwm7xbmh0hhhmsp8faxk7l5zclln0ydz7b64q9n2kwd83hd") (f (quote (("std" "downcast-rs/std") ("default" "std"))))))

