(define-module (crates-io cl on clonedir_lib) #:use-module (crates-io))

(define-public crate-clonedir_lib-0.1.0 (c (n "clonedir_lib") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "reflink") (r "^0.1.2") (d #t) (k 0)))) (h "1xq297ymssq1kw7rglf6pa388m2fsabcb9lvzhk2mkjy252ai0bk")))

(define-public crate-clonedir_lib-0.1.1 (c (n "clonedir_lib") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "reflink") (r "^0.1.2") (d #t) (k 0)))) (h "07j4fr8nr0z546bnv7s93p4qbw6gib5lb7wzgvfhydmw9xqi4w0a")))

