(define-module (crates-io cl on clone-fields) #:use-module (crates-io))

(define-public crate-clone-fields-0.1.0 (c (n "clone-fields") (v "0.1.0") (d (list (d (n "clone-fields-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1qwvp5qywxyyd0rxhn1wykr0ljvb7bcw00nmql172lvxzjw26awc")))

(define-public crate-clone-fields-0.4.0 (c (n "clone-fields") (v "0.4.0") (d (list (d (n "clone-fields-derive") (r "^0.4.0") (d #t) (k 0)))) (h "0gnilw84ma7c782mgpm7rah4jclij6snk3mymhwa3g35nrbd90m8")))

(define-public crate-clone-fields-0.4.1 (c (n "clone-fields") (v "0.4.1") (d (list (d (n "fields-converter-derive") (r "^0.1.0") (d #t) (k 0)))) (h "00fj05acn3yz1gqr7l5zfp247wrb8mvyi86abdsnhrzs1664rzb3")))

(define-public crate-clone-fields-0.5.0 (c (n "clone-fields") (v "0.5.0") (d (list (d (n "fields-converter-derive") (r "^0.1.3") (d #t) (k 0)))) (h "16pv9frn3c887p2f79p3w8ikrhgf5j2xlnqd6mapgbl0173njlf4")))

