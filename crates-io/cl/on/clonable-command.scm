(define-module (crates-io cl on clonable-command) #:use-module (crates-io))

(define-public crate-clonable-command-0.1.0 (c (n "clonable-command") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16v1bykcdbm8wg29rsgr7z917wv9sbynszgk1dwzw2gyd1hxwh21")))

(define-public crate-clonable-command-0.2.0 (c (n "clonable-command") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1k9bxn76ai31q9m8sgw1kpq2q0c142bn04hw5mvv5h7hs9x4fk59")))

