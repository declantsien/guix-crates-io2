(define-module (crates-io cl on clone_into_derive) #:use-module (crates-io))

(define-public crate-clone_into_derive-0.1.0 (c (n "clone_into_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sj4jm286wfgsjvy2v6njvnsvgjq73wmhrr1xfvn21s13mn54ij9")))

(define-public crate-clone_into_derive-0.1.1 (c (n "clone_into_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zyi3ncvp1jg458jx1ca63cf9djka67ziq9bdy94l3381n605kms")))

