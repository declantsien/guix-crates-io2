(define-module (crates-io cl on clone-github-subdir) #:use-module (crates-io))

(define-public crate-clone-github-subdir-0.1.0 (c (n "clone-github-subdir") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0b0s5jri5xkmpgnwqpp8k86wy22f5pq8xxg03v76rv48h7hf7pjn")))

(define-public crate-clone-github-subdir-0.1.1 (c (n "clone-github-subdir") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1d6cvgr4j6yvcs6da1p13sp5dvndy5gjww8j1951bydlbwx8dyq3")))

