(define-module (crates-io cl on cloner) #:use-module (crates-io))

(define-public crate-cloner-0.1.0 (c (n "cloner") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "13iz1fjl32sc3za67iar9cwl7q2qjphvbhwphdcig1vhx8qcbyg0")))

(define-public crate-cloner-0.1.1 (c (n "cloner") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "01b3j79p00kqlpwf166pw66vjckcvryw613zg041c5qq7zy258jb")))

