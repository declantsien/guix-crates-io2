(define-module (crates-io cl on clone) #:use-module (crates-io))

(define-public crate-clone-0.1.0 (c (n "clone") (v "0.1.0") (d (list (d (n "void") (r "^1") (k 0)))) (h "0whx6kc7h8jbskd03l56sxs4c83ap786ir2k8r066rl22bgcvlc6")))

(define-public crate-clone-0.1.1 (c (n "clone") (v "0.1.1") (d (list (d (n "void") (r "^1") (k 0)))) (h "0jw5r81mkgqwwqslbv906d6x6zd0h22win56lfrd4yjk68pfpr1n")))

(define-public crate-clone-0.1.2 (c (n "clone") (v "0.1.2") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)))) (h "1k0ab5ba3bxniqd2c6r1bkxrw82d9pzkqq4b279bcg6dfvak8ck7")))

(define-public crate-clone-0.0.0 (c (n "clone") (v "0.0.0") (h "0y8q0d8c79g19qxxz9sd5c274nqffvxw95zvmxap3x57cih654zk")))

