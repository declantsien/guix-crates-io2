(define-module (crates-io cl on clone_box) #:use-module (crates-io))

(define-public crate-clone_box-0.1.0 (c (n "clone_box") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1nrhmjfjhh82y1kmfgzqv6mfdd15fz5ngjgikwjvwmcjqbjx31ra")))

(define-public crate-clone_box-0.1.1 (c (n "clone_box") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1hjbl6fbqjr67723f09s44r26azw8xncs0v955pyxmfa73za2v91")))

(define-public crate-clone_box-0.1.2 (c (n "clone_box") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "08mb6nqj6s2ihk344wml1b4b9spxvyq1x64lfv2cqkdd9nh1zy76")))

(define-public crate-clone_box-0.1.3 (c (n "clone_box") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0mapfq5xnmwpyd5qjmd9s23a0nmikcq0w9jqh2hc7ldj9wfgdi4l")))

