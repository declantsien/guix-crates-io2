(define-module (crates-io cl on cloneable_dyn) #:use-module (crates-io))

(define-public crate-cloneable_dyn-0.1.0 (c (n "cloneable_dyn") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "021fdix6gbxxaajzlw3q8fvrsqa1qcax4zv8nx6c6svnskcl7l1d")))

(define-public crate-cloneable_dyn-0.1.1 (c (n "cloneable_dyn") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "155fahi92m4km20maf953vv2596cssl58pd2nv53w5q7zhwfv1ya")))

