(define-module (crates-io cl on clone-block) #:use-module (crates-io))

(define-public crate-clone-block-0.1.0 (c (n "clone-block") (v "0.1.0") (h "02x2mvjgw9dk20qjxy3p0nqjas36q1978lvyi1kaav6dlslkkr9s")))

(define-public crate-clone-block-0.1.1 (c (n "clone-block") (v "0.1.1") (h "0i33bx4hbwhdia75dgqldb4dmm3zp4q6fxi3bhyq06l5hjsisj3g")))

