(define-module (crates-io cl on clonelet) #:use-module (crates-io))

(define-public crate-clonelet-0.1.0 (c (n "clonelet") (v "0.1.0") (h "04zhavhh2wj6crx7j29a0d3sqsp46dz4y2fal9g626m7l9ygfr43")))

(define-public crate-clonelet-0.2.0 (c (n "clonelet") (v "0.2.0") (h "0dycdyzxc60nb17gsxi6qdy3hvdhwgqf73lqw1iy5x8fydq0qabd")))

