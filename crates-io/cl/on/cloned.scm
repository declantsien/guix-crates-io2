(define-module (crates-io cl on cloned) #:use-module (crates-io))

(define-public crate-cloned-0.1.0 (c (n "cloned") (v "0.1.0") (h "1j2zkr27q0nh1zsqpr699q0882ysjhl4lxj7xjgmfq16y5s9rkn2")))

(define-public crate-cloned-0.1.1 (c (n "cloned") (v "0.1.1") (h "1r09w9bp4d0dadid42dyd667w2825qvijprx4f1ajpaf80gvfhxl")))

