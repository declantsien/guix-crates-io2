(define-module (crates-io cl on clonablechild) #:use-module (crates-io))

(define-public crate-clonablechild-0.1.0 (c (n "clonablechild") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x1ai3qck4afq3byrcv5sajw091ipsr5gxckifwrwvi1ihlb4knp") (y #t)))

(define-public crate-clonablechild-0.1.1 (c (n "clonablechild") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0982dmyym5dzpan9nvhg3hq1mi4q2azr47wsvpxj3sf1a2l4cjaa") (y #t)))

