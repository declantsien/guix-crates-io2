(define-module (crates-io cl on clone-file) #:use-module (crates-io))

(define-public crate-clone-file-0.1.0 (c (n "clone-file") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1cp9g1jgrj1vyw1yl4rb7f8nallrnk9phx7v9l6pabdf89y4161r")))

