(define-module (crates-io cl -c cl-calc) #:use-module (crates-io))

(define-public crate-cl-calc-1.0.0 (c (n "cl-calc") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)))) (h "0p9l3mfnz4y4zzw02ksswa8y6nrshy1m2lpsb06dax4w5fh0d4jm")))

(define-public crate-cl-calc-1.0.1 (c (n "cl-calc") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)))) (h "1adzmf1nancby96cpcnfy38gs3g51l8jysdwxyp2sv49m6p6r88b")))

(define-public crate-cl-calc-1.0.2 (c (n "cl-calc") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)))) (h "0f259lp1r2rswpxhifc7995y10g91yzm3dh8kz7186cqgb7k1dqn")))

