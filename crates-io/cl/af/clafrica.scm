(define-module (crates-io cl af clafrica) #:use-module (crates-io))

(define-public crate-clafrica-0.1.0 (c (n "clafrica") (v "0.1.0") (d (list (d (n "clafrica-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)))) (h "15z4mqbxq4wp2psjai7ynqp923vprxhpgk4lh0wfp6hn3sar5v7b") (y #t)))

(define-public crate-clafrica-0.1.1 (c (n "clafrica") (v "0.1.1") (d (list (d (n "clafrica-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)))) (h "1bv56yfzqyfr4l8nxq5cvma33bmj8fqm96gajizaxqgzjv9m3al8")))

(define-public crate-clafrica-0.2.0 (c (n "clafrica") (v "0.2.0") (d (list (d (n "clafrica-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)) (d (n "rdev") (r "^0.5.2") (d #t) (k 0)))) (h "0anb6zdzyb27v0iqga8vj72mxkh2w1niwz1hqv161cp07jib8wxi")))

(define-public crate-clafrica-0.2.1 (c (n "clafrica") (v "0.2.1") (d (list (d (n "clafrica-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)) (d (n "rdev") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0jf67gcc4s8ny70kbhjprx4xsh6kwzqjigwglhhpjqs3qcih7zqg")))

(define-public crate-clafrica-0.2.2 (c (n "clafrica") (v "0.2.2") (d (list (d (n "clafrica-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)) (d (n "rdev") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0j4vny3iilcyskaf0i4s4h71l45y8h7ww7lmgk4f6mydq9xsqpd2")))

(define-public crate-clafrica-0.3.0 (c (n "clafrica") (v "0.3.0") (d (list (d (n "clafrica-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)) (d (n "rdev") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0057a8s1jvf28lb9qd8hpgilxc76prwh3c736rcanqjrihhgsnpk")))

(define-public crate-clafrica-0.3.1 (c (n "clafrica") (v "0.3.1") (d (list (d (n "clafrica-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)) (d (n "rdev") (r "^0.5.2") (d #t) (k 0)) (d (n "rstk") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0ylx88jgadba6c9mrqxy1faw7w47hljz9j1214293znd32d356pn")))

(define-public crate-clafrica-0.4.0 (c (n "clafrica") (v "0.4.0") (d (list (d (n "clafrica-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)) (d (n "rdev") (r "^0.5.2") (d #t) (k 0)) (d (n "rhai") (r "^1.15.1") (d #t) (k 0)) (d (n "rstk") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "046k2gsjblbbflkcnwvxj09gjyih2ziwxh7zs8ajmzsjh2427a29")))

(define-public crate-clafrica-0.4.1 (c (n "clafrica") (v "0.4.1") (d (list (d (n "clafrica-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)) (d (n "rdev") (r "^0.5.2") (d #t) (k 0)) (d (n "rhai") (r "^1.15.1") (d #t) (k 0)) (d (n "rstk") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0rnrdb7qns95bbc45wgh5xf87vnk6hwbz1k8zrgzigyx8zxyrwyz")))

