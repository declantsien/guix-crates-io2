(define-module (crates-io cl af clafrica-lib) #:use-module (crates-io))

(define-public crate-clafrica-lib-0.1.0 (c (n "clafrica-lib") (v "0.1.0") (h "0qg3hpwd88n483nwxic1m499bq3xbj65x2ch7sxwi1lkj70768kn")))

(define-public crate-clafrica-lib-0.1.1 (c (n "clafrica-lib") (v "0.1.1") (h "1jgbz8cc59lwca574kayg05jn5wyxyv4jwnrhyc0cg0y5kmlmn0w")))

(define-public crate-clafrica-lib-0.2.0 (c (n "clafrica-lib") (v "0.2.0") (h "1hzhlsxnxh9qjjmr1pnbznjw05zljfkd2a6af62a3d8xz2fmxjf6")))

(define-public crate-clafrica-lib-0.3.0 (c (n "clafrica-lib") (v "0.3.0") (h "1w0j19vkkq7afac374n6kmzx45cd1kdldhlw72jgxk331vxrx54w")))

(define-public crate-clafrica-lib-0.3.1 (c (n "clafrica-lib") (v "0.3.1") (h "076j9xjk7zsm7aq34lkk6m7z3c7n83cipmya2jr55zkyp9bmgbhr")))

(define-public crate-clafrica-lib-0.3.2 (c (n "clafrica-lib") (v "0.3.2") (h "01azxb3wxqh2ycz4sj8jwbb4r8xicknqrhw0fv9i14ahjxr9sgg2")))

