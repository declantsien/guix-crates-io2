(define-module (crates-io cl af clafrica-wish) #:use-module (crates-io))

(define-public crate-clafrica-wish-0.1.0 (c (n "clafrica-wish") (v "0.1.0") (d (list (d (n "clafrica") (r "^0.2.0") (d #t) (k 0)) (d (n "rstk") (r "^0.1.0") (d #t) (k 0)))) (h "196l7cx75xza1wy04acxfyjwlqv130frf6gl8lmhwpq4g99fm5aa") (y #t)))

(define-public crate-clafrica-wish-0.1.1 (c (n "clafrica-wish") (v "0.1.1") (d (list (d (n "clafrica") (r "^0.2.0") (d #t) (k 0)) (d (n "rstk") (r "^0.1.0") (d #t) (k 0)))) (h "00ca5z556p3f9i8n6kxwv2ian5288cls9hcf7mbliifhzhvrmr6x")))

(define-public crate-clafrica-wish-0.2.0 (c (n "clafrica-wish") (v "0.2.0") (d (list (d (n "clafrica") (r "^0.2.1") (d #t) (k 0)) (d (n "rstk") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "10grm44ix7zd71430s4l6n6x1w4qxi9yh666818qy02i5g6lm8ip")))

(define-public crate-clafrica-wish-0.2.1 (c (n "clafrica-wish") (v "0.2.1") (d (list (d (n "clafrica") (r "^0.2.2") (d #t) (k 0)) (d (n "rstk") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0rrghlghlr8snpljr6wzyyv53yqpzf6m7j61accx25jy44h651gz")))

(define-public crate-clafrica-wish-0.3.0 (c (n "clafrica-wish") (v "0.3.0") (d (list (d (n "clafrica") (r "^0.4.0") (d #t) (k 0)) (d (n "rstk") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "09mcrl4ix7q182gq892q475h79w2awh1qb0sqi5a42ddak4y8r02")))

(define-public crate-clafrica-wish-0.3.1 (c (n "clafrica-wish") (v "0.3.1") (d (list (d (n "clafrica") (r "^0.4.0") (d #t) (k 0)) (d (n "rstk") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0wv70310p2yrichz2b25361jvpar0j4jdfglj8zhzbaw24zx9z2n")))

