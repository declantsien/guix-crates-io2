(define-module (crates-io cl ob clob) #:use-module (crates-io))

(define-public crate-clob-0.1.0 (c (n "clob") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive" "min_const_generics"))) (d #t) (k 0)))) (h "0zsad3cg66his6iw7isbhfkbkbjh778aa2h35vfdh2mbh110csjp") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

