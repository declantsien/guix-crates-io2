(define-module (crates-io cl _u cl_utils) #:use-module (crates-io))

(define-public crate-cl_utils-0.0.1 (c (n "cl_utils") (v "0.0.1") (h "07s2z5m5nkzqmyzdaani38qfn0hmxi1i206hq3ypk4wd4r33za4k")))

(define-public crate-cl_utils-0.0.2 (c (n "cl_utils") (v "0.0.2") (h "0r27ip5x3l0vxn7xcsf2n59rm1iwajgs0vfbkmq32knpq5md6zkr")))

