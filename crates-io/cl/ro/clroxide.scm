(define-module (crates-io cl ro clroxide) #:use-module (crates-io))

(define-public crate-clroxide-1.0.1 (c (n "clroxide") (v "1.0.1") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1ybccfnbh6q87j872gd8mxqllzfq0pgmjs92s4ara4zpc90smgbi") (f (quote (("default-loader" "windows/Win32_System_LibraryLoader") ("default" "default-loader") ("debug"))))))

(define-public crate-clroxide-1.0.4 (c (n "clroxide") (v "1.0.4") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0prn6dr7xhjxs9y8cqzi8yijxp5zy656mfaz8gah8aihwal5vag4") (f (quote (("default-loader" "windows/Win32_System_LibraryLoader") ("default" "default-loader") ("debug"))))))

(define-public crate-clroxide-1.0.5 (c (n "clroxide") (v "1.0.5") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1apqg8135i3w82fy318wiakby0w48m3qph2vmyr8fzipsg74n09x") (f (quote (("default-loader" "windows/Win32_System_LibraryLoader") ("default" "default-loader") ("debug"))))))

(define-public crate-clroxide-1.0.6 (c (n "clroxide") (v "1.0.6") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)) (d (n "windows") (r "^0.44.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole" "Win32_System_Memory"))) (d #t) (k 2)))) (h "18wab5vd2d6hiw1cbrwxr1l47n7ipbd57j0i9f2062p5fzfdpxqz") (f (quote (("default-loader" "windows/Win32_System_LibraryLoader") ("default" "default-loader") ("debug"))))))

(define-public crate-clroxide-1.0.9 (c (n "clroxide") (v "1.0.9") (d (list (d (n "windows") (r "^0.46.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)) (d (n "windows") (r "^0.46.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole" "Win32_System_Memory"))) (d #t) (k 2)))) (h "1xrfhs3y3m9m8zlv4crrg1awap1ahjn0hynn3likr58lmd6bwx4i") (f (quote (("default-loader" "windows/Win32_System_LibraryLoader") ("default" "default-loader") ("debug"))))))

(define-public crate-clroxide-1.1.0 (c (n "clroxide") (v "1.1.0") (d (list (d (n "windows") (r "^0.46.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)) (d (n "windows") (r "^0.46.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole" "Win32_System_Memory"))) (d #t) (k 2)))) (h "1chxfivj8da89mr4km0qsai2zgla4lxz3s7rx9vqp1f8v1dswl7l") (f (quote (("default-loader" "windows/Win32_System_LibraryLoader") ("default" "default-loader") ("debug"))))))

(define-public crate-clroxide-1.1.1 (c (n "clroxide") (v "1.1.1") (d (list (d (n "windows") (r "^0.46.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)) (d (n "windows") (r "^0.46.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole" "Win32_System_Memory"))) (d #t) (k 2)))) (h "1wsf1fkcjm18kpbqsa200m6z0j6k2k2phw7s53nlnvysqcm1f6i8") (f (quote (("default-loader" "windows/Win32_System_LibraryLoader") ("default" "default-loader") ("debug"))))))

