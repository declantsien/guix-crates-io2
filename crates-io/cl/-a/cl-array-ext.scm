(define-module (crates-io cl -a cl-array-ext) #:use-module (crates-io))

(define-public crate-cl-array-ext-0.1.0 (c (n "cl-array-ext") (v "0.1.0") (h "1kn134hfrm52c531a9ahmgpcaglgyg3a44c8a0777grb61qc0gcz") (y #t)))

(define-public crate-cl-array-ext-0.1.1 (c (n "cl-array-ext") (v "0.1.1") (h "12xqy4ac06x2q5prxmn9dydrrrkhb9wpam32mi782lnxcflq8pbj")))

(define-public crate-cl-array-ext-0.1.2 (c (n "cl-array-ext") (v "0.1.2") (h "0106rvhknb7xxisb42y7l446k8d39pwh93jm9akf6sbs5ayl4k09")))

(define-public crate-cl-array-ext-0.1.3 (c (n "cl-array-ext") (v "0.1.3") (h "1jy19b6y4hksa24y482zz4bhk49xym7fwng0929rsdw437mcya6v")))

(define-public crate-cl-array-ext-0.1.4 (c (n "cl-array-ext") (v "0.1.4") (h "0cmf2q7d9rz7a46x3k0s5qv82rj03b5mv6hcgbszdinh95ld5kk5")))

(define-public crate-cl-array-ext-0.1.5 (c (n "cl-array-ext") (v "0.1.5") (h "1n0nw64vhbb1x039hh5rvvjgiypxca3i5s89mjvf17097wv5q3kf")))

