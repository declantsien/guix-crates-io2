(define-module (crates-io cl y- cly-impl) #:use-module (crates-io))

(define-public crate-cly-impl-0.1.0 (c (n "cly-impl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "isnt") (r "^0.1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)) (d (n "repc-impl") (r "^0.1.0") (d #t) (k 0)))) (h "1drbaasyfhscc7c6mn99xxh6ahsyqfzzzccz400n1iywxp1gf990")))

(define-public crate-cly-impl-0.1.1 (c (n "cly-impl") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "isnt") (r "^0.1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)) (d (n "repc-impl") (r "^0.1.1") (d #t) (k 0)))) (h "0zay7clxdarrmkzdi9b3hkvn1xz7f8jq0195jfl6kxmqp5rwskmm")))

