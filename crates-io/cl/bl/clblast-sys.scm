(define-module (crates-io cl bl clblast-sys) #:use-module (crates-io))

(define-public crate-clblast-sys-0.1.0 (c (n "clblast-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cl-sys") (r "^0.4.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 2)) (d (n "ocl-core") (r "^0.11.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)))) (h "1gbpnyq68zlriybkga27qdhx7nq7hqqvkdnqzf38bq43pqn19pws")))

(define-public crate-clblast-sys-0.2.0 (c (n "clblast-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cl-sys") (r "^0.4.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 2)))) (h "1y5x7bp8a8yr41i8d4g5np1w3s5az7g63yj8g4gcw5rk12rjd1vj")))

(define-public crate-clblast-sys-0.2.1 (c (n "clblast-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cl-sys") (r "^0.4.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ocl") (r "^0.19.3") (d #t) (k 2)))) (h "1n44ny4wkx4pp72x64i4xi1xr8ysjjalvnbjyig81nslgii3rhrd")))

