(define-module (crates-io cl ok clokwerk) #:use-module (crates-io))

(define-public crate-clokwerk-0.1.0 (c (n "clokwerk") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0pii7cpkppdqzj7zv72abgfc82sc0r51yvwsb9k5hwcjhd2fabzx")))

(define-public crate-clokwerk-0.2.0 (c (n "clokwerk") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0mm95fvdm72maa8h8px5s37y9kyyd64iffpan6fcppk4mi5zyp41")))

(define-public crate-clokwerk-0.2.1 (c (n "clokwerk") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1b2gdb2qaw28i9qwpbzx331v0fglghrzk0hgajaa489q6qyckpix")))

(define-public crate-clokwerk-0.2.2 (c (n "clokwerk") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0fc8zizv5r84gn1nf87ww0dcv2hfcfnxrvvivc5k1x0a5i2v2vz9")))

(define-public crate-clokwerk-0.3.0 (c (n "clokwerk") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 2)))) (h "002m8sanss4m6bv5v7ih4v6zq04b1r992r6y51h3vn12js230mbh")))

(define-public crate-clokwerk-0.3.1 (c (n "clokwerk") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 2)))) (h "1mki1msfs7dxidc7n34nl0z7a8yb0yjh76ncqcwv3wgycx62ppqr") (y #t)))

(define-public crate-clokwerk-0.3.2 (c (n "clokwerk") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 2)))) (h "0m1xxbm8yhbmfhp1vvsg0lwyw3kzcbvlz1grsvdb316d6chf3vl3") (y #t)))

(define-public crate-clokwerk-0.3.3 (c (n "clokwerk") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 2)))) (h "01pc5jwn24izkri6rl8pfbpkhn8dnl4yr9dcsnrd4dpzjlj5b64z")))

(define-public crate-clokwerk-0.3.4 (c (n "clokwerk") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 2)))) (h "1ha8vjhzninb07804dx4a6pbhmywdn5m9gv2rka2iynf79nplygr")))

(define-public crate-clokwerk-0.3.5 (c (n "clokwerk") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 2)))) (h "1a9pka2470n6mvsjq78zlfn5xbp4c0pvslhdvs7yqbkqvwz3g6bs")))

(define-public crate-clokwerk-0.4.0-rc1 (c (n "clokwerk") (v "0.4.0-rc1") (d (list (d (n "async-std") (r "^1.9") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 2)) (d (n "tokio") (r "^1.5") (f (quote ("rt" "time"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0wxz44g9d1m5kzd3fg1x1h03sfq6l5njksijhig9mqs87ymj5jlc") (f (quote (("default" "async") ("async"))))))

(define-public crate-clokwerk-0.4.0 (c (n "clokwerk") (v "0.4.0") (d (list (d (n "async-std") (r "^1.9") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 2)) (d (n "tokio") (r "^1.5") (f (quote ("rt" "time"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0xhdy480fj3zcrmwry3q86x1inrkdbmihrvsy7fpwvfbbwv8s45x") (f (quote (("default" "async") ("async"))))))

