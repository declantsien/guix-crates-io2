(define-module (crates-io cl oi cloister) #:use-module (crates-io))

(define-public crate-cloister-0.1.0 (c (n "cloister") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "00p7i83q7r63i38nxmkd7qdz6w8h86d3aq1k42ph1914znypdg7w")))

(define-public crate-cloister-0.1.1 (c (n "cloister") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "14v5yi54wqdwsjnsi64fr602jx7gks5smy52yfnlbpxllnj7gazg")))

