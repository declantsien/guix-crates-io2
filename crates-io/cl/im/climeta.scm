(define-module (crates-io cl im climeta) #:use-module (crates-io))

(define-public crate-climeta-0.1.0 (c (n "climeta") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "elsa") (r "^1.1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "13mw38vc9wi7lrd86pr5mpwlpl3s6729wp7idxyngn4q40r93az7")))

