(define-module (crates-io cl im climm) #:use-module (crates-io))

(define-public crate-climm-0.0.1 (c (n "climm") (v "0.0.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)))) (h "0wnmsxvqsqsa8557mi0nv7qk6dcig9qjcznz463lmk71bzpf6siq")))

(define-public crate-climm-0.0.2 (c (n "climm") (v "0.0.2") (h "1sb7h8vqws1zchhbl3ppcqi04xm3n7zcgsmvzy79wmfw7vbcqh9a")))

