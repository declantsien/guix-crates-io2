(define-module (crates-io cl im climage) #:use-module (crates-io))

(define-public crate-climage-0.1.0 (c (n "climage") (v "0.1.0") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "ocl") (r "^0.19.4") (d #t) (k 0)))) (h "13m324j0wcy6r4w2sl1pklm79p8xbkvcf5xsg65dkjvnbm9mpf8a")))

