(define-module (crates-io cl im climer) #:use-module (crates-io))

(define-public crate-climer-0.0.3 (c (n "climer") (v "0.0.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1xxzr5h4170fsqypg7dv44qcw8wy0zslha57mki7cw307jdab8b2")))

(define-public crate-climer-0.0.3-1 (c (n "climer") (v "0.0.3-1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0y0plq57is904xz5fq5mphib3949a2fr3c92l6pmh0jma7ik409h")))

(define-public crate-climer-0.0.4 (c (n "climer") (v "0.0.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1f133j85clhh3svdx1052rhp5vsdhf173dh3hvj42spwrxhryg8x")))

(define-public crate-climer-0.1.0 (c (n "climer") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0ahy374i77bss6pmg50zz2g9yd58p4ma5j24xc694kxjgsbll0hj")))

(define-public crate-climer-0.2.0 (c (n "climer") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0c33025bprsqas3i983szxja9b6gjwvz12lnz9y2jb2ik3vx7s84")))

(define-public crate-climer-0.3.0 (c (n "climer") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1fh61g33qspjgc70nkljg0043bvkq3x6dzfn15agrwh13c8cigfa") (f (quote (("cli" "clap"))))))

(define-public crate-climer-0.3.1 (c (n "climer") (v "0.3.1") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0n51rdznaamsc4lhd0rma3rzrw4440xrrsccs8r831v0h3lv0rd8") (f (quote (("cli" "clap"))))))

(define-public crate-climer-0.3.2 (c (n "climer") (v "0.3.2") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0n69g377vvbyy3lw2gwm0lv7m8b9him0rrkj38q3a1kiy5d84hdm") (f (quote (("cli" "clap"))))))

(define-public crate-climer-0.3.3 (c (n "climer") (v "0.3.3") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "043036p945xhwzh84brbmg07w8zmycv7h82c52czjix5znbn3dyz") (f (quote (("cli" "clap"))))))

(define-public crate-climer-0.3.4 (c (n "climer") (v "0.3.4") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "07xq7zf4dhv99klqgkzbn6vsr6szj9h8fwcxnvmyzbx21y7v0v4w") (f (quote (("cli" "clap"))))))

(define-public crate-climer-0.3.5 (c (n "climer") (v "0.3.5") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)))) (h "1ay60drddfkj7zsivby9fag7v293jq747as8dz6gnmg5pl7912ik") (f (quote (("serialize" "serde") ("cli" "clap"))))))

(define-public crate-climer-0.3.6 (c (n "climer") (v "0.3.6") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)))) (h "1vza7p1rhl82bcb07lvc4pk7595h1vkn1vikxrprbbx6l19zir5h") (f (quote (("serialize" "serde") ("cli" "clap"))))))

(define-public crate-climer-0.4.0 (c (n "climer") (v "0.4.0") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)))) (h "0bqhh81hbz42ll91nrdhbjqpgbc3glc3kj438zrs2sj6l25mk38j") (f (quote (("serialize" "serde") ("cli" "clap"))))))

(define-public crate-climer-0.5.0 (c (n "climer") (v "0.5.0") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)))) (h "0rd4jk1mcndgr7176macnsbrnlald08qrzmjc33vk8a1by55hf8c") (f (quote (("serialize" "serde") ("cli" "clap"))))))

(define-public crate-climer-0.6.0 (c (n "climer") (v "0.6.0") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)))) (h "0caxdw5lkf4hsg8l5h22qc582cw66zw52s5bjm1c5n8db7a1x3bv") (f (quote (("serialize" "serde") ("parser" "regex") ("default" "cli") ("cli" "clap" "parser"))))))

(define-public crate-climer-0.7.0 (c (n "climer") (v "0.7.0") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)))) (h "1zsnf2i5667446czcjibj000hqf0gr89abz1i3wc7h24p9g0hi3i") (f (quote (("serialize" "serde") ("parser" "regex") ("default" "cli") ("cli" "clap" "parser"))))))

(define-public crate-climer-0.7.1 (c (n "climer") (v "0.7.1") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "climer_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)))) (h "095xw8nlm5z78ap72j9cv781f4a03473n3qybk5v6r12ygrc10vd") (f (quote (("serialize" "serde") ("parser" "regex") ("default" "cli") ("cli" "clap" "parser"))))))

