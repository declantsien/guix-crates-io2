(define-module (crates-io cl im climb) #:use-module (crates-io))

(define-public crate-climb-0.1.0 (c (n "climb") (v "0.1.0") (h "0qpifbl92afbzixfq69cymqghqm0q0gjjcc6xdjd1wcal31c8557")))

(define-public crate-climb-0.1.1 (c (n "climb") (v "0.1.1") (h "0plshfxfylasqvqzh3210rzrjllx8v7lp0r4acn31xvfsm6qd7hn")))

(define-public crate-climb-1.0.0 (c (n "climb") (v "1.0.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0d3nllfq0swliqkcb15axfcv70akmb63bw5srv1zj7xpc8mx7c84")))

(define-public crate-climb-1.0.1 (c (n "climb") (v "1.0.1") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0jzy93pp4rn54ccj3pwq96vlikfv6zg6xx3flw1xa53z3rh9fa5d")))

(define-public crate-climb-1.0.11 (c (n "climb") (v "1.0.11") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "16jz18c86vbvrcmvvknr0hv7addp5kgyhnm4dskcwcn998x7akaw")))

(define-public crate-climb-1.0.2 (c (n "climb") (v "1.0.2") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1mhy9nvrpy2x67l9fd1m3cqsi87dh8l0cq3gamdc71awf4r9vhp5")))

(define-public crate-climb-1.0.21 (c (n "climb") (v "1.0.21") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0zf6ix1fk6d68z09mq56qg9s1i7zz6bd5705y0a59ilgh22463jr")))

(define-public crate-climb-1.0.22 (c (n "climb") (v "1.0.22") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1lllv696clvfd4w1idr7x87cv9wdl4b6335brs0kh72s1bcrcsm1")))

(define-public crate-climb-2.0.0 (c (n "climb") (v "2.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0wbh5h21cipd4n6fanx3z8zk5is09a0mp50ids1ggvd5pss52rdy")))

(define-public crate-climb-2.0.1 (c (n "climb") (v "2.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0dbqy9gmggwmxa7m3p9k5sq9ysca2vpf2wl096n9fvh2k44hn64k")))

(define-public crate-climb-2.0.2 (c (n "climb") (v "2.0.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0gf278vvdsis9w91q5szqdrh7y2i8rc8wvw7kzjspcxgylpfg8r4")))

(define-public crate-climb-2.0.3 (c (n "climb") (v "2.0.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1npiqzb7lqlrfcz7dak5dcgmm6g79k65p7kvm1chmrhvqaq7y3wr")))

(define-public crate-climb-2.0.4 (c (n "climb") (v "2.0.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0khkxq125sk1d62rrwrfwhn3w7l3nwj36lv1qzp36wn0bmnf2gnb")))

(define-public crate-climb-2.0.5 (c (n "climb") (v "2.0.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1jl5rqa6yzz2g7zsg9h40dsh7yc6if6pmkmkqha29wda0rymig64")))

