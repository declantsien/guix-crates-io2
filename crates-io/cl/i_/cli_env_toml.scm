(define-module (crates-io cl i_ cli_env_toml) #:use-module (crates-io))

(define-public crate-cli_env_toml-0.0.1 (c (n "cli_env_toml") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "06p5fbmpbxpwrw5x5m1l1ifdd5gb9c9557k6fpxcdndjj9mq31yd")))

(define-public crate-cli_env_toml-0.0.3 (c (n "cli_env_toml") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0ixb3bb6cmzi7ah9mrnzfi8cl6xniidhv56r7c98hadigwm75j2f")))

(define-public crate-cli_env_toml-0.0.5 (c (n "cli_env_toml") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0p8vyds11n3939sxpfnwifbb8gysdmxwcxj9w0bh16c4ic0c33sb")))

