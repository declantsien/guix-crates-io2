(define-module (crates-io cl i_ cli_chess) #:use-module (crates-io))

(define-public crate-cli_chess-0.1.0 (c (n "cli_chess") (v "0.1.0") (d (list (d (n "console") (r "^0.11.3") (d #t) (k 0)) (d (n "insta") (r "^0.16.1") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)))) (h "095c87fx7ad25c4h7nx6aizpaa5j28z3bbb7i98pm3s2c3z76dry")))

(define-public crate-cli_chess-0.1.1 (c (n "cli_chess") (v "0.1.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "016b8375lih7vg5jlfw49mz36gqvpd03m6nv5yndmarnnqxl3823")))

(define-public crate-cli_chess-0.2.0 (c (n "cli_chess") (v "0.2.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "13iid4pjr7hjzln3cndkwmjyl85pa3x3l271n44iabl08497g7d7")))

