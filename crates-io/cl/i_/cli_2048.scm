(define-module (crates-io cl i_ cli_2048) #:use-module (crates-io))

(define-public crate-cli_2048-0.2.0 (c (n "cli_2048") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "01xmpf9wz77al40mm4a71k9y5jdk98m3zmhq1a5w8i652f1123kj")))

