(define-module (crates-io cl i_ cli_helper) #:use-module (crates-io))

(define-public crate-cli_helper-0.1.0 (c (n "cli_helper") (v "0.1.0") (h "1vixrizsrg7kh5v4xbdszg24c8jdn02wjx67n0ivfhd7ycrys8dv")))

(define-public crate-cli_helper-0.1.1 (c (n "cli_helper") (v "0.1.1") (h "13p3sn9w8lkg30lqzabnaj12ln4xn2iflq33k96lhkmfgckc19k3") (f (quote (("file") ("default" "cli" "file") ("cli"))))))

(define-public crate-cli_helper-0.1.2 (c (n "cli_helper") (v "0.1.2") (h "0gpmcgjlp0dds2si861l0zg6lcxigzds89ilhgsv2k2y23jagxmh") (f (quote (("file") ("default" "cli") ("cli"))))))

(define-public crate-cli_helper-0.1.3 (c (n "cli_helper") (v "0.1.3") (h "19q33335w4adwb3wbgamffzd639828ja4isaxjds498j9zwnb44v") (f (quote (("file") ("default" "cli") ("cli"))))))

(define-public crate-cli_helper-0.1.4 (c (n "cli_helper") (v "0.1.4") (h "0ssx6iisbvd2fxmbv7rv4m14wrcy4vb694v357xmdi4sq90jwxpg") (f (quote (("file") ("default" "cli") ("cli"))))))

(define-public crate-cli_helper-0.1.5 (c (n "cli_helper") (v "0.1.5") (h "0iwazmzw27imhv490aqpvjz4fn87x1547y900mpw2wh3r887adnl") (f (quote (("file"))))))

(define-public crate-cli_helper-0.1.6 (c (n "cli_helper") (v "0.1.6") (h "192c84mnklz1sp0vxvglxw8xq1j5z921xayrqgrk3lldwl8w599w") (f (quote (("file"))))))

(define-public crate-cli_helper-0.1.7 (c (n "cli_helper") (v "0.1.7") (h "1zxkjjglq76i7215kl76fwdhilaa1rbr2v0ib39y65jcxpjyl34i") (f (quote (("file"))))))

