(define-module (crates-io cl i_ cli_todo_list) #:use-module (crates-io))

(define-public crate-cli_todo_list-0.1.0 (c (n "cli_todo_list") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 2)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1.4") (d #t) (k 2)))) (h "0mfqnq7biajg05q5ll93grkm9swx1z4dj92xkb72x1dz4fkc9pjx")))

