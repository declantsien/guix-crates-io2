(define-module (crates-io cl i_ cli_color_log) #:use-module (crates-io))

(define-public crate-cli_color_log-0.1.0 (c (n "cli_color_log") (v "0.1.0") (d (list (d (n "console") (r "^0.9.2") (d #t) (k 0)))) (h "1j7ypivyd3xiiw0dkkbfqqxdyhmhwh3h5pbyz95ncd0c91xcqvsx")))

(define-public crate-cli_color_log-0.1.1 (c (n "cli_color_log") (v "0.1.1") (d (list (d (n "console") (r "^0.9.2") (d #t) (k 0)))) (h "1p0s3i6jgrnkfrw4mskg08784bwih1rdk6v1g3n40h0rkas6wnqp")))

