(define-module (crates-io cl i_ cli_input) #:use-module (crates-io))

(define-public crate-cli_input-0.1.0 (c (n "cli_input") (v "0.1.0") (h "042bjx6s17ny5bi69mcbxz8qmcazsx0kclm9215q09a1n4c1q0qc")))

(define-public crate-cli_input-0.1.1 (c (n "cli_input") (v "0.1.1") (h "168y2lgcijcpd13mvihl0h183ww858h2h2pbihvbvma4qgabdzlj")))

(define-public crate-cli_input-0.1.2 (c (n "cli_input") (v "0.1.2") (h "007qkf12ydszg0vr21afsi9dhl15n4wnx23zddnldkski4ixw4cj")))

(define-public crate-cli_input-1.0.0 (c (n "cli_input") (v "1.0.0") (d (list (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)))) (h "1hhjnrldr3zn59l175m4lb6g5f2n0rzc2nsv1x7ik6n8z7n5q0p9")))

(define-public crate-cli_input-1.0.1 (c (n "cli_input") (v "1.0.1") (d (list (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)))) (h "18cszhx01qdfsck1lr93yadg9qrcj8plza0kj4z1cmg3n48kipjv")))

(define-public crate-cli_input-1.0.2 (c (n "cli_input") (v "1.0.2") (d (list (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)))) (h "0pilghazx3mjczs9zr7vqnqz9srj9xlfpl05g03nv26iga7jxkw2")))

(define-public crate-cli_input-1.0.3 (c (n "cli_input") (v "1.0.3") (d (list (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)))) (h "0ww6sfcr26yld62xdjca8ybglkgx2bk2axz70ppjrm0igy14fkc0")))

(define-public crate-cli_input-1.0.4 (c (n "cli_input") (v "1.0.4") (d (list (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)))) (h "06m74bxw4vghna0aq4c1fm3ya1w5qfm45zf918qjdcdafs3xpxl0")))

