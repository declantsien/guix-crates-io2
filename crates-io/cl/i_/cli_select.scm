(define-module (crates-io cl i_ cli_select) #:use-module (crates-io))

(define-public crate-cli_select-0.1.0 (c (n "cli_select") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "09xdfvsdsbvvi5v4pqy6c6v5a0zja9k4bcppqc01yiwf9a9w783y")))

(define-public crate-cli_select-0.1.1 (c (n "cli_select") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0qpdhf2hk9vs0snmvpff38p7mqadinyl96ffywndy1mbp0sjzjdz")))

(define-public crate-cli_select-0.1.2 (c (n "cli_select") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0956rc7cmd09c98lzinl0g190rcb8jb7cfln3bh9iljpy936myrs")))

(define-public crate-cli_select-0.1.3 (c (n "cli_select") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "07db8lzax95agyri1mqvrhfb84q0xfda905gqh3ksymcgck1l5wn")))

(define-public crate-cli_select-0.1.4 (c (n "cli_select") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1rl0bpv3xmj23gajffay89yyvilqxbji0zh2i6h4hsmibmhnnfmi")))

(define-public crate-cli_select-0.1.5 (c (n "cli_select") (v "0.1.5") (d (list (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1yp81ixpbww8rmd2r8zdgq0q7i8gkfl0727fy2yrhkx3yn1vzbyw") (f (quote (("color" "colored"))))))

(define-public crate-cli_select-0.1.6 (c (n "cli_select") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1a26ws5zcvkrin1c6vfsr9qnsigssx3c6pkvqr24jrqg3s5f9baa") (f (quote (("color" "colored"))))))

