(define-module (crates-io cl i_ cli_pipeviewer) #:use-module (crates-io))

(define-public crate-cli_pipeviewer-0.1.0 (c (n "cli_pipeviewer") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.2") (d #t) (k 0)))) (h "1df0nn5ng5kb2hmxjy31sw0g4qk2yrcamf0mb0j8fvw703v16b24")))

