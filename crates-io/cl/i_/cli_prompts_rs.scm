(define-module (crates-io cl i_ cli_prompts_rs) #:use-module (crates-io))

(define-public crate-cli_prompts_rs-0.1.0 (c (n "cli_prompts_rs") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "supports-unicode") (r "^2.0.0") (d #t) (k 0)))) (h "0irazsrkxbkwhllcyivshid8f04yqiri7vdk0gzny5sxhy2f61rd") (f (quote (("mock-term"))))))

(define-public crate-cli_prompts_rs-0.1.1 (c (n "cli_prompts_rs") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "supports-unicode") (r "^2.0.0") (d #t) (k 0)))) (h "1lxm8im8aiiwhflz27yqi808sy4l0ml649irbvj7i3iddwkjkgvw") (f (quote (("mock-term"))))))

(define-public crate-cli_prompts_rs-0.2.0 (c (n "cli_prompts_rs") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "supports-unicode") (r "^2.0.0") (d #t) (k 0)))) (h "064h4pl0473wjq2pfy9yap6garplgkd114p97a3jaxla7qm7idkx") (f (quote (("mock-term"))))))

(define-public crate-cli_prompts_rs-0.3.0 (c (n "cli_prompts_rs") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "supports-unicode") (r "^2.0.0") (d #t) (k 0)))) (h "1d7ivzsixmqr0gy9k5na2n27hi9akm4mpp9vd7ca47a62bjxssyq") (f (quote (("unstable") ("mock-term") ("docs" "unstable"))))))

