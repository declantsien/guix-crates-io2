(define-module (crates-io cl i_ cli_mega) #:use-module (crates-io))

(define-public crate-cli_mega-0.1.0 (c (n "cli_mega") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0i5y5pknbxf8an1c6kwm5qn33wl0h8w7jwkd2mwpd9k335fvfapn")))

(define-public crate-cli_mega-0.1.1 (c (n "cli_mega") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1w857irfpwvvyzdgp8s71fj60dbrbbm1szwxaw615lh3i34fy1dk")))

(define-public crate-cli_mega-0.1.2 (c (n "cli_mega") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1l5wrs36959psf5d7j2aa9mg62jfpkknzbk13g0jwgnhmag6wgm1")))

(define-public crate-cli_mega-0.1.3 (c (n "cli_mega") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1r03nycpi0y5jz52y9n3jm9a78vxnxkx6j28i7m9lpkylbx1naa9")))

(define-public crate-cli_mega-0.1.4 (c (n "cli_mega") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "12wi5fpw71lkjjnwk60a4vcz6diykysi6zjrrmmbnjpsir0ypj3y")))

(define-public crate-cli_mega-0.1.5 (c (n "cli_mega") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0kmm8iv4l927hghpqizpb1yly6i8iwrnsqdkwijb8ni5xxf4l252")))

(define-public crate-cli_mega-0.1.6 (c (n "cli_mega") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1k6flxsqws0q8vfa9j2rbslb3iinjjvmxabwpsp2wwcn7lc71s06")))

