(define-module (crates-io cl i_ cli_timestamps_generator) #:use-module (crates-io))

(define-public crate-cli_timestamps_generator-0.1.0 (c (n "cli_timestamps_generator") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "16i1h8xisajr2bljp62yydw7vd7yvrakdi00l4hm6nc57m7m8w6i")))

(define-public crate-cli_timestamps_generator-1.0.0 (c (n "cli_timestamps_generator") (v "1.0.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "16r8v0zi9ip86cdd6whdslhh5jbb1p0wynr47jkisjwwaz4vc6cw")))

(define-public crate-cli_timestamps_generator-1.0.1 (c (n "cli_timestamps_generator") (v "1.0.1") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1yv6vzd9lz8slgjp284k5ad6ia4gyiii4qdfraqazxqfa0zwzcli")))

