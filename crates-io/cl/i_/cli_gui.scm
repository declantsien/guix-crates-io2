(define-module (crates-io cl i_ cli_gui) #:use-module (crates-io))

(define-public crate-cli_gui-0.1.0 (c (n "cli_gui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "0rggx2fawdyn62w0yc4d9gnayhd3chqlhc22k5hj910l6zz39hlf")))

(define-public crate-cli_gui-0.1.1 (c (n "cli_gui") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "03fsjcphkya76a88z6w6nwz9w8mnnl0m45qbykvf0h50hi241g6l")))

