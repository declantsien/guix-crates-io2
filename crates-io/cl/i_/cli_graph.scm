(define-module (crates-io cl i_ cli_graph) #:use-module (crates-io))

(define-public crate-cli_graph-0.1.0 (c (n "cli_graph") (v "0.1.0") (h "19rp8r7ss3bd60bmpj0l98211jznq5ccsg0hbhdvz7m4p6gjqvvb")))

(define-public crate-cli_graph-0.2.0 (c (n "cli_graph") (v "0.2.0") (h "06zy12namnnwg8r013afl8fm1w6v0h8ybwwbd5j1i70r2y6snl5c") (y #t)))

(define-public crate-cli_graph-0.3.0 (c (n "cli_graph") (v "0.3.0") (h "1lsyzgf5fjk92r7ddl38qidxqbqmpadwbl7kpfr19fzfqn2lvija")))

