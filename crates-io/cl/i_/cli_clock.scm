(define-module (crates-io cl i_ cli_clock) #:use-module (crates-io))

(define-public crate-cli_clock-0.0.1 (c (n "cli_clock") (v "0.0.1") (h "0a5mqigy4fddjjca0kgka08mlmp51x6dwvxbnvl98xyk3k1dpys4")))

(define-public crate-cli_clock-0.1.0 (c (n "cli_clock") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0j7zdavanacmy49i8cg88w8lz1325nrgbjhn402kwn70d8r32rpz")))

