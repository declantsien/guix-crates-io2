(define-module (crates-io cl i_ cli_kit) #:use-module (crates-io))

(define-public crate-cli_kit-0.1.0 (c (n "cli_kit") (v "0.1.0") (h "1fj712bpa7g1p368nqq1g6l0n6n3jb1x1729mayh6sng7f8lmqsp")))

(define-public crate-cli_kit-0.1.1 (c (n "cli_kit") (v "0.1.1") (h "0y85r0iq9hr0hkaifh2mw02357x0bj6k03hk0a5zid23swz7qfya")))

(define-public crate-cli_kit-0.1.11 (c (n "cli_kit") (v "0.1.11") (h "1258mc8p36fhfpyy35yv41i7v6sjf1rnjgys9rfdhn6f94rnsnz7")))

