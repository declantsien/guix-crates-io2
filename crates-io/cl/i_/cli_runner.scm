(define-module (crates-io cl i_ cli_runner) #:use-module (crates-io))

(define-public crate-cli_runner-0.1.0 (c (n "cli_runner") (v "0.1.0") (d (list (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "100wys39p6mc15cvwxkacrqlhs3b6a9ybmqffsdf1098463mnna3")))

(define-public crate-cli_runner-0.2.0 (c (n "cli_runner") (v "0.2.0") (d (list (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0qifiahdwx741kyrha4dd5yq6n65s0nhy5slxjdy9mx40lwrk4r4")))

(define-public crate-cli_runner-0.2.1 (c (n "cli_runner") (v "0.2.1") (d (list (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "05j38lww42fc11aq6bhx8bw0n43i4k6hf8wan5af2lll7l2maq7c")))

