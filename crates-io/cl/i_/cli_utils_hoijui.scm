(define-module (crates-io cl i_ cli_utils_hoijui) #:use-module (crates-io))

(define-public crate-cli_utils_hoijui-0.6.0 (c (n "cli_utils_hoijui") (v "0.6.0") (d (list (d (n "git-version") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "046n046gk04b25fgpf6k0mryww3n2jalb77q6320z50lfh1pm0nr")))

(define-public crate-cli_utils_hoijui-0.6.1 (c (n "cli_utils_hoijui") (v "0.6.1") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "0jw65kdmw7i1cav1j3gk965zrq8nyrvqc4rr6pp3as67iqp0s2pv")))

(define-public crate-cli_utils_hoijui-0.6.2 (c (n "cli_utils_hoijui") (v "0.6.2") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "1grvqzhjjdy30diqfkgiqb20ddhjz15ilyanxbqxwsk9b8khnsb2")))

(define-public crate-cli_utils_hoijui-0.7.0 (c (n "cli_utils_hoijui") (v "0.7.0") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "17pasqa51mii5cbkzjing2ys81cicjx4xcpa59477xqy4nkma0hc")))

