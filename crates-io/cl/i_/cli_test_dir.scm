(define-module (crates-io cl i_ cli_test_dir) #:use-module (crates-io))

(define-public crate-cli_test_dir-0.1.0 (c (n "cli_test_dir") (v "0.1.0") (h "0ma43gbf5gjj0n4zsy446329n9ayxr7pwa0a94h5ismwkdshwp7w")))

(define-public crate-cli_test_dir-0.1.1 (c (n "cli_test_dir") (v "0.1.1") (h "0b8v30xzpvmmg4d9mbk1l6sqd0yfa2lxg5yya1brhamcfzf5lqgs")))

(define-public crate-cli_test_dir-0.1.2 (c (n "cli_test_dir") (v "0.1.2") (h "1nr9ahq6nwc25nf10yg75x43vgr98z84hdfnbjjmbxhqcclpy03x")))

(define-public crate-cli_test_dir-0.1.3 (c (n "cli_test_dir") (v "0.1.3") (h "1i92vqqxl5rk5vnq2qcbdhiwzr1b5gl51crpqpp4l7m0jmfi46v4")))

(define-public crate-cli_test_dir-0.1.4 (c (n "cli_test_dir") (v "0.1.4") (h "0h0wlzc5rgwk1556n0l62p34a6jm4azj6pk6vzkjzcgllb5j1yyq")))

(define-public crate-cli_test_dir-0.1.5 (c (n "cli_test_dir") (v "0.1.5") (h "0x5gck092qj96fhw1yriq8d9p8f5zrv0g3ig28hz4n8agx10pabl")))

(define-public crate-cli_test_dir-0.1.6 (c (n "cli_test_dir") (v "0.1.6") (h "15ppskxb0qgb3b6jg2yrkh2cfaf2bphbjw94fv2cggwsdc2bacl3")))

(define-public crate-cli_test_dir-0.1.7 (c (n "cli_test_dir") (v "0.1.7") (h "06fmd96s0fb0nya0h45mmmm769jd5q4bdpv7nzsd8f4mllw37iib")))

(define-public crate-cli_test_dir-0.1.8 (c (n "cli_test_dir") (v "0.1.8") (h "0fjbvgg1bzb1pymbj4i2yarcr4h169mpgxd2jvnl98cvkhl8lznc")))

