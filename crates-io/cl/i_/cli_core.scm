(define-module (crates-io cl i_ cli_core) #:use-module (crates-io))

(define-public crate-cli_core-0.1.0 (c (n "cli_core") (v "0.1.0") (d (list (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0d5b10qx8hcwq9n3c2cd94a5cc9vlkcmmyvspa98ql0kxm7jmkp1")))

