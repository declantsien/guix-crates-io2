(define-module (crates-io cl i_ cli_api_generator) #:use-module (crates-io))

(define-public crate-cli_api_generator-0.1.0 (c (n "cli_api_generator") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bl09pid4hh8h5g99v3xjiy9m5wfzarxjw0280lyrjl1aqxafz90")))

