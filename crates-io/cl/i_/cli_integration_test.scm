(define-module (crates-io cl i_ cli_integration_test) #:use-module (crates-io))

(define-public crate-cli_integration_test-0.1.0 (c (n "cli_integration_test") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0q9klnpdfilrjr7jsp3ifnmw4f43ccm57k859ksh5yqf5xh942r3")))

(define-public crate-cli_integration_test-0.1.1 (c (n "cli_integration_test") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1lqmlbls4yzyn082v213qpw58c5jx2rz529z8d6jn0p1q4dns9jh")))

(define-public crate-cli_integration_test-0.1.2 (c (n "cli_integration_test") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1npnmvzvhdmiw3s79nxjnlm29wqnvn1lyh0g2kdrgcsx852l4457")))

(define-public crate-cli_integration_test-0.2.0 (c (n "cli_integration_test") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1kx3gjf783l220r3k2zrfm24ylcaw5f2ckhbn2w4aqxdni38pds1")))

(define-public crate-cli_integration_test-0.2.1 (c (n "cli_integration_test") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1x9lk18x1k8ysxfx1wd8f1cq4514qv8r3flxnrjiw4srkb4bjkfz")))

(define-public crate-cli_integration_test-0.2.2 (c (n "cli_integration_test") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0lhaa7bznix3czngs8zn5qi9kn6c5h7f6yazli7vfd5kxvlnkmpg")))

(define-public crate-cli_integration_test-0.2.3 (c (n "cli_integration_test") (v "0.2.3") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0gjrc8s75cc4xj2hrgfc8fynai6kn2g928656ffp6m7clz4xccjs")))

