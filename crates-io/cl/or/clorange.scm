(define-module (crates-io cl or clorange) #:use-module (crates-io))

(define-public crate-clorange-0.1.0 (c (n "clorange") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "13dasaj519923ihz49dnf694g9pjf33b4x03zmkaqgrm12wvz5pk")))

(define-public crate-clorange-1.0.3 (c (n "clorange") (v "1.0.3") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1bkqqy59fi2ii00sp29kxc5fbmmihby5j6kcargc8gq160qhd0hw")))

(define-public crate-clorange-1.1.0 (c (n "clorange") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)))) (h "01ac4inwawyhzqngx5fx9blz7lcmjzvh3jb27qx12sgg87vkjzlp")))

