(define-module (crates-io cl ie client-handle-derive) #:use-module (crates-io))

(define-public crate-client-handle-derive-0.1.0 (c (n "client-handle-derive") (v "0.1.0") (d (list (d (n "client-handle-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)))) (h "1bw8zavs690j38ci400rlgrybays05aif5bc0a7g2n2db3cz3sx3")))

(define-public crate-client-handle-derive-0.2.0 (c (n "client-handle-derive") (v "0.2.0") (d (list (d (n "client-handle-core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)))) (h "1z2laxyxzjcna7xk2ghjk83n2klqd1cajnpaqra7xssyc6hv3flz")))

