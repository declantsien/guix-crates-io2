(define-module (crates-io cl ie clier_parser) #:use-module (crates-io))

(define-public crate-clier_parser-0.6.1 (c (n "clier_parser") (v "0.6.1") (h "158075869n0xz0j6767351qp2q8mizgs5jwwd819agy7ybki7ih7")))

(define-public crate-clier_parser-0.6.2 (c (n "clier_parser") (v "0.6.2") (h "0awzvncb8k14by2406b7b4sqm184cf8nia62jkfj2myi0dq3vilv")))

(define-public crate-clier_parser-0.7.0 (c (n "clier_parser") (v "0.7.0") (h "1jhqbxxwjh312ljp5zljs6534xj6igwc5mf1yyrl72zcb28rl6n0")))

(define-public crate-clier_parser-0.7.1 (c (n "clier_parser") (v "0.7.1") (h "18gk0wi9hyc9whm4li9biaax6askz4prr34j942rk932hd233npr")))

(define-public crate-clier_parser-0.7.2 (c (n "clier_parser") (v "0.7.2") (h "1l4k14i3av2ahhrp52allzrr4wxi689mqivg8jyjvnsjblmn42yx")))

(define-public crate-clier_parser-0.7.3 (c (n "clier_parser") (v "0.7.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1l74rrwsvybjr53bs3i6a6a01qx8nrzjwjdpw8zw23jzvjhf4ny8")))

(define-public crate-clier_parser-0.7.4 (c (n "clier_parser") (v "0.7.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0i5vx2yjj0grlvwbqn19v63irl3ac0615cf96hr6jc8cakcf982d")))

