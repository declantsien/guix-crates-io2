(define-module (crates-io cl ie client-handle) #:use-module (crates-io))

(define-public crate-client-handle-0.1.0 (c (n "client-handle") (v "0.1.0") (d (list (d (n "client-handle-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.73") (d #t) (k 2)))) (h "0vxq4kzvn75mkrzpk6mj4dz3241zs8761krj427549p0lvlygr38")))

(define-public crate-client-handle-0.2.0 (c (n "client-handle") (v "0.2.0") (d (list (d (n "client-handle-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.73") (d #t) (k 2)))) (h "1v4n044zbk1lybng84g2mv09zp43z10m8jhnws3zjjj02k022q6r") (s 2) (e (quote (("tokio" "dep:tokio"))))))

