(define-module (crates-io cl ie clier) #:use-module (crates-io))

(define-public crate-clier-0.0.1 (c (n "clier") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1dcyy2fz90y962hvc67gwanbdzf28ri81svnry367lrva2zbz8n3") (f (quote (("hooks")))) (y #t)))

(define-public crate-clier-0.0.2 (c (n "clier") (v "0.0.2") (h "0mdlrxk2wd6bljgadaq3b0kgd6z4phk89a2kgxq2v0z3lkajkhlh") (f (quote (("hooks") ("default" "hooks")))) (y #t)))

(define-public crate-clier-0.0.3 (c (n "clier") (v "0.0.3") (h "1zmvnvr15jc8izm2f92ghfkrysb9ajg5q9l514dl6bln2yvd4xms") (f (quote (("hooks") ("default" "hooks")))) (y #t)))

(define-public crate-clier-0.0.4 (c (n "clier") (v "0.0.4") (h "1n3qzc4v22bxm6pzia96k6r7alz8f0jyas45d51kshpsqbz15sb4") (f (quote (("hooks") ("default" "hooks")))) (y #t)))

(define-public crate-clier-0.0.5 (c (n "clier") (v "0.0.5") (h "06b8ybszq39hdjnmh890mb4bqfyk5wbv8r2isy7mcmrjlzdh823w") (f (quote (("hooks") ("default" "hooks")))) (y #t)))

(define-public crate-clier-0.1.0 (c (n "clier") (v "0.1.0") (h "1l6w01irbn38pl0qkq81vknq06n8jv8k44acjbyppp7436d6bgnh") (f (quote (("hooks") ("default" "hooks")))) (y #t)))

(define-public crate-clier-0.2.0 (c (n "clier") (v "0.2.0") (h "01rb37bahbsly4mha50b5rmhq5m4ha9gmzjyj89w4iwmfwj0npzr") (f (quote (("hooks") ("help" "hooks") ("default" "cli" "help") ("cli")))) (y #t)))

(define-public crate-clier-0.2.1 (c (n "clier") (v "0.2.1") (h "154kl5gf4kkr7j7myh92hp77bvzwzsq3vdp18ggkrncb19misna5") (y #t)))

(define-public crate-clier-0.2.2 (c (n "clier") (v "0.2.2") (h "05i47wi18nabbwms6vhh00pw4nz849jv96r012p35vknh1pdikvb") (y #t)))

(define-public crate-clier-0.3.0 (c (n "clier") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0i3wr7g71bqw9cyw28sh55wlhygqiwki8nbh0q2qlpxd10scr7sa") (y #t)))

(define-public crate-clier-0.3.1 (c (n "clier") (v "0.3.1") (d (list (d (n "cargo-watch") (r "^8.4.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1h06iq72v530vhv7n0r8kvdjbbjwx5piw4f7mgzpiddkrhk7171a") (y #t)))

(define-public crate-clier-0.3.2 (c (n "clier") (v "0.3.2") (d (list (d (n "cargo-watch") (r "^8.4.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "14vk180g0svdc7csfbbxp936x08zj78lhgnlicarf5gryvz4k8iv") (y #t)))

(define-public crate-clier-0.3.3 (c (n "clier") (v "0.3.3") (d (list (d (n "cargo-watch") (r "^8.4.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1k8i02lmwcb93i6s229pyx8g4vra2p7as16ipsll26xiw1ay1d1l") (y #t)))

(define-public crate-clier-0.4.0 (c (n "clier") (v "0.4.0") (d (list (d (n "cargo-watch") (r "^8.4.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1ba13srd0scrd62v3j4qjwqkryhljcn6v1jxzfa4mz2xjmb0ifa2") (y #t)))

(define-public crate-clier-0.4.1 (c (n "clier") (v "0.4.1") (d (list (d (n "cargo-watch") (r "^8.4.1") (d #t) (k 2)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "13vdhrs675vq0qpsfd93k1i5jxsdhlfagx98j5igi55kd0gsq75m") (y #t)))

(define-public crate-clier-0.4.3 (c (n "clier") (v "0.4.3") (d (list (d (n "cargo-rdme") (r "^1.4.2") (d #t) (k 2)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0jh617b3szrh1wrfwq29a1qxyr09i79a20zp1qgh8w3c1bv774g8") (y #t)))

(define-public crate-clier-0.5.0 (c (n "clier") (v "0.5.0") (d (list (d (n "cargo-rdme") (r "^1.4.2") (d #t) (k 2)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0ngz3hf9fa3c6pshzmvrqry6isg9d01zabrb7z2bjxiwpmakgrgp") (f (quote (("macros")))) (y #t)))

(define-public crate-clier-0.6.0 (c (n "clier") (v "0.6.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1zqwqwy2z7dd36740y6m3ifv52cvfsr4qi33p6059ckfvpmcpphp") (f (quote (("macros"))))))

(define-public crate-clier-0.6.1 (c (n "clier") (v "0.6.1") (d (list (d (n "clier_parser") (r "^0.6.1") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1nhqq0wx8md7l8m2f8fpqmlm8ccw4kb69vgi2mz0d5554c8407g7") (f (quote (("macros"))))))

(define-public crate-clier-0.6.2 (c (n "clier") (v "0.6.2") (d (list (d (n "clier_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1js9rgal96hcdcxclqa0yjhs8fcv54gq2kh3c6sjqmzq2sbvzym9") (f (quote (("macros"))))))

(define-public crate-clier-0.6.3 (c (n "clier") (v "0.6.3") (d (list (d (n "clier_parser") (r "^0.7.2") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0wa8q0qgmfz9dh8ly3ajvxvb4rsyfx9d0y18p2hmyydhgaxy4iv0") (f (quote (("macros"))))))

(define-public crate-clier-0.6.4 (c (n "clier") (v "0.6.4") (d (list (d (n "clier_parser") (r "^0.7.2") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1959w7437993my6i37ip4k4hcplgyizij5waq56hpw8fcal7rkm9") (f (quote (("macros"))))))

(define-public crate-clier-0.7.0 (c (n "clier") (v "0.7.0") (d (list (d (n "clier_parser") (r "^0.7.3") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0wi1v2kw7cv231q6m9kwpvs2q8n07qjrdz2xv1zjsbbyykyjzcx0") (f (quote (("macros"))))))

(define-public crate-clier-0.7.1 (c (n "clier") (v "0.7.1") (d (list (d (n "clier_parser") (r "^0.7.3") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0f03xidkvx5xznzfkz7vgrg134kjgpvxvgy7n0z0pdafmlbb4nkv") (f (quote (("macros"))))))

(define-public crate-clier-0.7.2 (c (n "clier") (v "0.7.2") (d (list (d (n "clier_parser") (r "^0.7.3") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "179dy0i2gwr9bpj78gv9y2bswdpmm2bg7sclpcrgg9kfdqn1zyp3") (f (quote (("macros"))))))

(define-public crate-clier-0.7.3 (c (n "clier") (v "0.7.3") (d (list (d (n "clier_parser") (r "^0.7.3") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1af0ybncw89hsh5s3dk65cizszljg5q6sl7wfwym3aj3dh19lhay") (f (quote (("macros"))))))

(define-public crate-clier-0.7.4 (c (n "clier") (v "0.7.4") (d (list (d (n "clier_parser") (r "^0.7.3") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "08ji3s3wva91dh3gjni6qk62g024ij758cw3rkj7dnk8ypfkkb0w") (f (quote (("macros"))))))

