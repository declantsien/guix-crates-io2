(define-module (crates-io cl ie clierr) #:use-module (crates-io))

(define-public crate-clierr-0.1.0 (c (n "clierr") (v "0.1.0") (h "152nflkn6xq829rpfjaxmcyygcqx8vrm43l0jnh7w7c6w9wsddiw")))

(define-public crate-clierr-0.2.0 (c (n "clierr") (v "0.2.0") (h "0wfyc9a00x5d8zp7rp2zfl8x6p6fffpxnjwrf7k5bz0zil2xrv6y")))

(define-public crate-clierr-0.3.0 (c (n "clierr") (v "0.3.0") (h "0vpqcji9ayyj80k7ynshkhn085zrr3riaphhasx9w7m13ibqs9kb")))

(define-public crate-clierr-0.4.0 (c (n "clierr") (v "0.4.0") (h "1dmd122vp3hsays8d6dc3lj6hvzy4dnar8cjij67i9k4cvg8dj6b")))

(define-public crate-clierr-0.5.0 (c (n "clierr") (v "0.5.0") (h "12m495db3ix83ikxsnras99mrpi6jfi7366flvlmb8rb8krsbhrl")))

(define-public crate-clierr-0.6.0 (c (n "clierr") (v "0.6.0") (h "06ph8gsn5264cmg2z2f97h70pnly3riw0xgdq1656wjksxgirp94")))

(define-public crate-clierr-0.7.0 (c (n "clierr") (v "0.7.0") (h "08k24gqd67ljvm8iyfsj4rxrkv9xhvhb81vih28l6cw63apb15m6")))

