(define-module (crates-io cl ie client-handle-core) #:use-module (crates-io))

(define-public crate-client-handle-core-0.1.0 (c (n "client-handle-core") (v "0.1.0") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "13dx19lmj9casdw0z916wx8l6dip50ca8792x2gbp0vlsmwqgczx")))

(define-public crate-client-handle-core-0.2.0 (c (n "client-handle-core") (v "0.2.0") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0yd4v0cdlbp543jp73g2v7fd5khi170zpfw40bhis7xf4cj5hvn6")))

(define-public crate-client-handle-core-0.3.0 (c (n "client-handle-core") (v "0.3.0") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0bmajzf3h1arii9qslmri1z4x0wa1j75chfvsqcjpsh8l3a3fnpb")))

