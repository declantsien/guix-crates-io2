(define-module (crates-io cl ie clientrust) #:use-module (crates-io))

(define-public crate-clientrust-0.1.0 (c (n "clientrust") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vyawgpjxclckl55q0niciqaisfilca3r7gygm8vvcqx4gszwfzg") (y #t) (r "1.78")))

(define-public crate-clientrust-0.1.1 (c (n "clientrust") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mjmx3cfccjiqg1qr03y582nq1mmaw5qpqslq8q5f924hcm8nzkb") (y #t) (r "1.78")))

(define-public crate-clientrust-0.1.2 (c (n "clientrust") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w21arvcmhhwqgxf7lbwfv00f9qpkqz244iizqlv1mqr9maprsjl") (y #t) (r "1.78")))

(define-public crate-clientrust-0.1.3 (c (n "clientrust") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nj97b5hq0p9fsd8w0z3fppl1087p5bkm579rfprbyn0m44lz0ys") (y #t) (r "1.78")))

