(define-module (crates-io cl ie clier_cli) #:use-module (crates-io))

(define-public crate-clier_cli-0.6.2 (c (n "clier_cli") (v "0.6.2") (d (list (d (n "clier") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "143nqswhi2b7sh4w7akd3ji8m9xd83lf3c67bysbxlm52cc770wk") (y #t)))

