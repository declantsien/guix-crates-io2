(define-module (crates-io cl og clog-cli) #:use-module (crates-io))

(define-public crate-clog-cli-0.9.1 (c (n "clog-cli") (v "0.9.1") (d (list (d (n "ansi_term") (r "~0.6.3") (o #t) (d #t) (k 0)) (d (n "clap") (r "~1.3.0") (d #t) (k 0)) (d (n "clog") (r "~0.9.0") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.21") (o #t) (d #t) (k 0)) (d (n "semver") (r "~0.1.20") (d #t) (k 0)) (d (n "time") (r "~0.1.32") (d #t) (k 0)))) (h "05akwkyilxfkx4v9qa050gjsvxl57vilzbwnyhkbpxp1bcasp3s8") (f (quote (("unstable") ("default" "color") ("debug") ("color" "ansi_term"))))))

(define-public crate-clog-cli-0.9.2 (c (n "clog-cli") (v "0.9.2") (d (list (d (n "ansi_term") (r "~0.7.2") (o #t) (d #t) (k 0)) (d (n "clap") (r "~2.2") (d #t) (k 0)) (d (n "clog") (r "~0.9.1") (d #t) (k 0)) (d (n "semver") (r "~0.2.3") (d #t) (k 0)) (d (n "time") (r "~0.1.32") (d #t) (k 0)))) (h "1nj10rnms9m8jsq3sqsjh601mnrkaj2i191fhgq3va5l85dybc5j") (f (quote (("unstable") ("default" "color") ("debug") ("color" "ansi_term"))))))

(define-public crate-clog-cli-0.9.3 (c (n "clog-cli") (v "0.9.3") (d (list (d (n "ansi_term") (r "~0.9.0") (o #t) (d #t) (k 0)) (d (n "clap") (r "~2.21.1") (d #t) (k 0)) (d (n "clog") (r "~0.9.1") (d #t) (k 0)) (d (n "semver") (r "~0.6.0") (d #t) (k 0)) (d (n "time") (r "~0.1.32") (d #t) (k 0)))) (h "0g8n2rylfglf0pzg6a04d74viiislxw40kgnf4pq8qdsyjjyylyw") (f (quote (("unstable") ("default" "color") ("debug") ("color" "ansi_term"))))))

