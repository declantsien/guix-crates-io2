(define-module (crates-io cl og clogi) #:use-module (crates-io))

(define-public crate-clogi-0.0.1 (c (n "clogi") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sqlite") (r "^0.26.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jv021f5f3bfilcmpy4f3hcm4zj26dw513c6mwri45mpx7sd20nw")))

(define-public crate-clogi-0.0.2 (c (n "clogi") (v "0.0.2") (d (list (d (n "config") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1sbw2ysd4scnp5m86yc97gm6awkmmfpmjvk46wxhd3wznfb69kzi")))

