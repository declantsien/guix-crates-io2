(define-module (crates-io cl og clogger) #:use-module (crates-io))

(define-public crate-clogger-0.1.0 (c (n "clogger") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fap1mx6qn594cmy9xxr7k6csbpfj30i6yhry86ilvhxk0iv0ilf")))

