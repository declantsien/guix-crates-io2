(define-module (crates-io cl og clog) #:use-module (crates-io))

(define-public crate-clog-0.3.0 (c (n "clog") (v "0.3.0") (d (list (d (n "clap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1lrilfk35mx58bkrivjh6zccbra618myg3gp1g8icwmlp5zslcyz")))

(define-public crate-clog-0.3.1 (c (n "clog") (v "0.3.1") (d (list (d (n "clap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0998n6a1bmhczff39vhqzhdh7lz7harw667q0yhlvzjkdspi5bwq")))

(define-public crate-clog-0.3.2 (c (n "clog") (v "0.3.2") (d (list (d (n "clap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1ql5hd3f4ghkd8x3nkb5wd27pnv90bpw00p603dl3sw8db7q301k")))

(define-public crate-clog-0.4.0 (c (n "clog") (v "0.4.0") (d (list (d (n "clap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "semver") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "toml") (r "*") (d #t) (k 0)))) (h "1ng3qr70za1zz49xfdz2wgz4p0cfwm8r92bszlghlyhgrrwpayb4")))

(define-public crate-clog-0.6.0 (c (n "clog") (v "0.6.0") (d (list (d (n "clap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (o #t) (d #t) (k 0)) (d (n "semver") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "toml") (r "*") (d #t) (k 0)))) (h "1fb5mvr3x1a2nijw4klvhg3g86ry0dnvz2n0qxkrrqg6srah4vhs") (f (quote (("unstable" "regex_macros") ("default"))))))

(define-public crate-clog-0.8.1 (c (n "clog") (v "0.8.1") (d (list (d (n "clap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (o #t) (d #t) (k 0)) (d (n "semver") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "toml") (r "*") (d #t) (k 0)))) (h "0l7ac3mj9gnyh3v7w3zky1cpd1lqvhm0d1f5647fykijzwb3dggx") (f (quote (("unstable") ("default") ("debug"))))))

(define-public crate-clog-0.8.2 (c (n "clog") (v "0.8.2") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (o #t) (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "toml") (r "*") (d #t) (k 0)))) (h "1hdp2iv3sby8wj6ayr13k7kyfaiq1mq2x5b436ayqygh5zj5zq9h") (f (quote (("unstable") ("default") ("debug"))))))

(define-public crate-clog-0.9.0 (c (n "clog") (v "0.9.0") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "regex_macros") (r "*") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)) (d (n "toml") (r "^0.1.23") (d #t) (k 0)))) (h "1dnbyp1kqn1bwz17adp8f3p019vra0455q3vvaz89p4kxm3yxya6") (f (quote (("unstable") ("default") ("debug"))))))

(define-public crate-clog-0.9.1 (c (n "clog") (v "0.9.1") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)) (d (n "toml") (r "^0.1.23") (d #t) (k 0)))) (h "04jzfp7hmxhz732ws8plrqck0nqy222j8my94lgnspq1d2qrwq4i") (f (quote (("unstable") ("default") ("debug"))))))

