(define-module (crates-io cl uu cluuname) #:use-module (crates-io))

(define-public crate-cluuname-0.1.0 (c (n "cluuname") (v "0.1.0") (d (list (d (n "clucstr") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "1agq3sr1k5yxi1bccngk6dpx1cq555svjvn7hadrj4q7j0n5d0h3") (f (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1.1 (c (n "cluuname") (v "0.1.1") (d (list (d (n "clucstr") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "00sacwrliv3pawc4w946422i9czjh68zjczvvhhh6fnfa8rsamb6") (f (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1.2 (c (n "cluuname") (v "0.1.2") (d (list (d (n "clucstr") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "095f7xnj4kgbc8waay0k3p34gfahdq39gvsxyd8ky1v6jlqp8s9j") (f (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1.4 (c (n "cluuname") (v "0.1.4") (d (list (d (n "clucstr") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "0mq2vw74ziddmc6aiix7w16jzzb3zpnibvv6p7q8caw6vm0zf69g") (f (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1.5 (c (n "cluuname") (v "0.1.5") (d (list (d (n "clucstr") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "0n5x2g7pzjggswx03qpbr4yihsc14hc5dmmhyjkbsz7w5ypq63ma") (f (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1.6 (c (n "cluuname") (v "0.1.6") (d (list (d (n "clucstr") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "1a2bkj5cmh8iqiv361f4pr9d2pbv7a73cwild9pjinvnl7xj0qf3") (f (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1.7 (c (n "cluuname") (v "0.1.7") (d (list (d (n "clucstr") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "0wvmzpidcf9nnr81gcbbrvp2i0ilkmr7ia07qddqqnrhkjdx1nmd") (f (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1.8 (c (n "cluuname") (v "0.1.8") (d (list (d (n "clucstr") (r "^0.1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "0fgkvsxv6x38ri4yci5dw0bk4b5qkzdzfdf35w2kyn8yapcn0bj7") (f (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1.9 (c (n "cluuname") (v "0.1.9") (d (list (d (n "clucstr") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "0q8l3skqn6szz14nl9irnfljjlnai15d52ggvhaql78zsrnnpiwv") (f (quote (("enable_domainname"))))))

