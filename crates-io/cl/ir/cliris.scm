(define-module (crates-io cl ir cliris) #:use-module (crates-io))

(define-public crate-cliris-0.1.0 (c (n "cliris") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1ncczb4gljr8kxr68j8z1h98axsk1jxr8irpqixm1gmfhyqnpl8y")))

(define-public crate-cliris-0.2.0 (c (n "cliris") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "iris-lib") (r "^0.1.0") (f (quote ("image"))) (d #t) (k 0)))) (h "1azkrbkffjn3ql2256viif3frj97c9v4zyv6vil9hvka46g2lw73")))

