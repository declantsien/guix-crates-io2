(define-module (crates-io cl ep clepsydra) #:use-module (crates-io))

(define-public crate-clepsydra-0.1.0 (c (n "clepsydra") (v "0.1.0") (h "0sly7wzlia428s14a5f6imknxkr6n5m03qyc2m3pvjpmn3j5hx8a")))

(define-public crate-clepsydra-0.2.0 (c (n "clepsydra") (v "0.2.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "concorde") (r "^0.7.0") (d #t) (k 0)) (d (n "duplexify") (r "^1.2.2") (d #t) (k 2)) (d (n "edelcrantz") (r "^0.6.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.8") (d #t) (k 0)) (d (n "pergola") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sluice") (r "^0.5.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "tracing") (r "^0.1.22") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.4") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.15") (f (quote ("chrono" "env-filter" "fmt"))) (d #t) (k 2)) (d (n "tracing-tracy") (r "^0.4.0") (f (quote ("enable"))) (o #t) (d #t) (k 0)))) (h "01sv3fk1rfdwj9qf5k58smnnkrx1rc6rwjjg81v0hy254bi79xbf") (f (quote (("tracy" "tracing-tracy") ("default"))))))

