(define-module (crates-io cl ey cleye) #:use-module (crates-io))

(define-public crate-cleye-0.1.0 (c (n "cleye") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "loggerv") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1aihfjg7z0p7181nj8gaxffrif7c65qaa36a0aan4nyaa1yd2zcd")))

