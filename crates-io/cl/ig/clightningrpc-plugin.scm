(define-module (crates-io cl ig clightningrpc-plugin) #:use-module (crates-io))

(define-public crate-clightningrpc-plugin-0.1.0-beta.1 (c (n "clightningrpc-plugin") (v "0.1.0-beta.1") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.2") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0007wxxvjq09w1hd6q4f8gd3x580xr5hp2j4pb6bvx0cbdfgslg2")))

(define-public crate-clightningrpc-plugin-0.3.0-beta.2 (c (n "clightningrpc-plugin") (v "0.3.0-beta.2") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.2") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15cx0g9k3g3k222yp93isfzd7s27m2i5ybzxqcazwy6pvcwcqmd3")))

(define-public crate-clightningrpc-plugin-0.3.0-beta.3 (c (n "clightningrpc-plugin") (v "0.3.0-beta.3") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.2") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00v2i8aprdm3pd2ynnnfr6713z6b324xddcjfax1ckszibhhnbgd")))

(define-public crate-clightningrpc-plugin-0.3.0-beta.4 (c (n "clightningrpc-plugin") (v "0.3.0-beta.4") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.2") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yiq4bqsvzmz4c5bxbn9n59sy105xki5l9id70z9kck0f81dl3a2")))

(define-public crate-clightningrpc-plugin-0.3.0-beta.5 (c (n "clightningrpc-plugin") (v "0.3.0-beta.5") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.2") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vrib7zrh956khalq9qkiyjalxfhzkiq9bi10i65dhk09w253m07")))

(define-public crate-clightningrpc-plugin-0.3.0-beta.6 (c (n "clightningrpc-plugin") (v "0.3.0-beta.6") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.3") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hw5g6vhjqmdixr04q9cwd9xw96i4xi5r9pf7qpg6w7fmbm2kyz8")))

(define-public crate-clightningrpc-plugin-0.3.0-beta.7 (c (n "clightningrpc-plugin") (v "0.3.0-beta.7") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.2") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hl90zrmc285jhiiqf95m5kl2naa1vrx59mbsl9wyhfj4x25qg35")))

(define-public crate-clightningrpc-plugin-0.3.0-beta.8 (c (n "clightningrpc-plugin") (v "0.3.0-beta.8") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gbiwaizlyb6f8hskqxhqza99yfcpg55bwz9x2gzrbazi4gxs1wq") (s 2) (e (quote (("log" "dep:log"))))))

