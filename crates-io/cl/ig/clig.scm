(define-module (crates-io cl ig clig) #:use-module (crates-io))

(define-public crate-Clig-0.1.0 (c (n "Clig") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0z1ywmfmziza1xgxr9hjkl1rghjr3si4viajiv1sv3gg23wlb4ar") (y #t)))

(define-public crate-Clig-0.1.1 (c (n "Clig") (v "0.1.1") (h "1gm391l6qixla3zfn94vm4xj9ky3jpqyw08pgidckdrdnslmmw3b")))

