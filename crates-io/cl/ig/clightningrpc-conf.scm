(define-module (crates-io cl ig clightningrpc-conf) #:use-module (crates-io))

(define-public crate-clightningrpc-conf-0.0.1-alpha.1 (c (n "clightningrpc-conf") (v "0.0.1-alpha.1") (d (list (d (n "albert_stream") (r "^0.0.3") (d #t) (k 0)))) (h "0pmjvcj3f8zxxc2rian7r4j1a4mjs4nb43qk7a68fl68z3xfqd0v")))

(define-public crate-clightningrpc-conf-0.0.1-alpha.2 (c (n "clightningrpc-conf") (v "0.0.1-alpha.2") (d (list (d (n "albert_stream") (r "^0.0.3") (d #t) (k 0)))) (h "0vcmqd4xyjfagm2glg4867if137dhhaa5afb74z7ndvbskg2q0qr")))

(define-public crate-clightningrpc-conf-0.0.1-alpha.3 (c (n "clightningrpc-conf") (v "0.0.1-alpha.3") (d (list (d (n "albert_stream") (r "^0.0.3") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)))) (h "19yqm24nj5ghfkcf2kgqhmdxay72ppv7hgqqpg70sdbvn7pqafpm")))

(define-public crate-clightningrpc-conf-0.0.1-alpha.4 (c (n "clightningrpc-conf") (v "0.0.1-alpha.4") (d (list (d (n "albert_stream") (r "^0.0.3") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)))) (h "1s9vzr5x43fhkp2wjs0spk14izjjhpp5zhfx82r6x7fpd1b4kzzr")))

(define-public crate-clightningrpc-conf-0.0.1-alpha.5 (c (n "clightningrpc-conf") (v "0.0.1-alpha.5") (d (list (d (n "albert_stream") (r "^0.0.3") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)))) (h "13lrsi65yyyhmq7hcmfahvgq7gy0pym62vdmljhc4h0ayvvvnabj")))

(define-public crate-clightningrpc-conf-0.0.1-alpha.6 (c (n "clightningrpc-conf") (v "0.0.1-alpha.6") (d (list (d (n "albert_stream") (r "^0.0.4") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)))) (h "0m3in9ij5v1m02f8dj029hwl24szh302h6a5zb477mcrm9qjp7lg")))

(define-public crate-clightningrpc-conf-0.0.1-alpha.7 (c (n "clightningrpc-conf") (v "0.0.1-alpha.7") (d (list (d (n "albert_stream") (r "^0.0.4") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)))) (h "1530jm70rlpbq3yh0gb59fhyyl08bslzg18w3y8ffia1gpknip75")))

(define-public crate-clightningrpc-conf-0.0.1-alpha.8 (c (n "clightningrpc-conf") (v "0.0.1-alpha.8") (d (list (d (n "albert_stream") (r "^0.0.4") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)))) (h "191f6ry5xzl6x8d9pqwifzhv1q3frz9fgw1gc0js95gzjifh4vg6")))

(define-public crate-clightningrpc-conf-0.0.1 (c (n "clightningrpc-conf") (v "0.0.1") (d (list (d (n "albert_stream") (r "^0.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "0pdxrl2i1skwfq2532arr9r8ijga5maqqxxb7hvx3fi1nlwjkbh5")))

(define-public crate-clightningrpc-conf-0.0.2 (c (n "clightningrpc-conf") (v "0.0.2") (d (list (d (n "albert_stream") (r "^0.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "0h8rf4dgqdvbb6xnbawn7w7mgbx73gb9shyl00hml7281nni0pdz")))

(define-public crate-clightningrpc-conf-0.0.3 (c (n "clightningrpc-conf") (v "0.0.3") (d (list (d (n "albert_stream") (r "^0.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "0cgzfginjsc1bskr66db2b9l635mqlq0i5jrrx28a6kviqhmcywj")))

