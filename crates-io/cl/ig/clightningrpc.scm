(define-module (crates-io cl ig clightningrpc) #:use-module (crates-io))

(define-public crate-clightningrpc-0.1.0 (c (n "clightningrpc") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "strason") (r "^0.4") (d #t) (k 0)))) (h "1y1jlgj8bg0nfgapyi3lsxqdwnq3ndzn3asx39xbaaxqfa3if11r")))

(define-public crate-clightningrpc-0.2.0 (c (n "clightningrpc") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x792gz5mlbpn98c2p70nmrch17ifbw57vvp3nr0qnpv3qymiblq")))

(define-public crate-clightningrpc-0.3.0-beta.1 (c (n "clightningrpc") (v "0.3.0-beta.1") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.1") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hn810ckp0linkmp0a62bkn6idc7x65x8akv2dwkqvsfy6y6ww9c")))

(define-public crate-clightningrpc-0.3.0-beta.2 (c (n "clightningrpc") (v "0.3.0-beta.2") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.2") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yjx9r1jk0y8vh0zw0w8nhw80758w6gvlmq3cvndi5y2rzmc1jsv")))

(define-public crate-clightningrpc-0.3.0-beta.3 (c (n "clightningrpc") (v "0.3.0-beta.3") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.2") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1c1gc6ibyld75jsk3vix5lymppf7dki75hkfgzx1fyr4ihh2dd7g")))

(define-public crate-clightningrpc-0.3.0-beta.4 (c (n "clightningrpc") (v "0.3.0-beta.4") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.3") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1sh81lwlvpf6lskjch2ypqyv17pypyqqlwgvpvzcy00azkv8q49k")))

(define-public crate-clightningrpc-0.3.0-beta.5 (c (n "clightningrpc") (v "0.3.0-beta.5") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.3") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "09ggnsk31rm7260x0w484lh8dki5x9hnmfb2nv7sibr3kykl60pn")))

(define-public crate-clightningrpc-0.3.0-beta.6 (c (n "clightningrpc") (v "0.3.0-beta.6") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.4") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1dcis4pbi44yy4nc4607j6bwd497h8ynkini342j8xphkyd57yvq")))

(define-public crate-clightningrpc-0.3.0-beta.7 (c (n "clightningrpc") (v "0.3.0-beta.7") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.4") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0gisaqrilpw9vkprqbz9bikwa2mfqa7xsiqyk54xrkf225ihr6a0")))

(define-public crate-clightningrpc-0.3.0-beta.8 (c (n "clightningrpc") (v "0.3.0-beta.8") (d (list (d (n "clightningrpc-common") (r "^0.3.0-beta.4") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "17v3jayjhzzvpzqc4f564ql9yycv6rywfh18ya0916ga73v9r9ks")))

