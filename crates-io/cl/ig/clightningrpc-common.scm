(define-module (crates-io cl ig clightningrpc-common) #:use-module (crates-io))

(define-public crate-clightningrpc-common-0.3.0-beta.1 (c (n "clightningrpc-common") (v "0.3.0-beta.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1px8hrxh9ymv9x7d3vm6pfi3nsxmpfz1vn0f1iqmccd3lf8qqj9g")))

(define-public crate-clightningrpc-common-0.3.0-beta.2 (c (n "clightningrpc-common") (v "0.3.0-beta.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12n26xfmpbs01pjbp83i17av5rgqsgfb7p5k4d4a2jhfnp7gl6qf")))

(define-public crate-clightningrpc-common-0.3.0-beta.3 (c (n "clightningrpc-common") (v "0.3.0-beta.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dwbw87zfbbqlvv59jx1fkmz6k38z1qnisigja7ywplbvcfq5n84")))

(define-public crate-clightningrpc-common-0.3.0-beta.4 (c (n "clightningrpc-common") (v "0.3.0-beta.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nng4rqc19mkqscn625frk7wg8hr6gbqz35l0n3krgzp3w6h7i3d")))

