(define-module (crates-io cl ac clacks_crypto) #:use-module (crates-io))

(define-public crate-clacks_crypto-0.0.3 (c (n "clacks_crypto") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "clacks_mtproto") (r "^0.0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0bys5izjxshjxcjl9zh74hmwvhgy8rzrikl0276601y7xys5ymm3")))

(define-public crate-clacks_crypto-0.0.4 (c (n "clacks_crypto") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "clacks_mtproto") (r "^0.0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0awlg4nlzbv4q6k9nalq93n70fr93m6v9f5s5akihi7xxm9sxh36")))

