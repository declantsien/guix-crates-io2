(define-module (crates-io cl ac clacks) #:use-module (crates-io))

(define-public crate-clacks-0.0.1 (c (n "clacks") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "clacks_tl_codegen") (r "^0.0.1") (d #t) (k 1)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "extfmt") (r "^0.1") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "1ibf8ch9xjgswnlmj537d19phidijgc47wna9wa1gv628dl1qjgb")))

(define-public crate-clacks-0.0.2 (c (n "clacks") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "clacks_tl_codegen") (r "^0.0.2") (d #t) (k 1)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "extfmt") (r "^0.1") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10") (o #t) (d #t) (k 1)))) (h "1mihhl2cgkqv8hlgck43969mdxi2w8ml02dbbjzqnd1dyxh0q5s6") (f (quote (("rustfmt-codegen" "rustfmt") ("default"))))))

