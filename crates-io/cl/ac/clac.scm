(define-module (crates-io cl ac clac) #:use-module (crates-io))

(define-public crate-clac-1.0.0 (c (n "clac") (v "1.0.0") (d (list (d (n "linked-list") (r "^0.0.3") (d #t) (k 0)))) (h "07id14yys0yxljqyni0rvz1x4ah1yinix0ifam3s789x489f55kv")))

(define-public crate-clac-1.1.0 (c (n "clac") (v "1.1.0") (d (list (d (n "linked-list") (r "^0.0.3") (d #t) (k 0)))) (h "1gjn996r1z69vk9xlrjgpwbcgz4qc5japwx7k9l3qzbah4hcfba8")))

