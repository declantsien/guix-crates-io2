(define-module (crates-io cl ac clacks_tl_codegen) #:use-module (crates-io))

(define-public crate-clacks_tl_codegen-0.0.1 (c (n "clacks_tl_codegen") (v "0.0.1") (d (list (d (n "pom") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0hqdibfgqfx9izn620swcmd82jyk9c6jn2yvzbyqqkwwb66nbxn5")))

(define-public crate-clacks_tl_codegen-0.0.2 (c (n "clacks_tl_codegen") (v "0.0.2") (d (list (d (n "pom") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0zz1n4nx6fhs8bjp5x364j1b5z3v92m758rsbqmnh833y38dh1jb")))

(define-public crate-clacks_tl_codegen-0.0.3 (c (n "clacks_tl_codegen") (v "0.0.3") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "pom") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0kv74sxcpk74sac4ryayayl8w26ggrqn03mhi3g81qc2kdvwwmyc") (f (quote (("rustfmt-codegen" "rustfmt") ("default" "rustfmt-codegen"))))))

(define-public crate-clacks_tl_codegen-0.0.4 (c (n "clacks_tl_codegen") (v "0.0.4") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "pom") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "06fqyqx7v67zjapxv70ppb6hjqavx3174fxy352c3cbk8zp0vy38") (f (quote (("rustfmt-codegen" "rustfmt") ("default"))))))

(define-public crate-clacks_tl_codegen-0.0.5 (c (n "clacks_tl_codegen") (v "0.0.5") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "pom") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "078qrp06l6810p18ybkgfcr1j7zpr5n99yxrigh14r157ahrc1p9") (f (quote (("rustfmt-codegen" "rustfmt") ("default"))))))

