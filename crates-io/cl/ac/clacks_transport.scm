(define-module (crates-io cl ac clacks_transport) #:use-module (crates-io))

(define-public crate-clacks_transport-0.0.3 (c (n "clacks_transport") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clacks_crypto") (r "^0.0.3") (d #t) (k 0)) (d (n "clacks_mtproto") (r "^0.0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1r8pb9496xf714z5a1rnsg6si3al2d9snp96h6xmc1nkz1g1rivy")))

(define-public crate-clacks_transport-0.0.4 (c (n "clacks_transport") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clacks_crypto") (r "^0.0.4") (d #t) (k 0)) (d (n "clacks_mtproto") (r "^0.0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1kh7khzz2yv2pwvz2w58znx0yjs0fwy8y4y8yb1n8jyrj50p4sdd")))

