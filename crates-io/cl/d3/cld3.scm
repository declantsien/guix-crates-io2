(define-module (crates-io cl d3 cld3) #:use-module (crates-io))

(define-public crate-cld3-0.1.0 (c (n "cld3") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1rizj5mvjailb05g2w5d4yq1bsj31bhpnx6vwahxwdm87n7n3g8s") (l "cxx-cld3")))

(define-public crate-cld3-0.1.1 (c (n "cld3") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "184rj4da2f1r57gk4hdn75cph444087p8pqbhvj6q3yss3nyf1cx") (l "cxx-cld3")))

