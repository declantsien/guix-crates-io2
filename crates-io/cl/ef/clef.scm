(define-module (crates-io cl ef clef) #:use-module (crates-io))

(define-public crate-clef-0.0.0 (c (n "clef") (v "0.0.0") (h "1qyzvciknzl0mq4li2k16x0m1psniwqvlwvy4p0z9rmiyxs84qwi")))

(define-public crate-clef-0.0.1 (c (n "clef") (v "0.0.1") (h "0s9yvawmcld98n0dnzig8nslq2vhzh57g2badb60cyx8gdn6cfxa")))

(define-public crate-clef-0.0.2 (c (n "clef") (v "0.0.2") (d (list (d (n "contracts") (r "^0.6.0") (d #t) (k 0)))) (h "0fw7m8r902fbg13q4gxc47kc3nl0ihvv7g3738xkqg73a42vn8mg")))

