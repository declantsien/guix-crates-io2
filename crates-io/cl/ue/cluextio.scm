(define-module (crates-io cl ue cluextio) #:use-module (crates-io))

(define-public crate-cluExtIO-0.1.2 (c (n "cluExtIO") (v "0.1.2") (h "0hqsihnl77579a92xnizi0h1pgnc1a3iqw5qpl181dn5gghh9lvh")))

(define-public crate-cluExtIO-0.1.6 (c (n "cluExtIO") (v "0.1.6") (h "0ginqxqjm6bx9g0jkgi71fanh71lb40spr641bvhx0h7lp0qmjri")))

(define-public crate-cluExtIO-0.1.7 (c (n "cluExtIO") (v "0.1.7") (h "1m8z5lcdxi6m44991k7a0zv2mdf9sfh6c5vdlmd0daqc18y32gvi")))

