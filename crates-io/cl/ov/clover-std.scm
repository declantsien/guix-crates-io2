(define-module (crates-io cl ov clover-std) #:use-module (crates-io))

(define-public crate-clover-std-0.1.0 (c (n "clover-std") (v "0.1.0") (d (list (d (n "clover") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "13qyb35q872z61g4lgi95jla78ccxrnsw0bam6vlyjqhxspc81p5")))

(define-public crate-clover-std-0.1.1 (c (n "clover-std") (v "0.1.1") (d (list (d (n "clover") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "02zfvz3n1dddlz0wvvpvgdc216h78crrqi21dxwwrhgpsqij1v0w")))

(define-public crate-clover-std-0.1.2 (c (n "clover-std") (v "0.1.2") (d (list (d (n "clover") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0i4f0ggchgwkxg9jiyq805lq1vqc2hl246y8g29jjbf884m086cw")))

(define-public crate-clover-std-0.1.3 (c (n "clover-std") (v "0.1.3") (d (list (d (n "clover") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1sld4nkps7vk8j4cp36vi96333cz65fwj3my2ifzcmcksmkyrha9")))

