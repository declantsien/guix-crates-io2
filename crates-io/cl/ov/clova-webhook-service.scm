(define-module (crates-io cl ov clova-webhook-service) #:use-module (crates-io))

(define-public crate-clova-webhook-service-0.0.1 (c (n "clova-webhook-service") (v "0.0.1") (d (list (d (n "lambda_http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1") (d #t) (k 0)))) (h "0kv4kc4qlsks1np8ipz7gnr7rjwlmj52diskar9n4v5lgdl0iwpm") (y #t)))

