(define-module (crates-io cl ov clover) #:use-module (crates-io))

(define-public crate-clover-0.0.1 (c (n "clover") (v "0.0.1") (h "1vcsp20lfn7n53jlb830f0g5qalp4mnyrlhmb93292bw259lni9q")))

(define-public crate-clover-0.0.2 (c (n "clover") (v "0.0.2") (h "1g8mvdrj4ljzcwrifh2hx16xxs83mq4ndz5jip4jihr94nqpnmdy")))

(define-public crate-clover-0.1.0 (c (n "clover") (v "0.1.0") (h "1rl9k4sl4za78rqvkvay7bvagiyq2ca73qc69z3vl9612zxmvlyw")))

(define-public crate-clover-0.1.1 (c (n "clover") (v "0.1.1") (h "0hfi6301r1hjvh79j7y30zbkh10c1vr2zz2mp662xbch1gqzly8p")))

(define-public crate-clover-0.1.2 (c (n "clover") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0ykf1r51arark12j9284c0bgfk30xlxmspn26k8pk6zc2x6wh38k")))

(define-public crate-clover-0.1.3 (c (n "clover") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0nb5xmwlv8d81701fcfym5wahl7bwzick0hiins26xivz141p3kl")))

