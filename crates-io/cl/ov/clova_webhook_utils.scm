(define-module (crates-io cl ov clova_webhook_utils) #:use-module (crates-io))

(define-public crate-clova_webhook_utils-0.0.1 (c (n "clova_webhook_utils") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1") (d #t) (k 0)))) (h "0dq0nga4i2nzzg6cxwakx7pfsk7cfgxhbpgjf553fim6i8v9vds7")))

(define-public crate-clova_webhook_utils-0.0.2 (c (n "clova_webhook_utils") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1") (d #t) (k 0)))) (h "0yxnyzy885v2jvysrfz091cl0nfn74szlnnnfk1zpp6zxaw9g5zb")))

(define-public crate-clova_webhook_utils-0.0.3 (c (n "clova_webhook_utils") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1") (d #t) (k 0)))) (h "1bhizj4k5dnd403ajdwhlxz6asfhpb1lgz9gm204qpsxdgfs3p4d")))

(define-public crate-clova_webhook_utils-0.0.4 (c (n "clova_webhook_utils") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1") (d #t) (k 0)))) (h "0hfc3ca4zq7yv6msg8011xp19k1hfgp4ni0x7saf9yxkpipzr00w")))

