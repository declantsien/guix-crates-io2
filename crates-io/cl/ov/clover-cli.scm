(define-module (crates-io cl ov clover-cli) #:use-module (crates-io))

(define-public crate-clover-cli-0.1.0 (c (n "clover-cli") (v "0.1.0") (d (list (d (n "clover") (r "^0.1.0") (d #t) (k 0)) (d (n "clover-std") (r "^0.1.0") (d #t) (k 0)))) (h "1hhsv7j8fvyxxyny4zba7i17y0hic7hb3nvbvlx06mn6v7dvkhpy")))

(define-public crate-clover-cli-0.1.1 (c (n "clover-cli") (v "0.1.1") (d (list (d (n "clover") (r "^0.1.0") (d #t) (k 0)) (d (n "clover-std") (r "^0.1.0") (d #t) (k 0)))) (h "1v9jwxzjy5i9s9d3z4wy21yqlx24ayh3mlz68qrm0qkcnsj80a1g")))

(define-public crate-clover-cli-0.1.2 (c (n "clover-cli") (v "0.1.2") (d (list (d (n "clover") (r "^0.1.2") (d #t) (k 0)) (d (n "clover-std") (r "^0.1.2") (d #t) (k 0)))) (h "0d68ig2cz7ankc4sn1f6m4rzjdii4bjwrbkzgmrdiknpr4s9yild")))

(define-public crate-clover-cli-0.1.3 (c (n "clover-cli") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clover") (r "^0.1.3") (d #t) (k 0)) (d (n "clover-std") (r "^0.1.3") (d #t) (k 0)))) (h "1aj2q6zizh2iqz69bhp5xajn7k0q04ix207gf5hf1my8f90y6mbk")))

