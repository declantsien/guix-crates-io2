(define-module (crates-io ug li ugli-derive) #:use-module (crates-io))

(define-public crate-ugli-derive-0.1.0 (c (n "ugli-derive") (v "0.1.0") (d (list (d (n "batbox") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0f5b2xqwm668cfr2nka0v3xqpj9x1kf6lpx6ay84p4h7n2ih9xpq")))

(define-public crate-ugli-derive-0.2.0 (c (n "ugli-derive") (v "0.2.0") (d (list (d (n "batbox") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0m8ibdjb62hkisbmdf61a651fp4ya44gh6k3zrc8524iw60bs1pz")))

(define-public crate-ugli-derive-0.3.0 (c (n "ugli-derive") (v "0.3.0") (d (list (d (n "batbox") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mqj7qhm767ahm114g7hlrb788gd0imqpbbmc56cmwliwi48448a")))

(define-public crate-ugli-derive-0.4.0 (c (n "ugli-derive") (v "0.4.0") (d (list (d (n "batbox") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18wp8accjfc2n9brsl3pv1bzn6h8p88n9dvp4a7isqnax5l2lj1d")))

(define-public crate-ugli-derive-0.5.0-alpha.0 (c (n "ugli-derive") (v "0.5.0-alpha.0") (d (list (d (n "batbox") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11xy87pih3kgng9za1j8078sf94y3lxv32afd669yqq14lbpndcf")))

(define-public crate-ugli-derive-0.5.0 (c (n "ugli-derive") (v "0.5.0") (d (list (d (n "batbox") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mkxj653663zdq91lg3i5080a09h28mrh7nx3qf1br2y59m8fbfm")))

(define-public crate-ugli-derive-0.6.0-alpha.0 (c (n "ugli-derive") (v "0.6.0-alpha.0") (d (list (d (n "batbox") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08nps48b1y4is41aqcb49bnj4psfwiy46zyxbdiz4faf7vbkx7hl")))

(define-public crate-ugli-derive-0.6.0 (c (n "ugli-derive") (v "0.6.0") (d (list (d (n "batbox") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wbrh5p6l86s422hqqragzlvi37y1v43vv5d9him30dgrzbach6g")))

(define-public crate-ugli-derive-0.7.0-alpha.0 (c (n "ugli-derive") (v "0.7.0-alpha.0") (d (list (d (n "batbox") (r "^0.7.0-alpha") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v5s6g4vcxw3zskrb8xxc55b0nqibm0iw8mca1nvsdmnba193b17")))

(define-public crate-ugli-derive-0.8.0 (c (n "ugli-derive") (v "0.8.0") (d (list (d (n "batbox") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "166q3fjg8dm5x9x0nz1an2l3a4bym5zf0hskyk9kiszc0qjspg39")))

(define-public crate-ugli-derive-0.9.0 (c (n "ugli-derive") (v "0.9.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02vd0mqp921wf38w2gvk2755pcx7l8kf9jh9sdv4kszxrqd3rr4c")))

(define-public crate-ugli-derive-0.11.0 (c (n "ugli-derive") (v "0.11.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p35a1m0x221ynsncqxlph1zmmy7xyq6s324qa7z0ll8mvmcqy47")))

(define-public crate-ugli-derive-0.13.0 (c (n "ugli-derive") (v "0.13.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08ycbraih2d9prwib6v0jbghd2s758lwhhb9a8nkcwj38vcp7ir9")))

(define-public crate-ugli-derive-0.15.0 (c (n "ugli-derive") (v "0.15.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bgn8yr5hfjr4g7krjxgl8l248z7i6mfb94cz42iyj2bym444arp")))

(define-public crate-ugli-derive-0.16.0 (c (n "ugli-derive") (v "0.16.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0c3w4ib1i1l17hs5bhcmg0n6hw628pzq0q7pmmck3n9vdqcrm2l3")))

(define-public crate-ugli-derive-0.17.0 (c (n "ugli-derive") (v "0.17.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1lp861znjh6574nafiqi36n39gk7z20wr8cv7dgichwgkd85nlqq")))

(define-public crate-ugli-derive-0.17.1 (c (n "ugli-derive") (v "0.17.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1iqq53vd03alyypzhqwqbij98ss6dh9i3bhficalw045q1r0aiff")))

(define-public crate-ugli-derive-0.17.2 (c (n "ugli-derive") (v "0.17.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ivms48fbs2x74zdqi8p2xgppzc56v0jib1wbdsryv538fy7g8gp")))

(define-public crate-ugli-derive-0.18.0 (c (n "ugli-derive") (v "0.18.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0pavmi9sdfqr7k9y1ykd7za9mi8v4n0g866n4mjq5crplja9ldyi")))

