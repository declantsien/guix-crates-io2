(define-module (crates-io ug li ugli-gl) #:use-module (crates-io))

(define-public crate-ugli-gl-0.1.0 (c (n "ugli-gl") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "1is8vmi7l4njcjp3rxczkrd0rl60vd2j0sbh6crhjlvi1aj7l1xj")))

(define-public crate-ugli-gl-0.1.1 (c (n "ugli-gl") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "17qlw4n7yxpk0avnc9piin1g3h9547akp1za8nmgk046igq0icqq")))

