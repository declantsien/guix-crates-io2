(define-module (crates-io ug li ugli-webgl) #:use-module (crates-io))

(define-public crate-ugli-webgl-0.1.0 (c (n "ugli-webgl") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5") (d #t) (k 0)) (d (n "webgl_generator") (r "^0.2") (d #t) (k 1)))) (h "0xngda70yj5m8kyrsm49m88yf9nrr7p6g2310zr29mmyb4gi1g2m")))

