(define-module (crates-io ug ui uguid) #:use-module (crates-io))

(define-public crate-uguid-0.5.0 (c (n "uguid") (v "0.5.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1368s4x4y30fsi1bvdnh9dlsk043ds4zrkbzwcy7qkrakv6wsmj6") (f (quote (("std"))))))

(define-public crate-uguid-0.6.0 (c (n "uguid") (v "0.6.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.137") (d #t) (k 2)))) (h "1yzifdr9iidw7rqd51q4qisghsm7iw4hcgrm51w23940dq37p734") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-uguid-0.7.0 (c (n "uguid") (v "0.7.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.137") (d #t) (k 2)))) (h "0pbvn4pjch09l7yqp81lj771sl88b3l97vazcirs0lsnaa9abbm8") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck"))))))

(define-public crate-uguid-1.0.0 (c (n "uguid") (v "1.0.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.137") (d #t) (k 2)))) (h "1551mc6m14a7fsar65n9q1bb1gzp7lpp43lhhnwp28d964hy57f7") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck"))))))

(define-public crate-uguid-1.0.1 (c (n "uguid") (v "1.0.1") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.137") (d #t) (k 2)))) (h "0wamx0bpxyiy8sl9bl64jghazfd2mgm8jh9i0yp4w63qm2fx1lkr") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck"))))))

(define-public crate-uguid-1.0.2 (c (n "uguid") (v "1.0.2") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.137") (d #t) (k 2)))) (h "1in0qp4wzcs2pzrhi19wdbhr5li8wbj2bxkaabaswjga9ki39ry6") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck"))))))

(define-public crate-uguid-1.0.3 (c (n "uguid") (v "1.0.3") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.137") (d #t) (k 2)))) (h "15acg7dqqrm4pbwc6g357gpiwq4mnrb0dxxa5a7dn9iym95i530b") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck"))))))

(define-public crate-uguid-1.0.4 (c (n "uguid") (v "1.0.4") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)))) (h "1b0sasmgx3pfyavpwsms0mqzgmq536088nhqpvjwnhav8aqm755m") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck"))))))

(define-public crate-uguid-1.1.0 (c (n "uguid") (v "1.1.0") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)))) (h "07wipvm8k6s1krgpw0c1zkavcdqgz1g191z7zi1kkdsyz6bh3xq9") (f (quote (("std")))) (y #t) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck"))))))

(define-public crate-uguid-1.1.1 (c (n "uguid") (v "1.1.1") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)))) (h "1x966pghvvgdfsm2biy7xpksbq359g30iai32ysnajfxzs45gza1") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck"))))))

(define-public crate-uguid-1.2.0 (c (n "uguid") (v "1.2.0") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)))) (h "0hh9nss4vrllvracx2lbis46jvni89aaqkjjprkn7sdrj6g9zf3c") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck"))))))

(define-public crate-uguid-1.2.1 (c (n "uguid") (v "1.2.1") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)))) (h "0wlnih1l9kbvkz8s4jr342xjwm1fai539dhwa7k1kq6687z94191") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck"))))))

(define-public crate-uguid-2.0.0 (c (n "uguid") (v "2.0.0") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "108z3qcw13h5ikaiqjvfyqdydl052v7n6v6l4pb46ywa4rzchk2r") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck")))) (r "1.60")))

(define-public crate-uguid-2.0.1 (c (n "uguid") (v "2.0.1") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "13bzj4bysz0z1zzm1qhyhi2x1rnjjl39bs1h79xp5gggbljvvpqn") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck")))) (r "1.60")))

(define-public crate-uguid-2.1.0 (c (n "uguid") (v "2.1.0") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1rdfh9r6bgp8pl36qhnznaawr49dxg82ald9dbd62pvch3q1dx8y") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck")))) (r "1.68")))

(define-public crate-uguid-2.2.0 (c (n "uguid") (v "2.2.0") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0cqmj9z91iwky094avxf09cgmn8wpp6lxmg9bj3ffh6jc2bfl55b") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck")))) (r "1.68")))

