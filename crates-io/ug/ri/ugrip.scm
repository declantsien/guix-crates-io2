(define-module (crates-io ug ri ugrip) #:use-module (crates-io))

(define-public crate-ugrip-0.1.0 (c (n "ugrip") (v "0.1.0") (h "0xgp07iad7chxsmr3nga7qmk3fpmm42lczmcsfvifvgacn20gby4")))

(define-public crate-ugrip-0.1.1 (c (n "ugrip") (v "0.1.1") (h "09kxpq2v8660b1jah1xqpxvkwaq30kmvs4zv9cy3ygrpb26sapna")))

