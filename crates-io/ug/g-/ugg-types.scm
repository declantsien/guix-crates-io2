(define-module (crates-io ug g- ugg-types) #:use-module (crates-io))

(define-public crate-ugg-types-0.1.0 (c (n "ugg-types") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1w9qqgwn46c3n9ayk3icqxy10bi8i01q16n32pd4ai1lrq9lzcdn") (f (quote (("client"))))))

(define-public crate-ugg-types-0.1.1 (c (n "ugg-types") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0m11jvp8yc77l1fy43j0h3sdlqvfl2s29k3svvb7iy72hy7p5b8z") (f (quote (("client"))))))

(define-public crate-ugg-types-0.1.2 (c (n "ugg-types") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0z7nzfqwzyl7pbyd5r1pb59b2wjx4r92im7z3r7886hn8gmwigy6") (f (quote (("client"))))))

(define-public crate-ugg-types-0.2.0 (c (n "ugg-types") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wq7jsixjbwx7141plqx9y31kz2laibgmkqvd7dfsjd0jhm1sh7x") (f (quote (("client"))))))

(define-public crate-ugg-types-0.2.1 (c (n "ugg-types") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1npyjk9ghdbjv5kkq6bdjhhmvzicv22madgswphdk0pm1f8pgjxb") (f (quote (("client"))))))

(define-public crate-ugg-types-0.2.2 (c (n "ugg-types") (v "0.2.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "19fdmqpgyzdbqz12c3xi623s4v8a4dj7nk572g7fa4kziw7sfynb") (f (quote (("client"))))))

(define-public crate-ugg-types-0.2.3 (c (n "ugg-types") (v "0.2.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ny4a193k017ixqg7sxdfs3lddvfrszjdpgj4cr20jz769dky5b7") (f (quote (("client"))))))

(define-public crate-ugg-types-0.2.4 (c (n "ugg-types") (v "0.2.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ba3j0kmc7jg4brmxvxzlrsbf64cz4jma53js28azrjqyhrx3j35") (f (quote (("client"))))))

(define-public crate-ugg-types-0.3.0 (c (n "ugg-types") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1q8v5w2z0r6v9d9i3np07pzg7d24xhggb6zpwpqq59p8x78yjyqm") (f (quote (("client")))) (r "1.68.2")))

(define-public crate-ugg-types-0.4.0 (c (n "ugg-types") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cb3dhyb6zad66mibpsdk343yyr7r50f6ifkdvhn2h4niyv4i6s8") (r "1.74")))

(define-public crate-ugg-types-0.4.1 (c (n "ugg-types") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p2szm3xk8bziz5ia1yr64mjn92322l19xfd9v7ag3l5m2lxgvd7") (r "1.74")))

(define-public crate-ugg-types-0.5.0 (c (n "ugg-types") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k5qnvlr2l8bylq4gy03wja9fflnlngc652rhsz1dsfw8zwkf97m") (r "1.74")))

(define-public crate-ugg-types-0.5.1 (c (n "ugg-types") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bjyjrh2g969q747wn7jia7b3978r10894qrfcg9cdbvqvz48h55") (r "1.74")))

(define-public crate-ugg-types-0.5.2 (c (n "ugg-types") (v "0.5.2") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "01j953hkwlmqx7basp31cq2r1zpz50kgh4hrkz7wdjci3jlzvq8k") (r "1.74")))

