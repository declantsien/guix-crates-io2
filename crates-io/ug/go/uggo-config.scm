(define-module (crates-io ug go uggo-config) #:use-module (crates-io))

(define-public crate-uggo-config-0.1.0 (c (n "uggo-config") (v "0.1.0") (d (list (d (n "config-better") (r "^1.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1sc5dqcrsfwmw7llnw01kh00h574rx7zgdw0drx9g9zhlib6c10y") (r "1.74")))

(define-public crate-uggo-config-0.1.1 (c (n "uggo-config") (v "0.1.1") (d (list (d (n "config-better") (r "^1.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "10a5pp333gmbm8mgngb29p04rx1m1nx78p5086p4j50l95861s0z") (r "1.74")))

(define-public crate-uggo-config-0.1.2 (c (n "uggo-config") (v "0.1.2") (d (list (d (n "config-better") (r "^1.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0h7qa3njyxw4fnnl4ayngqsl0v77hi8qkg7flq443s12jvqz7099") (r "1.74")))

