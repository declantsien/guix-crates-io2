(define-module (crates-io ug _m ug_max) #:use-module (crates-io))

(define-public crate-ug_max-1.0.0 (c (n "ug_max") (v "1.0.0") (h "1jvrqr63bxaayy81nlwrcwwc28k1aavyyrvq8ap990zgxnjxyb39")))

(define-public crate-ug_max-1.0.1 (c (n "ug_max") (v "1.0.1") (h "0gcn1ikwvhrp9q9qm7qk1rkla3vkmi331jdid9if1h1z1yxz8krd")))

(define-public crate-ug_max-1.0.2 (c (n "ug_max") (v "1.0.2") (h "16z6cy02krm97g898ds3pm52xf99vbl62q6h4h60704xrf1pxb7x")))

(define-public crate-ug_max-1.0.3 (c (n "ug_max") (v "1.0.3") (h "1yngy4a1z0v1gf200fshydisxirpsd3i2rflpzhiqlrqanb89n4q")))

(define-public crate-ug_max-1.0.4 (c (n "ug_max") (v "1.0.4") (h "1rc4yn6w0pb9bll2fwapr5l18fzzsi5jalc087ymfg4a0kadng24")))

(define-public crate-ug_max-1.0.5 (c (n "ug_max") (v "1.0.5") (h "0dcnk7n7akys6ywcl54mdnk3jd2v2c96njs75ips6q7z1jchrh3w")))

(define-public crate-ug_max-1.0.6 (c (n "ug_max") (v "1.0.6") (h "15hw74nynw530bq2hj81l5p1wlm0qr7py3krhwfcqvq8pgfl2z6c")))

(define-public crate-ug_max-2.0.0 (c (n "ug_max") (v "2.0.0") (h "1mkn9ai1qzbn6km71nz7g3xbwp096s0v99m4ja6cyj2rgdjxwiwy")))

(define-public crate-ug_max-3.0.0 (c (n "ug_max") (v "3.0.0") (h "18mv9zfgwhkqnqpnb3np98p9vk50ya5ffflgxw3nypqqjn5vz6ax")))

(define-public crate-ug_max-3.1.0 (c (n "ug_max") (v "3.1.0") (h "0g5r0a3pysb7f4ah7m1i07m246x1520vx2rh2cl39rkw4dsd0ngy")))

(define-public crate-ug_max-4.0.0 (c (n "ug_max") (v "4.0.0") (h "1ljfc54lj58k9asf65l9q0b6faa4pxjymwqk723cw3ldxd9hs34s")))

(define-public crate-ug_max-4.0.1 (c (n "ug_max") (v "4.0.1") (h "1k6y61mpg4srz4220mfnahxpwrlxyfqxfm32kfpgy8fj107pgv3w")))

(define-public crate-ug_max-4.0.2 (c (n "ug_max") (v "4.0.2") (h "127vw06z859nb24xpjyn97zil0hd3awcwh5zcmizxy45yljxxz3b")))

(define-public crate-ug_max-4.0.3 (c (n "ug_max") (v "4.0.3") (h "030qnx031ks8dih43p0nh048f7mp3i9i9xbly73v0rfhj3xqxy7j")))

