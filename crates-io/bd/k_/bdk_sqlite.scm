(define-module (crates-io bd k_ bdk_sqlite) #:use-module (crates-io))

(define-public crate-bdk_sqlite-0.1.0 (c (n "bdk_sqlite") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bdk_chain") (r "^0.15.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bdk_persist") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rikzihqqkpavvrgl8iz2jvgs9mwrdf8asnbp7dm2lax5hxl2rdn")))

