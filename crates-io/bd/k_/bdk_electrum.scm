(define-module (crates-io bd k_ bdk_electrum) #:use-module (crates-io))

(define-public crate-bdk_electrum-0.1.0 (c (n "bdk_electrum") (v "0.1.0") (d (list (d (n "bdk_chain") (r "^0.2") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "electrum-client") (r "^0.12") (d #t) (k 0)))) (h "16kzldzf591iqa2l305xq19alw9kfcq8yi3d7g1g2ifp2zzx6z0w")))

(define-public crate-bdk_electrum-0.2.0 (c (n "bdk_electrum") (v "0.2.0") (d (list (d (n "bdk_chain") (r "^0.4.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "electrum-client") (r "^0.12") (d #t) (k 0)))) (h "1259r9qdi7b63wz67y99iz54g3gj17ma716fh3y3wxzvycshn3lv")))

(define-public crate-bdk_electrum-0.3.0 (c (n "bdk_electrum") (v "0.3.0") (d (list (d (n "bdk_chain") (r "^0.5.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "electrum-client") (r "^0.12") (d #t) (k 0)))) (h "1ikhxl28lv490519x6wz1vrd84rafhcr7zpwim7yzjzr5lf6snbm")))

(define-public crate-bdk_electrum-0.4.0 (c (n "bdk_electrum") (v "0.4.0") (d (list (d (n "bdk_chain") (r "^0.6.0") (k 0)) (d (n "electrum-client") (r "^0.18") (d #t) (k 0)))) (h "092n33vmkd3axzdxrmblvbn2x6rbk32lqar0zr13mg7ypchfxscv")))

(define-public crate-bdk_electrum-0.5.0 (c (n "bdk_electrum") (v "0.5.0") (d (list (d (n "bdk_chain") (r "^0.7.0") (k 0)) (d (n "electrum-client") (r "^0.18") (d #t) (k 0)))) (h "12q50803mjp0n94jd5j7ldha6q3p9vrjawxqjwcwlq26ccvxav8n")))

(define-public crate-bdk_electrum-0.6.0 (c (n "bdk_electrum") (v "0.6.0") (d (list (d (n "bdk_chain") (r "^0.8.0") (k 0)) (d (n "electrum-client") (r "^0.18") (d #t) (k 0)))) (h "0lklwgc054gj8xd1vbvqjvq96cr2y4vjms2lkmfkrr3m34ls5w1w")))

(define-public crate-bdk_electrum-0.7.0 (c (n "bdk_electrum") (v "0.7.0") (d (list (d (n "bdk_chain") (r "^0.9.0") (k 0)) (d (n "electrum-client") (r "^0.18") (d #t) (k 0)))) (h "0bg2xq2ps6va0rkqvnlp8lldy9hdvgyhpq8lzrnc62fclw932l86")))

(define-public crate-bdk_electrum-0.8.0 (c (n "bdk_electrum") (v "0.8.0") (d (list (d (n "bdk_chain") (r "^0.10.0") (k 0)) (d (n "electrum-client") (r "^0.18") (d #t) (k 0)))) (h "0m43nnkv4sp11dgi8lprrcqg2r248gi48fmk2wnfal2wszydjhr2")))

(define-public crate-bdk_electrum-0.9.0 (c (n "bdk_electrum") (v "0.9.0") (d (list (d (n "bdk_chain") (r "^0.11.0") (k 0)) (d (n "electrum-client") (r "^0.18") (d #t) (k 0)))) (h "1zfxkp5agrpqna0crkaqpsgwxfjakfqzd1dv4y85ryycwkw0a3gi")))

(define-public crate-bdk_electrum-0.10.0 (c (n "bdk_electrum") (v "0.10.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bdk_chain") (r "^0.11.0") (k 0)) (d (n "bdk_testenv") (r "^0.1.0") (k 2)) (d (n "electrsd") (r "^0.25.0") (f (quote ("bitcoind_25_0" "esplora_a33e97e1" "legacy"))) (d #t) (k 2)) (d (n "electrum-client") (r "^0.18") (d #t) (k 0)))) (h "0a8h94arwwkccwc6ypv7s82ib8fwgnb19yqp28snw05074ymyli3")))

(define-public crate-bdk_electrum-0.11.0 (c (n "bdk_electrum") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bdk_chain") (r "^0.12.0") (k 0)) (d (n "electrsd") (r "^0.27.1") (f (quote ("bitcoind_25_0" "esplora_a33e97e1" "legacy"))) (d #t) (k 2)) (d (n "electrum-client") (r "^0.19") (d #t) (k 0)))) (h "0a029ynd4hrnh0nnmr4rb3b9zv7jmsfvy1dj0g5rndv95gh3k8wg")))

(define-public crate-bdk_electrum-0.12.0 (c (n "bdk_electrum") (v "0.12.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bdk_chain") (r "^0.13.0") (k 0)) (d (n "electrsd") (r "^0.27.1") (f (quote ("bitcoind_25_0" "esplora_a33e97e1" "legacy"))) (d #t) (k 2)) (d (n "electrum-client") (r "^0.19") (d #t) (k 0)))) (h "0cg2mm37rs98d0gwdfk29c4ml217rsr4ksgwf8j62dsi18nzbgps")))

(define-public crate-bdk_electrum-0.13.0 (c (n "bdk_electrum") (v "0.13.0") (d (list (d (n "bdk_chain") (r "^0.14.0") (d #t) (k 0)) (d (n "electrum-client") (r "^0.19") (d #t) (k 0)))) (h "1zy4xxizzcgr9p9r6rb1gxc5nc3av70v1ymx91xa6l8n0fqg7fs4")))

(define-public crate-bdk_electrum-0.14.0 (c (n "bdk_electrum") (v "0.14.0") (d (list (d (n "bdk_chain") (r "^0.15.0") (d #t) (k 0)) (d (n "electrum-client") (r "^0.19") (d #t) (k 0)))) (h "12d5pps5ili5fjgsjl91qryb2vx2r030yrs5431ivxximrsn5418")))

