(define-module (crates-io bd k_ bdk_file_store) #:use-module (crates-io))

(define-public crate-bdk_file_store-0.0.1 (c (n "bdk_file_store") (v "0.0.1") (d (list (d (n "bdk_chain") (r "^0.3") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pwnva19jky44x03fynflbrmamrxp0rrbz4halz482baavpgnm7d")))

(define-public crate-bdk_file_store-0.1.0 (c (n "bdk_file_store") (v "0.1.0") (d (list (d (n "bdk_chain") (r "^0.4.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0jc4q2a2q2ivdqw2nv1cpd1g7jvdf2qhica1lsavgxksn7fm6msj")))

(define-public crate-bdk_file_store-0.2.0 (c (n "bdk_file_store") (v "0.2.0") (d (list (d (n "bdk_chain") (r "^0.5.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1m4kqn955ifrwakkn02ws450p2jkyi5phxqqym97lkp7bhh447ph")))

(define-public crate-bdk_file_store-0.3.0 (c (n "bdk_file_store") (v "0.3.0") (d (list (d (n "bdk_chain") (r "^0.7.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0zpf7896pvnqxapg3sp41h2wn5l1np5hadl98ggm4av4jx5bxarg")))

(define-public crate-bdk_file_store-0.4.0 (c (n "bdk_file_store") (v "0.4.0") (d (list (d (n "bdk_chain") (r "^0.8.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0dp2ck0qrzbz6bz4k0gg0y886lkj7bardpkqh8rp525f9iqkhqif")))

(define-public crate-bdk_file_store-0.5.0 (c (n "bdk_file_store") (v "0.5.0") (d (list (d (n "bdk_chain") (r "^0.9.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "08yaz68dplc3vb70125cy1hml4bsy74sny8mzmjqrh9fp4293pl8")))

(define-public crate-bdk_file_store-0.6.0 (c (n "bdk_file_store") (v "0.6.0") (d (list (d (n "bdk_chain") (r "^0.10.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1f83gdzk952yih6cbg3md2sm43c3bvgjad60slxgc0qf9zcqbszh")))

(define-public crate-bdk_file_store-0.7.0 (c (n "bdk_file_store") (v "0.7.0") (d (list (d (n "bdk_chain") (r "^0.11.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "161kmyr28n9z032qs2rqk1cxwrs4qvqsb84qsxvpk4xwccr7xyqf")))

(define-public crate-bdk_file_store-0.8.0 (c (n "bdk_file_store") (v "0.8.0") (d (list (d (n "bdk_chain") (r "^0.11.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1b033fr5cfxqsiwsndwi0z4ki7qxwla59kb6ahfmsn3icqdqlgj0")))

(define-public crate-bdk_file_store-0.9.0 (c (n "bdk_file_store") (v "0.9.0") (d (list (d (n "bdk_chain") (r "^0.12.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1pijzcnwg2g8x5vv5j1h4y77svxr9jyxihhb3qsqx2afddmq068c")))

(define-public crate-bdk_file_store-0.10.0 (c (n "bdk_file_store") (v "0.10.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bdk_chain") (r "^0.13.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bdk_persist") (r "^0.1.0") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0wxx93j8vcnjs021bswyyfsw6fpy6kx9kn7j2k73nxskx0g02aj9")))

(define-public crate-bdk_file_store-0.11.0 (c (n "bdk_file_store") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bdk_chain") (r "^0.14.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bdk_persist") (r "^0.2.0") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1av9dmvq9qn7cj6hbk2sh48k5pgb9z6hnsw3l573i3fvbsd7xzax")))

(define-public crate-bdk_file_store-0.12.0 (c (n "bdk_file_store") (v "0.12.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bdk_chain") (r "^0.15.0") (f (quote ("serde" "miniscript"))) (d #t) (k 0)) (d (n "bdk_persist") (r "^0.3.0") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1xbbl61c7vb5z70qcscakhqw55px4h332mdy1dffqwxnn2sjbd53")))

