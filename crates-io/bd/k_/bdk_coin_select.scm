(define-module (crates-io bd k_ bdk_coin_select) #:use-module (crates-io))

(define-public crate-bdk_coin_select-0.1.0 (c (n "bdk_coin_select") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.30") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0ss8ydgc5blw5si83xm7w26glfc008nlzmnk0yf2c2wlvjrqwsnm") (f (quote (("std") ("default" "std"))))))

(define-public crate-bdk_coin_select-0.1.1 (c (n "bdk_coin_select") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.30") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "05n3fnbgf8prj2qdzizzhs8l8blh32bg6ljx87q86pk5qdkh2cn0") (f (quote (("std") ("default" "std"))))))

(define-public crate-bdk_coin_select-0.2.0 (c (n "bdk_coin_select") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.30") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1xkgzqgyz560r2zw9ccd9xhyxlacjxczqrcmhmif1364p67h4l6d") (f (quote (("std") ("default" "std"))))))

(define-public crate-bdk_coin_select-0.3.0 (c (n "bdk_coin_select") (v "0.3.0") (d (list (d (n "bitcoin") (r "^0.30") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1m3s125jy5kc04akl60q8sxy2jql8hhaizqlr1pm8rqgdzvln21w") (f (quote (("std") ("default" "std"))))))

