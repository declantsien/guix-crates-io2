(define-module (crates-io bd k_ bdk_persist) #:use-module (crates-io))

(define-public crate-bdk_persist-0.1.0 (c (n "bdk_persist") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bdk_chain") (r "^0.13.0") (k 0)))) (h "15gj5mxkpif2h1a24vi5ljjsmdhbkrwqcks3561q5vkih2rxdxzn") (r "1.63")))

(define-public crate-bdk_persist-0.2.0 (c (n "bdk_persist") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bdk_chain") (r "^0.14.0") (k 0)))) (h "1w34b6d8jncwkaiaxnv6x1yf16j43k946035aasg1l4d231078db") (f (quote (("default" "bdk_chain/std")))) (r "1.63")))

(define-public crate-bdk_persist-0.3.0 (c (n "bdk_persist") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bdk_chain") (r "^0.15.0") (k 0)))) (h "0z9yjxw9525li1ywna5hscbkzybmxsyp4v2j8f0hfzshx1lqalj1") (f (quote (("serde" "bdk_chain/serde") ("miniscript" "bdk_chain/miniscript") ("default" "bdk_chain/std" "miniscript")))) (r "1.63")))

