(define-module (crates-io bd k_ bdk_testenv) #:use-module (crates-io))

(define-public crate-bdk_testenv-0.1.0 (c (n "bdk_testenv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bdk_chain") (r "^0.11") (k 0)) (d (n "bitcoincore-rpc") (r "^0.17") (d #t) (k 0)) (d (n "electrsd") (r "^0.25.0") (f (quote ("bitcoind_25_0" "esplora_a33e97e1" "legacy"))) (d #t) (k 0)))) (h "0acz77v8qpfqlm2grcxzr142wx7gxdrw2kg5pz6pr9zy1smvkqdz") (f (quote (("std" "bdk_chain/std") ("serde" "bdk_chain/serde") ("default" "std")))) (r "1.63")))

(define-public crate-bdk_testenv-0.2.0 (c (n "bdk_testenv") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bdk_chain") (r "^0.12") (k 0)) (d (n "bitcoincore-rpc") (r "^0.18") (d #t) (k 0)) (d (n "electrsd") (r "^0.27.1") (f (quote ("bitcoind_25_0" "esplora_a33e97e1" "legacy"))) (d #t) (k 0)))) (h "0mimpfw5rjccv569xp36xmmq69yp5fk6wp7k38saxzzk61jg5zrc") (f (quote (("std" "bdk_chain/std") ("serde" "bdk_chain/serde") ("default" "std")))) (r "1.63")))

(define-public crate-bdk_testenv-0.3.0 (c (n "bdk_testenv") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bdk_chain") (r "^0.13") (k 0)) (d (n "bitcoincore-rpc") (r "^0.18") (d #t) (k 0)) (d (n "electrsd") (r "^0.27.1") (f (quote ("bitcoind_25_0" "esplora_a33e97e1" "legacy"))) (d #t) (k 0)))) (h "1k4csslqqs0bhpwn7x3s7nfg57hnqv399pd7hbqxfq6z157vpf88") (f (quote (("std" "bdk_chain/std") ("serde" "bdk_chain/serde") ("default" "std")))) (r "1.63")))

(define-public crate-bdk_testenv-0.4.0 (c (n "bdk_testenv") (v "0.4.0") (d (list (d (n "bdk_chain") (r "^0.14") (k 0)) (d (n "electrsd") (r "^0.27.1") (f (quote ("bitcoind_25_0" "esplora_a33e97e1" "legacy"))) (d #t) (k 0)))) (h "0sjwxysgzvamwp888fxs1micrr4cnlmkc6n9sk906djqycwcljf3") (f (quote (("std" "bdk_chain/std") ("serde" "bdk_chain/serde") ("default" "std")))) (r "1.63")))

(define-public crate-bdk_testenv-0.5.0 (c (n "bdk_testenv") (v "0.5.0") (d (list (d (n "bdk_chain") (r "^0.15") (k 0)) (d (n "electrsd") (r "^0.27.1") (f (quote ("bitcoind_25_0" "esplora_a33e97e1" "legacy"))) (d #t) (k 0)))) (h "0gf6nqnmfd1kvhdlk9dyhc8f0l58xq7pykr83js52h1kq4q7kba7") (f (quote (("std" "bdk_chain/std") ("serde" "bdk_chain/serde") ("default" "std")))) (r "1.63")))

