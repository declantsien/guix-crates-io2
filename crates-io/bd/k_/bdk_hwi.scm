(define-module (crates-io bd k_ bdk_hwi) #:use-module (crates-io))

(define-public crate-bdk_hwi-0.2.0 (c (n "bdk_hwi") (v "0.2.0") (d (list (d (n "bdk_wallet") (r "^1.0.0-alpha.12") (d #t) (k 0)) (d (n "hwi") (r "^0.8.0") (f (quote ("miniscript"))) (d #t) (k 0)))) (h "0pdhnp2c3qc0scdczlfl4880v7fcgbiahjpsc1ldsfy5nmhxndyr")))

