(define-module (crates-io bd ec bdecode) #:use-module (crates-io))

(define-public crate-bdecode-0.1.0 (c (n "bdecode") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 2)))) (h "1gpr9i069pjz4q1h3wcl6b80lpcpma0jy693yr2wmav7zvxvyfdr")))

