(define-module (crates-io bd wg bdwgc-sys) #:use-module (crates-io))

(define-public crate-bdwgc-sys-0.1.0 (c (n "bdwgc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1xyq98gnc21yd7s9psmnlppn4b8a9dibhag4qkfqqga2cn4khk3g") (y #t)))

(define-public crate-bdwgc-sys-0.1.1 (c (n "bdwgc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r ">=0.59.2") (d #t) (k 1)) (d (n "cmake") (r ">=0.1.48") (d #t) (k 1)))) (h "0l0hdkmprcczwwz48qs3xny13f50fllckzqz4mpvgqldkv2bpqzc")))

