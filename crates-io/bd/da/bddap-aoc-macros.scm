(define-module (crates-io bd da bddap-aoc-macros) #:use-module (crates-io))

(define-public crate-bddap-aoc-macros-0.1.0 (c (n "bddap-aoc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1hvj63h25ciiq1bgqlx832wq7ilh328k9lpad6pcjmxirdy1z7y0")))

(define-public crate-bddap-aoc-macros-0.1.1 (c (n "bddap-aoc-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0anjdvljlqizvwixz054biqhnlbpmqq9575lci0x804i8h3sjp6x")))

