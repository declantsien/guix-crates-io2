(define-module (crates-io bd ad bdaddr) #:use-module (crates-io))

(define-public crate-bdaddr-0.1.0 (c (n "bdaddr") (v "0.1.0") (d (list (d (n "aes") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1crfyjbg0pxw2r27qrf22xijhlj3gap1l6dkjr48v67jarn6jkrl") (f (quote (("matches" "generic-array" "aes") ("default"))))))

(define-public crate-bdaddr-0.1.1 (c (n "bdaddr") (v "0.1.1") (d (list (d (n "aes") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d4drab628yjip9307yvafclsccklyrlgvzi4bvl6xwcl13si30l") (f (quote (("matches" "aes") ("default"))))))

(define-public crate-bdaddr-0.1.2 (c (n "bdaddr") (v "0.1.2") (d (list (d (n "aes") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0js6k81fblinfb59v5ax7r0549wxnhcnvl96pajlkprm3660amg4") (f (quote (("matches" "aes") ("default"))))))

(define-public crate-bdaddr-0.2.0-alpha.1 (c (n "bdaddr") (v "0.2.0-alpha.1") (d (list (d (n "aes") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d992h1ciy6sh0sks0zsk8wigl41bn0nqm4l138yyi7zlpfvykbq") (f (quote (("matches" "aes") ("default")))) (r "1.56")))

(define-public crate-bdaddr-0.2.0-alpha.2 (c (n "bdaddr") (v "0.2.0-alpha.2") (d (list (d (n "aes") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13kj2rw6hfpyq4wg4b88811bgvawb6w5lp51x9z8sghigz85qc60") (f (quote (("matches" "aes") ("default")))) (r "1.56")))

(define-public crate-bdaddr-0.2.0-alpha.3 (c (n "bdaddr") (v "0.2.0-alpha.3") (d (list (d (n "aes") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xzxcr1526sr8l2qbdg1ikch61fcjz55m88mwm3qwwnfy4ss3rhk") (f (quote (("matches" "aes") ("default")))) (r "1.56")))

(define-public crate-bdaddr-0.2.0-alpha.4 (c (n "bdaddr") (v "0.2.0-alpha.4") (d (list (d (n "aes") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g7xsn1lmppy52vcn9cxkbp67m47kh5g074s1r3mpv9r6lk2m953") (f (quote (("matches" "aes") ("default")))) (r "1.56")))

