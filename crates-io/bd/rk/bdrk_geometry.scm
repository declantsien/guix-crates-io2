(define-module (crates-io bd rk bdrk_geometry) #:use-module (crates-io))

(define-public crate-bdrk_geometry-0.0.1 (c (n "bdrk_geometry") (v "0.0.1") (h "13g35j5r56i438gcfn0zrj5a0kxqly3l2q6lya161f10z353m3is")))

(define-public crate-bdrk_geometry-0.0.2 (c (n "bdrk_geometry") (v "0.0.2") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0pw9w9v0qc01fc8alcsd2wxjs3v0gvk49iz4j87r35wp97wdvi0i")))

(define-public crate-bdrk_geometry-0.0.3 (c (n "bdrk_geometry") (v "0.0.3") (h "0037632clqgh1y7qyi5fvnrhpjqms4s4nj9y10lc7hgwi8qy6py0")))

(define-public crate-bdrk_geometry-0.0.4 (c (n "bdrk_geometry") (v "0.0.4") (h "0hg9sp9gw3y3mwxn1ydvqim2b81szw1glyc4ikqk88fi76zilpll")))

