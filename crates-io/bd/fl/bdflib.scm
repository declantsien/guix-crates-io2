(define-module (crates-io bd fl bdflib) #:use-module (crates-io))

(define-public crate-bdflib-0.1.0 (c (n "bdflib") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "1331hll1i536zabf18gwam9x06kd0xgp6lsz3v18a62jm9k5lb54")))

(define-public crate-bdflib-0.1.1 (c (n "bdflib") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "1rqi4g8l73dsvwyg7f5b00w0n75v7l6sd7vgyymsflgmf1z043cs")))

(define-public crate-bdflib-0.1.2 (c (n "bdflib") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "1mi5rxyi7l59jihad1h9l3l80sspl1jf1rivm85049gh5cbhkvp6")))

(define-public crate-bdflib-0.1.3 (c (n "bdflib") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "1fq50rc0zxg25ffcizkw1lxshqlqxa6zk7w0vqxi6m02xk475aig")))

(define-public crate-bdflib-0.1.4 (c (n "bdflib") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "0wvzc1ryxwa5r3pkbnsyxk46rwqnk4mpcyhp3bj7hihdr5lnmwi7")))

(define-public crate-bdflib-0.1.5 (c (n "bdflib") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "16mbz6rnk4fsp5pybm09pj4awaqlajnf769kljr73k347fnvv7gg")))

(define-public crate-bdflib-0.2.0 (c (n "bdflib") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "1l6ypjsm6qbipwlbzp6gfzgpvcj8kvhm518my84xzp5i14rs8qil")))

(define-public crate-bdflib-0.3.0 (c (n "bdflib") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "1hhxcryihmhc7jvj2fd5vf0vx36a4n5kscqjp45mj50nd4l2ln8j")))

(define-public crate-bdflib-0.3.1 (c (n "bdflib") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "0clkx2j04f0wfsbvvgvckv532f7knwwsy1ppq31bc0pfzf5plibf")))

(define-public crate-bdflib-0.3.2 (c (n "bdflib") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "0zkwdfcdj2a1bkp6j8ihhfqwif6522dkmxfkzchmi2slvn35kyz5")))

(define-public crate-bdflib-0.3.3 (c (n "bdflib") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "11y5wyyp2dn3hni869v6zgcawn5hr90l0igs9lcmr1ninhhwn7zn")))

(define-public crate-bdflib-0.3.4 (c (n "bdflib") (v "0.3.4") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "1cwkphl7801xvy05nv2k76fw2bdcqc2cq134c5cfpbyampnd4vwj")))

(define-public crate-bdflib-0.3.5 (c (n "bdflib") (v "0.3.5") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "1s4iw1j6fpdgd1l1b3avah4m50xs2qgkq8km1yv0pzrbnn6y5gm6")))

(define-public crate-bdflib-0.3.6 (c (n "bdflib") (v "0.3.6") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "05xdjs97l7hj84xbrrmjbiybrmy03a1ymiv57iy5av02fk2vfgrg")))

(define-public crate-bdflib-0.3.7 (c (n "bdflib") (v "0.3.7") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "1nfn7igbb4mc3bass819vmj5hbk3fpbzrbmvyqmpmwvp4851mmm5")))

(define-public crate-bdflib-0.4.0 (c (n "bdflib") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "0wim0ya95nchx35jk8f53cp02nz37bknavfycjfah2v9w7ar5dgg")))

(define-public crate-bdflib-0.4.1 (c (n "bdflib") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "18p1a2km13dg23nbn5fbzndi6hq30w26sa3cxbcfhikfjq1dv0z8")))

(define-public crate-bdflib-0.4.2 (c (n "bdflib") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "02n9mclq0kwgmqsp4jjx3pbv6pl4w0jh6rs9wi0q0z2hx3lyll0z")))

(define-public crate-bdflib-0.4.3 (c (n "bdflib") (v "0.4.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "1jxhswlhx4wwkadz4fqbq7z0s0gqa1ffhmqd9x15w09id0n5031y")))

(define-public crate-bdflib-0.4.4 (c (n "bdflib") (v "0.4.4") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "1b6g9g7igdvnq7hj5rfx4la6aj6ws9msb8skg0q8q948a3j511iz")))

