(define-module (crates-io bd f- bdf-parser) #:use-module (crates-io))

(define-public crate-bdf-parser-0.1.0 (c (n "bdf-parser") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2.15") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0y1m8imgakawww4bs5zl1hibq7gh3y8v97g4bnswax2cr4m3c169")))

