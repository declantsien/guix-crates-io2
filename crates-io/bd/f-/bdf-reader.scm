(define-module (crates-io bd f- bdf-reader) #:use-module (crates-io))

(define-public crate-bdf-reader-0.1.0 (c (n "bdf-reader") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "simple_logger") (r "^2.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qjv01i7kx2jnhid252sw7dlrfqrap6f55hlfm46vvgdvvljdksv") (y #t)))

(define-public crate-bdf-reader-0.1.1 (c (n "bdf-reader") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "simple_logger") (r "^2.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03wngxmzd7mw1wvq4f6p4vjkclxjscjmxzibg3pwnhi73yfdcpwy")))

(define-public crate-bdf-reader-0.1.2 (c (n "bdf-reader") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "simple_logger") (r "^4.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hgwmxwd4kdgfnq53mr7gah5m9jzl5bjmgjgpa1wvr5f5h5qq6n6")))

