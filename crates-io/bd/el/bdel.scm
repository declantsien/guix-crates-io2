(define-module (crates-io bd el bdel) #:use-module (crates-io))

(define-public crate-bdel-0.1.0 (c (n "bdel") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1vqr3h2r8py7dp6bfy3cmnmdzgc7ynmfwai31yvkm1liahfma5jn")))

