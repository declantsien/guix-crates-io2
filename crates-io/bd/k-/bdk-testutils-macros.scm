(define-module (crates-io bd k- bdk-testutils-macros) #:use-module (crates-io))

(define-public crate-bdk-testutils-macros-0.2.0 (c (n "bdk-testutils-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0gvwzk7wm4gw68153jjkyxx4s1wlsn8h4nqnj7cy7sfcx1g2fc0n") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-bdk-testutils-macros-0.3.0 (c (n "bdk-testutils-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "03hilqp11dpinywc5k84r76rwrdhsv6l5hbkcm3b51pcz8ibaq31") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-bdk-testutils-macros-0.4.0 (c (n "bdk-testutils-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0hjrypd2bq603zxrqjzn0iz1kjq3gmim4zhnkq2b46wpkab3r54p") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-bdk-testutils-macros-0.5.0 (c (n "bdk-testutils-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1i22w2zzwv6nsm4pmisjn7rl8mvxvik8nqs35d7wi6kfrnpcqjhv") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-bdk-testutils-macros-0.6.0 (c (n "bdk-testutils-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1sw96zajzzk6d5w6x31br884n00w49v8g3kwn9p2s72ln01f4q5l") (f (quote (("debug" "syn/extra-traits"))))))

