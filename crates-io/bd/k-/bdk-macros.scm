(define-module (crates-io bd k- bdk-macros) #:use-module (crates-io))

(define-public crate-bdk-macros-0.2.0 (c (n "bdk-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1qgndkw9lwkdmp8y02qip23m54raphjy9mmsqc7yn8pj3n878a7n") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-bdk-macros-0.3.0 (c (n "bdk-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "19k7d86zg1b3x6879x0iw4l3awcw9m36clswcgqaiq67x7dmfrxr") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-bdk-macros-0.4.0 (c (n "bdk-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "18klc0wb2i4s2mz0sr42ziihccbfvfzqb3wshm2l2xshhavp0mdl") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-bdk-macros-0.5.0 (c (n "bdk-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0y7zg8pxi60dxrb0jp3rrsxk581br7a7x7qnrjamjv4lbq0i1xf3") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-bdk-macros-0.6.0 (c (n "bdk-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0v55l4jllpzn6cjyd7x9lrpjrsnng63ax0wjz9pbn8xfa079ihc1") (f (quote (("debug" "syn/extra-traits"))))))

