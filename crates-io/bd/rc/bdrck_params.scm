(define-module (crates-io bd rc bdrck_params) #:use-module (crates-io))

(define-public crate-bdrck_params-0.2.0 (c (n "bdrck_params") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)) (d (n "bdrck_test") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1dpdnz1369jp1fqr6pl2yy829z0ww4m89rwy2h7x3d9pyvm14h27")))

(define-public crate-bdrck_params-0.3.0 (c (n "bdrck_params") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)) (d (n "bdrck_test") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1jdknp5i9xdkp2nhalwzzi4qk9ksb50riy32cdh380f9xl95g51z")))

(define-public crate-bdrck_params-0.3.1 (c (n "bdrck_params") (v "0.3.1") (d (list (d (n "bdrck_test") (r "^0.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0w5di83nx60svw67l742hsgxr9q5v2pil2bdzfpgr4w0mfv03kab")))

(define-public crate-bdrck_params-0.3.2 (c (n "bdrck_params") (v "0.3.2") (d (list (d (n "bdrck_test") (r "^0.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0l13cn61hlpmhf2yl3czgya3xg1x87yb94cppgrza01jx0bds13h")))

