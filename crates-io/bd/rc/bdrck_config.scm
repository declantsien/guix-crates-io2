(define-module (crates-io bd rc bdrck_config) #:use-module (crates-io))

(define-public crate-bdrck_config-0.1.0 (c (n "bdrck_config") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 2)))) (h "0njcihaz5mam907k2gjcg3d7wqdi0w7p89xkj59j14ngm4q8npx1")))

(define-public crate-bdrck_config-0.1.1 (c (n "bdrck_config") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 2)))) (h "0h71kd8p421bd3j26f0xxzg9l4gnycgzdbzvj1rbz9bryk67db07")))

(define-public crate-bdrck_config-0.1.2 (c (n "bdrck_config") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 2)))) (h "0k3b113b985nnnxvq8bf6xr5rmqp54i4ib4jdjfxmna8rh2w35mn")))

(define-public crate-bdrck_config-0.1.3 (c (n "bdrck_config") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 2)))) (h "0nr7acbn0nq4d3rn2h0l0805ijvrdkkdnglx9ghxhqgw5yw45zk4")))

(define-public crate-bdrck_config-0.2.0 (c (n "bdrck_config") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 2)))) (h "1nvydvnnzcysf46g0gbygsvv4akzf1wnd5fidnispgaq9fgsd210")))

