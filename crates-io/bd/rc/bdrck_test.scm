(define-module (crates-io bd rc bdrck_test) #:use-module (crates-io))

(define-public crate-bdrck_test-0.1.0 (c (n "bdrck_test") (v "0.1.0") (h "1ad7y5i2l7rbz3wyf269llb9sr6xyh48dk11h0p8p75x3fkgabcj")))

(define-public crate-bdrck_test-0.1.1 (c (n "bdrck_test") (v "0.1.1") (h "0f2d6n48xc9qv407j4bdpw8a7fm915n77b40jk6iiw2b7jxbfi4z")))

