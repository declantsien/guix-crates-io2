(define-module (crates-io bd ay bdays) #:use-module (crates-io))

(define-public crate-bdays-0.0.1 (c (n "bdays") (v "0.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "1y2sdsh0jg3ilpvah6d6jsbcn6q0zxa0mrsjn61vr36gn8g0gzh4")))

(define-public crate-bdays-0.0.2 (c (n "bdays") (v "0.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "1mha0hah16y7p1gnkzbjk0rw76qh323fgg7vvw5wxl900bb57dh0")))

(define-public crate-bdays-0.1.0 (c (n "bdays") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1zr6cgvzam0zkgkvhx25biqs8fd2aylw69y1rbbp748qyg7kjdq3")))

(define-public crate-bdays-0.1.1 (c (n "bdays") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "05v783hh5a10xqhym8wsm5v99x9wm5n6p9bzyfmay836ri0j3k94")))

(define-public crate-bdays-0.1.2 (c (n "bdays") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1ynqbv9hmph3pljggwrsi6f47d0zmfj7h4dp10czv0qyqwhic39i")))

(define-public crate-bdays-0.1.3 (c (n "bdays") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (k 0)))) (h "08r0mrgk500ivdwbqrwykxqrh5zvs5jagq7fwpv5yvdgxfifyf9x")))

(define-public crate-bdays-0.1.4 (c (n "bdays") (v "0.1.4") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (k 0)))) (h "0gkpd9m7yynjmzgw74dpz7kv93c7n5wgwbpnidbgmncxzagx97ss")))

