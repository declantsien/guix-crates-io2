(define-module (crates-io bd di bddisasm-sys) #:use-module (crates-io))

(define-public crate-bddisasm-sys-0.1.0 (c (n "bddisasm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)))) (h "0vlx20z6jzq8qws3gkd28hnyvmv68q3hcwb1pql8h382bb3yna3a") (l "bddisasm")))

(define-public crate-bddisasm-sys-0.2.0 (c (n "bddisasm-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)))) (h "0fghld48g8i67g3dbdqb8xr7n43b42lhz1xchgxv68j1ch80imjl") (l "bddisasm")))

(define-public crate-bddisasm-sys-0.2.1 (c (n "bddisasm-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)))) (h "0zz1vdxq80nf4177ll64hdar6v2m21d8ncds96w1lwsvgjlfrsxv") (l "bddisasm")))

(define-public crate-bddisasm-sys-0.3.0 (c (n "bddisasm-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)))) (h "1c6172mbf5j55jpj8750p2nq1ip2qrvj8wd3xbg4z6j7jmkdpi3z") (l "bddisasm")))

(define-public crate-bddisasm-sys-0.2.3 (c (n "bddisasm-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.62.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)))) (h "1f9ignfbxgd6s73jmnh17zg07wr8hm3vvqldfiyr181ddn0hh7kb") (y #t) (l "bddisasm")))

(define-public crate-bddisasm-sys-0.4.2 (c (n "bddisasm-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.62.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)))) (h "16q9pdvml5snqika2l4z9abx9dizckzhfykqlqvlpyvshlg3l7vq") (l "bddisasm")))

