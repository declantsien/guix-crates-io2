(define-module (crates-io bd di bddisasm) #:use-module (crates-io))

(define-public crate-bddisasm-0.1.0 (c (n "bddisasm") (v "0.1.0") (d (list (d (n "bddisasm-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1xmpbx9arfx4ssxs0ji6pyxvv2hscz8mzbbnk2bc941vd85w7s1a")))

(define-public crate-bddisasm-0.2.0 (c (n "bddisasm") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bddisasm-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 2)))) (h "1rdykkq5n5nihwv48ng6s4c9940lcs51d87wi3z5mm1br45mwc3s") (f (quote (("std"))))))

(define-public crate-bddisasm-0.2.1 (c (n "bddisasm") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bddisasm-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 2)))) (h "1fn8gcw67c5bcw9j84nbvps1hpkpy35i6z16vyjki0l72gl4i66k") (f (quote (("std"))))))

(define-public crate-bddisasm-0.3.0 (c (n "bddisasm") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bddisasm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 2)))) (h "1an2i14azblmlddy5iafqh43i4ir8f0ybxjmandghyx89z2nzgjw") (f (quote (("std"))))))

(define-public crate-bddisasm-0.2.3 (c (n "bddisasm") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 2)) (d (n "bddisasm-sys") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 2)))) (h "0qrlibqnvcifsq1w8cv1167pwpq2195ydr5xb1l51lkl6gdbncal") (f (quote (("std")))) (y #t)))

(define-public crate-bddisasm-0.4.2 (c (n "bddisasm") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 2)) (d (n "bddisasm-sys") (r "^0.4.2") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 2)))) (h "17f8khdw4hnbjfbq39rg6szdgazz0d60b7bj2nysxw406jxqm193") (f (quote (("std"))))))

