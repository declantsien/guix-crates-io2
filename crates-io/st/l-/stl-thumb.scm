(define-module (crates-io st l- stl-thumb) #:use-module (crates-io))

(define-public crate-stl-thumb-0.5.0 (c (n "stl-thumb") (v "0.5.0") (d (list (d (n "cbindgen") (r "0.23.*") (d #t) (k 1)) (d (n "cgmath") (r "^0.18.0") (f (quote ("mint"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glium") (r "^0.31.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.1") (d #t) (k 0)) (d (n "stl_io") (r "^0.6.0") (d #t) (k 0)))) (h "0p4iq03crl3y72a0kccw9vh3p8ywhznkswc2269jws6d8i3ld53q")))

