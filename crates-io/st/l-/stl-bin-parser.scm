(define-module (crates-io st l- stl-bin-parser) #:use-module (crates-io))

(define-public crate-stl-bin-parser-0.0.0 (c (n "stl-bin-parser") (v "0.0.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.1") (d #t) (k 0)))) (h "0jcw3fh8rkx7faba4g33549bk8gwc6k34n7qf6m07vxm3rv1jy3s")))

(define-public crate-stl-bin-parser-0.0.1 (c (n "stl-bin-parser") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "nom") (r "^2.2.1") (d #t) (k 0)))) (h "095yj7npvbx1mjkjyqgfnwm75jglzhdmj40f7sdbnvl6nkm84739")))

