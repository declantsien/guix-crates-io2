(define-module (crates-io st ut stutter-rs) #:use-module (crates-io))

(define-public crate-stutter-rs-0.1.0 (c (n "stutter-rs") (v "0.1.0") (d (list (d (n "cactus") (r "^1.0.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "0v0f9qas1zfmgc86jhy64qimzrlwjwijap2wfgscr91m6jcs7m3h")))

(define-public crate-stutter-rs-0.1.1 (c (n "stutter-rs") (v "0.1.1") (d (list (d (n "cactus") (r "^1.0.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "0836w51yr6r1app57cm2gzkwy24r3rjzs8paajpsjpwyn0i5yw4b")))

