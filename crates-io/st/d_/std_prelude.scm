(define-module (crates-io st d_ std_prelude) #:use-module (crates-io))

(define-public crate-std_prelude-0.2.2 (c (n "std_prelude") (v "0.2.2") (h "1xqrsp4a0kd3s53hfjksrs227kadspw5xym1n919fjwsw0ndvnly")))

(define-public crate-std_prelude-0.2.3 (c (n "std_prelude") (v "0.2.3") (h "0707y44b2mchx1ffmrm3i4lpgzck4875s55n9ac9ip52srikiy1r")))

(define-public crate-std_prelude-0.2.4 (c (n "std_prelude") (v "0.2.4") (h "0sjky389w8kypn9by5mqzg3qfzik0la2jkf4q3kvcia3avkv1mra")))

(define-public crate-std_prelude-0.2.5 (c (n "std_prelude") (v "0.2.5") (h "06j86bvy5bbk5lgg2rqjx9n07smyrlvwjflp49kj6nbfhzk1aqy2")))

(define-public crate-std_prelude-0.2.7 (c (n "std_prelude") (v "0.2.7") (h "1xx5c3gzcrmj0q4qslkhwkyxphgv86gyl9284g3b893gn2xy3ycd")))

(define-public crate-std_prelude-0.2.8 (c (n "std_prelude") (v "0.2.8") (h "07cyq19wxmz3ppx8brmppar0h4q4hc7pk91n3f6ii7jp6hgqqf54")))

(define-public crate-std_prelude-0.2.9 (c (n "std_prelude") (v "0.2.9") (h "1p398wgm98fcai28x409myvymdq04mbk8zrsq7c68zsy45irfpy9")))

(define-public crate-std_prelude-0.2.10 (c (n "std_prelude") (v "0.2.10") (h "190s6d3k8hlc1lfg5ns4ih7kd9m1lys49az6987wnphvb9c5xl3m")))

(define-public crate-std_prelude-0.2.11 (c (n "std_prelude") (v "0.2.11") (h "0ay51dncwzvvg7zgmxkp8d2hzzw552w08c0dji4m7dn3br92gqfy")))

(define-public crate-std_prelude-0.2.12 (c (n "std_prelude") (v "0.2.12") (h "1ghcwnhnqn3rphyhlknmxpj5clzqva46z1vh25k5bpzzan2ff1w2")))

