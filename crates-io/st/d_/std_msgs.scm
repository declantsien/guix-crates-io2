(define-module (crates-io st d_ std_msgs) #:use-module (crates-io))

(define-public crate-std_msgs-4.2.3 (c (n "std_msgs") (v "4.2.3") (d (list (d (n "builtin_interfaces") (r "^1.2.1") (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "18y2sbjvw6gv8gj0ikqxrz0m8y08msd8bk55wf6ygi5lmnd5n0zk") (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde" "builtin_interfaces/serde"))))))

