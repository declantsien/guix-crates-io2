(define-module (crates-io st d_ std_collection_traits) #:use-module (crates-io))

(define-public crate-std_collection_traits-0.1.1 (c (n "std_collection_traits") (v "0.1.1") (h "0qk5mby9c5aq94527p1zrjxkrg8ih7033d41bkgkpgbdyd61gpvp")))

(define-public crate-std_collection_traits-0.1.2 (c (n "std_collection_traits") (v "0.1.2") (h "1ir1g9ixqhgrdb48w4y51bpngij8hp8c8jpr8479y4flc643qxaa")))

