(define-module (crates-io st d_ std_serde_shims) #:use-module (crates-io))

(define-public crate-std_serde_shims-0.2.0 (c (n "std_serde_shims") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "174ipfwflm7djppjxv2d96mz3qf67gwnygybvdvqcjsa5ncg4s6l")))

(define-public crate-std_serde_shims-0.2.1 (c (n "std_serde_shims") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "15x2swfr7qqf429igminbxrahx9vh0h6j7am2m9m9824d7szmpiz")))

