(define-module (crates-io st d_ std_srvs) #:use-module (crates-io))

(define-public crate-std_srvs-4.2.3 (c (n "std_srvs") (v "4.2.3") (d (list (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0canlgzsgqwcbs1jlqzwslr7n434ng128lfv11k6yv87465zrcfb") (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde"))))))

