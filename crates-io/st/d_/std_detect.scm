(define-module (crates-io st d_ std_detect) #:use-module (crates-io))

(define-public crate-std_detect-0.1.0 (c (n "std_detect") (v "0.1.0") (d (list (d (n "auxv") (r "^0.3.3") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "core_arch") (r "^0.1.0") (d #t) (k 2)) (d (n "cupid") (r "^0.6.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hacyrdmj25la7b7ml2b6rp8qlnx4s3v16kdhhsvhy4qaz1kp2a1")))

(define-public crate-std_detect-0.1.1 (c (n "std_detect") (v "0.1.1") (d (list (d (n "auxv") (r "^0.3.3") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cupid") (r "^0.6.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a4743f6m4qyqry0szdxbw43wxc70fyb8838xk57iv3xskql94k5")))

(define-public crate-std_detect-0.1.2 (c (n "std_detect") (v "0.1.2") (d (list (d (n "auxv") (r "^0.3.3") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cupid") (r "^0.6.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pxyicp28rvq9dcm1qfk36yry0cfbfzrsfb7pjbimfqdrg3hjn12")))

(define-public crate-std_detect-0.1.3 (c (n "std_detect") (v "0.1.3") (d (list (d (n "auxv") (r "^0.3.3") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cupid") (r "^0.6.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14gj3aqk3rhmsp3p0czgfqjd3w1d87fjpnhgaikjscfgynhyxspy")))

(define-public crate-std_detect-0.1.4 (c (n "std_detect") (v "0.1.4") (d (list (d (n "auxv") (r "^0.3.3") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cupid") (r "^0.6.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "0xick26b3139sfniy2m7mryi8w9w9k2nh74m5qb6cw3lljfd51f3") (f (quote (("std_detect_file_io") ("std_detect_dlsym_getauxval" "libc") ("default" "std_detect_dlsym_getauxval" "std_detect_file_io"))))))

(define-public crate-std_detect-0.1.5 (c (n "std_detect") (v "0.1.5") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "110rbmsj5s1ymhxlmm71nkwp3bfl52n9609b4n2gvfq51n41vdky") (f (quote (("std_detect_file_io") ("std_detect_dlsym_getauxval" "libc") ("default" "std_detect_dlsym_getauxval" "std_detect_file_io"))))))

