(define-module (crates-io st d_ std_io_iterators) #:use-module (crates-io))

(define-public crate-std_io_iterators-1.0.0 (c (n "std_io_iterators") (v "1.0.0") (d (list (d (n "escargot") (r "~0.5.7") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "121i4ylvv6ml55c308la6cvd7p8ig5k79qg488iq1nqnbidiqjax")))

