(define-module (crates-io st op stopper) #:use-module (crates-io))

(define-public crate-stopper-0.1.0 (c (n "stopper") (v "0.1.0") (d (list (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.5") (d #t) (k 0)) (d (n "waker-set") (r "^0.1.1") (d #t) (k 0)))) (h "1svxxsrsi2v4lyp2ym7dar0j8pcl0qjq0sc22mnkrj5njxcc7ibw")))

(define-public crate-stopper-0.1.1 (c (n "stopper") (v "0.1.1") (d (list (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.5") (d #t) (k 0)) (d (n "waker-set") (r "^0.2.0") (d #t) (k 0)))) (h "1rx5c88ca11gpmwwgk8qnbn1n5ld0kws6g99dm5w4pyigm8fdj9b")))

(define-public crate-stopper-0.2.0 (c (n "stopper") (v "0.2.0") (d (list (d (n "async-io") (r "^1.3.1") (d #t) (k 2)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.5") (d #t) (k 0)) (d (n "waker-set") (r "^0.2.0") (d #t) (k 0)))) (h "0wq0pm2843xj1dg2nz3ahvl7727wr78m2waww56r6mp3aqjbyws5")))

(define-public crate-stopper-0.2.1 (c (n "stopper") (v "0.2.1") (d (list (d (n "async-io") (r "^2.0.0") (d #t) (k 2)) (d (n "event-listener") (r "^3.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "1irkc18g6wknjn4giaig30zgrj44cvf5wiwcym0sif21xx2dmvln")))

(define-public crate-stopper-0.2.2 (c (n "stopper") (v "0.2.2") (d (list (d (n "async-io") (r "^2.0.0") (d #t) (k 2)) (d (n "event-listener") (r "^3.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "1yv6nhxwkd2zpljrrr7cl4cvdj0bx5zrssv0zzvlzah9d54smjy3")))

(define-public crate-stopper-0.2.3 (c (n "stopper") (v "0.2.3") (d (list (d (n "async-io") (r "^2.2.0") (d #t) (k 2)) (d (n "event-listener") (r "^4.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.0.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "1rnr2x2148356xqvzkn4ybl5k5rb57qx4jf95ixs43h28xmcy3l1")))

(define-public crate-stopper-0.2.4 (c (n "stopper") (v "0.2.4") (d (list (d (n "async-io") (r "^2.2.0") (d #t) (k 2)) (d (n "event-listener") (r "^4.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.0.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "1lvvazij475ismz5d8d1vpppcxkcjcjs45yzfvxssw1xmd9gz2zd")))

(define-public crate-stopper-0.2.5 (c (n "stopper") (v "0.2.5") (d (list (d (n "event-listener") (r "^5.2.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "test-harness") (r "^0.2.0") (d #t) (k 2)))) (h "1va2sm03ic42knn6b7wd59v0hrkpy52rm5x4ly52abg3bzf3dyj8")))

(define-public crate-stopper-0.2.6 (c (n "stopper") (v "0.2.6") (d (list (d (n "event-listener") (r "^5.2.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "test-harness") (r "^0.2.0") (d #t) (k 2)))) (h "1qcxrhd5wbiqv20d1kh70dmq5899jby8wp4l5j4060vbfvbdqagb")))

(define-public crate-stopper-0.2.7 (c (n "stopper") (v "0.2.7") (d (list (d (n "event-listener") (r "^5.2.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "test-harness") (r "^0.2.0") (d #t) (k 2)))) (h "06bh6fksy3b0qm27vda863w00hxwwhn7hy79aqbmj7kas41jvj36")))

