(define-module (crates-io st op stopwords) #:use-module (crates-io))

(define-public crate-stopwords-0.1.0 (c (n "stopwords") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "0zllny0838irrs79gqxddypa41jnvfa9nvfvq13fwg8k1r84sf4n")))

(define-public crate-stopwords-0.1.1 (c (n "stopwords") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gki3g46cxld57nqw4assh6lc9wryb8jxm0jj4aymrijw6k0hi8f")))

