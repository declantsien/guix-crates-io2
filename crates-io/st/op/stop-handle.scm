(define-module (crates-io st op stop-handle) #:use-module (crates-io))

(define-public crate-stop-handle-0.1.0 (c (n "stop-handle") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "1gibk7s77l2nlxpr35p59zryhib5w86ydcl3kixxqg9nyxpyli43")))

