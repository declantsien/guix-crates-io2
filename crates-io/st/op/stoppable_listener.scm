(define-module (crates-io st op stoppable_listener) #:use-module (crates-io))

(define-public crate-stoppable_listener-0.1.0 (c (n "stoppable_listener") (v "0.1.0") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0nvh4pl9ms3aanz4z5cmj1rka92dw59hz6rjbcn3qyv5l2yb4s89")))

