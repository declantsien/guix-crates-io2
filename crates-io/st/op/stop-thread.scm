(define-module (crates-io st op stop-thread) #:use-module (crates-io))

(define-public crate-stop-thread-0.1.0 (c (n "stop-thread") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "sig") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15ncbx19x2b5gvjkmw7i9cw2cl50qvkiyngyk186y0d483hn74a5")))

(define-public crate-stop-thread-0.1.1 (c (n "stop-thread") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1hz0lipckwdib8njz76qv956s6q41g9m102cm6n4wi4hrk2c8qbk")))

(define-public crate-stop-thread-0.1.2 (c (n "stop-thread") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ynh0az486azmcirfdzsnv612p9fmf3n0kdjyfld5cg54lmhsxbw")))

(define-public crate-stop-thread-0.1.3 (c (n "stop-thread") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1q5w1ay0acviz61vx1789fvd2h21nbp4nkmqfr2g1npj47l3hnhb")))

(define-public crate-stop-thread-0.1.4 (c (n "stop-thread") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0h0plkqnxw9bpx9475b2v1yns3mhyq95a9pvj4z55dcn2ls4jj9z")))

(define-public crate-stop-thread-0.1.5 (c (n "stop-thread") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1z9ad7lz39abxz3iq513p51lvf49khd68zh8dixvcc1bcic7qxin")))

(define-public crate-stop-thread-0.2.0 (c (n "stop-thread") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1cpxmjpw286liwpwrs0wbmy471wfmhaj9bnbsqmcs0dqmk4da8sc")))

