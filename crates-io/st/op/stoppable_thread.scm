(define-module (crates-io st op stoppable_thread) #:use-module (crates-io))

(define-public crate-stoppable_thread-0.1.0 (c (n "stoppable_thread") (v "0.1.0") (h "1sdvwyw4lp1f9yw2wv2chl1w1zw1n6zjdgkphp440iqnh8ppmljz")))

(define-public crate-stoppable_thread-0.1.1 (c (n "stoppable_thread") (v "0.1.1") (h "0gj315977jynaslmc3cvrd7i8ybcn655chysgaaphzh7599gjyrq")))

(define-public crate-stoppable_thread-0.1.2 (c (n "stoppable_thread") (v "0.1.2") (h "1amvqcf0hz0z28cy0wg71ihiqv3izh6i93r7sdq4zp2hgb9dg69c")))

(define-public crate-stoppable_thread-0.1.3 (c (n "stoppable_thread") (v "0.1.3") (h "1i9pqyi06xfbmg1v1f7iwk6yc2d07xzh83z9clv8672jj3nw75ng")))

(define-public crate-stoppable_thread-0.1.4 (c (n "stoppable_thread") (v "0.1.4") (h "1gkafw5051khgzlzwyw6riwpgnrlvjqxrwl6g9r0i3zxamscf9w2")))

(define-public crate-stoppable_thread-0.2.0 (c (n "stoppable_thread") (v "0.2.0") (h "02ki346xl37kc84fff76xcwzr60r5al8mkcimcgccr3s2dxhviz7")))

(define-public crate-stoppable_thread-0.2.1 (c (n "stoppable_thread") (v "0.2.1") (h "17q1cnmlr48vrr0hd0pfmmnl6mj89ak3404mzwgrralkscg0qw76")))

