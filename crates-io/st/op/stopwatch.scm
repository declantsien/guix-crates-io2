(define-module (crates-io st op stopwatch) #:use-module (crates-io))

(define-public crate-stopwatch-0.0.2 (c (n "stopwatch") (v "0.0.2") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1ifywzadrn94wds87m3bvvy7h6l37lgqav9fyjkqlczxx3qkn48d")))

(define-public crate-stopwatch-0.0.3 (c (n "stopwatch") (v "0.0.3") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "0iw92p8581vi455w3zz5r77n0yad8svp6qi0aa6fqjz0c2blgww5")))

(define-public crate-stopwatch-0.0.4 (c (n "stopwatch") (v "0.0.4") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "0ggjn1m5zng79awbq0jsz5p724dm4wk4ry4nwc0pw0f1bn2prrnc")))

(define-public crate-stopwatch-0.0.5 (c (n "stopwatch") (v "0.0.5") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "169axf1dmqw25wwv1rqyg3zkryyv75avajryl724fghr8a6g86i4")))

(define-public crate-stopwatch-0.0.6 (c (n "stopwatch") (v "0.0.6") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0xz2bm5aicgwfvj2bdk461jiyi7a6fc3xzvj03cq38hsyvnqmskx")))

(define-public crate-stopwatch-0.0.7 (c (n "stopwatch") (v "0.0.7") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0a7p7gcznjpmqd6wpdfbgk6dirw34yydh6b38lx4v94dqzmva11x")))

