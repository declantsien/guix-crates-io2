(define-module (crates-io st op stoplight) #:use-module (crates-io))

(define-public crate-stoplight-0.1.0 (c (n "stoplight") (v "0.1.0") (h "1jwxy9w3ykq7wbd51jsp23md2c6yzhcx1wml80djmyrnyf79kygj")))

(define-public crate-stoplight-0.2.0 (c (n "stoplight") (v "0.2.0") (h "1il79rpmrvcvqm9rr46ar835i98hcqs11hwppcwxnsiriv3jwnqa")))

(define-public crate-stoplight-0.2.1 (c (n "stoplight") (v "0.2.1") (h "1ssnq7x5wi3ndf61aaij7im9yxhsaqdhkpv1yh6fwdw5f77wyx5v")))

(define-public crate-stoplight-0.2.2 (c (n "stoplight") (v "0.2.2") (h "1bx2l4c638g5y73zmzgcnkgbhf8cv53ja5dhkz6zs3gcaw5by9kj")))

(define-public crate-stoplight-0.3.3 (c (n "stoplight") (v "0.3.3") (h "15frngy0xhrmnai3ncwpvjbdkx5i8nkww5mkkri8fsc6ihlya3b7")))

