(define-module (crates-io st op stop-words) #:use-module (crates-io))

(define-public crate-stop-words-0.1.0 (c (n "stop-words") (v "0.1.0") (h "17h9mwpm9d1xn09s705rcpapwgjzc259hh30z3a2gzm3132i4a9z")))

(define-public crate-stop-words-0.1.1 (c (n "stop-words") (v "0.1.1") (h "007sll947aym0804dydgz9qv76b8xnyhqg0rkrdjjlp9i74l5x3l")))

(define-public crate-stop-words-0.1.2 (c (n "stop-words") (v "0.1.2") (h "1hvd46ihfmn5q01iglsz42ix7dgmakwkmjr34d5gz3rh8mb5zryk")))

(define-public crate-stop-words-0.1.3 (c (n "stop-words") (v "0.1.3") (h "10cnrx1pi00w6cy65p70m95v8llc3b1vnkhgnalq412xv4n2d01x")))

(define-public crate-stop-words-0.1.4 (c (n "stop-words") (v "0.1.4") (h "11zg8q8pycd8nxk864506a5m6hra7kmraihjz35z8nfrrjrsy6l5")))

(define-public crate-stop-words-0.1.5 (c (n "stop-words") (v "0.1.5") (h "12hmcplsh87z22raxn37zr5yal4ybqai2acn73hkxsf8rrm7rgrp")))

(define-public crate-stop-words-0.2.0 (c (n "stop-words") (v "0.2.0") (h "1v4l6kfd3l9lsmvv2j8d6msw71mlcg2jbqm51kps24x92fd7m5yq")))

(define-public crate-stop-words-0.2.1 (c (n "stop-words") (v "0.2.1") (h "0l09g11cxmc3iww5fvq738kcnkblzkqjw5lj2ia48wgkngkjiwv5")))

(define-public crate-stop-words-0.2.2 (c (n "stop-words") (v "0.2.2") (h "02y0r9jj3ixffnhqxb479hjjhj5w299a73yqx2xpicvfnbcm2fxl")))

(define-public crate-stop-words-0.3.0 (c (n "stop-words") (v "0.3.0") (d (list (d (n "strum") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1qcibs3hqqfn0yplz3xd6sbdjm3vsz2abm0d29im12k5c0v5p38p") (f (quote (("enum" "strum" "strum_macros") ("default"))))))

(define-public crate-stop-words-0.3.1 (c (n "stop-words") (v "0.3.1") (d (list (d (n "strum") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "19djxlhhigkdrf9fda8xgyg5ncqjd4nrv7797dnj6nlhvln2qlwh") (f (quote (("enum" "strum" "strum_macros") ("default"))))))

(define-public crate-stop-words-0.3.2 (c (n "stop-words") (v "0.3.2") (d (list (d (n "strum") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "0s6i64ajvaf5by1qsd44my5nx0p684n4hpqghhw3d2603v3il436") (f (quote (("enum" "strum" "strum_macros") ("default"))))))

(define-public crate-stop-words-0.4.0 (c (n "stop-words") (v "0.4.0") (d (list (d (n "serde_json") (r "^1.0.64") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "0v3dnjqzar5mfnkxwlv43yrc2h8v6hcxfvjiiyh3i2jd1hdpicj5") (f (quote (("nltk") ("default" "serde_json"))))))

(define-public crate-stop-words-0.5.0 (c (n "stop-words") (v "0.5.0") (d (list (d (n "serde_json") (r "^1.0.68") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "1jilyyn6jixwchrw1gv3kphc8c4r4y4qayqpbh5hn8q60bkkbss6") (f (quote (("nltk") ("default" "serde_json"))))))

(define-public crate-stop-words-0.6.0 (c (n "stop-words") (v "0.6.0") (d (list (d (n "serde_json") (r "^1.0.68") (o #t) (d #t) (k 0)))) (h "1rd5454g5sk165ph1dq1p4lvglnk2qna4w40rh0mb52mfp8ky93v") (f (quote (("nltk") ("default" "serde_json"))))))

(define-public crate-stop-words-0.6.1 (c (n "stop-words") (v "0.6.1") (d (list (d (n "human_regex") (r "^0.2.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (o #t) (d #t) (k 0)))) (h "1jgy3b7nlgc267vr229sbb0jzzgjd0wf9gz3370lq1rrra5w25a8") (f (quote (("nltk") ("default" "serde_json"))))))

(define-public crate-stop-words-0.6.2 (c (n "stop-words") (v "0.6.2") (d (list (d (n "human_regex") (r "^0.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (o #t) (d #t) (k 0)))) (h "0gnciksgq1ygdvzhgjgbmmnbbglw3vnif4x0y4qpw0ccdmbwp5bs") (f (quote (("nltk") ("default" "serde_json"))))))

(define-public crate-stop-words-0.7.0 (c (n "stop-words") (v "0.7.0") (d (list (d (n "human_regex") (r "^0.2.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)))) (h "13fqmh1sskiwfs6p7335vn0l66a4yj511niphimylw113vzwfldj") (f (quote (("nltk") ("iso" "serde_json") ("default" "iso"))))))

(define-public crate-stop-words-0.7.1 (c (n "stop-words") (v "0.7.1") (d (list (d (n "human_regex") (r "^0.3.0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0pp1mjvd0f4pb0d4pc8n2v1wjnv9pbslyhqnpj7qz3qi8rjizirh") (f (quote (("nltk") ("iso" "serde_json") ("default" "iso"))))))

(define-public crate-stop-words-0.7.2 (c (n "stop-words") (v "0.7.2") (d (list (d (n "human_regex") (r "^0.3.0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "1rcdhfwaym48vj45rjbpm96b35qi1qkrgdq3hkdq88kmn5ahfiql") (f (quote (("nltk") ("iso") ("default" "iso") ("constructed"))))))

(define-public crate-stop-words-0.8.0 (c (n "stop-words") (v "0.8.0") (d (list (d (n "human_regex") (r "^0.3.0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "1k6x53dknivagy8nigpj1lwclkrwxmxjniwqz75jxq4xh16h4045") (f (quote (("nltk") ("iso") ("default" "iso") ("constructed"))))))

