(define-module (crates-io st df stdf) #:use-module (crates-io))

(define-public crate-stdf-0.1.0 (c (n "stdf") (v "0.1.0") (d (list (d (n "byte") (r "^0.2") (d #t) (k 0)) (d (n "stdf-record-derive") (r "^0.1.0") (d #t) (k 0)))) (h "02z3s903y6yz09ajvl98jrq00yjvigy53crk8canilccrw8whc83")))

(define-public crate-stdf-0.1.1 (c (n "stdf") (v "0.1.1") (d (list (d (n "byte") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "stdf-record-derive") (r "^0.1.1") (d #t) (k 0)))) (h "06hb4hmww68ic0liw00akl0x5v9z1mpm4752d4fhm62gz5ndakpv")))

(define-public crate-stdf-0.1.2 (c (n "stdf") (v "0.1.2") (d (list (d (n "byte") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "stdf-record-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0asg66arn2gllmqz4p9adkd8x9ygzy7z8lfa6as5fkk12w725a9r")))

(define-public crate-stdf-0.2.0 (c (n "stdf") (v "0.2.0") (d (list (d (n "byte") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "stdf-record-derive") (r "^0.2") (d #t) (k 0)))) (h "1aiw8cpj1bj32pk153i6nk7y3ddwv4kbb6qf6gdxmjyp2y1fqiag")))

