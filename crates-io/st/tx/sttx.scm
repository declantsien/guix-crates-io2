(define-module (crates-io st tx sttx) #:use-module (crates-io))

(define-public crate-sttx-0.1.0 (c (n "sttx") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1br7cf8iaswcgmx8cscbb50gmfwbiq9kykrxkmilhcx3wb8w4l43")))

