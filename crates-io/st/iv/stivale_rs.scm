(define-module (crates-io st iv stivale_rs) #:use-module (crates-io))

(define-public crate-stivale_rs-0.1.0 (c (n "stivale_rs") (v "0.1.0") (h "05ql73qnqjb39wkz1vka7vs7byjrhz0sav1xbf8rvzk4c6kf69s7") (f (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.1.1 (c (n "stivale_rs") (v "0.1.1") (h "1i7q8xyacg0ffm1j1zcr7pza6ixqxccn2mf7ccnyd2kmbs278zy4") (f (quote (("v2") ("v1") ("default" "v2")))) (y #t)))

(define-public crate-stivale_rs-0.1.2 (c (n "stivale_rs") (v "0.1.2") (h "131v1q2bjz1gr36d721hd7gbhjbwi71pvr1azmj3awmh4i83rj08") (f (quote (("v2") ("v1") ("default" "v2")))) (y #t)))

(define-public crate-stivale_rs-0.1.3 (c (n "stivale_rs") (v "0.1.3") (h "18287pwzvw1np3pyi0z3vj74rg2bir9hzsdz6ra897r344az2pra") (f (quote (("v2") ("v1") ("default" "v2")))) (y #t)))

(define-public crate-stivale_rs-0.1.4 (c (n "stivale_rs") (v "0.1.4") (h "0v24ylxxrfjvvz8zc4q9qm1xqnf8lxszh3qm6l4rs52w7xs8pn35") (f (quote (("v2") ("v1") ("default" "v1"))))))

(define-public crate-stivale_rs-0.1.5 (c (n "stivale_rs") (v "0.1.5") (h "107gdqvfjqd85syi8pay1yllnmd3kizs1zx1z1g51d0q0zv8drv2") (f (quote (("v2") ("v1") ("default" "v1"))))))

(define-public crate-stivale_rs-0.1.6 (c (n "stivale_rs") (v "0.1.6") (h "06sqrcnd94rl58nhgq6r120ma4pjnvrakrda57wqj9smbwxbp49w") (f (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.1.7 (c (n "stivale_rs") (v "0.1.7") (h "12x85wx3bf7skw2mjzzczhwy2ph4whgfqdlv8fwxfxg4g85s2d4d") (f (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.1.8 (c (n "stivale_rs") (v "0.1.8") (h "0dgixvar8s6yj99ag2ygc4gbvzsnrp4iamvxf6314h0b8fdpj08q") (f (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.1.9 (c (n "stivale_rs") (v "0.1.9") (h "12jd65h757khb2hjq849bhqfi679nhz2yf07yp1l9jiar7xl40cw") (f (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.2.0 (c (n "stivale_rs") (v "0.2.0") (h "1lxphaadvfgqmri0rqry1sxzfa3g97nvy1pbg364iw4wjr06nga5") (f (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.2.1 (c (n "stivale_rs") (v "0.2.1") (h "1y0mk03i7mpsbk8kjyla673cabqclib9blb29hmz65pfq052mbqy") (f (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.2.2 (c (n "stivale_rs") (v "0.2.2") (h "15map27jpzmqr1ygfhx25ma2naig92az37z10ycnqpdhci6sab3j") (f (quote (("v2") ("v1") ("default" "v2"))))))

