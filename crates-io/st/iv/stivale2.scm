(define-module (crates-io st iv stivale2) #:use-module (crates-io))

(define-public crate-stivale2-0.1.0 (c (n "stivale2") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "1rxis9hihjblx5mindlyv6d9mcr7i0q7c32ih0zx96lrby0yk064") (y #t)))

(define-public crate-stivale2-0.1.1 (c (n "stivale2") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "1srym74sg47lbi6924ckr65al9fbfrndwvy0iz0wlijn464swci7")))

