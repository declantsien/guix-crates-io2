(define-module (crates-io st iv stivale) #:use-module (crates-io))

(define-public crate-stivale-0.1.0 (c (n "stivale") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "0s51xxm8hlf7hndwbzyrhhbax5i7a1dgw603zzlkn5i8wzzb2qi2")))

(define-public crate-stivale-0.2.0 (c (n "stivale") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "1q5676skc2zpc2nq30d9ld9s8lm668pw5fgjarr8qqnjsn40ws5d")))

(define-public crate-stivale-0.2.1 (c (n "stivale") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "0wp711zjdqv2q3k1h7nawrhcdd9ba311v9dq0ll00qq08j5ggi7c")))

(define-public crate-stivale-0.2.2 (c (n "stivale") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "07yjsqbs56kz37zam9vc83f8cklrdkg8kqpr55jvac3pxivnr1s2")))

