(define-module (crates-io st iv stivale-boot) #:use-module (crates-io))

(define-public crate-stivale-boot-0.2.1 (c (n "stivale-boot") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "1lq4m0r46wzfqr1r02cy4dgzrg3n03zhclrixkx0kqd02xj8nczn")))

(define-public crate-stivale-boot-0.2.2 (c (n "stivale-boot") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "1f6pfrrbymi27p20d2ww1qcpn1pbnkfwi2a3q4lpxridm0svnsm8")))

(define-public crate-stivale-boot-0.2.3 (c (n "stivale-boot") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "1ifaqyiq39nqk8di3b5ziz6w0kkwnfwim02mfp07qx40akawjd0g")))

(define-public crate-stivale-boot-0.2.4 (c (n "stivale-boot") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "187mlgxyd38g31vi98xbnb7rys9inzfy77bfh122m55ljkbiak7q")))

(define-public crate-stivale-boot-0.2.5 (c (n "stivale-boot") (v "0.2.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "1ph3igvyc4x91nh174za4x1b71xb2x6jg33n93wgbx9crliqzqvy")))

(define-public crate-stivale-boot-0.2.6 (c (n "stivale-boot") (v "0.2.6") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0vc3a37nl22bh5y4d4a38jd0zg25iv7gygd1dh91acbbn2gcz11r")))

(define-public crate-stivale-boot-0.3.0 (c (n "stivale-boot") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "1shc937vlyly590fv4mm3wklg3n5jd3ribirrv07yhi0d0zwwp9g")))

(define-public crate-stivale-boot-0.3.1 (c (n "stivale-boot") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "stivale-proc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (o #t) (k 0)))) (h "1a15rwk5g9vnwf12lqxaa6xjyi997j13b3mq0n6dlyqn6glqprxb") (f (quote (("helper-macros" "stivale-proc") ("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

