(define-module (crates-io st oc stockbook) #:use-module (crates-io))

(define-public crate-stockbook-0.1.0 (c (n "stockbook") (v "0.1.0") (d (list (d (n "stockbook-stamp-macro") (r "=0.1.0") (d #t) (k 0)))) (h "0vxa3z1hfsc4h4gph1a29q3azldpzp05ly1b42h4k9kqbqar7m95")))

(define-public crate-stockbook-0.1.1 (c (n "stockbook") (v "0.1.1") (d (list (d (n "stockbook-stamp-macro") (r "=0.1.1") (d #t) (k 0)))) (h "1z8m19z5519pm3n2hnryia5760j56mlls6yy5032c2fpgmc93xr7")))

(define-public crate-stockbook-0.2.0 (c (n "stockbook") (v "0.2.0") (d (list (d (n "stockbook-stamp-macro") (r "=0.2.0") (d #t) (k 0)))) (h "0x6579gq8n7if3078hc7zd9b8yrmsc2ijqwiiwfh493g95ipflp3")))

(define-public crate-stockbook-0.3.0 (c (n "stockbook") (v "0.3.0") (d (list (d (n "avr-progmem") (r ">=0.2.0, <0.4.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "stockbook-stamp-macro") (r "=0.3.0") (d #t) (k 0)))) (h "0mhcmxvii4493hf8w991pjxbndf5xqn7wrfb3g5ph24ry5fa00fp") (f (quote (("progmem" "avr-progmem" "stockbook-stamp-macro/progmem"))))))

