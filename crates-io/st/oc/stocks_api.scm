(define-module (crates-io st oc stocks_api) #:use-module (crates-io))

(define-public crate-stocks_api-0.1.0 (c (n "stocks_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lramjq34ghp58yn4kgcl3ymdla60g5xid262y13fjy32vmlz6wp")))

(define-public crate-stocks_api-0.1.1 (c (n "stocks_api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vrgy4z9vk827n2ap1x887qcl1n3xgmvhjxid7jnkyhgrsqxpdn8")))

(define-public crate-stocks_api-0.1.2 (c (n "stocks_api") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03s3znm8hj8qjvrfaaabwl4ljcfhl3zlm6xl4m8lj8w32xrxljpi")))

(define-public crate-stocks_api-0.1.3 (c (n "stocks_api") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02z1n8jm241xhk2rw6l8a7phw0mf5b8kp6mav6b15b8yqvzwyp28")))

(define-public crate-stocks_api-0.1.4 (c (n "stocks_api") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17wc7x63wycyjw0niv3a11gbyl0gmc12yp59fgij30sv6p1hcb1x")))

(define-public crate-stocks_api-0.1.5 (c (n "stocks_api") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gs5gdy1qndkkfw56qms3l1r52gxgb09sd0bxmcdwzs5fvnf6znh")))

(define-public crate-stocks_api-0.1.6 (c (n "stocks_api") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xrgjxp4vjr0cz5srkqaapmsljg339lznp9w2wlj2r91z3hlgnva")))

