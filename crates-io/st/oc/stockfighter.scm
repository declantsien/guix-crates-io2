(define-module (crates-io st oc stockfighter) #:use-module (crates-io))

(define-public crate-stockfighter-0.1.0 (c (n "stockfighter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.2") (d #t) (k 0)) (d (n "serializable_enum") (r "^0.3.0") (d #t) (k 0)) (d (n "websocket") (r "^0.16.2") (d #t) (k 0)))) (h "1jljgqwss1sd6mwhaqfba6ksdgd9qvsrmi76b88zzvg6nnddgj8x")))

(define-public crate-stockfighter-0.1.1 (c (n "stockfighter") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.2") (d #t) (k 0)) (d (n "serializable_enum") (r "^0.3.0") (d #t) (k 0)) (d (n "websocket") (r "^0.16.2") (d #t) (k 0)))) (h "1zn9miqzd6j3j7mi82npqflhryfij2vg4kgbx3hyd27cmfrkmcal")))

(define-public crate-stockfighter-0.2.0 (c (n "stockfighter") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.2") (d #t) (k 0)) (d (n "serializable_enum") (r "^0.3.0") (d #t) (k 0)))) (h "1b1jma18jgksilgil7j4yirwp453clpm7y96sgwgb0m83hkj7np1") (y #t)))

(define-public crate-stockfighter-0.2.1 (c (n "stockfighter") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "hyper") (r "^0.9.8") (d #t) (k 0)) (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.10") (d #t) (k 0)) (d (n "serializable_enum") (r "^0.3.0") (d #t) (k 0)))) (h "02q7n4napqm0mxf66gis3779k5mmvjk7gji5h5f61rizb2kr7zzd")))

