(define-module (crates-io st oc stockbook-stamp-macro) #:use-module (crates-io))

(define-public crate-stockbook-stamp-macro-0.1.0 (c (n "stockbook-stamp-macro") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "180lv7ykzkvl61q5w18sqpzd1k65bzrjxi0cwis1469c2l3jn228")))

(define-public crate-stockbook-stamp-macro-0.1.1 (c (n "stockbook-stamp-macro") (v "0.1.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a8sbx7vjcsr9jj4z9cp86fvq45fs4ia6rc69dd6wxf9hzjx5960")))

(define-public crate-stockbook-stamp-macro-0.2.0 (c (n "stockbook-stamp-macro") (v "0.2.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "001a52amhg1ah3y4vh22i4wk5agzxh0hk9w8xq326h2fmn2ax693")))

(define-public crate-stockbook-stamp-macro-0.3.0 (c (n "stockbook-stamp-macro") (v "0.3.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1k6ipphxxq3dx27vdwfg1665w1d0wgqnp0zclbc9ci6y1a9n0a6p") (f (quote (("progmem"))))))

