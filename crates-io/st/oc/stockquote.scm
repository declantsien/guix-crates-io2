(define-module (crates-io st oc stockquote) #:use-module (crates-io))

(define-public crate-stockquote-0.1.0 (c (n "stockquote") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "05pcb65xkix9l455isrkl4yabn2p8wiv18k1zrh2n4zp7lghcq37")))

(define-public crate-stockquote-0.1.1 (c (n "stockquote") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "0815ii84x2xix1sj8vbmm2hz9kmbhb0p642mmb2afbrwyyn7qw41")))

(define-public crate-stockquote-0.1.2 (c (n "stockquote") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "08ayr3gj9hibaigxbf973cvjvwy9kfd42r3n5xgiy79cic82gnim")))

