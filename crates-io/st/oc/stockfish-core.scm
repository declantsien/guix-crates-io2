(define-module (crates-io st oc stockfish-core) #:use-module (crates-io))

(define-public crate-stockfish-core-0.1.0 (c (n "stockfish-core") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("extern_crate_alloc" "min_const_generics"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1dz4iqwcxd7nw9jryf9rimhsk5f16xcph969psx8npbnj0zr207f") (f (quote (("unaccelerated") ("default"))))))

(define-public crate-stockfish-core-0.1.1 (c (n "stockfish-core") (v "0.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("extern_crate_alloc" "min_const_generics"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1clxsm5pg3gpgh1pwax7xn7l2hizd6s691kj7vvlxs687ip4kivr") (f (quote (("unaccelerated") ("default"))))))

(define-public crate-stockfish-core-0.2.0 (c (n "stockfish-core") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("extern_crate_alloc" "min_const_generics"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0gdq8yd6bdr8gavpiw6q726335rmqynvqc643ggrjzfsqilcinff") (f (quote (("unaccelerated") ("default"))))))

