(define-module (crates-io st oc stockfighter-api) #:use-module (crates-io))

(define-public crate-stockfighter-api-0.1.0 (c (n "stockfighter-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.17") (d #t) (k 0)) (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)))) (h "0izqqmca9jkkn2a8dxg3svrvdvj2sg3ny2mlc2p6xw0fcxxvlbw4")))

