(define-module (crates-io st oc stochastic-queue) #:use-module (crates-io))

(define-public crate-stochastic-queue-0.1.0 (c (n "stochastic-queue") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1p1dnqcd7qqma0kbfldhs6m100c8cpdpp874kd0pg1m6z7lf3f5q")))

(define-public crate-stochastic-queue-0.1.1 (c (n "stochastic-queue") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "097r1x02y6fknh86varz6zhf2lih8x8bmh13fbhhkv09ax6kz2qi")))

(define-public crate-stochastic-queue-0.2.0 (c (n "stochastic-queue") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0c0k4m5v0ccy69hskpjpgsrmhq29xsgpj5218d9my6bd2bf9vblf")))

