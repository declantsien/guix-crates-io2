(define-module (crates-io st oc stockton-bsp) #:use-module (crates-io))

(define-public crate-stockton-bsp-1.0.0 (c (n "stockton-bsp") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1gas4ns2icqfillc72dk555fk6d7lid55xfjmihb71jqskssxj0d")))

(define-public crate-stockton-bsp-2.0.0 (c (n "stockton-bsp") (v "2.0.0") (d (list (d (n "bit-vec") (r "^0.5.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18.0") (d #t) (k 0)))) (h "09jgrqngh8jjp6ddj9r804ghphhzibmyv021ad2wgsk8knxg9s53")))

(define-public crate-stockton-bsp-3.0.0 (c (n "stockton-bsp") (v "3.0.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.20") (d #t) (k 0)))) (h "1922mp027nwqjv2v1crg1h5766bvl5g958alxligvi7k3bfkvd0y")))

(define-public crate-stockton-bsp-3.1.0 (c (n "stockton-bsp") (v "3.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.20") (d #t) (k 0)))) (h "17yg19gd5lvc6ldlicyl6ph1p1a9qsycdpl6v1cyk3va6cpskaiy")))

