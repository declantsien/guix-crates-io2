(define-module (crates-io st oc stochastic-processes) #:use-module (crates-io))

(define-public crate-stochastic-processes-0.1.0 (c (n "stochastic-processes") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)))) (h "01p42lsw5pzlxq2ih6qp14q2fczyk8ayflbpmsd9pd6k9gm6mz4s")))

(define-public crate-stochastic-processes-0.1.1 (c (n "stochastic-processes") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)))) (h "1adn65f0pnqahxrhhc9hk384rhbi31n236vbv9cwzmm78j3ai5b5")))

(define-public crate-stochastic-processes-0.1.2 (c (n "stochastic-processes") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.31") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-pickle") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)))) (h "198n78rxk9dl9ayplkryaw8a40qp31swqvy14dmd5icxpbks16mh") (s 2) (e (quote (("py" "dep:serde" "dep:serde-pickle") ("json" "dep:serde" "dep:serde_json"))))))

