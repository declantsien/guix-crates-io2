(define-module (crates-io st oc stochastic) #:use-module (crates-io))

(define-public crate-stochastic-0.0.1 (c (n "stochastic") (v "0.0.1") (h "0h69ixgcczar8pi0la3ww1ad4mqkf56b78d3mj5ddhh0qgas7x8c")))

(define-public crate-stochastic-0.2.0 (c (n "stochastic") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "complex") (r "*") (d #t) (k 0)) (d (n "czt") (r "*") (d #t) (k 0)) (d (n "probability") (r "*") (d #t) (k 0)))) (h "03bszkyigb0lv5c5flkzd12qz2r046f8b9ahh85a7504cjxvph1n")))

(define-public crate-stochastic-0.3.0 (c (n "stochastic") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "complex") (r "*") (d #t) (k 0)) (d (n "czt") (r "*") (d #t) (k 0)) (d (n "probability") (r "*") (d #t) (k 0)))) (h "0nknmmaiv5y7338wk1fvkqj5aw9mmx4r2crqbpxcj1h609hf8406")))

(define-public crate-stochastic-0.3.1 (c (n "stochastic") (v "0.3.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "complex") (r "*") (d #t) (k 0)) (d (n "czt") (r "*") (d #t) (k 0)) (d (n "probability") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 0)))) (h "0ff49lkx74p0kgf3mbw7ixyp37qms34b0kfgw1s2r21p9nddk65b")))

(define-public crate-stochastic-0.3.2 (c (n "stochastic") (v "0.3.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "complex") (r "*") (d #t) (k 0)) (d (n "czt") (r "*") (d #t) (k 0)) (d (n "probability") (r "^0.12") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 0)))) (h "12944cqcgq42klcf3wi61q3a7yg2lc4k92glms9h87ah0lf6djva")))

(define-public crate-stochastic-0.3.3 (c (n "stochastic") (v "0.3.3") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "complex") (r "*") (d #t) (k 0)) (d (n "czt") (r "*") (d #t) (k 0)) (d (n "probability") (r "^0.12") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 0)))) (h "15acvbdh2mjz7676s3ww3g4mm95frxffn6ivj3j6p5jjqhzs3y86")))

(define-public crate-stochastic-0.3.4 (c (n "stochastic") (v "0.3.4") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "complex") (r "^0.7") (d #t) (k 0)) (d (n "czt") (r "^0.1") (d #t) (k 0)) (d (n "probability") (r "^0.12") (d #t) (k 0)) (d (n "random") (r "^0.9") (d #t) (k 0)))) (h "04596z5k9r38xqkq34p988c5d3crf8bilgp391vrr2k9ygrcgjwv")))

(define-public crate-stochastic-0.4.0 (c (n "stochastic") (v "0.4.0") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "czt") (r "^0.2") (d #t) (k 0)) (d (n "probability") (r "^0.12") (d #t) (k 0)) (d (n "random") (r "^0.9") (d #t) (k 0)))) (h "04a2dn8izl748gi3ciksahdmvrgvhwhy7fcpbh7q353w6n5fc05q")))

(define-public crate-stochastic-0.4.1 (c (n "stochastic") (v "0.4.1") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "czt") (r "^0.2") (d #t) (k 0)) (d (n "probability") (r "^0.14") (d #t) (k 0)))) (h "0zs4r4j6bncx7y5pl4gicq7mq694dbg4c1ypwmh0xhlpkzgkllpy")))

(define-public crate-stochastic-0.5.0 (c (n "stochastic") (v "0.5.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "czt") (r "^0.2") (d #t) (k 0)) (d (n "probability") (r "^0.14") (d #t) (k 0)))) (h "18g1vr6vizj4xdsk9ah1mh7ld1m74s1gd7lhfb40lb2m6xqhcf11")))

(define-public crate-stochastic-0.5.1 (c (n "stochastic") (v "0.5.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "czt") (r "^0.2") (d #t) (k 0)) (d (n "probability") (r "^0.15") (d #t) (k 0)))) (h "1j9349537v23qyxzy97jbnp1spk98ai7gaciyzhqzfzk311xgn9w")))

(define-public crate-stochastic-0.5.2 (c (n "stochastic") (v "0.5.2") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "czt") (r "^0.3") (d #t) (k 0)) (d (n "probability") (r "^0.15") (d #t) (k 0)))) (h "1p7mhwrbpm70kp6m5vnc5ydjp5c60q8mhgqnsq694d5md95f015l")))

