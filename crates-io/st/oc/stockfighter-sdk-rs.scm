(define-module (crates-io st oc stockfighter-sdk-rs) #:use-module (crates-io))

(define-public crate-stockfighter-sdk-rs-0.0.1 (c (n "stockfighter-sdk-rs") (v "0.0.1") (h "0xlwi764xdrjbfqnbmmd0sqp6b1rg3wa6jnaqsqcl7iczli506lx")))

(define-public crate-stockfighter-sdk-rs-0.0.2 (c (n "stockfighter-sdk-rs") (v "0.0.2") (d (list (d (n "hyper") (r "~0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "17nhn3bbn871842axxq8v6lzyx3p7zph24wwkzg23zyv01cw1gbm")))

(define-public crate-stockfighter-sdk-rs-0.1.0 (c (n "stockfighter-sdk-rs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.4") (d #t) (k 0)) (d (n "hyper") (r "~0.7") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "websocket") (r "^0.15.2") (d #t) (k 0)))) (h "1y1myrxl2jn0azsbk7xbx938asbyrmgll6dipnw6wzigs1afdzah")))

