(define-module (crates-io st oc stock-scrape) #:use-module (crates-io))

(define-public crate-stock-scrape-0.1.0 (c (n "stock-scrape") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a7czklm79rz23xb9bl52i1b1ms1n1rm9agfch90z5pbacdpqgg3")))

