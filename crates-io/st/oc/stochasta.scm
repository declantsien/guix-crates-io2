(define-module (crates-io st oc stochasta) #:use-module (crates-io))

(define-public crate-stochasta-0.2.1 (c (n "stochasta") (v "0.2.1") (d (list (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1895vxnk4zi0a0rmj2w3hsci00ysn7l8m4k6cy8l1jdczffdyj7f")))

(define-public crate-stochasta-0.2.2 (c (n "stochasta") (v "0.2.2") (d (list (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "075kndxkzi93irnh92vjf211hfgcw6m3m5pl2pn54s2fm2a7615v")))

(define-public crate-stochasta-0.3.0 (c (n "stochasta") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wza0ydd88m5xpr7jarx40k8fy9hdvl3k1fng8dcz209yblbw3i3")))

(define-public crate-stochasta-0.4.0 (c (n "stochasta") (v "0.4.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0laqkq2zjsx0570imjkwim9lpwmfqp2y1ivmw0hz4fjmm2fa1qqz")))

(define-public crate-stochasta-0.4.2 (c (n "stochasta") (v "0.4.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jhjd96mnx14jlf2061chj2x599axhxx07s9dzxnjzjnfbsrpydc")))

(define-public crate-stochasta-0.5.0 (c (n "stochasta") (v "0.5.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "051qpvkz1vcv84746ga7bk6s0h2z8f06fnmjpvigqa9n2ynklmaw")))

(define-public crate-stochasta-0.6.0 (c (n "stochasta") (v "0.6.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14p8bz9jgg61z7rc66xkha64lf1q7xglhqhh5in9cmh8c0cxby42")))

(define-public crate-stochasta-0.6.1 (c (n "stochasta") (v "0.6.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0a795lmdq4p4spwl6xfdzw6z7j9bxmi6c12yfv02c2xc1dxqwxy4")))

(define-public crate-stochasta-0.6.2 (c (n "stochasta") (v "0.6.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0yx0mfy7cp14qqagjqhivj667mllwxgnwv0077l6ncbnih1wwj34")))

(define-public crate-stochasta-0.7.0 (c (n "stochasta") (v "0.7.0") (d (list (d (n "impls") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zixrf5m882yq1zjsmhv3p7fq9j35l1a8klwripx3b0awgma6d0f")))

(define-public crate-stochasta-0.7.1 (c (n "stochasta") (v "0.7.1") (d (list (d (n "impls") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xp6vq5fs6l6913sldha4c65idm6m422h1vwal89cc2706cbrf6n")))

(define-public crate-stochasta-0.7.2 (c (n "stochasta") (v "0.7.2") (d (list (d (n "impls") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qk4fn4gqna6s2whpjx7wlai0hsckiq6qink69wcgnvpbc60isgf")))

(define-public crate-stochasta-0.8.0 (c (n "stochasta") (v "0.8.0") (d (list (d (n "enumset") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "impls") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "033yyq90i3ygfkh1jfxihw6n2h3532a30g05rv9897fcqyd420xw") (f (quote (("playing_cards") ("default"))))))

(define-public crate-stochasta-0.8.1 (c (n "stochasta") (v "0.8.1") (d (list (d (n "enumset") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "impls") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07lajvav35bm145hrmgpv57lkl82z08x3d8xl08kfxz1qaw2xcb0") (f (quote (("playing_cards") ("default"))))))

(define-public crate-stochasta-0.8.2 (c (n "stochasta") (v "0.8.2") (d (list (d (n "enumset") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "impls") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vg7v7y36aji8migfdp0l4l2ik0d2qg9ml0c94gix8mx0qmqavjl") (f (quote (("playing_cards") ("default"))))))

(define-public crate-stochasta-0.8.3 (c (n "stochasta") (v "0.8.3") (d (list (d (n "enumset") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "impls") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.13") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11w7rh0kqfmry9l0kqpvymfp7n1g8wspa11zs8vzjjr1r7aj99r8") (f (quote (("playing_cards") ("default"))))))

