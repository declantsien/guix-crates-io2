(define-module (crates-io st em stem-cell) #:use-module (crates-io))

(define-public crate-stem-cell-0.0.0 (c (n "stem-cell") (v "0.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0vyjr91x53cgagxkqq83nwm1rrfz4gjaks6dlbivjqsidg43q1bw")))

(define-public crate-stem-cell-0.0.1 (c (n "stem-cell") (v "0.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "198vhcc9dc293vhkzqfv4kxba8bq1m3ck68z3qpjbsass8pnchw5")))

(define-public crate-stem-cell-0.0.2 (c (n "stem-cell") (v "0.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0zkcwq52v01hsqspyzs7b05adz7kgjbja383dw6jrca3mh6i3vcx")))

(define-public crate-stem-cell-0.0.3 (c (n "stem-cell") (v "0.0.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1hxc3n273ahy9d33k3bvakf4dq6sjyqlgjh3f54m28aaj21m7a1z")))

(define-public crate-stem-cell-0.0.4 (c (n "stem-cell") (v "0.0.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1n9yi28kzf7hm9d9damlfjh3s0c9blqrvwvkp24fr59zyy9bvd6v")))

(define-public crate-stem-cell-0.0.5 (c (n "stem-cell") (v "0.0.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "058gms753bcx75w2hypp9dns84dsy1s88w5px2sv9dc5lfivzvz9")))

(define-public crate-stem-cell-0.0.6 (c (n "stem-cell") (v "0.0.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1z2ps1qn5zbpg8px3p9m5xagcql1if4lzlc7hx4jxbpdh3mlnkmw")))

(define-public crate-stem-cell-0.0.7 (c (n "stem-cell") (v "0.0.7") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "05pn7nf03ij9cgdx02w70h6h2ki3v5v3z8f8qgx8h8q4l0lzfr36")))

