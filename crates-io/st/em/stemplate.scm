(define-module (crates-io st em stemplate) #:use-module (crates-io))

(define-public crate-stemplate-0.1.0 (c (n "stemplate") (v "0.1.0") (h "033k4s0vf14kh07dd24009r6spdz321fswh3xk94x3rr7dv642vm")))

(define-public crate-stemplate-0.1.1 (c (n "stemplate") (v "0.1.1") (h "06xv7b66r4il4div20l4xs0y6axwv8sg29m3dhm90bl1c1qggl95")))

(define-public crate-stemplate-0.1.2 (c (n "stemplate") (v "0.1.2") (h "0rj1wjxxgpr7vnrp6fjyk452kqrjzk6fp0ry5fi7cw2hjpdznbhq")))

(define-public crate-stemplate-0.1.3 (c (n "stemplate") (v "0.1.3") (h "0ysym7d568vwh8alajikdnjpc3bs317b9cw1hl0754xky4gz6vlb")))

(define-public crate-stemplate-0.1.4 (c (n "stemplate") (v "0.1.4") (h "0pv6pf6b8idx1cgvks1lq2c9dr0imjq8jfxiywdckbv1pmn5p28j")))

(define-public crate-stemplate-0.1.5 (c (n "stemplate") (v "0.1.5") (h "0nrma9fmv6j0njv742n8jacnr5kdx1ik0l4pyiiblgrdid5y44n6")))

(define-public crate-stemplate-0.1.6 (c (n "stemplate") (v "0.1.6") (h "0vdfag1vvydl6hwg6bch0xd5jd7wfjsp6x9smpld2ccg5fx36c2h")))

(define-public crate-stemplate-0.1.7 (c (n "stemplate") (v "0.1.7") (h "0hr4n8vg79gnzcb74nx8jps900snc5qipn0hn9yhhygiszk70rlm")))

(define-public crate-stemplate-0.1.8 (c (n "stemplate") (v "0.1.8") (h "1mm7rb1bib8s42lbsm0pd72haipm1p5jns9zsh7krkw4289ij3s6")))

(define-public crate-stemplate-0.1.9 (c (n "stemplate") (v "0.1.9") (h "0z5jif89sij8723rh7zw93hxlbc4ci41f60izrmsdds5x7pa10yi")))

(define-public crate-stemplate-0.1.10 (c (n "stemplate") (v "0.1.10") (h "0fr27fp75qlqrhpgdjl560p0havn3gx41l3m75lzw22d6w3p9rgf")))

(define-public crate-stemplate-0.1.11 (c (n "stemplate") (v "0.1.11") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1zr107qrzaxhmhdak2r57nzdajd2c08l8z3y6nayvli85rrfm8cf")))

(define-public crate-stemplate-0.1.12 (c (n "stemplate") (v "0.1.12") (h "0rhkwgwi13lk2j2ii8837wiyniylkyk5hx2vg4hap35gq456fvkq")))

