(define-module (crates-io st em stemmer) #:use-module (crates-io))

(define-public crate-stemmer-0.1.0 (c (n "stemmer") (v "0.1.0") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0y2al1lib4dpwk5j9nc1c41ywkv7xn84jpy81rvs8kxa429shk72")))

(define-public crate-stemmer-0.1.1 (c (n "stemmer") (v "0.1.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1kr6g432f6l67hc68gfppnl6awgqlgpl57hnn2mkyq3pi8dmvb5x")))

(define-public crate-stemmer-0.2.0 (c (n "stemmer") (v "0.2.0") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0vpfgvbq8nabwz3v773n3ji8hi81ym6hib68kkvvxvyy1i417ir6")))

(define-public crate-stemmer-0.3.0 (c (n "stemmer") (v "0.3.0") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0flbl8qz6knnf89r3pms340ivrzb4jq1hf0sbvl3ymi5y5s5y09p")))

(define-public crate-stemmer-0.3.1 (c (n "stemmer") (v "0.3.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jqbm31j9bpijpjxqmf2k9dlrqgg02976d9qwagrl3rc8zqyixb8")))

(define-public crate-stemmer-0.3.2 (c (n "stemmer") (v "0.3.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wa67jh2wj6lky6f7hxmzm0wpq2q9wzs423j2c3z8bs71d8m3j4b")))

