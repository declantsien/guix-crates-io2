(define-module (crates-io st em stemflow) #:use-module (crates-io))

(define-public crate-stemflow-0.5.2 (c (n "stemflow") (v "0.5.2") (h "14j3nmyh79zi2884hl427w24kl0d3k0gkj8jfp4lzzb6j0rl39gy")))

(define-public crate-stemflow-0.5.3 (c (n "stemflow") (v "0.5.3") (d (list (d (n "dot") (r "^0.1") (d #t) (k 0)))) (h "1zz3ddn1fm76qij507kli9b0r2skbzyswkfacv6yr3pynrqknvbn")))

