(define-module (crates-io st em stemma_soil_sensor) #:use-module (crates-io))

(define-public crate-stemma_soil_sensor-0.1.0 (c (n "stemma_soil_sensor") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1vc157w6ja62q2s948q4bnhjn49kc5zxaxj74ii435marc3yg5xa")))

(define-public crate-stemma_soil_sensor-0.1.1 (c (n "stemma_soil_sensor") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "02s3rn990hydjn954f5lqv2bg2kvvj35rrabfj2650n12v8i5dwg")))

(define-public crate-stemma_soil_sensor-0.1.2 (c (n "stemma_soil_sensor") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1p8c857vj0f62qxx8s0r1ypjrindhaxv0kj51ldxryb0gpp7rsbb")))

(define-public crate-stemma_soil_sensor-0.1.3 (c (n "stemma_soil_sensor") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1zm9y4jmnalzws07m7bfdnskfmri9k27cc3x7mgjdyhdjq429yp9")))

(define-public crate-stemma_soil_sensor-0.1.4 (c (n "stemma_soil_sensor") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0im0z759k1f92p3qk34fr70d3q9w4z2155sq36ghcqpda7whcwfb")))

(define-public crate-stemma_soil_sensor-0.1.5 (c (n "stemma_soil_sensor") (v "0.1.5") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "199clyicj98y3c1mmwzilhi7j0cibrpxaf8cz3dy7nvqg0iwqz6z")))

