(define-module (crates-io st ar starfish-api) #:use-module (crates-io))

(define-public crate-starfish-api-0.1.0 (c (n "starfish-api") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "figment") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "sea-orm-rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "starfish-core") (r "^0.1.0") (d #t) (k 0)))) (h "19rc9yy957yvqngd107bmvvx7x1s4yg1dfpk8wmalnpfc1ibd5c8")))

(define-public crate-starfish-api-0.1.1 (c (n "starfish-api") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "figment") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "sea-orm-rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "starfish-core") (r "^0.1.0") (d #t) (k 0)))) (h "0r8xwfrgvvprnf5kxwwr3aaar9bapminjvb0wfj0sjjji8wynzsn")))

