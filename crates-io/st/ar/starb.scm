(define-module (crates-io st ar starb) #:use-module (crates-io))

(define-public crate-starb-0.1.0 (c (n "starb") (v "0.1.0") (h "0b7v6b1aj6cfbc2lgx7vnxh2n3cmaykwhi71my9adwii20q9zpin")))

(define-public crate-starb-0.1.1 (c (n "starb") (v "0.1.1") (h "0pm3agkw6f5qxvf1d6ssk7rlfzicdaawxa3rcfs8s6512931z7qg")))

(define-public crate-starb-0.2.0 (c (n "starb") (v "0.2.0") (h "0hyd6kmv714lcv0zpdfaxihx2i1r40532w7jpvqavyxn2mhrgn5n")))

(define-public crate-starb-0.3.0 (c (n "starb") (v "0.3.0") (h "1pc02hln4bpw93hfqz7fl6kbvmm9x0yaywzbj88x1ljvf47gpnwj")))

(define-public crate-starb-0.3.1 (c (n "starb") (v "0.3.1") (h "05i7p867bjbwl3y86ly3q17mg4lziixvcq8v7iqyjv578rg5llr8")))

