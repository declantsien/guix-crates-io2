(define-module (crates-io st ar starry-night) #:use-module (crates-io))

(define-public crate-starry-night-0.0.0 (c (n "starry-night") (v "0.0.0") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1hn32y1gd8pdpn6p03xk3abwi5jmkwy7yy6038xr180isgbiy1sa")))

(define-public crate-starry-night-0.0.1 (c (n "starry-night") (v "0.0.1") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)))) (h "1yhpxkrjpfkwa7xd8qzjh5350zvh0df7qdbwj20f5hb6vjn9b7py")))

(define-public crate-starry-night-0.0.2 (c (n "starry-night") (v "0.0.2") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)))) (h "1vkl6xmp5yjmp30xpl1l9jbzvvrj57f7mji769v03199i2h9i7sr")))

