(define-module (crates-io st ar starlane-macros) #:use-module (crates-io))

(define-public crate-starlane-macros-0.1.0 (c (n "starlane-macros") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold" "parsing"))) (d #t) (k 0)))) (h "08r2q0knvh96knpx6pk9g65g08qc9siii25hxm2h6jck0ndj5f1f")))

