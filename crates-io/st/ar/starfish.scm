(define-module (crates-io st ar starfish) #:use-module (crates-io))

(define-public crate-starfish-1.0.0 (c (n "starfish") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w596h35ql4f6yl8y90pngc8j9z3i57q3dv6lb385qk80pjkk9im")))

(define-public crate-starfish-1.0.1 (c (n "starfish") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "196axnnpnsxfqgyipzqppxb16ws7df0rdxqzz2bvfw4qhhrwd32k")))

(define-public crate-starfish-1.0.2 (c (n "starfish") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1mvxdzxzvsf30cmag3ksfkdkqwks1sn1fy4bcb7hibwf3ywsn8yj")))

(define-public crate-starfish-1.1.0 (c (n "starfish") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zvy1sshjvbdra5anhwf13wc176mni9gl3j2rlqfspcrl81v2w7s")))

(define-public crate-starfish-1.2.0 (c (n "starfish") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1f5kjby7ar5jfwq1076wkyj4xyj8cy2rnfzkrq3cywxmv2dy8pf5")))

(define-public crate-starfish-1.2.1 (c (n "starfish") (v "1.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1v6cij4bk5lyyrwf34g4h2336034xhgr482csgv9hqkq192s65j6")))

(define-public crate-starfish-1.2.2 (c (n "starfish") (v "1.2.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gdjyv7ncd821qc2rbimj62j4r5w5y2gxagdxqxn8jhf8y0k3dzh")))

