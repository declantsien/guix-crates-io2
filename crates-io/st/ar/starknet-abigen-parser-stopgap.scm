(define-module (crates-io st ar starknet-abigen-parser-stopgap) #:use-module (crates-io))

(define-public crate-starknet-abigen-parser-stopgap-0.1.0 (c (n "starknet-abigen-parser-stopgap") (v "0.1.0") (d (list (d (n "starknet") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ra0dfccm0xqbf6s93pkc8b8b7hvq1b3lfj6k94kran9x727ccyk")))

