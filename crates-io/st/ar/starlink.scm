(define-module (crates-io st ar starlink) #:use-module (crates-io))

(define-public crate-starlink-0.1.0 (c (n "starlink") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 2)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.4") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (d #t) (k 1)))) (h "00dxsqbrzk0yndrgkz8jnp8p6i9sbb9paqq6f8gs0k31l7cwf4s4")))

(define-public crate-starlink-0.1.1 (c (n "starlink") (v "0.1.1") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 2)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.4") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (d #t) (k 1)))) (h "0v2lqn1npcwrn5kb16pydb7fjwfjzjm5f6kkkjsj7300alwdkb4a")))

(define-public crate-starlink-0.2.0 (c (n "starlink") (v "0.2.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 2)) (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.5") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5") (d #t) (k 1)))) (h "11g8n2z79wvrsb10z63jmh6wnsr83qw2kl2bi00qsghwdwzkp174")))

(define-public crate-starlink-0.3.0 (c (n "starlink") (v "0.3.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 2)) (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.5") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5") (d #t) (k 1)))) (h "1rc0ib8ys3dz3midv0nlad1bjqcvzrpm8131rsza8smw4rfphwdf")))

(define-public crate-starlink-0.3.1 (c (n "starlink") (v "0.3.1") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 2)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "00jnf0yhgras01qyzgy3lgcd0k7jsxv40hrdyjf7jk6xdffd785h")))

(define-public crate-starlink-1.0.0 (c (n "starlink") (v "1.0.0") (d (list (d (n "async-stream") (r "^0.3.5") (d #t) (k 2)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "0m538kyyzc476877ds05g7g4xzwgdwjwz0i4072fqpid34blnmw2")))

