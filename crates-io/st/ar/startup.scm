(define-module (crates-io st ar startup) #:use-module (crates-io))

(define-public crate-startup-0.0.1 (c (n "startup") (v "0.0.1") (h "1l934d787k4482x0v6wwgf56hbkybz7yi759nkx68gm9scmpxzr6") (y #t)))

(define-public crate-startup-0.1.0 (c (n "startup") (v "0.1.0") (h "048jq0d3ja65caghsvnssqfxmzcs8f4cjs8m9vghkbq61p7gvrm8") (y #t)))

(define-public crate-startup-0.1.1 (c (n "startup") (v "0.1.1") (h "0bwkip4pd9pajh25aays6d1xdd2qrx13xdial3h6sscpwkxq4jvf") (y #t)))

