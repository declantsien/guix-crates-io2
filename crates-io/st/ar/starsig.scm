(define-module (crates-io st ar starsig) #:use-module (crates-io))

(define-public crate-starsig-0.1.0 (c (n "starsig") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "merlin") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.3.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jknddf2ivcmwyk3rn8gvm7rkmcwrjl5v9nc0wb0yzbfbbhngb0q") (y #t)))

(define-public crate-starsig-0.2.0 (c (n "starsig") (v "0.2.0") (d (list (d (n "curve25519-dalek") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "merlin") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.3.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m7rn4f9p0dngalv6ijqbg816a65wa8dr77j91a3piagvr5qdd6p")))

(define-public crate-starsig-0.2.1 (c (n "starsig") (v "0.2.1") (d (list (d (n "curve25519-dalek") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "merlin") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.3.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nndafvmck9qnzd1jj88j2gqpmx8qwpna9fqcvsc7y3p6vgiy9s1")))

