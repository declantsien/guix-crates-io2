(define-module (crates-io st ar stardom-web) #:use-module (crates-io))

(define-public crate-stardom-web-0.1.0 (c (n "stardom-web") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "stardom-nodes") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("enable-interning"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Node" "Element" "Text" "Comment" "Range" "DocumentFragment" "NodeList" "Event"))) (d #t) (k 0)))) (h "0dg606cw7ba33bqa58zkzm8i06rqaf4y36asix1rqmazyvp3xm4a")))

