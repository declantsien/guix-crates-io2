(define-module (crates-io st ar starknet-selector-decoder) #:use-module (crates-io))

(define-public crate-starknet-selector-decoder-0.1.0 (c (n "starknet-selector-decoder") (v "0.1.0") (d (list (d (n "phf") (r "^0.11") (k 0)) (d (n "phf") (r "^0.11.1") (k 1)) (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 1)) (d (n "starknet") (r "^0.9.0") (d #t) (k 1)))) (h "138xjfjmcjzpsrwgy5ignyi7qc2a4gkc9z88fyl1wlsm46glbczv")))

(define-public crate-starknet-selector-decoder-0.1.1 (c (n "starknet-selector-decoder") (v "0.1.1") (d (list (d (n "phf") (r "^0.11") (k 0)) (d (n "phf") (r "^0.11.1") (k 1)) (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 1)) (d (n "starknet") (r "^0.9.0") (d #t) (k 1)))) (h "0y155q4gd4jrb2vca912zl5pi8vn3maywx1i6yfy5dkm219fg22m")))

(define-public crate-starknet-selector-decoder-0.1.2 (c (n "starknet-selector-decoder") (v "0.1.2") (d (list (d (n "phf") (r "^0.11") (k 0)) (d (n "phf") (r "^0.11.1") (k 1)) (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 1)) (d (n "starknet") (r "^0.9.0") (d #t) (k 1)))) (h "0zfnpkxi2l1f5ffp6097ivj5lqpzgwsfyjyjrppvjh0n2d49dys4")))

