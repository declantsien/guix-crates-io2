(define-module (crates-io st ar starknet-crypto-codegen) #:use-module (crates-io))

(define-public crate-starknet-crypto-codegen-0.1.0 (c (n "starknet-crypto-codegen") (v "0.1.0") (d (list (d (n "starknet-curve") (r "^0.1.0") (d #t) (k 0)) (d (n "starknet-ff") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (d #t) (k 0)))) (h "0iwc4nnj5b7fawga0v0411inwdb3rw5d08383z2fvxph602dfsb5")))

(define-public crate-starknet-crypto-codegen-0.2.0 (c (n "starknet-crypto-codegen") (v "0.2.0") (d (list (d (n "starknet-curve") (r "^0.2.0") (d #t) (k 0)) (d (n "starknet-ff") (r "^0.3.0") (k 0)) (d (n "syn") (r "^1.0.96") (k 0)))) (h "1imqiqnl07yrc3ajjin96mfd3fj5d85synns1qnjc0l2nmcd24s0")))

(define-public crate-starknet-crypto-codegen-0.3.0 (c (n "starknet-crypto-codegen") (v "0.3.0") (d (list (d (n "starknet-curve") (r "^0.2.1") (d #t) (k 0)) (d (n "starknet-ff") (r "^0.3.1") (k 0)) (d (n "syn") (r "^1.0.96") (k 0)))) (h "1flyv21m3qxxpjfzcpdkg8881wjdpp2nhp609b1mly5cyds8zw5z")))

(define-public crate-starknet-crypto-codegen-0.3.1 (c (n "starknet-crypto-codegen") (v "0.3.1") (d (list (d (n "starknet-curve") (r "^0.3.0") (d #t) (k 0)) (d (n "starknet-ff") (r "^0.3.2") (k 0)) (d (n "syn") (r "^2.0.15") (k 0)))) (h "1gcyzmfrgqkl0nppsipm2nkd37ac6k991fzz048dxnbhykqqip76")))

(define-public crate-starknet-crypto-codegen-0.3.2 (c (n "starknet-crypto-codegen") (v "0.3.2") (d (list (d (n "starknet-curve") (r "^0.4.0") (d #t) (k 0)) (d (n "starknet-ff") (r "^0.3.4") (k 0)) (d (n "syn") (r "^2.0.15") (k 0)))) (h "1ki08al99s8bv32d8z4fhhlp5xj3pjhhw1kfl7444da28nw2frdg")))

(define-public crate-starknet-crypto-codegen-0.3.3 (c (n "starknet-crypto-codegen") (v "0.3.3") (d (list (d (n "starknet-curve") (r "^0.4.2") (d #t) (k 0)) (d (n "starknet-ff") (r "^0.3.7") (k 0)) (d (n "syn") (r "^2.0.15") (k 0)))) (h "17yi0vr7m8srg8rkp7mwwan61q7bgyjk6wr33ivfjyscjfhmkhdv")))

