(define-module (crates-io st ar stardict_wrapper) #:use-module (crates-io))

(define-public crate-stardict_wrapper-0.0.1 (c (n "stardict_wrapper") (v "0.0.1") (d (list (d (n "stardict_lib") (r "^0.1.11") (f (quote ("sled"))) (d #t) (k 0)))) (h "0zf6wadni1xx44gcngh7iasya6dbrhjzcwzwyqg4rw9dbicfxka6")))

(define-public crate-stardict_wrapper-0.0.2 (c (n "stardict_wrapper") (v "0.0.2") (d (list (d (n "stardict_lib") (r "^0.1.11") (f (quote ("sled"))) (d #t) (k 0)))) (h "1n5bk2lm8hr4y28z4vvbg1dwi49ivv634lrp9xc0kdyyn3vys8c1")))

(define-public crate-stardict_wrapper-0.0.3 (c (n "stardict_wrapper") (v "0.0.3") (d (list (d (n "stardict_lib") (r "^0.1.11") (f (quote ("sled"))) (d #t) (k 0)))) (h "09mg8swzmg8hjabjgjrrazfdngjg3553sz8393jwyxg7mvqgvvqb")))

(define-public crate-stardict_wrapper-0.0.4 (c (n "stardict_wrapper") (v "0.0.4") (d (list (d (n "stardict_lib") (r "^0.1.11") (f (quote ("sled"))) (d #t) (k 0)))) (h "0rhs3kq2k5kksch8a3wg8zf2zxnyvzx8smz01jwm61p2rkix4yj4")))

(define-public crate-stardict_wrapper-0.0.5 (c (n "stardict_wrapper") (v "0.0.5") (d (list (d (n "stardict_lib") (r "^0.2.0") (d #t) (k 0)))) (h "0ibsfilsxyyvb9rmwa09246jy9dix5gfiv36ajnpm76310x00n74")))

