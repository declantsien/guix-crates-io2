(define-module (crates-io st ar starknet-curve) #:use-module (crates-io))

(define-public crate-starknet-curve-0.1.0 (c (n "starknet-curve") (v "0.1.0") (d (list (d (n "starknet-ff") (r "^0.2.0") (d #t) (k 0)))) (h "194vlaqf9hg0ib878r7fyyi3yy6kygz78m9knpcdn3q6sdwn1gl4")))

(define-public crate-starknet-curve-0.2.0 (c (n "starknet-curve") (v "0.2.0") (d (list (d (n "starknet-ff") (r "^0.3.0") (k 0)))) (h "0c2fd9lccj1yn2qzsa2ccbhr3nbkykfv9yamdng6bnhrbjijpcjw") (f (quote (("bigdecimal" "starknet-ff/bigdecimal"))))))

(define-public crate-starknet-curve-0.2.1 (c (n "starknet-curve") (v "0.2.1") (d (list (d (n "starknet-ff") (r "^0.3.1") (k 0)))) (h "1bxp0x6qgpqighkrnx6dy42k0f62d3xjwvdw2whlrm8lxzkvs3gy") (f (quote (("bigdecimal" "starknet-ff/bigdecimal"))))))

(define-public crate-starknet-curve-0.3.0 (c (n "starknet-curve") (v "0.3.0") (d (list (d (n "starknet-ff") (r "^0.3.2") (k 0)))) (h "0hh9zjsb4f5jixlhs9522nsgkly5d5s9ymp35hrw9r2rzyx109i5") (f (quote (("bigdecimal" "starknet-ff/bigdecimal"))))))

(define-public crate-starknet-curve-0.4.0 (c (n "starknet-curve") (v "0.4.0") (d (list (d (n "starknet-ff") (r "^0.3.4") (k 0)))) (h "0lihiyd7ygi2zqgrlqp1xsz0vjd7b51d9gyxhfzjlmsnms3hv2m6")))

(define-public crate-starknet-curve-0.4.1 (c (n "starknet-curve") (v "0.4.1") (d (list (d (n "starknet-ff") (r "^0.3.6") (k 0)))) (h "1bxi2mzb27yb9a79vk37fq1qlg36km1hbi42xrbgxcyzrbz59i33")))

(define-public crate-starknet-curve-0.4.2 (c (n "starknet-curve") (v "0.4.2") (d (list (d (n "starknet-ff") (r "^0.3.7") (k 0)))) (h "0nw7vfbsk28vqix9vcmgl2csld20ckl563z89cg7a4mkid8q7hyi")))

