(define-module (crates-io st ar starknet-contract-class) #:use-module (crates-io))

(define-public crate-starknet-contract-class-0.1.0 (c (n "starknet-contract-class") (v "0.1.0") (d (list (d (n "cairo-vm") (r "^0.8.0") (f (quote ("cairo-1-hints" "cairo-1-hints"))) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "lambda_starknet_api") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0sqjm66fj0vidslp8i30m7bpfb4ggwirh0ml6xlap1jqf0vg7d3h")))

(define-public crate-starknet-contract-class-0.2.0 (c (n "starknet-contract-class") (v "0.2.0") (d (list (d (n "cairo-vm") (r "^0.8.1") (f (quote ("cairo-1-hints" "cairo-1-hints"))) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "starknet_api") (r "^0.1.0") (d #t) (k 0)))) (h "0zywjajbyw8p8gp97c8bs013vvnn69gflvdkxwjc1sjfb54av74w")))

