(define-module (crates-io st ar starlark_derive) #:use-module (crates-io))

(define-public crate-starlark_derive-0.5.0 (c (n "starlark_derive") (v "0.5.0") (d (list (d (n "gazebo") (r "^0.2.1") (f (quote ("str_pattern_extensions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0py96w9wi88m443nf11ysissrhrdqsb1k5inax4jh2i3n9fcpmqh")))

(define-public crate-starlark_derive-0.6.0 (c (n "starlark_derive") (v "0.6.0") (d (list (d (n "gazebo") (r "^0.2.1") (f (quote ("str_pattern_extensions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c2jbzfly7lsjcfgbnamwc37xpi0a1fz1zdzs1j5ysdm4v69xj5w")))

(define-public crate-starlark_derive-0.7.0 (c (n "starlark_derive") (v "0.7.0") (d (list (d (n "gazebo") (r "^0.6.0") (f (quote ("str_pattern_extensions"))) (d #t) (k 0)) (d (n "gazebo_lint") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b1kcwnq70z1g61plkminzlqdsargc0i17yx6j77gxfkvxv1fy74")))

(define-public crate-starlark_derive-0.9.0 (c (n "starlark_derive") (v "0.9.0") (d (list (d (n "dupe") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "14ih3mabg4yzhsy8kz0y4jwpq9wcfplg297ygmygy0hshhcq63p5")))

(define-public crate-starlark_derive-0.10.0 (c (n "starlark_derive") (v "0.10.0") (d (list (d (n "dupe") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1xa9zk431m0kc210a07xrmazczzff0fkv5yf4yb7d1hxmdf7hcc1")))

(define-public crate-starlark_derive-0.11.0 (c (n "starlark_derive") (v "0.11.0") (d (list (d (n "dupe") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1jxsylhv67gmngv10yqc1i4fpxikmbq20bkzvsxfn128nkj7mjxf")))

(define-public crate-starlark_derive-0.12.0 (c (n "starlark_derive") (v "0.12.0") (d (list (d (n "dupe") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "01y8r3qvr8hgb7wr46sj35m8dfsqc1by1dy8rw560kqphv50g17f")))

