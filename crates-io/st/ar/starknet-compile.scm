(define-module (crates-io st ar starknet-compile) #:use-module (crates-io))

(define-public crate-starknet-compile-2.0.0-rc1 (c (n "starknet-compile") (v "2.0.0-rc1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.0.0-rc1") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.0.0-rc1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gkxcgzpyadfc3mvnxr1j3jklxxr2nv7f0vww5yb7zb7dnlyc784")))

(define-public crate-starknet-compile-2.0.0-rc2 (c (n "starknet-compile") (v "2.0.0-rc2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.0.0-rc2") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.0.0-rc2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0khg6by2s92vn7m0svdknhlq5yvmvl0zi3a557d50f8nm8m6bpll")))

(define-public crate-starknet-compile-2.0.0-rc3 (c (n "starknet-compile") (v "2.0.0-rc3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.0.0-rc3") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.0.0-rc3") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x09fpzzyr0mh712q2bgv4zcn5qk0gmy9m463yn0nbj3h6mkwm61")))

(define-public crate-starknet-compile-2.0.0-rc4 (c (n "starknet-compile") (v "2.0.0-rc4") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.0.0-rc4") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.0.0-rc4") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vbzj74a8k2dv3ybryc5sb2lvq9ijmyhn8dwnl9kzdj3f4fgw77f")))

(define-public crate-starknet-compile-2.0.0-rc5 (c (n "starknet-compile") (v "2.0.0-rc5") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.0.0-rc5") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.0.0-rc5") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0isixjpszlr817nh0r9f7g9s2i5h97wadj2krxbrw5i1n2j0l4vg")))

(define-public crate-starknet-compile-2.0.0-rc6 (c (n "starknet-compile") (v "2.0.0-rc6") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.0.0-rc6") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.0.0-rc6") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qcmc1mhi92s963ipza2i2hqwxyg4ia93fgj2zw1n454pr4czs5d")))

(define-public crate-starknet-compile-2.0.0-rc7 (c (n "starknet-compile") (v "2.0.0-rc7") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.0.0-rc7") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.0.0-rc7") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07pdcs5lx6nnc4baa2vxb32kb303l83fqhwxzk5jjkw9j5jfqh2r")))

(define-public crate-starknet-compile-2.0.0 (c (n "starknet-compile") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.0.0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00v52h6jp3wvvpnsfsarj2bmzs1gan7n4h53xaqzg4d0kfhd5d6r")))

(define-public crate-starknet-compile-2.0.1 (c (n "starknet-compile") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.0.1") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vmgbmz9yhndd032h9nl83x9vd036b0vx0rib76pyda7k21jyfic")))

(define-public crate-starknet-compile-2.0.2 (c (n "starknet-compile") (v "2.0.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.0.2") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "065ylmkhnvspb5c55ca3yga80bcgkbkvmmjbzgz81b4hzkmq6aa2")))

(define-public crate-starknet-compile-2.1.0-rc0 (c (n "starknet-compile") (v "2.1.0-rc0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.1.0-rc0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.1.0-rc0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ndbrzs0hgbg118bplsffpcmsnfzi73hfkwflfqvvcga4i4w6zpx")))

(define-public crate-starknet-compile-2.1.0-rc1 (c (n "starknet-compile") (v "2.1.0-rc1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.1.0-rc1") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.1.0-rc1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "092fkag2m5imm8mkbl5yg4y71dba6mi4qvx5271m8ppdxzsaz70n")))

(define-public crate-starknet-compile-2.1.0-rc2 (c (n "starknet-compile") (v "2.1.0-rc2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.1.0-rc2") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.1.0-rc2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i5hb5ndafvph32g7yl9lcbwbqp3h8j11bbhaqknqz5rc8g1gkp9")))

(define-public crate-starknet-compile-2.1.0-rc3 (c (n "starknet-compile") (v "2.1.0-rc3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.1.0-rc3") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.1.0-rc3") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sgmkgac4hy0ffsj4nav7hxyrklkab1s1083cp5vp21995fdh3cm")))

(define-public crate-starknet-compile-2.1.0-rc4 (c (n "starknet-compile") (v "2.1.0-rc4") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.1.0-rc4") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.1.0-rc4") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gr0jqkrandya9rg7mdj88067z16dvzp6v0x6r95di742m3jwzr8")))

(define-public crate-starknet-compile-2.1.0 (c (n "starknet-compile") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.1.0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qv6d9n4wi3gvalcp7j1c2an0ylcnv9d1rslx3vrjwn8c8p5c25k")))

(define-public crate-starknet-compile-2.1.1 (c (n "starknet-compile") (v "2.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.1.1") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mh364wws2r4k21fdn65xpw0z7w9f8jpfd1xvg6v1xdkiy4awgp3")))

(define-public crate-starknet-compile-2.2.0 (c (n "starknet-compile") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.2.0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fqk09js3kcgbqlgj44akmh6k4drcdh86fanqsyx8qsnzr3s7y29")))

(define-public crate-starknet-compile-2.3.0-rc0 (c (n "starknet-compile") (v "2.3.0-rc0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.3.0-rc0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.3.0-rc0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15kf3nrqiml69ndx8w58nphxn1rppr0x43p9gpal3z4nh34p2kp2")))

(define-public crate-starknet-compile-2.3.0 (c (n "starknet-compile") (v "2.3.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.3.0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rh64q70lmly1a3xci198f7yhs543fy94wra04mr2z37hns94k6k")))

(define-public crate-starknet-compile-2.3.1 (c (n "starknet-compile") (v "2.3.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.3.1") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x77qwbfd6978xpb9cb1xjwb9w2nk62h8z1ga50jvh7jwzpgbrbh")))

(define-public crate-starknet-compile-2.4.0-rc0 (c (n "starknet-compile") (v "2.4.0-rc0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.0-rc0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.0-rc0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02jirlwmm8wwjcaiivfnhvr5lpkki6f6v6k2bw8rmlqxjnqz6jlc")))

(define-public crate-starknet-compile-2.4.0-rc1 (c (n "starknet-compile") (v "2.4.0-rc1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.0-rc1") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.0-rc1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "03w9ybv0hphsck13xni0m3iv8ngxarz0m461ch94svgyyhq9zb3h")))

(define-public crate-starknet-compile-2.4.0-rc2 (c (n "starknet-compile") (v "2.4.0-rc2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.0-rc2") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.0-rc2") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kmfii47g2abrncvw1scbyzdfchjs1p0i58xsj1ssxkqdgs4d2mj")))

(define-public crate-starknet-compile-2.4.0-rc3 (c (n "starknet-compile") (v "2.4.0-rc3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.0-rc3") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.0-rc3") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q7pifd745mn0mkbri101cir9si3mqpz3ax69m3s3ccrn2943cbg")))

(define-public crate-starknet-compile-2.1.2 (c (n "starknet-compile") (v "2.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "=2.1.2") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "=2.1.2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lmry9w6n16bvgrv5whwc9qzg2dy56hxahqv2rrcclhgmm69ijsx")))

(define-public crate-starknet-compile-2.4.0-rc4 (c (n "starknet-compile") (v "2.4.0-rc4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.0-rc4") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.0-rc4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h1xcq0cd0v5vp7awny5bfknn44pvqz001c2cvrqhxypr1p5plif")))

(define-public crate-starknet-compile-2.4.0-rc5 (c (n "starknet-compile") (v "2.4.0-rc5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.0-rc5") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.0-rc5") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "10faxk3k8gmgs3snma2hby3vz0f5xsbrjx0llmig6vsj66c4vq3i")))

(define-public crate-starknet-compile-2.4.0-rc6 (c (n "starknet-compile") (v "2.4.0-rc6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.0-rc6") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.0-rc6") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qh602jvxv6q88dh323v7fny68vqgxh2sr22p88r96xyxq7ifzrq")))

(define-public crate-starknet-compile-2.4.0 (c (n "starknet-compile") (v "2.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "038acp9nkmhi1gnrh88fyh6jd0v0hapnzl38mawiw2r2ja8n1lh0")))

(define-public crate-starknet-compile-2.4.1 (c (n "starknet-compile") (v "2.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.1") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hg7qqhyw4q5a9372y387aq4rc6y7krbywirjxdix15gz4mym4kd")))

(define-public crate-starknet-compile-2.5.0-dev.0 (c (n "starknet-compile") (v "2.5.0-dev.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.5.0-dev.0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.5.0-dev.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fg2i9gkxw2v2h82zjgyx9zrkb8gswyvhard3ydmlsgcfhagdyfy")))

(define-public crate-starknet-compile-2.4.2 (c (n "starknet-compile") (v "2.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.2") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.2") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rbwidb73102caznbp07n7zp59bmlakj84igj6xz79wvgh3r8nbn")))

(define-public crate-starknet-compile-2.4.3 (c (n "starknet-compile") (v "2.4.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.3") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "18igfqhvrclpkkb8raqyxljyfix5pzwqpk8b8drwmayb4v45x5c4")))

(define-public crate-starknet-compile-2.5.0-dev.1 (c (n "starknet-compile") (v "2.5.0-dev.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.5.0-dev.1") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.5.0-dev.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "138016q2m5vfsvbf6m57hcbbhpkcqza78i8qdcsng8dpkplcqwcp")))

(define-public crate-starknet-compile-2.4.4 (c (n "starknet-compile") (v "2.4.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.4.4") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.4.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "011yi248pgkn199xzz8xpiidi3j4h3bi844lya4qqn2r7jncq2yr")))

(define-public crate-starknet-compile-2.5.0 (c (n "starknet-compile") (v "2.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.5.0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "00w6v1irhywb4nzcs8fzj2ngiqmbya0846r6anc4psdwy42k9w68")))

(define-public crate-starknet-compile-2.5.1 (c (n "starknet-compile") (v "2.5.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.5.1") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.5.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "19mcbvvakx8k4v44jfxky0az7fi9ax482wdap9ffwlh7q7ijr0ff")))

(define-public crate-starknet-compile-2.5.2 (c (n "starknet-compile") (v "2.5.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.5.2") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.5.2") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k0qsdcn4gwiczd23z4ndvqpy0fgiqdsv5aab53h20jsf2j0ni78")))

(define-public crate-starknet-compile-2.5.3 (c (n "starknet-compile") (v "2.5.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.5.3") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.5.3") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q7cslb11d5bj93nxq3mh0a6y1gsgm41ibmpp49103mb5lgmp4bz")))

(define-public crate-starknet-compile-2.6.0-rc.0 (c (n "starknet-compile") (v "2.6.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.6.0-rc.0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.6.0-rc.0") (d #t) (k 0)) (d (n "cairo-lang-starknet-classes") (r "^2.6.0-rc.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ifbhckzgbi521j3z1pq84z3jylp0pnayqz52h0ij8vz95lfrzh6")))

(define-public crate-starknet-compile-2.5.4 (c (n "starknet-compile") (v "2.5.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.5.4") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.5.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h83ra8dxrz0b34aiknvlzz0nagzlg9bwwfhm5lgw6xgfrrm3mgf")))

(define-public crate-starknet-compile-2.6.0-rc.1 (c (n "starknet-compile") (v "2.6.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "^2.6.0-rc.1") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "^2.6.0-rc.1") (d #t) (k 0)) (d (n "cairo-lang-starknet-classes") (r "^2.6.0-rc.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pwkn0m7d1k90mpj4adx3hdwid97vj8vk1j0d8bx34lv1c51qgwr")))

(define-public crate-starknet-compile-2.6.0 (c (n "starknet-compile") (v "2.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "~2.6.0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "~2.6.0") (d #t) (k 0)) (d (n "cairo-lang-starknet-classes") (r "~2.6.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p1xchaja1n82nq1dh29fdm7j3c8nmik0ka0v8pwwl9ybv6i2qpk")))

(define-public crate-starknet-compile-2.6.1 (c (n "starknet-compile") (v "2.6.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "~2.6.1") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "~2.6.1") (d #t) (k 0)) (d (n "cairo-lang-starknet-classes") (r "~2.6.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vnvvnyx54lc4nw6pgd4gvfx408qppl9y670aaxvsgd8ch6vq417")))

(define-public crate-starknet-compile-2.6.2 (c (n "starknet-compile") (v "2.6.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "~2.6.2") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "~2.6.2") (d #t) (k 0)) (d (n "cairo-lang-starknet-classes") (r "~2.6.2") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "19hps0lmmw2yiwqn2r2ldfpnqvdz0qn8bc3crlizpcsdcp93mpbf")))

(define-public crate-starknet-compile-2.6.3 (c (n "starknet-compile") (v "2.6.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "~2.6.3") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "~2.6.3") (d #t) (k 0)) (d (n "cairo-lang-starknet-classes") (r "~2.6.3") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ggjfqyswkr42gxd813dniq23gq662w6w111jz26rvy7fwd1rmb4")))

(define-public crate-starknet-compile-2.7.0-dev.0 (c (n "starknet-compile") (v "2.7.0-dev.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "cairo-lang-compiler") (r "~2.7.0-dev.0") (d #t) (k 0)) (d (n "cairo-lang-starknet") (r "~2.7.0-dev.0") (d #t) (k 0)) (d (n "cairo-lang-starknet-classes") (r "~2.7.0-dev.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "04p0vk33n9y5ww4wnzc4yg8ir8v71dr3j9c3an22v49ck993777a")))

