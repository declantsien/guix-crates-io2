(define-module (crates-io st ar starknet-macros) #:use-module (crates-io))

(define-public crate-starknet-macros-0.1.0 (c (n "starknet-macros") (v "0.1.0") (d (list (d (n "starknet-core") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "15czhqhdlknfsab6iyflpca7ycivmbpyj4gbbl4f0722vdi35baa") (f (quote (("use_imported_type") ("default"))))))

(define-public crate-starknet-macros-0.1.1 (c (n "starknet-macros") (v "0.1.1") (d (list (d (n "starknet-core") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0nrnlsyjlhwj6lb1lwq1m3dyqhh31nn1r8drbpnadbbvxlmk2m7f") (f (quote (("use_imported_type") ("default"))))))

(define-public crate-starknet-macros-0.1.2 (c (n "starknet-macros") (v "0.1.2") (d (list (d (n "starknet-core") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0jpndcmmnlyyxhhjrqfakk2z2hankk0ffifzdglas8pdw1g8d998") (f (quote (("use_imported_type") ("default"))))))

(define-public crate-starknet-macros-0.1.3 (c (n "starknet-macros") (v "0.1.3") (d (list (d (n "starknet-core") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "01i5xdbshsdflf9n9dcxin84mw0lfh1mp6xalblw7j4gnimnp17g") (f (quote (("use_imported_type") ("default"))))))

(define-public crate-starknet-macros-0.1.4 (c (n "starknet-macros") (v "0.1.4") (d (list (d (n "starknet-core") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1c1rd2lhhw2gl5phnv9w7w492480112y3wxgl196gvmpv9gf0vzn") (f (quote (("use_imported_type") ("default"))))))

(define-public crate-starknet-macros-0.1.5 (c (n "starknet-macros") (v "0.1.5") (d (list (d (n "starknet-core") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1lbp9m9d1d3l9qlxms0rqpi9nnzldpra7ls7xqz8cdapxfky22w4") (f (quote (("use_imported_type") ("default"))))))

(define-public crate-starknet-macros-0.1.6 (c (n "starknet-macros") (v "0.1.6") (d (list (d (n "starknet-core") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0yid2hic8ziv9y1zyia6yq2dpmlqzhyf6y82f05cs31gc5j2jpbw") (f (quote (("use_imported_type") ("default"))))))

(define-public crate-starknet-macros-0.1.7 (c (n "starknet-macros") (v "0.1.7") (d (list (d (n "starknet-core") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0ia9pcib7vhbsvgz5hg7kjc456d1azdqvanys1sygnwb0z9lkmcm") (f (quote (("use_imported_type") ("default"))))))

