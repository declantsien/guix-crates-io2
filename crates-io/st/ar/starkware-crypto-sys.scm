(define-module (crates-io st ar starkware-crypto-sys) #:use-module (crates-io))

(define-public crate-starkware-crypto-sys-0.1.0 (c (n "starkware-crypto-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0n608xv623k13n3v73wr4nzbwphi55j1ilcziahnar2zsqx38lw9") (y #t)))

(define-public crate-starkware-crypto-sys-0.1.1 (c (n "starkware-crypto-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "19bg2v2cvhlr1si0jn2wx2x7i89b5jwabvsbcq6r691gk0ilgdrj")))

(define-public crate-starkware-crypto-sys-0.1.2 (c (n "starkware-crypto-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "10vgqxr1c19yv0ladll7xpf16y4vwqv362jradkisvm21k4n6nmr")))

(define-public crate-starkware-crypto-sys-0.1.3 (c (n "starkware-crypto-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "00xp37dmmblq6570wnr1vg47ihcg6npfwqigbmd39gm4lnj0m7aa")))

