(define-module (crates-io st ar star) #:use-module (crates-io))

(define-public crate-star-0.1.0 (c (n "star") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)) (d (n "zstd") (r "^0.5.1") (d #t) (k 0)))) (h "1cmimzqz7m61vah80p0cn2f16yva28i4lx9rrb0z2pk422lhxx5d") (y #t)))

(define-public crate-star-0.1.1 (c (n "star") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)) (d (n "zstd") (r "^0.5.1") (d #t) (k 0)))) (h "0cw5iqi0avs9zvw6ch7x95rfv6hza4j0ay8645h56q84g2cnn9m8") (y #t)))

(define-public crate-star-0.1.2 (c (n "star") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)) (d (n "zstd") (r "^0.5.1") (d #t) (k 0)))) (h "1awrpbfmmklbm8dyn67zivdwi2xl2cj0cqrfnp0csvg3l4y05nl9")))

