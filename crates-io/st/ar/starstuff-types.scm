(define-module (crates-io st ar starstuff-types) #:use-module (crates-io))

(define-public crate-starstuff-types-0.1.2 (c (n "starstuff-types") (v "0.1.2") (d (list (d (n "assert_float_eq") (r "^1") (d #t) (k 0)) (d (n "auto_ops") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1a8c6db1dk4py5vm3x0fs9myh3r6d0xrpjzrwj5mm8yi2m8hm98g")))

(define-public crate-starstuff-types-0.1.4 (c (n "starstuff-types") (v "0.1.4") (d (list (d (n "assert_float_eq") (r "^1") (d #t) (k 0)) (d (n "auto_ops") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "197by3s0z1s6wi31n8rf9xvgnglyhxnbq6z2lckc4f41wj2f1p74")))

(define-public crate-starstuff-types-0.1.5 (c (n "starstuff-types") (v "0.1.5") (d (list (d (n "assert_float_eq") (r "^1") (d #t) (k 0)) (d (n "auto_ops") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0jqi4v1hcr6qhzgm40l9nssxbr0c0k3n2cb4if814z5lvdk42m2b")))

(define-public crate-starstuff-types-0.1.8 (c (n "starstuff-types") (v "0.1.8") (d (list (d (n "assert_float_eq") (r "^1") (d #t) (k 0)) (d (n "auto_ops") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1qaq290is95cwb2i8ry5f8yipj1spfl52kxh3r3jh3jjqn1lwvw4")))

(define-public crate-starstuff-types-0.1.9 (c (n "starstuff-types") (v "0.1.9") (d (list (d (n "assert_float_eq") (r "^1") (d #t) (k 0)) (d (n "auto_ops") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "1fkjz7vzvbb0pfhzll9ynq86y8i52mg3yy90xjdq5f6zra90ks5d")))

