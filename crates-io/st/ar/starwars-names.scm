(define-module (crates-io st ar starwars-names) #:use-module (crates-io))

(define-public crate-starwars-names-0.1.0 (c (n "starwars-names") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 0)))) (h "0j8qwdliirbfi0jnp9p9fblgicikrxgviyl0jgiwxb0nawk4wnw9")))

