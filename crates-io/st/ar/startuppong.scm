(define-module (crates-io st ar startuppong) #:use-module (crates-io))

(define-public crate-startuppong-0.1.0 (c (n "startuppong") (v "0.1.0") (d (list (d (n "hyper") (r "~0.5") (d #t) (k 0)) (d (n "mime") (r "^0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "11brmhakxbbmx8cy8xddmwk2bph1j2sh3xxc93i7ixfm7zhnd1n3")))

(define-public crate-startuppong-0.2.0 (c (n "startuppong") (v "0.2.0") (d (list (d (n "hyper") (r "~0.5") (d #t) (k 0)) (d (n "mime") (r "^0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "0mfam7y8lr4wyx8whl107xclyjk1dzkl9v5wp0hdijhb8mgl0j0q") (f (quote (("api_test"))))))

(define-public crate-startuppong-0.2.1 (c (n "startuppong") (v "0.2.1") (d (list (d (n "hyper") (r "^0.6.4") (d #t) (k 0)) (d (n "mime") (r "^0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "0dakdxa4mss8v215b2pzxvb41hv4ywkw4c7sh2fbyvyl11bwvlxn") (f (quote (("api_test"))))))

(define-public crate-startuppong-0.2.2 (c (n "startuppong") (v "0.2.2") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "mime") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0gr6ycashpg4x4igsdr0s6467cwhmxkzfi8i7aswsp5rry9q2vlq") (f (quote (("api_test"))))))

