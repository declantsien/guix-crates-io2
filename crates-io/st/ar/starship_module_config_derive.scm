(define-module (crates-io st ar starship_module_config_derive) #:use-module (crates-io))

(define-public crate-starship_module_config_derive-0.1.0 (c (n "starship_module_config_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (d #t) (k 0)))) (h "18b97qjvnqd4yfbdrxj381ys7x9y9yfpi2ajad8jb12qy7i4gl9n")))

(define-public crate-starship_module_config_derive-0.1.1 (c (n "starship_module_config_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (d #t) (k 0)))) (h "0knhif0b71f7akql1ndr87ndw9vb4q8015xbi381blbs8vwcs74x")))

(define-public crate-starship_module_config_derive-0.1.2 (c (n "starship_module_config_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (d #t) (k 0)))) (h "0ckkd2w7w0wgrz3w7g598cmy9s3hn2c7hhz80dv8sc8d2jsd026b")))

(define-public crate-starship_module_config_derive-0.2.1 (c (n "starship_module_config_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (d #t) (k 0)))) (h "09327jvaf2b4xwg0s8l3afrrg140illa0ff6wkwfi4i8pl7dpacp")))

