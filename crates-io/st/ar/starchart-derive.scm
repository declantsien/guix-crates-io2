(define-module (crates-io st ar starchart-derive) #:use-module (crates-io))

(define-public crate-starchart-derive-0.2.0 (c (n "starchart-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0i6n68z89xpjp1i46fz72ac206d7k5pd2hdkzwk850xnmwcgxynn")))

(define-public crate-starchart-derive-0.2.1 (c (n "starchart-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0w5yj67hzlkwq51sip9qc3333jvikkrb7ffw9v6h13yi0d537937")))

(define-public crate-starchart-derive-0.2.2 (c (n "starchart-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nn2dlybidfyxxilrpqkszmlgfv8c98d5zip2x00nc3kadz92shy")))

(define-public crate-starchart-derive-0.2.3 (c (n "starchart-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "089km6av1lzcnnwq7ri623v6sc36zxmi0r4ikivs23zkzx45m3hs")))

(define-public crate-starchart-derive-0.2.4 (c (n "starchart-derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qjbkhrf9k3p6kjjbahp0mb2ad33js2qdwzysd2mk06n1rwsbzac")))

(define-public crate-starchart-derive-0.3.0 (c (n "starchart-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "135l7wcv6rirhsiwdwqsj79l861rpm6s4h8hvbxqx49w9n85n8dm")))

(define-public crate-starchart-derive-0.4.0 (c (n "starchart-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1i8hbydmgwnlniqkn8afb38qyh7xai38w16ky75z2w0wfhjdyd90")))

(define-public crate-starchart-derive-0.5.0 (c (n "starchart-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02hh5hcf6z42vmg6dmkvwxvx6rg8h8ywr8ylpiq024167izp35yp")))

(define-public crate-starchart-derive-0.6.0 (c (n "starchart-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "13a8rgi7n994fwcnwal0rywqhkwyrwqxrkazdy5slaxzlal7x2ky")))

(define-public crate-starchart-derive-0.7.0 (c (n "starchart-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1vcm64dmbfw240srdaypgjk02i45v9hnk7sv2bn38ldic1k5yd0g")))

(define-public crate-starchart-derive-0.7.1 (c (n "starchart-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ak8qk6pxj4mhhrjjwpcqll3p2i1f1y5scz58p938kl1k0zycmcs")))

(define-public crate-starchart-derive-0.8.0 (c (n "starchart-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02hqbx84az4qw303l5mc5fb6vg6329j7a38cbcq1jvjlxplxzff6")))

(define-public crate-starchart-derive-0.9.0 (c (n "starchart-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1s7a1jy1dvl0xpdlr92kpix5zah06rwcisawbk6blg354gs396f3")))

(define-public crate-starchart-derive-0.10.0 (c (n "starchart-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lb3fbsif8nrw6xzblk62nz1bw800wh7m2vfirpqv0jrflif92bs")))

(define-public crate-starchart-derive-0.10.1 (c (n "starchart-derive") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ck5g1hq64v9vfwm80z5m6d8cpl65rs76389laq1r1z9n78r3hhz")))

(define-public crate-starchart-derive-0.10.2 (c (n "starchart-derive") (v "0.10.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1h9i3y4a6m4fgrwvn06a4fcj7jhi9qmq3mcd0n1l5zvljvps6i49")))

(define-public crate-starchart-derive-0.10.3 (c (n "starchart-derive") (v "0.10.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10gshqcy55mcmwjnmvlbgmqqiablp91bddsnqnki2yzkl53p6pai")))

(define-public crate-starchart-derive-0.11.0 (c (n "starchart-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1q53lm101q1z00dirih355kfqaqa91m137pp3i5kg5jk7s7m03cx")))

(define-public crate-starchart-derive-0.12.0 (c (n "starchart-derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wa9k3b5czwni967b6mja4xd1fzvvaa32r5pcj0hy13wl4ygr3dl")))

(define-public crate-starchart-derive-0.13.0 (c (n "starchart-derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1vhyc5019b8snnkv8mih3pw1pfp9spv9ik1ims145l3n5vmkcwjw")))

(define-public crate-starchart-derive-0.14.0 (c (n "starchart-derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1j94vf99rr1s1756vhmwjkv9d27sggyq5gr3gbbif132sp1srqgz")))

(define-public crate-starchart-derive-0.15.0 (c (n "starchart-derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17a5ixlgn7428g58a65xhv8kg4mzgmsdm6q7l6c18f83dhyg3l53")))

(define-public crate-starchart-derive-0.15.1 (c (n "starchart-derive") (v "0.15.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1am12gkgmnx60qmcvyvvgcyc9a39wa0pr4bnk28pws3nwc4nkcgp")))

(define-public crate-starchart-derive-0.16.0 (c (n "starchart-derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "136r59z4zhmz9q69ga6h25v8xqfm9s62r1g67813rxb7iz9xqzn8")))

(define-public crate-starchart-derive-0.16.1 (c (n "starchart-derive") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14p1mgmwnzzncwssj44bz65vnh59pr6zxac4lxdppyz62nb79jq0")))

(define-public crate-starchart-derive-0.17.0 (c (n "starchart-derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ya7qgkmgw59bs1k2b1r0rmgh78nj8za3ah2m0xfz6v0rqrxmq40")))

(define-public crate-starchart-derive-0.17.1 (c (n "starchart-derive") (v "0.17.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1v01q50nh2599q445yrmlxdqg8yyl8grpf173jjm0fjzbasj0hwj")))

(define-public crate-starchart-derive-0.18.0 (c (n "starchart-derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "07yy93plcgw0pscwndsafrg1b1qcqazm6pm39h39r43n9yanxk8x")))

(define-public crate-starchart-derive-0.18.1 (c (n "starchart-derive") (v "0.18.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0aycfw4h8sjmr8w2xpbrkbfv3pgxk1np7yvcy1apv1y29nrd1fnb")))

(define-public crate-starchart-derive-0.18.2 (c (n "starchart-derive") (v "0.18.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rhiqmwkmj04kavq12l10yg8nc4dkvapx07n2k1kyi7brvqdc783")))

(define-public crate-starchart-derive-0.19.0 (c (n "starchart-derive") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xdsns7c2qbxm3cik4v6iffry9gybrvmb8jiq5zg1ndjs2sfdhwd")))

