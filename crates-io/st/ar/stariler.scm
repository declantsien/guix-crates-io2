(define-module (crates-io st ar stariler) #:use-module (crates-io))

(define-public crate-stariler-0.0.1 (c (n "stariler") (v "0.0.1") (h "14gnbmxk24iqh65di0ma87i9pmkpab8vc63cdr2gi59lplbls5r1")))

(define-public crate-stariler-0.0.2 (c (n "stariler") (v "0.0.2") (h "1dcvw17fhmlmbldq812g14s502z3ilzbgz5sngfdf5v0k9kmhrpp")))

