(define-module (crates-io st ar starpu-sys) #:use-module (crates-io))

(define-public crate-starpu-sys-1.0.0 (c (n "starpu-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cl-sys") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "hwlocality-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.155") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "0drfnacy0g2pssf7lkmzra4vjbi500vb4dqgdga9jyqimvi4kd7d") (l "starpu") (s 2) (e (quote (("opencl" "dep:cl-sys")))) (r "1.78")))

