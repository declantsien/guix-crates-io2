(define-module (crates-io st ar stardom) #:use-module (crates-io))

(define-public crate-stardom-0.1.0 (c (n "stardom") (v "0.1.0") (d (list (d (n "stardom-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "stardom-nodes") (r "^0.1.0") (d #t) (k 0)) (d (n "stardom-reactive") (r "^0.1.0") (d #t) (k 0)) (d (n "stardom-render") (r "^0.1.0") (d #t) (k 0)) (d (n "stardom-web") (r "^0.1.0") (d #t) (k 0)))) (h "0sy80ixcfsrys3364wv1nyrbjn5c9bs04dn2ksaa6297z1h7c2pi")))

