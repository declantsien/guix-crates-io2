(define-module (crates-io st ar stargate-grpc-derive) #:use-module (crates-io))

(define-public crate-stargate-grpc-derive-0.1.0 (c (n "stargate-grpc-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qf4bwa3s28l9ancb1sxj278rs44yl4897bpqn2fjhjjnkssyp66")))

(define-public crate-stargate-grpc-derive-0.1.1 (c (n "stargate-grpc-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ik77r8b1zf01kxl53w29d4n0hxzd8gdbx18rxslhi5ir95pnaqb")))

(define-public crate-stargate-grpc-derive-0.2.0 (c (n "stargate-grpc-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14k5aadh4f9fwlzdn2pf5cfdghcvw5dm7nax1pvrh4xfp4rdjyzq")))

