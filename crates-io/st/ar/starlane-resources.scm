(define-module (crates-io st ar starlane-resources) #:use-module (crates-io))

(define-public crate-starlane-resources-0.1.0 (c (n "starlane-resources") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "starlane-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "0yakhvv8xa5n2sk1986g1iwd3a6z1y9q7dzwgr80mq3c7pi969qi")))

