(define-module (crates-io st ar starlark_module) #:use-module (crates-io))

(define-public crate-starlark_module-0.4.0 (c (n "starlark_module") (v "0.4.0") (d (list (d (n "gazebo") (r "^0.2.0") (f (quote ("str_pattern_extensions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k83ydckj6l6h02zpwrp66d5k7v450dac349zkalhy5m5g138jni")))

