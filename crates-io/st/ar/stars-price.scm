(define-module (crates-io st ar stars-price) #:use-module (crates-io))

(define-public crate-stars-price-0.1.0 (c (n "stars-price") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "coingecko") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (d #t) (k 0)))) (h "1l6x1h2ys79gyq0cqq6xvbx996fxjcq0xgh452iwa1bizl9ynf33")))

(define-public crate-stars-price-0.2.0 (c (n "stars-price") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "coingecko") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (d #t) (k 0)))) (h "1hj5wh8jyjglcjam55ah6bznfq79sh6w39f7p2hpdbj7ic3fc6w9")))

