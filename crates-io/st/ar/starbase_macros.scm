(define-module (crates-io st ar starbase_macros) #:use-module (crates-io))

(define-public crate-starbase_macros-0.1.0 (c (n "starbase_macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1p84hr67m3wd8inhxll3pwbrc4i4p8wv3bjq5xszdj3f7asfhxwj") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.1.1 (c (n "starbase_macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "15hdnvv5clmfyq833c8yyrq7bdrf5x0lpi10hrzhccwj0d5jnfac") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.1.2 (c (n "starbase_macros") (v "0.1.2") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1i0ka5axcsf8rvajmcyv0vhjgfx5j1b2nyf92pasrvx2628xiy68") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.1.3 (c (n "starbase_macros") (v "0.1.3") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "00g1ziqpyicrb33vd70s1crzcnnchsrm88q4nppbz7gb2bbvrp96") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.1.4 (c (n "starbase_macros") (v "0.1.4") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0lj4qb5igziq5972cgm33dwwiw3d9dndhqzdbp5rj2h4yy3nsnyi") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.1.5 (c (n "starbase_macros") (v "0.1.5") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0clwbkg1wncb85cp3m6n885ksvf2sr0hkz08am7g9w57y0yi03mf") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.1.6 (c (n "starbase_macros") (v "0.1.6") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1lj8gwqs70p1bz8dndzmwvpivhd9468vm48msh6504pmmacnxc48") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.1.7 (c (n "starbase_macros") (v "0.1.7") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0mjigwb850r6lgr2hhy1rs15rps9w1kvbxm9l9yhs2zwxssv6r95") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.1.8 (c (n "starbase_macros") (v "0.1.8") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1ix19s8h6h4kvbdh7n3jnblpm210vd05kq39lhg8637zbm3k6sqq") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.1.9 (c (n "starbase_macros") (v "0.1.9") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0kpm05kxs8l9nmxbyajsi0a9rrb7l8hk1zq68v68pnlsgipnxivs") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.1.10 (c (n "starbase_macros") (v "0.1.10") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1vdzvcm9qg20i896xpil2bkzs76ar590n3by3mzm08f9n6nka2sm") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.2.0 (c (n "starbase_macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1lbkmsz6l4hfbqbffi1n9c009c2zfk7sk9m7mw16p6hcmgfhijma") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.2.1 (c (n "starbase_macros") (v "0.2.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0c5aaskpcklp6qmj9lsj1jv3dyyzkad7hpspnqlhxfa8hrvf5dkb") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.2.2 (c (n "starbase_macros") (v "0.2.2") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "00x4kkg0qv9r89rp7lkdyijjpg3hhdrqlcn3ycmv1myhkv2q1x0f") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.2.3 (c (n "starbase_macros") (v "0.2.3") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "103hxh0bfija7fbf2ngln8rg8jggqc77xyjx11q93mpr6yv1j0jv") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.2.4 (c (n "starbase_macros") (v "0.2.4") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "08p23rmw15f6ygda3sb4clczlf1fwzpxndrnh1blair2c3vpfand") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.2.5 (c (n "starbase_macros") (v "0.2.5") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "0fixvys1sdxryynk00ccc7mk12ij0524vgz4ac9w000d32bra5b3") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.2.6 (c (n "starbase_macros") (v "0.2.6") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1zjdd1w0ma4sqfsif9y8p49vh8spcla1ax42ya0cv8k3dnmhsx2j") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.2.7 (c (n "starbase_macros") (v "0.2.7") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "133350n4crg75cvgrf344slrv0k4ifc2b9vibg7x2b3pmkhkpnkh") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.2.8 (c (n "starbase_macros") (v "0.2.8") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1nl1acplj0s799hhngkqrik2x0ja92n0pvxh2iwp483qcp983liy") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.2.9 (c (n "starbase_macros") (v "0.2.9") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "0zagnhgh7nk0dbval7b428g48f35492hl7s3kb0v9bzjlk3ng3lq") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.2.10 (c (n "starbase_macros") (v "0.2.10") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "18q293qiaqkpacqn5dx34iivm5q8q77lh7w9rn3xjcbndwbj7gjs") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.4.0 (c (n "starbase_macros") (v "0.4.0") (d (list (d (n "darling") (r "^0.20.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1kjz8ddyqxmy39c2kn77mpkrq4g9hh0blrfcd9ymbayrcyrc27cb") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.5.0 (c (n "starbase_macros") (v "0.5.0") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "067i2j51pkxl3xnj2zxidiiba013qyz56gd193xdz39ar04ynh7v") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.5.1 (c (n "starbase_macros") (v "0.5.1") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1ijkwjldzjb48s6mravvg59f569q87046cw8wrhgw9acas4v7xqa") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.5.2 (c (n "starbase_macros") (v "0.5.2") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "0fyinig8yqrynjh42j2qi3knfnqpjzg6pkjr08rcphij39yll918") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.6.0 (c (n "starbase_macros") (v "0.6.0") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0s3z340hs3mdmxgjiyvvfqrx0rpcxcp8ncmll6q3rhfk9lgyid26") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.6.1 (c (n "starbase_macros") (v "0.6.1") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (f (quote ("full"))) (d #t) (k 0)))) (h "11d9yk567k0azycwd4918fl87z154cvc6khazkp2g4qpw0rxd4ql") (f (quote (("tracing"))))))

(define-public crate-starbase_macros-0.6.2 (c (n "starbase_macros") (v "0.6.2") (d (list (d (n "darling") (r "^0.20.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full"))) (d #t) (k 0)))) (h "1v8a7iqrfj1s1q8y9l411ra0wrq6cqpi8lhn9qqdki24dciixqbj") (f (quote (("tracing"))))))

