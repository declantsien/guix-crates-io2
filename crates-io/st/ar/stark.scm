(define-module (crates-io st ar stark) #:use-module (crates-io))

(define-public crate-stark-0.1.1 (c (n "stark") (v "0.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (f (quote ("simd"))) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0h9i4ndf8xas6a9qn5h55xp0ly94mbhpcxf5dc6mkkvhajxms1y7")))

