(define-module (crates-io st ar stare) #:use-module (crates-io))

(define-public crate-stare-0.1.0 (c (n "stare") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "sysinfo") (r "^0.14.5") (d #t) (k 0)))) (h "1krp5ynsx3vphp0f2lsjya29gnsqmfjx3r1snhw71gsrk18c44rp")))

(define-public crate-stare-0.2.0 (c (n "stare") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "sysinfo") (r "^0.14.5") (d #t) (k 0)))) (h "0qmprbs70lx8c953wfzf9m4w2d7ikjaiqih9aq17v0vvgjcmx3wr")))

(define-public crate-stare-0.2.1 (c (n "stare") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "sysinfo") (r "^0.14.5") (d #t) (k 0)))) (h "1z6afr3gyc8kdd9cfky4xx455mm0614dxc6zjfl955kqhmfsz63a")))

