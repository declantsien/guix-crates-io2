(define-module (crates-io st ar stardom-nodes) #:use-module (crates-io))

(define-public crate-stardom-nodes-0.1.0 (c (n "stardom-nodes") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "stardom-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Event"))) (d #t) (k 0)))) (h "132739r1ghzp8dr8b2mgl76q920h631032c45h78yz4nfrlvf6c0")))

