(define-module (crates-io st ar starry-ecs) #:use-module (crates-io))

(define-public crate-starry-ecs-0.1.0 (c (n "starry-ecs") (v "0.1.0") (d (list (d (n "dyn-clone") (r "^1.0.14") (d #t) (k 0)))) (h "19bq5f8xrrir6sk4xpivafi40mj3rsnq86mpmc4k89q72lalblxy")))

(define-public crate-starry-ecs-0.1.1 (c (n "starry-ecs") (v "0.1.1") (d (list (d (n "dyn-clone") (r "^1.0.14") (d #t) (k 0)))) (h "0g4z2rm66iasiqmm0nlghn4nkb6gj5ydxi52nwa1fyhfiaasxnlf")))

(define-public crate-starry-ecs-0.1.2 (c (n "starry-ecs") (v "0.1.2") (d (list (d (n "dyn-clone") (r "^1.0.14") (d #t) (k 0)))) (h "0qhdz9dqvpwwshj5ax0dwf8msxbv09kblim5317bja60qzb4lbsi")))

(define-public crate-starry-ecs-0.2.0 (c (n "starry-ecs") (v "0.2.0") (d (list (d (n "dyn-clone") (r "^1.0.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0xgw2bmwgixmxj6adha0qvkf0hqdalwzjwf7jscmrb1zjgigmkmm")))

(define-public crate-starry-ecs-0.3.0 (c (n "starry-ecs") (v "0.3.0") (d (list (d (n "dyn-clone") (r "^1.0.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0xq43hzn1x0svm3v4ygckdlnfbv39g2smj7x40f2qjqii61qb8pb") (y #t)))

(define-public crate-starry-ecs-0.3.1 (c (n "starry-ecs") (v "0.3.1") (d (list (d (n "dyn-clone") (r "^1.0.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "12z37jvbna09w0fx1c0xwplrr23jxd8lbyrk9cxjrqx4jadp1zrj")))

(define-public crate-starry-ecs-0.4.0 (c (n "starry-ecs") (v "0.4.0") (d (list (d (n "dyn-clone") (r "^1.0.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1qjada92qxq5g29zj04g53c7334k19irysvyc9qcy75jfdzlp1dd")))

(define-public crate-starry-ecs-0.5.0 (c (n "starry-ecs") (v "0.5.0") (d (list (d (n "dyn-clone") (r "^1.0.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "12xp8pgc9kkzlnf235j3wgkmqrqwbjvwqkn6l1gw54dm11dyhv2v")))

