(define-module (crates-io st ar stardom-render) #:use-module (crates-io))

(define-public crate-stardom-render-0.1.0 (c (n "stardom-render") (v "0.1.0") (d (list (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "stardom-nodes") (r "^0.1.0") (d #t) (k 0)))) (h "1kgs8iljhy18ph5prm82066n6vi2w4zb7mb9csiijr6z790444rr")))

