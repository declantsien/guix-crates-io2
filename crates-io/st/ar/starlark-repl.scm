(define-module (crates-io st ar starlark-repl) #:use-module (crates-io))

(define-public crate-starlark-repl-0.2.0 (c (n "starlark-repl") (v "0.2.0") (d (list (d (n "codemap") (r "^0.1.1") (d #t) (k 0)) (d (n "codemap-diagnostic") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.5.3") (d #t) (k 0)) (d (n "starlark") (r "^0.2") (d #t) (k 0)))) (h "0vyai1zmmrx1h5srszbbksapm8a9wsbavbzia8gfswf8bcb657dx")))

(define-public crate-starlark-repl-0.3.0 (c (n "starlark-repl") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^0.10.2") (d #t) (k 2)) (d (n "codemap") (r "^0.1.1") (d #t) (k 0)) (d (n "codemap-diagnostic") (r "^0.1.1") (d #t) (k 0)) (d (n "linefeed") (r "^0.5.3") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "starlark") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r ">= 3, < 3.0.5") (d #t) (k 2)))) (h "1jq4rp6089p2h2dr067azympffanvb35s1hndvj6ks4zz90kl675")))

(define-public crate-starlark-repl-0.3.1 (c (n "starlark-repl") (v "0.3.1") (d (list (d (n "assert_cmd") (r "^0.10.2") (d #t) (k 2)) (d (n "codemap") (r "^0.1.1") (d #t) (k 0)) (d (n "codemap-diagnostic") (r "^0.1.1") (d #t) (k 0)) (d (n "linefeed") (r "^0.5.3") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "starlark") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3, <3.0.5") (d #t) (k 2)))) (h "0dlbpyycvd9ggkw63rlzln05m9z7pbh00i7wn5silg3sixvwh3rr")))

