(define-module (crates-io st ar starlight) #:use-module (crates-io))

(define-public crate-starlight-0.0.0 (c (n "starlight") (v "0.0.0") (h "1gvy7vbydyjc9xxvaz8f2mh7wy3lkbz0x7k7vrxfwg7wfd0gyhg2")))

(define-public crate-starlight-0.1.0 (c (n "starlight") (v "0.1.0") (d (list (d (n "awint") (r "^0.14") (f (quote ("rand_support" "dag"))) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (k 0)))) (h "1if818a4ibwdh5a805c0qzxnj3hkq16llkhishbcgwfd6dndvjjb") (f (quote (("zeroize_support" "awint/zeroize_support") ("try_support" "awint/try_support") ("serde_support" "awint/serde_support") ("default" "try_support") ("debug" "awint/debug"))))))

(define-public crate-starlight-0.1.1 (c (n "starlight") (v "0.1.1") (d (list (d (n "awint") (r "^0.14") (f (quote ("rand_support" "dag"))) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (k 0)))) (h "1njdbf0kmfcapikrp54k164qxv34p9kkr7fwfi1mfkmsd8k077c6") (f (quote (("zeroize_support" "awint/zeroize_support") ("try_support" "awint/try_support") ("serde_support" "awint/serde_support") ("default" "try_support") ("debug" "awint/debug"))))))

(define-public crate-starlight-0.2.0 (c (n "starlight") (v "0.2.0") (d (list (d (n "awint") (r "^0.15") (f (quote ("rand_support" "dag"))) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (k 0)))) (h "14j5zab5n5gy3sy3q8144962n09m5k0dvc090gfdiik3kl6890w4") (f (quote (("zeroize_support" "awint/zeroize_support") ("try_support" "awint/try_support") ("serde_support" "awint/serde_support") ("default" "try_support") ("debug" "awint/debug"))))))

(define-public crate-starlight-0.3.0 (c (n "starlight") (v "0.3.0") (d (list (d (n "awint") (r "^0.16") (f (quote ("rand_support" "dag"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sc8vn785ridwqz6g70q3yj8j5ykjm38c7yj4a63vq7jz7qkv007") (f (quote (("zeroize_support" "awint/zeroize_support") ("try_support" "awint/try_support") ("serde_support" "awint/serde_support") ("default" "try_support") ("debug" "awint/debug"))))))

(define-public crate-starlight-0.4.0 (c (n "starlight") (v "0.4.0") (d (list (d (n "awint") (r "^0.17") (f (quote ("rand_support" "dag"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lrjrfg5h6wgnhi9gwxmbx4c1blhgil38rv34ygn3cc9ms2wk6zs") (f (quote (("zeroize_support" "awint/zeroize_support") ("u32_ptrs" "awint/u32_for_pstate") ("try_support" "awint/try_support") ("serde_support" "awint/serde_support") ("gen_counters") ("gen_counter_for_pstate" "awint/gen_counter_for_pstate") ("default" "try_support") ("debug" "awint/debug"))))))

