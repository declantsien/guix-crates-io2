(define-module (crates-io st ar starbase-tools) #:use-module (crates-io))

(define-public crate-starbase-tools-0.1.0 (c (n "starbase-tools") (v "0.1.0") (h "04glpy90inilccrihyq2pcrsdianbwia94xs4s6gyxqy9rp2yf0y")))

(define-public crate-starbase-tools-0.1.1 (c (n "starbase-tools") (v "0.1.1") (h "0kn288si278ad40lksifkvffwsahgqkj9paxg2v7pb5j7dbwafpx")))

