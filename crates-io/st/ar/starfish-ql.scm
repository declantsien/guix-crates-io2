(define-module (crates-io st ar starfish-ql) #:use-module (crates-io))

(define-public crate-starfish-ql-0.1.0 (c (n "starfish-ql") (v "0.1.0") (d (list (d (n "smol") (r "^1.2") (d #t) (k 2)) (d (n "smol-potat") (r "^1.1") (d #t) (k 2)) (d (n "starfish-api") (r "^0.1.0") (d #t) (k 0)) (d (n "starfish-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "1y97idn1fasq6lmfqr01p7qlmixdaikhz3p6nrnps2bdjr1dyni4")))

(define-public crate-starfish-ql-0.1.1 (c (n "starfish-ql") (v "0.1.1") (d (list (d (n "smol") (r "^1.2") (d #t) (k 2)) (d (n "smol-potat") (r "^1.1") (d #t) (k 2)) (d (n "starfish-api") (r "^0.1.0") (d #t) (k 0)) (d (n "starfish-core") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0j9cc32hvwz4n20lzglkfz0bzfns90a2wiys2ima2dm58s1p8wlp")))

(define-public crate-starfish-ql-0.1.2 (c (n "starfish-ql") (v "0.1.2") (d (list (d (n "smol") (r "^1.2") (d #t) (k 2)) (d (n "smol-potat") (r "^1.1") (d #t) (k 2)) (d (n "starfish-api") (r "^0.1.1") (d #t) (k 0)) (d (n "starfish-core") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "15yp79z2l2r8dxx3bhk5vsxqw8ilkvkrpsf3434a9wkfpcqb02pi")))

