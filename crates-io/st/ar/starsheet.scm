(define-module (crates-io st ar starsheet) #:use-module (crates-io))

(define-public crate-starsheet-0.1.0 (c (n "starsheet") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0hgd7xb00y96p1gxg3f1ypp07gmlfdqaahcvhsfzi3yk8gakn7z2")))

(define-public crate-starsheet-0.1.1 (c (n "starsheet") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0y6arc7kqd29xvsqnyc67l9nf4n7viba2qsjlrf2mjgi3sskjdn6")))

