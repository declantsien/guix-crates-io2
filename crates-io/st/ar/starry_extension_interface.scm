(define-module (crates-io st ar starry_extension_interface) #:use-module (crates-io))

(define-public crate-starry_extension_interface-0.0.1 (c (n "starry_extension_interface") (v "0.0.1") (d (list (d (n "tauri") (r "^1.0.0-rc.13") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (d #t) (k 0)))) (h "1sbj1p0ncbzfinb3imzdbvgfh7cp5jbd0iax8ahsp43fhzxzcqzg")))

(define-public crate-starry_extension_interface-0.0.2 (c (n "starry_extension_interface") (v "0.0.2") (d (list (d (n "tauri") (r "^1.0.0-rc.13") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (d #t) (k 0)))) (h "0pgd7gr20ql7hwhfjhkaj5rfkqh50pq98iyldjz361xfhvpy85s3")))

(define-public crate-starry_extension_interface-0.0.3 (c (n "starry_extension_interface") (v "0.0.3") (d (list (d (n "tauri") (r "^1.0.0-rc.13") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (d #t) (k 0)))) (h "0parmmsa5s9g0fmkx2kr60j76g8b9hcfmsqp3k4q9549f2y12s6s")))

(define-public crate-starry_extension_interface-0.0.4 (c (n "starry_extension_interface") (v "0.0.4") (d (list (d (n "tauri") (r "^1.0.0-rc.13") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (d #t) (k 0)))) (h "0w4famgl76vj608692yabya09889cydns9bw4njd5psyibhv9vi0")))

