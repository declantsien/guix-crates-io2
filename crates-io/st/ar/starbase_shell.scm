(define-module (crates-io st ar starbase_shell) #:use-module (crates-io))

(define-public crate-starbase_shell-0.1.0 (c (n "starbase_shell") (v "0.1.0") (d (list (d (n "miette") (r "^7.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (k 0)) (d (n "sysinfo") (r "^0.30.11") (t "cfg(windows)") (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "16dnm47gclyv7b7pj4ssbxnazh1ixs7h4rknfnkjs956mnhk48gm") (f (quote (("default")))) (s 2) (e (quote (("miette" "dep:miette"))))))

(define-public crate-starbase_shell-0.1.1 (c (n "starbase_shell") (v "0.1.1") (d (list (d (n "miette") (r "^7.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (k 0)) (d (n "sysinfo") (r "^0.30.11") (t "cfg(windows)") (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "16x84jd7008j1016xkabg8mnjmmc9h63mn0i712nq4ayhhqjwan2") (f (quote (("default")))) (s 2) (e (quote (("miette" "dep:miette"))))))

(define-public crate-starbase_shell-0.1.2 (c (n "starbase_shell") (v "0.1.2") (d (list (d (n "miette") (r "^7.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (k 0)) (d (n "sysinfo") (r "^0.30.11") (t "cfg(windows)") (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "09vvyfj7vyga2cf09hjfj0gn6da8xyj0xi2szhjz4mn8lppfa778") (f (quote (("default")))) (s 2) (e (quote (("miette" "dep:miette"))))))

(define-public crate-starbase_shell-0.2.0 (c (n "starbase_shell") (v "0.2.0") (d (list (d (n "miette") (r "^7.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (k 0)) (d (n "sysinfo") (r "^0.30.11") (t "cfg(windows)") (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0cxnw44di21yj1y360rxyn5vwr957w55krmzlcwksc1z9n12hkhq") (f (quote (("default")))) (s 2) (e (quote (("miette" "dep:miette"))))))

(define-public crate-starbase_shell-0.2.1 (c (n "starbase_shell") (v "0.2.1") (d (list (d (n "miette") (r "^7.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (k 0)) (d (n "sysinfo") (r "^0.30.12") (t "cfg(windows)") (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1ly74g3yzxp3y0rkbsh8dkagx6zllq10rjyf0h5r2mgk6d8ym5lb") (f (quote (("default")))) (s 2) (e (quote (("miette" "dep:miette"))))))

