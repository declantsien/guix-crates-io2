(define-module (crates-io st ar starling-webhooks) #:use-module (crates-io))

(define-public crate-starling-webhooks-0.1.0 (c (n "starling-webhooks") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "04m6zw5rg3wqafxpawmzi70ays8shgip15z6f2mab9yz58g1a38m")))

