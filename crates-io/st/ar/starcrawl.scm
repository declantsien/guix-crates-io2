(define-module (crates-io st ar starcrawl) #:use-module (crates-io))

(define-public crate-starcrawl-0.1.0 (c (n "starcrawl") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "s2rs") (r "^0.7.15") (f (quote ("html"))) (d #t) (k 0)) (d (n "s2rs-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1cjpxcwakinkfxqxn5wknvj28knrlsbb33vn55h6v09xaa4bl4wd")))

