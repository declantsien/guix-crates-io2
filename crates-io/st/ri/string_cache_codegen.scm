(define-module (crates-io st ri string_cache_codegen) #:use-module (crates-io))

(define-public crate-string_cache_codegen-0.3.0 (c (n "string_cache_codegen") (v "0.3.0") (d (list (d (n "phf_generator") (r "^0.7.15") (d #t) (k 0)))) (h "01lh80dj9czi4108sll819fc8z3pkf9vn7dn3320xkjk6siyv4pz")))

(define-public crate-string_cache_codegen-0.3.1 (c (n "string_cache_codegen") (v "0.3.1") (d (list (d (n "phf_generator") (r "^0.7.15") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.3") (d #t) (k 0)))) (h "0pm00nddgaswmb2dd3f24cyykbq8vjgjc3bkj35ixflbghdgx78c")))

(define-public crate-string_cache_codegen-0.4.0 (c (n "string_cache_codegen") (v "0.4.0") (d (list (d (n "phf_generator") (r "^0.7.15") (d #t) (k 0)) (d (n "phf_shared") (r "^0.7.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.9") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.3") (d #t) (k 0)))) (h "1mxlz4gvh7yl45h5phs8p1yfij0pppr8g8q677rq352kqd8dx727")))

(define-public crate-string_cache_codegen-0.4.1 (c (n "string_cache_codegen") (v "0.4.1") (d (list (d (n "phf_generator") (r "^0.7.15") (d #t) (k 0)) (d (n "phf_shared") (r "^0.7.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.3") (d #t) (k 0)))) (h "1481pfmags6wf4pg6d6j89zx065zarkxz9s2s3fyi50lrw2kna9m")))

(define-public crate-string_cache_codegen-0.4.2 (c (n "string_cache_codegen") (v "0.4.2") (d (list (d (n "phf_generator") (r "^0.7.15") (d #t) (k 0)) (d (n "phf_shared") (r "^0.7.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.3") (d #t) (k 0)))) (h "1npl9zq9cd16d7irksblgk7l7g6qknnzsmr12hrhky2fcpp1xshy")))

(define-public crate-string_cache_codegen-0.4.4 (c (n "string_cache_codegen") (v "0.4.4") (d (list (d (n "phf_generator") (r "^0.7.15") (d #t) (k 0)) (d (n "phf_shared") (r "^0.7.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.3") (d #t) (k 0)))) (h "1ik78h9gs874i24rkyh0myg6x4ni2a9cazbv5yzs9yavnv8mxx7h")))

(define-public crate-string_cache_codegen-0.5.0 (c (n "string_cache_codegen") (v "0.5.0") (d (list (d (n "phf_generator") (r "^0.8") (d #t) (k 0)) (d (n "phf_shared") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1dqm68im8bvpwppsci4mahrfma5q51c010xz9xh7s2mnkywfp215")))

(define-public crate-string_cache_codegen-0.5.1 (c (n "string_cache_codegen") (v "0.5.1") (d (list (d (n "phf_generator") (r "^0.8") (d #t) (k 0)) (d (n "phf_shared") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "15vbk5i7kkj5bbx7f0fi477js4svw5py39gi4rk74anj35g8wk7j")))

(define-public crate-string_cache_codegen-0.5.2 (c (n "string_cache_codegen") (v "0.5.2") (d (list (d (n "phf_generator") (r "^0.10") (d #t) (k 0)) (d (n "phf_shared") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1249fafaa7r3m67zxcbcw1bddanygv13r3209bvlzgi2ny4h5cvb")))

