(define-module (crates-io st ri string_map) #:use-module (crates-io))

(define-public crate-string_map-0.0.1 (c (n "string_map") (v "0.0.1") (h "1a7iairzffgcrj7mpvb864r0fjbv5405xc51abzd83cs2k6iii6k")))

(define-public crate-string_map-0.1.0 (c (n "string_map") (v "0.1.0") (h "1naq22fzbxxqrjg688i0jysba2asm2qwzi70l9nf4fzlxipmnsvf")))

(define-public crate-string_map-0.2.0 (c (n "string_map") (v "0.2.0") (h "0w74rs348mlsyg0nvk7in9z622jljqalfaypz25pffb4inry01a0")))

(define-public crate-string_map-0.3.0 (c (n "string_map") (v "0.3.0") (h "0lkxprwd64gd8gf48pdwvaz4rya0p8li0qzb6yiqh1yl1salhi2l")))

(define-public crate-string_map-0.4.0 (c (n "string_map") (v "0.4.0") (h "14vr3ns5rv842s1kkgnpkdh35pdxvyg0hh6ivcz6hgmvkrmqnxc3")))

(define-public crate-string_map-0.4.1 (c (n "string_map") (v "0.4.1") (h "01h6r9d8m2hikjw72ggmx4pdwp20r1058d9gj9k9p4ib4i0a8nr0")))

