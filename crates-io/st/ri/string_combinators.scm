(define-module (crates-io st ri string_combinators) #:use-module (crates-io))

(define-public crate-string_combinators-0.0.1 (c (n "string_combinators") (v "0.0.1") (h "1lshlg51qqg0l6jqzm7lnl30h5impfyzwjswj7lhmay8rp8d5d0s") (y #t)))

(define-public crate-string_combinators-0.1.0 (c (n "string_combinators") (v "0.1.0") (h "0pxpb26ygj9lq194dg6w45xkxnmq4b9cxl0prjv8pjcj769c9zza") (y #t)))

