(define-module (crates-io st ri string_iter) #:use-module (crates-io))

(define-public crate-string_iter-0.1.0 (c (n "string_iter") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0frkbzcbmf67bag77s4kqh2awc6pm257ld3gfsvg7w2p20p77m78") (f (quote (("std") ("default" "std"))))))

