(define-module (crates-io st ri stringcase) #:use-module (crates-io))

(define-public crate-stringcase-0.1.0 (c (n "stringcase") (v "0.1.0") (h "0xm37hzqmbxgj4kl8ms3vmp3i7xp24ipfwa3n9lqw6axx0zdjl0d") (r "1.56.1")))

(define-public crate-stringcase-0.1.1 (c (n "stringcase") (v "0.1.1") (h "0cvdjsppwiksghxsnqgav37g1kyswpxhz579zwc6gbgfqysppq2a") (r "1.56.1")))

(define-public crate-stringcase-0.2.0 (c (n "stringcase") (v "0.2.0") (h "03yy6cnak7m7qrayw4j3ppbid8g8b2ygbc4wx3s730ilazdxp5i4") (r "1.56.1")))

(define-public crate-stringcase-0.2.1 (c (n "stringcase") (v "0.2.1") (h "1g50j9gpl21v49d4yqywcmwdfxn7phrh0dy6m3drjhhy3848m9z0") (r "1.56.1")))

