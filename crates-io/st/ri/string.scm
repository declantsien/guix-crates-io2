(define-module (crates-io st ri string) #:use-module (crates-io))

(define-public crate-string-0.0.0 (c (n "string") (v "0.0.0") (h "0j20mgl247nwk4gcgnmm0qz2yxgy5nhmbai2jyymjsfhc6yvjq5k")))

(define-public crate-string-0.1.0 (c (n "string") (v "0.1.0") (h "0alpk3lx5apcyfr7avjz3p45x2nmkjk0mz2hrbpskb3w1qh8py9i")))

(define-public crate-string-0.1.1 (c (n "string") (v "0.1.1") (h "0w096pzmlcsmrgxzsh31xr4vxmd01wiy3f2hhj7mh3zrsrhz5jh0")))

(define-public crate-string-0.1.2 (c (n "string") (v "0.1.2") (h "13fir55llgbp6nnq43xyw1xa666i16wqhczn8vdb25b1sz78r6cq")))

(define-public crate-string-0.1.3 (c (n "string") (v "0.1.3") (h "02zn65zh26mgb6l1l8wj357qyr4fl3mmqz9rnm48fwww1cfl2fdn")))

(define-public crate-string-0.2.0 (c (n "string") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)))) (h "158r3n6k1c0j9pb5lp7iajaiv0dh53xhmw2g8k1k93p36y4zpfyh") (f (quote (("default" "bytes"))))))

(define-public crate-string-0.2.1 (c (n "string") (v "0.2.1") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0vaxz85ja52fn66akgvggb29wqa5bpj3y38syykpr1pbrjzi8hfj") (f (quote (("default" "bytes"))))))

(define-public crate-string-0.3.0 (c (n "string") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)))) (h "1rn1n5k90s7bbyry9i83xrpgsba7219hhxm4yy6mvpqqhs3zp159") (f (quote (("default" "bytes"))))))

