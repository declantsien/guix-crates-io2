(define-module (crates-io st ri stringedits) #:use-module (crates-io))

(define-public crate-stringedits-0.1.0 (c (n "stringedits") (v "0.1.0") (h "02qqh1kycq303qyzlygxiz5gsn6cq5qy4w2csgx9ri0sqn2bcwvn")))

(define-public crate-stringedits-0.1.1 (c (n "stringedits") (v "0.1.1") (h "1pr6x7q2j5fygxnwvsw0qavbwz8r85fjsy6qfm79zf4wi25pafd6")))

(define-public crate-stringedits-0.1.2 (c (n "stringedits") (v "0.1.2") (h "01aij1nrfnpc90rl1rsw2b5idbkcskxlg9b55kshadcyhc167hss")))

(define-public crate-stringedits-0.1.3 (c (n "stringedits") (v "0.1.3") (h "0xs65n4cp8vdjll3vcrviaz1k1rhbs2a7hlnrb7aw7pf4c5gkrnl")))

(define-public crate-stringedits-0.1.4 (c (n "stringedits") (v "0.1.4") (h "05ygikx5bd0hh61yai7q38jjw3kaqx3gd55w313armmnnrmi5yf6")))

(define-public crate-stringedits-0.2.0 (c (n "stringedits") (v "0.2.0") (h "0xqf0rsaiwd0z1yfjbhvbsimw9c2a86pwxcbfc31lwh213y4krm2")))

