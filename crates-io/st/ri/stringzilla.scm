(define-module (crates-io st ri stringzilla) #:use-module (crates-io))

(define-public crate-stringzilla-3.0.0 (c (n "stringzilla") (v "3.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ab8gy11npp98znm8aym1pcl5i12p8ydjp31qinf29p4ficdpzsr")))

(define-public crate-stringzilla-3.1.0 (c (n "stringzilla") (v "3.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wqqs0ixfkv1yq2s5ga54xb4cj7a9zbhfn6ab1i4zchz2swh8giy")))

(define-public crate-stringzilla-3.1.1 (c (n "stringzilla") (v "3.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1m0nv4kg04kmjwbxp9bpav067l6jlyq162nyhwj1y8pq7155jlrk")))

(define-public crate-stringzilla-3.1.2 (c (n "stringzilla") (v "3.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0cg8yj39q5yagj1aysbsvc6xnfzwssksl04zdha3m6rcbvz53xwf")))

(define-public crate-stringzilla-3.2.0 (c (n "stringzilla") (v "3.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1chivlc48rh5bfv9ayqphs7nmw8giikzxiwsi8qhyrrc6r3z7774")))

(define-public crate-stringzilla-3.3.0 (c (n "stringzilla") (v "3.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1wlx6x0p6pfm7nvrb38hl73389jx5m0ppjv4nc3r0ys2qcd559sk")))

(define-public crate-stringzilla-3.3.1 (c (n "stringzilla") (v "3.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0krkywvsw49sladbjnvaqp3yn35rcq8566ib1cc2rrf91vz18qgr")))

(define-public crate-stringzilla-3.4.0 (c (n "stringzilla") (v "3.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1fbgj9wxak4gvk96jia3vi1cr0359dais6lr3yscz5l959b0ck2b")))

(define-public crate-stringzilla-3.4.1 (c (n "stringzilla") (v "3.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05a8qcxawiz5wmlc67wkwxb2gayr4bbvm84r53cmnm9bsdiw694f")))

(define-public crate-stringzilla-3.5.0 (c (n "stringzilla") (v "3.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1q93r59vc891zpxwkwj7k7qinwg7hrnvhfdwxkdjqv4sz6iy2r1r")))

(define-public crate-stringzilla-3.6.0 (c (n "stringzilla") (v "3.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0flp8kz86apl1d1k1fshc7chwcnhahwfp3psjq1d0r37xgirhz64")))

(define-public crate-stringzilla-3.6.1 (c (n "stringzilla") (v "3.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15wywfm1857zcapd44n875cbac86pii7ghj7r8zyyw9r20jay2vc")))

(define-public crate-stringzilla-3.6.2 (c (n "stringzilla") (v "3.6.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0xij3bjizi57pl280hky0snx9v27yhmfdd3wl7jyasjff1m3kvc2")))

(define-public crate-stringzilla-3.6.4 (c (n "stringzilla") (v "3.6.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05yi2gqvyshy55wvmcid9pk749xgphplg61gfh971v7zqcbcj0ak")))

(define-public crate-stringzilla-3.6.5 (c (n "stringzilla") (v "3.6.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0p3rw004sp3qds9sq37mcz070mralhjasisv8q611kdbxdyhjmnj")))

(define-public crate-stringzilla-3.6.6 (c (n "stringzilla") (v "3.6.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wagiakc17z33m1h0dvdhhvd3jqxp2847875x2kz7j6vxa3814ip")))

(define-public crate-stringzilla-3.6.7 (c (n "stringzilla") (v "3.6.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1dmvidv1ry6ff3jrcy5i32fn7ji4cyp90lcqa5942kpfj8a41frk")))

(define-public crate-stringzilla-3.6.8 (c (n "stringzilla") (v "3.6.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1azjvdvdkkl2vaa2f02l280dsx3qj3lx7gg639xzd0gf3m0iq10y")))

(define-public crate-stringzilla-3.7.0 (c (n "stringzilla") (v "3.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ajc0xs0aj9ddwkp6591iq7np1jhjvlapc1sckwj52z20v6x79si")))

(define-public crate-stringzilla-3.7.1 (c (n "stringzilla") (v "3.7.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "01ca7lliiifc0la76rnki3bx058jvbhw4bj84mjr0d8l0jywmkia")))

(define-public crate-stringzilla-3.7.2 (c (n "stringzilla") (v "3.7.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "14ynja159hcj0qh32yq4paxksd431860b5cb43q0477azffz7gyi")))

(define-public crate-stringzilla-3.7.3 (c (n "stringzilla") (v "3.7.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1p0xd08qrs5xz6iysf3z76xryn0sp3j5kpiclhxfamsnxy9nm16n")))

(define-public crate-stringzilla-3.8.0 (c (n "stringzilla") (v "3.8.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "12m1zh3inx4b65s3qpvsisd4pk6kvm0pqzdzy9bvybg07ik2wgqa")))

(define-public crate-stringzilla-3.8.1 (c (n "stringzilla") (v "3.8.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0nbwf381r822m4fmpvc9hw02kzgdqfwkpgamgxs1d3c0p2l63cr7")))

(define-public crate-stringzilla-3.8.2 (c (n "stringzilla") (v "3.8.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0y7wnb8gn26kr1w9pzhp6315yakwx57dsrbqnk3n35l99g473fid")))

(define-public crate-stringzilla-3.8.3 (c (n "stringzilla") (v "3.8.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0vpsdb02ibvbsmzwln3p649jfd99c62f9la0wq326rdhhhfqky4x")))

(define-public crate-stringzilla-3.8.4 (c (n "stringzilla") (v "3.8.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0v3flkha06s6gzw1nws9pr43w6bg0fcm2vljbcc303gbiqpkb8g9")))

