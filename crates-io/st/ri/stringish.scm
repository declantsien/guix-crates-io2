(define-module (crates-io st ri stringish) #:use-module (crates-io))

(define-public crate-stringish-0.1.0 (c (n "stringish") (v "0.1.0") (h "102qkfswdiqy3hvvlyhfbil4sgamk9r9gddrkkv7qp7hs8gnr2dr")))

(define-public crate-stringish-0.1.1 (c (n "stringish") (v "0.1.1") (h "1q7d5n8virjjpifhlpla9cvd604jbbvkfrcqbii933a5iwlc2wrv")))

