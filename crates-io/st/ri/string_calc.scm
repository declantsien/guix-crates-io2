(define-module (crates-io st ri string_calc) #:use-module (crates-io))

(define-public crate-string_calc-0.1.0 (c (n "string_calc") (v "0.1.0") (h "1zxn1vvd52bl5jx4qk3s76svdywxvvd45rim8rc3b9w62g8zdcnw")))

(define-public crate-string_calc-0.1.1 (c (n "string_calc") (v "0.1.1") (h "1hgm5x2x0gsxpvlszi1xcp89hrmvznsxj4gwaazzv3nghm7wibq9")))

(define-public crate-string_calc-0.2.0 (c (n "string_calc") (v "0.2.0") (h "0fzwla2d2gid0jzsw7lp3264i55ihyyh3hi12qwrm121cvr63ng0")))

(define-public crate-string_calc-0.2.1 (c (n "string_calc") (v "0.2.1") (h "1ljydcz4pmbz41h3c5b4xpq63k3mxm9psmifc3514c5v3zs4z75h")))

(define-public crate-string_calc-0.2.2 (c (n "string_calc") (v "0.2.2") (h "1fg05i53j0h6082yp7yas9qnw83fga6mcjbn7ggmj2xgm8xbmfp3")))

(define-public crate-string_calc-0.3.0 (c (n "string_calc") (v "0.3.0") (h "1fsa42yjz06llqhjzldl9j13nvd7pq9h2b8xysk37zi6lzs61hz3")))

