(define-module (crates-io st ri strings_set_file) #:use-module (crates-io))

(define-public crate-strings_set_file-0.1.0 (c (n "strings_set_file") (v "0.1.0") (d (list (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "171wwjv20iab53az1ymlqpgwmym8bg91rjzl5hylhdcyv0bi2bs7") (y #t)))

(define-public crate-strings_set_file-0.1.1 (c (n "strings_set_file") (v "0.1.1") (d (list (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "00gq6p4agymif73h6an134akj92vgp9dicrs7ysknlwhh1wq5wr5") (y #t)))

(define-public crate-strings_set_file-0.1.2 (c (n "strings_set_file") (v "0.1.2") (d (list (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "1w806hdl5kf6d32vyd0assnc6dqgkvyrqnk8mpyi68fd6z3wpkkj") (y #t)))

(define-public crate-strings_set_file-0.1.3 (c (n "strings_set_file") (v "0.1.3") (d (list (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "129mqpagcvpxylf9ymcziwyx8wbj30v04h5gdd42x3iicahrjjqv") (y #t)))

(define-public crate-strings_set_file-0.1.4 (c (n "strings_set_file") (v "0.1.4") (d (list (d (n "file_mmap") (r "^0.2") (d #t) (k 0)))) (h "133aviwmjxf5pqwaqis4cl07f71nrq0q39y9ai6zfrdksm7ci4l1") (y #t)))

