(define-module (crates-io st ri string_concat) #:use-module (crates-io))

(define-public crate-string_concat-0.0.0 (c (n "string_concat") (v "0.0.0") (h "1anin6x9k4c104hx10m25w670gdvz0pvz0m8z7zy2kz9djfw0bql")))

(define-public crate-string_concat-0.0.1 (c (n "string_concat") (v "0.0.1") (h "0lp7vxbdq3mg49637zirpq5hvlpzng7ykwmcb7nhzhpf55hyxhy3")))

