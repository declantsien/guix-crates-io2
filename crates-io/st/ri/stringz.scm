(define-module (crates-io st ri stringz) #:use-module (crates-io))

(define-public crate-stringz-0.1.0 (c (n "stringz") (v "0.1.0") (d (list (d (n "stringz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.8.1") (k 0)))) (h "0v8pyq9f213b9062wldyim3d7n1qryp46bb1nii8n336y0ww42sn") (y #t)))

(define-public crate-stringz-0.1.1 (c (n "stringz") (v "0.1.1") (d (list (d (n "stringz-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (k 0)))) (h "0054y4np2pvy50k92kd2ws2fgdg3zdaqyqzxmz1pd93nfk6cvjvc")))

(define-public crate-stringz-0.1.2 (c (n "stringz") (v "0.1.2") (d (list (d (n "stringz-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (k 0)))) (h "172im2nbfnbv2q1m7apq5b76v19q136by8gmlp9wxgfzffv0ws51")))

(define-public crate-stringz-0.2.0 (c (n "stringz") (v "0.2.0") (d (list (d (n "stringz-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "tuplez") (r "^0.9.0") (k 0)))) (h "16vdcpfm98fbfg801smvmjjskx3vfga1am0dzf6apf4x2czzjqb9")))

(define-public crate-stringz-0.3.0 (c (n "stringz") (v "0.3.0") (d (list (d (n "stringz-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "tuplez") (r ">=0.8.0") (k 0)))) (h "0g7l3yfl1khkrlxbdy4gm0wmdrn8g8fh9n2dvlaw5hj82gdn6pdp")))

(define-public crate-stringz-0.3.1 (c (n "stringz") (v "0.3.1") (d (list (d (n "stringz-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "tuplez") (r ">=0.8.0") (k 0)))) (h "1fa17dx6cx1g3wz612cgz3v0ingj20rc07h9whhbv2byqaf6m21c") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-stringz-0.3.2 (c (n "stringz") (v "0.3.2") (d (list (d (n "stringz-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "tuplez") (r ">=0.8.0") (k 0)))) (h "0rqypn2i69895hc616kgz7287j7ca209fq5wdjpv13qfg6fm7rrm") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-stringz-0.3.3 (c (n "stringz") (v "0.3.3") (d (list (d (n "stringz-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "tuplez") (r ">=0.8.0") (k 0)))) (h "1bj18r80rg76zrllqcsfbyjfxhxx4nmk1cc4nxzkfg66i8zbqc8q") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-stringz-0.3.4 (c (n "stringz") (v "0.3.4") (d (list (d (n "stringz-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "tuplez") (r ">=0.8.0") (k 0)))) (h "0ar6rqhvyrbh3kg4l3pd7a4ypgkpfcnqfkyci0kviv4iqxgzyfph") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-stringz-0.3.5-alpha (c (n "stringz") (v "0.3.5-alpha") (d (list (d (n "stringz-macros") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "tuplez") (r "^0.14.14-alpha") (k 0)))) (h "0lmwydy6aqpgxpay6rxv8mp7ky6qkpgkvqjyqi4kr5fbzn2c2y9m") (f (quote (("std" "tuplez/std") ("default" "std") ("alloc" "tuplez/alloc"))))))

(define-public crate-stringz-0.4.0 (c (n "stringz") (v "0.4.0") (d (list (d (n "stringz-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tuplez") (r ">=0.14.14") (k 0)))) (h "0g6qi5h4kkg5wzivd6zagh5bfcwf838k59h8z77jq89fgsb23l66") (f (quote (("std" "tuplez/std") ("default" "std") ("alloc" "tuplez/alloc"))))))

(define-public crate-stringz-0.4.1 (c (n "stringz") (v "0.4.1") (d (list (d (n "stringz-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tuplez") (r ">=0.14.14") (k 0)))) (h "03ianamh5jxsqcfmg31hhpmk2q6h5bsz81wszhaqmg2plwfxk7v9") (f (quote (("std" "tuplez/std") ("default" "std") ("alloc" "tuplez/alloc"))))))

