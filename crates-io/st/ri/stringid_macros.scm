(define-module (crates-io st ri stringid_macros) #:use-module (crates-io))

(define-public crate-stringid_macros-0.1.0 (c (n "stringid_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "182lcpi94fsh7yd47jq9d2a4g89rlgcnvjzm767y196mayaa7zkf") (r "1.56")))

