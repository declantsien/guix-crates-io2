(define-module (crates-io st ri stripper_interface) #:use-module (crates-io))

(define-public crate-stripper_interface-0.0.1 (c (n "stripper_interface") (v "0.0.1") (h "1x44ghzb4z431a084k2bb9vfhk6cqiiyvcw6d8nkr4m6bdjjhpbz")))

(define-public crate-stripper_interface-0.0.2 (c (n "stripper_interface") (v "0.0.2") (h "0drbs5avrljzzh8hq0lh8fwx9id46fcnfiw8g4hwrd6q9991wq9d")))

(define-public crate-stripper_interface-0.0.3 (c (n "stripper_interface") (v "0.0.3") (h "02lln5kh215imhlcsz1bz8zx8955k5jh9fmg63l8s4rzifd6xkxa")))

(define-public crate-stripper_interface-0.0.4 (c (n "stripper_interface") (v "0.0.4") (h "0243bgw5bvn70j2ig3chkiar3bbxvvggc1yif3zy29hwhv5cq77m")))

(define-public crate-stripper_interface-0.0.5 (c (n "stripper_interface") (v "0.0.5") (h "0mn5h3mfpdaxiy1q627m7rnpd1qrhlmln9r0zvifywm015wpz8cr")))

(define-public crate-stripper_interface-0.0.6 (c (n "stripper_interface") (v "0.0.6") (h "15nx6ah59xjxa1ap55rhplbi2hij00pkcv6wa9k6ylgxb97gslb3")))

