(define-module (crates-io st ri string_compressor) #:use-module (crates-io))

(define-public crate-string_compressor-0.1.0 (c (n "string_compressor") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)))) (h "1lnrmjias6xvy88q1vgz6hbn72a4ics7406s02chqwf6y7jm955h")))

(define-public crate-string_compressor-1.0.0 (c (n "string_compressor") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)))) (h "1j71inws8453n3y0kc25563w9pqr0mdv4zi5zizc39cvbz07yn9j")))

(define-public crate-string_compressor-1.0.1 (c (n "string_compressor") (v "1.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)))) (h "0sgziq5g21i82n8cdxdr38f56g6zz4fiw6lfgrgskjjm9jc5f4bj")))

