(define-module (crates-io st ri string_literal) #:use-module (crates-io))

(define-public crate-string_literal-0.1.0 (c (n "string_literal") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1rbjnq8c457dhjcxqbm281jw032wm6h47pik3377p5p4wf0sg2d7") (y #t)))

(define-public crate-string_literal-0.1.1 (c (n "string_literal") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0amgx7anm6dy1rcxkxjmfwbflqlza03c83z4j9nch30ahw9g4n70")))

