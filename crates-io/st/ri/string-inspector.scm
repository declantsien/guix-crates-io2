(define-module (crates-io st ri string-inspector) #:use-module (crates-io))

(define-public crate-string-inspector-0.0.1 (c (n "string-inspector") (v "0.0.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "1lrhyv3j2s1gy0kb1xwhjpvnviarbqzjmq10cx8bdw79qc5zdr7s")))

