(define-module (crates-io st ri string-utility) #:use-module (crates-io))

(define-public crate-string-utility-0.1.0 (c (n "string-utility") (v "0.1.0") (h "0dji0qg6fqc4l47hbsc62zhrxzv4ajbn6ix1bpp2lil0czqk4j3n") (r "1.64.0")))

(define-public crate-string-utility-0.2.0 (c (n "string-utility") (v "0.2.0") (h "1kms7z9305wymbp3d8fjlscr6h5h008cwk7hh277kisw27nrfwqj") (r "1.64.0")))

