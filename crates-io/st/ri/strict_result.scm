(define-module (crates-io st ri strict_result) #:use-module (crates-io))

(define-public crate-strict_result-1.0.0 (c (n "strict_result") (v "1.0.0") (h "07nyvwf51698n6z64bpcx29yz36bpdl3pw1x5cypzppjyk2slq9h")))

(define-public crate-strict_result-1.0.1 (c (n "strict_result") (v "1.0.1") (h "0cg24khrqnchzfymbk69hvd6l39hrc24283kjfll0vz0b5fmr1if")))

(define-public crate-strict_result-1.1.0 (c (n "strict_result") (v "1.1.0") (h "0y3xx9z5rnhabpqq7kw3x3p9139g91bvciv7p7k0lyckwnskc73j")))

(define-public crate-strict_result-1.2.0 (c (n "strict_result") (v "1.2.0") (h "1fjv5kscfjsbx811hcbj7x5yyi2par6b9cp0czjs2hqpd3lzlg81")))

