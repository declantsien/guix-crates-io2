(define-module (crates-io st ri string_io_and_mock) #:use-module (crates-io))

(define-public crate-string_io_and_mock-1.0.0 (c (n "string_io_and_mock") (v "1.0.0") (d (list (d (n "serial_test") (r "^2.0.0") (f (quote ("file_locks"))) (d #t) (k 0)))) (h "1d0q2jqdqwdf8kjv6sw7g9fsidg957km03vrgpjjbl9zrv7q4i93")))

(define-public crate-string_io_and_mock-1.0.1 (c (n "string_io_and_mock") (v "1.0.1") (d (list (d (n "serial_test") (r "^2.0.0") (f (quote ("file_locks"))) (d #t) (k 0)))) (h "04gbrl3zph43qqsbkrbs61q799j19ag2qszl295ygwcja6gc3n69")))

