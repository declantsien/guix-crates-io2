(define-module (crates-io st ri string_search) #:use-module (crates-io))

(define-public crate-string_search-1.0.0 (c (n "string_search") (v "1.0.0") (h "00kwqb3kwvhpjkx4mmpfmb24x8qwm1bw7jd282s9nv5fizpdbisx")))

(define-public crate-string_search-1.0.1 (c (n "string_search") (v "1.0.1") (h "1jl79cd607v8xpnvqkr9yg2z4ci47wcqvi042kdf8055y422cxfr")))

(define-public crate-string_search-1.0.2 (c (n "string_search") (v "1.0.2") (h "17i92im7z9qf5clnqfy49zykwj8zxyqd7xb90lq01p4f8c8pkh96")))

(define-public crate-string_search-1.0.3 (c (n "string_search") (v "1.0.3") (h "1hvljl3jdscwwl9p0p7f43s5p2r99lar8n3990vqi0jx8dij04pm")))

(define-public crate-string_search-1.0.4 (c (n "string_search") (v "1.0.4") (h "0zrqhgv27fbpcqnp8dkxrkmp69rlbpws87j90aiscfl3q0h081vx")))

