(define-module (crates-io st ri strict-yaml-rust) #:use-module (crates-io))

(define-public crate-strict-yaml-rust-0.1.0 (c (n "strict-yaml-rust") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1j242libish80r7w9n0m3iv0bv83s1ca8ih4kixwz2iqw9sadf9v")))

(define-public crate-strict-yaml-rust-0.1.1 (c (n "strict-yaml-rust") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1p3ck53bgcs69bf9y5ki4vrm3ijig5hdszc7v7415m6viav747jn")))

(define-public crate-strict-yaml-rust-0.1.2 (c (n "strict-yaml-rust") (v "0.1.2") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "18vi3y7q4d5r06ma35dryv22mblqb41xmlwqh56p0a4nnk3bxqsd")))

