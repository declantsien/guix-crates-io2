(define-module (crates-io st ri stringmetrics) #:use-module (crates-io))

(define-public crate-stringmetrics-0.1.6 (c (n "stringmetrics") (v "0.1.6") (h "09m0rz2ak6rgszpxfi32abppazg03cimvk6gni1w7h16xvi2nqdk")))

(define-public crate-stringmetrics-0.1.7 (c (n "stringmetrics") (v "0.1.7") (h "0npqspd7y5hn8jmgb8ipixq8hisw3baywqr34prfvmmcl0sjywvr")))

(define-public crate-stringmetrics-0.1.8 (c (n "stringmetrics") (v "0.1.8") (h "1bd1zrb8kl1i1gpmxw03r4176i2255p3xvpnxf6l895hrwl4yfiz")))

(define-public crate-stringmetrics-0.1.9 (c (n "stringmetrics") (v "0.1.9") (h "0pjrgzjb10yza3im4lhnnjjnx019c75anhhci85pay32b4iv54ia")))

(define-public crate-stringmetrics-0.1.10 (c (n "stringmetrics") (v "0.1.10") (h "1l36rwxj0cxazxa9rv9f10fkl5lsv0b162kvcirsclq3mnq2aqb6")))

(define-public crate-stringmetrics-0.1.12 (c (n "stringmetrics") (v "0.1.12") (h "0sqh5nvdk2k9ya8f3yd4mndbk9zb9d1m1fqj3z1gxrdm8sgv52pm")))

(define-public crate-stringmetrics-0.1.13 (c (n "stringmetrics") (v "0.1.13") (h "08lck8hdhy6b2ll7hw7n51bxcpl6z17v589p6jwpf0643vmxipp5")))

(define-public crate-stringmetrics-0.1.14 (c (n "stringmetrics") (v "0.1.14") (h "0zc8yhsjk1r8h2sxr09cdjhifscdvwlqij6214czhr3hqzk1hw7q")))

(define-public crate-stringmetrics-0.1.15 (c (n "stringmetrics") (v "0.1.15") (h "1vrz53z5w9qirfx7qz5p6hkdir4ncjmcv6vcxfrw69zkqzk02bw6")))

(define-public crate-stringmetrics-1.0.0 (c (n "stringmetrics") (v "1.0.0") (h "1gls981bm4w92n69jicrwpynxaab2c2df1k4f9h2hp9cqdw5f1h4")))

(define-public crate-stringmetrics-1.0.1 (c (n "stringmetrics") (v "1.0.1") (h "0kmy3i9ax5759mbx3fj8yixwy31r6q47kp3r63wqhnwkgb34d980")))

(define-public crate-stringmetrics-1.0.2 (c (n "stringmetrics") (v "1.0.2") (h "1yzycipvf4y0l3jsv5hhcfqv4jnqbasir7slry61l4bj4sz6bpnw")))

(define-public crate-stringmetrics-1.1.0 (c (n "stringmetrics") (v "1.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "065yh5bn6jbnxva74vnklgh7fzyyplhnsn538j8i171k9cji5823")))

(define-public crate-stringmetrics-1.1.1 (c (n "stringmetrics") (v "1.1.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "05kmlqg06vxk1pp3b67drix93h1bb3qrgj0lglkljyigmi633lcz")))

(define-public crate-stringmetrics-1.1.2 (c (n "stringmetrics") (v "1.1.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1vygd2bh4lpy98q325k95fdml0vdkbjmalxm8x8mlxim481zs4cn")))

(define-public crate-stringmetrics-2.0.0 (c (n "stringmetrics") (v "2.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "13hmc4l8yzs414kn89zcrwvhkn1q79xdhfsy02brf3b07qkl99xq")))

(define-public crate-stringmetrics-2.0.1 (c (n "stringmetrics") (v "2.0.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "16r7mgx1q5pv8vbgwwrr3ycnzxwgc10q62p05bv5r46kc3bb15kd")))

(define-public crate-stringmetrics-2.0.2 (c (n "stringmetrics") (v "2.0.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1sjgxci1kp6mh6a2f64zrzw690xi5f50i9r44azr4vd72psjw6nb")))

(define-public crate-stringmetrics-2.0.4 (c (n "stringmetrics") (v "2.0.4") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1vhx1sigx6zjnr0qh8qpa6f9lgm7ki4ybrbz9ybxs7jfzdpik86a")))

(define-public crate-stringmetrics-2.0.5 (c (n "stringmetrics") (v "2.0.5") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1552w9a8svw7xi217vba4bp5b14x2mk4vv0gmmxrbb7654gg0d0g")))

(define-public crate-stringmetrics-2.0.6 (c (n "stringmetrics") (v "2.0.6") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "028ikz42pm0s4wcy91lm9cy0ip9kwy16i4i1ad9lxzm4d0m2y2n0")))

(define-public crate-stringmetrics-2.0.7 (c (n "stringmetrics") (v "2.0.7") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1i1xr60hwf51fj9g8mxyyq8y8qd98g8bym1nr18rwmrp6hw11087")))

(define-public crate-stringmetrics-2.1.0 (c (n "stringmetrics") (v "2.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "003f263fg8h92cz5brghswkc3xjai1sd5jxcx85k5inaam1hz7k1")))

(define-public crate-stringmetrics-2.1.1 (c (n "stringmetrics") (v "2.1.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1lhf38vg593zyj2630fcvzfs089vgasifmjgnbkbawwz7n3v89v8")))

(define-public crate-stringmetrics-2.1.2 (c (n "stringmetrics") (v "2.1.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "117szjgif58ayq638cybjiz947qbw0xmilmpfg3bas17a08vhc5p")))

(define-public crate-stringmetrics-2.1.3 (c (n "stringmetrics") (v "2.1.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "01k70ff6chpr7r9i0cr48plmbv22q7dsm3fx8nkxkfdrw6m7fb7g")))

(define-public crate-stringmetrics-2.2.0 (c (n "stringmetrics") (v "2.2.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "04l3dk25y9acw322id89c1kfh5g8642frj5xzj8vv7a9nwxbrj68") (f (quote (("bench"))))))

(define-public crate-stringmetrics-2.2.1 (c (n "stringmetrics") (v "2.2.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "12i5vqm3a2y1i9p6s2nhaxmwb84ji3ip4fb32k3rh9iwj0n39yv7") (f (quote (("bench"))))))

(define-public crate-stringmetrics-2.2.2 (c (n "stringmetrics") (v "2.2.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "0rcpyfagysrwsass4nlwf7frwcd7h1bfr38bc2xmq94nrmkqcg3v") (f (quote (("bench"))))))

