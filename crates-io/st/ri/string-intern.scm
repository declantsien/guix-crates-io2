(define-module (crates-io st ri string-intern) #:use-module (crates-io))

(define-public crate-string-intern-0.1.0 (c (n "string-intern") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "13kqarfhaps300cyi5521iqgxg813zj1wrhly1kkwvzr0nklc51r")))

(define-public crate-string-intern-0.1.1 (c (n "string-intern") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "04hvybp1vcw8svv337xg8b6vd10wlqrjpd82n90zxknk696ypdpw")))

(define-public crate-string-intern-0.1.2 (c (n "string-intern") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0fkq9kx15i6jhhqa6frjns642xi8kswdkpc0m7sgg24v84d4ka7z")))

(define-public crate-string-intern-0.1.3 (c (n "string-intern") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1qwsvnfw7ypayrignfk32hnhimpabv8sn4w61ydlvagrfi7f2a0s")))

(define-public crate-string-intern-0.1.4 (c (n "string-intern") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "02pkhncy62r819407jfx6qzk6nhhyrq3kk71jyi1knp616bw8g9n")))

(define-public crate-string-intern-0.1.5 (c (n "string-intern") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 2)))) (h "07qjmiymi47h461gpyazrm34s74mm8sl53kranw914f2y0r9yg2s")))

(define-public crate-string-intern-0.1.6 (c (n "string-intern") (v "0.1.6") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 2)))) (h "0wqpmikxsw4xy3ikgcyqkc1cd5410jwwzwpwafi3pdp7adxx9rq3")))

(define-public crate-string-intern-0.1.7 (c (n "string-intern") (v "0.1.7") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 2)))) (h "1vh9b7926zh1qb8r5d7adpwni85wx42i7yby5q5cpz3fphi0yy3a") (f (quote (("default" "rustc-serialize" "serde"))))))

