(define-module (crates-io st ri string_finder) #:use-module (crates-io))

(define-public crate-string_finder-0.1.0 (c (n "string_finder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "0ipmnyrm33dn78yh7ncdccbvgvbgwcj0iwl5npnfr2ccx6m1sc0m")))

