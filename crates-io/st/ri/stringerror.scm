(define-module (crates-io st ri stringerror) #:use-module (crates-io))

(define-public crate-stringerror-0.1.1 (c (n "stringerror") (v "0.1.1") (h "0nza0nvy61y3yzh3kc24g3m7nm5if71kxrw9wr2blydpplq1a4j2")))

(define-public crate-stringerror-0.1.2 (c (n "stringerror") (v "0.1.2") (h "00d714cis0kynnayyfnsrf4mjhkffjm3hiwx7lphn9yiqsd9p18l")))

