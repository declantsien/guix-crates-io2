(define-module (crates-io st ri string-parser) #:use-module (crates-io))

(define-public crate-string-parser-0.1.0 (c (n "string-parser") (v "0.1.0") (h "0jm6p083y62grwbx9g90f0y8xiwxw258c7i026im7wphrapldz7g")))

(define-public crate-string-parser-0.1.1 (c (n "string-parser") (v "0.1.1") (h "1ga8ixlx5mcpjrn7f1cwmylic90576yswrv724zgfpij44i8xizr")))

(define-public crate-string-parser-0.1.2 (c (n "string-parser") (v "0.1.2") (h "0may68wa5byagx289k1cyrp93j9j6s6dlw5jmych96jhjccn12rn")))

(define-public crate-string-parser-0.1.3 (c (n "string-parser") (v "0.1.3") (h "0k2ss7cs6r2lxbkbhzg4b30mf38mz33x5c50dhvicr5p66qi5a2x")))

(define-public crate-string-parser-0.1.4 (c (n "string-parser") (v "0.1.4") (h "1mwmwklkz7zfl3cnn97gydg6m8zxarbskp54hy065j6jrn9x9y85")))

(define-public crate-string-parser-0.1.5 (c (n "string-parser") (v "0.1.5") (h "15imw8vhlfh2bi77pax7h7lr9an8larq1kbc7xkx8b5ddx37kq26")))

(define-public crate-string-parser-0.1.6 (c (n "string-parser") (v "0.1.6") (h "044czhdlibpf2kk4b1ajz6gsrx26k5nqcfxn5xj6jab8ps5jl0mw") (y #t)))

(define-public crate-string-parser-0.1.7 (c (n "string-parser") (v "0.1.7") (h "0zph923vw9kcxik9maqqx64ylnvmrvjqm4dvi0mj40xrid249yq6") (y #t)))

(define-public crate-string-parser-0.1.8 (c (n "string-parser") (v "0.1.8") (h "16d147j7v7n55x51qy9c7pg9gxpxxy9p925w78032s5vma0149rm") (y #t)))

