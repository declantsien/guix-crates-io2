(define-module (crates-io st ri stringmap) #:use-module (crates-io))

(define-public crate-stringmap-0.1.0 (c (n "stringmap") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rusty-cheddar") (r "^0.3") (d #t) (k 1)))) (h "09fknk1z395jy02nvzv1acl8gnswn6jyc756185m1xld5vb5ss7g")))

