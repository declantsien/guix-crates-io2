(define-module (crates-io st ri strip-ansi-cli) #:use-module (crates-io))

(define-public crate-strip-ansi-cli-0.0.0 (c (n "strip-ansi-cli") (v "0.0.0") (d (list (d (n "pipe-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "structopt-utilities") (r "^0.0.7") (d #t) (k 0)))) (h "1kgrxpisxyk8m1knidngmay9s69296xbbqh2xp4a7qihp7xc9vka")))

(define-public crate-strip-ansi-cli-0.1.0 (c (n "strip-ansi-cli") (v "0.1.0") (d (list (d (n "pipe-trait") (r "^0.2.1") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "structopt-utilities") (r "^0.0.8") (d #t) (k 0)))) (h "0id2d1bc1brk8arbgnaw7czlmbd1d20rrx55svb6bcmx045hpphz")))

