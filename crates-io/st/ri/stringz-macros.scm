(define-module (crates-io st ri stringz-macros) #:use-module (crates-io))

(define-public crate-stringz-macros-0.1.0 (c (n "stringz-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0b6r16gs09a9kcw9xnsw01s1m14mx2nsig39m53b5qy8m6bgffxd") (y #t)))

(define-public crate-stringz-macros-0.1.1 (c (n "stringz-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02858j13hdgda590d9zc901dzxsk7053747ijiyn00x9qh5fiv3s")))

(define-public crate-stringz-macros-0.1.2 (c (n "stringz-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cgpnzqdmd98k769sk4hlw2jjzal27hn5z9cq97rfas73i0fbrki")))

(define-public crate-stringz-macros-0.2.0-alpha (c (n "stringz-macros") (v "0.2.0-alpha") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p682788x0lmj9vrhirgc35xyhbdy716li73lpwdbpgs1bm5lnk1")))

(define-public crate-stringz-macros-0.2.0 (c (n "stringz-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "017awiyhg6x8nzsxnp48ccbd9h5sh45vhyn3f4nv982kib36jq20")))

