(define-module (crates-io st ri string-err) #:use-module (crates-io))

(define-public crate-string-err-0.1.0 (c (n "string-err") (v "0.1.0") (h "0y340fgg0spwwj64zbikp7z98a3cxj60rmvcv83mbz3gi5nw8fyk")))

(define-public crate-string-err-0.1.1 (c (n "string-err") (v "0.1.1") (h "1laqhfcgz3xc250mhqqy5y0jdykr4c39b9na8n4iflx6sb6cbdjy")))

