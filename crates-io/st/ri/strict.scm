(define-module (crates-io st ri strict) #:use-module (crates-io))

(define-public crate-strict-0.1.0 (c (n "strict") (v "0.1.0") (h "1y7474x4r3qnqmfrmj0vgz6lwjdjclwfs1pp4nygvb7frd9v294b")))

(define-public crate-strict-0.1.1 (c (n "strict") (v "0.1.1") (h "1jsj5pi4swa88sv5xrggg02z2jq2lyvqw9622kanwc0yfiw9vz18")))

(define-public crate-strict-0.1.2 (c (n "strict") (v "0.1.2") (h "13hzb4yfigqj4i58wlwcjaarblpl41pkfl80pbr9cs7pg8320y2f")))

(define-public crate-strict-0.1.3 (c (n "strict") (v "0.1.3") (h "0lsdxrpyx9frqy3jp4j3yraw03y0fi9mc7pv36clbwzw8pn15z42")))

(define-public crate-strict-0.1.4 (c (n "strict") (v "0.1.4") (h "1vphysahrh16nj4k3ldw9qqjqjcada7pzj5bfq62nlfhif7za6lr")))

(define-public crate-strict-0.2.0 (c (n "strict") (v "0.2.0") (h "01j0h28xzg07kd1km5m0wz88asp6hwh45n8q8bdkjymqlpz4897l") (r "1.56")))

