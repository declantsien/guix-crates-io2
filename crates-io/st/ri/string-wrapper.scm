(define-module (crates-io st ri string-wrapper) #:use-module (crates-io))

(define-public crate-string-wrapper-0.1.0 (c (n "string-wrapper") (v "0.1.0") (h "16d3ffy5mzn8v1s8j3am6y5bxdh11jmhlwz8j0rbq1ckfij1b72b") (y #t)))

(define-public crate-string-wrapper-0.1.1 (c (n "string-wrapper") (v "0.1.1") (h "1wz6bc4s4y90ws056bscjarw2a07ps5phdg048arsqw5drqlxjyp")))

(define-public crate-string-wrapper-0.1.2 (c (n "string-wrapper") (v "0.1.2") (h "06jblv2nzdwlvdah6bmjb28vb2qzbmx8bs6r24l1g90fp9r1bjs8")))

(define-public crate-string-wrapper-0.1.3 (c (n "string-wrapper") (v "0.1.3") (h "00f067nyjh6cyrcnfw8xfwfx8n0x0kxg0z519z4jalhqm2q95pa6")))

(define-public crate-string-wrapper-0.1.4 (c (n "string-wrapper") (v "0.1.4") (h "127bkjnbygqdkiw77xw86xy8a8l324f8ia5yq5xwzkdyxclvrypl")))

(define-public crate-string-wrapper-0.1.5 (c (n "string-wrapper") (v "0.1.5") (h "15gng3nyk4bn6rq1dzh4zffppljs3ikxyljcxsd1d85g23bb0knf")))

(define-public crate-string-wrapper-0.1.6 (c (n "string-wrapper") (v "0.1.6") (d (list (d (n "serde") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 2)))) (h "1mhwxibwssvvp87xb5ggdd51337wkj9y6if4gjhgn4gd9pcyfmhv") (f (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-string-wrapper-0.1.7 (c (n "string-wrapper") (v "0.1.7") (d (list (d (n "serde") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 2)))) (h "1x39059sd6qkj34zizhxhrzjvsslnw03j8wiqba5rj8j6rahns6h") (f (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-string-wrapper-0.2.0 (c (n "string-wrapper") (v "0.2.0") (d (list (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)))) (h "0a5g1wphdjkvj4lrklm3dm97v3qwmrbn7pk59879zv8q0yqsv5az") (f (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-string-wrapper-0.3.0 (c (n "string-wrapper") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1304cf2rx6zzjmhchzpw7967vpxlp1if5fry4zgphzzk5sf1lccz") (f (quote (("use_serde" "serde" "serde_derive"))))))

