(define-module (crates-io st ri string-sharp) #:use-module (crates-io))

(define-public crate-string-sharp-0.1.0 (c (n "string-sharp") (v "0.1.0") (h "130798c4l6bvwiqfb37kj0ggcnzaljirpsqmdg0y226qbsglfd08")))

(define-public crate-string-sharp-0.1.1 (c (n "string-sharp") (v "0.1.1") (h "18dz4ygx3lsrlnpbwn1k3v5xyv550rfnxv2pjpim5kzq4bldmqp1")))

(define-public crate-string-sharp-0.1.2 (c (n "string-sharp") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "1p4axmpyah4v0cwkk6dl683p3zr6s01q186b5c7fkv7v04cka91d")))

(define-public crate-string-sharp-0.1.3 (c (n "string-sharp") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "1390hrk24kwrk392ha94i6fymlbdiz7vqwb2pnprmvb0a388yifl")))

(define-public crate-string-sharp-0.1.4 (c (n "string-sharp") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "19xwjslhgf4mjb90f35sajaqvna6adkd0kh13i1x3jjcq4y5ppgl")))

(define-public crate-string-sharp-0.1.5 (c (n "string-sharp") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "18qbxw2rsz7fcpz2yszdj9g67v75ibg03bafd1dvx5qmhsb09xic")))

(define-public crate-string-sharp-0.1.7 (c (n "string-sharp") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "083jxxsfn1j82xkx9jjkx70768gf4g3zlhnzpy0ihgymjcg3vwqj")))

(define-public crate-string-sharp-0.1.8 (c (n "string-sharp") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "0zx2z4dyk10wf0zakxqx4ikxjsqcmgimmigz2bpk7lqdgs0plm56")))

(define-public crate-string-sharp-0.1.9 (c (n "string-sharp") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "1gzn9jfxnawyyyxdwm2hinr8mr8x20n59fzzqjy85qa4spdf8xg8")))

(define-public crate-string-sharp-0.1.10 (c (n "string-sharp") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "0wawjyb5l9vid6r84cjsw7qfkqfrfs9hr86d2pz622xaj1ii88js")))

