(define-module (crates-io st ri stripng) #:use-module (crates-io))

(define-public crate-stripng-0.1.0 (c (n "stripng") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "iowrap") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0c3s1jykpmpk1vrzrj2xdlvxwhf4j76hpk5srrp751wbj79byfq2")))

