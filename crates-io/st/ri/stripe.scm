(define-module (crates-io st ri stripe) #:use-module (crates-io))

(define-public crate-stripe-0.0.1 (c (n "stripe") (v "0.0.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1rxik9i426dn5s6gbxhgs9a6snii8bb101lw4xrnj70g8gb60gv8")))

(define-public crate-stripe-0.0.3 (c (n "stripe") (v "0.0.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "12nzbrbb02y2hdfgjbw7is8x0y99c5f7qwzsjba5bx6vch00zv1l")))

(define-public crate-stripe-0.0.4 (c (n "stripe") (v "0.0.4") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0spmzyldfwk3lm1l2cb0wzqj1w7726kf3fpar2qk6l09xs0d3zhm")))

(define-public crate-stripe-0.0.5 (c (n "stripe") (v "0.0.5") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1sf4r5fb650wjjs4mk1wbh8yj9z3qrh2qs0mrnxwkbnqkqmgphwc")))

