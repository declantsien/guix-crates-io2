(define-module (crates-io st ri stringy) #:use-module (crates-io))

(define-public crate-stringy-0.1.0 (c (n "stringy") (v "0.1.0") (h "1gaccqp9n9wrp633jlh6nx0460fwpgw81gacs8akng2wvarclh4m")))

(define-public crate-stringy-0.1.1 (c (n "stringy") (v "0.1.1") (h "156l3dg62rs9m5fz1sfvblk9984swjkk8zbal7rbflspblrvnj9a")))

(define-public crate-stringy-0.1.2 (c (n "stringy") (v "0.1.2") (h "13p2b82c42y5hwha6qk3c4jbnc30fh7mq37j4w5097apxx1iails")))

(define-public crate-stringy-0.2.0 (c (n "stringy") (v "0.2.0") (h "1vr6sw89nrkjix86wfi1j6n5n1h1blsyy760qvq601rqzmjd66bl")))

(define-public crate-stringy-0.2.1 (c (n "stringy") (v "0.2.1") (h "00llczlv3k33al22slv4ssy5ly97lsxbjaf4madqkdnwdwnkay3m")))

(define-public crate-stringy-0.2.2 (c (n "stringy") (v "0.2.2") (h "1z9zvm2zsirwnlcsj0z3d95srnz0sgdbaabsihz4r0z1lzz2dvdr")))

