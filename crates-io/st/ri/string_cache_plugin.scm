(define-module (crates-io st ri string_cache_plugin) #:use-module (crates-io))

(define-public crate-string_cache_plugin-0.1.0 (c (n "string_cache_plugin") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "1mrrbwn8pm1rc7vm99qbzfz765y6kxfhkh9si29q1my4gv094imq")))

(define-public crate-string_cache_plugin-0.1.1 (c (n "string_cache_plugin") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.1.0") (d #t) (k 0)))) (h "0zz8gx85f89fzha9axxvgca03phn2jvm4yag12x1milvk4bxpmyd")))

(define-public crate-string_cache_plugin-0.1.2 (c (n "string_cache_plugin") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.1.0") (d #t) (k 0)))) (h "0za60vr2s3mqczq2x3f8vy8yb1fxbkdjc8kx768ig1d1f5fifysv")))

(define-public crate-string_cache_plugin-0.1.3 (c (n "string_cache_plugin") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.1.0") (d #t) (k 0)))) (h "103hzx1gpji1ls9nqbydifqayrhclk5q2hpl3j1553c8qwcdr6f8")))

(define-public crate-string_cache_plugin-0.1.4 (c (n "string_cache_plugin") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.1.0") (d #t) (k 0)))) (h "0q6psi14z6hyq2bcps3gfs888i6yavwz0j2v825281rxvizkjyzc")))

(define-public crate-string_cache_plugin-0.1.5 (c (n "string_cache_plugin") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.1.0") (d #t) (k 0)))) (h "0d1spjkix3cb16m7p24agfh4jyiw7s1wfxzg7sw7azhqq2rb1s7q")))

(define-public crate-string_cache_plugin-0.1.6 (c (n "string_cache_plugin") (v "0.1.6") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.1.0") (d #t) (k 0)))) (h "0gf0rdjhdyn313zags4prr02v2qwqf3r7nv0qca8i0i4lmvp4mxf")))

(define-public crate-string_cache_plugin-0.1.7 (c (n "string_cache_plugin") (v "0.1.7") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.1.6") (d #t) (k 0)))) (h "1ybqqgcjldjxllm2rkn4bpjkhbhhd1vx6jxqs8gis2bblmyxs9qw")))

(define-public crate-string_cache_plugin-0.1.8 (c (n "string_cache_plugin") (v "0.1.8") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.1.8") (d #t) (k 0)))) (h "1jlgzmcz35mhx1m8xvxyx5ymzv24f4lbydr3f2svivmc5sslg7sp")))

(define-public crate-string_cache_plugin-0.1.9 (c (n "string_cache_plugin") (v "0.1.9") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.1.9") (d #t) (k 0)))) (h "1jzm9imz5d9339raib4mzl482prm7dyvg2x1ik7kyl3fnp6vg8lb")))

(define-public crate-string_cache_plugin-0.1.10 (c (n "string_cache_plugin") (v "0.1.10") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)) (d (n "string_cache_shared") (r "^0.1.9") (d #t) (k 0)))) (h "0mj2in4kss14c6mi3jhlm00xlh4ak22chziyjxjzbf2r5y4m0296")))

