(define-module (crates-io st ri string_table) #:use-module (crates-io))

(define-public crate-string_table-1.0.0 (c (n "string_table") (v "1.0.0") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0") (d #t) (k 0)))) (h "1s1khyah7hxl40r5rc52xi4pfdp8gvl3z8cgs7larn5dc7cjw5px")))

