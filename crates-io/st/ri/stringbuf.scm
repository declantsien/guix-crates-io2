(define-module (crates-io st ri stringbuf) #:use-module (crates-io))

(define-public crate-stringbuf-0.1.0 (c (n "stringbuf") (v "0.1.0") (h "0lz7mb008rc50zhcfyl40lbrqqrdjri1nb5wnpgink1hcd6k7vs9") (y #t)))

(define-public crate-stringbuf-0.1.1 (c (n "stringbuf") (v "0.1.1") (h "01fd4hv31d7m2qra05azn882rybkap3dvd0mjv8xahf1sh72z14s") (y #t)))

