(define-module (crates-io st ri string-patterns) #:use-module (crates-io))

(define-public crate-string-patterns-0.1.0 (c (n "string-patterns") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "003ggpcnfknszc5mq6pljlg1p59wr8krvmbkkmac5xsmq88a9069") (y #t)))

(define-public crate-string-patterns-0.1.1 (c (n "string-patterns") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1drv09ljwigiaigvjcpbcw5lbx5f1p1crdb1gd9igjilqclsza47") (y #t)))

(define-public crate-string-patterns-0.1.2 (c (n "string-patterns") (v "0.1.2") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "08bak0jmb30j9x9dk0hszy1mz4gbsn86jdkn036h35vngrdb9bb6") (y #t)))

(define-public crate-string-patterns-0.1.3 (c (n "string-patterns") (v "0.1.3") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0k3nbmjrwrw4i7wbvd68yrfj72y9zi41mqpaxajh5iwb5issbfpr") (y #t)))

(define-public crate-string-patterns-0.1.4 (c (n "string-patterns") (v "0.1.4") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1xayi27lm2dfqrhj8h1if1b0xxqvqj76wdh6gs0l164bdngfmwbs") (y #t)))

(define-public crate-string-patterns-0.1.5 (c (n "string-patterns") (v "0.1.5") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0g417kh5ligaxh2x2ikkjqrbx61dp39a9nz3kvs11d31mf1pl81r") (y #t)))

(define-public crate-string-patterns-0.1.6 (c (n "string-patterns") (v "0.1.6") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0h9av9qd5axn10gayydgpwwnn8bgpxra1jfasvx1gqcj62hdlz5k") (y #t)))

(define-public crate-string-patterns-0.1.7 (c (n "string-patterns") (v "0.1.7") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0zkflnyrinvb8l5wrwpsk3i8kcwilz82wa1q1sa7k01k0m6ncm29") (y #t)))

(define-public crate-string-patterns-0.1.8 (c (n "string-patterns") (v "0.1.8") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "18fhgg107yba93x762k1wh82nnxghqykn4irxqnn6l0j6aan6ixn") (y #t)))

(define-public crate-string-patterns-0.1.9 (c (n "string-patterns") (v "0.1.9") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "116lcvnjm7xx9qqk8qfjg7cm8fbikm57nbxvkvc9m8ngb5dk60nn") (y #t)))

(define-public crate-string-patterns-0.2.0 (c (n "string-patterns") (v "0.2.0") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "17h7nwxkwndscjblm2vsyzs907yxg2nj9bqs17mg2arvsql8fac4") (y #t)))

(define-public crate-string-patterns-0.2.1 (c (n "string-patterns") (v "0.2.1") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0j2xacc13siwahmac6zgl9m1dwbh411xq0wv9mkh943dp3vgscyx") (y #t)))

(define-public crate-string-patterns-0.2.2 (c (n "string-patterns") (v "0.2.2") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1r6ca9apq9xfdvfvwqfxq9q76m2fcwrbqq1wmfhnhxwbyadvnmv5") (y #t)))

(define-public crate-string-patterns-0.2.3 (c (n "string-patterns") (v "0.2.3") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1i7h0dvr6zvxh5mix0k6qizch8cfdm5xmmx7xiic7xlqvqv8aajk") (y #t)))

(define-public crate-string-patterns-0.2.4 (c (n "string-patterns") (v "0.2.4") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "172l2vdsldijpqn1vff9qsdjy87lx0rah250sm59jcmjxn5wcymk") (y #t)))

(define-public crate-string-patterns-0.2.5 (c (n "string-patterns") (v "0.2.5") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0m420cxpc98zw23divi0hslvfl2hn7k7dppv2nypyyqgv8ca9ykv") (y #t)))

(define-public crate-string-patterns-0.2.6 (c (n "string-patterns") (v "0.2.6") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "00banzz03kflzna615glg1jwcal7ww4svmmpjsw515xcpx7yjvbn") (y #t)))

(define-public crate-string-patterns-0.2.7 (c (n "string-patterns") (v "0.2.7") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1fgiq0bxwi6qql51cz6n6r8abh3f1mkhb2zmd5wlbjh4a84j85hr") (y #t)))

(define-public crate-string-patterns-0.2.8 (c (n "string-patterns") (v "0.2.8") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0zx9sl924q6zdazzr88kcdss78f5g0sj6h4s811bfldkn0rdk308") (y #t)))

(define-public crate-string-patterns-0.2.9 (c (n "string-patterns") (v "0.2.9") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0b7z545mwh0y5wza5d0wpvbzszqlfs8y0sk9n4wzrqar5v4k23x0") (y #t)))

(define-public crate-string-patterns-0.2.10 (c (n "string-patterns") (v "0.2.10") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1zv6a4m91wphq2anz1pjcng6ly1485f8165qv3zhk0mj4i35snz2") (y #t)))

(define-public crate-string-patterns-0.2.11 (c (n "string-patterns") (v "0.2.11") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1vqzbxkhh5mh4xavcxmlmm2v4gmqvd91skrvaap67zckh5grnmcn") (y #t)))

(define-public crate-string-patterns-0.2.12 (c (n "string-patterns") (v "0.2.12") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0cy2n98a0mxpsmi5hhhirhd91pccq96iw5qlnldmx9rv14nk8ji7") (y #t)))

(define-public crate-string-patterns-0.2.13 (c (n "string-patterns") (v "0.2.13") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0rrd0khph91xdvak58qibgc6lr4237j3p45kli03mz90fnzqnx4h") (y #t)))

(define-public crate-string-patterns-0.2.14 (c (n "string-patterns") (v "0.2.14") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1krlmzylcvn24n3zhc6fap9b2cnrmy7j1ikgyx3npzxndhhkjxc2") (y #t)))

(define-public crate-string-patterns-0.2.15 (c (n "string-patterns") (v "0.2.15") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "12hn5k7swdyzwrpq6xk5kjd9wl02wi2wvjalddls19d3m93y8p2a") (y #t)))

(define-public crate-string-patterns-0.2.16 (c (n "string-patterns") (v "0.2.16") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "11njk93pxzmphr2vmaj0m0j02qz73k2b18csah7168vcyim2c9gn") (y #t)))

(define-public crate-string-patterns-0.2.17 (c (n "string-patterns") (v "0.2.17") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1sicbb8hy1ryyqx99fp5h8109cy9g380kwar7izcfqif7b88l2nm") (y #t)))

(define-public crate-string-patterns-0.2.18 (c (n "string-patterns") (v "0.2.18") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0yy0lxfnp2k4amsf7ljyngmyvf1vh94lr2fnshijsa43gxkad0q2") (y #t)))

(define-public crate-string-patterns-0.2.19 (c (n "string-patterns") (v "0.2.19") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1jkb06jnqr7h4cbazmlpg6hqhpj208f2n4gzbwlk8j7kq1965092") (y #t)))

(define-public crate-string-patterns-0.2.20 (c (n "string-patterns") (v "0.2.20") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1pjwykd4lw3srdjdzmxg5n18g6g0lh7cch04ik3cxdw5rmazzrrn") (y #t)))

(define-public crate-string-patterns-0.2.21 (c (n "string-patterns") (v "0.2.21") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "184l6x4c9ng9wyr97g9ynlmc9vh3q13l1sw805wj2hmfgy22djjs") (y #t)))

(define-public crate-string-patterns-0.2.22 (c (n "string-patterns") (v "0.2.22") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0kzhd7ax1gl46lb4jylr318arw443g0x9v1jc3vs3d5nz6iivynj") (y #t)))

(define-public crate-string-patterns-0.2.23 (c (n "string-patterns") (v "0.2.23") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1rrbd6c1dhjnf4ki2s90a0sxcrdkrlg82q7yh6dni9gjb2njj7w2") (y #t)))

(define-public crate-string-patterns-0.2.24 (c (n "string-patterns") (v "0.2.24") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0hhcj69g7kafi3gj3achx6q1x0spnchpw2wxc192nfxdg1pr0s4g") (y #t)))

(define-public crate-string-patterns-0.2.25 (c (n "string-patterns") (v "0.2.25") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1cjwzvvbj1hp612r45nr5hkqm8i29z0r5vgig498s19d2ckrifly") (y #t)))

(define-public crate-string-patterns-0.2.26 (c (n "string-patterns") (v "0.2.26") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1pf0gyayqlv5j89ii5lay8ngi3445ml4x3n5wy0mg6ij7lfgvjxc") (y #t)))

(define-public crate-string-patterns-0.2.27 (c (n "string-patterns") (v "0.2.27") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1w9kbrsrjivhs167qqylxbcmnkzw9saagf14rj8cpkbjjr62q9sw") (y #t)))

(define-public crate-string-patterns-0.2.28 (c (n "string-patterns") (v "0.2.28") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0bnajwpnwwirxzw5n3450g21vpbcid6kk1v4k1i8cpmk86sppgin") (y #t)))

(define-public crate-string-patterns-0.2.29 (c (n "string-patterns") (v "0.2.29") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "14w15ghjdf0083ismzxrndzywa98ckr4cdmakkls8sknq90ly15r") (y #t)))

(define-public crate-string-patterns-0.2.30 (c (n "string-patterns") (v "0.2.30") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0kwphxdr0r91y4504a8pmcxqvzijjc6bj771lm129i46prff700b") (y #t)))

(define-public crate-string-patterns-0.2.31 (c (n "string-patterns") (v "0.2.31") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1ji9z9g90jnb3kdsn1ymn8ihnx3q45xfgqpn067alkff3fgdf8nb")))

(define-public crate-string-patterns-0.3.0 (c (n "string-patterns") (v "0.3.0") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1ypf58zck0bl6jvjz2bgr6wnabyy20nplgn52si7d1bblzi640dj") (y #t)))

(define-public crate-string-patterns-0.3.1 (c (n "string-patterns") (v "0.3.1") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "027zgrr2srlvvqkj8lv2lanf768dfyg6vr4y2livd4lyxcwdjyzz") (y #t)))

(define-public crate-string-patterns-0.3.2 (c (n "string-patterns") (v "0.3.2") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1vb0kp9k84kflvykqi5xadk6575hb9ns30dpvhpn9z68yrcxjwa1") (y #t)))

(define-public crate-string-patterns-0.3.3 (c (n "string-patterns") (v "0.3.3") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1y9740y4crd2kjswsnlizl3iydz0sv4sdcjr3bh7519gz9lkcwdh") (y #t)))

(define-public crate-string-patterns-0.3.4 (c (n "string-patterns") (v "0.3.4") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1ph1zrsdp7zspcrl4xa7k4r9h4x7ljwdyhvg1n4b3hr69mmmy97j") (y #t)))

(define-public crate-string-patterns-0.3.5 (c (n "string-patterns") (v "0.3.5") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "05dhj0jmgh81ap3ajnhnajhsbp31ma95dx5c0gqj89d6rnrng62n") (y #t)))

(define-public crate-string-patterns-0.3.6 (c (n "string-patterns") (v "0.3.6") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1cs7n1dfzg950s7s1vzrn4x4nymp1nbi7pr1hr72fi44zi63wb9s") (y #t)))

(define-public crate-string-patterns-0.3.7 (c (n "string-patterns") (v "0.3.7") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1jsbql24caagddnmn07ynv6n6r0px3aj2mvhv55yj4xq4642vlac") (y #t)))

(define-public crate-string-patterns-0.3.8 (c (n "string-patterns") (v "0.3.8") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "18jny85hmg759a15wcvn1d33wlnix6ybr5qk8d1hsz34yy6f9k8d")))

(define-public crate-string-patterns-0.3.9 (c (n "string-patterns") (v "0.3.9") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "103hq7pg77swds5ry22akllllm9flzffgmmi26n433f7g4n3pzxn")))

