(define-module (crates-io st ri string-newtype) #:use-module (crates-io))

(define-public crate-string-newtype-0.1.0 (c (n "string-newtype") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0cincrs8li2b0g3a59rgp9c49xwv1ybwkryvrg8i6lw4hjihcxpd") (s 2) (e (quote (("smol_str" "dep:smol_str") ("serde" "dep:serde")))) (r "1.68")))

(define-public crate-string-newtype-0.1.1 (c (n "string-newtype") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0sbr8g4mmzy5xpr3zp609afcj2d2sn1xw8nn0mdgidq27s6vv7hx") (s 2) (e (quote (("smol_str" "dep:smol_str") ("serde" "dep:serde")))) (r "1.68")))

