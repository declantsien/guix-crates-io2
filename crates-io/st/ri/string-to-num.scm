(define-module (crates-io st ri string-to-num) #:use-module (crates-io))

(define-public crate-string-to-num-0.1.1 (c (n "string-to-num") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0z023g3izh125357r5z19i2yc09vpd0vqbh866l0wdv197mx5iql") (y #t)))

(define-public crate-string-to-num-0.1.2 (c (n "string-to-num") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "05012zxsvxm8kvjgjkqny3glqnc6lh81b7l0mlwh4cpwpfjvaxbg")))

