(define-module (crates-io st ri stripper) #:use-module (crates-io))

(define-public crate-stripper-0.1.0 (c (n "stripper") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "09lqg9l46ibkjf07rc9d917wd248584v88yrjakh6j80m55l2vbb")))

(define-public crate-stripper-0.1.1 (c (n "stripper") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1cxyzfwivmfhvdqd2k870fl97cjg34q6slp3m8zcmpl4m58hx6j9")))

