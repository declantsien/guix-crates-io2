(define-module (crates-io st ri string_cache_shared) #:use-module (crates-io))

(define-public crate-string_cache_shared-0.1.0 (c (n "string_cache_shared") (v "0.1.0") (h "067hyx2gdvqb1r3ghg5rs5shl7vfzmfv0yrmsimi6hi2mykj637d")))

(define-public crate-string_cache_shared-0.1.2 (c (n "string_cache_shared") (v "0.1.2") (h "0zrfsv3rp46jzl08p3ch59qr7fgslfs820hjm3z12r7wh3accvys")))

(define-public crate-string_cache_shared-0.1.3 (c (n "string_cache_shared") (v "0.1.3") (h "1gdxbzmcgxi3y300b0j3mdkab5r8w3dk7j7vj102x2zyy4dqawh3")))

(define-public crate-string_cache_shared-0.1.4 (c (n "string_cache_shared") (v "0.1.4") (d (list (d (n "debug_unreachable") (r "^0.0.5") (d #t) (k 0)) (d (n "phf") (r "^0.7.3") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.3") (d #t) (k 1)))) (h "0rvdxsi9q6xi7nfg1x1z9xlkqbr3089mmv47lw679hass3gvbfvq")))

(define-public crate-string_cache_shared-0.1.5 (c (n "string_cache_shared") (v "0.1.5") (d (list (d (n "debug_unreachable") (r "^0.0.6") (d #t) (k 0)) (d (n "phf") (r "^0.7.3") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.3") (d #t) (k 1)))) (h "1kapf734kdlpaa2mp9qkj7jfpsqknqgd8n370h9wr7khah2ks3rw")))

(define-public crate-string_cache_shared-0.1.6 (c (n "string_cache_shared") (v "0.1.6") (d (list (d (n "debug_unreachable") (r "^0.0.6") (d #t) (k 0)) (d (n "phf_generator") (r "^0.7.4") (d #t) (k 1)) (d (n "phf_shared") (r "^0.7.4") (d #t) (k 0)))) (h "18sz47q4njb4zpzr12sp1as6swxcgvjddbl7bxxnracbil1ckjzf")))

(define-public crate-string_cache_shared-0.1.7 (c (n "string_cache_shared") (v "0.1.7") (d (list (d (n "debug_unreachable") (r "^0.0.6") (d #t) (k 0)) (d (n "phf_generator") (r "^0.7.4") (d #t) (k 1)) (d (n "phf_shared") (r "^0.7.4") (d #t) (k 0)))) (h "0cn1xyvvzhpw04zk4grm4na14mzy3gv14wk8d28x502x8541jy4g")))

(define-public crate-string_cache_shared-0.1.8 (c (n "string_cache_shared") (v "0.1.8") (d (list (d (n "debug_unreachable") (r "^0.0.6") (d #t) (k 0)) (d (n "phf_generator") (r "^0.7.4") (d #t) (k 1)) (d (n "phf_shared") (r "^0.7.4") (d #t) (k 0)))) (h "1ycjssx5lcwr6qkhpan93i9kvfq3czab8lssqi5zq78ckqi9ani7")))

(define-public crate-string_cache_shared-0.1.9 (c (n "string_cache_shared") (v "0.1.9") (d (list (d (n "debug_unreachable") (r "^0.0.6") (d #t) (k 0)) (d (n "phf_generator") (r "^0.7.4") (d #t) (k 1)) (d (n "phf_shared") (r "^0.7.4") (d #t) (k 0)))) (h "1x3pjjk7lh731f62sqklvs5cslq3xkqrm0535mm6fw3acg30ld2a")))

(define-public crate-string_cache_shared-0.1.10 (c (n "string_cache_shared") (v "0.1.10") (d (list (d (n "debug_unreachable") (r "^0.0.6") (d #t) (k 0)) (d (n "phf_generator") (r "^0.7.4") (d #t) (k 1)) (d (n "phf_shared") (r "^0.7.4") (d #t) (k 0)))) (h "1a34mflqvfx2ifvsp1vj1p535h4l1bhkmp35mdc96kxi4i3iqffv")))

(define-public crate-string_cache_shared-0.1.11 (c (n "string_cache_shared") (v "0.1.11") (d (list (d (n "debug_unreachable") (r "^0.0.6") (d #t) (k 0)) (d (n "phf_generator") (r "^0.7.4") (d #t) (k 1)) (d (n "phf_shared") (r "^0.7.4") (d #t) (k 0)))) (h "0sfl5j2wsvc0khzqyrhd9pdfkha6brrgr2jg225b8a0yk1lclq0p")))

(define-public crate-string_cache_shared-0.1.12 (c (n "string_cache_shared") (v "0.1.12") (d (list (d (n "debug_unreachable") (r "^0.0.6") (d #t) (k 0)) (d (n "phf_generator") (r "^0.7.4") (d #t) (k 1)) (d (n "phf_shared") (r "^0.7.4") (d #t) (k 0)))) (h "185i05pj2k7lhq2g2lxj8w8nm81lkbswjsn7xqx7f7n145vcrsdd")))

(define-public crate-string_cache_shared-0.3.0 (c (n "string_cache_shared") (v "0.3.0") (h "1z7dpdix1m42x6ddshdcpjf91ml9mhvnskmiv5kd8hcpq0dlv25i")))

