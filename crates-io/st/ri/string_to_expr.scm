(define-module (crates-io st ri string_to_expr) #:use-module (crates-io))

(define-public crate-string_to_expr-0.0.1 (c (n "string_to_expr") (v "0.0.1") (h "0ciic935z2cjysrc43sf534psj87s0idagxck8bhj3a36ik0ql8l")))

(define-public crate-string_to_expr-0.0.2 (c (n "string_to_expr") (v "0.0.2") (h "0mqsfzazg9635cqih5i9zsw37ncwn6b23vv2wdrg4cgjgx8yd40i")))

