(define-module (crates-io st ri stripe_wh_verify) #:use-module (crates-io))

(define-public crate-stripe_wh_verify-0.1.0 (c (n "stripe_wh_verify") (v "0.1.0") (d (list (d (n "constant_time_eq") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.9.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0v6ymyyc0acxcvf9ny8bvwzrxp6lj095kr1ldml0g4h9ss7allmz")))

