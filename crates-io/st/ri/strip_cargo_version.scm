(define-module (crates-io st ri strip_cargo_version) #:use-module (crates-io))

(define-public crate-strip_cargo_version-0.0.1 (c (n "strip_cargo_version") (v "0.0.1") (d (list (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "103mw853wgcn3q70whsg2ygpcbwqr4872jly759zw33ig3p61gsd")))

(define-public crate-strip_cargo_version-0.0.2 (c (n "strip_cargo_version") (v "0.0.2") (d (list (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "0mwc15s7c8rk7azajvn7brhjqh1j3fi5ia62sfs7a1qfrqzqvj7x")))

(define-public crate-strip_cargo_version-0.0.3 (c (n "strip_cargo_version") (v "0.0.3") (d (list (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "0za80ifwh3gyd5j0w1iqk7vxdnxkk885zawv02q0laf2mmsyw4mk")))

