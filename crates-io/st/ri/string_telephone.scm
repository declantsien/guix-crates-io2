(define-module (crates-io st ri string_telephone) #:use-module (crates-io))

(define-public crate-string_telephone-0.0.1 (c (n "string_telephone") (v "0.0.1") (d (list (d (n "time") (r "~0.0.3") (d #t) (k 0)))) (h "1kvizqzlqgkix9wx95ygi106jsrscdzmyisqg3ipvk6dfdxl6ap0")))

(define-public crate-string_telephone-0.0.2 (c (n "string_telephone") (v "0.0.2") (d (list (d (n "time") (r "^0.1.12") (d #t) (k 0)))) (h "1r4smf45c0kpz8aqmkx0smwr9lwv39ykn7ca5pvzngdzaxsxzv98")))

(define-public crate-string_telephone-0.0.3 (c (n "string_telephone") (v "0.0.3") (d (list (d (n "time") (r "^0.1.15") (d #t) (k 0)))) (h "12agivqvjnq7hzsgz4y6nkdhlgz5d60j1rnwklqqlirlcdlhra4i")))

