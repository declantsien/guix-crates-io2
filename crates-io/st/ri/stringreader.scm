(define-module (crates-io st ri stringreader) #:use-module (crates-io))

(define-public crate-stringreader-0.1.0 (c (n "stringreader") (v "0.1.0") (h "1q81p2pxijsb80kq8vpy8nv8m121rkfw783a5v7di5is0fyw5dlz")))

(define-public crate-stringreader-0.1.1 (c (n "stringreader") (v "0.1.1") (h "01cr2lzx5hwy9fafi3vr0jcnd0lxfhvdlxyzsb6zclipsq1pngli")))

