(define-module (crates-io st ri string_template) #:use-module (crates-io))

(define-public crate-string_template-0.1.0 (c (n "string_template") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1iq9bcxfqzy8sbbily1gjlarrkxryad9z5vqa4lv7zwraysd8r4z")))

(define-public crate-string_template-0.2.0 (c (n "string_template") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1r8vmv1b7b2hk2yviqi728p9ylnd99wrmd8pkmh3hrcpmnh3hx0r")))

(define-public crate-string_template-0.2.1 (c (n "string_template") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08338hzmj6vi5fpzjak6h2spw797p4y0rswsbj4m1a9z5imjqvyw")))

