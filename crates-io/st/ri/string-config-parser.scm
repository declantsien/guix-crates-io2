(define-module (crates-io st ri string-config-parser) #:use-module (crates-io))

(define-public crate-string-config-parser-0.1.0 (c (n "string-config-parser") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v204srjmdb9r15ga7lcdp8cxix527siv0jgdfbi9j0sc8nd293n") (y #t)))

(define-public crate-string-config-parser-0.2.0 (c (n "string-config-parser") (v "0.2.0") (h "1j2j37m33fhcq3080zkw0s09528kv42p8jy47k8yg5aqkknbiz6x") (y #t)))

(define-public crate-string-config-parser-0.2.1 (c (n "string-config-parser") (v "0.2.1") (h "0wrlink6vikl9679crjil8r1vz45c1jwk2ymc4nhjp4cpxj15n24")))

