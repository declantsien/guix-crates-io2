(define-module (crates-io st ri stringly_typed) #:use-module (crates-io))

(define-public crate-stringly_typed-0.1.0 (c (n "stringly_typed") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.38") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.38") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.14") (d #t) (k 2)) (d (n "stringly_typed_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1c73ngsqabk05kffmi7dd29aa238hz10x8d43lgd7mlxp5xwvb4i") (f (quote (("std") ("default" "std"))))))

