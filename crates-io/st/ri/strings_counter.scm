(define-module (crates-io st ri strings_counter) #:use-module (crates-io))

(define-public crate-strings_counter-1.0.0 (c (n "strings_counter") (v "1.0.0") (h "033gljwb5aikravbqix56k614q8h8id1x5wnjsa5hn05nwfbbr3w")))

(define-public crate-strings_counter-1.0.1 (c (n "strings_counter") (v "1.0.1") (d (list (d (n "async-recursion") (r "^0.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)))) (h "03bip368nz8sz4g3l8h2833cmd6105mcmbk73irpmh591gbvf34p")))

(define-public crate-strings_counter-1.0.2 (c (n "strings_counter") (v "1.0.2") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)))) (h "0sflnkwdsiwbkw6f34m8qv9wb3mjg9vgxb0na1mwa6y8i27ypibs")))

