(define-module (crates-io st ri strict-num) #:use-module (crates-io))

(define-public crate-strict-num-0.1.0 (c (n "strict-num") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (o #t) (k 0)))) (h "1l9b7d0qrdg3nvqim1xvkcis37jmdf565qyxxx2x5vlad4h5zxlx") (f (quote (("default" "approx-eq") ("approx-eq" "float-cmp"))))))

(define-public crate-strict-num-0.1.1 (c (n "strict-num") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (o #t) (k 0)))) (h "0cb7l1vhb8zj90mzm8avlk815k40sql9515s865rqdrdfavvldv6") (f (quote (("default" "approx-eq") ("approx-eq" "float-cmp"))))))

(define-public crate-strict-num-0.2.0 (c (n "strict-num") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0s5c0p0xwy0p5v18yi7sa5l7phgjn2pczh8b4x64yr80p7mda3r9") (f (quote (("default" "approx-eq") ("approx-eq" "float-cmp")))) (r "1.56")))

