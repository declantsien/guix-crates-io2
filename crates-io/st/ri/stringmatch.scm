(define-module (crates-io st ri stringmatch) #:use-module (crates-io))

(define-public crate-stringmatch-0.1.0 (c (n "stringmatch") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "00qpr5yg3qvr296acgdf7si4rmxqff2jzsim0pq6qym1rwmbxv9y")))

(define-public crate-stringmatch-0.2.0 (c (n "stringmatch") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06jv9zdfndacm9dcd95574n0jhhdh0flwzpg5g7bpvi4hsqz2kma")))

(define-public crate-stringmatch-0.3.0 (c (n "stringmatch") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06srcy7r9ihili1cdsdwi9pydmmpm7g9g9qi5k5sc3nzxsdqb7r2")))

(define-public crate-stringmatch-0.3.2 (c (n "stringmatch") (v "0.3.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1zc4iy8k76540qjh2x6yrlwr44ahib0g8wc1nfi8si105r0w9w9c")))

(define-public crate-stringmatch-0.3.3 (c (n "stringmatch") (v "0.3.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0h1vb2yiw1p12h86yrs8vbs7ww58qggw4pw9iy1w65h3fymzmh58")))

(define-public crate-stringmatch-0.4.0 (c (n "stringmatch") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0pfhazx02cay517qj8mslm94ya36hx5prii7c71cvw4j3n0c1bba") (f (quote (("serde_derive" "serde") ("default"))))))

