(define-module (crates-io st ri string-builder) #:use-module (crates-io))

(define-public crate-string-builder-0.1.0 (c (n "string-builder") (v "0.1.0") (h "1fyyyvh3an17swvp66qw0vqp50fj0gw4zkyhvdf3ifrvvajdahkf")))

(define-public crate-string-builder-0.2.0 (c (n "string-builder") (v "0.2.0") (h "04ja7anxbf0gssngy4kg5fl84invjlkc9gla51m7kwmi1w3hml9b")))

