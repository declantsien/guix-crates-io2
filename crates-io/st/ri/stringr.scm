(define-module (crates-io st ri stringr) #:use-module (crates-io))

(define-public crate-stringr-0.1.0 (c (n "stringr") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "158001b1m914xinxz3jvx00l7vv0yvi87j4prsnqvk3p8cplvnwr")))

(define-public crate-stringr-0.1.1 (c (n "stringr") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1r22yznv6y299s09jnch5ic1zp5v67nia0y79dg38r6iwaj9wnjj")))

(define-public crate-stringr-0.1.2 (c (n "stringr") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1d8fi8lzw2jd4njrqpzppw6ycwzhvqn8lp7k3cbblrl2mn5ni4bj")))

(define-public crate-stringr-0.1.3 (c (n "stringr") (v "0.1.3") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "04wsm5q9m30cqgy233aiwrpbg4ra0mr1pdiv9g2klb2la2n5a1mr")))

