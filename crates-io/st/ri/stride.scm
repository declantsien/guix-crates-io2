(define-module (crates-io st ri stride) #:use-module (crates-io))

(define-public crate-stride-0.0.0 (c (n "stride") (v "0.0.0") (h "11cqnhfxrdl40nqy5scni69jibzqqwkv7mi780fhh2dh5b50my0v")))

(define-public crate-stride-0.1.0 (c (n "stride") (v "0.1.0") (h "0gl47w5m5vfn9y65ippfjyjz87klvgnpb0b80ghidm3b0236325a")))

(define-public crate-stride-0.1.1 (c (n "stride") (v "0.1.1") (h "03ffsqz6wklgbywgmv1ivz06hivn4qfbb0iyifra0jncss1l8ajb")))

(define-public crate-stride-0.2.0 (c (n "stride") (v "0.2.0") (h "0xy8xzhxi37jbw9gc9bqyndr6hdkba2g3wfmzibcpmshs5c0bn6a")))

(define-public crate-stride-0.3.0 (c (n "stride") (v "0.3.0") (h "0m62bakq8gczch1sd6d19sqqliafz953pla5iw9q1hbfca0f7yv6")))

