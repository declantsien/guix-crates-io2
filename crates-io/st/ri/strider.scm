(define-module (crates-io st ri strider) #:use-module (crates-io))

(define-public crate-strider-0.1.0 (c (n "strider") (v "0.1.0") (h "1y6ddn7jxyiqwh01c3qs2qcrjzfhi7kzggad5w18c4ngfa8al8x1")))

(define-public crate-strider-0.1.1 (c (n "strider") (v "0.1.1") (h "0iwnrb63d5h9v34kr2lghhl62h57qfrb4hfiqzwjhy01ghhkr4pc")))

(define-public crate-strider-0.1.2 (c (n "strider") (v "0.1.2") (h "0fj1z2ifqa9szjhyqr6r511zhyx1z4vfairih4r404bq7v4i97y3")))

(define-public crate-strider-0.1.3 (c (n "strider") (v "0.1.3") (h "01y0dynsdgrqjfrz5w11qg6ziz96nwdrc9sgyh4bkla1yap45szq")))

