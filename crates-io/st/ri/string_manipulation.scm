(define-module (crates-io st ri string_manipulation) #:use-module (crates-io))

(define-public crate-string_manipulation-0.1.0 (c (n "string_manipulation") (v "0.1.0") (h "0yzpxpq9721rvgi9aa85183h5zqgw3f7bcdvlpypmv611c7w5cdb")))

(define-public crate-string_manipulation-0.1.1 (c (n "string_manipulation") (v "0.1.1") (h "1ns8cs3hldmy7c3gzbq6ip2vldfgysbcnkr6c5i50s5wz9gg4ryl")))

(define-public crate-string_manipulation-0.1.2 (c (n "string_manipulation") (v "0.1.2") (h "0wyd34hj8ixnrv7x03dh5g69qhsnq2b15d67a7iizld861k5xd65")))

(define-public crate-string_manipulation-0.1.3 (c (n "string_manipulation") (v "0.1.3") (h "1vp7xck5vd2x310bqkzpx1nk5m4wdckssms2n3pda1zmdv88y4vv")))

(define-public crate-string_manipulation-0.1.4 (c (n "string_manipulation") (v "0.1.4") (h "12y95r1ivk74p5qb2rqs0v6miajsgss1ngsiiz8zqb1nrffph5vd")))

(define-public crate-string_manipulation-0.1.5 (c (n "string_manipulation") (v "0.1.5") (h "15fhwgg7djkfm037yik5dll9vw5s7ki74l8zrcrj81knn1yix8bm")))

(define-public crate-string_manipulation-0.1.6 (c (n "string_manipulation") (v "0.1.6") (h "1m4z6ki74fvbl6dpqi7cgkg7kcavzhf3y9fbj8v760gvcw6898h8")))

(define-public crate-string_manipulation-0.1.7 (c (n "string_manipulation") (v "0.1.7") (h "0hp2ah0a03r3vl91r765v9s6xp5fi9dkr46vl8gwpvpsk1wcbwvf")))

