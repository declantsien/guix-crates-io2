(define-module (crates-io st ri stringly_typed_derive) #:use-module (crates-io))

(define-public crate-stringly_typed_derive-0.1.0 (c (n "stringly_typed_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)) (d (n "synstructure") (r "^0.8.1") (d #t) (k 0)))) (h "18gk63nx9paj2gvbq01fm1mzc3bij8y0cmfnszj9q3mjxj9kpjsa")))

