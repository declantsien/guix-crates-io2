(define-module (crates-io st ri string_mac) #:use-module (crates-io))

(define-public crate-string_mac-0.1.0 (c (n "string_mac") (v "0.1.0") (h "029h68rs2rbr1b0sjf31dwhkfl9z3mccyi4zyyvr1bm61d11y6c7")))

(define-public crate-string_mac-1.0.0 (c (n "string_mac") (v "1.0.0") (h "06brh546skaymr6a95j9mb2qwv2xy750b41rgidxyzdj8a5n2pdv")))

