(define-module (crates-io st ri string-pool) #:use-module (crates-io))

(define-public crate-string-pool-0.0.0 (c (n "string-pool") (v "0.0.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0f5s6my9jvfsjcyix17xwx7w291bv5qgxzgcfrmn5pllkvvmjjr1")))

(define-public crate-string-pool-0.1.0 (c (n "string-pool") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1szpyp5bdi41fl0abfhba791igyg3y1pcv8iwbw0d0glikvjsbpn")))

(define-public crate-string-pool-0.2.0 (c (n "string-pool") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "lazy-wrap") (r "^0.4.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kpwxwf6n4n06bd2jicr158pz26slqg3xyb3x7hw01fri8cvfxg0") (f (quote (("nightly"))))))

(define-public crate-string-pool-0.2.1 (c (n "string-pool") (v "0.2.1") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "lazy-wrap") (r "^0.4.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ah07byjliw8xjm57jzck01ij5ygcsv2ppppgsnyqajpfk64sbyf") (f (quote (("nightly"))))))

