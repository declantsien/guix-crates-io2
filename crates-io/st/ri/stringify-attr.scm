(define-module (crates-io st ri stringify-attr) #:use-module (crates-io))

(define-public crate-stringify-attr-1.0.0 (c (n "stringify-attr") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0i3dx724mv1wz6vy3my0cws6vhwj9siwvbgmpbi4mijb8c0f4j8z")))

