(define-module (crates-io st ri string32) #:use-module (crates-io))

(define-public crate-string32-0.1.0 (c (n "string32") (v "0.1.0") (d (list (d (n "mediumvec") (r "^1.1.0") (d #t) (k 0)) (d (n "usize_cast") (r "^1.1.0") (d #t) (k 0)))) (h "171prnfjz8zxzc5gmzmljhllzcmbli2aa10bvf4idiiy87b1c99y")))

(define-public crate-string32-0.2.0 (c (n "string32") (v "0.2.0") (d (list (d (n "mediumvec") (r "^1.2.0") (d #t) (k 0)) (d (n "usize_cast") (r "^1.1.0") (d #t) (k 0)))) (h "0iq2adzk36dk0n2m9mhnshpg0mazpzp5075rvqa10agp2d7sg8lw")))

