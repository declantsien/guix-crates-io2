(define-module (crates-io st ri string-literals) #:use-module (crates-io))

(define-public crate-string-literals-1.0.0 (c (n "string-literals") (v "1.0.0") (h "0lwlvryjhyw4mq045ggaf26754721pn78x7h1h9i5z2v8bg95hrn") (r "1.56.0")))

(define-public crate-string-literals-1.0.1 (c (n "string-literals") (v "1.0.1") (h "16238ib8rdar3flmypn6jkd6y2s1yp0vnzw7c8hxfnlns6zlg4hk") (r "1.56.0")))

