(define-module (crates-io st ri stringly_conversions) #:use-module (crates-io))

(define-public crate-stringly_conversions-0.1.0 (c (n "stringly_conversions") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_str_helpers") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1dymhf4p8jv1zxihdpdiiycrxx9i6sqznh7ldnsrnnvqyr6chhig") (f (quote (("alloc")))) (y #t)))

(define-public crate-stringly_conversions-0.1.1 (c (n "stringly_conversions") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_str_helpers") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0ykaac4x810f5hsfn45r7m1gpb9qrrz8vvfazy4x5m1d947hhqzz") (f (quote (("alloc"))))))

