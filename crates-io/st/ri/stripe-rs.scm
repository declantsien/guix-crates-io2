(define-module (crates-io st ri stripe-rs) #:use-module (crates-io))

(define-public crate-stripe-rs-0.1.0 (c (n "stripe-rs") (v "0.1.0") (d (list (d (n "curl") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "url") (r "^0.2.35") (d #t) (k 0)))) (h "041adg4qscy5zqyv004b613s117m4r29rici0a4rif87v2zqp376")))

