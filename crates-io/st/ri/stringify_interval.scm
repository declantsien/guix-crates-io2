(define-module (crates-io st ri stringify_interval) #:use-module (crates-io))

(define-public crate-stringify_interval-0.1.0 (c (n "stringify_interval") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0pg04m25rhpsgssk9wnx06fqxc9q3jvjp8ijsvnhxgv1nzw3rp94")))

