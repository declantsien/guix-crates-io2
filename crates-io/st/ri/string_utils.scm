(define-module (crates-io st ri string_utils) #:use-module (crates-io))

(define-public crate-string_utils-0.1.0 (c (n "string_utils") (v "0.1.0") (d (list (d (n "vector_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1mv4cdmahk9xl0xd5b2bgyd2zvl44wcv86bhj4wxri4hi0521gv4")))

(define-public crate-string_utils-0.1.1 (c (n "string_utils") (v "0.1.1") (d (list (d (n "vector_utils") (r "^0.1.0") (d #t) (k 0)))) (h "0jrq6a38cf3fdsp9yl5scpz2h1l9wdp1ggm1ymbqzyajkjppy2ld")))

(define-public crate-string_utils-0.1.2 (c (n "string_utils") (v "0.1.2") (d (list (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1d61cxgv2bbcdzfp64sckp5dgp7gds5fagk9w9sa2sbpff08s8vv")))

(define-public crate-string_utils-0.1.3 (c (n "string_utils") (v "0.1.3") (d (list (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "01gxyhsjpqv21qsh1wdzvdxg5nr0b1d8g7n9a2b9nfxjyrlgjblx")))

(define-public crate-string_utils-0.1.4 (c (n "string_utils") (v "0.1.4") (d (list (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1l11a0gcb89s8vy2ww93rr3d9xy5mfqjyp9bmcmhz0dq9srz4iq2")))

