(define-module (crates-io st ri stringlit) #:use-module (crates-io))

(define-public crate-stringlit-1.0.0 (c (n "stringlit") (v "1.0.0") (h "0k42jnx0z46j2pr5pgr53asqlnwzqlzd05jwa7f76gz33dkq5z79")))

(define-public crate-stringlit-2.0.0 (c (n "stringlit") (v "2.0.0") (h "0l875b53cr8ab60gn77r7ql4b65jpca8vp5s9i560gv1wy6j3bdg")))

(define-public crate-stringlit-2.1.0 (c (n "stringlit") (v "2.1.0") (h "18ddwy3qg6mplg91mrlw0favi9lyg3m0v63mghkz85a9d87y63g8")))

