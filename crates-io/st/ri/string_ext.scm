(define-module (crates-io st ri string_ext) #:use-module (crates-io))

(define-public crate-string_ext-0.0.1 (c (n "string_ext") (v "0.0.1") (h "0wrbw0vh9ygdn82n8538mmdy6s4y3l13kv5lhjy4gspci9xz89zw")))

(define-public crate-string_ext-0.0.2 (c (n "string_ext") (v "0.0.2") (h "0dvas0il9nlm97s7qmg54zdwlcy87xgww2d7if8fypcymdgcs9a3")))

(define-public crate-string_ext-0.0.3 (c (n "string_ext") (v "0.0.3") (h "0s8hj0ixsyxsf5zkxzihp58kgjc0sfaw3wgs0bjsd84zlbl0lzib")))

