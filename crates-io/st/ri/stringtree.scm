(define-module (crates-io st ri stringtree) #:use-module (crates-io))

(define-public crate-stringtree-0.1.0 (c (n "stringtree") (v "0.1.0") (h "0j2chq3qj0gbjzwfbd7q7rr0kk66d5w9gdb157jqm709r6c7m927")))

(define-public crate-stringtree-0.2.0 (c (n "stringtree") (v "0.2.0") (h "0js1a4z079wawj4r1xsq9qpyr5l91bl5qkdxw4w38g8sdnim21w9")))

