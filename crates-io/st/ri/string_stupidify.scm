(define-module (crates-io st ri string_stupidify) #:use-module (crates-io))

(define-public crate-string_stupidify-0.1.0 (c (n "string_stupidify") (v "0.1.0") (h "0qadbxqv59rwqnch6jms7cz6ddp3fv1wd62qv2f6f4zkl8lwlyf3")))

(define-public crate-string_stupidify-0.1.1 (c (n "string_stupidify") (v "0.1.1") (h "18ckxli320qka3x2ba2balgcjzwfxjv5pzv2iz5if59v8vsmfr5r")))

(define-public crate-string_stupidify-0.1.2 (c (n "string_stupidify") (v "0.1.2") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0k6h31pdpvzls5yy2g5q5shak9ykr11grf24rc5dz68ip79ylidi")))

(define-public crate-string_stupidify-0.1.3 (c (n "string_stupidify") (v "0.1.3") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "02grppa28br0bwlxsxj0h1s0lcc981nzf9m34qkz8xgkpab5xxzc")))

(define-public crate-string_stupidify-0.1.4 (c (n "string_stupidify") (v "0.1.4") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0r2zmd0g088p1kja9dyz80xr3fhfm2hvpvklmk638jd30jlbacmg")))

(define-public crate-string_stupidify-0.2.0 (c (n "string_stupidify") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1yfdvvzz1ld9cfhv2b14irag2v32n7smj17xii4kaskk0caj6847")))

(define-public crate-string_stupidify-0.2.1 (c (n "string_stupidify") (v "0.2.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1w2bmsg3q90dmnnah8p520bddfm282cgprc9nq1kvkv8m33c2vy8")))

