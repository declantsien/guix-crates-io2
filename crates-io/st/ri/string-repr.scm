(define-module (crates-io st ri string-repr) #:use-module (crates-io))

(define-public crate-string-repr-1.0.0 (c (n "string-repr") (v "1.0.0") (h "05q01bmwms3k5vag60aqmm0d4lrhyjr7qcjk4hh69bm9l941545n")))

(define-public crate-string-repr-1.0.1 (c (n "string-repr") (v "1.0.1") (h "09mx8rkbi2dxw1ciwjmvmcw79b3inyd0khr6hazdk64nk52qlql1")))

