(define-module (crates-io st ri stringprep) #:use-module (crates-io))

(define-public crate-stringprep-0.1.0 (c (n "stringprep") (v "0.1.0") (d (list (d (n "unicode-bidi") (r "^0.2") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1l37g3wa744mgwijjza6f81zlb888my0svbxhaxpa2h0ih4rvzy5")))

(define-public crate-stringprep-0.1.1 (c (n "stringprep") (v "0.1.1") (d (list (d (n "unicode-bidi") (r "^0.3") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "02fnynkyzmhqyjk8kyavgg2vjpnqzhqnrmq7pm6bgb7fwyn380j1")))

(define-public crate-stringprep-0.1.2 (c (n "stringprep") (v "0.1.2") (d (list (d (n "unicode-bidi") (r "^0.3") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1hd1x7s8fnzqwz5fm2pq0jh10n024zvwnldmykzm8x5qfk5liqwf")))

(define-public crate-stringprep-0.1.3 (c (n "stringprep") (v "0.1.3") (d (list (d (n "unicode-bidi") (r "^0.3") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1nm8a652a13hing4y3znnpyhp8ppbcv1aaqf5q89gkpdwyykfdyv")))

(define-public crate-stringprep-0.1.4 (c (n "stringprep") (v "0.1.4") (d (list (d (n "finl_unicode") (r "^1.2.0") (d #t) (k 0)) (d (n "unicode-bidi") (r "^0.3") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1rkfsf7riynsmqj3hbldfrvmna0i9chx2sz39qdpl40s4d7dfhdv")))

(define-public crate-stringprep-0.1.5 (c (n "stringprep") (v "0.1.5") (d (list (d (n "unicode-bidi") (r "^0.3") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)) (d (n "unicode-properties") (r "^0.1.1") (d #t) (k 0)))) (h "1cb3jis4h2b767csk272zw92lc6jzfzvh8d6m1cd86yqjb9z6kbv")))

