(define-module (crates-io st ri string-cases) #:use-module (crates-io))

(define-public crate-string-cases-0.1.0 (c (n "string-cases") (v "0.1.0") (h "04mfh1vhf6g9mlh7p0ig8byzvv2lwk7xlzsxl4fjh05c5q1g13y6")))

(define-public crate-string-cases-0.2.0 (c (n "string-cases") (v "0.2.0") (h "0vrbxq9gkw3qbyd4155hr09gwh1px5ms7mgrdisvw3wy3x3267d3")))

