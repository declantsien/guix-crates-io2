(define-module (crates-io st ri strings) #:use-module (crates-io))

(define-public crate-strings-0.0.1 (c (n "strings") (v "0.0.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0814086xqzwbnk5liqmfjdbmqil6i3vz9211g1h4y2s8md369y2l")))

(define-public crate-strings-0.1.0 (c (n "strings") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0sm2ggxi1y5z3larngg0yvwh2q033c21ln09vmihs8ad5jzxhxfs")))

(define-public crate-strings-0.1.1 (c (n "strings") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0g0z91zazkw1afzl1b4bniqjp3yg8g5zg4az37w3vz22pkhiwj5a")))

