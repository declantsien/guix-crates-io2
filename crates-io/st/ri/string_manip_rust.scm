(define-module (crates-io st ri string_manip_rust) #:use-module (crates-io))

(define-public crate-string_manip_rust-0.1.0 (c (n "string_manip_rust") (v "0.1.0") (h "0a9yd9fr65yxb38ivbpxpc5inf5rr9rnpsfz6f8968ibmmk657ig")))

(define-public crate-string_manip_rust-0.1.1 (c (n "string_manip_rust") (v "0.1.1") (h "1a7danfka0chp3958llpzs85dhplvikz7vwd3vm36qmm5y8bl5wy")))

(define-public crate-string_manip_rust-0.1.2 (c (n "string_manip_rust") (v "0.1.2") (h "1ivainanaisb58c9zfy5i2553q764yhxr56j5nsfjbv04y1nm4kf")))

