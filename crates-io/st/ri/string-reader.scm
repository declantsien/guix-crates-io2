(define-module (crates-io st ri string-reader) #:use-module (crates-io))

(define-public crate-string-reader-0.1.0 (c (n "string-reader") (v "0.1.0") (h "1xj93a75h4lsagqxm9xb76s0pz67dp21svssrd2f9zkw8f49gkrl")))

(define-public crate-string-reader-0.1.1 (c (n "string-reader") (v "0.1.1") (h "1figr3qn5c703lxm2bm10d4000kgm4qkcy552vvhwbaa0hiqg52v")))

