(define-module (crates-io st ri string_dataframe) #:use-module (crates-io))

(define-public crate-string_dataframe-0.1.0 (c (n "string_dataframe") (v "0.1.0") (d (list (d (n "parquet") (r "^6.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "term-table") (r "^1.3.2") (d #t) (k 0)))) (h "0mix4w1pp4hjbbbs4k0myrvhd7cbwbkjgrgn4ivjhnxzfhn6f1xx")))

