(define-module (crates-io st ri strict_linking) #:use-module (crates-io))

(define-public crate-strict_linking-0.1.0 (c (n "strict_linking") (v "0.1.0") (d (list (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (d #t) (k 0)))) (h "0kdpjsd33r8ghl9hh6r9xzfnmv1z7ma6gmv6v7rw96g7yq02c38d")))

(define-public crate-strict_linking-0.1.1 (c (n "strict_linking") (v "0.1.1") (d (list (d (n "syn") (r "^1.0.90") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "17cs7gv5dfn0kh5406m6hparq51flnxmc24adakz6glpf59cl7l9")))

(define-public crate-strict_linking-0.1.2 (c (n "strict_linking") (v "0.1.2") (d (list (d (n "syn") (r "^1.0.90") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1543xy9j7gn3zagvi0rrzwwm5hdygcnvmdb9m4qbi83nc2fk81rn")))

(define-public crate-strict_linking-0.1.3 (c (n "strict_linking") (v "0.1.3") (d (list (d (n "syn") (r "^1.0.90") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0hbq7p497z12vbj5l50yqx3yaf9jk59vwh2x01v3g351jbi9ccrv")))

(define-public crate-strict_linking-0.1.4 (c (n "strict_linking") (v "0.1.4") (d (list (d (n "syn") (r "^1.0.90") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "10db70dhhnrr5n4ym5zss61d941p9cgm1qrsq3ykqshq28b62zi9")))

