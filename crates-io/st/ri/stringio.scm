(define-module (crates-io st ri stringio) #:use-module (crates-io))

(define-public crate-stringio-0.1.0 (c (n "stringio") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)))) (h "0b0qr3blcm84q8mpbl223swi659xdi5an4nrh05m67sag3kdlmic")))

(define-public crate-stringio-0.3.0 (c (n "stringio") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)))) (h "1mljyd69vp96f60nh9zlaw24005z48papb6glzanncxsk7m9xbiw")))

