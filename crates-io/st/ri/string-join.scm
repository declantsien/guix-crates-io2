(define-module (crates-io st ri string-join) #:use-module (crates-io))

(define-public crate-string-join-0.1.0 (c (n "string-join") (v "0.1.0") (h "0d5l62pn059pk5k1x1fpdb4ir88asd152d1bm1hg5116yzd76fri")))

(define-public crate-string-join-0.1.1 (c (n "string-join") (v "0.1.1") (h "05xnvfjxnhlcgjw391lixwfkh9ihwi1626ikj16j7fl5vi05smsv")))

(define-public crate-string-join-0.1.2 (c (n "string-join") (v "0.1.2") (h "11ljsi0sqqsmvsn2la1h0x8vfkbdscyz1w005mb40mh51v0z7i40")))

