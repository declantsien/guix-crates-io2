(define-module (crates-io st ri stripmargin) #:use-module (crates-io))

(define-public crate-stripmargin-0.1.0 (c (n "stripmargin") (v "0.1.0") (h "1yl72njls5ywnfjiry1wnglafrv1c49jf26mz09r060lwbyk6qx1")))

(define-public crate-stripmargin-0.1.1 (c (n "stripmargin") (v "0.1.1") (h "0160frbxjpaj74rpqvylcvbl7mznfm05wwdgl2xzii5830vflkbq")))

