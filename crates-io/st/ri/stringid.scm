(define-module (crates-io st ri stringid) #:use-module (crates-io))

(define-public crate-stringid-0.0.1 (c (n "stringid") (v "0.0.1") (h "02yj606596m2m410rhja2alqbhay63460219968dl0lqly0s0szh")))

(define-public crate-stringid-0.0.2 (c (n "stringid") (v "0.0.2") (h "1pj0lzx1zrw82blla6r3c65y2ww1qm3kn8l6ih1j5srvppfbg7vf")))

(define-public crate-stringid-0.1.0 (c (n "stringid") (v "0.1.0") (d (list (d (n "cityhasher") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "stringid_macros") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0sx4dikj0zi7vqlj4skabs3xbpk6yf3mdhpmla53q0fi0aj250py") (f (quote (("default" "macros")))) (s 2) (e (quote (("macros" "dep:stringid_macros")))) (r "1.72")))

