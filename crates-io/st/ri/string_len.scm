(define-module (crates-io st ri string_len) #:use-module (crates-io))

(define-public crate-string_len-0.1.1 (c (n "string_len") (v "0.1.1") (h "0hgxdzcwav4hy6j6gs3vydysgidhpwk7yb70drmh0g7q0jcarim4")))

(define-public crate-string_len-0.1.2 (c (n "string_len") (v "0.1.2") (h "1ww43kpswrw9bmvmwcqd26r56b6l70b19ispqz6kg05s6wlf1da9")))

