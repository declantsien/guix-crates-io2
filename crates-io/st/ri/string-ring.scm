(define-module (crates-io st ri string-ring) #:use-module (crates-io))

(define-public crate-string-ring-0.1.0 (c (n "string-ring") (v "0.1.0") (d (list (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (k 0)))) (h "1kr44k2v3dpxqfzw02aknc8r15mx8rgia4v90wywibsnvrw98fiq") (f (quote (("std" "no-std-compat/std" "memchr/std") ("default" "std"))))))

(define-public crate-string-ring-0.1.1 (c (n "string-ring") (v "0.1.1") (d (list (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (k 0)))) (h "1xg3l6lk51c07aa8j44qwqj2vm8c6w8mhdksygxs3pd1ncvvfw1q") (f (quote (("std" "no-std-compat/std" "memchr/std") ("default" "std"))))))

