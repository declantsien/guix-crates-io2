(define-module (crates-io st ri string-box) #:use-module (crates-io))

(define-public crate-string-box-1.0.0 (c (n "string-box") (v "1.0.0") (d (list (d (n "widestring") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1kqy9m8g0fqnsvlhf9jiql0r6lvjh47kkrsyf5apy77qqw5cfkys")))

(define-public crate-string-box-1.0.1 (c (n "string-box") (v "1.0.1") (d (list (d (n "widestring") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fv6alqvqxshm5r27yb08r49s09mksa60p5zpymklc0g553rw0k9")))

(define-public crate-string-box-1.1.0 (c (n "string-box") (v "1.1.0") (d (list (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "089qlzfi5l72mjh6yd1g763v1krirzv0pfyxi0d7z9n5kwbb610a")))

(define-public crate-string-box-1.1.1 (c (n "string-box") (v "1.1.1") (d (list (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "1xsbbhmz2bjqr2kvv65fsg9id6dmnd66njm7y660xb4cyjx22x9w")))

