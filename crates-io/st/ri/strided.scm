(define-module (crates-io st ri strided) #:use-module (crates-io))

(define-public crate-strided-0.1.0 (c (n "strided") (v "0.1.0") (h "04gcfvbhcd4iwyh8j24l5x81bamwkrbczac59rl0vvdf5d2zfd59")))

(define-public crate-strided-0.2.0 (c (n "strided") (v "0.2.0") (d (list (d (n "num") (r "~0.0.3") (d #t) (k 0)))) (h "170naw8n5s264apf845vv23y5zk1lxaj9sybhsiva5xrdjmqr6pp")))

(define-public crate-strided-0.2.1 (c (n "strided") (v "0.2.1") (d (list (d (n "num") (r "~0.0.3") (d #t) (k 0)))) (h "1pjx49db102lbgf1q2kjrvzs8iwdnsab19ic2agajjcqy95m2kjp")))

(define-public crate-strided-0.2.2 (c (n "strided") (v "0.2.2") (d (list (d (n "num") (r "~0.0.5") (d #t) (k 2)))) (h "0i9rdsd8cvh0cai7fd2mvlylb7n8qrx0ijv4314bxd62k6ipfldb")))

(define-public crate-strided-0.2.3 (c (n "strided") (v "0.2.3") (d (list (d (n "num") (r "~0.1.2") (d #t) (k 2)))) (h "1hds47dspxk9532d5qsyn4rp9qw6qqg14kxi0hk99p3xs4apy84c")))

(define-public crate-strided-0.2.4 (c (n "strided") (v "0.2.4") (d (list (d (n "num") (r "~0.1.2") (d #t) (k 2)))) (h "12f5xy8611knv1dy29jqqff0vxhc9mpa4ayw9sy8jzsqql24jbiq")))

(define-public crate-strided-0.2.5 (c (n "strided") (v "0.2.5") (d (list (d (n "num") (r "~0.1.2") (d #t) (k 2)))) (h "11l1gqa31rh6g1abpd5nc1y6n86xfsyf3pyrh9l4jfm948bzzk8f")))

(define-public crate-strided-0.2.6 (c (n "strided") (v "0.2.6") (d (list (d (n "num") (r "~0.1.2") (d #t) (k 2)))) (h "03wknnh58dxk66vkvvfdvk272b9l29r03i2jgl22r8nlc4svf10j")))

(define-public crate-strided-0.2.7 (c (n "strided") (v "0.2.7") (d (list (d (n "num") (r "^0") (d #t) (k 2)))) (h "0bx1gz2vm8fnmwdndb6mdhh9lclfdb1b67zckjaxjicnzn59kzda")))

(define-public crate-strided-0.2.8 (c (n "strided") (v "0.2.8") (d (list (d (n "num") (r "^0") (d #t) (k 2)))) (h "0j7z6hsbqb991015zjcnaxx9dv3fgqh3fs64xf65gxb83135iwj3")))

(define-public crate-strided-0.2.9 (c (n "strided") (v "0.2.9") (d (list (d (n "num") (r "^0") (d #t) (k 2)))) (h "1imwp6hly4iqsrfm2464pn921cm0v2jazwn6vrkp4whac5vnmsg3") (f (quote (("unstable"))))))

