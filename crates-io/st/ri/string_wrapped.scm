(define-module (crates-io st ri string_wrapped) #:use-module (crates-io))

(define-public crate-string_wrapped-0.1.0 (c (n "string_wrapped") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "01llp2yzrmf39qd5l009iagia5i3752nkia6igwwv83n9i6zaxay")))

(define-public crate-string_wrapped-0.1.1 (c (n "string_wrapped") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1ppzsh07pv64jby2xdcnwil4kvg2ahcl2izizyifshlxrrbc2xsf")))

