(define-module (crates-io st ri stringx) #:use-module (crates-io))

(define-public crate-stringx-0.1.0 (c (n "stringx") (v "0.1.0") (h "04ibg7ldz5hhkclhfh2czh27pzn4g1vymjsacsfr02ns0gnbgl7s")))

(define-public crate-stringx-0.1.1 (c (n "stringx") (v "0.1.1") (h "185zdf6m3id1whfrnphiad599vhy156j0jjjxqwscrk0dqbkcdj8")))

