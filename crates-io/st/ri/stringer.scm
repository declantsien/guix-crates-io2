(define-module (crates-io st ri stringer) #:use-module (crates-io))

(define-public crate-stringer-0.1.0 (c (n "stringer") (v "0.1.0") (h "1miv3l81jxjq3sd1dxilbr3npxxgx9mxbjcm42n6viz70j45vss3")))

(define-public crate-stringer-0.1.1 (c (n "stringer") (v "0.1.1") (h "0bw5b0x6wg8awbkvay4krsml12aaj2i9h0sggj903yr5qprgs0hm")))

(define-public crate-stringer-0.1.2 (c (n "stringer") (v "0.1.2") (h "13p9jr1pz24la4q4wqgcwawr8wkw54z1ibgsxk3cs9r89f11ixhw")))

(define-public crate-stringer-0.1.3 (c (n "stringer") (v "0.1.3") (h "02cak2sp5cmbmhp92xrkrygynhhnfa0il0sw36pky8q1vw37fv3w")))

(define-public crate-stringer-0.1.5 (c (n "stringer") (v "0.1.5") (h "15xkid1kbssv2li6g311yz39rkjjdy7qmrhlrbvz6i2kripr92f4")))

(define-public crate-stringer-0.1.6 (c (n "stringer") (v "0.1.6") (h "1s6g3j2v0y61wlr0xcnbzahbq9y5nm6xfcv2n8bxj37bgyr6zgw2")))

