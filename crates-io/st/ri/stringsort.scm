(define-module (crates-io st ri stringsort) #:use-module (crates-io))

(define-public crate-stringsort-1.0.0 (c (n "stringsort") (v "1.0.0") (h "12k7zdkspjv0ykgxcvx21hfcxxh8hcfyi1ys69ahb36x3qdkczaj")))

(define-public crate-stringsort-2.0.0 (c (n "stringsort") (v "2.0.0") (h "18vwwn0f9mp34nwy5y8975awhlhj1xk2j4cc3p8ssxgacj09p15j")))

(define-public crate-stringsort-2.0.1 (c (n "stringsort") (v "2.0.1") (h "0ynp3izhzq6ry5qpas0vmqizdph6xhxwdwv50y39bdv0n3aynsih")))

