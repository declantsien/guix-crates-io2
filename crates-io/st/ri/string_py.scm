(define-module (crates-io st ri string_py) #:use-module (crates-io))

(define-public crate-string_py-0.1.0 (c (n "string_py") (v "0.1.0") (h "1ig88cpgmjs5ps46cnjfblsy5hsxrdvsmzf8gv6i955v5kph57nx")))

(define-public crate-string_py-0.2.0 (c (n "string_py") (v "0.2.0") (h "0mpf44n11z5zmx7iiz1100nkny1z2l61lx0l1ycad4m9xymg6p1b")))

(define-public crate-string_py-0.3.0 (c (n "string_py") (v "0.3.0") (h "1vm1gmf5s8n5gg4ga5bv15qwamkqy6gw0l6c5cjyshbkifjzj0kh")))

