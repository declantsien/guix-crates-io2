(define-module (crates-io st ri strip-ansi-escapes) #:use-module (crates-io))

(define-public crate-strip-ansi-escapes-0.1.0 (c (n "strip-ansi-escapes") (v "0.1.0") (d (list (d (n "vte") (r "^0.3.2") (d #t) (k 0)))) (h "1vmc6cwxsvp02b17b6x42mnnnn5vlc1dqbcqc2a71yms59p6fqwx")))

(define-public crate-strip-ansi-escapes-0.1.1 (c (n "strip-ansi-escapes") (v "0.1.1") (d (list (d (n "vte") (r "^0.10") (d #t) (k 0)))) (h "1n36ly9vxb1wr5q76i7995xr7c0pb1pc8g7a3a3n47vwrwwvn701")))

(define-public crate-strip-ansi-escapes-0.1.2 (c (n "strip-ansi-escapes") (v "0.1.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "vte") (r "^0.11") (k 0)))) (h "0pih0x46v7gaqa3ld14pn7121cmjwzpszldgyw3dsvszfmjyvl6s") (y #t)))

(define-public crate-strip-ansi-escapes-0.2.0 (c (n "strip-ansi-escapes") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "vte") (r "^0.11") (k 0)))) (h "1ymwcax1vyacqxx5xisfsynm7n1bvmhskvsaylac915k8gwqxzsm")))

