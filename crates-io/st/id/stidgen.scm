(define-module (crates-io st id stidgen) #:use-module (crates-io))

(define-public crate-stidgen-0.1.0 (c (n "stidgen") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01v3qg9b7a5bdy4hbgh1305n362i4ri7iab2j9jlxvdbwlbq1igv")))

(define-public crate-stidgen-0.1.1 (c (n "stidgen") (v "0.1.1") (d (list (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fvh3kwy71cajz2s9z2nylszyh0s22l98xh9qbp1ac21zf1pkqns")))

