(define-module (crates-io st sp stspin220) #:use-module (crates-io))

(define-public crate-stspin220-0.1.0 (c (n "stspin220") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "embedded-time") (r "^0.10.0") (d #t) (k 0)))) (h "0qrkccasanqnv8xiakc6lm76a7ijbxa67lylwjiq75prvlkwib7w")))

(define-public crate-stspin220-0.2.1 (c (n "stspin220") (v "0.2.1") (d (list (d (n "step-dir") (r "^0.2.0") (d #t) (k 0)))) (h "1lz0m0wmrlf5a86xic095r2v9xx59asy1hdnz12vxsflmk914p9a")))

(define-public crate-stspin220-0.3.0 (c (n "stspin220") (v "0.3.0") (d (list (d (n "step-dir") (r "^0.3.0") (d #t) (k 0)))) (h "0wrdlbiapsg5zpryg8r6aymia9c3283j922s169szy4m7f668w0c")))

(define-public crate-stspin220-0.4.0 (c (n "stspin220") (v "0.4.0") (d (list (d (n "step-dir") (r "^0.4.0") (f (quote ("stspin220"))) (k 0)))) (h "00p5nksn0sw6mds17fs6a7l418xzzs9ip4k53i9p5ziskhkd6xbg")))

(define-public crate-stspin220-0.4.1 (c (n "stspin220") (v "0.4.1") (d (list (d (n "step-dir") (r "^0.4.1") (f (quote ("stspin220"))) (k 0)))) (h "186kmmkhhhlqjsm53pvz4j7x0ivk67x8pnfw59yfj42b2m3gjr6z")))

(define-public crate-stspin220-0.5.0 (c (n "stspin220") (v "0.5.0") (d (list (d (n "stepper") (r "^0.5.0") (f (quote ("stspin220"))) (k 0)))) (h "124a0i1w98mfwr5hh5065p0dyjf01d757qlrskbrk1skb3xlgiv4")))

(define-public crate-stspin220-0.6.0 (c (n "stspin220") (v "0.6.0") (d (list (d (n "stepper") (r "^0.6.0") (f (quote ("stspin220"))) (k 0)))) (h "0nmp028lp892963y8kc3pvhghh8aww7ds1ps048y7s3g3009p42m")))

