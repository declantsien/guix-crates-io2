(define-module (crates-io st oi stoic-dotfiles) #:use-module (crates-io))

(define-public crate-stoic-dotfiles-0.1.0 (c (n "stoic-dotfiles") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1h9k65nf196qw2nphxh1g5c56nk9iidsw74c3syyfkim6nqcbhx1")))

(define-public crate-stoic-dotfiles-0.1.1 (c (n "stoic-dotfiles") (v "0.1.1") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "07jdj6zhhrndkw9scxb7wdvmxcdk8wc8fvdpbnyvhsgpm2z9qln1")))

(define-public crate-stoic-dotfiles-0.1.2 (c (n "stoic-dotfiles") (v "0.1.2") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "11z0k0zb1wxxya02i7znc7cwvbz740z0a2f84wqn3s6lnh1mbhf2")))

(define-public crate-stoic-dotfiles-0.1.3 (c (n "stoic-dotfiles") (v "0.1.3") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "12fi93cfpwmsg0kg98adb70dzrw8syrzy7pk8n5jaqsxmavq97mp")))

(define-public crate-stoic-dotfiles-0.2.0 (c (n "stoic-dotfiles") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1hp2gsfhpx0pnw073h9l9qsw130z7bzv8i9j4kq6c08vcqwhwz38")))

(define-public crate-stoic-dotfiles-0.2.1 (c (n "stoic-dotfiles") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1g9r9rj558gqgrsb1d2h3hgyi1ayqy59v2bxasis1zypmsyl4c0w")))

