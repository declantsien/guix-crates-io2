(define-module (crates-io st oi stoicsay) #:use-module (crates-io))

(define-public crate-stoicsay-0.1.0 (c (n "stoicsay") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05lpjxi5bz2fhzd3hkya7ai0ys7nw3gzblm0knrajfgv6y9qlw9q")))

