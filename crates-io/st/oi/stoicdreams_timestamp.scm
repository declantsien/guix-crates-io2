(define-module (crates-io st oi stoicdreams_timestamp) #:use-module (crates-io))

(define-public crate-stoicdreams_timestamp-0.1.0 (c (n "stoicdreams_timestamp") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16agawgifabblfypbf968cq8qg9d5lzsmrq5712lwvqi1nqgqn8w")))

(define-public crate-stoicdreams_timestamp-0.1.1 (c (n "stoicdreams_timestamp") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "080cygzf0frs28csaxp8zagdwaxv1787wqi73nngp7mk3nv5cigs")))

(define-public crate-stoicdreams_timestamp-0.1.2 (c (n "stoicdreams_timestamp") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zbfakwidxs2arhlxa0672bwph2ppzlx2v4a309yg340c3zk9qgn") (f (quote (("default"))))))

