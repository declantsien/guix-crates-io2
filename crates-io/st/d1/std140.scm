(define-module (crates-io st d1 std140) #:use-module (crates-io))

(define-public crate-std140-0.1.0 (c (n "std140") (v "0.1.0") (d (list (d (n "std140-macros") (r "^0.1.0") (d #t) (k 0)))) (h "111fy12xjmwr6lzcqavhkf0fsh62d3znzl12vw29r2f4kc5140br")))

(define-public crate-std140-0.1.1 (c (n "std140") (v "0.1.1") (d (list (d (n "std140-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1bmbcxy5szj1qxv7c8wkgmvm5r2q19fq48nxg8vrvyva0q1yk4m6")))

(define-public crate-std140-0.1.2 (c (n "std140") (v "0.1.2") (d (list (d (n "std140-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0wdsxvjg3qr5mhsh5r9b51dqa073zhf136ydskk8dyhnmlq7aixi")))

(define-public crate-std140-0.2.0 (c (n "std140") (v "0.2.0") (d (list (d (n "std140-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0ijiiwzkp1269h5sbk9sg9mc2h44w2cg0v5mizzwix5knvjyl9mc")))

(define-public crate-std140-0.2.1 (c (n "std140") (v "0.2.1") (d (list (d (n "std140-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1f7vinm8yx1s4y78di9s2hs1vkhr3pk8xpim7rclq5h4dc3ydjhw")))

(define-public crate-std140-0.2.2 (c (n "std140") (v "0.2.2") (d (list (d (n "std140-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1b9li5547qgyafaqn1rvmw4m45bn5k1gjm9wmj3kpg1lr7iijklr")))

(define-public crate-std140-0.2.3 (c (n "std140") (v "0.2.3") (d (list (d (n "std140-macros") (r "^0.1.1") (d #t) (k 0)))) (h "13hxd7zhjwjyxc9kpcx1aiynp0wi3din15l9hdgvdb7jbn7fylk4")))

(define-public crate-std140-0.2.4 (c (n "std140") (v "0.2.4") (d (list (d (n "std140-macros") (r "^0.1.2") (d #t) (k 0)))) (h "127ajrjij61cci6ypd0rv5whzpvjyn647w6qmnaqgqz6y3l2h70h")))

(define-public crate-std140-0.2.5 (c (n "std140") (v "0.2.5") (d (list (d (n "std140-macros") (r "^0.1.2") (d #t) (k 0)))) (h "19brnaa58mmnkas0zkqy7qr2vgk5s0kvqsqhjhhr3vpljzi6wb6m")))

(define-public crate-std140-0.2.6 (c (n "std140") (v "0.2.6") (d (list (d (n "std140-macros") (r "^0.1.2") (d #t) (k 0)))) (h "0m9mpc3kjj3qzigzjhzv7kq7qr35k6kq5zvwa1891bd5avc3qagp")))

