(define-module (crates-io st d1 std140-macros) #:use-module (crates-io))

(define-public crate-std140-macros-0.1.0 (c (n "std140-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1fdyizw3ymr4wcrkx493lig5xak23dzs04p4b99hz2rlxz1aldfq")))

(define-public crate-std140-macros-0.1.1 (c (n "std140-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1lc3c2jlbw6n5z04p7b4dkxlsych9wfh0lq9ih02pch7avv2f9mb")))

(define-public crate-std140-macros-0.1.2 (c (n "std140-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "07aqmvndbzawrdpm5v7ra69ndg7aaa1mkikcngarsdp8q3341ryi")))

