(define-module (crates-io st d1 std1) #:use-module (crates-io))

(define-public crate-std1-0.1.0 (c (n "std1") (v "0.1.0") (h "0lkv8ksq1yc0ndvy6l3qj2sx41zqsxh28312cx8gwjcrhcjfi92l") (y #t)))

(define-public crate-std1-0.2.0 (c (n "std1") (v "0.2.0") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)))) (h "1pammkv2vnfpm95sa5wzpfzananxwf7r98fx7y2g2bymssk1dhf4")))

