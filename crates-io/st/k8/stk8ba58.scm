(define-module (crates-io st k8 stk8ba58) #:use-module (crates-io))

(define-public crate-stk8ba58-0.1.0 (c (n "stk8ba58") (v "0.1.0") (d (list (d (n "accelerometer") (r "^0.12.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "15zlqfy66rgf427psd6x75nfbf488n1yd2cgx387n75m9jdw3q2n")))

(define-public crate-stk8ba58-0.2.0 (c (n "stk8ba58") (v "0.2.0") (d (list (d (n "accelerometer-hal") (r "^0.12.0") (d #t) (k 0) (p "accelerometer")) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0dkhlly1b5mrhsxd908fjwz3h80i708h28lh8yjzmdw2cnn88j0n")))

(define-public crate-stk8ba58-0.2.1 (c (n "stk8ba58") (v "0.2.1") (d (list (d (n "accelerometer-hal") (r "^0.12.0") (d #t) (k 0) (p "accelerometer")) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "179hm15ar5d1fvph38dfxcyli4340ar5wzsbhiprbhaxvxlq33qf")))

(define-public crate-stk8ba58-0.3.0 (c (n "stk8ba58") (v "0.3.0") (d (list (d (n "accelerometer-hal") (r "^0.12.0") (d #t) (k 0) (p "accelerometer")) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1jwdz3kdskfk26gmga6d8wv1zqb5i5fj51r7pcljdxlzvsmpkqak")))

(define-public crate-stk8ba58-0.3.1 (c (n "stk8ba58") (v "0.3.1") (d (list (d (n "accelerometer-hal") (r "^0.12.0") (d #t) (k 0) (p "accelerometer")) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1505f2n48304d6pi7m65wpmp4wjaijm1ql544rph97wbz80b6fny")))

(define-public crate-stk8ba58-1.0.0 (c (n "stk8ba58") (v "1.0.0") (d (list (d (n "accelerometer-hal") (r "^0.12.0") (d #t) (k 0) (p "accelerometer")) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1v6z4ghfdwgw5sgs6l31x0b61sllyyzbpr4m4xd2r9ijc8p24a33")))

(define-public crate-stk8ba58-1.0.1 (c (n "stk8ba58") (v "1.0.1") (d (list (d (n "accelerometer-hal") (r "^0.12") (d #t) (k 0) (p "accelerometer")) (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (d #t) (k 2)) (d (n "defmt-rtt") (r "^0.4.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "panic-probe") (r "^0.3.1") (d #t) (k 2)) (d (n "rp-pico") (r "^0.8.0") (d #t) (k 2)))) (h "0czx49baqgwqgrqakrlncg90zs0zn6x7ilad7fxlf3844lvn1djz")))

(define-public crate-stk8ba58-1.0.2 (c (n "stk8ba58") (v "1.0.2") (d (list (d (n "accelerometer-hal") (r "^0.12") (d #t) (k 0) (p "accelerometer")) (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (d #t) (k 2)) (d (n "defmt-rtt") (r "^0.4.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "panic-probe") (r "^0.3.1") (d #t) (k 2)) (d (n "rp-pico") (r "^0.8.0") (d #t) (k 2)))) (h "0qrs13aw0fzd6w7cx12xs4dfyy0861g2abhfvj88r4h1lymwaklf")))

