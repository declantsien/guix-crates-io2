(define-module (crates-io st rp strp_macros) #:use-module (crates-io))

(define-public crate-strp_macros-1.0.0 (c (n "strp_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1f8ah6lb092qxvazbclgq33dpqhan7w7ask72ff94qsfnfqx21dc")))

(define-public crate-strp_macros-1.0.1 (c (n "strp_macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0qi5faihmr32v7p36vani92ndbvi4qm1dhnnrh2lxpnvnpybds8f")))

(define-public crate-strp_macros-2.0.0 (c (n "strp_macros") (v "2.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "18s7h7i829y6x7a667hq9jjgwfnchfsknnzhjzlr9bn1bjlpld3f") (f (quote (("no_std"))))))

(define-public crate-strp_macros-2.0.1 (c (n "strp_macros") (v "2.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1i73aqgjkb53z9jr5czzq19iqdz70r2zs4891nq760avyika55kg") (f (quote (("no_std"))))))

(define-public crate-strp_macros-2.0.2 (c (n "strp_macros") (v "2.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1bc2kmjl63w92nlhmhq8n3gima33vfihfvvdg40g1xry6al3gsn3") (f (quote (("no_std"))))))

(define-public crate-strp_macros-2.1.0 (c (n "strp_macros") (v "2.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1vhwdchm04b7mfaq4x4z6a9c6b80rd8pcfcwy9mbifd0bhdsvlfi") (f (quote (("std"))))))

(define-public crate-strp_macros-2.1.1 (c (n "strp_macros") (v "2.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "05lr0lkxzjdf3y1wmk4jdzxymphys8v6b734qdzbf0kvxpifinmb") (f (quote (("std"))))))

(define-public crate-strp_macros-3.0.0 (c (n "strp_macros") (v "3.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1fy9k2l84mv1p4kgyxa0dwwxpqfv2q8p17hnrfqzyx6k9f63r3f0") (f (quote (("std"))))))

(define-public crate-strp_macros-3.1.0 (c (n "strp_macros") (v "3.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1ia1s84kmv3cb8vkc669s24s3cypnbqzlk3zsayvlf4cdn7pxmb1") (f (quote (("std"))))))

