(define-module (crates-io st rp strprox) #:use-module (crates-io))

(define-public crate-strprox-0.1.0 (c (n "strprox") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "02nmnv2704k0s5hx43aqkm5i4lvwk5iz2jyf23f3lj90si95wbgl")))

(define-public crate-strprox-0.1.1 (c (n "strprox") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0varxvljxnshfb7vid5d4g33b90dg3xl45v807i58fnf1ds0gpr6")))

(define-public crate-strprox-0.1.2 (c (n "strprox") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0dm07074j5masplsbcmycfzjzlqwn9par033k58hkadnv3d47wdk")))

(define-public crate-strprox-0.1.3 (c (n "strprox") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ql14vgc9b0i7fmalyhf6jyzq0jlqszlmjrd2cb928vaa8xfhxcc")))

(define-public crate-strprox-0.1.4 (c (n "strprox") (v "0.1.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09z1vbaarbi2gmh10csvq2knv1hk2acclhli9fv056ksxnmr8xrx") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1.5 (c (n "strprox") (v "0.1.5") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qimy0fpvphr4h4ap3whgxyr30pamcmw0n41f3jhp0z3385102n7") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1.6 (c (n "strprox") (v "0.1.6") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m6d83fd4zlmyflw6pzzxln2iz2dgwmbiclfcpnj7xgk6pcz27z1") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1.7 (c (n "strprox") (v "0.1.7") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1rkj4jm74rn18lbxah6sjgzzsh1v5s1a3r757zji1b85zw3z4jy7") (f (quote (("wasm" "wasm-bindgen")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1.8 (c (n "strprox") (v "0.1.8") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0hrcihvsnw76yppf1935p1y4i8aqxfi19dj6z6ginv9wngs9zq41") (f (quote (("wasm" "wasm-bindgen")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1.9 (c (n "strprox") (v "0.1.9") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "04va3fh11nlnci5q0d4jz9jbqz87ay6xbgqmrjk0xid7m6rpq8ky") (f (quote (("wasm" "wasm-bindgen")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1.10 (c (n "strprox") (v "0.1.10") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "07d39s5nb3cg9cb3vis0kj5d597jlrzmi3b2yzni72h1g74f7919") (f (quote (("wasm" "wasm-bindgen")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.2.0 (c (n "strprox") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "12nfmzi8z1qx76x3038rwpw3hkzv88kd82gjn3krnkqbgk043i4x") (f (quote (("wasm" "wasm-bindgen" "js-sys")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.3.0 (c (n "strprox") (v "0.3.0") (d (list (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1nd4qqap0d1nnl0wzymikw9m4larfznxlw19hrfly8zfnc7z12bb") (f (quote (("wasm" "wasm-bindgen" "js-sys")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.3.1 (c (n "strprox") (v "0.3.1") (d (list (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "08js35zqw3k66hsrvd4m92pxxmgp3mykzmchc3dm2al8k6cicx5c") (f (quote (("wasm" "wasm-bindgen" "js-sys")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.3.2 (c (n "strprox") (v "0.3.2") (d (list (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0ja4pf4hnyk7l62l9ajrr8jvvn7lvj7yd4gz75d16zjq1iyjzzlx") (f (quote (("wasm" "wasm-bindgen" "js-sys")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.3.3 (c (n "strprox") (v "0.3.3") (d (list (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1ryzjw70clzxnhnxy47xnzlzm87rl66sgm4hm020shab7bb4h060") (f (quote (("wasm" "wasm-bindgen" "js-sys")))) (s 2) (e (quote (("serde" "dep:serde"))))))

