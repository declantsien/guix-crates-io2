(define-module (crates-io st rp strptime) #:use-module (crates-io))

(define-public crate-strptime-0.1.0 (c (n "strptime") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "0j7aaxs5l3swmzl5kipc4fkpgzmp22d92c23pj20zqirxry1bwr0") (r "1.70")))

(define-public crate-strptime-0.2.0 (c (n "strptime") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "0dcip8lhj9p26n0bk2mkkv4clxxip0wh1j306r9449cv7621zzsy") (r "1.70")))

(define-public crate-strptime-0.2.1 (c (n "strptime") (v "0.2.1") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "1dv09mqap6533vb62di4d5f9pciqsh9zsbqm4p1l6zq92fyn5qlx") (r "1.70")))

(define-public crate-strptime-0.2.2 (c (n "strptime") (v "0.2.2") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "18vpnlakf1hvnrcm00in2rl7sdvbbi1z8hy9wmshkxl7xz5lcj6j") (r "1.70")))

(define-public crate-strptime-0.2.3 (c (n "strptime") (v "0.2.3") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "0fn4ib3isz37l65yym6zvvfv7zc0ypp60avnjq3dr6w9lb4457rw") (r "1.70")))

(define-public crate-strptime-1.0.0 (c (n "strptime") (v "1.0.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "0bxpidlscbdj9zmcwkszakx5gyqvjj1r3bq9dg79ngg46a4akvik") (r "1.70")))

(define-public crate-strptime-1.1.0 (c (n "strptime") (v "1.1.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "1z13nl8fdgfi22j3ijpdaw0f38fw4blmrk2h42s3mmzp6lzh0kv9") (y #t) (r "1.70")))

(define-public crate-strptime-1.1.1 (c (n "strptime") (v "1.1.1") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "1q3ygna50vg51rica2qmfl3zh3kpg13vhjd5zqfhjsa830cx3zx7") (r "1.70")))

(define-public crate-strptime-1.1.2 (c (n "strptime") (v "1.1.2") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "06qsn92rbbdvq2isw6lphlnmzg3m99pmglsg4glvnvjafpqlkyhi") (r "1.70")))

(define-public crate-strptime-1.1.3 (c (n "strptime") (v "1.1.3") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "0z2lxr1baagwysxa97bjj8ixsrqfmsk0g040mgam9k71rjbwr45f") (r "1.70")))

