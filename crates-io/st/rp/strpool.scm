(define-module (crates-io st rp strpool) #:use-module (crates-io))

(define-public crate-strpool-1.0.0 (c (n "strpool") (v "1.0.0") (d (list (d (n "ahash") (r "^0.8.3") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 1)))) (h "0zv23dd1khakjjr34z2549pn5pyhdaj9877738jw3cp9nmbjwkck")))

(define-public crate-strpool-1.0.1 (c (n "strpool") (v "1.0.1") (d (list (d (n "ahash") (r "^0.8.3") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 1)))) (h "13y86s3qapbkahdzdps4g1y3hqjdz6nlk1b1w3k718kcmj3prnn5")))

(define-public crate-strpool-1.0.2 (c (n "strpool") (v "1.0.2") (d (list (d (n "ahash") (r "^0.8.3") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 1)))) (h "1z55p8yxr5gbqn05779mvn1927zi3d3jpvinxs8glcivbzs923fy")))

(define-public crate-strpool-1.0.3 (c (n "strpool") (v "1.0.3") (d (list (d (n "ahash") (r "^0.8.3") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 1)))) (h "1ccy7dn25wmfmj96hma5jd0q08b2z1py1hvbspsr8xil29qhprna")))

(define-public crate-strpool-1.0.4 (c (n "strpool") (v "1.0.4") (d (list (d (n "ahash") (r "^0.8") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 1)))) (h "1vwn0272b3aa2ksr09j1v9g2b2xws5vvvi650pna5fpqzh84q738")))

(define-public crate-strpool-1.1.0 (c (n "strpool") (v "1.1.0") (d (list (d (n "ahash") (r "^0.8") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 1)))) (h "1km8j7l6m3nk5mb8zalkz1bcdk9xbj7kzhbj98h0kyh8xh554y88")))

(define-public crate-strpool-1.1.1 (c (n "strpool") (v "1.1.1") (d (list (d (n "cityhasher") (r "^0.1") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1hj0qq9diivddkmz30x8qgyk2d3gczgsqxc3rmqx0qm95b6blf3q") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde" "std"))))))

