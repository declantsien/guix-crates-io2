(define-module (crates-io st ow stowaway-derive) #:use-module (crates-io))

(define-public crate-stowaway-derive-0.1.0 (c (n "stowaway-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "stowaway") (r "^2.0.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "1n0dp4hy492j423x2z4aq94y4p9g9srigvmchcd7c5mcsm6adiyp")))

