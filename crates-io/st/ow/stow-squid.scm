(define-module (crates-io st ow stow-squid) #:use-module (crates-io))

(define-public crate-stow-squid-0.1.1 (c (n "stow-squid") (v "0.1.1") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1z57yy50cmx0rq13ddq7phi5k5z7h3w6b4sxnxkj2zs90l6dhwjj")))

