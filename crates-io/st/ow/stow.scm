(define-module (crates-io st ow stow) #:use-module (crates-io))

(define-public crate-stow-0.1.0 (c (n "stow") (v "0.1.0") (h "0gyr49fj0krrd8zqc9a9dvmkaf9vdpbym1qdhr0m4xkar80ravq9")))

(define-public crate-stow-0.2.0 (c (n "stow") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt" "macros"))) (d #t) (k 0)))) (h "1fx76lqqjbk3crgmmnfxx1vd63mblcnhhw9wxn1dl1h7g1rv59bi")))

