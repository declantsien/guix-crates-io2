(define-module (crates-io st ow stowaway) #:use-module (crates-io))

(define-public crate-stowaway-0.1.0 (c (n "stowaway") (v "0.1.0") (h "1p9fbmg6pa46dnys3y9riiaspdwlzxbgpn57v5r21ry4bq1avnc8") (y #t)))

(define-public crate-stowaway-0.1.1 (c (n "stowaway") (v "0.1.1") (h "13dcfbg70076fgq7ay5sf4c0ckjzc0vd7q82a6l6rqywbjx4am75") (y #t)))

(define-public crate-stowaway-1.0.0 (c (n "stowaway") (v "1.0.0") (h "1mwvk2igaqcwcyd39zq89yib8iykmg459y6k4907103qsmzis2r1") (y #t)))

(define-public crate-stowaway-1.1.0 (c (n "stowaway") (v "1.1.0") (h "158sjvw5z0jd2nl0rgwvq2rxmm0rzqspxnv503cy568m113k59dq") (y #t)))

(define-public crate-stowaway-1.1.1 (c (n "stowaway") (v "1.1.1") (h "06sqcrdlx267dv6mfhv3h8mv8c01sw4v8ggnnbwl09xgyzzksizk") (y #t)))

(define-public crate-stowaway-1.1.2 (c (n "stowaway") (v "1.1.2") (h "14f806bdp4jy7svjzzaib1i6m5fwh3jyixzamq73r1l2wxhkvc9j") (y #t)))

(define-public crate-stowaway-2.0.0 (c (n "stowaway") (v "2.0.0") (d (list (d (n "rustversion") (r "^1.0.2") (d #t) (k 0)))) (h "1dlzg7gxlfcwxikgqvlfpbnhqd3gmwm4avyvhfj7q2p2kncp5yxa") (y #t)))

(define-public crate-stowaway-2.1.0 (c (n "stowaway") (v "2.1.0") (d (list (d (n "rustversion") (r "^1.0.2") (d #t) (k 0)) (d (n "stowaway-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1c1rran5s7vgz7mkh33b4i3y1n9g1niyg83f69r6914ak7g33bif") (f (quote (("derive" "stowaway-derive")))) (y #t)))

(define-public crate-stowaway-2.2.0 (c (n "stowaway") (v "2.2.0") (d (list (d (n "rustversion") (r "^1.0.2") (d #t) (k 0)) (d (n "stowaway-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0yinj01v4cl4pkr6928p2b1fwd01qv59h6h3kxmqcd6msl6xaj1l") (f (quote (("derive" "stowaway-derive") ("default" "derive")))) (y #t)))

(define-public crate-stowaway-2.3.0 (c (n "stowaway") (v "2.3.0") (d (list (d (n "rustversion") (r "^1.0.2") (d #t) (k 0)) (d (n "stowaway-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1aniklblfxww785j4g1pmbaj625av4gyp0qphq5938bdws010ygf") (f (quote (("derive" "stowaway-derive")))) (y #t)))

(define-public crate-stowaway-2.3.1 (c (n "stowaway") (v "2.3.1") (d (list (d (n "rustversion") (r "^1.0.2") (d #t) (k 0)) (d (n "stowaway-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0k70ni4kcq4nvm3bvd9i1ihh4kcip373dy8jq4dfgrgzjililmaq") (f (quote (("derive" "stowaway-derive")))) (y #t)))

