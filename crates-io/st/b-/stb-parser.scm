(define-module (crates-io st b- stb-parser) #:use-module (crates-io))

(define-public crate-stb-parser-0.1.0-snapshot (c (n "stb-parser") (v "0.1.0-snapshot") (d (list (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "1fgbpxfa2jgbsvsqr3kf1jxzqirmhlz5k8959vh76ymqz77vlz5g")))

