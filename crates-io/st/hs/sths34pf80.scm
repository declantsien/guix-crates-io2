(define-module (crates-io st hs sths34pf80) #:use-module (crates-io))

(define-public crate-sths34pf80-0.1.0 (c (n "sths34pf80") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1rg5msy0969dqh4p6ds9jbcib3krpg7k08akkjx7j9h6k2xznjfi") (f (quote (("default"))))))

(define-public crate-sths34pf80-0.1.1 (c (n "sths34pf80") (v "0.1.1") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1d8dkdxw33hq896zxiv105vycjqldjpmnr0ygdahp9sykbsj3mas") (f (quote (("default"))))))

(define-public crate-sths34pf80-0.1.2 (c (n "sths34pf80") (v "0.1.2") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0dj1lf4h72hphq5rry70jwyqnva5iwjvqd233948f514si56f80z") (f (quote (("default"))))))

(define-public crate-sths34pf80-0.1.3 (c (n "sths34pf80") (v "0.1.3") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "13sbh1c0dzfbizbb4zvf801gh55nf4dbkgq64i9jms8kb52pzzd4") (f (quote (("default"))))))

(define-public crate-sths34pf80-0.1.4 (c (n "sths34pf80") (v "0.1.4") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "03sq32wg59s19mawv4aj71j4wgyfp23nsr2wm8cas0qfk3s9iqra") (f (quote (("default"))))))

(define-public crate-sths34pf80-0.1.5 (c (n "sths34pf80") (v "0.1.5") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0ajmvb8zzrqp9wh7cm4ks63vgls3rf1jpxf41pyh0fz92hn9nhhi") (f (quote (("default"))))))

(define-public crate-sths34pf80-0.1.6 (c (n "sths34pf80") (v "0.1.6") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1gswrrj0rcwvj42iv238qfal6ds83wqvd1bjwlwfjbgzb6ysjjfi") (f (quote (("default"))))))

(define-public crate-sths34pf80-0.1.7 (c (n "sths34pf80") (v "0.1.7") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embuild") (r "=0.31.4") (d #t) (k 1)) (d (n "log") (r "^0.4.18") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1c5f8a1iwgpqlbcj43kgyxymk87s0g6c9kpryb0ivpqz4nf7191f") (f (quote (("default"))))))

