(define-module (crates-io st ew stewart-utils) #:use-module (crates-io))

(define-public crate-stewart-utils-0.1.0 (c (n "stewart-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "stewart") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "0g2zy870wzcb3w0mzkvl1qycpx20a5s1095qw27zns3k7zc0p7n0")))

(define-public crate-stewart-utils-0.2.0 (c (n "stewart-utils") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "stewart") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "0nbyjzrqlxzh47zgdi9spx384hyl20b3jqir6scmfj7wrv1k28m2")))

(define-public crate-stewart-utils-0.3.0 (c (n "stewart-utils") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "stewart") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "09x583fqbfyzh0xh4ll40qmd3b6da7y0n028d2p95q7bzvv4p5nn")))

