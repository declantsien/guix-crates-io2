(define-module (crates-io st ew stew) #:use-module (crates-io))

(define-public crate-stew-0.1.0 (c (n "stew") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.7") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)))) (h "0g7gw219h6dmdyv9nfpapmd6ys49g5ayy1ywlif6qnrhxqrbxa2h") (f (quote (("output-test-images"))))))

(define-public crate-stew-0.2.0 (c (n "stew") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4.7") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "19mjgmw5mq9sfsp73pns59ww4rvz020ra391l3dflhjz6hn2dcc2") (f (quote (("output-test-images"))))))

