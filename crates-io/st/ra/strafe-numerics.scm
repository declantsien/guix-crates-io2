(define-module (crates-io st ra strafe-numerics) #:use-module (crates-io))

(define-public crate-strafe-numerics-0.1.0 (c (n "strafe-numerics") (v "0.1.0") (d (list (d (n "r2rs-base") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-mass") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-nmath") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-rfit") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-stats") (r "~0.1") (d #t) (k 0)))) (h "14qwl3ckfms9ygqpvl7y8fkw85bndsr3kzdk009zw5cwbxgp430k") (y #t)))

(define-public crate-strafe-numerics-0.1.1 (c (n "strafe-numerics") (v "0.1.1") (d (list (d (n "r2rs-base") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-mass") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-nmath") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-rfit") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-stats") (r "~0.1") (d #t) (k 0)))) (h "12kgd2aqygsgns9czsipnyjm8v538k2b2880y37w7ddir2sbnh8f")))

