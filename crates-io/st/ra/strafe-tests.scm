(define-module (crates-io st ra strafe-tests) #:use-module (crates-io))

(define-public crate-strafe-tests-0.1.0 (c (n "strafe-tests") (v "0.1.0") (d (list (d (n "r2rs-mass") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-nmath") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-rfit") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-stats") (r "~0.1") (d #t) (k 0)))) (h "0ymhzp1r09z5m2sz55hd9my9n3darabvfcl35vxglvpg7fagdk2i") (y #t)))

(define-public crate-strafe-tests-0.1.1 (c (n "strafe-tests") (v "0.1.1") (d (list (d (n "r2rs-mass") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-nmath") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-rfit") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-stats") (r "~0.1") (d #t) (k 0)))) (h "0ai8zh6abha68h0va3xil9kyzc697834d01f7z2y7k37bs35i4mj")))

