(define-module (crates-io st ra strainer) #:use-module (crates-io))

(define-public crate-strainer-0.1.0 (c (n "strainer") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "syntect") (r "^4.1.0") (o #t) (d #t) (k 0)))) (h "0nhcnvgxr6jfa1hvr525r9xwj6c1zqs9mhdwycpz0n1xfm7cm8lj") (f (quote (("syntax-highlighting" "syntect"))))))

(define-public crate-strainer-0.2.0 (c (n "strainer") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "syntect") (r "^4.1.0") (o #t) (d #t) (k 0)))) (h "17b41anxjagkdd7c2rivqr3f3xfr8bp00g0gpq203j7abhbpw4al") (f (quote (("syntax-highlighting" "syntect"))))))

(define-public crate-strainer-0.3.0 (c (n "strainer") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "syntect") (r "^4.1.0") (o #t) (d #t) (k 0)))) (h "022iasz7an1c5lvd7sdlyybrivm2j8cww4az1x5p832d9ylgm3x8") (f (quote (("syntax-highlighting" "syntect"))))))

(define-public crate-strainer-0.4.0 (c (n "strainer") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "syntect") (r "^4.1.0") (o #t) (d #t) (k 0)))) (h "1dmwj9wb4pamdd28swvmbn1sn8xzd200gdlhg58lhl7vq597mng9") (f (quote (("syntax-highlighting" "syntect"))))))

(define-public crate-strainer-0.5.0 (c (n "strainer") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (k 0)) (d (n "syntect") (r "^4.1.0") (o #t) (d #t) (k 0)))) (h "178rnh70dgls4b7dxyvjyw4f495cc9816i37aprdh5mr339rhjkz") (f (quote (("syntax-highlighting" "syntect"))))))

