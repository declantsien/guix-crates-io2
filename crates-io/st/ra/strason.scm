(define-module (crates-io st ra strason) #:use-module (crates-io))

(define-public crate-strason-0.1.0 (c (n "strason") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)))) (h "0qar70rjm6a73qma7c0qa4i31fir4b9zd4yx07x4z6hkdkd12my9")))

(define-public crate-strason-0.2.0 (c (n "strason") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)))) (h "0n8gy9aqzmaliqpz4gr9p508jq4bkwv6c6sfz0cr8i48v0qfm9nq")))

(define-public crate-strason-0.2.1 (c (n "strason") (v "0.2.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)))) (h "1hjs5dh4f29c7f8mjlzsgvf2yvdq2jvfzszz6sfsb5hxfdil65fb")))

(define-public crate-strason-0.3.0 (c (n "strason") (v "0.3.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)))) (h "1cz9lby7bdfnp2vn061z8v4795p0hi4qip9ivr6w2x7drshv7084")))

(define-public crate-strason-0.3.1 (c (n "strason") (v "0.3.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 2)))) (h "01wymnv2ysg7plqsbnmw8g16k1lzxcrfixirc7r16c55pkhv64ia")))

(define-public crate-strason-0.3.2 (c (n "strason") (v "0.3.2") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 2)))) (h "042rkyzvag1zsmcrq35v8i97yj6pkn341l9bwsjcxc52027fkmqh")))

(define-public crate-strason-0.3.3 (c (n "strason") (v "0.3.3") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 2)))) (h "058iccrlsrg2yrniqp64bdwjp47n9b3gw4prgfwj1cg7fjd0j00f")))

(define-public crate-strason-0.3.4 (c (n "strason") (v "0.3.4") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 2)))) (h "1d8jszkmlrb8z0mhc7q0inkkmncr690chc81fknlcb642l2gz8y8")))

(define-public crate-strason-0.4.0 (c (n "strason") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vrc7qvj2xzpiwjdcyd3pipy808a6h63hwl0af6knn1cwf50klfw") (f (quote (("utf16") ("default" "utf16"))))))

