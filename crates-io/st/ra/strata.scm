(define-module (crates-io st ra strata) #:use-module (crates-io))

(define-public crate-strata-0.1.0 (c (n "strata") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1nzal5jzp8arfc6hg0k3pq8y2bk3yrc3hcqhvy74mf6wpnqv5218")))

(define-public crate-strata-0.1.1 (c (n "strata") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0jn2qfvm79ckw8glln0kny5mh6dika1xqai34wyha9pz3vrpc9df")))

