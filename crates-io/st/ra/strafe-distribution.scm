(define-module (crates-io st ra strafe-distribution) #:use-module (crates-io))

(define-public crate-strafe-distribution-0.1.0 (c (n "strafe-distribution") (v "0.1.0") (d (list (d (n "r2rs-nmath") (r "~0.1") (d #t) (k 0)))) (h "1njjr0ll8ixd5s9y440r8fnp8xy7cy2sfa5fmkydr41chn9yqhcx") (y #t)))

(define-public crate-strafe-distribution-0.1.1 (c (n "strafe-distribution") (v "0.1.1") (d (list (d (n "r2rs-nmath") (r "~0.1") (d #t) (k 0)))) (h "1zg2p4pzzvkk86wl4qmgkgvwmm9v702r65a5jc66g93azbb1n2qj")))

