(define-module (crates-io st ra stratum-common) #:use-module (crates-io))

(define-public crate-stratum-common-0.1.0 (c (n "stratum-common") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.29.1") (o #t) (d #t) (k 0)))) (h "1swzraz00mg4bm6r5dxjcc0yf3jz27lwlk2yljmp99ginh7f814f")))

(define-public crate-stratum-common-1.0.0 (c (n "stratum-common") (v "1.0.0") (d (list (d (n "bitcoin") (r "^0.29.1") (o #t) (d #t) (k 0)))) (h "1dhw1d55b0qfp4rjjhgvfwisc1fd1r6qc59q1p9rbc2mqm6jvmmj")))

