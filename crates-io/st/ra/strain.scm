(define-module (crates-io st ra strain) #:use-module (crates-io))

(define-public crate-strain-0.0.1 (c (n "strain") (v "0.0.1") (d (list (d (n "rug") (r "^1.24.1") (d #t) (k 0)))) (h "0d4nxghf7ssgsa3y87kc1x6gfiysdabj93hrv4c7gfsa510a1k46")))

(define-public crate-strain-0.0.4 (c (n "strain") (v "0.0.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.6.2") (f (quote ("default" "use-system-libs"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rug") (r "^1.24.1") (f (quote ("default" "complex"))) (d #t) (k 0)))) (h "03rdd2bkivmh5f6h491r4fd3w577sl9y1cxfbnyxc9jsw5zcmyc9")))

(define-public crate-strain-0.0.5 (c (n "strain") (v "0.0.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.6.2") (f (quote ("default" "use-system-libs"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rug") (r "^1.24.1") (f (quote ("default" "complex"))) (d #t) (k 0)))) (h "1pc902mz5c9f02f614wpsxv4mhacvda1gviihj723cna3nqnzcpp")))

(define-public crate-strain-0.0.6 (c (n "strain") (v "0.0.6") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.6.2") (f (quote ("default" "use-system-libs"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rug") (r "^1.24.1") (f (quote ("default" "complex"))) (d #t) (k 0)))) (h "0nijfr1qlv0w3li2r0hs41ag2myz4awk90rqky8c30831x9fyq82")))

(define-public crate-strain-0.1.0 (c (n "strain") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.6.2") (f (quote ("default" "use-system-libs"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rug") (r "^1.24.1") (f (quote ("default" "complex"))) (d #t) (k 0)))) (h "0b6sb25mys2r09xaabbgdmwq61hyb9s2yim4sy7snn8qr0qm0mgj")))

