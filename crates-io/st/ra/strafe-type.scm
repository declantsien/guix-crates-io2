(define-module (crates-io st ra strafe-type) #:use-module (crates-io))

(define-public crate-strafe-type-0.1.0 (c (n "strafe-type") (v "0.1.0") (d (list (d (n "nalgebra") (r "~0.32") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "numfmt") (r "~1") (d #t) (k 0)) (d (n "polars") (r "~0.38") (d #t) (k 0)) (d (n "strafe-error") (r "~0.1") (d #t) (k 0)))) (h "19ycjzqyrb9p91kjvz4l0jx5ggpfqha1hjsb2ci85cg6n9d5w3sp") (y #t)))

(define-public crate-strafe-type-0.1.1 (c (n "strafe-type") (v "0.1.1") (d (list (d (n "nalgebra") (r "~0.32") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "numfmt") (r "~1") (d #t) (k 0)) (d (n "polars") (r "~0.38") (d #t) (k 0)) (d (n "strafe-error") (r "~0.1") (d #t) (k 0)))) (h "0gndgk923gdfkzh9friii8nw50a24lavk8p8mwxziw3ljn1c7n66")))

