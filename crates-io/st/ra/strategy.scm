(define-module (crates-io st ra strategy) #:use-module (crates-io))

(define-public crate-strategy-0.1.0 (c (n "strategy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "ethers") (r "^2.0.7") (f (quote ("ws"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)))) (h "14csnf68ild21i3f7r9aq9jam2sdfl0ai15y9mmpbw5rd7f8d9ny")))

(define-public crate-strategy-0.1.1 (c (n "strategy") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "ethers") (r "^2.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)))) (h "0xa30asa8363dsn4xq88fv4a142nrm85rl4fyz0mx0q4lhgpqj4f")))

