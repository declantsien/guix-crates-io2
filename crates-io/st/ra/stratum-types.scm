(define-module (crates-io st ra stratum-types) #:use-module (crates-io))

(define-public crate-stratum-types-0.1.0 (c (n "stratum-types") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "124qw89s722fl4w07kqxa3dyn48zgvn2a9kf51ghkq72rmdi64kh")))

(define-public crate-stratum-types-0.1.1 (c (n "stratum-types") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hvh7ilhcdyc449dixm6x1q8pbrmrl7w1qzyp4a4l7civlblm01a")))

(define-public crate-stratum-types-0.1.2 (c (n "stratum-types") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02vqn05iljnss4aph15j0fbaj83npjyil5154996ckqrnk5pqj0c")))

(define-public crate-stratum-types-0.1.3 (c (n "stratum-types") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a0mhlqmg0sxmvmlcb9qg33ls17z40p7zn7s584dqy54j4vk1v62")))

(define-public crate-stratum-types-0.1.4 (c (n "stratum-types") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1982asj6bw2z9f0lxkp708dg7g62lx2jhxjn1sxrvkx5g92fgws6")))

(define-public crate-stratum-types-0.1.5 (c (n "stratum-types") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n52prgbb40h8zm6465nyk7wjz3g7v8g4dlj0qw1nvi74irrbk48")))

(define-public crate-stratum-types-0.1.6 (c (n "stratum-types") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fdn6dslk23lswsvcf9apfvi2zlih8p1sr5jwhn0gszfag83vv0n")))

(define-public crate-stratum-types-0.1.7 (c (n "stratum-types") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08mkdjbxmqcypxdx55ij2kkfs1jrrjryyd91fn6yd33sbj3nzqj6")))

(define-public crate-stratum-types-0.1.8 (c (n "stratum-types") (v "0.1.8") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nsm2zq6s38wlz2cmwlb9cvb1fqjv1c8ia1p4vs7rrscq1620xpa")))

(define-public crate-stratum-types-0.1.9 (c (n "stratum-types") (v "0.1.9") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lj7jlmjldag5m2xsalyff03nf97g35n5wy9v0r80ap6s7cr2fdq")))

(define-public crate-stratum-types-0.2.0 (c (n "stratum-types") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09ahxihfmnsza14shrzzdwqv1sc9c303ip8lc1mzx51v7rfnqvzk")))

(define-public crate-stratum-types-0.3.0 (c (n "stratum-types") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ydab2r30qz3p1yyyvdvgf25v9lccnzc9dymq9zb58r0vn4cz2iy")))

(define-public crate-stratum-types-0.3.1 (c (n "stratum-types") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qrxdymbzrlnm35qchkpl6f9rmqpb9q2sf9qgjcy230ahfhyvs20")))

(define-public crate-stratum-types-0.3.2 (c (n "stratum-types") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "15xybnid8icpnjj88hszchcl1fx3f1b00j0bq8z462fjxzzahv8l")))

(define-public crate-stratum-types-0.4.0 (c (n "stratum-types") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "1wzig4jycf7bcxay7rsj9fdgzwvg1lmmrsv9mriiav6blcqaf327")))

(define-public crate-stratum-types-0.4.1 (c (n "stratum-types") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "0bp6kp81hd3fmp5abjqpa16j0rz00qg5556iv0bvmnkllvfs2axb")))

(define-public crate-stratum-types-0.5.0 (c (n "stratum-types") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "0lbsh57gix6ww839if5szsd75y5zgc6myk3z5vkynjgimzd2732x")))

(define-public crate-stratum-types-0.6.0 (c (n "stratum-types") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "1cgx9w3zdpb6r407p7kzkc1asindh3pfhijvdxy6kkxr4ndf7haw")))

(define-public crate-stratum-types-0.6.1 (c (n "stratum-types") (v "0.6.1") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "1ix7h0ilsb28gg1jfs4w4cfm45m7qqv86xczw5ns84wyfga5m70p")))

(define-public crate-stratum-types-0.6.2 (c (n "stratum-types") (v "0.6.2") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "1qi6xj339mm0p20n98yvggswppkk841i0zpl1d86mxic9g61fwlm")))

(define-public crate-stratum-types-0.6.3 (c (n "stratum-types") (v "0.6.3") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "0clygqswzan2yzwq547vs4k4z4fy3v82pfhrcl8nw328yxj3i130")))

(define-public crate-stratum-types-0.7.0 (c (n "stratum-types") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "18i045wqha1bs6a45b7kg8nwl4av5644vpzj5fjvg866j23rxb1p")))

(define-public crate-stratum-types-0.8.0 (c (n "stratum-types") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "0028y16yjkifr02qkzr6qkb9nz9vicg437sn34dwmd8nkxp41hma")))

(define-public crate-stratum-types-0.9.0 (c (n "stratum-types") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "11njr4yijklrnxf4w26a51li9qq0hl7fx2xqw4vqg9wbwjp2q4ji")))

(define-public crate-stratum-types-0.9.1 (c (n "stratum-types") (v "0.9.1") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "08k176jczcrkzw8d0l2ah050i598zh0sby7n4dzss2rqpl0v2cbn")))

(define-public crate-stratum-types-0.9.2 (c (n "stratum-types") (v "0.9.2") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "12cmhz51aqzbgqsva7z7imfb2sy1h2847pk4l63a1m6arhb203yf")))

(define-public crate-stratum-types-0.9.3 (c (n "stratum-types") (v "0.9.3") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "0kv025am2fvjrdjcy790a40zm4qlgq9mzg8s9ji3pilbm4qz747w")))

(define-public crate-stratum-types-0.9.4 (c (n "stratum-types") (v "0.9.4") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "009542z03n7igvvyzvkgdgm57kmkcs2lqmb9qaiv6ijazjjjz6yp")))

(define-public crate-stratum-types-0.9.5 (c (n "stratum-types") (v "0.9.5") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "1drpwqi2ykg8nsng9x17wgiilin995gc9hspzg0f8zsbz1h70fgl")))

(define-public crate-stratum-types-0.9.6 (c (n "stratum-types") (v "0.9.6") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "19r2anajvgmgvhwy26a434cr6rwyqqh9zgm0rsdlw31fkfrpb764")))

(define-public crate-stratum-types-0.9.7 (c (n "stratum-types") (v "0.9.7") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "0yp2ghgmzayy89parhfnxvk6lqx7c5v9hmwwmdfiy580qpvnrmxr")))

(define-public crate-stratum-types-0.9.8 (c (n "stratum-types") (v "0.9.8") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "1636sfarqb97gz2nj175c4vvrghzgd44kpvrh76s2df7s2zqn2di")))

(define-public crate-stratum-types-0.9.9 (c (n "stratum-types") (v "0.9.9") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "0pijfzm3v61f1npgdvqxj1cwqfx4g77vgzvqilvrnnc86r6as8jh")))

(define-public crate-stratum-types-0.9.10 (c (n "stratum-types") (v "0.9.10") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "1zf5hcdp5ya4br07v1440yldmbx05sxnpdmadvz63vzrpx1m6zap")))

(define-public crate-stratum-types-0.9.11 (c (n "stratum-types") (v "0.9.11") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "1vkx5s2nj5dxs7m4librfcmrj6dzj4nmnr8c3fc7n7mw18nxqq23")))

(define-public crate-stratum-types-0.9.12 (c (n "stratum-types") (v "0.9.12") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "1n4pa2h2mfkxi7977mcf49g0yzrzknx3fxan9yhz1aqbx3yaqpcx")))

(define-public crate-stratum-types-0.9.13 (c (n "stratum-types") (v "0.9.13") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "0x6hliiy3hf97nlwzwcdlkjha5i897gv976d0b3kvsp6yqgj3yyh")))

(define-public crate-stratum-types-0.9.14 (c (n "stratum-types") (v "0.9.14") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "0sciiqvzjs4cdlcysmlz4hpfrm77flcyzs7rl6f5jzacfgaif48v")))

(define-public crate-stratum-types-0.9.15 (c (n "stratum-types") (v "0.9.15") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "0agsd9axdmdw47p1yl2rskqszlmyfdi3rk482rgqf9sk23xj1mr1")))

(define-public crate-stratum-types-0.9.16 (c (n "stratum-types") (v "0.9.16") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)))) (h "1yb40scgfhazfygq74i2k7k1wxm06865r9753nmw4ax834yldlcz")))

(define-public crate-stratum-types-0.9.17 (c (n "stratum-types") (v "0.9.17") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "074zpkc4139yh9v39g82m5dv0xi791sl6cnpch01z2qjlf80yszx")))

(define-public crate-stratum-types-0.9.18 (c (n "stratum-types") (v "0.9.18") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1cylggrjdcivrfdp4fgz3ivl7hmg9mwl36prb9zmwsva3rvb7d3m")))

(define-public crate-stratum-types-0.9.19 (c (n "stratum-types") (v "0.9.19") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "02a2hmjjcis3mwswfh57bdxxqmk7nf10g6byd6968rpl7drj5sd3")))

(define-public crate-stratum-types-0.9.20 (c (n "stratum-types") (v "0.9.20") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0a83ig9r9y9mns35m60n6j5105m9rcf96fvw4p1ygk32xsi5flqs")))

(define-public crate-stratum-types-0.9.21 (c (n "stratum-types") (v "0.9.21") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0sbg4q9vkg2gkkqgs3v338nkaqhfcqvicydc89zg3a6r3z4lfmak")))

(define-public crate-stratum-types-0.9.22 (c (n "stratum-types") (v "0.9.22") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0d3h83xkywificvp37kqxfhrz188333xp1jwcx13sf8rrlq7zyj7")))

(define-public crate-stratum-types-0.9.23 (c (n "stratum-types") (v "0.9.23") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1h65dbriq6xgpn7l36fmw6nrs41qwawimrg5s0sx1i20gf4w5dwn")))

(define-public crate-stratum-types-0.9.24 (c (n "stratum-types") (v "0.9.24") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0jg549pjvanfp267wwr11xfhy66jk97jf6z7wcy8jvxjvc1ihmlf")))

(define-public crate-stratum-types-0.9.25 (c (n "stratum-types") (v "0.9.25") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0ikpk7865hsl63pv702va44i1cpjfnkn4zrjc2r3ns211fmyaazz")))

