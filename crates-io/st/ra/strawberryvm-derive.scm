(define-module (crates-io st ra strawberryvm-derive) #:use-module (crates-io))

(define-public crate-strawberryvm-derive-0.1.0 (c (n "strawberryvm-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "086xwj14s9gl9m2mc9m3bldcrwd5ga3k85h71zas70a4mqjh506h")))

(define-public crate-strawberryvm-derive-0.1.1 (c (n "strawberryvm-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1szicri6jmb5y46pj1nnhqh51hrnjfn9vd5vkkq0c71faw8s344i")))

(define-public crate-strawberryvm-derive-0.1.2 (c (n "strawberryvm-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0xq1c0h1bw9wvlxbihqkwhwg4406346i54jqzg7s56abzsbnxain")))

