(define-module (crates-io st ra strace-parse) #:use-module (crates-io))

(define-public crate-strace-parse-0.1.0 (c (n "strace-parse") (v "0.1.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0-beta2") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "1pw7i0871k2cnsr4s08z6a1w8ynv1p72jn2781y7pfi6hqgvz7rr")))

(define-public crate-strace-parse-0.2.0 (c (n "strace-parse") (v "0.2.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0-beta2") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "050nxkm0psn42r3rd0qlzgiyi4jiznd2r0iwymx2z9c32w98rqqk")))

(define-public crate-strace-parse-0.3.0 (c (n "strace-parse") (v "0.3.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0-beta2") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "0gyz4b9iazx3hdmf7s94rnlhfd6c2dagg8xi2wgpmy6g5w2wrlhi")))

(define-public crate-strace-parse-0.4.0 (c (n "strace-parse") (v "0.4.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0-beta2") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "1nlyxxqgkdpw2bfapnnq9sgzm5d7zs1i96sx6vg19qqfyxnzx863")))

