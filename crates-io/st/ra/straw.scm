(define-module (crates-io st ra straw) #:use-module (crates-io))

(define-public crate-straw-0.1.0 (c (n "straw") (v "0.1.0") (h "0z5kyk3c3mrg7jg7rjca2nsvsrbs1fgjykl9mqqsfd5lj47hvg3y")))

(define-public crate-straw-0.2.0 (c (n "straw") (v "0.2.0") (h "1vzbkw2q1j3y2ibimiy36ylar4qfyb0kin1cpq4cmgpik2i89jpn")))

(define-public crate-straw-0.3.0 (c (n "straw") (v "0.3.0") (h "14n3ypnzkx9zmxpd3jsnig2mhmb79gmmgflxq5h14ab575hij449")))

