(define-module (crates-io st ra strafe) #:use-module (crates-io))

(define-public crate-strafe-0.1.0 (c (n "strafe") (v "0.1.0") (d (list (d (n "strafe-datasets") (r "~0.1") (d #t) (k 0)) (d (n "strafe-distribution") (r "~0.1") (d #t) (k 0)) (d (n "strafe-numerics") (r "~0.1") (d #t) (k 0)) (d (n "strafe-plot") (r "~0.1") (d #t) (k 0)) (d (n "strafe-tests") (r "~0.1") (d #t) (k 0)) (d (n "strafe-trait") (r "~0.1") (d #t) (k 0)) (d (n "strafe-type") (r "~0.1") (d #t) (k 0)))) (h "11vmp8aa8bbzrvdjmad8xchblh92w9mw8grmp264cv3ync8cqn78") (y #t)))

(define-public crate-strafe-0.1.1 (c (n "strafe") (v "0.1.1") (d (list (d (n "strafe-datasets") (r "~0.1") (d #t) (k 0)) (d (n "strafe-distribution") (r "~0.1") (d #t) (k 0)) (d (n "strafe-numerics") (r "~0.1") (d #t) (k 0)) (d (n "strafe-plot") (r "~0.1") (d #t) (k 0)) (d (n "strafe-tests") (r "~0.1") (d #t) (k 0)) (d (n "strafe-trait") (r "~0.1") (d #t) (k 0)) (d (n "strafe-type") (r "~0.1") (d #t) (k 0)))) (h "009f8qlrc7kxmhgwm6lnzwzqhlcm8hywx54xp4wqzmp26vbch3kw")))

