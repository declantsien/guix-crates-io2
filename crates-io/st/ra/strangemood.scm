(define-module (crates-io st ra strangemood) #:use-module (crates-io))

(define-public crate-strangemood-0.1.0 (c (n "strangemood") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.18.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.18.2") (d #t) (k 0)) (d (n "spl-governance") (r "^2.1.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1x6sny5an2b0v25dxw3y3ks3r4zargq80bxl6yig4pcfqcqbn1yi") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-strangemood-0.1.1 (c (n "strangemood") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.18.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.18.2") (d #t) (k 0)) (d (n "spl-governance") (r "^2.1.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1i105x4fzr81gbp6zhzdw2kfcc7nm3inna1f6i918553jd99rp1q") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

