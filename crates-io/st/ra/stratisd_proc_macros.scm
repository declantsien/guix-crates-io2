(define-module (crates-io st ra stratisd_proc_macros) #:use-module (crates-io))

(define-public crate-stratisd_proc_macros-0.1.0 (c (n "stratisd_proc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zm6dfs58c4l3gspbhbyckn74ws5fgpv4v90l6b503h4zsv1mij6")))

(define-public crate-stratisd_proc_macros-0.2.0 (c (n "stratisd_proc_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xagm8npmnq753wjr731d6ba4xk9xdvxvxxvnw8xyzlq56axc96y")))

(define-public crate-stratisd_proc_macros-0.2.1 (c (n "stratisd_proc_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dcjpcl06lbrpdr8pafiq686dkd59mpg9i78dimmfj6645ly2pn8") (r "1.71.0")))

