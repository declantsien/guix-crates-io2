(define-module (crates-io st ra stratos) #:use-module (crates-io))

(define-public crate-stratos-0.1.0 (c (n "stratos") (v "0.1.0") (d (list (d (n "kern") (r "^1") (d #t) (k 0)) (d (n "plotlib") (r "^0.4.0") (d #t) (k 0)))) (h "1wnawnl049avlxxim0n0rzayh6x9d83yy5kwayxas0rnisfc45x8")))

(define-public crate-stratos-0.1.1 (c (n "stratos") (v "0.1.1") (d (list (d (n "kern") (r "^1") (d #t) (k 0)) (d (n "plotlib") (r "^0.4.0") (d #t) (k 0)))) (h "1hshsaafmczv3nxg7wq7jc9knvvys02xd5rjlv3n7b7myda2b20m")))

(define-public crate-stratos-0.1.2 (c (n "stratos") (v "0.1.2") (d (list (d (n "kern") (r "^1") (d #t) (k 0)) (d (n "plotlib") (r "^0.4.0") (d #t) (k 0)))) (h "12d4wclddv305yx3fg86aaydhp9bs0fip9qn6g2zcrgql4jnxb2i")))

(define-public crate-stratos-0.1.3 (c (n "stratos") (v "0.1.3") (d (list (d (n "kern") (r "^1") (d #t) (k 0)) (d (n "plotlib") (r "^0.4.0") (d #t) (k 0)))) (h "0vpmph4d41cc40ngwfd1wa1fd2dr9w6pyj3b2ymwlwkhsmdclcf5")))

(define-public crate-stratos-0.1.4 (c (n "stratos") (v "0.1.4") (d (list (d (n "kern") (r "^1") (d #t) (k 0)) (d (n "plotlib") (r "^0.4.0") (d #t) (k 0)))) (h "0n22az4xwmrwv99a8p0z7kai8ii7rh4m6kcx1vjsnwpm5692j22i")))

(define-public crate-stratos-0.1.5 (c (n "stratos") (v "0.1.5") (d (list (d (n "kern") (r "^1") (d #t) (k 0)) (d (n "plotlib") (r "^0.4.0") (d #t) (k 0)))) (h "0bddpbgg0lvxs9a8r24qnhxb5zji61h34cvkinma9vvhkw2p0mlb")))

(define-public crate-stratos-0.1.8 (c (n "stratos") (v "0.1.8") (d (list (d (n "kern") (r "^1.1.0") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.0") (d #t) (k 0)))) (h "1jdcscpahsjcb8ihxaw6m94d9lnaiq981gxipfli14b3j63rnv97")))

(define-public crate-stratos-0.1.9 (c (n "stratos") (v "0.1.9") (d (list (d (n "kern") (r "^1.1.0") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.0") (d #t) (k 0)))) (h "0x1glpq5xrf7gcm3n1xar9dnaqqbywg7hx8d3z39v3vixmacxzyg")))

(define-public crate-stratos-0.1.10 (c (n "stratos") (v "0.1.10") (d (list (d (n "kern") (r "^1.1.4") (d #t) (k 0)) (d (n "lhi") (r "= 0.0.0-dev.11") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "02d0w20yz745cx4wcj6xmd55s5dg7yc0dw2fkdvmmrp4xn74a5sq")))

(define-public crate-stratos-0.1.11 (c (n "stratos") (v "0.1.11") (d (list (d (n "kern") (r "^1.1.6") (d #t) (k 0)) (d (n "lhi") (r "= 0.0.0-dev.13") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "0lkcd8s6rwbaa8jnl4c5v57id4mjj44nkw06p86hyiyb4f0202r3")))

