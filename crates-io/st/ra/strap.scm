(define-module (crates-io st ra strap) #:use-module (crates-io))

(define-public crate-strap-0.1.0 (c (n "strap") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m3kivbq3d8ik27q88mm5mqmrc4v41wxg8nrm296l94dplin8i3g")))

