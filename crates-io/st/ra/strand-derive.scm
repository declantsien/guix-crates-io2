(define-module (crates-io st ra strand-derive) #:use-module (crates-io))

(define-public crate-strand-derive-0.1.0 (c (n "strand-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qlz9zan23gq7lgxyls4dv47kia2v38aa6hs2py1s4f99wi2vdbf")))

(define-public crate-strand-derive-0.2.0 (c (n "strand-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gijmz76xr0f2rh3ycsclv9bsjb07nhp075ix1182wq2nllnxqr4")))

(define-public crate-strand-derive-0.3.0 (c (n "strand-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "039c08id7n8bfqbskn7nv38db8x5a8nfwx8p7my3hgvknwncs9dx")))

(define-public crate-strand-derive-0.4.0 (c (n "strand-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "0d5s4mh9yc7xd8qr9q2c2fdf9z62pdcfg166w4aixx4kpnrd494b")))

(define-public crate-strand-derive-0.5.0 (c (n "strand-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "117g7n7xg7963k645cfraq1b2sbmhicpnywy3ifh4k9slyk07z54")))

