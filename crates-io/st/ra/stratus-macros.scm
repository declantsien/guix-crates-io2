(define-module (crates-io st ra stratus-macros) #:use-module (crates-io))

(define-public crate-stratus-macros-0.1.0 (c (n "stratus-macros") (v "0.1.0") (d (list (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0xrnnb545mmzmcj5ajf7qf19m2m4mmk4jlcfb7g4iqhlc1hakbvp")))

(define-public crate-stratus-macros-0.1.1 (c (n "stratus-macros") (v "0.1.1") (d (list (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "10bwd8a78xbaqblpnsjb25jsxrqq5pbd93bir5qi48d25s5ra07p")))

