(define-module (crates-io st ra strafe-testing) #:use-module (crates-io))

(define-public crate-strafe-testing-0.1.0 (c (n "strafe-testing") (v "0.1.0") (d (list (d (n "approx") (r "~0.5") (d #t) (k 0)) (d (n "nalgebra") (r "~0.32") (d #t) (k 0)) (d (n "num") (r "~0.4") (d #t) (k 0)) (d (n "proptest") (r "~1") (o #t) (d #t) (k 0)) (d (n "r2rs-nmath") (r "~0.1") (d #t) (k 0)) (d (n "strafe-type") (r "~0.1") (d #t) (k 0)))) (h "000jhi50mdpj10pg54x4hwdan0h5s7iarkshyv8l1dq8gw3w5g7f") (y #t)))

(define-public crate-strafe-testing-0.1.1 (c (n "strafe-testing") (v "0.1.1") (d (list (d (n "approx") (r "~0.5") (d #t) (k 0)) (d (n "nalgebra") (r "~0.32") (d #t) (k 0)) (d (n "num") (r "~0.4") (d #t) (k 0)) (d (n "proptest") (r "~1") (o #t) (d #t) (k 0)) (d (n "r2rs-nmath") (r "~0.1") (d #t) (k 0)) (d (n "strafe-type") (r "~0.1") (d #t) (k 0)))) (h "1q7pqfmmr4ypl0f56dgj8fxn4496qzphch824ldiza7hi83z5nrf")))

