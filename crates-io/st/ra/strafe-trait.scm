(define-module (crates-io st ra strafe-trait) #:use-module (crates-io))

(define-public crate-strafe-trait-0.1.0 (c (n "strafe-trait") (v "0.1.0") (d (list (d (n "nalgebra") (r "~0.32") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "strafe-consts") (r "~0.1") (d #t) (k 0)) (d (n "strafe-type") (r "~0.1") (d #t) (k 0)))) (h "0qdqgl71a7dx4v1b81xd0a4mymyxph27nsr51hdsj344gwgwahsf") (y #t)))

(define-public crate-strafe-trait-0.1.1 (c (n "strafe-trait") (v "0.1.1") (d (list (d (n "nalgebra") (r "~0.32") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "strafe-consts") (r "~0.1") (d #t) (k 0)) (d (n "strafe-type") (r "~0.1") (d #t) (k 0)))) (h "0q0aarqd7b0ifj0y5rdvdzqg3vy16f4pcs5wb3fs9p8kb6x9j2ai")))

