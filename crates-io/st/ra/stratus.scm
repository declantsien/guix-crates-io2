(define-module (crates-io st ra stratus) #:use-module (crates-io))

(define-public crate-stratus-0.1.0 (c (n "stratus") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "stratus-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0m8lp8v0c2zhpwzr33kamda0nbh7gpmv8pwd6dx2qa6ywvvmrr86")))

(define-public crate-stratus-0.1.1 (c (n "stratus") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "stratus-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0dv3s7p9i6f22ysgz449bjddriyxfgsng1b7qlqmyxpmzja8vn1s")))

