(define-module (crates-io st ra strava) #:use-module (crates-io))

(define-public crate-strava-0.0.1 (c (n "strava") (v "0.0.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1jpjpn9gr8bx1hp749vph6mval4fm22nq8im5as65hv30spz67r4")))

(define-public crate-strava-0.0.2 (c (n "strava") (v "0.0.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0vm88lnscbmx9x4f4bs4z6igrqgp7b6hicdcg6rk7p09rm216haa")))

(define-public crate-strava-0.1.0 (c (n "strava") (v "0.1.0") (d (list (d (n "hyper") (r "~0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "18vvjkvvm297bq04amhm0fzkbalvjlka60db8607vj4g2gjydzkq") (f (quote (("api_test"))))))

(define-public crate-strava-0.1.1 (c (n "strava") (v "0.1.1") (d (list (d (n "hyper") (r "~0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "191l2f9p4zjm86q76bwjr2kab8174s9iqc59zbqhyi4h439dzga8") (f (quote (("api_test"))))))

(define-public crate-strava-0.2.0 (c (n "strava") (v "0.2.0") (d (list (d (n "hyper") (r "~0.10.8") (d #t) (k 0)) (d (n "hyper-native-tls") (r "~0.2.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "0vla35dizmf5ywhb8lrzdl92la7c28gbk95j2iha6majzd05lj5i") (f (quote (("api_test"))))))

