(define-module (crates-io st ra strafe-consts) #:use-module (crates-io))

(define-public crate-strafe-consts-0.1.0 (c (n "strafe-consts") (v "0.1.0") (h "1wmxly7l8gliqh4h6sb5fx4armc3y7fqdjklnrzrrxl3rps5n4mp") (y #t)))

(define-public crate-strafe-consts-0.1.1 (c (n "strafe-consts") (v "0.1.1") (h "06lxlmzy8yzvzfyjs43gbmjrpi8mr46xgy734408xmprzg53hw94")))

