(define-module (crates-io st ra strawberrymilk) #:use-module (crates-io))

(define-public crate-strawberrymilk-1.0.0 (c (n "strawberrymilk") (v "1.0.0") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "07dr52aba6hhxhaysrv9vkr7klr2x4nhi880ham95xxf7js1av3n")))

(define-public crate-strawberrymilk-1.1.0 (c (n "strawberrymilk") (v "1.1.0") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "005vwnww1ylvz3gfbx2pbkviwgdwiwp2yrhl9n97ib8nm0d4nv1s")))

(define-public crate-strawberrymilk-1.2.0 (c (n "strawberrymilk") (v "1.2.0") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "kstring") (r "^1.0.6") (d #t) (k 0)) (d (n "liquid") (r "^0.24.0") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0j1h1gafgyv22cmbgvkwcaddxncv95d0yyj7cy5sbv8hffhfpbk8")))

