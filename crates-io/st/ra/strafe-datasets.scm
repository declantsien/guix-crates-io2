(define-module (crates-io st ra strafe-datasets) #:use-module (crates-io))

(define-public crate-strafe-datasets-0.1.0 (c (n "strafe-datasets") (v "0.1.0") (d (list (d (n "polars") (r "~0.38") (f (quote ("default" "lazy"))) (d #t) (k 0)) (d (n "r2rs-datasets") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-mass") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-rfit") (r "~0.1") (d #t) (k 0)))) (h "0sziy3k6c5gx383g4h24v0q4l0n6ya1f9fpibnfzq2i6nmd4l4rp") (y #t)))

(define-public crate-strafe-datasets-0.1.1 (c (n "strafe-datasets") (v "0.1.1") (d (list (d (n "polars") (r "~0.38") (f (quote ("default" "lazy"))) (d #t) (k 0)) (d (n "r2rs-datasets") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-mass") (r "~0.1") (d #t) (k 0)) (d (n "r2rs-rfit") (r "~0.1") (d #t) (k 0)))) (h "0xgqfhvjxa4qb1pbm1dsajq1cym6r39p75mgvsq3i4n6639v2hvg")))

