(define-module (crates-io st ra strava_client) #:use-module (crates-io))

(define-public crate-strava_client-0.1.0 (c (n "strava_client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1q1jdbi5l8wc1yq958qq5q7bdqfcbvhb2i19ylm0smkf0npqs4qz")))

