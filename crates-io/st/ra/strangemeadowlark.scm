(define-module (crates-io st ra strangemeadowlark) #:use-module (crates-io))

(define-public crate-strangemeadowlark-0.1.0 (c (n "strangemeadowlark") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bumpalo") (r "^3.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1") (d #t) (k 0)))) (h "00qaqbm8pxb7q2349xv13xw58wv5mhd7qd792g5g3a58ag0f1wid")))

(define-public crate-strangemeadowlark-0.1.2 (c (n "strangemeadowlark") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bumpalo") (r "^3.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1") (d #t) (k 0)))) (h "0mimhs8vydxw0sqkc1s2xy4vwkfjkxddaa9h8rjnbhfh5axp860v")))

(define-public crate-strangemeadowlark-0.1.3 (c (n "strangemeadowlark") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bumpalo") (r "^3.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1") (d #t) (k 0)))) (h "1vx5pvhfnysh054lghxkcws7f4nqkn3j0h3iai4flwpyszqx5vdb")))

