(define-module (crates-io st #{77}# st7735) #:use-module (crates-io))

(define-public crate-st7735-0.1.0 (c (n "st7735") (v "0.1.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5") (d #t) (k 0)))) (h "0rzx4k4ji8xrdy11aaq94g90jhcz3i2z6mq1v7734c932iv0pn1j")))

