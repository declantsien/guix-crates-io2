(define-module (crates-io st #{77}# st7735-async-low) #:use-module (crates-io))

(define-public crate-st7735-async-low-0.0.1 (c (n "st7735-async-low") (v "0.0.1") (d (list (d (n "mockall") (r "^0.9") (f (quote ("nightly"))) (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.4") (f (quote ("rt"))) (d #t) (k 2)))) (h "0fq9khbgf375mkhj1ah276x702c6xnfjsjayzhs1i86ca9qzgkxp")))

(define-public crate-st7735-async-low-0.0.2 (c (n "st7735-async-low") (v "0.0.2") (d (list (d (n "mockall") (r "^0.9") (f (quote ("nightly"))) (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.4") (f (quote ("rt"))) (d #t) (k 2)))) (h "000bav4p2rw31j7fr0qr3abr583y6y1k8362i0vzw7cxibaaydvq")))

