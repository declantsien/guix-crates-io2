(define-module (crates-io st #{77}# st7789v) #:use-module (crates-io))

(define-public crate-st7789v-0.1.0 (c (n "st7789v") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "spidev") (r "^0.5.1") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.6.1") (d #t) (k 0)) (d (n "tinybmp") (r "^0.4.0") (d #t) (k 0)))) (h "0qm473dc181v29chdxxbnw79l43ap8akzrnr7y46b96xf0zc4937") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-st7789v-0.1.1 (c (n "st7789v") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "spidev") (r "^0.5.1") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.6.1") (d #t) (k 0)))) (h "1ji2q59w67ggjxn00zbis5gi3mrxhqlcc68a4fssj6habycxarkk") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-st7789v-0.1.2 (c (n "st7789v") (v "0.1.2") (d (list (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "spidev") (r "^0.5.1") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.6.1") (d #t) (k 0)))) (h "09f0iifkh3v0x2sbjvp9fwczsmrw55xv88ab7r7byx08514abf3j") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

