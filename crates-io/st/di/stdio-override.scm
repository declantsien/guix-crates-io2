(define-module (crates-io st di stdio-override) #:use-module (crates-io))

(define-public crate-stdio-override-0.1.0 (c (n "stdio-override") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ky5ibdhfdj0r7r77wirypm1srkkii25nwini82n9gw4yvw0005r") (f (quote (("test-readme" "doc-comment"))))))

(define-public crate-stdio-override-0.1.1 (c (n "stdio-override") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "01wxhnjckwlilx3g90cppkvnw5pd5jzh73n5j9x9p22p2q2yzb7w") (f (quote (("test-readme" "doc-comment"))))))

(define-public crate-stdio-override-0.1.2 (c (n "stdio-override") (v "0.1.2") (d (list (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1jf5j4ship1l6lzhndhm13hvs6lfcnsayzra51baambsdx0gm1sz") (f (quote (("test-readme" "doc-comment"))))))

(define-public crate-stdio-override-0.1.3 (c (n "stdio-override") (v "0.1.3") (d (list (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1j1hq3f6y1n7dxry125r50gql1w501f9ak6s2yksjnp9s892bhm9") (f (quote (("test-readme" "doc-comment"))))))

