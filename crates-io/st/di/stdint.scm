(define-module (crates-io st di stdint) #:use-module (crates-io))

(define-public crate-stdint-0.1.0 (c (n "stdint") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "0v0cw4jsz7drhk09w9h57av1i2m5h50xn49cp7wdmqk2mlhqk11g")))

(define-public crate-stdint-0.2.0 (c (n "stdint") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "0z8cp17m69732rf3g947cp8rqk5bzxi9rjbyr797jljzqwrqggqd") (f (quote (("std") ("default" "std"))))))

