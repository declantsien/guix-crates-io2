(define-module (crates-io st di stdin-should-be-tty) #:use-module (crates-io))

(define-public crate-stdin-should-be-tty-0.1.0 (c (n "stdin-should-be-tty") (v "0.1.0") (d (list (d (n "nix") (r "^0.8.1") (d #t) (k 0)))) (h "1kp4qhg9bhf6sqhr6shm0ijbm6adp2qcd48l3csbf7q88585dgdy")))

