(define-module (crates-io st di stdin-readlines) #:use-module (crates-io))

(define-public crate-stdin-readlines-0.1.0 (c (n "stdin-readlines") (v "0.1.0") (h "0ahq7ia1wzcnkdp05mffbw5hp90dw83q02qqr4viprqhgi8m78ga") (y #t)))

(define-public crate-stdin-readlines-0.1.1 (c (n "stdin-readlines") (v "0.1.1") (h "0qrn0z5dgh0pqpnj10mq135nd8kxpsghzj93d04451px7y43v4iv")))

