(define-module (crates-io st di stdin_parser_derive) #:use-module (crates-io))

(define-public crate-stdin_parser_derive-0.1.0 (c (n "stdin_parser_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "stdin_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mb2zsajjv029axy9c5s9svmz2gqpqagh8dbrylz3ywp7asapgb0")))

