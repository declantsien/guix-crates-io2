(define-module (crates-io st di stdinman) #:use-module (crates-io))

(define-public crate-stdinman-0.1.0 (c (n "stdinman") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serenity") (r "^0.11") (f (quote ("voice"))) (d #t) (k 0)) (d (n "songbird") (r "^0.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "13zf906lfifgwl0ddk9yvqx6b3miyi0fqimhvrg0328zibvamcsn")))

