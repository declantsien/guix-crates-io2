(define-module (crates-io st di stdio2) #:use-module (crates-io))

(define-public crate-stdio2-0.1.0 (c (n "stdio2") (v "0.1.0") (h "0bcg755i9v6hvg9qg7l14if8p31y2897y6y5ijilnq6017617rrx")))

(define-public crate-stdio2-0.0.1 (c (n "stdio2") (v "0.0.1") (h "1w4bsdzzn3kp56761agsa56l85mq8fx06xnly5l9hqggx8radqgk")))

