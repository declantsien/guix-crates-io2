(define-module (crates-io st di stdinix) #:use-module (crates-io))

(define-public crate-stdinix-0.1.0 (c (n "stdinix") (v "0.1.0") (h "02x2ccbp3nz7nhvqdwj5nkf8ib2w78a88zlpdl4xzyzylkvib8ha")))

(define-public crate-stdinix-0.2.0 (c (n "stdinix") (v "0.2.0") (h "1f5q8dpng8gribp3vx0xaccdpz3v8lwh4xan3i90rg2s74v0vqsl")))

