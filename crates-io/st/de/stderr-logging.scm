(define-module (crates-io st de stderr-logging) #:use-module (crates-io))

(define-public crate-stderr-logging-0.0.1 (c (n "stderr-logging") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-extra") (r "^0.0.13") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "06gclww69lyzz2jh2nhchxm64wpcc8jixk9ry0bdzqpxicli1bjz")))

