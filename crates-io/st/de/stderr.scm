(define-module (crates-io st de stderr) #:use-module (crates-io))

(define-public crate-stderr-0.1.0 (c (n "stderr") (v "0.1.0") (h "0jb24irv3jv4hp1whhwnmsasgdpqp7k8sykc0rp1cqnwdpwmfyrq")))

(define-public crate-stderr-0.1.1 (c (n "stderr") (v "0.1.1") (h "15mmfyjnbrsild4npmy3x11syakrrsfjdw1hxwycwv16c6amgafs")))

(define-public crate-stderr-0.2.0 (c (n "stderr") (v "0.2.0") (h "1av64yhaf5alhj8qkygcg3xri8k4q2a719bf2dp9ixarqj15000p")))

(define-public crate-stderr-0.2.1 (c (n "stderr") (v "0.2.1") (h "1jk6dbpz5ycvfpffnd8r3k25hvffc5dhs4p9a3c9vwrh70rbgrv7")))

(define-public crate-stderr-0.2.2 (c (n "stderr") (v "0.2.2") (h "0hdcmb48d7v1a21qjsjw2qds64a5raj12mdms5rmr2qf6x4rawks")))

(define-public crate-stderr-0.3.0 (c (n "stderr") (v "0.3.0") (h "03iagh4583h62x0qx3bynid48wqbxz6hm005ikddckbsq5bzwbva")))

(define-public crate-stderr-0.5.0 (c (n "stderr") (v "0.5.0") (h "09nxywzxkmiyplndjh1nckzwlashy4whayx09dv9kanl5fmi155p")))

(define-public crate-stderr-0.6.0 (c (n "stderr") (v "0.6.0") (h "1czx7xw3qhq76iahj6kmv9114v1z4xdhcfig9rcak9ayixzfq6x5")))

(define-public crate-stderr-0.6.1 (c (n "stderr") (v "0.6.1") (h "1ss5y6xhakxr46qbhmdpcaylmjxn689knyxd1yph5nk5wlnynk7l")))

(define-public crate-stderr-0.7.0 (c (n "stderr") (v "0.7.0") (h "0y01ppik9ldw73qydkzly8gr23fm0g3rl2hnpdgsghjsffldlr6j")))

(define-public crate-stderr-0.7.1 (c (n "stderr") (v "0.7.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1z0kl2wygnv2z7n97800xgsslf0rr7jwingzqsv64b3c0c6blpvw")))

(define-public crate-stderr-0.8.0 (c (n "stderr") (v "0.8.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1y4yh54jlq37clzan53xpwr6chj5nnsnz6a40p8bwrihrd2kbrly")))

