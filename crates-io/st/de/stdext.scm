(define-module (crates-io st de stdext) #:use-module (crates-io))

(define-public crate-stdext-0.1.0 (c (n "stdext") (v "0.1.0") (h "1fjgqqsf7yndml6d35f89n4ymvr7d1v04nh1ayyw14yhkgwhshx8")))

(define-public crate-stdext-0.1.1 (c (n "stdext") (v "0.1.1") (h "1ni2j08krayn06hdn57y3alagxkfpv9hjy5d6rngzzbyg68748nc")))

(define-public crate-stdext-0.2.0 (c (n "stdext") (v "0.2.0") (h "041inqzpp4i4rvxmi3rpz2dpdbnl0jz3kdq5g0w5zjfgwa10bnyr")))

(define-public crate-stdext-0.2.1 (c (n "stdext") (v "0.2.1") (h "1v73rshfi7y6xk5svcfk1fafj4w8dz9p8zlhg423vd3w92pb8qaa")))

(define-public crate-stdext-0.3.0 (c (n "stdext") (v "0.3.0") (h "1p32vbxyvmvr31b3xnqjk41265c7zlkwc2dn82kbm1bjv092sm2f")))

(define-public crate-stdext-0.3.1 (c (n "stdext") (v "0.3.1") (h "01z2qd9571hysw38yxavp9blya9ssmklh4zgjyw2yhc2mqr6nfvg")))

(define-public crate-stdext-0.3.2 (c (n "stdext") (v "0.3.2") (h "0nzknicr8r81r7ww2kx09d6zafg3yy9m03wb381ffk379ppzc4k0")))

(define-public crate-stdext-0.3.3 (c (n "stdext") (v "0.3.3") (h "1ha9f3nfnikp2x5h5c5n3l7140zjcgp0pm2mnayjvb0qgkmqxwja")))

