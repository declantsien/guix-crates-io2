(define-module (crates-io st dm stdmacros) #:use-module (crates-io))

(define-public crate-stdmacros-0.1.0 (c (n "stdmacros") (v "0.1.0") (h "0cgv4jwzvg1czsfhpg8pkcvfcr21pa12s12ilmbgz4759p0x74hh")))

(define-public crate-stdmacros-0.1.1 (c (n "stdmacros") (v "0.1.1") (h "1l5j02ajkfcw0zb6gkz9ikcn0is83lav39qqr84dhl75vnj4yc77")))

