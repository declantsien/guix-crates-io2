(define-module (crates-io st r- str-macro) #:use-module (crates-io))

(define-public crate-str-macro-0.1.0 (c (n "str-macro") (v "0.1.0") (h "1p207walfsvzhhxlz5sgr2zx33cya3k7h890w6apismnfjj8rcjp")))

(define-public crate-str-macro-0.1.1 (c (n "str-macro") (v "0.1.1") (h "14z0qp334rr2201qk6xg5bvq616lib0sl16gqcwskqjj6wisn4wn")))

(define-public crate-str-macro-0.1.2 (c (n "str-macro") (v "0.1.2") (h "1kafccn4ja35ygqws8vr5dw6iqzgdnfr82wjs5l4dbclnx8aqkp3")))

(define-public crate-str-macro-0.1.3 (c (n "str-macro") (v "0.1.3") (h "060wlgcj765n8cvic8mqd01yzbnsvl6bflbdi78wmziar477rrpr")))

(define-public crate-str-macro-0.1.4 (c (n "str-macro") (v "0.1.4") (h "1p966jx6sfg1lzrl6i8m4k11r04pjrfsblxbqpkmmgps2sfk6509")))

(define-public crate-str-macro-1.0.0 (c (n "str-macro") (v "1.0.0") (h "0r28asfn2kdx3g41g0hf4d5hdkviiz3f4dnx0dxiqx3hca318xcv")))

