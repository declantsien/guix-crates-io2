(define-module (crates-io st r- str-match) #:use-module (crates-io))

(define-public crate-str-match-0.1.0 (c (n "str-match") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0xv4df7p3svahfgsmdl476qnm72a5ka8va05d0zjipakdmwxik93") (f (quote (("default") ("attribute"))))))

(define-public crate-str-match-0.1.1 (c (n "str-match") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1c2l1kkf8knqg7b6ai7p0r62nnvi2ycxcdiq4yr68x3x8g0ifpmr") (f (quote (("default") ("attribute"))))))

(define-public crate-str-match-0.1.2 (c (n "str-match") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0pllaagn9vagv5cdhmp2diiplqswjs491zpajhnl6vjmjj49fp4d") (f (quote (("default") ("attribute"))))))

