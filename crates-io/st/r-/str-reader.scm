(define-module (crates-io st r- str-reader) #:use-module (crates-io))

(define-public crate-str-reader-0.1.0 (c (n "str-reader") (v "0.1.0") (h "09agszs9x799ccjc1775w2kgil2c12wdks2ina11bcs43h8davny")))

(define-public crate-str-reader-0.1.1 (c (n "str-reader") (v "0.1.1") (h "0fwxnvjs2i8hyiy9m48hdd8ww4cfxvxj84k0ngpj7n1w1d6w26yp")))

(define-public crate-str-reader-0.1.2 (c (n "str-reader") (v "0.1.2") (h "045bpm4g04j2wdyndl2kxjxl6k7bkc46wxdqzfzy0ipckaw21am6")))

