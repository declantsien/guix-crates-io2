(define-module (crates-io st r- str-queue) #:use-module (crates-io))

(define-public crate-str-queue-0.0.1 (c (n "str-queue") (v "0.0.1") (d (list (d (n "memchr") (r "^2.4.0") (o #t) (k 0)))) (h "0nv7qydk3y5j0j1lr3vllfnww99ih2bb7wq4nfhkifvf069hxws0") (f (quote (("std_with_memchr" "std" "memchr/std") ("std") ("default" "std"))))))

