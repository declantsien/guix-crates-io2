(define-module (crates-io st r- str-block) #:use-module (crates-io))

(define-public crate-str-block-0.1.0 (c (n "str-block") (v "0.1.0") (d (list (d (n "proclet") (r "^0.1") (f (quote ("proc-macro" "literal-value"))) (k 0)))) (h "0wbh51m6za3nxl7qzl996714zw7wf35j225cp10f84fyzsrphnfw")))

(define-public crate-str-block-0.1.1 (c (n "str-block") (v "0.1.1") (d (list (d (n "proclet") (r "^0.2") (f (quote ("proc-macro" "literal-value"))) (k 0)))) (h "1mlk1g16zbjadkfw61gmyfpf3n843k7v3zbvhznkx0xc2i7l658k")))

(define-public crate-str-block-0.1.2 (c (n "str-block") (v "0.1.2") (d (list (d (n "proclet") (r "^0.3") (f (quote ("proc-macro" "literal-value"))) (k 0)))) (h "1jn4rkw9v1xvdf92jwpdrn9sfds920lf28yd3xcpns8kjb23zfy3")))

