(define-module (crates-io st r- str-intern) #:use-module (crates-io))

(define-public crate-str-intern-0.0.1 (c (n "str-intern") (v "0.0.1") (h "1inmpfnzbqfq3lxi6fjqh01a5fybx5cvn7lpyyyg3j34d8azbrn2") (f (quote (("global") ("default" "global"))))))

(define-public crate-str-intern-0.0.2 (c (n "str-intern") (v "0.0.2") (h "1xsj5h2amycbbgip4zdmji2q04j1ykk90mi0za7ak9347cdxg13i") (f (quote (("global") ("default" "global"))))))

