(define-module (crates-io st r- str-cat) #:use-module (crates-io))

(define-public crate-str-cat-0.1.0 (c (n "str-cat") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0bfajai1f748qcph8y39z7dy6gy470nylc5cm0c7l1aarjjmydwl")))

(define-public crate-str-cat-0.1.1 (c (n "str-cat") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "15nhy8fciz49mhfx7c9whl4hliy4x7lgfl8dvmii4h5723fddqll")))

(define-public crate-str-cat-0.1.2 (c (n "str-cat") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0vgh884rm2fgq89783c1pya9i0b11204ribzxpw35z7irq03wbhf")))

(define-public crate-str-cat-0.1.3 (c (n "str-cat") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1vvsmcf6lsgz41bhhb58l6zys5k5b77jydqsd5jvggmflmgv2irf")))

(define-public crate-str-cat-0.2.0 (c (n "str-cat") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1nxrvgpp82jra68921jswl71xgkw6jp8sh10gay46yj6hhl27bb9")))

