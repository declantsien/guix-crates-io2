(define-module (crates-io st r- str-buf) #:use-module (crates-io))

(define-public crate-str-buf-1.0.0 (c (n "str-buf") (v "1.0.0") (h "1ypzpzmx0lgdjipnpqm8kby7hbwrvf01zzhsnc7xq8xnps09xp49")))

(define-public crate-str-buf-1.0.1 (c (n "str-buf") (v "1.0.1") (h "16rij62p2nkim0p9rpwssg2yg1ra60vj1g0jxzrskrx6grhq2py4")))

(define-public crate-str-buf-1.0.2 (c (n "str-buf") (v "1.0.2") (h "1qqj5aqa7hflnjgpmlhiqsj4bzpxwjpkp8a2svl4l8fr1q6xgi6f")))

(define-public crate-str-buf-1.0.3 (c (n "str-buf") (v "1.0.3") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1hz0n7jvw488ba3h6dpc7xb5y3srcmyjw2lz75mjwhwmnq415kjw")))

(define-public crate-str-buf-1.0.4 (c (n "str-buf") (v "1.0.4") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0ylm42disghsjc8sz8ibj70gvamgpzhqbaxkgqak5ykipwcf8iqa")))

(define-public crate-str-buf-1.0.5 (c (n "str-buf") (v "1.0.5") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0shprf95kywspn4vbn706n8kvh6n473c5sffmdbsz77zni1kcjnl")))

(define-public crate-str-buf-2.0.0 (c (n "str-buf") (v "2.0.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "07ykbnhzlfsbaqwzb7gszv9dvpdqmlmbdwdkgiq9f34krnkfcsm6") (y #t)))

(define-public crate-str-buf-2.0.1 (c (n "str-buf") (v "2.0.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0bnb70z88bd12hrlpmmxq8mxammrzza87ncjsl7d7qhvayvbsw29") (y #t)))

(define-public crate-str-buf-2.0.2 (c (n "str-buf") (v "2.0.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0r3g699758s21024g691dy1xkf3xgxxns7szq88cn8nbj8d3y9zm") (y #t)))

(define-public crate-str-buf-2.0.3 (c (n "str-buf") (v "2.0.3") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "12sgmh5ygwy6g9prb1v6hlr626hgcychsjwpfcfk0sli17z8mgk0") (y #t)))

(define-public crate-str-buf-2.0.4 (c (n "str-buf") (v "2.0.4") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1cpg3q4px2f55cm2nln7bcqpfrqg16hxlbxdqr8wicjgmr36fgsx")))

(define-public crate-str-buf-3.0.0 (c (n "str-buf") (v "3.0.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0zndxi15vwir82nz0h314m6imv2akavcjj1bhvxafjq1myb613ag")))

(define-public crate-str-buf-2.0.5 (c (n "str-buf") (v "2.0.5") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "07mf7mzrrji0g6d44y0phdw7x49xl7sv3l3qrh6l89hi40lwnwq8")))

(define-public crate-str-buf-3.0.1 (c (n "str-buf") (v "3.0.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0jpc18a0s0spgq854x3yr1lzr1kmf1cxzshdaq2d42nw1j8f4cj6")))

(define-public crate-str-buf-1.0.6 (c (n "str-buf") (v "1.0.6") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1l7q4nha7wpsr0970bfqm773vhmpwr9l6rr8r4gwgrh46wvdh24y")))

(define-public crate-str-buf-3.0.2 (c (n "str-buf") (v "3.0.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (o #t) (k 0)))) (h "0cagpgn8q21yvr1vffkp4mgfx4z85cb6ch1ml4z3ryg2akp74nz7")))

(define-public crate-str-buf-3.0.3 (c (n "str-buf") (v "3.0.3") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (o #t) (k 0)))) (h "0d4c1lql6kgd6kvc603rnd729arwdg5m60fvshpkqwaw4avrgsqc")))

