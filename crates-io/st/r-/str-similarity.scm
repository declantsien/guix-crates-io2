(define-module (crates-io st r- str-similarity) #:use-module (crates-io))

(define-public crate-str-similarity-0.1.0 (c (n "str-similarity") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0pp6n7760anyp52rx5cd91h58f5fy1pn5majryjgfwfi999swf60") (y #t)))

(define-public crate-str-similarity-1.0.0 (c (n "str-similarity") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "04wp2kp4y7q93nw8pq5d6sq4b5x4av0lcvpzkzq1pir0x2xhzfhx") (y #t)))

(define-public crate-str-similarity-1.0.1 (c (n "str-similarity") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "03hnq18bm656m541145b959rm822jiaa0lgv7kykjd62k02r57lb") (y #t)))

(define-public crate-str-similarity-1.1.0 (c (n "str-similarity") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1mjkmwc9r2clv7nbl3v8rmlfm5dq24f1zvg60pmziaac6j45liyz") (y #t)))

(define-public crate-str-similarity-1.1.1 (c (n "str-similarity") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "03xbag706k6cpnqzidh62rc56j9jwgq421yz2yl8z6xgw2hh976d") (y #t)))

(define-public crate-str-similarity-1.1.2 (c (n "str-similarity") (v "1.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1a0xazxxljk4jcin55vir6m133gz42nv98sd1j57abm0y66wfhgr") (y #t)))

(define-public crate-str-similarity-2.0.0 (c (n "str-similarity") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "07y8zz1p66nl5i1mkbqj7d7p2fwa8da0g7d280jkrvnpi8dbyv39") (y #t)))

(define-public crate-str-similarity-3.0.0 (c (n "str-similarity") (v "3.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "10dswlzjgnpinbiwmnlfakkrq0bdlfnrrxlc7z9blwghggjgjq7p")))

(define-public crate-str-similarity-3.0.1 (c (n "str-similarity") (v "3.0.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1nk0agmy1b10n23wsi41g02ndsfgy7jdhdhlmk6hcbk1k8g9y9k1")))

(define-public crate-str-similarity-3.0.2 (c (n "str-similarity") (v "3.0.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0dzzqq0j1crnaaijza7qzblmr19d5qq4r3j61jzjcql0va012ymm")))

