(define-module (crates-io st r- str-utils) #:use-module (crates-io))

(define-public crate-str-utils-0.1.0 (c (n "str-utils") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (k 2)) (d (n "unicase") (r "^2") (d #t) (k 0)))) (h "1bzxgl70naf7cyy4pi7lhnzvwzh4g6yzk5jbq0i3k37xfw5wb673") (f (quote (("std") ("default"))))))

(define-public crate-str-utils-0.1.1 (c (n "str-utils") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (k 2)) (d (n "unicase") (r "^2") (d #t) (k 0)))) (h "11da0xlp76l2wmya2grhk7c4pd8iffiq11ij4gy43fnarj1sp4za") (f (quote (("std") ("default"))))))

(define-public crate-str-utils-0.1.2 (c (n "str-utils") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (k 2)) (d (n "unicase") (r "^2") (d #t) (k 0)))) (h "0vfmm6mjry7mhwfqg50b36aycydb67w4hgqhw9c5a7lq8r0sx0rc") (f (quote (("std") ("default"))))))

(define-public crate-str-utils-0.1.3 (c (n "str-utils") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (k 2)) (d (n "unicase") (r "^2") (d #t) (k 0)))) (h "0qzlhyd0lwlv1vrq90cwzl8lkbxwivfqs3jxq3ix97kdc4zci6pj") (f (quote (("std") ("default"))))))

(define-public crate-str-utils-0.1.4 (c (n "str-utils") (v "0.1.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (k 2)) (d (n "unicase") (r "^2") (d #t) (k 0)))) (h "0zya3prif95wbhl8m6cgxq9mw08kxnyh71ymccd871x0llnq9ay4")))

(define-public crate-str-utils-0.1.5 (c (n "str-utils") (v "0.1.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "manifest-dir-macros") (r "^0.1.6") (d #t) (k 2)) (d (n "unicase") (r "^2") (d #t) (k 0)))) (h "0az1qa5cs5y84rw4im50zd3hwssv4kh0vmaq9dya8r74k2z5ycmi")))

(define-public crate-str-utils-0.1.6 (c (n "str-utils") (v "0.1.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "manifest-dir-macros") (r "^0.1.6") (d #t) (k 2)) (d (n "unicase") (r "^2") (d #t) (k 0)))) (h "02b7jjvm7gcnhjxp2awm3pakzml5jxzwgfd2yig0gim4i4mfa1dm")))

(define-public crate-str-utils-0.1.7 (c (n "str-utils") (v "0.1.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "manifest-dir-macros") (r "^0.1.6") (d #t) (k 2)) (d (n "unicase") (r "^2") (d #t) (k 0)))) (h "15xmm1ba3h7lry9pl26mmwyi0p95gpr25q5ri58lbzd887av7g30") (r "1.60")))

