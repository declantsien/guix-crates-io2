(define-module (crates-io st r- str-concat) #:use-module (crates-io))

(define-public crate-str-concat-0.1.0 (c (n "str-concat") (v "0.1.0") (h "1hp2nrd6yxydg1n4xsc0c2hi8qmfk7gi5jjpsbnnb3n9b3bz6pgw") (y #t)))

(define-public crate-str-concat-0.1.1 (c (n "str-concat") (v "0.1.1") (h "0lchmhf6mdgya3gn7vpd35vi0f4rz2n1d9rhalwb72zwizcgdnmp") (y #t)))

(define-public crate-str-concat-0.1.2 (c (n "str-concat") (v "0.1.2") (h "1m99hvk1n9a5bm2i1wn74j9znnj4a2zanfvsxkcrq7921q9r1f17") (y #t)))

(define-public crate-str-concat-0.1.4 (c (n "str-concat") (v "0.1.4") (h "07pzmli97gj4h80kqljka792xsxlc8vdjqkw2yqhwq15y96cbzb4") (y #t)))

(define-public crate-str-concat-0.2.0 (c (n "str-concat") (v "0.2.0") (h "0d16c35iz7r4vrzldla9sn781hji17swxrgmrpily72092g96s1l")))

