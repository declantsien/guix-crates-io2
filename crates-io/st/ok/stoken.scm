(define-module (crates-io st ok stoken) #:use-module (crates-io))

(define-public crate-stoken-0.0.0 (c (n "stoken") (v "0.0.0") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)))) (h "0yw6iswhs98h462ynkh6sli1ams439a5p390x73f83mf7l3c3fsq")))

(define-public crate-stoken-0.0.1 (c (n "stoken") (v "0.0.1") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "1cbj09h72cfq3qfi7ni0iwjngsw7fbr7zczn88hqvgmnp86g9fjz")))

(define-public crate-stoken-0.0.2 (c (n "stoken") (v "0.0.2") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "0mgn1lz9wx8cdn9yfa1mnwyqczcg3rf2b9gbmrmz3gcpwnxw5rbb")))

(define-public crate-stoken-0.0.3 (c (n "stoken") (v "0.0.3") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hng0cxdavr733s6z5858vgz9arg7nlnxjqq7aycjwmxr40q57q2")))

(define-public crate-stoken-0.0.4 (c (n "stoken") (v "0.0.4") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kkvzxkqll7py6ykkjyk27ra17q3b8hvzsp3y5jaqsv8yy7ab1rc")))

(define-public crate-stoken-0.0.5 (c (n "stoken") (v "0.0.5") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19aq0lxx5cbb237az39i3ylj6c2jigjkqj187faaf1aj0ik0jcqs")))

(define-public crate-stoken-0.0.6 (c (n "stoken") (v "0.0.6") (d (list (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fdfy2xn8gph59mvk7aha55idj8i6x9r6pjzv3hgl4rqwia47lhk")))

