(define-module (crates-io st ok stokes) #:use-module (crates-io))

(define-public crate-stokes-0.1.0 (c (n "stokes") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "netcdf") (r "^0.8.3") (d #t) (k 0)))) (h "0xv5fjl76x1fv2v7fanazkfn34yhnqavkr716x79qlppyhr8653i")))

