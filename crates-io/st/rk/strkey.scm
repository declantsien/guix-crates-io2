(define-module (crates-io st rk strkey) #:use-module (crates-io))

(define-public crate-strkey-0.1.0 (c (n "strkey") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m6n674k7i7n7q7cygx9h669w6z1pxzz3xc1j2ijisaizcs19bc4")))

