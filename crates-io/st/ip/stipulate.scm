(define-module (crates-io st ip stipulate) #:use-module (crates-io))

(define-public crate-stipulate-0.0.0 (c (n "stipulate") (v "0.0.0") (h "1wcllal1icmxkxac7kbszc04d1ylfba7f567k2zs1ms37vjrxhgf")))

(define-public crate-stipulate-0.0.1 (c (n "stipulate") (v "0.0.1") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "1ppl1ky58ld1ncypv2wxnqnb36591wx5skqhc2vfyrqphzqmyl6x")))

(define-public crate-stipulate-0.0.2 (c (n "stipulate") (v "0.0.2") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "0dgs1fs1xw4y8j61msx1r1p4v116qgyps4pn53cc7qwxzrfgscvs")))

