(define-module (crates-io st ea steamy-controller) #:use-module (crates-io))

(define-public crate-steamy-controller-0.1.0 (c (n "steamy-controller") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "libusb") (r "^0.2") (d #t) (k 0)))) (h "16ckxzdwgvvbbqwiz5l8salb2pb8yj8kdnqcgs4q455k4gfj32mi")))

(define-public crate-steamy-controller-0.1.1 (c (n "steamy-controller") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "hid") (r "^0.1") (f (quote ("build"))) (d #t) (t "cfg(not(target_os = \"linux\"))") (k 0)) (d (n "libusb") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0d9zl2rha9xd3pb6dbjhp7n4ppmjsf7icnca0jn7sadpw4py0jsk")))

(define-public crate-steamy-controller-0.2.0 (c (n "steamy-controller") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "hid") (r "^0.2") (f (quote ("build"))) (d #t) (t "cfg(not(target_os = \"linux\"))") (k 0)) (d (n "libusb") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "09hpj18nr12li5b2qvpp5a2fzksjpqighdr7f6vgk62mpnv6vjkk")))

(define-public crate-steamy-controller-0.2.1 (c (n "steamy-controller") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "hid") (r "^0.2") (f (quote ("build"))) (d #t) (t "cfg(not(target_os = \"linux\"))") (k 0)) (d (n "libusb") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "17pfx2hnbfjz74ih8dfnmj9y5xi7gn6pb8yybkjr9bkhpn33idvp")))

