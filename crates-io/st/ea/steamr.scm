(define-module (crates-io st ea steamr) #:use-module (crates-io))

(define-public crate-steamr-0.1.0 (c (n "steamr") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fybh10rjlm07b1j5qdxhb2b1l5hzc2scqyg4v8abyvxdc89rc2a")))

(define-public crate-steamr-0.2.0 (c (n "steamr") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d33y9k46ryh2z3s3ihda8kgxd91z09i63r6q82qidf8jna1xybp")))

(define-public crate-steamr-0.3.0 (c (n "steamr") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "15r837q873b8havzzvspx2pk4djpprmmbrdz6awqmmxmcq1qrc5m")))

(define-public crate-steamr-0.3.1 (c (n "steamr") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.12.1") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1z4sg4yb1p9rxz3jvi27lc18fsynf8yia2h9q9hxrbfq9sdz0hdr")))

(define-public crate-steamr-0.4.0 (c (n "steamr") (v "0.4.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.1") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0x6j7lmasy7xyglp5van3r40a892xb9cbxr3z0c8yjf1197aqhbx")))

