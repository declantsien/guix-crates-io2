(define-module (crates-io st ea steam-api) #:use-module (crates-io))

(define-public crate-steam-api-0.3.0 (c (n "steam-api") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)))) (h "0m128s32f6xyg1ycykcn4k5bpr2kwii0psn3bnc72y97l5z8a88f")))

(define-public crate-steam-api-0.4.0 (c (n "steam-api") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)))) (h "02qvkbcwlldv448zmyygl7w9a8rs4hy3l42vm1zg9r6bqfgk5hjq")))

(define-public crate-steam-api-0.4.1 (c (n "steam-api") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)))) (h "0ngw654grp8safslrq9b1cwdx6dj7nm81ph6k2xkva0fgzib354z")))

