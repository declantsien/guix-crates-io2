(define-module (crates-io st ea steam_shortcuts_util) #:use-module (crates-io))

(define-public crate-steam_shortcuts_util-1.0.0 (c (n "steam_shortcuts_util") (v "1.0.0") (d (list (d (n "ascii") (r "1.0.*") (d #t) (k 0)) (d (n "crc32fast") (r "1.2.*") (d #t) (k 0)) (d (n "nom") (r "7.0.*") (d #t) (k 0)) (d (n "nom_locate") (r "3.0.*") (d #t) (k 0)))) (h "05306lh3713qcm1x1k80lh1zcb1rw9a9n5pmnkbyypj69lpilzag")))

(define-public crate-steam_shortcuts_util-1.1.0 (c (n "steam_shortcuts_util") (v "1.1.0") (d (list (d (n "ascii") (r "1.0.*") (d #t) (k 0)) (d (n "crc32fast") (r "1.2.*") (d #t) (k 0)) (d (n "nom") (r "7.0.*") (d #t) (k 0)) (d (n "nom_locate") (r "3.0.*") (d #t) (k 0)))) (h "1a6kn9b65da7yrsanaq50nxa58azhm0ppbysjdd7hnjj93hzrhnq")))

(define-public crate-steam_shortcuts_util-1.1.1 (c (n "steam_shortcuts_util") (v "1.1.1") (d (list (d (n "ascii") (r "1.0.*") (d #t) (k 0)) (d (n "crc32fast") (r "1.2.*") (d #t) (k 0)) (d (n "nom") (r "7.0.*") (d #t) (k 0)) (d (n "nom_locate") (r "3.0.*") (d #t) (k 0)))) (h "1sv3x3nz2y4na21abrqgsm5fril0fqq64cqmj7f7qxfyhz6r38zz")))

(define-public crate-steam_shortcuts_util-1.1.2 (c (n "steam_shortcuts_util") (v "1.1.2") (d (list (d (n "ascii") (r "1.0.*") (d #t) (k 0)) (d (n "crc32fast") (r "1.2.*") (d #t) (k 0)) (d (n "nom") (r "7.0.*") (d #t) (k 0)) (d (n "nom_locate") (r "3.0.*") (d #t) (k 0)))) (h "1rwqq95bpk0yk7fii6dhzv4idkfr4jhrr6dycxka8yvfca2czvji")))

(define-public crate-steam_shortcuts_util-1.1.3 (c (n "steam_shortcuts_util") (v "1.1.3") (d (list (d (n "ascii") (r "1.0.*") (d #t) (k 0)) (d (n "crc32fast") (r "1.2.*") (d #t) (k 0)) (d (n "nom") (r "7.0.*") (d #t) (k 0)) (d (n "nom_locate") (r "3.0.*") (d #t) (k 0)))) (h "1p6hnzqysy7wpp1ll9g4x1phxp2bmcvp0gm6a22bzvw805pvh0qm")))

(define-public crate-steam_shortcuts_util-1.1.4 (c (n "steam_shortcuts_util") (v "1.1.4") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0") (d #t) (k 0)))) (h "0l6bhw32va8apy6l9sxqhlp428vgxyps6ljsw2sab6spl1629bk6")))

(define-public crate-steam_shortcuts_util-1.1.5 (c (n "steam_shortcuts_util") (v "1.1.5") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "nom") (r "^6.2.1") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0") (d #t) (k 0)))) (h "0fw7ivg3s08kjqybq00j0zb4amch0q94zrccs7dv004an29a8fqm")))

(define-public crate-steam_shortcuts_util-1.1.7 (c (n "steam_shortcuts_util") (v "1.1.7") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)))) (h "1y0agkcn043fmvxz8x2nm46j65zdl0d6sm0ki33bn81i32fqn937")))

(define-public crate-steam_shortcuts_util-1.1.8 (c (n "steam_shortcuts_util") (v "1.1.8") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)))) (h "1dl9d8z8i8sz7x5rg2i4fjxp79f51kvm7g7fmjbb34rsnaykwm6h")))

