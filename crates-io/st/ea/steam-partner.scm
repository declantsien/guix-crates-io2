(define-module (crates-io st ea steam-partner) #:use-module (crates-io))

(define-public crate-steam-partner-0.0.0 (c (n "steam-partner") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "09xvi32840a3fi0bv91h55zwn6lh75dbzwp3hwbmzlgyr9ldysmy") (f (quote (("default"))))))

