(define-module (crates-io st ea steamaudio) #:use-module (crates-io))

(define-public crate-steamaudio-0.0.0 (c (n "steamaudio") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "glam") (r "^0.22") (d #t) (k 0)) (d (n "rodio") (r "^0.16") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0y1dcccnqy0703rj11zqd1ajan2x003x8vi4ixww7ib8njivch0f")))

(define-public crate-steamaudio-0.1.0 (c (n "steamaudio") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "rodio") (r "^0.17") (o #t) (k 0)) (d (n "rodio") (r "^0.17") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1c81j0rkd4yl2xxg6avxk3rkdj123gzpq9xrhfq9sdlrxzx7pacz") (s 2) (e (quote (("rodio" "dep:rodio"))))))

