(define-module (crates-io st ea steamstacks-bindings) #:use-module (crates-io))

(define-public crate-steamstacks-bindings-0.1.0 (c (n "steamstacks-bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 1)))) (h "10y0d30317lf783hfkdx4zxlma6h6p8p00ib077wg22x9qg63d4k") (f (quote (("rebuild-bindings" "bindgen"))))))

