(define-module (crates-io st ea steamy-vdf) #:use-module (crates-io))

(define-public crate-steamy-vdf-0.1.0 (c (n "steamy-vdf") (v "0.1.0") (d (list (d (n "nom") (r "^1.2") (d #t) (k 0)))) (h "1w3953gbxjw0zmmbwbvfbskgns2gih38yd308wca79w4aiys0k88")))

(define-public crate-steamy-vdf-0.2.0 (c (n "steamy-vdf") (v "0.2.0") (d (list (d (n "nom") (r "^1.2") (d #t) (k 0)))) (h "0ydcgwn3ds6gw5q2w28q0vs28gdcxfrkdzfkqdqzwjri96njfcak")))

