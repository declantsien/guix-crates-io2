(define-module (crates-io st ea stealthereum-cli) #:use-module (crates-io))

(define-public crate-stealthereum-cli-0.1.0 (c (n "stealthereum-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eth-stealth-addresses") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1h5q9nnbwbczs8zd23lp6dqpn3pb24vkpy92wiv0sxykdh5sp0kg")))

(define-public crate-stealthereum-cli-0.1.1 (c (n "stealthereum-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eth-stealth-addresses") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1rgad7j9iyfd5apljp2gwaq2h0rnaqc2vn7k5j4rfvzf2jk65b99")))

(define-public crate-stealthereum-cli-0.2.0 (c (n "stealthereum-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eth-stealth-addresses") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1y7ypmd02sf9qlzrgfc295pravwq3b772mgrwrmq01rjxyhrgjjm")))

