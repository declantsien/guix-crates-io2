(define-module (crates-io st ea steamws) #:use-module (crates-io))

(define-public crate-steamws-0.1.0 (c (n "steamws") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "steamworks") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1cgb7lb4xm8l5mj4aw0anaqfsw8vgkrr3yk4h642wl0h07fzrlm5")))

