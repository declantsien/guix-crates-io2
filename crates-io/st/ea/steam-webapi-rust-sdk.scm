(define-module (crates-io st ea steam-webapi-rust-sdk) #:use-module (crates-io))

(define-public crate-steam-webapi-rust-sdk-0.0.1 (c (n "steam-webapi-rust-sdk") (v "0.0.1") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "00wflbmy47sxpndkvm31j6jjmphfdlchwpb325ih6dm1kymkyb7r") (y #t) (r "1.56")))

(define-public crate-steam-webapi-rust-sdk-0.0.2 (c (n "steam-webapi-rust-sdk") (v "0.0.2") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1ydlx7s7yp1hrcv4mbyiwvyj7icavf0bdl25ycd9l7blpsr5wswh") (y #t) (r "1.56")))

(define-public crate-steam-webapi-rust-sdk-0.0.3 (c (n "steam-webapi-rust-sdk") (v "0.0.3") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0s5k0qpq1wc7mya14mndfr6j2v3433m1bs4mrbi381fn2l397fg9") (y #t) (r "1.56")))

(define-public crate-steam-webapi-rust-sdk-0.0.4 (c (n "steam-webapi-rust-sdk") (v "0.0.4") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "url-build-parse") (r "^2.0.0") (d #t) (k 0)))) (h "142pv4izf4ycdvvva7vfmij5dg24vi43d0xq50mhv07ci38jxw8y") (y #t) (r "1.64")))

(define-public crate-steam-webapi-rust-sdk-0.0.5 (c (n "steam-webapi-rust-sdk") (v "0.0.5") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "url-build-parse") (r "^2.0.0") (d #t) (k 0)))) (h "0b9i9vnv6n84fdljkj4131lhjd1i9hfm4gcyshrqnkzzwmbzdxr9") (y #t) (r "1.64")))

(define-public crate-steam-webapi-rust-sdk-0.0.6 (c (n "steam-webapi-rust-sdk") (v "0.0.6") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "url-build-parse") (r "^2.0.0") (d #t) (k 0)))) (h "1hgz76l0ds9vpwspwwkkx31gp785gs6qazcly57fwa3fip3hjhl2") (y #t) (r "1.64")))

(define-public crate-steam-webapi-rust-sdk-0.0.7 (c (n "steam-webapi-rust-sdk") (v "0.0.7") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "url-build-parse") (r "^2.0.0") (d #t) (k 0)))) (h "1g9xwpaqnapzpi4lvfk4xzqpvkiaxikkkhw70q7yw2knzhk30n4n") (r "1.64")))

(define-public crate-steam-webapi-rust-sdk-0.0.8 (c (n "steam-webapi-rust-sdk") (v "0.0.8") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "url-build-parse") (r "^2.0.0") (d #t) (k 0)))) (h "1ilyfc0klnyijc8x03k21cdwf6d6mbflajw6jm4c0fbvz4ryhvk7") (r "1.65")))

