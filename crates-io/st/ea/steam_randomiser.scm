(define-module (crates-io st ea steam_randomiser) #:use-module (crates-io))

(define-public crate-steam_randomiser-0.1.0 (c (n "steam_randomiser") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 0)))) (h "08nygcq3mbngxwwn1dh7hyjjxl0vcc6m3zcg6q8an7q1sr9smkjn")))

(define-public crate-steam_randomiser-0.1.1 (c (n "steam_randomiser") (v "0.1.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 0)))) (h "0xx3nyz7wxpgnzdl07zdiz8j4gwlnphcxpswlq747dbzaw1p6iy1")))

(define-public crate-steam_randomiser-0.2.0 (c (n "steam_randomiser") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 0)))) (h "1flxpah9b9vc9kvnmn8fmhli8as2vxkiaixx3d8am8jcb04xiq8i")))

(define-public crate-steam_randomiser-0.4.0 (c (n "steam_randomiser") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 0)))) (h "0ag6gws03c0k4ir6znsqqcx4zziaqmckcb2bz9wnrpjzcgvd1mnh")))

(define-public crate-steam_randomiser-0.4.1 (c (n "steam_randomiser") (v "0.4.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0yrxqksgrz5j3m580jmyh4sbscymrhw6qc2jx0dpydsgydd084aq")))

(define-public crate-steam_randomiser-0.4.2 (c (n "steam_randomiser") (v "0.4.2") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("std" "derive"))) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1fhd78mpwhhpls6wjqcnfzx3n03p82s0ipvmnp1jb9in36hdzmcf")))

(define-public crate-steam_randomiser-0.5.0 (c (n "steam_randomiser") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "0fsxblrrbckp381ynj58c504myc9n42kwll08rv3byq6clvv795f")))

