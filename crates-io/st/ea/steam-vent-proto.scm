(define-module (crates-io st ea steam-vent-proto) #:use-module (crates-io))

(define-public crate-steam-vent-proto-0.1.0 (c (n "steam-vent-proto") (v "0.1.0") (d (list (d (n "protobuf") (r "=2.24.1") (d #t) (k 0)))) (h "1hnjzfawgkyh7f0h91cg0a48lgagnk7l3vxgapi05fha8s0vwaq8")))

(define-public crate-steam-vent-proto-0.2.0 (c (n "steam-vent-proto") (v "0.2.0") (d (list (d (n "protobuf") (r "=3.2.0") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protobuf-codegen") (r "=3.2.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "0w5gmpwxr3r9nrs9zbhnhrrw6i0fxb3mq4519l66j0452npibyi7")))

(define-public crate-steam-vent-proto-0.3.0 (c (n "steam-vent-proto") (v "0.3.0") (d (list (d (n "protobuf") (r "=3.2.0") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "1ykx4599g1p56911ci8zyfsvhx2psnpqwhdm4218fl9jhd85jpxd")))

(define-public crate-steam-vent-proto-0.4.0 (c (n "steam-vent-proto") (v "0.4.0") (d (list (d (n "protobuf") (r "=3.4.0") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "1idsy929jh19hlhgqm0iyzm5aiwx8mqivrbm9a11ca9n1023hvla")))

