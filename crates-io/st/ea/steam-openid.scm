(define-module (crates-io st ea steam-openid) #:use-module (crates-io))

(define-public crate-steam-openid-0.1.0 (c (n "steam-openid") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0xy4cj0vl4jwdkrpdl5ywwxfphv2gwlp5cknrqrn3260x4d9n0gz")))

(define-public crate-steam-openid-0.2.0 (c (n "steam-openid") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1q67x6279a00zakvd8ckrldcjp15nb4nwhqyd0kk4kaf82k7yzdq")))

