(define-module (crates-io st ea steamkit-kv) #:use-module (crates-io))

(define-public crate-steamkit-kv-0.2.0 (c (n "steamkit-kv") (v "0.2.0") (d (list (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nrygjy889hhk4710mys6191d7y9pm1m3gccg5l1qajyydylvwwl")))

(define-public crate-steamkit-kv-0.2.1 (c (n "steamkit-kv") (v "0.2.1") (d (list (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wr7by869mw6n2mag0ar3n75rz9zii51plq5ncl27sb253xwzkd0")))

(define-public crate-steamkit-kv-0.2.2 (c (n "steamkit-kv") (v "0.2.2") (d (list (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f43k1bl9chjx4na98gj489sjz2gw4w1cxhivilr5fx2ss0bh9ym") (y #t)))

(define-public crate-steamkit-kv-0.2.3 (c (n "steamkit-kv") (v "0.2.3") (d (list (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bfs20khrglkhig523hrn1xwc7lqbfm6imlm1aps48hrfmk7miv0")))

