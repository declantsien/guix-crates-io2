(define-module (crates-io st ea steamkit-vpk) #:use-module (crates-io))

(define-public crate-steamkit-vpk-0.1.0 (c (n "steamkit-vpk") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom-derive") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0slnsz478rq3xffji5gh9zkcxcxx3sqgwic8k5rrdg9r1kd5dyl4") (y #t)))

(define-public crate-steamkit-vpk-0.1.1 (c (n "steamkit-vpk") (v "0.1.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom-derive") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ki1y8ixpp7s5r9wxqhz57pv4dn1rslwhkfkwrajay6yqzlbxw8w")))

