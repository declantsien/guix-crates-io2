(define-module (crates-io st ea steam-rss) #:use-module (crates-io))

(define-public crate-steam-rss-0.2.1 (c (n "steam-rss") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "opml") (r "^1.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "075w6dqfs8mzlia13rkdpc52wi95sf24q9v7xwv8zzhy9hpw6g67")))

(define-public crate-steam-rss-0.2.2 (c (n "steam-rss") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "opml") (r "^1.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "012a390mmpysvbx9b5fjvxlxkcjbl5csbrxrk6qzfgka9qgqd83c")))

