(define-module (crates-io st ea steamkit) #:use-module (crates-io))

(define-public crate-steamkit-0.1.0 (c (n "steamkit") (v "0.1.0") (d (list (d (n "steamkit-client") (r "^0.1.0") (d #t) (k 0)) (d (n "steamkit-protos") (r "^0.1.0") (d #t) (k 0)) (d (n "steamkit-vdf") (r "^0.1.0") (d #t) (k 0)))) (h "01xqzg83n2x87xcjwn5fl00z9iz2rfhffix8axjf3i6pqvdj78y3")))

