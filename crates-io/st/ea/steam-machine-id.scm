(define-module (crates-io st ea steam-machine-id) #:use-module (crates-io))

(define-public crate-steam-machine-id-0.1.0 (c (n "steam-machine-id") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (d #t) (k 0)))) (h "0byj6kfl8xmpc7z46wql2zxpz7lc03q7pnzkg82iw7238cyfikg2")))

(define-public crate-steam-machine-id-0.1.1 (c (n "steam-machine-id") (v "0.1.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (d #t) (k 0)))) (h "01jhcbszj2l13bccchrkj8d1n6dwbv1ldjylmj4n4gzm0f1xrzmm")))

