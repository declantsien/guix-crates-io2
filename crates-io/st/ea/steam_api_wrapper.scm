(define-module (crates-io st ea steam_api_wrapper) #:use-module (crates-io))

(define-public crate-steam_api_wrapper-0.1.0 (c (n "steam_api_wrapper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lviahbmnnsnww7jljcywhg3mq1g54giibl5ani8m9hf1sj20vbx")))

(define-public crate-steam_api_wrapper-0.2.0 (c (n "steam_api_wrapper") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vywbzihq08nb080mws2bmv6fimrr7xi9i1fl4x6vlrdw876y48b")))

(define-public crate-steam_api_wrapper-0.2.1 (c (n "steam_api_wrapper") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ys8zg6jd5pmcjfrl999r5jjcyi898mlskl4x1q9fcxpcyf9j3cg")))

