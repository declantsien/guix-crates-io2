(define-module (crates-io st ea steamworks-sys) #:use-module (crates-io))

(define-public crate-steamworks-sys-0.1.0 (c (n "steamworks-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "16i4qny7v2bi92x4cy213kig79w6673rjkr2hn6hprkbdkh18n78") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.2.0 (c (n "steamworks-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "0giqwi7gwan8r4jwj8fsprk1hw32l4ql03hvxbs1lz98a6mrsv0q") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.2.1 (c (n "steamworks-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "0lgwd2pr46xvc2d9673n445ahlmg8khbcqyzkmi2131fxwmnfr9v") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.2.2 (c (n "steamworks-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "13higli36hn17dk3a4c31ps0yzh8af7ayp07rwxn1srwcfjy8djf") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.2.3 (c (n "steamworks-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "0ka2vkri142kpxps3j0y1jrxiipscic9n2xcn8j5m15xdwvsy8z2") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.3.0 (c (n "steamworks-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.14") (d #t) (k 1)))) (h "00mkppqxrg13yjp8dinvwghizx82py15wkgr8wvwmiac1yvb5178") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.3.1 (c (n "steamworks-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.14") (d #t) (k 1)))) (h "0i838r5i8dfddymn12hrg5ppczz6b9nvaw74lcvr0dd9s48qfp89") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.4.0 (c (n "steamworks-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 1)))) (h "1ikykqf5806lkqprr0g69a9gsycpwgy4mk989scjpgy0al4s7s3i") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.5.0 (c (n "steamworks-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 1)))) (h "08gv7my46m9vg7zix3xc4l9ggfazd4cws9g45xgrnn9xj0c5z4jb") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.6.0 (c (n "steamworks-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 1)))) (h "1b50rbh425qvlgqz39l4gslbccvg04hpbd2shf2l8x3ki1xk4779") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.6.1 (c (n "steamworks-sys") (v "0.6.1") (d (list (d (n "cc") (r "^1.0.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 1)))) (h "17471nzs9hyx9rxqx772r79dpr85rkzhn6jg0pa18ai1gzgnv567") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.7.0 (c (n "steamworks-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)))) (h "0jhw8aynp0y26mnvhvlpgjcidm5q0syr9nnd9bm5xn731qhdplk3") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.8.0 (c (n "steamworks-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0ra1knnzfrfzmpm6mb5ck8mnvjgg2s77w7gysmd7nf9wp7lcgwhr") (f (quote (("docs-only") ("default"))))))

(define-public crate-steamworks-sys-0.9.0 (c (n "steamworks-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)))) (h "1wz4vcl57ipfznzwj03bfc531riy4rn4bz3rc19xhnv0yd0p0j1v") (f (quote (("rebuild-bindings" "bindgen") ("default"))))))

(define-public crate-steamworks-sys-0.10.0 (c (n "steamworks-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)))) (h "1dlphykdvyrk7maachamnwhsrysq950m4gqf48jx4613slf33sfy") (f (quote (("rebuild-bindings" "bindgen") ("default"))))))

(define-public crate-steamworks-sys-0.11.0 (c (n "steamworks-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.69.2") (o #t) (d #t) (k 1)))) (h "0iaq2wwb9h2p58h2qs5wfbbsbha85a1716yb4bzsmsp8iw7v1xky") (f (quote (("rebuild-bindings" "bindgen") ("default")))) (r "1.71.1")))

