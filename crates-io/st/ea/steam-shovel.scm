(define-module (crates-io st ea steam-shovel) #:use-module (crates-io))

(define-public crate-steam-shovel-0.1.0 (c (n "steam-shovel") (v "0.1.0") (d (list (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1gs4awww20pbhqkh5wyyli5yqbh82sa9da2jgvz91yk1iq31gh9x")))

(define-public crate-steam-shovel-0.1.1 (c (n "steam-shovel") (v "0.1.1") (d (list (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "00qbnzz9l91yc3rrby7zh2qqcqig0wdn7zms204q0js0mi0jcz9y")))

(define-public crate-steam-shovel-0.1.2 (c (n "steam-shovel") (v "0.1.2") (d (list (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "06ih132nh3jpgsjddam2413dfzxm2xxb9yrxjw260nraldij8b13")))

