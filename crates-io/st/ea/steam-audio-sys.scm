(define-module (crates-io st ea steam-audio-sys) #:use-module (crates-io))

(define-public crate-steam-audio-sys-0.1.0 (c (n "steam-audio-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rmllzj6vlalr0qipl3aczdl7myiql0wad76flqr55djdbkhicf6")))

(define-public crate-steam-audio-sys-0.3.0 (c (n "steam-audio-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "glam") (r "^0.20.2") (d #t) (k 0)) (d (n "lewton") (r "^0.10.2") (d #t) (k 0)))) (h "1s3gnf4v0pd3j5x2c54mav322xgdygiwcbrxgyq9ph3702739bly")))

