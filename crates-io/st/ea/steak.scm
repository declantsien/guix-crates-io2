(define-module (crates-io st ea steak) #:use-module (crates-io))

(define-public crate-steak-1.0.0 (c (n "steak") (v "1.0.0") (d (list (d (n "cosmwasm-std") (r "^0.16") (d #t) (k 0)) (d (n "cw20") (r "^0.9") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "terra-cosmwasm") (r "^2.2") (d #t) (k 0)))) (h "1ghl9c7biv8gnsgl0zhcgrpciwm0qxfm9h667hjwxll4vaba5add")))

