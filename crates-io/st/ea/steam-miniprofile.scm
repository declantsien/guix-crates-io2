(define-module (crates-io st ea steam-miniprofile) #:use-module (crates-io))

(define-public crate-steam-miniprofile-0.1.0 (c (n "steam-miniprofile") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "07q0szx9a73fmia15lffmad7qypqrikiwx90s8jz5r2m8i9r04kq")))

(define-public crate-steam-miniprofile-0.2.0 (c (n "steam-miniprofile") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "keyvalues-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1plgc2z5g2vmin0mfaxhnpdx86zr2qxnk0gfjv2mya57wam5nn54")))

(define-public crate-steam-miniprofile-0.2.1 (c (n "steam-miniprofile") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "keyvalues-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vgh0cvf04p410xjz9zvig5php6w5q9jlsxy8a7wcp7cmdxh6lsy")))

