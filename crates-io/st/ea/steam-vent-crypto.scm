(define-module (crates-io st ea steam-vent-crypto) #:use-module (crates-io))

(define-public crate-steam-vent-crypto-0.1.0 (c (n "steam-vent-crypto") (v "0.1.0") (d (list (d (n "aes") (r "^0.7") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 1)) (d (n "block-modes") (r "^0.8") (d #t) (k 0)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rsa") (r "^0.4") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1w7bdmjsq040xj1vqqshw9cawccmn4pz639f2bijn2472dnxmdx3")))

(define-public crate-steam-vent-crypto-0.2.0 (c (n "steam-vent-crypto") (v "0.2.0") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rsa") (r "^0.9.2") (d #t) (k 0)) (d (n "rsa") (r "^0.9.2") (f (quote ("pem"))) (d #t) (k 1)) (d (n "sha-1") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1grfv2byrm0hlmayrmr9zw7wsqlk37nak5lsy4931n1xc9nhw3gb")))

