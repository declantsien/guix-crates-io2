(define-module (crates-io st ea steamid-ng) #:use-module (crates-io))

(define-public crate-steamid-ng-0.1.0 (c (n "steamid-ng") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "try_opt") (r "^0.1.1") (d #t) (k 0)))) (h "196sgzaz5458s286bpq0fn0hfyxrc1zn545gigq385f8qp05qzwj")))

(define-public crate-steamid-ng-0.1.1 (c (n "steamid-ng") (v "0.1.1") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "try_opt") (r "^0.1.1") (d #t) (k 0)))) (h "0dpm23m4c182sgh8fibxijyfriylhqyzhb8hws4k9m20qlfw9nlv")))

(define-public crate-steamid-ng-0.1.2 (c (n "steamid-ng") (v "0.1.2") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "try_opt") (r "^0.1.1") (d #t) (k 0)))) (h "10bxgzkys496j8iq8i58vnw3xmb962zdjf91g6d0h0r1qyla9ycz")))

(define-public crate-steamid-ng-0.2.2 (c (n "steamid-ng") (v "0.2.2") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 2)))) (h "1hcs1sbpzwjd5vx6c7d31ilzp7mxp8s9gvbda6yf2lrf6mzl6diw") (y #t)))

(define-public crate-steamid-ng-0.2.0 (c (n "steamid-ng") (v "0.2.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 2)))) (h "110bq0wm3q2f96kjsrlfi75wy3dfpmkf5nkwz0nh7g2r69yszkg7")))

(define-public crate-steamid-ng-0.3.0 (c (n "steamid-ng") (v "0.3.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 2)))) (h "0xcggrqv6zqxpd0935wh6my875g0bhl0j5ppzgfyv4x6x05iqmxk")))

(define-public crate-steamid-ng-0.3.1 (c (n "steamid-ng") (v "0.3.1") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 2)))) (h "12agygwa7s0464zjvff61mxvrr7jji9vjlf50amhqrkdb4k2rqm5")))

(define-public crate-steamid-ng-0.3.2 (c (n "steamid-ng") (v "0.3.2") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "04zglx75xi7gir2smxrciixxrhd0spdxywx12zj2sajahwfrhr4s") (y #t)))

(define-public crate-steamid-ng-0.3.3 (c (n "steamid-ng") (v "0.3.3") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "07v8jmycmldzaj084yjkl09lnbpsl96fvjssqwhy8mml32ywkhbz")))

(define-public crate-steamid-ng-0.3.4 (c (n "steamid-ng") (v "0.3.4") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 2)))) (h "0scvshgarhkyygjsylbylr6vk0633l3wnnr4fnajay8z6rx7aarb")))

(define-public crate-steamid-ng-1.0.0 (c (n "steamid-ng") (v "1.0.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0wrnwf9ar7v390c7gnvfn4jrg175ivwblv9nqmqabjx2zbw4kc7z")))

