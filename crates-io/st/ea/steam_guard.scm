(define-module (crates-io st ea steam_guard) #:use-module (crates-io))

(define-public crate-steam_guard-1.0.0 (c (n "steam_guard") (v "1.0.0") (d (list (d (n "base64") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)))) (h "1rrqgcvav8l5afd932i836hdhzbvax169d4n0mfrkrvkpgk1sg5c") (f (quote (("default" "base64")))) (y #t)))

(define-public crate-steam_guard-1.0.1 (c (n "steam_guard") (v "1.0.1") (d (list (d (n "base64") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)))) (h "1n5hr8g7yhfi0zqx7pzc75h9dzi4f4z3n1mpm9fpm05mz3qcdq0k") (f (quote (("default" "base64"))))))

