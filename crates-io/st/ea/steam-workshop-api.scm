(define-module (crates-io st ea steam-workshop-api) #:use-module (crates-io))

(define-public crate-steam-workshop-api-0.2.0 (c (n "steam-workshop-api") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0iyfpgp11vana7bzidy57w6bh2jfjmflbzc14h01w1rjhx2wvrvh")))

(define-public crate-steam-workshop-api-0.2.1 (c (n "steam-workshop-api") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0gxlp2msnhssn74ykg6b18bi0bvfwyrh91wif768b6c02gz2mr74")))

(define-public crate-steam-workshop-api-0.2.2 (c (n "steam-workshop-api") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0arb1p83fzwpy9j2z6jiyy46m7w3k5af602gzl1xb9dfsazlim18")))

(define-public crate-steam-workshop-api-0.2.4 (c (n "steam-workshop-api") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1ralg2hg4a3hi29kq9m166d52957y500062vgrm3np6vj0ma0h73")))

(define-public crate-steam-workshop-api-0.2.5 (c (n "steam-workshop-api") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "08rm0w7g5mxdxm2dg4irzwixdaizq53yap6qp5czjbb7sr6c267d")))

(define-public crate-steam-workshop-api-0.3.0 (c (n "steam-workshop-api") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "19mzvs4fwb3ib5y81i6l72ry0f194pv13kiahnasvxiagcvfnq02")))

