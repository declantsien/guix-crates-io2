(define-module (crates-io st ea steamgriddb_api) #:use-module (crates-io))

(define-public crate-steamgriddb_api-0.1.0 (c (n "steamgriddb_api") (v "0.1.0") (d (list (d (n "reqwest") (r "0.11.*") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "urlencoding") (r "2.1.*") (d #t) (k 0)))) (h "0rmbflqvnimwdmx3cq40m8ydarnza8nqazd0h9jk3yvf1avph49g") (y #t)))

(define-public crate-steamgriddb_api-0.1.1 (c (n "steamgriddb_api") (v "0.1.1") (d (list (d (n "reqwest") (r "0.11.*") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "urlencoding") (r "2.1.*") (d #t) (k 0)))) (h "0cg2y3vpi7zy01nk37cil3m805600l53jbf678j2sfinkf74hjrg") (y #t)))

(define-public crate-steamgriddb_api-0.1.2 (c (n "steamgriddb_api") (v "0.1.2") (d (list (d (n "reqwest") (r "0.11.*") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "urlencoding") (r "2.1.*") (d #t) (k 0)))) (h "1ff6c9jj03b8ckx0kkc2bhv7slm5nsqn733bqznhf7id8clny65c") (y #t)))

(define-public crate-steamgriddb_api-0.1.3 (c (n "steamgriddb_api") (v "0.1.3") (d (list (d (n "reqwest") (r "0.11.*") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "urlencoding") (r "2.1.*") (d #t) (k 0)))) (h "072sqm4sdyis0534p568dlikrbd0mg9hjmvvgsfj4ywf3x74ad3b")))

(define-public crate-steamgriddb_api-0.2.0 (c (n "steamgriddb_api") (v "0.2.0") (d (list (d (n "reqwest") (r "0.11.*") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "urlencoding") (r "2.1.*") (d #t) (k 0)))) (h "0prdn4inmf5wf261v62j3p1xpz0hwc9vaf8rx81bq5nbm789v629")))

(define-public crate-steamgriddb_api-0.2.1 (c (n "steamgriddb_api") (v "0.2.1") (d (list (d (n "reqwest") (r "0.11.*") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "urlencoding") (r "2.1.*") (d #t) (k 0)))) (h "02sl4xi06fjnd2kpc8k9vrfnvja2lz7nnqqwx27kpbhicrz0mw2p")))

(define-public crate-steamgriddb_api-0.3.0 (c (n "steamgriddb_api") (v "0.3.0") (d (list (d (n "reqwest") (r "0.11.*") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "urlencoding") (r "2.1.*") (d #t) (k 0)))) (h "1iq5wxg70f58h9avbg2drg4qm43yqcbjcj4scjaf25x59f48m5hl")))

(define-public crate-steamgriddb_api-0.3.1 (c (n "steamgriddb_api") (v "0.3.1") (d (list (d (n "reqwest") (r "0.11.*") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "urlencoding") (r "2.1.*") (d #t) (k 0)))) (h "0adzcb2k03naczd66jqyl72hm7c1pn4fy35aqcbryn0drxnsvg8f") (f (quote (("default" "async") ("blocking") ("async"))))))

