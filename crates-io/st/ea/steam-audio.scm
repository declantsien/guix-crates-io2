(define-module (crates-io st ea steam-audio) #:use-module (crates-io))

(define-public crate-steam-audio-0.2.0 (c (n "steam-audio") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.32.3") (d #t) (k 1)))) (h "06z0hmkk19hm2mxh5l0966yl0r3xic6g2zp8lmrrsz0x02pb0fks")))

(define-public crate-steam-audio-0.2.1 (c (n "steam-audio") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.32.3") (d #t) (k 1)))) (h "0jsgdgns9cs1ynnsrpcmfk7kr675lz6a9ypfvkj7dq103dfvwxri")))

(define-public crate-steam-audio-0.2.2 (c (n "steam-audio") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.32.3") (d #t) (k 1)))) (h "143qd8c5156bqwzk8zpvw21kqm9im8bm3jy1j9bb3bpbqb7x4kh4")))

(define-public crate-steam-audio-0.2.3 (c (n "steam-audio") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.32.3") (d #t) (k 1)))) (h "1q3lcvyv4fy2w6zpgvkz1ih96ywdw5p678gr1gkc3ha158mz6cwm")))

(define-public crate-steam-audio-0.3.0 (c (n "steam-audio") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "glam") (r "^0.20.2") (d #t) (k 0)) (d (n "lewton") (r "^0.10.2") (d #t) (k 0)) (d (n "steam-audio-sys") (r "^0.3.0") (d #t) (k 0)))) (h "04b6k0hwicwr6a9hvdsgfj5lmz6jlqr9liwxpx5pz1wwhvvzypsc")))

