(define-module (crates-io st ea steam-switcher) #:use-module (crates-io))

(define-public crate-steam-switcher-0.1.0 (c (n "steam-switcher") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27.7") (d #t) (k 0)))) (h "05f0q9ydf6gv8sfrpfw27xv0xsmcv01kj46vzimkfvg9kddlqqs1")))

