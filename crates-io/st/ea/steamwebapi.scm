(define-module (crates-io st ea steamwebapi) #:use-module (crates-io))

(define-public crate-steamwebapi-0.1.0 (c (n "steamwebapi") (v "0.1.0") (d (list (d (n "hyper") (r "~0.6.13") (d #t) (k 0)) (d (n "serde") (r "~0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.6.0") (d #t) (k 0)))) (h "1fkdblaj54rvrqgs4n8zq1sik66pn3s21rih5flqn7gbn697y8sd")))

(define-public crate-steamwebapi-0.2.0 (c (n "steamwebapi") (v "0.2.0") (d (list (d (n "hyper") (r "~0.6.13") (d #t) (k 0)) (d (n "serde") (r "~0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.6.0") (d #t) (k 0)))) (h "09cdr6jmscps5kl2589na61n4xfjhv0s6ylhbcrgqc4rc5vcy8k0")))

(define-public crate-steamwebapi-0.3.0 (c (n "steamwebapi") (v "0.3.0") (d (list (d (n "hyper") (r "~0.6.13") (d #t) (k 0)) (d (n "serde") (r "~0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.6.0") (d #t) (k 0)))) (h "1zj7r18isz3lbhk7h3rwvfp4c6zcnyfafc8ynjjz0w58iaykxp24")))

(define-public crate-steamwebapi-0.4.0 (c (n "steamwebapi") (v "0.4.0") (d (list (d (n "hyper") (r "~0.6.13") (d #t) (k 0)) (d (n "serde") (r "~0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.6.0") (d #t) (k 0)) (d (n "steamid") (r "~0.1.0") (d #t) (k 0)))) (h "0d1ijvxca4bml3cdp381vqnwpsnkaznkjchxm2hn52mrjlzg5ipd")))

