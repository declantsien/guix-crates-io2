(define-module (crates-io st ea steam-shortcut-sync) #:use-module (crates-io))

(define-public crate-steam-shortcut-sync-1.0.0 (c (n "steam-shortcut-sync") (v "1.0.0") (d (list (d (n "ctrlc") (r "^3.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0ncahd4gk50wlxlsgng9i5dadqwixbqgakr8lp5brl1cdkwsbf9a")))

(define-public crate-steam-shortcut-sync-1.0.1 (c (n "steam-shortcut-sync") (v "1.0.1") (d (list (d (n "ctrlc") (r "^3.2.2") (f (quote ("termination"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1rfvpvls7dy7fd4xzphi8bwmxhbvw27x6k4vddyrh7zxm1rh4hsv")))

(define-public crate-steam-shortcut-sync-1.0.2 (c (n "steam-shortcut-sync") (v "1.0.2") (d (list (d (n "ctrlc") (r "^3.2.2") (f (quote ("termination"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0g3zkdhj416hzb20vy5qd173xfwgvlh1knn28xy7pf99qpqzcwi8")))

