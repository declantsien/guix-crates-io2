(define-module (crates-io st ea steam_store_api) #:use-module (crates-io))

(define-public crate-steam_store_api-0.1.0 (c (n "steam_store_api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (f (quote ("serde_json" "json"))) (d #t) (k 0)) (d (n "rust_iso3166") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^4.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1.24.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "08gv30c4zq42vx6bg723ambhg399nn17xlzsr7mdqgq4mplcdswy")))

