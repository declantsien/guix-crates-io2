(define-module (crates-io st ea steamid) #:use-module (crates-io))

(define-public crate-steamid-0.1.0 (c (n "steamid") (v "0.1.0") (h "1g7sjjbj7003ibf6df8s319q5rzqmfa4rr3ashp6yg989dxivfqq")))

(define-public crate-steamid-0.2.0 (c (n "steamid") (v "0.2.0") (d (list (d (n "redis") (r "*") (o #t) (d #t) (k 0)))) (h "0ypan0bi2x19qccaj4f7py0mfrjhclkhrnrdbchv0py0ahc0dyw7") (f (quote (("redis-support" "redis"))))))

(define-public crate-steamid-0.3.0 (c (n "steamid") (v "0.3.0") (d (list (d (n "redis") (r "*") (o #t) (d #t) (k 0)))) (h "17l7c6q4jscifsmpzglhj0ysz04pj11di6kic0bfnx8095ndy2ad") (f (quote (("redis-support" "redis"))))))

