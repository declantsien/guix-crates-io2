(define-module (crates-io st ea steam-kit) #:use-module (crates-io))

(define-public crate-steam-kit-0.1.0 (c (n "steam-kit") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1njr1yccpwl7kylbqwigvlw7xf2ql6sqghiznmcygjz4r9a1xkgr") (y #t)))

(define-public crate-steam-kit-0.1.1 (c (n "steam-kit") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p93h4bxkxn6jk2bxg0s8bwzifd13nfcr1wahhczaszpvpfyr3zj") (y #t)))

(define-public crate-steam-kit-0.1.2 (c (n "steam-kit") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d23s5sw4lm5jnnihgkq1brh63w1czdyl4s1zy3qjcx4qk155j5b")))

