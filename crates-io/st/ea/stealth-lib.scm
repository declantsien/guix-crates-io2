(define-module (crates-io st ea stealth-lib) #:use-module (crates-io))

(define-public crate-stealth-lib-0.1.0 (c (n "stealth-lib") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)))) (h "1xjc9xib2qhdgalyvaa97l354sma3ib3jn1g36rpap1184jvys94")))

(define-public crate-stealth-lib-0.1.1 (c (n "stealth-lib") (v "0.1.1") (d (list (d (n "borsh") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)))) (h "0rw9yksrf3awc24dp0h8p47njznrdlbj74zj3lfs6kic110rvc1w")))

(define-public crate-stealth-lib-0.1.2 (c (n "stealth-lib") (v "0.1.2") (d (list (d (n "borsh") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)))) (h "098nrg2g0pjlwj6bnipdp39p7ajm12iksbyd86jcbhja6ajyfhwz")))

(define-public crate-stealth-lib-0.1.3 (c (n "stealth-lib") (v "0.1.3") (d (list (d (n "borsh") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)))) (h "1kscn76d1809q8cqnrbhixb7ga4iqlhjbmx7vig8bbvw45gql0k4")))

(define-public crate-stealth-lib-0.1.4 (c (n "stealth-lib") (v "0.1.4") (d (list (d (n "borsh") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)))) (h "0scqcnjxfx05z653vv6p0bpfcf2ygsah6qk348fdm5fh28l8gqs1")))

