(define-module (crates-io st ea steam-util) #:use-module (crates-io))

(define-public crate-steam-util-0.3.0 (c (n "steam-util") (v "0.3.0") (h "0kvnkp40ksnyv95lw3j42vibaf9011ifzyacv6srm7wvbbdni7mj") (y #t)))

(define-public crate-steam-util-0.1.0 (c (n "steam-util") (v "0.1.0") (h "15w2l3pjba7pnvl3qy0j3aip87f1i3kxv0j0b0p26b8xk84cacc6") (y #t)))

