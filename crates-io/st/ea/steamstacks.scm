(define-module (crates-io st ea steamstacks) #:use-module (crates-io))

(define-public crate-steamstacks-0.1.0 (c (n "steamstacks") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "steamstacks-bindings") (r "^0.1.0") (d #t) (k 0)))) (h "1lccjnvn2bybvmc5zpqh5lb0yyqv16ska0aqiy0x5nkj025zpkvh")))

