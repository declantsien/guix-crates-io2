(define-module (crates-io st ea steam-crypto) #:use-module (crates-io))

(define-public crate-steam-crypto-0.1.0 (c (n "steam-crypto") (v "0.1.0") (d (list (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "pem-parser") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "15shlirxs6hgp0kcvjcq693wviavadmbqpbfbc5j5pmx0kb329h3")))

(define-public crate-steam-crypto-0.1.1 (c (n "steam-crypto") (v "0.1.1") (d (list (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "pem-parser") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "07dmgqwjnfb10jj02b03zm99rxpbwfls38ij6jgjxbx1gn896paj")))

