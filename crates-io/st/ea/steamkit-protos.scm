(define-module (crates-io st ea steamkit-protos) #:use-module (crates-io))

(define-public crate-steamkit-protos-0.1.0 (c (n "steamkit-protos") (v "0.1.0") (d (list (d (n "protobuf") (r "^3.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.2") (d #t) (k 1)) (d (n "protoc-bin-vendored") (r "^3.0") (d #t) (k 1)))) (h "01bnsg2ldkz1r6mz80r9bdms3k2v2xznjfp53af80d3qcwfs7msa")))

