(define-module (crates-io st ea stealth) #:use-module (crates-io))

(define-public crate-stealth-0.0.0 (c (n "stealth") (v "0.0.0") (d (list (d (n "stealth_macro") (r "^0.0.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_Kernel" "Win32_System_SystemInformation" "Win32_System_SystemServices" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (t "cfg(windows)") (k 2)))) (h "0vgw240b1m2kmjn59vsswp9dcd2yijaafakbyx0xjslkcnkldyf4")))

