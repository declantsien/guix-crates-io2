(define-module (crates-io st ea steam-shortcut-sync-client) #:use-module (crates-io))

(define-public crate-steam-shortcut-sync-client-1.0.0 (c (n "steam-shortcut-sync-client") (v "1.0.0") (h "005ljsbva5jy7ic517cbr5lc78j3zmhjwxnw2zdjrrz9fmwj8r2c")))

(define-public crate-steam-shortcut-sync-client-1.0.1 (c (n "steam-shortcut-sync-client") (v "1.0.1") (h "11ya48ij9lhbszpas0hfgmab95k08m36zqzmscxjw0z87qpqa3n4")))

(define-public crate-steam-shortcut-sync-client-1.0.2 (c (n "steam-shortcut-sync-client") (v "1.0.2") (h "09m14ndc2z5gvm3456pil9q9dkz506cah9zjvmlhyddbxlfgxy1l")))

