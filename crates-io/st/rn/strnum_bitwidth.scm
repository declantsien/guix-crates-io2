(define-module (crates-io st rn strnum_bitwidth) #:use-module (crates-io))

(define-public crate-strnum_bitwidth-0.1.1 (c (n "strnum_bitwidth") (v "0.1.1") (h "0vxvnkwnxacv1pha4c4nv3y7sblv9nkwmrc2hcdl6x7mz8iajixs")))

(define-public crate-strnum_bitwidth-0.1.2 (c (n "strnum_bitwidth") (v "0.1.2") (h "02d5f60sxbs0h3z50zy81c680axif01i7hpflhdh03682vfcns2x")))

