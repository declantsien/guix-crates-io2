(define-module (crates-io st rn strng) #:use-module (crates-io))

(define-public crate-strng-0.0.0 (c (n "strng") (v "0.0.0") (h "0qs6wd97lf23hr4fc0bn9q4fdpwk93jy5j1rdf4mxz6kgaw1nz54")))

(define-public crate-strng-0.1.0 (c (n "strng") (v "0.1.0") (d (list (d (n "containers") (r "^0.9") (d #t) (k 0)))) (h "1fx52l7sf6gixfkdxm0419iwgb5rrg4cvqgi9zwjj388pf2v86q3")))

(define-public crate-strng-0.1.1 (c (n "strng") (v "0.1.1") (d (list (d (n "containers") (r "^0.9") (d #t) (k 0)))) (h "17x34p41aj9nncrmv2l6zqrdj0i5lmraqa0cvm2l4b7f858lkmbm")))

(define-public crate-strng-0.2.0 (c (n "strng") (v "0.2.0") (d (list (d (n "containers") (r "^0.9") (d #t) (k 0)))) (h "0wf5h5wmi9bzliq9f5h1zw7r346skx3rivr839yv3wq2bdbmfr6b")))

(define-public crate-strng-0.2.1 (c (n "strng") (v "0.2.1") (d (list (d (n "containers") (r "^0.9") (d #t) (k 0)))) (h "0j8mszdagzpys79fi1qcq4y7x2p3ja3rjba36f5ifldd67k9isdj")))

(define-public crate-strng-0.2.2 (c (n "strng") (v "0.2.2") (d (list (d (n "containers") (r "^0.9") (d #t) (k 0)))) (h "0r36sv3246sm1blghrrai05bzldj361sdijabxl09qvc9zbpc56r")))

