(define-module (crates-io st ro stroming) #:use-module (crates-io))

(define-public crate-stroming-0.0.1 (c (n "stroming") (v "0.0.1") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jbvi6jx57mamcgwl5zys2ip1shdpwh5mask515v23pkjs6g0p91")))

(define-public crate-stroming-0.0.2 (c (n "stroming") (v "0.0.2") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1gifpzc05nf7ygxlpw4fbfg09n1fcrn72ri1c8pgmfix4y4jy6mn")))

(define-public crate-stroming-0.0.3 (c (n "stroming") (v "0.0.3") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "06ixbpx6zwr5fmgqz3idgfzdw5qx2z3px7x81b0dbpd2mhi88ad3")))

(define-public crate-stroming-0.0.4 (c (n "stroming") (v "0.0.4") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wjcb8hp8d46zh0nlgahrki4nf21bykf3p0499j5aqwhkgb8hg8z")))

(define-public crate-stroming-0.0.5 (c (n "stroming") (v "0.0.5") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "09sav0l24qhprhiscwna8wd57fv112ck1q9igj5iiivnda0igf3h")))

(define-public crate-stroming-0.0.6 (c (n "stroming") (v "0.0.6") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "153f66a3yffqdmqdnmjifzw54vgiyh94hz8anmixdzq1ma869jxy")))

(define-public crate-stroming-0.0.7 (c (n "stroming") (v "0.0.7") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1v79aykxa049r9npsk1bcmh67xg8nm007hiqibsm4kxirwdf5m6n")))

(define-public crate-stroming-0.0.8 (c (n "stroming") (v "0.0.8") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1z5ipb9hili3srdx3qrpv67rzxr517s95cdy27j11g3gq8cppxhv")))

(define-public crate-stroming-0.0.9 (c (n "stroming") (v "0.0.9") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "04flqh6cm4bwwchyik74p49mjy26frrbsh8kg2jrbmdjr092lnwi")))

(define-public crate-stroming-0.0.10 (c (n "stroming") (v "0.0.10") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "09f0n3arvw5b88nmrr1hwf4463ppjd0rijmpbcaz1bcz8fwfjss8")))

