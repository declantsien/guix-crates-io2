(define-module (crates-io st ro stroke) #:use-module (crates-io))

(define-public crate-stroke-0.1.0 (c (n "stroke") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "tinyvec") (r "^1.1.1") (d #t) (k 0)))) (h "0npsx7w0dfdl9drz1bynhhfb9qd8z5wpmgdwz0kiz45c6daqsvzx")))

(define-public crate-stroke-0.2.0 (c (n "stroke") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (d #t) (k 0)))) (h "0kzsz7c142h4cxc6y9sza8wccb9j4wj3q7ji6fxwxsikhpxbv7cs")))

