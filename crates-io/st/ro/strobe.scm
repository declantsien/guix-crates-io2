(define-module (crates-io st ro strobe) #:use-module (crates-io))

(define-public crate-strobe-0.1.0 (c (n "strobe") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1cbq0pl27bb671n7gzkn8b15c3vr9lzfjf1ys078qgh9nm2087ns") (f (quote (("std") ("default" "std") ("_benchmarking" "std" "rand"))))))

(define-public crate-strobe-0.1.1 (c (n "strobe") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0q8zjfmbm652lqf5a0x3av740q464zww5ln3x4r6vi7xzj0bmb8m") (f (quote (("std") ("default" "std") ("_benchmarking" "std" "rand"))))))

(define-public crate-strobe-0.1.2 (c (n "strobe") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1my72i7lyphqdm7iyhwzns59c9fk1r5wgs0yvg33i553pp0w3x5j") (f (quote (("std") ("default" "std") ("_benchmarking" "std" "rand"))))))

(define-public crate-strobe-0.1.3 (c (n "strobe") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0y7wiqcib6irqyfjkr5b8d4s5nyasygcwrck9zldf4klqxxjbgzv") (f (quote (("std") ("default" "std"))))))

(define-public crate-strobe-0.2.0 (c (n "strobe") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0nxxkhfrpj2a9w8gvpjhrkj04lac153mbjxcml5znplwf1jx3wh6") (f (quote (("std") ("default" "std" "align_64") ("align_selected") ("align_rust" "align_selected") ("align_8" "align_selected") ("align_64" "align_selected") ("align_512" "align_selected") ("align_4" "align_selected") ("align_32" "align_selected") ("align_256" "align_selected") ("align_2" "align_selected") ("align_16" "align_selected") ("align_128" "align_selected") ("align_1024" "align_selected") ("align_1" "align_selected"))))))

