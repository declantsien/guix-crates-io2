(define-module (crates-io st ro strong-xml-derive) #:use-module (crates-io))

(define-public crate-strong-xml-derive-0.1.3 (c (n "strong-xml-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pm0r2cb516qdni1vda4iiz96cyhjgacgmckmy2l5hrzxyqbc7f8")))

(define-public crate-strong-xml-derive-0.2.0 (c (n "strong-xml-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "1zyymqndyxkq0krfaw31xffdcz7yfjip3pvjcf8q35q8sl7hd8wf")))

(define-public crate-strong-xml-derive-0.2.1 (c (n "strong-xml-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "0zwd95nlva3gmq8v9b21n894snyizwq53ni1rdzxk0dgc8ig3lkl")))

(define-public crate-strong-xml-derive-0.3.0 (c (n "strong-xml-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0ljf9hnb3h8hav49abspr1sn5v96gsm8l7zqaslmd2l9nfmvxhm2")))

(define-public crate-strong-xml-derive-0.4.0 (c (n "strong-xml-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "103bw5692wn4824fjrs4yqagxipn5anrqrd1pz1jhi9plfw7bqa1")))

(define-public crate-strong-xml-derive-0.4.1 (c (n "strong-xml-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "1kaqk4kxyw5akfjp3wj3zkivr5l899n5c0hxf2wrlvhqgla78046")))

(define-public crate-strong-xml-derive-0.5.0 (c (n "strong-xml-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0vzsalp59dd0jgkinqscwy3295bbxh43vaw0ncypbclrqrzssq3k")))

(define-public crate-strong-xml-derive-0.6.0-alpha (c (n "strong-xml-derive") (v "0.6.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0wg20wlnl90974pj7w1vl0njf63nzhnidc1h5l0bqlvv9fhz58p4")))

(define-public crate-strong-xml-derive-0.6.0 (c (n "strong-xml-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "1pmzw9h98yw5addmlnw5n95zmcb8mkjxjd2i95fzaqafnrgy5r6j")))

(define-public crate-strong-xml-derive-0.6.1 (c (n "strong-xml-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s9vw6kgmqiidnhzb05yvq40c3db0y5saybkgpxa754j31q3c1g9")))

(define-public crate-strong-xml-derive-0.6.2 (c (n "strong-xml-derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nnpm74v9sf7lvghy38i6j2m541rplvrwmcvahcv2msmgzlsagrc")))

(define-public crate-strong-xml-derive-0.6.3 (c (n "strong-xml-derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qf7smxaaglj7p9fs0csd0cyvwcy30w96pdy2aqi65ijk7s83iwj")))

