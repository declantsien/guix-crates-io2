(define-module (crates-io st ro strom) #:use-module (crates-io))

(define-public crate-strom-0.1.0 (c (n "strom") (v "0.1.0") (h "1pxl0cam05vwvb66w52p11zkj2rb51gsa75mqsxx9frmqwx8zqaq")))

(define-public crate-strom-0.1.1 (c (n "strom") (v "0.1.1") (h "0jmqccwaj48fkcwg3j4s55q2y2br5ms93qhn6zm3054710l8h96d")))

