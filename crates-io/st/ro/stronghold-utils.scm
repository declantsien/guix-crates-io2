(define-module (crates-io st ro stronghold-utils) #:use-module (crates-io))

(define-public crate-stronghold-utils-0.3.0 (c (n "stronghold-utils") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "riker") (r "^0.4") (d #t) (k 0)) (d (n "stronghold-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1ccswlqqz1hssl29asybywax8hd97klp4wxlm8cglgb2y1arrd55")))

(define-public crate-stronghold-utils-0.3.1 (c (n "stronghold-utils") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "stronghold-derive") (r "^0.3.0") (d #t) (k 0)))) (h "19nmrf2madgf2n51jqsdlv7hk5l0ql3dyqqzgg7m4bk5qajz9a82") (y #t)))

(define-public crate-stronghold-utils-0.4.0 (c (n "stronghold-utils") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "stronghold-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0p0pzrnsp9nyd4swcihb7xrlr3h691zr4b8ig85301im10x4kn1h")))

(define-public crate-stronghold-utils-0.4.1 (c (n "stronghold-utils") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "stronghold-derive") (r "^0.3.1") (d #t) (k 0)))) (h "0lfrdjlhmk7gqigfj68968qckjxfmrj2vczjf6bsi3jy43bf075y")))

(define-public crate-stronghold-utils-1.0.0 (c (n "stronghold-utils") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "stronghold-derive") (r "^1.0.0") (d #t) (k 0)))) (h "1mjy8y99g5lbylj9ybar5mgmh8ba3jyrvr36gwz1apmgk1422043")))

