(define-module (crates-io st ro strops) #:use-module (crates-io))

(define-public crate-strops-0.1.0 (c (n "strops") (v "0.1.0") (h "09mxaxyjk4p1svxs8fkkbnzi1zciyjw49d3rzbr3dz2my49qdqiw")))

(define-public crate-strops-0.1.1 (c (n "strops") (v "0.1.1") (h "0gmm37ib5hayv3m7ynriwcsfslfqakw9khqxbs69dy0nh7vlh953")))

(define-public crate-strops-0.1.2 (c (n "strops") (v "0.1.2") (h "1h89s0krf4v4cnq1zz721f116brs1d4djs97qjj9i584qn4sjfsl")))

(define-public crate-strops-0.1.3 (c (n "strops") (v "0.1.3") (h "14isz6ip5qgr3w2v9sq3mfyxz6kcnlphz21vq689ajfpvmaij9cc")))

(define-public crate-strops-0.1.4 (c (n "strops") (v "0.1.4") (h "1cq6gqn60gqiwg6lsljz98fc8gyrsi0a4iag5f2shjhlcmfdyq5w")))

(define-public crate-strops-0.1.4-test (c (n "strops") (v "0.1.4-test") (h "0cwdfgvz7fjmypqma1xh6grwgyx3d7lyvn6m0w90py3npcx1iw7l")))

(define-public crate-strops-0.1.4-test-1 (c (n "strops") (v "0.1.4-test-1") (h "1661dd7xic5hpdj08frnfs3ghvxps99djilbfvakyljlm1lg4gif")))

