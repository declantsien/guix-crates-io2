(define-module (crates-io st ro strong-type) #:use-module (crates-io))

(define-public crate-strong-type-0.1.0 (c (n "strong-type") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0qlm1qkys3ndaq1ha84w1wq8zhsiy9kqmibw69c50wfz687mjcq5") (y #t)))

(define-public crate-strong-type-0.1.1 (c (n "strong-type") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1a53cbfl7pnc50559n6q5zxarm4fpmw0137zjcmkb40s766ja6c4")))

(define-public crate-strong-type-0.2.0 (c (n "strong-type") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0qchq12cvmdl1a4f04cmdbhr4x5p92lsdwv931yh4slqvaw8zjad")))

(define-public crate-strong-type-0.3.0 (c (n "strong-type") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1pk9pf9q816y2f97ix0mppj00qxwx61jxvq7yis00zg002r4ii3h")))

(define-public crate-strong-type-0.4.0 (c (n "strong-type") (v "0.4.0") (d (list (d (n "strong-type-derive") (r "^0.4.0") (d #t) (k 0)))) (h "0kylfhvjpwasys4avhwdizk6w124fwk9v1slyg53jp8kfwg2gvf5")))

(define-public crate-strong-type-0.5.0 (c (n "strong-type") (v "0.5.0") (d (list (d (n "strong-type-derive") (r "^0.5.0") (d #t) (k 0)))) (h "15w4q998ha6wz093lzx3605lnijcfn1qs33jjh83g779qyhmpwvy")))

(define-public crate-strong-type-0.6.0 (c (n "strong-type") (v "0.6.0") (d (list (d (n "strong-type-derive") (r "^0.6.0") (d #t) (k 0)))) (h "0z6m0dybhf650888b2lxnymgkhphz0icd1lh1jzhai704ybiaiim")))

(define-public crate-strong-type-0.7.0 (c (n "strong-type") (v "0.7.0") (d (list (d (n "strong-type-derive") (r "^0.7.0") (d #t) (k 0)))) (h "11pjbi9bijs0agr3lfmvg30v3lz92yka239l33bv723rxd9nhlvy")))

(define-public crate-strong-type-0.8.0 (c (n "strong-type") (v "0.8.0") (d (list (d (n "strong-type-derive") (r "^0.8.0") (d #t) (k 0)))) (h "1fdcrma7nrkspbwrndxjw9ilfqrw4svhggsy333nr5q0ygz1yadp")))

(define-public crate-strong-type-0.9.0 (c (n "strong-type") (v "0.9.0") (d (list (d (n "strong-type-derive") (r "^0.9.0") (d #t) (k 0)))) (h "15hff4n4x7k0vz9s90cy7amph1ply5yrhykzdwg1390dgmaq2izx")))

(define-public crate-strong-type-0.10.0 (c (n "strong-type") (v "0.10.0") (d (list (d (n "strong-type-derive") (r "^0.10.0") (d #t) (k 0)))) (h "0jz8njkxfcagzxkwadvic68nwrb62601lgyiwnpi36v6kvzg7lxx")))

(define-public crate-strong-type-0.11.0 (c (n "strong-type") (v "0.11.0") (d (list (d (n "strong-type-derive") (r "^0.11.0") (d #t) (k 0)))) (h "0agaq23i5wq4y73mcai0xqykng93dzzvna5imwp8gdjdbc842jv5")))

(define-public crate-strong-type-0.11.1 (c (n "strong-type") (v "0.11.1") (d (list (d (n "strong-type-derive") (r "^0.11.1") (d #t) (k 0)))) (h "15kxailn34sapkl87xsc574vbx12c7rzp86ahavqa4z7kfn64qjc")))

(define-public crate-strong-type-0.11.2 (c (n "strong-type") (v "0.11.2") (d (list (d (n "strong-type-derive") (r "^0.11.2") (d #t) (k 0)))) (h "03h1538rcfayx3979l4k1jh3pmvfw5wajcr7clrb8z1l8q3yspx4")))

