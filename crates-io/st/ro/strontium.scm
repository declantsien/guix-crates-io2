(define-module (crates-io st ro strontium) #:use-module (crates-io))

(define-public crate-strontium-0.5.1 (c (n "strontium") (v "0.5.1") (d (list (d (n "bytes") (r "^0.4.10") (d #t) (k 0)))) (h "1wx0amkmcgdqdvs6p3v3h9g9wk8lx815a4bjzs8vigs1pkf64iki")))

(define-public crate-strontium-0.5.2 (c (n "strontium") (v "0.5.2") (d (list (d (n "bytes") (r "^0.4.10") (d #t) (k 0)))) (h "0fanpfn6ag638fbfy862jc507ia4w5vdwf88hw7r9kqb3c7yx7rl")))

(define-public crate-strontium-0.5.3 (c (n "strontium") (v "0.5.3") (d (list (d (n "bytes") (r "^0.4.10") (d #t) (k 0)))) (h "1zg383bkm1jm3ryma615ph27kkkd4iy6fhgji6sndajqlg6vn4g8")))

(define-public crate-strontium-0.5.4 (c (n "strontium") (v "0.5.4") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)))) (h "0v2m7f3b7jbf37wz11hdjjsnzjslzaa1hyy8nyiszs4by7r1mnzn")))

(define-public crate-strontium-0.5.5 (c (n "strontium") (v "0.5.5") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1pg81v4dgwp9mix8zxp9hp4gqs6zq94az7bb2r1d74c4p63jrq8x")))

(define-public crate-strontium-0.6.0 (c (n "strontium") (v "0.6.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ld3qrj9d1z5nil1ib4c3vpd6qdyhl76gb70cxvv6ajbnqkycfp7")))

