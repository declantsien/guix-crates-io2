(define-module (crates-io st ro stronghold) #:use-module (crates-io))

(define-public crate-stronghold-0.1.0 (c (n "stronghold") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1wcb6p8nsffwp11hd8l02z21xzd43lnyr3rigbdws888vj1ng1kx")))

(define-public crate-stronghold-0.2.0 (c (n "stronghold") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "06h4vmdw4hx3zyjd3mvpqa6ddnhghv525qz4pimxmd1f22gxsf3v")))

(define-public crate-stronghold-0.2.1 (c (n "stronghold") (v "0.2.1") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "06767x94xlyh2vhvncsplgshgjmadygl3w738rna4pgb1gdm26x0")))

