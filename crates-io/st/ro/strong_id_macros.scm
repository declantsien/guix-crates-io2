(define-module (crates-io st ro strong_id_macros) #:use-module (crates-io))

(define-public crate-strong_id_macros-0.1.0 (c (n "strong_id_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10zafjgkplv56f5jraya3ll2zrfhczhpp61rf7cnhszsryan36j8") (f (quote (("uuid-v8") ("uuid-v7") ("uuid-v6") ("uuid-v5") ("uuid-v4") ("uuid-v3") ("uuid-v1") ("uuid") ("serde") ("delimited"))))))

(define-public crate-strong_id_macros-0.1.1 (c (n "strong_id_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1442g22bjk07wa7ki2nrib1bmpc5zq7psrbwr38wdljnf993mddv") (f (quote (("uuid-v8") ("uuid-v7") ("uuid-v6") ("uuid-v5") ("uuid-v4") ("uuid-v3") ("uuid-v1") ("uuid") ("serde") ("delimited")))) (y #t)))

(define-public crate-strong_id_macros-0.1.2 (c (n "strong_id_macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ss1zsigrp5h197zqjbsqzygy9sb4hz64dzchc7wpqbkx5p57irz") (f (quote (("uuid-v8") ("uuid-v7") ("uuid-v6") ("uuid-v5") ("uuid-v4") ("uuid-v3") ("uuid-v1") ("uuid") ("serde") ("delimited"))))))

(define-public crate-strong_id_macros-0.1.3 (c (n "strong_id_macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vfg5if4s40h8009iw5298b9v9gc1dx2dmq1mhpcp3pqrjf8p7xr") (f (quote (("uuid-v8") ("uuid-v7") ("uuid-v6") ("uuid-v5") ("uuid-v4") ("uuid-v3") ("uuid-v1") ("uuid") ("serde") ("delimited"))))))

(define-public crate-strong_id_macros-0.2.0 (c (n "strong_id_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ki7j0a11bdjnxgngvlj0c3nsgcfs7pxkfbgx7yhbars8aly9l6i") (f (quote (("uuid-v8") ("uuid-v7") ("uuid-v6") ("uuid-v5") ("uuid-v4") ("uuid-v3") ("uuid-v1") ("uuid") ("serde") ("delimited"))))))

(define-public crate-strong_id_macros-0.3.0 (c (n "strong_id_macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09i9ncv7hxxgl3yprma5xz2kw96lbyqxzmqhf0lbpyajpcl5zkas") (f (quote (("uuid-v8") ("uuid-v7") ("uuid-v6") ("uuid-v5") ("uuid-v4") ("uuid-v3") ("uuid-v1") ("uuid") ("serde") ("delimited"))))))

(define-public crate-strong_id_macros-0.4.0 (c (n "strong_id_macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15nypq178laikwwwvrzk5rfzqbfc6gla932jng9507n0k84b1qkp") (f (quote (("uuid-v8") ("uuid-v7") ("uuid-v6") ("uuid-v5") ("uuid-v4") ("uuid-v3") ("uuid-v1") ("uuid") ("serde") ("delimited"))))))

