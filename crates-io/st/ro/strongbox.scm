(define-module (crates-io st ro strongbox) #:use-module (crates-io))

(define-public crate-strongbox-0.1.0 (c (n "strongbox") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1v7prah97h5jxl9z4vxmna5fh0yan10p6q1ghg0ixh5h3c0hm7c5")))

