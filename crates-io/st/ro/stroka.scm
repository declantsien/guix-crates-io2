(define-module (crates-io st ro stroka) #:use-module (crates-io))

(define-public crate-stroka-1.0.0-beta.1 (c (n "stroka") (v "1.0.0-beta.1") (d (list (d (n "minivec") (r "^0.3") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "str-buf") (r "^2.0.2") (k 0)))) (h "08a9wydmjmpcq8rahxdb8210qw0lmrxghq5m8vfkc0skz5cbqlsy") (f (quote (("std"))))))

(define-public crate-stroka-1.0.0-beta.2 (c (n "stroka") (v "1.0.0-beta.2") (d (list (d (n "minivec") (r "^0.3") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "str-buf") (r "^2.0.2") (k 0)))) (h "0m3g9yqv5l189lsvlnx54l5zw5akl00wk5qg56a8wdq8bzg24ywk") (f (quote (("std"))))))

(define-public crate-stroka-1.0.0-beta.3 (c (n "stroka") (v "1.0.0-beta.3") (d (list (d (n "minivec") (r "^0.4") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "str-buf") (r "^2.0.2") (k 0)))) (h "060bjhw59digqdfqxkz75kilc19562f37lfmaxlzhlwkk0vyiqhb") (f (quote (("std"))))))

(define-public crate-stroka-1.0.0-beta.4 (c (n "stroka") (v "1.0.0-beta.4") (d (list (d (n "minivec") (r "^0.4") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "str-buf") (r "^3.0.1") (k 0)))) (h "1f1a3n9hmw4v304v8zhazs1pi5zamfz7wcabvxv89k418bnhrvcw") (f (quote (("std"))))))

