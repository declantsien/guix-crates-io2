(define-module (crates-io st ro strophe) #:use-module (crates-io))

(define-public crate-strophe-0.1.0 (c (n "strophe") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "076hx6xzyz0pix0ik3916j4v3vwrgf2b1g257jhh7ir9xrjq5g1d")))

(define-public crate-strophe-0.0.2 (c (n "strophe") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0hx356hb9lnmgb0ansfy469vl8w9ja77pikhc15k3sdy48f3wm2a")))

(define-public crate-strophe-0.0.1 (c (n "strophe") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0520j4d12hvibjs3x2fixg57wygljjhq8mx3r2cm7jnwzwkikygj")))

(define-public crate-strophe-0.1.1 (c (n "strophe") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0zwadigc7qyajk5kqam57ka427m3hkcz6wapj6dn6qpsiqshzvfq")))

