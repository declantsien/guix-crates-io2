(define-module (crates-io st ro stroke-width-transform) #:use-module (crates-io))

(define-public crate-stroke-width-transform-0.1.0 (c (n "stroke-width-transform") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.24.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "show-image") (r "^0.14.0") (f (quote ("image"))) (d #t) (k 2)))) (h "0gw0bnb7wbwzgzmlsz0xapvss92rhr0f6vj8vhyjxbvrg4sczpyq") (y #t)))

(define-public crate-stroke-width-transform-0.1.1 (c (n "stroke-width-transform") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.24.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "show-image") (r "^0.14.0") (f (quote ("image"))) (d #t) (k 2)))) (h "10q83p67nskc3pxky88a6682w8p8cz0wipgnfly3z8madf4wflck") (s 2) (e (quote (("rayon" "dep:rayon"))))))

