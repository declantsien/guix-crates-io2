(define-module (crates-io st ro strong-type-derive) #:use-module (crates-io))

(define-public crate-strong-type-derive-0.4.0 (c (n "strong-type-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1j83sbiki0narq7s8snvz9bwfsq9q2d5rv3qwdildwdwzv49jbbl")))

(define-public crate-strong-type-derive-0.5.0 (c (n "strong-type-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1hphfr70wjg89y7qk2c2cwil581m88y2sp0xinl0nhc6rnfdsswz")))

(define-public crate-strong-type-derive-0.6.0 (c (n "strong-type-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1s5izkkqjqkmf3gkbsd5lgybfgf1pp8cfffc6lk6qvf158fs4bls")))

(define-public crate-strong-type-derive-0.7.0 (c (n "strong-type-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1f8fi7bgnd22rx8rvxpb9d1xal3ywf3x64bysz9ibwswvzs9a41p")))

(define-public crate-strong-type-derive-0.8.0 (c (n "strong-type-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "19yhc6b49v91kv2w8apbrwwhxincrcrdb1q9l0nd8qkf0ilqir6z")))

(define-public crate-strong-type-derive-0.9.0 (c (n "strong-type-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "05qgyzmz09pj7216caq11hjpsxiv2kzrjmp28csjh7kqr9njvyhj")))

(define-public crate-strong-type-derive-0.10.0 (c (n "strong-type-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0kw8ciyhn9pqjq3rj8iqc9v7qqax6y7wzkbky5m17cnbgdla0nqn")))

(define-public crate-strong-type-derive-0.11.0 (c (n "strong-type-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1g1i1ibynrk25nk619wiqm8xhg6cjhds109ds3xfyr2zlyh2g4cr")))

(define-public crate-strong-type-derive-0.11.1 (c (n "strong-type-derive") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1hajq7rknaffls5vl2g9i4d0j6p3621pcm8ckr58pp1lnzm3fvw1")))

(define-public crate-strong-type-derive-0.11.2 (c (n "strong-type-derive") (v "0.11.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1iqpg9f8wbj406riw4vrl5jhw9icxb1smgsxkw4hrwbrzqkl2fpr")))

