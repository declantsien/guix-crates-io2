(define-module (crates-io st ro strongly) #:use-module (crates-io))

(define-public crate-strongly-0.0.1 (c (n "strongly") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "proc-macro" "parsing"))) (k 0)))) (h "17pxbacxhyam5jhcyd6al9zzrsxh64frp3f19cznxmbpznjm7d58") (f (quote (("std") ("default" "std")))) (y #t) (r "1.58")))

(define-public crate-strongly-0.1.1 (c (n "strongly") (v "0.1.1") (d (list (d (n "paste") (r "^1") (k 2)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "serde") (r "^1") (k 2)) (d (n "syn") (r "^2") (f (quote ("proc-macro" "full" "derive" "parsing" "printing"))) (k 0)))) (h "0j39r22895afhjac7hwffc8458pa42hva3sj1zx40lb4127555hy") (f (quote (("std") ("default" "std")))) (r "1.67")))

