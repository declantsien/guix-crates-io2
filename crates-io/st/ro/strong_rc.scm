(define-module (crates-io st ro strong_rc) #:use-module (crates-io))

(define-public crate-strong_rc-0.1.0 (c (n "strong_rc") (v "0.1.0") (d (list (d (n "field-offset") (r "^0.1.1") (d #t) (k 0)))) (h "1a8krc888qvw91iyr9yn52w9hhlz1yfv92fn7r084z10s76vd0xh")))

(define-public crate-strong_rc-0.1.1 (c (n "strong_rc") (v "0.1.1") (d (list (d (n "field-offset") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1khd6xyxhpmkr51ivh9j5dhy3mrjwzb7w98b9fc6i1smsbj1q1z7")))

(define-public crate-strong_rc-0.1.2 (c (n "strong_rc") (v "0.1.2") (d (list (d (n "field-offset") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0ifbmzjfiwhijvl1x2rl0jmdfzrpry85w9fbi2xzgxgd2hlmnbm6")))

(define-public crate-strong_rc-0.1.3 (c (n "strong_rc") (v "0.1.3") (d (list (d (n "field-offset") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0p3jb1dg6q7v09z4mahgs8r7kxj624gnv7bl6sygvrs4i26kzr19")))

(define-public crate-strong_rc-0.1.4 (c (n "strong_rc") (v "0.1.4") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0va1p52nsf42d1nmhz59sbphw72zhr6nbwisxw8q8ffmz9ai46xr")))

