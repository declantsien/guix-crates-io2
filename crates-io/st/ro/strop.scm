(define-module (crates-io st ro strop) #:use-module (crates-io))

(define-public crate-strop-0.1.0 (c (n "strop") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "enum-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "17j02g6m0p4vjkhkk8zlczjr21y9iv8qxqzl4sizdwbda7l3ads5")))

(define-public crate-strop-0.1.1 (c (n "strop") (v "0.1.1") (d (list (d (n "mos6502") (r "^0.2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jdhzjx62gncbsj8pfsx0d34hpx1vyyn46h8ikv3snsjcpwpafri")))

