(define-module (crates-io st um stumpless-sys) #:use-module (crates-io))

(define-public crate-stumpless-sys-0.1.0 (c (n "stumpless-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r ">=0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "095hgh8vvhv2z3vkllz78k6g0nyg4ji0fny9177gk48r7dznl08j") (f (quote (("wel") ("socket") ("network") ("journald"))))))

(define-public crate-stumpless-sys-0.2.0 (c (n "stumpless-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r ">=0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0v2pawxhw6dfz7hllc5pj45fsz08l41lkmdlmfzcwxkl0fbw68za") (f (quote (("wel") ("socket") ("network") ("journald") ("default" "network"))))))

(define-public crate-stumpless-sys-0.2.1 (c (n "stumpless-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.154") (d #t) (k 0)))) (h "0py1ijl59n8qf0m7373bd39kjlxlhzk1aa0lkamcf3zf276q3pg6") (f (quote (("wel") ("socket") ("network") ("journald") ("default" "network"))))))

(define-public crate-stumpless-sys-0.3.0 (c (n "stumpless-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.155") (d #t) (k 0)))) (h "0p0spq9rbpklcnm4gw5y6xsjdd598v240b6nx340gsma2pgrcf51") (f (quote (("wel") ("sqlite") ("socket") ("network") ("journald") ("default" "network"))))))

