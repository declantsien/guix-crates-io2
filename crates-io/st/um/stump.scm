(define-module (crates-io st um stump) #:use-module (crates-io))

(define-public crate-stump-0.2.0 (c (n "stump") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "1w685x24k0q5jl335n10zs8y77vf5wzkvd56q04hi5ydj1gy8yim")))

(define-public crate-stump-0.2.1 (c (n "stump") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "01q2wqnxd0n67cjcmpjp93c9ayxvx7cr0gvp01myf7pi28i7sxs5")))

