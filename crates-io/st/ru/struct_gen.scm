(define-module (crates-io st ru struct_gen) #:use-module (crates-io))

(define-public crate-struct_gen-0.1.0 (c (n "struct_gen") (v "0.1.0") (h "05wg9r66a2db5bjv4xiiq3arrhhrpzl65hkzxdx3xbw42isk88qq")))

(define-public crate-struct_gen-0.1.1 (c (n "struct_gen") (v "0.1.1") (d (list (d (n "struct_gen_derive") (r "^0.1.0") (d #t) (k 0)))) (h "08ci3m6cqgnq7c21f7w7xb3hql386j41cvi3ybs3sghw8xyrvzbv")))

