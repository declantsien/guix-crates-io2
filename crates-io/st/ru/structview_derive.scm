(define-module (crates-io st ru structview_derive) #:use-module (crates-io))

(define-public crate-structview_derive-0.1.0 (c (n "structview_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0v4n1vfv2s65y0x807nazyc5scjwk16jmfdlzri762nchnxzpydf")))

(define-public crate-structview_derive-0.2.0 (c (n "structview_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1vc4av9v2a2v6rgidh0g2cgaac53g26bhzw06hkhy22r43dmpfl9")))

(define-public crate-structview_derive-0.3.0 (c (n "structview_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15zssly0730igqvq5mwrk9viq9662xd7x7y9x81vv0fprs9axspf")))

(define-public crate-structview_derive-0.3.1 (c (n "structview_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hkxixrk3qr1i28b1jkn7zb4bnsdqh5h5dlkqzafd19xsnv7p41q")))

(define-public crate-structview_derive-1.0.0 (c (n "structview_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1km3haxk149pmnqkai86hsbmlgf00kjw6qlv3584wxk8rhyk25qj")))

(define-public crate-structview_derive-1.1.0 (c (n "structview_derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16mmafp2ikd4z3ggc0y5l6li31dxfzz0zxhdmpnfk4yfk1z9af77")))

