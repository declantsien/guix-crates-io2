(define-module (crates-io st ru structpath) #:use-module (crates-io))

(define-public crate-structpath-0.1.0 (c (n "structpath") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0m2j04p7l97qz2rm3wrqvvg6agmywka4bydnikyrswb1xpgd91wl")))

(define-public crate-structpath-0.2.0 (c (n "structpath") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1np355ps1bbdf5qlv4dzgxs583n9fd0269pa91dl4zjd9szff73j")))

(define-public crate-structpath-0.2.1 (c (n "structpath") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "16vvraq09v9cxwh4vqp47kxipgjm8gmvid8mvzfwmzwiggb1cj54")))

