(define-module (crates-io st ru structype_derive) #:use-module (crates-io))

(define-public crate-structype_derive-1.0.0 (c (n "structype_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)))) (h "16c71qlgkdp7yl8smcjw35k4aa21vj53q2ykmck7bf8cv4l5f6pb")))

(define-public crate-structype_derive-2.0.0 (c (n "structype_derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)))) (h "1x91lvikcdbzr89xi0m78zyqwxlgbbl26hlk8f0wk8b0y9jhsjl5")))

(define-public crate-structype_derive-3.0.0 (c (n "structype_derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "structype") (r "^3.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)))) (h "180045lakiw7pcl7z2iqczna8xmwg99whyk20c95gf4avqwbdphg")))

