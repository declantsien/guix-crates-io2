(define-module (crates-io st ru structfromdir) #:use-module (crates-io))

(define-public crate-structfromdir-0.1.0 (c (n "structfromdir") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0j0scqi8xd9sws90gcf8nlzj8vwq136a1ikycah780bwykwrd5zk") (r "1.56.1")))

