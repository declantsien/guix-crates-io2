(define-module (crates-io st ru struct_iterable) #:use-module (crates-io))

(define-public crate-struct_iterable-0.1.0 (c (n "struct_iterable") (v "0.1.0") (d (list (d (n "struct_iterable_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "struct_iterable_internal") (r "^0.1.1") (d #t) (k 0)))) (h "0i0pmxjr1ynfl14ly4qh0266km8dqw9ni2f2jafdflni2q1ndslk") (y #t)))

(define-public crate-struct_iterable-0.1.1 (c (n "struct_iterable") (v "0.1.1") (d (list (d (n "struct_iterable_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "struct_iterable_internal") (r "^0.1.1") (d #t) (k 0)))) (h "04kpis178xgj00lzjfhi9n08zdkrg02nryj15svm19khci60d6l4")))

