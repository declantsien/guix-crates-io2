(define-module (crates-io st ru structre_proc_macros) #:use-module (crates-io))

(define-public crate-structre_proc_macros-0.0.1 (c (n "structre_proc_macros") (v "0.0.1") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "0zng0sg6rh33r01ncngci38qxvp93dxw6v0xrrss9cspkn56krv6") (f (quote (("unicode") ("default" "unicode"))))))

(define-public crate-structre_proc_macros-0.0.2 (c (n "structre_proc_macros") (v "0.0.2") (d (list (d (n "genemichaels") (r "^0.1.21") (d #t) (k 2)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "12hdm8g1lkg1cvm6v1k8035i8ardafd3kg930jbnpmhbdw755xsw") (f (quote (("unicode") ("default" "unicode"))))))

(define-public crate-structre_proc_macros-0.0.3 (c (n "structre_proc_macros") (v "0.0.3") (d (list (d (n "genemichaels") (r "^0.1.21") (d #t) (k 2)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "0cq6c257vvfmp0rxq395dhcb8yi1x794f0r88gbywdmldlqrf0bb") (f (quote (("unicode") ("default" "unicode"))))))

