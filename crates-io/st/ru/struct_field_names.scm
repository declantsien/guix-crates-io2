(define-module (crates-io st ru struct_field_names) #:use-module (crates-io))

(define-public crate-struct_field_names-0.1.0 (c (n "struct_field_names") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18p121p662mzn0lpdd0g8rnzpn79acy92j032a5cbrcrch2y4jfx")))

(define-public crate-struct_field_names-0.2.0 (c (n "struct_field_names") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v9l54p4s2whxv152fn2qcj2vsnbdfq9jlvmmw1vb8fbnc8vv9sp")))

(define-public crate-struct_field_names-0.2.1 (c (n "struct_field_names") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12xckrkircwh9c0fwg5pg85yk7d4j7vk7j23q0hbry6pp6m44iaz")))

