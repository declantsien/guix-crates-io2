(define-module (crates-io st ru struct2vec_derive) #:use-module (crates-io))

(define-public crate-struct2vec_derive-0.1.0 (c (n "struct2vec_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "struct2vec") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0alc4i1cxbafggfrflz4hpa1hdd63db1qn5169nklw2jmq4pc19j") (y #t)))

(define-public crate-struct2vec_derive-0.1.1 (c (n "struct2vec_derive") (v "0.1.1") (d (list (d (n "lit") (r "^1.0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "struct2vec") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10v7m1qkxzd8g2qnx6zyn9r3gaasmpcfm0qhfah3vdmf68a22clz") (y #t)))

(define-public crate-struct2vec_derive-0.1.2 (c (n "struct2vec_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "struct2vec") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11k666pz3bhna7m6hyip4zvrg934mdyf0l0208xc1da3l5j841al") (y #t)))

(define-public crate-struct2vec_derive-0.1.3 (c (n "struct2vec_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "struct2vec") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0g1y2gnrvxkk47z66xil3wbxm0iwdr3n1ys9yvpb9dhd903ac9i4") (y #t)))

(define-public crate-struct2vec_derive-0.1.4 (c (n "struct2vec_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "struct2vec") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1psa39742w9dmbh5m84iwk28z48h4h27fklz1ghc5fd7gamy1ija") (y #t)))

(define-public crate-struct2vec_derive-0.1.5 (c (n "struct2vec_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "struct2vec") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1w9vppgnx5xgqncyv95z1qpxi7z8s9wj0ipr8rgkcfnsgj42i0pm") (y #t)))

(define-public crate-struct2vec_derive-0.1.6 (c (n "struct2vec_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "struct2vec") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zxdhn0w7s2jd51z7gpzsprjrzwcwlycw96gl18xwqavkw2bfchn") (y #t)))

(define-public crate-struct2vec_derive-0.1.7 (c (n "struct2vec_derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "struct2vec") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0g4aw8z5b9ccwxwgqv73jxb7qf40r5jzd3h79vxih65zqw2d9b4h") (y #t)))

(define-public crate-struct2vec_derive-0.1.8 (c (n "struct2vec_derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zsghp5hch32anfidyn6j7g7g8f1k4xvs170sa02dscccm7kl7qg")))

(define-public crate-struct2vec_derive-0.1.9 (c (n "struct2vec_derive") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "struct2vec") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "00prja7mscc2drays19ixqwc6f2ij891lrl4nbx1d2zr4ysqgagy") (y #t)))

