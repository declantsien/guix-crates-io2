(define-module (crates-io st ru structz) #:use-module (crates-io))

(define-public crate-structz-0.1.0 (c (n "structz") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "stringz") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (d #t) (k 2)))) (h "0ky4paa9jsy2m52386arngjjk4l4wqz365bvhs603pml8hdqj11w") (y #t)))

(define-public crate-structz-0.1.1 (c (n "structz") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "stringz") (r "^0.1.1") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (d #t) (k 2)))) (h "1fzvn86mzwmdala00zvl3s1zlf8gzflnh2c34ikm32kvdqpfc69d")))

(define-public crate-structz-0.1.2 (c (n "structz") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "stringz") (r "^0.1.2") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (d #t) (k 2)))) (h "0lyb2hn3w8x0cill9wriwxx540pyjx8lxc6cgdrd5bwhrmxmf0hd")))

(define-public crate-structz-0.2.0 (c (n "structz") (v "0.2.0") (d (list (d (n "stringz") (r "^0.1.2") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (d #t) (k 0)))) (h "06k9gpq5xyrslx81n7grqb80mlfkyq7m2skbn3s26l26rva672qd")))

(define-public crate-structz-0.2.1 (c (n "structz") (v "0.2.1") (d (list (d (n "stringz") (r "^0.1.2") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (d #t) (k 0)))) (h "0vz70jlvxvn3ycq6fdnlgicf5rfpv1wmdcgarhsvjbrccl62y3zk")))

(define-public crate-structz-0.2.2 (c (n "structz") (v "0.2.2") (d (list (d (n "stringz") (r "^0.1.2") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (d #t) (k 0)))) (h "1pg5n8xxrj1wca8h0b53vj3b9wdmw6nc600gif5iq5x7qkhbir4x")))

(define-public crate-structz-0.2.3 (c (n "structz") (v "0.2.3") (d (list (d (n "stringz") (r "^0.1.2") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (d #t) (k 0)))) (h "1wriz4s9nb7hycvi5r0pc05dra187yv2c4d8fdr2bacnxn7zd7ng")))

(define-public crate-structz-0.2.4 (c (n "structz") (v "0.2.4") (d (list (d (n "stringz") (r "^0.1.2") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (d #t) (k 0)))) (h "1v8rx3vzmbdjf2dipimv6ad8dqhpgzxy0li105clfcvaw2rjdbc7")))

(define-public crate-structz-0.2.5 (c (n "structz") (v "0.2.5") (d (list (d (n "stringz") (r "^0.1.2") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (d #t) (k 0)))) (h "1478iv25adm4hdxhdsiwm2jhb9qp0rca8qzgmxl9xcw5hsyllbxq")))

(define-public crate-structz-0.2.6 (c (n "structz") (v "0.2.6") (d (list (d (n "stringz") (r "^0.1.2") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.8.2") (d #t) (k 0)))) (h "1sixc586idraimyr72hd4ay6ysx1jmxnlwlc503c1xb6qxza2dv3")))

(define-public crate-structz-0.3.0 (c (n "structz") (v "0.3.0") (d (list (d (n "stringz") (r "^0.2.0") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.9.0") (d #t) (k 0)))) (h "0cnn7iwpcmy2qxyk59i340wp6fq8jzr5mi5w6b3bi28awrdajn6x")))

(define-public crate-structz-0.3.1 (c (n "structz") (v "0.3.1") (d (list (d (n "stringz") (r "^0.2.0") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.9.0") (d #t) (k 0)))) (h "1zanjvgzxzrf0ylwr5fcpbxg51r4qa33sgnb9v9kh09y4v050km1")))

(define-public crate-structz-0.3.2 (c (n "structz") (v "0.3.2") (d (list (d (n "stringz") (r "^0.2.0") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.9.0") (d #t) (k 0)))) (h "083gkjf29yhn7b75wmlbscd4v0za7vpyz5p4r5n2zg3dbkixafal")))

(define-public crate-structz-0.4.0 (c (n "structz") (v "0.4.0") (d (list (d (n "stringz") (r "^0.2.0") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.9.0") (d #t) (k 0)))) (h "17hv3zkrxc1p85gpaj4hw1pnhsz107rz1qs4afyjkl24l98rb934")))

(define-public crate-structz-0.5.0 (c (n "structz") (v "0.5.0") (d (list (d (n "stringz") (r ">=0.1.0") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r ">=0.9.0") (d #t) (k 0)))) (h "0jkgbdz7k3yl54axfq4hg04kncy6mhfa0v01x8i5rsrg0j4mci0z")))

(define-public crate-structz-0.5.1 (c (n "structz") (v "0.5.1") (d (list (d (n "stringz") (r ">=0.1.0") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r ">=0.10.0") (d #t) (k 0)))) (h "1vk9wpw7yjvnrj60d94inb3cz4h432w2drgfhxpa98gv6hql9411")))

(define-public crate-structz-0.5.2 (c (n "structz") (v "0.5.2") (d (list (d (n "stringz") (r ">=0.1.0") (d #t) (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r ">=0.10.0") (d #t) (k 0)))) (h "0a3wxfzn5h3aw79xb3fsfhb71kzl886dlbmdb7p0bxly5247mzqk")))

(define-public crate-structz-0.5.3 (c (n "structz") (v "0.5.3") (d (list (d (n "stringz") (r ">=0.3.1") (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r ">=0.14.11") (k 0)))) (h "1ynd2wszq762r445arc2zmli6d02kb3r2d93zsfr62w6md8dprnm") (f (quote (("std" "tuplez/std" "stringz/std") ("default") ("alloc" "tuplez/alloc" "stringz/alloc"))))))

(define-public crate-structz-0.5.4 (c (n "structz") (v "0.5.4") (d (list (d (n "stringz") (r ">=0.3.1") (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r ">=0.14.11") (k 0)))) (h "1cr5yh1sryb8ra0y7y2rh64v9ygkp60an665lnvcymdbh05wl5f6") (f (quote (("std" "tuplez/std" "stringz/std") ("default") ("alloc" "tuplez/alloc" "stringz/alloc"))))))

(define-public crate-structz-0.5.5 (c (n "structz") (v "0.5.5") (d (list (d (n "stringz") (r ">=0.3.1") (k 0)) (d (n "structz-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tuplez") (r ">=0.14.11") (k 0)))) (h "1r4l3mdp8a8nln0n68hvnqs093pfgh1siic17aglprjxpdgnh7cd") (f (quote (("std" "tuplez/std" "stringz/std") ("default") ("alloc" "tuplez/alloc" "stringz/alloc"))))))

(define-public crate-structz-0.5.6-alpha (c (n "structz") (v "0.5.6-alpha") (d (list (d (n "stringz") (r "^0.3.5-alpha") (k 0)) (d (n "structz-macros") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "tuplez") (r "=0.14.14-alpha") (k 2)))) (h "0bzd2fl1gjg2j38s0wk55nv5hcb0h3plhiww52kk5r46snmdpz67") (y #t)))

(define-public crate-structz-0.6.0 (c (n "structz") (v "0.6.0") (d (list (d (n "stringz") (r ">=0.4.0") (k 0)) (d (n "structz-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tuplez") (r "^0.14.14") (k 2)))) (h "0l2786vy7sz7m3r7n3r0hcb3h6zma6va86iwbmbg89sp8cg99jg8")))

