(define-module (crates-io st ru struct_fragment) #:use-module (crates-io))

(define-public crate-struct_fragment-0.0.1 (c (n "struct_fragment") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0c6wwsgf1ic8kkxjmymd8avs5yap38ym5axvv0d7lq3ga5sw2pjm")))

