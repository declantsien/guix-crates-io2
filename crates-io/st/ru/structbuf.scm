(define-module (crates-io st ru structbuf) #:use-module (crates-io))

(define-public crate-structbuf-0.1.0 (c (n "structbuf") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "1w51pi2k3fm8y1nrcbv7j3kah90bpyq0a38c395mp2g83cn3ggn7") (r "1.56")))

(define-public crate-structbuf-0.2.0 (c (n "structbuf") (v "0.2.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "151xmgv17b4sg614brc7vsd53bn656cyajjd96ajq07b9il1l9i6") (r "1.56")))

(define-public crate-structbuf-0.3.0 (c (n "structbuf") (v "0.3.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "1psrnqsylxl8l4gcphyvwldf0sqh70xrak3sbjrxmyvpygrkh58r") (r "1.56")))

(define-public crate-structbuf-0.3.1 (c (n "structbuf") (v "0.3.1") (d (list (d (n "smallvec") (r "^1") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "1r1b4ykjd8hh3834vs8rjza80jz1gd80qdcpjki5ja75a8bw2mil") (r "1.56")))

(define-public crate-structbuf-0.3.2 (c (n "structbuf") (v "0.3.2") (d (list (d (n "smallvec") (r "^1") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "0anipawdnmkaw1aqw8lwp01v7z3410m7qmxp1ns2c3chn5xivqm7") (r "1.56")))

(define-public crate-structbuf-0.3.3 (c (n "structbuf") (v "0.3.3") (d (list (d (n "smallvec") (r "^1") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "0c16v7zzn0y5a3w17h6fvlvdds42z4myfpvarif408qh4djjs81z") (r "1.56")))

(define-public crate-structbuf-0.3.4 (c (n "structbuf") (v "0.3.4") (d (list (d (n "smallvec") (r "^1") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "1dks00d6k72yjnlngjlk0jwzac12mkxv2ibg5xw66jfgg87dpwjz") (r "1.56")))

