(define-module (crates-io st ru strustle) #:use-module (crates-io))

(define-public crate-strustle-0.1.0 (c (n "strustle") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w3jcmqhpd2a93d9y8mx1j98698213wsqf5nbv3ds3zdadn648ws")))

