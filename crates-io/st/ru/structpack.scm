(define-module (crates-io st ru structpack) #:use-module (crates-io))

(define-public crate-structpack-0.5.0 (c (n "structpack") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "funty") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h3zdq1jji30kvf4alq9hd7b8syfgg2yh3c58h5112cw7qkmgs7j")))

(define-public crate-structpack-0.6.0 (c (n "structpack") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "funty") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zhwccvnw23zzwyd6n8xqm9vb82zdbf9k2di413lj0qfg2lj2z0p")))

(define-public crate-structpack-0.6.1 (c (n "structpack") (v "0.6.1") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "funty") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v32cm0hzqsjlx93mdz4jdzlzbr32l9qcyb6k4g03m2rpvrxlpyk")))

(define-public crate-structpack-0.7.0-beta.1 (c (n "structpack") (v "0.7.0-beta.1") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "funty") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0046yi76v2h73y21aijlz1m5x6snrb7ha1z1f908fakzx1cghz87")))

(define-public crate-structpack-0.7.0-beta.3 (c (n "structpack") (v "0.7.0-beta.3") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "funty") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dj6blxzh7a3az09crdw47lnap79jybsf7igb5l9x6al6d6ck635")))

(define-public crate-structpack-0.7.0-beta.5 (c (n "structpack") (v "0.7.0-beta.5") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "funty") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c6mrg946aj8hqdplzbgz68lwjinnmfsnnsfgc82zwm1g67nr1ks")))

(define-public crate-structpack-0.7.0 (c (n "structpack") (v "0.7.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "funty") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lxqsln2k4c4v0z3ahqh1yp31vp3xkwy0xlh38140vkhrfvsia7x")))

(define-public crate-structpack-1.0.0 (c (n "structpack") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "funty") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11bh440j7xblypgvnly9h81zfibmkjnw54vx55xisspm4qblw1z5")))

