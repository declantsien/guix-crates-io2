(define-module (crates-io st ru structure) #:use-module (crates-io))

(define-public crate-structure-0.1.0 (c (n "structure") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "structure-macro-impl") (r "^0.1.0") (d #t) (k 0)))) (h "0jggpizi9b4xsng8502kvygfy9xamnbb1f52rsg1jhivd0k0qpv6")))

(define-public crate-structure-0.1.1 (c (n "structure") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "structure-macro-impl") (r "^0.1.1") (d #t) (k 0)))) (h "0s0id30zj12awni54jmldbjrkxgxkyzmvl365mn7p12i1gnba9ba")))

(define-public crate-structure-0.1.2 (c (n "structure") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "structure-macro-impl") (r "^0.1.2") (d #t) (k 0)))) (h "0ngss4aylxg0pjwj8x5pv159hvh92ldikn8lic0mp4zqkkmrldx8")))

