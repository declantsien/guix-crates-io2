(define-module (crates-io st ru struct_iterable_derive) #:use-module (crates-io))

(define-public crate-struct_iterable_derive-0.1.0 (c (n "struct_iterable_derive") (v "0.1.0") (d (list (d (n "erased-serde") (r "^0.3.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "struct_iterable_internal") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "0b36g1lmva2jg5lssyylp4nyp7bqrjsgdwhjs3ls8gm4i373kfcb")))

