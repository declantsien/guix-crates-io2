(define-module (crates-io st ru struqture-py-macros) #:use-module (crates-io))

(define-public crate-struqture-py-macros-1.0.0-beta.1 (c (n "struqture-py-macros") (v "1.0.0-beta.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1akzjci93bqfgy95vjm935572gg25psg4nvyziwvwbd52jlcvyyl") (r "1.57")))

(define-public crate-struqture-py-macros-1.0.0-beta.2 (c (n "struqture-py-macros") (v "1.0.0-beta.2") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1hzx1188lxfdfrlbym6hz8kxp4hn0hm028v1am9d4r6h54q58m4m") (r "1.57")))

(define-public crate-struqture-py-macros-1.0.0-beta.3 (c (n "struqture-py-macros") (v "1.0.0-beta.3") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1v9wrra4ahpsp6s1msk3asyfxjvmmk264ab4qcza77a28bz84phz") (r "1.57")))

(define-public crate-struqture-py-macros-1.0.0-beta.4 (c (n "struqture-py-macros") (v "1.0.0-beta.4") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "08msb0xv4ar1i26wpy8vgrvj2gs7vhgnvi066bd1vf8s1yzllzrn") (r "1.57")))

(define-public crate-struqture-py-macros-1.0.0-beta.5 (c (n "struqture-py-macros") (v "1.0.0-beta.5") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "15y60g8vki9w0xb95lvspjayr1zgqm2fmrcl5g1xqshvmyzf1lhz") (r "1.57")))

(define-public crate-struqture-py-macros-1.0.0 (c (n "struqture-py-macros") (v "1.0.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "139r1k4kqxbd13f33lk1x0f9rgrdnjgz4g0jb5gzwc4d4fwpr8gn") (r "1.57")))

(define-public crate-struqture-py-macros-1.0.1 (c (n "struqture-py-macros") (v "1.0.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1nqgcrcfzpvj5ngksxvpca6nq98gcrcjnsggk24xd0ynlh4si938") (r "1.57")))

(define-public crate-struqture-py-macros-1.1.0 (c (n "struqture-py-macros") (v "1.1.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1lb4pvxwnfsn5pd9l4r7xy7k8r8msin5cj5v70l4qp92qw4gwnwy") (r "1.57")))

(define-public crate-struqture-py-macros-1.1.1 (c (n "struqture-py-macros") (v "1.1.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0fqdgpldf7kh99bg1mx9pcb4v88dn0hy373daixjfgxc43vzgi3h") (r "1.57")))

(define-public crate-struqture-py-macros-1.2.0 (c (n "struqture-py-macros") (v "1.2.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0n3bnlr86hj944bf2cj4kgmmx7digqdj4w03ampzcsrxn8mzcwb4") (r "1.57")))

(define-public crate-struqture-py-macros-1.3.0 (c (n "struqture-py-macros") (v "1.3.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1fkmiswm8fvvvmiqrxw9mqwk6vam45kl5j582cydq9lg3c0jldmd") (r "1.57")))

(define-public crate-struqture-py-macros-1.3.1 (c (n "struqture-py-macros") (v "1.3.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0p0g6rsa6cwgrpswnngy5aw98djm533vq8phcf795kl6fjbv2wx6") (r "1.57")))

(define-public crate-struqture-py-macros-1.4.0-alpha.2 (c (n "struqture-py-macros") (v "1.4.0-alpha.2") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0s3rvgqq8m8h565nnlvicqfaarm0hrxhqkwyp5rryih3866vaqjf") (r "1.57")))

(define-public crate-struqture-py-macros-1.4.0-alpha.3 (c (n "struqture-py-macros") (v "1.4.0-alpha.3") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1y7qwi8bqkz3hn4dqlw3vygaqw8gxzd4c55d25xsxadykyblnvdj") (r "1.57")))

(define-public crate-struqture-py-macros-1.4.0-alpha.4 (c (n "struqture-py-macros") (v "1.4.0-alpha.4") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1dsf3l9vm3jzajnvk6w20wlfpy6k8kk4xf5h2qp61ll75a2c74ah") (r "1.57")))

(define-public crate-struqture-py-macros-1.4.0 (c (n "struqture-py-macros") (v "1.4.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1vwhj4kql6yfqgn5n58hn1cg44b13p5rckcax1r4zm584g8lklv8") (r "1.57")))

(define-public crate-struqture-py-macros-1.4.1 (c (n "struqture-py-macros") (v "1.4.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1hksz29pxxf1nsnf6ir3b8k95zifqj7kivlg3ywjl2dyjhy952l6") (r "1.57")))

(define-public crate-struqture-py-macros-1.5.0 (c (n "struqture-py-macros") (v "1.5.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "169rvr4sy2jkxdfm5y1lflsgc4wnl97zgzgidzcgm366m8hyprin") (r "1.57")))

(define-public crate-struqture-py-macros-1.5.1 (c (n "struqture-py-macros") (v "1.5.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0ayvf80p6qixyzryj6c33m7ln38hcfn3k61msxsh5pyfhh1dh3ry") (r "1.57")))

(define-public crate-struqture-py-macros-1.5.2 (c (n "struqture-py-macros") (v "1.5.2") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0xkm3mvsy73wyk27zs3x44cg7lifvc8xz6m385pm6fb6l59vnflb") (r "1.57")))

(define-public crate-struqture-py-macros-1.6.0 (c (n "struqture-py-macros") (v "1.6.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0yaccijqj71qrd4i99lrn1w1gcfjcbw9xd9hp9gyb54ys62vwx4j") (r "1.57")))

(define-public crate-struqture-py-macros-1.6.1 (c (n "struqture-py-macros") (v "1.6.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "13plm0jnk5ljmn1d7kd94ckswrfy77yyariigf2b8xddg59zf59j") (r "1.57")))

(define-public crate-struqture-py-macros-2.0.0-alpha.0 (c (n "struqture-py-macros") (v "2.0.0-alpha.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0x89gyac1rvmmmjf8s23sqj6czcdjfwmnhlllkqpzimdwjfh4hip") (r "1.57")))

(define-public crate-struqture-py-macros-1.7.0 (c (n "struqture-py-macros") (v "1.7.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0wsfdpm58963c5anzlrwq8hcs36rd74bjcdazf1pqgncanbvqz2p") (r "1.57")))

(define-public crate-struqture-py-macros-1.7.1 (c (n "struqture-py-macros") (v "1.7.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1qq2vkky26qchpv56kl9si83sqnklxbg12m95k1dw8lpgyhrvs8f") (r "1.57")))

(define-public crate-struqture-py-macros-1.6.2 (c (n "struqture-py-macros") (v "1.6.2") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0cyb2i3r871i9finc1xlpamnii4ak5nb8fnicsks9czk204m0dqm") (r "1.57")))

(define-public crate-struqture-py-macros-1.5.3 (c (n "struqture-py-macros") (v "1.5.3") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "18sy3afrdz2w781c02r8nf9yg7mk81y0wnfpxbfvcrf5c20cjszv") (r "1.57")))

