(define-module (crates-io st ru structs-from-excel) #:use-module (crates-io))

(define-public crate-structs-from-excel-0.1.0 (c (n "structs-from-excel") (v "0.1.0") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)))) (h "12p1q0n4qfq6a1m2m4ylqzqc5b22nkk2bkwz1l88z7rhh6vw6w4i")))

(define-public crate-structs-from-excel-0.2.0 (c (n "structs-from-excel") (v "0.2.0") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)))) (h "0xphm6zblcdg29584h84szxj28q0gd44s8fq3vk3vznia30rqipc")))

(define-public crate-structs-from-excel-0.2.1 (c (n "structs-from-excel") (v "0.2.1") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)))) (h "1nb585r8vxdlkl56qh0b2wgk5q0vpxdl59nn4nim9xr3a0brxg9q")))

(define-public crate-structs-from-excel-0.2.2 (c (n "structs-from-excel") (v "0.2.2") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)))) (h "06pb3xxqs2cngzyqigf0aqjg06csldq5ddndc1rxd24n3cdanp50")))

