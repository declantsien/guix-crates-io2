(define-module (crates-io st ru structmap) #:use-module (crates-io))

(define-public crate-structmap-0.1.0 (c (n "structmap") (v "0.1.0") (d (list (d (n "structmap-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0kb58vkjfnz6g5jyfrh15hfvp5i5br7cv88bmv4p7j0hhfblrs74")))

(define-public crate-structmap-0.1.1 (c (n "structmap") (v "0.1.1") (d (list (d (n "structmap-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1v65mlji63q3qgg2kvgbqbp4vrvcy121gc52qgznzl4g59q0kgil")))

(define-public crate-structmap-0.1.2 (c (n "structmap") (v "0.1.2") (d (list (d (n "structmap-derive") (r "^0.1.0") (d #t) (k 0)))) (h "02x0v51grz9v84xdvw395w8s5d5q22dbdsm84cdxaam3rh72id0a")))

(define-public crate-structmap-0.1.3 (c (n "structmap") (v "0.1.3") (d (list (d (n "structmap-derive") (r "^0.1.3") (d #t) (k 0)))) (h "0l1l1hkrgsnr2hyl6bliad1gfwkv2a2k3anpjyq8cnd5ic2ym9hb")))

(define-public crate-structmap-0.1.4 (c (n "structmap") (v "0.1.4") (d (list (d (n "structmap-derive") (r "^0.1.4") (d #t) (k 0)))) (h "1ml0a5ca14yplgppc83wpw3yzjfzvfzlabmvkv85mabpbyq2lw23")))

(define-public crate-structmap-0.1.5 (c (n "structmap") (v "0.1.5") (d (list (d (n "structmap-derive") (r "^0.1.5") (d #t) (k 0)))) (h "18qbcnrfplps6hf82n6rp6hlhxb0ppqdbj6czsvx7bz6rd0f4k80")))

(define-public crate-structmap-0.1.6 (c (n "structmap") (v "0.1.6") (d (list (d (n "structmap-derive") (r "^0.1.5") (d #t) (k 0)))) (h "128dd2sq6vqvch50vnjlmf0xj1ykkf0rc8an4sbgqvnfgb278136")))

