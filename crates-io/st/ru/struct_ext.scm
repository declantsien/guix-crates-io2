(define-module (crates-io st ru struct_ext) #:use-module (crates-io))

(define-public crate-struct_ext-0.1.0 (c (n "struct_ext") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "108gh8pys5dgnah2svn1crvyj46xh5bcprpc9klhd8yrfwhrkab9")))

(define-public crate-struct_ext-0.1.1 (c (n "struct_ext") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0dnaxq6gz2lvzwq0lvp41dvz20mjm74s0fv8r81b3mdzhww6ihxh")))

