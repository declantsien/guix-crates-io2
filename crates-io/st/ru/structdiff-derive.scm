(define-module (crates-io st ru structdiff-derive) #:use-module (crates-io))

(define-public crate-structdiff-derive-0.1.0 (c (n "structdiff-derive") (v "0.1.0") (h "1d8a5lyk6p4grr53f19zbs70m9phy9qm2alg19rrl3dgf5k5swgb")))

(define-public crate-structdiff-derive-0.2.0 (c (n "structdiff-derive") (v "0.2.0") (d (list (d (n "nanoserde") (r "^0.1.30") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0lhsxsdg727jfsdsd0r21p5g57ivan7zxrg4w1ipwcnksjdzic5l") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.3.0 (c (n "structdiff-derive") (v "0.3.0") (d (list (d (n "nanoserde") (r "^0.1.30") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p181iizvr6wbvkarbhsvz0c0563fzg5ih7r749g1jpwa1bgfz9x") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.4.0 (c (n "structdiff-derive") (v "0.4.0") (d (list (d (n "nanoserde") (r "^0.1.30") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vr6y00facrki7xqf6fb7msdla7rgj71nl4s2l5xvn6rmcijygd4") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.4.1 (c (n "structdiff-derive") (v "0.4.1") (d (list (d (n "nanoserde") (r "^0.1.30") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1m36sl4v8n8pd6h3702rh5vfzamvbrg3pw2jf098baxg8wgy34px") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.5.0 (c (n "structdiff-derive") (v "0.5.0") (d (list (d (n "nanoserde") (r "^0.1.30") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "059w7z0dl8i64n359gi3a5m4p2asicsccw3vkpr9j6j9dp5ssdfa") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.5.1 (c (n "structdiff-derive") (v "0.5.1") (d (list (d (n "nanoserde") (r "^0.1.30") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1iypwyg9i07nafwiqlb28gvs3ib5qgnk8l0cr74669fz2qz6icxc") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.5.2 (c (n "structdiff-derive") (v "0.5.2") (d (list (d (n "nanoserde") (r "^0.1.30") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1myldsziqnjh936ayqic94nikikpma66f61pr8pvn1xm93vy8njh") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.5.3 (c (n "structdiff-derive") (v "0.5.3") (d (list (d (n "nanoserde") (r "^0.1.30") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07mq9rk79hqn03bi1nisahad6k71pdaba5p113ih1cq1p78k9183") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.5.4 (c (n "structdiff-derive") (v "0.5.4") (d (list (d (n "nanoserde") (r "^0.1.30") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kxvyw9klgb0gmg592djsr5gn4fapqvfkbjp6p0ykab0xzkczvf4") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.5.5 (c (n "structdiff-derive") (v "0.5.5") (d (list (d (n "nanoserde") (r "^0.1.32") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gzfcf9v3zihrzjfh89bl55km47vq2mi9j1vqv20bq3fqbqd8vgw") (f (quote (("default") ("debug_diffs")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.5.6 (c (n "structdiff-derive") (v "0.5.6") (d (list (d (n "nanoserde") (r "^0.1.32") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pcswzym34rj2sc04pddw1a2srn5anp88krrccc7f9wrm4q735ak") (f (quote (("default") ("debug_diffs")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.5.7 (c (n "structdiff-derive") (v "0.5.7") (d (list (d (n "nanoserde") (r "^0.1.32") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1yapa63adh6hv42nw1w5yf3v7f8yvwvhr0fjzyjh81ibz46pii4c") (f (quote (("generated_setters") ("default") ("debug_diffs")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.5.8 (c (n "structdiff-derive") (v "0.5.8") (d (list (d (n "nanoserde") (r "^0.1.32") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17m54zhc0pxbvv5qm6mprb1b46769h141g6hgyddkzi72cmrnrmk") (f (quote (("generated_setters") ("default") ("debug_diffs")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.5.9 (c (n "structdiff-derive") (v "0.5.9") (d (list (d (n "nanoserde") (r "^0.1.32") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rkzb0vfgabnmm2p1mwwf9c3a3ylimsxk15sm6avyh2ydl0w41fv") (f (quote (("generated_setters") ("default") ("debug_diffs")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.6.0 (c (n "structdiff-derive") (v "0.6.0") (d (list (d (n "nanoserde") (r "^0.1.37") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qgalm0ka5yxns6rnl3nd0ln771cy2mvrxr1crgzcc1fj80zrsh3") (f (quote (("generated_setters") ("default") ("debug_diffs")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.6.1 (c (n "structdiff-derive") (v "0.6.1") (d (list (d (n "nanoserde") (r "^0.1.37") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1sjcw6f2x4irdpl5yrr86dyblm3ymxcv7ciyplxj086ksxalhyg7") (f (quote (("generated_setters") ("default") ("debug_diffs")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.6.2 (c (n "structdiff-derive") (v "0.6.2") (d (list (d (n "nanoserde") (r "^0.1.37") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1js6qxlr3c0b10bi82gaikqynf3w2iws94q3znpm87ba2bz7vd4f") (f (quote (("generated_setters") ("default") ("debug_diffs")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.6.3 (c (n "structdiff-derive") (v "0.6.3") (d (list (d (n "nanoserde") (r "^0.1.37") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "151jnzshyk1lj8p03np4v5w4yyj48ip4yr3ykdwc9vn35vxcj75b") (f (quote (("generated_setters") ("default") ("debug_diffs")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.7.0 (c (n "structdiff-derive") (v "0.7.0") (d (list (d (n "nanoserde") (r "^0.1.37") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17ff1m3x1fawdjggwm9h46cx2zphp0pglpqbmws775apba6fdy6s") (f (quote (("generated_setters") ("default") ("debug_diffs")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

(define-public crate-structdiff-derive-0.7.1 (c (n "structdiff-derive") (v "0.7.1") (d (list (d (n "nanoserde") (r "^0.1.37") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cqxi50issjp9mz13y7xxmr16dnhilzblqh9kh43sgaa5c1m14li") (f (quote (("generated_setters") ("default") ("debug_diffs")))) (s 2) (e (quote (("serde" "dep:serde") ("nanoserde" "dep:nanoserde"))))))

