(define-module (crates-io st ru struct-diff) #:use-module (crates-io))

(define-public crate-struct-diff-0.1.0 (c (n "struct-diff") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "15hb6dcxy1rblzz42yv0faiwvs2ydl5dkbhka7linkhzy70ca3sd")))

(define-public crate-struct-diff-0.2.0 (c (n "struct-diff") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1i81h4glz8cnxg5zf25rjw7ab4ivaa16d2r4ayy7f1f2zs0bkkz4")))

(define-public crate-struct-diff-0.2.1 (c (n "struct-diff") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1g8idskyj4w746mzb4hmw1dx8a5dgc4r3r42aflcq0mxkxq24q6z")))

(define-public crate-struct-diff-0.2.2 (c (n "struct-diff") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1mafijxng6hzmas7lgxzikivblvff45glqbgbqhkfhy4nxnmz5zj")))

(define-public crate-struct-diff-0.2.3 (c (n "struct-diff") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ymp6v5d3lfc3bfpr87zixlrmsvcq05r7z416x8am9dxbjbma106")))

