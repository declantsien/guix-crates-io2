(define-module (crates-io st ru structview) #:use-module (crates-io))

(define-public crate-structview-0.1.0 (c (n "structview") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "structview_derive") (r "^0.1") (d #t) (k 0)))) (h "12r516vy5xdvqa7nnnjsn7ppzi9ncmgywlxb5zc8gpqjnnkzfrga")))

(define-public crate-structview-0.2.0 (c (n "structview") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "structview_derive") (r "^0.2") (d #t) (k 0)))) (h "180irc1pgfpwk9fdk6imdsyk6csgd7c07p0wjjny7lg11sr0jj8h")))

(define-public crate-structview-0.3.0 (c (n "structview") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "structview_derive") (r "^0.3") (d #t) (k 0)))) (h "1fywmm7dk3gmjgsrcnh6w4mr6w4lmbl8ki3hg2r4yw0vak29xazz") (f (quote (("std") ("default" "std"))))))

(define-public crate-structview-0.3.1 (c (n "structview") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "compiletest_rs") (r "= 0.3.22") (f (quote ("stable"))) (d #t) (k 2)) (d (n "structview_derive") (r "^0.3") (d #t) (k 0)))) (h "0lp7dl4v34qxb15w4r88airw5vbk6c637a04wgjw772sjv08l441") (f (quote (("std") ("default" "std"))))))

(define-public crate-structview-1.0.0 (c (n "structview") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "compiletest_rs") (r "^0.4") (d #t) (k 2)) (d (n "structview_derive") (r "= 1.0.0") (d #t) (k 0)))) (h "0ph2jd6m43vrjjqcy8388s5hf8haii1gnl15vjwr4d63fqj43xhd") (f (quote (("std") ("default" "std"))))))

(define-public crate-structview-1.1.0 (c (n "structview") (v "1.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "compiletest_rs") (r "^0.5") (d #t) (k 2)) (d (n "structview_derive") (r "=1.1.0") (d #t) (k 0)))) (h "10i1kjwn1d726zdga07hpwxkvysiw4q3x2z9inkphw9i25kmj3cc") (f (quote (("std") ("default" "std"))))))

