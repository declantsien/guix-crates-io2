(define-module (crates-io st ru struct_auto_from) #:use-module (crates-io))

(define-public crate-struct_auto_from-0.1.0 (c (n "struct_auto_from") (v "0.1.0") (d (list (d (n "fake") (r "^2.6.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l3apgkssbb96dahwgmv5li8fdrv9cnpvxbmi17c2n6rb9qiy83y")))

(define-public crate-struct_auto_from-0.2.0 (c (n "struct_auto_from") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "fake") (r "^2.6.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i2k0dfn9865z6a65050pwg4pmi6yyb4a1zdh8ja2qvswv9i0j3q")))

