(define-module (crates-io st ru struct_mapping) #:use-module (crates-io))

(define-public crate-struct_mapping-1.0.0 (c (n "struct_mapping") (v "1.0.0") (h "0fwhpmghcdpm34mmxpq9hw4mi4iy8x42d3ak9qljpxzkm9j0gjpq") (f (quote (("default")))) (y #t)))

(define-public crate-struct_mapping-1.0.1 (c (n "struct_mapping") (v "1.0.1") (d (list (d (n "struct_mapping_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "struct_mapping_derive") (r "^1.0") (d #t) (k 2)))) (h "1dn6wib93gqaa9dfa2zr2kazf2477f8gwx0yj36jighjda8fgm3g") (f (quote (("derive" "struct_mapping_derive") ("default"))))))

