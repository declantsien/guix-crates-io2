(define-module (crates-io st ru struct2vec) #:use-module (crates-io))

(define-public crate-struct2vec-0.1.0 (c (n "struct2vec") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1glj9mxwy9xfmxv9q6mhpfi51zc18jysyd8lfkk6gc3cw8h4ni3k") (y #t)))

(define-public crate-struct2vec-0.1.1 (c (n "struct2vec") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0gackwsawyxs503qf9ngrmjwh1c9wcd1r0vfnk0qqbz4m4943zca") (y #t)))

(define-public crate-struct2vec-0.1.2 (c (n "struct2vec") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "struct2vec_derive") (r "^0.1.7") (d #t) (k 0)))) (h "1fvpwnd2bwggzlgpiv29dprbc0yamq99d6mx2l8i46pjmlrmbizm") (f (quote (("struct2vec_derive")))) (y #t)))

(define-public crate-struct2vec-0.1.3 (c (n "struct2vec") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "struct2vec_derive") (r "^0.1") (d #t) (k 0)))) (h "01b5sl0s0y3zwjl770j1nb2sr0csi09xbwd03v880lpymrbdpmgy") (f (quote (("struct2vec_derive")))) (y #t)))

(define-public crate-struct2vec-0.1.4 (c (n "struct2vec") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "struct2vec_derive") (r "^0.1") (d #t) (k 0)))) (h "0k6kpbswizhpnyghsk080bs88bdpy8izprvvdi0wz19800nq7kln") (f (quote (("struct2vec_derive"))))))

