(define-module (crates-io st ru structdump-derive) #:use-module (crates-io))

(define-public crate-structdump-derive-0.1.0 (c (n "structdump-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "structdump") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0nz5zr4w9rynfvynai66lq3vn4aiqsch0y9vpszlmff9cwdb7gpp")))

(define-public crate-structdump-derive-0.1.1 (c (n "structdump-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "structdump") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "16rcq3ib16vldznwmp2mksj91vs6jdpl8g88ca8vpk7k5n46a8xa")))

(define-public crate-structdump-derive-0.1.2 (c (n "structdump-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "120q1jwzlpkh16zbghvm54saz9340l4z0an6zk5jrq3pq3ykghq6")))

(define-public crate-structdump-derive-0.2.0 (c (n "structdump-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0a6zmq4nvbx9ibg4mljzaww32v3agvisga89vb71n7x1rxchpk19")))

