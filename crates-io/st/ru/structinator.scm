(define-module (crates-io st ru structinator) #:use-module (crates-io))

(define-public crate-structinator-0.1.1 (c (n "structinator") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structinator_traits") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09nxi2zf90lf8b02kbd6fkblzanzi6idk1kgzbdv4hym584xbmkj")))

