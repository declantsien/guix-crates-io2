(define-module (crates-io st ru structdoc) #:use-module (crates-io))

(define-public crate-structdoc-0.1.0 (c (n "structdoc") (v "0.1.0") (d (list (d (n "bitflags") (r "~1") (d #t) (k 0)) (d (n "itertools") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 2)) (d (n "serde_derive") (r "~1") (d #t) (k 2)) (d (n "structdoc-derive") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "~0.6") (d #t) (k 2)))) (h "0fv3dgj19c482f9yspd9cj8w6v4dil7b1z9vqkf1b99x2xni2dj1") (f (quote (("default" "structdoc-derive"))))))

(define-public crate-structdoc-0.1.1 (c (n "structdoc") (v "0.1.1") (d (list (d (n "bitflags") (r "~1") (d #t) (k 0)) (d (n "itertools") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 2)) (d (n "serde_derive") (r "~1") (d #t) (k 2)) (d (n "structdoc-derive") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "~0.6") (d #t) (k 2)))) (h "1fafrshhgb95wi3pd39znjsc1jsb6118j72a9blnd5ps8qycrgbn") (f (quote (("default" "structdoc-derive"))))))

(define-public crate-structdoc-0.1.2 (c (n "structdoc") (v "0.1.2") (d (list (d (n "bitflags") (r "~1") (d #t) (k 0)) (d (n "itertools") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 2)) (d (n "serde_derive") (r "~1") (d #t) (k 2)) (d (n "structdoc-derive") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "~0.6") (d #t) (k 2)))) (h "1d8mm9zbaq1bfhxh29vnjnc7l1ypxsyad96fl41d20dcjiifcg73") (f (quote (("default" "structdoc-derive"))))))

(define-public crate-structdoc-0.1.3 (c (n "structdoc") (v "0.1.3") (d (list (d (n "bitflags") (r "~1") (d #t) (k 0)) (d (n "itertools") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 2)) (d (n "serde_derive") (r "~1") (d #t) (k 2)) (d (n "structdoc-derive") (r "~0.1.3") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "~0.6") (d #t) (k 2)))) (h "16w71baqm1ap77v5iv7ciif3vny2acd0dyg8778i64330lswm97b") (f (quote (("default" "structdoc-derive"))))))

(define-public crate-structdoc-0.1.4 (c (n "structdoc") (v "0.1.4") (d (list (d (n "bitflags") (r "~1") (d #t) (k 0)) (d (n "itertools") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 2)) (d (n "serde_derive") (r "~1") (d #t) (k 2)) (d (n "structdoc-derive") (r "~0.1.4") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "04bzjwlg8cxfbqgmg2i3s5y0lgmcsdj173byix2sa3dlf6955n4g") (f (quote (("default" "structdoc-derive"))))))

