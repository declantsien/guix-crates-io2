(define-module (crates-io st ru structure_test) #:use-module (crates-io))

(define-public crate-structure_test-0.1.0 (c (n "structure_test") (v "0.1.0") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)))) (h "0p971jd9fb2nvv780wfqzs0z5b80dq1aa1jakwr1msy78xvkc4wa")))

(define-public crate-structure_test-0.1.1 (c (n "structure_test") (v "0.1.1") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)))) (h "1dvd09kr0q367jra1h9ydc7psf69bcm9nvs98pghlxbw6nzl63d1")))

