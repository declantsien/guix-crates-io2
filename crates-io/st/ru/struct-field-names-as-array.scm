(define-module (crates-io st ru struct-field-names-as-array) #:use-module (crates-io))

(define-public crate-struct-field-names-as-array-0.1.0 (c (n "struct-field-names-as-array") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("derive" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0pa2fdjpzcjadm0h6lz9xfds9krdzaxs4v300b35fy1i2iq2vk87")))

(define-public crate-struct-field-names-as-array-0.1.1 (c (n "struct-field-names-as-array") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("derive" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1zl4w5lqpvrij22c8903x7jmcjacznnj7q92yb35xldyqqdglpqm")))

(define-public crate-struct-field-names-as-array-0.1.2 (c (n "struct-field-names-as-array") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("derive" "printing" "extra-traits"))) (d #t) (k 0)))) (h "152pv0bb0awaaidg17ll9nrwnl4iic6rdv1kk2c1j408zwln6w5a")))

(define-public crate-struct-field-names-as-array-0.1.3 (c (n "struct-field-names-as-array") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("derive" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1c724q5bzyfvdxw8zghsjha5wk60wwpncgsyzqr2wbbz7x3d20zp")))

(define-public crate-struct-field-names-as-array-0.1.4 (c (n "struct-field-names-as-array") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("derive" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1zpi62i3himlmi2xcxri7id2cc69sm5i7q86lgxzq7llyf0mp6lz")))

(define-public crate-struct-field-names-as-array-0.2.0 (c (n "struct-field-names-as-array") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("derive" "printing" "extra-traits"))) (d #t) (k 0)))) (h "09w6kalfwadg14aj8xg0vnca0lbakdj9lfjw59py1kim9crjskz1")))

(define-public crate-struct-field-names-as-array-0.3.0 (c (n "struct-field-names-as-array") (v "0.3.0") (d (list (d (n "struct-field-names-as-array-derive") (r "=0.3.0") (o #t) (d #t) (k 0)))) (h "1nadcwvb6syhk4z5hrv3vsn17vyjain660zlqj9ck70zfyp4pfi2") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:struct-field-names-as-array-derive"))))))

