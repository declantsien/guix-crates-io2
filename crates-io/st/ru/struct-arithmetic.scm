(define-module (crates-io st ru struct-arithmetic) #:use-module (crates-io))

(define-public crate-struct-arithmetic-0.1.0 (c (n "struct-arithmetic") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "004fvfsb73dwiipjz1cnl0i03in43ikcm7z7hy2y6pvn5byc6g3p")))

(define-public crate-struct-arithmetic-0.1.2 (c (n "struct-arithmetic") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0aby6djls3sxrx9j2a17lf6hvx6g8p0hi6kylnx4ijr1vxnfgap4")))

(define-public crate-struct-arithmetic-0.2.0 (c (n "struct-arithmetic") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01smnwshzqvvmvbf0l4xm3305kqqzwzwmvzfsfg53kxnq276rqla")))

(define-public crate-struct-arithmetic-0.2.1 (c (n "struct-arithmetic") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a2i60yygkv2lr8k2ddj73pak5wl5gknz5wfazv38z8xzsi155np")))

(define-public crate-struct-arithmetic-0.2.2 (c (n "struct-arithmetic") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11ggp2hdlbj5z8clxapxyyp2cq7wk3cqvv597jr3f58yhnxlcy9n")))

(define-public crate-struct-arithmetic-0.3.0 (c (n "struct-arithmetic") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "029ckjamq5g5dslxf20khaf4xn3ss5jzagg4lm82ikfxh46nwpbq")))

(define-public crate-struct-arithmetic-0.3.1 (c (n "struct-arithmetic") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h7qxhrd80ywzjyra9capfhryf8spyvm1jpvz0yykiagvrg4cjfr")))

(define-public crate-struct-arithmetic-0.3.2 (c (n "struct-arithmetic") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gwhwrak2iy5shmhml6wkz6p7pkh1sqdg507m6blb4k0apji1ljl") (y #t)))

(define-public crate-struct-arithmetic-0.3.3 (c (n "struct-arithmetic") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xn1jqiqa9s3488lzjfyrnishpmgws29gkdw60ab6kynjcrjk4f6")))

