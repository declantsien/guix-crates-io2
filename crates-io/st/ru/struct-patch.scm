(define-module (crates-io st ru struct-patch) #:use-module (crates-io))

(define-public crate-struct-patch-0.1.0 (c (n "struct-patch") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.1") (d #t) (k 0)) (d (n "struct-patch-trait") (r "=0.1") (d #t) (k 0)))) (h "0jgi5xhd3l649kr0vhq47i08falm5kaj0jjli2gj7w3ls4gydmz0") (y #t)))

(define-public crate-struct-patch-0.1.1 (c (n "struct-patch") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.1.1") (d #t) (k 0)) (d (n "struct-patch-trait") (r "=0.1.1") (d #t) (k 0)))) (h "1ljryrgkwng38xgm9j94l8rw099x7g35ick4y9h34jmr1hjiac46")))

(define-public crate-struct-patch-0.1.2 (c (n "struct-patch") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.1.2") (d #t) (k 0)) (d (n "struct-patch-trait") (r "=0.1.2") (d #t) (k 0)))) (h "1hka8sar0jgq4wjh7m5vdnzqrrlqaq068mab41zwrls6isf3rpwr")))

(define-public crate-struct-patch-0.1.3 (c (n "struct-patch") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.1.3") (d #t) (k 0)) (d (n "struct-patch-trait") (r "=0.1.3") (d #t) (k 0)))) (h "0556pw1ag0fy6x53nyc4xnl4p9k79s1lnm04dsmi9d9k4pc791wb") (y #t)))

(define-public crate-struct-patch-0.1.4 (c (n "struct-patch") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.1.4") (d #t) (k 0)) (d (n "struct-patch-trait") (r "=0.1.4") (d #t) (k 0)))) (h "0ralv3rsapg8zci6d46j63irjm1kpx524d5s976mczfh8c5xx8mv") (f (quote (("status" "struct-patch-trait/status" "struct-patch-derive/status"))))))

(define-public crate-struct-patch-0.1.5 (c (n "struct-patch") (v "0.1.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.1.5") (d #t) (k 0)) (d (n "struct-patch-trait") (r "=0.1.5") (d #t) (k 0)))) (h "1hnqz01msavpp49rm89q5k97pr65i17lf3yq870whxmb01lh1niy") (f (quote (("status" "struct-patch-trait/status" "struct-patch-derive/status"))))))

(define-public crate-struct-patch-0.2.0 (c (n "struct-patch") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.2.0") (d #t) (k 0)) (d (n "struct-patch-trait") (r "=0.2.0") (d #t) (k 0)))) (h "040xlhzwkxd7zilm1abd7gv6lcwpmh5i3hqr86dl7bv0hdqw6h0a") (f (quote (("status" "struct-patch-trait/status" "struct-patch-derive/status"))))))

(define-public crate-struct-patch-0.2.1 (c (n "struct-patch") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.2.1") (d #t) (k 0)))) (h "05mfccnz4xxsnpsqnwiskhcr2kjnawcccjnkal0x2kdhgb8ql7ic") (f (quote (("status" "struct-patch-derive/status") ("default" "status")))) (y #t)))

(define-public crate-struct-patch-0.2.2 (c (n "struct-patch") (v "0.2.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.2.2") (d #t) (k 0)))) (h "129h9hykpfrchksxi66k73s5a8gksjc1l8d5sl1kcm57nprphfcz") (f (quote (("status" "struct-patch-derive/status") ("default" "status"))))))

(define-public crate-struct-patch-0.2.3 (c (n "struct-patch") (v "0.2.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.2.3") (d #t) (k 0)))) (h "1d17j0kp720a2a5q0g3h3g0mbhazml18lnbd5zlaa3ilf5r5vhp0") (f (quote (("status" "struct-patch-derive/status") ("default" "status"))))))

(define-public crate-struct-patch-0.3.0 (c (n "struct-patch") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.3.0") (d #t) (k 0)))) (h "1avddhhcsz0kndqx1li2knnv3dj2w8nn3v6z1kiacwbpcdky6icg") (f (quote (("status" "struct-patch-derive/status") ("default" "status"))))))

(define-public crate-struct-patch-0.3.1 (c (n "struct-patch") (v "0.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.3.1") (d #t) (k 0)))) (h "1mncrmir66vqccxvj75ysmc16icjhr3hwwbhsrnhf5f0xp01bgya") (f (quote (("status" "struct-patch-derive/status") ("default" "status"))))))

(define-public crate-struct-patch-0.3.2 (c (n "struct-patch") (v "0.3.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.3.2") (d #t) (k 0)))) (h "0s2wg8497mx7vzqdf907ad4a218gb8ixn46rlh1k0zjc0xlzvz8k") (f (quote (("status" "struct-patch-derive/status") ("default" "status"))))))

(define-public crate-struct-patch-0.4.0 (c (n "struct-patch") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.4.0") (d #t) (k 0)))) (h "00fv1qhli71bms2d0xz2sdlbh8wl5ylkzy69qvcwlp3wjb1k3sc9") (f (quote (("status" "struct-patch-derive/status") ("default" "status"))))))

(define-public crate-struct-patch-0.4.1 (c (n "struct-patch") (v "0.4.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "struct-patch-derive") (r "=0.4.1") (d #t) (k 0)))) (h "1hlmncwmq6005znjjk12f5asx4r4m7cfzaxv88i1gcw97r9fylkw") (f (quote (("status" "struct-patch-derive/status") ("default" "status"))))))

