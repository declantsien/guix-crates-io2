(define-module (crates-io st ru structified_enum) #:use-module (crates-io))

(define-public crate-structified_enum-0.1.0 (c (n "structified_enum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0qrkhcwmv80h87kfcdlxywk2i4fkagf4njsvz9b6prxaan26z43f")))

