(define-module (crates-io st ru struct-patch-derive) #:use-module (crates-io))

(define-public crate-struct-patch-derive-0.1.0 (c (n "struct-patch-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struct-patch-trait") (r "=0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1iw5la6p1f66flfg1kc5jz4v5kbpw7d5sy3vx2gg9gq0rh4zpfmf")))

(define-public crate-struct-patch-derive-0.1.1 (c (n "struct-patch-derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0f7iszdq55y0a7ilnz8mcglmw7nq881vh4fy2954rqfj48aq27vc")))

(define-public crate-struct-patch-derive-0.1.2 (c (n "struct-patch-derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1h6drlzw9273ygzykz85idvd61xz3dh2myg5avvidx980i3dqfdc")))

(define-public crate-struct-patch-derive-0.1.3 (c (n "struct-patch-derive") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1ibcdp10vvfmkqgvk4ljq2kw277sm6gic03rp756kf1b4s03g1nz")))

(define-public crate-struct-patch-derive-0.1.4 (c (n "struct-patch-derive") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "09ha3bqxl0i66firp4g94k86fj5b9lnyf97q9hbgfipgfyn2vmz7") (f (quote (("status"))))))

(define-public crate-struct-patch-derive-0.1.5 (c (n "struct-patch-derive") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0v9hnqssww2j0w1jbxi0kg6chadl22bnhgx7a9a6v0pkyasvy3c9") (f (quote (("status"))))))

(define-public crate-struct-patch-derive-0.2.0 (c (n "struct-patch-derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "10xmib4a8km7ch76czxql6fry6kz0c6driwy0mk2cdk329jd11pm") (f (quote (("status"))))))

(define-public crate-struct-patch-derive-0.2.1 (c (n "struct-patch-derive") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0c09yqkxlyznihn7jzj4053bikkzw0bbnhm6v0xa3w09mkk0hzgq") (f (quote (("status"))))))

(define-public crate-struct-patch-derive-0.2.2 (c (n "struct-patch-derive") (v "0.2.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1ddliqzi2wc5wm93041wrf7vp4yi9gcx84hdn1yhpmmagivyvkjw") (f (quote (("status"))))))

(define-public crate-struct-patch-derive-0.2.3 (c (n "struct-patch-derive") (v "0.2.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "17yjl0z6778x3gxr4gchh1nn3kdd72lr87mclsswqby38ddlibfy") (f (quote (("status"))))))

(define-public crate-struct-patch-derive-0.3.0 (c (n "struct-patch-derive") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1zbd83dqxsn1hxgxigh91d2hmay4dk5ph0fhpr3dgr0csbrcp9kf") (f (quote (("status"))))))

(define-public crate-struct-patch-derive-0.3.1 (c (n "struct-patch-derive") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0134yqyipy6ma53iarlqbpmimd143f0fp78wrwgriqvfzajd33z9") (f (quote (("status"))))))

(define-public crate-struct-patch-derive-0.3.2 (c (n "struct-patch-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1la09bn6608l5saqdbgnidncyhk5hainkxhafma2msyfrk5w8cmp") (f (quote (("status"))))))

(define-public crate-struct-patch-derive-0.4.0 (c (n "struct-patch-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "00k43fszj816mv7krhk4np5681lqdhigc9m8062z7b7mbdwkbnkf") (f (quote (("status"))))))

(define-public crate-struct-patch-derive-0.4.1 (c (n "struct-patch-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1nsdjvm1xinqx53s520y4fkqshjdfff3r4rglamrzrgb4yf38jpi") (f (quote (("status"))))))

