(define-module (crates-io st ru struct-from-avro) #:use-module (crates-io))

(define-public crate-struct-from-avro-0.1.0 (c (n "struct-from-avro") (v "0.1.0") (d (list (d (n "rsgen-avro") (r "^0.11.2") (d #t) (k 0)))) (h "11sjxqq59zmdgli5qalkfxd1nvlh9fzrg4525rkjk9r7bhfij6jn")))

(define-public crate-struct-from-avro-0.1.1 (c (n "struct-from-avro") (v "0.1.1") (d (list (d (n "rsgen-avro") (r "^0.11.2") (d #t) (k 0)))) (h "06vvjfqjjzywn0zgw0pvyb2bm9rh2qlpdxya308ik77fcvad6n6z")))

(define-public crate-struct-from-avro-0.1.2 (c (n "struct-from-avro") (v "0.1.2") (d (list (d (n "rsgen-avro") (r "^0.11.2") (d #t) (k 0)))) (h "08xxpls69qcsj6f9f9b23wx5c5z9jqni3zfh6jm5ksxayr1q3f30")))

(define-public crate-struct-from-avro-0.1.3 (c (n "struct-from-avro") (v "0.1.3") (d (list (d (n "rsgen-avro") (r "^0.11.2") (d #t) (k 0)))) (h "190vyzy41nvr47kwgwc8b44ap7m8g3n4600zvjy20awvwbp4xva1")))

(define-public crate-struct-from-avro-0.1.4 (c (n "struct-from-avro") (v "0.1.4") (d (list (d (n "rsgen-avro") (r "^0.11.2") (d #t) (k 0)))) (h "158i8f8r7z5klgf3sxdfankkwvw6rzc4bz9c0f92yqg3iqdwpx75")))

