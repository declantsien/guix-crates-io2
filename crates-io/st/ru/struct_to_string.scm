(define-module (crates-io st ru struct_to_string) #:use-module (crates-io))

(define-public crate-struct_to_string-0.1.0 (c (n "struct_to_string") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1068zzjqjfk200ljw5mbm4si13xdf9n6g5lb646zd4l49rbj3h98")))

(define-public crate-struct_to_string-0.2.0 (c (n "struct_to_string") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "09aw7h02nzrn25jhd3zlafsffk0906f6fwzzh1mirsw9dvyg7ydq")))

