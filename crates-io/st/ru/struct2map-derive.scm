(define-module (crates-io st ru struct2map-derive) #:use-module (crates-io))

(define-public crate-struct2map-derive-0.1.6 (c (n "struct2map-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1gidb8wsc9nfv1yc3vwzd3iirrw02rqj2v5whjaygzmhwhzlf85c")))

