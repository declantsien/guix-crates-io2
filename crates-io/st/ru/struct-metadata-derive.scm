(define-module (crates-io st ru struct-metadata-derive) #:use-module (crates-io))

(define-public crate-struct-metadata-derive-0.1.0 (c (n "struct-metadata-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ksnag0afg0rygpl2skv67m6sg9541wrzdknvn2qmmg20phz53b0")))

(define-public crate-struct-metadata-derive-0.1.1 (c (n "struct-metadata-derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j7lmxra2lyd2ri9gy479i2s8jd0rrfxivl32p09l66nwnh0qrdf")))

