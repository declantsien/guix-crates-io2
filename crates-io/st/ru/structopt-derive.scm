(define-module (crates-io st ru structopt-derive) #:use-module (crates-io))

(define-public crate-structopt-derive-0.0.0 (c (n "structopt-derive") (v "0.0.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0mq5f3sq9v7y44z0h8w7shnx4kka9043bqr5y9ipiqhiy3sl4ry2")))

(define-public crate-structopt-derive-0.0.1 (c (n "structopt-derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "09sf1bgaiphn2hyazdj3na35c1glgnf6i69my6ddwqdvv72d2lzb")))

(define-public crate-structopt-derive-0.0.2 (c (n "structopt-derive") (v "0.0.2") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0qxp8jwjlnv0hj4lv8s737ywkrhxwsm99lm574v1vmq2wlchw4mi")))

(define-public crate-structopt-derive-0.0.3 (c (n "structopt-derive") (v "0.0.3") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0987l8aafbd6580nv91fry02azl18k1ydmlwb52zgfr3m16mykqy")))

(define-public crate-structopt-derive-0.0.5 (c (n "structopt-derive") (v "0.0.5") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1pb8m992600p05fsikqhq9f32h593n3amnpj93qfmqmv7h314i99")))

(define-public crate-structopt-derive-0.1.0 (c (n "structopt-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0r95rj6gm1mfr32bckrlgi3dv8ja718s8pv5zyjgnwpz30h9zscd")))

(define-public crate-structopt-derive-0.1.1 (c (n "structopt-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1jhcrx23bb3gkry94p7z8h5xfl74h1xnxfkg3brv61mifh1qadhj")))

(define-public crate-structopt-derive-0.1.2 (c (n "structopt-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "00sbmzk0ba45ws2hrsqn3vc7lc8dnxyiqb0kpkxcwh3pm296s5s2")))

(define-public crate-structopt-derive-0.1.3 (c (n "structopt-derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1vxwl76fns5qa9z45janb0hy5klq2vvn28ishwijixmbyjd988lg")))

(define-public crate-structopt-derive-0.1.4 (c (n "structopt-derive") (v "0.1.4") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1vb8lkla5v1azw8ipmv1xzv735plix9g9pnr71rkhcvzc8nhyvv3")))

(define-public crate-structopt-derive-0.1.5 (c (n "structopt-derive") (v "0.1.5") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "105clmq96ym7c3a4dwa7sywdi3g5ac5sgxw3q45msbw3961hsl6l")))

(define-public crate-structopt-derive-0.1.6 (c (n "structopt-derive") (v "0.1.6") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0p1py7cfwai2hwv2n2adc8w13hfigqzyphg0dmycpv51lz4ik8ad")))

(define-public crate-structopt-derive-0.2.0 (c (n "structopt-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1dak7cc9hjvjlpckmc6qmhg072wlf23nx4s4045h8h0lmzdy20ax")))

(define-public crate-structopt-derive-0.2.1 (c (n "structopt-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "17vi4phw8jpkq4vzy0jm827jvaf3vvsmcmc5nzq08w7a2gkihcm4")))

(define-public crate-structopt-derive-0.2.2 (c (n "structopt-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1766mfzvp24kzbaknfvzddpxmsf47nwf9wqdb0wik2xgcdhgs3h5")))

(define-public crate-structopt-derive-0.2.3 (c (n "structopt-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0vgcp019bsxb9z172llljjqavdk6swwjfk3iapi64b4wi228fgrp")))

(define-public crate-structopt-derive-0.2.4 (c (n "structopt-derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1hmq9xxylii4n1gkx62fdn6063dsmf9d94pbcf1d2jfmqnbg11ac")))

(define-public crate-structopt-derive-0.2.5 (c (n "structopt-derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0ny5zma1mami628x7rbzklj8nwcpfk89sbwdibr18c8mhsgkwf17") (f (quote (("testing_only_proc_macro2_nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.6 (c (n "structopt-derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1bam6cfjqa13dwfi624dw420zvahqba896pf7r2wwm9j3wjdddls") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.7 (c (n "structopt-derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1fi8c8mpz3hqazq322h4wcjg198kd3cbr77p46iy7nn5pwrhn119") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.8 (c (n "structopt-derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "19jks6zbi2h6mszlfhd1y4ifcvimzxnx5z03kz7cyxjrliakw1lm") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.9 (c (n "structopt-derive") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0p3x5yczzq6rkakcna1gbkagv18q0j0k5hw3bgzl6jy12vqnq8bj") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.10 (c (n "structopt-derive") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1bxj7m5xfv87w6dzmnsig8rlry9b4jirchy1jkann5k2vk6fig2c") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.11 (c (n "structopt-derive") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0pxm4vx3h469bk3h13355vw8pqb8r32rdi5cqjjrk9sxb3dyb0qk") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.12 (c (n "structopt-derive") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "04d37qwrwzx2kvdw6hqhnl1wgqpcsyfz3r9a6zkid7fyjvz03zqp") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.13 (c (n "structopt-derive") (v "0.2.13") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1himdqxyiyp3wb76qpfai1n115iknml6451i39gci8p7z860jljk") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.14 (c (n "structopt-derive") (v "0.2.14") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "014zanrvpzmiy8kd7pshdlcbvp0fahk3g3ah733vxc0038mig67g") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.15 (c (n "structopt-derive") (v "0.2.15") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1illph1wp0ksirwk4nf2yzyj5dprhrmbfvrapkzychnha5ryp2jj") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.16 (c (n "structopt-derive") (v "0.2.16") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0gsj6i74digxjgppajlpzl6kwjh22vx7mjp395pf33ggx059vmf6") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.2.17 (c (n "structopt-derive") (v "0.2.17") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.10") (d #t) (k 0)))) (h "0dc2dxijndf4r2821m96w2xqqvklh7ll1yb2dwcd5kjlfk430yn5") (f (quote (("paw") ("nightly" "proc-macro2/nightly")))) (y #t)))

(define-public crate-structopt-derive-0.2.18 (c (n "structopt-derive") (v "0.2.18") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "01sis9z5kqmyhvzbnmlzpdxcry99a0b9blypksgnhdsbm1hh40ak") (f (quote (("paw") ("nightly" "proc-macro2/nightly"))))))

(define-public crate-structopt-derive-0.3.0 (c (n "structopt-derive") (v "0.3.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vdwxfxzw9acr1y1g4wm8w1i6bgv949q6q5h9lw388206990fn15") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.3.1 (c (n "structopt-derive") (v "0.3.1") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dzz8bl90dw4lji3l5pmgc2id4qgilgh4zcnfvm0sfj6blbfbs9a") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.3.2 (c (n "structopt-derive") (v "0.3.2") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08pi0lvnfmxj3xq533csch9p3bdivs93qain3n9qbyxlylqxgbgk") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.3.3 (c (n "structopt-derive") (v "0.3.3") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bmcccbbqw94hrrirhwpd30vvmxn30wcwjjwgzzj2kkb8wzc3q4g") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.3.4 (c (n "structopt-derive") (v "0.3.4") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mv91ka14njs6kwlbqk75czj0i0qykl2pjfls0m5s5hl2j2235ji") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.3.5 (c (n "structopt-derive") (v "0.3.5") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03j1yd8biai9qgzvpvi7wq62pf6lm27sgrwz1alyng2v0102ssga") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.3.6 (c (n "structopt-derive") (v "0.3.6") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03imzwyb0jbs23pajmckyh1vnrhd9x5rmmyhbxl6ahkrx98yxwhz") (f (quote (("paw")))) (y #t)))

(define-public crate-structopt-derive-0.4.0 (c (n "structopt-derive") (v "0.4.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14vspl6r48kbvx2n1gj6kp89z31vl8ji121s6mdrs2jalclzi5qa") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.1 (c (n "structopt-derive") (v "0.4.1") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)))) (h "02iwn2sk8kl4d687lxin735crslgsxisjwrybw2mi2bv5xyshl7x") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.2 (c (n "structopt-derive") (v "0.4.2") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)))) (h "0r2zkrk5v3yk103rkzd082p4630lry25c2jxcd7d352v3ym68l09") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.3 (c (n "structopt-derive") (v "0.4.3") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "clone-impls"))) (k 0)))) (h "132y7304iy4k9ny95cji29h2p876i2nh9zanzc982049wjyqj8ic") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.4 (c (n "structopt-derive") (v "0.4.4") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gghaqdgb9yrvhj2ii7ikwcbwjb20m78s8b0m5ndivzlw209rry6") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.5 (c (n "structopt-derive") (v "0.4.5") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c04bbzc5bmr2ns6qy35yz55nn3xvlq4dpwxdynnljb9ikhvi21z") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.6 (c (n "structopt-derive") (v "0.4.6") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6rxgjj6i9bvl83xsl38nfcq3a72gfhjlb6hvii5bzv11yci2d4") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.7 (c (n "structopt-derive") (v "0.4.7") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ylj5w6l7p7z7qxx7biph3znaxj5kvbcp5b75qaa5rxf2d5wlffj") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.8 (c (n "structopt-derive") (v "0.4.8") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0661n4qs9g0g51qwvmvj7dh2kiqmpw4jbdpazfj64rv1vvwi612i") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.9 (c (n "structopt-derive") (v "0.4.9") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16azipvc5db8xbbngb56pyx3y5iiy03xrqj2pr8vxvmf6mrv63hy") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.10 (c (n "structopt-derive") (v "0.4.10") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pscws9pwz837aaa6xkjgnhgpq0ww7czl7bmlr97a1r5308i69ay") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.11 (c (n "structopt-derive") (v "0.4.11") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1j95na20zqvs24fxz6gfkg38j0hjqm7z2n2xslylnahj5187fbn9") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.12 (c (n "structopt-derive") (v "0.4.12") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10xknh4k2fdjjndk0kp3q56jmshf4kzwqsci3s6j8xmsvzj7vi4g") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.13 (c (n "structopt-derive") (v "0.4.13") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s4imsva3xnsz61kq3j3vs4p40qlzidgywa9acha48wy5x4irrb5") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.14 (c (n "structopt-derive") (v "0.4.14") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "143gjwvz3s86hwp070km83y25n8kqp5f01kb1dr19f4ilkywvaav") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.15 (c (n "structopt-derive") (v "0.4.15") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "044w7z0bnsvla4d26s1r8s4q9sbx4f60c02yfxa1mxgmxi5964vq") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.16 (c (n "structopt-derive") (v "0.4.16") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fnhfbvqasx2n9ak57vi3d2r6d55xmjz2vfg4lqsqhwr5j586k8k") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.17 (c (n "structopt-derive") (v "0.4.17") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kq7nnk4p3ixp9zd899m2rjckhfqndzkzq1rxww68ldw9vgrwc8k") (f (quote (("paw"))))))

(define-public crate-structopt-derive-0.4.18 (c (n "structopt-derive") (v "0.4.18") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1q5gcigmvw0cinjxzpyrkflliq5r1ivljmrvfrl3phcwgwraxdfw") (f (quote (("paw"))))))

