(define-module (crates-io st ru structenv_derive) #:use-module (crates-io))

(define-public crate-structenv_derive-0.0.1 (c (n "structenv_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "035x5dw38ymlm63kxak0a32hcqk86qmkpvqjnb8i4v918ixq6vx5")))

