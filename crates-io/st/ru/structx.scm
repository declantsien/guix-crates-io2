(define-module (crates-io st ru structx) #:use-module (crates-io))

(define-public crate-structx-0.1.0 (c (n "structx") (v "0.1.0") (d (list (d (n "inwelling") (r "^0.2") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1") (d #t) (k 0)))) (h "03lzai2bii53kl1szzayl5ycxdj24vdp227r21w1ici50yh98qr7")))

(define-public crate-structx-0.1.1 (c (n "structx") (v "0.1.1") (d (list (d (n "inwelling") (r "^0.2") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1") (d #t) (k 0)))) (h "1krcd42zfny0bs071z2c3nphd4kqc1k93q2h57zgfc4m9ryxrs4l")))

(define-public crate-structx-0.1.2 (c (n "structx") (v "0.1.2") (d (list (d (n "inwelling") (r "^0.2") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1") (d #t) (k 0)))) (h "0yzpahvzsp35ndxjck9f6q9nzic57yzzy4zl53aba8sc36674z7i")))

(define-public crate-structx-0.1.3 (c (n "structx") (v "0.1.3") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1") (d #t) (k 0)))) (h "1jr3h60852llx08mpvlklbgm11p5zp8b9n71392md7zpp9sqn9vs")))

(define-public crate-structx-0.1.4 (c (n "structx") (v "0.1.4") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs") (r "^0.3") (f (quote ("structx"))) (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "parsing" "visit" "visit-mut"))) (d #t) (k 1)))) (h "0hf3fpzjkk8qn16h40k5xarv6ypm576s93acyzq679qbam3jvv1z")))

(define-public crate-structx-0.1.5 (c (n "structx") (v "0.1.5") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs") (r "^0.3") (f (quote ("structx"))) (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "parsing" "visit" "visit-mut"))) (d #t) (k 1)))) (h "149wp8h9gmm8s4vzn9hjr3mgix1pr90yrhp0viwg23sx78k1kym5")))

(define-public crate-structx-0.1.6 (c (n "structx") (v "0.1.6") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs") (r "^0.3") (f (quote ("structx"))) (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "parsing" "visit" "visit-mut"))) (d #t) (k 1)))) (h "1x4578jbj6c7nyda7gmnl9wx0iq09h6010y3i60disl5sygfgqnj")))

(define-public crate-structx-0.1.7 (c (n "structx") (v "0.1.7") (d (list (d (n "inwelling") (r "^0.4") (d #t) (k 1)) (d (n "lens-rs") (r "^0.3") (f (quote ("structx"))) (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "parsing" "visit" "visit-mut"))) (d #t) (k 1)))) (h "1c5qr6adq9wbd9ii14czvyzbrz0v02zqbf4kgmm61hqa6qq741ng")))

(define-public crate-structx-0.1.8 (c (n "structx") (v "0.1.8") (d (list (d (n "inwelling") (r "^0.5") (d #t) (k 1)) (d (n "lens-rs") (r "^0.3") (f (quote ("structx"))) (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "parsing" "visit" "visit-mut"))) (d #t) (k 1)))) (h "1ix5q5fb8jrmasq43avmvfhc326ynw0wxc2m9cb02l2nkyv5w948")))

(define-public crate-structx-0.1.9 (c (n "structx") (v "0.1.9") (d (list (d (n "inwelling") (r "^0.5") (d #t) (k 1)) (d (n "lens-rs") (r "^0.3") (f (quote ("structx"))) (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "parsing" "visit" "visit-mut"))) (d #t) (k 1)))) (h "0nz5gz6nq7g0s3rky3gfagp54ig3n38ycrfq3d99hjjayyhgdmlq")))

(define-public crate-structx-0.1.10 (c (n "structx") (v "0.1.10") (d (list (d (n "inwelling") (r "^0.5") (d #t) (k 1)) (d (n "lens-rs") (r "^0.3") (f (quote ("structx"))) (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "parsing" "visit" "visit-mut"))) (d #t) (k 1)))) (h "0islh8qlggd2djj5awxy77n737b392niilsfvzd8jgv147mmiwjv")))

(define-public crate-structx-0.1.11 (c (n "structx") (v "0.1.11") (d (list (d (n "inwelling") (r "^0.5") (d #t) (k 1)) (d (n "lens-rs") (r "^0.3") (f (quote ("structx"))) (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "structx_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "parsing" "visit" "visit-mut"))) (d #t) (k 1)))) (h "05jc8di7x2w8nln8r047m6ar2335ml5ddcgzvbsd67vyic7dl7gj")))

