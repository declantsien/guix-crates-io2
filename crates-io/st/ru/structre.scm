(define-module (crates-io st ru structre) #:use-module (crates-io))

(define-public crate-structre-0.0.1 (c (n "structre") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (f (quote ("std"))) (k 0)) (d (n "structre_proc_macros") (r "^0.0.1") (d #t) (k 0)))) (h "1x6m9x7ivmzw1165wpm17894h56fznlhv732jcf663dvrq6sn7pk") (f (quote (("unicode" "structre_proc_macros/unicode" "regex/unicode") ("default" "unicode"))))))

(define-public crate-structre-0.0.2 (c (n "structre") (v "0.0.2") (d (list (d (n "regex") (r "^1.7.0") (f (quote ("std"))) (k 0)) (d (n "structre_proc_macros") (r "^0.0.1") (d #t) (k 0)))) (h "0lhasjrx0pd70v79jy0izxay3d69z3xrlpzk52jwa1zm3mrych6w") (f (quote (("unicode" "structre_proc_macros/unicode" "regex/unicode") ("default" "unicode"))))))

(define-public crate-structre-0.0.3 (c (n "structre") (v "0.0.3") (d (list (d (n "regex") (r "^1.7.0") (f (quote ("std"))) (k 0)) (d (n "structre_proc_macros") (r "^0.0.2") (d #t) (k 0)))) (h "0ba63hj6k0n7389gk1di2r4v845qqrqc27mw091k2qpis3nig6sj") (f (quote (("unicode" "structre_proc_macros/unicode" "regex/unicode") ("default" "unicode"))))))

(define-public crate-structre-0.0.4 (c (n "structre") (v "0.0.4") (d (list (d (n "regex") (r "^1.7.0") (f (quote ("std"))) (k 0)) (d (n "structre_proc_macros") (r "^0.0.3") (d #t) (k 0)))) (h "1pkn3kv4msdminyyzf0b5vl87z8096nh9p2xd694fnyi0h7c8xd8") (f (quote (("unicode" "structre_proc_macros/unicode" "regex/unicode") ("default" "unicode"))))))

