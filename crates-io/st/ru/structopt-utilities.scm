(define-module (crates-io st ru structopt-utilities) #:use-module (crates-io))

(define-public crate-structopt-utilities-0.0.0 (c (n "structopt-utilities") (v "0.0.0") (d (list (d (n "pipe-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1295881d1fgk4r3j3154frj19nvvdh4sswzi4axiymhqkfciaq3h")))

(define-public crate-structopt-utilities-0.0.1 (c (n "structopt-utilities") (v "0.0.1") (d (list (d (n "pipe-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "117vbiz7rz0wdbcdwiksz0l90wg4zwzp1cpzvbdji3sxkli4djfd")))

(define-public crate-structopt-utilities-0.0.2 (c (n "structopt-utilities") (v "0.0.2") (d (list (d (n "pipe-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "10882mab1v21izyhv7jrn6dgwiswkqx0a0pf6072kxdgw0izjhd3")))

(define-public crate-structopt-utilities-0.0.3 (c (n "structopt-utilities") (v "0.0.3") (d (list (d (n "pipe-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.4.10") (d #t) (k 0)))) (h "1r73596r1dvjkdfjissm9p65vps5lc9wbbiin19fp3m2lan36hr2")))

(define-public crate-structopt-utilities-0.0.4 (c (n "structopt-utilities") (v "0.0.4") (d (list (d (n "pipe-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "04gpl2zghj9imp89axr423a02m4ndqkj53pimpaky0fila5ggrlm")))

(define-public crate-structopt-utilities-0.0.5 (c (n "structopt-utilities") (v "0.0.5") (d (list (d (n "pipe-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "17yylmz52zxin9nn2mzw5xjnwljsas2r65ifzwyg0f5a7dpabiic")))

(define-public crate-structopt-utilities-0.0.6 (c (n "structopt-utilities") (v "0.0.6") (d (list (d (n "pipe-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1vizqxw9p8kh77jvj05llsp6l1znjnxnvcpv3rv9fvhm91cvyfpc")))

(define-public crate-structopt-utilities-0.0.7 (c (n "structopt-utilities") (v "0.0.7") (d (list (d (n "pipe-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1fkq8vcp1dprc1lr6pv0qisr27ql2vr410nxjnjpyr2a0c3kypic")))

(define-public crate-structopt-utilities-0.0.8 (c (n "structopt-utilities") (v "0.0.8") (d (list (d (n "pipe-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "0a2d1dqilgnpqivyiki7cmpxainmc34bnz87baq7a7acx21n8zir")))

(define-public crate-structopt-utilities-0.1.0 (c (n "structopt-utilities") (v "0.1.0") (d (list (d (n "pipe-trait") (r "^0.3.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0w9b60sxzyqxpksavvx2c2irgl02zxsykngscc2b1ianlfpa3n46")))

