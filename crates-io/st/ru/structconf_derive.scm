(define-module (crates-io st ru structconf_derive) #:use-module (crates-io))

(define-public crate-structconf_derive-0.1.0 (c (n "structconf_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fz1mj3qxl8ypcp5kxq8yxarrsl6lq4wa0j2wlq6brrcwfv2za2h")))

(define-public crate-structconf_derive-0.1.1 (c (n "structconf_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ia5jp9wljyiz3v1sjllrwpng6jslix75fmccibvaj5lc4npkdkc")))

(define-public crate-structconf_derive-0.1.2 (c (n "structconf_derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yg3hr9i45nvqfldbs4dic1glaic2ji9h1w7npj3s09zg5mr48lp")))

(define-public crate-structconf_derive-0.1.3 (c (n "structconf_derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h1rbcw18bh6kl7vzgj8p0x51nasx72cqz0c0mn91ff11jj1r13h")))

(define-public crate-structconf_derive-0.2.0 (c (n "structconf_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0khnnxf3wf22g7zfany6bxzsszimsphqrsdsj2d1ihw27f3j9b8d")))

(define-public crate-structconf_derive-0.3.0 (c (n "structconf_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "199bm4jwh3lq09a845qydzw5yi435w4zvzwdbax9ivhzyp0r47h5")))

(define-public crate-structconf_derive-0.3.1 (c (n "structconf_derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r14740jzpijrdnli68cf4f5p0ipljp9wwyqy94xshggbyn8ncix")))

(define-public crate-structconf_derive-0.3.2 (c (n "structconf_derive") (v "0.3.2") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ilz6994xgp94yniw8r9rm91bsniq3vv9z5k5bdlkmn4khi0jrjb")))

(define-public crate-structconf_derive-0.4.0 (c (n "structconf_derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "036fzah9x7p0x9qj6qi388m7va60gsa001zgvv8l3a92n8ybwy6g")))

(define-public crate-structconf_derive-0.5.0 (c (n "structconf_derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fyx3sl9vvjnfnqmgv3wr7fixqq4ymszvfwpl8fws9i7f3qynwqh")))

