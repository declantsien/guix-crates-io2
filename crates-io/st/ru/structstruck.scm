(define-module (crates-io st ru structstruck) #:use-module (crates-io))

(define-public crate-structstruck-0.1.0 (c (n "structstruck") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "venial") (r "^0.2.1") (d #t) (k 0)))) (h "11pgid0p6sxfd1ydf69r6mcxch3fhllwgcvd8k9xaxdc06wagj6m")))

(define-public crate-structstruck-0.1.1 (c (n "structstruck") (v "0.1.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "venial") (r "^0.3.0") (d #t) (k 0)))) (h "1s3yy5br7w8fzq3ypd0c85n54g0kdfr7ky2hha9dyfag7nfs49xi")))

(define-public crate-structstruck-0.2.0 (c (n "structstruck") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "venial") (r "^0.4.0") (d #t) (k 0)))) (h "0wlyfvlqhf2apdpnb0661hcflscvh8zpbkqxmjv33mx4gbayfx00")))

(define-public crate-structstruck-0.2.1 (c (n "structstruck") (v "0.2.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "venial") (r "^0.4.0") (d #t) (k 0)))) (h "0hslcs6c66d4if4087rh9qgh30m3rd725nxajgq5nvbgkqw83g4i")))

(define-public crate-structstruck-0.2.2 (c (n "structstruck") (v "0.2.2") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "venial") (r "^0.4.0") (d #t) (k 0)))) (h "10f19fgf4k8ivdvs00rm6kjzj53s0s7yjda0bnpvn9wdqixsi9wj")))

(define-public crate-structstruck-0.3.0 (c (n "structstruck") (v "0.3.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "venial") (r "^0.4.0") (d #t) (k 0)))) (h "1wlyyb4cyixahf6vm6ji5zh055w28i95b9r04ab783099kjwsqqc")))

(define-public crate-structstruck-0.4.0 (c (n "structstruck") (v "0.4.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1.23") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (d #t) (k 2)))) (h "1pifqbgmirxbqyj9w2gvd7irc92fj0gb15pkc23ma0b3sdpxb63l")))

(define-public crate-structstruck-0.4.1 (c (n "structstruck") (v "0.4.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1.23") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 2)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)))) (h "0vd6gsscqz6nx7nwpcch85p83h2sgahccppq6lxdv6rdgb42w18s")))

