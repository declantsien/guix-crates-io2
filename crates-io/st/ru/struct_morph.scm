(define-module (crates-io st ru struct_morph) #:use-module (crates-io))

(define-public crate-struct_morph-0.1.0 (c (n "struct_morph") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n3imsqrbmg8haspdz3n7nh8vfmf62v5r6rbdkrznvyv18icfgcq") (y #t)))

(define-public crate-struct_morph-0.2.0 (c (n "struct_morph") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1154naiwa8fx9syzifabbszb0ana1b1r2s6pfj9z73m20vaqjh5k") (y #t)))

(define-public crate-struct_morph-0.3.0 (c (n "struct_morph") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pgjdjykjz7m43vxbm8myv8inz9pf5bxh2q0zqqzqapa4xp0r62y") (y #t)))

(define-public crate-struct_morph-0.4.0 (c (n "struct_morph") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hz1dmwcjry00ymwb2fnpp9p8cyww7kzsqxx44fzg0aiib2f6qyn") (y #t)))

(define-public crate-struct_morph-0.5.0 (c (n "struct_morph") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d1bffcn1fgghnywk1ix5p9mfhaa15wk01r2cypg4v411l65jdqj")))

(define-public crate-struct_morph-0.6.0 (c (n "struct_morph") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1harn174pddsab12g4w2b6v5flcqwzc1yrp1mablx2x2z5a7y88s")))

