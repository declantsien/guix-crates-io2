(define-module (crates-io st ru strung_derive) #:use-module (crates-io))

(define-public crate-strung_derive-0.1.0 (c (n "strung_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s6n637d9yfcylzyhvg44gxyq9cv42mx58ax28rqjncy8mg8w3d9")))

(define-public crate-strung_derive-0.1.1 (c (n "strung_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xf6rm9fm3gkkgzk1hv8s6b5f6ml7d3h72kj85q8cwnzjq5v1s88")))

(define-public crate-strung_derive-0.1.2 (c (n "strung_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wg1k0ds3i66j59267qkfpwp8p9nql7x1a1hci4gxgw1py4vkdsd")))

(define-public crate-strung_derive-0.1.3 (c (n "strung_derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00jwzmdh2am92ffpb15g1qgbs9yx0xw4pxiaz6qmm8r9nhwyvplg")))

(define-public crate-strung_derive-0.1.4 (c (n "strung_derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "06icd5bcjwz18ach6grvy0q5xxqf0x7047zwid8lwnn0vxffmdkq")))

