(define-module (crates-io st ru struct-patch-trait) #:use-module (crates-io))

(define-public crate-struct-patch-trait-0.1.0 (c (n "struct-patch-trait") (v "0.1.0") (h "1r7ghcbfkq4mfnjx6hyf986yzqziqd2bmfbg6q7j778c1ghiz6rl")))

(define-public crate-struct-patch-trait-0.1.1 (c (n "struct-patch-trait") (v "0.1.1") (h "0lcvac2bwykvzkfi4iijgzvp5lm0myx9987wh53ijs2nd7c2c1w2")))

(define-public crate-struct-patch-trait-0.1.2 (c (n "struct-patch-trait") (v "0.1.2") (h "1dlx91vgskiqrclzdmyv6fypf0hmhppidn91nn4sdxb69fqxqz1c")))

(define-public crate-struct-patch-trait-0.1.3 (c (n "struct-patch-trait") (v "0.1.3") (h "1rll970hrzxy6rwy9a8zmk51apcqsxvwmskjk94w2y8h282k5s0p")))

(define-public crate-struct-patch-trait-0.1.4 (c (n "struct-patch-trait") (v "0.1.4") (h "1y14ghf4xwnj247xx9454ycrdfipw91zbbx1mg3vz5bkj7fgjbjd") (f (quote (("status"))))))

(define-public crate-struct-patch-trait-0.1.5 (c (n "struct-patch-trait") (v "0.1.5") (h "1liqkjbqz39mxdnvfk9k3j21nfj7c9pbjbq5jkfjvddhn493g2jl") (f (quote (("status"))))))

(define-public crate-struct-patch-trait-0.2.0 (c (n "struct-patch-trait") (v "0.2.0") (h "0jm23vrd7n9apv4856x6pafv6ss3hn5802j6f7g63lrmccsakspa") (f (quote (("status"))))))

