(define-module (crates-io st ru structbuilder_derive) #:use-module (crates-io))

(define-public crate-structbuilder_derive-0.1.0 (c (n "structbuilder_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ryqdkmmiif6scydjfx9gsfn0fhn83zps7br0313266kwqkrv6d7")))

(define-public crate-structbuilder_derive-0.1.1 (c (n "structbuilder_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hxgl94mn1w4zhg1g4whf18vwdhlknrdr4w8h9gdcri0k383ykli")))

(define-public crate-structbuilder_derive-0.2.0 (c (n "structbuilder_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12kkddcvnrpqv2w0qpdqqs1rh50cx9yk65rmpd8mr251rcc14ny8")))

(define-public crate-structbuilder_derive-0.2.1 (c (n "structbuilder_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00gss1n92jjfm8v73xasn2l538qhsh475nq2qgd22vn8fihq6nmb")))

