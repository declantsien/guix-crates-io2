(define-module (crates-io st ru structinator_traits) #:use-module (crates-io))

(define-public crate-structinator_traits-0.1.0 (c (n "structinator_traits") (v "0.1.0") (d (list (d (n "enum_unwrapper") (r "^0.1") (d #t) (k 2)))) (h "1h5znb3glryl2qj5ybadw0pjbhkq0dc75zjckrh0sy79y1i3arfb") (y #t)))

(define-public crate-structinator_traits-0.1.1 (c (n "structinator_traits") (v "0.1.1") (d (list (d (n "enum_unwrapper") (r "^0.1") (d #t) (k 2)))) (h "1kqz9wdkzyw70qdnaxip8s8v3yxspiq2lisn5gfcvbv54zy29ahq") (y #t)))

(define-public crate-structinator_traits-0.1.2 (c (n "structinator_traits") (v "0.1.2") (d (list (d (n "enum_unwrapper") (r "^0.1") (d #t) (k 2)))) (h "1lrkkw3wy70bf52hf2xnhkssm1vflav9i6vnfnm8bfwys0lmvvpv") (y #t)))

(define-public crate-structinator_traits-0.1.3 (c (n "structinator_traits") (v "0.1.3") (h "0wqa8aqdwqfgp6wh11aycqd4d07k2rl9imxr76z6q923nrzzv67f") (y #t)))

(define-public crate-structinator_traits-0.1.4 (c (n "structinator_traits") (v "0.1.4") (d (list (d (n "enum_unwrapper") (r "^0.1") (d #t) (k 2)) (d (n "structinator") (r "^0.1") (d #t) (k 2)))) (h "1rjfbfzr6f3sccfa0m6s40da5py8r498kqj8xywa4lf98wyqiv80") (y #t)))

(define-public crate-structinator_traits-0.1.5 (c (n "structinator_traits") (v "0.1.5") (d (list (d (n "enum_unwrapper") (r "^0.1") (d #t) (k 2)) (d (n "structinator") (r "^0.1") (d #t) (k 2)))) (h "0qq4n008h85zk5h9k5nv9iy3py3f1dz7za8bncz99mbvcbndk5wd")))

