(define-module (crates-io st ru struct-string-template) #:use-module (crates-io))

(define-public crate-struct-string-template-0.1.0 (c (n "struct-string-template") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "10qpj49z6w6p8g8wgbhvqxm5gcgdzw46hxjkj7ysfx50hnx9gz6q")))

