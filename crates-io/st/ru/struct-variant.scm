(define-module (crates-io st ru struct-variant) #:use-module (crates-io))

(define-public crate-struct-variant-1.0.0 (c (n "struct-variant") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0ahkxc4fwz2hvnwbml5lvqg0p6dayp8b2a8ahrb676q4g91n13zv") (f (quote (("doc") ("default")))) (y #t)))

(define-public crate-struct-variant-1.0.1 (c (n "struct-variant") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "17k32ygwwf2hal0idi3mk4yvz3d2hmlmc8jjv20p0vpzxrywyykw") (f (quote (("doc") ("default")))) (y #t)))

(define-public crate-struct-variant-1.0.2 (c (n "struct-variant") (v "1.0.2") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1w9dbv2969nnsra5iblxhq0na486w2wymqfbm1aaj4c0vlhb1cai") (f (quote (("doc") ("default"))))))

