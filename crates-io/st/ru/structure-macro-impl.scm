(define-module (crates-io st ru structure-macro-impl) #:use-module (crates-io))

(define-public crate-structure-macro-impl-0.1.0 (c (n "structure-macro-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "1pxpi57zq8s6yqjbf2dpd7a1lxivv15prm2ixxx0gda3higbnhqa")))

(define-public crate-structure-macro-impl-0.1.1 (c (n "structure-macro-impl") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "0wpmxvzqb0adlxp859acq36720ybmjlqm1hv8zanzwykgdc81zai")))

(define-public crate-structure-macro-impl-0.1.2 (c (n "structure-macro-impl") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "1jksyxhp7z83rxm6x427pps8f03hgymzz3v8g1cbpj194jgm5h70")))

