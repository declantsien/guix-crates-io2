(define-module (crates-io st ru struf_derive) #:use-module (crates-io))

(define-public crate-struf_derive-0.0.1 (c (n "struf_derive") (v "0.0.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1dwcrm34x6rf6jk7pf4knd2awpl0gsi4pn3sn3f6cbwcnlgap429")))

