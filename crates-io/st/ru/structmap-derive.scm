(define-module (crates-io st ru structmap-derive) #:use-module (crates-io))

(define-public crate-structmap-derive-0.1.0 (c (n "structmap-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15ndhq4aidvf16mjm9ay67zkv6lx5nv5lk0q7kqkn1a20vc7ldyr")))

(define-public crate-structmap-derive-0.1.1 (c (n "structmap-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05h8mx5mvqk465yhcrv2xjmr1kx2f2kcsksrnji1a81cpwnpnii0")))

(define-public crate-structmap-derive-0.1.2 (c (n "structmap-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kbafim0i97wybw4r6b634w7pkf1ycvwmy0vg2nfryknkf75d7h8")))

(define-public crate-structmap-derive-0.1.3 (c (n "structmap-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0f4xd7l0pnjn224k0l0cia3ghwwsk76m45vwivz45c44a1hszr7m")))

(define-public crate-structmap-derive-0.1.4 (c (n "structmap-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1ia94yhhjd4cbrnz2vbrh3x7ricmx3gdpgzqh62xj9f7gik97xgr")))

(define-public crate-structmap-derive-0.1.5 (c (n "structmap-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0195nwnpngrndjsf4lcshls8pm5rqbyrf41rypd1vknqssnn74fk")))

(define-public crate-structmap-derive-0.1.6 (c (n "structmap-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11jrw2wqcibjnkw06myy0qxarbfsmplvgix8cjb63bb2g1csq5q1")))

