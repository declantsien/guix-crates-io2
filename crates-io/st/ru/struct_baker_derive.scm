(define-module (crates-io st ru struct_baker_derive) #:use-module (crates-io))

(define-public crate-struct_baker_derive-0.1.0 (c (n "struct_baker_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fhn9y45admnpxigd4aisjxlch0yxr38lhcyg3zgrxbclr34i5f7")))

(define-public crate-struct_baker_derive-0.1.1 (c (n "struct_baker_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lsky6r7hpl4vvgq6zgzb193zfl3pzn163ygg7q8w11fixps1lc3")))

