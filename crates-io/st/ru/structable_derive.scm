(define-module (crates-io st ru structable_derive) #:use-module (crates-io))

(define-public crate-structable_derive-0.1.0 (c (n "structable_derive") (v "0.1.0") (d (list (d (n "darling") (r "~0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mgzllqpj5fcackvi9ncd3j19b4wdmykkwyaw0gypj4wybgk09k3")))

(define-public crate-structable_derive-0.1.1 (c (n "structable_derive") (v "0.1.1") (d (list (d (n "darling") (r "~0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vgvyyaq8brcym2ljsgsx60dpx8nnwvrfg697k3ax1z6aqdf289q") (r "1.75")))

(define-public crate-structable_derive-0.1.2 (c (n "structable_derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hllc4d346jp57rndy86naqhmnnlklz5rj0ihwqppxix3zbsghf0") (r "1.75")))

(define-public crate-structable_derive-0.1.3 (c (n "structable_derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mk7inghjq0hcxfvrd7xp9qgayk9a4sjyr1nh5iakqv9i303z4wp") (r "1.75")))

