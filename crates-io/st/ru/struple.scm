(define-module (crates-io st ru struple) #:use-module (crates-io))

(define-public crate-struple-0.1.0 (c (n "struple") (v "0.1.0") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "struple-impl") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "1nbk14mxfrg0ggjnsns9v01b1mj033ll9if6hs96524sfyd5wrax")))

(define-public crate-struple-0.1.1 (c (n "struple") (v "0.1.1") (d (list (d (n "struple-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "16d1aalvcv585538n8p7z31q5zxdnri4ldw17crznch2zw1bczyx") (f (quote (("default") ("big-tuples"))))))

