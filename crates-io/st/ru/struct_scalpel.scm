(define-module (crates-io st ru struct_scalpel) #:use-module (crates-io))

(define-public crate-struct_scalpel-0.1.0 (c (n "struct_scalpel") (v "0.1.0") (d (list (d (n "enable-ansi-support") (r "^0.2.1") (d #t) (k 0)) (d (n "struct_scalpel_proc_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0hsj5rs5ngsnw49lfvy9md60fwn9b9jw51mpfvx4sidrfapa3r5k") (f (quote (("dissect_std") ("default" "dissect_std"))))))

(define-public crate-struct_scalpel-0.1.1 (c (n "struct_scalpel") (v "0.1.1") (d (list (d (n "enable-ansi-support") (r "^0.2.1") (d #t) (k 0)) (d (n "struct_scalpel_proc_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1qghwlbn4z33ymvnz6p082sjbipm55mibhddg1m9qaifa1jbwz6c") (f (quote (("dissect_std") ("default" "dissect_std"))))))

