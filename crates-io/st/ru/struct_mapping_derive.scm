(define-module (crates-io st ru struct_mapping_derive) #:use-module (crates-io))

(define-public crate-struct_mapping_derive-1.0.0 (c (n "struct_mapping_derive") (v "1.0.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struct_mapping") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1z5dbgd8hhqi58khnq00y276cdafrgvfbhbrzqdbik21z9z66wdw")))

(define-public crate-struct_mapping_derive-1.0.1 (c (n "struct_mapping_derive") (v "1.0.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struct_mapping") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0s79a3naxm2485c75fmiaai84098kryx0pqc3vwn8axglpy48fzr")))

