(define-module (crates-io st ru structsy_tools) #:use-module (crates-io))

(define-public crate-structsy_tools-0.1.0 (c (n "structsy_tools") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "structsy") (r "^0.4") (d #t) (k 0)))) (h "12hrs4nqbz0sk4bkbk50pl1m8yfvfksqk3jc1phdmall9dl8mqbv") (f (quote (("serde_integration" "structsy/serde_info" "serde"))))))

(define-public crate-structsy_tools-0.2.0 (c (n "structsy_tools") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "structsy") (r "^0.5") (d #t) (k 0)) (d (n "structsy-derive") (r "^0.5") (d #t) (k 2)))) (h "1ajc11kg4vlk0g3mjzbjpid3vwdfr9sbi2ccj29726hv6hwyiywl") (f (quote (("serde_integration" "structsy/serde_info" "serde"))))))

