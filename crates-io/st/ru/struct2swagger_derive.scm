(define-module (crates-io st ru struct2swagger_derive) #:use-module (crates-io))

(define-public crate-struct2swagger_derive-0.1.0 (c (n "struct2swagger_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "struct2swagger") (r "^0.1.0") (d #t) (k 0)))) (h "0s7zvjnhia6qzdwik9ghipl6hk97d8wqamr2minf0fkzjngyga02")))

(define-public crate-struct2swagger_derive-0.1.2 (c (n "struct2swagger_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "struct2swagger") (r "^0.1.1") (d #t) (k 0)))) (h "1rxwgikm1ibcfarm59ga5qi46hdhkfc0hyxwcgqrr5v5y6mjqc4a")))

(define-public crate-struct2swagger_derive-0.1.3 (c (n "struct2swagger_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "struct2swagger") (r "^0.1.3") (d #t) (k 0)))) (h "0wv0j740msdir4jr7yixgkqd6xz9prmqc4xyjw8vx1hy1sy02ipa")))

(define-public crate-struct2swagger_derive-0.1.5 (c (n "struct2swagger_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "struct2swagger") (r "^0.1.5") (d #t) (k 0)))) (h "1xwyk5xa4hvk51bb085hqxvs2i5i1fn7f0dynbbmqbhbp9m47am8")))

(define-public crate-struct2swagger_derive-0.1.6 (c (n "struct2swagger_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "struct2swagger") (r "^0.1.6") (d #t) (k 0)))) (h "0hqsvq5l2xj6fy0mida5rxgasa8jr0p4yhfxqvzhsz5f28cxkpla")))

