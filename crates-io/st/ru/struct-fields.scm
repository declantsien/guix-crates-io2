(define-module (crates-io st ru struct-fields) #:use-module (crates-io))

(define-public crate-struct-fields-0.0.1 (c (n "struct-fields") (v "0.0.1") (d (list (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "14vx4lazv55adpi4jgriym3282xjp7zpllzbagpdh570f5zhkf1r")))

(define-public crate-struct-fields-0.0.2 (c (n "struct-fields") (v "0.0.2") (d (list (d (n "affix") (r "^0.1.2") (d #t) (k 0)))) (h "0rkkkm1xh4jyny283d6rf3n1ihphz8lxb9mrk3iy6h2kq4jwi7qm")))

