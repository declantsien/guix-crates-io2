(define-module (crates-io st ru struct_deser) #:use-module (crates-io))

(define-public crate-struct_deser-0.1.0 (c (n "struct_deser") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "struct_deser-derive") (r "^0.1.0") (d #t) (k 2)))) (h "1h2cdia80c6d1wjlnh2h6hw8w3fn7w8pm7lwalk219i38sl93hyp")))

(define-public crate-struct_deser-0.1.1 (c (n "struct_deser") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "struct_deser-derive") (r "^0.1.0") (d #t) (k 2)))) (h "0rd8syp8b4jb6ba95n0c183i26y82i56k3pnf92d8m9ldyb2mdk8")))

