(define-module (crates-io st ru struct-map) #:use-module (crates-io))

(define-public crate-struct-map-0.1.0 (c (n "struct-map") (v "0.1.0") (d (list (d (n "to_hash_map") (r "^0.1") (d #t) (k 0)))) (h "00rnhpgi4qdmzg2fwb6lpa0wv7ndpl5p30v52v2qf0hx8zhw3vgl")))

(define-public crate-struct-map-0.1.1 (c (n "struct-map") (v "0.1.1") (d (list (d (n "to_hash_map") (r "^0.1") (d #t) (k 0)))) (h "19gvdlj9vrrhc2akz4nkqi9jlayxk97l2fvxfx92rbjzl8rcfilz")))

