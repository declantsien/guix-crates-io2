(define-module (crates-io st ru struct_scalpel_proc_macro) #:use-module (crates-io))

(define-public crate-struct_scalpel_proc_macro-0.1.0 (c (n "struct_scalpel_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)))) (h "1x21xg8vma9kdrjgw1z8r2ffqnz5x0mddik4nbx0cmxalbmy57z3")))

