(define-module (crates-io st ru structural-convert) #:use-module (crates-io))

(define-public crate-structural-convert-0.1.0 (c (n "structural-convert") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustc-workspace-hack") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1gnc8pvh7ppi8kmxqikqy2mcjgkg49jjgizq5ak9dk4v15ff4dqy")))

(define-public crate-structural-convert-0.2.0 (c (n "structural-convert") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustc-workspace-hack") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1m45ljvl1p85s1k52m5ab3pw5vb3gzbn0jwzqpjr59an7jlr5spr")))

(define-public crate-structural-convert-0.3.0 (c (n "structural-convert") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1636qx7y4q35hhasrsb57781fy4fvnwc8yz0shz54b1srjkjdlyr")))

(define-public crate-structural-convert-0.4.0 (c (n "structural-convert") (v "0.4.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gcxarsihy4xlbwa8ibjrp5v3lz9js8yg0gzrfyb861ii4zpbmm9")))

(define-public crate-structural-convert-0.5.0 (c (n "structural-convert") (v "0.5.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xp6ga8f2nxl70k2dkqjg74h4jrdwi9rm0a2f5fbflqwd1d5qmd7")))

(define-public crate-structural-convert-0.5.1 (c (n "structural-convert") (v "0.5.1") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19053gv6dfwqfniawdkd2ibkf6ak5n6m5b5nw068rr400lxp29md")))

(define-public crate-structural-convert-0.6.0 (c (n "structural-convert") (v "0.6.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xmd430n19skjsg60dwkgmimnmr3za3ia9lzavhnbvls6yj0zfhh")))

(define-public crate-structural-convert-0.7.0 (c (n "structural-convert") (v "0.7.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bzzrbda4hmp207aqnwlc4k5f6axgwpz4c55wyqhsxdhj39qpv3q")))

(define-public crate-structural-convert-0.8.0 (c (n "structural-convert") (v "0.8.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0x48sgvrcd35664rkgadbnp03aywhf47f8cp6148jw4mj4zm4sf0")))

(define-public crate-structural-convert-0.8.1 (c (n "structural-convert") (v "0.8.1") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0x1wc8yfvs8krh1jhwa9a191xbk16qxwml7xsaxnndg6h4wycm7n")))

(define-public crate-structural-convert-0.8.2 (c (n "structural-convert") (v "0.8.2") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "076mpffim03isg1s8i5v3r2gb7vrdjv8frpb8b51cz99vbc9lfs0")))

(define-public crate-structural-convert-0.8.3 (c (n "structural-convert") (v "0.8.3") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xh20k5c2fzn7jjppandxydn8cgn4aknz6kwrarzclm2khy5vid8")))

(define-public crate-structural-convert-0.9.0 (c (n "structural-convert") (v "0.9.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14abgivg16qq586nykda0wmbn6ha5pbr6xxjra69ai10dm4l7br3")))

(define-public crate-structural-convert-0.10.0 (c (n "structural-convert") (v "0.10.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0srbxnixh21jpi4vj9bblxd2ckk7d9zqrs4qgzsx91da1dxvc96q")))

(define-public crate-structural-convert-0.11.0 (c (n "structural-convert") (v "0.11.0") (d (list (d (n "structural-convert-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1djw3sb5ns59g0vgf570903a123w5wcbipc4mclbn3nc1h8mbk1p")))

(define-public crate-structural-convert-0.12.0 (c (n "structural-convert") (v "0.12.0") (d (list (d (n "structural-convert-derive") (r "^0.2.0") (d #t) (k 0)))) (h "01ljh4na190bcbjj62z0iglqhkgjwghgckywfrfnrqiv8zvcnxrj")))

(define-public crate-structural-convert-0.13.0 (c (n "structural-convert") (v "0.13.0") (d (list (d (n "structural-convert-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1mhrf82sgnid16nxdl8p1hpk9q18gvh7r3i54sijm6lmlgxb3j77")))

