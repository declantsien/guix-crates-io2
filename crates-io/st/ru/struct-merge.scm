(define-module (crates-io st ru struct-merge) #:use-module (crates-io))

(define-public crate-struct-merge-0.1.0 (c (n "struct-merge") (v "0.1.0") (d (list (d (n "struct-merge-codegen") (r "^0.1") (d #t) (k 0)))) (h "0cjj1dfnqsymyl9szp45k9lnxhn4l5fyvris09h3x4la36sibbgp")))

(define-public crate-struct-merge-0.1.1 (c (n "struct-merge") (v "0.1.1") (d (list (d (n "struct-merge-codegen") (r "^0.1") (d #t) (k 0)))) (h "1fsnqbfi7xjngmz21r17yaf7ywqij3shfff9zgz3nd91s4j8fqx7")))

