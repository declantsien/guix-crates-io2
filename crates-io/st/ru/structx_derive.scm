(define-module (crates-io st ru structx_derive) #:use-module (crates-io))

(define-public crate-structx_derive-0.1.0 (c (n "structx_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "08c8v9l5wpl9zs2kfq8j7j6jhrnxvzb6069d69v32gnhkmw27p07")))

(define-public crate-structx_derive-0.1.1 (c (n "structx_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "06h31p9nmlvs58hlwq5i31z2hry6g45l196dm2bv0vfqg49m63sp")))

(define-public crate-structx_derive-0.1.2 (c (n "structx_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "19a3i2xp29s6j8i8014zik081nlxyzhiskakaxqsjdpfv2dd2p4q")))

(define-public crate-structx_derive-0.1.3 (c (n "structx_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "15i7v1l28q1y4zdh9zj26039w7am39kbczjb0p50x5cdzg14xfm4")))

(define-public crate-structx_derive-0.1.4 (c (n "structx_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0kbrs9gl1bdlimhf72pzm97rg2vp28hqanqww7gpvisa8aarg47f")))

