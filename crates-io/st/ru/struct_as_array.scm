(define-module (crates-io st ru struct_as_array) #:use-module (crates-io))

(define-public crate-struct_as_array-0.1.0 (c (n "struct_as_array") (v "0.1.0") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "02vcvkr97j14472kwxldynrr1n83cy8q82bs20jmzj44mqwx3q2x")))

(define-public crate-struct_as_array-0.1.1 (c (n "struct_as_array") (v "0.1.1") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "114ds9ci46wx3chq1fik43sz0zxcn0mx01qrapvp965jc5pvvg2k")))

(define-public crate-struct_as_array-0.2.0 (c (n "struct_as_array") (v "0.2.0") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "14wzfjblva1mn22i0nj7dx14ps7j4q35nswv3x3257f0grj1rzwf")))

