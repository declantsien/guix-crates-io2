(define-module (crates-io st ru struct_scheme) #:use-module (crates-io))

(define-public crate-struct_scheme-0.0.1 (c (n "struct_scheme") (v "0.0.1") (d (list (d (n "derive") (r "^0.1.0") (d #t) (k 0)))) (h "0bah5hm8h33r3n47q8nzrc50kba3k3fgz5adpk90wklcvdicx5mp") (y #t)))

(define-public crate-struct_scheme-0.0.3 (c (n "struct_scheme") (v "0.0.3") (d (list (d (n "struct-scheme-derive-macros") (r "^0.1.3") (d #t) (k 0)))) (h "0f22zabgg5g2qawdwh3dhvng37gfqq995sc7pb0sn3mkgsw1gyls")))

