(define-module (crates-io st ru structmapper) #:use-module (crates-io))

(define-public crate-structmapper-0.1.0 (c (n "structmapper") (v "0.1.0") (d (list (d (n "structmapper-codegen") (r "^0.1") (d #t) (k 0)))) (h "0k192bvb2p9a9f3c9ka8mdza3yv4y6543ajllpdjbpcirbihwybb")))

(define-public crate-structmapper-0.2.0 (c (n "structmapper") (v "0.2.0") (d (list (d (n "structmapper-codegen") (r "^0.2") (d #t) (k 0)))) (h "07s9vpinp0p3vjli6yscnmfcqg1dfqnicm949qs7r7009yk4lh00")))

(define-public crate-structmapper-0.2.1 (c (n "structmapper") (v "0.2.1") (d (list (d (n "structmapper-codegen") (r "^0.2") (d #t) (k 0)))) (h "11myq7qz4fp709bzr37gf1y1bvkr4lrpngq2f1lhh115ndi43lrr")))

(define-public crate-structmapper-0.3.0 (c (n "structmapper") (v "0.3.0") (d (list (d (n "structmapper-codegen") (r "^0.2") (d #t) (k 0)))) (h "1364sasiv7qdz8bwg536q7h38vr3qhp3drb5hrvspswdrnjacfxb") (y #t)))

(define-public crate-structmapper-0.3.1 (c (n "structmapper") (v "0.3.1") (d (list (d (n "structmapper-codegen") (r "^0.3.1") (d #t) (k 0)))) (h "1rkh6x8b1frkksc9l20m7ab44ff99diq03pv7c6r6yn6lda57qn3")))

(define-public crate-structmapper-0.4.0 (c (n "structmapper") (v "0.4.0") (d (list (d (n "structmapper-codegen") (r "^0.4.0") (d #t) (k 0)))) (h "0g85838g5x8fkfsnfl6m9hr2p400pnn9ib4bws9mi349wv4f7p0i")))

(define-public crate-structmapper-0.4.1 (c (n "structmapper") (v "0.4.1") (d (list (d (n "structmapper-codegen") (r "^0.4.1") (d #t) (k 0)))) (h "0bmr7fhzib9jr5jkvlj9mjvx6afpbdxypd7gc0m2g1w9qx5gfnfz")))

(define-public crate-structmapper-0.4.2 (c (n "structmapper") (v "0.4.2") (d (list (d (n "structmapper-codegen") (r "^0.4.2") (d #t) (k 0)))) (h "0x2rgwzx6jjfqxd4qba3lirx9ncyiv2cdxrkrwxlqcxrv22a97cz")))

