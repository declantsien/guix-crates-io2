(define-module (crates-io st ru structured-spawn) #:use-module (crates-io))

(define-public crate-structured-spawn-1.0.0 (c (n "structured-spawn") (v "1.0.0") (d (list (d (n "futures-concurrency") (r "^7.3.0") (d #t) (k 2)) (d (n "pin-project") (r "^1.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ci8hydp4qcl98xnzgyj0jc9hbs9v9l2npzydcs1khy6yiwjfi2a")))

(define-public crate-structured-spawn-1.0.1 (c (n "structured-spawn") (v "1.0.1") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 2)) (d (n "futures-concurrency") (r "^7.3.0") (d #t) (k 2)) (d (n "pin-project") (r "^1.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1msrski65sg4hsv1fjpd8cwkyq29agcmizg34iqr41nx3cq4jmh1")))

