(define-module (crates-io st ru structural-shapes) #:use-module (crates-io))

(define-public crate-structural-shapes-0.1.0 (c (n "structural-shapes") (v "0.1.0") (h "056yfzx72qlxfp18pyjpj7lqymkkxf4dhjjj4ywr3sxfxb1inias")))

(define-public crate-structural-shapes-0.1.1 (c (n "structural-shapes") (v "0.1.1") (h "04w6vkac3jjy1mv7aw3jxgvbl38rfl5jk511iq1wpid2mxmyrik3")))

(define-public crate-structural-shapes-0.1.2 (c (n "structural-shapes") (v "0.1.2") (h "1h919qls35d419nnrxcchq8sa8rlagq82cfkv8b8gjfrvw7pgx70")))

(define-public crate-structural-shapes-0.1.3 (c (n "structural-shapes") (v "0.1.3") (h "13ski5q5zv0spmr4hvw4krbmqj0mhzqvwv2qmjib557190q7zzhz")))

(define-public crate-structural-shapes-0.1.4 (c (n "structural-shapes") (v "0.1.4") (h "1cljmq5kpa2s97dh4lbn33w25iphzqhhkzqjlflwfyk6yf3qz5jz")))

(define-public crate-structural-shapes-0.1.5 (c (n "structural-shapes") (v "0.1.5") (h "0na5caqmp6w23x39zdg5aqq1w8qldp2c80ifs1qgpc16k1w5s22g")))

(define-public crate-structural-shapes-0.2.0 (c (n "structural-shapes") (v "0.2.0") (h "1wzbb6njm41gzx5bxjd795l83xzrsvygl99y5gpg3wng6br2d49h")))

(define-public crate-structural-shapes-0.2.1 (c (n "structural-shapes") (v "0.2.1") (h "153msw9y9ccgpprh196rc7xprpsmj1za86wf4365dh123kvh37ag")))

(define-public crate-structural-shapes-0.2.2 (c (n "structural-shapes") (v "0.2.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "typenum") (r "^1.15.0") (d #t) (k 0)) (d (n "uom") (r "^0.31.1") (d #t) (k 0)))) (h "0q9rajdfb16pb146gxbyjq58v3gpsh2sj9q5cvxn63n1mama9xq6")))

(define-public crate-structural-shapes-0.2.3 (c (n "structural-shapes") (v "0.2.3") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "typenum") (r "^1.15.0") (d #t) (k 0)) (d (n "uom") (r "^0.34.0") (d #t) (k 0)))) (h "0vnvyyw5m99avsj3i35nmy4my77wad2pbhk4zzjbjark4v0gs2nj")))

