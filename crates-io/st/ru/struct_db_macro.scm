(define-module (crates-io st ru struct_db_macro) #:use-module (crates-io))

(define-public crate-struct_db_macro-0.1.0 (c (n "struct_db_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19zmdv2wmmjxwsngb109dcla7y0g13i66qfrpbkrjkrz1a12yrmk")))

(define-public crate-struct_db_macro-0.2.0 (c (n "struct_db_macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12bqw4rmy88q80p4jcyvgwqal7bz1fak9qiz70f7hhsxaylcadqy")))

(define-public crate-struct_db_macro-0.3.0 (c (n "struct_db_macro") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02204iiidqjr3rlknj4rn96kd2ms1l6qh2s12s04n8gar9kpa7w7")))

(define-public crate-struct_db_macro-0.3.1 (c (n "struct_db_macro") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13idr4s1yymilcfv5wkwiwd0qxkkn0g924zrjk2h3q6pg92knakk")))

(define-public crate-struct_db_macro-0.3.2 (c (n "struct_db_macro") (v "0.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bml8g8d9gbppqn5rc9wwlzvk2clb1549hnn5hkdkqyyqrfc58ax") (y #t)))

(define-public crate-struct_db_macro-0.3.3 (c (n "struct_db_macro") (v "0.3.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07p79cxrzwz0qphgpxc02narybkc9jyiyfh20iv5dzr6d2c2r8ay")))

(define-public crate-struct_db_macro-0.3.4 (c (n "struct_db_macro") (v "0.3.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05kdlj0v4myjqky0klp0384x0gmxmhpkc5iings5l700lghvi57d")))

(define-public crate-struct_db_macro-0.3.5 (c (n "struct_db_macro") (v "0.3.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "146vw8ff6c9pgqcvmh5r0kzrcgc653z9p3bbvmv6cji74h26gn4a")))

(define-public crate-struct_db_macro-0.3.6 (c (n "struct_db_macro") (v "0.3.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sb73dncrcfgv7vr96b8mh2wcby1zwzkp3aiij5ys0d70lbxlbpk")))

(define-public crate-struct_db_macro-0.3.7 (c (n "struct_db_macro") (v "0.3.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "186m85s0c5iaqib4qk275i1550f220cq477xhgc26spw4igs0js8")))

(define-public crate-struct_db_macro-0.3.8 (c (n "struct_db_macro") (v "0.3.8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14nmw86lldjqldwakb4zq8ck5mpxf0yrr738fyayi8h7895rdf34")))

(define-public crate-struct_db_macro-0.3.9 (c (n "struct_db_macro") (v "0.3.9") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ykm149bb8hak262qcwz596a2ihw98z7zq148rgj5s06bnaahwv4")))

(define-public crate-struct_db_macro-0.3.10 (c (n "struct_db_macro") (v "0.3.10") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dpm1q3w2njznsz1p3y56cjvki67kpr6abfhamqpn9ab2y7njsh7")))

(define-public crate-struct_db_macro-0.3.11 (c (n "struct_db_macro") (v "0.3.11") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xfsjp9r5cqa5jvczg6xylqpmpsj8rxlzz8ajp3md7af7lhm5zvn")))

(define-public crate-struct_db_macro-0.3.12 (c (n "struct_db_macro") (v "0.3.12") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y33xj2wjx5slcf0qwz2rk86lgr0dhipn4sd59rsph99qi331qsf")))

(define-public crate-struct_db_macro-0.3.13 (c (n "struct_db_macro") (v "0.3.13") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04nm7gs0ip367c68hmvhpp7kfvqnb6k7qfgi7wsjpa29a8pca6i5")))

(define-public crate-struct_db_macro-0.4.0 (c (n "struct_db_macro") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00m85y6y8wwx3anjb64dhic51dpiy9b8bsqm4i1g8vadqrsakf56")))

(define-public crate-struct_db_macro-0.4.1 (c (n "struct_db_macro") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xgwqavy08f51pzpjibzal8lqp29ik83phs4yqa4ipmzl9iwgk30")))

(define-public crate-struct_db_macro-0.4.2 (c (n "struct_db_macro") (v "0.4.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mi0w8lpkd5srqkmvjcadw9kdyynw0f1c2nyyjy2di09nvd9d11w")))

(define-public crate-struct_db_macro-0.4.3 (c (n "struct_db_macro") (v "0.4.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kjy5lpmag86bxw911c26niwrglq1s3ghchq5mc2ndg14h523y11")))

(define-public crate-struct_db_macro-0.4.4 (c (n "struct_db_macro") (v "0.4.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fph4g4yx6j2w7lk270hccawgrmdi3k35djx9gr8wwz37dhydd9n")))

(define-public crate-struct_db_macro-0.4.5 (c (n "struct_db_macro") (v "0.4.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0628d264jfhk22kk9lhw4nl2xawmaf0kxhy1adarar2kfy5b3739")))

(define-public crate-struct_db_macro-0.4.6 (c (n "struct_db_macro") (v "0.4.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xpp68zcc1zk5g5w51nwn4hxp6hjbdvfm22ndc0b16wjkp1j14rg")))

