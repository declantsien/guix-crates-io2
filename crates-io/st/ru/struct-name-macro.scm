(define-module (crates-io st ru struct-name-macro) #:use-module (crates-io))

(define-public crate-struct-name-macro-0.0.1 (c (n "struct-name-macro") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struct-name") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yjkd0lk44g72gy7wd4gdfmqssm6dl72pnnckclmlbq1ybgvzm6c")))

(define-public crate-struct-name-macro-0.0.2 (c (n "struct-name-macro") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struct-name") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03pisjk794zz6yfr7zpvf6ayn3d2hj6w14s7ynx85zba39wdkq9b")))

