(define-module (crates-io st ru structopt-toml-derive) #:use-module (crates-io))

(define-public crate-structopt-toml-derive-0.1.0 (c (n "structopt-toml-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "0m3bkh1b8v4nwpgw0vfb330fqq360z6szygznczv5fxlfb0b26az")))

(define-public crate-structopt-toml-derive-0.1.1 (c (n "structopt-toml-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "1i9h837i0mlhfra8327p52p7xv3w3fv84iwa53wacnq8a3l40lyq")))

(define-public crate-structopt-toml-derive-0.1.2 (c (n "structopt-toml-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "120d6v1549s01868c9vj0gpkwkmqy3c6d9mzb81gzipj2kyc8z04")))

(define-public crate-structopt-toml-derive-0.2.0 (c (n "structopt-toml-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "1yvbz5xwmyhpmkvrj9qy3gqpadhs359ysiq1b216sdzh26hk0j1x")))

(define-public crate-structopt-toml-derive-0.2.1 (c (n "structopt-toml-derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "0firjfp6pm59jlyh7kp47cf9psbwh00ycva8syinnk4cm761h0nc")))

(define-public crate-structopt-toml-derive-0.2.2 (c (n "structopt-toml-derive") (v "0.2.2") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "038wcq4asjv0qfzjyqbmk9wy91clkx5kp4fbplg62vsrd7k6xx9a")))

(define-public crate-structopt-toml-derive-0.2.3 (c (n "structopt-toml-derive") (v "0.2.3") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "18kq7fy4xhdd36phvyk97hw7pz0sglk49fy455kr5y2d90kgyxhn")))

(define-public crate-structopt-toml-derive-0.2.4 (c (n "structopt-toml-derive") (v "0.2.4") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "1yzcv2d2ss39qyijjp3xzlblv85nfmfa82bqg0mw6g20lsg031az")))

(define-public crate-structopt-toml-derive-0.3.0 (c (n "structopt-toml-derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "0lphi2q3wk4m32az1c3zyjh8vk80pdilj1k1mwphb9wvmvpcx8wi")))

(define-public crate-structopt-toml-derive-0.4.0 (c (n "structopt-toml-derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "1lv50xma8d115r9357jg88pf0bhgq834m1wnbrlbjvlmyhmnbiaw")))

(define-public crate-structopt-toml-derive-0.4.1 (c (n "structopt-toml-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0kdi49ayi0n6plpiwgzqa7s5hv9n0486n9dlg5qjd3bw64dq7yv8")))

(define-public crate-structopt-toml-derive-0.4.2 (c (n "structopt-toml-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1z56ggps25dyx1v87jlcf9w3fdd407zsg8g6qpngzp49zzf71ydi")))

(define-public crate-structopt-toml-derive-0.4.3 (c (n "structopt-toml-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "00w67gfsfd6qcnf3rr1b5qdpvmbjxddhir4mm40dcgj79dgb7p3s")))

(define-public crate-structopt-toml-derive-0.4.4 (c (n "structopt-toml-derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "0mb7ym4dcgdjlq74j3m0rb7y0114smqr0a1zjm6f8zw6bj61h29h")))

(define-public crate-structopt-toml-derive-0.4.5 (c (n "structopt-toml-derive") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "1yaiddpi7aj3wnzbi309ixcrvrcvbd2sjayki6npkicm7ba3q7h5")))

(define-public crate-structopt-toml-derive-0.5.0 (c (n "structopt-toml-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "0gh13m9bz9aw6qilchn4cy61dzlbb8svhxsy7vwvls5gcfjh4qri")))

(define-public crate-structopt-toml-derive-0.5.1 (c (n "structopt-toml-derive") (v "0.5.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "1bvdgn11b7c7106b0w6ayxr1gl8r2ab5xkphq8z2mlkqj6s5fv11")))

