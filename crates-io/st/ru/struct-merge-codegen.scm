(define-module (crates-io st ru struct-merge-codegen) #:use-module (crates-io))

(define-public crate-struct-merge-codegen-0.1.0 (c (n "struct-merge-codegen") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10gfvgxbpzf9xqxq1xc8qv8480vk83b2agvgjjvwlg44dg4k0svb")))

(define-public crate-struct-merge-codegen-0.1.1 (c (n "struct-merge-codegen") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19ql95zz19v4rnl2ga9118rc0lzqj9pqxkfcpp2mgj58wmbfwvcf")))

