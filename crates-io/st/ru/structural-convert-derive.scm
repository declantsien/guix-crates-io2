(define-module (crates-io st ru structural-convert-derive) #:use-module (crates-io))

(define-public crate-structural-convert-derive-0.1.0 (c (n "structural-convert-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hgfgy8xsxk7xym67b5hf5fbc3gmipcvddx46y0krn7fqzig2lf7")))

(define-public crate-structural-convert-derive-0.2.0 (c (n "structural-convert-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pb9smcfl677lsgzz81vvxfg4yd55dyla7qars6jn2nr45w0nslk")))

(define-public crate-structural-convert-derive-0.3.0 (c (n "structural-convert-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16cx3iv1bwfby81lzkq4vpz8yl4fyj3rs93gwk466wc2b17kwnva")))

