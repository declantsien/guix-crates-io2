(define-module (crates-io st ru struct_field) #:use-module (crates-io))

(define-public crate-struct_field-0.1.0 (c (n "struct_field") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ypr2cl92anmvr7haphwacki82qgzccc0nnv9vhw5pagga5acmdb")))

(define-public crate-struct_field-0.1.1 (c (n "struct_field") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gw5mn20b6x54d08wbw16wdwwpgmm0yxcjj9gfpar3krn5lk5k9a")))

(define-public crate-struct_field-0.1.2 (c (n "struct_field") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13lq343hncg8cimnhrs48mshx1lcbjlpbvv56gcnirfz48cm5i0i")))

(define-public crate-struct_field-0.1.3 (c (n "struct_field") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struct_field_names") (r "^0.2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06vb73v2pxxinf8lkxcmma7h9g82r2da18plmmy4sd6mvqb28ix0")))

(define-public crate-struct_field-0.1.4 (c (n "struct_field") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struct_field_names") (r "^0.2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vxyiqmm82wragkxkb1dhi7kl8lnpf01l6afkqwrx8g97qcijx9v")))

(define-public crate-struct_field-0.1.5 (c (n "struct_field") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struct_field_names") (r "^0.2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y2x04anichzj34y9fs8196jm6gak47k53n5a6wgisklm23kb6qi")))

(define-public crate-struct_field-0.1.6 (c (n "struct_field") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "struct_field_names") (r "^0.2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1p03vy64icda882sqs91w57pg52i9xg4nz90syzz1xfkgkw97n3b")))

