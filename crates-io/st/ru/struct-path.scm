(define-module (crates-io st ru struct-path) #:use-module (crates-io))

(define-public crate-struct-path-0.1.1 (c (n "struct-path") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)))) (h "0cpkb3magsla12l9xrpr54dfh40npzcgvlk2ig0cdygrg2dlpg5b")))

(define-public crate-struct-path-0.1.2 (c (n "struct-path") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)))) (h "1y9c8sds9i9by3cb5jcbx2h1zjnzrr58afzwclajmg7p8n587372")))

(define-public crate-struct-path-0.1.3 (c (n "struct-path") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)))) (h "0kif0q45z51sxjwzg6qml7sfgx4blf9bx7swyvcg6q7675h1y78y")))

(define-public crate-struct-path-0.1.4 (c (n "struct-path") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)))) (h "0q9hf95v52fyx7l1mmdjqdmynn3yk36hk1jb226853n27k4xdfqi")))

(define-public crate-struct-path-0.2.0 (c (n "struct-path") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)))) (h "1wl0swq4h1dd1l5bqc2wfb738v7jljs2akvp3rz0vqfk9qsp6mr7")))

(define-public crate-struct-path-0.2.2 (c (n "struct-path") (v "0.2.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)))) (h "0xdgmj5x0yj9bass1plbcm5mslkyawavan4wywqh72hpq72cbk9c")))

(define-public crate-struct-path-0.2.3 (c (n "struct-path") (v "0.2.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)))) (h "1iwg2a8bxig26xax848sa0rrapmww6y4nfsrv8z5083krwldz7l9")))

