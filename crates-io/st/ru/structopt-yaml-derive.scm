(define-module (crates-io st ru structopt-yaml-derive) #:use-module (crates-io))

(define-public crate-structopt-yaml-derive-0.4.5 (c (n "structopt-yaml-derive") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "0kn13j7pqm959rd94x3dblrvm3j60lwldj79aa188b8qnz6wcwxx")))

(define-public crate-structopt-yaml-derive-0.4.6 (c (n "structopt-yaml-derive") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "02f63nkb8b27ajzf2ji911h5chyf8bafjd0iwv5bdvy7y4qw4bj6")))

