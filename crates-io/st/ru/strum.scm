(define-module (crates-io st ru strum) #:use-module (crates-io))

(define-public crate-strum-0.1.0 (c (n "strum") (v "0.1.0") (d (list (d (n "strum_macros") (r "^0.1.0") (d #t) (k 2)))) (h "0pdn21gl1j9gxppp07zpfnp5a2dc93r2pd10zwhwc218mxr56vf3")))

(define-public crate-strum-0.2.0 (c (n "strum") (v "0.2.0") (d (list (d (n "strum_macros") (r "^0.2.0") (d #t) (k 2)))) (h "10myji5khb2rgw197yiaagjlg0yz9wbz8s8v0l9l293v016csy6s")))

(define-public crate-strum-0.2.1 (c (n "strum") (v "0.2.1") (d (list (d (n "strum_macros") (r "^0.2.1") (d #t) (k 2)))) (h "14k4pd29p3g68r9vyvkz4kmdlr9087cfr8imzq7xpzgb5vbpj1bj")))

(define-public crate-strum-0.2.2 (c (n "strum") (v "0.2.2") (d (list (d (n "strum_macros") (r "^0.2.2") (d #t) (k 2)))) (h "0s3jpxqis6yg171m0f50gkgfg3kqyfknmnnkm3cc6gb7yxv3sqbc")))

(define-public crate-strum-0.5.0 (c (n "strum") (v "0.5.0") (h "1yng68shck8k8vwb2myh1b4gf8xcgvzidljlyjffdgd53sizgzv1")))

(define-public crate-strum-0.5.1 (c (n "strum") (v "0.5.1") (d (list (d (n "strum_macros") (r "^0.5.0") (d #t) (k 2)))) (h "0nxlin3h1qz79w0zfggn07bxbp9bm53g003g8dx8xphkdx2m6j3p")))

(define-public crate-strum-0.6.0 (c (n "strum") (v "0.6.0") (d (list (d (n "strum_macros") (r "^0.6.0") (d #t) (k 2)))) (h "1likfkwn3nj2qv9jgac52m8jg4c1wisb9zp84h0ryc2dwlij3jbw")))

(define-public crate-strum-0.8.0 (c (n "strum") (v "0.8.0") (d (list (d (n "strum_macros") (r "^0.8.0") (d #t) (k 2)))) (h "1np7ymlq2402l3i6ljxgfdmxxf5akp927zkzahg08zji1xry99jc")))

(define-public crate-strum-0.9.0 (c (n "strum") (v "0.9.0") (d (list (d (n "strum_macros") (r "^0.9.0") (d #t) (k 2)))) (h "0i95aglal34csmizk6rmm7byzdy0lwivgi7qvid7pl3dvnsj37h9")))

(define-public crate-strum-0.10.0 (c (n "strum") (v "0.10.0") (d (list (d (n "strum_macros") (r "^0.10.0") (d #t) (k 2)))) (h "02xxmw2q0dsfzg1x05a6igcf1m3zv0xa2icdkpwfb7pa343xcf4g")))

(define-public crate-strum-0.11.0 (c (n "strum") (v "0.11.0") (d (list (d (n "strum_macros") (r "^0.11.0") (d #t) (k 2)))) (h "0vk7fyqvlnpwnh11s4ckivy0vl7x3x68r035yi46maqr2l3s5hzn")))

(define-public crate-strum-0.12.0 (c (n "strum") (v "0.12.0") (d (list (d (n "strum_macros") (r "^0.12.0") (d #t) (k 2)))) (h "0ilw8gw0q93v7whjj9zs2m1r6p11jd8gpxp2lja5xkhyisf6yb8g")))

(define-public crate-strum-0.13.0 (c (n "strum") (v "0.13.0") (d (list (d (n "strum_macros") (r "^0.13.0") (d #t) (k 2)))) (h "0hwijrmvkr9k41ldb2rnxhmwii0q294sddkdgarrpzw2qjcgrczn")))

(define-public crate-strum-0.14.0 (c (n "strum") (v "0.14.0") (d (list (d (n "strum_macros") (r "^0.14.0") (d #t) (k 2)))) (h "01i8bcab3k5fp8h4q9xk201yspfscrqb6hsjzzhzqzvfaxgy440q")))

(define-public crate-strum-0.15.0 (c (n "strum") (v "0.15.0") (d (list (d (n "strum_macros") (r "^0.15.0") (d #t) (k 2)))) (h "0pzhc270dvnrs3g1cs64gb9qyipxmpqq05lm0hbhagsk74qc7lg5")))

(define-public crate-strum-0.16.0 (c (n "strum") (v "0.16.0") (d (list (d (n "strum_macros") (r "^0.16.0") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.16.0") (d #t) (k 2)))) (h "08ld32irbvx3z6ndc9a7vfkxcgpsfvyf651kfqs03n8nibwghf31") (f (quote (("derive" "strum_macros"))))))

(define-public crate-strum-0.17.0 (c (n "strum") (v "0.17.0") (d (list (d (n "strum_macros") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.0") (d #t) (k 2)))) (h "1sg6i0ymkk6l0jh4my4cvpysa416gnfvazm3qncnnkhsd7lks8dd") (f (quote (("derive" "strum_macros")))) (y #t)))

(define-public crate-strum-0.17.1 (c (n "strum") (v "0.17.1") (d (list (d (n "strum_macros") (r "^0.17.1") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 2)))) (h "0b3dn24vhbqqnd1pjighlivfn3fjgrg6r4a7wgs15dsk1n1gn3jk") (f (quote (("derive" "strum_macros"))))))

(define-public crate-strum-0.18.0 (c (n "strum") (v "0.18.0") (d (list (d (n "strum_macros") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 2)))) (h "0asjskn1qhqqfiq673np0gvmnd1rsp506m38vk53gi7l93mq3gap") (f (quote (("derive" "strum_macros"))))))

(define-public crate-strum-0.19.0 (c (n "strum") (v "0.19.0") (d (list (d (n "strum_macros") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.0") (d #t) (k 2)))) (h "0vbjwplkcwpcr9mj75gggq7h2pf01c1y5my9snvkqzgr6qlb38gm") (f (quote (("derive" "strum_macros"))))))

(define-public crate-strum-0.19.1 (c (n "strum") (v "0.19.1") (d (list (d (n "strum_macros") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.0") (d #t) (k 2)))) (h "19nzdb2c9wc123axazbxapfcxxlcawh3cq8sa9c3rk37b36pk1sl") (f (quote (("derive" "strum_macros"))))))

(define-public crate-strum-0.19.2 (c (n "strum") (v "0.19.2") (d (list (d (n "strum_macros") (r "^0.19.2") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.2") (d #t) (k 2)))) (h "1gjss4ycnyvl88sfl6aw5gamyyrwcyq7lricjarbg8sx2s6sa91r") (f (quote (("derive" "strum_macros"))))))

(define-public crate-strum-0.19.3 (c (n "strum") (v "0.19.3") (d (list (d (n "strum_macros") (r "^0.19.2") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.2") (d #t) (k 2)))) (h "1k828nx0bgybijrv7bcdl575hv1jzb899wmin3ld2csr548g3601") (f (quote (("derive" "strum_macros"))))))

(define-public crate-strum-0.19.4 (c (n "strum") (v "0.19.4") (d (list (d (n "strum_macros") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.19") (d #t) (k 2)))) (h "16xwq2c9lx5kxrmd2ymzv0q5ag1qaf8ljw12fy0c6psdi1fdhkzz") (f (quote (("derive" "strum_macros"))))))

(define-public crate-strum-0.19.5 (c (n "strum") (v "0.19.5") (d (list (d (n "strum_macros") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.19") (d #t) (k 2)))) (h "1d8i5xwkc2z7z02ibln80z1bmpjhpi9k5ckpljwj0mrvgrm2i6mq") (f (quote (("derive" "strum_macros"))))))

(define-public crate-strum-0.20.0 (c (n "strum") (v "0.20.0") (d (list (d (n "strum_macros") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 2)))) (h "0p5cslmdnz261kiwmm4h7qsmv9bh83r0f9lq6f2z2mxsnl4wa63k") (f (quote (("derive" "strum_macros"))))))

(define-public crate-strum-0.21.0 (c (n "strum") (v "0.21.0") (d (list (d (n "strum_macros") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 2)))) (h "1qnd2by1zrwgx7li0hmwy7jbzjwz1ky697qjg85nga8zzny6py5a") (f (quote (("derive" "strum_macros"))))))

(define-public crate-strum-0.22.0 (c (n "strum") (v "0.22.0") (d (list (d (n "strum_macros") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 2)))) (h "17kjz02z1wwfw1rcfdsmsbpazngnqhz23zhwychql727gly8kb7p") (f (quote (("std") ("derive" "strum_macros") ("default" "std"))))))

(define-public crate-strum-0.23.0 (c (n "strum") (v "0.23.0") (d (list (d (n "strum_macros") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 2)))) (h "1fvhkg7di4psfw289v2flv19i28rcflq1g1z3n2rl76iqy8lpqfa") (f (quote (("std") ("derive" "strum_macros") ("default" "std"))))))

(define-public crate-strum-0.24.0 (c (n "strum") (v "0.24.0") (d (list (d (n "strum_macros") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 2)))) (h "1y77vshrhm1grlgcfmnm0nxpsv0pb5zcb97zy6rbh106nz0wysp9") (f (quote (("std") ("derive" "strum_macros") ("default" "std"))))))

(define-public crate-strum-0.24.1 (c (n "strum") (v "0.24.1") (d (list (d (n "phf") (r "^0.10") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 2)))) (h "0gz6cjhlps5idwasznklxdh2zsas6mxf99vr0n27j876q12n0gh6") (f (quote (("std") ("derive" "strum_macros") ("default" "std"))))))

(define-public crate-strum-0.25.0 (c (n "strum") (v "0.25.0") (d (list (d (n "phf") (r "^0.10") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 2)))) (h "09g1q55ms8vax1z0mxlbva3vm8n2r1179kfvbccnkjcidzm58399") (f (quote (("std") ("derive" "strum_macros") ("default" "std"))))))

(define-public crate-strum-0.26.0 (c (n "strum") (v "0.26.0") (d (list (d (n "phf") (r "^0.10") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 2)))) (h "03kfgifxi4gh8bdq83kzzxp427ilg93k56ckfg3ikk56r0x42z0i") (f (quote (("std") ("derive" "strum_macros") ("default" "std"))))))

(define-public crate-strum-0.26.1 (c (n "strum") (v "0.26.1") (d (list (d (n "phf") (r "^0.10") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 2)))) (h "0przl5xmy1gzf4rlp8ff7wla43slsyk1vlg2xdjsk6nzmpl96fvj") (f (quote (("std") ("derive" "strum_macros") ("default" "std"))))))

(define-public crate-strum-0.26.2 (c (n "strum") (v "0.26.2") (d (list (d (n "phf") (r "^0.10") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 2)))) (h "0aayk2m3cw9zz12qn82kqaayq43xdgdpcy9b5d1lq6d504syr32x") (f (quote (("std") ("derive" "strum_macros") ("default" "std"))))))

