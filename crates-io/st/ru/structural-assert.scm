(define-module (crates-io st ru structural-assert) #:use-module (crates-io))

(define-public crate-structural-assert-0.1.0 (c (n "structural-assert") (v "0.1.0") (d (list (d (n "memoffset") (r "^0.6.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("full"))) (d #t) (k 0)))) (h "01fz8likb6bx9k6dz1xphylqbbahkcjxvlq51g3v6rvdacx51jaj")))

