(define-module (crates-io st ru struct2swagger) #:use-module (crates-io))

(define-public crate-struct2swagger-0.1.0 (c (n "struct2swagger") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0jihd6i3if78l2j238ij31r4vy7rsfvl27q3lcw4nb7kvaz0cyj9")))

(define-public crate-struct2swagger-0.1.1 (c (n "struct2swagger") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1r0d7lxi2i7rkbc830sqdv9c8k4x338qibyys1ljj7v5ia3g3nax")))

(define-public crate-struct2swagger-0.1.2 (c (n "struct2swagger") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "09sya4daqrwfn0z4zk6rpv9fafm4gdndml06857ygszp4s58h5vr")))

(define-public crate-struct2swagger-0.1.3 (c (n "struct2swagger") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1s7f1g2lj21jan5cqdsrv0k4sch8j72986h103v5gn25xyc6pg28")))

(define-public crate-struct2swagger-0.1.5 (c (n "struct2swagger") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "14i8hyga4biwb38qgvqjzmr9yx3gnwpl99qqrkkzr0rf0dwllrph")))

(define-public crate-struct2swagger-0.1.6 (c (n "struct2swagger") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1yxplgb3vx5gr2vs6ml3c01avg3r6y4zbczda1m4q85d8l03y45v")))

