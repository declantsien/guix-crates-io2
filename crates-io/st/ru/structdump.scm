(define-module (crates-io st ru structdump) #:use-module (crates-io))

(define-public crate-structdump-0.1.0 (c (n "structdump") (v "0.1.0") (h "02gla69rzgzfqkgvm63v4qg0asv38bnjarj0b1hg36nsvgai670j")))

(define-public crate-structdump-0.1.1 (c (n "structdump") (v "0.1.1") (h "0cxz4l0pggwpsybwdi9d2p8c6m5ylbpc3hk9720yz3bfkf0v4qkm")))

(define-public crate-structdump-0.1.2 (c (n "structdump") (v "0.1.2") (h "1f19vcqpmp2b2zddgd84zb6fn9v5mvzv84rrs5kxnbrll0ryq5if")))

(define-public crate-structdump-0.2.0 (c (n "structdump") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structdump-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1hbn83aarvx5b9swxvr7xsybcb5ihb0d9c76lvc83wkva0kh6mxh") (f (quote (("derive" "structdump-derive"))))))

