(define-module (crates-io st ru structz-macros) #:use-module (crates-io))

(define-public crate-structz-macros-0.1.0 (c (n "structz-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f5v1xfnvb0vn2bqjkfzgwyjfxqq1igzdvyblgb2qpmcjaz5c540")))

(define-public crate-structz-macros-0.2.0-alpha (c (n "structz-macros") (v "0.2.0-alpha") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sva3yrjykkkij3sf4mn2blrhfldpc7qwc6iqjzsgvrvibj4ablp")))

(define-public crate-structz-macros-0.2.0 (c (n "structz-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0010f4nzqyrdqyf5j1cv8l13m2r3g7ar3hsxbr7mj0n6yp09kpmm")))

