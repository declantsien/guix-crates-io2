(define-module (crates-io st ru structlog) #:use-module (crates-io))

(define-public crate-structlog-0.1.0 (c (n "structlog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0hc8ly0gjf6hanjc35ikd2jgiyaqk2jg7s1fxs0byd0ppdbqjs2x")))

(define-public crate-structlog-0.1.1 (c (n "structlog") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0sqqdps5ganc7kyrwr2y3qa5s6nc9j1rhmhyd785ilcrlcj0rjjq")))

(define-public crate-structlog-0.1.2 (c (n "structlog") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1kbifx8isx5pia2087g6ddswws6aqc9jswz0nnwpmvij36ldivdl")))

(define-public crate-structlog-0.1.3 (c (n "structlog") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d0095s004yh6wjngdvp108jbfxn4y8c3iy2x376bv656y0hpics")))

(define-public crate-structlog-0.1.5 (c (n "structlog") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zk5hx5wjsbw6h6aqnk07vb38b6iv6nm455r4l5gjpi9ndhiw4vb")))

(define-public crate-structlog-0.1.6 (c (n "structlog") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rjrb5y4y61gn5abslcjlbbc3yw5zbb8l2mg5dbs4am3rrzb0997")))

(define-public crate-structlog-0.1.7 (c (n "structlog") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jp2cnmiahk04fkr2mzk18pyl20vayg82mavrfic70dkmr2a4cm2")))

(define-public crate-structlog-0.1.8 (c (n "structlog") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zfck8yl7j83i3mvz0xjaxy73hjaaxq36xad6flsl40l5znqdp9h")))

