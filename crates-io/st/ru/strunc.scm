(define-module (crates-io st ru strunc) #:use-module (crates-io))

(define-public crate-strunc-0.1.0 (c (n "strunc") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "135a7jkl32l58w0vzm88f5bzknjas9jmf8iy4q7ghi3qlas5w2l4")))

(define-public crate-strunc-0.1.1 (c (n "strunc") (v "0.1.1") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0kgnl5w9vhlw3paimmqa25gn6zfzfyqwnhagzs86f03i3gcaxcjx")))

