(define-module (crates-io st ru structures) #:use-module (crates-io))

(define-public crate-structures-1.0.0 (c (n "structures") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "len-trait") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "str-macro") (r "^1.0.0") (d #t) (k 0)))) (h "1g6wkja6vbaribdmb0pxrd4qzhj7753a54y1116g6jz0c7l1pnj9") (y #t)))

(define-public crate-structures-1.0.1 (c (n "structures") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "len-trait") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "str-macro") (r "^1.0.0") (d #t) (k 0)))) (h "0qjfdyys6f223b5q636fc27j0vn8rh81ls0mvjkshi719hxp3zp6") (y #t)))

(define-public crate-structures-1.0.2 (c (n "structures") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "len-trait") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "str-macro") (r "^1.0.0") (d #t) (k 0)))) (h "1b0xjswacvxg40wvkjkd7iy5nd08fckksgi55bqfgp1r2wvg9vry")))

(define-public crate-structures-1.1.0 (c (n "structures") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "len-trait") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "str-macro") (r "^1.0.0") (d #t) (k 0)))) (h "076wym5z1vidmld86phzwh5q268rkx46d711wbyc1apnq2mmyc0w")))

