(define-module (crates-io st ru structmeta) #:use-module (crates-io))

(define-public crate-structmeta-0.1.0 (c (n "structmeta") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "structmeta-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (d #t) (k 0)))) (h "0il2rrm6p8cv7cfx2lh5mafp7xd5k6ywsgkkbxbqc2w3mnvp77r7")))

(define-public crate-structmeta-0.1.1 (c (n "structmeta") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "structmeta-derive") (r "=0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (d #t) (k 0)))) (h "0cpxsam5scpi9k3dlffkrcafljdd92nwrn4rwsbfz4a5lx1yx3ds")))

(define-public crate-structmeta-0.1.2 (c (n "structmeta") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "structmeta-derive") (r "=0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (d #t) (k 0)))) (h "09xipm9xdh77cfbvcxjpk3kp4la66k2k9a3lly90b0gsf0wd0k3l")))

(define-public crate-structmeta-0.1.3 (c (n "structmeta") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "structmeta-derive") (r "=0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (f (quote ("full"))) (d #t) (k 2)))) (h "02kjbbrrkkzhkzkairq3m1xrk1rzd917iaafwwgksvh3zm940nxm")))

(define-public crate-structmeta-0.1.4 (c (n "structmeta") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "structmeta-derive") (r "=0.1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (f (quote ("full"))) (d #t) (k 2)))) (h "0wdcqg9nzjq11hxj3rixhvwzj3xvyxjwwshsz8xjwvw9i995p4ar")))

(define-public crate-structmeta-0.1.5 (c (n "structmeta") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "structmeta-derive") (r "=0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 2)))) (h "1qyjwgsgllwgi8f9yglv153pr7k81ihrmnc7rg1b57x8b8aw5n8v")))

(define-public crate-structmeta-0.1.6 (c (n "structmeta") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.24") (d #t) (k 0)) (d (n "structmeta-derive") (r "=0.1.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "full"))) (d #t) (k 2)))) (h "0alyl12b7fab8izrpliil73sxs1ivr5vm0pisallmxlb4zb44j0h")))

(define-public crate-structmeta-0.2.0 (c (n "structmeta") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "structmeta-derive") (r "=0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("extra-traits" "full"))) (d #t) (k 2)))) (h "0bcj4c2p2j091mn9ld2hbcx77flqjx65ihb9gbb5c12gal4rxbbq")))

(define-public crate-structmeta-0.3.0 (c (n "structmeta") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "structmeta-derive") (r "=0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "full"))) (d #t) (k 2)))) (h "0afk0s9paazsvyvsirxvbnqp3blhdck3fmfhdw7xf209skc7a59f")))

