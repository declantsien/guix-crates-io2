(define-module (crates-io st ru structured-output) #:use-module (crates-io))

(define-public crate-structured-output-0.1.0 (c (n "structured-output") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mipw4qvw0yjbm2vra6rkyh7bnry7l2pnbqga6i3444kpabdrn63")))

(define-public crate-structured-output-0.1.1 (c (n "structured-output") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zhyfpkx9pc817qdqrpf7wg5s2f20rvlx853jhh1x11nv0wyf2ka")))

