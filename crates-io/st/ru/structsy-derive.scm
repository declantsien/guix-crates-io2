(define-module (crates-io st ru structsy-derive) #:use-module (crates-io))

(define-public crate-structsy-derive-0.1.0 (c (n "structsy-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0c03wmfs052c5i69wxy59g146awssg7hb5prqqz364lk3qy6bz4s")))

(define-public crate-structsy-derive-0.2.0 (c (n "structsy-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0ya2g4gr1md3km8f46spa7467zv6chhrv7whr05hj91q3b71291s")))

(define-public crate-structsy-derive-0.3.0 (c (n "structsy-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1c21qhjgjc9adq9cw58q9ybkaxwjhy5iy9z8pd3k466aayjw6q7x")))

(define-public crate-structsy-derive-0.4.0 (c (n "structsy-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1nrg6xd8dfs7wl9ydanzsxpkigljrlvpq0i1s96nqk0jzl6l82pd")))

(define-public crate-structsy-derive-0.5.0 (c (n "structsy-derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1h74hzbfd9ikgbgsx7qgd5fz8c28xvgn2k4qg97d896zq809q3hf")))

(define-public crate-structsy-derive-0.5.2 (c (n "structsy-derive") (v "0.5.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1ych6hkc22hn6awp67ynfw2ml656pjnwlqam6vdy9qcfbfzccr20")))

