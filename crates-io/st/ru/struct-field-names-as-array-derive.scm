(define-module (crates-io st ru struct-field-names-as-array-derive) #:use-module (crates-io))

(define-public crate-struct-field-names-as-array-derive-0.3.0 (c (n "struct-field-names-as-array-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0f097br4bw19lx7yc3xvqjvgmgc3n8i1i8bin55hxqiwgyszinx2")))

