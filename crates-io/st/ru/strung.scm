(define-module (crates-io st ru strung) #:use-module (crates-io))

(define-public crate-strung-0.1.0 (c (n "strung") (v "0.1.0") (d (list (d (n "strung_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "11b356llk5fz2m9s55qrnyk7zpyw3r4lcv79ibv7xfbqlcaq4w8f") (s 2) (e (quote (("default" "dep:strung_derive"))))))

(define-public crate-strung-0.1.1 (c (n "strung") (v "0.1.1") (d (list (d (n "strung_derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "12jrwhqymhdpjcixln8ajbdql25w28cj1n3l65g3qlarps8bspfm") (s 2) (e (quote (("default" "dep:strung_derive"))))))

(define-public crate-strung-0.1.2 (c (n "strung") (v "0.1.2") (d (list (d (n "strung_derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "040vhizxmnfcnba07z8d70vrl5pd9qk4fa5mdb70fps9gvnjxp6w") (s 2) (e (quote (("default" "dep:strung_derive"))))))

(define-public crate-strung-0.1.3 (c (n "strung") (v "0.1.3") (d (list (d (n "strung_derive") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0ayjw7qjcxkdz9dxw05vrxcif3m9zznlkp3ywa3rmd2q22s0clws") (s 2) (e (quote (("default" "dep:strung_derive"))))))

(define-public crate-strung-0.1.4 (c (n "strung") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "strung_derive") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0y343r8k8swxz05m3rdr091pq77s1cv2djnps37xs6mv8v1g3sqq") (s 2) (e (quote (("default" "dep:strung_derive"))))))

