(define-module (crates-io st ru structured_persistent_logger) #:use-module (crates-io))

(define-public crate-structured_persistent_logger-0.1.0 (c (n "structured_persistent_logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("kv_unstable_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "01w3pf755r10zkrxg0ng1kpxfawgp85zsgky68bsxp5xv30czjwa")))

