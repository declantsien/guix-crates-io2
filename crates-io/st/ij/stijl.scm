(define-module (crates-io st ij stijl) #:use-module (crates-io))

(define-public crate-stijl-0.1.0 (c (n "stijl") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)))) (h "1xk9j0bkapqa9qmqqn1kb8s9i9h46vn2ilswjddl5wivqnayk3zc")))

(define-public crate-stijl-0.2.0 (c (n "stijl") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)))) (h "06hrdd55gx51i7vg2fry7q25l8ayn28wyp45qqawqrwpcc8qcksk")))

(define-public crate-stijl-0.2.1 (c (n "stijl") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)))) (h "0gd667d8qf2268fdfchqrw7l0p77w0sihl9z810ni604hc52xx1s")))

(define-public crate-stijl-0.2.2 (c (n "stijl") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)))) (h "0b0lsbi9rlzad292m6haa1gljrps0mpblj8q4wyzipzbh0a5j35n")))

(define-public crate-stijl-0.3.0 (c (n "stijl") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)))) (h "1xrhh2h6imak0b8cf34dkz1d3c5s9sar4y74za7mx4d13ds48sjj")))

(define-public crate-stijl-0.4.0 (c (n "stijl") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)))) (h "0zbx04r7kg8b60366yjrd6k697d1vjizvb3bfqpqrsdl8fa4qawm")))

(define-public crate-stijl-0.4.1 (c (n "stijl") (v "0.4.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)))) (h "1n8wyvnl0db0jic5k1qrxq5i9ppn4xdiv20sg8j12ms6aawy45vi")))

(define-public crate-stijl-0.4.2 (c (n "stijl") (v "0.4.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)))) (h "1f4lp728ch34ryal2cjy0pv9a48a2x6yx5ha9q09zpxkbd44f7a9")))

(define-public crate-stijl-0.4.3 (c (n "stijl") (v "0.4.3") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "06im8lby8qwbryriwhysyi56v9kn14z72pyyh7qbvaq7np7v2l1h")))

(define-public crate-stijl-0.5.0 (c (n "stijl") (v "0.5.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0v9w8nvkl527w4qspl9s5w9ndjw20gifimvhrnc8jrmsxf3x599n")))

(define-public crate-stijl-0.5.1 (c (n "stijl") (v "0.5.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.32") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0m36cyrjing4v6wb5nxqq00bib286d50hn39axy58p8hb64kg0fs")))

(define-public crate-stijl-0.5.2 (c (n "stijl") (v "0.5.2") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.32") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0kcyds1m9p0pbypvmh78rfc4ddh7rcc4b9rz221h565sh1cdd309")))

