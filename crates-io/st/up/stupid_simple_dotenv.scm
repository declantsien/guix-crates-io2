(define-module (crates-io st up stupid_simple_dotenv) #:use-module (crates-io))

(define-public crate-stupid_simple_dotenv-0.1.0 (c (n "stupid_simple_dotenv") (v "0.1.0") (h "0r9r1hc32im0pvq6mqn8kikzs8jbc6yvs773pl5kkg996fi7j4va")))

(define-public crate-stupid_simple_dotenv-0.1.1 (c (n "stupid_simple_dotenv") (v "0.1.1") (h "0m91d1xaj90ww5iq73fnrlxsnldg4cgwdlyhqnjxwmaz1l1cgapk")))

(define-public crate-stupid_simple_dotenv-0.2.0 (c (n "stupid_simple_dotenv") (v "0.2.0") (h "171hbz3nz2d69lwf6g8q8r5mq3dg3rc93xvyb27q2x7bylba25jz")))

(define-public crate-stupid_simple_dotenv-0.2.1 (c (n "stupid_simple_dotenv") (v "0.2.1") (h "07spv7dfnghyy2mnjrbifwf4gv3md8kfpxg6vz65nv9zwp9192df")))

(define-public crate-stupid_simple_dotenv-0.2.3 (c (n "stupid_simple_dotenv") (v "0.2.3") (h "0kmvac28s7pm30ccnypyjb0b5cfza23y2dgf0lf31g74zqlb4w0r")))

(define-public crate-stupid_simple_dotenv-0.2.4 (c (n "stupid_simple_dotenv") (v "0.2.4") (h "16yy4cymnpxdcmm0g31iksjyjl8ri7qlmxqbshzl0a753wdfjh6n")))

