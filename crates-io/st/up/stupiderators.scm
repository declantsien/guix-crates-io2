(define-module (crates-io st up stupiderators) #:use-module (crates-io))

(define-public crate-stupiderators-0.1.0 (c (n "stupiderators") (v "0.1.0") (h "1yqfka6z3npcb8cld1yiqc2z9k14sbqbzinyydrgc1cf5qjxafd4")))

(define-public crate-stupiderators-0.2.0 (c (n "stupiderators") (v "0.2.0") (h "0lplpw7l0kffid6ik30fdfckbm6wb17rs6x8r5ib6bbqcpzn429v")))

(define-public crate-stupiderators-0.2.1 (c (n "stupiderators") (v "0.2.1") (h "0836ljghm8qlhfrgql5cqdsbj4nsill4lvhibyaw4plz5cff4g9x")))

