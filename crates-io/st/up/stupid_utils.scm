(define-module (crates-io st up stupid_utils) #:use-module (crates-io))

(define-public crate-stupid_utils-0.1.0 (c (n "stupid_utils") (v "0.1.0") (h "0rxl8xj4l9qcn5hklhwhxbnf83nd2v7psxq8lf440xknn22lyz2f")))

(define-public crate-stupid_utils-0.1.1 (c (n "stupid_utils") (v "0.1.1") (h "0f5a48q06cl74mdhfiiy2g9v1wkkz5vl4wjvgrbpscy2wsf2vxwi")))

(define-public crate-stupid_utils-0.2.1 (c (n "stupid_utils") (v "0.2.1") (h "0asd5mrgk76ny4spd3pncgn99br0nz076ma6sch607klwypyjcc8")))

(define-public crate-stupid_utils-0.2.2 (c (n "stupid_utils") (v "0.2.2") (h "19nj31z8i5i8spn70yncrilamz2rxc47s9k1y1is2c5cgqzl3zqv")))

(define-public crate-stupid_utils-0.2.3 (c (n "stupid_utils") (v "0.2.3") (h "1xarcqywh8mn4i3jf7g7caqz47yc3i7jxkx0d7y58nzilmrw66if")))

(define-public crate-stupid_utils-0.2.4 (c (n "stupid_utils") (v "0.2.4") (h "0srw1j7y1f9yxsnxzdh4yjwnr28ylk2njz8gmbd190m52bgxqlc3")))

(define-public crate-stupid_utils-0.2.5 (c (n "stupid_utils") (v "0.2.5") (h "0j97smdql3r0ls019rqh4a3w3vxrsim8zszy6v20hr4byb55cpks")))

(define-public crate-stupid_utils-0.2.6 (c (n "stupid_utils") (v "0.2.6") (h "0xx4816djg6c1q0sm117xxnd57vsr83ynqnbkn0p6zh7pi027fji")))

