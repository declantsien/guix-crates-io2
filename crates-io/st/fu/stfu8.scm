(define-module (crates-io st fu stfu8) #:use-module (crates-io))

(define-public crate-stfu8-0.1.0 (c (n "stfu8") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4.1") (d #t) (k 2)) (d (n "proptest") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "regex_generate") (r "^0.2.0") (d #t) (k 2)))) (h "0n5lw7id7frqpn8i2j3akxfz66x5s34rzl9yy80fbrj1ai7yd2r9")))

(define-public crate-stfu8-0.1.1 (c (n "stfu8") (v "0.1.1") (d (list (d (n "lazy_static") (r ">= 1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4.1") (d #t) (k 2)) (d (n "proptest") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "regex") (r ">= 0.2.0") (d #t) (k 0)) (d (n "regex_generate") (r "^0.2.0") (d #t) (k 2)))) (h "09s5n8idrs27rgnh0yhyd5d0k26wwg45rv3iz8s9y4l94b9wxyyw")))

(define-public crate-stfu8-0.2.0 (c (n "stfu8") (v "0.2.0") (d (list (d (n "lazy_static") (r ">= 1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4.1") (d #t) (k 2)) (d (n "proptest") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "regex") (r ">= 0.2.0") (d #t) (k 0)) (d (n "regex_generate") (r "^0.2.0") (d #t) (k 2)))) (h "02w1d8bffw70db26a1y4dcxnv7vpcy3277f0in5jqm6wh0djnnhg") (f (quote (("testing") ("default" "testing"))))))

(define-public crate-stfu8-0.2.1 (c (n "stfu8") (v "0.2.1") (d (list (d (n "lazy_static") (r ">= 1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4.1") (d #t) (k 2)) (d (n "proptest") (r "^0.3.4") (d #t) (k 2)) (d (n "regex") (r ">= 0.2.0") (d #t) (k 0)))) (h "0n91l0vsm0gkf342bwawhagprfv28li3cljbx7a6jwg79ak5598p") (f (quote (("testing") ("default" "testing"))))))

(define-public crate-stfu8-0.2.2 (c (n "stfu8") (v "0.2.2") (d (list (d (n "lazy_static") (r ">= 1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4.1") (d #t) (k 2)) (d (n "proptest") (r "^0.3.4") (d #t) (k 2)) (d (n "regex") (r ">= 0.2.0") (d #t) (k 0)))) (h "0zi5bf4pha63hiv2vh8ap7070kzs3r54vas951k5l2rcdnf8jdsh") (f (quote (("testing") ("default" "testing"))))))

(define-public crate-stfu8-0.2.3 (c (n "stfu8") (v "0.2.3") (d (list (d (n "lazy_static") (r ">= 1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4.1") (d #t) (k 2)) (d (n "proptest") (r "^0.3.4") (d #t) (k 2)) (d (n "regex") (r ">= 0.2.0") (d #t) (k 0)))) (h "0n6l9qzafdg4nn3lyibz7vb7wl9bhjlfaffq0yb2lw6lpvvy4rfa") (f (quote (("testing") ("default" "testing"))))))

(define-public crate-stfu8-0.2.4 (c (n "stfu8") (v "0.2.4") (d (list (d (n "lazy_static") (r ">= 1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4.1") (d #t) (k 2)) (d (n "proptest") (r "^0.3.4") (d #t) (k 2)) (d (n "regex") (r ">= 0.2.0") (d #t) (k 0)))) (h "0xyv4axwc9rihg3f5fjdy7s0ahnz1iq6lq06blwkq2ihwcrh9xsb") (f (quote (("testing") ("default" "testing"))))))

(define-public crate-stfu8-0.2.5 (c (n "stfu8") (v "0.2.5") (d (list (d (n "lazy_static") (r ">=1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r ">=0.2.0") (d #t) (k 0)))) (h "0xp8nw6pfjrxz16rzrd66lm3jm8hd45v8qpvvj3mlpfq9xk0r7q1") (f (quote (("testing") ("default" "testing"))))))

(define-public crate-stfu8-0.2.6 (c (n "stfu8") (v "0.2.6") (d (list (d (n "lazy_static") (r ">=1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r ">=0.2.0") (d #t) (k 0)))) (h "1rsf0ms590zaxh6sri7dvdbp1aci96i8p3sp74c60fvk545rf40k") (f (quote (("testing") ("default" "testing"))))))

(define-public crate-stfu8-0.2.7 (c (n "stfu8") (v "0.2.7") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0y0rzzphh2mzfhjz0sxymnjn0s4ap21c74f469s9xycky24iw7z5") (f (quote (("testing") ("default" "testing"))))))

