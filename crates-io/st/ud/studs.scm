(define-module (crates-io st ud studs) #:use-module (crates-io))

(define-public crate-studs-0.1.0 (c (n "studs") (v "0.1.0") (d (list (d (n "studs-macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0661qdnk0r1f1qv5wfvysziag541hhqipaq7pd734cahxzpimf3l") (f (quote (("no-std" "studs-macro/no-std") ("macro" "studs-macro") ("default" "macro") ("async"))))))

(define-public crate-studs-0.1.1 (c (n "studs") (v "0.1.1") (d (list (d (n "studs-macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "16rpgp7w82zpczaiv62ai8nb4h3xkyg8mif7j9grx5gn2aycwf45") (f (quote (("no-std" "studs-macro/no-std") ("macro" "studs-macro") ("default" "macro") ("async"))))))

(define-public crate-studs-0.1.2 (c (n "studs") (v "0.1.2") (d (list (d (n "studs-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0waxdskwaisn35bkpyzrs738jgahd92r882y2lxiak8dyrgrh8pk") (f (quote (("macros" "studs-macros") ("default" "macros"))))))

(define-public crate-studs-0.1.3 (c (n "studs") (v "0.1.3") (d (list (d (n "studs-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0znj0ygs97kkb9f1vrzgw5hl3j28ljr18xf4ygnmzl4fl5akn1v0") (f (quote (("reflect") ("macros" "studs-macros") ("default" "macros"))))))

