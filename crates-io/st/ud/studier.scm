(define-module (crates-io st ud studier) #:use-module (crates-io))

(define-public crate-studier-0.1.0 (c (n "studier") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zvrjx14bxfnbkjq7av7v7vgjq2gakda2vb2rgrqjpryfqr1i2ii")))

(define-public crate-studier-1.0.0 (c (n "studier") (v "1.0.0") (d (list (d (n "enum-as-inner") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sbvkbipr3ahvyfgfw5c7irlhvp36j5k1m0h6dgr4j7slj2r0ynk")))

(define-public crate-studier-1.0.1 (c (n "studier") (v "1.0.1") (d (list (d (n "enum-as-inner") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ghxigpx17nfsvbqcaml37jb02q1jw5aaa2rhwjrnzl9yg9jmzq6")))

(define-public crate-studier-1.0.2 (c (n "studier") (v "1.0.2") (d (list (d (n "enum-as-inner") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0adwhiak13ijcr857rbgjcvv49grvcakr19asmfy01h8zv3wgfv3")))

(define-public crate-studier-1.1.0 (c (n "studier") (v "1.1.0") (d (list (d (n "enum-as-inner") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j1x6416wbg7h5f1na4z8nnag3hbf7dc4njm03dm0lyisqc9crz4")))

