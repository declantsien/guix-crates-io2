(define-module (crates-io st ud study-example) #:use-module (crates-io))

(define-public crate-study-example-0.2.0 (c (n "study-example") (v "0.2.0") (d (list (d (n "c-kzg") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0y82539f9q7d3hc6ckm3frbbds073rvnz2l8c11qhrgn5s7443ll")))

(define-public crate-study-example-0.2.1 (c (n "study-example") (v "0.2.1") (d (list (d (n "c-kzg") (r "^0.1.0") (d #t) (k 0)) (d (n "hello_macro_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "098gqypkknw77riyx2ywjkgny4k71k2ww57zppghwqr5681r78w2")))

