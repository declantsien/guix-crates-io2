(define-module (crates-io st ud studs-macros-lib) #:use-module (crates-io))

(define-public crate-studs-macros-lib-0.1.1 (c (n "studs-macros-lib") (v "0.1.1") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "12rz3g7kkm7kfzc6vpmi5pl5z39hlfq6hpi0mkcx09rmg6jrq1k1") (f (quote (("default"))))))

