(define-module (crates-io st ud studs-macros) #:use-module (crates-io))

(define-public crate-studs-macros-0.1.1 (c (n "studs-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "studs-macros-lib") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "07px6ql9pa6rnaazy6yd5dlan1rcfi5f6g547lhzz5pyxvlscmvq")))

