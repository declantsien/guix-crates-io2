(define-module (crates-io st ud stud-macros) #:use-module (crates-io))

(define-public crate-stud-macros-0.1.0 (c (n "stud-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06vh04n13c0fdn940b86f6rgvr5qqcbiakbzfidz17s43bkxkhw5") (f (quote (("bin"))))))

(define-public crate-stud-macros-0.1.1 (c (n "stud-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vcvbcdjm9kv84b2fpa6axw5szlrk7fmnnj40p8aqzaah9zldjcd") (f (quote (("bin"))))))

