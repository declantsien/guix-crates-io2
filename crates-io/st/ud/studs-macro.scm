(define-module (crates-io st ud studs-macro) #:use-module (crates-io))

(define-public crate-studs-macro-0.1.0 (c (n "studs-macro") (v "0.1.0") (d (list (d (n "studs-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1r13manwyypxqs9m8gkxbqqf6w4qi9cwz30qg0il985g45hzynsz") (f (quote (("no-std"))))))

(define-public crate-studs-macro-0.1.1 (c (n "studs-macro") (v "0.1.1") (d (list (d (n "studs-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "120s5c5g89847h0ip922ryiaia2c5s4bg61in25fc8xbxk4yz5vi") (f (quote (("no-std") ("from"))))))

