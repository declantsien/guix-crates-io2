(define-module (crates-io st ud studentvue) #:use-module (crates-io))

(define-public crate-studentvue-0.1.0 (c (n "studentvue") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17.2") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0zvfb18bp01dbgrln9mcxrdqm09r1p85q9hv1ip7dqjim6bjbchj")))

