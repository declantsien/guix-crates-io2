(define-module (crates-io st ud stud) #:use-module (crates-io))

(define-public crate-stud-0.1.0 (c (n "stud") (v "0.1.0") (h "161swc3vgydvad1v2qihvqs2hnkjqfv2vgr0rz04wp3ah27yz552")))

(define-public crate-stud-0.1.1 (c (n "stud") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "stud-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "signal"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (o #t) (d #t) (k 0)) (d (n "yansi") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1blhd1q9r4sip7g8nbc5j71gj0kyq1r5h4gcvwrkzhgq2ay54dgj") (f (quote (("default" "bin")))) (s 2) (e (quote (("bin" "stud-macros/bin" "dep:clap" "dep:tracing-subscriber" "dep:yansi" "dep:tokio"))))))

