(define-module (crates-io st ud study_rpn_calc_2022_04_07) #:use-module (crates-io))

(define-public crate-study_rpn_calc_2022_04_07-0.1.0 (c (n "study_rpn_calc_2022_04_07") (v "0.1.0") (h "1pzlsx55z295vzbdmghbh3k3618adfvaaynxhii41rxzxvinrjnz") (y #t)))

(define-public crate-study_rpn_calc_2022_04_07-0.1.1 (c (n "study_rpn_calc_2022_04_07") (v "0.1.1") (h "0dxsc0hvh2af5i5a0h04asd825k88a1f6yc9khhcfsiycdschdz8") (y #t)))

(define-public crate-study_rpn_calc_2022_04_07-0.1.2 (c (n "study_rpn_calc_2022_04_07") (v "0.1.2") (h "00m7fnhqc7kszrzywldy11v8h9k7rg18yz3h894dy6dwvsfgfkwc") (y #t)))

