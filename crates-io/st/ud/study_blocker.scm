(define-module (crates-io st ud study_blocker) #:use-module (crates-io))

(define-public crate-study_blocker-0.1.0 (c (n "study_blocker") (v "0.1.0") (d (list (d (n "eframe") (r "^0.20.1") (f (quote ("persistence" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01wd7vihiq6297gnrp17mrgam4mjq515d34rhfqfd4d11jswmz02")))

(define-public crate-study_blocker-0.2.0 (c (n "study_blocker") (v "0.2.0") (d (list (d (n "eframe") (r "^0.20.1") (f (quote ("persistence" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08ws1yh7g66l4nq4cbdyg9fakl9bagsnci2dmldm9gcgsfm6x2sr")))

(define-public crate-study_blocker-0.2.1 (c (n "study_blocker") (v "0.2.1") (d (list (d (n "eframe") (r "^0.20.1") (f (quote ("persistence" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m00ml7kp9am86zncwfdmzbin13j2s6pg259l1d62v2rc8yplmmd")))

(define-public crate-study_blocker-0.2.2 (c (n "study_blocker") (v "0.2.2") (d (list (d (n "eframe") (r "^0.20.1") (f (quote ("persistence" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0465ap7l6ym3889c7f730zkdilf8l4lgjp8xdq1lbfrk9dpbgx2s")))

