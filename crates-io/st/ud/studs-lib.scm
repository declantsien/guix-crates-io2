(define-module (crates-io st ud studs-lib) #:use-module (crates-io))

(define-public crate-studs-lib-0.1.0 (c (n "studs-lib") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0n9d11sa23qlbw52kxxdfxsrdyc644m85hw3g5mqjjqdm0zal0sl") (f (quote (("no-std") ("default"))))))

(define-public crate-studs-lib-0.1.1 (c (n "studs-lib") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "08ml7a9s0giv25rjv5xqfqix79y2665f0n8ahixi6hnf8awlxi7k") (f (quote (("no-std") ("default"))))))

