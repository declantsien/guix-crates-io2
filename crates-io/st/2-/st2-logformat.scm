(define-module (crates-io st #{2-}# st2-logformat) #:use-module (crates-io))

(define-public crate-st2-logformat-0.1.0 (c (n "st2-logformat") (v "0.1.0") (d (list (d (n "abomonation") (r "^0.7") (d #t) (k 0)) (d (n "abomonation_derive") (r "^0.3") (d #t) (k 0)) (d (n "differential-dataflow") (r "^0.10.0") (d #t) (k 0)) (d (n "timely") (r "^0.10.0") (d #t) (k 0)))) (h "00mk9ycwpff74ci853zyk272s9bdqmy260n32r7blam9jqqm7nf8")))

