(define-module (crates-io st om stomper) #:use-module (crates-io))

(define-public crate-stomper-0.1.0 (c (n "stomper") (v "0.1.0") (d (list (d (n "libstomper") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "08jqnxyhy3ilycq82gvyi3v4skx04bz5y6s5an4gcgfxqav4nzzm")))

(define-public crate-stomper-0.2.0 (c (n "stomper") (v "0.2.0") (d (list (d (n "libstomper") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "11g2rb1ql7naas6qr0xn3ibvrg775wmcckqazbzminwlq2j4nsz4")))

