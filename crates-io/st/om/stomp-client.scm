(define-module (crates-io st om stomp-client) #:use-module (crates-io))

(define-public crate-stomp-client-0.1.0 (c (n "stomp-client") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "stomp-parser") (r "^0.5") (d #t) (k 0)))) (h "0fqwh3laindigibhjpbl4x64jbb3g510ipqzd6fgkqpvwrb9f9wn")))

