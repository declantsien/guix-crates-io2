(define-module (crates-io st om stomp) #:use-module (crates-io))

(define-public crate-stomp-0.3.3 (c (n "stomp") (v "0.3.3") (h "16vj9cn4wy70ll0flb1b8dd2g0fd261mjjwr9981zwd7wk2d32n4")))

(define-public crate-stomp-0.3.4 (c (n "stomp") (v "0.3.4") (h "0pdv80c5ir2jjvq6l7szkkqkqbn5qmplfmvkwpjsd5nl68zqxh74")))

(define-public crate-stomp-0.3.5 (c (n "stomp") (v "0.3.5") (h "187av6zdgkv5q32j1acslpc42mppqn501zhj0w0n0081nr7finb7")))

(define-public crate-stomp-0.3.6 (c (n "stomp") (v "0.3.6") (h "0y2l2arvvmg1sjimg8jb4gbp3lnn5r6d29d2dka9irdrf0p9a11m")))

(define-public crate-stomp-0.3.7 (c (n "stomp") (v "0.3.7") (h "1bb5zddya8h59m0k31np9nis6rrl9756bhj0i0iw0kkmji5gqld4")))

(define-public crate-stomp-0.4.0 (c (n "stomp") (v "0.4.0") (h "01q5qacz1w5fyrfj4mpn85r4jv25mapdd78wk6ygvw8bvjpkqqlz")))

(define-public crate-stomp-0.5.0 (c (n "stomp") (v "0.5.0") (h "049q97q1v2l209381f4l368bhzhrphdrv3lf613kjsaqw32lmzsd")))

(define-public crate-stomp-0.5.1 (c (n "stomp") (v "0.5.1") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0v8n2g5x3s112pjkmla450pad68341n125nzgb4vnhkyqcr3xqkz")))

(define-public crate-stomp-0.5.2 (c (n "stomp") (v "0.5.2") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "07fa0gq6xrnj18frj9a6869zzwig3l67k54ahijvqk37j0dxxc4h")))

(define-public crate-stomp-0.6.0 (c (n "stomp") (v "0.6.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "10ikhx7jkp8mwsrk9xlvr4fk80vmxhrm8ci01rlr06kfa9k6p3gk")))

(define-public crate-stomp-0.7.0 (c (n "stomp") (v "0.7.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1dzvq4x37bm5309akwpd2qi3q3pdmx2rym11wr71pilrzxawn7j2")))

(define-public crate-stomp-0.8.0 (c (n "stomp") (v "0.8.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "094fv3x15avgsz3fb4vkssgy7fsfh4na5ipxqlzbd3snw2dy6dxx")))

(define-public crate-stomp-0.8.1 (c (n "stomp") (v "0.8.1") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1mmpinp2rndwb57n11ahv5j6i90vfqxysmaxy6hzln4994b346lr")))

(define-public crate-stomp-0.8.2 (c (n "stomp") (v "0.8.2") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0n7hn1kp8446hih674jrdmy8swn3abjrlya80v03d044129gxwqr")))

(define-public crate-stomp-0.8.3 (c (n "stomp") (v "0.8.3") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "01cmy83wlbad8pnnyffiwnhviy7fns02k7g6g9gis2ibp4ncgsj3")))

(define-public crate-stomp-0.8.4 (c (n "stomp") (v "0.8.4") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0y3zlin83j7mclsxx9in6293lrq95wr8gh8453cmfcik87frhirq")))

(define-public crate-stomp-0.9.0 (c (n "stomp") (v "0.9.0") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "mio") (r "*") (d #t) (k 0)) (d (n "unicode-segmentation") (r "*") (d #t) (k 0)))) (h "0faqkcbg0q38asw5347wcfmkka156nwnvk35fc90bzj9ij0gn8rn")))

(define-public crate-stomp-0.10.0 (c (n "stomp") (v "0.10.0") (d (list (d (n "lifeguard") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "mio") (r "*") (d #t) (k 0)) (d (n "unicode-segmentation") (r "*") (d #t) (k 0)))) (h "0mqwmwg0rjl3pa2kaaijg71jwm3byc6d96zqd73wk25svcyszjpf")))

(define-public crate-stomp-0.10.1 (c (n "stomp") (v "0.10.1") (d (list (d (n "lifeguard") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "mio") (r "*") (d #t) (k 0)) (d (n "unicode-segmentation") (r "*") (d #t) (k 0)))) (h "027pksksgfyw2yw4midj15h6m9h1jnxgc2hxxmwyspi9l3xb84jv")))

(define-public crate-stomp-0.10.2 (c (n "stomp") (v "0.10.2") (d (list (d (n "lifeguard") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^0.1") (d #t) (k 0)))) (h "1zq0wakid7zck4zl9lavh6aj8yhx91ixgh8lhysx8j8rchn448qa")))

(define-public crate-stomp-0.11.0 (c (n "stomp") (v "0.11.0") (d (list (d (n "lifeguard") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^0.1") (d #t) (k 0)))) (h "0jl718k1gz2a0gdyn9vgrg2pm42rx2r4j8dy63z1rbf3cj70yljz")))

