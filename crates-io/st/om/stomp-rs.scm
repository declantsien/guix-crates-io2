(define-module (crates-io st om stomp-rs) #:use-module (crates-io))

(define-public crate-stomp-rs-0.0.1 (c (n "stomp-rs") (v "0.0.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05dys2b4sx5ris1w1wwmdsx8jh0bdp8vh01j69za9wdnwr6ix1l2")))

(define-public crate-stomp-rs-0.0.2 (c (n "stomp-rs") (v "0.0.2") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bwxqg2nqacy9kf09iwxc8k0rg0zpp2fzgkl55hqlfzqig3x6759")))

(define-public crate-stomp-rs-0.0.3 (c (n "stomp-rs") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "16mvnwcgjdmasd1434825i9icgzm6wz940hwpdi7ljbpvf2dslan")))

(define-public crate-stomp-rs-0.0.4 (c (n "stomp-rs") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nsi2nk7v3bg6kphyls03mxs96qlf4gbjmn5m5mjs8rpjm9w49nx") (y #t)))

(define-public crate-stomp-rs-0.0.5 (c (n "stomp-rs") (v "0.0.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nby08nm9xi65hsxn9qfyx7pz9mhm0fkym88d4m66xqx0sdb9l4f")))

(define-public crate-stomp-rs-0.0.6 (c (n "stomp-rs") (v "0.0.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1pmc89xhkl0k0a22n71czwplf47dkrrqz9j2dvjprnsdlhvqv8ld")))

(define-public crate-stomp-rs-0.0.7 (c (n "stomp-rs") (v "0.0.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0v0hl8fffln5m4n94rfmfm2yfc86589jddn38prfnz8fiqhn59wa")))

(define-public crate-stomp-rs-0.0.8 (c (n "stomp-rs") (v "0.0.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1j5h2xb6wwc9y00v2vv0lccqf0xyy0cz4lwd8ccgdgr1xl67b6hf")))

