(define-module (crates-io st om stomp-test-utils) #:use-module (crates-io))

(define-public crate-stomp-test-utils-0.1.0 (c (n "stomp-test-utils") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "time" "test-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "03jksylp5aclxai3qm3h26a1w21gxgkd6nmyigrx6bp88bqi4qh9")))

(define-public crate-stomp-test-utils-0.2.0 (c (n "stomp-test-utils") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "time" "test-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0r83yh6pk71bzsg3900ipv47hrbf49c1ws0a0k88xzi5g0zf36hv")))

(define-public crate-stomp-test-utils-0.2.1 (c (n "stomp-test-utils") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "time" "test-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "04n6vmcz0q8bxbvd4a9rhixsdasb8n046iz74dpfiybzqym7wafy")))

