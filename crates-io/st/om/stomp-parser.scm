(define-module (crates-io st om stomp-parser) #:use-module (crates-io))

(define-public crate-stomp-parser-0.2.0 (c (n "stomp-parser") (v "0.2.0") (d (list (d (n "nom") (r "^6.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-trace") (r "^0.2.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "15nay1rwizvshci6pdaakl1kf2dg5j8c7ysmkkqkrh1wg65h1pvy")))

(define-public crate-stomp-parser-0.3.0 (c (n "stomp-parser") (v "0.3.0") (d (list (d (n "nom") (r "^6.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-trace") (r "^0.2.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1nggj2vz5zaix4nkgg742knhnrb5wcmnfixq9n7slwc1z2yavilp")))

(define-public crate-stomp-parser-0.3.1 (c (n "stomp-parser") (v "0.3.1") (d (list (d (n "nom") (r "^6.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1ilzda5nqh9npas8xqc7isrdhxigry0jxkmlihharcwx9affrszg")))

(define-public crate-stomp-parser-0.3.3 (c (n "stomp-parser") (v "0.3.3") (d (list (d (n "nom") (r "^6.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1f5d1rw13a1a5nwclkh1k9v9dnpcbwsg6q70ksaczyha5dblkdxb")))

(define-public crate-stomp-parser-0.4.0 (c (n "stomp-parser") (v "0.4.0") (d (list (d (n "nom") (r "^6.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "0a7gpd33hypw3ydsriks5kz4mvwkpsg40mv12by89aiwmhpb53qj")))

(define-public crate-stomp-parser-0.4.1 (c (n "stomp-parser") (v "0.4.1") (d (list (d (n "nom") (r "^6.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "11p90zavnqr1m9lk58ccirxy3sb3gn30iq2jxcm3i7kq8gxil6jq")))

(define-public crate-stomp-parser-0.5.0 (c (n "stomp-parser") (v "0.5.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "nom") (r "^6.2.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "0gk4g6521rx60c43rh4k7sgsi1afdr0zb6zkrphc8kcpci1fr69i")))

(define-public crate-stomp-parser-0.6.0 (c (n "stomp-parser") (v "0.6.0") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "13vhac3m958ls5y8a5ms0db6z0jj1hpddrhkd282bx6v3xdhwkdd")))

