(define-module (crates-io st re stream-utils) #:use-module (crates-io))

(define-public crate-stream-utils-0.1.0 (c (n "stream-utils") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "02j4di2g12xi2wpia2zk6d5265yivacdrd07c56i2sih2shaqfc6")))

(define-public crate-stream-utils-0.2.0 (c (n "stream-utils") (v "0.2.0") (d (list (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "ntest_timeout") (r "^0.9.0") (d #t) (k 2)) (d (n "tokio") (r "^1.29") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0a936cqh8llrghlw0cvvplxicl1jamys58ylmjm7qnj4m6fm4d9p")))

