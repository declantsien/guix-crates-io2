(define-module (crates-io st re stream-unconsume) #:use-module (crates-io))

(define-public crate-stream-unconsume-0.3.1 (c (n "stream-unconsume") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "push-trait") (r "^0.6.0") (d #t) (k 0)))) (h "0d113wvjchbaqf7ykhg253qpf4bmifyfqixmwgvffzf49ingyvr2")))

