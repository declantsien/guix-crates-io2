(define-module (crates-io st re stream-map-any) #:use-module (crates-io))

(define-public crate-stream-map-any-0.1.0 (c (n "stream-map-any") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2.20") (f (quote ("stream" "macros"))) (d #t) (k 2)))) (h "168d9fkkwkibyimp4kyl5xpn969clvqf3b9wa8f8828brzr7xchh")))

(define-public crate-stream-map-any-0.2.0 (c (n "stream-map-any") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2.20") (f (quote ("stream" "macros"))) (d #t) (k 2)))) (h "1rr3pxl987bhgzmm4xy3amiiqf84j3simdl8cc4bijpwpcra57jv")))

(define-public crate-stream-map-any-0.2.1 (c (n "stream-map-any") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2.20") (f (quote ("stream" "macros"))) (d #t) (k 2)))) (h "169n2nq1f3hiaain6qapgn4m3gdlm78lc6azzqvs9k36vyyc32sk")))

(define-public crate-stream-map-any-0.2.2 (c (n "stream-map-any") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2.20") (f (quote ("stream" "macros"))) (d #t) (k 2)))) (h "0gjqppp8mhzfdf9kzs62w3hpwzm4gvh5fl3hnc86scy7vsx1dbzb")))

