(define-module (crates-io st re stream_generator) #:use-module (crates-io))

(define-public crate-stream_generator-0.1.0 (c (n "stream_generator") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "rt-core" "sync"))) (d #t) (k 2)))) (h "09idx16yw20crn3ki7mlq97rrmj44dimzwzhv2kpmzzhifq0gvry")))

