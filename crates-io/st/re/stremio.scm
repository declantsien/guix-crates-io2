(define-module (crates-io st re stremio) #:use-module (crates-io))

(define-public crate-stremio-0.1.0 (c (n "stremio") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "030khkrxkwszrb1r9jklp6vwrb6790f1qy6day9r287vjgsvk10m")))

(define-public crate-stremio-0.1.1 (c (n "stremio") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1xx65ijjfqdlx37k159vcv56l8fnr2yqxy0j2hnvndlma4k2yjfb")))

(define-public crate-stremio-0.1.2 (c (n "stremio") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1ri7x741v3885d3790svnq9allyc2a70hs97cgiprc66d4nin8b1")))

(define-public crate-stremio-0.1.3 (c (n "stremio") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0kf6jhlzhr8l2gh89ggcnnw98lmx237bhfbb5c93i5sxhnswxwhp")))

