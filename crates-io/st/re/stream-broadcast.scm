(define-module (crates-io st re stream-broadcast) #:use-module (crates-io))

(define-public crate-stream-broadcast-0.1.0 (c (n "stream-broadcast") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1phcmnmhgv470ycy335n62nyh3yy6yx9a0ai9qrxzmic0lrvxgiz") (y #t)))

(define-public crate-stream-broadcast-0.1.1 (c (n "stream-broadcast") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "17yy2sd688jd4hi3qwympfj6v0m1x983vpscr9pa29j5msa1h6g8")))

(define-public crate-stream-broadcast-0.2.0 (c (n "stream-broadcast") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "15xbh4dyz4qf25hv90bpmmyqblqh48pgjkhfw30bbm7bbjdrpvjr") (y #t)))

(define-public crate-stream-broadcast-0.2.1 (c (n "stream-broadcast") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0zapdrq3bghi6raawcvifignp8n9r2k9pvmqsxnv4lrhx1kb6al9")))

(define-public crate-stream-broadcast-0.2.2 (c (n "stream-broadcast") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "00k18qcnz5gjbdyifn6pyxjr3ic7bcaxx0rxyc26amzjarb7rsiy")))

(define-public crate-stream-broadcast-0.2.3 (c (n "stream-broadcast") (v "0.2.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "12yshxcrpzmcnr8ad1z4lcbwm237kadlwnq0my6jp7axck4ic54f") (y #t)))

(define-public crate-stream-broadcast-0.3.0 (c (n "stream-broadcast") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1ada03m0y7iqbdpc4zfaxvc63habc6m33wsjgm28fsqmjaanhfwi")))

