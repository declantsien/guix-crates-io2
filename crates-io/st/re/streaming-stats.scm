(define-module (crates-io st re streaming-stats) #:use-module (crates-io))

(define-public crate-streaming-stats-0.1.1 (c (n "streaming-stats") (v "0.1.1") (h "0g2s7v4z7qcxcjmpgbgnwh0zh04m5mqra7y4p67ccir16zbyvrgs")))

(define-public crate-streaming-stats-0.1.2 (c (n "streaming-stats") (v "0.1.2") (h "1v4xsm5rh1jjvij3fbwjjr3a9iy35alqpqc5n04f2lv3jarnwzzf")))

(define-public crate-streaming-stats-0.1.3 (c (n "streaming-stats") (v "0.1.3") (h "1pwd3mmycf7qji3xwln6r0zi3lv2z1yb6w1wm3qznwczndcay3wd")))

(define-public crate-streaming-stats-0.1.4 (c (n "streaming-stats") (v "0.1.4") (h "03gnafvkpi58880329kznkb8hvg33c31nw46gix658f12yx9516b")))

(define-public crate-streaming-stats-0.1.5 (c (n "streaming-stats") (v "0.1.5") (h "1anyz72p1ssg01yvyzajnrx145apwns4yvjv0sagrhw66arswgvv")))

(define-public crate-streaming-stats-0.1.6 (c (n "streaming-stats") (v "0.1.6") (h "0iv5qzxry6cy2c09bf6hr0zq2yr3sdk86g0qn46wvkcajdys3292")))

(define-public crate-streaming-stats-0.1.7 (c (n "streaming-stats") (v "0.1.7") (h "0vlg2x1r8fv98nyqlkha8vpa0gw6lsv6srz9vrzd4n2ifh62w67f")))

(define-public crate-streaming-stats-0.1.8 (c (n "streaming-stats") (v "0.1.8") (h "1nniqww1y8sqapnlkkyj2mi42pzql8744jyz4xvflf6cpafmsl4h")))

(define-public crate-streaming-stats-0.1.9 (c (n "streaming-stats") (v "0.1.9") (h "1aqndldn4cczmf2df8k8r2yb8p5kf7pljxznrd08i05pfdhmawl5")))

(define-public crate-streaming-stats-0.1.10 (c (n "streaming-stats") (v "0.1.10") (h "0qyvhzbnr0qw7rgj4k33609ca9bpfsfsr777ziqcw81r1p0aiiip")))

(define-public crate-streaming-stats-0.1.11 (c (n "streaming-stats") (v "0.1.11") (h "09ifm1d7ibsi5075my57k5aadppxq2bh952q29vkla4q38faljgf")))

(define-public crate-streaming-stats-0.1.12 (c (n "streaming-stats") (v "0.1.12") (h "0d6zrvm7w6q4120rliww374yjd5xv7zpimdwbsnnx88d33y9i0r6")))

(define-public crate-streaming-stats-0.1.13 (c (n "streaming-stats") (v "0.1.13") (h "1hpf23zc3x92rmsc5l6w7wl11kngz4d9y0ysmnjlgfdnfnrvfyxl")))

(define-public crate-streaming-stats-0.1.14 (c (n "streaming-stats") (v "0.1.14") (h "1wcaz1g9r2xpxd6zg6ih19vzhn3855gcxpvnpnaky15dlmgahg31")))

(define-public crate-streaming-stats-0.1.15 (c (n "streaming-stats") (v "0.1.15") (h "0zkw60yp0yq1996yw24s5wvls6idhljf6rkmgsv2yisrnz4634f8")))

(define-public crate-streaming-stats-0.1.16 (c (n "streaming-stats") (v "0.1.16") (h "1lp2aihbjb20hpy33p6i9mxsni4p4djl99q5kl6za4p63vymjvnm")))

(define-public crate-streaming-stats-0.1.17 (c (n "streaming-stats") (v "0.1.17") (h "02fshnnf2p1gykvrb5wr148wl0n30n8k9hmhndnil7v0bwx3mkai")))

(define-public crate-streaming-stats-0.1.18 (c (n "streaming-stats") (v "0.1.18") (h "19dyb881hjgdf07shydrkrfry7dxfibgc2ndpmvl8qwlqylgklgc")))

(define-public crate-streaming-stats-0.1.19 (c (n "streaming-stats") (v "0.1.19") (h "0ga7lzsnrj2zzzx5pp2w9dks4qpn30qaaks27bl71npn5d8i01rz")))

(define-public crate-streaming-stats-0.1.20 (c (n "streaming-stats") (v "0.1.20") (h "0f6j5ap6943945qq458sxqm2glb11rc7agknrq63d6npnwni3vmi")))

(define-public crate-streaming-stats-0.1.21 (c (n "streaming-stats") (v "0.1.21") (h "0860qmc0cf2s87d0s87fkvw8gvjwr2h65hqhqbji88afb19j13ry")))

(define-public crate-streaming-stats-0.1.22 (c (n "streaming-stats") (v "0.1.22") (h "0k5wilcj85q6wl29xvm2sx54mn9c5g3m751ig1yknws79nj9pvns")))

(define-public crate-streaming-stats-0.1.23 (c (n "streaming-stats") (v "0.1.23") (h "1dgqli8pxdhdxny8yqxy44n0y7s0gnxwkdjyamcqih10c17xip43")))

(define-public crate-streaming-stats-0.1.25 (c (n "streaming-stats") (v "0.1.25") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1zhrdkg7qg6wq1wqrq0kzyxc9wgwmck7hf0sxck7m46kld7x29m1")))

(define-public crate-streaming-stats-0.1.24 (c (n "streaming-stats") (v "0.1.24") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "15bqvl7hgm3axxg9ysn5bl5fqf1x6icq82bj2z33295czvrriphm")))

(define-public crate-streaming-stats-0.1.26 (c (n "streaming-stats") (v "0.1.26") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1kayw0dp65amyf638hc897agmdmpbhfmcjjbxvy383j8s4nfiqlj")))

(define-public crate-streaming-stats-0.1.27 (c (n "streaming-stats") (v "0.1.27") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1x3al1g91rsmy0dz3l3x1yq6y3b0k6wvik0cad1vysj4x0fhy2a1")))

(define-public crate-streaming-stats-0.1.28 (c (n "streaming-stats") (v "0.1.28") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "12cb05jqyx6f6ril3vx2z7zzqfgvxffpkn95w72n46p1h3b0qggi")))

(define-public crate-streaming-stats-0.1.29 (c (n "streaming-stats") (v "0.1.29") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "09cbpj40g1wcmkz9bpm9yrqyrvqizn6kmwac1jpb2prq6gr2phyv")))

(define-public crate-streaming-stats-0.2.0 (c (n "streaming-stats") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1x70k5kvnnc39gvcyzsy4qzc12dwyxky24pzgk225syfa2jkl8sg")))

(define-public crate-streaming-stats-0.2.2 (c (n "streaming-stats") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0l7xz4g6709s80zqpvlhrg0qhgz64r94cwhmfsg8xhabgznbp2px")))

(define-public crate-streaming-stats-0.2.3 (c (n "streaming-stats") (v "0.2.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0iz5dlq51w5hxjrv6a4hpf8rrj91kgvy0s9mhj0j12il9v771mmh")))

