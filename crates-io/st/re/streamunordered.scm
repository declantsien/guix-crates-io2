(define-module (crates-io st re streamunordered) #:use-module (crates-io))

(define-public crate-streamunordered-0.1.0 (c (n "streamunordered") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)))) (h "03lvghm1cgn1cg8d2iy4nxbc3aq2lv7yn5jcy79z25h70ji1gd9s")))

(define-public crate-streamunordered-0.1.1 (c (n "streamunordered") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)))) (h "00vyya82zvwx5k9s3jyfbx4jfgw97jhjis466rl4vvfvvqvrq0k8")))

(define-public crate-streamunordered-0.2.0 (c (n "streamunordered") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)))) (h "15adbf30ry8j2vnvbgdp6c83kmb1c82k1sbfngl6f17dc8fs5y1k")))

(define-public crate-streamunordered-0.3.0 (c (n "streamunordered") (v "0.3.0") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)))) (h "1rwsz9xzbhz51zap7d0vp3yy1grfd4rlxil778ws8wzv3ibgj9y3")))

(define-public crate-streamunordered-0.3.1 (c (n "streamunordered") (v "0.3.1") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)))) (h "07wc18670kzpl8ffxkdpaxjixyrdfr08ygfa15a149msrkgqnp1l")))

(define-public crate-streamunordered-0.3.2 (c (n "streamunordered") (v "0.3.2") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)))) (h "1qypfrd0xb7zxykk7ipqjhf2nk1nxgksgcpg58kj0cbdh7lbw83n") (y #t)))

(define-public crate-streamunordered-0.3.3 (c (n "streamunordered") (v "0.3.3") (d (list (d (n "async-bincode") (r "^0.4.3") (d #t) (k 2)) (d (n "bincode") (r "^1.0.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1wrx5svpbsn77qi4vg8m7fh6a0qndr01wzgdm4j9345mk5vh9ak6")))

(define-public crate-streamunordered-0.4.0 (c (n "streamunordered") (v "0.4.0") (d (list (d (n "async-bincode") (r "^0.4.3") (d #t) (k 2)) (d (n "bincode") (r "^1.0.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0ljpnscl7jwkxbzmkvh66754cnm1ff8p30075s0hq1k3skir6kpq")))

(define-public crate-streamunordered-0.4.1 (c (n "streamunordered") (v "0.4.1") (d (list (d (n "async-bincode") (r "^0.4.3") (d #t) (k 2)) (d (n "bincode") (r "^1.0.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "099dvs40sgrdj407ia12r5wrq16r0hm8696nppaikk9sd80zy690")))

(define-public crate-streamunordered-0.5.0-alpha.1 (c (n "streamunordered") (v "0.5.0-alpha.1") (d (list (d (n "async-bincode") (r "^0.5.0-alpha.1") (d #t) (k 2)) (d (n "bincode") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-core-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-sink-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.4") (d #t) (k 2)))) (h "0ami1mdik8z7j3m2ciab4xk9lx7s6vgyb158p2dg8m28pkgqlfwn")))

(define-public crate-streamunordered-0.5.0-alpha.2 (c (n "streamunordered") (v "0.5.0-alpha.2") (d (list (d (n "async-bincode") (r "^0.5.0-alpha.6") (d #t) (k 2)) (d (n "bincode") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-sink-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 2)))) (h "1my4794azh1qpc25fnvhgfbpyvmzwnm5bvswpsi7qhrwa2n9b5ly")))

(define-public crate-streamunordered-0.5.0-alpha.3 (c (n "streamunordered") (v "0.5.0-alpha.3") (d (list (d (n "async-bincode") (r "= 0.5.0-alpha.6") (d #t) (k 2)) (d (n "bincode") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-sink-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 2)))) (h "1bi1rx972sy4cix0w6j1ir9p898gwmqac9jdfxhr2viir6iiikbh")))

(define-public crate-streamunordered-0.5.0 (c (n "streamunordered") (v "0.5.0") (d (list (d (n "async-bincode") (r "^0.5.0") (d #t) (k 2)) (d (n "bincode") (r "^1.0.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "10cf2539v7x9l10havc0x4fzmhdv1p94avrhrvivh68h3w9lqc9y")))

(define-public crate-streamunordered-0.5.1 (c (n "streamunordered") (v "0.5.1") (d (list (d (n "async-bincode") (r "^0.5.0") (d #t) (k 2)) (d (n "bincode") (r "^1.0.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1jlrhv7js6ddcxj9b803g68mcfhb2ykzhjg6prq87vlg6ghlwfgr")))

(define-public crate-streamunordered-0.5.2 (c (n "streamunordered") (v "0.5.2") (d (list (d (n "async-bincode") (r "^0.6.0") (d #t) (k 2)) (d (n "bincode") (r "^1.0.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)))) (h "0d8fx49j48y08jasgmky1vc9sd4h44h1bpwn4wvzadwagkipd1g6")))

(define-public crate-streamunordered-0.5.3 (c (n "streamunordered") (v "0.5.3") (d (list (d (n "async-bincode") (r "^0.7.0") (d #t) (k 2)) (d (n "bincode") (r "^1.0.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.1") (d #t) (k 2)))) (h "05lsdw3182k5iyah2pv7rnl8v848ipcc7zr0qdy7jjsfhm999d4n")))

