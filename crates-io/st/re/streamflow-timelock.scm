(define-module (crates-io st re streamflow-timelock) #:use-module (crates-io))

(define-public crate-streamflow-timelock-0.0.1 (c (n "streamflow-timelock") (v "0.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0dm8y944z60p8ag4215pw782cn2v2zni053qrhgc8wzwr04zk56m")))

(define-public crate-streamflow-timelock-0.1.0 (c (n "streamflow-timelock") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1zs14829nnksn94wc0g9ppa2w5crcj6wfnr973iwgchgylv1gax0")))

(define-public crate-streamflow-timelock-0.1.1 (c (n "streamflow-timelock") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.14") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1m8qkn7xsxirn1xdihvlbdhkxprli8vpm3r5qavcwjkms8346bcb")))

(define-public crate-streamflow-timelock-0.2.0 (c (n "streamflow-timelock") (v "0.2.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "002xzlxyp2p8q742gvrvlyxq5b5fc55c0aaam5jlwrzmgxin6wpx")))

(define-public crate-streamflow-timelock-0.2.1 (c (n "streamflow-timelock") (v "0.2.1") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0baig8109rx81zhnqh76459jrgv7dd25d9r9h2zij42mgsilw2bv")))

(define-public crate-streamflow-timelock-0.3.0 (c (n "streamflow-timelock") (v "0.3.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1dm304wcjirrb4m6ym65amcshv746n5gg8vgawmp4lchwsb1jfik")))

(define-public crate-streamflow-timelock-0.3.1 (c (n "streamflow-timelock") (v "0.3.1") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0asfihfvhf2cpb75ihfkp6lx9d16axvxbxml7562r676pspz9q32")))

(define-public crate-streamflow-timelock-0.3.2 (c (n "streamflow-timelock") (v "0.3.2") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1jn1953lchwy2p3lwigb2nrgbgis7q6xhzl11ig49kh8lg154r2w")))

