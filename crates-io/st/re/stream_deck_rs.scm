(define-module (crates-io st re stream_deck_rs) #:use-module (crates-io))

(define-public crate-stream_deck_rs-0.1.0 (c (n "stream_deck_rs") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "hidapi") (r "^0.5.0") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 0)))) (h "001zjdazr1cflk1ad86d53q2f4h98bcn44rlcw81m8gxgsd3s4zq")))

(define-public crate-stream_deck_rs-0.1.1 (c (n "stream_deck_rs") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "hidapi") (r "^0.5.0") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 0)))) (h "1xcb460ck0cpp7apc756qzmqhlqidwl2n5kwx0z6cfi3vzj3xia3")))

