(define-module (crates-io st re streaminterpolator) #:use-module (crates-io))

(define-public crate-streaminterpolator-0.1.0 (c (n "streaminterpolator") (v "0.1.0") (h "1vgs1858m4nivshxv7my1gwbvdr9wp7lrjj5ixzl8prd9n78jfz8")))

(define-public crate-streaminterpolator-0.1.1 (c (n "streaminterpolator") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (d #t) (k 0)))) (h "0j491186cs8ywi1yzha8iq03xs8d3dfkk1szxks99fqg05rlwnlp")))

