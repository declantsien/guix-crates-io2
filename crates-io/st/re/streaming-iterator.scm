(define-module (crates-io st re streaming-iterator) #:use-module (crates-io))

(define-public crate-streaming-iterator-0.1.0 (c (n "streaming-iterator") (v "0.1.0") (h "1ggdnb6nqjw03x3zc7cz9n79z4v13373wgynvb123nw8b436ng3n") (f (quote (("std"))))))

(define-public crate-streaming-iterator-0.1.1 (c (n "streaming-iterator") (v "0.1.1") (h "0gyyqlg9ycc16gligm7j5lnap2k5dhw6ilfkxw5zphwq0mhmz905") (f (quote (("std"))))))

(define-public crate-streaming-iterator-0.1.2 (c (n "streaming-iterator") (v "0.1.2") (h "1c98h1d9pza6sl3d0v7zmv94n7rx052fap0s8h7z2i1m9lnprwk4") (f (quote (("std"))))))

(define-public crate-streaming-iterator-0.1.3 (c (n "streaming-iterator") (v "0.1.3") (h "1h75998kcs03q1hirqbma4lnqzp8kqhpjz9xmhrma50m7rb5x2f6") (f (quote (("std"))))))

(define-public crate-streaming-iterator-0.1.4 (c (n "streaming-iterator") (v "0.1.4") (h "0bqckfi0qnn7l74r843bwx32nv935h6k6v6ww9yjb9rxn0s25mqp") (f (quote (("std"))))))

(define-public crate-streaming-iterator-0.1.5 (c (n "streaming-iterator") (v "0.1.5") (h "1xc1gyq137b020m29g85zdhbbdrksdmhf3dq4ri4fjlrfz0kacih") (f (quote (("std"))))))

(define-public crate-streaming-iterator-0.1.6 (c (n "streaming-iterator") (v "0.1.6") (h "0500lwrry4fjz88zmhk2vakll849xk8bwm9vp0q66mqbizgvy5s8") (f (quote (("std"))))))

(define-public crate-streaming-iterator-0.1.7 (c (n "streaming-iterator") (v "0.1.7") (h "1mwgw826a0cgdqlwciqfk9nwj2551vw1qr4jsijg4msfblfvi180") (f (quote (("std")))) (r "1.31")))

(define-public crate-streaming-iterator-0.1.8 (c (n "streaming-iterator") (v "0.1.8") (h "08kfjya8n7d5bbkjjcskr1i0vhv39llpg4fccy3yz19gmadd0pfm") (f (quote (("std")))) (r "1.31")))

(define-public crate-streaming-iterator-0.1.9 (c (n "streaming-iterator") (v "0.1.9") (h "0845zdv8qb7zwqzglpqc0830i43xh3fb6vqms155wz85qfvk28ib") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

