(define-module (crates-io st re streemap) #:use-module (crates-io))

(define-public crate-streemap-0.1.0 (c (n "streemap") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ab3240vkak3gv4j70qgx2bxrw19s0q8xiqi5pljfpn5kqx30zzi")))

