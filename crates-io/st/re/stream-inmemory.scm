(define-module (crates-io st re stream-inmemory) #:use-module (crates-io))

(define-public crate-stream-inmemory-0.1.0 (c (n "stream-inmemory") (v "0.1.0") (h "0nng829yb0xa61q2sq0k4ka2wx94axg1v6p03bca2p3nrx576acp")))

(define-public crate-stream-inmemory-0.1.1 (c (n "stream-inmemory") (v "0.1.1") (h "0bvva5rw6sqp53b70wq1b0pbqyn7rf030yskd20mmgdpbq61pzi8") (y #t)))

(define-public crate-stream-inmemory-0.2.0 (c (n "stream-inmemory") (v "0.2.0") (h "1z287l88fydnlmj94vccki4qjjzxbxzg6mrp43fh0fcg95dakc93")))

(define-public crate-stream-inmemory-0.3.0 (c (n "stream-inmemory") (v "0.3.0") (h "11z0j4s88gr72gifpbjix5ki6vs9gjqrnkisfxi7f8hkl1gi4gjg")))

(define-public crate-stream-inmemory-0.3.1 (c (n "stream-inmemory") (v "0.3.1") (h "1wssmlqrl547sjbh299h8xr9njnkkjimmgq1l8d6lcgn66mfcp3m")))

