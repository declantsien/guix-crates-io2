(define-module (crates-io st re streamdal-gjson) #:use-module (crates-io))

(define-public crate-streamdal-gjson-0.8.1-with-set-rename (c (n "streamdal-gjson") (v "0.8.1-with-set-rename") (h "0b7byn42crm59gafvi4qwfm6sh5x1xl9n8mkgff5hy1lhkdvnldw")))

(define-public crate-streamdal-gjson-0.8.1-delete-path (c (n "streamdal-gjson") (v "0.8.1-delete-path") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)))) (h "06axj96w08hd46d2kf2v5xzh4cn529n1rnsmiybnwh342gg2vfwi")))

(define-public crate-streamdal-gjson-0.8.2-delete-path (c (n "streamdal-gjson") (v "0.8.2-delete-path") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)))) (h "177339wmjkrzzlw0d4wr21v7c3dcd0yxq8pwgsvk968c22afidzn")))

(define-public crate-streamdal-gjson-0.8.3-add-traits (c (n "streamdal-gjson") (v "0.8.3-add-traits") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)))) (h "03448950zg6v6lkvadagc5vgh3qhghx0k2c69pbj4snawc19f4s7")))

(define-public crate-streamdal-gjson-0.8.4-delete-path-fix (c (n "streamdal-gjson") (v "0.8.4-delete-path-fix") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)))) (h "1qgkgx1vbqi61lmyzvyhxjbsk33d2ryl9pkhvw6jm34zi9r3a3j7")))

