(define-module (crates-io st re streaming-zip) #:use-module (crates-io))

(define-public crate-streaming-zip-0.1.0 (c (n "streaming-zip") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)))) (h "134nqysg79vikawl7wswv77pl1bx3yafywzrh01ifdif5nszv7mn")))

(define-public crate-streaming-zip-0.2.0 (c (n "streaming-zip") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)))) (h "0kvpzlpv1njx98zi9y01ac3q33nihir7s4vxlz25ign64fd8nyrm")))

(define-public crate-streaming-zip-0.3.0 (c (n "streaming-zip") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)))) (h "0y8nj7vnmhrb4jllbgb50h207aw2741qvgag00dl0i03cqw2h8g4")))

(define-public crate-streaming-zip-0.4.0 (c (n "streaming-zip") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)))) (h "1kgyqxvi3d5mg015llwxrxhq0z9iljszrwaxqlxzbv2hqgczyri5")))

(define-public crate-streaming-zip-0.5.0 (c (n "streaming-zip") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)))) (h "1n0df02rl8xahjmw2ss4rxicicz21zxz40jzih24njs1j10502m4")))

