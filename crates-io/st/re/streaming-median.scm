(define-module (crates-io st re streaming-median) #:use-module (crates-io))

(define-public crate-streaming-median-0.1.0 (c (n "streaming-median") (v "0.1.0") (d (list (d (n "arraydeque") (r "^0.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "xorshift") (r "^0.1.3") (d #t) (k 0)))) (h "1j8hfh9wwml2dhc3f0mbq6pb71fa4mbgrzr0x8gnrd1sx4jpd2ij")))

