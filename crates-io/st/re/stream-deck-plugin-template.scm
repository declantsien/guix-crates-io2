(define-module (crates-io st re stream-deck-plugin-template) #:use-module (crates-io))

(define-public crate-stream-deck-plugin-template-0.1.0 (c (n "stream-deck-plugin-template") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "stream-deck-plugin") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "05gl6xja2gg2f838702avn1lr4482dssyfn3f4n1i5a03p1lcrlp")))

