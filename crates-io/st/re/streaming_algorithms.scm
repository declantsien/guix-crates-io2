(define-module (crates-io st re streaming_algorithms) #:use-module (crates-io))

(define-public crate-streaming_algorithms-0.1.0 (c (n "streaming_algorithms") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)))) (h "04fhcb5h8l9gz84dqxk3lypain3d52zck154drljvv4mmyj6gcyc") (f (quote (("simd-accel" "bytecount/simd-accel") ("avx-accel" "bytecount/avx-accel"))))))

(define-public crate-streaming_algorithms-0.1.1 (c (n "streaming_algorithms") (v "0.1.1") (d (list (d (n "packed_simd") (r "^0.3") (f (quote ("into_bits"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)))) (h "1d9yvh2kixz800rag0fapyf8npp1kz8l1609fbdbb7gi51mgr41y")))

(define-public crate-streaming_algorithms-0.1.2 (c (n "streaming_algorithms") (v "0.1.2") (d (list (d (n "packed_simd") (r "^0.3") (f (quote ("into_bits"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)))) (h "0556h07v4shd0jn9rs5lfsicldgw6fy1qlwyqbcpnhhavrz4vzv8")))

(define-public crate-streaming_algorithms-0.2.0 (c (n "streaming_algorithms") (v "0.2.0") (d (list (d (n "packed_simd") (r "^0.3") (f (quote ("into_bits"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)))) (h "0bd79w3h0j4k4fvfqskkz6kvlfjlm7szcm64a60h328gv9957cji")))

(define-public crate-streaming_algorithms-0.3.0 (c (n "streaming_algorithms") (v "0.3.0") (d (list (d (n "packed_simd") (r "^0.3") (f (quote ("into_bits"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)))) (h "03v7yqfv6asg9kygyg07rm9frcri8xcc9vk8bp4qa0vysf8yp9qx") (f (quote (("nightly" "packed_simd"))))))

