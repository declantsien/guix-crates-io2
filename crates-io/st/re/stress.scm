(define-module (crates-io st re stress) #:use-module (crates-io))

(define-public crate-stress-0.1.0 (c (n "stress") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1k9c1xd5q0hj2yl53gjkhb84yavw699g6n355f8ygpzf9did8hj6")))

(define-public crate-stress-0.1.1 (c (n "stress") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "04z7fyd1a3jixhw6wcpc8g2pcibz7d2480ib0am0zwqn1p15p4b3")))

(define-public crate-stress-0.2.0 (c (n "stress") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "182pkrkvqpaqsgcn0kdvmpcibw1xghsgacskdw5ljnwhb2265lzc")))

(define-public crate-stress-0.2.1 (c (n "stress") (v "0.2.1") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1l3v8yjb8mnhk3wsc1njhil37p0r3xlfd8m9ccfs6psf8yzk898r")))

(define-public crate-stress-0.3.0 (c (n "stress") (v "0.3.0") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "06nscinw0lsz967xqzkm6vsm1qn5cjrm6v2xd996lxbzc35mhhvn")))

