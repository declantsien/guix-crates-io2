(define-module (crates-io st re streams) #:use-module (crates-io))

(define-public crate-streams-0.0.1 (c (n "streams") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "04zb8333qmaaafnclxr7ayj1ny9pmq1gfyicxp9d61j3r64hjhff")))

(define-public crate-streams-0.0.2 (c (n "streams") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0q6xz3cs1l9zg38kqi8d4s6ba7lilfl9rvabsg49vc63hqsxz8r5")))

