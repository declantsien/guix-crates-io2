(define-module (crates-io st re stream_buffer) #:use-module (crates-io))

(define-public crate-stream_buffer-0.2.0 (c (n "stream_buffer") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0") (d #t) (k 0)))) (h "12cs3g9gdjvb1d7zgafg5qkm2bqlvynf9kf16id7a39lbblf36ag")))

