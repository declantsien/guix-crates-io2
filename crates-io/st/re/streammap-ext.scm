(define-module (crates-io st re streammap-ext) #:use-module (crates-io))

(define-public crate-streammap-ext-0.1.0 (c (n "streammap-ext") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.2.0") (f (quote ("full" "test-util"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "17am1p8hfvbraaggczcsm97lri3sw7bdm0sg8a4vp501i9yykgld")))

