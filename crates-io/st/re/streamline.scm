(define-module (crates-io st re streamline) #:use-module (crates-io))

(define-public crate-streamline-0.0.1 (c (n "streamline") (v "0.0.1") (h "01kna8q39msq60w214ii86qf3c4nxv81yf82fs9w63nfrr4k98xq") (y #t)))

(define-public crate-streamline-0.0.2 (c (n "streamline") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.27") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (k 2)))) (h "0s71f97nqlay72crh3wphankldi10bwjw44lxdyib9f2jxjb41p9")))

(define-public crate-streamline-0.0.3 (c (n "streamline") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1.27") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (k 2)))) (h "14apx1f02nccj8ma0irss89hhcjjg1wx22hq4va8m3bmbvj7k8k1")))

(define-public crate-streamline-0.0.4 (c (n "streamline") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1.27") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (k 2)))) (h "15vqqamvwkzvksi3dd35vdaw5ir09bjycglziyaxnpm45l1as0dj")))

(define-public crate-streamline-0.0.5 (c (n "streamline") (v "0.0.5") (d (list (d (n "async-trait") (r "^0.1.27") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (k 2)))) (h "0z9i7q3k0w04wpzpfzr83vifb2xkhfhkh6h18lnqxzg7kk9fz0x4")))

(define-public crate-streamline-0.0.6 (c (n "streamline") (v "0.0.6") (d (list (d (n "async-trait") (r "^0.1.27") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (k 2)))) (h "0g7b71vny6d396lw85yny9yiqxqc55py521k7n34xji6js6g0c6l")))

(define-public crate-streamline-0.0.7 (c (n "streamline") (v "0.0.7") (d (list (d (n "async-trait") (r "^0.1.27") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2.13") (k 2)))) (h "1ysq33psl5h84pcnc17bnh9jkwcladv0gdlp6c9sa6n5kipvslgy")))

