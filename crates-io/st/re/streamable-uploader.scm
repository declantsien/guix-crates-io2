(define-module (crates-io st re streamable-uploader) #:use-module (crates-io))

(define-public crate-streamable-uploader-0.1.0 (c (n "streamable-uploader") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("multipart" "blocking"))) (d #t) (k 0)))) (h "0sgaqfz9mwhza3acl68pwqg9fyrdsxkwf94dy6as8jk8lb864kcq")))

(define-public crate-streamable-uploader-0.1.1 (c (n "streamable-uploader") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("multipart" "blocking"))) (d #t) (k 0)) (d (n "rpassword") (r "^5.0") (d #t) (k 0)))) (h "1mhp4zxadxxapbmf1rk5lnhpxq0yj8i5i53nxd1wq0jskmilzg7f")))

(define-public crate-streamable-uploader-0.1.2 (c (n "streamable-uploader") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("multipart" "blocking"))) (d #t) (k 0)) (d (n "rpassword") (r "^5.0") (d #t) (k 0)))) (h "01c5dpxxf641c7p7fmzdaj60qr8ya6mqy1lpcx9gf1fnrwdak3ky")))

(define-public crate-streamable-uploader-0.1.3 (c (n "streamable-uploader") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("multipart" "blocking" "json"))) (d #t) (k 0)) (d (n "rpassword") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c7llxl32pyl0k9560qj1ck0hcm6yb7qaj6xkj0plmna8qc3k0a3")))

