(define-module (crates-io st re streamdal-wasm-transform) #:use-module (crates-io))

(define-public crate-streamdal-wasm-transform-0.0.8 (c (n "streamdal-wasm-transform") (v "0.0.8") (d (list (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.1-with-set-rename") (d #t) (k 0)) (d (n "streamdal-protos") (r "^0.0.115") (d #t) (k 0)))) (h "00368s04wd301hdalipid44bqlkmxld1rkq90lknrn1adcyg15yi")))

(define-public crate-streamdal-wasm-transform-0.0.9 (c (n "streamdal-wasm-transform") (v "0.0.9") (d (list (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.1-with-set-rename") (d #t) (k 0)) (d (n "streamdal-protos") (r "^0.0.115") (d #t) (k 0)))) (h "0j5rdcwqypm2a8rgjis04k45nwis5gxf7igh7wz8c0k9kf9rzyga")))

(define-public crate-streamdal-wasm-transform-0.0.10 (c (n "streamdal-wasm-transform") (v "0.0.10") (d (list (d (n "sha256") (r "=1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.1-with-set-rename") (d #t) (k 0)) (d (n "streamdal-protos") (r "=0.0.115") (d #t) (k 0)))) (h "19bspvrp128gf9w5ahzhlgg1hri90w4s6xfhyrqb1wc1am11ckqy")))

(define-public crate-streamdal-wasm-transform-0.0.11 (c (n "streamdal-wasm-transform") (v "0.0.11") (d (list (d (n "sha256") (r "=1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.1-with-set-rename") (d #t) (k 0)) (d (n "streamdal-protos") (r "=0.0.115") (d #t) (k 0)))) (h "0w46kc76sshjcwy83vrmfqv7h49ldlyjw9z5vk3jjlp1bv4z10g5")))

(define-public crate-streamdal-wasm-transform-0.0.1 (c (n "streamdal-wasm-transform") (v "0.0.1") (d (list (d (n "sha256") (r "=1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.1-with-set-rename") (d #t) (k 0)) (d (n "streamdal-protos") (r "=0.0.115") (d #t) (k 0)))) (h "08bg5mcqry08r2a6js6zq1qgjd840930frh8j2z59vlfbkhs427g")))

(define-public crate-streamdal-wasm-transform-0.0.2 (c (n "streamdal-wasm-transform") (v "0.0.2") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "sha256") (r "=1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.2-delete-path") (d #t) (k 0)) (d (n "streamdal-protos") (r "=0.1.6") (d #t) (k 0)))) (h "0jy8ibjkl1mq3krfnwzr9ny85qkh2pfpy19rncqqszp81jpyp4qf")))

(define-public crate-streamdal-wasm-transform-0.1.1 (c (n "streamdal-wasm-transform") (v "0.1.1") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "sha256") (r "=1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.2-delete-path") (d #t) (k 0)) (d (n "streamdal-protos") (r "=0.1.6") (d #t) (k 0)))) (h "1p6zbivpjl1ymqp9w2kcny5fgffl26knj3p9hsv5ypqrmcigrxfn")))

(define-public crate-streamdal-wasm-transform-0.1.2 (c (n "streamdal-wasm-transform") (v "0.1.2") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "sha256") (r "=1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.2-delete-path") (d #t) (k 0)) (d (n "streamdal-protos") (r "=0.1.7") (d #t) (k 0)))) (h "1j1lrbl5m3nl0j3zqw9x30jajbk2g62g2lpgf0dgpgzfrnkfr9wz")))

(define-public crate-streamdal-wasm-transform-0.1.3 (c (n "streamdal-wasm-transform") (v "0.1.3") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "sha256") (r "=1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.2-delete-path") (d #t) (k 0)) (d (n "streamdal-protos") (r "=0.1.7") (d #t) (k 0)))) (h "12z6m194mi35z3jnyfdq15cssk4rk9wsjn2m150qbigkragymv1p")))

(define-public crate-streamdal-wasm-transform-0.1.4 (c (n "streamdal-wasm-transform") (v "0.1.4") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "sha256") (r "=1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.2-delete-path") (d #t) (k 0)) (d (n "streamdal-protos") (r "=0.1.8") (d #t) (k 0)))) (h "1q81lx31zpy7plqlswmz5jlxjwxmdl5rsbf6pv6ssk1j6aj5gg80")))

(define-public crate-streamdal-wasm-transform-0.1.5 (c (n "streamdal-wasm-transform") (v "0.1.5") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "sha256") (r "=1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.2-delete-path") (d #t) (k 0)) (d (n "streamdal-protos") (r "=0.1.8") (d #t) (k 0)))) (h "1z4dhfdp2q001ki4pz959n955sq1hy7ndx2xc59yxrd1cbm9h2w3")))

(define-public crate-streamdal-wasm-transform-0.1.6 (c (n "streamdal-wasm-transform") (v "0.1.6") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "sha256") (r "=1.1.4") (d #t) (k 0)) (d (n "streamdal-gjson") (r "^0.8.2-delete-path") (d #t) (k 0)) (d (n "streamdal-protos") (r "=0.1.10") (d #t) (k 0)))) (h "16wydwlph1rhbblfbg23czxrr033kbf6np7wy0qyh1zn5pb87938")))

