(define-module (crates-io st re stream-cipher) #:use-module (crates-io))

(define-public crate-stream-cipher-0.1.0 (c (n "stream-cipher") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "0xq7kbk5w85nilsc0jn3l550yms8jkl8qwzvig8h0wl4n9nbqjdc") (f (quote (("dev")))) (y #t)))

(define-public crate-stream-cipher-0.1.1 (c (n "stream-cipher") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "1aql70r7qmqr8mfq63gqf97pygw5jmggkrppvh7wws8d8wc63p1h") (f (quote (("std") ("dev")))) (y #t)))

(define-public crate-stream-cipher-0.2.0 (c (n "stream-cipher") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "1xlmljzrdib9qai34ra50p50m8711hdp66719cm69xrx83s6b8f8") (f (quote (("std") ("dev")))) (y #t)))

(define-public crate-stream-cipher-0.2.1 (c (n "stream-cipher") (v "0.2.1") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "1jhjg4z85wgnfmcb074ir2l27jp60m4i6wwvi1bn4mc6bmcnxx2y") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-stream-cipher-0.2.2 (c (n "stream-cipher") (v "0.2.2") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "17wxmvynd5qg8yzblcbsmsf97xk0pz5ykk4l0cxv50camwsc2skn") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-stream-cipher-0.3.0 (c (n "stream-cipher") (v "0.3.0") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "1g1nd8r6pph70rzk5yyvg7a9ji7pkap9ddiqpp4v9xa9ys0bqqc8") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-stream-cipher-0.3.1 (c (n "stream-cipher") (v "0.3.1") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "0scsc7mrmqb7nj2xgiz0mm7wgla4440whim6bihhiyxsd4s1p8sq") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-stream-cipher-0.3.2 (c (n "stream-cipher") (v "0.3.2") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "1333qng84n6b15p8kndhajlgvbp1rgdddx04xgsvrjlnb1m2acc1") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-stream-cipher-0.4.0 (c (n "stream-cipher") (v "0.4.0") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "05ld6a3mlhnsf6rpcdb2njw745fdfx1m2vma77gy0a2mxhb135lp") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-stream-cipher-0.4.1 (c (n "stream-cipher") (v "0.4.1") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "120y04k3d2jyfnvyrlf38x6bf0yckyk30c7zf8v8qaq4fjcyvy09") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-stream-cipher-0.5.0 (c (n "stream-cipher") (v "0.5.0") (d (list (d (n "blobby") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1qy8qd6hhqc69slgvbyz09nbdl755jnfj23ii5hrz385270hcwls") (f (quote (("std") ("dev" "blobby") ("default" "block-cipher"))))))

(define-public crate-stream-cipher-0.6.0 (c (n "stream-cipher") (v "0.6.0") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "18xakfy0gqlcgd98gvc8pi98h0shyjw8a7lxiz5xjwjmpjnsk9vg") (f (quote (("std") ("dev" "blobby") ("default" "block-cipher"))))))

(define-public crate-stream-cipher-0.7.0 (c (n "stream-cipher") (v "0.7.0") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1m9l7hj0rw8np3b6vyhp3c2h13r121h8xz48gkxl3qdxyng0grn1") (f (quote (("std") ("dev" "blobby") ("default" "block-cipher")))) (y #t)))

(define-public crate-stream-cipher-0.7.1 (c (n "stream-cipher") (v "0.7.1") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "12gs5kaff52qk4i7hc7z33saq56c2nk3w9acn9fz5n6qk3w1a3n8") (f (quote (("std") ("dev" "blobby") ("default" "block-cipher"))))))

(define-public crate-stream-cipher-0.8.0-pre (c (n "stream-cipher") (v "0.8.0-pre") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "17jvbw3aygs8vi50v1cmbnzjhn038g0miwzm3i3588irny32ghl1") (f (quote (("std") ("dev" "blobby") ("default" "block-cipher"))))))

(define-public crate-stream-cipher-0.99.99 (c (n "stream-cipher") (v "0.99.99") (h "0dyzsvkk7wr2sscwh9kfw0mxj7d69ah5aj9l9lc5ha4jfm4qg0aw")))

