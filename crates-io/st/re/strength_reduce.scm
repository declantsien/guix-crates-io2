(define-module (crates-io st re strength_reduce) #:use-module (crates-io))

(define-public crate-strength_reduce-0.1.0 (c (n "strength_reduce") (v "0.1.0") (h "1f123m503fn28vafc59yrxpay667s91sccfjc3f0722bv5myhj0p")))

(define-public crate-strength_reduce-0.1.1 (c (n "strength_reduce") (v "0.1.1") (h "1qrps40kwhbn4wlvgq47wh621rcpivzf0vnjshdjc07zwvvj44ng")))

(define-public crate-strength_reduce-0.2.0 (c (n "strength_reduce") (v "0.2.0") (h "08hdsc8v300hyna3c5llnxcm2qz7n7z00n0x609i7iwrrr58m1j5") (y #t)))

(define-public crate-strength_reduce-0.2.1 (c (n "strength_reduce") (v "0.2.1") (d (list (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "0wpc682lmjlvbp71ifczh8c9y34k7h9qgfbw9j3glzf6nh957ix2")))

(define-public crate-strength_reduce-0.2.2 (c (n "strength_reduce") (v "0.2.2") (d (list (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "0vp9x1yj09sr99m46vayf26brzg4b2vk4m0fkvzdpbrgpba1nnwn")))

(define-public crate-strength_reduce-0.2.3 (c (n "strength_reduce") (v "0.2.3") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0m12phq654mfxpmh2h5akqkag5ha6nlhjc2bp9jwarr5r1qjzzx3")))

(define-public crate-strength_reduce-0.2.4 (c (n "strength_reduce") (v "0.2.4") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "10jdq9dijjdkb20wg1dmwg447rnj37jbq0mwvbadvqi2gys5x2gy")))

