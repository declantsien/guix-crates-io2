(define-module (crates-io st re streamson-generator) #:use-module (crates-io))

(define-public crate-streamson-generator-5.0.0 (c (n "streamson-generator") (v "5.0.0") (d (list (d (n "streamson-lib") (r "^5.0.0") (d #t) (k 0)))) (h "1qd47gqajrb3lrxcynsamw0895611karqgcb2gd2zrqvir7fbamj")))

(define-public crate-streamson-generator-5.0.1 (c (n "streamson-generator") (v "5.0.1") (d (list (d (n "streamson-lib") (r "^5.0.1") (d #t) (k 0)))) (h "1h20sjijq9wla2y4mjw4ry434zgy6ki8al0alr2f7nnsiznh14ib")))

(define-public crate-streamson-generator-5.0.2 (c (n "streamson-generator") (v "5.0.2") (d (list (d (n "streamson-lib") (r "^5.0.2") (d #t) (k 0)))) (h "0vvssbpb8bwvfxgxw863ygxz0fvgw0fhzhrg523q1x77175v769p")))

(define-public crate-streamson-generator-6.0.0 (c (n "streamson-generator") (v "6.0.0") (d (list (d (n "streamson-lib") (r "^6.0.0") (d #t) (k 0)))) (h "1ikwi8hxzsrdw99f7krrii83x4xfdq4nwr6gkj4xlp74239gpni2")))

(define-public crate-streamson-generator-6.1.0 (c (n "streamson-generator") (v "6.1.0") (d (list (d (n "streamson-lib") (r "^6.1.0") (d #t) (k 0)))) (h "1k8amfkhzpc0kj00i4i6rz1cbf4izl8zsgsk49pr1yaazips16is")))

(define-public crate-streamson-generator-6.2.0 (c (n "streamson-generator") (v "6.2.0") (d (list (d (n "streamson-lib") (r "^6.2.0") (d #t) (k 0)))) (h "1sh9xrpdb4pmscrgq00sad374pw0fc1hpl0443r6m74y9sbjbzq1")))

(define-public crate-streamson-generator-6.3.0 (c (n "streamson-generator") (v "6.3.0") (d (list (d (n "streamson-lib") (r "^6.3.0") (d #t) (k 0)))) (h "0zygnrl9j4157rz0pgv57j472i1kbrnajfpsg1yynag5nkf2xvrz")))

(define-public crate-streamson-generator-6.3.1 (c (n "streamson-generator") (v "6.3.1") (d (list (d (n "streamson-lib") (r "^6.3.1") (d #t) (k 0)))) (h "0s4pslw61ldhvmb2019dai06kv48cf94d4cav6yrhw7alqf1b8hg")))

(define-public crate-streamson-generator-7.0.0 (c (n "streamson-generator") (v "7.0.0") (d (list (d (n "streamson-lib") (r "^7.0.0") (d #t) (k 0)))) (h "1gahg6dyg997abjknd5x9qjy87xra7hhd6bxvxnpzmnk5g1zzrml")))

(define-public crate-streamson-generator-7.0.1 (c (n "streamson-generator") (v "7.0.1") (d (list (d (n "streamson-lib") (r "^7.0.1") (d #t) (k 0)))) (h "0vlnnbrm8679pw0y203xc1193fix53y0xmkwalj7afc94vyckd06")))

(define-public crate-streamson-generator-7.1.0 (c (n "streamson-generator") (v "7.1.0") (d (list (d (n "streamson-lib") (r "^7.1.0") (d #t) (k 0)))) (h "0zwkz1iw97yn3prbd18ccankay5yjza592lmmj4yqzzcdrry9gm2")))

