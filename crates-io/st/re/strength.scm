(define-module (crates-io st re strength) #:use-module (crates-io))

(define-public crate-strength-0.1.0 (c (n "strength") (v "0.1.0") (h "09kdwjdx05pmf982m22s9vaynbl9sp7dgdd34736by2347907pjs") (y #t)))

(define-public crate-strength-0.1.1 (c (n "strength") (v "0.1.1") (h "0vm4kxkj534cnj9kq2ab9yn0lqyi0d4v47wqzbzjvsq7ss34xwxq") (y #t)))

