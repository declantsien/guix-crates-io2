(define-module (crates-io st re streambed-storage) #:use-module (crates-io))

(define-public crate-streambed-storage-0.9.1 (c (n "streambed-storage") (v "0.9.1") (d (list (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.90") (d #t) (k 0)) (d (n "streambed") (r "^0.9.1") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0drzhicbwfqfyg0f15hni6qqdhlpbz3bchc1jwvjfh8j9fccj5fg") (r "1.70.0")))

