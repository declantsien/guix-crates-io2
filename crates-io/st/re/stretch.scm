(define-module (crates-io st re stretch) #:use-module (crates-io))

(define-public crate-stretch-0.1.0 (c (n "stretch") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ref_eq") (r "^1.0.0") (d #t) (k 0)))) (h "1iqgh8f30jrqbsq86jzjp219zms5jqb7wqicf23h2dqqsy6j2vbl")))

(define-public crate-stretch-0.1.1 (c (n "stretch") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ref_eq") (r "^1.0.0") (d #t) (k 0)))) (h "0lp91lgzn4xm9hh4z6ijiazn2ycp67kqilnhf3k3n8b86cnm9md5")))

(define-public crate-stretch-0.1.2 (c (n "stretch") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ref_eq") (r "^1.0.0") (d #t) (k 0)))) (h "0n1yr7mis40n1pdvwm6jxw0s1bmvvlh9yj5x728xrvm77sfyz3a0")))

(define-public crate-stretch-0.1.3 (c (n "stretch") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ref_eq") (r "^1.0.0") (d #t) (k 0)))) (h "1khd30gwlnwipvn3vv4fgm7v7ijrvcd12vw1qpwfpazmz4j5k2n1")))

(define-public crate-stretch-0.1.4 (c (n "stretch") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ref_eq") (r "^1.0.0") (d #t) (k 0)))) (h "18c2s4s8sqj8sdmz492fxwpqna28cvlkaiyxdqv3bz4wfmcy2627")))

(define-public crate-stretch-0.1.5 (c (n "stretch") (v "0.1.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ref_eq") (r "^1.0.0") (d #t) (k 0)))) (h "1mn3zdvkg46gwca3666j9vv4hy1hbdzcjsj6nqy7g6ifmz2prqiv")))

(define-public crate-stretch-0.1.6 (c (n "stretch") (v "0.1.6") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ref_eq") (r "^1.0.0") (d #t) (k 0)))) (h "15mk5qgfdn0fz4c19scvckk0yv2wr6byvag7mczlsi0m8d6ms21m")))

(define-public crate-stretch-0.1.7 (c (n "stretch") (v "0.1.7") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ref_eq") (r "^1.0.0") (d #t) (k 0)))) (h "17hm479mhnd0m6ph2vq9wlp468sgh60cvqjkn8khfm5ggwlcci4f")))

(define-public crate-stretch-0.1.8 (c (n "stretch") (v "0.1.8") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "1rnws61dm7xf21gjk67cr4yjshzncwjgnwyvnqpddgyagmaqkpxs") (f (quote (("std") ("default" "std"))))))

(define-public crate-stretch-0.1.9 (c (n "stretch") (v "0.1.9") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)) (d (n "simple-error") (r "^0.1") (d #t) (k 0)))) (h "1vl1jplirb514fk1k59xsxy89dzvwp5z764rr6r0i0cjz4kz5jxd") (f (quote (("std") ("default" "std"))))))

(define-public crate-stretch-0.1.10 (c (n "stretch") (v "0.1.10") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "0552xwbx14qkvdgvpbj2dlbcpl041f5m1a5zgiwa3fd5zxb9m05c") (f (quote (("std") ("default" "std"))))))

(define-public crate-stretch-0.1.11 (c (n "stretch") (v "0.1.11") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "0szn36g22qv5d3hj6r6hj38gykxx7g9v5psgic3kvrrahpyxvq26") (f (quote (("std") ("default" "std"))))))

(define-public crate-stretch-0.1.12 (c (n "stretch") (v "0.1.12") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "0iklqaj4bc5vq31jjrai7sca8syq0s2l453gp4bdrx0l4qmd50cr") (f (quote (("std") ("default" "std"))))))

(define-public crate-stretch-0.2.0 (c (n "stretch") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "0g96rv8kkla2d2vg1hlnz2s8kq52f5a1ksxjjkddm0j3cwpw2yr4") (f (quote (("std") ("default" "std"))))))

(define-public crate-stretch-0.2.1 (c (n "stretch") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "16f9s4v93yppw6j0hpc4f0h767hh2g7scw4grpynzqa0j5djvxmr") (f (quote (("std") ("default" "std"))))))

(define-public crate-stretch-0.2.2 (c (n "stretch") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "1236y6k88pibcgkvjldsbm11hc1jwh18z42lll4jj6ic4hkij6dg") (f (quote (("std") ("default" "std"))))))

(define-public crate-stretch-0.3.0 (c (n "stretch") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "186c7y89llnbba7s1msfydyickgcf7g5qpasslcr9gazg9y04cch") (f (quote (("std") ("default" "std"))))))

(define-public crate-stretch-0.3.1 (c (n "stretch") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "1gnkgpr0gi14yxb15gq8r5r0wawlvyhawfrbj337i1s2k2jyay0k") (f (quote (("std") ("default" "std"))))))

(define-public crate-stretch-0.3.2 (c (n "stretch") (v "0.3.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)))) (h "11vdmli145j6yakgr7hkzgbnz1kqsb9rq3zrxl1g6dz11k9cc3bv") (f (quote (("std") ("default" "std"))))))

