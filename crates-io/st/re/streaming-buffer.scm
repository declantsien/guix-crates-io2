(define-module (crates-io st re streaming-buffer) #:use-module (crates-io))

(define-public crate-streaming-buffer-0.6.0 (c (n "streaming-buffer") (v "0.6.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0dybygrnh8xsn19m269pyb895giq9325xkax142zd62273m72531") (y #t)))

