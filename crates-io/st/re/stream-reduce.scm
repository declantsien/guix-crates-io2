(define-module (crates-io st re stream-reduce) #:use-module (crates-io))

(define-public crate-stream-reduce-0.1.0 (c (n "stream-reduce") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "13n5mypigy7832d6ff3jkpddn8spzl6qdrvfh8a05w5wlwk9c127")))

