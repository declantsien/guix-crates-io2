(define-module (crates-io st re stream_assert) #:use-module (crates-io))

(define-public crate-stream_assert-0.1.0 (c (n "stream_assert") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.28") (k 0)))) (h "1wc9chl8hivm2qv3y511fq39x837d9ay7n9zf6x8v4chvir14jaq")))

(define-public crate-stream_assert-0.1.1 (c (n "stream_assert") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.28") (k 0)))) (h "15jbd1wzd1pl7mc7wkhrcnj5km4bv51gng664w23y0f9ff56gavz")))

