(define-module (crates-io st re stream-vbyte) #:use-module (crates-io))

(define-public crate-stream-vbyte-0.1.0 (c (n "stream-vbyte") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "1iay7jrzrra5xbfg1n7c3lhdprw80y6hkkb92wpncmzlpn1w45fk")))

(define-public crate-stream-vbyte-0.2.0 (c (n "stream-vbyte") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "x86intrin") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "1vpsi899fxpc9yd8l3ghlj6s8fwywcvfk8689yxwmxrqanwk03jw") (f (quote (("x86_ssse3" "x86intrin") ("default"))))))

(define-public crate-stream-vbyte-0.3.0 (c (n "stream-vbyte") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "x86intrin") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "1488cla2v5lnf720gb4yqk8ylwkf4c2j7hc1q226h0jjw0riyxp4") (f (quote (("x86_ssse3" "x86intrin") ("x86_sse41" "x86intrin") ("default"))))))

(define-public crate-stream-vbyte-0.3.1 (c (n "stream-vbyte") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "x86intrin") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "0yzyavicih9lg1v9bgdvjk48g9y14w9fn66mrl8yfr1wd4qf41aj") (f (quote (("x86_ssse3" "x86intrin") ("x86_sse41" "x86intrin") ("default"))))))

(define-public crate-stream-vbyte-0.3.2 (c (n "stream-vbyte") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "x86intrin") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "19kjzrq87ysbsb0l1qfh4gxbbgszqwbhnq5ksfd11vvxwi2yikhl") (f (quote (("x86_ssse3" "x86intrin") ("x86_sse41" "x86intrin") ("default"))))))

(define-public crate-stream-vbyte-0.4.0 (c (n "stream-vbyte") (v "0.4.0") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "0sv6yda3ip7i2shxghw16ljr8nvg71zid7cll0xc3w205n38p746") (f (quote (("x86_ssse3") ("x86_sse41") ("default"))))))

(define-public crate-stream-vbyte-0.4.1 (c (n "stream-vbyte") (v "0.4.1") (d (list (d (n "clap") (r "^4.3.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0lcy3ch6h88c3cxkql219zy6jjkdnjmbwciyc3nhlzv9ph2s011g") (f (quote (("x86_ssse3") ("x86_sse41") ("default")))) (r "1.64.0")))

