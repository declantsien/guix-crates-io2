(define-module (crates-io st re stream-kmerge) #:use-module (crates-io))

(define-public crate-stream-kmerge-0.1.0 (c (n "stream-kmerge") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "sync"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1f1lgspsb3czrlk4iaxf139xyglrzs7cfsfkinkchf4cmn7q474k")))

(define-public crate-stream-kmerge-0.1.1 (c (n "stream-kmerge") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "sync"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1110d074rjlzmgai54zz0rv4h2n6ix7qi07d1halkqli4pl1m2ga")))

(define-public crate-stream-kmerge-0.1.2 (c (n "stream-kmerge") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "sync"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0ajy9b4l613i05nljb18rii2gnhbwsvfv2qv56i3mzg0frbywh7b")))

(define-public crate-stream-kmerge-0.2.0 (c (n "stream-kmerge") (v "0.2.0") (d (list (d (n "binary-heap-plus") (r "^0.4") (d #t) (k 0)) (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "sync"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "138q45cvlbbwprr2gdbm3jj36si79yw1jy07z83qsn0vnynzayq4")))

