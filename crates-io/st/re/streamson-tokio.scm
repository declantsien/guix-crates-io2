(define-module (crates-io st re streamson-tokio) #:use-module (crates-io))

(define-public crate-streamson-tokio-0.2.0 (c (n "streamson-tokio") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "0mzzmdhx5fwbaq9hbpxvzpjwz9jsphl9ia0smsxy16k21bq0i909")))

(define-public crate-streamson-tokio-1.0.0 (c (n "streamson-tokio") (v "1.0.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1dd92imi1szd0yk2npq1fckvkihxnhdqi058ab9nnzn4vdqrgpfm")))

(define-public crate-streamson-tokio-1.0.1 (c (n "streamson-tokio") (v "1.0.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1j4gflh5n9y2kn49hy989lz01bkhwx9lbhs9qvsc4gfwl5dpgrns")))

(define-public crate-streamson-tokio-1.0.2 (c (n "streamson-tokio") (v "1.0.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "01mh03n50jmk9lmykfhb7a297jnmgrs38832l99hj4diq3lpxmks")))

(define-public crate-streamson-tokio-2.0.0 (c (n "streamson-tokio") (v "2.0.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1h78z9v08142jhp0v4i5p0hrcq4vd2skrvxbx0kpj63yjxqpnzq1")))

(define-public crate-streamson-tokio-3.0.0 (c (n "streamson-tokio") (v "3.0.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1c0dj0vw00x8vzaqnsh5k2hqpl2kwwb52q4d1kngwla5wl1fsmwb")))

(define-public crate-streamson-tokio-4.0.0 (c (n "streamson-tokio") (v "4.0.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^4.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "0x1m6cv1hlsghchxf76b5nqszfiybqnm10ji24jymbr1q36qvqni")))

(define-public crate-streamson-tokio-4.1.0 (c (n "streamson-tokio") (v "4.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^4.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "00p4wax3fxk9kfg30z62djb3fpnvv5xdw51awdk3lj23cs2py73a")))

(define-public crate-streamson-tokio-5.0.0 (c (n "streamson-tokio") (v "5.0.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^5.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "0jk5s1wy8c6vn8lm4wwkpj7irs8i6fifbdnyrj9yidygsynmpvw6")))

(define-public crate-streamson-tokio-5.0.1 (c (n "streamson-tokio") (v "5.0.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^5.0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1hhvdpi77jj4cjfpq7hrxcvy0a6xs194pmf598vari9278a10gnj")))

(define-public crate-streamson-tokio-5.0.2 (c (n "streamson-tokio") (v "5.0.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^5.0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "0yrvcfay24s70fpimh2svxxv80r88b0xkk8f06j6kf68kr5idjwv")))

(define-public crate-streamson-tokio-6.0.0 (c (n "streamson-tokio") (v "6.0.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^6.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "14afmbm9r137r02dlyhlggbdv8vp36pg10s145v5k1zhj41srnxk")))

(define-public crate-streamson-tokio-6.1.0 (c (n "streamson-tokio") (v "6.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^6.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1mz0zmq3xfhfq39ljhn5fqv15qyryv511fs9dalf3f3z4wy3p9v4")))

(define-public crate-streamson-tokio-6.2.0 (c (n "streamson-tokio") (v "6.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^6.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.4") (f (quote ("codec"))) (d #t) (k 0)))) (h "1zay04fzxgk3fvbngw6qhzn8z127kqn2szvxy1ykvypib47iljag")))

(define-public crate-streamson-tokio-6.3.0 (c (n "streamson-tokio") (v "6.3.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^6.3.0") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.4") (f (quote ("codec"))) (d #t) (k 0)))) (h "1by8rjvv7lby9zw6lrgrmg7h5l7rfh8p1xqq7zagsa0ssqvkc2ry")))

(define-public crate-streamson-tokio-6.3.1 (c (n "streamson-tokio") (v "6.3.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^6.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.4") (f (quote ("codec"))) (d #t) (k 0)))) (h "1nhxxassz37qwgrwyqwzf4mwz7s11i8mizw3qp4spn64fzsglszz")))

(define-public crate-streamson-tokio-7.0.0 (c (n "streamson-tokio") (v "7.0.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^7.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.4") (f (quote ("codec"))) (d #t) (k 0)))) (h "1bgf0k2079j7jqln4q6avc887bndw3nhz4rpf1l34fqdbbjqdxib")))

(define-public crate-streamson-tokio-7.0.1 (c (n "streamson-tokio") (v "7.0.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^7.0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.4") (f (quote ("codec"))) (d #t) (k 0)))) (h "09n60pvghclcw8jvrgmw5avlxj3nbakc1h7d7r2k646pfjjl4732")))

(define-public crate-streamson-tokio-7.1.0 (c (n "streamson-tokio") (v "7.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "streamson-lib") (r "^7.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.4") (f (quote ("codec"))) (d #t) (k 0)))) (h "18rvxpgaz7fbdfbybl3jl4pqi8476x994rmqy50k760hidnynn1i")))

