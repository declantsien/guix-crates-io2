(define-module (crates-io st re stremio-serde-hex) #:use-module (crates-io))

(define-public crate-stremio-serde-hex-0.1.0 (c (n "stremio-serde-hex") (v "0.1.0") (d (list (d (n "array-init") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0s8f2jd7lmd1aby10xmaya2siswl9phsd7qa9imiwymrsv200mjs")))

