(define-module (crates-io st re stream-jenkins-logs) #:use-module (crates-io))

(define-public crate-stream-jenkins-logs-0.1.0 (c (n "stream-jenkins-logs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "06c0frx2ja85dc01di27jmvvql5m82qa4n0hhjhlzkdza6c9sx19")))

(define-public crate-stream-jenkins-logs-0.1.1 (c (n "stream-jenkins-logs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0ykil0q1pkkdqbpjsjr3hni3xaj16f2pnvrydzi2f04bxjx306q2")))

(define-public crate-stream-jenkins-logs-0.1.2 (c (n "stream-jenkins-logs") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "00hqrqwsj5kfa07cvgdh53kdm4pjxakbldbhkkhq7jhxfapbg3si")))

