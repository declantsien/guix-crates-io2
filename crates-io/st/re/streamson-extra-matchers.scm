(define-module (crates-io st re streamson-extra-matchers) #:use-module (crates-io))

(define-public crate-streamson-extra-matchers-4.1.0 (c (n "streamson-extra-matchers") (v "4.1.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "streamson-lib") (r "^4.1.0") (d #t) (k 0)))) (h "153jcz18nm2j34mf75hd5il9m8790835af5vnclpnjfscyifzbnr") (f (quote (("with_regex" "regex") ("default" "with_regex"))))))

(define-public crate-streamson-extra-matchers-5.0.0 (c (n "streamson-extra-matchers") (v "5.0.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "streamson-lib") (r "^5.0.0") (d #t) (k 0)))) (h "0382qkwy4v6qbc2l8z8q9ns3636nh3p07dlw02drnw5rncmqs255") (f (quote (("with_regex" "regex") ("default" "with_regex"))))))

(define-public crate-streamson-extra-matchers-5.0.1 (c (n "streamson-extra-matchers") (v "5.0.1") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "streamson-lib") (r "^5.0.1") (d #t) (k 0)))) (h "0yx7x45fjy7ndyl934fabdajip3kiv6n9jknxqkk1qmg39cladrs") (f (quote (("with_regex" "regex") ("default" "with_regex"))))))

(define-public crate-streamson-extra-matchers-5.0.2 (c (n "streamson-extra-matchers") (v "5.0.2") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "streamson-lib") (r "^5.0.2") (d #t) (k 0)))) (h "1c0d4j7kybk1dpx6h32xlk3flk7kl1n8v0grhm8r3dz36bjqxps4") (f (quote (("with_regex" "regex") ("default" "with_regex"))))))

(define-public crate-streamson-extra-matchers-6.0.0 (c (n "streamson-extra-matchers") (v "6.0.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "streamson-lib") (r "^6.0.0") (d #t) (k 0)))) (h "1y289233gz96x4qrkiq00xg9qgm6xvmymfdid0b4dz5n312vwy5j") (f (quote (("with_regex" "regex") ("default" "with_regex"))))))

(define-public crate-streamson-extra-matchers-6.1.0 (c (n "streamson-extra-matchers") (v "6.1.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "streamson-lib") (r "^6.1.0") (d #t) (k 0)))) (h "0lnzrhgb8d04c2j02sp3wc0dzc3kz6dx892bm0w8kp5f61kd0fwf") (f (quote (("with_regex" "regex") ("default" "with_regex"))))))

(define-public crate-streamson-extra-matchers-6.2.0 (c (n "streamson-extra-matchers") (v "6.2.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "streamson-lib") (r "^6.2.0") (d #t) (k 0)))) (h "0m5kr15zvzwyq69vriwi09sbc4d1h43g3bhf6fakav69l7s59ywq") (f (quote (("with_regex" "regex") ("default" "with_regex"))))))

(define-public crate-streamson-extra-matchers-6.3.0 (c (n "streamson-extra-matchers") (v "6.3.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "streamson-lib") (r "^6.3.0") (d #t) (k 0)))) (h "0z3yghhffz1yw91rq66rb12vw5z1cwg6h1ljrl3a8ypxbx9dvml3") (f (quote (("with_regex" "regex") ("default" "with_regex"))))))

(define-public crate-streamson-extra-matchers-6.3.1 (c (n "streamson-extra-matchers") (v "6.3.1") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "streamson-lib") (r "^6.3.1") (d #t) (k 0)))) (h "1i02cinrqhz1w50fpvimqvmhd2290ixbk2fmxpyh1mc08mwxkmgn") (f (quote (("with_regex" "regex") ("default" "with_regex"))))))

