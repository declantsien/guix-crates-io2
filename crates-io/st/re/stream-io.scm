(define-module (crates-io st re stream-io) #:use-module (crates-io))

(define-public crate-stream-io-0.0.0 (c (n "stream-io") (v "0.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.4") (d #t) (k 0)))) (h "1nxbb5ifkg1mwb77j8npjbwnn4wydraphxqkwraw90wlcgbl26c5") (f (quote (("default") ("csharp"))))))

