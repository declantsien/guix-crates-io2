(define-module (crates-io st re stream-window) #:use-module (crates-io))

(define-public crate-stream-window-0.1.0 (c (n "stream-window") (v "0.1.0") (d (list (d (n "async-stream") (r ">=0.1.0") (d #t) (k 0)) (d (n "async-stream") (r ">=0.1.0") (d #t) (k 2)) (d (n "futures") (r ">=0.1.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.1.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-stream") (r ">=0.1.0") (d #t) (k 0)) (d (n "tokio-test") (r ">=0.1.0") (d #t) (k 2)))) (h "0j24kwa0z35583fd5px1xnrf5b1y8ghpnzh9pxzjb2992s9fgpq9")))

(define-public crate-stream-window-0.1.1 (c (n "stream-window") (v "0.1.1") (d (list (d (n "async-stream") (r ">=0.1.0") (d #t) (k 0)) (d (n "async-stream") (r ">=0.1.0") (d #t) (k 2)) (d (n "futures") (r ">=0.1.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.1.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-stream") (r ">=0.1.0") (d #t) (k 0)) (d (n "tokio-test") (r ">=0.1.0") (d #t) (k 2)))) (h "0ss9r9mm2gcni120hvr118jdvwdbm4bg9xn2hyvlqfxmcxb7mh3c")))

(define-public crate-stream-window-0.1.2 (c (n "stream-window") (v "0.1.2") (d (list (d (n "async-stream") (r ">=0.1.0") (d #t) (k 0)) (d (n "async-stream") (r ">=0.1.0") (d #t) (k 2)) (d (n "futures") (r ">=0.1.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.1.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio-stream") (r ">=0.1.0") (d #t) (k 0)) (d (n "tokio-test") (r ">=0.1.0") (d #t) (k 2)))) (h "1ry51kc1i98mpq8pabw76l1p7zl5hqgd1xrhfqb5zcnjckcdh0jr")))

