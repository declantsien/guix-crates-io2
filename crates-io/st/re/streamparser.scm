(define-module (crates-io st re streamparser) #:use-module (crates-io))

(define-public crate-streamparser-0.1.0 (c (n "streamparser") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "direct-executor") (r "^0.3.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.5") (k 0)) (d (n "futures-io") (r "^0.3.5") (o #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (k 2)) (d (n "nom") (r "^5.1.1") (o #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.7") (k 0)))) (h "1kzjiwfd5hwmhzsfyzpaibmlmhdc67lcaiz58akaw3vjpz8scqgc") (f (quote (("std" "alloc" "futures-io" "futures-io/std") ("nom-adapters" "nom") ("default" "std") ("alloc"))))))

