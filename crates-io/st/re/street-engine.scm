(define-module (crates-io st re street-engine) #:use-module (crates-io))

(define-public crate-street-engine-0.1.0 (c (n "street-engine") (v "0.1.0") (d (list (d (n "fastlem") (r "^0.1.4") (d #t) (k 2)) (d (n "naturalneighbor") (r "^1.2.2") (d #t) (k 2)) (d (n "noise") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 2)) (d (n "rstar") (r "^0.12.0") (d #t) (k 0)) (d (n "terrain-graph") (r "^1.0.1") (d #t) (k 2)) (d (n "tiny-skia") (r "^0.11.4") (d #t) (k 2)))) (h "1k1libygmfc25cckfl2wvs2sa4gk2y4c8gn4yqb3lis42dg1hy8v")))

