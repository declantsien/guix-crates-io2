(define-module (crates-io st re stream-consumer-task) #:use-module (crates-io))

(define-public crate-stream-consumer-task-0.1.0 (c (n "stream-consumer-task") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 2)) (d (n "rdkafka") (r "^0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0") (d #t) (k 0)))) (h "0miwr4vpxpvaa21fz9kl4hgjknjzmf88xfvcrvrz56mys6pgqywr") (s 2) (e (quote (("kafka" "dep:rdkafka"))))))

