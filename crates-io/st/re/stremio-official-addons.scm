(define-module (crates-io st re stremio-official-addons) #:use-module (crates-io))

(define-public crate-stremio-official-addons-2.0.0 (c (n "stremio-official-addons") (v "2.0.0") (h "0y909saxvkgkk3yk3x4j3795a3akj4s3ydhq7y5balvlpwys88gh")))

(define-public crate-stremio-official-addons-2.0.3 (c (n "stremio-official-addons") (v "2.0.3") (h "0kkxd0i61vrybq341cq8ygk72k4h9bql22plj467qnqgl084gahs")))

(define-public crate-stremio-official-addons-2.0.4 (c (n "stremio-official-addons") (v "2.0.4") (h "081q1fgrhsk1qmj4m2dgsv4lv5lyn7400hp5k95zwk5glcxda3gr")))

(define-public crate-stremio-official-addons-2.0.5 (c (n "stremio-official-addons") (v "2.0.5") (h "0b5zsdg8wbn39k0gfclirylk85ij3zc6nw23cn3h9k9wz48p0jz9")))

(define-public crate-stremio-official-addons-2.0.7 (c (n "stremio-official-addons") (v "2.0.7") (h "1im09hf7gl8kgscnfiwqsqjgpspnbcg4czfkxdam2il0d69b1k73")))

(define-public crate-stremio-official-addons-2.0.8 (c (n "stremio-official-addons") (v "2.0.8") (h "099139bm6w6pbqa14f1qhkfhaxbn3cmfckag47wd460vzaacxmq9")))

(define-public crate-stremio-official-addons-2.0.9 (c (n "stremio-official-addons") (v "2.0.9") (h "1c1iw4a8d5zwr8yq733p4h8wd4df1c4h4bxi47w46b6igi678nc8")))

(define-public crate-stremio-official-addons-2.0.10 (c (n "stremio-official-addons") (v "2.0.10") (h "1gprp4xsmr78snyjcgkmzkjfbcs02n3v7jyzfhvkvs16gbbncn7q")))

(define-public crate-stremio-official-addons-2.0.11 (c (n "stremio-official-addons") (v "2.0.11") (h "1dixc5bn4k9vcixbkv60prm00mv422qbqikfgad5gji64m38w1vy")))

(define-public crate-stremio-official-addons-2.0.12 (c (n "stremio-official-addons") (v "2.0.12") (d (list (d (n "once_cell") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (k 0)))) (h "1nbss4amx702l9nzr97gryzdq1fypk1vmvhfmpr66i19dy1va0sv") (f (quote (("std" "serde_json/std" "once_cell/std") ("json" "serde_json" "once_cell") ("default" "json" "std"))))))

