(define-module (crates-io st re stream-unpack) #:use-module (crates-io))

(define-public crate-stream-unpack-0.1.0 (c (n "stream-unpack") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03grhhs5qwsw8hng20k8iigr54g4k1jgwr67qk3gjg5xb0s0f02a") (f (quote (("zip-comments" "zip") ("zip") ("default" "zip" "deflate")))) (s 2) (e (quote (("deflate" "dep:inflate"))))))

(define-public crate-stream-unpack-0.1.1 (c (n "stream-unpack") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z61s5f19pq5hmbs6pr3mi903kjsnbaxfyvxrm7jr79rlqdglx2v") (f (quote (("zip-comments" "zip") ("zip") ("default" "zip" "deflate")))) (s 2) (e (quote (("deflate" "dep:inflate"))))))

(define-public crate-stream-unpack-0.1.2 (c (n "stream-unpack") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mxhm887ry5760mk6a5fnh45c5qg8sdr863fi2kr73pfd4jzd0m7") (f (quote (("zip-comments" "zip") ("zip") ("default" "zip" "deflate")))) (s 2) (e (quote (("deflate" "dep:inflate"))))))

(define-public crate-stream-unpack-1.0.0 (c (n "stream-unpack") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0s9r714zfmlymnp3l7w39qdxy3mwl2cmswj6b3dj75bzr1r1babm") (f (quote (("zip-comments" "zip") ("zip") ("default" "zip" "deflate")))) (s 2) (e (quote (("deflate" "dep:inflate"))))))

(define-public crate-stream-unpack-1.0.1 (c (n "stream-unpack") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00kgv66iybns7pnmiygfxxm0h9s6iklmps291m8zp3ks9yiaa973") (f (quote (("zip-comments" "zip") ("zip") ("default" "zip" "deflate")))) (s 2) (e (quote (("deflate" "dep:inflate"))))))

