(define-module (crates-io st re streetsweeper) #:use-module (crates-io))

(define-public crate-streetsweeper-0.1.0 (c (n "streetsweeper") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ik5a7ln09gxkm5v8asvl07vms503brxidy07bzvwmsm8wh5qagw")))

(define-public crate-streetsweeper-0.2.0 (c (n "streetsweeper") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0r27172binpm5j3a1xvcw53wxhib46jxbablhq9059z3fpclzmzn")))

