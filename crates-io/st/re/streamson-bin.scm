(define-module (crates-io st re streamson-bin) #:use-module (crates-io))

(define-public crate-streamson-bin-0.1.0 (c (n "streamson-bin") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "streamson-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0kcafyf0xql4z06l7l002hmfw2436j5kxz9w2x6bc7rdnh2na84d")))

(define-public crate-streamson-bin-0.2.0 (c (n "streamson-bin") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "streamson-lib") (r "^0.2.0") (d #t) (k 0)))) (h "0z4jvl4ndlhhg410gmq74xsq5a9k7jsp9wqmpbslzylqzy919gah")))

(define-public crate-streamson-bin-1.0.0 (c (n "streamson-bin") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "streamson-lib") (r "^1.0.0") (d #t) (k 0)))) (h "0qiwnj3lawxkzrkwpymzbxhvn71911pc3qh4p990z62c6zrakwlm")))

(define-public crate-streamson-bin-1.0.1 (c (n "streamson-bin") (v "1.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "streamson-lib") (r "^1.0.1") (d #t) (k 0)))) (h "0bpbzcl28nskn0rlynyp7g2iqss7hjc6xza352phgr5rshy0sim7")))

(define-public crate-streamson-bin-1.0.2 (c (n "streamson-bin") (v "1.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "streamson-lib") (r "^1.0.2") (d #t) (k 0)))) (h "05bas0abgpcdyacazkcgn2jynlg1hlvxlz89wd2lswdfh7jy2hpb")))

(define-public crate-streamson-bin-2.0.0 (c (n "streamson-bin") (v "2.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "streamson-lib") (r "^2.0.0") (d #t) (k 0)))) (h "02i9w6jl8nk62fh523fqz0wz5v4baj5a2g9rpagr02yi174dmhdd")))

(define-public crate-streamson-bin-3.0.0 (c (n "streamson-bin") (v "3.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "streamson-lib") (r "^3.0.0") (d #t) (k 0)))) (h "05ch6xmkrfccdf30ic7w724hbhi05fx7zwr4qwblp9fn8lhq6jcc")))

(define-public crate-streamson-bin-4.0.0 (c (n "streamson-bin") (v "4.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "streamson-lib") (r "^4.0.0") (d #t) (k 0)))) (h "0gj901gz30r4whqkpgfnjwl5fd00ldp3a0kbsjpm3433x4x6v6j3")))

(define-public crate-streamson-bin-4.1.0 (c (n "streamson-bin") (v "4.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "streamson-lib") (r "^4.1.0") (d #t) (k 0)))) (h "0fgrvgdb7603hjz6644ip8954swpbhs26cya8k141lihaznxnv7k")))

(define-public crate-streamson-bin-5.0.0 (c (n "streamson-bin") (v "5.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "streamson-lib") (r "^5.0.0") (d #t) (k 0)))) (h "1q87r9dbck0zgmigc1s5gdjzbm04f30mr7cvh9hiivjzfj5v6b16")))

(define-public crate-streamson-bin-5.0.1 (c (n "streamson-bin") (v "5.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "streamson-lib") (r "^5.0.1") (d #t) (k 0)))) (h "1q3pjmrgl08x3cril0mx93jg85g3m52fqwcxaaawxh0zggh76bpa")))

(define-public crate-streamson-bin-5.0.2 (c (n "streamson-bin") (v "5.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "streamson-lib") (r "^5.0.2") (d #t) (k 0)))) (h "0ilj04z4j5h7flxjz3b0l9dl06sj6hlzcrlpp0dg7is8ff7pfm6q")))

(define-public crate-streamson-bin-6.0.0 (c (n "streamson-bin") (v "6.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "streamson-lib") (r "^6.0.0") (d #t) (k 0)))) (h "07crmzj9qqgi8k4vn57278mhh2y1rk4xjfas9yg8qvww8gvkxx19")))

(define-public crate-streamson-bin-6.1.0 (c (n "streamson-bin") (v "6.1.0") (d (list (d (n "assert_cmd") (r "~1.0.1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "streamson-lib") (r "^6.1.0") (d #t) (k 0)))) (h "1sfzwq05bx8q3sbqvbl1z8dyix0w4km5djnq5r4hrq583r1nqwsj") (f (quote (("test" "assert_cmd") ("default"))))))

(define-public crate-streamson-bin-6.2.0 (c (n "streamson-bin") (v "6.2.0") (d (list (d (n "assert_cmd") (r "~1.0.1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "streamson-lib") (r "^6.2.0") (d #t) (k 0)))) (h "1570avllsykpxnh86sg85frl38sp8x4m0c13z7100ki4qyyjr49m") (f (quote (("test" "assert_cmd") ("default"))))))

(define-public crate-streamson-bin-6.3.0 (c (n "streamson-bin") (v "6.3.0") (d (list (d (n "assert_cmd") (r "~1.0.1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "streamson-extra-matchers") (r "^6.3.0") (d #t) (k 0)) (d (n "streamson-lib") (r "^6.3.0") (d #t) (k 0)))) (h "14lxrd5z53gf9psa0q7iqg6vfyhp3xfxscxl77zlz4prxz0bynbq") (f (quote (("test" "assert_cmd") ("default"))))))

(define-public crate-streamson-bin-6.3.1 (c (n "streamson-bin") (v "6.3.1") (d (list (d (n "assert_cmd") (r "~1.0.1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "streamson-extra-matchers") (r "^6.3.1") (d #t) (k 0)) (d (n "streamson-lib") (r "^6.3.1") (d #t) (k 0)))) (h "1haf8b63x6qnbd6fv0nhm26700x5z0j314mnvsggwpklzq05vdpg") (f (quote (("test" "assert_cmd") ("default"))))))

(define-public crate-streamson-bin-7.0.0 (c (n "streamson-bin") (v "7.0.0") (d (list (d (n "assert_cmd") (r "~1.0.1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "streamson-lib") (r "^7.0.0") (d #t) (k 0)))) (h "011kfifiz8c1mzaysnwr5fdnvvz309gm9livz2s1lhipkf5gj8rd") (f (quote (("test" "assert_cmd") ("default"))))))

(define-public crate-streamson-bin-7.0.1 (c (n "streamson-bin") (v "7.0.1") (d (list (d (n "assert_cmd") (r "~1.0.1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "predicates") (r "~1.0.7") (o #t) (d #t) (k 0)) (d (n "streamson-lib") (r "^7.0.1") (d #t) (k 0)))) (h "12ww1x2a4ah6byb0gmjbx6g34fjls12lpxvbaqad6qaidsv659di") (f (quote (("test" "assert_cmd" "predicates") ("default"))))))

(define-public crate-streamson-bin-7.1.0 (c (n "streamson-bin") (v "7.1.0") (d (list (d (n "assert_cmd") (r "~1.0.1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 1)) (d (n "man") (r "~0.3.0") (o #t) (d #t) (k 0)) (d (n "man") (r "~0.3.0") (o #t) (d #t) (k 1)) (d (n "predicates") (r "~1.0.7") (o #t) (d #t) (k 0)) (d (n "streamson-lib") (r "^7.1.0") (d #t) (k 0)))) (h "1fc6nq327l1mrckmk2h8gbhj2kwp5vd30n1psg7dc0pidlbwqz6n") (f (quote (("test" "assert_cmd" "predicates") ("manpage" "man") ("default"))))))

