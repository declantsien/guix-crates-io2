(define-module (crates-io st re streamlet) #:use-module (crates-io))

(define-public crate-streamlet-0.1.0 (c (n "streamlet") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "gstreamer") (r "^0.16") (d #t) (k 0)) (d (n "gstreamer-player") (r "^0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "tui") (r "^0.12") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0y24p2qfwf32mincw12xrg36qd1m39i602m3d4xn09m3zh0z330b")))

