(define-module (crates-io st re stream-more) #:use-module (crates-io))

(define-public crate-stream-more-0.1.0 (c (n "stream-more") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 2)) (d (n "binary-heap-plus") (r "^0.5.0") (d #t) (k 0)) (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "1l369kjzysyyxvgn7vzjnxq28mi73xy7cl4q9kyrmr0hw4rq2330")))

(define-public crate-stream-more-0.1.1 (c (n "stream-more") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 2)) (d (n "binary-heap-plus") (r "^0.5.0") (d #t) (k 0)) (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "02rldm7f8jkphwm1g3bfdagjrvqq79ixv3993dzgpw054h6bd3ac")))

(define-public crate-stream-more-0.1.3 (c (n "stream-more") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 2)) (d (n "binary-heap-plus") (r "^0.5.0") (d #t) (k 0)) (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "0n7ckf9mvwda6a7xzhnx62dj2wsnxdwgrmip89rxpq5amf03yr91")))

