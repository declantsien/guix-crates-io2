(define-module (crates-io st re streams-orderednogaps) #:use-module (crates-io))

(define-public crate-streams-orderednogaps-0.1.0 (c (n "streams-orderednogaps") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.0-pre.1") (d #t) (k 2)))) (h "0siz63mzvs8lpyli9rd9ahmcqf8bcszv82q65a85l45bf2a6zzbv")))

(define-public crate-streams-orderednogaps-0.1.1 (c (n "streams-orderednogaps") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.0-pre.1") (d #t) (k 2)))) (h "1rncsad9ngsajjswmpy8wcff211caicpwzbgznf0hgdpakg5dvvg")))

