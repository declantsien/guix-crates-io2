(define-module (crates-io st re streaming-json-completer) #:use-module (crates-io))

(define-public crate-streaming-json-completer-0.1.0 (c (n "streaming-json-completer") (v "0.1.0") (d (list (d (n "arbitrary-json") (r "^0.1.1") (d #t) (k 2)) (d (n "combine") (r "^4.6.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "13vyz34gil87js9w7vca1gmighbirb5lbm0sdmn7rjp973yvvxwm") (f (quote (("std") ("default" "std"))))))

(define-public crate-streaming-json-completer-0.2.0-dev (c (n "streaming-json-completer") (v "0.2.0-dev") (d (list (d (n "arbitrary-json") (r "^0.1.1") (d #t) (k 2)) (d (n "combine") (r "^4.6.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0hr8406zvpwvan50zvaxiq46gkal86827qfc6q75hgcjfq4zzscq") (f (quote (("std") ("default" "std"))))))

