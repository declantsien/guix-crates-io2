(define-module (crates-io st re streamcraft) #:use-module (crates-io))

(define-public crate-streamcraft-0.0.1 (c (n "streamcraft") (v "0.0.1") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)))) (h "1nw6fpsjh2c2mhc9gm8jcimg5lxj7xbzj2yhrwad6h2bs67j3f68") (f (quote (("elements-text") ("elements-io") ("elements-all" "all-elements-text" "all-elements-io") ("element-texttestsrc" "elements-text") ("element-stdoutlog" "elements-text") ("element-filesrc" "elements-io") ("default" "elements-all") ("all-elements-text" "element-stdoutlog" "element-texttestsrc") ("all-elements-io" "element-filesrc"))))))

