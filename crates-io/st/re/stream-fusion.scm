(define-module (crates-io st re stream-fusion) #:use-module (crates-io))

(define-public crate-stream-fusion-0.1.0 (c (n "stream-fusion") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports" "async_tokio" "async"))) (d #t) (k 2)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0jbwq3303fc0zikhbwfbpgdvn5yx4q1fslv6zjrzpvifhakkkzq2")))

