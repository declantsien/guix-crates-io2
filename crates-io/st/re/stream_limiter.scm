(define-module (crates-io st re stream_limiter) #:use-module (crates-io))

(define-public crate-stream_limiter-1.0.0 (c (n "stream_limiter") (v "1.0.0") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "05h0c1x2cv6hq31lqqx0pmv6k9fnbkjz900wphi4bqb8mq1a1yb8")))

(define-public crate-stream_limiter-1.1.0 (c (n "stream_limiter") (v "1.1.0") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1jhcwh0180lgvhg4z6mhcwkwqjy7ywpfzbfb5xxqq32jxlw2q67j")))

(define-public crate-stream_limiter-1.2.0 (c (n "stream_limiter") (v "1.2.0") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0mjrfmgsd38hykwvaq4di9isgiqc7m287zbipzmsxssw48im7hf8")))

(define-public crate-stream_limiter-2.0.0 (c (n "stream_limiter") (v "2.0.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 2)))) (h "1g04ksbfj0lxml3ddk1y3mk7s7wfdqqjsglsvjl00iz4xr4mvf4j")))

(define-public crate-stream_limiter-3.0.0 (c (n "stream_limiter") (v "3.0.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 2)))) (h "00pzf7r1kvv64c4si5jygw1pgikj11kkw1xqpjwbj9gr50pmf8w9") (f (quote (("heavy_testing"))))))

(define-public crate-stream_limiter-3.0.1 (c (n "stream_limiter") (v "3.0.1") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 2)))) (h "1avdhkcripxch11k85ssa1ykp8akrdrs7d1sd8ybmrb94naqv539") (f (quote (("heavy_testing"))))))

(define-public crate-stream_limiter-3.1.1 (c (n "stream_limiter") (v "3.1.1") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 2)))) (h "1jcmr3vd653zza5d186vzb1mjpxn5yshzpas2m89cf3k1b21almg") (f (quote (("heavy_testing"))))))

(define-public crate-stream_limiter-3.2.0 (c (n "stream_limiter") (v "3.2.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 2)))) (h "1kwhry6mc0a041fdfqsfbgpf186g26mlld5iprdif3akiwldpb49") (f (quote (("heavy_testing"))))))

