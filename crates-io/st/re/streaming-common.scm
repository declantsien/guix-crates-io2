(define-module (crates-io st re streaming-common) #:use-module (crates-io))

(define-public crate-streaming-common-0.1.0 (c (n "streaming-common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1096mafama0r603xmxr3fhqphnhjn8527w8lxqdagigszmxadw75") (y #t)))

(define-public crate-streaming-common-0.6.0 (c (n "streaming-common") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "03zw2nxsc7lm7cbhx9ds19x1rs2pgdls49zmn88sfp7z79d3pdd1") (y #t)))

