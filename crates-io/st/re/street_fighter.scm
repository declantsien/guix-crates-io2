(define-module (crates-io st re street_fighter) #:use-module (crates-io))

(define-public crate-street_fighter-0.1.0 (c (n "street_fighter") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_audio") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0rby3r3g6jwgprj1m3jhf8b57lckk3mwcpz0n8q3spaz0rhj5ssy")))

