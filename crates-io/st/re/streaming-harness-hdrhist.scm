(define-module (crates-io st re streaming-harness-hdrhist) #:use-module (crates-io))

(define-public crate-streaming-harness-hdrhist-0.2.0 (c (n "streaming-harness-hdrhist") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "textplots") (r "^0.2") (d #t) (k 2)))) (h "014grcfxxd4ch8fgmkl0qai0w8ccj3h6cnxcg4j6pxqawzmc011a")))

(define-public crate-streaming-harness-hdrhist-0.2.1 (c (n "streaming-harness-hdrhist") (v "0.2.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "textplots") (r "^0.2") (d #t) (k 2)))) (h "1lhi3v6v5579spcw24lwygvg6vap6jf5mlxbkx2nz8zznbjmkcjs")))

(define-public crate-streaming-harness-hdrhist-0.3.0 (c (n "streaming-harness-hdrhist") (v "0.3.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "textplots") (r "^0.2") (d #t) (k 2)))) (h "1ma3za8sq3v95j9jakbx370ar2kc935ylnx2mysz3div3s8glmsc")))

(define-public crate-streaming-harness-hdrhist-0.4.0 (c (n "streaming-harness-hdrhist") (v "0.4.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "textplots") (r "^0.2") (d #t) (k 2)))) (h "0rkvd82fx6570j6f0x3cljc1inzi067y9hvka76c0lpsmyxwqlli")))

