(define-module (crates-io st re street_index) #:use-module (crates-io))

(define-public crate-street_index-0.1.0 (c (n "street_index") (v "0.1.0") (h "0bzn3nvyaxpqiz5wsicsn78d4f2b07yhgbrwhmdp199813zp96nd")))

(define-public crate-street_index-0.1.1 (c (n "street_index") (v "0.1.1") (h "175agcmd0hdla8dp7n0flp6q2ci90k0pmf3692kjz8q10bph9n7l")))

