(define-module (crates-io st re streamcore_message_client) #:use-module (crates-io))

(define-public crate-streamcore_message_client-0.1.1 (c (n "streamcore_message_client") (v "0.1.1") (d (list (d (n "amqprs") (r "^1.4.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("parking_lot"))) (d #t) (k 0)))) (h "1zzwbn9d1ggh6x6jwj4dpbw831r0bqk1jfbn8wax8z9b097hhr16")))

(define-public crate-streamcore_message_client-0.1.2 (c (n "streamcore_message_client") (v "0.1.2") (d (list (d (n "amqprs") (r "^1.4.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("parking_lot"))) (d #t) (k 0)))) (h "04a2af4gwgy2l3p5xnf9nkxfnhbw0flwk2dsakvhlzzyah33lpqw")))

(define-public crate-streamcore_message_client-0.1.3 (c (n "streamcore_message_client") (v "0.1.3") (d (list (d (n "amqprs") (r "^1.4.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("parking_lot"))) (d #t) (k 0)))) (h "0agy8avs7s4w9cwvam4l2vfm7hrmr7mrnlh4w8a1z028rhqgbz5i")))

