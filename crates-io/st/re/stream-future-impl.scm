(define-module (crates-io st re stream-future-impl) #:use-module (crates-io))

(define-public crate-stream-future-impl-0.1.0 (c (n "stream-future-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0al139m4mvkyy0sxch6craw2ys9nrvdmfbskb4z9nsv2fi6lj057")))

(define-public crate-stream-future-impl-0.1.1 (c (n "stream-future-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1vcw9sdw2c41cwd0vvn3rgbf0zqgi00a7dd00v4xqm2mz8rjd7jz")))

(define-public crate-stream-future-impl-0.2.0 (c (n "stream-future-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "07hcq5gw5biksaq69a8w5r9kgd6bm9jm3rq77pf74bsyc9y3fq8f")))

(define-public crate-stream-future-impl-0.3.0 (c (n "stream-future-impl") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1jbb0pslz5xfzyfpvllfi8s5bi34bfwj55fyzlxcg5mkgil2m4sh")))

(define-public crate-stream-future-impl-0.3.1 (c (n "stream-future-impl") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1dn96rzxf9iin8fc0s0n0rbmk2ad9qqhpkl52jzayf72qc6kk2yj")))

(define-public crate-stream-future-impl-0.4.0 (c (n "stream-future-impl") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "101av22ggblzrrgyh2w65linvlg8sj915aslkpwy2glc040370vh")))

(define-public crate-stream-future-impl-0.5.0 (c (n "stream-future-impl") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1yygvkwh3c6jnyk56a19g7x7z16p3ahlfmr6y2apkcamcd70gv4r")))

