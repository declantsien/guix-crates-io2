(define-module (crates-io st re streaming-decompression) #:use-module (crates-io))

(define-public crate-streaming-decompression-0.1.0 (c (n "streaming-decompression") (v "0.1.0") (d (list (d (n "fallible-streaming-iterator") (r "^0.1") (d #t) (k 0)))) (h "14ncaqb723wz5xx2ws7g8d3qx9hlhrx95wllf152qx6wsnn8gilv")))

(define-public crate-streaming-decompression-0.1.1 (c (n "streaming-decompression") (v "0.1.1") (d (list (d (n "fallible-streaming-iterator") (r "^0.1") (d #t) (k 0)))) (h "0mfvaajxwwd0m92yrasn728ns8w4k2nnfkdpdq0wbkklrgvk2cam")))

(define-public crate-streaming-decompression-0.1.2 (c (n "streaming-decompression") (v "0.1.2") (d (list (d (n "fallible-streaming-iterator") (r "^0.1") (d #t) (k 0)))) (h "1wscqj3s30qknda778wf7z99mknk65p0h9hhs658l4pvkfqw6v5z")))

