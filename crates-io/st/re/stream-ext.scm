(define-module (crates-io st re stream-ext) #:use-module (crates-io))

(define-public crate-stream-ext-0.1.0 (c (n "stream-ext") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "governor") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "leaky-bucket") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wrjlqsf886xkvjyvpdzkkk543sfcg97l36jxfny3qsy9r288k77") (f (quote (("default")))) (r "1.38")))

(define-public crate-stream-ext-0.2.0 (c (n "stream-ext") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "governor") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "leaky-bucket") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01s0ya01kp7zfzhlp6rdqyq8czkl13apm7rb31x3nhsc5l2s6386") (f (quote (("default")))) (y #t) (r "1.38")))

(define-public crate-stream-ext-0.2.1 (c (n "stream-ext") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "governor") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "leaky-bucket") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16hvjbqwpzy9piz68z5d7m729l1czqzjlqysmpdjy2rzrfhw6ask") (f (quote (("default")))) (r "1.38")))

(define-public crate-stream-ext-0.2.2 (c (n "stream-ext") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "governor") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "leaky-bucket") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bz3qwqbyqdyrc15abw3cs5c3ma27cg3zy6plxcywfi1dw3n2ajg") (f (quote (("default")))) (r "1.38")))

(define-public crate-stream-ext-0.2.3 (c (n "stream-ext") (v "0.2.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "governor") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "leaky-bucket") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jyqgzxhs58qd387d3h2rj7ljvfkl1hlr4nhphsfrz1kbxrs3awl") (f (quote (("default")))) (r "1.47")))

(define-public crate-stream-ext-0.2.4 (c (n "stream-ext") (v "0.2.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "governor") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "leaky-bucket") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17njbsfpl0z44141rj3vksd0hhd9a4b2kcjwg842g7pdi4kpkn6g") (f (quote (("default")))) (r "1.47")))

