(define-module (crates-io st re streem) #:use-module (crates-io))

(define-public crate-streem-0.1.0 (c (n "streem") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "18j0r0h5zdwxsgiqwy2bkyx6didvhcn20iqrk9xwh0n4flybzg2n")))

(define-public crate-streem-0.1.1 (c (n "streem") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "054i37wc8mn4n81dh5vpi3p4223nmd2290r15nqpqxlhmsjrc4v4")))

(define-public crate-streem-0.2.0 (c (n "streem") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "0d5blqs5l0ja2f9cp1vc7ib5gkcma4h6cpfpgnrhbzmhhj0hr2vc")))

