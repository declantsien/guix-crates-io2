(define-module (crates-io st re stream-extractor) #:use-module (crates-io))

(define-public crate-stream-extractor-0.2.0 (c (n "stream-extractor") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pcap-file") (r "^2.0.0") (d #t) (k 0)) (d (n "pnet") (r "^0.33.0") (d #t) (k 0)))) (h "1k738y3xp16gkhil6kv6nrs514f666v16kpmnqzdm4pdahacwl36")))

(define-public crate-stream-extractor-0.4.0 (c (n "stream-extractor") (v "0.4.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pcap-file") (r "^2.0.0") (d #t) (k 0)) (d (n "pnet") (r "^0.33.0") (d #t) (k 0)))) (h "0bim9g6725qgx899i7sclksj0as881kvfhl5b0nh7pkvsj1lv2k5")))

