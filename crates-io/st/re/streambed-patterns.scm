(define-module (crates-io st re streambed-patterns) #:use-module (crates-io))

(define-public crate-streambed-patterns-0.9.1 (c (n "streambed-patterns") (v "0.9.1") (d (list (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("sync" "macros" "io-util" "rt" "time"))) (d #t) (k 0)))) (h "1fd7ji130gzxb83bl2jzvx4i1345j3kp3xa4xd3i6jxwq2hp90qm") (r "1.70.0")))

