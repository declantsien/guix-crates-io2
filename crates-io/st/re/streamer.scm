(define-module (crates-io st re streamer) #:use-module (crates-io))

(define-public crate-streamer-0.1.0 (c (n "streamer") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "hyper") (r "^0.14.19") (f (quote ("stream"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "16s8jirp0cxasfd0a1wdhc0szj0a84iczq9lwn72d89m6s69v3xc") (f (quote (("default")))) (s 2) (e (quote (("hyper" "default" "dep:hyper"))))))

(define-public crate-streamer-0.1.1 (c (n "streamer") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "hyper") (r "^0.14.19") (f (quote ("stream"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0f881g9awwgzpqsnxb1gn3kzmyz67n4bzxnli2w1zjjgli0jvrix") (f (quote (("default")))) (s 2) (e (quote (("hyper" "default" "dep:hyper"))))))

(define-public crate-streamer-0.1.2 (c (n "streamer") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "hyper") (r "^0.14.19") (f (quote ("stream"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "1ihaax3v9d388zkfafjaxm4515zv1rfph66w2d2sr6ggfnl5618x") (f (quote (("default")))) (s 2) (e (quote (("hyper" "default" "dep:hyper"))))))

