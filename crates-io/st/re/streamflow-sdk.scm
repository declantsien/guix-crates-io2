(define-module (crates-io st re streamflow-sdk) #:use-module (crates-io))

(define-public crate-streamflow-sdk-0.4.0 (c (n "streamflow-sdk") (v "0.4.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1wfzz0wz46yq15apq161384msxdm7pfiqnp42j9la3w6w1wkdbsr") (y #t)))

(define-public crate-streamflow-sdk-0.5.0 (c (n "streamflow-sdk") (v "0.5.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)))) (h "0inai9q2bmgs178a4zhyqm74rmymrd9j735ljv2jnzyk4hbl6nl7") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.5.1 (c (n "streamflow-sdk") (v "0.5.1") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)))) (h "0dg3xss971r0i8dqynnvqcxiwsmy6l8c2947l6b0qr9gvbnx315l") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.5.2 (c (n "streamflow-sdk") (v "0.5.2") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)))) (h "1z2p51v8q8v5xqvpjzgy83n081sqnp6vkn0bgzv2rzybbmbzjwjk") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.5.3-alpha (c (n "streamflow-sdk") (v "0.5.3-alpha") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)))) (h "0ipqxxias1lci2ls2xz9ir9z3w4hvknqmml0wfgrpgm95bb58pf1") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.5.3 (c (n "streamflow-sdk") (v "0.5.3") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)))) (h "1cgvgyd9d7m707w4h6p1cnnamn46pz4qwg8fjsv5r97slpdpvqhc") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.0-alpha.1 (c (n "streamflow-sdk") (v "0.6.0-alpha.1") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.2") (d #t) (k 0)))) (h "15nz3wpvlvnjkwy4c51mx4wf7l9v27ls08y3ys6rdzx5cka4ss2k") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.0-alpha.2 (c (n "streamflow-sdk") (v "0.6.0-alpha.2") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.2") (d #t) (k 0)))) (h "0pcq75my4bnk9aslrzlhym8gbspg13k8xzh1jaz63prlapybnpk1") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.0-alpha.3 (c (n "streamflow-sdk") (v "0.6.0-alpha.3") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.2") (d #t) (k 0)))) (h "03ymp8mrjrz9zwfz18g4r522kb2s1klv3q3jcsas170j95s4mn3j") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.0-alpha.4 (c (n "streamflow-sdk") (v "0.6.0-alpha.4") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.2") (d #t) (k 0)))) (h "06siyd25hdzmnpfq2zvm3dlc0cxxjp6ibk0703cypjrhmhyfcz1x") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.0-alpha.5 (c (n "streamflow-sdk") (v "0.6.0-alpha.5") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.2") (d #t) (k 0)))) (h "1w8lavwf176s0ssksgkc6f2r0vyfjwbi4p5b9q48y9rw5xvqxxbx") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.0-alpha.6 (c (n "streamflow-sdk") (v "0.6.0-alpha.6") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.2") (d #t) (k 0)))) (h "1078gzzc4q627a5zgbg021zqkzp1c7s2fqzx4mc6amjcgpnxm82k") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.0-alpha.7 (c (n "streamflow-sdk") (v "0.6.0-alpha.7") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.2") (d #t) (k 0)))) (h "1b2lk8qcidr80il4k1pn1dhzhwgqd2jimckhs7r82ag2gpw0mjqm") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.0-alpha.8 (c (n "streamflow-sdk") (v "0.6.0-alpha.8") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.2") (d #t) (k 0)))) (h "0af8rphsav5mazw1hwihxn2kwrrq3yw11i1yjfy6wirnz7nwmdl6") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.0-alpha.9 (c (n "streamflow-sdk") (v "0.6.0-alpha.9") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.2") (d #t) (k 0)))) (h "12nr41w7rx2q3djkvdkp4kbsnskxxi128fddb378rbz2q3f0ihxj") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.0-alpha.10 (c (n "streamflow-sdk") (v "0.6.0-alpha.10") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.2") (d #t) (k 0)))) (h "0vxzhdawk3aapfff7lrq9gfqpgbs89il1jk0lsppz1hm3f6rw56w") (f (quote (("no-entrypoint") ("mainnet") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.0 (c (n "streamflow-sdk") (v "0.6.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "borsh") (r "^0.10.2") (d #t) (k 0)))) (h "0sd7i5nz6m4zq4y2q9f5mfbva078672b094x9mhd71f0j1ryxih3") (f (quote (("no-entrypoint") ("mainnet") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.1-alpha.1 (c (n "streamflow-sdk") (v "0.6.1-alpha.1") (d (list (d (n "anchor-lang") (r ">=0.24.2, <1") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.24.2, <1") (d #t) (k 0)))) (h "0zxlgxprqcmnk1zl8hrjvarcj38irf3jm2af095qn4l5p0b6l8yh") (f (quote (("no-entrypoint") ("mainnet") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.1 (c (n "streamflow-sdk") (v "0.6.1") (d (list (d (n "anchor-lang") (r ">=0.24.2, <1") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.24.2, <1") (d #t) (k 0)))) (h "0jijp9ycqgpqk40m5rg8slqahgaf3xsnxkg2vm4dj3mv4p14r8zh") (f (quote (("no-entrypoint") ("mainnet") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.2 (c (n "streamflow-sdk") (v "0.6.2") (d (list (d (n "anchor-lang") (r ">=0.24.2, <1") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.24.2, <1") (d #t) (k 0)))) (h "1ch03bymxbgs4iyyys5fm9ia9i23hay053if4aab6zvpllgjfx39") (f (quote (("no-entrypoint") ("mainnet") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.3 (c (n "streamflow-sdk") (v "0.6.3") (d (list (d (n "anchor-lang") (r ">=0.24.2, <1") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.24.2, <1") (d #t) (k 0)))) (h "1x479pdkpdrbdr5idj7dgs1acpdr1064j7waqg4kwq0big4a9qmy") (f (quote (("no-entrypoint") ("mainnet") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.4 (c (n "streamflow-sdk") (v "0.6.4") (d (list (d (n "anchor-lang") (r ">=0.24.2, <1") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.24.2, <1") (d #t) (k 0)))) (h "0znx2x4h3ccnclz7fmpyr8fmkmp6v8agrr63ylal77nd6nfzzjb9") (f (quote (("no-entrypoint") ("mainnet") ("cpi" "no-entrypoint"))))))

(define-public crate-streamflow-sdk-0.6.5 (c (n "streamflow-sdk") (v "0.6.5") (d (list (d (n "anchor-lang") (r ">=0.24.2, <1") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.24.2, <1") (d #t) (k 0)))) (h "1aawd4m15241m15zldnych3s8m7z3iq1x3j7f2i0y6c1d1i6m3zn") (f (quote (("no-entrypoint") ("mainnet") ("cpi" "no-entrypoint"))))))

