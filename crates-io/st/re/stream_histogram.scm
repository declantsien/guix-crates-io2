(define-module (crates-io st re stream_histogram) #:use-module (crates-io))

(define-public crate-stream_histogram-0.1.0 (c (n "stream_histogram") (v "0.1.0") (d (list (d (n "linked-list") (r "^0.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0k3hzpyp2rqf5w102rf7j4mhi536dx9gqsibbql9cpbbqr78a3h1")))

(define-public crate-stream_histogram-0.1.1 (c (n "stream_histogram") (v "0.1.1") (d (list (d (n "linked-list") (r "^0.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "063fc6qkd8ld1j11mxn5066q4645lbijicyyramdjmc67wxpylj2")))

