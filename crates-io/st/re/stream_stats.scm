(define-module (crates-io st re stream_stats) #:use-module (crates-io))

(define-public crate-stream_stats-0.1.0 (c (n "stream_stats") (v "0.1.0") (h "1d4qxha903bahihdww35y3mad108bwa5mz2i1aqdc99q54m231yy")))

(define-public crate-stream_stats-0.1.1 (c (n "stream_stats") (v "0.1.1") (h "14b9qisj16yf69qpqyk10b642fgnkdi8bmphyb4qply08s2a54nl")))

