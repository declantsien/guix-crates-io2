(define-module (crates-io st re streaming-harness) #:use-module (crates-io))

(define-public crate-streaming-harness-0.1.0 (c (n "streaming-harness") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "streaming-harness-hdrhist") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "timely") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0v94ssg57sdd5b5hihz9ag3k693rnysl0lzf998d089g8kq5jqcf") (f (quote (("timely-support" "timely") ("hdrhist-support" "streaming-harness-hdrhist") ("default"))))))

(define-public crate-streaming-harness-0.1.1 (c (n "streaming-harness") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "streaming-harness-hdrhist") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "timely") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0m3a1fx8qjx7wnmi3rrp6vcpx1c06nxff972pgnlj56kxj6qpba7") (f (quote (("timely-support" "timely") ("hdrhist-support" "streaming-harness-hdrhist") ("default"))))))

(define-public crate-streaming-harness-0.2.0 (c (n "streaming-harness") (v "0.2.0") (d (list (d (n "hdrhist") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "timely") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0jghsmym14xdbqm8ik2flfdi82ihfmrk0pbmxlap5my7651v7gvp") (f (quote (("timely-support" "timely") ("hdrhist-support" "hdrhist") ("default"))))))

