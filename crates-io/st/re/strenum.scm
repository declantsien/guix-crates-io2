(define-module (crates-io st re strenum) #:use-module (crates-io))

(define-public crate-strenum-0.0.1 (c (n "strenum") (v "0.0.1") (h "05mdg8413429mcavlsrh2w4yzi155117n52k4jxmrn2j4921k54n")))

(define-public crate-strenum-0.0.2 (c (n "strenum") (v "0.0.2") (h "115x0d613n2ignd0w16i78mkg3k4a64hsx053ks51blb0sxlz3w6")))

(define-public crate-strenum-0.0.3 (c (n "strenum") (v "0.0.3") (h "11zsw189isgmzjr0f5j2y8nys0hizjn7frsbhnhfy3kfc71qxm1y")))

(define-public crate-strenum-0.0.4 (c (n "strenum") (v "0.0.4") (d (list (d (n "enum_primitive") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1pma29hjajhn3ri1zvkdiwzsmw0vr11sc6361vkm8rsm3a2marfi")))

(define-public crate-strenum-0.0.5 (c (n "strenum") (v "0.0.5") (d (list (d (n "enum_primitive") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1yxsfhhaglv2mifir3a4cv7xbnxl9vz5q5p9jb07gzbqdh3a9lwp")))

(define-public crate-strenum-0.0.6 (c (n "strenum") (v "0.0.6") (d (list (d (n "enum_primitive") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1dmhhwnibnnrknjljhvsxw2r2drx5xvszbpmfvxmf82z6v4vcqrs")))

