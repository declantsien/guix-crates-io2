(define-module (crates-io st re streamvbyte) #:use-module (crates-io))

(define-public crate-streamvbyte-0.1.0 (c (n "streamvbyte") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2") (d #t) (k 2)) (d (n "streamvbyte-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bl37pjdf24ja86hnyh59f0452fcxxvm9zkpxazby0r2f249vg21")))

(define-public crate-streamvbyte-0.1.1 (c (n "streamvbyte") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2") (d #t) (k 2)) (d (n "streamvbyte-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0q3b1j2nsa56av1h1mvqm4pjb4r7jnpv8brgq0j1d1x39v3jd8xk")))

