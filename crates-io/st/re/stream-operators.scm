(define-module (crates-io st re stream-operators) #:use-module (crates-io))

(define-public crate-stream-operators-0.1.0 (c (n "stream-operators") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.11") (d #t) (k 0)))) (h "08cxjs0k4q8nxpb7gd25b4vjdhsz7a1qpnqbf8z87av40ivakajy")))

(define-public crate-stream-operators-0.1.1 (c (n "stream-operators") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.11") (d #t) (k 0)))) (h "06ywyw0zwkky0dbiw97dvrcyjjmxchlawj89i9wbyi6sv9y4sglh")))

