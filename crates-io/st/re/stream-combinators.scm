(define-module (crates-io st re stream-combinators) #:use-module (crates-io))

(define-public crate-stream-combinators-0.1.0 (c (n "stream-combinators") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-stdin") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 2)))) (h "00i7c9jf8jrbi0rk6dbiqch19mxxqvd42n62vmrrlvbq81isxv96")))

