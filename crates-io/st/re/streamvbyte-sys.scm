(define-module (crates-io st re streamvbyte-sys) #:use-module (crates-io))

(define-public crate-streamvbyte-sys-0.1.0 (c (n "streamvbyte-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2") (d #t) (k 2)))) (h "1hv2v64hlyiwwi7vckynh41m9np3ln1l8s3n9ca8chhdyzrmmx9i")))

