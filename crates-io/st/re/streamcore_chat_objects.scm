(define-module (crates-io st re streamcore_chat_objects) #:use-module (crates-io))

(define-public crate-streamcore_chat_objects-0.1.2 (c (n "streamcore_chat_objects") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1awsnz4fcgs7y5n91gdzx6bks9nmvvgh7kmp2g9rzmv7c7a1il06")))

(define-public crate-streamcore_chat_objects-0.1.3 (c (n "streamcore_chat_objects") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yvbiy6b7bgyaifcf5gfq8w4yfhrkr221xmljd5p90ryd7kfw06l")))

(define-public crate-streamcore_chat_objects-0.1.4 (c (n "streamcore_chat_objects") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "05j64swfm8kzkg5djs578rndiv2q51ilk4v2ym4p2719zrlwkjgb")))

