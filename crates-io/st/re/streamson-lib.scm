(define-module (crates-io st re streamson-lib) #:use-module (crates-io))

(define-public crate-streamson-lib-0.1.0 (c (n "streamson-lib") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0x46l2fnnj3h9cgymkyy80fhvsgyz14s88phi7mrc739f14p7rw8")))

(define-public crate-streamson-lib-0.2.0 (c (n "streamson-lib") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "093a0bdbcd28xf7b5s9c4ir8lij6am128rqfmqxpfqdw8ia6g25z")))

(define-public crate-streamson-lib-1.0.0 (c (n "streamson-lib") (v "1.0.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1f3rr9zfs1lxj63msnlk9jmkqij7wfkfrsrdf9ycm4mqsibli4jd")))

(define-public crate-streamson-lib-1.0.1 (c (n "streamson-lib") (v "1.0.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1d3f9km07mw08ss86ixbzz2xg0bjgv37099nd334hm19r8rzfy5v")))

(define-public crate-streamson-lib-1.0.2 (c (n "streamson-lib") (v "1.0.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0cd7y8302xvgnwmwkz78jdgyqim0hypvailbwy7fcslxh4ilndzl")))

(define-public crate-streamson-lib-2.0.0 (c (n "streamson-lib") (v "2.0.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0br4769278a46l5b6lhl87ai8rx8h91k0r3m1n3a4hlrldw9sbck")))

(define-public crate-streamson-lib-3.0.0 (c (n "streamson-lib") (v "3.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0gvqw8zgii1hpijb6zfmcdjkgrv07482hgfvrgvk0pk8mrh5r4cp")))

(define-public crate-streamson-lib-4.0.0 (c (n "streamson-lib") (v "4.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0rlgll1c9yhcx8wz1pyp8jj17p3v14ncdfa06gnnqzy35hqxfchs")))

(define-public crate-streamson-lib-4.1.0 (c (n "streamson-lib") (v "4.1.0") (d (list (d (n "criterion") (r "=0.3.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1gpx6v9k55hs8cpqz32zr0m6c2l2fs7infb3dk15pglx2dsnk2pb")))

(define-public crate-streamson-lib-5.0.0 (c (n "streamson-lib") (v "5.0.0") (d (list (d (n "criterion") (r "=0.3.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "00qfd62wgb3lz8g2iz8l3z73irw9sn3ma7qczyf6x0p42zazyjhf")))

(define-public crate-streamson-lib-5.0.1 (c (n "streamson-lib") (v "5.0.1") (d (list (d (n "criterion") (r "=0.3.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "02jfmq34miz6mma78dw3v7vhnai2pik00wjk5ydb52by40gfr8d6")))

(define-public crate-streamson-lib-5.0.2 (c (n "streamson-lib") (v "5.0.2") (d (list (d (n "criterion") (r "=0.3.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "029f43pjbm59dk236fd2w2nq7278ca2yyr4a0iy9s4xkir1lvk8v")))

(define-public crate-streamson-lib-6.0.0 (c (n "streamson-lib") (v "6.0.0") (d (list (d (n "criterion") (r "=0.3.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0wrkgyb5xskcbva0q1iv722bhn7r95mwldf174imdqzhp7295g5f")))

(define-public crate-streamson-lib-6.1.0 (c (n "streamson-lib") (v "6.1.0") (d (list (d (n "criterion") (r "=0.3.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1d2089shlh1n73xq0kc2q9b8mrrxq1dcnyai00jwc2rp1gnxpi8c")))

(define-public crate-streamson-lib-6.2.0 (c (n "streamson-lib") (v "6.2.0") (d (list (d (n "criterion") (r "~0.3.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "030xh8dk9plyqqzpfchw5cxn4mwvj0m5k93fl321ns12grlc1h6h")))

(define-public crate-streamson-lib-6.3.0 (c (n "streamson-lib") (v "6.3.0") (d (list (d (n "criterion") (r "~0.3.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "12bsyaq6i4p7g4wqabc04iaapj2k07jim041ns9campp7k99172n")))

(define-public crate-streamson-lib-6.3.1 (c (n "streamson-lib") (v "6.3.1") (d (list (d (n "criterion") (r "~0.3.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0hi17slkm6bg1dcaa0y70xxa517bmw250xyc5sdrsz7xiwpkp387")))

(define-public crate-streamson-lib-7.0.0 (c (n "streamson-lib") (v "7.0.0") (d (list (d (n "criterion") (r "~0.3.3") (d #t) (k 2)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstest") (r "~0.6.4") (d #t) (k 2)) (d (n "sedregex") (r "~0.2.4") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "01wb3gafibyq2qbmqh7ri7jjc6kxs6r3x119db4rxnz8f4nf1qx8") (f (quote (("with_regex" "regex" "sedregex") ("default" "with_regex"))))))

(define-public crate-streamson-lib-7.0.1 (c (n "streamson-lib") (v "7.0.1") (d (list (d (n "criterion") (r "~0.3.3") (d #t) (k 2)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstest") (r "~0.6.4") (d #t) (k 2)) (d (n "sedregex") (r "~0.2.4") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0ar8fgw44xf5bfdf1dq4y6kinx02wi25lzqpsyzwpgi4j068fzw1") (f (quote (("with_regex" "regex" "sedregex") ("default" "with_regex"))))))

(define-public crate-streamson-lib-7.1.0 (c (n "streamson-lib") (v "7.1.0") (d (list (d (n "criterion") (r "~0.3.3") (d #t) (k 2)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstest") (r "~0.6.4") (d #t) (k 2)) (d (n "sedregex") (r "~0.2.4") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0q4k1xbzh1scfi2zyxdqx4qxn22axvdysyjjqiq7nknblyckh9qv") (f (quote (("with_regex" "regex" "sedregex") ("default" "with_regex"))))))

