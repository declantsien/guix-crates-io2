(define-module (crates-io st re streamdal-protos) #:use-module (crates-io))

(define-public crate-streamdal-protos-0.0.115 (c (n "streamdal-protos") (v "0.0.115") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0jshv2m82rfpxlrfab12s279jxh3qgxzwbsp14lha2q074wrxkn6")))

(define-public crate-streamdal-protos-0.0.116 (c (n "streamdal-protos") (v "0.0.116") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0cij6720m2dvsnhirfkixx26cmryzn6rw041bmlqd7asdnzh6ppa")))

(define-public crate-streamdal-protos-0.0.117 (c (n "streamdal-protos") (v "0.0.117") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "01qfp12ba0dlqs1zwrwza5gmgakrr46di2ghf1z9pwww9jdgjg9p")))

(define-public crate-streamdal-protos-0.0.118 (c (n "streamdal-protos") (v "0.0.118") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1jnk6z87b4r2lam39hqgxnk7v5jfn6hrf86g21s8l67xhqgq0w5q")))

(define-public crate-streamdal-protos-0.0.119 (c (n "streamdal-protos") (v "0.0.119") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0ry45g1rvgpva5cb2kyqiqvypd3yp5gs1k3wl5z2xir2ymkm3mq3")))

(define-public crate-streamdal-protos-0.0.120 (c (n "streamdal-protos") (v "0.0.120") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1l3j3cmi9vz51vz7x4317sdw3m5lpaf9ils2a1hcshb3i151ydws")))

(define-public crate-streamdal-protos-0.0.121 (c (n "streamdal-protos") (v "0.0.121") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "058famiimgkv495j90hly4vpkrhp7cq3mgn4n8qml6nlrl2a50mq")))

(define-public crate-streamdal-protos-0.0.122 (c (n "streamdal-protos") (v "0.0.122") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0x3vmfp2f6mflzrdblv9gbilprpfnbhk8fzici7yb9jz7xwix47p")))

(define-public crate-streamdal-protos-0.0.123 (c (n "streamdal-protos") (v "0.0.123") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "18r6lakky6wfwn9a49727qpmndnxvn7bbkawzwckv88c5nc1vblq")))

(define-public crate-streamdal-protos-0.0.124 (c (n "streamdal-protos") (v "0.0.124") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "10bn8mhp2647ywhqggl38ymamk41avsnm9hmk6l0fnw2xhjj1raf")))

(define-public crate-streamdal-protos-0.0.125 (c (n "streamdal-protos") (v "0.0.125") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1axxc6nd299qrzc55v36k47m7xhnzb2hn995kwqjcngdd5gf9jvd")))

(define-public crate-streamdal-protos-0.0.2 (c (n "streamdal-protos") (v "0.0.2") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "028n077jxm91wqizpaigw5yq1cdgrx4730nxk1s19ld9z12zq1az")))

(define-public crate-streamdal-protos-0.0.126 (c (n "streamdal-protos") (v "0.0.126") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "02fcdm90zn5r40acygdhkdkxa9di1krq6nm0sk0lkzbvlzv83bcj")))

(define-public crate-streamdal-protos-0.0.127 (c (n "streamdal-protos") (v "0.0.127") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0sysn40c7vbxfdhg6l69llc284j73b489nxg43ny5mph17p20p0i")))

(define-public crate-streamdal-protos-0.0.128 (c (n "streamdal-protos") (v "0.0.128") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0xdjz8b83f4m63jnn2sm36z04jv0lb5w64l8qj2ai78xykhnjhga")))

(define-public crate-streamdal-protos-0.0.130 (c (n "streamdal-protos") (v "0.0.130") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "05c0fb5wid82ifgsk5bk2vcnw73cvwm49jc36z735gsvyhprad3z")))

(define-public crate-streamdal-protos-0.0.131 (c (n "streamdal-protos") (v "0.0.131") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "18ynbm38x1jmisildx6c44wcibcn331rhcnvhvgw1c5wvh078rkj")))

(define-public crate-streamdal-protos-0.0.132 (c (n "streamdal-protos") (v "0.0.132") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1dqqxy7xw4yqb90w8vjlxblz80zynf0ng1r6vhapv6vhdzbca05a")))

(define-public crate-streamdal-protos-0.0.136 (c (n "streamdal-protos") (v "0.0.136") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1avzqbj35k64mrdf4yjdxxdc45lmfmrdwl7vjgndlvmk0igglbfa")))

(define-public crate-streamdal-protos-0.0.137 (c (n "streamdal-protos") (v "0.0.137") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0y11fhhrghk34mikkqg5hdq9rkbfhisqrqj6ra3j3cqirph94dlm")))

(define-public crate-streamdal-protos-0.1.2 (c (n "streamdal-protos") (v "0.1.2") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0qj0l09il0a6bxps9xw9az4w0whxh6i8nqj041mrzvgrcggqrlr4")))

(define-public crate-streamdal-protos-0.1.3 (c (n "streamdal-protos") (v "0.1.3") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0qb0nfg6b9i9jx3n2wg4spksqlg1r1pcl9saypkklphkzymzqnar")))

(define-public crate-streamdal-protos-0.1.4 (c (n "streamdal-protos") (v "0.1.4") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1xs8j21ny3f82x8j4680nzzd774m5g31ji75hv07p9j4wpbzry5g")))

(define-public crate-streamdal-protos-0.1.5 (c (n "streamdal-protos") (v "0.1.5") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1zgyqas3y5nschiw9c25wdxqnbwxkdzldqzdr1r1n7qdlmnqdrcm")))

(define-public crate-streamdal-protos-0.1.6 (c (n "streamdal-protos") (v "0.1.6") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1l2ajyw3p8qnb3s43qf7g1giwk56g27xv0p38aik7gh9fgmzjlsv")))

(define-public crate-streamdal-protos-0.1.7 (c (n "streamdal-protos") (v "0.1.7") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0b5cniwpf1ii148y0vvch6bnf2q611vdjf03665689143s1yl4iv")))

(define-public crate-streamdal-protos-0.1.8 (c (n "streamdal-protos") (v "0.1.8") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0q91bng5mgmmrazfcryk3njxvr64qy3ip9c8avvyys4qnp283ap0")))

(define-public crate-streamdal-protos-0.1.9 (c (n "streamdal-protos") (v "0.1.9") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "13vqbadb8b1a6i581n4pz1cf8jf2y30rfaixb22m7vn3vrcnpx5h")))

(define-public crate-streamdal-protos-0.1.10 (c (n "streamdal-protos") (v "0.1.10") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1fcmqw6j93hn0mgzmx5my3dfxqdfb02xr0yai7z3vp4yamyy6jmi")))

(define-public crate-streamdal-protos-0.1.11 (c (n "streamdal-protos") (v "0.1.11") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0qqaczk49dd210mvv1sw0siivgwlpfbghzidc940akg3kyr81cln")))

(define-public crate-streamdal-protos-0.1.12 (c (n "streamdal-protos") (v "0.1.12") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1995g1m3jp6d2089mnq3ii205yrl47m6hgn05ldqszydspf4kdiv")))

(define-public crate-streamdal-protos-0.1.13 (c (n "streamdal-protos") (v "0.1.13") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1prpbakfvw9sx4vf6d78snarcprv6z8jad47pkdx5xgbw73zdc7z")))

(define-public crate-streamdal-protos-0.1.14 (c (n "streamdal-protos") (v "0.1.14") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "00raglqc32mwmb88b9z0sw7fhi4hnq3as2r1npk7q5cdxldnv629")))

(define-public crate-streamdal-protos-0.1.15 (c (n "streamdal-protos") (v "0.1.15") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "18d7kxg0rqpgg6fy11zyc575xvmkx1fshppss75rcnszfzxpi75n")))

(define-public crate-streamdal-protos-0.1.16 (c (n "streamdal-protos") (v "0.1.16") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1p7b2xvf2g849glkra2h3lrxwby92kf09qrg6zy97d6sfxqpa7yi")))

(define-public crate-streamdal-protos-0.1.17 (c (n "streamdal-protos") (v "0.1.17") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0969f72wla2kjdm2wm9vf3gyi638cv7b22n6p15xza93vi63g810")))

(define-public crate-streamdal-protos-0.1.18 (c (n "streamdal-protos") (v "0.1.18") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "030bazl5br2g8q3h61aw0idwhis8mvgy52bdm2gdh18xmzrp6nkq")))

(define-public crate-streamdal-protos-0.1.19 (c (n "streamdal-protos") (v "0.1.19") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0fih9znnf8cw94mrqacikfhm5fgbbld8za8cjxzvfp0jkl4wvsv3")))

(define-public crate-streamdal-protos-0.1.20 (c (n "streamdal-protos") (v "0.1.20") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1bfixfpx5yah6f9lcihkskn0k6vvkw08djpfhw6apwrjs01d81l6")))

(define-public crate-streamdal-protos-0.1.21 (c (n "streamdal-protos") (v "0.1.21") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1cppcg9dmqichl4y7mbs2r2xlwchkkg9sj13zkvim7rfgr95ff3q")))

(define-public crate-streamdal-protos-0.1.22 (c (n "streamdal-protos") (v "0.1.22") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "16nbnjzgppr396nd6fz7snzg0yqhx4d6v5sxr6wvjw7kf6r0i6lp")))

(define-public crate-streamdal-protos-0.1.23 (c (n "streamdal-protos") (v "0.1.23") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "1b9qgkp3lajp8nx4gq3w4l3rv07ci6r4mgra0j023fig8x6a1di0")))

(define-public crate-streamdal-protos-0.1.24 (c (n "streamdal-protos") (v "0.1.24") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0mp5fzb2bzc9fvgpl1v06r7pg64n9p9qp3x778py24zxhy0fr75v")))

(define-public crate-streamdal-protos-0.1.25 (c (n "streamdal-protos") (v "0.1.25") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0mrxckqa1c9ah890b41avjr9spxkpvjk2a7hx2c3qp1h3g96g0jz")))

(define-public crate-streamdal-protos-0.1.26 (c (n "streamdal-protos") (v "0.1.26") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "07sp6lda5rxg0sd3fskk77gywnkrxg3lq2r03vs634q9j67lkyrv")))

(define-public crate-streamdal-protos-0.1.27 (c (n "streamdal-protos") (v "0.1.27") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0f4a9j9p4sxr2q9zwdkzd7qavpf2fh151350qyp2aapqvdr8h4qc")))

(define-public crate-streamdal-protos-0.1.29 (c (n "streamdal-protos") (v "0.1.29") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "0fs9gic2wx410xqc2drvidci39vgx6ldgm60pxfqaihsn4bl6bsp")))

(define-public crate-streamdal-protos-0.1.30 (c (n "streamdal-protos") (v "0.1.30") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "0ipym447krnkssd3c4n6qqwwq8278frjbvjzs9936wwsa7z6clr4")))

(define-public crate-streamdal-protos-0.1.31 (c (n "streamdal-protos") (v "0.1.31") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "08jxbznrs4rk869gn57ilqyams1z2j6vg3j28z1v3j4bdf9khy7k")))

(define-public crate-streamdal-protos-0.1.32 (c (n "streamdal-protos") (v "0.1.32") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "09dddnhkxi88zjkp6m68svqbcyid0fwgjz0zl4v0ysbbmiysn778")))

(define-public crate-streamdal-protos-0.1.33 (c (n "streamdal-protos") (v "0.1.33") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "09xgilwdj8wwxa2vhxpfj1bldzxv24gib3q7xvqlnsb4q0swipq4")))

(define-public crate-streamdal-protos-0.1.34 (c (n "streamdal-protos") (v "0.1.34") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "1j9wcgb5s7mx37xy8ydw028zmqqg4awsm0k87wmmn74wdzdsn7p9")))

(define-public crate-streamdal-protos-0.1.35 (c (n "streamdal-protos") (v "0.1.35") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "03g86jacs6dns9kx8a7fr2z28i2a19193zc4wg737w59kyyzj1cx")))

(define-public crate-streamdal-protos-0.1.36 (c (n "streamdal-protos") (v "0.1.36") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "1iwnsb5xilgw3hbd160rdd3gzi2aggwbkpmh7bpvpbhyasqr2kjx")))

(define-public crate-streamdal-protos-0.1.37 (c (n "streamdal-protos") (v "0.1.37") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "0kzpd6gmpdc20czz0zi9amjxvgbbdsv2mnrl2db33jvxdd5brld3")))

(define-public crate-streamdal-protos-0.1.38 (c (n "streamdal-protos") (v "0.1.38") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "10fbjcmy49972dqmhcbp3dp2ajnq3vxwqnqqsjx3yg1vvvsd57zj")))

(define-public crate-streamdal-protos-0.1.39 (c (n "streamdal-protos") (v "0.1.39") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "04i7jhyrs2ipxfn1h86srfbbkhqfjjrg9m3a70qnyn4b0sc5k2ii")))

(define-public crate-streamdal-protos-0.1.40 (c (n "streamdal-protos") (v "0.1.40") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "09n5p6g5n8xjx7y65mg0p4nwyxz2326rqzg5lvfmqj44wsxa6pml")))

(define-public crate-streamdal-protos-0.1.41 (c (n "streamdal-protos") (v "0.1.41") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "1yn4ypaph47z69fa0j5i38d42zvcypy1nb14yynbxjhqwk5vd5mb")))

(define-public crate-streamdal-protos-0.1.42 (c (n "streamdal-protos") (v "0.1.42") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "0ib11j2gq0844ql66dkcpax9sc58rz2rwbipzk0v7n3y6513v08s")))

(define-public crate-streamdal-protos-0.1.43 (c (n "streamdal-protos") (v "0.1.43") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "194zi28m7bihg4mcplsq4w4paql0jkhba0g4fv12mry0v6zic2l0")))

(define-public crate-streamdal-protos-0.1.44 (c (n "streamdal-protos") (v "0.1.44") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "1hp9k2z458ddwxlaa329b695vh6zn35av5c34rx5sm2853ypxy57")))

(define-public crate-streamdal-protos-0.1.45 (c (n "streamdal-protos") (v "0.1.45") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "1v7sdik4dwgrjwf1mc63wb849jjrj2964qrffrhbplf4dda42pz1")))

(define-public crate-streamdal-protos-0.1.46 (c (n "streamdal-protos") (v "0.1.46") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "0hlx9fzzsscavhirligj9yq9cgjwd0cl925gxp7rqbgv1blfyn3d")))

(define-public crate-streamdal-protos-0.1.47 (c (n "streamdal-protos") (v "0.1.47") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "1gk9wqlk9jsbdsbl9gfd67xs52f2hpl0fv1r7726qj4h5n1xjg45")))

(define-public crate-streamdal-protos-0.1.48 (c (n "streamdal-protos") (v "0.1.48") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "1qxjpwrq4gra3v45ac3l2gcrl2w5h0nzjrx65bnk5l0k0p1y9fz9")))

(define-public crate-streamdal-protos-0.1.49 (c (n "streamdal-protos") (v "0.1.49") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "0ks7sazsj5g96pfhyh4d2hhsw9bl5lkg2rl75wcyqdxhidvb5xhs")))

(define-public crate-streamdal-protos-0.1.50 (c (n "streamdal-protos") (v "0.1.50") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "077dyr0ib6rf1180fi7snfzwzn3mcijxxgxv2lzaw2nj60skabzp")))

(define-public crate-streamdal-protos-0.1.51 (c (n "streamdal-protos") (v "0.1.51") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "1jfwyyl3dpafx7af2bvrmy6xjrh9s9jaa9l76671kp086klksgmw")))

(define-public crate-streamdal-protos-0.1.52 (c (n "streamdal-protos") (v "0.1.52") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "13c4r42p0mavq850ksir14m77829ad2ki4d8v1m92avm85mwnwk9")))

(define-public crate-streamdal-protos-0.1.53 (c (n "streamdal-protos") (v "0.1.53") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "026381jzcf5dlxlszlh5cdf84z6awxj211q03gd87qmsk1ll4pwv")))

(define-public crate-streamdal-protos-0.1.54 (c (n "streamdal-protos") (v "0.1.54") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "1al7mckw8ckjvl6v1maq7cfxjlb3c2rf14s2w0l74s0cdhiy3vyr")))

(define-public crate-streamdal-protos-0.1.55 (c (n "streamdal-protos") (v "0.1.55") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "1cb3i2xqrz5bpa4wd8nsn7snyq0zw0n24q7jcpys1yakncqs8s4q")))

(define-public crate-streamdal-protos-0.1.56 (c (n "streamdal-protos") (v "0.1.56") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "143cqbqclg2wnvqkhz5l67ppabnvbbd8m079sm9bg231yqn4rnmg")))

(define-public crate-streamdal-protos-0.1.57 (c (n "streamdal-protos") (v "0.1.57") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)))) (h "1jnz8caacnpw8i0c43d528bm051z98xx86cn08cxhigz2zgywg4z")))

