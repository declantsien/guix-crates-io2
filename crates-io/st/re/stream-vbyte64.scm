(define-module (crates-io st re stream-vbyte64) #:use-module (crates-io))

(define-public crate-stream-vbyte64-0.1.0 (c (n "stream-vbyte64") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0k3wxhysy1qhbn0l3dripjiffhdaamsv49nsa7av0cqyglwmsbln")))

(define-public crate-stream-vbyte64-0.1.1 (c (n "stream-vbyte64") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "07w2w26z16xpa2mcbndxys1n8a80k1ml59sy9mqsml7mz633cxja")))

(define-public crate-stream-vbyte64-0.1.2 (c (n "stream-vbyte64") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1hqpxzzs9v7sf7ckpxkj7qcn9lr6v0p6l1vn4cs03llqw1anc83x")))

