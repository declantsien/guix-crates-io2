(define-module (crates-io st re streamsha) #:use-module (crates-io))

(define-public crate-streamsha-0.1.0 (c (n "streamsha") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0jzkfiwhxsw9kgk5wvpdknav3lqlg9z30z5vfbimrzck6lqg1v0v")))

(define-public crate-streamsha-1.0.0 (c (n "streamsha") (v "1.0.0") (d (list (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0bjlbjzpcw4vchllg8h88kavv3jv7912l0716v2mfs1w8smfb7rz")))

