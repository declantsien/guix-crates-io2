(define-module (crates-io st re streamhouse-derive) #:use-module (crates-io))

(define-public crate-streamhouse-derive-0.0.1 (c (n "streamhouse-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18brg4lz22wps7vixicy1i9ic99wcgbja2z61wm2fbj8lrhycmw0")))

