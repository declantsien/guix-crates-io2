(define-module (crates-io st re streebog) #:use-module (crates-io))

(define-public crate-streebog-0.1.0 (c (n "streebog") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.1") (d #t) (k 2)) (d (n "digest") (r "^0.2") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0b2970icxa3qs5fgdywxs782z3xhrknbc4xhbks7l21pq65mywv5") (y #t)))

(define-public crate-streebog-0.1.1 (c (n "streebog") (v "0.1.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.1") (d #t) (k 2)) (d (n "digest") (r "^0.2") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0hlx7sabb7a7rahg7wp6jci3wz63vbv5lr5ancy8jynqglbqipvl") (y #t)))

(define-public crate-streebog-0.2.0 (c (n "streebog") (v "0.2.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2") (d #t) (k 2)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "12aayjg0v650flcws10c54bfpajv1kyzh9x3s7v5jjyn53wiains") (y #t)))

(define-public crate-streebog-0.2.1 (c (n "streebog") (v "0.2.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2") (d #t) (k 2)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "1dyya46mcbcr1crsjh7jvxfar2bfv0zvvjf5nhycp31xj5j1q8va") (y #t)))

(define-public crate-streebog-0.2.2 (c (n "streebog") (v "0.2.2") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2") (d #t) (k 2)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "1k62ra6hnbp485yyzagp34asjbvyznvisn31zrh7sj1jq7bxlp59") (y #t)))

(define-public crate-streebog-0.3.0 (c (n "streebog") (v "0.3.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "1qmnxm58sq26v1n84hb3cld8bkpnv66w749k5i2zvl14a7pnwp0f") (y #t)))

(define-public crate-streebog-0.3.1 (c (n "streebog") (v "0.3.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "0ifqf98swcxr0biixk35m6nrwzrvfjq77rv427qivimb0hkad6rl") (y #t)))

(define-public crate-streebog-0.4.0 (c (n "streebog") (v "0.4.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4.0") (d #t) (k 2)) (d (n "digest") (r "^0.5.0") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "14p8rhnpysqkn2xcgdqcd5a290r27gbbxkz027him1inlc7i3snn") (y #t)))

(define-public crate-streebog-0.4.1 (c (n "streebog") (v "0.4.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "0gs1zj1xs0w10rnaq5dlg801lbnfq04jikcdvbk8pqj7z2bcxhgq") (y #t)))

(define-public crate-streebog-0.5.0 (c (n "streebog") (v "0.5.0") (d (list (d (n "block-buffer") (r "^0.2") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "0ljwdrn5n2dj032zvnjx7y1v8d99lsv23cs65yy5d5npphnkigwv") (y #t)))

(define-public crate-streebog-0.7.0 (c (n "streebog") (v "0.7.0") (d (list (d (n "block-buffer") (r "^0.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "0gghlc5wnz3ck7j021v7qkb0n02za3ipdip4b20p5vi8vqdb4z09") (y #t)))

(define-public crate-streebog-0.8.0 (c (n "streebog") (v "0.8.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0742sk8cq1zwc5yvyfr2lqz7h4baxp2lrqhjmjhh2crhrqvmr8g0") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-streebog-0.9.0 (c (n "streebog") (v "0.9.0") (d (list (d (n "block-buffer") (r "^0.9") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "00gwqrv0s9vzjps65galsj35j6c851kkz799px63548k0nx1xlil") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-streebog-0.9.1 (c (n "streebog") (v "0.9.1") (d (list (d (n "block-buffer") (r "^0.9") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1xsx51ayyql2mhy404fxsdmhzmak72dkypck1dm6xc9zlsk8nqn3") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-streebog-0.9.2 (c (n "streebog") (v "0.9.2") (d (list (d (n "block-buffer") (r "^0.9") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0lz7ajfqdqbrnj01m1xc01ch1g0s9391ma36qqkiyf1074d1r8nr") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-streebog-0.10.0 (c (n "streebog") (v "0.10.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "1w7sxj3risp0zqm6r4mc73bd3fn3bnlxi4l10gp7661i5asr6ajz") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-streebog-0.10.1 (c (n "streebog") (v "0.10.1") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "0h8jc87c66y7bzrv2ssg5137ilxz2fivldf9am6f3x46rmbbv54d") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-streebog-0.10.2 (c (n "streebog") (v "0.10.2") (d (list (d (n "digest") (r "^0.10.4") (d #t) (k 0)) (d (n "digest") (r "^0.10.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "0dnm1f3bkm8rvskvl3cvhh1f2nbrpckr8c3hw1hc7kj2ibnyczwy") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std"))))))

(define-public crate-streebog-0.11.0-pre.0 (c (n "streebog") (v "0.11.0-pre.0") (d (list (d (n "digest") (r "=0.11.0-pre.3") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "1qy9qmjv9bc28zmry1hfk0y6djn7cikb57ilpz95kq17m6r05ywm") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (r "1.71")))

(define-public crate-streebog-0.11.0-pre.1 (c (n "streebog") (v "0.11.0-pre.1") (d (list (d (n "digest") (r "=0.11.0-pre.4") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "1filwi6gkwc783wy7h9s2d55x2fwp1h3xpj7v2hij6xnk67155ci") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (r "1.71")))

(define-public crate-streebog-0.11.0-pre.2 (c (n "streebog") (v "0.11.0-pre.2") (d (list (d (n "digest") (r "=0.11.0-pre.7") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "0hwqzij13bg24s3yx9k85rk639lyklhzvhrdrwcz1x4xcqczfj88") (f (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (r "1.71")))

(define-public crate-streebog-0.11.0-pre.3 (c (n "streebog") (v "0.11.0-pre.3") (d (list (d (n "digest") (r "=0.11.0-pre.8") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "028wqyjrd7rvzyk3lcj4hxl9wxhidaq65imaqy3g3z3b1gpslslh") (f (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (r "1.71")))

