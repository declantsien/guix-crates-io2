(define-module (crates-io st re streamdal-transform) #:use-module (crates-io))

(define-public crate-streamdal-transform-0.0.7 (c (n "streamdal-transform") (v "0.0.7") (d (list (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "snitch-gjson") (r "^0.8.1-with-set") (d #t) (k 0)) (d (n "streamdal-protos") (r "^0.0.115") (d #t) (k 0)))) (h "0ygaxd9fzzpvdd75bc7mzrdhk882zpmhiyymgq07s7wf6l1x8hvz") (y #t)))

