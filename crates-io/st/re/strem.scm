(define-module (crates-io st re strem) #:use-module (crates-io))

(define-public crate-strem-0.0.0 (c (n "strem") (v "0.0.0") (h "0zl0r2j926xga54ykh3mlh9nd8r1c0f2jq2jgz1dn5vazi08yn2c")))

(define-public crate-strem-0.1.0 (c (n "strem") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0gf28pz72im2df9i5ma2va6k1llr84kv547a2cnb67cqv4im6qgx") (s 2) (e (quote (("export" "dep:image" "dep:imageproc"))))))

(define-public crate-strem-0.1.1 (c (n "strem") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "00hswhhzxr63gz9966l7ac6d11pwwncqy25hrjln4v4p667g9dvi") (s 2) (e (quote (("export" "dep:image" "dep:imageproc"))))))

