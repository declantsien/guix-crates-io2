(define-module (crates-io st re streak) #:use-module (crates-io))

(define-public crate-streak-0.0.1 (c (n "streak") (v "0.0.1") (d (list (d (n "dotenv") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_url_params") (r "^0.1") (d #t) (k 0)))) (h "0p81sk6jkphscyf7xr0crcx1lsn4ar5axsn8p5kydhxz754ihw4z")))

