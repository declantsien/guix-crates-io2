(define-module (crates-io st re stree_cmd) #:use-module (crates-io))

(define-public crate-stree_cmd-0.2.0 (c (n "stree_cmd") (v "0.2.0") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "suffix_tree") (r "*") (d #t) (k 0)))) (h "12ndsfiy5h9w4smjgljf0rwr0ksrfk3z0wnipgjc5bcy7fvc8yfa")))

(define-public crate-stree_cmd-0.2.1 (c (n "stree_cmd") (v "0.2.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "suffix_tree") (r "^0.2") (d #t) (k 0)))) (h "1zslrjniywvabzswajj1lb9zw01clwz9c01xdmgy597qzcmx06yp")))

