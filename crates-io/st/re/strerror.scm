(define-module (crates-io st re strerror) #:use-module (crates-io))

(define-public crate-strerror-0.1.0 (c (n "strerror") (v "0.1.0") (h "15gmf0jq8qa27v8x0m84wmv9442022p27m7xqffvq1pqxx4w151z")))

(define-public crate-strerror-0.2.0 (c (n "strerror") (v "0.2.0") (h "13554byk8cwla6gzva9j8m0qlrw4ikafayd85yyaa4qisc20innd")))

(define-public crate-strerror-0.2.1 (c (n "strerror") (v "0.2.1") (h "15myh3b2zj8l0y3algi0zj4hddzb4bjf657sx75kbaiaj0z5pg13")))

(define-public crate-strerror-0.2.2 (c (n "strerror") (v "0.2.2") (h "17fka4zgd0bfsradyimnqjgsq37b7lkk92yd6ijcj4dls3ni3d7a")))

(define-public crate-strerror-0.3.0 (c (n "strerror") (v "0.3.0") (h "0rszhhn9vs1d2v0rg0lqlclrjwg6xndvzi68in41gpyln5z0pply")))

(define-public crate-strerror-0.3.1 (c (n "strerror") (v "0.3.1") (h "06s1wcc4hi0jjd4653zkglvq8pw02421lhpwp4k7g07gwx5zlwnx")))

(define-public crate-strerror-0.3.2 (c (n "strerror") (v "0.3.2") (h "1w9hlnl1nfhlvgafj6vplwf91hzfrp23xsans9r4brb800c6myjb")))

(define-public crate-strerror-0.3.3 (c (n "strerror") (v "0.3.3") (h "1nr9i43qqdw7izbsaqzin6f0k3cilc2g330y9ic0j6x43147nz1m")))

(define-public crate-strerror-0.3.4 (c (n "strerror") (v "0.3.4") (h "1kqvc61fc6l5j36cfhc8jvhrcdidsxcnjqvgibwbxzv1hxz4ciwb")))

(define-public crate-strerror-0.3.5 (c (n "strerror") (v "0.3.5") (h "17r5299ww29yr9apq26qq8bxi0vrkacv2lwcxi7l89ad238b7lll")))

(define-public crate-strerror-0.3.6 (c (n "strerror") (v "0.3.6") (h "0kpjmv8pf21k6z00msvdksc1dlj75fvbx26m2cxg8pskvq1f3mr6")))

(define-public crate-strerror-0.3.7 (c (n "strerror") (v "0.3.7") (h "1nki779wr9q5gda8na79wd7brgigz2ymvw91hin0i1h6wq9jh7lr")))

(define-public crate-strerror-0.3.8 (c (n "strerror") (v "0.3.8") (h "1l7g4sv3kgww7v4h1dz2vdr4diwgs1mi2cr5y22pr3ai33y0dvyf")))

(define-public crate-strerror-0.3.9 (c (n "strerror") (v "0.3.9") (h "038l9dxayl5q8d47s3w8r15il0ciwrzncljdyq3bffk0jimlvsc6")))

(define-public crate-strerror-0.3.10 (c (n "strerror") (v "0.3.10") (h "0z0jmw3fqf2v11cvyyjww4xx4if1gxblkm6zbkgfcr8g1k4xpazl")))

(define-public crate-strerror-0.3.11 (c (n "strerror") (v "0.3.11") (h "0zdqb9n5ga4yrkfcmylp2z1i17b9gn02g6gk56qvxqmmz4ajgh2c")))

(define-public crate-strerror-0.4.0 (c (n "strerror") (v "0.4.0") (h "06hkl85cll6blzrwkiczbzsz23f8nx8415nhmhxj1w5q2bc8429j")))

(define-public crate-strerror-0.4.1 (c (n "strerror") (v "0.4.1") (h "1fgmfj0c5az99p2y9dj8p9caq2hdrg0c6b9452vlws90mmrby1dx")))

(define-public crate-strerror-0.5.0 (c (n "strerror") (v "0.5.0") (h "0hq3w13jnkvd9lql55dhyj0nm3qrad1hbdgln6441ynzdg7kiykg")))

(define-public crate-strerror-0.5.1 (c (n "strerror") (v "0.5.1") (h "0jyhr9z4r7xfzclbn33risi1qdarya1h71vslxjamgriw3p51a7p")))

(define-public crate-strerror-0.6.0 (c (n "strerror") (v "0.6.0") (h "1rjmvljba3rrjhf4vpiqj7rqy8d2cffc3x02vmndgg67dbjrk0xn")))

(define-public crate-strerror-0.6.1 (c (n "strerror") (v "0.6.1") (h "03yjfli2kp5q3q2q3yg2xf69zbv0fz4bhflhzpmqlqp33s2c2hwn")))

(define-public crate-strerror-0.6.2 (c (n "strerror") (v "0.6.2") (h "18s0rxzwg54zsxmgnq7cp3z6g6yrk8jpfk20kl23q6jm1rbwvpqz")))

(define-public crate-strerror-0.6.3 (c (n "strerror") (v "0.6.3") (h "1p5j12nypd4qy9p7nrnpa4p2sr5l487jq9v9s1vib3c6klvmdmkx")))

