(define-module (crates-io st re stream_router) #:use-module (crates-io))

(define-public crate-stream_router-0.0.1 (c (n "stream_router") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0fd1mnark9lpvkiwlnc395h5200il2w1bxg2v9sa6bzr63dcdq0s")))

(define-public crate-stream_router-0.1.0 (c (n "stream_router") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "03b393fh8byca0hnzbii4dc62p1wxsbcmrpnl3icmz760nqfjnlv")))

(define-public crate-stream_router-0.2.0 (c (n "stream_router") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 2)) (d (n "num_stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1r407gvs1s92xj5lhrdlcqjy4bpyisyzjxzc75f4rhw423b04jv4")))

(define-public crate-stream_router-0.2.1 (c (n "stream_router") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 2)) (d (n "num_stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1py133d4whx3npj7zrbjpmzp46qf101ikw0hpnfqfkh5v96773va")))

