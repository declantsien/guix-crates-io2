(define-module (crates-io st re streamflow) #:use-module (crates-io))

(define-public crate-streamflow-0.1.1 (c (n "streamflow") (v "0.1.1") (d (list (d (n "solana-program") (r "^1.7.1") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.7.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.7.1") (d #t) (k 2)))) (h "0b5c9nmb7biznqkwh9c574f1w14qm8islcmn0q9hilb5n9y90nw8")))

