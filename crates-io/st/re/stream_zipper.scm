(define-module (crates-io st re stream_zipper) #:use-module (crates-io))

(define-public crate-stream_zipper-0.1.0 (c (n "stream_zipper") (v "0.1.0") (d (list (d (n "miniz_oxide") (r "^0.3.6") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "0h0qjvz2jfqif57wnxq9h3a70883523iy9pbbcnihvy7ri3v0i4h")))

(define-public crate-stream_zipper-0.2.0 (c (n "stream_zipper") (v "0.2.0") (d (list (d (n "miniz_oxide") (r "^0.3.6") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "1fijzf228y9slbr0p5c1kq48msrddkg6gfw3dhgbl1frnpc9cq8c")))

