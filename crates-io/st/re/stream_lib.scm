(define-module (crates-io st re stream_lib) #:use-module (crates-io))

(define-public crate-stream_lib-0.1.0 (c (n "stream_lib") (v "0.1.0") (d (list (d (n "hls_m3u8") (r "^0.1.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0ifhh1ndaxbfqzr0s7fcpvg66m63bwbzk5mdx0kx5nlz4w6y7d2x")))

(define-public crate-stream_lib-0.2.0 (c (n "stream_lib") (v "0.2.0") (d (list (d (n "hls_m3u8") (r "^0.1.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0mmc89f37hjg1jlsgli0av33vckry9plrm627wbq6czfiq4skjjy") (f (quote (("spinner" "indicatif") ("default"))))))

(define-public crate-stream_lib-0.3.0 (c (n "stream_lib") (v "0.3.0") (d (list (d (n "hls_m3u8") (r "^0.1.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1gwqbs6a7wggdlric1yg4rpnfhj98z4m03w957c9kj0w1farvq9r") (f (quote (("spinner" "indicatif") ("default"))))))

