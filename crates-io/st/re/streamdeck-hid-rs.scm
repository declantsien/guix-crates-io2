(define-module (crates-io st re streamdeck-hid-rs) #:use-module (crates-io))

(define-public crate-streamdeck-hid-rs-0.1.0 (c (n "streamdeck-hid-rs") (v "0.1.0") (d (list (d (n "hidapi") (r "~1") (d #t) (k 0)) (d (n "image") (r "~0") (d #t) (k 0)) (d (n "imageproc") (r "~0") (d #t) (k 0)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "mockall") (r "~0") (d #t) (k 0)))) (h "0w6qa1kxcjr9akbf07yayjafyv1l5w3q5qsyws2256z1z4kr2n2i")))

(define-public crate-streamdeck-hid-rs-0.1.1 (c (n "streamdeck-hid-rs") (v "0.1.1") (d (list (d (n "hidapi") (r "~1") (d #t) (k 0)) (d (n "image") (r "~0") (d #t) (k 0)) (d (n "imageproc") (r "~0") (d #t) (k 0)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "mockall") (r "~0") (d #t) (k 0)))) (h "1k4sp9jfvgllass0g5ixzgg86m30phsqlj3iqra7icj6z82c5ksq")))

