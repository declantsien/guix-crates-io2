(define-module (crates-io st re stream-cancel) #:use-module (crates-io))

(define-public crate-stream-cancel-0.1.0 (c (n "stream-cancel") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1rzapbaylcnca3bc9lyc3wb1kfrvdjm6qn0w70ka16wzcg7my7kl")))

(define-public crate-stream-cancel-0.2.0 (c (n "stream-cancel") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1rciya95shbx9p2z79rcmv8hcs0gi918zhvvrph6sphka9c6lq8r")))

(define-public crate-stream-cancel-0.2.1 (c (n "stream-cancel") (v "0.2.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0igk577kkd5lsmla1ddbn17p3wypyw9c2j8iv5g8jiwv8fkwrhl1")))

(define-public crate-stream-cancel-0.3.0 (c (n "stream-cancel") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0dvl4fb7j34xad66ii2wsmw3nc9rksdlz3p2vvnb0wmbzmnda941")))

(define-public crate-stream-cancel-0.4.0 (c (n "stream-cancel") (v "0.4.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0sgd979fd83l6n809wdv629wghzswa19y57dz6cazaqfsjnnjx7k")))

(define-public crate-stream-cancel-0.4.1 (c (n "stream-cancel") (v "0.4.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1hzw3gdhxlwbi218z2jnsvllhfzfzn1wg255r91qjsldgzwcz6zi")))

(define-public crate-stream-cancel-0.4.2 (c (n "stream-cancel") (v "0.4.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1inpds2flfnbw2jss8h43dxynw66gyaiqc2q9sn1n6jrxyr4p64l")))

(define-public crate-stream-cancel-0.4.3 (c (n "stream-cancel") (v "0.4.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "07wij63wi2f3v8qg82f05z159bvbqk58hp6zraxw3myg3x9m3jkh")))

(define-public crate-stream-cancel-0.4.4 (c (n "stream-cancel") (v "0.4.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1pxf5vik479hgh4nif9x2f7cajdz70kinrygxn7fqdc9jshgwqlx")))

(define-public crate-stream-cancel-0.5.0-alpha.1 (c (n "stream-cancel") (v "0.5.0-alpha.1") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0-alpha.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.4") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.4") (d #t) (k 2)))) (h "1i5hdcwkl2wac0ianqm9sij0m4dn25l1bv3c0d3asbca6izprb25")))

(define-public crate-stream-cancel-0.5.0-alpha.2 (c (n "stream-cancel") (v "0.5.0-alpha.2") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0-alpha.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.5") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.5") (d #t) (k 2)))) (h "0y5yyc8ql238rlni9r2yci1yjnd1fk7gdsc7hv0fn6bq3qy0j611")))

(define-public crate-stream-cancel-0.5.0-alpha.3 (c (n "stream-cancel") (v "0.5.0-alpha.3") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "0hiysxqy5rj0jnnphzjsyy53bwl1ma6cghqvnqgz3a0y698dq1nn")))

(define-public crate-stream-cancel-0.5.0-alpha.4 (c (n "stream-cancel") (v "0.5.0-alpha.4") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 2)) (d (n "tokio-sync") (r "= 0.2.0-alpha.6") (d #t) (k 0)))) (h "03gq6qwqraczy4hsa6b9c765xg1gm4851jvk03zl2ykr84dvrxn8")))

(define-public crate-stream-cancel-0.5.0 (c (n "stream-cancel") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1xrqj8c3i8vcbv8bwmsx0fqyxa0gmxxzjk8kc0vd6s4804n3hkyy")))

(define-public crate-stream-cancel-0.5.1 (c (n "stream-cancel") (v "0.5.1") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1mgb62biyd70jfh944x1c4gsjbmdj67p2vzs3pjhd98za4sib1b5")))

(define-public crate-stream-cancel-0.5.2 (c (n "stream-cancel") (v "0.5.2") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0j4fz8sbj0p8n02wjq23r9c5ilw0fiin2r2rnc40vvcs0fzgbhhm")))

(define-public crate-stream-cancel-0.6.0 (c (n "stream-cancel") (v "0.6.0") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0ii61h2mnx2zkpcigr9z79dsyjzdc0l6rpc8p5f6y7qm8ljsv4hg") (y #t)))

(define-public crate-stream-cancel-0.6.1 (c (n "stream-cancel") (v "0.6.1") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0wpzbbjxx58pp3cvnx33gry727bc1s4k1aas71z08kq314q460f7")))

(define-public crate-stream-cancel-0.6.2 (c (n "stream-cancel") (v "0.6.2") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0css5flmzyxq9iy16kqk1mvj3spab5isxn0qv1pppv7s9l0cmgpw")))

(define-public crate-stream-cancel-0.7.0 (c (n "stream-cancel") (v "0.7.0") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.3.0") (f (quote ("sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0n47qrkb8xzqdli90zy9mirflz0x8lvcb95w33fi7zf7f9arh0m7")))

(define-public crate-stream-cancel-0.8.0 (c (n "stream-cancel") (v "0.8.0") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-project") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.1") (f (quote ("net"))) (d #t) (k 2)))) (h "0k1cjzvj7rdlrk4zqsjc1lkqn2mglyn49chaw0jz32iykvzlhs7k")))

(define-public crate-stream-cancel-0.8.1 (c (n "stream-cancel") (v "0.8.1") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-project") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.1") (f (quote ("net"))) (d #t) (k 2)))) (h "16qmmmajk8l5gs4hs2h9y34vyx2nykgzqhnrq26gn2ajf6r9w2kv")))

(define-public crate-stream-cancel-0.8.2 (c (n "stream-cancel") (v "0.8.2") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-project") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.1") (f (quote ("net"))) (d #t) (k 2)))) (h "18qlympdvkaizz295n8c1j95awjv1qf9b858d26z2k0ysydvz7sz") (r "1.63.0")))

