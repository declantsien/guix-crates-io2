(define-module (crates-io st do stdout-sink) #:use-module (crates-io))

(define-public crate-stdout-sink-0.2.1 (c (n "stdout-sink") (v "0.2.1") (d (list (d (n "ipc-chan") (r "^0.7.0") (d #t) (k 0)))) (h "09vklz8ny3yp1vhwcrpxhb50kdc3zwmrdaxd5l70j00vbiacys3w")))

(define-public crate-stdout-sink-0.2.2 (c (n "stdout-sink") (v "0.2.2") (d (list (d (n "ipc-chan") (r "^0.7.0") (d #t) (k 0)))) (h "0g54033ydaadp70ql6ckmh119v9dqy2kx35djmzrsx3qpgx70lai")))

(define-public crate-stdout-sink-0.3.1 (c (n "stdout-sink") (v "0.3.1") (d (list (d (n "ipc-chan") (r "^0.8.0") (d #t) (k 0)))) (h "19cvpprd0kxaqyzkzhc6bwab9n48frwj964gyi183lx87p046xfg")))

