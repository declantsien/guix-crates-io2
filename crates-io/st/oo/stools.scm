(define-module (crates-io st oo stools) #:use-module (crates-io))

(define-public crate-stools-0.1.0 (c (n "stools") (v "0.1.0") (h "0nifyg7b4r3bhs0kcp616grmpwq236il6n8l2y8fh9nw24vyydr7") (y #t)))

(define-public crate-stools-0.1.1 (c (n "stools") (v "0.1.1") (h "13g1kikwlr4a29kziqh0lzsz4899myf29mjap2l184160xrqrax6") (y #t)))

(define-public crate-stools-0.1.3 (c (n "stools") (v "0.1.3") (h "0da3aqs3n0vjk196sgfcwsqpc1wrp9inais5ydaw07v7x4177r48") (y #t)))

(define-public crate-stools-1.0.0 (c (n "stools") (v "1.0.0") (h "0i5n2zabq8i0660x9lpdb2s4qr1nfr52lymicsrjbjh54chqhf2f") (y #t)))

(define-public crate-stools-1.0.1 (c (n "stools") (v "1.0.1") (h "026whzmm0xlqs77fd6qcws3za81incd4r3jfy52hq3pn3ighghcb") (y #t)))

(define-public crate-stools-1.0.3 (c (n "stools") (v "1.0.3") (h "0v1ginbsh6x42ly7lylrkb2gyszh98wml2351zyp2mw9xipqh7ka") (y #t)))

(define-public crate-stools-1.0.4 (c (n "stools") (v "1.0.4") (h "0wsfpcpipbr3104m7wg1fx1jfgf1b1f2jx1cg4016rk1szbr1323") (y #t)))

(define-public crate-stools-1.0.5 (c (n "stools") (v "1.0.5") (h "1dkwpx8i666xx3ij25cwm8h8xdwfdz991gcc32s4ygn8lj9d0n35") (y #t)))

(define-public crate-stools-1.0.7 (c (n "stools") (v "1.0.7") (h "10sb0v90xfzm63qgc3mfriib4wkbkpfqk2y7b1p753wziw82qj6g") (y #t)))

(define-public crate-stools-1.0.8 (c (n "stools") (v "1.0.8") (h "063am7ly6cq9cxaabyv800bv3y0vcgpp01613n5b4475q3vhvadi") (y #t)))

(define-public crate-stools-1.1.0 (c (n "stools") (v "1.1.0") (h "0nncczzhrpy30lsmv0hs3zyvdw6kvkvjxck78rpnn1h5vgjgjlwx") (f (quote (("hook_key")))) (y #t)))

(define-public crate-stools-1.1.1 (c (n "stools") (v "1.1.1") (h "025acwgg2kizyc4438d688kz78w71qxgg6cfppl6fg1c151zrilf") (f (quote (("hook_key")))) (y #t)))

(define-public crate-stools-1.1.2 (c (n "stools") (v "1.1.2") (h "0b8kcgxpgkw7xrp0fhhy5z13jinybyp247crh0j3m4rcxjqh6n9c") (f (quote (("hook_key"))))))

(define-public crate-stools-1.1.4 (c (n "stools") (v "1.1.4") (h "05968zx4spxm9brga3g4x9m2i0p8y1yyjj59m4qz23a3347wa2l8") (f (quote (("hook_key"))))))

(define-public crate-stools-1.1.6 (c (n "stools") (v "1.1.6") (h "1qn9603cxls7gq2r50ymlwas9zkx4yyrlb9mqgzp3prcyy85awgd") (f (quote (("hook_key"))))))

