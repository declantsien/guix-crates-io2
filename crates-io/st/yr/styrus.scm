(define-module (crates-io st yr styrus) #:use-module (crates-io))

(define-public crate-styrus-0.1.0 (c (n "styrus") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0v3s9bmnbd087li8hxayfp1rmsf9gdpkr032kgmc8zynlmqbc74s")))

(define-public crate-styrus-0.1.1 (c (n "styrus") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "0h2vxllary8ppscvva6a5qv0g2rsglclmcjknx016fz0qs63rmvb")))

(define-public crate-styrus-0.1.2 (c (n "styrus") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "02xwd92f2b53vn3p4nb6vkwljqw3wfp2s0ipymbl1gm4cmf4zsmc")))

