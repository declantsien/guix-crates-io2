(define-module (crates-io st l_ stl_rotate) #:use-module (crates-io))

(define-public crate-stl_rotate-0.1.0 (c (n "stl_rotate") (v "0.1.0") (h "1qc6cs1qr84y550bx6qd01d0nzqnc67frkdq6l4b2abi5qm518cx")))

(define-public crate-stl_rotate-0.2.0 (c (n "stl_rotate") (v "0.2.0") (h "02da2pppfx3bx7qp9bc30vxxhfmdp2lyj175gvhjfhp29a7ah375")))

