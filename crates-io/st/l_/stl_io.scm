(define-module (crates-io st l_ stl_io) #:use-module (crates-io))

(define-public crate-stl_io-0.3.0 (c (n "stl_io") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0yb9qf8snx1rvlwdn825lh1ya23h66000x4cxaqlxlqqxvmzn0ry")))

(define-public crate-stl_io-0.3.1 (c (n "stl_io") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1fbfgbyqcfxq3bprppdwr64q3dp423lflgmvdswdhr7xazwf59kr")))

(define-public crate-stl_io-0.3.2 (c (n "stl_io") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0d7s1cf5ri15bmqg1dwcm01v152fs4ga70v1ppx2fcd8aikp63yd")))

(define-public crate-stl_io-0.3.3 (c (n "stl_io") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1lkzfn5zclx2gs30qdkr32mvmbfh3w6kpsqxx7kskfrywdc6xwnv")))

(define-public crate-stl_io-0.3.4 (c (n "stl_io") (v "0.3.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "18nrqicwqdvmkc0mzy2xryr1nm2h9g4b86qdsw7xj3h89jdaqngc")))

(define-public crate-stl_io-0.3.5 (c (n "stl_io") (v "0.3.5") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1xvnlsr8cii2fbscvlsdkfcw988qqa04a4jvv80zww4yb39br6k5")))

(define-public crate-stl_io-0.3.6 (c (n "stl_io") (v "0.3.6") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0dchqxg919rg913zbx9kcigga5q7b8dvzx36x715aily74246lnr")))

(define-public crate-stl_io-0.3.7 (c (n "stl_io") (v "0.3.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0l38bmwv471p9k0v9ryfgfr1m0dcc1mfsindni5qfdng8nqqj8bq")))

(define-public crate-stl_io-0.3.8 (c (n "stl_io") (v "0.3.8") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "17ywip9nxnrg6v7pqm0qcbj932cblibqfb5wcp7svlzkci7lh41q")))

(define-public crate-stl_io-0.4.0 (c (n "stl_io") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1pkdb8nbsd1mqvxn8vqxd1dpi5pv8blzq410k23ff9ysbgl7n2nk")))

(define-public crate-stl_io-0.4.1 (c (n "stl_io") (v "0.4.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1wry22amgv2m65fl1991h29h6kp4b9s0br7c7l199bbgig70cbv6")))

(define-public crate-stl_io-0.4.2 (c (n "stl_io") (v "0.4.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0v6h7hmr36mfyspzx0lcl45qqwmhw22rdq3l0b869549c84i72fw")))

(define-public crate-stl_io-0.5.0 (c (n "stl_io") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1s7fhl9ssy29dhbnpi30gqnbz8a3nzr0nn6y3by1jdrai2wz7mpa")))

(define-public crate-stl_io-0.5.1 (c (n "stl_io") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)))) (h "0akvddicpwcmqxlhscqv1s3n4czcw7av4qwk8w95vknnvkpzw7c2")))

(define-public crate-stl_io-0.5.2 (c (n "stl_io") (v "0.5.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)))) (h "1lfprplskbza8gfdxk1hn7v7zdnwh0y696vsldzgb8r1g9v7ian9")))

(define-public crate-stl_io-0.6.0 (c (n "stl_io") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)))) (h "0p86bwld7mw6vxhy8aldys916pyidkv6miawi2fjdv5k108y8s2w")))

(define-public crate-stl_io-0.7.0 (c (n "stl_io") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)))) (h "1xvw9zs9dn726w4l5q8bmcsxbp66i80wzh7k6an6drijwq1yn86m")))

