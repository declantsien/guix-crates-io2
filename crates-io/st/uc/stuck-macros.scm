(define-module (crates-io st uc stuck-macros) #:use-module (crates-io))

(define-public crate-stuck-macros-0.1.1 (c (n "stuck-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03p6k9956xm6l91bqgl27dvml4rli36qr2y69bihsn744ixaw20v")))

(define-public crate-stuck-macros-0.1.3 (c (n "stuck-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a2ll1xb6vkpnlvnwlbiy9shb3ly09mr5c7mxz99r20iklgsambw")))

(define-public crate-stuck-macros-0.1.4 (c (n "stuck-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m13gwqpzf0jpyay359dgkvja5bvfvc1p1zz18swcv5qw68rw2ab")))

(define-public crate-stuck-macros-0.1.5 (c (n "stuck-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0msf722qg49zd3ybziaxgkbmqv7igyd6q0zwb5n2yfk7zx50szns")))

(define-public crate-stuck-macros-0.2.0 (c (n "stuck-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ci3frr5b1bb6xx06w5rxckwvbs7qk96w3z84hf6w08j7yfb47k7")))

(define-public crate-stuck-macros-0.3.0 (c (n "stuck-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j1wf5jvm2ciamx9nbm5bnq47k5yw958zd0r5fp97p6212zk5rx6")))

(define-public crate-stuck-macros-0.3.1 (c (n "stuck-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02yh6wafwivbir1bv7im8iw1lmlqha1g4z4g6wcangamsrkgpfpc")))

