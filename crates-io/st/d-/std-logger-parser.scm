(define-module (crates-io st d- std-logger-parser) #:use-module (crates-io))

(define-public crate-std-logger-parser-0.1.0 (c (n "std-logger-parser") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.86") (k 0)) (d (n "log") (r "^0.4.14") (k 0)))) (h "0sbgzk6qk7afj4gashwfxkv7vc2ilbv1jvmn9d361s1mpnhh1fdz")))

