(define-module (crates-io st d- std-shims) #:use-module (crates-io))

(define-public crate-std-shims-0.1.0 (c (n "std-shims") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.13") (d #t) (k 0)))) (h "00jjcd408h4cx24hm07zx9qxkd1wp2989vlg84va0p6abprmd4kr") (f (quote (("std") ("default" "std"))))))

(define-public crate-std-shims-0.1.1 (c (n "std-shims") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "spin") (r "^0.9") (f (quote ("mutex" "once"))) (d #t) (k 0)))) (h "09kr6c1x6yf8bi7x8208lrwpm81y2330aliam2k7a2qvydh97r4h") (f (quote (("std") ("default" "std")))) (r "1.70")))

