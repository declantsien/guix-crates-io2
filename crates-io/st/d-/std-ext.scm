(define-module (crates-io st d- std-ext) #:use-module (crates-io))

(define-public crate-std-ext-0.1.0 (c (n "std-ext") (v "0.1.0") (h "0zp3qvin1aaj6ix8khwpw3g57kkpm88fw9vkng92p2kndja0gc63") (r "1.38")))

(define-public crate-std-ext-0.1.1 (c (n "std-ext") (v "0.1.1") (d (list (d (n "async-lock") (r "^2.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "05jpz5pfls1g2c3kaj7sk1k30j3fbh9f5si0q810l627lc0zbj0r") (r "1.38")))

(define-public crate-std-ext-0.1.2 (c (n "std-ext") (v "0.1.2") (d (list (d (n "async-lock") (r "^2.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0s9cqhqvzccwk747xrk4j6jwq734m0rcyns5i77ms90hq72ccm8g") (r "1.47")))

(define-public crate-std-ext-0.1.3 (c (n "std-ext") (v "0.1.3") (d (list (d (n "async-lock") (r "^2.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1y5asrdjdljz9xg31569nxdzkncnrpmswflq6kq0gbkk93wrf2ls") (r "1.47")))

(define-public crate-std-ext-0.2.0 (c (n "std-ext") (v "0.2.0") (d (list (d (n "async-lock") (r "^2.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0pfghkyc6hdmdr3fg2hm1ksiixbk5nyrk6c770sb1mh9bsj38hyx") (r "1.47")))

(define-public crate-std-ext-0.2.1 (c (n "std-ext") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1c2g1nvy4sv9ylp01g8zd8hblmcvrf82ldyh0zppgy1f9xynhbz5") (r "1.47")))

(define-public crate-std-ext-0.2.2 (c (n "std-ext") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "08bl5py5609n6fjw9r22rl2ldzjsapfa4a99yqq8rhbqwnwhfx3r") (r "1.47")))

