(define-module (crates-io st d- std-embedded-nal-async) #:use-module (crates-io))

(define-public crate-std-embedded-nal-async-0.1.0 (c (n "std-embedded-nal-async") (v "0.1.0") (d (list (d (n "async-io") (r "^1.9") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (d #t) (k 0)) (d (n "dns-lookup") (r "^2.0.4") (d #t) (k 0)) (d (n "embedded-io-async") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "embedded-nal-async") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("socket" "net" "uio"))) (d #t) (k 0)))) (h "1mm92j9vp4awn070f63x93hdgabx5ngcknd6nm1p4546crj1c9z6") (r "1.75")))

(define-public crate-std-embedded-nal-async-0.2.0 (c (n "std-embedded-nal-async") (v "0.2.0") (d (list (d (n "async-io") (r "^1.9") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (d #t) (k 0)) (d (n "dns-lookup") (r "^2.0.4") (d #t) (k 0)) (d (n "embedded-io-async") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "embedded-nal-async") (r "^0.7") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("socket" "net" "uio"))) (d #t) (k 0)))) (h "1gfsgnx0n0p5nlxmycwg5symin6arip5160bhhn95k8chqfnfdrj") (r "1.75")))

