(define-module (crates-io st d- std-io-peek) #:use-module (crates-io))

(define-public crate-std-io-peek-0.1.0 (c (n "std-io-peek") (v "0.1.0") (h "1lfrxfivx4yzljmwnn4fwqmg0n9irs7rk2wh65sf8hcid5r2715k")))

(define-public crate-std-io-peek-0.2.0 (c (n "std-io-peek") (v "0.2.0") (h "0pwrjfc4kw4n157h3hsg3x83cr70rq5pn7279nl3nymnxbjqn0hk")))

