(define-module (crates-io st d- std-mel) #:use-module (crates-io))

(define-public crate-std-mel-0.8.0-rc1 (c (n "std-mel") (v "0.8.0-rc1") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lvvcbi88m3lqrv1vl9k5rf6sb0nv30g2gb714rnngy8715jrwm9") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-std-mel-0.8.0-rc2 (c (n "std-mel") (v "0.8.0-rc2") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc2") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nqlmarj6ih38grcmkgq19wplkbginq30rxbvspfi5clcif8vrys") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-std-mel-0.8.0-rc3 (c (n "std-mel") (v "0.8.0-rc3") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc3") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fcrrgq7fwzff2f2x768gd6daadp1qja2dqpf16jij1bwyfcfp08") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-std-mel-0.8.0 (c (n "std-mel") (v "0.8.0") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "^0.8.0") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k1fy5hi59ch0rb86pkglnp91sqaf3mlnccvgjx1rab861iv67sl") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

