(define-module (crates-io st un stun_codec) #:use-module (crates-io))

(define-public crate-stun_codec-0.1.0 (c (n "stun_codec") (v "0.1.0") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1f4n7cp54f1bmdw2w3x6hwfxjpa94p3sb83d6ixl4x2fahr5s10m")))

(define-public crate-stun_codec-0.1.1 (c (n "stun_codec") (v "0.1.1") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1kw6sjnfy1mkafyn965qmfpqcln224rwp2cpm0q7lrv5h93gsbgg")))

(define-public crate-stun_codec-0.1.2 (c (n "stun_codec") (v "0.1.2") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1mx34mng3lcif88sisarjmganwx7mdd3fa42dqjmnr9cfqqjq17f")))

(define-public crate-stun_codec-0.1.3 (c (n "stun_codec") (v "0.1.3") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1w5kr5yb1lbzs89kqcdf9927njwgdrkm0ac2c4mlmmxq266ksp1c")))

(define-public crate-stun_codec-0.1.4 (c (n "stun_codec") (v "0.1.4") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1flgcvv38a9yvk5403kd523cv5l0bz7ppi904ijx7jpxk4yg79pm")))

(define-public crate-stun_codec-0.1.5 (c (n "stun_codec") (v "0.1.5") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1rczbhdak5v1zmrp99ih7rf2wfb3yzfamzgnbnbblb6d5m8p6r9g")))

(define-public crate-stun_codec-0.1.6 (c (n "stun_codec") (v "0.1.6") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "17wz2lv04vzch3nhblbg367pkf39nclsmd1hzc1rpzyc7gvqxx7r")))

(define-public crate-stun_codec-0.1.7 (c (n "stun_codec") (v "0.1.7") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1f1y1bpjdw356nfbjhlcfqak81c17r5a1rbmsgpf3j8fndrvqd0v")))

(define-public crate-stun_codec-0.1.8 (c (n "stun_codec") (v "0.1.8") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0ci6fsqzdbw8pvbd96xsb3rqyysjsg17gixg28fdl8x159cl0jgb")))

(define-public crate-stun_codec-0.1.9 (c (n "stun_codec") (v "0.1.9") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1agbm15h2c08wwzlq8lfmyjg74zlgmhkcxkpii12m7ny4nrp4kkd")))

(define-public crate-stun_codec-0.1.10 (c (n "stun_codec") (v "0.1.10") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0wvcbb5qka9jzyh2ynsirsqx415pvqch6x4hml583wx90s2wmi13")))

(define-public crate-stun_codec-0.1.11 (c (n "stun_codec") (v "0.1.11") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0f0f6y8w63199f4rfbgcfp0jgxph86qbnvqc8163k77djqsm7hda")))

(define-public crate-stun_codec-0.1.12 (c (n "stun_codec") (v "0.1.12") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1k7c18a34fjmz15651kj05kd8ig17clkwwbhniv84mdjna274hj2")))

(define-public crate-stun_codec-0.1.13 (c (n "stun_codec") (v "0.1.13") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0q2s7c40dcr4iigg86w5r0j0k8bflh7rfqx21227635ig5a1q900")))

(define-public crate-stun_codec-0.2.0 (c (n "stun_codec") (v "0.2.0") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^2") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "1cgkpry28xnjnxi9d0j2rgy0rmzlp45cajwgxpzw3bwjwp2gc78z")))

(define-public crate-stun_codec-0.3.0 (c (n "stun_codec") (v "0.3.0") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "0bzswx7i7p00w97ixbvly13sch0fys8zsqdw0xjpxan87pdy2lvf")))

(define-public crate-stun_codec-0.3.1 (c (n "stun_codec") (v "0.3.1") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "065j755dn7mz44zv9v4g7mhk2j4mqx05xg9mdrmnq4vvjnr7yalp")))

(define-public crate-stun_codec-0.3.2 (c (n "stun_codec") (v "0.3.2") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "0nz0qwk9jrbcybcj877jjsl0q0wknijykv6nxq4wjfx68ikzd2a0")))

(define-public crate-stun_codec-0.3.3 (c (n "stun_codec") (v "0.3.3") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "190xd87rfaccivibvgk8yhbk90h6qdbhi1swpq62278axk9igzqy")))

(define-public crate-stun_codec-0.3.4 (c (n "stun_codec") (v "0.3.4") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "0jyd6pvlhkp4lfzvwwc116dqyqsgxqnqf320bh9j5ig4n3gxlh0k")))

(define-public crate-stun_codec-0.3.5 (c (n "stun_codec") (v "0.3.5") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "0nvv4h9j7cq8kfwcyviwnvlc46hz19mp5km3dhmlza5xw2prvvgy")))

