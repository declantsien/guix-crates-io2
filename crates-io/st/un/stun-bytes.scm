(define-module (crates-io st un stun-bytes) #:use-module (crates-io))

(define-public crate-stun-bytes-1.0.0 (c (n "stun-bytes") (v "1.0.0") (d (list (d (n "r-ex") (r "^1.0.0") (f (quote ("endian" "carve" "copy-from" "from-arr" "splice"))) (d #t) (k 0)))) (h "1qcppa3m19xkhyccqrsgiqrk056lrr8nzn7dvx23axkrqhgrbg1a") (f (quote (("default") ("cookie"))))))

(define-public crate-stun-bytes-1.0.1 (c (n "stun-bytes") (v "1.0.1") (d (list (d (n "r-ex") (r "^1.0.1") (f (quote ("endian" "carve" "copy-from" "from-arr" "splice"))) (d #t) (k 0)))) (h "04izkzh9avifqm81ardf1grax9037nf9yj2ddvhg32jch5kddsrv") (f (quote (("default") ("cookie"))))))

