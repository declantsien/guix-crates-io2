(define-module (crates-io st un stun_codec_blazh) #:use-module (crates-io))

(define-public crate-stun_codec_blazh-0.1.13 (c (n "stun_codec_blazh") (v "0.1.13") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1y55b2gz5zrnvrcdyx10a1kkp4x6swmfyrxd0mgbhsq6pmv93zcn")))

