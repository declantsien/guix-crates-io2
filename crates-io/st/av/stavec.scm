(define-module (crates-io st av stavec) #:use-module (crates-io))

(define-public crate-stavec-0.1.0 (c (n "stavec") (v "0.1.0") (h "0drg6qpmcd2pq5h6amrn7xp50sjgw3a4aav26iky1zs0n2k02wa5") (f (quote (("std") ("default" "std"))))))

(define-public crate-stavec-0.1.1 (c (n "stavec") (v "0.1.1") (h "0vrpa1lhkywvn5cqqrl3ixqsvn7p78siadn95n47wc6kif2k8fzc") (f (quote (("std") ("default" "std"))))))

(define-public crate-stavec-0.1.2 (c (n "stavec") (v "0.1.2") (h "07s475lxyvk0r16i28zdzxms1l88j2h4bnsp6d2590zrdxz3fypj") (f (quote (("std") ("default" "std"))))))

(define-public crate-stavec-0.2.0-rc.0 (c (n "stavec") (v "0.2.0-rc.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1lj8h7kcd7qg6l15zd23h9phq5vrjzmwixs5h2j37inwivfl57j7") (f (quote (("std" "num-traits/std") ("repr-c") ("default" "std"))))))

(define-public crate-stavec-0.2.0-rc.2 (c (n "stavec") (v "0.2.0-rc.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0pj35bsgc6cb2wlsa91bhap2r1bv1rqa2c7mswil1nxam8r3ppbc") (f (quote (("std" "num-traits/std") ("repr-c") ("default" "std"))))))

(define-public crate-stavec-0.2.0 (c (n "stavec") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1r2wk7gg0921mk4s4nc5py9n41vdhzgwgdcd2qrpnj7zbdq96xbw") (f (quote (("std" "num-traits/std") ("repr-c") ("default" "std")))) (y #t)))

(define-public crate-stavec-0.2.1 (c (n "stavec") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0wv52770labmcjclxyb225vp7hjmv552l4pbnjf6mi2ca3bdpdfh") (f (quote (("std" "num-traits/std") ("repr-c") ("default" "std"))))))

(define-public crate-stavec-0.3.0 (c (n "stavec") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0az0ixwll4h03rh3mmbqfj0yd1mblx8x4hrzij85ncvi4n0gjqcs") (f (quote (("std" "num-traits/std") ("repr-c") ("default" "std"))))))

