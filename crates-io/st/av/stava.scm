(define-module (crates-io st av stava) #:use-module (crates-io))

(define-public crate-stava-0.1.0 (c (n "stava") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "15y3l77p79pnjaaqbzxsav3y257jrxj6ba8sf7vq8zp19a0x4j5z")))

(define-public crate-stava-0.2.0 (c (n "stava") (v "0.2.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "19k4cs4xvrj60il7bm5v1r2n05qzspf3knxrjlg173d4sbypv719")))

(define-public crate-stava-0.3.0 (c (n "stava") (v "0.3.0") (d (list (d (n "clap") (r "^2.31.2") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std" "perf"))) (k 0)))) (h "1gy5xz9l5b19ay5x53a1cb5ck4c255zsr08m0mpjc0ri5lzphva4")))

(define-public crate-stava-0.3.1 (c (n "stava") (v "0.3.1") (d (list (d (n "clap") (r "^2.31.2") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std" "perf"))) (k 0)))) (h "1jpg8nay3f1mcrj1cbw30xz8n5mj0sphhrdzyss5sk481cw1jrbv")))

(define-public crate-stava-0.4.0 (c (n "stava") (v "0.4.0") (d (list (d (n "clap") (r "^2.31.2") (k 0)) (d (n "include_dir") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std" "perf"))) (k 0)))) (h "1k3zhsixzkxy4rz6wgim9js1hp14pzam84fz80c4c9jq3009h4yh")))

(define-public crate-stava-0.4.1 (c (n "stava") (v "0.4.1") (d (list (d (n "clap") (r "^2.31.2") (k 0)) (d (n "include_dir") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std" "perf"))) (k 0)))) (h "1lnkv80hblmbg04bq7nbq525ycybnbdakqqywpar121gaan42rfc")))

(define-public crate-stava-0.5.0 (c (n "stava") (v "0.5.0") (d (list (d (n "clap") (r "^2.31.2") (k 0)) (d (n "include_dir") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std" "perf"))) (k 0)))) (h "0fbrnh6s9p015iml8fmr2306w52l8l1pfgrw01hm76hzsgl6c4zn")))

(define-public crate-stava-0.6.0 (c (n "stava") (v "0.6.0") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2.31.2") (k 0)) (d (n "include_dir") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.2") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (f (quote ("std" "perf"))) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "13z5bw22hy72nw3lhsgbb2ik1679z01yaznhaq8q1ikgxp7g4hdh")))

(define-public crate-stava-0.6.1 (c (n "stava") (v "0.6.1") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2.31.2") (k 0)) (d (n "include_dir") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.2") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (f (quote ("std" "perf"))) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1nifka0cpm7nazxqnwrzwwpiw4rp56glsngia69wwkl963ynnlng")))

(define-public crate-stava-0.6.2 (c (n "stava") (v "0.6.2") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "clap") (r "^2.33.1") (k 0)) (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "regex") (r "^1.4.2") (f (quote ("std" "perf"))) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "15yv7c96gz8nl1rh1d59aizh6j0zqc1pm575x82iqlwdnmq42496")))

