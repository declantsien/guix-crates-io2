(define-module (crates-io st b_ stb_truetype) #:use-module (crates-io))

(define-public crate-stb_truetype-0.1.0 (c (n "stb_truetype") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "0ljsqbbj3bbqjshxmzi0ra8qckn3hpqyp95n20kgnbi1pj92v8bm")))

(define-public crate-stb_truetype-0.1.1 (c (n "stb_truetype") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "0jbb9mkakxnqbrkr9rca8x01dcrbga3kgca4gn23brs6xqryfxkw")))

(define-public crate-stb_truetype-0.1.2 (c (n "stb_truetype") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "026sdzxhbmfbnbql4bskwxwm7f7bznrnx0rysq4f57gw8042gwzw")))

(define-public crate-stb_truetype-0.2.0 (c (n "stb_truetype") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "0ysig67ll41s7b64cmmkfbj6jn4in790fggvzp2g04mlrwamr5d0")))

(define-public crate-stb_truetype-0.2.1 (c (n "stb_truetype") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "01c8jnqb1d76hbcbv94vbxi2fdhv17p6k5yrw1vs94x4i2sw7d91")))

(define-public crate-stb_truetype-0.2.2 (c (n "stb_truetype") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "0f2m5npaf6yw0jj8i6j8x8zh1pg090182a49qvxwy4fxmcw2pkjj")))

(define-public crate-stb_truetype-0.2.3 (c (n "stb_truetype") (v "0.2.3") (d (list (d (n "approx") (r "^0.3") (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "0gfsqdrhmbrs11xpz8fjidb46mrw17w6p9z82bmdfxi5q1zndhg5")))

(define-public crate-stb_truetype-0.2.4 (c (n "stb_truetype") (v "0.2.4") (d (list (d (n "approx") (r "^0.3") (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "1wcavb4nnwxf77y0ikjslxbk1i3c86spwz0zvq4mjr6q6qqpvyj8")))

(define-public crate-stb_truetype-0.2.5 (c (n "stb_truetype") (v "0.2.5") (d (list (d (n "approx") (r "^0.3") (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "0w5dr5gcr07k4lkf7ds8gak4q11il8cbwhf35ni2jq9vnihd59vi")))

(define-public crate-stb_truetype-0.2.6 (c (n "stb_truetype") (v "0.2.6") (d (list (d (n "approx") (r "^0.3") (k 2)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "1fk8ar6wn7vnxfcqvg8lhbh47dg544s6kr4bzxa1vs5qbm8dzdv9")))

(define-public crate-stb_truetype-0.2.7 (c (n "stb_truetype") (v "0.2.7") (d (list (d (n "approx") (r "^0.3") (k 2)) (d (n "byteorder") (r "^1.1") (k 0)) (d (n "libm") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "15p0sx3hwzi2kg7d1g07hd07nbpp87n6ysjqgvy4nlahidhry41m") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-stb_truetype-0.3.0 (c (n "stb_truetype") (v "0.3.0") (d (list (d (n "approx") (r "^0.3") (k 2)) (d (n "byteorder") (r "^1.1") (k 0)) (d (n "libm") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0ykggf9iaifsg2q441j5lld33iv0d2nhw9s54nnw7jsjzgb10hl2") (f (quote (("std") ("default" "std"))))))

(define-public crate-stb_truetype-0.2.8 (c (n "stb_truetype") (v "0.2.8") (d (list (d (n "stb_truetype_next") (r "^0.3") (d #t) (k 0) (p "stb_truetype")))) (h "0pa2rfqjlkh015lny0ls8w28r38y8pw2kgff1xl5lk19h91yq6wx")))

(define-public crate-stb_truetype-0.3.1 (c (n "stb_truetype") (v "0.3.1") (d (list (d (n "approx") (r "^0.3") (k 2)) (d (n "byteorder") (r "^1.1") (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0lgvnh3ma6cz811bk8imj45djz76zs47b8327sgnmik2x03nnyzp") (f (quote (("std") ("default" "std"))))))

