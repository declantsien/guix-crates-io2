(define-module (crates-io st b_ stb_truetype_rust) #:use-module (crates-io))

(define-public crate-stb_truetype_rust-2.27.0 (c (n "stb_truetype_rust") (v "2.27.0") (h "050va8s8qmbk675gyc7lpm8dg9hldgy90njgnq0a383hrs4h1ab5") (y #t)))

(define-public crate-stb_truetype_rust-1.26.1 (c (n "stb_truetype_rust") (v "1.26.1") (h "03pzfw31vm5ikhf9753sp7v2790dqklmgrn8ikd9xx26qm79ywf9")))

