(define-module (crates-io st b_ stb_rect_pack_sys) #:use-module (crates-io))

(define-public crate-stb_rect_pack_sys-0.1.0 (c (n "stb_rect_pack_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.61") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1qdkqqdx6d72n5by6pqyb0wv15g8d0adl97lxfs67fg1s9kb7a4l")))

(define-public crate-stb_rect_pack_sys-0.1.1 (c (n "stb_rect_pack_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.61") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0cm9i783pvn5dxw0g0v10w8n68bssdjg4bmqhcyxl0nmf3kl9i8g")))

(define-public crate-stb_rect_pack_sys-0.1.2 (c (n "stb_rect_pack_sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.61") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0wvh0vh76zvl4drln4pbb5qy8a101p29ggh57hk74r2nha61nc7m")))

(define-public crate-stb_rect_pack_sys-0.1.4 (c (n "stb_rect_pack_sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.61") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0c5pfkknx4rfir74jaxc7ks5f6jhppd09y631vp69pnlx32l9q8q")))

(define-public crate-stb_rect_pack_sys-0.1.5 (c (n "stb_rect_pack_sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.63") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0y8zc8v5jj5gvv90w411ryb83gdk2fm540d6imjsv8c6flx06z20")))

(define-public crate-stb_rect_pack_sys-0.2.0 (c (n "stb_rect_pack_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.64") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1d65z5j777pxkxmf4d0n6yb70imzkbv3mvc2ii0y51dwjdsz9q4z")))

(define-public crate-stb_rect_pack_sys-0.3.0 (c (n "stb_rect_pack_sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "12s4xh34j000a6818rv2imy83a0f8s3lmjx80qw14325sjk84isl")))

