(define-module (crates-io st b_ stb_image_rust) #:use-module (crates-io))

(define-public crate-stb_image_rust-2.27.0 (c (n "stb_image_rust") (v "2.27.0") (h "0vqf4apvnjfiy6jar39qirrpm556ha3ba3n80q6h1bfkgah3707n")))

(define-public crate-stb_image_rust-2.27.1 (c (n "stb_image_rust") (v "2.27.1") (h "0gby0b6azxq7hrz1mnd945ciyvad6g6nvnfxdgwsxnrhfwv18pkd")))

(define-public crate-stb_image_rust-2.27.2 (c (n "stb_image_rust") (v "2.27.2") (h "1amnc11vrhxqr3b8f3svxzd1qid8am5gwb6j8ygy1m81mc1a10nn")))

