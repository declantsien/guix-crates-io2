(define-module (crates-io st b_ stb_image_write_rust) #:use-module (crates-io))

(define-public crate-stb_image_write_rust-1.16.0 (c (n "stb_image_write_rust") (v "1.16.0") (h "1mwsg1msdd0avaygpfd1if79c6n4d0pi56gqpllrvn8mmgngdhq7")))

(define-public crate-stb_image_write_rust-1.16.1 (c (n "stb_image_write_rust") (v "1.16.1") (h "1d33n8avk41psqx84sard0ahn8sa62xdlw776sygfgr1sm28py5f")))

