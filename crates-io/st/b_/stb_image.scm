(define-module (crates-io st b_ stb_image) #:use-module (crates-io))

(define-public crate-stb_image-0.1.0 (c (n "stb_image") (v "0.1.0") (h "15sc8l2jf21449fpq77cx9krq2swpmw4w06gqrsg1rd6m553y91a")))

(define-public crate-stb_image-0.2.1 (c (n "stb_image") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1dbbzpvjrwmhlbhq1c2hk3y2550m155iq16rkqag13fxlld6493a")))

(define-public crate-stb_image-0.2.2 (c (n "stb_image") (v "0.2.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "14knxr1v1pw763nyqz6fg87yb1z9ypgfvdds3x2yxpq0lra8x8px")))

(define-public crate-stb_image-0.2.3 (c (n "stb_image") (v "0.2.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "07hy1mhlgmhr9mpv6zqq8mzj33pi7ysb7xg65chm4pxhxx1dwfma")))

(define-public crate-stb_image-0.2.4 (c (n "stb_image") (v "0.2.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0qdffq2a1c7n1l99bhlgbvmg95vakh3c0wd3y0shsr243m8kl2nl")))

(define-public crate-stb_image-0.2.5 (c (n "stb_image") (v "0.2.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0r608cmql9r5r01b7inm9zm82zlz0jawp601k9cnglqlv1izwrg8")))

(define-public crate-stb_image-0.3.0 (c (n "stb_image") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1lzwykh1sbsgf40gyf1bi1anfy04ry54lv5imgj64cyxnygainmz")))

