(define-module (crates-io st b_ stb_rect_pack) #:use-module (crates-io))

(define-public crate-stb_rect_pack-0.1.0 (c (n "stb_rect_pack") (v "0.1.0") (d (list (d (n "stb_rect_pack_sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13lsp2ii9cywy7axdk4bi4hs6icyvw58892p99knmyv8prymaf8y")))

(define-public crate-stb_rect_pack-0.2.0 (c (n "stb_rect_pack") (v "0.2.0") (d (list (d (n "stb_rect_pack_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g87iwajyihpnax8pqp70qqm110n4bpywmmsnc6fsd3zl90ldimw")))

(define-public crate-stb_rect_pack-0.3.0 (c (n "stb_rect_pack") (v "0.3.0") (d (list (d (n "stb_rect_pack_sys") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h3i1bz7n9264wyfjz725b8n04wb02agym4aq2bg0iyf6xml7d9d")))

