(define-module (crates-io st pe stperf) #:use-module (crates-io))

(define-public crate-stperf-0.1.0 (c (n "stperf") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0qkiw584s6a2ds0hbi8fjr4gb9cb9gv9j248cyzhmqjqvgm25z1k") (f (quote (("disabled") ("default"))))))

(define-public crate-stperf-0.1.1 (c (n "stperf") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0gxlgw3sf6chz5vzf1nvvrw14vdb8pkss55f1if10nx6xwvsqina") (f (quote (("disabled") ("default"))))))

(define-public crate-stperf-0.1.2 (c (n "stperf") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "15yj0ilq04scpnxgdxh0api7kwv7cpf5cjlbx09fk5slzg7xdz7p") (f (quote (("disabled") ("default"))))))

(define-public crate-stperf-0.1.3 (c (n "stperf") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1difmpmjrink9b4v8m0pl6a2dwal3737mbn6si5spdrnqs2ympmj") (f (quote (("disabled") ("default"))))))

(define-public crate-stperf-0.1.4 (c (n "stperf") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0jyj9hyixq1ybm0bg58nakb04sz87ah6a8a8m14z4ikm2dg7c6r0") (f (quote (("disabled") ("default"))))))

