(define-module (crates-io st ry stry-evermore) #:use-module (crates-io))

(define-public crate-stry-evermore-0.1.0 (c (n "stry-evermore") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (o #t) (k 0)))) (h "0fm8dh9ibgdxg50ibraw3byvwhhlf1mkjsv9i4fspwgr0adk2l3d") (f (quote (("with-tracing" "tracing" "tracing-futures") ("with-log" "log"))))))

