(define-module (crates-io st ry stry-attrouter) #:use-module (crates-io))

(define-public crate-stry-attrouter-0.1.0 (c (n "stry-attrouter") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x9vyxmnfhi8iaj8qmp21rxlhpz3vvv2aa51lbli6bi4hmgm63rm") (f (quote (("with-warp") ("with-tide") ("default" "with-warp"))))))

