(define-module (crates-io st r_ str_indices) #:use-module (crates-io))

(define-public crate-str_indices-0.1.0 (c (n "str_indices") (v "0.1.0") (h "18wf2zfc2vabim38lp4gsfv1x1h1v9a2afdjcr8vgr2l22qvcwhz")))

(define-public crate-str_indices-0.2.0 (c (n "str_indices") (v "0.2.0") (h "1pq0v28y92g9qzc3yl4s0nbwkwm8ya1vzl2a6pdalwdwix2crx7z")))

(define-public crate-str_indices-0.3.0 (c (n "str_indices") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0r67ayxpncfpr772vlwhcxhijjyf33vj79hxnmz9ry491gb157fa")))

(define-public crate-str_indices-0.3.1 (c (n "str_indices") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1am47wn0wmwk69gankd76v1fmb4yvcsw94jdggicbr46qi4alfr8")))

(define-public crate-str_indices-0.3.2 (c (n "str_indices") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1y3kp1hc92fz4pzlz09zy0692qlsjnr8ayk6rl0i35a73cxddymd")))

(define-public crate-str_indices-0.4.0 (c (n "str_indices") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1h7jl3j0q1ip9gbmiwrkr2x2w1i0lms47s0bc9sf05y8h3x9k4cx") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-str_indices-0.4.1 (c (n "str_indices") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1pfnfjmkandxqmq04nqa5nwgsggq8jp8z4xivr9fqhk8j9j620jz") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-str_indices-0.4.2 (c (n "str_indices") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1r1b7qk6wbz8dk93l5qnbv74dwcd52dgmjbq2lrql3g5x3fsxvmq") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-str_indices-0.4.3 (c (n "str_indices") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0p6kggjax1mx0niq22dsm5xq2jvg6l4nyrm8a6f0138yaav7qmg9") (f (quote (("simd") ("default" "simd"))))))

