(define-module (crates-io st r_ str_splitter) #:use-module (crates-io))

(define-public crate-str_splitter-0.1.0 (c (n "str_splitter") (v "0.1.0") (h "1xa8cjhc41s26ps047q10rb8qgq288h7vg5dgf700kimifblbiv2")))

(define-public crate-str_splitter-0.1.1 (c (n "str_splitter") (v "0.1.1") (h "18229847r82y59iky50b55hv7687kq375cjd1hyikzfqyaaf00pc")))

