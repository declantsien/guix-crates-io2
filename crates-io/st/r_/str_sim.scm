(define-module (crates-io st r_ str_sim) #:use-module (crates-io))

(define-public crate-str_sim-0.1.0 (c (n "str_sim") (v "0.1.0") (h "1hnrlapka56aqb0h1z6jgk8xgwlifvx2b74h98lcqd9bdbzvr3lb")))

(define-public crate-str_sim-0.1.1 (c (n "str_sim") (v "0.1.1") (h "0lyq250hm2ymlkv58yncp7cfd1a1sp0r0p91qmk6kdzyrmxhh64h")))

(define-public crate-str_sim-0.1.2 (c (n "str_sim") (v "0.1.2") (h "07hn2388ik39x72csppk3dlkdfxnbgjh2arcpwp81sii0v0yh9bj")))

