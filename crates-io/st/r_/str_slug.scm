(define-module (crates-io st r_ str_slug) #:use-module (crates-io))

(define-public crate-str_slug-0.1.0 (c (n "str_slug") (v "0.1.0") (d (list (d (n "blake3") (r "^0.3.1") (d #t) (k 0)) (d (n "deunicode") (r "^1.1.0") (d #t) (k 0)))) (h "17y7yaq158v9s5ipwsd4kn13836nxv3rkgbbna0s58bjr14wvb7m")))

(define-public crate-str_slug-0.1.1 (c (n "str_slug") (v "0.1.1") (d (list (d (n "blake3") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "deunicode") (r "^1.1.0") (d #t) (k 0)))) (h "0vy8gi5vqa8fi5gfz4l9hzgbqms48j8ky34kb4cw30ag5a60jr87") (f (quote (("hash" "blake3"))))))

(define-public crate-str_slug-0.1.2 (c (n "str_slug") (v "0.1.2") (d (list (d (n "blake3") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "deunicode") (r "^1.1.0") (d #t) (k 0)))) (h "005v0sjxlvhmb3yscysg0yhks3hnc3kzl7ayvk9gvbpscqa5drc2") (f (quote (("hash" "blake3"))))))

(define-public crate-str_slug-0.1.3 (c (n "str_slug") (v "0.1.3") (d (list (d (n "blake3") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "deunicode") (r "^1.1.0") (d #t) (k 0)))) (h "1yl5vlw6iq9v3a0xv0nwaj6nbq87xw09ylx0axr9mncj1gm97nl4") (f (quote (("hash" "blake3"))))))

