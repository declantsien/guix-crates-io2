(define-module (crates-io st r_ str_overlap) #:use-module (crates-io))

(define-public crate-str_overlap-0.1.0 (c (n "str_overlap") (v "0.1.0") (h "06jpaaw25k9j08zj6c1ynmgcyaglz4flf2cd21qfyz6f8dfsligw")))

(define-public crate-str_overlap-0.1.1 (c (n "str_overlap") (v "0.1.1") (h "1ppmg8r57wlwzchmvnmin2anprczqa308rsh88z363gnjxl9xhkn")))

(define-public crate-str_overlap-0.2.0 (c (n "str_overlap") (v "0.2.0") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "1h1xgghn9v99jarpdwhaa69f94h884xjpb59aqh9k8whi5w7r171")))

(define-public crate-str_overlap-0.3.0 (c (n "str_overlap") (v "0.3.0") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "0c6qnxdl20ql60r2n1wvmwz4f620vcdvj8l6yrqbngr4kk1vm9n8")))

(define-public crate-str_overlap-0.4.0 (c (n "str_overlap") (v "0.4.0") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "1ynmd3b9707d17mcxf8wl73yd5fmy7qljrqz8anb98iibyrm9kcb")))

(define-public crate-str_overlap-0.4.1 (c (n "str_overlap") (v "0.4.1") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "0wgw6mvg9inhv0n3fb78hf5wll8rkjw5j6pzfz8w8fnfdahfsr44") (y #t)))

(define-public crate-str_overlap-0.4.2 (c (n "str_overlap") (v "0.4.2") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "0znm3ri5asxk5v35mmmbiylf16v1d0crsb3hji9r4fh742lh63xs")))

(define-public crate-str_overlap-0.4.3 (c (n "str_overlap") (v "0.4.3") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "120biddcy49japy39bc0z345yz8g20nhn25xmyn36i52fm4dk8kh")))

