(define-module (crates-io st r_ str_assert) #:use-module (crates-io))

(define-public crate-str_assert-0.1.0 (c (n "str_assert") (v "0.1.0") (d (list (d (n "dissimilar") (r "^1.0") (d #t) (k 0)))) (h "0m2dw4zfa5qfch89sg7ll5kpv4y6ndwyd164mih6ni2kygjjfsfm")))

(define-public crate-str_assert-0.2.0 (c (n "str_assert") (v "0.2.0") (d (list (d (n "dissimilar") (r "^1.0") (d #t) (k 0)))) (h "1fvvg82rc5bsbxsap1a81wiwr3h3g1ga2nvbz4p1ajcamjqm97q8")))

