(define-module (crates-io st r_ str_split_mut) #:use-module (crates-io))

(define-public crate-str_split_mut-0.1.0 (c (n "str_split_mut") (v "0.1.0") (h "0453z5id8a6c0fnl3y08p022q29w91vhgchkbwdyd7ramrn59jwn") (y #t)))

(define-public crate-str_split_mut-0.1.1 (c (n "str_split_mut") (v "0.1.1") (h "0swdsi802i26vcxd88ng1kffcmxlqgrcwngl1g6k8906w5hkpkzc")))

(define-public crate-str_split_mut-0.1.2 (c (n "str_split_mut") (v "0.1.2") (h "16413id4accp23d9nwz7axl04xw57bwgrzaqac1fqvhfb75lba85")))

(define-public crate-str_split_mut-0.2.0 (c (n "str_split_mut") (v "0.2.0") (h "0rr7vs1lglqakgjcypm3p3f25zbzmjhd9lyf6wvq0rka6i9ly2rb")))

(define-public crate-str_split_mut-0.2.1 (c (n "str_split_mut") (v "0.2.1") (h "0n2yccdnviggafmbary958j8jivzv5ainjir470nxzzx839phz1p")))

(define-public crate-str_split_mut-0.3.0 (c (n "str_split_mut") (v "0.3.0") (h "1spwah7yb1xw81gn7qzdxspvf25a07pc9q4rjsa7ac9m9nbcppwr") (y #t)))

(define-public crate-str_split_mut-0.3.1 (c (n "str_split_mut") (v "0.3.1") (h "1v0wf4dkzk9d17i0cvmmy2xmyc7xy7y7026vnd8p0jxm15xfssv4") (y #t)))

(define-public crate-str_split_mut-0.4.0 (c (n "str_split_mut") (v "0.4.0") (h "0c9nq04r07b6jzwqfkm701yhzpplz8mcsk8zfadflqa4if3i65q9")))

(define-public crate-str_split_mut-0.4.1 (c (n "str_split_mut") (v "0.4.1") (h "0jbg7ad8xy6ngz8q3rnq9bdhhd16spl378jf82x4pir94bqb3anf")))

