(define-module (crates-io st r_ str_to_bytes) #:use-module (crates-io))

(define-public crate-str_to_bytes-0.1.0 (c (n "str_to_bytes") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "0jmychz69kzy0d08w1zx170rys87q8i0w7fsgiydnba8p4qap7if")))

(define-public crate-str_to_bytes-0.2.0 (c (n "str_to_bytes") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "0jb9xapzw718wqfnadj41lbs1b9wr3vlvd6ps9a7ndblqjva504n")))

(define-public crate-str_to_bytes-0.1.1 (c (n "str_to_bytes") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1h7vw60f58dja2hqq296a4f8v7a5cz90yhwyhg024l7sxf7kjqsf")))

(define-public crate-str_to_bytes-0.2.1 (c (n "str_to_bytes") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "04ppd8zy5zj3k91518izhkh2m76a61macvmn51k9fhc1pp5w8h5j")))

(define-public crate-str_to_bytes-0.2.2 (c (n "str_to_bytes") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1vy6iz7yncnv0ysdcxn77hxim6mbnfhdyak3piwwv3jpzcfcfl6s")))

(define-public crate-str_to_bytes-0.2.3 (c (n "str_to_bytes") (v "0.2.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1nncbnq36jbxs2z3j7gi3b0bvppj9y3zxhnpdav7d5b43m4gwggf")))

