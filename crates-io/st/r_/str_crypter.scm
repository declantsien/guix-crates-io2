(define-module (crates-io st r_ str_crypter) #:use-module (crates-io))

(define-public crate-str_crypter-1.0.0 (c (n "str_crypter") (v "1.0.0") (h "1925qyigim27ssiciv37zv4622kcaqnqzha76cjnqms7ghhl0ygk")))

(define-public crate-str_crypter-1.0.1 (c (n "str_crypter") (v "1.0.1") (h "00p68fvmpxgapw9iz2vwddc4ll1a09g74k4zhzq1d81ck7w2aqsa")))

(define-public crate-str_crypter-1.0.2 (c (n "str_crypter") (v "1.0.2") (h "1wm55wpmcqvh0rbas6li1pda2vmp6jga820nnh559gfbs24pi1f5")))

