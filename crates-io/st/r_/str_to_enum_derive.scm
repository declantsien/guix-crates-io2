(define-module (crates-io st r_ str_to_enum_derive) #:use-module (crates-io))

(define-public crate-str_to_enum_derive-0.1.0 (c (n "str_to_enum_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0r2rgqrx3wjd76cyzs2vck3mlnibg7j4b7l78rrl1j49z347lhip")))

(define-public crate-str_to_enum_derive-0.2.0 (c (n "str_to_enum_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0p931pq80nmfi8glr2xml736xg1i3vvizm6mya2xxhvwp9q3dq8i")))

