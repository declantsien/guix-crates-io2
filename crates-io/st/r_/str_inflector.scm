(define-module (crates-io st r_ str_inflector) #:use-module (crates-io))

(define-public crate-str_inflector-0.12.0 (c (n "str_inflector") (v "0.12.0") (d (list (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1xsfxyfckrjrj7zjb8vqj9ffi7q77i5gh05ys4xb75bnba6q82zc") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

