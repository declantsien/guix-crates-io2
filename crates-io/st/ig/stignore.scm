(define-module (crates-io st ig stignore) #:use-module (crates-io))

(define-public crate-stignore-1.0.0 (c (n "stignore") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "18l66zs5z3nc56sswsxsja6ql7flcvgvmvnwn5pfdcip5sf0d5cq")))

