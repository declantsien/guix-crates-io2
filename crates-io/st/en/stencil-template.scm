(define-module (crates-io st en stencil-template) #:use-module (crates-io))

(define-public crate-stencil-template-0.1.0 (c (n "stencil-template") (v "0.1.0") (h "0g6sx62bbfc16v26f4k68wff06qgq8749hpgyrlxw8bdqvxlzw80")))

(define-public crate-stencil-template-0.1.1 (c (n "stencil-template") (v "0.1.1") (h "08zjx515kcn0cxay2mbg2d8kl741g56knwq1pfg8z9z4djaqrapn")))

