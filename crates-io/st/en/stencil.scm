(define-module (crates-io st en stencil) #:use-module (crates-io))

(define-public crate-stencil-0.1.0 (c (n "stencil") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.9") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0j3w45bpg6l9j48pix9y3riz2dw775a2nb0ci655g9kvabls9l4p")))

(define-public crate-stencil-0.2.0 (c (n "stencil") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.9") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1i6fli8z6g1v11mjagxdnmnvysjw13vb8n81y1qbznyidyy2rmxz")))

