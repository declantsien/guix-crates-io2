(define-module (crates-io st en steno-drill) #:use-module (crates-io))

(define-public crate-steno-drill-1.0.0 (c (n "steno-drill") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tui") (r "^0.17") (d #t) (k 0)))) (h "01r0b9vf8panrmm4x5g1w8pbhzvcaql7z6rci5bvi3w3srr8rc74")))

