(define-module (crates-io st en stentorian) #:use-module (crates-io))

(define-public crate-stentorian-0.1.0 (c (n "stentorian") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "components") (r "^0.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "failure") (r "^0.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "log") (r "^0.4") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "serde") (r "^1.0") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (t "i686-pc-windows-msvc") (k 0)))) (h "1lwjsprhxhmwxabfi07vw1lm71bi6f23y23w83hml2ls90r7r55z") (y #t)))

(define-public crate-stentorian-0.1.1 (c (n "stentorian") (v "0.1.1") (h "1gljn3y1js78jqrinq2c2qqlmixbqj3q3ii2ldd81j829ksg4gnz") (y #t)))

