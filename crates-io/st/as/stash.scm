(define-module (crates-io st as stash) #:use-module (crates-io))

(define-public crate-stash-0.0.1 (c (n "stash") (v "0.0.1") (h "060cmx6lz92d61aklgl0r5bd013pz66sgajiqcagsqy2mwxymjp1")))

(define-public crate-stash-0.1.0 (c (n "stash") (v "0.1.0") (h "19a5bja8aykny4wdnz27pa6qy4q1i9s9nxv31wni5wg072ch2n6b")))

(define-public crate-stash-0.1.1 (c (n "stash") (v "0.1.1") (d (list (d (n "unreachable") (r "^0.1.1") (d #t) (k 0)))) (h "0iwygws3yh2ap0j0mxvsj9maxbnd1h3lwpsybjk4pk8dcz14ikh0")))

(define-public crate-stash-0.1.2 (c (n "stash") (v "0.1.2") (d (list (d (n "unreachable") (r "^0.1.1") (d #t) (k 0)))) (h "165m3kr72sdyzis1s163dqlrbq4x8gc9f4lqw5c6xh36v6h2f7yc")))

(define-public crate-stash-0.1.3 (c (n "stash") (v "0.1.3") (d (list (d (n "unreachable") (r "^0.1.1") (d #t) (k 0)))) (h "1rgm1azfymncwqdnibdvdv9vvvvrdi3gzsvhhfdwn6kdig5l51ib")))

(define-public crate-stash-0.1.4 (c (n "stash") (v "0.1.4") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1") (d #t) (k 0)))) (h "1wsflx656x7dc7n92lnc2csfxasqnhc9bzmb3hnsgiqsw0iqr458") (f (quote (("serialization" "serde" "serde_derive"))))))

(define-public crate-stash-0.1.5 (c (n "stash") (v "0.1.5") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)))) (h "06n9gmmnsrd1621q09kxnaswkvg69clasakbw0cni5snj3jh35p7") (f (quote (("serialization" "serde" "serde_derive"))))))

