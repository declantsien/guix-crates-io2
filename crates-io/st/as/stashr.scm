(define-module (crates-io st as stashr) #:use-module (crates-io))

(define-public crate-stashr-0.1.0 (c (n "stashr") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1hk1781g5c3gd2yf6z8fagssc0l4n8mfpswah47cc7nq65r80d6v")))

(define-public crate-stashr-0.2.0 (c (n "stashr") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0p64k2drlrqrz1z46klcfmvfmij006xkfb25xsv0ah5j3kqygin1")))

(define-public crate-stashr-0.3.0 (c (n "stashr") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0r5adw49kiv24r5fwf7nv2bx6ghqql611dscxzm950dszzvdgr7b")))

(define-public crate-stashr-0.3.1 (c (n "stashr") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1liv962gcq5kcpqllhz4bl5add269i17xrwvmiwm83v252r0nw5n")))

(define-public crate-stashr-0.3.2 (c (n "stashr") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0a0g1w96chd27b5kqsylj0mn31yd94km0z8p4q91xd7skbv3p7ia")))

(define-public crate-stashr-0.3.3 (c (n "stashr") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1ar5ckl4c6gfrj537yqnn4gyfmla17jpvi3j1mmnsmh9j2dgglws")))

(define-public crate-stashr-0.4.0 (c (n "stashr") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1nhnvz486fw0mnlbra7bp9m2vz7igb60582hys87m4p3b2vghvgp")))

