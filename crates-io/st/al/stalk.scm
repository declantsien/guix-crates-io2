(define-module (crates-io st al stalk) #:use-module (crates-io))

(define-public crate-stalk-0.1.1 (c (n "stalk") (v "0.1.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "1a9pyra1ph36g7wp0x81k6v9izr5vsbn44bs0dd6q6jabwiigllv")))

