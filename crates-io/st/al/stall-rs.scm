(define-module (crates-io st al stall-rs) #:use-module (crates-io))

(define-public crate-stall-rs-0.1.3 (c (n "stall-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "rustc_version_runtime") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("suggestions" "color"))) (d #t) (k 0)))) (h "14nh2qrs7dinrdxip9v5zy1i2h9ifzb19kk1976shy86gnvn08sq") (f (quote (("default")))) (y #t)))

