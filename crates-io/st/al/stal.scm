(define-module (crates-io st al stal) #:use-module (crates-io))

(define-public crate-stal-0.1.0 (c (n "stal") (v "0.1.0") (h "0ipjc00dwxvq9imiv2d3vphf42d17abw5885p35kp833yhqb95rs")))

(define-public crate-stal-0.1.1 (c (n "stal") (v "0.1.1") (h "1hvp0l2bz6g3irbks9vnihh2y6spgm1rsp1r2dd5kdw6my41i60n")))

(define-public crate-stal-0.1.2 (c (n "stal") (v "0.1.2") (h "1las3120x44b07y0swaq3v955fa88inh0lw0v86xbnarx1aci4gb")))

