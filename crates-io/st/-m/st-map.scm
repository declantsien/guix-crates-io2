(define-module (crates-io st -m st-map) #:use-module (crates-io))

(define-public crate-st-map-0.1.0 (c (n "st-map") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1n59gm60s87mqdx2i18cy83kr1ly4x5ival9mmg7cyrqg9z00rqi")))

(define-public crate-st-map-0.1.1 (c (n "st-map") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0iagwv9hdk63aagd2yk22qzysjjpw6jgvhayxi7f0adz58pyil2w")))

(define-public crate-st-map-0.1.2 (c (n "st-map") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.2.1") (d #t) (k 0)))) (h "0fbwnwg3ly7qwjkpklmjz9lvkj572jpnwwlmg62dnbbwik44rhcz")))

(define-public crate-st-map-0.1.3 (c (n "st-map") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.2.1") (d #t) (k 0)))) (h "193nywlr20pcs0dv3nks8kd6ybcmj53kcckx2zjcb1nplwmd0nhs")))

(define-public crate-st-map-0.1.4 (c (n "st-map") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.2.1") (d #t) (k 0)))) (h "1l820pisfi134v3wy0na480wl7rf69kgxzvmgc560ngqb0xb3biw")))

(define-public crate-st-map-0.1.5 (c (n "st-map") (v "0.1.5") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.2.2") (d #t) (k 0)))) (h "17w94hkwhlrgq4mr0rghhigw3is6avxiilvpc3bwnxkcxy0h838c") (y #t)))

(define-public crate-st-map-0.1.6 (c (n "st-map") (v "0.1.6") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.2.3") (d #t) (k 0)))) (h "0kqc1jgkh12rzl928a7h4s8sshggiw8775xxj9rkpxzm3lx9z75w")))

(define-public crate-st-map-0.1.7 (c (n "st-map") (v "0.1.7") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.2.4") (d #t) (k 0)))) (h "1v3wxv76pwfy2wrg07n5wz073mn2723dd216hmmqkvlpymc69p9d") (y #t)))

(define-public crate-st-map-0.1.8 (c (n "st-map") (v "0.1.8") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.2.5") (d #t) (k 0)))) (h "1vdmg8sr3iynkblcd97pl4yslisdnn7lgm4dlpab0xph6lc8k7gh")))

(define-public crate-st-map-0.2.0 (c (n "st-map") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0iwhbb0cf8zh9x5hmwc0l2iam1rl1h3ciq3asxbgk8g59g8xalpk")))

(define-public crate-st-map-0.2.1 (c (n "st-map") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.3.1") (d #t) (k 0)))) (h "0j4l0s7yhrvk05p5h1i068dbf9f377gil8135i0f4gdia52v6n5s")))

(define-public crate-st-map-0.2.2 (c (n "st-map") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.3.2") (d #t) (k 0)))) (h "0is2zfzfp0djbgmvsak0px07gz8fby3rvz080b3va09q791kkd89")))

(define-public crate-st-map-0.2.3 (c (n "st-map") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "static-map-macro") (r "^0.3.2") (d #t) (k 0)))) (h "18c9d9x6kaa70mnn30zssyprqj37vyr70ls98s9l0ff8bi75r2la")))

