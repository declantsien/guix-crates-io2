(define-module (crates-io st ay stayalive) #:use-module (crates-io))

(define-public crate-stayalive-0.1.0 (c (n "stayalive") (v "0.1.0") (d (list (d (n "threadpool") (r "^1.7.0") (d #t) (k 0)))) (h "1xcwjvlfq2w3cz5i80lva7j29h219dvz1yrna203l3brhpv9sn19")))

(define-public crate-stayalive-0.2.0 (c (n "stayalive") (v "0.2.0") (d (list (d (n "threadpool") (r "^1.7.0") (d #t) (k 0)))) (h "1x4nqdi30qz7zv50wdj23dn3li2rc6b2ig1jrklycig8i0q7izkn")))

(define-public crate-stayalive-0.2.1 (c (n "stayalive") (v "0.2.1") (d (list (d (n "threadpool") (r "^1.7.0") (d #t) (k 0)))) (h "09pgibm4k8llmvs1cziv089z1gjz8zxkzqk3wxrp28jbhh4bfnh3")))

(define-public crate-stayalive-0.3.0 (c (n "stayalive") (v "0.3.0") (d (list (d (n "threadpool") (r "^1.7.0") (d #t) (k 0)))) (h "0qlblpnwfl32q7xfa2dfay7bmpayjji1j6mmddv1l64js360pxxs")))

(define-public crate-stayalive-0.3.1 (c (n "stayalive") (v "0.3.1") (d (list (d (n "threadpool") (r "^1.7.0") (d #t) (k 0)))) (h "0nhzajqnk7sdk7x7jisg0ss0vzvh8d4brb8vxbgma7n6wsz87k7r")))

(define-public crate-stayalive-0.3.2 (c (n "stayalive") (v "0.3.2") (d (list (d (n "threadpool") (r "^1.7.0") (d #t) (k 0)))) (h "0fhbpfif1f47x96fx3456zv60d4rg0ymb2i4mzi75pm17pxk0x0b")))

(define-public crate-stayalive-0.4.0 (c (n "stayalive") (v "0.4.0") (d (list (d (n "threadpool") (r "^1.7.0") (d #t) (k 0)))) (h "0c9dwdc38gszjf05firf42klh8wkm0xlqg85k2b60gcwwd1886pq")))

(define-public crate-stayalive-0.5.0 (c (n "stayalive") (v "0.5.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.0") (d #t) (k 0)))) (h "0y33agppncf0yasadlzraki2vpkzl3vlyfv1s8qclz1p1m8xpz07")))

(define-public crate-stayalive-0.5.1 (c (n "stayalive") (v "0.5.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.0") (d #t) (k 0)))) (h "1qk5s3r3szw8inhvpg8w0vmk0ic95qwp01qd4ilyw71w1c54cyib") (y #t)))

