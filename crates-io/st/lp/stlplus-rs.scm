(define-module (crates-io st lp stlplus-rs) #:use-module (crates-io))

(define-public crate-stlplus-rs-0.1.0 (c (n "stlplus-rs") (v "0.1.0") (d (list (d (n "arrow2") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vrfnllwnpgkvgalcahxladwxnj19pxwcm3xnmad08i0jlkac99j")))

(define-public crate-stlplus-rs-0.1.1 (c (n "stlplus-rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1idzryrkw610n3gmrp4b6n09vnzsp98spx4a4vjqqb4spkhcngkj")))

(define-public crate-stlplus-rs-0.1.2 (c (n "stlplus-rs") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02v6dvsv8zzdc8ywl3qk8dfsi7z6hn8vi7giccj2pz4lhn98njhi")))

(define-public crate-stlplus-rs-0.1.3 (c (n "stlplus-rs") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hg3y5yknrkdxg20pvxvsj7pg6z0s74xjallrgqn8x461rw53bp4")))

