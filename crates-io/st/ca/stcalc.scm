(define-module (crates-io st ca stcalc) #:use-module (crates-io))

(define-public crate-stcalc-0.1.0 (c (n "stcalc") (v "0.1.0") (h "1wmchh0hr1hn4n35rkr8wk38qw0khxcw29myi5133hwkgl89mmgs") (y #t)))

(define-public crate-stcalc-0.1.1 (c (n "stcalc") (v "0.1.1") (h "07m2c8jbi803w2giw9987jn80ihv1m7hygk02p5b3bg6n1815vxx")))

(define-public crate-stcalc-0.1.2 (c (n "stcalc") (v "0.1.2") (h "1l0aydzan1qk8arhhw4kg0cnzhkmzhk8325lksr4xfqpkmy2q4xm")))

(define-public crate-stcalc-1.0.0 (c (n "stcalc") (v "1.0.0") (h "0mi9zg1sr5b9jylscb7zk57cxwwbqfap1i8wrfkwqdyjg9kb7rd2")))

(define-public crate-stcalc-1.0.1 (c (n "stcalc") (v "1.0.1") (h "0kj4pn4n0m2ra7xa1qgmmdx1zw2fw8n1b6ry6qzqvpp5m5ym1zip")))

