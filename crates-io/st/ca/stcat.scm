(define-module (crates-io st ca stcat) #:use-module (crates-io))

(define-public crate-stcat-0.1.0 (c (n "stcat") (v "0.1.0") (d (list (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.5.0") (d #t) (k 0)))) (h "1ml33b321kjl5byhhb2mhg5rlbdga14jbzfaw0gl6z5mpz66ww3q")))

(define-public crate-stcat-0.2.0 (c (n "stcat") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.3.3") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "sloggers") (r "^0.2.8") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.6.2") (d #t) (k 0)))) (h "0rpbh34bvw403yabfv6kvqvw607bpsbkwnynx2xs12agqfidq3q6") (y #t)))

(define-public crate-stcat-0.2.1 (c (n "stcat") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.3.3") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "sloggers") (r "^0.2.8") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.6.2") (d #t) (k 0)))) (h "0a71qk08z6pyfxxcjb7dbhxaxlij86yvgv7idzgabw0gn8zas8pw") (y #t)))

(define-public crate-stcat-0.2.2 (c (n "stcat") (v "0.2.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.3.3") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "sloggers") (r "^0.2.8") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.6.2") (d #t) (k 0)))) (h "1bnykckdfvd5mxnz0rpzb896l841y7r472i9w9cjsbxhgij7mbis")))

(define-public crate-stcat-0.2.3 (c (n "stcat") (v "0.2.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.3.3") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "sloggers") (r "^0.2.8") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.6.2") (d #t) (k 0)))) (h "1kddbggd8slkikxgr11m8chlx23r06hkqn4gl3cp8wb939wnv3bm")))

