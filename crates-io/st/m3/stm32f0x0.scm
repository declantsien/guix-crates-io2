(define-module (crates-io st m3 stm32f0x0) #:use-module (crates-io))

(define-public crate-stm32f0x0-0.1.0 (c (n "stm32f0x0") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1x4f8fzylvhj8vw4cswqvzpaa7adznrcqv0pjiqfg145qk5sfyis") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f0x0-0.1.1 (c (n "stm32f0x0") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1k5jjypwn3arr8hhgkajwnw4aq33iw26k3qn5pybxnfpbjpxx0vn") (f (quote (("rt" "cortex-m-rt"))))))

