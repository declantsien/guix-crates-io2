(define-module (crates-io st m3 stm32f303xe) #:use-module (crates-io))

(define-public crate-stm32f303xe-0.1.0 (c (n "stm32f303xe") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (f (quote ("cm7-r0p1"))) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1s6zdndkdw8xi4dlsjfmyzz1fsv5m55gvs5i9kfva183xnd9yw7q") (f (quote (("rt" "cortex-m-rt"))))))

