(define-module (crates-io st m3 stm32h5) #:use-module (crates-io))

(define-public crate-stm32h5-0.0.0 (c (n "stm32h5") (v "0.0.0") (h "1jkrrnkhr6n0j53vq198g14xpspqv1yw97d7a0vy5a9mdda945qs")))

(define-public crate-stm32h5-0.15.1 (c (n "stm32h5") (v "0.15.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "06gzf9s1502fjqn0qr6d1vkna5d2rpj95q3bfcqs85p8n2jks0cl") (f (quote (("stm32h573") ("stm32h563") ("stm32h562") ("stm32h503") ("rt" "cortex-m-rt/device") ("default" "critical-section" "rt")))) (s 2) (e (quote (("atomics" "dep:portable-atomic")))) (r "1.65")))

