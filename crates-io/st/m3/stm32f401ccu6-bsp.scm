(define-module (crates-io st m3 stm32f401ccu6-bsp) #:use-module (crates-io))

(define-public crate-stm32f401ccu6-bsp-0.1.0 (c (n "stm32f401ccu6-bsp") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "stm32f4") (r "^0.10.0") (f (quote ("stm32f401"))) (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8.2") (f (quote ("stm32f401" "rt"))) (d #t) (k 0)))) (h "17qag95l9jqbzdc03mpnv4gpma7zkrqb0illy1kgyrkd446m56jn")))

(define-public crate-stm32f401ccu6-bsp-0.1.1 (c (n "stm32f401ccu6-bsp") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "panic-rtt-core") (r "^0.1.1") (d #t) (k 2)) (d (n "ssd1306") (r "^0.3.1") (d #t) (k 2)) (d (n "stm32f4") (r "^0.10.0") (f (quote ("stm32f401"))) (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8.2") (f (quote ("stm32f401" "rt"))) (d #t) (k 0)))) (h "0dh9ac5m53xjpv1bg3ryfmrk8z121b5q1h54xwq80a77bm8ad5px")))

