(define-module (crates-io st m3 stm32f41x) #:use-module (crates-io))

(define-public crate-stm32f41x-0.1.0 (c (n "stm32f41x") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "14bbghi29pfb4cz3irngdylx3ni79wgdnx7mf89xvzpvg3ykfvij") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f41x-0.1.1 (c (n "stm32f41x") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0jhlr5c37yy1k9ps7p81c5x9wsjbg2ngbdp7ibgsa1pj0039vphj") (f (quote (("rt" "cortex-m-rt"))))))

