(define-module (crates-io st m3 stm32_i2s_v12x) #:use-module (crates-io))

(define-public crate-stm32_i2s_v12x-0.1.0 (c (n "stm32_i2s_v12x") (v "0.1.0") (d (list (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 2)) (d (n "panic-rtt-target") (r "^0.1.1") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "rtt-target") (r "^0.3.0") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0y850r1h2fyyk43qvldnba2nr3nkhhrrakmhr3n8vgfxfrs549k1")))

(define-public crate-stm32_i2s_v12x-0.2.0 (c (n "stm32_i2s_v12x") (v "0.2.0") (d (list (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 2)) (d (n "panic-rtt-target") (r "^0.1.1") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "rtt-target") (r "^0.3.0") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1bgl6wwjgyga4am9cykn5nkxynjvmsmgjlydfca0azsaja5fjavx")))

(define-public crate-stm32_i2s_v12x-0.3.0 (c (n "stm32_i2s_v12x") (v "0.3.0") (d (list (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 2)) (d (n "panic-rtt-target") (r "^0.1.1") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "rtt-target") (r "^0.3.0") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "stm32f4xx-hal") (r "^0.13.0") (f (quote ("stm32f412" "rt"))) (d #t) (k 2)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "046wx2d0jspnahb87hz4cfiiaqyh1q36dgm0h007crfs47y63540")))

(define-public crate-stm32_i2s_v12x-0.4.0 (c (n "stm32_i2s_v12x") (v "0.4.0") (d (list (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "08jqps79rrykhim9bbiiwhcvv0rr01wwni90ksqfl4nir67l5ml9")))

(define-public crate-stm32_i2s_v12x-0.5.0 (c (n "stm32_i2s_v12x") (v "0.5.0") (d (list (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ynz1fzbvdjg3kar4f70ncf4v27p72fj4gvbq9s29qxqyq8731a2")))

(define-public crate-stm32_i2s_v12x-0.5.1 (c (n "stm32_i2s_v12x") (v "0.5.1") (d (list (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0imrsjcglp1iwy56kvw9garabqxp9mzfdgpviwxy89gqf74p2x5x")))

