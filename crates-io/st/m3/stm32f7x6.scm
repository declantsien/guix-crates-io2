(define-module (crates-io st m3 stm32f7x6) #:use-module (crates-io))

(define-public crate-stm32f7x6-0.1.0 (c (n "stm32f7x6") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1d0xk6rzq3xm6qq8bfisihks5xqm1n3s2m1yvmjfsvwa2px2qc4n") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f7x6-0.2.0 (c (n "stm32f7x6") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "12pw5wfd48x88b2y4cg0x8k2yzxfdbd67njym7wqc5i5zi5jbgpq") (f (quote (("rt" "cortex-m-rt"))))))

