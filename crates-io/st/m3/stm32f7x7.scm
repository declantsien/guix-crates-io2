(define-module (crates-io st m3 stm32f7x7) #:use-module (crates-io))

(define-public crate-stm32f7x7-0.1.0 (c (n "stm32f7x7") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0abndry47kjgaw54pkn4p9c7l9scf3gf9qg4ikgkmm1ijjx2dxm3") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f7x7-0.2.0 (c (n "stm32f7x7") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1bmiy2lvlx71sq0xsnsjv2dhs91bv3wkjn6fnc1bqsrd8qxjzg8f") (f (quote (("rt" "cortex-m-rt"))))))

