(define-module (crates-io st m3 stm32h7xx) #:use-module (crates-io))

(define-public crate-stm32h7xx-0.1.0 (c (n "stm32h7xx") (v "0.1.0") (d (list (d (n "alloc-cortex-m") (r "^0.3.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.0") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.3.0") (d #t) (k 0)))) (h "0jzkkxz5qc0d7p015dkzqqj9hr5nqkcigd5ggjj75a84p74fbalm")))

(define-public crate-stm32h7xx-0.2.0 (c (n "stm32h7xx") (v "0.2.0") (d (list (d (n "alloc-cortex-m") (r "^0.3.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.0") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.3.0") (d #t) (k 0)) (d (n "stm32h7") (r "^0.2.0") (f (quote ("stm32h7x3"))) (d #t) (k 0)))) (h "0p686n51j2gwknpv5kw98zil2yi5n7jyc0p7hxzq34mma99x5bvc")))

