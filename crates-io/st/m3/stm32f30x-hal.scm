(define-module (crates-io st m3 stm32f30x-hal) #:use-module (crates-io))

(define-public crate-stm32f30x-hal-0.1.0 (c (n "stm32f30x-hal") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 0)))) (h "1xfxsz9ksy2qq3c7pladyhhid4bip6crvd537aamgsn0slws1a7n")))

(define-public crate-stm32f30x-hal-0.1.1 (c (n "stm32f30x-hal") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 0)))) (h "09nz9spr63mqqvc8m73d3yf8mfncc6cy0ccp4mjpdfbazpbcv8b1") (f (quote (("rt" "stm32f30x/rt"))))))

(define-public crate-stm32f30x-hal-0.1.2 (c (n "stm32f30x-hal") (v "0.1.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 0)))) (h "117ysc91bpdrh0j7gn8pwx89a6pm44v03l8cl9blh9sf626bpwjh") (f (quote (("rt" "stm32f30x/rt"))))))

(define-public crate-stm32f30x-hal-0.2.0 (c (n "stm32f30x-hal") (v "0.2.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.7.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0b28swpysqs6ajjzbnnpva3zc7rsg4gjnqb69yrzg585nmhkchbb") (f (quote (("rt" "stm32f30x/rt"))))))

