(define-module (crates-io st m3 stm32wl) #:use-module (crates-io))

(define-public crate-stm32wl-0.12.1 (c (n "stm32wl") (v "0.12.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1vmbag8551q0a032nv602d8pxq1c3saq6fbcqpgixgrbdgp2znw3") (f (quote (("stm32wle5") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32wl-0.13.0 (c (n "stm32wl") (v "0.13.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1hw941nw83h1v8hbxqixxamhx5am6fvg240s6l4g0lh2g1b686rj") (f (quote (("stm32wle5") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32wl-0.14.0 (c (n "stm32wl") (v "0.14.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xz8apbclyw7m528dx8aqn033c1njb1k3yk0pg20s8w4fb55qhdr") (f (quote (("stm32wle5") ("stm32wl5x_cm4") ("stm32wl5x_cm0p") ("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-stm32wl-0.15.0 (c (n "stm32wl") (v "0.15.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "06z58ik1hiazzqd0v5l8kh8p2f8n7n4sbkd3sfvw1jw8hk47jf31") (f (quote (("stm32wle5") ("stm32wl5x_cm4") ("stm32wl5x_cm0p") ("rt" "cortex-m-rt/device") ("default" "rt")))) (y #t)))

(define-public crate-stm32wl-0.15.1 (c (n "stm32wl") (v "0.15.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "13w1apnv3xmyx6xfb4yhww1kgi01i9i1fgx09jnmkwz7bv7zbha1") (f (quote (("stm32wle5") ("stm32wl5x_cm4") ("stm32wl5x_cm0p") ("rt" "cortex-m-rt/device") ("default" "rt"))))))

