(define-module (crates-io st m3 stm32l4-stm32hal) #:use-module (crates-io))

(define-public crate-stm32l4-stm32hal-0.13.0 (c (n "stm32l4-stm32hal") (v "0.13.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "09bwnxspdinflsjbxmbmmsw5z7ilh0cixva30xpw5wkbw8s2bqjb") (f (quote (("stm32l4x6") ("stm32l4x5") ("stm32l4x3") ("stm32l4x2") ("stm32l4x1") ("stm32l4r9") ("stm32l412") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32l4-stm32hal-0.13.1 (c (n "stm32l4-stm32hal") (v "0.13.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "01xansgvzzs4iyn6x3rnbs3a3cb27c642hwpyfy9jqf8cq511zdj") (f (quote (("stm32l4x6") ("stm32l4x5") ("stm32l4x3") ("stm32l4x2") ("stm32l4x1") ("stm32l4r9") ("stm32l412") ("rt" "cortex-m-rt/device") ("default"))))))

