(define-module (crates-io st m3 stm32l5) #:use-module (crates-io))

(define-public crate-stm32l5-0.11.0 (c (n "stm32l5") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0i27f0rrv41pkllzdb9xd2a6321vn3a9ypzqgk0fbwfalazxancx") (f (quote (("stm32l562") ("stm32l552") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32l5-0.12.0 (c (n "stm32l5") (v "0.12.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1gwdkbriabssdq72sgs2657q53dvmincf7z4f3scx3qphcdn7slw") (f (quote (("stm32l562") ("stm32l552") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32l5-0.12.1 (c (n "stm32l5") (v "0.12.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "05mhbnc3vdcx2zcy58h4g9l0xy75p2jc0ngjsm6l93xkwwxhwgpi") (f (quote (("stm32l562") ("stm32l552") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32l5-0.13.0 (c (n "stm32l5") (v "0.13.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0p99402v425slb6dxgswa54kl7f8nib1xjz9qdyhbnl2nchcdr8q") (f (quote (("stm32l562") ("stm32l552") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32l5-0.14.0 (c (n "stm32l5") (v "0.14.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1mqyrm094h0mm0k5rmr8s9sl77zwfb6kbmbahvhbjfprb8ixvmvs") (f (quote (("stm32l562") ("stm32l552") ("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-stm32l5-0.15.0 (c (n "stm32l5") (v "0.15.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0zqcn1hc9liaxnd2k9m2a49l7jwzd54b5avpva1rk2vqcr5zn5dw") (f (quote (("stm32l562") ("stm32l552") ("rt" "cortex-m-rt/device") ("default" "rt")))) (y #t)))

(define-public crate-stm32l5-0.15.1 (c (n "stm32l5") (v "0.15.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ihgm07v2aqxnmkiimw4qvc2gdks1rdiffw6zr8wjpy05iygp36f") (f (quote (("stm32l562") ("stm32l552") ("rt" "cortex-m-rt/device") ("default" "rt"))))))

