(define-module (crates-io st m3 stm32wb-stm32hal) #:use-module (crates-io))

(define-public crate-stm32wb-stm32hal-0.13.0 (c (n "stm32wb-stm32hal") (v "0.13.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1qs3s9nwbzi0g4zm4gq0v5vz875j12dzrw8skr2vm802114i15i9") (f (quote (("stm32wb55") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32wb-stm32hal-0.13.1 (c (n "stm32wb-stm32hal") (v "0.13.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0r3b5qmbnsal22jjf1piwdzqbhgsy1bk3b2kl82wqsasywsiki79") (f (quote (("stm32wb55") ("rt" "cortex-m-rt/device") ("default"))))))

