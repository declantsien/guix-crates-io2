(define-module (crates-io st m3 stm32-device-signature) #:use-module (crates-io))

(define-public crate-stm32-device-signature-0.1.0 (c (n "stm32-device-signature") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)))) (h "17kk11vwyczd24gy1nnkasj48alia04bc2kim9rdadrfb7kxpdjn") (f (quote (("stm32l4") ("stm32l0") ("stm32g0") ("stm32f4") ("stm32f3") ("stm32f1") ("stm32f0"))))))

(define-public crate-stm32-device-signature-0.2.0 (c (n "stm32-device-signature") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)))) (h "1dhmf1l90md5iypya3nq23sab1gm38g44fn45zy5akw7p0i91avb") (f (quote (("stm32l4") ("stm32l0") ("stm32g0") ("stm32f4") ("stm32f3") ("stm32f1") ("stm32f0"))))))

(define-public crate-stm32-device-signature-0.2.1 (c (n "stm32-device-signature") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)))) (h "0wi3gp52ndc6b48c2j0223y498ws1ql1v5gw3vfxwy87b277vzm6") (f (quote (("stm32wb5x") ("stm32l4") ("stm32l0") ("stm32g0") ("stm32f4") ("stm32f3") ("stm32f1") ("stm32f0"))))))

(define-public crate-stm32-device-signature-0.3.0 (c (n "stm32-device-signature") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)))) (h "05d6sw8vkylqpyhiy0k3nshp45sfwyasl26fmjcnlbkbdcc4hp3w") (f (quote (("stm32wb5x") ("stm32l4") ("stm32l0") ("stm32g0") ("stm32f4") ("stm32f3") ("stm32f2") ("stm32f1") ("stm32f0"))))))

(define-public crate-stm32-device-signature-0.3.1 (c (n "stm32-device-signature") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)))) (h "08hm8cnazk11vj2p0yb3kwc8wfzqayd0ifr524y9ds9b6cijamgv") (f (quote (("stm32wb5x") ("stm32l4") ("stm32l0") ("stm32g0") ("stm32f73x") ("stm32f72x") ("stm32f4") ("stm32f3") ("stm32f2") ("stm32f1") ("stm32f0"))))))

(define-public crate-stm32-device-signature-0.3.2 (c (n "stm32-device-signature") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)))) (h "0y339nx2mrvpdgksilp48myikhvz6djj7mnng8qpdzjwpw8vwlfr") (f (quote (("stm32wb5x") ("stm32l4") ("stm32l0") ("stm32h7bx") ("stm32h7ax") ("stm32h75x") ("stm32h74x") ("stm32h73x") ("stm32h72x") ("stm32g0") ("stm32f73x") ("stm32f72x") ("stm32f4") ("stm32f3") ("stm32f2") ("stm32f1") ("stm32f0"))))))

(define-public crate-stm32-device-signature-0.3.3 (c (n "stm32-device-signature") (v "0.3.3") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)))) (h "1w5wjm0zydpyy3cms9f3gf8mf0rhgqa6jf2v5m3cr91y1c3chqp2") (f (quote (("stm32wb5x") ("stm32l4") ("stm32l0") ("stm32h7bx") ("stm32h7ax") ("stm32h75x") ("stm32h74x") ("stm32h73x") ("stm32h72x") ("stm32g0") ("stm32f77x") ("stm32f76x") ("stm32f73x") ("stm32f72x") ("stm32f4") ("stm32f3") ("stm32f2") ("stm32f1") ("stm32f0"))))))

