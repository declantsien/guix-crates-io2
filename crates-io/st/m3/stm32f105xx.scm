(define-module (crates-io st m3 stm32f105xx) #:use-module (crates-io))

(define-public crate-stm32f105xx-0.1.0 (c (n "stm32f105xx") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "15i05w39aayrrbma2q5iqzyp0q116r9afxbxsix5rk695d8cb9j6") (f (quote (("rt" "cortex-m-rt"))))))

