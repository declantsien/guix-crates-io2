(define-module (crates-io st m3 stm32f429-hal) #:use-module (crates-io))

(define-public crate-stm32f429-hal-0.0.0 (c (n "stm32f429-hal") (v "0.0.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f429") (r "^0.3.0") (d #t) (k 0)))) (h "0vpfnbnm11vy0nh72i4yw90qmb4kjrppxr4dma4q8brws0b37inv") (f (quote (("rt" "stm32f429/rt")))) (y #t)))

(define-public crate-stm32f429-hal-0.0.1 (c (n "stm32f429-hal") (v "0.0.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f429") (r "^0.4.0") (d #t) (k 0)))) (h "0kn237k0ajbw3h4h7lh5mll9w6flsn741m26kg1r082giyvmk400") (f (quote (("rt" "stm32f429/rt")))) (y #t)))

(define-public crate-stm32f429-hal-0.0.2 (c (n "stm32f429-hal") (v "0.0.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f429") (r "^0.4.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0vk7qwhrdypcfsqgx0dxswwcx7nc76zahxw97cqr7ylgjgg1ad4a") (f (quote (("rt" "stm32f429/rt")))) (y #t)))

(define-public crate-stm32f429-hal-0.1.0 (c (n "stm32f429-hal") (v "0.1.0") (d (list (d (n "cast") (r "^0.2") (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)) (d (n "stm32f429") (r "^0.5") (d #t) (k 0)) (d (n "void") (r "^1.0") (k 0)))) (h "0ip7qfnl6z5qcby8p5xrkjyymbli257l871nx488z2f60dpcd3ci") (f (quote (("rt" "stm32f429/rt")))) (y #t)))

(define-public crate-stm32f429-hal-0.1.1 (c (n "stm32f429-hal") (v "0.1.1") (d (list (d (n "cast") (r "^0.2") (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)) (d (n "stm32f429") (r "^0.6") (d #t) (k 0)) (d (n "void") (r "^1.0") (k 0)))) (h "1wrif6r549cw6vyyiwrd5js4rb4zc9nsrdrv4mllad3klg0js9g2") (f (quote (("rt" "stm32f429/rt")))) (y #t)))

