(define-module (crates-io st m3 stm32l0x1) #:use-module (crates-io))

(define-public crate-stm32l0x1-0.0.1 (c (n "stm32l0x1") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1m56ac7f9zc6mj6sv934584s80ahn33i8hjhi1q3avqh9insmvmr")))

(define-public crate-stm32l0x1-0.13.1 (c (n "stm32l0x1") (v "0.13.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1jc2fj25ns00l6q6zhigxax34v1x1lk3n8gjhkjvwrysvx6zvhc9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-stm32l0x1-0.13.2 (c (n "stm32l0x1") (v "0.13.2") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0dpyv4zid8dwvxi3kv7jxk345rl91z7jzakzhg55xzyzcr27hmbg") (f (quote (("rt" "cortex-m-rt/device"))))))

