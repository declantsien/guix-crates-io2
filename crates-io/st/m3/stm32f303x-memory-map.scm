(define-module (crates-io st m3 stm32f303x-memory-map) #:use-module (crates-io))

(define-public crate-stm32f303x-memory-map-0.1.0 (c (n "stm32f303x-memory-map") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.2.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0q6lmvi3q6mmj2g4jqkdhqh3as96svaczjz75s80j5qy03xfqhnp")))

(define-public crate-stm32f303x-memory-map-0.1.1 (c (n "stm32f303x-memory-map") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.2.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "02ydn59iwnxnycmxcfc7d0wnhh91d6nq922m236g7bc1az8wz8mz")))

