(define-module (crates-io st m3 stm32f303cct6-robotdyn-bsp) #:use-module (crates-io))

(define-public crate-stm32f303cct6-robotdyn-bsp-0.1.0 (c (n "stm32f303cct6-robotdyn-bsp") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "stm32f3") (r "^0.10.0") (f (quote ("stm32f303"))) (d #t) (k 0)) (d (n "stm32f3xx-hal") (r "^0.4.3") (f (quote ("stm32f303" "rt"))) (d #t) (k 0)))) (h "1asivp32a3f45rrr2kipr0970vafyxxz0rnjypd4z4q4r66lf8lh")))

(define-public crate-stm32f303cct6-robotdyn-bsp-0.1.1 (c (n "stm32f303cct6-robotdyn-bsp") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "stm32f3") (r "^0.10.0") (f (quote ("stm32f303"))) (d #t) (k 0)) (d (n "stm32f3xx-hal") (r "^0.4.3") (f (quote ("stm32f303" "rt"))) (d #t) (k 0)))) (h "026xsyfx0rhizigvndcw3ivnbcsiqm5v5hp3y2z5igrv2d6vyqir")))

