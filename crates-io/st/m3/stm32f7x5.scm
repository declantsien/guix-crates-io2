(define-module (crates-io st m3 stm32f7x5) #:use-module (crates-io))

(define-public crate-stm32f7x5-0.1.0 (c (n "stm32f7x5") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0951gfbbiwd67yanj8ifllkiif135qk4hx70cf8098kv1nkl716k") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f7x5-0.2.0 (c (n "stm32f7x5") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1xfmrs1vw1l3lm9icdvlqfsfhy8h1vr8fgk1dj19qgya3ldm2jfj") (f (quote (("rt" "cortex-m-rt"))))))

