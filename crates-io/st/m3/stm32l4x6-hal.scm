(define-module (crates-io st m3 stm32l4x6-hal) #:use-module (crates-io))

(define-public crate-stm32l4x6-hal-0.0.1 (c (n "stm32l4x6-hal") (v "0.0.1") (d (list (d (n "cast") (r "^0") (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32l4x6") (r "^0") (d #t) (k 0)))) (h "0ncwvykkfchdj9vpjkc3825yqbhk3d8ssfmmf1p7mlw87syj83sf") (f (quote (("rt" "stm32l4x6/rt") ("STM32L476VG"))))))

(define-public crate-stm32l4x6-hal-0.0.2 (c (n "stm32l4x6-hal") (v "0.0.2") (d (list (d (n "cast") (r "^0") (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32l4x6") (r "^0") (d #t) (k 0)))) (h "1zinvyskyx3fmjacv50ggdhjwq1x3hldmbddas8qhqzdb9k87dnd") (f (quote (("rt" "stm32l4x6/rt") ("STM32L476VG"))))))

(define-public crate-stm32l4x6-hal-0.0.3 (c (n "stm32l4x6-hal") (v "0.0.3") (d (list (d (n "cast") (r "^0") (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32l4x6") (r "^0") (d #t) (k 0)))) (h "1d63s4a3h66x9scns9xqlfslbfway79qx92d61mkvjm55qfpks1h") (f (quote (("rt" "stm32l4x6/rt") ("STM32L476VG"))))))

(define-public crate-stm32l4x6-hal-0.0.4 (c (n "stm32l4x6-hal") (v "0.0.4") (d (list (d (n "cast") (r "^0") (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32l4x6") (r "^0") (d #t) (k 0)))) (h "1xnwhkf6anwnq768nxbqncbqcdsyhf09xix3wsn83qas3z7x4vyl") (f (quote (("rt" "stm32l4x6/rt") ("STM32L476VG"))))))

(define-public crate-stm32l4x6-hal-0.0.5 (c (n "stm32l4x6-hal") (v "0.0.5") (d (list (d (n "cast") (r "^0") (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32l4x6") (r "^0") (d #t) (k 0)))) (h "138v7843r8aj815vhknr5c4nhnzmy3mfb2zig11byphcbvncj0v3") (f (quote (("rt" "stm32l4x6/rt") ("STM32L476VG"))))))

(define-public crate-stm32l4x6-hal-0.0.6 (c (n "stm32l4x6-hal") (v "0.0.6") (d (list (d (n "cast") (r "^0") (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32l4x6") (r "^0") (d #t) (k 0)))) (h "12pg20by5r6kcfqbpw6yv3ci26fhs2hvm1c6saqf8yrhk0x0s126") (f (quote (("rt" "stm32l4x6/rt") ("STM32L476VG"))))))

(define-public crate-stm32l4x6-hal-0.0.7 (c (n "stm32l4x6-hal") (v "0.0.7") (d (list (d (n "cast") (r "^0") (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32l4x6") (r "^0") (d #t) (k 0)))) (h "0l176dpmfwnkndj7yw9c28z2pcajzg6b9862njkc93919sa6hdb1") (f (quote (("rt" "stm32l4x6/rt") ("STM32L476VG"))))))

(define-public crate-stm32l4x6-hal-0.1.0 (c (n "stm32l4x6-hal") (v "0.1.0") (d (list (d (n "cast") (r "^0.2") (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)) (d (n "stm32l4x6") (r "^0.5") (d #t) (k 0)) (d (n "void") (r "^1") (k 0)))) (h "1msyx57s9ajngaz2yb4kxkfk94h0j3nl2j6palcp03ch2x2qr4z0") (f (quote (("rt" "stm32l4x6/rt") ("STM32L496AG") ("STM32L476VG"))))))

