(define-module (crates-io st m3 stm32-bootloader-client) #:use-module (crates-io))

(define-public crate-stm32-bootloader-client-0.1.0 (c (n "stm32-bootloader-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "defmt") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "mcp2221") (r "^0.1.0") (d #t) (k 2)))) (h "1qah1kd25l9gmv339d75s9nj9vwhg6zld8s4rgkfdjzacinsvh3z") (f (quote (("std") ("default" "std"))))))

