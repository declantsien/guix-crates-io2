(define-module (crates-io st m3 stm32-hal) #:use-module (crates-io))

(define-public crate-stm32-hal-0.1.1-alpha.0 (c (n "stm32-hal") (v "0.1.1-alpha.0") (d (list (d (n "stm32f103xx") (r "^0.7.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "007khy8qjb8fyh32k7dqxfvz3pwd7kvinv3s6wm1a68ybd7dfgiw") (f (quote (("use-stm32f103xx" "stm32f103xx" "vcell"))))))

(define-public crate-stm32-hal-0.2.0 (c (n "stm32-hal") (v "0.2.0") (d (list (d (n "stm32f103xx") (r "^0.7.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0yi90klfr44kfi80202x2jyn9h41h24wv0q6kiqc0rrvilhwjl72") (f (quote (("use-stm32f103xx" "stm32f103xx" "vcell"))))))

