(define-module (crates-io st m3 stm32f429) #:use-module (crates-io))

(define-public crate-stm32f429-0.0.1 (c (n "stm32f429") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0c8m1zscwfj4v1q9fx26j4s09r98l70bb4kl3vs06g6sw4fi4m8m") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.0.2 (c (n "stm32f429") (v "0.0.2") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1xwj1lz8fhaq7rap004mfqzd8z01913bxcr1zmwg5a9g4fxgr0l5") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.1.0 (c (n "stm32f429") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "08sfwicf1knld9cbfqg0bjbpa64l49hzxmc4ck1k86iymcm07ih4") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.2.0 (c (n "stm32f429") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1s8iamlqhkx8zx2dcwk0j1vglrhq05ym70291xxv5ngm3ddajzcs") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.2.1 (c (n "stm32f429") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "12r28pxkkwkh14wvvhmh853hsyx58rrx8y8ypad64s8dvgfck67i") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.2.2 (c (n "stm32f429") (v "0.2.2") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0wlgkpriwyycr7fprn79j6ls3lvpg133p7r9n37mwjfn4pk6hdnq") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.2.3 (c (n "stm32f429") (v "0.2.3") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0cya8d6kmkvaf9jllnp008i0hd4khyndgx1x8g3hv0cy2jrkhgvz") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.2.5 (c (n "stm32f429") (v "0.2.5") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "103frz6vcp3z0bqhwnjrj7pk9b0awvg1wkaj29jzw7j18lnsxrk1") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.2.6 (c (n "stm32f429") (v "0.2.6") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "11kall0d9g6iq8m0l1943skq3800l6pp6v5iyxsi3vx2i0nvpa33") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.2.7 (c (n "stm32f429") (v "0.2.7") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "07sjn3srzbn9lbrlivm58d2ch4cahh4cjh8pg4i21whmvffidw6b") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.2.8 (c (n "stm32f429") (v "0.2.8") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1b1ywkzmfsp9adby24fzbw8bzvfyn41d39v6ghis8da4z8alzbk0") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.3.0 (c (n "stm32f429") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1bva0albjpd7vmxid82cyylmxpllgqxbgnrrr09m8m0qlw1d8isn") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.4.0 (c (n "stm32f429") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1c9i1l703gpa3nblwmfydpy7q4ly8bzrkangw97swji9fk7h9b46") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.5.0 (c (n "stm32f429") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5") (f (quote ("device"))) (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1cnadwp2w8qp6vxzisk7qwpyfiwc17jnifxj3sajm8gnj4007787") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.6.0 (c (n "stm32f429") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (f (quote ("device"))) (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0rsl27b6xdm05xdaymka8zdi57m6sywinmiaxkjw8n1h3acldmap") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f429-0.6.1 (c (n "stm32f429") (v "0.6.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (f (quote ("device"))) (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0cvk147prskbzvcq6ai3l3kksl2lqixdxwr57qndqhck7wb84g54") (f (quote (("rt" "cortex-m-rt"))))))

