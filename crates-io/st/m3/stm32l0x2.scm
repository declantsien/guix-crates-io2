(define-module (crates-io st m3 stm32l0x2) #:use-module (crates-io))

(define-public crate-stm32l0x2-0.0.1 (c (n "stm32l0x2") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0x3sslblnd219m9ccfd5v6gj4723fb27d6q749dsicqql85h79mn")))

