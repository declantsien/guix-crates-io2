(define-module (crates-io st m3 stm32f30x) #:use-module (crates-io))

(define-public crate-stm32f30x-0.1.0 (c (n "stm32f30x") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0jz5mgzy22qla49k8n2g4vq1m2paf568lia5ffmv5qm7w4vs5sp0")))

(define-public crate-stm32f30x-0.2.0 (c (n "stm32f30x") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0q56mcljzfnr68xkshrc9lr846v5gixb39nzp3y11qb4s26kwzi8")))

(define-public crate-stm32f30x-0.3.0 (c (n "stm32f30x") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1s06c5wgfs4fca4fgv9nn6vimi8nlsagp45ldi6xm0q16wmzyv9d")))

(define-public crate-stm32f30x-0.3.1 (c (n "stm32f30x") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "195w8dn504w0ayc0mrg5wy3kx9k9qqjkd2856sial7z79n3460rm")))

(define-public crate-stm32f30x-0.3.2 (c (n "stm32f30x") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "119aqkq8hq9xnqgx9zjzna6isrxckb1amvvb2gn3sj2mnqr692ic")))

(define-public crate-stm32f30x-0.4.0 (c (n "stm32f30x") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1gaihl20wrpv4vlciv4wfk308grmbw5lyvjs2qixi2y9m8cnma7m")))

(define-public crate-stm32f30x-0.4.1 (c (n "stm32f30x") (v "0.4.1") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1q77j3szwbmrkygj7x3ra39bckm80d9ib4jrvg1pbb9hbd6mrw2b")))

(define-public crate-stm32f30x-0.5.0 (c (n "stm32f30x") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1yj4qgwk9w61s6liqnxfyanaqjw9w5sya29l0y84r49cyxfc3nrg") (f (quote (("rt" "cortex-m-rt")))) (y #t)))

(define-public crate-stm32f30x-0.5.1 (c (n "stm32f30x") (v "0.5.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "05qfk4i2ihpp96ckna78jkdh5zfzak7vsi3rzq1gy7d4z9g3sqx7") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f30x-0.6.0 (c (n "stm32f30x") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1ygl4hb58hkpb3bf0k6kh664xikw6bip9fw8ds1y6amgsi1z7xkc") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f30x-0.6.1 (c (n "stm32f30x") (v "0.6.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1d2qz1r7smm45yr2516biv1c8pb41k52h56xvvvkki19pmavzlh2") (f (quote (("rt" "cortex-m-rt")))) (y #t)))

(define-public crate-stm32f30x-0.6.2 (c (n "stm32f30x") (v "0.6.2") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0amcnly60v6ii5zmf3r1alrvwxvy754j8920r8p100wwl56f1l9l") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f30x-0.7.0 (c (n "stm32f30x") (v "0.7.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1gfsz0wbrpl2wlxx6yi53n0fb4v642yz9d2hgq8amg9km9wkr9fx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-stm32f30x-0.7.1 (c (n "stm32f30x") (v "0.7.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">= 0.5.0, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0fvnh4w4cvb53pz9f1g91sk250ql2g9a6pv4i4nqr36ywmgdpkvc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-stm32f30x-0.8.0-alpha.1 (c (n "stm32f30x") (v "0.8.0-alpha.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "177adlww0ibd0jnp47p5r47y4mciwf72kn9pmz9pwvzp99dkk968") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-stm32f30x-0.8.0 (c (n "stm32f30x") (v "0.8.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "17sbnjyy4ywdv0lhmcgd86cl0316fgpxfzrgv30g1b2h1pzl32kn") (f (quote (("rt" "cortex-m-rt/device"))))))

