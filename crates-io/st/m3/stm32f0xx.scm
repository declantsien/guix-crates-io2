(define-module (crates-io st m3 stm32f0xx) #:use-module (crates-io))

(define-public crate-stm32f0xx-0.1.0 (c (n "stm32f0xx") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "10rlqxh8amfbfgzxff4cidf12dv0955rq9xk955r1ak1qpjxiw05")))

(define-public crate-stm32f0xx-0.2.1 (c (n "stm32f0xx") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0563v9zgf57arfx8901m2h3p0r59i3x5q5r6xdb5pwpqz4wzkfxk")))

(define-public crate-stm32f0xx-0.3.0 (c (n "stm32f0xx") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1r6y3q1qp9m84f0adg6m0vi64h1mp5qw4s2mrccywl3k8yymlvig")))

