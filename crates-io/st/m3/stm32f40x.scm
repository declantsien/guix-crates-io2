(define-module (crates-io st m3 stm32f40x) #:use-module (crates-io))

(define-public crate-stm32f40x-0.1.0 (c (n "stm32f40x") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0x2cjbc2y1i8n9gqqcr5nvm9rml8a51bb7i76cvbydqd8f4y0v6d")))

(define-public crate-stm32f40x-0.1.1 (c (n "stm32f40x") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "02qb7335i0srv6vczanzv4rw1hcx5jfc2nvrfhs8hvr095966kpy")))

(define-public crate-stm32f40x-0.1.2 (c (n "stm32f40x") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1y0h17np01va6qywjmw23h6i3fqj1p1k24ncw23y7jq1gp8xby3l")))

(define-public crate-stm32f40x-0.1.3 (c (n "stm32f40x") (v "0.1.3") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "05vv0gr8sgnkfdncq06375kdfyj17xk6znm79njzhysh0286fmc0")))

(define-public crate-stm32f40x-0.1.4 (c (n "stm32f40x") (v "0.1.4") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1mmchjik7swsw7sw51c1fyvxdlpbwi5y4pmc2cnddllb649npfc9")))

(define-public crate-stm32f40x-0.1.5 (c (n "stm32f40x") (v "0.1.5") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1rgxf1rnryy57z23550h3dssj3h45msbrnmvfrn3d95l32f6gqbz")))

(define-public crate-stm32f40x-0.2.0 (c (n "stm32f40x") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.2.11") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1mx285a6f4g5f904bq9458b5i98081vpxrpbj1fp88qvr1q8k042")))

(define-public crate-stm32f40x-0.2.1 (c (n "stm32f40x") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "06brmkdk9whswwlf726lnny0mrr990jccky6gk4aix37kh3xs170") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f40x-0.3.0 (c (n "stm32f40x") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "11spz3n5rjy4nczkghagzypmzyqm8ghjk7plaqkpswvrzh8vn1rs") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f40x-0.4.0 (c (n "stm32f40x") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1k98xr8k07ymr3zfz33jra582j7zg8ks1lpvaixz82xmq8f5fsa8") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f40x-0.5.0 (c (n "stm32f40x") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1kgcpyjpcb0zkq81w7jx4iw9k58y0d2y12l62y4hjyjiqa4cvfns") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f40x-0.6.0 (c (n "stm32f40x") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1jqbnyfbx15bm3lfiqvkd502zjiy1galbglbzvf0s5f2yn14h819") (f (quote (("rt" "cortex-m-rt"))))))

