(define-module (crates-io st m3 stm32f2) #:use-module (crates-io))

(define-public crate-stm32f2-0.1.0 (c (n "stm32f2") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0pinjx7krdpmvl90bc3smw4720ma7gyk0b5d1wzlx65p2sgn286i") (f (quote (("stm32f217") ("stm32f215") ("rt") ("default"))))))

(define-public crate-stm32f2-0.1.1 (c (n "stm32f2") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1gpyx886rj7jj4lgkia77vngc1asyzz3mik2wyj4l0cqwzf8dplm") (f (quote (("stm32f217") ("stm32f215") ("rt") ("default"))))))

(define-public crate-stm32f2-0.2.0 (c (n "stm32f2") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "05pbyq7jnfipw0j89jyf2xc0jxvr6jqjvyl3rzvcgrx2nacggx3b") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.2.1 (c (n "stm32f2") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1875d4gqhg40v20gnaw579rnpr8j9r01c320z9fj0cw8sjg45adw") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.2.2 (c (n "stm32f2") (v "0.2.2") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0qpn8mqhnc2gxl2snpb0jbd6qib4n37apivhgkyzjfxhvcdnys5n") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.2.3 (c (n "stm32f2") (v "0.2.3") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0bs8paz0finq6xn54vpdxgb65f7b88rcclcx4h6dy59sdw4r5l0z") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.3.0 (c (n "stm32f2") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1w97d4lzgawbcfywnwchas43pk1c4j8xpg8p1bsq4b4qdl5kw4j4") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default")))) (y #t)))

(define-public crate-stm32f2-0.3.1 (c (n "stm32f2") (v "0.3.1") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "17mlfd2xcfnx0wcwhsanrkv6ahxzyh6fwxxn4aszvfa7w98aqdfi") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.3.2 (c (n "stm32f2") (v "0.3.2") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1ccm97jq7xfirsdqbg68725bh9ji908d2pnlbz5xrnhghch24b00") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.4.0 (c (n "stm32f2") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1cwvjwk2wirqpgk1n3scjqkgk7dx66gdidsh0g7rv2kkd8g3llqh") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.5.0 (c (n "stm32f2") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0fx1k5h94vr94h73x75nnvgqfm4j212dgk3ari8805s9mgd2835w") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.6.0 (c (n "stm32f2") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1cw0h6lhp35bzi6y2hqa8gljg5nb44bxf1i9fwd4rxd66y44lh2g") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.7.0 (c (n "stm32f2") (v "0.7.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1m0r6f20d5i129bhiwv5qghypri2abkzj78kj0hq63bhph9z8kc2") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.7.1 (c (n "stm32f2") (v "0.7.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0qlhz8xkvgmnxmkd2blz1yn3lnqnx171jnxmd1fzmij652xp2n93") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.8.0 (c (n "stm32f2") (v "0.8.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0n7f2v77amdfll0v86lfjixq477qc3dmf2dx0gfvymqk5cp5qyci") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.9.0 (c (n "stm32f2") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0zgnjd0m7200713x7ji6fz5wnmak1ijlwjldj5l6jj2msql9nmyy") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.10.0 (c (n "stm32f2") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0cy28nvkpldka09c1mncfxs4gs27504djs916vfdphpjk9w1j757") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.11.0 (c (n "stm32f2") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "03xf47y1i60s3dk7fxm8rshzhigzmy8yjrk14g53qgggag8pj35q") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.12.0 (c (n "stm32f2") (v "0.12.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "07df902vnl0392y85s86mh0zk5j421mh1w828clnzgx2yskqyh81") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.12.1 (c (n "stm32f2") (v "0.12.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0gc23mpghsip53armp4spp8b8dfz2f99ssjs9da4n09232gyp5qi") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.13.0 (c (n "stm32f2") (v "0.13.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1z1p0pk23960ij8niqgcxzgffhl8bbadxpk3brp1qpzszmklyzy6") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f2-0.14.0 (c (n "stm32f2") (v "0.14.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0dckwvg8c9biqij7c9g344ic8rybx8rz00gyn6swyrjdkkv59v8d") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-stm32f2-0.15.0 (c (n "stm32f2") (v "0.15.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0fqv3a990376pbj52fkqn4a1gqa1p030ycxchbklxsngmvd9wgaq") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default" "rt")))) (y #t)))

(define-public crate-stm32f2-0.15.1 (c (n "stm32f2") (v "0.15.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1qhxqg4w284m8mhxqh9r6i12ksgfq790jw9r4hpr5wjpqcr8bjnl") (f (quote (("stm32f217") ("stm32f215") ("rt" "cortex-m-rt/device") ("default" "rt"))))))

