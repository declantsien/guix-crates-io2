(define-module (crates-io st m3 stm32f7x9) #:use-module (crates-io))

(define-public crate-stm32f7x9-0.1.0 (c (n "stm32f7x9") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0fxwnnxk7m38vnnslbjrbph9dmag7dghqqr62pv7jr1qayds6bw6") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f7x9-0.2.0 (c (n "stm32f7x9") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1dl4l0k4m59m1m2f83khvax6x2gj1fyshhb1nz395a2a22ycldk7") (f (quote (("rt" "cortex-m-rt"))))))

