(define-module (crates-io st m3 stm32builder) #:use-module (crates-io))

(define-public crate-stm32builder-0.0.1 (c (n "stm32builder") (v "0.0.1") (h "05d5b6w7a97n4651p44scw3q57psws48xsqlw3jpiqm4a66aawxf") (y #t)))

(define-public crate-stm32builder-0.1.0 (c (n "stm32builder") (v "0.1.0") (d (list (d (n "handlebars") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "070gw417iwji4ffg63ac4085rrkfb4k1hpwzfbr6mwkfv8cp2ggx") (f (quote (("render" "handlebars") ("default" "render"))))))

