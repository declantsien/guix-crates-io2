(define-module (crates-io st m3 stm32f042) #:use-module (crates-io))

(define-public crate-stm32f042-0.5.0 (c (n "stm32f042") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1d8f70ra0wn99w1xjzk2a14rfhq8zqjfpw6bssbzysmkmqyg3rqj") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f042-0.5.1 (c (n "stm32f042") (v "0.5.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.13") (f (quote ("abort-on-panic"))) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1niiwm798gy580jbsm4ga64asldv1jsj5ripmppkayad6xmlc6ah") (f (quote (("rt") ("default" "rt"))))))

(define-public crate-stm32f042-0.5.2 (c (n "stm32f042") (v "0.5.2") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.15") (f (quote ("abort-on-panic"))) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "06110c41g8lclwy80xgb5j2z23qqkp9ar24im0wqak5vvkmn71n0") (f (quote (("rt") ("default" "rt"))))))

(define-public crate-stm32f042-0.5.3 (c (n "stm32f042") (v "0.5.3") (d (list (d (n "bare-metal") (r "^0.1.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (d #t) (k 0)) (d (n "panic-abort") (r "^0.1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "012vaz3wmr33vh1xy4i34hj2gjj5pn4z913q2jjxf4rabl5i5v7h") (f (quote (("rt") ("default" "rt"))))))

(define-public crate-stm32f042-0.6.0 (c (n "stm32f042") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "panic-abort") (r "^0.1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1zy9bmkkg9vnj1pxy88zym8mfijxag1l7vn1ssnhj345amms21s5") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-stm32f042-0.6.1 (c (n "stm32f042") (v "0.6.1") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "panic-abort") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "194c7jdd11i0kvj3vsk7hrh9n7jksbgni9k3aml4p29i3mbwyfd1") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-stm32f042-0.6.2 (c (n "stm32f042") (v "0.6.2") (d (list (d (n "bare-metal") (r "^0.2.3") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "panic-abort") (r "^0.3.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0v8xa13akk62wbnvsgzdzjy5gkanlxqqfp6cln2qrdsnh5yqs9ak") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-stm32f042-0.6.3 (c (n "stm32f042") (v "0.6.3") (d (list (d (n "bare-metal") (r "^0.2.3") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "panic-abort") (r "^0.3.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "14cb1if8l1x54icjvhdyf4wqakj9m55z2yqkygy7qcpb6gxz3xmj") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt"))))))

