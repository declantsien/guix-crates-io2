(define-module (crates-io st m3 stm32wb-hci) #:use-module (crates-io))

(define-public crate-stm32wb-hci-0.1.2 (c (n "stm32wb-hci") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "defmt") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1gldkqmbclj3i7p4vxbqm2m73rq1s00f86j3cpvk3z2jhzilqhp6") (f (quote (("version-5-0") ("version-4-2") ("version-4-1") ("ms") ("default" "version-5-0" "ms"))))))

(define-public crate-stm32wb-hci-0.1.3 (c (n "stm32wb-hci") (v "0.1.3") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0r13k8xqj4ba4d9m8rnajq1gzyy6ymaawf02904k3c1195am0nwg") (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-stm32wb-hci-0.1.4 (c (n "stm32wb-hci") (v "0.1.4") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "109cybvsfbzclbwnnsamhgrzdpj8pqgifl924sqbpmns1by6zzyp") (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-stm32wb-hci-0.16.0 (c (n "stm32wb-hci") (v "0.16.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0yknbpwy50mi3p3rndfcz4y0ml4jg9bglvxwlxnh4ps0pyzw9kjy") (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-stm32wb-hci-0.17.0 (c (n "stm32wb-hci") (v "0.17.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0v9d8g392sbl12y2mrlsn0qw9g6dkk7bh4z9x909hd45mxpknc6a") (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-stm32wb-hci-0.17.1 (c (n "stm32wb-hci") (v "0.17.1") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0b5hb8mam95acqwzylcs7ckb1qik22i507xq9svp6hkjhall5knx") (y #t) (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-stm32wb-hci-0.17.2 (c (n "stm32wb-hci") (v "0.17.2") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1cm9qhdkqzlkj2dazzy4bfrhm5ksafkl1p678ws4j97ny3q6zyyh") (s 2) (e (quote (("defmt" "dep:defmt"))))))

