(define-module (crates-io st m3 stm32f429x) #:use-module (crates-io))

(define-public crate-stm32f429x-0.2.0 (c (n "stm32f429x") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "11058cj9yqs9nlf77sq17asxjaixq7ri6nx5jyq730vcdfcy6b69")))

(define-public crate-stm32f429x-0.3.0 (c (n "stm32f429x") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "054xiqskdd7w0fccwab15jyazq6f0snghc9v3wg8gf0vsq0xyy8k")))

(define-public crate-stm32f429x-0.3.1 (c (n "stm32f429x") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0xdkv9y36pgd25aljh2a567i0galk7w7cwjvqdqb5d4f3n81shdz")))

