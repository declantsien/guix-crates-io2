(define-module (crates-io st m3 stm32wb-pac) #:use-module (crates-io))

(define-public crate-stm32wb-pac-0.1.0 (c (n "stm32wb-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1h3csvk2xa40i59vag72y4935kzwvsv3fkwac68zjp7qnzj2fxki") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-stm32wb-pac-0.2.0 (c (n "stm32wb-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1jwsf5dvg88gf49fg12iss4w3yj9cg9zai3pd5j6gnm3l6pqj5g2") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-stm32wb-pac-0.2.1 (c (n "stm32wb-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1iccp94q7l2l9afy1f9c1qbd52zbxljsiy4l5s3bdgqz1ir0g8dy") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-stm32wb-pac-0.2.2 (c (n "stm32wb-pac") (v "0.2.2") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1676rdkafhnb6xra9ldv9hl82bii7vplkjp4lql0bc1fshsh999k") (f (quote (("rt" "cortex-m-rt/device"))))))

