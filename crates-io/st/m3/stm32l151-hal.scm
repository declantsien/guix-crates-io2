(define-module (crates-io st m3 stm32l151-hal) #:use-module (crates-io))

(define-public crate-stm32l151-hal-0.1.0 (c (n "stm32l151-hal") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32l151") (r "^0.2.1") (f (quote ("rt"))) (d #t) (k 0)))) (h "17fy7zhanzh8r64wg74h7m5mp0c76rbrp9ia2dp53lh7w1za2yny")))

(define-public crate-stm32l151-hal-0.2.0 (c (n "stm32l151-hal") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32l1") (r "^0.1.1") (f (quote ("stm32l151" "rt"))) (d #t) (k 0)))) (h "1crn3yjyw2xr7hchgjs55saxd0ijhql38bcy4wk5crlqryi1wjh1")))

(define-public crate-stm32l151-hal-0.3.0 (c (n "stm32l151-hal") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32l1") (r "^0.2.2") (f (quote ("stm32l151" "rt"))) (d #t) (k 0)))) (h "1dis6m35wl4bcclgb3dn2nq4zvs6gnzscarlvdssxilxawxi02ir")))

(define-public crate-stm32l151-hal-0.4.0 (c (n "stm32l151-hal") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32l1") (r "^0.2.3") (f (quote ("stm32l151" "rt"))) (d #t) (k 0)))) (h "1pb69jh1yb452lwb610sgqnq4d53hkfswdgpbcingprkmzkalp4k")))

(define-public crate-stm32l151-hal-0.5.0 (c (n "stm32l151-hal") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32l1") (r "^0.3.2") (f (quote ("stm32l151" "rt"))) (d #t) (k 0)))) (h "1i8pg16r7kg507kzy22cns8hjqn19r513hqxpihc64mrqzbw9gcp")))

