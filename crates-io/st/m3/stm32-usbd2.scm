(define-module (crates-io st m3 stm32-usbd2) #:use-module (crates-io))

(define-public crate-stm32-usbd2-0.7.0 (c (n "stm32-usbd2") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "stm32f1xx-hal") (r "^0.7.0") (f (quote ("stm32f103"))) (d #t) (k 2)) (d (n "usb-device") (r "^0.3.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "054paflb6wjrw2bm1d900bs4hld9rgia02jv6kravlvp77sz5ikg")))

