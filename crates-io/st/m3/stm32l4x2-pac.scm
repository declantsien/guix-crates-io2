(define-module (crates-io st m3 stm32l4x2-pac) #:use-module (crates-io))

(define-public crate-stm32l4x2-pac-0.0.1 (c (n "stm32l4x2-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0hrd0559065gb0v4gwm6igh9n6jn6cpapz35f4njvy8hh2czgx3k") (f (quote (("rt" "cortex-m-rt/device"))))))

