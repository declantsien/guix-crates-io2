(define-module (crates-io st m3 stm32f469xx) #:use-module (crates-io))

(define-public crate-stm32f469xx-0.1.0 (c (n "stm32f469xx") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0ch5sfi5ikd2a15d7m64w9kg71znkcrb7ndi5l32m7wjz822bly0") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f469xx-0.2.0 (c (n "stm32f469xx") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0b3shnvbwwgjcf24bnlxxw9q2n1hk09blwxfvbdzswsynympa3yj") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f469xx-0.2.1 (c (n "stm32f469xx") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1bxicb892a7shjjl6nzn0d11k1459q3z2bkm1khrd1dfbq1f84sw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-stm32f469xx-0.3.0 (c (n "stm32f469xx") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1n6z7ljmmr8j4xqxn7i0wf2941vizw6fgkn4mxxbpidbmryhk38p") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-stm32f469xx-0.4.0 (c (n "stm32f469xx") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0kr2x6yb80a7sc2k0b9xb8w3r2v7f5vivds4nkg2z75ib3ap8kkm") (f (quote (("rt" "cortex-m-rt/device"))))))

