(define-module (crates-io st m3 stm32f30x-memory-map) #:use-module (crates-io))

(define-public crate-stm32f30x-memory-map-0.1.0 (c (n "stm32f30x-memory-map") (v "0.1.0") (d (list (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "19wmv3dwsjcm3mb7k7j9i7jvvanhkkd6fwkz52ahbb7lpqav3vv2")))

(define-public crate-stm32f30x-memory-map-0.1.1 (c (n "stm32f30x-memory-map") (v "0.1.1") (d (list (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "1ji7v81grzvj471zvv7ph92gx3ayyyd19s3bk1ljbxbwyxymsfl2")))

(define-public crate-stm32f30x-memory-map-0.1.2 (c (n "stm32f30x-memory-map") (v "0.1.2") (d (list (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "1sfx5qsk77shgwzwgxpr4l68qj6b3l3csmgx05brwb9v7brn4f8h")))

