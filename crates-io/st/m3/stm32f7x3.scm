(define-module (crates-io st m3 stm32f7x3) #:use-module (crates-io))

(define-public crate-stm32f7x3-0.1.0 (c (n "stm32f7x3") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0wdd3fh0mrsd752dfy9m42xpvxczm1mfjm1z207v63p3g9p734h2") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f7x3-0.2.0 (c (n "stm32f7x3") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "08q80q68q5w9fadacfmr06z5bkvjgb5d20hgc0w7jfbldd86av9z") (f (quote (("rt" "cortex-m-rt"))))))

