(define-module (crates-io st m3 stm32f411xx) #:use-module (crates-io))

(define-public crate-stm32f411xx-0.0.1 (c (n "stm32f411xx") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1jnk3zbg769vpxqb0y89aalswhdh1pjawr6zzdjgv31b0184dvwv")))

