(define-module (crates-io st m3 stm32f030_cf) #:use-module (crates-io))

(define-public crate-stm32f030_cf-0.0.1 (c (n "stm32f030_cf") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0h4gm0r5s2i9frszz1qwx48ma204wh7y4952csfmn4pr5jxlflc8")))

