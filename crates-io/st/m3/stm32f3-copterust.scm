(define-module (crates-io st m3 stm32f3-copterust) #:use-module (crates-io))

(define-public crate-stm32f3-copterust-0.12.1 (c (n "stm32f3-copterust") (v "0.12.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1xn80yj5zag9511n74b4qrx506lcr3qbf8rpcxq7b33qg074phfw") (f (quote (("stm32f3x8") ("stm32f3x4") ("stm32f373") ("stm32f303") ("stm32f302") ("stm32f301") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f3-copterust-0.12.2 (c (n "stm32f3-copterust") (v "0.12.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1z0vq9082pvr9qdcy826fxjhddnd0y3i023azlffr554wrkwnf6l") (f (quote (("stm32f3x8") ("stm32f3x4") ("stm32f373") ("stm32f303") ("stm32f302") ("stm32f301") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32f3-copterust-0.12.3 (c (n "stm32f3-copterust") (v "0.12.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0ra7bfqm901mal39biiw6qvnbldxmka4h1pxm162ffnbfsigg1zk") (f (quote (("stm32f3x8") ("stm32f3x4") ("stm32f373") ("stm32f303") ("stm32f302") ("stm32f301") ("rt" "cortex-m-rt/device") ("default"))))))

