(define-module (crates-io st m3 stm32f0x2) #:use-module (crates-io))

(define-public crate-stm32f0x2-0.1.0 (c (n "stm32f0x2") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1i57wid0rrfxnnz48kfwhpkik65vq5qrnqlc5chiz9kpckqq7lpm") (f (quote (("rt" "cortex-m-rt"))))))

