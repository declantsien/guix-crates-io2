(define-module (crates-io st m3 stm32wb) #:use-module (crates-io))

(define-public crate-stm32wb-0.12.1 (c (n "stm32wb") (v "0.12.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "01rkay36m14486j512jjw6y3j1fy815ww9kkyj8lnk65rlciafx9") (f (quote (("stm32wb55") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32wb-0.13.0 (c (n "stm32wb") (v "0.13.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0s4myy61frnkliwlqmqcxs0qqaycf04j88i7ggkkvnhngp3px1xm") (f (quote (("stm32wb55") ("rt" "cortex-m-rt/device") ("default"))))))

(define-public crate-stm32wb-0.14.0 (c (n "stm32wb") (v "0.14.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "12a9ryan0q1ygd5fbkajl3hbb6br1bpajl9c3hdyfcq28c9l2hqp") (f (quote (("stm32wb55") ("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-stm32wb-0.15.0 (c (n "stm32wb") (v "0.15.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1c5g2jgjddjf2bmsaz4fzy9ihnngp2rpjpwiq6wy0zha7gbgaaqd") (f (quote (("stm32wb55") ("rt" "cortex-m-rt/device") ("default" "rt")))) (y #t)))

(define-public crate-stm32wb-0.15.1 (c (n "stm32wb") (v "0.15.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bg6n21vz76brn97yr12xiwif8wsqjnyyr90rmwiw0ldjznc3wna") (f (quote (("stm32wb55") ("rt" "cortex-m-rt/device") ("default" "rt"))))))

