(define-module (crates-io st m3 stm32) #:use-module (crates-io))

(define-public crate-stm32-0.0.0 (c (n "stm32") (v "0.0.0") (h "1lyd16hlbay1h29g736lnxayq2qqq4wwz9bimk9bfhg7yi8r3jwm")))

(define-public crate-stm32-0.0.1 (c (n "stm32") (v "0.0.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1pl3v1c8xibm7cp9n9clcny7qf2si99aj01bckyagm7rxpbxp592")))

