(define-module (crates-io st m3 stm32f103xx) #:use-module (crates-io))

(define-public crate-stm32f103xx-0.1.0 (c (n "stm32f103xx") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0h472m2mjispqzrqdwwxlhmfh18fk3phlcy7za5rmisdx8nlfqgp")))

(define-public crate-stm32f103xx-0.2.0 (c (n "stm32f103xx") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0w8xm8cs3pj3y3wpqw1nkmfrbzd65axz5m5xahp7xa875x5zsd52")))

(define-public crate-stm32f103xx-0.3.0 (c (n "stm32f103xx") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1935a9pwx6z9szr635742yd10jry6hmyxfgv9i3hrphhm50y95gh")))

(define-public crate-stm32f103xx-0.3.1 (c (n "stm32f103xx") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "12m1lg3dvrpydk1rvwj58is66w3rchd59cyg2lbrw498vb3lf77p")))

(define-public crate-stm32f103xx-0.4.0 (c (n "stm32f103xx") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0a2zhsq0pbxxrq7a9l3d036dvwq9hy8gk32h41gldw498bhszbh7")))

(define-public crate-stm32f103xx-0.4.1 (c (n "stm32f103xx") (v "0.4.1") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0idbs3fxj1w52347r7gijs6klg6d8j65bz75w0bj4430p8vknz5l")))

(define-public crate-stm32f103xx-0.5.0 (c (n "stm32f103xx") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1rfmgp9hph38i4aa9f2dnlrlw2wsazqap88ycy4d2wnpmajn1hq9")))

(define-public crate-stm32f103xx-0.6.0 (c (n "stm32f103xx") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1l4swvg4yra0abjh2wlhybw5gclx6r2z801qnyvkhj42srmnyd46") (y #t)))

(define-public crate-stm32f103xx-0.6.1 (c (n "stm32f103xx") (v "0.6.1") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0xnaryssfz3j2prqmyxcz5r34mw79jiy6qp61jqwwfdlkmxlxg07")))

(define-public crate-stm32f103xx-0.7.0 (c (n "stm32f103xx") (v "0.7.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "14vl4h3bb9gj1fs19yx98xqhyggbvqapcznw9xs8kvdhxdz1blpb") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f103xx-0.7.1 (c (n "stm32f103xx") (v "0.7.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0q92bajs35pvr58lbwd2s680ins2bvvgf8x5gysf5x0c8d8gxvay") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f103xx-0.7.2 (c (n "stm32f103xx") (v "0.7.2") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0k1ak37w98v988ka5nzhz3h49wg0nnlnaqlmlzx55mws958nbfwm") (f (quote (("rt" "cortex-m-rt")))) (y #t)))

(define-public crate-stm32f103xx-0.7.3 (c (n "stm32f103xx") (v "0.7.3") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1sd0vn4wavircgn3ykaq3c7vywg24lx4smsp6a3a2mv5hxyblj6s") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f103xx-0.7.4 (c (n "stm32f103xx") (v "0.7.4") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1fnh12h530znca0ph1myzvgwxvyh82zp1fwqmg665g74bb8pqwlq") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f103xx-0.7.5 (c (n "stm32f103xx") (v "0.7.5") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0wjsmk263fc816yqb7xmq8knaw3gc91gl025n8h06b1y3nkk5r5z") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f103xx-0.8.0 (c (n "stm32f103xx") (v "0.8.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1a1avaksg7sjbclyrkxyfvh4in6n0vcmbm6q33xblvgfmf2x8m1k") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f103xx-0.9.0 (c (n "stm32f103xx") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1ngnghzhx1lhgyd59175v8gscikf7aq51yfi19wd9cismr360ky4") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f103xx-0.9.1 (c (n "stm32f103xx") (v "0.9.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1wq0gjcpw7n38g4jwagd8cq3g6k080n43088fqlcm3hm9q356gff") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f103xx-0.10.0 (c (n "stm32f103xx") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0bilqlzcs64b0myqfj6ag1wsmfyhgavj1bkppq0xg6y2qxi4i660") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-stm32f103xx-0.8.1 (c (n "stm32f103xx") (v "0.8.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "02662nr9fjhxbpad9js830m8fy47q9f8gwk9za6d1cjqkdksj5w9") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f103xx-0.11.0-alpha.1 (c (n "stm32f103xx") (v "0.11.0-alpha.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "02s2ij9fiaqsjx7r3hlnkhcxrnfbyh14icwcvcf33zlbgh63dgm9") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-stm32f103xx-0.11.0 (c (n "stm32f103xx") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "05d6biz3b0917rgm315cyppv4661cxd2nrzz4nwwsk852i0iwnq5") (f (quote (("rt" "cortex-m-rt/device"))))))

