(define-module (crates-io st m3 stm32f401re) #:use-module (crates-io))

(define-public crate-stm32f401re-0.1.0 (c (n "stm32f401re") (v "0.1.0") (h "05di17s8hynaaaimy4j04bsyxhpjw6fn4lbhhhfmd22s80hyfbp3") (y #t)))

(define-public crate-stm32f401re-0.2.0 (c (n "stm32f401re") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.5.0") (k 0)))) (h "0a3jp5q201ffh7qk8zvik6f74v60rwpw1zlhkavgw5sm6p15hw4p") (y #t)))

