(define-module (crates-io st m3 stm32l4x6) #:use-module (crates-io))

(define-public crate-stm32l4x6-0.3.0 (c (n "stm32l4x6") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "05bjfzs4gx573dwmhmscn7f4avkjja8dycdbilymc2w7kj3wyyiq")))

(define-public crate-stm32l4x6-0.4.0 (c (n "stm32l4x6") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "14l828y5fpnnf9pj12iaqaxrpg6v17hkr717387q543cads1zrxj") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32l4x6-0.5.0 (c (n "stm32l4x6") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0msdr8alv3pb4s14dcncfifwikjspahw9570w7q7s61a7l9yc0w7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-stm32l4x6-0.5.1 (c (n "stm32l4x6") (v "0.5.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">= 0.5.0, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1kgaa4rqsqi05qvk40dhpjd21b9hglj84qxrdvfgwbhknaqqkmk4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-stm32l4x6-0.6.0 (c (n "stm32l4x6") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">= 0.5.0, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "02k14npqd3anv06fynmdwydf3383w5xk3psl58y82ylgp8jzbwvl") (f (quote (("rt" "cortex-m-rt/device"))))))

