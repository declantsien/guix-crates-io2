(define-module (crates-io st m3 stm32l4x6-memory-map) #:use-module (crates-io))

(define-public crate-stm32l4x6-memory-map-0.1.0 (c (n "stm32l4x6-memory-map") (v "0.1.0") (d (list (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "1i39pfpph7l6v92i9sy4rgdpi5rnnq42gwg39mjs7gzcp9vzlld9") (y #t)))

(define-public crate-stm32l4x6-memory-map-0.2.0 (c (n "stm32l4x6-memory-map") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.2.4") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0fcwf1yz4wy5gzpg6nil7p9klpwmzdq48z3s8zfr7rkzp579blaf") (y #t)))

