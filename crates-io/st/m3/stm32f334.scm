(define-module (crates-io st m3 stm32f334) #:use-module (crates-io))

(define-public crate-stm32f334-0.1.0 (c (n "stm32f334") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0kdxbpn1m9542a0nlr66w8v1cg07w8p7l5wnsznwavfr9zlzg59n") (f (quote (("rt" "cortex-m-rt"))))))

