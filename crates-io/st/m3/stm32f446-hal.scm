(define-module (crates-io st m3 stm32f446-hal) #:use-module (crates-io))

(define-public crate-stm32f446-hal-0.0.1 (c (n "stm32f446-hal") (v "0.0.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f4") (r "^0.2.2") (f (quote ("stm32f446"))) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "09rja4kifklpkxhk085mq6a39a5g7lbvx8zy1khd484pcxpsxrcj") (f (quote (("rt" "stm32f4/rt"))))))

