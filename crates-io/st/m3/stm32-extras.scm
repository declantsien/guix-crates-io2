(define-module (crates-io st m3 stm32-extras) #:use-module (crates-io))

(define-public crate-stm32-extras-0.1.0 (c (n "stm32-extras") (v "0.1.0") (d (list (d (n "stm32f103xx") (r "^0.7.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "09qimgcpv9m7n9ivl87v8p8bd7nhh6rwvr6wssz4xzca1f6h1a4a") (f (quote (("use-stm32f103xx" "stm32f103xx" "vcell")))) (y #t)))

