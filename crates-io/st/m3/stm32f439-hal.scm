(define-module (crates-io st m3 stm32f439-hal) #:use-module (crates-io))

(define-public crate-stm32f439-hal-0.1.0 (c (n "stm32f439-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "stm32f439") (r "^0.1.0") (d #t) (k 0)))) (h "1jph99r8nf2yfv2nrs0yp2hika6j182c84ng4mnwrvl0slmv8isx")))

