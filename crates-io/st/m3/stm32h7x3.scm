(define-module (crates-io st m3 stm32h7x3) #:use-module (crates-io))

(define-public crate-stm32h7x3-0.0.1 (c (n "stm32h7x3") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0hj89kix0cxmd6in6b59la90izzy0cdry4zvba6v96dqk77ap77n") (f (quote (("rt" "cortex-m-rt"))))))

