(define-module (crates-io st m3 stm32l0x3) #:use-module (crates-io))

(define-public crate-stm32l0x3-0.0.1 (c (n "stm32l0x3") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0id2wq32s2873d9pyz12da9g8id77wr0mzd1d3m6r1xblk6jqwcr")))

