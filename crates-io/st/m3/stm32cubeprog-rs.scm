(define-module (crates-io st m3 stm32cubeprog-rs) #:use-module (crates-io))

(define-public crate-stm32cubeprog-rs-0.0.1 (c (n "stm32cubeprog-rs") (v "0.0.1") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "07abn92lfa2va31xalsc2sllbsx3flzhdpr85wgc6dg9qy7sn9xs")))

(define-public crate-stm32cubeprog-rs-0.0.2 (c (n "stm32cubeprog-rs") (v "0.0.2") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0mpgrkkwz10jpvmkpwv91yi9hh98zryxjj1zi0zzizzib1fdj0wg")))

(define-public crate-stm32cubeprog-rs-0.0.3 (c (n "stm32cubeprog-rs") (v "0.0.3") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1xvb2xgjmhr3f8ppsj4jwnvzpbyf70hwq55whpgkxybrp6ln95r8")))

(define-public crate-stm32cubeprog-rs-0.0.4 (c (n "stm32cubeprog-rs") (v "0.0.4") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0kda1yiacx32666dz8yxqkx7anz2y3m1i6h351bnaxs0vnb08zy2")))

