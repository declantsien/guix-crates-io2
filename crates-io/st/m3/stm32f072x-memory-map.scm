(define-module (crates-io st m3 stm32f072x-memory-map) #:use-module (crates-io))

(define-public crate-stm32f072x-memory-map-0.1.0 (c (n "stm32f072x-memory-map") (v "0.1.0") (d (list (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "19gdrvirq04mqffkzj98j1qzigxa5p44vm0rid3lp09xi46xnvhl")))

