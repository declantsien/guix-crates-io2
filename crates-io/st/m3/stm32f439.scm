(define-module (crates-io st m3 stm32f439) #:use-module (crates-io))

(define-public crate-stm32f439-0.1.0 (c (n "stm32f439") (v "0.1.0") (h "17jq9zq55qir5szdy45sj5ww32s57zzi6x5ygp1qks4wq1mls1nv")))

(define-public crate-stm32f439-0.1.1 (c (n "stm32f439") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1i69wn2vzbxx3167wy1pd8v9n47hmsgllfch2wrapz9hvg7y0xx4")))

