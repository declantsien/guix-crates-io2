(define-module (crates-io st m3 stm32f446) #:use-module (crates-io))

(define-public crate-stm32f446-0.1.0 (c (n "stm32f446") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1va6q1z1ffah16m3sdjis44s08zr4jxgx23zpmnni3visxgizjkd") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-stm32f446-0.1.1 (c (n "stm32f446") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "16p105bphm18z01axbix866ax2862yqhqaymnpwmi9jmvban4zh1") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

