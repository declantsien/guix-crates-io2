(define-module (crates-io st m3 stm32l5xx-hal) #:use-module (crates-io))

(define-public crate-stm32l5xx-hal-0.0.0 (c (n "stm32l5xx-hal") (v "0.0.0") (d (list (d (n "bare-metal") (r "^0.2.4") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1y582gscd3w2jgmkraz99lzs13ny0xbys8cpqgkpbxqgzhra6sg7")))

