(define-module (crates-io st m3 stm32-fmc) #:use-module (crates-io))

(define-public crate-stm32-fmc-0.1.0 (c (n "stm32-fmc") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (k 0)))) (h "1dzxxi6ajjqswlpb2nw1c0lxyl8gphxjg81zbbs6lnikz3y9naa7") (f (quote (("sdram") ("default" "sdram"))))))

(define-public crate-stm32-fmc-0.1.1 (c (n "stm32-fmc") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (k 0)))) (h "1qjgdwygr4qfd6mza6f8zi2mgq8wwifqxgpn085671hx86qj5jxl") (f (quote (("sdram") ("default" "sdram"))))))

(define-public crate-stm32-fmc-0.1.2 (c (n "stm32-fmc") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (k 0)))) (h "1k1f6pggd6hci52411kanhdaffsb5qgvmwy7a0nap0flwijcnrfs") (f (quote (("sdram") ("default" "sdram"))))))

(define-public crate-stm32-fmc-0.2.0 (c (n "stm32-fmc") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0n4j229n0bxr53fw92xih74cmwk3gzk5s72b6nxqgzvib4w75552") (f (quote (("sdram") ("default" "sdram"))))))

(define-public crate-stm32-fmc-0.2.1 (c (n "stm32-fmc") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "002hnv5wb9n0mihcy7jh27c06pm7hrc55wqc70hi6p4zcnn6r19l") (f (quote (("sdram") ("default" "sdram"))))))

(define-public crate-stm32-fmc-0.2.2 (c (n "stm32-fmc") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0l8cxw3szb79pigmhgh146m4cmh44m5r21x7as0swcl3w9aq4vkz") (f (quote (("sdram") ("default" "sdram"))))))

(define-public crate-stm32-fmc-0.2.3 (c (n "stm32-fmc") (v "0.2.3") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "052mxd4652dyynpdrndhv6qray6xs7ry37hr9vrx6hgafda5wjf2") (f (quote (("sdram") ("default" "sdram"))))))

(define-public crate-stm32-fmc-0.2.4 (c (n "stm32-fmc") (v "0.2.4") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0x5fx42if04f8375a3s2y6x65f4ylk4ymwrwi2148xfysndyw5ng") (f (quote (("sdram") ("default" "sdram"))))))

(define-public crate-stm32-fmc-0.3.0 (c (n "stm32-fmc") (v "0.3.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0qwdclv37wbxm0gmyxjynvkkf3nwcnvnlpbz6z5lw6g66c7xc3l3") (f (quote (("trace-register-values") ("sdram") ("nand") ("default" "sdram" "nand"))))))

