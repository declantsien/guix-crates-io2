(define-module (crates-io st m3 stm32f7x2) #:use-module (crates-io))

(define-public crate-stm32f7x2-0.1.0 (c (n "stm32f7x2") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "17nxmcyzmj53ji748vn1bxv6w4qvv11vs1jq20qj0fry4cw8xpc5") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-stm32f7x2-0.2.0 (c (n "stm32f7x2") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1cjs7rhj473cxbkgidk8dk2hsxc3ahkz5rkr6xc8wbb81dg91x4p") (f (quote (("rt" "cortex-m-rt"))))))

