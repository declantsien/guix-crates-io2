(define-module (crates-io st lv stlv) #:use-module (crates-io))

(define-public crate-stlv-0.1.0 (c (n "stlv") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)))) (h "0dsc3sdp46aar78z0smz9nwal5h51n8wgyjbysi9352yvgpfspc8")))

(define-public crate-stlv-0.1.1 (c (n "stlv") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)))) (h "1jg8l4lp09w5d50x53rsd3k364034nz9dz5sdf7rgcxmdfsjn2jf")))

(define-public crate-stlv-0.1.2 (c (n "stlv") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)))) (h "1cbgxm3cbbil4s74v07s0p2p9kawmyvx8n218y1z8azhh9xs1drc")))

(define-public crate-stlv-0.1.3 (c (n "stlv") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0yapcs87gzr0hwj5y0gn845niv6li4ankwm1sdcdfh6c1mdbzv3x")))

