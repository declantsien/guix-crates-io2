(define-module (crates-io st #{73}# st7306) #:use-module (crates-io))

(define-public crate-st7306-0.8.2 (c (n "st7306") (v "0.8.2") (d (list (d (n "embedded-graphics") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)))) (h "1pmzag9g4lb75zyf8qylv2w0z69ynhjndsa5za6sdlhy40h2ldba") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

