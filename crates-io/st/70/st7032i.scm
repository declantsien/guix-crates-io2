(define-module (crates-io st #{70}# st7032i) #:use-module (crates-io))

(define-public crate-st7032i-0.0.1 (c (n "st7032i") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "12mmnk4r4pkj5iscrxvfymyr03afb2lalmrjj91m9vvphh0nmj36")))

(define-public crate-st7032i-0.0.2 (c (n "st7032i") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "01ir016pc27x1y4fzjdp2rkq36b60i30imqskr03ybrwdca4k5dj")))

(define-public crate-st7032i-0.0.3 (c (n "st7032i") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "03n6a63l11nf1ig1lhjsl9kakqjac3cm9r7zbbm2zqjg9f6cg2w8")))

(define-public crate-st7032i-0.0.4 (c (n "st7032i") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0x6grbd4m3v4zc93g4d5s92v51r66cy9alwkq4d57bxjhbzqw99z")))

