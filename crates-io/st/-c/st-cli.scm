(define-module (crates-io st -c st-cli) #:use-module (crates-io))

(define-public crate-st-cli-0.3.3 (c (n "st-cli") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.0") (d #t) (k 0)))) (h "0k5zly7m73b09yrrzyfhsy3zza6za6470j3c25w2wclkyykarklr")))

(define-public crate-st-cli-0.3.5 (c (n "st-cli") (v "0.3.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.0") (d #t) (k 0)))) (h "0xcxd592insl3ybq0kw82f2a2z9f6shjwhazq3n2pqmh4vy9a4n9")))

(define-public crate-st-cli-0.3.6 (c (n "st-cli") (v "0.3.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.0") (d #t) (k 0)))) (h "1jys6q5zaskkbsxf835p9x7bf6ndlflgx5mpivdacn94wmsnh0x0")))

(define-public crate-st-cli-0.5.0 (c (n "st-cli") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.0") (d #t) (k 0)))) (h "15w5wc4azs1x3prxp9vxr9bxgy7n0xkr8hlkz1nrkbwpa4z5wbrr")))

(define-public crate-st-cli-0.6.1 (c (n "st-cli") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.0") (d #t) (k 0)))) (h "092dafs1qhmirc8y9n0grm8lq9l4vvab62jd6g3xmn30zn884s59")))

(define-public crate-st-cli-0.6.4 (c (n "st-cli") (v "0.6.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "which") (r "^4.0") (d #t) (k 0)))) (h "0qfjw8kxy2n8c8dr73ycjpj627m90wvwwq4k168dwgdmiydb8cpc")))

