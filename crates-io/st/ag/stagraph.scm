(define-module (crates-io st ag stagraph) #:use-module (crates-io))

(define-public crate-stagraph-0.1.0 (c (n "stagraph") (v "0.1.0") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "clilog") (r "^0.2.3") (d #t) (k 0)) (d (n "compact_str") (r "^0.7.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "libertyparse") (r "^0.3.0") (f (quote ("direction_provider"))) (d #t) (k 0)) (d (n "netlistdb") (r "^0.3.0") (d #t) (k 0)) (d (n "readonly") (r "^0.2.7") (d #t) (k 0)) (d (n "spefparse") (r "^0.2.0") (d #t) (k 0)) (d (n "ucc") (r "^0.2.0") (d #t) (k 1)) (d (n "ulib") (r "^0.3.1") (d #t) (k 0)) (d (n "zeroable") (r "^0.2.0") (d #t) (k 0)))) (h "0x9j39d0qhclwnll5b137pxv9icii2wdk87xwbza8202crg7h6ih") (l "stagraph")))

