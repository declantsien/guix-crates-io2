(define-module (crates-io st ag stag) #:use-module (crates-io))

(define-public crate-stag-0.0.1 (c (n "stag") (v "0.0.1") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "image") (r "^0.17.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "02ni0rljfsnqjf87b92026dmfxz05k3fk3bfazi5mcn9yk1d4cxw")))

(define-public crate-stag-0.1.1 (c (n "stag") (v "0.1.1") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "image") (r "^0.17.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "0y7l62cgfab3nkl0yxdvp7g0qckxv9i6xgp862vk96amri256p2m")))

(define-public crate-stag-0.2.0 (c (n "stag") (v "0.2.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "image") (r "^0.17.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "1cdw5mx0rgd4d33q5mpdlii7w346bmhb2hc3s6px7pc9md2fz2n3")))

(define-public crate-stag-0.3.0 (c (n "stag") (v "0.3.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "image") (r "^0.17.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "1yi8dlyhx7vl56bzk1c6qi8hcl7s94r6ciywfpw82yj5sc39mbyc")))

