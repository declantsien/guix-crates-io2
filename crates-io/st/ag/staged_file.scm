(define-module (crates-io st ag staged_file) #:use-module (crates-io))

(define-public crate-staged_file-0.1.0 (c (n "staged_file") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1xyrvzp3ygip4syq0mxgb3jsp6rpdrf0mcigj7fypvjbivzlhslf") (y #t)))

(define-public crate-staged_file-0.2.0 (c (n "staged_file") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0075pk6jr6kvma2dyg6njns5fygyxd33g29np94arkddmf6rwjzp") (y #t)))

(define-public crate-staged_file-0.3.0 (c (n "staged_file") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0x7qxnwq9spc237xlm8lqbdc6dax937an55qii935qxhgfh7y024") (y #t) (r "1.63.0")))

(define-public crate-staged_file-0.4.0 (c (n "staged_file") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0sdwl4pqda74slglqznilaxmk2ri9rkcz3llspkb4qr6wbg7wzab") (r "1.65.0")))

(define-public crate-staged_file-0.5.0 (c (n "staged_file") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 0)))) (h "147b330z9v4qw7g5h8iwnswvx0ai5qmf8ppx99vmarn0gl453p32") (r "1.69.0")))

