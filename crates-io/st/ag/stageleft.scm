(define-module (crates-io st ag stageleft) #:use-module (crates-io))

(define-public crate-stageleft-0.1.0 (c (n "stageleft") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "stageleft_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0lv1z57gvgvjxvmjk14apgsnm9s3qfbz3mq7ikzgrk7gczb04zda")))

(define-public crate-stageleft-0.2.0 (c (n "stageleft") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "stageleft_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "09cff8y53i24j6n703xdk4p07j57a9n977www3hqyzihpz627jd0")))

(define-public crate-stageleft-0.2.1 (c (n "stageleft") (v "0.2.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "stageleft_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1jmj5icqf9py4c8bxv76ywrrc98275zgbd37g2n5acvakrirqlca")))

(define-public crate-stageleft-0.3.0 (c (n "stageleft") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "stageleft_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0gkc1k9bzsbmjk1w1jvf7b2407bzyziv33c8hncrxwz285zsw8sv")))

