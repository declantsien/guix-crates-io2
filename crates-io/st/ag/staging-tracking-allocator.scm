(define-module (crates-io st ag staging-tracking-allocator) #:use-module (crates-io))

(define-public crate-staging-tracking-allocator-0.0.0 (c (n "staging-tracking-allocator") (v "0.0.0") (h "1mrwdycci9730hn3npji3gzq1fk30ba470ay2y8wcqabshphll7a")))

(define-public crate-staging-tracking-allocator-1.0.0 (c (n "staging-tracking-allocator") (v "1.0.0") (h "1nw7kr0z8jij7d96pzzc8197ma76q71v4nx96w64agi52p5axgam")))

(define-public crate-staging-tracking-allocator-2.0.0 (c (n "staging-tracking-allocator") (v "2.0.0") (h "0kf3j7060r66f7l1sb7p8a234jild5x788r7m6av3sihrqd3qv5g")))

