(define-module (crates-io st ag staged-builder) #:use-module (crates-io))

(define-public crate-staged-builder-0.1.0 (c (n "staged-builder") (v "0.1.0") (d (list (d (n "staged-builder-internals") (r "^0.1.0") (d #t) (k 0)))) (h "030vga0gggmjmfdjkvv5zgj24lizdiigfzlmyp59niff3xyms4yp")))

(define-public crate-staged-builder-0.1.1 (c (n "staged-builder") (v "0.1.1") (d (list (d (n "staged-builder-internals") (r "^0.1.1") (d #t) (k 0)))) (h "05bqb3v5rxbzhgd62xfmcnprfxcczv7b7y2gfh0wyh3m0aa6mvil")))

(define-public crate-staged-builder-0.1.2 (c (n "staged-builder") (v "0.1.2") (d (list (d (n "staged-builder-internals") (r "^0.1.2") (d #t) (k 0)))) (h "1dh7dkln21mhim3998w1483anqv79zq7vls2wqc4zkh2373bgvdi")))

(define-public crate-staged-builder-0.2.0 (c (n "staged-builder") (v "0.2.0") (d (list (d (n "staged-builder-internals") (r "^0.2.0") (d #t) (k 0)))) (h "155yx3w6bghkpmkq7z6g6s06m2p87bw80074bi1747ly2q99nrax")))

