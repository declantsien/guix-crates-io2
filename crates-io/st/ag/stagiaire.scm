(define-module (crates-io st ag stagiaire) #:use-module (crates-io))

(define-public crate-stagiaire-0.1.0 (c (n "stagiaire") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0aba2hkmffjxgscs78x0ndqh1fbrmd89qjssdmpjy6cm4x75f79w")))

(define-public crate-stagiaire-0.2.1 (c (n "stagiaire") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1jq4gkgrldlli9sasyn7d4avx4y7yc8vqj97655jbb616mndfk76")))

(define-public crate-stagiaire-0.3.0 (c (n "stagiaire") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hz8k4n867jck35j5azdinwp06kv6nnk87ch5d3nz5xnm2qhk5cw") (s 2) (e (quote (("serde" "dep:serde"))))))

