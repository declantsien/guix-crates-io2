(define-module (crates-io st ag staged-builder-internals) #:use-module (crates-io))

(define-public crate-staged-builder-internals-0.1.0 (c (n "staged-builder-internals") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1qdpvc9n0x8q559ydcz52km09vcgdg0qa6d270w5a27k3hai74i0")))

(define-public crate-staged-builder-internals-0.1.1 (c (n "staged-builder-internals") (v "0.1.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fmha84l7vyry02zfsraaipy23djcdwsq5f86fv3cfqyany2pm6x")))

(define-public crate-staged-builder-internals-0.1.2 (c (n "staged-builder-internals") (v "0.1.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0fh4rai8gz3qq6lym462fyg5p8c9bajvmabvblib0p4d759ymnqy")))

(define-public crate-staged-builder-internals-0.2.0 (c (n "staged-builder-internals") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "structmeta") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "131s0vks107z8axjh9s1sd6q35y681wj60hq0yfdw0ig3bghdaji")))

