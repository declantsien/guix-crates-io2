(define-module (crates-io st ag stage) #:use-module (crates-io))

(define-public crate-stage-0.1.0 (c (n "stage") (v "0.1.0") (h "1d0izanj3xh6f333vyr8yf4sx00g7110ax578zkyn8lmazzwpg95")))

(define-public crate-stage-0.2.0 (c (n "stage") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.2") (d #t) (k 0)) (d (n "dashmap") (r "^3.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "mirai") (r "^0.0.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.13") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.0") (d #t) (k 2)))) (h "08rqkv6frk4k0zdyy7043y5sbkrrw671zyc9zzn4fngl5q4k78hp") (f (quote (("use-mirai" "mirai") ("udp" "use-mirai" "mirai/udp") ("tcp" "use-mirai" "mirai/tcp") ("default" "tcp" "udp"))))))

