(define-module (crates-io st ag stageleft_tool) #:use-module (crates-io))

(define-public crate-stageleft_tool-0.1.0 (c (n "stageleft_tool") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "sha256") (r "^1.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "syn-inline-mod") (r "^0.6.0") (d #t) (k 0)))) (h "0d2ni2jfmplv55m721053b8xm2v8vyp0xck3g4b2ykfpshksq7b6")))

(define-public crate-stageleft_tool-0.1.1 (c (n "stageleft_tool") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "sha256") (r "^1.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "syn-inline-mod") (r "^0.6.0") (d #t) (k 0)))) (h "1blhbr5wjh1l8yqsm4pzmb4ris9kxcn9ncrdg4mnvwq2x5s80nmy")))

(define-public crate-stageleft_tool-0.2.0 (c (n "stageleft_tool") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "sha256") (r "^1.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "syn-inline-mod") (r "^0.6.0") (d #t) (k 0)))) (h "12a9wvnyyv3apwp3pi6v697frzvcxx4mkfpa0yrvn946j5m1afj1")))

