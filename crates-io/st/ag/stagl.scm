(define-module (crates-io st ag stagl) #:use-module (crates-io))

(define-public crate-stagl-0.1.0 (c (n "stagl") (v "0.1.0") (d (list (d (n "posh") (r "^0.1") (d #t) (k 0)))) (h "1vr348v4shhf5ljf2571593bcp47hp8jjfd2rc2qvd2x8jpym13b")))

(define-public crate-stagl-0.1.1 (c (n "stagl") (v "0.1.1") (h "0s2pmhk5nmw6hddn8iqfywxqwsyx7fnda7vvc05g4f44zrr56f49")))

