(define-module (crates-io st ap stapifaction_derive) #:use-module (crates-io))

(define-public crate-stapifaction_derive-0.0.1 (c (n "stapifaction_derive") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0p6m6iaqm102qijfknrpiz4smmql6nwaki7b17dajyl0hhm3kaf5")))

(define-public crate-stapifaction_derive-0.0.2 (c (n "stapifaction_derive") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0j0haaryqrj1c0vj1dgdgfgzhjs0rk2s1akmdkmr0q1s1d1njhdy")))

