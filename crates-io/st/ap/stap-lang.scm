(define-module (crates-io st ap stap-lang) #:use-module (crates-io))

(define-public crate-stap-lang-0.2.0 (c (n "stap-lang") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "rail-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1gxc74qfpsvahbk3kg8jr52wrv28yblgs9vs38zbwq491zpz8zs7")))

(define-public crate-stap-lang-0.2.1 (c (n "stap-lang") (v "0.2.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "rail-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "19ps1fni2skwx60k7vlvy9hbvplx036g5hw71gl0rvf0glpd87rz")))

