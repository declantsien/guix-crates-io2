(define-module (crates-io st it stitchy-core) #:use-module (crates-io))

(define-public crate-stitchy-core-0.1.0 (c (n "stitchy-core") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("jpeg" "png" "gif" "bmp"))) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (o #t) (d #t) (k 0)))) (h "16c8jqcipmc9v2aqx6dijjhdqnlrr8jhg7hbpnk9fq17yxy4grc4") (s 2) (e (quote (("parser" "dep:clap" "dep:serde" "dep:serde_json"))))))

(define-public crate-stitchy-core-0.1.1 (c (n "stitchy-core") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("jpeg" "png" "gif" "bmp"))) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (o #t) (d #t) (k 0)))) (h "01in3g5c2b6zfch97d1r08b6iafcvbcz2l4ks1w6dh5z4wskfzwk") (s 2) (e (quote (("parser" "dep:clap" "dep:serde" "dep:serde_json"))))))

(define-public crate-stitchy-core-0.1.2 (c (n "stitchy-core") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "image") (r "^0.24.9") (f (quote ("jpeg" "png" "gif" "bmp"))) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (o #t) (d #t) (k 0)))) (h "12cjjvdhzngja7pj5kqpz63nx76ip9kzgz7bdi9cqgd6dyigv11w") (s 2) (e (quote (("parser" "dep:clap" "dep:serde" "dep:serde_json"))))))

