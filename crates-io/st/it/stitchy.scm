(define-module (crates-io st it stitchy) #:use-module (crates-io))

(define-public crate-stitchy-0.1.2 (c (n "stitchy") (v "0.1.2") (d (list (d (n "image") (r "^0.23.4") (f (quote ("jpeg" "png" "gif" "bmp"))) (k 0)) (d (n "structopt") (r "^0.3.14") (k 0)))) (h "1a22a0nrv4ffd15dsnja20vx8x4h7pj4xbak613yiyjbld8i6s6p")))

(define-public crate-stitchy-0.1.3 (c (n "stitchy") (v "0.1.3") (d (list (d (n "image") (r "^0.23.4") (f (quote ("jpeg" "png" "gif" "bmp"))) (k 0)) (d (n "structopt") (r "^0.3.14") (k 0)))) (h "1jc03fqf746khq07xv9lxqgqpl53nq5c4s6gmwd58rgzras0pg6d")))

(define-public crate-stitchy-0.1.4 (c (n "stitchy") (v "0.1.4") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "image") (r "^0.24.0") (f (quote ("jpeg" "png" "gif" "bmp"))) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)))) (h "1jwmiy77ha6bljql574xxs6z03gwp7n2nv0zzrcmwmxvxp2yc8r5")))

(define-public crate-stitchy-0.1.5 (c (n "stitchy") (v "0.1.5") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("std" "derive"))) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "stitchy-core") (r "^0.1.0") (f (quote ("parser"))) (d #t) (k 0)))) (h "1iyyzikpahkpadx5lgfsl0j8l9h2kbls28j0c33v3bvvsjmls7q8")))

(define-public crate-stitchy-0.1.6 (c (n "stitchy") (v "0.1.6") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("std" "derive"))) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "stitchy-core") (r "^0.1.1") (f (quote ("parser"))) (d #t) (k 0)))) (h "066rg20vzkmzzl84f4ygpfyin1g4bh19xsxlp29lp1s5mi0vc30v")))

(define-public crate-stitchy-0.1.7 (c (n "stitchy") (v "0.1.7") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("std" "derive"))) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "stitchy-core") (r "^0.1.2") (f (quote ("parser"))) (d #t) (k 0)))) (h "0jc4zvryr054pmy8aj41ysi59d3srj05ikcvhw6bf6r87wx0qspq")))

