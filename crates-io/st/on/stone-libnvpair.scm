(define-module (crates-io st on stone-libnvpair) #:use-module (crates-io))

(define-public crate-stone-libnvpair-0.2.0 (c (n "stone-libnvpair") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "stone-libnvpair-sys") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a2bgjb10b69wjdbyskbyqyx81zfl4w0pwwfsc0ngm9naw793676")))

