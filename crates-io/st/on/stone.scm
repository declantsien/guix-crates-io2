(define-module (crates-io st on stone) #:use-module (crates-io))

(define-public crate-stone-0.0.0 (c (n "stone") (v "0.0.0") (h "14mswi18hryvpiiglmgwf77hp82c9ra285a2ckrv4bz5rr8y657c") (y #t)))

(define-public crate-stone-0.0.1 (c (n "stone") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "lasso") (r "^0.3.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1wrb2qhi90hv2fgh13f67wkw30a02m37n2rm405326gmrkyhb2pl") (y #t)))

(define-public crate-stone-0.0.2 (c (n "stone") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dynasmrt") (r "^1.0.0") (d #t) (k 0)) (d (n "lasso") (r "^0.3.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "13kradnkw08n8l6mpc7z2xi0xyhq9dq71yl8nm37j0d5aa0lmnj2") (y #t)))

(define-public crate-stone-0.0.3 (c (n "stone") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dynasmrt") (r "^1.0.0") (d #t) (k 0)) (d (n "lasso") (r "^0.3.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0qyfibnsgadx9zmswrh5bsa08iagfnh1hdi0z4nhmsa5d9ivnpl8") (y #t)))

