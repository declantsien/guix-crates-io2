(define-module (crates-io st on stonks) #:use-module (crates-io))

(define-public crate-stonks-0.1.0 (c (n "stonks") (v "0.1.0") (h "11gh90ppbab6lm7jrf3f0gkgvwpwnsl97b4clq0a6bzls6d3fw24")))

(define-public crate-stonks-0.1.1 (c (n "stonks") (v "0.1.1") (h "1q7fiwl35fyf6llamflaw5vdcakbmwag2br7457p87nz1dm5sxjp")))

(define-public crate-stonks-0.2.0 (c (n "stonks") (v "0.2.0") (h "1qkg0ls527l7nflsjk8cxj0f7v4b5i7zqg7qmylywzagxsajcvms")))

