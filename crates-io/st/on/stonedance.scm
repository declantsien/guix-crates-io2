(define-module (crates-io st on stonedance) #:use-module (crates-io))

(define-public crate-stonedance-0.1.0 (c (n "stonedance") (v "0.1.0") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "volmark") (r "^0.1.0") (d #t) (k 0)))) (h "0d34wk3bf6x41r9bb9vjbjslkn1sd7x2q408z4b90dw7pcic1l1v")))

