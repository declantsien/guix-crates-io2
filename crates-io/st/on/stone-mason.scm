(define-module (crates-io st on stone-mason) #:use-module (crates-io))

(define-public crate-stone-mason-0.1.0 (c (n "stone-mason") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "aws-config") (r "^1.0.1") (f (quote ("behavior-version-latest"))) (d #t) (k 2)) (d (n "aws-sdk-bedrockruntime") (r "^1.1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1dizp0d152s7fjrs8y5jrlayclli4srpv9vkszfskf6ycfhd7kq4")))

