(define-module (crates-io st on stone-libnvpair-sys) #:use-module (crates-io))

(define-public crate-stone-libnvpair-sys-0.2.0 (c (n "stone-libnvpair-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zv1nsmxhv13hgm78scvv9airyn710plprmj7v28mnfsykl5avsk")))

