(define-module (crates-io st dt stdto_derive) #:use-module (crates-io))

(define-public crate-stdto_derive-0.1.0 (c (n "stdto_derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ggpavfy6lbjpr67wxb3l4jwzqh3zqydkn1anxk6zmqnbn3spqhk")))

(define-public crate-stdto_derive-0.2.0 (c (n "stdto_derive") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "141jfmkchpld2yyzjvjnm673cf3sv30cf6rmlrzr2wyv3b55s3xy")))

(define-public crate-stdto_derive-0.3.0 (c (n "stdto_derive") (v "0.3.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05h316pc84h2z55i9dyq09568v40pacy02fcdlj864p4kwp07jn5")))

(define-public crate-stdto_derive-0.4.0 (c (n "stdto_derive") (v "0.4.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1il6r7khz4wrf1q37z5qb6sqiqk2jh0zfgbkcypclkqj9jcaqnh3")))

(define-public crate-stdto_derive-0.5.0 (c (n "stdto_derive") (v "0.5.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q1m6245h4s98flhgprbshrhsdjilvkai9cn9jkwd8akxdc42id4")))

(define-public crate-stdto_derive-0.6.0 (c (n "stdto_derive") (v "0.6.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nh6y61420032gg6b381smzw6pgxycsxjgnqbyjbz57dgj63vvsx")))

(define-public crate-stdto_derive-0.7.0 (c (n "stdto_derive") (v "0.7.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c7vzp83zk0hnj8bgvfnkz6c6xs9rhcvqk8lgvx3f8lrr9f9z1y3")))

(define-public crate-stdto_derive-0.8.0 (c (n "stdto_derive") (v "0.8.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0503bjv0kcr98i1vn4v2ji2pkkhzvfxgjn7rw84wxvkpsci3728n")))

(define-public crate-stdto_derive-0.9.0 (c (n "stdto_derive") (v "0.9.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fkh2bwg1h7awcqp09hk4k8a0h8rkl97zzmdzwmb13cc1517hcy4")))

(define-public crate-stdto_derive-0.10.0 (c (n "stdto_derive") (v "0.10.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13j2rs1f4fzbn1rhpzaqlin5s2sqk5h6y70zj4iw5n45cm0vsd55")))

(define-public crate-stdto_derive-0.11.0 (c (n "stdto_derive") (v "0.11.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15g1iwda34hxkyqyw945pnjv7sxbqshb1vfkrnw8fbywfjib8m4d")))

(define-public crate-stdto_derive-0.12.0 (c (n "stdto_derive") (v "0.12.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wz2w5iql560qiv6hcfx1w4k82lkpnyyq3h6p7s5xxd4j16dalva")))

(define-public crate-stdto_derive-0.13.0 (c (n "stdto_derive") (v "0.13.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kpiby3dvaf1s2aib0gfqpihljjn71yiqylw6flllyxsp0fsdza2")))

(define-public crate-stdto_derive-0.14.0 (c (n "stdto_derive") (v "0.14.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17xsp1y08kfsjrirah9niplf5fr7y5i0xrcqp37dlmclkydf068n")))

