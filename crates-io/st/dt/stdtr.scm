(define-module (crates-io st dt stdtr) #:use-module (crates-io))

(define-public crate-stdtr-0.1.0 (c (n "stdtr") (v "0.1.0") (d (list (d (n "failure") (r "~0.1.5") (d #t) (k 0)) (d (n "stdtr-sys") (r "~0.1") (d #t) (k 0)))) (h "1pw07hc9dqr9k7n9wppkjkh5bqaq6dslyl0vxi2z3qrihj58qsi6")))

(define-public crate-stdtr-0.2.0 (c (n "stdtr") (v "0.2.0") (d (list (d (n "stdtr-sys") (r "~0.1") (d #t) (k 0)))) (h "1y0vkin3vyy1l2lspywz4jd7dzs43z0mcx2djbzbl22pg2jm59ac")))

