(define-module (crates-io st _r st_ring_buffer) #:use-module (crates-io))

(define-public crate-st_ring_buffer-0.1.0 (c (n "st_ring_buffer") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^2") (d #t) (k 2)))) (h "0havwsgavc83dzkbdbq3x7fkgyli5di2sdirpiqmyxy0nw628xhl")))

(define-public crate-st_ring_buffer-0.2.0 (c (n "st_ring_buffer") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^2") (d #t) (k 2)))) (h "1ah7l6z1bh5l72h8cscgrli0gdd624fx3z4bi3lrrxdzs06n1pn5") (f (quote (("std") ("default"))))))

(define-public crate-st_ring_buffer-1.0.0 (c (n "st_ring_buffer") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^2") (d #t) (k 2)))) (h "1sl6x2ch54d4w77jdf80763pydxafszmk9fxi8rpnrfivmcx4zbg") (f (quote (("std") ("default"))))))

