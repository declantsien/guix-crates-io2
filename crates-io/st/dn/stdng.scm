(define-module (crates-io st dn stdng) #:use-module (crates-io))

(define-public crate-stdng-0.1.3 (c (n "stdng") (v "0.1.3") (h "1qnky6806mvvxbfi4bi399zbp38p5sixdr7vnsk33mgnvjzlq1gb")))

(define-public crate-stdng-0.1.4 (c (n "stdng") (v "0.1.4") (h "0zh5h3fqsbjb3wz6x7dcrldxs3hw124hz6lmni8h1w6jd8ymschs")))

(define-public crate-stdng-0.1.5 (c (n "stdng") (v "0.1.5") (h "19n2z0wxnnwxj6z85qnfvb9h5f04ca36kwkxvzqr2lchxm7cmg5v")))

(define-public crate-stdng-0.1.6 (c (n "stdng") (v "0.1.6") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1a9pwxyan8whj88b8mi8rhiwa265v2r10w1d0dcvqhp59zs2vxlv")))

