(define-module (crates-io st ai stainless2) #:use-module (crates-io))

(define-public crate-stainless2-0.1.12 (c (n "stainless2") (v "0.1.12") (h "18fx2snj9v0wibc71lnblqlngr8iapiilyzmk1b9m176ssqxj8r5")))

(define-public crate-stainless2-0.1.13 (c (n "stainless2") (v "0.1.13") (h "1j4ys32j89xf91z4lzk4qm4qqrfpjj3c58bpz62cabfzsg7qyd32")))

(define-public crate-stainless2-0.1.14 (c (n "stainless2") (v "0.1.14") (h "0ifc6zklxvb42958c2nwq814jpql5k31jwwk0vbal7y9i7kvclfs")))

