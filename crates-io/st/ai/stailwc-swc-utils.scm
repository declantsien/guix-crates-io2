(define-module (crates-io st ai stailwc-swc-utils) #:use-module (crates-io))

(define-public crate-stailwc-swc-utils-0.8.0 (c (n "stailwc-swc-utils") (v "0.8.0") (d (list (d (n "swc_core") (r "=0.40.16") (f (quote ("common" "ecma_visit" "ecma_ast"))) (d #t) (k 0)))) (h "0487dsrp4cj13d8k1alyl9lk3fxm9vpb52b6hk835av2wdlsn783")))

(define-public crate-stailwc-swc-utils-0.8.1 (c (n "stailwc-swc-utils") (v "0.8.1") (d (list (d (n "swc_core") (r "=0.40.16") (f (quote ("common" "ecma_visit" "ecma_ast"))) (d #t) (k 0)))) (h "1ppdjj35s1pl6jxlxf49rkig2wpmn7y89kd1vxf9lljbx4li8pbj")))

(define-public crate-stailwc-swc-utils-0.9.0 (c (n "stailwc-swc-utils") (v "0.9.0") (d (list (d (n "swc_core") (r "=0.40.16") (f (quote ("common" "ecma_visit" "ecma_ast"))) (d #t) (k 0)))) (h "0wklvf5bynl7syvmyqq6qz015wac1j8m6ys8xay44jn3d3v30mxd")))

(define-public crate-stailwc-swc-utils-0.10.0 (c (n "stailwc-swc-utils") (v "0.10.0") (d (list (d (n "swc_core") (r "=0.40.16") (f (quote ("common" "ecma_visit" "ecma_ast"))) (d #t) (k 0)))) (h "05v00fig6ihqslnnz503q1dy4h0rfsc7agg6gbaq4sj8zm28wdjh")))

(define-public crate-stailwc-swc-utils-0.11.0 (c (n "stailwc-swc-utils") (v "0.11.0") (d (list (d (n "swc_core") (r "=0.40.16") (f (quote ("common" "ecma_visit" "ecma_ast"))) (d #t) (k 0)))) (h "10lk3hg487czxwm0n4kvb60lyi8lhnnbjklzhkrk3p13yxhscbcw")))

(define-public crate-stailwc-swc-utils-0.11.1 (c (n "stailwc-swc-utils") (v "0.11.1") (d (list (d (n "swc_core") (r "=0.40.16") (f (quote ("common" "ecma_visit" "ecma_ast"))) (d #t) (k 0)))) (h "1zmaq4ynqm75c034qp40hgs21yncpisi03rfw8b3n850vz8skmss")))

(define-public crate-stailwc-swc-utils-0.12.0 (c (n "stailwc-swc-utils") (v "0.12.0") (d (list (d (n "swc_core") (r "=0.40.16") (f (quote ("common" "ecma_visit" "ecma_ast"))) (d #t) (k 0)))) (h "0r1iqdrhxwpqp2w6cx02hyfa7q1h7601iy4gpm6aamqc99v7js23")))

(define-public crate-stailwc-swc-utils-0.13.0 (c (n "stailwc-swc-utils") (v "0.13.0") (d (list (d (n "swc_core") (r "=0.55.5") (f (quote ("common" "ecma_visit" "ecma_ast"))) (d #t) (k 0)))) (h "1klpijx3rzyw6l6k2vhk203hbrg44cpj7nrj7i2rd4nmf7dqyk8j")))

(define-public crate-stailwc-swc-utils-0.14.0 (c (n "stailwc-swc-utils") (v "0.14.0") (d (list (d (n "swc_core") (r "=0.55.5") (f (quote ("common" "ecma_visit" "ecma_ast"))) (d #t) (k 0)))) (h "0qzzdbighxwhf5z8nmqds3s4mgg5j3jnp6pz8wn4rdv2554b20x6")))

(define-public crate-stailwc-swc-utils-0.15.0 (c (n "stailwc-swc-utils") (v "0.15.0") (d (list (d (n "swc_core") (r "=0.55.5") (f (quote ("common" "ecma_visit" "ecma_ast"))) (d #t) (k 0)))) (h "07l5iylygfrzyyymz38mh9i03fmx0mm8db52viwkhrkfrl92avqf")))

(define-public crate-stailwc-swc-utils-0.16.0 (c (n "stailwc-swc-utils") (v "0.16.0") (d (list (d (n "swc_core") (r "=0.76.9") (f (quote ("common" "ecma_visit" "ecma_ast"))) (d #t) (k 0)))) (h "01wmy2k6r4zkkabi5sflzfg5mbinv5ckf5za6pqszn9aii4acnp2")))

