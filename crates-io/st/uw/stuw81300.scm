(define-module (crates-io st uw stuw81300) #:use-module (crates-io))

(define-public crate-stuw81300-0.1.0 (c (n "stuw81300") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "micromath") (r "^2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1jfcbar7nqvh8fdfngmc1w7yi3b7sfpss5n6xpsc9i5y5c4h9q8j")))

(define-public crate-stuw81300-0.2.0 (c (n "stuw81300") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "micromath") (r "^2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "00h3gnb8b8bi4fxyl8w41y9q1vjdjmq7ps3sd5079qs1km07wvcm")))

