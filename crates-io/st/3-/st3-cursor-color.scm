(define-module (crates-io st #{3-}# st3-cursor-color) #:use-module (crates-io))

(define-public crate-st3-cursor-color-0.1.0 (c (n "st3-cursor-color") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "palette") (r "^0.2.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "08q8q5swvw0hm4w2vm0jywi8p69zs0199j8wwybpsz8i1kjrrh9c")))

