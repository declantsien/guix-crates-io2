(define-module (crates-io st ep stepflow-json) #:use-module (crates-io))

(define-public crate-stepflow-json-0.0.1 (c (n "stepflow-json") (v "0.0.1") (d (list (d (n "stepflow") (r "^0.0.7") (k 0)) (d (n "tinyjson") (r "^2.2.0") (d #t) (k 0)))) (h "1npk1dgmiwq4pxqaimllid4qs623y7p0m5m7zx1r7yzlzqb1vcz3")))

(define-public crate-stepflow-json-0.0.2 (c (n "stepflow-json") (v "0.0.2") (d (list (d (n "stepflow") (r "^0.0.7") (k 0)) (d (n "tinyjson") (r "^2.2.0") (d #t) (k 0)))) (h "12gxh430y4gsydlx5j7pywy7b1smlm53dmr4w89xym9yir3mfx4j")))

(define-public crate-stepflow-json-0.0.3 (c (n "stepflow-json") (v "0.0.3") (d (list (d (n "stepflow") (r "^0.0.8") (k 0)) (d (n "tinyjson") (r "^2.2.0") (d #t) (k 0)))) (h "0bkrsxakifzkadavj0afnfbbglq114nd91di9v5ghlmk1ymzznq9")))

