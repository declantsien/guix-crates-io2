(define-module (crates-io st ep stepwise) #:use-module (crates-io))

(define-public crate-stepwise-0.0.0 (c (n "stepwise") (v "0.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "12sm4zxkjxw4cgz73iynsbbf3pr54hgb5h8nj8skgay70wbhzhxh")))

(define-public crate-stepwise-0.0.1 (c (n "stepwise") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1gmixg1wc8a81942x7k9lqj0jalbn4hr1aan12lqksr6lqmaawym")))

