(define-module (crates-io st ep stepper_macros) #:use-module (crates-io))

(define-public crate-stepper_macros-0.1.0 (c (n "stepper_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1j9h90q5gi3hbbhxqw1j1d76ipr7p3zhx61abzld192097661gv8") (y #t)))

(define-public crate-stepper_macros-0.1.1 (c (n "stepper_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0n8fk7fdzpyhwn4ly09acszchhkl7ka5mghh27gs9d1kfnznk99z") (y #t)))

