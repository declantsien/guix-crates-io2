(define-module (crates-io st ep stepper-interface) #:use-module (crates-io))

(define-public crate-stepper-interface-0.1.0 (c (n "stepper-interface") (v "0.1.0") (d (list (d (n "fluence") (r "=0.2.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.20") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "11vv5d1z6yn24vgh63i42vc4dksqbdxhkqk2vwy16gx3m3997gm2")))

(define-public crate-stepper-interface-0.1.1 (c (n "stepper-interface") (v "0.1.1") (d (list (d (n "fluence") (r "=0.2.13") (d #t) (k 0)) (d (n "fluence-it-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)))) (h "1pbd4i9scvpz15dxvhczv8bbxsgygmp7nqq34z2r1w7vpl1sk6cd")))

(define-public crate-stepper-interface-0.1.2 (c (n "stepper-interface") (v "0.1.2") (d (list (d (n "fluence") (r "^0.2.18") (d #t) (k 0)) (d (n "fluence-it-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (d #t) (k 0)))) (h "09vd6amjl8v81wl2vln9p97skhm6h4a1m9jmdwxshnr0cqsydgcm")))

