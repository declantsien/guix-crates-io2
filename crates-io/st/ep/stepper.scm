(define-module (crates-io st ep stepper) #:use-module (crates-io))

(define-public crate-stepper-0.1.0 (c (n "stepper") (v "0.1.0") (h "0ajj3cp46xz99dzsk5r5q1vmmj7i59rfq58lbkw86v41vmcif7b6") (y #t)))

(define-public crate-stepper-0.5.0 (c (n "stepper") (v "0.5.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (d #t) (k 0)) (d (n "embedded-time") (r "^0.10.1") (d #t) (k 0)) (d (n "fixed") (r "^1.6.0") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.3") (d #t) (k 0)) (d (n "ramp-maker") (r "^0.2.0") (d #t) (k 0)) (d (n "replace_with") (r "^0.1.7") (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 2)))) (h "0rdj9vniwa6cmyqlify73fxv4g17nr975vmdy2s6b26ciy1g6599") (f (quote (("stspin220") ("drv8825") ("default" "drv8825" "stspin220"))))))

(define-public crate-stepper-0.6.0 (c (n "stepper") (v "0.6.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "embedded-hal-stable") (r "^0.2.4") (d #t) (k 0) (p "embedded-hal")) (d (n "fixed") (r "^1.6.0") (d #t) (k 2)) (d (n "fugit") (r "^0.3.5") (d #t) (k 0)) (d (n "fugit-timer") (r "^0.1.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.3") (d #t) (k 0)) (d (n "ramp-maker") (r "^0.2.0") (d #t) (k 0)) (d (n "replace_with") (r "^0.1.7") (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 2)))) (h "0sw6ksrhvmprbg4ljj8iw1cw62gxajhddwdj8lm279l3mkpn5vmf") (f (quote (("stspin220") ("drv8825") ("dq542ma") ("default" "drv8825" "stspin220" "dq542ma"))))))

