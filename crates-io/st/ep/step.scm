(define-module (crates-io st ep step) #:use-module (crates-io))

(define-public crate-step-0.1.0 (c (n "step") (v "0.1.0") (h "0mirvhmlypdpq1s5vlwmd4n19d5sy3hnhk6siwz8fg9prbil1gpk") (y #t)))

(define-public crate-step-0.2.0 (c (n "step") (v "0.2.0") (h "1g7hc0msx0kcxn0v6iwfck74rg20mzaik6ybkkgarl5p6ziqx29y") (y #t)))

