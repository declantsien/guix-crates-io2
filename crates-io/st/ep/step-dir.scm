(define-module (crates-io st ep step-dir) #:use-module (crates-io))

(define-public crate-step-dir-0.2.0 (c (n "step-dir") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "embedded-time") (r "^0.10.0") (d #t) (k 0)))) (h "0xkw4cja2g6xpnwj476blh0n2id7hl6sykdwq8jg928jks843lz9") (f (quote (("stspin220") ("default" "stspin220"))))))

(define-public crate-step-dir-0.3.0 (c (n "step-dir") (v "0.3.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (d #t) (k 0)) (d (n "embedded-time") (r "^0.10.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.3") (d #t) (k 0)))) (h "1c4vmgk5fi6n4iwy89ixa048il4pnmv0w44vksqq81h32lsv7bqw") (f (quote (("stspin220") ("drv8825") ("default" "drv8825" "stspin220"))))))

(define-public crate-step-dir-0.4.0 (c (n "step-dir") (v "0.4.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (d #t) (k 0)) (d (n "embedded-time") (r "^0.10.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.3") (d #t) (k 0)))) (h "1l7qpminlg6qalryr1mqg4lsvmr4bwxxlhlnwhr48yf4znaqvxx1") (f (quote (("stspin220") ("drv8825") ("default" "drv8825" "stspin220"))))))

(define-public crate-step-dir-0.4.1 (c (n "step-dir") (v "0.4.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (d #t) (k 0)) (d (n "embedded-time") (r "^0.10.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.3") (d #t) (k 0)))) (h "013y3wb5gjfy875sqdgw65gpc7i0mjba297xdxsc2lfv67yawlav") (f (quote (("stspin220") ("drv8825") ("default" "drv8825" "stspin220"))))))

(define-public crate-step-dir-0.5.0 (c (n "step-dir") (v "0.5.0") (h "1rya10y3d6k2gpzmjxm8bavgn49fq5sz9aay32fwqwz2srcwn1vv")))

