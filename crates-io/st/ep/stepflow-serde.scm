(define-module (crates-io st ep stepflow-serde) #:use-module (crates-io))

(define-public crate-stepflow-serde-0.0.1 (c (n "stepflow-serde") (v "0.0.1") (h "0jy58b797r774nsis3kr21zn1wcnchb2sn2dy4l3anyd13zh9437")))

(define-public crate-stepflow-serde-0.0.2 (c (n "stepflow-serde") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)) (d (n "stepflow") (r "^0.0.2") (d #t) (k 0)))) (h "0k38nh0j174w4dlysjfvv37843lwxrah26aw383vm98pvrvx0cd2")))

(define-public crate-stepflow-serde-0.0.3 (c (n "stepflow-serde") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)) (d (n "stepflow") (r "^0.0.4") (d #t) (k 0)))) (h "0lbifw4dnx46h85ydpzqsm92qqrka1cylzsk4985ylqbm5avw5qv")))

(define-public crate-stepflow-serde-0.0.4 (c (n "stepflow-serde") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)) (d (n "stepflow") (r "^0.0.5") (d #t) (k 0)))) (h "00jcahpwfb4n5w3ra1rkkh4z2dvql08wbvnqlim33qsjcc146rhi")))

(define-public crate-stepflow-serde-0.0.5 (c (n "stepflow-serde") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)) (d (n "stepflow") (r "^0.0.6") (d #t) (k 0)))) (h "1ga77dq4lwd9632jq78dq3gjpq3rm13v3qgjklq6p50nhglfn31r")))

