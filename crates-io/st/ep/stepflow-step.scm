(define-module (crates-io st ep stepflow-step) #:use-module (crates-io))

(define-public crate-stepflow-step-0.0.1 (c (n "stepflow-step") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stepflow-base") (r "^0.0.1") (d #t) (k 0)) (d (n "stepflow-data") (r "^0.0.1") (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "0kpwxsy489vc1jnayw7jsg2gli55azwrf48vl07li1a2sx8ma2dr")))

(define-public crate-stepflow-step-0.0.2 (c (n "stepflow-step") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stepflow-base") (r "^0.0.2") (d #t) (k 0)) (d (n "stepflow-data") (r "^0.0.2") (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "1nnhs4z85hdhvr5j3ykg04vcnsx99kvz7c2kgxp2jlz4frz35yq6")))

(define-public crate-stepflow-step-0.0.3 (c (n "stepflow-step") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stepflow-base") (r "^0.0.3") (d #t) (k 0)) (d (n "stepflow-data") (r "^0.0.3") (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "0d8wbwx8c8k9339byfc88jdlh3m1225khnxfqilxsbs1yabci5zb")))

(define-public crate-stepflow-step-0.0.4 (c (n "stepflow-step") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stepflow-base") (r "^0.0.4") (d #t) (k 0)) (d (n "stepflow-data") (r "^0.0.4") (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "0j94qhm3xivjqn2vwc5hghmjj9aq9mwh9cdfs10c5k21sgwraw4p")))

(define-public crate-stepflow-step-0.0.5 (c (n "stepflow-step") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "stepflow-base") (r "^0.0.5") (d #t) (k 0)) (d (n "stepflow-data") (r "^0.0.5") (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "1b5j8kd273qklnb5ppxy3z055wnzkyk8y61xb45k7h38cv3k68vj") (f (quote (("serde-support" "serde" "stepflow-base/serde-support" "stepflow-data/serde-support"))))))

