(define-module (crates-io st ep stepflow-data) #:use-module (crates-io))

(define-public crate-stepflow-data-0.0.1 (c (n "stepflow-data") (v "0.0.1") (d (list (d (n "http") (r "^0.2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "stepflow-base") (r "^0.0.1") (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "121r60cpsrf8yc7ijnsndglfxzrhl5cvrw4fr0xs47x6m4cqa5cm")))

(define-public crate-stepflow-data-0.0.2 (c (n "stepflow-data") (v "0.0.2") (d (list (d (n "http") (r "^0.2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "stepflow-base") (r "^0.0.2") (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "0izkyz7di055gg7507079jliq6358rf9wacp72pv80alwpml1ia9")))

(define-public crate-stepflow-data-0.0.3 (c (n "stepflow-data") (v "0.0.3") (d (list (d (n "http") (r "^0.2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "stepflow-base") (r "^0.0.3") (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "1nnysafjd05fkwi5igqc3h18h8gkpmq4vpn40wari99nyjdn803n")))

(define-public crate-stepflow-data-0.0.4 (c (n "stepflow-data") (v "0.0.4") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "stepflow-base") (r "^0.0.4") (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "0v57azsclbl83k6i7aq0h4fpk3yfkmr309w5za7f2w9r1v0zf7d5")))

(define-public crate-stepflow-data-0.0.5 (c (n "stepflow-data") (v "0.0.5") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 2)) (d (n "regex") (r "^1.4.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "stepflow-base") (r "^0.0.5") (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "1mm02x7hvyvxrzbz5cz6c3dknnv3cliws9vksdaya4b2bn1mfdc3") (f (quote (("serde-support" "serde"))))))

