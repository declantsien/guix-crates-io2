(define-module (crates-io st ep stepflow-base) #:use-module (crates-io))

(define-public crate-stepflow-base-0.0.1 (c (n "stepflow-base") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10p0sclkyr0yvxns3yrgsrzgaizlmsp74h40gxpsk2rbmj96hr33") (f (quote (("test"))))))

(define-public crate-stepflow-base-0.0.2 (c (n "stepflow-base") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1clxkfifgq3z8d4ik002yail36lanzfiqhxhh9dsp55c4f1x6mdj") (f (quote (("test"))))))

(define-public crate-stepflow-base-0.0.3 (c (n "stepflow-base") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "0kf4cym3q833yjga8f6asq7nc1r6wgi7rzdh73xpjfgm67lwv311") (f (quote (("test"))))))

(define-public crate-stepflow-base-0.0.4 (c (n "stepflow-base") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "0nw8qm3w6pi0c3cp0ypdj6f2xj7b1h1m0q6qf88vmnwzfzdpz6db") (f (quote (("test"))))))

(define-public crate-stepflow-base-0.0.5 (c (n "stepflow-base") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "stepflow-test-util") (r "^0.0.1") (d #t) (k 2)))) (h "1ihiwizdp902ml91726c44hggihc5l1h6j90qgzhfp62n2gxjnvg") (f (quote (("serde-support" "serde"))))))

