(define-module (crates-io st dl stdlib-rs) #:use-module (crates-io))

(define-public crate-stdlib-rs-0.0.1 (c (n "stdlib-rs") (v "0.0.1") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0ls7pb2m6dfmgvwb1bqxmv1xn25m5dvikw2kc2pi792r6l2rklh9") (f (quote (("build_deps" "man"))))))

(define-public crate-stdlib-rs-0.0.2 (c (n "stdlib-rs") (v "0.0.2") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0zyvys18d6j4h3v0fksk0gg3v7a9zb0ncnl9am4cbxrgh9c89m1l") (f (quote (("build_deps" "man"))))))

