(define-module (crates-io st or stor) #:use-module (crates-io))

(define-public crate-stor-0.1.0 (c (n "stor") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7.9") (o #t) (d #t) (k 0)))) (h "08j3cc20gwl5sjjxw2z1hhbxcxd4kk4ivx4fzj9hlnm25kd8hm5d") (f (quote (("default" "alloc" "heapless") ("alloc"))))))

(define-public crate-stor-0.1.1 (c (n "stor") (v "0.1.1") (d (list (d (n "heapless") (r "^0.7.9") (o #t) (d #t) (k 0)))) (h "1gmp9nhcnsyzhijhsswhbq8alv5sdwg6jyxly56b97jwc96im3rq") (f (quote (("default" "alloc" "heapless") ("alloc"))))))

