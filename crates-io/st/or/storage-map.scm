(define-module (crates-io st or storage-map) #:use-module (crates-io))

(define-public crate-storage-map-0.1.0 (c (n "storage-map") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6.3") (d #t) (k 2)))) (h "12yj78fzyqjp8n26khsyr3aj4h3kh5jbwv4zdglrrhhm0ps66hsh")))

(define-public crate-storage-map-0.1.1 (c (n "storage-map") (v "0.1.1") (d (list (d (n "lock_api") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6.3") (d #t) (k 2)))) (h "02mhj7mys225g3cf750zaq9qq2ya3n0r233h49yh9fq9fc0lkg0n")))

(define-public crate-storage-map-0.1.2 (c (n "storage-map") (v "0.1.2") (d (list (d (n "lock_api") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6.3") (d #t) (k 2)))) (h "0ka2znnlginlzj6crnpz25aw1d78if42wwlfdj3l22dsrikz356b")))

(define-public crate-storage-map-0.2.0 (c (n "storage-map") (v "0.2.0") (d (list (d (n "lock_api") (r "^0.3.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9.0") (d #t) (k 2)))) (h "0pn4m6cwki028xfzsycs6d8kw1g4n7b3d9s4m4jdr4f5llllh2px")))

(define-public crate-storage-map-0.3.0 (c (n "storage-map") (v "0.3.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 2)))) (h "0v10fp5a2005b18is8ncp0iv7kqjylngf0sk3n2afmda8d3b32s1")))

