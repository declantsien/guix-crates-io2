(define-module (crates-io st or storage-trait) #:use-module (crates-io))

(define-public crate-storage-trait-0.1.0 (c (n "storage-trait") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.3") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)))) (h "1w0ch5r77xhdypr7g4wishn2x5fm56cbsjvxaqrmdgvw0dac82d3")))

(define-public crate-storage-trait-0.1.1 (c (n "storage-trait") (v "0.1.1") (d (list (d (n "dashmap") (r "^5.3") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)))) (h "0abdgj72mbskij6aiwbdl3nkkynpcnkaddz6kcp00sr2zi80i3c7")))

(define-public crate-storage-trait-0.1.2 (c (n "storage-trait") (v "0.1.2") (d (list (d (n "dashmap") (r "^5.3") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)))) (h "0b4l2ysxj3w36fbnglwska1rg1c085biwb2hjqlslyn1nnw299zy")))

(define-public crate-storage-trait-0.1.3 (c (n "storage-trait") (v "0.1.3") (d (list (d (n "dashmap") (r "^5.3") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)))) (h "1gvcfb4kwd1cigrd7bwsqq5rgwqkih3i3brv28rqqnrzdvlvfc06")))

(define-public crate-storage-trait-0.1.4 (c (n "storage-trait") (v "0.1.4") (d (list (d (n "dashmap") (r "^5.3") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)))) (h "023rlrp882kap78a4ci7gapqdqgrc8h83lf96sxmlqbd3f3qrzlv")))

