(define-module (crates-io st or storagevec) #:use-module (crates-io))

(define-public crate-storagevec-0.1.0 (c (n "storagevec") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "tinymap") (r "^0.2.2") (d #t) (k 0)) (d (n "tinyvec") (r "^0.4.1") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "0f0z4q33hrc6hrkgirywd6hf2516vs0y5idi03fk9g2ffrwzksz3") (f (quote (("stack" "tinyvec/alloc") ("default" "alloc") ("alloc" "hashbrown"))))))

(define-public crate-storagevec-0.1.1 (c (n "storagevec") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "tinymap") (r "^0.2.2") (d #t) (k 0)) (d (n "tinyvec") (r "^0.4.1") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "0sgg9shkxr3wr2jci07fsvwddrxwhsmv7gijqn1sf82lilrjwk4x") (f (quote (("stack" "tinyvec/alloc") ("default" "alloc") ("alloc" "hashbrown"))))))

(define-public crate-storagevec-0.1.2 (c (n "storagevec") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "tinymap") (r "^0.2.3") (d #t) (k 0)) (d (n "tinyvec") (r "^0.4.1") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "1ww7qpj19s4jmr8r2nxqdl8s2mfmnqqlr0q5i87plkc6g6yx1sc8") (f (quote (("stack" "tinyvec/alloc") ("default" "alloc") ("alloc" "hashbrown"))))))

(define-public crate-storagevec-0.1.3 (c (n "storagevec") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "tinymap") (r "^0.2.3") (d #t) (k 0)) (d (n "tinyvec") (r "^0.4.1") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "0p3qxknwwjqyh1lxx8f4yifhqn9f68fssiblqj1g1rrnl48ppd2s") (f (quote (("stack" "tinyvec/alloc") ("default" "alloc") ("alloc" "hashbrown"))))))

(define-public crate-storagevec-0.1.4 (c (n "storagevec") (v "0.1.4") (d (list (d (n "hashbrown") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "tinymap") (r "^0.2.3") (d #t) (k 0)) (d (n "tinyvec") (r "^0.4.1") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "1rqi9blgc44f76h4c6zsws2cgbw62wdx7hz9g0kym7q8bqgss7g8") (f (quote (("stack" "tinyvec/alloc") ("default" "alloc") ("alloc" "hashbrown"))))))

(define-public crate-storagevec-0.1.5 (c (n "storagevec") (v "0.1.5") (d (list (d (n "hashbrown") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "tinymap") (r "^0.2.4") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0.0") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "10ji6j626bc3jya9863rbwjw31hkxjhxs6mazk4p69mdjshx023x") (f (quote (("stack" "tinyvec/alloc") ("default" "alloc") ("alloc" "hashbrown"))))))

(define-public crate-storagevec-0.1.6 (c (n "storagevec") (v "0.1.6") (d (list (d (n "hashbrown") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "tinymap") (r "^0.2.4") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0.0") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "1c93xsqjm1wwc5n0vjms47yalryji4xmyqqm6f0mbqjai6k4jjvf") (f (quote (("stack" "tinyvec/alloc") ("default" "alloc") ("alloc" "hashbrown"))))))

(define-public crate-storagevec-0.2.0 (c (n "storagevec") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "tinymap") (r "^0.2.4") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0.0") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "16lhaxsn0w55la54nz3n5y0i572rda7c3c0sn4mkk4skilpzas4i") (f (quote (("stack" "tinyvec/alloc") ("default" "alloc") ("alloc" "hashbrown"))))))

(define-public crate-storagevec-0.2.1 (c (n "storagevec") (v "0.2.1") (d (list (d (n "hashbrown") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "tinymap") (r "^0.2.4") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0.0") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "0dcirj3c3zfvv2v93ydcg4crxv02nkjrib2qgxjbxmjpy0h9jvxy") (f (quote (("stack" "tinyvec/alloc") ("default" "alloc") ("alloc" "hashbrown"))))))

