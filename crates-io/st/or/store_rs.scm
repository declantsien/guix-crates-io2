(define-module (crates-io st or store_rs) #:use-module (crates-io))

(define-public crate-store_rs-0.1.0 (c (n "store_rs") (v "0.1.0") (h "0nbqf335k26acy709sig8gn6vdg3xapi3v7jrch07qva1gwrn4ya") (y #t)))

(define-public crate-store_rs-1.0.0 (c (n "store_rs") (v "1.0.0") (h "04llw4gw8nc3pqai60jzm71vkgi63zi331b6wwcsq984rv80j7k2")))

