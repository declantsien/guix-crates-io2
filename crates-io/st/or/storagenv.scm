(define-module (crates-io st or storagenv) #:use-module (crates-io))

(define-public crate-storagenv-0.1.0 (c (n "storagenv") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)))) (h "1mp76ydddc36rbd6w5rw2n0svj7nbf3n2xj7rp7ddfmm8n54crms")))

(define-public crate-storagenv-0.1.1 (c (n "storagenv") (v "0.1.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)))) (h "0ym6azicnm4sg3cm19d8sczbmvgvj4b1dbrkqzjwkb7hjx26mfwx")))

(define-public crate-storagenv-0.1.2 (c (n "storagenv") (v "0.1.2") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)))) (h "0waxshz3n580yds62g8y9ks8pk77wa1k3fcgjx6vq2hnjgc8lf84")))

