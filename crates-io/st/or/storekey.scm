(define-module (crates-io st or storekey) #:use-module (crates-io))

(define-public crate-storekey-0.1.0 (c (n "storekey") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0rayrj6zrw7cjjslvqzrzx1iz1xpgjqjhzfmy0l5y4nzkjdxgclf")))

(define-public crate-storekey-0.2.0 (c (n "storekey") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1myxkf48dk520kqh12d8y7rg17g8rxbqca356b5qf9khip0nnj2w")))

(define-public crate-storekey-0.3.0 (c (n "storekey") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1636zz2w7mnj6szng984jisdb31lkzml44l0d6s91kbxxmwsq19w")))

(define-public crate-storekey-0.4.0 (c (n "storekey") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "00y0zsv6i9d20nvag2p90nsik2jrgknlgh5dzsj0gq6bf27v8skx")))

(define-public crate-storekey-0.4.1 (c (n "storekey") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "14xqmnhla5wbx2z60z0whm3zqgx9dbs9sz3pfiwgljc8mb0r4ma7")))

(define-public crate-storekey-0.5.0 (c (n "storekey") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1cl5xwm7n3mmyhw2ki55cr3rkj8c9q8qf7gp8jrj6paahcrjii23")))

