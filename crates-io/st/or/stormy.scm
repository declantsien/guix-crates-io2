(define-module (crates-io st or stormy) #:use-module (crates-io))

(define-public crate-stormy-0.1.0 (c (n "stormy") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.84") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "01dqlhym0pccx53z1j0vqjbccifv1mlz15vkijkf6nb9n0zgwc1w")))

(define-public crate-stormy-0.2.0 (c (n "stormy") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.84") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1lpbpg39v10bms7hxfhhgkddhsbbllwv7hms2v9b1481bp010vq0")))

(define-public crate-stormy-0.2.1 (c (n "stormy") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.84") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1wacz1afgfa5h0imikw0rlxyad9405bbbix447v8l65xz3d0r027")))

(define-public crate-stormy-0.3.0 (c (n "stormy") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.84") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1fg5nmdv3bv8n8k0qagzs8vy6cbjz7rxrfsrax290xspkwf62p3f")))

