(define-module (crates-io st or storey-encoding) #:use-module (crates-io))

(define-public crate-storey-encoding-0.1.0 (c (n "storey-encoding") (v "0.1.0") (h "0d5vjhhvlr5wqqfnscd4j3sgrb9qqs1r2whxva8x32hzarga6157")))

(define-public crate-storey-encoding-0.1.1 (c (n "storey-encoding") (v "0.1.1") (h "1l2az6l8mf5dsw9gp23wcycc1ix58000h6bl3ak3m7awciq5d119")))

