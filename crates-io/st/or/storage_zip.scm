(define-module (crates-io st or storage_zip) #:use-module (crates-io))

(define-public crate-storage_zip-0.1.0 (c (n "storage_zip") (v "0.1.0") (h "174y1dn2aa4smm437bpfl9qdmk0fhzybvszy3kplvdgdc79aqxbs")))

(define-public crate-storage_zip-0.1.1 (c (n "storage_zip") (v "0.1.1") (h "1s5d6nf6cb71g2mcs11fqdkjbg4ms151w3r83rb12i9mx53d679r")))

