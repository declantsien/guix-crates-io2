(define-module (crates-io st or stormz) #:use-module (crates-io))

(define-public crate-stormz-0.1.0 (c (n "stormz") (v "0.1.0") (d (list (d (n "billboard") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.10") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "0ilfjai3n6n30g3sy0igavjg6c4pqwfh8cf1kn4w2l6d1kcg3hpr")))

(define-public crate-stormz-0.2.0 (c (n "stormz") (v "0.2.0") (d (list (d (n "billboard") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.10") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "0ap76m97cas34whdvk4bayj2rdamfxblyj6a361xnafmjl3ycic6")))

