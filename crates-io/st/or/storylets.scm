(define-module (crates-io st or storylets) #:use-module (crates-io))

(define-public crate-storylets-0.1.0 (c (n "storylets") (v "0.1.0") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "throne") (r "^0.1.1") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0rifjq16bly5d4ha9fzibmwh09gx55d2k6ck24xic1g60cc53bn6")))

(define-public crate-storylets-0.1.1 (c (n "storylets") (v "0.1.1") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "throne") (r "^0.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0842ql213133xvvnmybv0364jmx5ljd8gqa2y2aaamiaabnvz69y")))

(define-public crate-storylets-0.2.0 (c (n "storylets") (v "0.2.0") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "throne") (r "^0.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "00gn8j6543q7xkkz1xmc311icardpgap12dp5lvminxivqkfmn46")))

(define-public crate-storylets-0.2.1 (c (n "storylets") (v "0.2.1") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "throne") (r "^0.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1vymaqcgpgcq8krrkg8hz2pk7sg8jvm33xpvzbi6xv72nsrkrqxw")))

(define-public crate-storylets-0.3.0 (c (n "storylets") (v "0.3.0") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "throne") (r "^0.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1qvpd3gh7jf4i6gv1nkpp5rzbcki0gjrn6mlsffp8kjfywi9flp2")))

(define-public crate-storylets-0.4.0 (c (n "storylets") (v "0.4.0") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "throne") (r "^0.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0kkgflphw9pgqa42wwb4gi6a47dkmx8mklb543b70419xwfs1szh")))

(define-public crate-storylets-0.4.1 (c (n "storylets") (v "0.4.1") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "throne") (r "^0.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "06vy3b4v22rl6r55l9nkbpd2y3vpdp6x36haw31x2s49q9l7f2aj")))

(define-public crate-storylets-0.4.2 (c (n "storylets") (v "0.4.2") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "throne") (r "^0.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1c34fqjgi71z0w2x913hwv3hvznk9y5qbmzivf49vgczx3j4dcgn")))

(define-public crate-storylets-0.4.3 (c (n "storylets") (v "0.4.3") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "throne") (r "^0.2.1") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1f0m1fvqlxyxaxdw7x476a637zk0q4c020hqf7sy3bs03c25qd6g")))

(define-public crate-storylets-0.4.4 (c (n "storylets") (v "0.4.4") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "throne") (r "^0.2.1") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1gcwf89hyx8gfii3md9par2ga9gr92f38fh8b2qla4irpqbibf3v")))

(define-public crate-storylets-0.5.0 (c (n "storylets") (v "0.5.0") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "throne") (r "^0.3.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0mchlgqhw0jhr6xzambv7ja72ylbgiviqmwjh2b80k579fnzfcmk")))

