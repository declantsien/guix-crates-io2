(define-module (crates-io st or store_rs_core) #:use-module (crates-io))

(define-public crate-store_rs_core-0.1.0 (c (n "store_rs_core") (v "0.1.0") (h "1n9nbihb9nagd2z0kfxs51dzjm2hfjrzyi0mra66bkflwyzmjli5") (y #t)))

(define-public crate-store_rs_core-1.0.0 (c (n "store_rs_core") (v "1.0.0") (h "1rs2yxh3vny4si1hmzy8lgmrhgh1gwsaiqy9gi2jmk405fjd6hxk")))

