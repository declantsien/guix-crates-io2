(define-module (crates-io st or storyboard-client) #:use-module (crates-io))

(define-public crate-storyboard-client-0.1.0 (c (n "storyboard-client") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1da30ks2y0psbawvqph88kaijvx6c88w67164akdnixgw4x6nrri")))

(define-public crate-storyboard-client-0.2.0 (c (n "storyboard-client") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06grxmmb28r57bmf5alh616p1ym8nzvqi0dd2s3aan0wqil0qkvl")))

(define-public crate-storyboard-client-0.2.1 (c (n "storyboard-client") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bzf95sdcab4zscy81pdy763bcx83dh142alc3ksx7qiz0m2spg2")))

