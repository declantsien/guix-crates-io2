(define-module (crates-io st or stormlight_utils) #:use-module (crates-io))

(define-public crate-stormlight_utils-0.1.0 (c (n "stormlight_utils") (v "0.1.0") (h "1p5akda2b1kmw6i6yz8dbfb83nr7saiicjanb4f9nqa83a9yf0nc") (y #t)))

(define-public crate-stormlight_utils-0.1.1 (c (n "stormlight_utils") (v "0.1.1") (h "022dqg97ygjgv2wbq5zphh08a16ncfb3wc5l7zjmhsm53ziywwx8")))

