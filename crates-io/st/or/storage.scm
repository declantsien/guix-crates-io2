(define-module (crates-io st or storage) #:use-module (crates-io))

(define-public crate-storage-0.0.1 (c (n "storage") (v "0.0.1") (h "1fdmn22smf9zsdymzqzp42vgy23f1sl63sv6rjxibr4z6kz7il6j")))

(define-public crate-storage-0.1.0 (c (n "storage") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1rwwi7iwxd8xjmrw65kfb8ic7inisck5rf04az3fs7mdpqdvv9z5")))

(define-public crate-storage-0.2.0 (c (n "storage") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0cc5q6rmziqfkspd1hsk4w5v566xwmc5szksjsrrgrf6ddm8kay7")))

(define-public crate-storage-0.2.1 (c (n "storage") (v "0.2.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1h7ga2h6jrv1mbrk31qdhjfx54kzqsawp58p10qgbx6y63j4a39d")))

(define-public crate-storage-0.3.0 (c (n "storage") (v "0.3.0") (d (list (d (n "matrix") (r "*") (d #t) (k 0)))) (h "0wj7d755x0wbv4dpy5jyk0maag3xk2lmhj1m7shy2q0wvvcq4sd5")))

(define-public crate-storage-0.4.0 (c (n "storage") (v "0.4.0") (d (list (d (n "matrix") (r "*") (d #t) (k 0)))) (h "1xxh4iv1vjyrz2lqjwvpr349gvisn28cy73422y3jn39ggaa8pnl")))

(define-public crate-storage-0.5.0 (c (n "storage") (v "0.5.0") (d (list (d (n "matrix") (r "*") (d #t) (k 0)))) (h "0mhb7d8yw5mlczwnd6y74b6g8gpnprfgar0nhkz8w3zwcvs9pnai")))

