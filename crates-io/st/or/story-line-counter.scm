(define-module (crates-io st or story-line-counter) #:use-module (crates-io))

(define-public crate-story-line-counter-1.0.0 (c (n "story-line-counter") (v "1.0.0") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "~0.9") (d #t) (k 0)) (d (n "regex") (r "~1.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qgz9wrjy7pb1qcp1zpk0rc4651j08q7bpscqqiac7jaaqxg6635")))

