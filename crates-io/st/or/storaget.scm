(define-module (crates-io st or storaget) #:use-module (crates-io))

(define-public crate-storaget-0.0.1 (c (n "storaget") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0yqhr7qhc4hblafncp6sj9xacb56p0j3vaqjwds7z9r6j0jalbnn")))

(define-public crate-storaget-0.1.0 (c (n "storaget") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1rsw17rzv9fpcacb916hkhxmb5aigynq8zazjh0rmsypgffy3gfx")))

(define-public crate-storaget-0.1.1 (c (n "storaget") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1x8li1mkgcdd7frgg24yniziga5ba2pcfsdjy05k1s1ry9jll52k")))

(define-public crate-storaget-0.2.0 (c (n "storaget") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "070sazads2c8isnp2md00y7jbvfcm06azxnn3j00bq8ck7g8grz1")))

(define-public crate-storaget-0.2.1 (c (n "storaget") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1vhv94zgmmzvh3hi815mrg34an93avqjh9dgmh7wp5jq3qlrq9z5")))

(define-public crate-storaget-0.2.2 (c (n "storaget") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0zd55s79nszvbawl9k37r7r0nwxj6z1x8c6hjljy9kn23qd6p4sv")))

(define-public crate-storaget-0.2.3 (c (n "storaget") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "15085jmvgrz68y4k82h1kcklblaqnyfmmhp7pga62z2sz36l2b27")))

(define-public crate-storaget-0.2.4 (c (n "storaget") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "02j7f18rrr949s561wwr2jbxavnvvdjkv166mh9avvvzzbgczsk7")))

(define-public crate-storaget-0.3.0 (c (n "storaget") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "07bjr08851bi3dj185gcd7ngc53vgjv6453vfaiphz3v35vhdb76")))

(define-public crate-storaget-0.4.0 (c (n "storaget") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0smmvzp96r9832p510gva5g1346d0csswfwyqf75v2c6srxlpjly")))

(define-public crate-storaget-0.5.0 (c (n "storaget") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0sgpb9agl2shb2yhwib33frrw6y9ng9b63xcnncxbg50dfzqsgkw")))

(define-public crate-storaget-0.6.0 (c (n "storaget") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "07v52fh12g94mksr75ak4dv9qlqksmr6q56jnix7hrvldki26z19")))

(define-public crate-storaget-0.7.0 (c (n "storaget") (v "0.7.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1b39lv8h1ylm9r1asdkf1cb2phj0lrj9bai4pmzw5lsppqzna1fj")))

(define-public crate-storaget-0.7.1 (c (n "storaget") (v "0.7.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1djmr19ibv5w7fj98zs615842d9bsixvhmlqc5l0m3y68dmgfygv")))

(define-public crate-storaget-0.8.0 (c (n "storaget") (v "0.8.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "106vilg1cn7k8wrkqcsflhjs7m6srpi008x6agi4445k5fksn614")))

(define-public crate-storaget-0.8.1 (c (n "storaget") (v "0.8.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "09dfax2qsshq49lgr0cya612z34mr7kc9k8i66lwjiqn9xs71xc4")))

