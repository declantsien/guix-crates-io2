(define-module (crates-io st or storey) #:use-module (crates-io))

(define-public crate-storey-0.1.0 (c (n "storey") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06f4v9wn0fvap01k1jwgjqg16vkkybrd93yilxylk7a3v9x45767") (r "1.65")))

(define-public crate-storey-0.2.0 (c (n "storey") (v "0.2.0") (d (list (d (n "storey-encoding") (r "^0.1") (d #t) (k 0)) (d (n "storey-storage") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0js9xvf604mv8mnhx66jnr6z9dwxpr1k8z7sddksg7fca8wdisls") (r "1.65")))

