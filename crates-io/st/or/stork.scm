(define-module (crates-io st or stork) #:use-module (crates-io))

(define-public crate-stork-0.0.1 (c (n "stork") (v "0.0.1") (d (list (d (n "async-stream") (r "^0.2.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "121ii53hmjayg72sbd1a70y6d8n4dayflms1l75csn00jggr35sc")))

(define-public crate-stork-0.0.2 (c (n "stork") (v "0.0.2") (d (list (d (n "async-stream") (r "^0.2.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1j1rszkz3gglrm0xvmba9fhl04iyj0rxcnvf36h6p0h61ydj56qj")))

(define-public crate-stork-0.0.3 (c (n "stork") (v "0.0.3") (d (list (d (n "async-stream") (r "^0.2.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0fkr128ra5iwq09d4j0zpbk7vy78h624xygg5l1m9lw5bkh3cw3q")))

