(define-module (crates-io st or storage-client-interface) #:use-module (crates-io))

(define-public crate-storage-client-interface-0.1.0 (c (n "storage-client-interface") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "aws-config") (r "^0.56.1") (o #t) (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^0.30.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1afhk2apqdjsk1wxkilya7w4z8yzjfhrn8mlxlx7s642bl88mc0m") (f (quote (("default" "s3")))) (s 2) (e (quote (("s3" "dep:aws-sdk-s3" "dep:aws-config"))))))

(define-public crate-storage-client-interface-0.2.0 (c (n "storage-client-interface") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "aws-config") (r "^0.56.1") (o #t) (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^0.30.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "14wgr2grkfmf2nrbp4vi6r4f4xh1m6gkkgy8x7rb77m5v4qcg86r") (f (quote (("default" "s3")))) (s 2) (e (quote (("s3" "dep:aws-sdk-s3" "dep:aws-config"))))))

