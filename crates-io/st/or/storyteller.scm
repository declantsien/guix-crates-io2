(define-module (crates-io st or storyteller) #:use-module (crates-io))

(define-public crate-storyteller-0.1.0 (c (n "storyteller") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1lmd5f7g2q9v53jkbq9ldzmfqlcxcbjx4k0l24v5fvd0izy1959l")))

(define-public crate-storyteller-0.2.0 (c (n "storyteller") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1vz8nrjrfdgynpbb20rrlvqik8ygmmg04hikm5bmx8vkcr2lp9kc")))

(define-public crate-storyteller-0.3.1 (c (n "storyteller") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "08c1ipjfjdssfjv3am5hj6iy1ib9y6xfqjjx7xrprcvyjfy847mh")))

(define-public crate-storyteller-0.3.2 (c (n "storyteller") (v "0.3.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0k3hilm49dykfkqfm5x0m9a6ppfcddz6aar9s6h0hy1bz87hxyyn")))

(define-public crate-storyteller-0.4.0 (c (n "storyteller") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "096shvv36bl03c2rj32g7mr7l8yify0gmzmz0p1r89ncx4w3nzw9")))

(define-public crate-storyteller-0.4.1 (c (n "storyteller") (v "0.4.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0kllnxbxk1lvjh94yal4gxrd5id31hlmqxydyn4948bhsg9y42kf") (f (quote (("experimental-handle_disconnect_ack"))))))

(define-public crate-storyteller-0.4.2 (c (n "storyteller") (v "0.4.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "055cwr6xjwmjr58prp63l8ml4gl69dd7wb1lnh77xn5ymac09d13") (f (quote (("experimental_handle_disconnect_ack"))))))

(define-public crate-storyteller-0.5.0 (c (n "storyteller") (v "0.5.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "0cfgn2lridzp6hzg9k0igiz4yslz7a7aq8fsl7j1rw4r8n49bcvk") (f (quote (("default" "channel_reporter") ("channel_reporter" "crossbeam-channel"))))))

(define-public crate-storyteller-0.6.0 (c (n "storyteller") (v "0.6.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "1z3hlqyr751r6yh7pfx32hncs452ksg428k2658ygiia6qwd2mz5") (f (quote (("default" "channel_reporter") ("channel_reporter" "crossbeam-channel"))))))

(define-public crate-storyteller-0.6.1 (c (n "storyteller") (v "0.6.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "1nf6nlk26h12s499wji0jww07y6315j41qv8vyvlbh1vysn5h76p") (f (quote (("default" "channel_reporter") ("channel_reporter" "crossbeam-channel"))))))

(define-public crate-storyteller-0.7.0 (c (n "storyteller") (v "0.7.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "once_cell") (r "~1.14.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "14mqkpj4fbfcw1sxwnzpg5lmjxdigf2k5yiylgzhl3yi1bvwjx3q") (f (quote (("default" "channel_reporter") ("channel_reporter" "crossbeam-channel" "once_cell"))))))

(define-public crate-storyteller-0.8.0 (c (n "storyteller") (v "0.8.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "1xvm7scbsj5arkiwkq24rqwzw3q8c41wlwvi9k0ha5piz0q5jy68") (f (quote (("default" "channel_reporter") ("channel_reporter" "crossbeam-channel")))) (r "1.56")))

(define-public crate-storyteller-0.8.1 (c (n "storyteller") (v "0.8.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0a42sskja37g5ry686hykblhackhabcsmbh2m21bfpypp7512rbp") (f (quote (("default" "channel_reporter") ("channel_reporter" "crossbeam-channel")))) (r "1.56")))

