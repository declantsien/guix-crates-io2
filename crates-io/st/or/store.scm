(define-module (crates-io st or store) #:use-module (crates-io))

(define-public crate-store-0.1.0-alpha.1 (c (n "store") (v "0.1.0-alpha.1") (d (list (d (n "byteio") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1gz0nm94glvxlh0z1jpjnc06kzrw719pkxxxwiqxsf0axsqkhbyn") (f (quote (("std" "byteio/std" "serde/std") ("default" "std") ("alloc" "byteio/alloc" "serde/alloc"))))))

(define-public crate-store-0.1.0-alpha.2 (c (n "store") (v "0.1.0-alpha.2") (d (list (d (n "byteio") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1xbssm7nx6z3zknpnbyri1hca1yhrh86r3jvjy5w6jkrrd9867k3") (f (quote (("std" "byteio/std" "serde/std") ("default" "std") ("alloc" "byteio/alloc" "serde/alloc"))))))

(define-public crate-store-0.1.0-alpha.3 (c (n "store") (v "0.1.0-alpha.3") (d (list (d (n "byteio") (r "^0.2") (k 0)) (d (n "nano-leb128") (r "^0.1") (f (quote ("byteio_ext"))) (k 0)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0qr2y4qjc4myicfka7iw7qx2zrwq1y2qx172dbz62g5klyaszl2s") (f (quote (("std" "byteio/std" "nano-leb128/std" "serde/std") ("default" "std") ("alloc" "byteio/alloc" "serde/alloc"))))))

