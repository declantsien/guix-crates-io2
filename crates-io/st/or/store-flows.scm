(define-module (crates-io st or store-flows) #:use-module (crates-io))

(define-public crate-store-flows-0.1.0 (c (n "store-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0nqwizcznknwlp9r8lybnkq61rkdkmkdb9ipmqv87v993kmrk7bi")))

(define-public crate-store-flows-0.1.1 (c (n "store-flows") (v "0.1.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0c4pb65wna5pmc4gfrysbb9rrj7aazaxd3a91bmjl22s29886bsh")))

(define-public crate-store-flows-0.1.2 (c (n "store-flows") (v "0.1.2") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0dzdpv940s66bvdpbrik1vn3hx2zzwl92j2zc0v1i3zs37avaaq5")))

(define-public crate-store-flows-0.1.3 (c (n "store-flows") (v "0.1.3") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1zvrlkcm2rmv34hvxxsg068gcdbknw91kwv6haz7bh1q865yfx45")))

(define-public crate-store-flows-0.1.4 (c (n "store-flows") (v "0.1.4") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "193jxvz41mxzpx8hxld4njb2f2699xyl6pzffl0k8lwhhn111k8h")))

(define-public crate-store-flows-0.1.5 (c (n "store-flows") (v "0.1.5") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "10k87vmmjzx4fp528xfib8kf7372zs8rn98zqxpb48f86ynhmd4a")))

(define-public crate-store-flows-0.1.6 (c (n "store-flows") (v "0.1.6") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0y5wd0xk61kba9yfl1rb6lr0wxlxry2m4jsj5mnw09y70hs6m001")))

(define-public crate-store-flows-0.1.7 (c (n "store-flows") (v "0.1.7") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "17jiz10h7g72mqr04g1xkkmrjgdd854h9gfz9wslf843z4jm1abj")))

(define-public crate-store-flows-0.1.8 (c (n "store-flows") (v "0.1.8") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1dg32xws6gn52ly4zbis0pm1inb8jakzjna6zwzp0qz62pi07m9g")))

(define-public crate-store-flows-0.1.9 (c (n "store-flows") (v "0.1.9") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1664mai88a8n3h484cy127n6vkgdsgvgs381fhnnqs19kq0x7iv5")))

(define-public crate-store-flows-0.2.0 (c (n "store-flows") (v "0.2.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0yw372fzb2sq528aqvzy2liy4wfxrd4qacq5j6cvijk63znpfyqb")))

(define-public crate-store-flows-0.2.1 (c (n "store-flows") (v "0.2.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0skr8ahlx2d1p0mfdinbs1ybfh8iapb2nnzk5w8nq624sl2d37qi")))

(define-public crate-store-flows-0.3.0 (c (n "store-flows") (v "0.3.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1gjzlc54q7b52lqrdkr81sfcxk9kidl9sd0m7qj0cdn1ra7fca15")))

(define-public crate-store-flows-0.3.1 (c (n "store-flows") (v "0.3.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0mnshpyz8312dxxys50n4lhpcv2idv21p8jmgcqrzmfgwsp05a9g")))

(define-public crate-store-flows-0.3.2 (c (n "store-flows") (v "0.3.2") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "066j0qpkp3h07hkgrifhaj790ri0fi4v94dyk6gzzxfa3xcjx1jh")))

