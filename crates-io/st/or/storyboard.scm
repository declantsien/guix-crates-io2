(define-module (crates-io st or storyboard) #:use-module (crates-io))

(define-public crate-storyboard-0.1.0 (c (n "storyboard") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)))) (h "0zq8ahqqq0c858rq6l5fqlfi23vr665wzzb3m7am6idfqnqnib9w")))

(define-public crate-storyboard-0.1.1 (c (n "storyboard") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)))) (h "0x3qcwscq71xaxxyi1z1f9hbmckdh0a8x5yr5ps65khr7j3kzgqd")))

