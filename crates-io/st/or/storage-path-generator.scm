(define-module (crates-io st or storage-path-generator) #:use-module (crates-io))

(define-public crate-storage-path-generator-0.1.0 (c (n "storage-path-generator") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1vaf26n3gzjnfdfikgv3kw70qfmylp1lmcc4sxnp90ldyyxk0y4g")))

(define-public crate-storage-path-generator-0.1.1 (c (n "storage-path-generator") (v "0.1.1") (h "0dpq7svgay0xdi989rbzg8sjk7ijr2blmnj96qqw864qmrfx64bz")))

