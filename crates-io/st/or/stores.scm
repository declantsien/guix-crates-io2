(define-module (crates-io st or stores) #:use-module (crates-io))

(define-public crate-stores-0.0.1 (c (n "stores") (v "0.0.1") (d (list (d (n "globals") (r "^0.1.3") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0k5bpm2qjm5j2ibcvgrnc3hdjiff8klgm733pzkh0azxsypvvwca")))

(define-public crate-stores-0.0.2 (c (n "stores") (v "0.0.2") (d (list (d (n "globals") (r "^0.1.3") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "028ygj6xq7ggzgnfm39ngn5449h75b6891vap0038scr9ln7yp7j")))

(define-public crate-stores-0.0.3 (c (n "stores") (v "0.0.3") (d (list (d (n "globals") (r "^0.1.3") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "040pa51gpbbyl8ghd4minila6i1a46qg1aq30yg5zs7ssa8sg09s")))

(define-public crate-stores-0.0.4 (c (n "stores") (v "0.0.4") (d (list (d (n "globals") (r "^0.1.3") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "10wkrg14107w5mnq4ygpvgxwbp0x933fw7f26hfpphfsp4360ldz")))

(define-public crate-stores-0.1.0 (c (n "stores") (v "0.1.0") (d (list (d (n "globals") (r "^0.1.3") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "1qkqaca5gjai68mswcqxxhk7dzabrjhx04i44n84qls8mxc9q70m")))

