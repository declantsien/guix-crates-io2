(define-module (crates-io st or storm_macro) #:use-module (crates-io))

(define-public crate-storm_macro-0.1.0 (c (n "storm_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0drm39h8bvhpx3l8g1vg9n2p3iqcnwwsm0cni9n70wn05s5jakg3")))

