(define-module (crates-io st tp sttp) #:use-module (crates-io))

(define-public crate-sttp-0.1.0 (c (n "sttp") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "1x80pq6sd5rbdnyndb6i57i79r80qngxc5k10m0vxd8wsjcyj4z9")))

