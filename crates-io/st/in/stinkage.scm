(define-module (crates-io st in stinkage) #:use-module (crates-io))

(define-public crate-stinkage-0.0.0 (c (n "stinkage") (v "0.0.0") (h "1nrqk6hy5id0gp1z2qn3z0wpqi9f6qagmz92814bz7rzm0f12jhy") (y #t)))

(define-public crate-stinkage-0.0.1 (c (n "stinkage") (v "0.0.1") (d (list (d (n "ring") (r "^0.13") (d #t) (k 0)))) (h "0324axl5rg7c77cqsc31d53sck7l7x5wrwdslm7bxpvdp6r96hvb") (y #t)))

