(define-module (crates-io st rm strmath) #:use-module (crates-io))

(define-public crate-strmath-0.1.0 (c (n "strmath") (v "0.1.0") (h "1v6fjdk3yml4qg9zbyk9n6239f3s1i6fh44135795di92n46l6nk") (y #t)))

(define-public crate-strmath-0.1.1 (c (n "strmath") (v "0.1.1") (h "0zq5afq802gkd82r7hh8wz1cwzm67y07crf42c77myi5lw73n53m") (y #t)))

(define-public crate-strmath-0.1.2 (c (n "strmath") (v "0.1.2") (h "134ghhr65vszn3m4zdddzb92i5djim9b8h7gwgcjb8bj6m4m57wh") (y #t)))

(define-public crate-strmath-0.2.1 (c (n "strmath") (v "0.2.1") (h "07kp9q896kpszkx1yrcrcj0mv8p23fjxs29ki4sjmwafkc14098d") (y #t)))

(define-public crate-strmath-0.3.0 (c (n "strmath") (v "0.3.0") (h "12njlw0wrgj7qcin1y877mc53gr9pd1a2y7fzcfm7vdwi8vb7jy7") (y #t)))

(define-public crate-strmath-0.3.1 (c (n "strmath") (v "0.3.1") (h "0jxibcjl1r5kpqrm7wxckyrwfy22v1agmfqdqv3ma2hjbrw8g78v")))

(define-public crate-strmath-0.4.0 (c (n "strmath") (v "0.4.0") (h "1jqzi1srma8mp3ka21hqp42kixq0s4mh08xr3brvvrr9lhsvym8j") (f (quote (("sub") ("rem") ("mul") ("full" "add" "sub" "mul" "div" "rem") ("div") ("add")))) (y #t)))

(define-public crate-strmath-0.4.1 (c (n "strmath") (v "0.4.1") (h "0x4i99h95wfw2y33sxq0k8bnhs72q6nilgh1sd0dqrypn3dvmg0a") (f (quote (("sub") ("rem") ("mul") ("math" "add" "sub" "mul" "div" "rem") ("full" "math" "display") ("div") ("display") ("add")))) (y #t)))

(define-public crate-strmath-0.4.2 (c (n "strmath") (v "0.4.2") (h "0c13a2f4i9x0wdkkia3xqjg865zxcybq9ggzvdr4bx6gj805qknr") (f (quote (("sub") ("rem") ("mul") ("math" "add" "sub" "mul" "div" "rem") ("full" "math" "display") ("div") ("display") ("add")))) (y #t)))

(define-public crate-strmath-0.4.4 (c (n "strmath") (v "0.4.4") (h "1jf1szgb8jvv3kyafnbnpj4q00xdfmj78c1l9zrfix72201s54r7") (f (quote (("sub") ("rem") ("mul") ("math" "add" "sub" "mul" "div" "rem") ("full" "math" "display") ("div") ("display") ("add")))) (y #t)))

(define-public crate-strmath-0.4.5 (c (n "strmath") (v "0.4.5") (h "1ik1q5z321gdghpn1c1ax11m82l4kk171a60ppxmzagnncxbsigw") (f (quote (("sub") ("rem") ("mul") ("math" "add" "sub" "mul" "div" "rem") ("full" "math" "display") ("div") ("display") ("add"))))))

