(define-module (crates-io st rm strmprivacy_schema_strmprivacy_demo) #:use-module (crates-io))

(define-public crate-strmprivacy_schema_strmprivacy_demo-1.0.2 (c (n "strmprivacy_schema_strmprivacy_demo") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "strm-privacy-driver") (r "^0.1") (d #t) (k 0)))) (h "0m9ijc28l26kl6f6z6dbxn5i00fmm2dk87hfczjvwi0bysx2j0vc")))

(define-public crate-strmprivacy_schema_strmprivacy_demo-1.0.3 (c (n "strmprivacy_schema_strmprivacy_demo") (v "1.0.3") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "strm-privacy-driver") (r "^0.1") (d #t) (k 0)))) (h "095rh6hmkikhnsdf7av9vw02jxxchf1blndqpkzva8c9bsfcs02c")))

