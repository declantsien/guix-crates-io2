(define-module (crates-io st rm strmline) #:use-module (crates-io))

(define-public crate-strmline-0.0.1 (c (n "strmline") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.27") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (k 2)))) (h "1dcq4pi7cirvh4sr3j7y0vdsj5zizv6wmajnjrbf693vpqzwybjd") (y #t)))

(define-public crate-strmline-0.0.2 (c (n "strmline") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.27") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (k 2)))) (h "064y4yfjmxz94965v9dqc1kc1m6bsb4q46csviba67661zvc2fxn") (y #t)))

