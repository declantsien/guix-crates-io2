(define-module (crates-io st rm strmap) #:use-module (crates-io))

(define-public crate-strmap-1.0.0 (c (n "strmap") (v "1.0.0") (d (list (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18ciiwvrfrd8jxs8g23wynwhg7r8wp72br32h7kg0cn77wmzq9ws")))

