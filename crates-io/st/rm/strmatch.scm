(define-module (crates-io st rm strmatch) #:use-module (crates-io))

(define-public crate-strmatch-0.1.0 (c (n "strmatch") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0rrjcynzjwnn7fj7ca5880n70jnv08cvyhlwvq0rwkypgrnpqv4c")))

(define-public crate-strmatch-0.1.1 (c (n "strmatch") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "1mbpk2rjrkqvy28f0y3mnx23l6q0d2r666nm6p0fxz8swwyfhmdp")))

