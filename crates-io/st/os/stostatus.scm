(define-module (crates-io st os stostatus) #:use-module (crates-io))

(define-public crate-stostatus-0.1.0 (c (n "stostatus") (v "0.1.0") (h "1mgg8wav4zanzbkvpnmxh0jlkk7hz7ri7pvail2ghzwy0lxkjcs8")))

(define-public crate-stostatus-0.1.1 (c (n "stostatus") (v "0.1.1") (h "1zm64dw7xprdk9qrkjv4yni8pmbhadpn07wr0msz5a1qwk168qpd")))

