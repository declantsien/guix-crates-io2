(define-module (crates-io st ub stubs_rust) #:use-module (crates-io))

(define-public crate-stubs_rust-0.1.0 (c (n "stubs_rust") (v "0.1.0") (d (list (d (n "ocaml") (r "^0.22.4") (d #t) (k 0)))) (h "1lifi6cmgi4kvj3cr1vym5hk956w3qnmqd3s8akk1x4l47y60ay9")))

(define-public crate-stubs_rust-0.1.1 (c (n "stubs_rust") (v "0.1.1") (d (list (d (n "ocaml") (r "^0.22.4") (d #t) (k 0)))) (h "0g04iwvvgdkxzsl70kx3dj8smpbwwyk11zs704l7w2ppb9nydhhz")))

(define-public crate-stubs_rust-0.1.2 (c (n "stubs_rust") (v "0.1.2") (d (list (d (n "ocaml") (r "^0.22.4") (d #t) (k 0)))) (h "1v7y90n7sy4c3gj9aybn8hgmvwmir14bmry7svwf72gp1y0bq9d4")))

