(define-module (crates-io st ub stubr-build) #:use-module (crates-io))

(define-public crate-stubr-build-0.4.8 (c (n "stubr-build") (v "0.4.8") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "cargo") (r "^0.54.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "0fkqcw1dh2nnj97dziijbmgrha9g4lnqmldl7kppxhqs1mq4w8vb")))

(define-public crate-stubr-build-0.4.9 (c (n "stubr-build") (v "0.4.9") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "cargo") (r "^0.55.0") (d #t) (k 0)) (d (n "cargo-util") (r "^0.1.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "0n2z9f2xw0hflqpgl0k3srmwsxics92q5w1x8lmjssihp9zfbsa4")))

(define-public crate-stubr-build-0.4.10 (c (n "stubr-build") (v "0.4.10") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "cargo") (r "^0.56.0") (d #t) (k 0)) (d (n "cargo-util") (r "^0.1.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "04jxjy3c8sf572v7i41lg73kbp7465q0v4vmdvrn069hkyffrqmc")))

(define-public crate-stubr-build-0.4.11 (c (n "stubr-build") (v "0.4.11") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "cargo") (r "^0.57.0") (d #t) (k 0)) (d (n "cargo-util") (r "^0.1.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "124n9ni1428l6qhzk31ds9zjzdpq1kbhvar8m60gc7ddgfccgqpl")))

(define-public crate-stubr-build-0.4.12 (c (n "stubr-build") (v "0.4.12") (d (list (d (n "anyhow") (r "^1.0.51") (k 0)) (d (n "cargo") (r "^0.58.0") (d #t) (k 0)) (d (n "cargo-util") (r "^0.1.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "0ihlvd4nl4d0g4rm3av5gjva4dcsi66km6xczd2z851cqm6iz012")))

(define-public crate-stubr-build-0.4.13 (c (n "stubr-build") (v "0.4.13") (d (list (d (n "anyhow") (r "^1.0.52") (k 0)) (d (n "cargo") (r "^0.58.0") (d #t) (k 0)) (d (n "cargo-util") (r "^0.1.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "0j9xyh7snz7y7d2gpqwq2z8kjq40nxn3k66mmxf9n3jbxlw36n82")))

(define-public crate-stubr-build-0.4.14 (c (n "stubr-build") (v "0.4.14") (d (list (d (n "anyhow") (r "^1.0.54") (k 0)) (d (n "cargo") (r "^0.59.0") (d #t) (k 0)) (d (n "cargo-util") (r "^0.1.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "12h8rg72as8m044nrj2i56cfl9r7bbpnwhlmmdzl4jdwy309cyz9")))

(define-public crate-stubr-build-0.5.0-rc.1 (c (n "stubr-build") (v "0.5.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.56") (k 0)) (d (n "cargo") (r "^0.61.0") (d #t) (k 0)) (d (n "cargo-util") (r "^0.1.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "1377l3ycznllhdcksxs2qv4hwmln2ii98bxxmhyvn4idmqyy3bcp")))

(define-public crate-stubr-build-0.5.0-rc.2 (c (n "stubr-build") (v "0.5.0-rc.2") (d (list (d (n "anyhow") (r "^1.0.65") (k 0)) (d (n "cargo") (r "^0.65.0") (d #t) (k 0)) (d (n "cargo-util") (r "^0.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "1www1hvp2wh1fbsnqgvd8v8gi2k8xy9mssd9bdyn39g1j4pki6a8")))

(define-public crate-stubr-build-0.5.0 (c (n "stubr-build") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.65") (k 0)) (d (n "cargo") (r "^0.66") (d #t) (k 0)) (d (n "cargo-util") (r "^0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)))) (h "0yvrqxrsa7njzz33pvzrpwky4lbxyjyrj2nipdq2x8j020bkzpx5")))

(define-public crate-stubr-build-0.5.1 (c (n "stubr-build") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.65") (k 0)) (d (n "cargo") (r "^0.68") (d #t) (k 0)) (d (n "cargo-util") (r "^0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)))) (h "0isdfmc9mvfpm1wqh3j304ldpfdhgy93wm5ylpglzrlypm4fd1ym")))

(define-public crate-stubr-build-0.6.0-rc.1 (c (n "stubr-build") (v "0.6.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.65") (k 0)) (d (n "cargo") (r "^0.68") (d #t) (k 0)) (d (n "cargo-util") (r "^0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)))) (h "1i5qs8b3ggqa0qvr7y6zllwafwb3bw54wkki9ksjpa85fcrhhx9w")))

(define-public crate-stubr-build-0.6.0 (c (n "stubr-build") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.65") (k 0)) (d (n "cargo") (r "^0.70") (d #t) (k 0)) (d (n "cargo-util") (r "^0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)))) (h "13nxblpy0ch44mrx4pdkbbd71qxl0mgwllqjcpygvvmhvnxlkin9")))

(define-public crate-stubr-build-0.6.1 (c (n "stubr-build") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.65") (k 0)) (d (n "cargo") (r "^0.70") (d #t) (k 0)) (d (n "cargo-util") (r "^0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)))) (h "0pz9mizccz52xif2vs682njfbc53nzkaf61xf50rgyaw1l49kiks")))

(define-public crate-stubr-build-0.6.2 (c (n "stubr-build") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.65") (k 0)) (d (n "cargo") (r "^0.70") (d #t) (k 0)) (d (n "cargo-util") (r "^0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)))) (h "13r35rd7ihmfj5yn2nb3bb4dbzbpvpirbrc18p6cc0p6bhyrjx9d")))

