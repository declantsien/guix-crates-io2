(define-module (crates-io st ub stubit) #:use-module (crates-io))

(define-public crate-stubit-0.1.0 (c (n "stubit") (v "0.1.0") (h "0lvdlqiyzvxbfjvnx6916spcjmbi2dwj4kjlgf2lghy6sfk7ijsi") (r "1.71.1")))

(define-public crate-stubit-0.2.0 (c (n "stubit") (v "0.2.0") (h "0w5vvjw1pamfxbm3vjl9krl9annyw4bgd3wv9g8gdzd0nzn3xxfj") (r "1.71.1")))

(define-public crate-stubit-0.3.0 (c (n "stubit") (v "0.3.0") (h "1dlaxl9ppacmzfp6pihlyfrj5xdqlfm3ny8n4lrrksiy3xfab08q") (r "1.71")))

(define-public crate-stubit-0.3.1 (c (n "stubit") (v "0.3.1") (h "0grkj43hwyv41421lqjxpa17hhxq23nhikcqal6glllybak1qbck") (r "1.71")))

