(define-module (crates-io st ub stubby) #:use-module (crates-io))

(define-public crate-stubby-0.1.0 (c (n "stubby") (v "0.1.0") (h "1a125gkdffs8mff4xhg4lmg6qyr8c8sawjmmkq1127ywjr19aivk") (y #t)))

(define-public crate-stubby-0.2.0 (c (n "stubby") (v "0.2.0") (h "03dbryb42wfcjvrcabmdn6s60m8x877m7f6s7c0cv7wlcvcgkgkw") (y #t)))

(define-public crate-stubby-0.2.1 (c (n "stubby") (v "0.2.1") (h "00apvghs38vyvhimi7rz0vwvbk87k7ha1yh9xh7plzcx5rlsscns") (y #t)))

(define-public crate-stubby-0.2.2 (c (n "stubby") (v "0.2.2") (h "1958nb0sp8zasmlvqwivzs324wrpaqf4f05sm50d2xavsiixv65a") (y #t)))

(define-public crate-stubby-0.2.3 (c (n "stubby") (v "0.2.3") (h "12s36qz6fg7imwrl05ivp6m532glyzrngi38b4c9raj4a2iz59jz") (y #t)))

(define-public crate-stubby-0.2.4 (c (n "stubby") (v "0.2.4") (h "09qflzvvxpvhg6rp71kvsr0ckq983i755jfpy104dvb29a3h4sdp") (y #t)))

(define-public crate-stubby-0.2.5 (c (n "stubby") (v "0.2.5") (d (list (d (n "tokio") (r "^1.29.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1ppfvn0mxlgqvdxpvjpny98mqarkxxnibvl6hah67gj70ih6vp2j") (y #t)))

(define-public crate-stubby-0.2.6 (c (n "stubby") (v "0.2.6") (d (list (d (n "tokio") (r "^1.29.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1rlrjg68kdg59p1w3iaxqpfs671nq0jjwabbgkv5jcln3ximc26w") (y #t) (r "1.58")))

(define-public crate-stubby-0.3.0 (c (n "stubby") (v "0.3.0") (d (list (d (n "tokio") (r "^1.29.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "074yv0clm5p79hc3hqgzaavk99v9hic9yvllg4i9j2qn7nasg3pv") (y #t) (r "1.58")))

(define-public crate-stubby-0.3.1 (c (n "stubby") (v "0.3.1") (d (list (d (n "tokio") (r "^1.29.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "19zfbyaz654vnb1x7pxg5ykgga6igjgkxgmlkh9jzf98w6sh8nyv") (f (quote (("type-safe")))) (r "1.58")))

(define-public crate-stubby-0.3.2 (c (n "stubby") (v "0.3.2") (d (list (d (n "tokio") (r "^1.29.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1g950lg7j5g8iglcqaj129mqv4qrpp1igyny808b6cxh0p4mknrm") (f (quote (("type-safe")))) (r "1.58")))

