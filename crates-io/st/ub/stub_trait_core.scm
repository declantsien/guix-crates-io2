(define-module (crates-io st ub stub_trait_core) #:use-module (crates-io))

(define-public crate-stub_trait_core-0.1.0 (c (n "stub_trait_core") (v "0.1.0") (h "0cxljwp1xdvyxy7c27pspbsm2qmjfb21ys199cj2l5ki5kdwjb91")))

(define-public crate-stub_trait_core-0.2.0 (c (n "stub_trait_core") (v "0.2.0") (h "0is9dhas4gghlp2mfap3ybgsadii8szqm2971bxk7vhfb6mn9b1a")))

