(define-module (crates-io st ub stub_trait) #:use-module (crates-io))

(define-public crate-stub_trait-0.1.0 (c (n "stub_trait") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "stub_trait_core") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d1rqk8amivbi38cn823az90sk5xm1p36ld4zmqylqqqcgisxgn4")))

(define-public crate-stub_trait-0.2.0 (c (n "stub_trait") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "stub_trait_core") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bw1wsx9f86gh1j8fvbsvzwhmgd89fyw1qc2hh6yqid1b2y1cag5")))

(define-public crate-stub_trait-1.0.0 (c (n "stub_trait") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kd01ily7dzh5qyjjmbp3w89wyw5xb43vzz3k85jzx7lxdv61ly3")))

(define-public crate-stub_trait-1.1.0 (c (n "stub_trait") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dyc3zsqlhx9kwmm8yp5jjyjfd1ypkqip2z69d18x2fz9cp4fknn")))

