(define-module (crates-io st ub stubble) #:use-module (crates-io))

(define-public crate-stubble-0.1.0 (c (n "stubble") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0srl37z1cg8qs2n82vblrff0lblfns3i91naz5nl6lld87qkfr01")))

(define-public crate-stubble-0.1.1 (c (n "stubble") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0112r9wrrd5wfmw8zs4mz013jd1b0xmc7xkvm274wyxyszvzdas7")))

(define-public crate-stubble-0.2.0 (c (n "stubble") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "16pxcp5saiwr82fwqj72iybfrvwa6cglg5vvjqpj05lfk872icb6")))

(define-public crate-stubble-0.2.1 (c (n "stubble") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "1bg5x9kch7yynm8bwa452j7hd61qv3jsz088xjdv1qd42rrybikx")))

(define-public crate-stubble-0.2.2 (c (n "stubble") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "0y1i0h1gc1naywsfgm9f5ddw1wl8wy5lar9my8q7g5vmgja6lwr5")))

(define-public crate-stubble-0.2.3 (c (n "stubble") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "15nhhy6a8aqzahg91hrqmni0qjg20bhh25cyaqjws9lcws299cq8")))

(define-public crate-stubble-0.3.0 (c (n "stubble") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libstubble") (r "^0.3.0") (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "16kc2cfn8llfmhsifcsizgkva3n6y2qw18hgn30221gs33xm70pk")))

(define-public crate-stubble-0.3.1 (c (n "stubble") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libstubble") (r "^0.3.1") (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "04vwk2c9xg7fkybz6pswfvjq9wasaqs2fh6k2n7jb51a5lqcxnnw")))

(define-public crate-stubble-0.3.2 (c (n "stubble") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libstubble") (r "^0.3.2") (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "0gmq06fadw04rrviaa55kdsr9sr23d7v79l880wrh75gxwaa7dsg")))

