(define-module (crates-io st yl stylometry) #:use-module (crates-io))

(define-public crate-stylometry-0.1.0 (c (n "stylometry") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "stop-words") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1n4hxgvnjknv2xbmc70w4pbkc9yip6bzlg1yysplpapxj87jqkml")))

