(define-module (crates-io st yl styler) #:use-module (crates-io))

(define-public crate-styler-0.1.0 (c (n "styler") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "styler_core") (r "^0.1.0") (d #t) (k 0)))) (h "13izfi0fpw6ajlka9skj5337jw627wsq072nqsgqlas406y0a2f1") (y #t)))

(define-public crate-styler-0.1.1 (c (n "styler") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "styler_core") (r "^0.1.1") (d #t) (k 0)))) (h "03gzxcj9zvw0a12akhhdhfyi82zj3pc8ag6qdcc4m8vkcy3sk2jq") (y #t)))

(define-public crate-styler-0.1.2 (c (n "styler") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "styler_core") (r "^0.1.2") (d #t) (k 0)))) (h "1i5rhav0d802cwklrs455zklnfzjvm3a9q8fk532rf71x0ms77r2") (y #t)))

