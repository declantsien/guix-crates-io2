(define-module (crates-io st yl stylers_core) #:use-module (crates-io))

(define-public crate-stylers_core-1.0.0-alpha (c (n "stylers_core") (v "1.0.0-alpha") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0h5112j4lld76aaqg02v3z8lzq14ngg1cfkj527yb81kydj06djb")))

(define-public crate-stylers_core-1.0.1 (c (n "stylers_core") (v "1.0.1") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "142wzsmm334vsy4kzcmbhi6hls659cazbjwg7l3396jfl4vkwmx1")))

(define-public crate-stylers_core-1.0.2 (c (n "stylers_core") (v "1.0.2") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1f194227nk1ck1yr5pyd82hsycmlaiz88cwdcpgvcmr7m9hrmmrg")))

