(define-module (crates-io st yl stylist-tailwind-macros) #:use-module (crates-io))

(define-public crate-stylist-tailwind-macros-0.2.0 (c (n "stylist-tailwind-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stylist-tailwind-core") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1h67rx268zfyfszbdhf4p1h8ndzvyx91n9lszjn4j33b4fs3z18h")))

