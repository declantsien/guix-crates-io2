(define-module (crates-io st yl style-term) #:use-module (crates-io))

(define-public crate-style-term-1.0.0 (c (n "style-term") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "1arpmy4gr1q8dsksh6y0rcl8pdvj4cxc1kib62qvbncgabnnv96l") (f (quote (("disable_color") ("default_four_bit"))))))

