(define-module (crates-io st yl style_freak) #:use-module (crates-io))

(define-public crate-style_freak-0.1.0 (c (n "style_freak") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)))) (h "0s9vv6fcvbxdalz27dkpc2s11vaz6xiz3aw68ci915vxa8pzdf8a")))

(define-public crate-style_freak-0.1.1 (c (n "style_freak") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)))) (h "16ix3zd0ksggq8mpl6wlczd54wcg6p7cljzxbjmqh81vhw1nmw82")))

(define-public crate-style_freak-0.1.2 (c (n "style_freak") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)))) (h "16qplx8cwkmamxmg61jxgd39qk37apfdgzzxrl6cjbz8md5ay2z3")))

