(define-module (crates-io st yl stylish-macros) #:use-module (crates-io))

(define-public crate-stylish-macros-0.0.0 (c (n "stylish-macros") (v "0.0.0") (h "1wlqd69vgh9d4alzmzlzk3dvfxnqr30li24c2bqdasb59l67gqjn")))

(define-public crate-stylish-macros-0.1.0 (c (n "stylish-macros") (v "0.1.0") (d (list (d (n "nom") (r "^6.0.1") (f (quote ("std"))) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (k 0)) (d (n "quote") (r "^1.0.8") (k 0)) (d (n "stylish-style") (r "=0.1.0") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "parsing" "proc-macro" "printing"))) (k 0)))) (h "0m63f9dmc9zfmlnbdaas2wdwypg5pqs2qm74h5aw04izdbvmzxyb")))

(define-public crate-stylish-macros-0.1.1 (c (n "stylish-macros") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.1") (f (quote ("std"))) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (k 0)) (d (n "quote") (r "^1.0.8") (k 0)) (d (n "stylish-style") (r "=0.1.0") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "unicode-ident") (r "^1.0.3") (k 0)))) (h "1b7zd7imhsbphyjlkyfp5g1rkkbh2vaixhxahp7imv8vxhyzncn9")))

