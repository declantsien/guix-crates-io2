(define-module (crates-io st yl stylus-sdk-bf) #:use-module (crates-io))

(define-public crate-stylus-sdk-bf-0.1.0 (c (n "stylus-sdk-bf") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "wasmer") (r "^4.1.1") (d #t) (k 0)))) (h "1r5286xpjiwiqvq22wy0m3hizd2dpchc64g2rcxfv8dm1ikl08pq")))

(define-public crate-stylus-sdk-bf-0.1.1 (c (n "stylus-sdk-bf") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "wasmer") (r "^4.1.1") (d #t) (k 0)))) (h "1lka30q90nwp5553smbyfyrd01h78v9npb12qgw9f92lkvcg8z3d")))

