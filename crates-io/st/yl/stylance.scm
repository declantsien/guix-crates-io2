(define-module (crates-io st yl stylance) #:use-module (crates-io))

(define-public crate-stylance-0.0.1 (c (n "stylance") (v "0.0.1") (d (list (d (n "stylance-macros") (r "^0.0.1") (d #t) (k 0)))) (h "0y8azllx309gqyzlqq8qhgn9cjxhbjmk57h9aq9wj8ahyywdyar9") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.0.2 (c (n "stylance") (v "0.0.2") (d (list (d (n "stylance-macros") (r "^0.0.2") (d #t) (k 0)))) (h "0adkaz3bla36m20pdgiylygycha4c8nl9qklqy4ywamyjjva594j") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.0.3 (c (n "stylance") (v "0.0.3") (d (list (d (n "stylance-macros") (r "^0.0.3") (d #t) (k 0)))) (h "0mag0lkjd8q1c6x4gp4ai9xdl3pwhzjh2csxm8fkxyix6v1ch5kz") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.0.4 (c (n "stylance") (v "0.0.4") (d (list (d (n "stylance-macros") (r "^0.0.4") (d #t) (k 0)))) (h "02q2npqi8y76892xswx2cjwwlkmi15q5zi2z64fi97m096zv97y3") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.0.5 (c (n "stylance") (v "0.0.5") (d (list (d (n "stylance-macros") (r "^0.0.5") (d #t) (k 0)))) (h "1cpbbqrc6i616fc5hwi66mmqkji7l14lz37ghmjzkx30jqlx31ww") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.0.6 (c (n "stylance") (v "0.0.6") (d (list (d (n "stylance-macros") (r "^0.0.6") (d #t) (k 0)))) (h "1lwnikgfbfjx64mq6w1y0fzq15i00kcizb0gcg6czfwr1mg3zxpv") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.0.7 (c (n "stylance") (v "0.0.7") (d (list (d (n "stylance-macros") (r "^0.0.7") (d #t) (k 0)))) (h "0k2zcfynwxwxsm48qcqbsd1r0hfqqjxw6d1ar47dyrl5i4lzc5sv") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.0.8 (c (n "stylance") (v "0.0.8") (d (list (d (n "stylance-macros") (r "^0.0.8") (d #t) (k 0)))) (h "1h39sgy6vxx465b4d3x0pwngcw2r38qiz7pw9mmp7fdzlbnh2sqc") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.0.9 (c (n "stylance") (v "0.0.9") (d (list (d (n "stylance-macros") (r "^0.0.9") (d #t) (k 0)))) (h "1rf2mxaqs4ki4vk4ilp6pdxq38ns10bfzyshm8kr8zz59wzm39rp") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.0.10 (c (n "stylance") (v "0.0.10") (d (list (d (n "stylance-macros") (r "^0.0.10") (d #t) (k 0)))) (h "06w4lsx9il05bs6si94gilcpk2nxmq2yf4sl7s30ywaa6x5kyclh") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.0.11 (c (n "stylance") (v "0.0.11") (d (list (d (n "stylance-macros") (r "^0.0.11") (d #t) (k 0)))) (h "1da27dp83s73ahqf5md47md8vm3pzrk610bsiimm6q24hxp0ljx2") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.0.12 (c (n "stylance") (v "0.0.12") (d (list (d (n "stylance-macros") (r "^0.0.12") (d #t) (k 0)))) (h "055v01cmp1yhypn140px8ampw7vvbhm3rbhbm1lsxwmscb9j88la") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.1.0 (c (n "stylance") (v "0.1.0") (d (list (d (n "stylance-macros") (r "^0.1.0") (d #t) (k 0)))) (h "03kxfmg6909823h7hcxbj6ycwv0aca4lvkix9gw7nh5p94vd4j0g") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.1.1 (c (n "stylance") (v "0.1.1") (d (list (d (n "stylance-macros") (r "^0.1.1") (d #t) (k 0)))) (h "17ja6fm7rg1ff12i2kn696c1bs7y3s936zxqjj9khlgw3lb84263") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.2.0 (c (n "stylance") (v "0.2.0") (d (list (d (n "stylance-macros") (r "^0.2.0") (d #t) (k 0)))) (h "14d5vk8i0q3nbqf9yfd4cjwwdzrkyidxv098vh0pk87xmnhkzrsq") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.3.0 (c (n "stylance") (v "0.3.0") (d (list (d (n "stylance-macros") (r "^0.3.0") (d #t) (k 0)))) (h "18sjdvscp6hqwv8pq5vc3zz9s7ql2bxyjnwissrqknscg3inm52r") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.4.0 (c (n "stylance") (v "0.4.0") (d (list (d (n "stylance-macros") (r "^0.4.0") (d #t) (k 0)))) (h "18l3b8vy44np6152kpl7xxplhy6xx14dh40djaxgbvpysgmq6aia") (f (quote (("nightly" "stylance-macros/nightly"))))))

(define-public crate-stylance-0.5.0 (c (n "stylance") (v "0.5.0") (d (list (d (n "stylance-macros") (r "^0.5.0") (d #t) (k 0)))) (h "16a3jyka3drh96irppdvvs40qpxskmr5nanvs879p14jg3c9ylbh") (f (quote (("nightly" "stylance-macros/nightly"))))))

