(define-module (crates-io st yl stylance-core) #:use-module (crates-io))

(define-public crate-stylance-core-0.0.1 (c (n "stylance-core") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "1fmxsyz6llf2qryn2lx9qai8cxa94f1mdcmwas0g8ds6qy6zvi7j")))

(define-public crate-stylance-core-0.0.2 (c (n "stylance-core") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "01k3q88ilc362br7xfgyg22bgwyjhwpig6sihmfi64cbr7dag04p")))

(define-public crate-stylance-core-0.0.3 (c (n "stylance-core") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "0v9yygac8b87nm1ysd2kmn3z990r0gvwrnl2r71b9n4vfnhwrkl0")))

(define-public crate-stylance-core-0.0.4 (c (n "stylance-core") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "18q5yarh0bb2mvlr4lndn50narl8v8j58sxad1l176xl27f6mnd3")))

(define-public crate-stylance-core-0.0.5 (c (n "stylance-core") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "19cfq2s3lv9dlgrj8pjrhvriv0vgr6yay3yvqyz769f11jckn5sh")))

(define-public crate-stylance-core-0.0.6 (c (n "stylance-core") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "0h2r0sl6lsf5ig0pr8i124s7i3hjg1yawx0hcrwl7kf0wlsjnhbb")))

(define-public crate-stylance-core-0.0.7 (c (n "stylance-core") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "1lfmklwjz3cv2cf877pcnl6ynr25k683k1ibdp3fin92a15nqn5c")))

(define-public crate-stylance-core-0.0.8 (c (n "stylance-core") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "0kcqchzvpw345q2h375l7rdmaldarq6gw4raf279vwdn4ncd6c6w")))

(define-public crate-stylance-core-0.0.9 (c (n "stylance-core") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "0cv1bfybhridmmvyv5dmlkz709i9dy5j9qz9aqzvimz4ymp3wlmc")))

(define-public crate-stylance-core-0.0.10 (c (n "stylance-core") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "0j23lc8nfp5m7yk7p1icc313ncwf6ghcnjmaa17567gihnbx589l")))

(define-public crate-stylance-core-0.0.11 (c (n "stylance-core") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "0nsdx72lnq293z7fw0srv3d0h2ryw95b939569mwi1lrs3kn3gxa")))

(define-public crate-stylance-core-0.0.12 (c (n "stylance-core") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "1sv6162zszxhwcwjmwci7hp9sci04dq03waa73lc0vixrkwhkl96")))

(define-public crate-stylance-core-0.1.0 (c (n "stylance-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "0zbkiycalsc5iffq8fdzwsc4l8jm5jh1f47kf6gmsskad54fsj2s")))

(define-public crate-stylance-core-0.1.1 (c (n "stylance-core") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "1vdjnl6jrslcaidjf36kwsfhbc940ljsaacar4pn4s5rg1dvawv2")))

(define-public crate-stylance-core-0.2.0 (c (n "stylance-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "1d2jl8irzkgrd53i9x8b90w7y8brlmbhj3gmdf29vwfgnxp0d9ab")))

(define-public crate-stylance-core-0.3.0 (c (n "stylance-core") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "1gwj2n00l9ps88z9qy8chc9g0bi3vdff3bfdcbrzkn4hklw2qibb")))

(define-public crate-stylance-core-0.4.0 (c (n "stylance-core") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "0224w4zg473k75pcy0aj7sc6nk341rcipl0a632jl2lrpihc57aw")))

(define-public crate-stylance-core-0.5.0 (c (n "stylance-core") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 0)))) (h "1dbr6n7qh3allr8cj5rjca6d0ynnzxwl3ajcmj2sw2h9vvgw9qpj")))

