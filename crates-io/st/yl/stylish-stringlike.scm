(define-module (crates-io st yl stylish-stringlike) #:use-module (crates-io))

(define-public crate-stylish-stringlike-0.1.0 (c (n "stylish-stringlike") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "066jmid8nr6qp3h74nzsy1fk9m404fkm72f7kvc4pra6zv7qlh6k")))

(define-public crate-stylish-stringlike-0.2.0 (c (n "stylish-stringlike") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "0c4w0g1wwpdxkqngv7d5n4b9vkimr4i15m06sam0hy7xi8pvja6j")))

(define-public crate-stylish-stringlike-0.3.0 (c (n "stylish-stringlike") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1c31j3fr91d6mfqk26p3kc842hd4mkhapb7jjw68l5qmxk8623il")))

