(define-module (crates-io st yl stylish) #:use-module (crates-io))

(define-public crate-stylish-0.0.0 (c (n "stylish") (v "0.0.0") (h "0xiq4j8sclh7qi8qqysnvzjb5di8q9iclb1460lqvwddviqz5bb5")))

(define-public crate-stylish-0.1.0 (c (n "stylish") (v "0.1.0") (d (list (d (n "stylish-ansi") (r "^0.1.0") (o #t) (k 0)) (d (n "stylish-core") (r "^0.1.0") (k 0)) (d (n "stylish-html") (r "^0.1.0") (o #t) (k 0)) (d (n "stylish-macros") (r "^0.1.0") (o #t) (k 0)) (d (n "stylish-plain") (r "^0.1.0") (o #t) (k 0)))) (h "0gy1b1v375cp1v4bqkjagx34fv8mvqygqr6x687khx5ijn82q3vi") (f (quote (("default" "std" "macros") ("_tests")))) (s 2) (e (quote (("std" "alloc" "stylish-core/std" "stylish-ansi?/std" "stylish-html?/std" "stylish-plain?/std") ("plain" "dep:stylish-plain") ("macros" "stylish-core/macros" "stylish-ansi?/macros" "stylish-html?/macros" "stylish-plain?/macros" "dep:stylish-macros") ("html" "dep:stylish-html") ("ansi" "dep:stylish-ansi") ("alloc" "stylish-core/alloc" "stylish-ansi?/alloc" "stylish-html?/alloc" "stylish-plain?/alloc"))))))

