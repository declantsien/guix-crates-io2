(define-module (crates-io st yl stylers) #:use-module (crates-io))

(define-public crate-stylers-0.1.0 (c (n "stylers") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0hdcp3wvq271s8mg1i9zicy9k5q8fshwna78k2i15fhlcz4g8jx4")))

(define-public crate-stylers-0.1.1 (c (n "stylers") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "10z2nvlxhaflx4c1il9pfkxb34vrgcrfj5azlwp0z2i52zm3w50w")))

(define-public crate-stylers-0.2.0 (c (n "stylers") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "01ybagir6xg2h8qbxw25si9bmm2rwpvw8imqg6q5dy91rqzskdfx")))

(define-public crate-stylers-0.2.1 (c (n "stylers") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0hx5xyk4g2hgs1b1j75pr997fn7b3slgyg1dph85fqw1sp4bgc74")))

(define-public crate-stylers-0.2.2 (c (n "stylers") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0zqnzlgnbjjh7wykzcarijmv8fqm67293909n6qi756j86kxavpc")))

(define-public crate-stylers-0.3.0 (c (n "stylers") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "14mjfi4ak6l7m5pkpfyvv87hjcml8k0w8zi19i6r511253jrghxi")))

(define-public crate-stylers-0.3.1 (c (n "stylers") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0gmnc3j9bj5711ff3xcyac8zn6j49sqb8jpx5xpyhg66xnrhiwr8")))

(define-public crate-stylers-0.3.2 (c (n "stylers") (v "0.3.2") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0md9cfklbhjcgvxqsfmv3fyhzryrbs4vh89f9gzmrjxkyknhdqq3")))

(define-public crate-stylers-1.0.0-alpha (c (n "stylers") (v "1.0.0-alpha") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "stylers_core") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "stylers_macro") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "104aj94s30xzzajhk4p3r5h19y12x28qczjnvb10dv3xkjyaxpn4")))

