(define-module (crates-io st yl stylish-html) #:use-module (crates-io))

(define-public crate-stylish-html-0.0.0 (c (n "stylish-html") (v "0.0.0") (h "0mx2izj5lw5iaxf92j78h8x3shpyvcxh18pcskzfda4645kv1kan")))

(define-public crate-stylish-html-0.1.0 (c (n "stylish-html") (v "0.1.0") (d (list (d (n "askama_escape") (r "^0.10.3") (k 0)) (d (n "stylish") (r "^0.1.0") (d #t) (k 2)) (d (n "stylish-core") (r "^0.1.0") (k 0)))) (h "1bjs8f1akbkjjhnkmlcvlxa8zmpsc58y9phqcn6k6xwis9lw9xm6") (f (quote (("std" "alloc" "stylish-core/std") ("macros" "stylish-core/macros") ("default" "std") ("alloc" "stylish-core/alloc"))))))

(define-public crate-stylish-html-0.1.1 (c (n "stylish-html") (v "0.1.1") (d (list (d (n "askama_escape") (r "^0.10.3") (k 0)) (d (n "stylish") (r "^0.1.0") (d #t) (k 2)) (d (n "stylish-core") (r "^0.1.0") (k 0)))) (h "1d98xwqnzlmaw28rx70ay1nhxzc6hpafk2f4qa1cflm8ml33yraw") (f (quote (("std" "alloc" "stylish-core/std") ("macros" "stylish-core/macros") ("default" "std") ("alloc" "stylish-core/alloc"))))))

