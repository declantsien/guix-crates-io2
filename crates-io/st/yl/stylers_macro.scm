(define-module (crates-io st yl stylers_macro) #:use-module (crates-io))

(define-public crate-stylers_macro-1.0.0-alpha (c (n "stylers_macro") (v "1.0.0-alpha") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "stylers_core") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "03090i9m2wdw56449a8zcnh5xar9yji0iybjr9dpalxkpni3z99z")))

(define-public crate-stylers_macro-1.0.1 (c (n "stylers_macro") (v "1.0.1") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "stylers_core") (r "^1.0.1") (d #t) (k 0)))) (h "0ydrd1bn4ksavwj5lj2n3aq5r38isrd0i5vc8lkz0g20c7kbn69d")))

(define-public crate-stylers_macro-1.0.2 (c (n "stylers_macro") (v "1.0.2") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "stylers_core") (r "^1.0.2") (d #t) (k 0)))) (h "18103l8xhixi17287v22qp1kdxrg9n9x80i8lhivld3km03l9jnp")))

