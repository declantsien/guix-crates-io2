(define-module (crates-io st yl style-expand-shorthand) #:use-module (crates-io))

(define-public crate-style-expand-shorthand-0.1.0 (c (n "style-expand-shorthand") (v "0.1.0") (d (list (d (n "cssparser") (r "^0.31.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "03bdd6lz523vniv9a8bm6f0vfgvmjx0fl0j3jjdmp10qmpfcc4mp")))

