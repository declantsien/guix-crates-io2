(define-module (crates-io st yl stylist-tailwind) #:use-module (crates-io))

(define-public crate-stylist-tailwind-0.1.0 (c (n "stylist-tailwind") (v "0.1.0") (d (list (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "stylist") (r "^0.13") (f (quote ("macros"))) (d #t) (k 0)))) (h "0qw4ccqw5svr0z4lg0f9z68p7yr8grx95psnxdfdspd5kqkm5v34")))

(define-public crate-stylist-tailwind-0.1.1 (c (n "stylist-tailwind") (v "0.1.1") (d (list (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "stylist") (r "^0.13") (f (quote ("macros"))) (d #t) (k 0)))) (h "0m354l165k8hbiz7xfyv24b1ll3z6w1jc31fzvf6sak9z213q3r5")))

(define-public crate-stylist-tailwind-0.1.2 (c (n "stylist-tailwind") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "stylist") (r "^0.13") (f (quote ("macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1z2lrm0nis9cdvdlpda93dszb47plpd0gylax187x39jar0sf1qd") (y #t)))

(define-public crate-stylist-tailwind-0.1.3 (c (n "stylist-tailwind") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "scraper") (r "^0.18") (d #t) (k 1)) (d (n "stylist") (r "^0.13") (f (quote ("macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 1)))) (h "0m86mql5aa7dxn97p6iqmjxb8y5dddd6ys078y9p1nk9vidln28z")))

(define-public crate-stylist-tailwind-0.1.4 (c (n "stylist-tailwind") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "scraper") (r "^0.18") (d #t) (k 1)) (d (n "stylist") (r "^0.13") (f (quote ("macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 1)))) (h "0p0bqh8ywiyx6mpr504xqk5d0nx1fg7m4wsiz0gl8f97fp9spgz8")))

(define-public crate-stylist-tailwind-0.2.0 (c (n "stylist-tailwind") (v "0.2.0") (d (list (d (n "stylist-tailwind-core") (r "^0.2.0") (d #t) (k 0)) (d (n "stylist-tailwind-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1mxivb2pv96lhkxb1lkn2h7kfl4zgjihy7mgdxa2bp8mrfaj2859")))

(define-public crate-stylist-tailwind-0.2.1 (c (n "stylist-tailwind") (v "0.2.1") (d (list (d (n "stylist-tailwind-core") (r "^0.2.0") (d #t) (k 0)) (d (n "stylist-tailwind-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1xhb6i5l71n8hv5wrfhxyrq28hy7lsgfkz7csjwkdp7a48vvg3lg")))

