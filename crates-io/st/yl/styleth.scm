(define-module (crates-io st yl styleth) #:use-module (crates-io))

(define-public crate-styleth-0.1.4 (c (n "styleth") (v "0.1.4") (d (list (d (n "atomic-counter") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0rs15in1w8i042r3gbjslz8ahxrkhgnqy7g3mxdixzgxadq8nwgf")))

