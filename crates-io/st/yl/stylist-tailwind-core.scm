(define-module (crates-io st yl stylist-tailwind-core) #:use-module (crates-io))

(define-public crate-stylist-tailwind-core-0.2.0 (c (n "stylist-tailwind-core") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1nhw5y1b4dnz6kw9kqdxqvpgib6afvdcqsp221v80jjvvmy50989")))

