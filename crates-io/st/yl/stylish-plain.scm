(define-module (crates-io st yl stylish-plain) #:use-module (crates-io))

(define-public crate-stylish-plain-0.0.0 (c (n "stylish-plain") (v "0.0.0") (h "1z3f45pgiqlzcqmg6g52m3xnmbwrvjk3v01wds0ng4zn3zqswfzw")))

(define-public crate-stylish-plain-0.1.0 (c (n "stylish-plain") (v "0.1.0") (d (list (d (n "stylish") (r "^0.1.0") (d #t) (k 2)) (d (n "stylish-core") (r "^0.1.0") (k 0)))) (h "0a527pr2m14v49yhlswl90py17pb5zrhs880gsrfmv8xywi54mgq") (f (quote (("std" "alloc" "stylish-core/std") ("macros" "stylish-core/macros") ("default" "std") ("alloc" "stylish-core/alloc") ("_tests"))))))

