(define-module (crates-io st yl styled-yew) #:use-module (crates-io))

(define-public crate-styled-yew-0.1.0 (c (n "styled-yew") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.20") (d #t) (k 0)))) (h "1gkd90k5bb9bccbjjjadaaz6a7x5xshmnav0smwr8sihg3j52gd8")))

(define-public crate-styled-yew-0.1.1 (c (n "styled-yew") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.20") (d #t) (k 0)))) (h "02m0a5k2wh5y0nw4vd2whbnp8467mbsmm75jlhb4qwckc8v4drdh")))

(define-public crate-styled-yew-0.1.2 (c (n "styled-yew") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.20") (d #t) (k 0)))) (h "1aah8dph5616y2213d42h39xvq6gii3kjpvgx6g447rq1af0k0yj")))

(define-public crate-styled-yew-0.2.0 (c (n "styled-yew") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.20") (d #t) (k 0)))) (h "1gyrhycq5ww7yrx4xcifabycv1fmffmy0cf8kh30s6z4s1wvpxa2")))

(define-public crate-styled-yew-0.3.0 (c (n "styled-yew") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.46") (f (quote ("CssStyleSheet" "Document" "Element" "HtmlHeadElement" "HtmlStyleElement" "StyleSheet"))) (d #t) (k 0)))) (h "0468cdw200qsvf5mj14lwmrr9d8jhhd3hvbp40fb2n9cfclmrxfm")))

