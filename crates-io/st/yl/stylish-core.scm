(define-module (crates-io st yl stylish-core) #:use-module (crates-io))

(define-public crate-stylish-core-0.0.0 (c (n "stylish-core") (v "0.0.0") (h "0vj7zgqlc9c5gahnv1jbh45vy15s9b0wp5k2qlri7dx8hijpgfsf")))

(define-public crate-stylish-core-0.1.0 (c (n "stylish-core") (v "0.1.0") (d (list (d (n "stack_dst") (r "^0.6.0") (k 0)) (d (n "stylish") (r "^0.1.0") (d #t) (k 2)) (d (n "stylish-macros") (r "=0.1.0") (o #t) (k 0)) (d (n "stylish-style") (r "=0.1.0") (k 0)) (d (n "with_builtin_macros") (r "^0.0.3") (o #t) (k 0)))) (h "1afn77pm1l3hynsg6pvfxag3d5ip189v6qxd63k70zf630zv3k55") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("macros" "dep:with_builtin_macros" "dep:stylish-macros"))))))

(define-public crate-stylish-core-0.1.1 (c (n "stylish-core") (v "0.1.1") (d (list (d (n "stack_dst") (r "^0.6.0") (k 0)) (d (n "stylish") (r "^0.1.0") (d #t) (k 2)) (d (n "stylish-macros") (r "=0.1.1") (o #t) (k 0)) (d (n "stylish-style") (r "=0.1.0") (k 0)) (d (n "with_builtin_macros") (r "^0.0.3") (o #t) (k 0)))) (h "1mx5j4x4axd777d8xbwh3g8l9mi7mqmgbs259i1d4s6ryx92h7pr") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("macros" "dep:with_builtin_macros" "dep:stylish-macros"))))))

