(define-module (crates-io st yl styled_env_logger) #:use-module (crates-io))

(define-public crate-styled_env_logger-0.4.0 (c (n "styled_env_logger") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11hs8rsqniinc8albl4ygj39d1kmpm6wjinhkzbrjx8abizi3jas") (y #t)))

(define-public crate-styled_env_logger-0.1.0 (c (n "styled_env_logger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ggbhbwy8q7lpbj0fxvshjil3y6lspv5k3lqc3bq4cr8s6klb49n") (y #t)))

(define-public crate-styled_env_logger-0.1.1 (c (n "styled_env_logger") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0g6iirb2gd7hs6lqc7wxg3mm8pkqlaq943czhdjjr2jm0r1v7gjr")))

(define-public crate-styled_env_logger-0.1.2 (c (n "styled_env_logger") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1iypcc36lfqs84i0nhcl9g1cdm2xhik66j0v1w0zxmm5zxhdvxhi")))

