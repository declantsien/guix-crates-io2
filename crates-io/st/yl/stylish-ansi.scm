(define-module (crates-io st yl stylish-ansi) #:use-module (crates-io))

(define-public crate-stylish-ansi-0.0.0 (c (n "stylish-ansi") (v "0.0.0") (h "1m3s7791x8izsmn0lhzb3qb2xzabmjkg0rghchd0qyaws9096fma")))

(define-public crate-stylish-ansi-0.1.0 (c (n "stylish-ansi") (v "0.1.0") (d (list (d (n "stylish") (r "^0.1.0") (d #t) (k 2)) (d (n "stylish-core") (r "^0.1.0") (k 0)))) (h "1v7w6nvmw11z53l4074phaql061006c3hzmv5pjx0mbij56msfvh") (f (quote (("std" "alloc" "stylish-core/std") ("macros" "stylish-core/macros") ("default" "std") ("alloc" "stylish-core/alloc"))))))

(define-public crate-stylish-ansi-0.1.1 (c (n "stylish-ansi") (v "0.1.1") (d (list (d (n "stylish") (r "^0.1.0") (d #t) (k 2)) (d (n "stylish-core") (r "^0.1.0") (k 0)))) (h "1sch863d0n1az34r0nx8pskm6ivb9hq46vv6n2w9baiychg4p4w3") (f (quote (("std" "alloc" "stylish-core/std") ("macros" "stylish-core/macros") ("default" "std") ("alloc" "stylish-core/alloc"))))))

