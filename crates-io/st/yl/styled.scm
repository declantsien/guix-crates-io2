(define-module (crates-io st yl styled) #:use-module (crates-io))

(define-public crate-styled-0.1.0 (c (n "styled") (v "0.1.0") (d (list (d (n "leptos") (r ">=0.1") (k 0)) (d (n "leptos_meta") (r ">=0.1") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0cfvqj034wnryn9qifzr8c8madx7zb4w28bfyrz4dbi94frxgipy")))

(define-public crate-styled-0.1.1 (c (n "styled") (v "0.1.1") (d (list (d (n "leptos") (r ">=0.1.0") (k 0)) (d (n "leptos_meta") (r ">=0.1.0") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "18qphpj0kfajib6wi3psi320fq5v0gnsznaaf0mrgz9djy8g25lb")))

(define-public crate-styled-0.1.2 (c (n "styled") (v "0.1.2") (d (list (d (n "leptos") (r "^0.1") (k 0)) (d (n "leptos_meta") (r "^0.1") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0k0zw5jw0lrw3ik4663d99wbbwdfi3rn7cgr79ymjxrg623f9500")))

(define-public crate-styled-0.1.3 (c (n "styled") (v "0.1.3") (d (list (d (n "leptos") (r "^0.1") (k 0)) (d (n "leptos_meta") (r "^0.1") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0idjl1jq24gc9jxw00vpwm5islqj4qm8jwhpafcv21axkff7d033")))

(define-public crate-styled-0.1.4 (c (n "styled") (v "0.1.4") (d (list (d (n "leptos") (r "^0.1") (k 0)) (d (n "leptos_meta") (r "^0.1") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "10hrd4lx1bhdsgn35fca782cp51yrar20006r0z74zz0l1i3jp33")))

(define-public crate-styled-0.1.5 (c (n "styled") (v "0.1.5") (d (list (d (n "leptos") (r "^0.1") (f (quote ("serde"))) (k 0)) (d (n "leptos_meta") (r "^0.1") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "1abvq6lfxwf4acp4siklbpzmnriqkj1li9h6jcn9377jsf8s8j0f")))

(define-public crate-styled-0.1.6 (c (n "styled") (v "0.1.6") (d (list (d (n "leptos") (r "^0.1.3") (f (quote ("serde"))) (k 0)) (d (n "leptos_meta") (r "^0.1.3") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0qmnw3z51qkw106dqf5q5g2i5wfzjdd5js7x00yaa4dck1djg7pi")))

(define-public crate-styled-0.1.7 (c (n "styled") (v "0.1.7") (d (list (d (n "leptos") (r "^0.1.3") (f (quote ("serde"))) (k 0)) (d (n "leptos_meta") (r "^0.1.3") (k 0)) (d (n "leptos_reactive") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0fkdx9fr720z5dgjrv6w69xy7j2j27kcmg59cggwdnyzyxp6bk4j")))

(define-public crate-styled-0.1.8 (c (n "styled") (v "0.1.8") (d (list (d (n "leptos") (r "^0.1.3") (f (quote ("serde"))) (k 0)) (d (n "leptos_meta") (r "^0.1.3") (k 0)) (d (n "leptos_reactive") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0f3bcbrz4rj6cxx1bm4zkbbhlym6d3cfhnikxpacnzs7aiw3rpjd")))

(define-public crate-styled-0.1.9 (c (n "styled") (v "0.1.9") (d (list (d (n "leptos") (r "^0.1.3") (f (quote ("serde"))) (k 0)) (d (n "leptos_meta") (r "^0.1.3") (k 0)) (d (n "leptos_reactive") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0d5pxm6nqrvgxss6kmz3pg6vxrh9j7l4di03ahqw0597gwwpv7dr")))

(define-public crate-styled-0.1.13 (c (n "styled") (v "0.1.13") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde"))) (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "148m88j9r7nmyk5dkvk0k8nllg57j2a3p1ma41njhidmkhs76x94")))

(define-public crate-styled-0.1.15 (c (n "styled") (v "0.1.15") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde"))) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "05xbl9i4x9xgxqzw594wrrx6j5iyjg05aqcsc4mj8b4v4ql79whp")))

(define-public crate-styled-0.1.16 (c (n "styled") (v "0.1.16") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde"))) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0knnygv7qggmvg833lw6bqd01h5wxcpgf9qipj5hcbk90viyrgsz")))

(define-public crate-styled-0.1.17 (c (n "styled") (v "0.1.17") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde"))) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "02dcb4md349d2fdxb1k3l7pgny2k3yl22k73xyx6dgk0zsv96k9i")))

(define-public crate-styled-0.1.18 (c (n "styled") (v "0.1.18") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde"))) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0h7wbmc7iai38nh84acdpnsdq73z088awmzh06k1qfhxw2i4vsbg")))

(define-public crate-styled-0.1.19 (c (n "styled") (v "0.1.19") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde"))) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "1glwx6ymp3nf231hbll7yg5jg1dg95w4wp0f24j4n0j0xssnljsl")))

(define-public crate-styled-0.1.20 (c (n "styled") (v "0.1.20") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde"))) (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "1fyj3c6q48apdcv2cw3i8ag6n9yn1c1mlciyx3y2lry5smx6hmqy")))

(define-public crate-styled-0.1.21 (c (n "styled") (v "0.1.21") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde" "ssr"))) (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0xqjgrv3b8n8gnjnx9863hpdsgdp9jj48kqbr2yg7zjkvfcgjpf0") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.22 (c (n "styled") (v "0.1.22") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde" "ssr"))) (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "13k2cslg1247k6f4rbn1a0qls0xamq9rwwins7ydv5i6yvw8dbky") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.23 (c (n "styled") (v "0.1.23") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde" "ssr"))) (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0x2dhy2n1b1nja3h0k4af34bvc83xxix4lrrhiz4cjyxh8gcyflm") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.24 (c (n "styled") (v "0.1.24") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde" "ssr"))) (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "1bxda7daiydb3k75wybnqa6b0cyrlwnlysmvpk4sxab9xl6xcc7x") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.26 (c (n "styled") (v "0.1.26") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde" "ssr"))) (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "styled_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "04wr66zkzdfxr100v12y9j6zzz69xq2girxhksxfm84hwcr1w1kb") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.27 (c (n "styled") (v "0.1.27") (d (list (d (n "leptos") (r "^0.2.0-beta") (f (quote ("serde" "ssr"))) (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "styled_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0lzzbz1i4m49b8grvsbim2djca8q0d4pf6174hbsl4h0vcbyzf02") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.28 (c (n "styled") (v "0.1.28") (d (list (d (n "leptos") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "styled_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0bfly6hfjfam21shrj1dn15dbk00v6in2cjbxznnw9ck89q9ddw7") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.29 (c (n "styled") (v "0.1.29") (d (list (d (n "leptos") (r "^0.2.0-beta") (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "styled_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "13yp1y8p78lmwm7ladqcvx2fi8vwz8xxkjlrajcxrxpyjjhwyj1j") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.30 (c (n "styled") (v "0.1.30") (d (list (d (n "leptos") (r "^0.2.0-beta") (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "styled_macro") (r "^0.1.3") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "1nr2f7j2bydi0diy555q7bg6ssn2f34lnixp4g34x8ff90r50hyc") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.31 (c (n "styled") (v "0.1.31") (d (list (d (n "leptos") (r "^0.2.0-beta") (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "styled_macro") (r "^0.1.4") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "051bh3f8d15iy9r6yiinhjvaj5pjc9s5bfxsgspbw37k8fs8ypyh") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.32 (c (n "styled") (v "0.1.32") (d (list (d (n "leptos") (r "^0.2.0-beta") (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "00lfh1iz24jvqfx7nn59p0ykwy8gzdyiwn95wx890zdjvymd5l18") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.33 (c (n "styled") (v "0.1.33") (d (list (d (n "leptos") (r "^0.2.0-beta") (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "0bf69yci2hrf26rkbv7m8d6yisykfjanhjm06mp2bj23fsrppd7l") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.34 (c (n "styled") (v "0.1.34") (d (list (d (n "leptos") (r "^0.2.0-beta") (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "1b531qyhlhlcd833bggbbb7fhls3c3khkhicanqal63yn2g7phsj") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.35 (c (n "styled") (v "0.1.35") (d (list (d (n "leptos") (r "^0.2.0-beta") (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "1d7303bcs3by9zaa2x386i8kgj9ks9wrk6v5dfzx779vl85qwrji") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.36 (c (n "styled") (v "0.1.36") (d (list (d (n "leptos") (r "^0.2.0-beta") (k 0)) (d (n "leptos_meta") (r "^0.2.0-beta") (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stylist") (r "^0.12.0") (d #t) (k 0)))) (h "10wnm2dg1zsg4wvpv8y55vsxshiswfzcbqifjgv2bjiykrpa6v20") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr"))))))

(define-public crate-styled-0.1.37 (c (n "styled") (v "0.1.37") (d (list (d (n "leptos") (r "^0.4.8") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "stylist") (r "^0.12.1") (d #t) (k 0)))) (h "0b6k69fzp0q8sm17wwlrmm76bfbcpykw9s6ykagqvx1rj8211kbx")))

(define-public crate-styled-0.1.38 (c (n "styled") (v "0.1.38") (d (list (d (n "leptos") (r "^0.4.8") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "stylist") (r "^0.12.1") (d #t) (k 0)))) (h "0z8a3zq9bh6dw7zzkyzm1596fza0fryq6dliy4l50m39mhkrjq7y")))

(define-public crate-styled-0.1.39 (c (n "styled") (v "0.1.39") (d (list (d (n "leptos") (r "^0.4.8") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "stylist") (r "^0.12.1") (d #t) (k 0)))) (h "0s364az9ghixnvmsaywvxnzks293s3z3yw5ydgr8d3536lcgqqw2")))

(define-public crate-styled-0.1.40 (c (n "styled") (v "0.1.40") (d (list (d (n "leptos") (r "^0.4.8") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "stylist") (r "^0.12.1") (d #t) (k 0)))) (h "0ivhvgjgnkdicjc961h6fmdwaqqy15dpsghvsvdbsyg8dm0v0f3y")))

(define-public crate-styled-0.1.41 (c (n "styled") (v "0.1.41") (d (list (d (n "leptos") (r "^0.4.8") (f (quote ("nightly" "csr"))) (d #t) (k 0)) (d (n "leptos_meta") (r "^0.4.8") (f (quote ("nightly" "csr"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "stylist") (r "^0.12.1") (d #t) (k 0)))) (h "1912ar64i5wr1dwqkki1wg0adgds3lw1ix7mb04rw9jcghm9yg3z")))

(define-public crate-styled-0.2.0 (c (n "styled") (v "0.2.0") (d (list (d (n "leptos") (r "^0.5.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "leptos_meta") (r "^0.5.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "stylist") (r "^0.13") (d #t) (k 0)))) (h "09lj9hwn5s0walqlp06275663grlmwj0b6kb3knqkm1vs9qdld6a") (f (quote (("ssr" "leptos/ssr" "leptos_meta/ssr" "stylist/ssr") ("csr" "leptos/csr" "leptos_meta/csr"))))))

