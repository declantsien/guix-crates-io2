(define-module (crates-io st yl styler_core) #:use-module (crates-io))

(define-public crate-styler_core-0.1.0 (c (n "styler_core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0cg423s30gk8cxzzn8drmvbpn38xb2bxjfdz3x1k4h8wh1gfyipx") (y #t)))

(define-public crate-styler_core-0.1.1 (c (n "styler_core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0cxif9waxhpr1ajazmz5f74sjsm1a51qq3d4rsvf3xmvpn75n4vw") (y #t)))

(define-public crate-styler_core-0.1.2 (c (n "styler_core") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "05fp9p8b2103va7ym5m49p3nph0dyll78zk2xx8iny7gki1dah7d") (y #t)))

