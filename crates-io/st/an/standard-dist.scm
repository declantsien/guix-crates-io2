(define-module (crates-io st an standard-dist) #:use-module (crates-io))

(define-public crate-standard-dist-1.0.0 (c (n "standard-dist") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.72") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0z2vnbdf0fpaqdb46z7rjaxn07zzgp05gxklgcp2dri7fjn1cnlc")))

