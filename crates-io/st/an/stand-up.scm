(define-module (crates-io st an stand-up) #:use-module (crates-io))

(define-public crate-stand-up-0.1.0 (c (n "stand-up") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zrjvh9dg9c8ai33jlc1if4vw5llg0gpf7f7fvahs8604gsrdakb")))

