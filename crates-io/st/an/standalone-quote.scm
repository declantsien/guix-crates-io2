(define-module (crates-io st an standalone-quote) #:use-module (crates-io))

(define-public crate-standalone-quote-0.4.2 (c (n "standalone-quote") (v "0.4.2") (d (list (d (n "rustc-ap-proc_macro") (r "^40.0.0") (d #t) (k 0)) (d (n "standalone-proc-macro2") (r "^0.2.2") (d #t) (k 0)))) (h "1qdmjqdcphgkcpzg8ld802ph5ildj9rwrbbf9zdr3zqkh5xj5ga5")))

(define-public crate-standalone-quote-0.5.0 (c (n "standalone-quote") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.2.3") (k 0)))) (h "0ffqx1d4jvwpldqflv2zms46cmprb9izaa76sv8ykrwqdlfsrvfw") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

