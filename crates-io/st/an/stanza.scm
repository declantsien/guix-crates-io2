(define-module (crates-io st an stanza) #:use-module (crates-io))

(define-public crate-stanza-0.1.0 (c (n "stanza") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "term_size") (r "^0.3.2") (d #t) (k 2)))) (h "13ggilqd39q70vrgw7i4lf3qb4k5y3s2q29fmhbxdfq8ylylkfy7")))

(define-public crate-stanza-0.2.0 (c (n "stanza") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "term_size") (r "^0.3.2") (d #t) (k 2)))) (h "0vq98vafdlyzamcqb637am6ipixmvkcyzki4j7y3sfj2lqs7kd5k")))

(define-public crate-stanza-0.3.0 (c (n "stanza") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "maybe-owned") (r "^0.3.4") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 2)))) (h "0ybinz6z660cwvnl0ci5y67zn6a03j3i3gxn6hsc8h4y87qzgzvf")))

(define-public crate-stanza-0.4.0 (c (n "stanza") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 2)))) (h "0fa6facdwhpgrb1xxq3gjfkcywkg0vshprfd07lk3xdsn75r8h4r")))

(define-public crate-stanza-0.5.0 (c (n "stanza") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 2)))) (h "071cd5dpawnkkz822llhxbpw22bd0hics5j5578vcbanv7fcjb1p")))

(define-public crate-stanza-0.5.1 (c (n "stanza") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 2)))) (h "0x2zjm60np8wxsn0h1f9f655jxsi9kazwizdbl0llljvgac0n8sx")))

