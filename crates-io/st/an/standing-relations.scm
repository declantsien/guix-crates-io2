(define-module (crates-io st an standing-relations) #:use-module (crates-io))

(define-public crate-standing-relations-0.1.0 (c (n "standing-relations") (v "0.1.0") (d (list (d (n "goldenfile") (r "^1.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1gk1w4fwjk7hc0c6d31vrz71kxms9wz0028mfra3d2hs2z67g8m3")))

(define-public crate-standing-relations-0.1.1 (c (n "standing-relations") (v "0.1.1") (d (list (d (n "goldenfile") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1frsls06gnhs7qg8sl160wrmy9dlnw4sinj8i90lkfpj91asxlpb")))

(define-public crate-standing-relations-0.1.2 (c (n "standing-relations") (v "0.1.2") (d (list (d (n "goldenfile") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zmvbrgl8d084gwvr7cqy48cg9v29ygxrl424lgiadcs7cs6cx58")))

