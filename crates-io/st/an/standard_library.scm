(define-module (crates-io st an standard_library) #:use-module (crates-io))

(define-public crate-standard_library-0.1.0 (c (n "standard_library") (v "0.1.0") (d (list (d (n "assert-type-eq") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "merkletree") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1pgm34kw8x7zgcmwh7dhbyh7a1h652ripvhkdax4hhcrp3cvqxjv") (f (quote (("png") ("ico" "bmp" "png") ("bmp")))) (r "1.56.0")))

