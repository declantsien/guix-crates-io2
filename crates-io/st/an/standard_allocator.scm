(define-module (crates-io st an standard_allocator) #:use-module (crates-io))

(define-public crate-standard_allocator-0.1.0 (c (n "standard_allocator") (v "0.1.0") (d (list (d (n "loca") (r "^0.5.4") (d #t) (k 0)))) (h "06g3dvwy5mbw3kx6rljb1rz758q8jyffzx5kvcs42gwg9jxb3l2z") (f (quote (("stable-rust" "loca/stable-rust"))))))

