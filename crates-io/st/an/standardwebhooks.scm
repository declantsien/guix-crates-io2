(define-module (crates-io st an standardwebhooks) #:use-module (crates-io))

(define-public crate-standardwebhooks-1.0.0 (c (n "standardwebhooks") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r ">1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0a0yvwnnxnbwidzc3mhn809a7vgzbk85fl48naamwc0ys6ggwxs1") (y #t)))

(define-public crate-standardwebhooks-1.0.1 (c (n "standardwebhooks") (v "1.0.1") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r ">1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1b440alx2ycky4q3l3mm1j9qj2qah4hb40wchc50yr09vkpn8wkl")))

