(define-module (crates-io st an standard_card) #:use-module (crates-io))

(define-public crate-standard_card-1.0.0 (c (n "standard_card") (v "1.0.0") (h "0nf0nr17kwn0wvpp267b1jdm85f4afgmkq6kr4s0z1hlfkg7iynm")))

(define-public crate-standard_card-1.1.0 (c (n "standard_card") (v "1.1.0") (h "0cr58dwmmyccfj4kd78qrvh1rc2anl7ffwbrbjgm5bl7sc3amm8v")))

(define-public crate-standard_card-1.2.0 (c (n "standard_card") (v "1.2.0") (h "11fnf5wxmp29d08xs4m74m3kmm8mk1jyrqc58jipsc7fdnns78mw")))

(define-public crate-standard_card-1.3.0 (c (n "standard_card") (v "1.3.0") (h "1v6hd7hblya7yv1xfir6phwhmxsdnwrdz73j0s1z4y4zf9vqgx50")))

