(define-module (crates-io st an standalone-proc-macro2) #:use-module (crates-io))

(define-public crate-standalone-proc-macro2-0.2.1 (c (n "standalone-proc-macro2") (v "0.2.1") (d (list (d (n "rustc-ap-proc_macro") (r "^40.0.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1vin4ar5820p744ay9iabk80bji9n1n1rcsi88rxqvp1f11cabya") (f (quote (("nightly"))))))

(define-public crate-standalone-proc-macro2-0.2.2 (c (n "standalone-proc-macro2") (v "0.2.2") (d (list (d (n "rustc-ap-proc_macro") (r "^40.0.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "042l20mzvncgfi4cirvlzipgzh0fplj2mgch6is8akdhgy6f5c04") (f (quote (("nightly"))))))

