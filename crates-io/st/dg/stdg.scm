(define-module (crates-io st dg stdg) #:use-module (crates-io))

(define-public crate-stdg-0.1.0 (c (n "stdg") (v "0.1.0") (d (list (d (n "euclid") (r "^0.20.0") (d #t) (k 0)) (d (n "font-kit") (r "^0.4.0") (d #t) (k 0)) (d (n "minifb") (r "^0.13.0") (d #t) (k 0)) (d (n "png") (r "^0.15.0") (d #t) (k 0)) (d (n "raqote") (r "^0.7.5") (d #t) (k 0)))) (h "0vvpz4s0pjzj7h4kn2yza4rw82j7yvgmk3d3zha8iw2inc28pqf9")))

(define-public crate-stdg-0.2.0 (c (n "stdg") (v "0.2.0") (d (list (d (n "euclid") (r "^0.20.0") (d #t) (k 0)) (d (n "font-kit") (r "^0.4.0") (d #t) (k 0)) (d (n "minifb") (r "^0.13.0") (d #t) (k 0)) (d (n "png") (r "^0.15.0") (d #t) (k 0)) (d (n "raqote") (r "^0.7.5") (d #t) (k 0)))) (h "0zfsr0iswqvsn5cp9fimnlm65bsp78i87n4rzz3d8pnhp9zgfmfn")))

