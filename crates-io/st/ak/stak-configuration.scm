(define-module (crates-io st ak stak-configuration) #:use-module (crates-io))

(define-public crate-stak-configuration-0.1.0 (c (n "stak-configuration") (v "0.1.0") (h "0fpc589a0dzj0c27a7fw76vnkk6fy8gnac19k8hgw1vhhxh4j2f7")))

(define-public crate-stak-configuration-0.1.1 (c (n "stak-configuration") (v "0.1.1") (h "02nph0kak6mnlkzvq30y3imzz6s56bash8nsshmzx621y75bn3ip")))

(define-public crate-stak-configuration-0.1.2 (c (n "stak-configuration") (v "0.1.2") (h "0x8z6ciki768af0k7i6052lnd23y1ipc8zpw4q6jqr6yysixyfwv")))

(define-public crate-stak-configuration-0.1.3 (c (n "stak-configuration") (v "0.1.3") (h "0f9mxfvhl8x1wpbsck30820ryaj8gbnzi3223xw28ch736anp35y")))

(define-public crate-stak-configuration-0.1.4 (c (n "stak-configuration") (v "0.1.4") (h "1bx45v2bi509sb0imqbldk346fm0fb0791hllipqbiq5hbiblxlz")))

(define-public crate-stak-configuration-0.1.5 (c (n "stak-configuration") (v "0.1.5") (h "0g1v761hm0gbrv3863ppzyg8zv0hdp4dd84sqsg78kq5zc1ckwm3")))

(define-public crate-stak-configuration-0.1.6 (c (n "stak-configuration") (v "0.1.6") (h "02yvx2nnl7smrysvyryjki8h4df9v1n6r4c8qf5ikw75g711hax0")))

(define-public crate-stak-configuration-0.1.7 (c (n "stak-configuration") (v "0.1.7") (h "0bwriiwr0ln6vz5c9qbhgsppxhmrz1nxagv71kfxn2smmilyypag")))

(define-public crate-stak-configuration-0.1.8 (c (n "stak-configuration") (v "0.1.8") (h "0fxz4qbplh621kzw020yvbbbg3jy5kwchc4z15hhf68hh4qy6lxa")))

(define-public crate-stak-configuration-0.1.9 (c (n "stak-configuration") (v "0.1.9") (h "1ny4q6p1vvl6q3klwm1hsqwizbnj2dhzc82aj9xbzr2k0bvr5ifk")))

(define-public crate-stak-configuration-0.1.10 (c (n "stak-configuration") (v "0.1.10") (h "0l4m0w2xq9ap2jp7367y7sjsfrkgihqd4sgj5mw6pbf9hvknkwrf")))

(define-public crate-stak-configuration-0.1.11 (c (n "stak-configuration") (v "0.1.11") (h "18pa0ybqndda5c9kx6kd36p9g0l5l0xji2gn5q4nyq48c9a6f5n1")))

(define-public crate-stak-configuration-0.1.12 (c (n "stak-configuration") (v "0.1.12") (h "09xxr1nw5w3cqkjb3r8v6xw0blhh40mc2n59xs5j04139kd4jfp7")))

(define-public crate-stak-configuration-0.1.13 (c (n "stak-configuration") (v "0.1.13") (h "02j2jxg506gg066zk7wkay7kv03qk03km347nv9d3maqj19rx15c")))

(define-public crate-stak-configuration-0.1.14 (c (n "stak-configuration") (v "0.1.14") (h "0q5gc2h6ww870xb65al9m278j3ylwcrrj5m9caixzkj2adpc9xly")))

(define-public crate-stak-configuration-0.1.15 (c (n "stak-configuration") (v "0.1.15") (h "1ll04k0ky7agdmmmb012vjga9pmy9bafilnbclrafyj808dx552m")))

(define-public crate-stak-configuration-0.1.16 (c (n "stak-configuration") (v "0.1.16") (h "1ra082ljaq6486gchj72y98wf82c9397mvq2d8r6r1zvpd39clvv")))

(define-public crate-stak-configuration-0.1.17 (c (n "stak-configuration") (v "0.1.17") (h "06m72ajiry7rcs80rrvqgk36zhl205jq8n8mg89r7kan3hndcs8a")))

(define-public crate-stak-configuration-0.1.18 (c (n "stak-configuration") (v "0.1.18") (h "0z62j4ry2w49hcvh6pw3vbxapyasa8r5sx6nvkaslr8sakw4mzn3")))

(define-public crate-stak-configuration-0.1.19 (c (n "stak-configuration") (v "0.1.19") (h "0b2nn05cr7m0ipzkg9fchfmpsa4hz5id2pk36x7yp119wf7kmb34")))

(define-public crate-stak-configuration-0.1.20 (c (n "stak-configuration") (v "0.1.20") (h "0fn9vzqziyb5q3b2m933j8ks2xwqnpfxzr8vs7dkh5ibhbr2akbs")))

(define-public crate-stak-configuration-0.1.21 (c (n "stak-configuration") (v "0.1.21") (h "0vvdznz9l3aj44yl5yi2iwwpp9d82z5193cdqj054mw5g3j2g7iv")))

(define-public crate-stak-configuration-0.1.22 (c (n "stak-configuration") (v "0.1.22") (h "0yzkyibaxviqw93v8q5wjl13rarbw23mvc51nrf3m7yqv8gsyra4")))

(define-public crate-stak-configuration-0.1.23 (c (n "stak-configuration") (v "0.1.23") (h "1l55ycm1wbfq2y4s5yzpllfzmdkwcrpj7817syhcnk8fq7f1f3zn")))

(define-public crate-stak-configuration-0.1.24 (c (n "stak-configuration") (v "0.1.24") (h "0x3ay420hbfpwsymnbmqy431nrbb6hkra54nickiy94ki7qalrwa")))

(define-public crate-stak-configuration-0.1.25 (c (n "stak-configuration") (v "0.1.25") (h "12vsiw0qk0nz6h7m8vzk4i8qmwa02rn0q1kag7irs4sqfc11qz5n")))

(define-public crate-stak-configuration-0.1.26 (c (n "stak-configuration") (v "0.1.26") (h "1i8r7xn48lawjmsyfgc735k8hzfxshq3qmpy7ha9bwpaf1db3j7k")))

(define-public crate-stak-configuration-0.1.27 (c (n "stak-configuration") (v "0.1.27") (h "18smd3xbv730c1ng6xjcyn1k0wy61gxnx1r96mxp5ic29r2vljiq")))

(define-public crate-stak-configuration-0.1.28 (c (n "stak-configuration") (v "0.1.28") (h "10m5mjvwijams0896rymig5hfrsy2q979cz993c0wpfg930hlpfh")))

(define-public crate-stak-configuration-0.1.29 (c (n "stak-configuration") (v "0.1.29") (h "1w0f5jzdwni77d70s3flirlv3j6gh735ad44r46c3iphk2r0fi07")))

(define-public crate-stak-configuration-0.1.30 (c (n "stak-configuration") (v "0.1.30") (h "151vw0dh4ky2j1p1cszgxp3mwylqay3fimllqsfyx5c2bhi085gv")))

(define-public crate-stak-configuration-0.1.31 (c (n "stak-configuration") (v "0.1.31") (h "1np43pyfh1nfcddgfplpwk4xg82pd45pis796nlhip4fzqrb5yi8")))

(define-public crate-stak-configuration-0.1.32 (c (n "stak-configuration") (v "0.1.32") (h "02d4k4bcdj36r0l69x6rgcm8kbr338vqkjgdd1srs2jqayvfq0vg")))

(define-public crate-stak-configuration-0.1.33 (c (n "stak-configuration") (v "0.1.33") (h "081mfagcnyms39a5bb353wrvs5gjflhq6gwlr9mnxm0jf0x20ryq")))

(define-public crate-stak-configuration-0.1.34 (c (n "stak-configuration") (v "0.1.34") (h "0ipysgi6hqx3p2h5zj9ah3qq0n1mrcsyc9w35hi5k8ckq6q7b1di")))

(define-public crate-stak-configuration-0.1.35 (c (n "stak-configuration") (v "0.1.35") (h "19bmxaa9zbsi8j6c6d9vhvhdgpwafahn1cmyzxnpi9kphxxrr40p")))

