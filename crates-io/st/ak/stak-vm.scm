(define-module (crates-io st ak stak-vm) #:use-module (crates-io))

(define-public crate-stak-vm-0.1.0 (c (n "stak-vm") (v "0.1.0") (d (list (d (n "code") (r "^0.1") (d #t) (k 0) (p "stak-code")) (d (n "device") (r "^0.1") (d #t) (k 0) (p "stak-device")) (d (n "insta") (r "^1.31.0") (d #t) (k 2)))) (h "1dlyi3z9j1m4v3dqydyg3ilwvd6b54dph0mfc42i8alkgchzvgyi") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.1.1 (c (n "stak-vm") (v "0.1.1") (d (list (d (n "code") (r "^0.1") (d #t) (k 0) (p "stak-code")) (d (n "device") (r "^0.1") (d #t) (k 0) (p "stak-device")) (d (n "insta") (r "^1.31.0") (d #t) (k 2)))) (h "0dlgjf4w2jmlnqz6nic1ng0an2wvkcyg9dl12ia2b0rjv1bs9w0l") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.1.2 (c (n "stak-vm") (v "0.1.2") (d (list (d (n "code") (r "^0.1") (d #t) (k 0) (p "stak-code")) (d (n "device") (r "^0.1") (d #t) (k 0) (p "stak-device")) (d (n "insta") (r "^1.31.0") (d #t) (k 2)))) (h "19ga0ldwnjjzxf3wclfx7064rc792zv6qhks6k3mznw2435y47qj") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.2.0 (c (n "stak-vm") (v "0.2.0") (d (list (d (n "code") (r "^0.1") (d #t) (k 0) (p "stak-code")) (d (n "device") (r "^0.1") (d #t) (k 0) (p "stak-device")) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "091gk58483h0wxj3ch9i8ni2jnc1qd2qrs5z3r9bmw37pgvh8kaf") (f (quote (("trace_heap") ("trace") ("std") ("no_inline") ("gc_always"))))))

(define-public crate-stak-vm-0.2.1 (c (n "stak-vm") (v "0.2.1") (d (list (d (n "code") (r "^0.1") (d #t) (k 0) (p "stak-code")) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "12bz5iwhy0qjk9d3ghrhvvph2ijxzzdpkya83g4dsh54xpjdaldm") (f (quote (("trace_heap") ("trace") ("std") ("no_inline") ("gc_always") ("debug"))))))

(define-public crate-stak-vm-0.2.2 (c (n "stak-vm") (v "0.2.2") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "stak_code") (r "^0.1") (d #t) (k 0) (p "stak-code")))) (h "0hwklh3d7557v7vsp7b64z8v3fj3a0gmjzhmgbbj0zq0yfyh9i9v") (f (quote (("trace_heap") ("trace") ("std") ("no_inline") ("gc_always"))))))

(define-public crate-stak-vm-0.2.3 (c (n "stak-vm") (v "0.2.3") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "stak_code") (r "^0.1") (d #t) (k 0) (p "stak-code")))) (h "1r1vs2yx3nc3n00v7jx25hcjijli9kngfsjrva3cv2kkjcz3y4w9") (f (quote (("trace_heap") ("trace") ("std") ("no_inline") ("gc_always"))))))

(define-public crate-stak-vm-0.3.0 (c (n "stak-vm") (v "0.3.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "00qfpx04m9qqap7b0k84yfi32laxjgydb1ycz928pma05sfy1zs1") (f (quote (("trace_heap") ("trace") ("std") ("no_inline") ("gc_always"))))))

(define-public crate-stak-vm-0.3.1 (c (n "stak-vm") (v "0.3.1") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "17xfxpmj2sgdic6mnrrddqzymdmxy3qkzmja1cx2yjshs16m7f5l") (f (quote (("trace_heap") ("trace") ("std") ("no_inline") ("gc_always"))))))

(define-public crate-stak-vm-0.3.2 (c (n "stak-vm") (v "0.3.2") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0x69lw834kd3q438pg87vnbmky846dcj6vgc4kfvqhv1f61nl286") (f (quote (("trace_heap") ("trace") ("std") ("no_inline") ("gc_always"))))))

(define-public crate-stak-vm-0.3.3 (c (n "stak-vm") (v "0.3.3") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc"))) (d #t) (k 2)))) (h "03g3wwwqmpnabb8vhxp39487swpmjas07dwqb4vdq8j7jmdf8hhy") (f (quote (("trace_heap") ("trace") ("std") ("no_inline") ("gc_always"))))))

(define-public crate-stak-vm-0.3.4 (c (n "stak-vm") (v "0.3.4") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1wnpw67pwc72arl5s3linh9nd957i38crry38yrfd6n4gcl40qbr") (f (quote (("trace_heap") ("trace") ("std") ("no_inline") ("gc_always"))))))

(define-public crate-stak-vm-0.3.5 (c (n "stak-vm") (v "0.3.5") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "163nh7lhcx858nh0ppwzki28ydcca7jc9ndjanxm2pn5l2g5gk8y") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.6 (c (n "stak-vm") (v "0.3.6") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "17dn6lfy5drv4x6nd2z0zq7mhypbbxan70pn1vza5pg647navxl1") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.7 (c (n "stak-vm") (v "0.3.7") (d (list (d (n "insta") (r "^1.35.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1fpi4qdgdn3dyxqv1n332hsp9w5vsahg1zx7kpp75gfvx65c0q8c") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.8 (c (n "stak-vm") (v "0.3.8") (d (list (d (n "insta") (r "^1.35.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0jvcdvawsw55rhfmd2gdnmxb6ngiwgsa92wq4xk1m683v75yrv6p") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.9 (c (n "stak-vm") (v "0.3.9") (d (list (d (n "insta") (r "^1.35.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1m9mbnvvm0cbfr6gz7s14iygfawrmihq170vc628a5hl12vwxll7") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.10 (c (n "stak-vm") (v "0.3.10") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0blcms3ayn981nylvp47c84imcv39v8zig5hl273m9lsm4bigpp0") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.11 (c (n "stak-vm") (v "0.3.11") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1w4qxrxd8rlqiwax0g6irchb31f4d68pr6npshbzhljrcfvy9qxr") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.12 (c (n "stak-vm") (v "0.3.12") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "00xixx75r6zj78vyyv7s4s3mdh8wcrl0hnx8lfik0w5vjzvbf3x9") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.13 (c (n "stak-vm") (v "0.3.13") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1gip8jycpv0vr2xrrv3jwxxfl982zq2h06q0zg54prixh6vmnf1p") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.14 (c (n "stak-vm") (v "0.3.14") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0jhc3abpnizbgr2vb6k7hm95kpsrr8qlydcw5l9k8fmkf4rqv209") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.15 (c (n "stak-vm") (v "0.3.15") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1jbka9wv0b1b0af0pybcfl3an59245lbky1f048vmiwzap0jhsyf") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.16 (c (n "stak-vm") (v "0.3.16") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0560n059mwjwin63d4w4spb5z48vzlnf62cgabs1dciwj2vdhinq") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.17 (c (n "stak-vm") (v "0.3.17") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "18myq4c7q81iwg2z0f3mhgqjbrc47pw7xryc00wqsmx3nddi035z") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.18 (c (n "stak-vm") (v "0.3.18") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "173nply9x1039nhpfz61xjl9ak8k1n9z1cy6m526lndfkayx48vj") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.19 (c (n "stak-vm") (v "0.3.19") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "05d2ba222fcga4ax9bpl1b6n8mna07r2v5vh8bki7sg56lbzkgn1") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.20 (c (n "stak-vm") (v "0.3.20") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1lgq00np14rgb8lfggnbpl6arqwj4cxk0jvm0q50klk1172zm0x6") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.21 (c (n "stak-vm") (v "0.3.21") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0n73vy1v48pgynzagqrr557il0nqfqk9rqbjcm3cvrl2k6p1sn2s") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.22 (c (n "stak-vm") (v "0.3.22") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1vqjrny232rh8y4x5a5nbahp2x2mhcp7irp0yajawnfa3c1bhz81") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.23 (c (n "stak-vm") (v "0.3.23") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0gy8mni085bhj0l6pr9mf69qyx65q8qkppwkdps5gl7p2kl50bnm") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.24 (c (n "stak-vm") (v "0.3.24") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0590c52k2iwqgn4h6waakpwn8bx3v3d921vik0igsiz8wjpbxbay") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.25 (c (n "stak-vm") (v "0.3.25") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1cyy774472951xn0q3fg33ixihbjapf2m4z7m73ag9p3j0zzblg5") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.26 (c (n "stak-vm") (v "0.3.26") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1sg0n145by2fm0bgxr6kixsc8n9cmcxfrpn6njmdcjxd98djgxi6") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.27 (c (n "stak-vm") (v "0.3.27") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1flv7iq4730a96nk68djxf0m0pr7pn72n2cpqakn2sab2jyy72b2") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.28 (c (n "stak-vm") (v "0.3.28") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1mibyfa19pvxb9gs4aikfw1fiyr7kq50pr6dwdasq9ky1xlk3y1s") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.29 (c (n "stak-vm") (v "0.3.29") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1kmjs22rlq20s6kgybd34fvizdlj0n6fahrl32023aq508gpkd4r") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.3.30 (c (n "stak-vm") (v "0.3.30") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1grb7crrb7jzb59g5rbny0pv4cpyxih4wbd9v5j2zjivfg6hch85") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.4.0 (c (n "stak-vm") (v "0.4.0") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0b2g303nrqqbi3xn5vwv6qmdj33hi6zdis8gwxgyhfvpqlx11axy") (f (quote (("trace_heap") ("trace") ("std") ("gc_always"))))))

(define-public crate-stak-vm-0.4.1 (c (n "stak-vm") (v "0.4.1") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "19dpcbkfigp6ixm3qmvh4g2fyi3ngnr744vqnbj9qbbd81q52k4l") (f (quote (("trace_instruction") ("trace_heap") ("std") ("profile") ("gc_always"))))))

(define-public crate-stak-vm-0.4.2 (c (n "stak-vm") (v "0.4.2") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0pfwppbz0lc4gvrqlql0pjh9rlqblapf1jrxsnar9zil9z6a6mrj") (f (quote (("trace_instruction") ("trace_heap") ("std") ("profile") ("gc_always"))))))

(define-public crate-stak-vm-0.4.3 (c (n "stak-vm") (v "0.4.3") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0x0kb1a347wdrdv3bap1asbyzp61ppwbsc5r0fgkx418midxqqp2") (f (quote (("trace_instruction") ("trace_heap") ("std") ("profile") ("gc_always"))))))

(define-public crate-stak-vm-0.4.4 (c (n "stak-vm") (v "0.4.4") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1ly49fmf9lmq5hb8vd114q6jc3iammfrb90a4ld86xzx6jfcs7p4") (f (quote (("trace_instruction") ("trace_heap") ("std") ("profile") ("gc_always"))))))

(define-public crate-stak-vm-0.4.5 (c (n "stak-vm") (v "0.4.5") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "09g9bs49f0n47nk211x6316lxq403i9g4bqcd3pra44ah6gkjzfg") (f (quote (("trace_instruction") ("trace_heap") ("std") ("profile") ("gc_always"))))))

(define-public crate-stak-vm-0.4.6 (c (n "stak-vm") (v "0.4.6") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "03lkxbm4dcy99i8lqybgd9wv41lcyi4dvsx9rzrkp06zhnb96a08") (f (quote (("trace_instruction") ("trace_heap") ("std") ("profile") ("gc_always"))))))

(define-public crate-stak-vm-0.4.7 (c (n "stak-vm") (v "0.4.7") (d (list (d (n "insta") (r "^1.39.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "15dpr4qk2rbw4p9gvj39xhafqfxad14ykki0j1bcjzafsbh9gp4r") (f (quote (("trace_instruction") ("trace_heap") ("std") ("profile") ("gc_always"))))))

(define-public crate-stak-vm-0.4.8 (c (n "stak-vm") (v "0.4.8") (d (list (d (n "insta") (r "^1.39.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "1rii5qxd2cmypkis8vn3j91jjpqig501wc0kz2j279d6bqn7822h") (f (quote (("trace_instruction") ("trace_heap") ("std") ("profile") ("gc_always"))))))

(define-public crate-stak-vm-0.4.9 (c (n "stak-vm") (v "0.4.9") (d (list (d (n "insta") (r "^1.39.0") (d #t) (k 2)) (d (n "stak-code") (r "^0.2") (d #t) (k 0)))) (h "0sfzhv8wp15x5r93w4yqcw601p8smxmpfbwm9h3ndki4sbvx1c7c") (f (quote (("trace_instruction") ("trace_heap") ("std") ("profile") ("gc_always"))))))

