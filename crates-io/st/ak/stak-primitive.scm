(define-module (crates-io st ak stak-primitive) #:use-module (crates-io))

(define-public crate-stak-primitive-0.1.1 (c (n "stak-primitive") (v "0.1.1") (d (list (d (n "device") (r "^0.1") (d #t) (k 0) (p "stak-device")) (d (n "vm") (r "^0.2") (d #t) (k 0) (p "stak-vm")))) (h "0682322izavfdq4jcpw52yzwmhsi4n2akyvp1h1zyb29r63dwbq3") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.1.2 (c (n "stak-primitive") (v "0.1.2") (d (list (d (n "device") (r "^0.1") (d #t) (k 0) (p "stak-device")) (d (n "vm") (r "^0.2") (d #t) (k 0) (p "stak-vm")))) (h "1yhiykj4y3gmldfwnnk3vcz891dfmwxzvx8k8chdsbashjydsp2i") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.1.3 (c (n "stak-primitive") (v "0.1.3") (d (list (d (n "device") (r "^0.1") (d #t) (k 0) (p "stak-device")) (d (n "vm") (r "^0.2") (d #t) (k 0) (p "stak-vm")))) (h "1cmf48v1xaxa1gd2qr4v6cl4pb6f6c2g51sh5v5q3rg995lhfqif") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.0 (c (n "stak-primitive") (v "0.2.0") (d (list (d (n "stak-device") (r "^0.1") (d #t) (k 0)) (d (n "stak-vm") (r "^0.2") (d #t) (k 0)))) (h "0mishgmmi3mdr5nr31swn50fs25xsysxmg6rp7f6fsm7nnlzmxrx") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.1 (c (n "stak-primitive") (v "0.2.1") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "1lzd7j1602j7qa201dyfjk815pinkv07ghis5psk47k88yvi9vni") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.2 (c (n "stak-primitive") (v "0.2.2") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "1mwss29cspcv0m556hapsxh49a4nrqrvnqvjyyq2px2mb1kwywql") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.3 (c (n "stak-primitive") (v "0.2.3") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "0isdfwy8mgysxhf7b7q7zgqzym9riz61mvpm52d5mvgkvfz7x7jf") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.4 (c (n "stak-primitive") (v "0.2.4") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "1h0vwcyrmc6xqcdp8h9ba2052c2f33x1bvh3a0ns814l5x91lnm9") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.5 (c (n "stak-primitive") (v "0.2.5") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "03dpadzqjbb8lkq20rmam9a2xgnrkiqrijybk54i11j4nysyrmfn") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.6 (c (n "stak-primitive") (v "0.2.6") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "1kp1x7zb4zlv9026fikbs9rvyd9x6p544p5z6jbrybbrdakpc7mp") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.7 (c (n "stak-primitive") (v "0.2.7") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "0rvh81z5058rvwhcwhxrbzrn8arrjw99r54v1lnlxrb7arsxg4d2") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.8 (c (n "stak-primitive") (v "0.2.8") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "0i5kpwxhpyi3c9jj0q9hy9plpncmbdb7cbqwib5bks9q5xdka9gh") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.9 (c (n "stak-primitive") (v "0.2.9") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "1nz4qndrad7i7srswadw5lnpnvch37k2g149j9ksgfxsy38a8kgd") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.10 (c (n "stak-primitive") (v "0.2.10") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "09hd09yzmf3awgww4cqswb872ny1a6l1sg0lhpfc65xsm0kzpa31") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.11 (c (n "stak-primitive") (v "0.2.11") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "0w79nfsb9anf9pgqwba87q91hd8v62fzmrmnp0ajfh6y7b0dy393") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.12 (c (n "stak-primitive") (v "0.2.12") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "1b9hz0zh67y0lay4mgii193nkpphgsvs2f25k8ln5hc01ba1kjgi") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.13 (c (n "stak-primitive") (v "0.2.13") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "18xjxan4wwxv9z4vpn5jzkn327n0l2x62gd3wdsf134a09kp001b") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.14 (c (n "stak-primitive") (v "0.2.14") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "09y7k38x5l5g50rnzfl7jrs1d8kysdfkl3q5l6xzm4xdwpay349s") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.15 (c (n "stak-primitive") (v "0.2.15") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "1pw0p50vibk22ngw8hx9z18cqq13k6rwl2clls0k9v4rf11iyl38") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.16 (c (n "stak-primitive") (v "0.2.16") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "112ql0g10jss534w2d06b38bnk0wkvp7a84x5vdf4khz7cc6qkzq") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.17 (c (n "stak-primitive") (v "0.2.17") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "17ncynkmk2hc9ab6q636a2h6c9gvm0w6daj4yy3bli93df1wxp10") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.18 (c (n "stak-primitive") (v "0.2.18") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "146ahrwln1yrmfg3cx79q5yayv5zz482pnvpsbjkxn8rj3wvzc5b") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.19 (c (n "stak-primitive") (v "0.2.19") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "0aifxkl33ji34dz8dyz879srwji177jd09rwn2jigbk9qml39zld") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.20 (c (n "stak-primitive") (v "0.2.20") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "15ybwhmr0fhndi2ags1pzkmsz6gbnhp3p7jmvl35krb547m2c8d9") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.21 (c (n "stak-primitive") (v "0.2.21") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "0775xz6icj3lagqf9hz0cv7dmhd3xb1vczfrm823nm6q6x3vqamz") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.22 (c (n "stak-primitive") (v "0.2.22") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "17g2zqx2d9kdgpi3q2jd8afmi9p6a4g0mnjz2pm60q56lpmm9my2") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.23 (c (n "stak-primitive") (v "0.2.23") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "1qkwwhd176g658x2k7rhblm097ywi9mpkrm6wffhw4h7ya7xz8gj") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.24 (c (n "stak-primitive") (v "0.2.24") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "0a1mr54yc9w2826lg8hgzwi1ap20laipvyspw9kcjd2xzc3hs637") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.25 (c (n "stak-primitive") (v "0.2.25") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "0np0pqvsz3lxa2m8qv2w177gxb5fvy6l2gxgaplizhhs4j4vs5h7") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.26 (c (n "stak-primitive") (v "0.2.26") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "12pvb2mkg4m6w4p8ynvd58g0xr01hlcn3cqfmvq4jir0xh194w39") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.27 (c (n "stak-primitive") (v "0.2.27") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "0a34n87y73yc8jxbax0zlnxldlcq78qsmfilzf51jrpgbn485psw") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.28 (c (n "stak-primitive") (v "0.2.28") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "1m2pl29rrm59r00nb2ygb43nw2an6h658m0hbz6ljvgyzfmcqapb") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.29 (c (n "stak-primitive") (v "0.2.29") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "1qz679nkq4jcy2vdv7wd534yxj4wafx8sxmpprmmw8mdbx2j6llc") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.30 (c (n "stak-primitive") (v "0.2.30") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3") (d #t) (k 0)))) (h "0qmyn94xgknxvyjpv2pwpzd746p9dmysiilihqa94xbvbc71zzgc") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.31 (c (n "stak-primitive") (v "0.2.31") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4") (d #t) (k 0)))) (h "01n4h2209xfkavka11b3j50m6q68iid9vah5iavvih8paifvnvw2") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.32 (c (n "stak-primitive") (v "0.2.32") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4") (d #t) (k 0)))) (h "0gpmhk8c9xj1qr7x0aig1jkmnyycif2w4wkiib16zhcnhn6czxr2") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.33 (c (n "stak-primitive") (v "0.2.33") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4") (d #t) (k 0)))) (h "0wa1xj5nwqqv7bqj836b3b1j0wasm0cs6b3vs03bavlb4fmyjp5d") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.34 (c (n "stak-primitive") (v "0.2.34") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4") (d #t) (k 0)))) (h "1phcx0vjxwwann8f4lfl1war920pvgz1dma40z6706d4k1x6mpgq") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.35 (c (n "stak-primitive") (v "0.2.35") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4") (d #t) (k 0)))) (h "0c0dci367qzpbv7wiif5pqynbzj3hx7zw4j99bwa0gvi21a716rf") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.36 (c (n "stak-primitive") (v "0.2.36") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4") (d #t) (k 0)))) (h "1c2hr6dw3zcfhggwwpqbyhaaib19ls4qvkjm4x3rbmrh8qw734d9") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.37 (c (n "stak-primitive") (v "0.2.37") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4") (d #t) (k 0)))) (h "08wy4d2m57mikbrxjq0n2fh4n92c9ymgbs3ssrgm96jlfxnackcp") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.38 (c (n "stak-primitive") (v "0.2.38") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4") (d #t) (k 0)))) (h "0q6c6dyrq3jya87a0arfi9pkhk13i3n8i59ckqy1x2lkpq8vkg9h") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.39 (c (n "stak-primitive") (v "0.2.39") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4") (d #t) (k 0)))) (h "0p98xbxh9dxrri0a39imfzbswc32zryibyczvpg79rwf4bic4lk1") (f (quote (("std"))))))

(define-public crate-stak-primitive-0.2.40 (c (n "stak-primitive") (v "0.2.40") (d (list (d (n "stak-device") (r "^0.2") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4") (d #t) (k 0)))) (h "1xq70pkzcy7imypryk27vj0z1phfdi23jl8kazglflivw02484rg") (f (quote (("std"))))))

