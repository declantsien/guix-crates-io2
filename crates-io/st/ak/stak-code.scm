(define-module (crates-io st ak stak-code) #:use-module (crates-io))

(define-public crate-stak-code-0.1.0 (c (n "stak-code") (v "0.1.0") (d (list (d (n "insta") (r "^1.31.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1prlfc43nxxys2r2qcgp5bip74qcciq20lm3hl97h87xfiqajsmq") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.1.1 (c (n "stak-code") (v "0.1.1") (d (list (d (n "insta") (r "^1.31.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "150sfaxy8ph9c3py1qxcc57z8iw3qzazzmhy4nsf36jl39abwzxg") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.1.2 (c (n "stak-code") (v "0.1.2") (d (list (d (n "insta") (r "^1.31.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1la5ziynd789cnci0jzipzalpkwbzxfnqv8bl4pwpyck750s8kmy") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.1.3 (c (n "stak-code") (v "0.1.3") (d (list (d (n "insta") (r "^1.31.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1f5mpy143h470ky4ws4cad3v9yr2zhp41mg8hpx7cd6g8b3if8aq") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.1.4 (c (n "stak-code") (v "0.1.4") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0kd5kiy90qblfwq7rm7pqldvj3p8f3d886pi2g75inw04h2bhv9h") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.1.5 (c (n "stak-code") (v "0.1.5") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1xyn6y8nzm7826z2dwh35im14h5sj396sskyqyrqd4d4nwfdb65v") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.1.6 (c (n "stak-code") (v "0.1.6") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0sgr4hcynb6qmgnc8wh7vcfb8yafs9kwip1qcxvx8vky93l8hg7i") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.1.7 (c (n "stak-code") (v "0.1.7") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0g7ykkwmlf89hg6gnip7332wh3vy6ys2mdqk3dzarnmf6j4cgd36") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.0 (c (n "stak-code") (v "0.2.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "191fh34hgmwjby5xgc5dn6m32rqghlii3p9sk8rdm3rxwfj3l9ns") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.1 (c (n "stak-code") (v "0.2.1") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1sh2s0lq7xag7qpbczc7hnvpajv72hxgchv7gsdqvhi987i979ak") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.2 (c (n "stak-code") (v "0.2.2") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1hy78wjzwn2mq12yp0id971bx0lmhbmvfvl04hhlial8zsacrg8c") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.3 (c (n "stak-code") (v "0.2.3") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "01vv9pvxgppz5q32gv5zi4m5dx7b34sm2ycf3rid88cwfmca0hda") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.4 (c (n "stak-code") (v "0.2.4") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "08m3dxf4m1r4kzgq5s1cfi2p18h8hspgx9j1k6yk5cn5dndswzj8") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.5 (c (n "stak-code") (v "0.2.5") (d (list (d (n "insta") (r "^1.35.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1j9hjwmhjrwwbys774rwgzkpyw7d8y9mzvzn18w1xv3g0ax1grph") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.6 (c (n "stak-code") (v "0.2.6") (d (list (d (n "insta") (r "^1.35.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "152m6ajk8anh3jzy756zhvdgx0z6vrylvdhb7gpbgxxhwxl66aly") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.7 (c (n "stak-code") (v "0.2.7") (d (list (d (n "insta") (r "^1.35.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "107cv7ppwrdx413jq9mskf1623yzbrnr6swnrr62vnhxixmlqkly") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.8 (c (n "stak-code") (v "0.2.8") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "07cj5r9plpvwpmw76nacvvwiqxxa9dhzkbdffclmc1p81i1s6dk2") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.9 (c (n "stak-code") (v "0.2.9") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0ja97d0v8s74z3ivx7ks2kva7xarqpf5cln08xvd0j6yxw6gggjv") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.10 (c (n "stak-code") (v "0.2.10") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0w7hmb9fngnqjbyw27w4j1gm05zjda410xn7z38iih254fakc08z") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.11 (c (n "stak-code") (v "0.2.11") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1f7rq842npzdyjqmh080ipijif3n8r4h5hmjjvy4ad8rh6k8v8dy") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.12 (c (n "stak-code") (v "0.2.12") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0890r4alfbmcbgxaxw4q4gpynrhh0mnvyn3a2h9120zwl111idap") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.13 (c (n "stak-code") (v "0.2.13") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0a9b0p9dn9kvg0clcxs4s1fmncbbwdlc1zkf4w1a67hc2cykb6hv") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.14 (c (n "stak-code") (v "0.2.14") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1cj07s3v4665anrkb0iqa8yh9wsnc6p5qax2c39xzah2cysd6v8g") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.15 (c (n "stak-code") (v "0.2.15") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0f47b94af2kjr7v7s3cx2zh1izwaz2whhldvjn97a7lmd1ikfqza") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.16 (c (n "stak-code") (v "0.2.16") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1qcbs3hgy88cgg99k39zmnk9pq67jj74lnmihaz96lv1r1px3zlq") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.17 (c (n "stak-code") (v "0.2.17") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0i46gl5ganmcba03jfksv22va9lnhfcbhz4b2mg2a6hgv9a4b93z") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.18 (c (n "stak-code") (v "0.2.18") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "17n1i8mfz9ya3wx9zn38b00c1x5wli4s1zax25hz3swyi5ij8874") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.19 (c (n "stak-code") (v "0.2.19") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1mqapxx5yinh78kadmybw7n9ixq7h435mdkdnklb4qzld1av4972") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.20 (c (n "stak-code") (v "0.2.20") (d (list (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "09ahf26dhnxyzx6kc08skmxam6q5lnc3f5gsmgchsdwn6wq6jqqi") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.21 (c (n "stak-code") (v "0.2.21") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "14jss5f90718d52bacl82j5qdv56pny2klx0ahj3sy6gq8m3qy66") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.22 (c (n "stak-code") (v "0.2.22") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "05ssrzmbgfq8k6c6wnba1rsa0hgcy6cgrvd4acnxnb0970a3lv3n") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.23 (c (n "stak-code") (v "0.2.23") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0zms496nvcxhdfv54l7p1xvhml4hq3zrmka9dk8a30vm6ihmv9mx") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.24 (c (n "stak-code") (v "0.2.24") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0qah6h9qw80m59g41dhg5kg0smg4q304fc3b87n799mrxnlb55r3") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.25 (c (n "stak-code") (v "0.2.25") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "05bi0rrqzflz9j1c75dwpq1jq01ianahwrywx3l8r7vjls58vmal") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.26 (c (n "stak-code") (v "0.2.26") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0fbz07vakvg7v35h91jnyvw1wnk3cqvsap3zfw5wpz82dvlb1msm") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.27 (c (n "stak-code") (v "0.2.27") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1h71j4myhlykm84bv3p0nrmrccqldr6rml6dhwp0w12y6n51pp4r") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.28 (c (n "stak-code") (v "0.2.28") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0dm0gc2nwma9fjv5x3nibvz23rl0y4a1y77r9bifn4401rsylccg") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.29 (c (n "stak-code") (v "0.2.29") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0c7ywsd63qi8xm1v3rs2c90zfvjacdxz3p1sdkf9rng691z0fbln") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.30 (c (n "stak-code") (v "0.2.30") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0bxsgx2z8dw1bj1ffbxz64bi7b2iqw2p8h0x286495jsf268wcgm") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.31 (c (n "stak-code") (v "0.2.31") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "19z2p7xfv963f9a1ahcma98bd9qldqgaz34qn9pjb6zrb5696l11") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.32 (c (n "stak-code") (v "0.2.32") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "07g3nlszi1vzv9ihwiilg8qwq9xddd1abb9cyp0z1j50b4skpl3n") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.33 (c (n "stak-code") (v "0.2.33") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "06av9vz3dq27rxwzpda782djgyl6l6kiiihy2lsvmzg4hq6jm9y0") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.34 (c (n "stak-code") (v "0.2.34") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0fni1v1kw3y6cgrngs53vwi022051jiz4cac40mx3xnlj6ba67k0") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.35 (c (n "stak-code") (v "0.2.35") (d (list (d (n "insta") (r "^1.38.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "15acyy3kwh06xxbghrvaglm7iyjdnf8ajw397y25n797l18wkjcn") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.36 (c (n "stak-code") (v "0.2.36") (d (list (d (n "insta") (r "^1.39.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1rnnyniajfvn2r8xyps25yhfxjs80yv9x03fdl4027326srxpj9q") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.37 (c (n "stak-code") (v "0.2.37") (d (list (d (n "insta") (r "^1.39.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "15d9chyym1jm5kflcgs4kvdsmrarb678x8njh0mbynj5zcv3h1cw") (f (quote (("std") ("alloc"))))))

(define-public crate-stak-code-0.2.38 (c (n "stak-code") (v "0.2.38") (d (list (d (n "insta") (r "^1.39.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1c042valcyc6lzl9dg00k8ihvxvv9nkd9iv908fqswz966b0l967") (f (quote (("std") ("alloc"))))))

