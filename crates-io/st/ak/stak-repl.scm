(define-module (crates-io st ak stak-repl) #:use-module (crates-io))

(define-public crate-stak-repl-0.1.0 (c (n "stak-repl") (v "0.1.0") (d (list (d (n "stak-sac") (r "^0.1.35") (d #t) (k 0)))) (h "0yzz7zy99k7gh2i06711k3mpc96f3fdv1y9qcjh0lr0syjmswcir")))

(define-public crate-stak-repl-0.1.1 (c (n "stak-repl") (v "0.1.1") (d (list (d (n "stak-sac") (r "^0.1.37") (d #t) (k 0)))) (h "00wxkcdiin5najfr4ga7vbcsrr7v7d1is8vwncckdz51b41sbfkj")))

(define-public crate-stak-repl-0.1.2 (c (n "stak-repl") (v "0.1.2") (d (list (d (n "stak-sac") (r "^0.1.38") (d #t) (k 0)))) (h "06c15y232lb12szxwwxwkwjfdvyszfq8aqsv7f8dywp9i3h68rx5")))

(define-public crate-stak-repl-0.1.3 (c (n "stak-repl") (v "0.1.3") (d (list (d (n "stak-sac") (r "^0.1.39") (d #t) (k 0)))) (h "1kq7ylamdgkrn9xy1g5pq602ld3gzq8d1rh0hlyn63xzsfvnkxv2")))

(define-public crate-stak-repl-0.1.4 (c (n "stak-repl") (v "0.1.4") (d (list (d (n "stak-sac") (r "^0.1.40") (d #t) (k 0)))) (h "052q14zn8krr24g9hpb1a70lrhh1pp2fgb6vgnzcs471f8kxvrxp")))

(define-public crate-stak-repl-0.1.5 (c (n "stak-repl") (v "0.1.5") (d (list (d (n "stak-sac") (r "^0.1.41") (d #t) (k 0)))) (h "1zmsqx5w28dsxkxyl5qpwqskk9qwnfkp1l90hy84bgbvyql3cs8f")))

