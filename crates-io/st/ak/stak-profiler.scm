(define-module (crates-io st ak stak-profiler) #:use-module (crates-io))

(define-public crate-stak-profiler-0.1.0 (c (n "stak-profiler") (v "0.1.0") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "1x0p2d4lan5c98bmq80g27hb7sqig5d7kclhd1rxzb6ma41chi9m")))

(define-public crate-stak-profiler-0.1.1 (c (n "stak-profiler") (v "0.1.1") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "stak-vm") (r "^0.4.2") (d #t) (k 0)))) (h "19383n5rs6caajc02rx1m862kypihyyvrqfgv3a9fjzkzg6v1gpa")))

(define-public crate-stak-profiler-0.1.2 (c (n "stak-profiler") (v "0.1.2") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "stak-vm") (r "^0.4.3") (d #t) (k 0)))) (h "00r69mdvq1pa3pbcpxyavmn8dw050d349kkvnfljhdj4nzspqkha")))

(define-public crate-stak-profiler-0.1.3 (c (n "stak-profiler") (v "0.1.3") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "stak-vm") (r "^0.4.4") (d #t) (k 0)))) (h "1fg2il9vjfnhh5x5wn5a7knivsawf6sw8h78lrfplh8whhb33bjb")))

(define-public crate-stak-profiler-0.1.4 (c (n "stak-profiler") (v "0.1.4") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "stak-vm") (r "^0.4.5") (d #t) (k 0)))) (h "1kfi24830wb4cldwq7na2fh0fvbmzd1n23li3qb60riq7zzrd0ab")))

(define-public crate-stak-profiler-0.1.5 (c (n "stak-profiler") (v "0.1.5") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "stak-vm") (r "^0.4.6") (d #t) (k 0)))) (h "0wl9f70hslj88w5cc09dikjy3ykzxkwc4laykck3nmpx0680c07i")))

(define-public crate-stak-profiler-0.1.6 (c (n "stak-profiler") (v "0.1.6") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "stak-vm") (r "^0.4.7") (d #t) (k 0)))) (h "0ls9nmwvsh3qh4ggw3dkf59nw5lvdjbqlah97i4y27463ypb1saz")))

(define-public crate-stak-profiler-0.1.7 (c (n "stak-profiler") (v "0.1.7") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "stak-vm") (r "^0.4.8") (d #t) (k 0)))) (h "0wkx63hqpq7cwz1gfhak78x6q96p1wzkymkxk2bsb3xnpqlsgy3s")))

(define-public crate-stak-profiler-0.1.8 (c (n "stak-profiler") (v "0.1.8") (d (list (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "stak-vm") (r "^0.4.9") (d #t) (k 0)))) (h "0r0wvb4adcdyihzq2i18924k0pkl9kck6c0c88zpzp5rqk3c5xwd")))

