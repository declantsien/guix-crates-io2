(define-module (crates-io st ak stakker_tui) #:use-module (crates-io))

(define-public crate-stakker_tui-0.0.1 (c (n "stakker_tui") (v "0.0.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)) (d (n "stakker") (r "^0.1") (d #t) (k 0)) (d (n "stakker_mio") (r "^0.1") (d #t) (k 0)))) (h "1c26bvvvw5whm07njqxj433nh3ay43ji3nmfw9rzd0mqfx37kshj") (f (quote (("unstable") ("default"))))))

(define-public crate-stakker_tui-0.0.2 (c (n "stakker_tui") (v "0.0.2") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)) (d (n "stakker") (r "^0.1") (d #t) (k 0)) (d (n "stakker_mio") (r "^0.1") (d #t) (k 0)))) (h "192m312xdhxr05vqfal90fd0xm4a2smvnrmj3imkxmr7jp60g7n9") (f (quote (("unstable") ("default"))))))

(define-public crate-stakker_tui-0.0.3 (c (n "stakker_tui") (v "0.0.3") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)) (d (n "stakker") (r "^0.2") (d #t) (k 0)) (d (n "stakker_mio") (r "^0.2") (d #t) (k 0)))) (h "0x5bifgn1gfv1h132mqbg7r30jipk7pchw62x0wdgx56yfl8l437") (f (quote (("unstable") ("default"))))))

(define-public crate-stakker_tui-0.0.4 (c (n "stakker_tui") (v "0.0.4") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)) (d (n "stakker") (r "^0.2") (d #t) (k 0)) (d (n "stakker_mio") (r "^0.2") (d #t) (k 0)))) (h "1xrb793r4yfy8dribagcw275c5v49ncrmg9s0faj62vk7vfdj9qc") (f (quote (("unstable") ("default"))))))

