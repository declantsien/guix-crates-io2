(define-module (crates-io st ak stak-compile) #:use-module (crates-io))

(define-public crate-stak-compile-0.1.1 (c (n "stak-compile") (v "0.1.1") (d (list (d (n "sac") (r "^0.1") (d #t) (k 0) (p "stak-sac")))) (h "0qsxsydjpqpnjamm1spk5fhf3sd6bff6yby9kgg34y5ah3d0gp38")))

(define-public crate-stak-compile-0.1.2 (c (n "stak-compile") (v "0.1.2") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0angqzyik5mgmlxfgnsza4ddffxxgyvz149xs5qdgv7wvhsdyk17")))

(define-public crate-stak-compile-0.1.3 (c (n "stak-compile") (v "0.1.3") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0rjqf9yrd9r7nxrp2q6fxr4lx5c3l6y60yaaf5bdysg21s5hf0rz")))

(define-public crate-stak-compile-0.1.5 (c (n "stak-compile") (v "0.1.5") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1wvgyjlh4vmccchl30pyrsia0d5f2ifqvcahm6ggrbi7dqlm1axx")))

(define-public crate-stak-compile-0.1.6 (c (n "stak-compile") (v "0.1.6") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1i9fni6cgdrrldxrjyh04ai64drd3h31z0mcmpfhyambxi2hbfa1")))

(define-public crate-stak-compile-0.1.7 (c (n "stak-compile") (v "0.1.7") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "165b290s0d5nl6gm1zi64c78xzgf4rckl1cvk7bnb0shs1ad10fv")))

(define-public crate-stak-compile-0.1.8 (c (n "stak-compile") (v "0.1.8") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0a7iqjnjjj0nnz812wyns6jw6pgyjyjbrp5xn342ig96dbv3q9ga")))

(define-public crate-stak-compile-0.1.9 (c (n "stak-compile") (v "0.1.9") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "13qdcbwi26jm4l7wc93bgfky02dmvihy2v49d2fy7md57q420g7q")))

(define-public crate-stak-compile-0.1.10 (c (n "stak-compile") (v "0.1.10") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1igvfd9bw60bgfi1088rsp0pgrrkvlma22bl1z42r1pnxq7lg1sy")))

(define-public crate-stak-compile-0.1.11 (c (n "stak-compile") (v "0.1.11") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "06qhqvgfmv3xdwa8k6iqpzgryxvgyn9f12vk2lizy2fpcd006aac")))

(define-public crate-stak-compile-0.1.12 (c (n "stak-compile") (v "0.1.12") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0sr1sc1j5fnh2cydiigbmyrj48y706r64yxp5g2fwnlwxy5d0dks")))

(define-public crate-stak-compile-0.1.13 (c (n "stak-compile") (v "0.1.13") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0abzm9jrvnwz4kwasw6svnrhadx1avgzrf7a3ryf6ajpbwnapdbw")))

(define-public crate-stak-compile-0.1.14 (c (n "stak-compile") (v "0.1.14") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1jzvnwgcz2vsh1akd8ay2f28bbz6p71y99500yghlk77vdd27yg8")))

(define-public crate-stak-compile-0.1.15 (c (n "stak-compile") (v "0.1.15") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0mf5yr69d8g1ra5ql76ykdyicf65p3yxvwcna3iy0zz50hm3v9pk")))

(define-public crate-stak-compile-0.1.16 (c (n "stak-compile") (v "0.1.16") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1qyvmpx240xri0gpl67hb7a7ydxj77y5bqgh9712yyjiayn74r09")))

(define-public crate-stak-compile-0.1.17 (c (n "stak-compile") (v "0.1.17") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1s5mya6d8jv46nsncnlar3g4chpbyz498nvizl3s81k86nbr52h7")))

(define-public crate-stak-compile-0.1.18 (c (n "stak-compile") (v "0.1.18") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "01lpn3gif5jm0kl2wds56ai0cb8aycw6p1mfyipsaqkh36m3980a")))

(define-public crate-stak-compile-0.1.19 (c (n "stak-compile") (v "0.1.19") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0wvdfmv5z83zjyjjypfwmc9kf6afk1rkc2y2xzdidrzh7knnd28x")))

(define-public crate-stak-compile-0.1.20 (c (n "stak-compile") (v "0.1.20") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "13y4hzq3ng4d0jni7n7gpl4qhym8hnapsfx0vdl3ifb0flibdv2x")))

(define-public crate-stak-compile-0.1.21 (c (n "stak-compile") (v "0.1.21") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "03i9zx400s88mylk7q62y3jzxv1rqpxy9x9x8yp01g6diz3xa7lc")))

(define-public crate-stak-compile-0.1.22 (c (n "stak-compile") (v "0.1.22") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1arpi95i88jx0c26rzjxnmn8vb8fkckqqrk27bb8hmqqbzqnydld")))

(define-public crate-stak-compile-0.1.23 (c (n "stak-compile") (v "0.1.23") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0y35wh8bqcmlyngp9yjm5ywhzvcjc8mqd81dg7l9mwmarv2q83jc")))

(define-public crate-stak-compile-0.1.24 (c (n "stak-compile") (v "0.1.24") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "16kzmnrf6nxjb2971kfmpbf9w5cdsn6nwg93s81nl7fhs45v9ja5")))

(define-public crate-stak-compile-0.1.25 (c (n "stak-compile") (v "0.1.25") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0a8238s5ym532hc4hsv2mpzd310gpiyky5qhvwj0n9ffig1jgca8")))

(define-public crate-stak-compile-0.1.26 (c (n "stak-compile") (v "0.1.26") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0bkgn6pvf9wm9kv30va339dbdb6pbzikvyd4nsd2nafwm24p04pi")))

(define-public crate-stak-compile-0.1.27 (c (n "stak-compile") (v "0.1.27") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0z5khgvgp2dv5l51fkwkx85qvzrlzw7wlx7qcv750w7mmk6gz00p")))

(define-public crate-stak-compile-0.1.28 (c (n "stak-compile") (v "0.1.28") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "16c10nrivs0ccjgksw4kf1jngibbi5vgmpmlv9jg7qgg69fnr12f")))

(define-public crate-stak-compile-0.1.29 (c (n "stak-compile") (v "0.1.29") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1sx2l333x9r9xma3ajyd1v85ym7b0x57cgl5982b0mggllvzpm7f")))

(define-public crate-stak-compile-0.1.30 (c (n "stak-compile") (v "0.1.30") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "04c5zm8n1k70q9cggchqlvwblab65kkf9myqlxzkclfhk44vmbfy")))

(define-public crate-stak-compile-0.1.31 (c (n "stak-compile") (v "0.1.31") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "07sckhd2znlnics1v5hh8mlml994xsfp0csnjh0sy76v7mfzihda")))

(define-public crate-stak-compile-0.1.32 (c (n "stak-compile") (v "0.1.32") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "10n1bk0jnl144lmghianyahzxyfi2fx15p89dhvp3alx2xkdc172")))

(define-public crate-stak-compile-0.1.33 (c (n "stak-compile") (v "0.1.33") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0sas9zxbighw1wmi5qdc0f84j7f7kn2i56idgbvigrqazsvwd3wz")))

(define-public crate-stak-compile-0.1.34 (c (n "stak-compile") (v "0.1.34") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1072sj941mdwbxwvca9h2l6210z0pgq7d5xh0bb0q9jqypwd0k8f")))

(define-public crate-stak-compile-0.1.35 (c (n "stak-compile") (v "0.1.35") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "10l92fvxiscfv6g1cww703gxl2yk5dfaw708b0k77jknfd5j7jnw")))

(define-public crate-stak-compile-0.1.36 (c (n "stak-compile") (v "0.1.36") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "14d5ilwa006ybhp01x3n8xsdlddgj2mpb8ji1spbf5bkzk4nrhrw")))

(define-public crate-stak-compile-0.1.37 (c (n "stak-compile") (v "0.1.37") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0g0khg3c4pb9p7ib1mhssn8zvmz6zymzsky2gaqyqzrrs3bqli6q")))

(define-public crate-stak-compile-0.1.38 (c (n "stak-compile") (v "0.1.38") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1mzigs6b9wfl0yncrh4h2wpn0wk9r4s5vgs6bmahwv8vq2nwk6cp")))

(define-public crate-stak-compile-0.1.39 (c (n "stak-compile") (v "0.1.39") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1wgdfvppjcan5s4bvc6mybjsbighs3yppamb8k7k5mpkcjl91bby")))

(define-public crate-stak-compile-0.1.40 (c (n "stak-compile") (v "0.1.40") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "0ywla7njbj76iv9dhpr31hfv8ig4s1ykpgq9pxs5k0dnndryslsn")))

(define-public crate-stak-compile-0.1.41 (c (n "stak-compile") (v "0.1.41") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1kzx23w3l5nrvj1ja3igyclgw0anh6wi4r5c9gq0m7irrrcamw7p")))

(define-public crate-stak-compile-0.1.42 (c (n "stak-compile") (v "0.1.42") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "1gqkblzm0pyxs39wsjdyiyjncj2fcprc9gk8l3lnj8mgdd054sz3")))

(define-public crate-stak-compile-0.1.43 (c (n "stak-compile") (v "0.1.43") (d (list (d (n "stak-sac") (r "^0.1") (d #t) (k 0)))) (h "09ymxwz3chkv9mrwisc1nc219zl8qlcdgvz2jgslmlywqb8m09m5")))

