(define-module (crates-io st ak stak-macro) #:use-module (crates-io))

(define-public crate-stak-macro-0.1.0 (c (n "stak-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1lagflxnwx8910ak6z2zciyznhs19aj748dpghhi79zykza9pcqh")))

(define-public crate-stak-macro-0.1.1 (c (n "stak-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0l6md44fjcv80g3gqiijx2bvnj55bq12vx4wlbnb39zg7wnrn77l")))

(define-public crate-stak-macro-0.1.2 (c (n "stak-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0r5825czy2dnk3rpl6skkvg0gvv3vg1v050j6qpmm9r3d5fnfzn4")))

(define-public crate-stak-macro-0.1.3 (c (n "stak-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1qfzvqly2a6a5gklg2kcg7xfsirlj009dxvlrxml8wcv5xrwj4km")))

(define-public crate-stak-macro-0.1.4 (c (n "stak-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0j6yy08q4fbkkpq5fqax6yi5b5xp99nxarj45cr9jamiwc7x56r1")))

(define-public crate-stak-macro-0.1.5 (c (n "stak-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "04f8qkjjmsj2ic1882f1py29c6mqflgi253jmsx5w43qmrbf79ls")))

(define-public crate-stak-macro-0.1.6 (c (n "stak-macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "06k3g52zx5fkwfsywrhs9jm51fxxdh2s0y5805amqjgjhrphhbbm")))

(define-public crate-stak-macro-0.1.7 (c (n "stak-macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "16mrkarlpzzn6pgn95c8cnjz07x25rpnn47s35jr54baq7a9lrr8")))

(define-public crate-stak-macro-0.1.8 (c (n "stak-macro") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "12qqa0wmp4h4dzgy41h8z04baly18zsab2nnpkpn8j1h761gba89")))

(define-public crate-stak-macro-0.1.9 (c (n "stak-macro") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "136r5cl0yiizi1qnj9a88rv20h1lxzxmd73v2lhisr04hcdbiywi")))

(define-public crate-stak-macro-0.1.10 (c (n "stak-macro") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "0q2cl9zh3b1xylw2n434kbs1bj2mqz1bir4f6rwqh3q8nbldfjsd")))

(define-public crate-stak-macro-0.1.11 (c (n "stak-macro") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "0ga5w257i525y8pvlaa2a5lsj67w7spqsyfllriak8znzpvfal3l")))

(define-public crate-stak-macro-0.1.12 (c (n "stak-macro") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1qp5s72p37hram6ipzp3zsl34514nhpyi0rr9sbp3s8k6qy4lss1")))

(define-public crate-stak-macro-0.1.13 (c (n "stak-macro") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "16y6v36fkgc9wjasragqy55j52bbljkghnm35j65g8xx5k8s3b72")))

(define-public crate-stak-macro-0.1.14 (c (n "stak-macro") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1rxik79xs4k0g0yy89a2i3vvjl88lvpyz1kx4x7pilq1q3j1zn8i")))

(define-public crate-stak-macro-0.1.15 (c (n "stak-macro") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1w3knp8lcmvk0fj5fcsv9kckrgchjimggjrnabyqgpxg4prj9piz")))

(define-public crate-stak-macro-0.1.16 (c (n "stak-macro") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1w5kxxlvg5s4bab4kvg5d6fqm59g1njib565ybbz0g5cfmwz178p")))

(define-public crate-stak-macro-0.1.17 (c (n "stak-macro") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1z875v860nwbrnyjic6m1y6hq6gaf9hn4mqxy52kiy5sp4gwpl8m")))

(define-public crate-stak-macro-0.1.18 (c (n "stak-macro") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "1rqammd94d1si88z9jgjlcq43vqr1c1x4jwnmg4k2h6vi7r91vf5")))

(define-public crate-stak-macro-0.1.19 (c (n "stak-macro") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "07x1qd7xqi3x7xmk40fvca8r4m2j6yqjrpg137b243ysiyq8x5a0")))

(define-public crate-stak-macro-0.1.20 (c (n "stak-macro") (v "0.1.20") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "0xb8pjxcc0dxcv4qazlx562zwhaz0d33zl20jxa7f5ab9p6w1z5s")))

(define-public crate-stak-macro-0.1.21 (c (n "stak-macro") (v "0.1.21") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "1vv72afns978m6irk6ns6w11s86l371m4n400r7rbhzany2cx78l")))

(define-public crate-stak-macro-0.1.22 (c (n "stak-macro") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "176a7i1a43jh2pzqa5ml8f4j89w3halrsb1jbd8g4s3fl1m909bk")))

(define-public crate-stak-macro-0.1.23 (c (n "stak-macro") (v "0.1.23") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "08lhv281inas9ldczvqgv6hpb1jhi7md297n2s0x12y0bwblxqxm")))

(define-public crate-stak-macro-0.1.24 (c (n "stak-macro") (v "0.1.24") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "18bijwxrfnm4cqp8yc2a8kqfdfm1788csdwf2xnycik7pzyi2y2w")))

(define-public crate-stak-macro-0.1.25 (c (n "stak-macro") (v "0.1.25") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1m4lmqpncikx1lpsb80k37zklvy78mhbswirz66s8jwa0ww3vw8g")))

(define-public crate-stak-macro-0.1.26 (c (n "stak-macro") (v "0.1.26") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0ls5s9gwqv2yz7sx4xaz8nqrj46vghgnvhg0dn9b0dnqls1i2105")))

(define-public crate-stak-macro-0.1.27 (c (n "stak-macro") (v "0.1.27") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1y578x99yz33z5nqdc9yvg1mr7fj20k2yhixqixbq178nq9gvq99")))

(define-public crate-stak-macro-0.1.28 (c (n "stak-macro") (v "0.1.28") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1r06si6vlisrra5vgi37pywdbslb8lxh6d6pb297fy7kl9dk2b09")))

(define-public crate-stak-macro-0.1.29 (c (n "stak-macro") (v "0.1.29") (d (list (d (n "proc-macro2") (r "^1.0.80") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1xnyh76fh0frzw8xmzxx5pmgkvbywwg3lcirg7228c9cnklhvgnc")))

(define-public crate-stak-macro-0.1.30 (c (n "stak-macro") (v "0.1.30") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.6") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0wp17iraxmdhxi43q4g7b0wqzl7nap99avdmwfzxmkfpbvvxkg6y")))

(define-public crate-stak-macro-0.1.31 (c (n "stak-macro") (v "0.1.31") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0d4k9qj5krfnck0rl8qyy4mhyb9iqsqvg2xfpmbyh2r7cpjibscq")))

(define-public crate-stak-macro-0.1.32 (c (n "stak-macro") (v "0.1.32") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "09z3bzix47r6wbad50gazgjh062hr06pxam8j7j1wjynrlzl0wg8")))

(define-public crate-stak-macro-0.1.33 (c (n "stak-macro") (v "0.1.33") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1pbrz2xqq9ng80vkzr0x8d2fa0a7af31ga6xgb6g570ahvg2pdgl")))

(define-public crate-stak-macro-0.1.34 (c (n "stak-macro") (v "0.1.34") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1k2myfn5qba5adk2qqxpc1a1kxfsibmay7sqwqgwc8x8rim5wsyn")))

(define-public crate-stak-macro-0.1.35 (c (n "stak-macro") (v "0.1.35") (d (list (d (n "proc-macro2") (r "^1.0.82") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.11") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "17jfmfkvv26szj72nqafib52rqd9h4sg231pm1drx26rz7hfa0nq")))

(define-public crate-stak-macro-0.1.36 (c (n "stak-macro") (v "0.1.36") (d (list (d (n "proc-macro2") (r "^1.0.82") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.12") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "0hnkzdks2xbw2zg9lgjm8agrr1gnl9maqwc5ppmvqrm5whgkln8k")))

(define-public crate-stak-macro-0.1.37 (c (n "stak-macro") (v "0.1.37") (d (list (d (n "proc-macro2") (r "^1.0.82") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.13") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (d #t) (k 0)))) (h "0rp98kmmcaxjcbvc04dbzdimx7swz8sxjfdxvkm0gmbaz0p2ymxx")))

(define-public crate-stak-macro-0.1.38 (c (n "stak-macro") (v "0.1.38") (d (list (d (n "proc-macro2") (r "^1.0.82") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.14") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (d #t) (k 0)))) (h "1xlb6bm1123d8czr2a7scrng0z5fii3mnyly2wn8f3q1hcighzbb")))

(define-public crate-stak-macro-0.1.39 (c (n "stak-macro") (v "0.1.39") (d (list (d (n "proc-macro2") (r "^1.0.83") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.15") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (d #t) (k 0)))) (h "1jqdm727biddnzpmn8v17kmdlgjz6rknqn563vdqi5jkvkgn7m7g")))

(define-public crate-stak-macro-0.1.40 (c (n "stak-macro") (v "0.1.40") (d (list (d (n "proc-macro2") (r "^1.0.84") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-compiler") (r "^0.1") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.16") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)))) (h "1r6lxrhmrnaxhps3dzmpz0r8vdf32hvgka7j7np9nl4jm0viin20")))

