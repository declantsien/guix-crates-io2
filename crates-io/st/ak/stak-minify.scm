(define-module (crates-io st ak stak-minify) #:use-module (crates-io))

(define-public crate-stak-minify-0.1.0 (c (n "stak-minify") (v "0.1.0") (d (list (d (n "stak-sac") (r "^0.1.24") (d #t) (k 0)))) (h "1fjsf8by0x4z4csdi66kf7jqa9gckaizvd3y8kbicbzr0aw5h6kh")))

(define-public crate-stak-minify-0.1.1 (c (n "stak-minify") (v "0.1.1") (d (list (d (n "stak-sac") (r "^0.1.25") (d #t) (k 0)))) (h "1ns1cmndwxpwxinxynjdllqr1nslwmfyjxxxzbr88lb3vh7ql8lg")))

(define-public crate-stak-minify-0.1.2 (c (n "stak-minify") (v "0.1.2") (d (list (d (n "stak-sac") (r "^0.1.26") (d #t) (k 0)))) (h "13jk7bh363ps45jy9w8rff8zajz8dgsmrj5gr9ikg2rx4pc2q5wa")))

(define-public crate-stak-minify-0.1.3 (c (n "stak-minify") (v "0.1.3") (d (list (d (n "stak-sac") (r "^0.1.27") (d #t) (k 0)))) (h "1ljfyfh5b1h60z4sx2zjh1d2z2xad2mk8f4g06akaldwlnr1ydzi")))

(define-public crate-stak-minify-0.1.4 (c (n "stak-minify") (v "0.1.4") (d (list (d (n "stak-sac") (r "^0.1.28") (d #t) (k 0)))) (h "1zb2v5hasnq7n8rxyrmkzjgq7n4r57z6kvg6868hkk8psvzra0yf")))

(define-public crate-stak-minify-0.1.5 (c (n "stak-minify") (v "0.1.5") (d (list (d (n "stak-sac") (r "^0.1.29") (d #t) (k 0)))) (h "19h600aypb5q96pxs0xs99krbf14mr0vsr2lrr4s559fvnkcyqxh")))

(define-public crate-stak-minify-0.1.6 (c (n "stak-minify") (v "0.1.6") (d (list (d (n "stak-sac") (r "^0.1.30") (d #t) (k 0)))) (h "1fdl07ik8pvccmd7ryzzc47q0qkfyiykvrqwglwrqg9z0wngd985")))

(define-public crate-stak-minify-0.1.7 (c (n "stak-minify") (v "0.1.7") (d (list (d (n "stak-sac") (r "^0.1.31") (d #t) (k 0)))) (h "1fsghjmkw1yxw064kr5p8fpifpd6yvfdsysp9afk2dmmw56620dj")))

(define-public crate-stak-minify-0.1.8 (c (n "stak-minify") (v "0.1.8") (d (list (d (n "stak-sac") (r "^0.1.32") (d #t) (k 0)))) (h "0lknlw16sr2c3dq4dpg27rsnnlchbvj51vmknka09rwmrxhcca8q")))

(define-public crate-stak-minify-0.1.9 (c (n "stak-minify") (v "0.1.9") (d (list (d (n "stak-sac") (r "^0.1.33") (d #t) (k 0)))) (h "14i7523lj7r59flmv71ky09w7na861q566b6cavnc1j43zwwfhkw")))

(define-public crate-stak-minify-0.1.10 (c (n "stak-minify") (v "0.1.10") (d (list (d (n "stak-sac") (r "^0.1.34") (d #t) (k 0)))) (h "1qprw5z0mds90vakkaicc5n4f0mzljxahxl3m8jlr0j2dvk6m5jg")))

(define-public crate-stak-minify-0.1.11 (c (n "stak-minify") (v "0.1.11") (d (list (d (n "stak-sac") (r "^0.1.35") (d #t) (k 0)))) (h "0p8f4av4msa4xrrn1vxj1xnxpq3by6rvi1qvk3vgfcgpn9mgxkcw")))

(define-public crate-stak-minify-0.1.12 (c (n "stak-minify") (v "0.1.12") (d (list (d (n "stak-sac") (r "^0.1.36") (d #t) (k 0)))) (h "141i49zqprvw7bsb2581z618zz465yrhycl7slv7wvdm2ahf2mf1")))

(define-public crate-stak-minify-0.1.13 (c (n "stak-minify") (v "0.1.13") (d (list (d (n "stak-sac") (r "^0.1.37") (d #t) (k 0)))) (h "1b7sgaxhk4jalbwxkvym1c00kvi2g08cmlb9gxrdr497r9z20lzw")))

(define-public crate-stak-minify-0.1.14 (c (n "stak-minify") (v "0.1.14") (d (list (d (n "stak-sac") (r "^0.1.38") (d #t) (k 0)))) (h "0dnm5dnarbvsf3ki6zrdcyf3857aaryzg6mqb941iqkcw27immm3")))

(define-public crate-stak-minify-0.1.15 (c (n "stak-minify") (v "0.1.15") (d (list (d (n "stak-sac") (r "^0.1.39") (d #t) (k 0)))) (h "0wb3bsssa4z2hfhdjxlxkd916745yzc75g99345aq1mvzpqv4x1d")))

(define-public crate-stak-minify-0.1.16 (c (n "stak-minify") (v "0.1.16") (d (list (d (n "stak-sac") (r "^0.1.40") (d #t) (k 0)))) (h "0fwhd4m9qgb6phn1fg1vjcq7bkbj8j8cc2aip3ph2fzsx0k3k089")))

(define-public crate-stak-minify-0.1.17 (c (n "stak-minify") (v "0.1.17") (d (list (d (n "stak-sac") (r "^0.1.41") (d #t) (k 0)))) (h "1698iwdcckcsi4zi7glsndf77yh26baa05pbwp7py0jzi7r36lw8")))

