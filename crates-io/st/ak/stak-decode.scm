(define-module (crates-io st ak stak-decode) #:use-module (crates-io))

(define-public crate-stak-decode-0.1.0 (c (n "stak-decode") (v "0.1.0") (d (list (d (n "code") (r "^0.1") (f (quote ("alloc" "std"))) (d #t) (k 0) (p "stak-code")))) (h "1djr30d2r7xy4hfbqqh3bry0ncc05lwxi4661zi16zx7m6izfgm5")))

(define-public crate-stak-decode-0.1.1 (c (n "stak-decode") (v "0.1.1") (d (list (d (n "code") (r "^0.1") (f (quote ("alloc" "std"))) (d #t) (k 0) (p "stak-code")))) (h "07411j1r85ml2bkynl1lk2ys4gfkrn7ccbqw07g4yz0f4ah6yvk8")))

(define-public crate-stak-decode-0.1.2 (c (n "stak-decode") (v "0.1.2") (d (list (d (n "code") (r "^0.1") (f (quote ("alloc" "std"))) (d #t) (k 0) (p "stak-code")))) (h "0d60xx6c3dnyza41f5g4zfb00cb5m1ipd27m3l8kv3a69093m4s9")))

(define-public crate-stak-decode-0.1.3 (c (n "stak-decode") (v "0.1.3") (d (list (d (n "code") (r "^0.1") (f (quote ("alloc" "std"))) (d #t) (k 0) (p "stak-code")))) (h "1h0syvdkm14sandmdgainx0vgx8w9zivh2nr1mc6shrihzq68r10")))

(define-public crate-stak-decode-0.1.4 (c (n "stak-decode") (v "0.1.4") (d (list (d (n "code") (r "^0.1") (f (quote ("alloc" "std"))) (d #t) (k 0) (p "stak-code")))) (h "1302a5rgbf9lf08999xvjkxcbrfvkzp5f5q3216kbk76ya8j4cl2")))

(define-public crate-stak-decode-0.1.5 (c (n "stak-decode") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0aygal552gk2ack7227km78yd6ny3zgyabx2zjlk21h4wvi0bf4z")))

(define-public crate-stak-decode-0.1.6 (c (n "stak-decode") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1jw704qnh516szq02qnks42c7s0c662c3vyfgzay3ip9wmgab7y5")))

(define-public crate-stak-decode-0.1.7 (c (n "stak-decode") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1xzps9g0f4dc6js7pjb7p40q1p143jjz15ngl9733prgnvbb2s7b")))

(define-public crate-stak-decode-0.1.8 (c (n "stak-decode") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0kxni8dqwy1sd1ykwh9gidim4q29px8qbgd2ddxr3qqsk6fg0438")))

(define-public crate-stak-decode-0.1.9 (c (n "stak-decode") (v "0.1.9") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1qq1xmgbpc2snd52i9vrl82jk1vvpadjrcwqvn4m1i7fvcgy8ag6")))

(define-public crate-stak-decode-0.1.10 (c (n "stak-decode") (v "0.1.10") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "18wcwpcd6vbj3s0pc85g7c45zwmri332qza0cx1hjc4zzrf94npp")))

(define-public crate-stak-decode-0.1.11 (c (n "stak-decode") (v "0.1.11") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0zrxqa7b6bq89xm99yjik6j1a2gs87675f4qh9096xdyxvyd4azx")))

(define-public crate-stak-decode-0.1.12 (c (n "stak-decode") (v "0.1.12") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1qwwhlhdi9ck4m6mvg3p47s07dyh6k8lbn11gx5kdigljysdam08")))

(define-public crate-stak-decode-0.1.13 (c (n "stak-decode") (v "0.1.13") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1mvz30xx94lb6vps35bm7dqqyfq8sa8xm6l2brqxxcwiwc6gf9jq")))

(define-public crate-stak-decode-0.1.14 (c (n "stak-decode") (v "0.1.14") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1b412cx2wplibiypf74dxv1m78i8r5d31zd858ia7aj4qy0pqypx")))

(define-public crate-stak-decode-0.1.15 (c (n "stak-decode") (v "0.1.15") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1rii0sx4vk25q7b93c5rdppdx1hbcyzqnx8wrcm317jbc59mw9x8")))

(define-public crate-stak-decode-0.1.16 (c (n "stak-decode") (v "0.1.16") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0g6d1m5df1nnmk4w3541nkdqaap8k2zpv2gsc82ss8g1q3v5z617")))

(define-public crate-stak-decode-0.1.17 (c (n "stak-decode") (v "0.1.17") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1nsw89b2kn426gj1bb3plxiw2qsnpvx3a07g1vpfwkc5yj6v5qh2")))

(define-public crate-stak-decode-0.1.18 (c (n "stak-decode") (v "0.1.18") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "06fxjai66xhpixkx2ijdv03xxh1zxs2684fs972ihlc6k2m73n8l")))

(define-public crate-stak-decode-0.1.19 (c (n "stak-decode") (v "0.1.19") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0kyxvnmhfbf0g861j7s33lnp36s5mzbf0v1ns05yskz4615ryi8d")))

(define-public crate-stak-decode-0.1.20 (c (n "stak-decode") (v "0.1.20") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0wfr6dvi72x9gksikbm7s94gxc9dr2aap2935b07lczn4n65fnmq")))

(define-public crate-stak-decode-0.1.21 (c (n "stak-decode") (v "0.1.21") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "077vlzyy41dv76nmayqmgaid5rb64xpjvsdaklrd08526hgijg6s")))

(define-public crate-stak-decode-0.1.22 (c (n "stak-decode") (v "0.1.22") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "06dy2jzwz6rg5ygqvf4h85ib30ifvhscycx3vq6s4h6s60bly62l")))

(define-public crate-stak-decode-0.1.23 (c (n "stak-decode") (v "0.1.23") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "18m8kkds2jxns8npr6ymws8ac5xm236v92p8hnhrrn88b9a9hnbr")))

(define-public crate-stak-decode-0.1.24 (c (n "stak-decode") (v "0.1.24") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0fmhnsydn58s9wlpxky8x48zbqn9v75mk6s9mqdyv5mb9pkbimkr")))

(define-public crate-stak-decode-0.1.25 (c (n "stak-decode") (v "0.1.25") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "09iimnc7cm0brdipamj34y1ff6lm3a04why7mljfrba82qjj1y9n")))

(define-public crate-stak-decode-0.1.26 (c (n "stak-decode") (v "0.1.26") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1bkh6p15xbd4bf2yzmrp1ha1bq03cinvpkj8z9gmz1aqfd7876h9")))

(define-public crate-stak-decode-0.1.27 (c (n "stak-decode") (v "0.1.27") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1c4ckjm7r7izbvf3mk8vzs7wy8mnyic858dp1iysrq96l9dh03b3")))

(define-public crate-stak-decode-0.1.28 (c (n "stak-decode") (v "0.1.28") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0900am2yf74dykb234gsdii92aifjgqqyky39y3l2s3y9a1di28k")))

(define-public crate-stak-decode-0.1.29 (c (n "stak-decode") (v "0.1.29") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1bxybp9nc74c8lxlwgzmpl5zxy300bdkjyrbnscic5w567222fzk")))

(define-public crate-stak-decode-0.1.30 (c (n "stak-decode") (v "0.1.30") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0skqinx80agrj9kp0p4g2gli996l9z0v6xz8ig5akjb2cjd0jhir")))

(define-public crate-stak-decode-0.1.31 (c (n "stak-decode") (v "0.1.31") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1zka40aag7256bk89n9k17mzai2anvamgximh74mjpmbdmypw60f")))

(define-public crate-stak-decode-0.1.32 (c (n "stak-decode") (v "0.1.32") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0ymdykcd5gmzqpdhffsds5qmzhg8vqgxw1fb0352s0b8cakbcdz1")))

(define-public crate-stak-decode-0.1.33 (c (n "stak-decode") (v "0.1.33") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "02kq9lyr5d62mrhlpm14g9gq14kfq583w2nhv7hix1nxslfl7afh")))

(define-public crate-stak-decode-0.1.34 (c (n "stak-decode") (v "0.1.34") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0jmrzdznrcdxf3ni4v4fckddhmhykx48r0zvwr4vcli1ns96jgn5")))

(define-public crate-stak-decode-0.1.35 (c (n "stak-decode") (v "0.1.35") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1szklh67wmwh57v7ymj1k6pyi931d6nrjnrp12k3am9513g2gwvf")))

(define-public crate-stak-decode-0.1.36 (c (n "stak-decode") (v "0.1.36") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "13dsx1hw460kc4w3vizmvqak7pnlsvslr9k8222awvqc1z0992la")))

(define-public crate-stak-decode-0.1.37 (c (n "stak-decode") (v "0.1.37") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1jxfgarhc6im9bn6hrkmj9q1gszkr19g3x6dyk045mhf3g2ghz2b")))

(define-public crate-stak-decode-0.1.38 (c (n "stak-decode") (v "0.1.38") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0y85yg4kv77fiy5sxjbdg1c8an7b3jxgqc7bzd3bhf1vqhs6gvz3")))

(define-public crate-stak-decode-0.1.39 (c (n "stak-decode") (v "0.1.39") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1dj117qbqbf4iw4fg1r8m94dspjrsislrmqc2yg3kjv7fkxx0m6m")))

(define-public crate-stak-decode-0.1.40 (c (n "stak-decode") (v "0.1.40") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0s21jzd74aqs2f95z395rzrd53kfbmijiwyzp0hkk310sh0fnql9")))

(define-public crate-stak-decode-0.1.41 (c (n "stak-decode") (v "0.1.41") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0cd3zgfmyyaw4xj5i6mnp6q766i5y9sfcpyf84g33m42s7zx59b4")))

(define-public crate-stak-decode-0.1.42 (c (n "stak-decode") (v "0.1.42") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "07f2xl6jk06ynr5a9n596kkk54d35df9giz2969cldb5mkbvdiq8")))

(define-public crate-stak-decode-0.1.43 (c (n "stak-decode") (v "0.1.43") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "1d592j79apak3ylg914anv9irvymkjqs5hvqknl6r1awfn0y6gql")))

(define-public crate-stak-decode-0.1.44 (c (n "stak-decode") (v "0.1.44") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-code") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)))) (h "0mcwvk7kg0d1yg7ls6bl6w59wkb11d3axqkdpaijr0gj491fiynd")))

