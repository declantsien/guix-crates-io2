(define-module (crates-io st ak stak-macro-util) #:use-module (crates-io))

(define-public crate-stak-macro-util-0.1.0 (c (n "stak-macro-util") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1sa6y83ply0fb37vsrmdhmbbq2gy3qa24b6mwrh15xfqylirmjg5")))

(define-public crate-stak-macro-util-0.1.1 (c (n "stak-macro-util") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "17513f4yhrmjni1wmfrzxfhixzrzs6jg8a7l8qaqqcqmnnwk6v99")))

(define-public crate-stak-macro-util-0.1.2 (c (n "stak-macro-util") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1p2v68sq42zwlk674mfkfqzghc71gj2waawqk7wlp0ryqf2d1bvv")))

(define-public crate-stak-macro-util-0.1.3 (c (n "stak-macro-util") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0rh1giy1bnm9rd4fbcsr4nbc41c20zz5akxmx201aa17wcfx6i8k")))

(define-public crate-stak-macro-util-0.1.4 (c (n "stak-macro-util") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1h4k7hxflv8xsn2gbx4a054zyar5p4274iq26qc1cvk34rvg8xb1")))

(define-public crate-stak-macro-util-0.1.5 (c (n "stak-macro-util") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.80") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1m11vpkf749flxbsfl36fwlp5fizl2vb6g1grlqkk0znxxra2bm1")))

(define-public crate-stak-macro-util-0.1.6 (c (n "stak-macro-util") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0qz5pfx3fqzf7yl9xzx74n2cr6d9yj6sqn577cdahqj8ykbx8rp3")))

(define-public crate-stak-macro-util-0.1.7 (c (n "stak-macro-util") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1jszkfbwk6jw065f5p5nc68kpy541p0ps58q9z57g953acfhq954")))

(define-public crate-stak-macro-util-0.1.8 (c (n "stak-macro-util") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1ds2sczm7a2fr27kz812hlsw9kgj7gip150mmms47k770bzxk9z8")))

(define-public crate-stak-macro-util-0.1.9 (c (n "stak-macro-util") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "15ij0z4ykbw17hn9930513y7c7fmj6shfagg3vj0rqqq6326n44z")))

(define-public crate-stak-macro-util-0.1.10 (c (n "stak-macro-util") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "07mypwnlyyyhkrhy6sfgw37jkaqaz6phdspqlgiqkv0nhiw7c9ch")))

(define-public crate-stak-macro-util-0.1.11 (c (n "stak-macro-util") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "07vybsp30g96877d7rwq7kf760qssn0cwqa2s5sl04ra5w58s9ff")))

(define-public crate-stak-macro-util-0.1.12 (c (n "stak-macro-util") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "1wr6117604rc011qkcl2vdpx8h5svby1g55nrxqmmjcc85xw5xfd")))

(define-public crate-stak-macro-util-0.1.13 (c (n "stak-macro-util") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (d #t) (k 0)))) (h "186fpkjncgqmi87w18bq6ab83f1a4f9rp2824jwkjjr3z1gh26ad")))

(define-public crate-stak-macro-util-0.1.14 (c (n "stak-macro-util") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (d #t) (k 0)))) (h "1f1bbfp5hvrv92vik4wxv04yxxghr2m91ic2n1n7w66y534jl1ph")))

(define-public crate-stak-macro-util-0.1.15 (c (n "stak-macro-util") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (d #t) (k 0)))) (h "03kjy13rc0ikw3f8krkyiwr45kbrg2h1m2p2l7wzirhpn6hs18z7")))

(define-public crate-stak-macro-util-0.1.16 (c (n "stak-macro-util") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)))) (h "0cxjzc4rk2bvaqkk8fgjxf0fivk8zlpagxhxab9hadn0481x8xca")))

