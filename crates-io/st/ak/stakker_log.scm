(define-module (crates-io st ak stakker_log) #:use-module (crates-io))

(define-public crate-stakker_log-0.0.1 (c (n "stakker_log") (v "0.0.1") (d (list (d (n "stakker") (r "^0.2.0-beta.1") (f (quote ("logger"))) (d #t) (k 0)))) (h "1s9qrvvdx3sidrkm2bzi5c01zf7v75jp7yh5f65hsl1ixzqjp853")))

(define-public crate-stakker_log-0.0.2 (c (n "stakker_log") (v "0.0.2") (d (list (d (n "stakker") (r "^0.2") (f (quote ("logger"))) (d #t) (k 0)))) (h "1mj82p4yxvmcfqrlwkpd5qfmghhpjm7siypaxr0j0nfang7nmgd1")))

(define-public crate-stakker_log-0.0.3 (c (n "stakker_log") (v "0.0.3") (d (list (d (n "stakker") (r "^0.2") (f (quote ("logger"))) (d #t) (k 0)))) (h "1ra86mgjdphyy0m4wriylx7r99iinm29dbb90zxsn6rf3la5scrj")))

(define-public crate-stakker_log-0.1.0 (c (n "stakker_log") (v "0.1.0") (d (list (d (n "stakker") (r "^0.2") (f (quote ("logger"))) (d #t) (k 0)))) (h "1m7nw5zwjw14w1f549x4b18rfmhdhx2dx9wv3dzy19j0all8yn89")))

