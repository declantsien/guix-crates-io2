(define-module (crates-io st ak stak-minifier-macro) #:use-module (crates-io))

(define-public crate-stak-minifier-macro-0.1.0 (c (n "stak-minifier-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (d #t) (k 0)))) (h "0f2q3r9x78whgb25yvrgv46imcd2cvvdn8wnr3n5q727dbkfrlgz")))

(define-public crate-stak-minifier-macro-0.1.1 (c (n "stak-minifier-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1rp4mibb93lg1fvq18q23acwi85rz2awc8widw200hc9ydxgj8m4")))

(define-public crate-stak-minifier-macro-0.1.2 (c (n "stak-minifier-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.1") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "09xkn93w2iq711h1q3qpyjpf1ppyc7vcpmdc5s5rvxcj58q680jq")))

(define-public crate-stak-minifier-macro-0.1.3 (c (n "stak-minifier-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0am1bb6l8fvn96xqnjf18147rcxm2wn5v21c7sfd6drf8vdvmblv")))

(define-public crate-stak-minifier-macro-0.1.4 (c (n "stak-minifier-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.3") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "018d4lnnycws4ng1127y3nk7h8a3zs3v827a10zyzgmx3g6bdqlq")))

(define-public crate-stak-minifier-macro-0.1.5 (c (n "stak-minifier-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.4") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "14h28sbqwqada6isi14bn36s954036jg0a23hn1fk5kjb9jk91am")))

(define-public crate-stak-minifier-macro-0.1.6 (c (n "stak-minifier-macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.80") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.5") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.6") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1wp5yh18snliyvz4f33d8g7i0lc26qk2i9m8nindr83c839kc629")))

(define-public crate-stak-minifier-macro-0.1.7 (c (n "stak-minifier-macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.6") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1n63k7m4fxvnxryap4krz10wr5xzrky0cxyzi9x2lhcfky2klrp1")))

(define-public crate-stak-minifier-macro-0.1.8 (c (n "stak-minifier-macro") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.7") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1bwzxmj9ng6cagxpmziqwibhazcm8vn5rjajhl8qvrsfawbq4i4d")))

(define-public crate-stak-minifier-macro-0.1.9 (c (n "stak-minifier-macro") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.8") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "19qkbwwrsxgvdp76d304idbx4kfz3b0lg8fgh9swqgqki8karrf1")))

(define-public crate-stak-minifier-macro-0.1.10 (c (n "stak-minifier-macro") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.9") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "15g4717x6x9az70plbz8rcp3qcdiswxcnvpmvylri3vmg40l9a9q")))

(define-public crate-stak-minifier-macro-0.1.11 (c (n "stak-minifier-macro") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.10") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.11") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1c6c9zif7gpm63phf1b65wzljzj2zhn9a2z6hnyn541idy16dj3s")))

(define-public crate-stak-minifier-macro-0.1.12 (c (n "stak-minifier-macro") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.82") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.11") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.12") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "0bgalvpvqf2jaxk5x0la2pnxplgi0s8p95q98c7bhm42n2cjw6wa")))

(define-public crate-stak-minifier-macro-0.1.13 (c (n "stak-minifier-macro") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.82") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.12") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.13") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "11lvnp0j0q34a9kjqw6sbdgrzrb005113ci07cx1iwiavkvv6axh")))

(define-public crate-stak-minifier-macro-0.1.14 (c (n "stak-minifier-macro") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0.82") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.13") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.14") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (d #t) (k 0)))) (h "0vsrays2flrhmf9rd96bqrdsmw2in1c9jbx9spdrwn9zn7g2za24")))

(define-public crate-stak-minifier-macro-0.1.15 (c (n "stak-minifier-macro") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0.82") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.14") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.15") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (d #t) (k 0)))) (h "088nhl2pfrvzllzxanvqbryb5p4c7lhkb8jbxrcrmd8g0ll0m33v")))

(define-public crate-stak-minifier-macro-0.1.16 (c (n "stak-minifier-macro") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0.83") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.15") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.16") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (d #t) (k 0)))) (h "1cl1w3pbqqfgb40x8yl2y70xdb2x1vg9yfa47ac2gqay65llwy2r")))

(define-public crate-stak-minifier-macro-0.1.17 (c (n "stak-minifier-macro") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0.84") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "stak-macro-util") (r "^0.1.16") (d #t) (k 0)) (d (n "stak-minifier") (r "^0.1.17") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)))) (h "0jrbxl241abrxi29mk084vhw74xk08al203wjd946d8l8sv19q5v")))

