(define-module (crates-io st ak stak-device) #:use-module (crates-io))

(define-public crate-stak-device-0.1.0 (c (n "stak-device") (v "0.1.0") (h "0jya0zp8zr60anncjvgdb40vidzcz0jdh4yhv26z0h89nb0kjnbd") (f (quote (("std"))))))

(define-public crate-stak-device-0.1.2 (c (n "stak-device") (v "0.1.2") (h "07kzkmqdr571k96zn4dcqka6x4wpyylx5y5ya5xbl4mybnz64ibg") (f (quote (("std"))))))

(define-public crate-stak-device-0.1.3 (c (n "stak-device") (v "0.1.3") (h "0bc8vxmcgakiiv8i4g6jdinfg5rcics2y846dx370qrsxaaqc59m") (f (quote (("std"))))))

(define-public crate-stak-device-0.1.4 (c (n "stak-device") (v "0.1.4") (h "07gs7snzfvwfgblicsn57380v9kaya6xrhjkspizjsd9xq26viwc") (f (quote (("std"))))))

(define-public crate-stak-device-0.1.5 (c (n "stak-device") (v "0.1.5") (h "0mvi3irgsc3qydqq0pidbavfbzmj2iw69038yxz3vswq4rj130bf") (f (quote (("std"))))))

(define-public crate-stak-device-0.1.6 (c (n "stak-device") (v "0.1.6") (h "1rhms60xjb93ag7h09chi3yldwxnaz1v618l4alawx8lfv2vdapb") (f (quote (("std"))))))

(define-public crate-stak-device-0.1.7 (c (n "stak-device") (v "0.1.7") (h "18y43r67ycv65zhrpwad1rrza4w3dx8dp2pyi5jwq129x30yv8d2") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.0 (c (n "stak-device") (v "0.2.0") (h "1yna1w56z3xziig0hfk7m3x9wrbbmh8s61i04acj86hlxmmpms7m") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.1 (c (n "stak-device") (v "0.2.1") (h "12vv0kjshyg037cbw1q7h4a0av20kh2g7crvgal25ar5csy9amqq") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.2 (c (n "stak-device") (v "0.2.2") (h "0jmvs5sx6gwldffs803s53xfhgynhdwadkg8dvyhzkl7as5hlr09") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.3 (c (n "stak-device") (v "0.2.3") (h "03qwf3122r50v6m73h1rwg422cx2448halnmsa6zhd24amwd4d19") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.4 (c (n "stak-device") (v "0.2.4") (h "04njpkkc04c2lq62fz46xfhxmfkcl5wpg0rwirlnbsqrw56sg6qi") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.5 (c (n "stak-device") (v "0.2.5") (h "1l98rmvk9qpz7inn57z6qv2vrn7xx98g2x9fk9rm008bnnxf6b1r") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.6 (c (n "stak-device") (v "0.2.6") (h "0vly6ilgsd4800cnr5v253vkzl5g49mzbl1n81phfz3c6n1h7g3z") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.7 (c (n "stak-device") (v "0.2.7") (h "0k7ias6v3jpcmcylxnywmkhq8l3pdldmv7m6qrijm5nsbh99r6nh") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.8 (c (n "stak-device") (v "0.2.8") (h "11nmqk4b06j2immqn65lv415r49jb7i7s68ffwa4a3kxbj0k8a4q") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.9 (c (n "stak-device") (v "0.2.9") (h "143ihyprl9ihlnh0h5k899y5pgikvm9qiq6ljnzigx52kxy3fjji") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.10 (c (n "stak-device") (v "0.2.10") (h "0nn9vcg9izphbz9lbn0pb3q6qlzvh0wx7kvb3anid128ahsbvnmn") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.11 (c (n "stak-device") (v "0.2.11") (h "0hv2vjpmlkvbcq6q0cixfqz6jwslcf95vvw4l01yq0ypd67mc0g9") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.12 (c (n "stak-device") (v "0.2.12") (h "1avaxvrbgxl5flh4g6vwnprjypsbs23gx1cjdpvhpinzinzpg3ki") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.13 (c (n "stak-device") (v "0.2.13") (h "0ggx9a5br6pwn95h98lq0fq6dxdxr5aliilr5bgwijahf3sv3xvq") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.14 (c (n "stak-device") (v "0.2.14") (h "0y4c97jn0z6qh997s7b89pw7ca820in5jb5ywywh7lgahjlmdin1") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.15 (c (n "stak-device") (v "0.2.15") (h "1acx2p4axzid36c6f8cbqkh0y49vijssbs6bskkflvi830f9q9vn") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.16 (c (n "stak-device") (v "0.2.16") (h "0jsk9hd3cq58j2pn8s4sgnp0vvr1jrzr9bz18f6g02fpjva8gjjr") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.17 (c (n "stak-device") (v "0.2.17") (h "1vanr73j1z2j7hyjh2fv0gdz9i73vgar2z7ljazd9964bd4a2zqw") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.18 (c (n "stak-device") (v "0.2.18") (h "0sljimzr3pqcnj68bcphixy2qgi7dmw5g014gjl620lfnnddv2dr") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.19 (c (n "stak-device") (v "0.2.19") (h "0j5czrzrid9gspkcsh355bgkfs8n7hijgcdz40wjm8ixg19s79l6") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.20 (c (n "stak-device") (v "0.2.20") (h "1z27yr0pgdy06zkf686bihxsbx6rvrmzrnc84y6vl9s2p96llhnv") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.21 (c (n "stak-device") (v "0.2.21") (h "0f7znz8vgsv7vqrysrp6yz5mqmqxycviww3kvwr4d0hwf2fh752q") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.22 (c (n "stak-device") (v "0.2.22") (h "1g0d8j7yysl0spc0j5hpvjzxzjgr5db6l9wa6psy04rcqawgkrb8") (f (quote (("std"))))))

(define-public crate-stak-device-0.2.23 (c (n "stak-device") (v "0.2.23") (d (list (d (n "libc") (r "^0.2.153") (o #t) (k 0)))) (h "025ikrgpwa38jbzcwprjc9iahrpq9zapicx9gg34mmv1pfd2fdf3") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.24 (c (n "stak-device") (v "0.2.24") (d (list (d (n "libc") (r "^0.2.153") (o #t) (k 0)))) (h "03y8ly373p8w6asb0hs5wjzgiwsk243qr8am1jzb0n2j2wg5nyrk") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.25 (c (n "stak-device") (v "0.2.25") (d (list (d (n "libc") (r "^0.2.153") (o #t) (k 0)))) (h "1hzysb5xdfx8px0s8bqv542fcrb1kigph380qq49n5nl9ysxl651") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.26 (c (n "stak-device") (v "0.2.26") (d (list (d (n "libc") (r "^0.2.153") (o #t) (k 0)))) (h "1gpnb4yx7ylvrnn71ak4vrq4n38vj4ixk0lfrcps7js2l6v3mjhx") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.27 (c (n "stak-device") (v "0.2.27") (d (list (d (n "libc") (r "^0.2.153") (o #t) (k 0)))) (h "1rgqw2m6g4pqg0xmr05i72ccy67b5xlq3giy38r0b4mycpx6k945") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.28 (c (n "stak-device") (v "0.2.28") (d (list (d (n "libc") (r "^0.2.153") (o #t) (k 0)))) (h "120yfbssdgblrlq6vkwmjbn40vs8ffyrgib63ng3nc2pq4jcrmnk") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.29 (c (n "stak-device") (v "0.2.29") (d (list (d (n "libc") (r "^0.2.153") (o #t) (k 0)))) (h "0hdd6518iv2gz9qyz5nlpm0zx0fdn9mnr1r8kpqmxh9yb58fzr5f") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.30 (c (n "stak-device") (v "0.2.30") (d (list (d (n "libc") (r "^0.2.153") (o #t) (k 0)))) (h "0aaj2wgcy28z31zn8ilkqgivj4z9av0x55h4fw7d8l092qs5k6mc") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.31 (c (n "stak-device") (v "0.2.31") (d (list (d (n "libc") (r "^0.2.153") (o #t) (k 0)))) (h "18x19qn53lxsqsf0cbis3k9hi5asyxw0kkyxs3n556zzghkd6h0z") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.32 (c (n "stak-device") (v "0.2.32") (d (list (d (n "libc") (r "^0.2.153") (o #t) (k 0)))) (h "13c0h68dmac3kb4jcdbz4j0y0qa518dv66arfwlc4grc4s0434m6") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.33 (c (n "stak-device") (v "0.2.33") (d (list (d (n "libc") (r "^0.2.154") (o #t) (k 0)))) (h "1x6q70r2fw6cnmbm039bi6hdn602xxb7805ccxh9zmws3yllj0x6") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.34 (c (n "stak-device") (v "0.2.34") (d (list (d (n "libc") (r "^0.2.154") (o #t) (k 0)))) (h "0ybrj11vb3fsbzcxpm0bnngg6h8xh4qmglp6zhhr0l2ss09jwh56") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.35 (c (n "stak-device") (v "0.2.35") (d (list (d (n "libc") (r "^0.2.154") (o #t) (k 0)))) (h "0lif16039fad070z0569kbq8rz0k9k8d2b7dzkj56i7y5r4bixrp") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.36 (c (n "stak-device") (v "0.2.36") (d (list (d (n "libc") (r "^0.2.154") (o #t) (k 0)))) (h "0x4h6g5676rkcjicdqdb42b4h0228bqd3hjv03gs8gk376dvh2xb") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.37 (c (n "stak-device") (v "0.2.37") (d (list (d (n "libc") (r "^0.2.154") (o #t) (k 0)))) (h "0i0qsr3izn0nvn53fifjld5q3l86hb55p04y0n423n1b1fwcnbc7") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.38 (c (n "stak-device") (v "0.2.38") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "0iq951j2490bgj61kaprmkkp3innzlfx5ganrm886i4fk6bdwf8y") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-stak-device-0.2.39 (c (n "stak-device") (v "0.2.39") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "0v1d8rqyl4ywkrlrlidgz76lpqxxacc0bj9xyfg6l8rxzm4rdsvi") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

