(define-module (crates-io st ak stakker_async_await) #:use-module (crates-io))

(define-public crate-stakker_async_await-0.0.1 (c (n "stakker_async_await") (v "0.0.1") (d (list (d (n "futures-core") (r "^0.3.13") (d #t) (k 0)) (d (n "stakker") (r "^0.2.2") (d #t) (k 0)))) (h "0z8gzhjcx6jylx83hq86rd3kglwfi1p44314b51xy37103m065bz")))

