(define-module (crates-io st ak stakker_mio) #:use-module (crates-io))

(define-public crate-stakker_mio-0.0.1 (c (n "stakker_mio") (v "0.0.1") (d (list (d (n "mio") (r "^0.7.0-alpha.1") (f (quote ("os-poll" "os-util" "tcp" "udp" "uds"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.0") (d #t) (k 0)))) (h "1ahm7xmq55arvjs7nfslgzrbgfhnzxflizndvpk2q8mjq5ma0sbx")))

(define-public crate-stakker_mio-0.0.2 (c (n "stakker_mio") (v "0.0.2") (d (list (d (n "mio") (r "^0.7.0") (f (quote ("os-poll" "os-util" "tcp" "udp" "uds"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.0.2") (d #t) (k 0)))) (h "0qgvhpjnxc693y8nfp2dhrrb1yrzbn7w6kpjhqzn9vwhg0lxkkrp")))

(define-public crate-stakker_mio-0.1.0 (c (n "stakker_mio") (v "0.1.0") (d (list (d (n "mio") (r "^0.7.0") (f (quote ("os-poll" "os-util" "tcp" "udp" "uds"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.1.0") (d #t) (k 0)))) (h "0z4yvnxiqilx54sv38lwrxqa829al2bgw732pqxbnip3ghyb2xj1")))

(define-public crate-stakker_mio-0.1.1 (c (n "stakker_mio") (v "0.1.1") (d (list (d (n "mio") (r "^0.7.0") (f (quote ("os-poll" "os-util" "tcp" "udp" "uds"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.1") (d #t) (k 0)))) (h "15acc524ajfsm4jgiycy8v6wfr6pz1hxnnyxxdv85hlkd3sr7qpm")))

(define-public crate-stakker_mio-0.1.2 (c (n "stakker_mio") (v "0.1.2") (d (list (d (n "mio") (r "^0.7.0") (f (quote ("os-poll" "os-util" "tcp" "udp" "uds"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.1") (d #t) (k 0)))) (h "1yhgl4nawr7k99w0m13s0wa9xrdqa38chazdaxzbmfiavb4mzld8")))

(define-public crate-stakker_mio-0.1.3 (c (n "stakker_mio") (v "0.1.3") (d (list (d (n "mio") (r "^0.7.0") (f (quote ("os-poll" "os-util" "tcp" "udp" "uds"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.1") (d #t) (k 0)))) (h "0k903g7nqljf3wpnh9jg9xhj5yq78hhnpdaqm55i4razhshvdq5w")))

(define-public crate-stakker_mio-0.2.0 (c (n "stakker_mio") (v "0.2.0") (d (list (d (n "mio") (r "^0.7") (f (quote ("os-poll" "os-util" "tcp" "udp" "uds"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.2") (d #t) (k 0)))) (h "1lvkic4rd92d83r0hay05srwz2jnqclsfpd0irbarhc2jfz9cmrn")))

(define-public crate-stakker_mio-0.2.1 (c (n "stakker_mio") (v "0.2.1") (d (list (d (n "mio") (r "^0.7") (f (quote ("os-poll" "os-util" "tcp" "udp" "uds"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.2") (d #t) (k 0)))) (h "04ir2zavblj95bv3mv29wl4x8allh8cda519dbvi1nf44mzb0m0y")))

(define-public crate-stakker_mio-0.2.2 (c (n "stakker_mio") (v "0.2.2") (d (list (d (n "mio") (r "^0.7") (f (quote ("os-poll" "os-util" "tcp" "udp" "uds"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.2") (d #t) (k 0)))) (h "1wvxlhd5w7cavqkg115zjwj5kq5sd8lxwyfbklxv82l65v0wliz7")))

(define-public crate-stakker_mio-0.2.3 (c (n "stakker_mio") (v "0.2.3") (d (list (d (n "mio") (r "^0.7") (f (quote ("os-poll" "os-util" "tcp" "udp" "uds"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.2") (d #t) (k 0)))) (h "0m311y6kzl2zhw612y9l8w0hwnqwyz28ngsh3zyksg403db4x3l4")))

(define-public crate-stakker_mio-0.2.4 (c (n "stakker_mio") (v "0.2.4") (d (list (d (n "mio") (r "^0.8") (f (quote ("os-poll" "os-ext" "net"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.2") (d #t) (k 0)))) (h "14jh66v52qq72ch87jlmssnfxwhkklsbj3s7i91f2nik0w265zsl")))

(define-public crate-stakker_mio-0.2.5 (c (n "stakker_mio") (v "0.2.5") (d (list (d (n "mio") (r "^0.8") (f (quote ("os-poll" "os-ext" "net"))) (d #t) (k 0)) (d (n "rustls") (r "^0.20.8") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "stakker") (r "^0.2") (d #t) (k 0)))) (h "16dl4giwqjrcha9823nv0fiycgb91radf70n82smj0frdif4n58n")))

