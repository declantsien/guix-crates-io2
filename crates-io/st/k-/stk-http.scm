(define-module (crates-io st k- stk-http) #:use-module (crates-io))

(define-public crate-stk-http-0.2.0 (c (n "stk-http") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (d #t) (k 0)) (d (n "stk") (r "^0.2.0") (d #t) (k 0)) (d (n "stk-json") (r "^0.2.0") (d #t) (k 2)))) (h "1h2a9rwfj91j812d272ynddvcip005hw3fbfh9h79fjzzd79rqav")))

(define-public crate-stk-http-0.2.1 (c (n "stk-http") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (d #t) (k 0)) (d (n "stk") (r "^0.2.1") (d #t) (k 0)) (d (n "stk-json") (r "^0.2.1") (d #t) (k 2)))) (h "0srijz3fhb2327iihjpbqvxcnapw2py9h9dygd015zfyxpsnh5wm")))

