(define-module (crates-io st k- stk-json) #:use-module (crates-io))

(define-public crate-stk-json-0.2.0 (c (n "stk-json") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "stk") (r "^0.2.0") (d #t) (k 0)))) (h "14016axki2lvn6mna6ii6sb47z7qjdda4v5i0gycnwhjf50nfigi")))

(define-public crate-stk-json-0.2.1 (c (n "stk-json") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "stk") (r "^0.2.1") (d #t) (k 0)))) (h "0aznz2lygwbi3ji4a5h8kv35kllx9v8v4a4ibaqla45xcwxbb6j5")))

