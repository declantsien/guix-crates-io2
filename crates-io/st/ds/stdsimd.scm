(define-module (crates-io st ds stdsimd) #:use-module (crates-io))

(define-public crate-stdsimd-0.0.1 (c (n "stdsimd") (v "0.0.1") (h "0kvrp1nzdxas1nbx011apafyma4pl7kaapz467cafvzj3sswjr55")))

(define-public crate-stdsimd-0.0.2 (c (n "stdsimd") (v "0.0.2") (h "1gvhir9lnsf2xnvh700iaczfr80dl2h5agy1n4h5ac1ncvr7m2c8")))

(define-public crate-stdsimd-0.0.3 (c (n "stdsimd") (v "0.0.3") (d (list (d (n "cupid") (r "^0.3") (d #t) (k 2)))) (h "1q9vzs9hfnp04wfbwiwmc0qdi9a7y494h0v36sb0ms6qljafz13a") (f (quote (("strict"))))))

(define-public crate-stdsimd-0.0.4 (c (n "stdsimd") (v "0.0.4") (d (list (d (n "auxv") (r "^0.3.3") (d #t) (k 2)) (d (n "coresimd") (r "^0.0.4") (d #t) (k 0)))) (h "04hpss7s5xk8dbq9h3cd0gzb16816w0kahrahm45wk6i8409wmkk") (f (quote (("strict" "coresimd/strict") ("intel_sde" "coresimd/intel_sde"))))))

(define-public crate-stdsimd-0.1.0 (c (n "stdsimd") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "coresimd") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ga5y5d5j78x39ffjshqc35zqjgfxms9a050j8v6l4qh8i1ipwm2") (f (quote (("wasm_simd128" "coresimd/wasm_simd128") ("default"))))))

(define-public crate-stdsimd-0.1.1 (c (n "stdsimd") (v "0.1.1") (d (list (d (n "auxv") (r "^0.3.3") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "coresimd") (r "^0.1.1") (d #t) (k 0)) (d (n "cupid") (r "^0.6.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0qzcxckylsdxa6wdfl821zccad2srbvzyrg1hxwc2pz0wk7vylx1") (f (quote (("wasm_simd128" "coresimd/wasm_simd128") ("default"))))))

(define-public crate-stdsimd-0.1.2 (c (n "stdsimd") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "coresimd") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06mijlf0mvagbm30zy4xgm4nb830mciwbvik8afqkld7grgy287i") (f (quote (("wasm_simd128" "coresimd/wasm_simd128") ("default"))))))

