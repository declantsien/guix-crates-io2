(define-module (crates-io st am stamp-suite) #:use-module (crates-io))

(define-public crate-stamp-suite-0.1.0 (c (n "stamp-suite") (v "0.1.0") (h "08pskax5hc280jm1g29k11jdashwlg24pvfx99gq10dr103yjvqk")))

(define-public crate-stamp-suite-0.1.1 (c (n "stamp-suite") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1dhmd3qs0gvk5cmsy8mvdlamph7kl3m78kgnf2wb5k56m6ihp0fz")))

