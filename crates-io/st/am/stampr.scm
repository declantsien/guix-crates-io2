(define-module (crates-io st am stampr) #:use-module (crates-io))

(define-public crate-stampr-0.1.2 (c (n "stampr") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "handlebars") (r "^3.0.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "handlebars_misc_helpers") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "08s829j2dk4fzxk0j85fgm7rkfsl3yrlcid1skwbbxc93yb7pb9a")))

