(define-module (crates-io st am stamm) #:use-module (crates-io))

(define-public crate-stamm-0.1.0 (c (n "stamm") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0pjzixpdkh2q0bsd1jimmzdjmz2i1j58fyask29vpbq628vbnxy4")))

(define-public crate-stamm-0.1.1 (c (n "stamm") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0jpddpjrdqi3fkm4q17fp3bkvrawina4j0lz9cips6r6dkmb8c3y")))

(define-public crate-stamm-0.2.0 (c (n "stamm") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kpqi2lmnd0z33q91arjrd3b25dz2bq332dy98274nncxkcjifqg")))

