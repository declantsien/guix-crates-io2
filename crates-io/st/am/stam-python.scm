(define-module (crates-io st am stam-python) #:use-module (crates-io))

(define-public crate-stam-python-0.5.0 (c (n "stam-python") (v "0.5.0") (d (list (d (n "pyo3") (r "^0.20.2") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "stam") (r "^0.10.1") (d #t) (k 0)))) (h "1pv35v8c2dvb2cxj0a40q03l2m0pwk56nj7dipl93382ng56hq66") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-stam-python-0.6.0 (c (n "stam-python") (v "0.6.0") (d (list (d (n "pyo3") (r "^0.20.3") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "stam") (r "^0.11.0") (d #t) (k 0)))) (h "0r3g87313avfppf2j5dw3137kvkgky3xcfb1xw4bxbb9iy5rwh4w") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-stam-python-0.8.0 (c (n "stam-python") (v "0.8.0") (d (list (d (n "pyo3") (r "^0.20.3") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "stam") (r "^0.14.0") (d #t) (k 0)) (d (n "stam-tools") (r "^0.7.1") (d #t) (k 0)))) (h "0iw3m3vfhw67n7kcyx15p5qyh0r4p8f7y9ykzgqqgz3861gwsj22") (f (quote (("default" "pyo3/extension-module"))))))

(define-public crate-stam-python-0.8.1 (c (n "stam-python") (v "0.8.1") (d (list (d (n "pyo3") (r "^0.20.3") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "stam") (r "^0.14.0") (d #t) (k 0)) (d (n "stam-tools") (r "^0.7.1") (d #t) (k 0)))) (h "0nvryp9gz7dszany2r1h1m542sr4va8gishzhlzbr2sn5j7zwylh") (f (quote (("default" "pyo3/extension-module"))))))

