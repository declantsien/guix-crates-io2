(define-module (crates-io st am stamp-templates) #:use-module (crates-io))

(define-public crate-stamp-templates-0.1.0 (c (n "stamp-templates") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "handlebars") (r "^3.0.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "handlebars_misc_helpers") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "17qvrwc65kjm5nmhs04jrd0766yv8cvafv2mhdzvvk8brvv75pqv")))

(define-public crate-stamp-templates-0.1.1 (c (n "stamp-templates") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "handlebars") (r "^3.0.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "handlebars_misc_helpers") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0hpc59yn0mx7l0z90pms8c8nbpbvz2pk3hpivflh105x41yr8ha7")))

(define-public crate-stamp-templates-0.1.2 (c (n "stamp-templates") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "handlebars") (r "^3.0.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "handlebars_misc_helpers") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0yp415vgk5rm3i3x9mdkr2hldn7qxamhzczspglynm458w5yvn32")))

