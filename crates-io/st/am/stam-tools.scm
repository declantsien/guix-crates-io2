(define-module (crates-io st am stam-tools) #:use-module (crates-io))

(define-public crate-stam-tools-0.1.0 (c (n "stam-tools") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "stam") (r "^0.3.0") (d #t) (k 0)))) (h "0yagvrhxiy92iwr5am96sd15sc2vgxwsj28hx4qkszrvy77n77id")))

(define-public crate-stam-tools-0.1.1 (c (n "stam-tools") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "stam") (r "^0.4.0") (d #t) (k 0)))) (h "0w9ddycza8w1386368sxvjfri2khla2mvlij75jkqbi3akv798fg")))

(define-public crate-stam-tools-0.1.2 (c (n "stam-tools") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "stam") (r "^0.5.0") (d #t) (k 0)))) (h "1za9yk32nad67pay048arm8fy4dzp67cqyfwbdmxbrk2sjzan8gm")))

(define-public crate-stam-tools-0.1.3 (c (n "stam-tools") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "stam") (r "^0.6.0") (d #t) (k 0)))) (h "1sbqb3jnfnki07mlnjmlx1h1s0b3rf9mfmzfzs78485qyq6wz39q")))

(define-public crate-stam-tools-0.2.0 (c (n "stam-tools") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "stam") (r "^0.7.0") (d #t) (k 0)))) (h "00ajhv5ani9ylhz1nxlpwkhx1s1vjs48jabx8pk4x7hnwnyqil43")))

(define-public crate-stam-tools-0.3.0 (c (n "stam-tools") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "stam") (r "^0.8.0") (d #t) (k 0)))) (h "13x0jfc7rhc2rfv6vz2kqf2jcwlwmmaxgl4kywwyp25qf213svin")))

(define-public crate-stam-tools-0.4.0 (c (n "stam-tools") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "stam") (r "^0.9.0") (d #t) (k 0)))) (h "1ja703gjr9miv0a9aygxa4d3p71c0ds4al1xflm0zi2irw3cm07n")))

(define-public crate-stam-tools-0.5.0 (c (n "stam-tools") (v "0.5.0") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "seal") (r "^0.1.5") (d #t) (k 0)) (d (n "stam") (r "^0.10.1") (d #t) (k 0)))) (h "0xcngqzg4j6bzz21pqjr33fg2xln0z5jzksdkgq8jaq5y26bvxqc")))

(define-public crate-stam-tools-0.6.0 (c (n "stam-tools") (v "0.6.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "seal") (r "^0.1.5") (d #t) (k 0)) (d (n "stam") (r "^0.11.0") (d #t) (k 0)))) (h "13xjkcllkwa2d1g0h66hlys5ff7c9hpkx3r2zqgr0kbl2scydij9")))

(define-public crate-stam-tools-0.6.1 (c (n "stam-tools") (v "0.6.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "seal") (r "^0.1.5") (d #t) (k 0)) (d (n "stam") (r "^0.12.0") (d #t) (k 0)))) (h "0ld9g8sk0fax19gkh5p46b2lir562ccfhq70m8ih23sfz5f4j5qy")))

(define-public crate-stam-tools-0.7.0 (c (n "stam-tools") (v "0.7.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "seal") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stam") (r "^0.13.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0jsa8j86app6avkx1nkszhw7c7s6dcclsr7n4ilyp9lh8bws1rw6")))

(define-public crate-stam-tools-0.7.1 (c (n "stam-tools") (v "0.7.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "roxmltree") (r "^0.20.0") (d #t) (k 0)) (d (n "seal") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stam") (r "^0.14.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.13") (d #t) (k 0)))) (h "0s1kvg8qid508j837mb7f9c1b88g2m2cqkqx7ina96wk2mfffgwv")))

(define-public crate-stam-tools-0.7.2 (c (n "stam-tools") (v "0.7.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "roxmltree") (r "^0.20.0") (d #t) (k 0)) (d (n "seal") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stam") (r "^0.14.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.13") (d #t) (k 0)))) (h "1bw2mbc8rbm584nycjdkdr8ffm6bqj2pj2gz9a4a37fk595k8jhl")))

