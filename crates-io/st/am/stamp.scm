(define-module (crates-io st am stamp) #:use-module (crates-io))

(define-public crate-stamp-0.1.0 (c (n "stamp") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1nisiymwq1hl5l78bakfphzdqizxflf290scmip00xdgii05g6nj")))

