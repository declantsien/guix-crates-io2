(define-module (crates-io st am stama) #:use-module (crates-io))

(define-public crate-stama-1.0.0 (c (n "stama") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (f (quote ("unstable-rendered-line-info"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "093p7d34bkrxkpqr28aajmws58il4xkk3f7kaazwksmq8h61nwys")))

