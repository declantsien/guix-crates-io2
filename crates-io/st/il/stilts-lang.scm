(define-module (crates-io st il stilts-lang) #:use-module (crates-io))

(define-public crate-stilts-lang-0.1.0 (c (n "stilts-lang") (v "0.1.0") (d (list (d (n "miette") (r "^5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06lcj01jrwaxcbp5g8n9jzm19afcrynbgc40wj5x1xfmjzrphji4")))

(define-public crate-stilts-lang-0.1.2 (c (n "stilts-lang") (v "0.1.2") (d (list (d (n "miette") (r "^5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0c0max6c82f771blw0y8lc0fi6s66aiinc1yx97hbmr6gj9gzv1q")))

(define-public crate-stilts-lang-0.2.0 (c (n "stilts-lang") (v "0.2.0") (d (list (d (n "miette") (r "^5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0h5q4xzgn0ns74qihahcd5b46a50lq76b87gv3xh1d8mrr42l00g")))

