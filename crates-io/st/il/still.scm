(define-module (crates-io st il still) #:use-module (crates-io))

(define-public crate-still-0.0.1 (c (n "still") (v "0.0.1") (d (list (d (n "actix-files") (r "^0.6.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.80") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gd9isv0kksqy00ixf0z8yrcqmakf3x1wm337kppdkfp5fngz5a4")))

