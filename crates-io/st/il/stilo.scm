(define-module (crates-io st il stilo) #:use-module (crates-io))

(define-public crate-stilo-0.1.0 (c (n "stilo") (v "0.1.0") (h "16jzhgbb8y45df6iibmj8yfnk3h7zr2nzvcrccsh825989ypi993")))

(define-public crate-stilo-0.1.1 (c (n "stilo") (v "0.1.1") (h "0i8s1mbsr1s3hqm2c1ihchlpiv87gbss62wc99ysi3zd5za7yaab")))

(define-public crate-stilo-0.2.0 (c (n "stilo") (v "0.2.0") (h "1rpl8p1rcnkm2df3aca2r2hdckj1zxlhvqwbwf9x2mac092cg26p")))

(define-public crate-stilo-0.2.1 (c (n "stilo") (v "0.2.1") (h "1i66aqas2k6ijhfm9r8f6fnfkdq1lysqz7vnsny4cmkap6lx2pbs")))

(define-public crate-stilo-0.2.2 (c (n "stilo") (v "0.2.2") (h "17p4lhj38ra4i1v87d4m2ny4qc9iyimk3ida22n7q6n2a9gmmc8h")))

(define-public crate-stilo-0.3.0 (c (n "stilo") (v "0.3.0") (h "1vs7f0h0l4brhq94b6p7fiv5ysm27pqzg86f48100zbwsdfzb4xp")))

(define-public crate-stilo-0.3.1 (c (n "stilo") (v "0.3.1") (h "1q46mbiqh2gdy0cwgja0amqy226f46gvhqvccjh61anjw5gz04kj")))

(define-public crate-stilo-0.3.2 (c (n "stilo") (v "0.3.2") (h "0d33gjblcqq9mqs0xih902gzn44d0za9zvc925xjqlq8hwygz80x")))

