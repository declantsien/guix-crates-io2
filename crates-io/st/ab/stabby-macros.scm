(define-module (crates-io st ab stabby-macros) #:use-module (crates-io))

(define-public crate-stabby-macros-0.1.0 (c (n "stabby-macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r5i64csv9ikm99h48s5yd5wlxqyryh65hw593klyagi9abd9lvi") (y #t)))

(define-public crate-stabby-macros-0.1.1 (c (n "stabby-macros") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0swiy09508q3gg1avsfw0nzm8lfk8cx4a1qccvi1my0zd41x61b8") (y #t)))

(define-public crate-stabby-macros-0.1.2 (c (n "stabby-macros") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iydkdqzgwiqgpj11vknhhq1wf8aqc8l1nvkpz10npmnvb41fjx7") (y #t)))

(define-public crate-stabby-macros-0.1.3 (c (n "stabby-macros") (v "0.1.3") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r4whc1q2c2ayvdwh5b7cjch1iafpwg8x86i1a4p0wa2vd9pi7wx")))

(define-public crate-stabby-macros-0.1.4 (c (n "stabby-macros") (v "0.1.4") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i39aasxmabyj1kmm1gj8f4r68f1s3rg36vbiwpa39vbb27i4g1k")))

(define-public crate-stabby-macros-1.0.0 (c (n "stabby-macros") (v "1.0.0") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nd6p3p61igwk536byyi3prrz6fvg7a1zsqhgnr484h87ynli5s0")))

(define-public crate-stabby-macros-1.0.1 (c (n "stabby-macros") (v "1.0.1") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04abpjvfik6vii1m9cms7dnk1a6svwaxyjqbw0yi8h4mkhaawn2j")))

(define-public crate-stabby-macros-1.0.2 (c (n "stabby-macros") (v "1.0.2") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i7r0xr7r5zzgphankswdfid0364svdfnpg8qv5cdji1x3nwi3vq")))

(define-public crate-stabby-macros-1.0.3 (c (n "stabby-macros") (v "1.0.3") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fax5sh3n0d97gp4vfzayghr2lsgc5j955505kia3mw16d34i1lx")))

(define-public crate-stabby-macros-1.0.4 (c (n "stabby-macros") (v "1.0.4") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sdnrmprfiazj946di4avxrwk6hpc76mhgy9apznl7gh0k7jmjv1")))

(define-public crate-stabby-macros-1.0.5 (c (n "stabby-macros") (v "1.0.5") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0frfcfdign74r8ji0nqhpyyapf4bz0kpjskbff1r4ppjm6ns09xc")))

(define-public crate-stabby-macros-1.0.6 (c (n "stabby-macros") (v "1.0.6") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02a40mynrydi81giabkbrsa6227rbnhv99d81plbf4av7ffb0x3p")))

(define-public crate-stabby-macros-1.0.7 (c (n "stabby-macros") (v "1.0.7") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a7r9g74gawz4vy2mi6v1ip8rvw7c02rzlv85miyqn47ng529nps")))

(define-public crate-stabby-macros-1.0.8 (c (n "stabby-macros") (v "1.0.8") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nwpq0igrd0g6aj6la6v0wij5fp3phazydpn17mq9gwlv12qcq3c")))

(define-public crate-stabby-macros-1.0.9 (c (n "stabby-macros") (v "1.0.9") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l9m8ks84wnnygqnh9bma7lfnmwnys8jrk79xjd94chgsilrbgrc")))

(define-public crate-stabby-macros-1.0.10 (c (n "stabby-macros") (v "1.0.10") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j5m45hm68m2cy64fcx3f58hy8p4cp2mnx71vsw8ghw74n588r7m")))

(define-public crate-stabby-macros-2.0.0 (c (n "stabby-macros") (v "2.0.0") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06cywikv8jndsm0j4mxfx1vc8xm334na7vjpanlgkrsrss8nyikh")))

(define-public crate-stabby-macros-2.0.1 (c (n "stabby-macros") (v "2.0.1") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x5j2wjc3hqgyk33g53hzvyac0iawz5qhxakk0zhw284i732458z")))

(define-public crate-stabby-macros-3.0.0 (c (n "stabby-macros") (v "3.0.0") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wsh823zn6f6cvjlfay69dbyx79wmkl72d8dgza9r65h27sjqf20")))

(define-public crate-stabby-macros-3.0.1 (c (n "stabby-macros") (v "3.0.1") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rs2fb49yvjzcnglm63r4b8vvca343qjkacr5ndjpkyn3qrxk6zl")))

(define-public crate-stabby-macros-3.0.2 (c (n "stabby-macros") (v "3.0.2") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07kax64qvv8qwazi6k4qp3kdsn35vg7q74fgnw2glrccf9smm57p")))

(define-public crate-stabby-macros-3.0.3-rc (c (n "stabby-macros") (v "3.0.3-rc") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vydpb9gz63v4a9k5m7wfwv8rx2j0arnix9rqjvl2f86slinmgxb")))

(define-public crate-stabby-macros-3.0.3-rc1 (c (n "stabby-macros") (v "3.0.3-rc1") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kz5rkbazd2fmsgdpnjwc0vaqfxdjz4mav3chwf85pyz1q6bb27p")))

(define-public crate-stabby-macros-3.0.3-rc2 (c (n "stabby-macros") (v "3.0.3-rc2") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0298z3smm5z4y3sga1vss6z6wmgcwj66vpcq4fj1jc4g9zgm53k8")))

(define-public crate-stabby-macros-3.0.3 (c (n "stabby-macros") (v "3.0.3") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c427yc5gg0v82wisysimizdccx3h3c1a1ia8kfpl15zx1crzan6")))

(define-public crate-stabby-macros-4.0.0 (c (n "stabby-macros") (v "4.0.0") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gi8gf6kw8b1m2zpy1h27pc31dnq6zblgwyz1712bpiypqjsc7ng")))

(define-public crate-stabby-macros-4.0.1 (c (n "stabby-macros") (v "4.0.1") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cgmnf3k8qaj1wnhys57yr0dhlx9f23xvjm9jy9iblmichcnm8d7")))

(define-public crate-stabby-macros-4.0.2 (c (n "stabby-macros") (v "4.0.2") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17vg6yw15n33vbbszx6m409fs3qqa7kdy2i9jc5fzkrp27a4kdlk")))

(define-public crate-stabby-macros-4.0.3 (c (n "stabby-macros") (v "4.0.3") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "180367cybzfnj880lfkc2ljx53x1rg00ikw02dcmds9xrw2dxzg3")))

(define-public crate-stabby-macros-4.0.4 (c (n "stabby-macros") (v "4.0.4") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ghk0mfbmp32bl702hzniiycz2vnm62qbv9n5xl1xmyk1f5ng3my")))

(define-public crate-stabby-macros-4.0.5 (c (n "stabby-macros") (v "4.0.5") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10wijc2xdnfmicqdgx1s0cqwpfsilzy1x0vkw2k1yyxk7ahi4rvk")))

(define-public crate-stabby-macros-5.0.0 (c (n "stabby-macros") (v "5.0.0") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sm5s19c69bv4pl1ggp50x6rxsr8dy8i82678sg0p0p2y6kxcb6w")))

(define-public crate-stabby-macros-5.0.1 (c (n "stabby-macros") (v "5.0.1") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xd2b01s0svh00x8mg1jkl5siis61p9kh0gs12d95fgs04qvv5yv")))

