(define-module (crates-io st ab stable-alloc-shim) #:use-module (crates-io))

(define-public crate-stable-alloc-shim-0.57.0 (c (n "stable-alloc-shim") (v "0.57.0") (h "0p2nwk20c1h0xf2p6c08a547d4vjfgyhw42dfk1svaab4gnmxkcq") (y #t)))

(define-public crate-stable-alloc-shim-0.64.0 (c (n "stable-alloc-shim") (v "0.64.0") (h "1d8x9wxsh7pbryrrgk8v6gcicqdfj2r343l61y9rmw9nx7c9bmhy")))

