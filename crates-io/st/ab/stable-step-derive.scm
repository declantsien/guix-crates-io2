(define-module (crates-io st ab stable-step-derive) #:use-module (crates-io))

(define-public crate-stable-step-derive-0.1.0 (c (n "stable-step-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01165ljbwn2yvclyrrhp9gd1qbarp5809skkf0zi0v8c0482c6aw")))

