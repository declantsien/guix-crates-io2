(define-module (crates-io st ab stable-skiplist) #:use-module (crates-io))

(define-public crate-stable-skiplist-0.1.0 (c (n "stable-skiplist") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "1vivzmxm7sg8k9j4fqdv8n6pmmsaryr8vfg8qs5ghqcghlik57is") (f (quote (("unstable"))))))

(define-public crate-stable-skiplist-0.1.1 (c (n "stable-skiplist") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "0myly8p9sx96x0qg0k6m9x3248b6v9kajh0x6zm339ka74dll9r5") (f (quote (("unstable"))))))

