(define-module (crates-io st ab stable-swap-math) #:use-module (crates-io))

(define-public crate-stable-swap-math-1.2.0 (c (n "stable-swap-math") (v "1.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.0.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "0jgi1sisjwxzhkvjll814dqi2y55gln0p1xd03xb6hinf7zrpvd9")))

(define-public crate-stable-swap-math-1.3.0 (c (n "stable-swap-math") (v "1.3.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.0.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "058df65x27ahvyrp6pq93jn8r8k7rddgi8hmccgxawkswi2pflpn")))

(define-public crate-stable-swap-math-1.4.0 (c (n "stable-swap-math") (v "1.4.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.0.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "1346sh2sqzwhfc6xmn98bgmsb9aiv24is67r8j59nhhy0ahlkgb5")))

(define-public crate-stable-swap-math-1.4.1 (c (n "stable-swap-math") (v "1.4.1") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.0.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "151nlr0ma9rm2ha47kcvy7i0zb2iz0n3nnqlndmkhizfycd9b52s")))

(define-public crate-stable-swap-math-1.4.2 (c (n "stable-swap-math") (v "1.4.2") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.0.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "1cplj859abkx5j4661336zid14vkiqhaxb7533rn964l6yih5715")))

(define-public crate-stable-swap-math-1.5.3 (c (n "stable-swap-math") (v "1.5.3") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1.0.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "0s63gvahrsfhi2hydrqblgfmpfmm5bwk23q4299f7sa7n8q42xar")))

(define-public crate-stable-swap-math-1.5.5 (c (n "stable-swap-math") (v "1.5.5") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "0j964dci2jgwxxpv7sr84p07hq2hdzk619yw26h8lns8hc0qg4nd")))

(define-public crate-stable-swap-math-1.5.6 (c (n "stable-swap-math") (v "1.5.6") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "06qgkl0hdvcrkq5zxdljkzrfm17syxh2x51lp17la8xgn5m38fg2")))

(define-public crate-stable-swap-math-1.5.7 (c (n "stable-swap-math") (v "1.5.7") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "18dspns6h80al53xsq0q59047zzh5xgrz4fm78fi8hhcdr2qqbn4")))

(define-public crate-stable-swap-math-1.6.0 (c (n "stable-swap-math") (v "1.6.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "014i01bd260xfa58czys1idf27qs1qs314xz0ny414ppci39jjgm")))

(define-public crate-stable-swap-math-1.6.6 (c (n "stable-swap-math") (v "1.6.6") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "stable-swap-sim") (r "^0.1") (d #t) (k 2)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "1ipzkkj3g2agdnyg1kv9b5j964c14jcmay9zz02rqp9ayl0b901i")))

(define-public crate-stable-swap-math-1.6.7 (c (n "stable-swap-math") (v "1.6.7") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "stable-swap-sim") (r "^0.1") (d #t) (k 2)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "12jd375swqsv5smm553v8wsh0gxrfs376km5bizf05gmnm1vx0l6")))

(define-public crate-stable-swap-math-1.6.8 (c (n "stable-swap-math") (v "1.6.8") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "stable-swap-sim") (r "^0.1") (d #t) (k 2)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "0bsd74npzv8mbsn6g6895qlz3hznz515gbarmlp7k1n5vlnqn210")))

(define-public crate-stable-swap-math-1.6.9 (c (n "stable-swap-math") (v "1.6.9") (d (list (d (n "borsh") (r "^0.9.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "stable-swap-sim") (r "^0.1") (d #t) (k 2)) (d (n "uint") (r "=0.9.1") (k 0)))) (h "0nrd0ma02jjir85fbry864jdd3fgiwj3anw7kd95af5cnsj0mg04")))

(define-public crate-stable-swap-math-1.7.0 (c (n "stable-swap-math") (v "1.7.0") (d (list (d (n "borsh") (r "^0.9.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "stable-swap-sim") (r "^0.1") (d #t) (k 2)) (d (n "uint") (r "=0.9.1") (k 0)))) (h "0rwlapbr3wg99w1r091k22sz84qjm2w51lrr55yya5cnzxrzzsa6")))

(define-public crate-stable-swap-math-1.7.1 (c (n "stable-swap-math") (v "1.7.1") (d (list (d (n "borsh") (r "^0.9.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "stable-swap-sim") (r "^0.1") (d #t) (k 2)) (d (n "uint") (r "=0.9.1") (k 0)))) (h "0yqjg5pq7ic09hycflqgz616d4kc3gbsxyph14578vzhngwzv65a")))

(define-public crate-stable-swap-math-1.7.2 (c (n "stable-swap-math") (v "1.7.2") (d (list (d (n "borsh") (r "^0.9.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "stable-swap-sim") (r "^0.1") (d #t) (k 2)) (d (n "uint") (r "=0.9.1") (k 0)))) (h "13p7xffwgm5iw4fc69cigq4mw405zwpavkn4j7b5fkm83rx94bsi")))

(define-public crate-stable-swap-math-1.7.3 (c (n "stable-swap-math") (v "1.7.3") (d (list (d (n "borsh") (r "^0.9.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "stable-swap-sim") (r "^0.1") (d #t) (k 2)) (d (n "uint") (r "=0.9.1") (k 0)))) (h "0jv8ypb94r0vswpsfbkyp2p0sq01qan5qiknl12fhqlp42wdqmh8")))

(define-public crate-stable-swap-math-1.8.0 (c (n "stable-swap-math") (v "1.8.0") (d (list (d (n "borsh") (r "^0.9.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "stable-swap-sim") (r "^0.1") (d #t) (k 2)) (d (n "uint") (r "=0.9.1") (k 0)))) (h "0s8kbhjv06galcdb3i3qx772qnflfhpj5qwk7df20qcmi2708bs4")))

(define-public crate-stable-swap-math-1.8.1 (c (n "stable-swap-math") (v "1.8.1") (d (list (d (n "borsh") (r "^0.9.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "stable-swap-sim") (r "^0.1") (d #t) (k 2)) (d (n "uint") (r "=0.9.1") (k 0)))) (h "049k1p6rnfgyxnplccr7kvk7n48sgzp2vcq3r33c82r5c9fvn0pl")))

