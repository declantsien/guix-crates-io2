(define-module (crates-io st ab stable-typeid-macro) #:use-module (crates-io))

(define-public crate-stable-typeid-macro-0.0.1 (c (n "stable-typeid-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "18lfjfs2k0w4z8hikjfj2cx8n1nyl0j35v6mswnpakpr8ljyn26b")))

