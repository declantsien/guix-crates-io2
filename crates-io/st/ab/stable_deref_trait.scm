(define-module (crates-io st ab stable_deref_trait) #:use-module (crates-io))

(define-public crate-stable_deref_trait-1.0.0 (c (n "stable_deref_trait") (v "1.0.0") (h "0azh2rrnbv97pydwl09aspsxds4vafmy60icbs610j226q72w4qm") (f (quote (("std") ("default" "std"))))))

(define-public crate-stable_deref_trait-1.1.0 (c (n "stable_deref_trait") (v "1.1.0") (h "1ymkqdn2dbn4c6nkkiyqyk15l22l6z06rx1fn6cgbr9g15p5kg7z") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-stable_deref_trait-1.1.1 (c (n "stable_deref_trait") (v "1.1.1") (h "1j2lkgakksmz4vc5hfawcch2ipiskrhjs1sih0f3br7s7rys58fv") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-stable_deref_trait-1.2.0 (c (n "stable_deref_trait") (v "1.2.0") (h "1lxjr8q2n534b2lhkxd6l6wcddzjvnksi58zv11f9y0jjmr15wd8") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

